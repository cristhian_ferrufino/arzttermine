# java-library

... providing some useful utilities and reusable code to prevent code-duplication and to minimize the project setup.

Read more about [Java-Setup](https://github.com/arzttermine/documentation/wiki/Service-Setup) or [java in our environment](https://github.com/arzttermine/documentation/wiki/Java-in-Detail).
You can pull modules from this project from our [nexus repository](https://nexus.arzttermine.de/#browse/browse/components:maven-releases)

#### Currently provided modules:
0. [elasticsearch](/elasticsearch) (2.2.0) -> provides possibility to index, delete and update documents to elasticsearch
0. [support](/support) (1.0.0) -> reusable code and useful utilities
0. [spring](/spring) (1.1.0) -> reusable and standardized spring support
0. [web](/web) (1.0.0) -> shared DTOs or commonly used classes
0. [client](/client) (1.0.1) -> can be used to define and request external APIs
0. [messaging](/messaging) (1.0.2) -> can be used to publish and consume messages via AMQP

You can find the latest version in the `module/pom.xml`

#### Dependencies
Most of our module dependencies are only available during testing or compilation time.
If you discover ClassDefNotFound-exception there might be a missing dependency which has to be provided by your application.
