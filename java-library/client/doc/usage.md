# client

### Creating a operation
```java
public class GetAppointmentOperation extends Operation.Builder<AppointmentDto>
{
    @Override
    public Operation<AppointmentDto> build() {
        setPath("/appointments/{id}");
        addParameter(new Parameter("id"));

        setResponseTypeClass(AppointmentDto.class);

        return new Operation<>(this);
    }
}
```

### Creating a client

```java
@Component
public class AppointmentClient extends ClientAdapter
{
    public AppointmentClient(AppointmentClientSettings settings) {
        super(settings.getHost(), settings.getPort(), settings.getScheme());
    }

    public AppointmentDto getAppointment(final long id, final RequestOptions options)
    {
        GetAppointmentOperation operation = new GetAppointmentOperation();
        operation.set(Parameter.Type.PATH, "id", String.valueOf(id));

        return execute(operation, options);
    }
}
```

### Usage
```java
@Service
public class Service
{
    @Autowired
    private AppointmentClient client;
    
    public AppointmentDto get(long id)
    {
        RequestOptions options = new RequestOptions();
        options.setAuthorization(UUID.randomUUID());
        
        return client.getAppointment(id, options);
    }
}
```
