# client

This module allows you to define and request external APIs with less configuration and ease of use.

### Usage
```xml
<dependency>
    <groupId>com.arzttermine.library</groupId>
    <artifactId>client</artifactId>
    <version>${library-client.version}</version>
</dependency>

<!-- not included dependency -->
<dependency>
    <groupId>com.arzttermine.library</groupId>
    <artifactId>support</artifactId>
    <version>${library-support.version}</version>
</dependency>

<!-- not included dependency -->
<dependency>
    <groupId>com.arzttermine.library</groupId>
    <artifactId>web</artifactId>
    <version>${library-web.version}</version>
</dependency>
```

Define your [clients and operations](doc/usage.md)
