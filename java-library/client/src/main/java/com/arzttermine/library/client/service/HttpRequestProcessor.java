package com.arzttermine.library.client.service;

import com.arzttermine.library.client.model.Operation;
import com.arzttermine.library.client.model.param.Parameter;
import com.arzttermine.library.client.support.visitor.ResponseVisitor;
import com.arzttermine.library.suport.exception.ResponseErrorException;
import com.arzttermine.library.suport.exception.ServiceUnavailableException;
import com.arzttermine.library.web.struct.HttpStatus;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class HttpRequestProcessor implements RequestProcessor
{
    private static final Logger log = LoggerFactory.getLogger(HttpRequestProcessor.class);
    private static final MediaType CONTENT_TYPE = MediaType.parse("application/json; charset=UTF-8");

    private final ObjectMapper objectMapper;
    private final Collection<ResponseVisitor> visitors;
    private final OkHttpClient client;

    public HttpRequestProcessor(OkHttpClient client, List<ResponseVisitor> visitors, ObjectMapper objectMapper) {
        this.client = client;
        this.visitors = visitors;
        this.objectMapper = objectMapper;
    }

    @Override
    public <T> T request(final String target, final Operation<T> operation) {
        final Request.Builder builder = new Request.Builder().url(target);
        RequestBody requestBody = null;

        if (operation.hasRequestBody()) {
            try {
                requestBody = RequestBody.create(CONTENT_TYPE, objectMapper.writeValueAsBytes(operation.getBody()));
            } catch (JsonProcessingException e) {
                log.error("could not serialize request body", e);
                throw new ResponseErrorException(null, 400);
            }
        }

        builder.method(operation.getMethod().name(), requestBody);

        operation.getParameters().stream().filter(p -> p.getType().equals(Parameter.Type.HEADER))
            .forEach(h -> builder.addHeader(h.getName(), h.getValue()));

        log.debug("sending request to external service {}", operation.getEndpoint());
        final Response response;

        try {
            response = client.newCall(builder.build()).execute();
        } catch (IOException e) {
            log.error("http request to {} failed or timed out", operation.getEndpoint(), e);
            throw new ServiceUnavailableException(operation.getEndpoint());
        }

        log.debug("got response from request to {}", operation.getEndpoint());

        return treatResponse(operation, response);
    }

    private <T> T treatResponse(final Operation<T> operation, final Response response)
    {
        if (response.code() == HttpStatus.NO_CONTENT.getCode() || null == response.body()) {
            return null;
        }

        final List<ResponseVisitor> allVisitors = new ArrayList<>(visitors);
        allVisitors.addAll(operation.getVisitors());

        allVisitors.stream().filter(v -> v.supportsCode(response.code()))
            .forEach(v -> v.visit(operation, response));

        if (null == operation.getResponseTypeClass()) {
            return null;
        }

        try {
            return objectMapper.readValue(response.body().bytes(), operation.getResponseTypeClass());
        } catch (IOException e) {
            log.error("error while reading from upstream {} or invalid response detected", operation.getEndpoint(), e);
            throw new ResponseErrorException(null, 500);
        }
    }
}
