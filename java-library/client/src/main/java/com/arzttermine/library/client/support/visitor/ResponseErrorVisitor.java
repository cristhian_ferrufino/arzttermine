package com.arzttermine.library.client.support.visitor;

import com.arzttermine.library.client.model.Operation;
import com.arzttermine.library.suport.exception.*;
import com.arzttermine.library.web.dto.response.ErrorDto;
import com.arzttermine.library.web.struct.HttpStatus;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class ResponseErrorVisitor implements ResponseVisitor
{
    private static final Logger log = LoggerFactory.getLogger(ResponseErrorVisitor.class);

    private final ObjectMapper objectMapper;

    public ResponseErrorVisitor(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public <T> void visit(Operation<T> operation, Response response) {
        if (response.isSuccessful() || null != operation.getErrorType() || null == response.body()) {
            return; // custom error or noop handling is required
        }

        final List<ErrorDto> errors;

        try {
            errors = Arrays.asList(objectMapper.readValue(response.body().bytes(), ErrorDto[].class));
        } catch (IOException e) {
            log.warn("could not read errors from response or connection closed unexpected", e);
            throw new ResponseErrorException(null, response.code());
        }

        final HttpStatus code = HttpStatus.fromCode(response.code())
            .orElse(null);

        switch (code) {
            case NOT_FOUND:
                throw new EntityNotFoundException(errors.get(0));
            case FORBIDDEN:
            case UNAUTHORIZED:
                throw new AccessDeniedException();
            case CONFLICT:
                errors.stream().filter(e -> e.getCode().equals("ALREADY_EXISTS")).findFirst().ifPresent(e -> {
                    throw new EntityAlreadyExistException(e);
                });
        }

        throw new ResponseErrorListException(errors, code);
    }
}
