package com.arzttermine.library.client.config;

import com.arzttermine.library.client.config.settings.HttpClientSettings;
import com.arzttermine.library.client.service.HttpRequestProcessor;
import com.arzttermine.library.client.service.RequestProcessor;
import com.arzttermine.library.client.support.visitor.CustomResponseErrorVisitor;
import com.arzttermine.library.client.support.visitor.ResponseErrorVisitor;
import com.arzttermine.library.client.support.visitor.ResponseVisitor;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.OkHttpClient;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

public abstract class RequestProcessorConfigurer
{
    @Autowired
    private ObjectMapper objectMapper;

    public RequestProcessor requestSender(HttpClientSettings settings, List<ResponseVisitor> additionalVisitors)
    {
        final OkHttpClient.Builder builder = new OkHttpClient.Builder()
            .followRedirects(false)
            .followSslRedirects(true)
            .connectTimeout(settings.getConnectTimeout(), TimeUnit.MILLISECONDS)
            .writeTimeout(settings.getWriteTimeout(), TimeUnit.MILLISECONDS)
            .readTimeout(settings.getReadTimeout(), TimeUnit.MILLISECONDS);

        final OkHttpClient client = configure(builder).build();

        final List<ResponseVisitor> visitors = Arrays.asList(
            new ResponseErrorVisitor(objectMapper),
            new CustomResponseErrorVisitor(objectMapper)
        );

        if (null != additionalVisitors) {
            visitors.addAll(additionalVisitors);
        }

        return new HttpRequestProcessor(client, Collections.unmodifiableList(visitors), objectMapper);
    }

    protected OkHttpClient.Builder configure(OkHttpClient.Builder client) {
        return client;
    }
}
