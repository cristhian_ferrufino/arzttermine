package com.arzttermine.library.client.model;

public class Paging
{
    private final int page;
    private final int size;

    Paging(int page, int size) {
        if (size > 500) {
            throw new IllegalArgumentException(size + " is too big");
        }

        this.page = page;
        this.size = size;
    }

    public int getPage() {
        return page;
    }

    public int getSize() {
        return size;
    }
}
