package com.arzttermine.library.client.model.param;

public class QueryParameter extends Parameter
{
    public QueryParameter(String name, Object value) {
        super(Type.QUERY, name);
        setValue((null == value) ? null : String.valueOf(value));
    }
}
