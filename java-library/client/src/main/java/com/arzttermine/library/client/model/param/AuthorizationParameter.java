package com.arzttermine.library.client.model.param;

import java.util.UUID;

public final class AuthorizationParameter extends Parameter
{
    public AuthorizationParameter(UUID token) {
        super(Type.HEADER, "Authorization");
        setValue((null == token) ? null : String.format("Bearer %s", token));
    }
}
