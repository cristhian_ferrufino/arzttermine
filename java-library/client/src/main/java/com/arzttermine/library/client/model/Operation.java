package com.arzttermine.library.client.model;

import com.arzttermine.library.client.model.param.Parameter;
import com.arzttermine.library.client.model.struct.RequestMethod;
import com.arzttermine.library.client.support.visitor.ResponseVisitor;

import java.util.*;

public final class Operation<ResponseType>
{
    private final String path;
    private final String endpoint;
    private final RequestMethod method;
    private final Class<ResponseType> responseTypeClass;
    private final Class<? extends Object> errorType;
    private final List<ResponseVisitor> visitors;
    private final List<Parameter> parameters;
    private final Object body;

    public Operation(final Builder<ResponseType> builder) {
        path = builder.path;
        method = builder.method;
        responseTypeClass = builder.responseTypeClass;
        errorType = builder.errorType;
        visitors = builder.visiters;
        parameters = builder.parameters;
        body = builder.body;
        endpoint = path.split("/", 1)[0];
    }

    public String getEndpoint() {
        return endpoint;
    }

    public Object getBody() {
        return body;
    }

    public boolean hasRequestBody() {
        return null != body;
    }

    public List<Parameter> getParameters() {
        return parameters;
    }

    public RequestMethod getMethod() {
        return method;
    }

    public Class<ResponseType> getResponseTypeClass() {
        return responseTypeClass;
    }

    public List<ResponseVisitor> getVisitors() {
        return visitors;
    }

    public String getPath() {
        return path;
    }

    public Class<? extends Object> getErrorType() {
        return errorType;
    }

    public static abstract class Builder<ResponseType>
    {
        private String path;
        private RequestMethod method = RequestMethod.GET;
        private Class<ResponseType> responseTypeClass;
        private Class<? extends Object> errorType;
        private final List<ResponseVisitor> visiters = new LinkedList<>();
        private final List<Parameter> parameters = new ArrayList<>();
        private Object body;

        public abstract Operation<ResponseType> build();

        public Builder<ResponseType> setRequestBody(Object body) {
            this.body = body;
            return this;
        }

        public Builder<ResponseType> set(Parameter.Type type, String key, Collection<String> values) {
            final boolean set = values.stream().map(v -> parameters.stream()
                .filter(p -> p.getType().equals(type))
                .filter(p -> p.getName().equals(key))
                .filter(p -> null == p.getValue())
                .findFirst()
                .map(param -> { param.setValue(v); return param; }))
                .anyMatch(Optional::isPresent);

            if (!set) {
                values.forEach(v -> parameters.add(new Parameter(type, key, v)));
            }

            return this;
        }

        public Builder<ResponseType> set(Parameter.Type type, String key, String value) {
            set(type, key, Collections.singletonList(value));
            return this;
        }

        public Builder<ResponseType> addParameter(Parameter parameter) {
            if (null != parameter.getValue()) {
                Optional<Parameter> existing = parameters.stream()
                    .filter(p -> p.getName().equals(parameter.getName()))
                    .filter(p -> null == p.getValue())
                    .findFirst();

                if (existing.isPresent()) {
                    existing.get().setValue(parameter.getValue());
                    return this;
                }
            }

            parameters.add(parameter);
            return this;
        }

        public Builder<ResponseType> addVisiter(ResponseVisitor visiter) {
            visiters.add(visiter);
            return this;
        }

        protected Builder<ResponseType> setMethod(RequestMethod method) {
            this.method = method;
            return this;
        }

        protected Builder<ResponseType> setPath(String path) {
            this.path = path;
            return this;
        }

        protected Builder<ResponseType> setResponseTypeClass(Class<ResponseType> typeClass) {
            this.responseTypeClass = typeClass;
            return this;
        }

        public Builder<ResponseType> setErrorType(Class<? extends Object> errorType) {
            this.errorType = errorType;
            return this;
        }
    }
}
