package com.arzttermine.library.client.model;

import com.arzttermine.library.client.model.param.AuthorizationParameter;
import com.arzttermine.library.client.model.param.Parameter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.*;
import java.util.stream.Collectors;

public class RequestOptions
{
    private final MultiValueMap<Parameter.Type, Parameter> parameters = new LinkedMultiValueMap<>();
    private Paging paging;

    public void addParameter(final Parameter parameter) {
        parameters.compute(parameter.getType(), (type, params) -> {
            if (null == params) {
                params = new ArrayList<>();
            }

            params.add(parameter);
            return params;
        });
    }

    public void setPaging(int page, int size) {
        paging = new Paging(page, size);
    }

    public boolean hasPaging() {
        return null != paging;
    }

    public Paging getPaging() {
        return paging;
    }

    public void setAuthorization(final UUID token) {
        addParameter(new AuthorizationParameter(token));
    }

    public MultiValueMap<Parameter.Type, Parameter> getParameters() {
        return parameters;
    }
}
