package com.arzttermine.library.client.support;

import com.arzttermine.library.client.model.Operation;
import com.arzttermine.library.client.model.RequestOptions;
import com.arzttermine.library.client.model.param.Parameter;
import com.arzttermine.library.client.model.param.QueryParameter;
import com.arzttermine.library.client.service.RequestProcessor;
import okhttp3.HttpUrl;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.function.Function;

/**
 * A {@link ClientAdapter} is used to process the provided {@link Operation.Builder} before requesting an external service.
 * Typically a client extends the ClientAdapter and provides the corresponding API settings.
 * The client is used to execute any kind of {@link Operation.Builder}. A builder should specify the external API endpoint.
 *
 * The usage requires that you define your client as bean in the context
 * and there must be at least one bean of type {@link RequestProcessor} available.
 * The {@link RequestProcessor} requires the final version of an operation as instance of {@link Operation}.
 *
 * {@see RequestProcessorConfigurer}
 */
public abstract class ClientAdapter
{
    @Autowired
    private RequestProcessor requestProcessor;

    private final Function<String, HttpUrl.Builder> targetBuilder;

    protected ClientAdapter(final String host, final int port, final String scheme) {
        targetBuilder = path -> new HttpUrl.Builder()
            .host(host)
            .port(port)
            .scheme(scheme)
            .addPathSegments(path);
    }

    protected ClientAdapter(String host, int port) {
        this(host, port, "http");
    }

    protected ClientAdapter(String host) {
        this(host, 80, "http");
    }

    protected <T> T execute(Operation.Builder<T> builder, RequestOptions options) {
        options.getParameters().forEach((key, value) -> value.forEach(builder::addParameter));

        if (options.hasPaging()) {
            builder.addParameter(new QueryParameter("page", options.getPaging().getPage()));
            builder.addParameter(new QueryParameter("size", options.getPaging().getSize()));
        }

        final Operation<T> operation = builder.build();

        final String finalUrl = mergeUrlPath(operation.getPath(), operation.getParameters()).substring(1);
        final HttpUrl.Builder urlBuilder = targetBuilder.apply(finalUrl);

        operation.getParameters().stream()
            .filter(p -> null != p.getValue())
            .filter(p -> !p.getValue().isEmpty())
            .filter(p -> p.getType().equals(Parameter.Type.QUERY))
            .forEach(e -> urlBuilder.addQueryParameter(e.getName(), e.getValue()));

        return requestProcessor.request(urlBuilder.build().toString(), operation);
    }

    protected <T> T execute(Operation.Builder<T> builder) {
        return execute(builder, new RequestOptions());
    }

    private String mergeUrlPath(String path, List<Parameter> parameters) {
        for (final Parameter param : parameters) {
            if (!param.getType().equals(Parameter.Type.PATH)) {
                continue;
            }

            final String key = String.format("{%s}", param.getName());

            while (path.contains(key)) {
                path = path.replace(key, param.getValue());
            }
        }

        return path;
    }
}
