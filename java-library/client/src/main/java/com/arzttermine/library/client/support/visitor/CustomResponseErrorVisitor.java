package com.arzttermine.library.client.support.visitor;

import com.arzttermine.library.client.model.Operation;
import com.arzttermine.library.suport.exception.ResponseErrorException;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public final class CustomResponseErrorVisitor implements ResponseVisitor
{
    private static final Logger log = LoggerFactory.getLogger(ResponseErrorVisitor.class);

    private final ObjectMapper objectMapper;

    public CustomResponseErrorVisitor(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public <T> void visit(Operation<T> operation, Response response) {
        if (response.isSuccessful() || null == operation.getErrorType()) {
            return;
        }

        final Class<? extends Object> errorType = operation.getErrorType();

        try {
            Object responseBody = objectMapper.readValue(response.body().bytes(), errorType);
            throw new ResponseErrorException(responseBody, response.code());
        } catch (IOException e) {
            log.warn("could not read customized error response for object {} or connection closed unexpected", errorType.getCanonicalName(), e);
            throw new RuntimeException(e);
        }
    }
}
