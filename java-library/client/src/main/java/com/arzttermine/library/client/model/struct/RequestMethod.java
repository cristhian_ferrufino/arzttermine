package com.arzttermine.library.client.model.struct;

public enum RequestMethod
{
    GET,
    PUT,
    POST,
    HEAD,
    DELETE,
}
