package com.arzttermine.library.client.model.param;

public class Parameter
{
    private final String name;
    private final Type type;
    private String value;

    public Parameter(Type type, String name, String value) {
        this.name = name;
        this.type = type;
        this.value = value;
    }

    public Parameter(Type type, String name) {
        this(type, name, null);
    }

    public Parameter(String name) {
        this(Type.PATH, name);
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Type getType() {
        return type;
    }

    public enum Type
    {
        PATH,
        QUERY,
        HEADER,
    }
}
