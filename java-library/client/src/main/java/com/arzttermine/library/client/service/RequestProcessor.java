package com.arzttermine.library.client.service;

import com.arzttermine.library.client.model.Operation;
import com.arzttermine.library.client.model.RequestOptions;

public interface RequestProcessor
{
    <T> T request(String target, Operation<T> operation);
}
