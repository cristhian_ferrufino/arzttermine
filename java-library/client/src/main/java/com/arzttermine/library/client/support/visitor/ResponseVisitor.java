package com.arzttermine.library.client.support.visitor;

import com.arzttermine.library.client.model.Operation;
import okhttp3.Response;

public interface ResponseVisitor
{
    <T> void visit(Operation<T> operation, Response response);

    default boolean supportsCode(int code) {
        return true;
    }
}
