package com.arzttermine.library.client.support.visitor;

import com.arzttermine.library.client.model.Operation;
import com.arzttermine.library.client.support.ClientAdapterTest;
import com.arzttermine.library.suport.exception.EntityAlreadyExistException;
import com.arzttermine.library.suport.exception.EntityNotFoundException;
import com.arzttermine.library.suport.exception.ResponseErrorException;
import com.arzttermine.library.suport.exception.ResponseErrorListException;
import com.arzttermine.library.web.dto.response.ErrorDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ResponseErrorVisitorTest
{
    @Spy
    private ObjectMapper objectMapper = new ObjectMapper();

    @InjectMocks
    private ResponseErrorVisitor visitor;

    private Request request = new Request.Builder().url("http://localhost/").get().build();

    @Test
    public void visit_should_do_nothing_when_response_is_successful()
    {
        Response response = new Response.Builder().code(200).protocol(Protocol.HTTP_2).request(request).build();

        visitor.visit(null, response);
    }

    @Test
    public void visit_should_do_nothing_when_operation_has_custom_error_type()
    {
        Response response = new Response.Builder().code(500).protocol(Protocol.HTTP_2).request(request).build();
        Operation<?> operation = new ClientAdapterTest.ExampleOperation()
            .setErrorType(Operation.class)
            .build();

        visitor.visit(operation, response);
    }

    @Test(expected = ResponseErrorException.class)
    public void visit_should_throw_ResponseErrorException_when_error_list_is_not_readable() throws IOException
    {
        Response response = new Response.Builder()
            .code(500)
            .body(ResponseBody.create(MediaType.parse("application/json"), new byte[0]))
            .protocol(Protocol.HTTP_2)
            .request(request)
            .build();

        Operation<?> operation = new ClientAdapterTest.ExampleOperation().build();

        visitor.visit(operation, response);
    }

    @Test(expected = ResponseErrorListException.class)
    public void visit_should_throw_ResponseErrorListException_when_error_code_is_not_treated() throws IOException
    {
        byte[] payload = new byte[0];
        Response response = new Response.Builder()
            .code(415)
            .body(ResponseBody.create(MediaType.parse("application/json"), payload))
            .protocol(Protocol.HTTP_2)
            .request(request)
            .build();

        doReturn(new ErrorDto[]{new ErrorDto("^^" ,"^^", "^^")}).when(objectMapper)
            .readValue(eq(payload), eq(ErrorDto[].class));

        Operation<?> operation = new ClientAdapterTest.ExampleOperation().build();

        visitor.visit(operation, response);
    }

    @Test(expected = EntityNotFoundException.class)
    public void visit_should_throw_EntityNotFound_when_code_is_404() throws IOException
    {
        byte[] payload = new byte[0];
        Response response = new Response.Builder()
            .code(404)
            .body(ResponseBody.create(MediaType.parse("application/json"), payload))
            .protocol(Protocol.HTTP_2)
            .request(request)
            .build();

        Operation<?> operation = new ClientAdapterTest.ExampleOperation().build();

        doReturn(new ErrorDto[]{new ErrorDto("^^" ,"^^", "^^")}).when(objectMapper)
            .readValue(eq(payload), eq(ErrorDto[].class));

        visitor.visit(operation, response);
    }

    @Test(expected = EntityAlreadyExistException.class)
    public void visit_should_throw_EntityAlreadyExists_when_code_is_409_with_ALREADY_EXIST() throws IOException
    {
        byte[] payload = objectMapper.writeValueAsBytes(new ErrorDto[]{new ErrorDto("ALREADY_EXISTS", "", "")});
        Response response = new Response.Builder()
            .code(409)
            .body(ResponseBody.create(MediaType.parse("application/json"), payload))
            .protocol(Protocol.HTTP_2)
            .request(request)
            .build();

        Operation<?> operation = new ClientAdapterTest.ExampleOperation().build();

        visitor.visit(operation, response);
    }
}
