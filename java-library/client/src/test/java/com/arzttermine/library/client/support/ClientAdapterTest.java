package com.arzttermine.library.client.support;

import com.arzttermine.library.client.model.Operation;
import com.arzttermine.library.client.model.RequestOptions;
import com.arzttermine.library.client.model.param.AuthorizationParameter;
import com.arzttermine.library.client.model.param.Parameter;
import com.arzttermine.library.client.model.param.QueryParameter;
import com.arzttermine.library.client.model.struct.RequestMethod;
import com.arzttermine.library.client.service.RequestProcessor;
import com.arzttermine.library.client.support.visitor.CustomResponseErrorVisitorTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.stereotype.Component;

import java.util.UUID;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class ClientAdapterTest
{
    @Mock
    private RequestProcessor requestProcessor;

    @InjectMocks
    private TestClientAdapterImpl client;

    @Test
    public void execute_should_build_correct_path()
    {
        Operation.Builder<?> builder = new ExampleOperation();

        RequestOptions options = new RequestOptions();
        options.addParameter(new Parameter(Parameter.Type.PATH, "test", "it-works"));
        options.addParameter(new QueryParameter("example", "test"));

        client.execute(builder, options);

        String expectedUrl = "http://localhost/somewhere/it-works/abc?example=test";
        verify(requestProcessor, times(1)).request(eq(expectedUrl), any());
    }

    @Test
    public void execute_should_set_paging_parameters_if_present()
    {
        Operation.Builder<?> builder = new ExampleOperation();

        RequestOptions options = new RequestOptions();
        options.setPaging(5, 12);

        when(requestProcessor.request(anyString(), any())).thenAnswer(invocation -> {
            Operation<?> operation = (Operation<?>) invocation.getArguments()[1];
            assertEquals("5", operation.getParameters().stream().filter(p -> p.getName().equals("page")).findFirst().get().getValue());
            assertEquals("12", operation.getParameters().stream().filter(p -> p.getName().equals("size")).findFirst().get().getValue());

            return true;
        });

        client.execute(builder, options);

        verify(requestProcessor, times(1)).request(anyString(), any());
    }

    @Test
    public void execute_should_merge_parameters_from_options()
    {
        Operation.Builder<?> builder = new ExampleOperation();
        builder.addParameter(new QueryParameter("example", null));
        builder.addParameter(new QueryParameter("another", "param"));
        builder.addParameter(new AuthorizationParameter(null));
        builder.set(Parameter.Type.PATH, "test", "ok");

        UUID token = UUID.randomUUID();
        RequestOptions options = new RequestOptions();
        options.addParameter(new QueryParameter("example", "test"));
        options.addParameter(new QueryParameter("another", "test"));
        options.setAuthorization(token);

        String expectedUrl = "http://localhost/somewhere/ok/abc?example=test&another=param&another=test";
        when(requestProcessor.request(anyString(), any())).thenAnswer(invocation -> {
            String url = (String) invocation.getArguments()[0];
            assertEquals(expectedUrl, url);

            Operation<?> operation = (Operation<?>) invocation.getArguments()[1];
            assertEquals("/somewhere/{test}/abc", operation.getEndpoint());
            assertEquals("/somewhere/{test}/abc", operation.getPath());
            assertEquals("Bearer " + token, operation.getParameters().stream().filter(p -> p.getName().equals("Authorization")).findFirst().get().getValue());

            return null;
        });

        client.execute(builder, options);

        verify(requestProcessor, times(1)).request(eq(expectedUrl), any());
    }

    @Component
    public static final class TestClientAdapterImpl extends ClientAdapter
    {
        public TestClientAdapterImpl() {
            super("localhost");
        }
    }

    public static final class ExampleOperation extends Operation.Builder<Object>
    {
        @Override
        public Operation<Object> build() {
            setPath("/somewhere/{test}/abc");

            return new Operation<>(this);
        }
    }

    public static final class ExamplePostOperation extends Operation.Builder<Object>
    {
        @Override
        public Operation<Object> build() {
            setPath("/somewhere/{test}/post");
            setMethod(RequestMethod.POST);

            addParameter(new Parameter("test"));

            return new Operation<>(this);
        }
    }

    public static final class ExampleResponseOperation extends Operation.Builder<CustomResponseErrorVisitorTest.DTO>
    {
        @Override
        public Operation<CustomResponseErrorVisitorTest.DTO> build() {
            setPath("/somewhere/{test}");
            setMethod(RequestMethod.GET);
            setResponseTypeClass(CustomResponseErrorVisitorTest.DTO.class);

            return new Operation<>(this);
        }
    }
}
