package com.arzttermine.library.client.model;

import com.arzttermine.library.client.model.param.Parameter;
import com.arzttermine.library.client.support.ClientAdapterTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class OperationTest
{
    @Test
    public void set_should_override_param_if_there_is_one_without_value()
    {
        Operation.Builder<?> builder = new ClientAdapterTest.ExampleOperation();
        builder.set(Parameter.Type.PATH, "test", "value");

        Operation<?> operation = builder.build();

        assertEquals(1, operation.getParameters().size());
        assertEquals("test", operation.getParameters().get(0).getName());
        assertEquals("value", operation.getParameters().get(0).getValue());
    }

    @Test
    public void set_should_not_override_param_if_there_is_none_without_value()
    {
        Operation.Builder<?> builder = new ClientAdapterTest.ExampleOperation();
        builder.addParameter(new Parameter(Parameter.Type.PATH, "test", "value"));

        Operation<?> operation = builder.build();

        assertEquals(1, operation.getParameters().size());
        assertEquals("test", operation.getParameters().get(0).getName());
        assertEquals("value", operation.getParameters().get(0).getValue());
    }

    @Test
    public void addParameter_should_add_new_parameter_if_no_one_is_present()
    {
        Operation.Builder<?> builder = new ClientAdapterTest.ExamplePostOperation();
        builder.addParameter(new Parameter(Parameter.Type.PATH, "another", "value"));

        Operation<?> operation = builder.build();

        assertEquals(2, operation.getParameters().size());
        assertEquals("another", operation.getParameters().get(0).getName());
        assertEquals("value", operation.getParameters().get(0).getValue());
        assertEquals("test", operation.getParameters().get(1).getName());
        assertNull(operation.getParameters().get(1).getValue());
    }
}
