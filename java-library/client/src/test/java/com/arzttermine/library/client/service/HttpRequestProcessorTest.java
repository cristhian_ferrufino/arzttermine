package com.arzttermine.library.client.service;

import com.arzttermine.library.client.model.Operation;
import com.arzttermine.library.client.support.ClientAdapterTest;
import com.arzttermine.library.client.support.visitor.CustomResponseErrorVisitorTest;
import com.arzttermine.library.client.support.visitor.ResponseVisitor;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.*;
import okio.Buffer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class HttpRequestProcessorTest
{
    @Mock
    private OkHttpClient httpClient;

    @Spy
    private ObjectMapper objectMapper = new ObjectMapper();

    private HttpRequestProcessor processor;

    private ResponseVisitor visitor = mock(ResponseVisitor.class);
    private Request request = new Request.Builder().url("http://localhost/").get().build();
    private Response response = new Response.Builder().request(request).protocol(Protocol.HTTP_2).code(200).build();

    @Before
    public void setupProcessor() throws Exception {
        when(visitor.supportsCode(anyInt())).thenReturn(true);
        processor = new HttpRequestProcessor(httpClient, Collections.singletonList(visitor), objectMapper);

        Call call = mock(Call.class);
        when(call.execute()).thenReturn(response);

        when(httpClient.newCall(any())).thenReturn(call);
    }

    @Test
    public void request_should_parse_request_body() throws Exception
    {
        CustomResponseErrorVisitorTest.DTO dto = new CustomResponseErrorVisitorTest.DTO();
        String content = objectMapper.writeValueAsString(dto);

        Operation<?> operation = new ClientAdapterTest.ExamplePostOperation().setRequestBody(dto).build();

        processor.request("http://host/target", operation);

        reset(httpClient);
        when(httpClient.newCall(any())).thenAnswer(invocation -> {
            Request request = (Request) invocation.getArguments()[0];
            Buffer response = new okio.Buffer();
            request.body().writeTo(response);

            assertEquals(content, response.readUtf8());

            return response;
        });

        verify(objectMapper, times(1)).writeValueAsBytes(dto);
    }

    @Test
    public void request_should_visit_visitors_when_response_body_is_not_empty() throws Exception
    {
        CustomResponseErrorVisitorTest.DTO dto = new CustomResponseErrorVisitorTest.DTO();
        Operation<?> operation = new ClientAdapterTest.ExamplePostOperation().setRequestBody(dto).build();
        byte[] bytes = objectMapper.writeValueAsBytes(dto);
        reset(objectMapper);

        Call call = mock(Call.class);
        when(call.execute()).thenReturn(new Response.Builder()
            .request(request)
            .protocol(Protocol.HTTP_2)
            .body(ResponseBody.create(MediaType.parse("application/json"), bytes))
            .code(200)
            .build());

        when(httpClient.newCall(any())).thenReturn(call);

        processor.request("http://host/target", operation);

        verify(objectMapper, times(1)).writeValueAsBytes(dto);
        verify(visitor, times(1)).visit(eq(operation), any());
    }

    @Test
    public void request_should_visit_eligible_visitors_only() throws Exception
    {
        when(visitor.supportsCode(200)).thenReturn(false);
        processor = new HttpRequestProcessor(httpClient, Collections.singletonList(visitor), objectMapper);

        CustomResponseErrorVisitorTest.DTO dto = new CustomResponseErrorVisitorTest.DTO();
        Operation<?> operation = new ClientAdapterTest.ExamplePostOperation().setRequestBody(dto).build();
        byte[] bytes = objectMapper.writeValueAsBytes(dto);
        reset(objectMapper);

        Call call = mock(Call.class);
        when(call.execute()).thenReturn(new Response.Builder()
            .request(request)
            .protocol(Protocol.HTTP_2)
            .body(ResponseBody.create(MediaType.parse("application/json"), bytes))
            .code(200)
            .build());

        when(httpClient.newCall(any())).thenReturn(call);

        processor.request("http://host/target", operation);

        verify(objectMapper, times(1)).writeValueAsBytes(dto);
        verify(visitor, never()).visit(any(), any());
    }

    @Test
    public void request_should_parse_response_when_response_type_is_provided() throws Exception
    {
        CustomResponseErrorVisitorTest.DTO dto = new CustomResponseErrorVisitorTest.DTO();
        dto.prop = 42;

        Operation<CustomResponseErrorVisitorTest.DTO> operation = new ClientAdapterTest.ExampleResponseOperation().build();
        byte[] bytes = objectMapper.writeValueAsBytes(dto);
        reset(objectMapper);

        Call call = mock(Call.class);
        when(call.execute()).thenReturn(new Response.Builder()
            .request(request)
            .protocol(Protocol.HTTP_2)
            .body(ResponseBody.create(MediaType.parse("application/json"), bytes))
            .code(200)
            .build());

        when(httpClient.newCall(any())).thenReturn(call);

        CustomResponseErrorVisitorTest.DTO result = processor.request("http://host/target", operation);

        assertEquals(dto.prop, result.prop);
        verify(objectMapper, times(1)).readValue(bytes, CustomResponseErrorVisitorTest.DTO.class);
        verify(visitor, times(1)).visit(eq(operation), any());
    }
}
