package com.arzttermine.library.client.support.visitor;

import com.arzttermine.library.client.model.Operation;
import com.arzttermine.library.client.support.ClientAdapterTest;
import com.arzttermine.library.suport.exception.ResponseErrorException;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class CustomResponseErrorVisitorTest
{
    @Spy
    private ObjectMapper objectMapper = new ObjectMapper();

    @InjectMocks
    private CustomResponseErrorVisitor visitor;

    private Request request = new Request.Builder().url("http://localhost/").get().build();

    @Test
    public void visit_should_do_nothing_when_response_is_successful()
    {
        Response response = new Response.Builder().code(200).protocol(Protocol.HTTP_2).request(request).build();

        visitor.visit(null, response);
    }

    @Test
    public void visit_should_do_nothing_when_operation_does_not_have_a_custom_error_type()
    {
        Response response = new Response.Builder().code(500).protocol(Protocol.HTTP_2).request(request).build();
        Operation<?> operation = new ClientAdapterTest.ExampleOperation().build();

        visitor.visit(operation, response);
    }

    @Test(expected = ResponseErrorException.class)
    public void visit_should_throw_exception_with_parsed_expected_error_type() throws IOException
    {
        DTO dto = new DTO();
        dto.prop = 2;

        byte[] payload = objectMapper.writeValueAsBytes(dto);
        Response response = new Response.Builder()
            .code(404)
            .body(ResponseBody.create(MediaType.parse("application/json"), payload))
            .protocol(Protocol.HTTP_2)
            .request(request)
            .build();

        Operation<?> operation = new ClientAdapterTest.ExampleOperation()
            .setErrorType(DTO.class)
            .build();

        try {
            visitor.visit(operation, response);
        } catch (ResponseErrorException e) {
            assertEquals(dto.prop, e.getResponse(DTO.class).prop);

            throw e;
        }
    }

    public static final class DTO {
        public int prop = 1;
    }
}
