package com.arzttermine.library.elasticsearch.service;

import com.arzttermine.library.elasticsearch.model.BulkAction;
import com.arzttermine.library.web.IdAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.entity.ContentType;
import org.apache.http.nio.entity.NStringEntity;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.ResponseListener;
import org.elasticsearch.client.RestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.Charset;
import java.util.*;
import java.util.concurrent.ExecutorService;

public class DocumentProcessor
{
    private static final Logger log = LoggerFactory.getLogger(DocumentProcessor.class);

    private final RestClient client;
    private final ExecutorService executorService;
    private final ObjectMapper objectMapper;
    private final String prefix;

    public DocumentProcessor(RestClient client, ExecutorService executor, ObjectMapper objectMapper, String prefix) {
        this.client = client;
        this.executorService  = executor;
        this.objectMapper = objectMapper;
        this.prefix = Optional.ofNullable(prefix).orElse("");
    }

    public <T extends IdAccessor> void index(final String index, final String type, final T doc)
    {
        indexDocument(index, type, doc);
    }

    public <T extends IdAccessor> void indexAsync(final String index, final String type, final T doc)
    {
        if (null != executorService) {
            executorService.submit(() -> index(index, type, doc));
        } else {
            index(index, type, doc);
        }
    }

    public void execute(final String index, final String type, final Collection<BulkAction> actions)
    {
        runBulkUpdate(index, type, actions);
    }

    public void executeAsync(final String index, final String type, final Collection<BulkAction> actions)
    {
        if (null != executorService) {
            executorService.submit(() -> execute(index, type, actions));
        } else {
            execute(index, type, actions);
        }
    }

    private <T extends IdAccessor> void indexDocument(String index, String type, T data)
    {
        try {
            final String payload = objectMapper.writeValueAsString(data);

            client.performRequestAsync(
                "PUT",
                String.format("/%s%s/%s/%d", prefix, index, type, data.getId()),
                Collections.emptyMap(),
                new NStringEntity(payload, ContentType.APPLICATION_JSON),
                new IndexResponseListener(type, data.getId(), false)
            );
        } catch (JsonProcessingException e) {
            log.error("could not serialize or encode request payload to index another document", e);
        }
    }

    /**
     * Bulk indexing in elasticsearch requires a specific formatted request payload.
     * {@see https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-bulk.html}
     *
     * Each "document update" has two rows (HEAD and BODY).
     * The HEAD should provide the requested action and the corresponding id from {@link IdAccessor}
     *   -> We omit the index and type, as we already provide them in the URL.
     *
     * The BODY should contain the document to index
     * Each "document update" and even the HEAD and BODY MUST be separated by a new line.
     * The end of the whole document should contain EOF-line ending.
     *
     * example:
     * {
     *      index: {
     *          _id: 42
     *      }
     * } <-- note the missing comma
     * {
     *      id: 42,
     *      myField: "value"
     * } <-- note the missing comma
     * {
     *     update: {
     *         _id: 43
     *     }
     * } <-- note the missing comma
     * {
     *     doc: {
     *         myField: "newValue"
     *     }
     * } <-- note the missing comma
     * {
     *     "delete": {
     *         _id: 44,
     *         _index: "appointments",
     *         _type: "practice-42"
     *     }
     * }
     * <-- note the trailing line
     *
     * @param index String
     * @param type String
     * @param data Collection of bulk actions
     */
    private void runBulkUpdate(final String index, final String type, final Collection<BulkAction> data)
    {
        try {
            final StringBuilder builder = new StringBuilder();

            for (final BulkAction<?> d : data) {
                final Map<String, Object> head = new HashMap<>();
                head.put(d.getType().name().toLowerCase(), new BulkAction.ItemHead(d));
                builder.append(objectMapper.writeValueAsString(head)).append("\n");

                if (d.getType().equals(BulkAction.ActionType.INDEX)) {
                    builder.append(objectMapper.writeValueAsString(d.getDocument())).append("\n");
                }
                else if (d.getType().equals(BulkAction.ActionType.UPDATE)) {
                    builder.append("{\"doc\":")
                        .append(objectMapper.writeValueAsString(d.getDocument()))
                        .append("}\n");
                }

                if (log.isDebugEnabled())
                    log.debug("added bulk action for {} on {} ({}/{})",
                        d.getType(),
                        d.getDocument().getId(),
                        Optional.ofNullable(d.getIndex()).orElse(index),
                        Optional.ofNullable(d.getIndexType()).orElse(type));
            }

            client.performRequestAsync(
                "PUT",
                String.format("/%s%s/%s/_bulk", prefix, index, type),
                Collections.emptyMap(),
                new NStringEntity(builder.toString(), ContentType.APPLICATION_JSON),
                new IndexResponseListener(index, data.size(), true)
            );
        } catch (JsonProcessingException e) {
            log.error("could not serialize or encode request payload to index another document", e);
        }
    }

    private static final class IndexResponseListener implements ResponseListener
    {
        private final String index;
        private final long idOrAmount;
        private final boolean bulk;

        private IndexResponseListener(String index, long idOrCount, boolean bulk) {
            this.index = index;
            this.bulk = bulk;
            this.idOrAmount = idOrCount;
        }

        @Override
        public void onSuccess(Response response) {
            if (bulk) {
                log.info("updated {} documents in elasticsearch index {} using bulk mode", idOrAmount, index);
                return;
            }

            log.info("updated document with id {} in elasticsearch index {}", idOrAmount, index);
        }

        @Override
        public void onFailure(Exception e) {
            // {@TODO: monitoring}
            log.error("could not update documents in elasticsearch index {}", index, e);
        }
    }
}
