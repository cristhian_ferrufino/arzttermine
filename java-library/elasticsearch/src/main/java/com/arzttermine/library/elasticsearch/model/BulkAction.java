package com.arzttermine.library.elasticsearch.model;

import com.arzttermine.library.web.IdAccessor;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Optional;

public class BulkAction<T extends IdAccessor>
{
    private final ActionType type;
    private final T document;
    private String index;
    private String indexType;

    private BulkAction(ActionType type, T document) {
        this.type = type;
        this.document = document;
    }

    public static <Doc extends IdAccessor> BulkAction update(Doc document) {
        return new BulkAction<>(ActionType.UPDATE, document);
    }

    public static <Doc extends IdAccessor> BulkAction delete(Doc document) {
        return new BulkAction<>(ActionType.DELETE, document);
    }

    public static <Doc extends IdAccessor> BulkAction index(Doc document) {
        return new BulkAction<>(ActionType.INDEX, document);
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getIndexType() {
        return indexType;
    }

    public void setIndexType(String indexType) {
        this.indexType = indexType;
    }

    public ActionType getType() {
        return type;
    }

    public T getDocument() {
        return document;
    }

    public enum ActionType {
        UPDATE,
        DELETE,
        INDEX,
    }

    public static final class ItemHead {
        @JsonProperty("_id")
        public Object id;

        @JsonProperty("_index")
        @JsonInclude(JsonInclude.Include.NON_NULL)
        public String index;

        @JsonProperty("_type")
        @JsonInclude(JsonInclude.Include.NON_NULL)
        public String type;

        public ItemHead(BulkAction action) {
            this.id = action.getDocument().getId();
            this.index = action.getIndex();
            this.type = action.getIndexType();
        }
    }
}
