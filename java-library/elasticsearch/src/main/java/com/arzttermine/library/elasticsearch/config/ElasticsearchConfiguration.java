package com.arzttermine.library.elasticsearch.config;

import com.arzttermine.library.elasticsearch.config.settings.ElasticsearchSettings;
import com.arzttermine.library.elasticsearch.service.DocumentProcessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;

import java.util.concurrent.ExecutorService;

public class ElasticsearchConfiguration
{
    public DocumentProcessor documentProcessor(ElasticsearchSettings settings, ExecutorService executor, ObjectMapper objectMapper)
    {
        return new DocumentProcessor(restClient(settings), executor, objectMapper, settings.getPrefix());
    }

    protected RestClientBuilder configure(RestClientBuilder builder) {
        return builder;
    }

    private RestClient restClient(ElasticsearchSettings settings)
    {
        return configure(RestClient.builder(settings.getTargets().stream()
            .map(host -> new HttpHost(host.getHost(), host.getPort(), host.getScheme()))
            .toArray(HttpHost[]::new)))
            .build();
    }
}
