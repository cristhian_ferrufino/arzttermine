package com.arzttermine.library.elasticsearch.config.settings;

import java.util.List;

public class ElasticsearchSettings
{
    private List<HostAndPort> targets;

    private String prefix;

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(final String prefix) {
        this.prefix = prefix;
    }

    public List<HostAndPort> getTargets() {
        return targets;
    }

    public void setTargets(List<HostAndPort> targets) {
        this.targets = targets;
    }
}
