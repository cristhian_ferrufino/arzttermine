package com.arzttermine.library.elasticsearch.service;

import com.arzttermine.library.elasticsearch.model.BulkAction;
import com.arzttermine.library.web.IdAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.elasticsearch.client.ResponseListener;
import org.elasticsearch.client.RestClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DocumentProcessorTest
{
    @Spy
    private ObjectMapper objectMapper = new ObjectMapper();

    @Mock
    private RestClient restClient;

    @Mock
    private ExecutorService executorService;

    @InjectMocks
    private DocumentProcessor documentProducer;

    @Test
    public void publish_should_submit_task_to_another_thread()
    {
        documentProducer.executeAsync("index", "type", Collections.singletonList(BulkAction.index(new TestObject())));

        verify(executorService, times(1)).submit(any(Runnable.class));
    }

    @Test
    public void publish_should_send_bulk_indexing_for_collections()
    {
        when(executorService.submit(any(Runnable.class))).thenAnswer(invocation -> {
            ((Runnable) invocation.getArguments()[0]).run();
            return CompletableFuture.completedFuture(null);
        });

        doAnswer(invocation -> {
            assertEquals("PUT", invocation.getArguments()[0]);
            assertEquals("/index/type/_bulk", invocation.getArguments()[1]);

            String payload = convertHttpEntity((HttpEntity) invocation.getArguments()[3]);
            assertEquals("{\"index\":{\"_id\":42}}\n{\"prop\":\"test\",\"id\":42}\n{\"index\":{\"_id\":42}}\n{\"prop\":\"test\",\"id\":42}\n", payload);

            return null;
        }).when(restClient).performRequestAsync(any(), any(), any(), any(), any(ResponseListener.class));

        documentProducer.execute("index", "type", Arrays.asList(BulkAction.index(new TestObject()), BulkAction.index(new TestObject())));

        verify(restClient, times(1)).performRequestAsync(any(), any(), any(), any(), any(ResponseListener.class));
        verify(executorService, never()).submit(any(Runnable.class));
    }

    @Test
    public void publish_should_prefix_index()
    {
        doAnswer(invocation -> {
            assertEquals("PUT", invocation.getArguments()[0]);
            assertEquals("/my-prefix_index/type/42", invocation.getArguments()[1]);

            return invocation;
        }).when(restClient).performRequestAsync(any(), any(), any(), any(), any(ResponseListener.class));

        DocumentProcessor withPrefixProcessor = new DocumentProcessor(restClient, null, objectMapper, "my-prefix_");

        withPrefixProcessor.index("index", "type", new TestObject());

        verify(restClient, times(1)).performRequestAsync(any(), any(), any(), any(), any(ResponseListener.class));
    }

    @Test
    public void publish_should_index_single_document_by_id()
    {
        when(executorService.submit(any(Runnable.class))).thenAnswer(invocation -> {
            ((Runnable) invocation.getArguments()[0]).run();
            return CompletableFuture.completedFuture(null);
        });

        doAnswer(invocation -> {
            assertEquals("PUT", invocation.getArguments()[0]);
            assertEquals("/index/type/42", invocation.getArguments()[1]);

            String payload = convertHttpEntity((HttpEntity) invocation.getArguments()[3]);
            assertEquals("{\"prop\":\"test\",\"id\":42}", payload);

            return null;
        }).when(restClient).performRequestAsync(any(), any(), any(), any(), any(ResponseListener.class));

        documentProducer.index("index", "type", new TestObject());

        verify(restClient, times(1)).performRequestAsync(any(), any(), any(), any(), any(ResponseListener.class));
        verify(executorService, never()).submit(any(Runnable.class));
    }

    @Test
    public void publish_should_update_single_document_by_id()
    {
        when(executorService.submit(any(Runnable.class))).thenAnswer(invocation -> {
            ((Runnable) invocation.getArguments()[0]).run();
            return CompletableFuture.completedFuture(null);
        });

        doAnswer(invocation -> {
            assertEquals("PUT", invocation.getArguments()[0]);
            assertEquals("/index/type/_bulk", invocation.getArguments()[1]);

            String payload = convertHttpEntity((HttpEntity) invocation.getArguments()[3]);
            assertEquals("{\"update\":{\"_id\":42}}\n{\"doc\":{\"prop\":\"test\",\"id\":42}}\n", payload);

            return null;
        }).when(restClient).performRequestAsync(any(), any(), any(), any(), any(ResponseListener.class));

        documentProducer.execute("index", "type", Collections.singletonList(BulkAction.update(new TestObject())));

        verify(restClient, times(1)).performRequestAsync(any(), any(), any(), any(), any(ResponseListener.class));
        verify(executorService, never()).submit(any(Runnable.class));
    }

    @Test
    public void publish_should_index_single_document_by_id_async()
    {
        documentProducer.indexAsync("index", "type", new TestObject());

        verify(executorService, times(1)).submit(any(Runnable.class));
    }

    public static final class TestObject implements IdAccessor
    {
        public String prop = "test";

        @Override
        public long getId() {
            return 42;
        }
    }

    private String convertHttpEntity(HttpEntity entity) throws Exception
    {
        InputStream payload = entity.getContent();

        int count = payload.available();
        byte[] bytes = new byte[count];
        payload.read(bytes, 0, count);

        return new String(bytes, StandardCharsets.UTF_8);
    }
}
