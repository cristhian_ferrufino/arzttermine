# elasticsearch

This modules supports access to elasticsearch.

You can use this client to index single documents into our search. 
To delete, update or index documents you can use `DocumentProcessor.execute` to execute multiple bulk actions (delete, update and index) at once.

### Usage
```java
@Configuration
@Profile("!testing")
public class SearchConfiguration extends com.arzttermine.library.elasticsearch.config.ElasticsearchConfiguration {
    @Bean
    public DocumentProcessor documentProcessor(com.arzttermine.library.elasticsearch.config.settings.ElasticsearchSettings settings, ExecutorService executor, ObjectMapper objectMapper)
    {
        return super.documentProcessor(settings, executor, objectMapper);
    }
}

@Component
public class SearchUpdater {
    @Autowired
    private com.arzttermine.library.elasticsearch.service.DocumentProcessor documentProcessor;
    
    public void update() {
        MyDocument doc = new MyDocument();
        doc.setId(42L);
        
        List<com.arzttermine.library.elasticsearch.model.BulkAction> actions = new ArrayList();
        actions.add(com.arzttermine.library.elasticsearch.model.BulkAction.index(new MyDocument()));
        actions.add(com.arzttermine.library.elasticsearch.model.BulkAction.delete(doc));
        
        documentProcessor.execute("appointments", practiceId, actions);
    }
}
```

```xml
<dependency>
    <groupId>com.arzttermine.library</groupId>
    <artifactId>elasticsearch</artifactId>
    <version>${elasticsearch.version}</version>
</dependency>

<!-- not included dependency -->
<dependency>
    <groupId>com.arzttermine.library</groupId>
    <artifactId>support</artifactId>
    <version>${library-support.version}</version>
</dependency>

<!-- not included dependency -->
<dependency>
    <groupId>com.arzttermine.library</groupId>
    <artifactId>web</artifactId>
    <version>${library-web.version}</version>
</dependency>
```

### Docker
Run a docker container providing elasticsearch:  
```
docker run -d \
   -p 9200:9200 \
   -p 9300:9300 \
   --network=services \
   --name elasticsearch \
   elasticsearch:5.1-alpine
```
