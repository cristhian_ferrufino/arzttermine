package com.arzttermine.library.spring.data;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.Collection;
import java.util.Collections;
import java.util.function.Consumer;
import java.util.function.Function;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class BatchProcessingUtilTest
{
    @Test
    public void process_should_fetch_and_supply_batches_as_util()
    {
        Function<Pageable, Page<Object>> fetcher =
            (pageable -> new PageImpl<>(Collections.singletonList(new Object()), pageable, 1));

        Consumer<Collection<Object>> consumer = mock(Consumer.class);

        doAnswer(invocation -> {
            assertEquals(1, ((Collection<?>) invocation.getArguments()[0]).size());
            return null;
        }).when(consumer).accept(any());

        BatchProcessingUtil.process(fetcher, consumer, null, null, 10, 0);

        verify(consumer, times(1)).accept(any());
    }

    @Test
    public void process_should_increase_page_and_call_consumer_for_each_batch()
    {
        Function<Pageable, Page<Object>> fetcher =
            (pageable -> new PageImpl<>(Collections.singletonList(new Object()), pageable, 20));

        Consumer<Collection<Object>> consumer = mock(Consumer.class);

        BatchProcessingUtil.process(fetcher, consumer, null, null, 10, 0);

        verify(consumer, times(2)).accept(any());
    }

    @Test
    public void process_should_not_call_consumer_if_no_elements_are_found()
    {
        Function<Pageable, Page<Object>> fetcher =
            (pageable -> new PageImpl<>(Collections.emptyList(), pageable, 0));

        Consumer<Collection<Object>> consumer = mock(Consumer.class);

        BatchProcessingUtil.process(fetcher, consumer, null, null, 10, 0);

        verify(consumer, never()).accept(any());
    }

    @Test
    public void process_should_call_finished_listener()
    {
        Function<Pageable, Page<Object>> fetcher =
            (pageable -> new PageImpl<>(Collections.singletonList(new Object()), pageable, 1));

        Consumer<Collection<Object>> consumer = mock(Consumer.class);
        Consumer<Boolean> listener = mock(Consumer.class);

        BatchProcessingUtil.process(fetcher, consumer, listener, null, 10, 0);

        verify(consumer, times(1)).accept(any());
        verify(listener, times(1)).accept(true);
    }
}
