package com.arzttermine.library.spring.advices.exception;

import com.arzttermine.library.suport.exception.ServiceUnavailableException;
import com.arzttermine.library.web.dto.response.ErrorDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.ResourceAccessException;

import java.util.Collection;
import java.util.Collections;

@ControllerAdvice
public class SpringExceptionAdvice
{
    @ExceptionHandler
    public ResponseEntity<Collection<ErrorDto>> handleResourceAccessException(ResourceAccessException e)
    {
        return new ResponseEntity<>(
            Collections.singletonList(new ServiceUnavailableException("resource").getErrorDto()),
            HttpStatus.SERVICE_UNAVAILABLE
        );
    }
}
