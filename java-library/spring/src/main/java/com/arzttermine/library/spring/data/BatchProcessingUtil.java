package com.arzttermine.library.spring.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Collection;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

public class BatchProcessingUtil
{
    private static final Logger log = LoggerFactory.getLogger(BatchProcessingUtil.class);

    public static <T> void process(
        final Function<Pageable, Page<T>> fetcher,
        final Consumer<Collection<T>> processor,
        final int pageSize
    ) {
        process(fetcher, processor, null, null, pageSize, 5000);
    }

    public static <T> void process(
        final Function<Pageable, Page<T>> fetcher,
        final Consumer<Collection<T>> processor,
        final Consumer<Boolean> finished,
        final int pageSize
    ) {
        process(fetcher, processor, finished, null, pageSize, 5000);
    }

    public static <T> void process(
        final Function<Pageable, Page<T>> fetcher,
        final Consumer<Collection<T>> processor
    ) {
        process(fetcher, processor, null, null, 50, 5000);
    }

    public static <T> void process(
        final Function<Pageable, Page<T>> fetcher,
        final Consumer<Collection<T>> processor,
        final Sort sorting,
        final int pageSize
    ) {
        process(fetcher, processor, null, sorting, pageSize, 5000);
    }

    public static <T> void process(
        final Function<Pageable, Page<T>> fetcher,
        final Consumer<Collection<T>> processor,
        final Consumer<Boolean> finished,
        final Sort sorting,
        final int pageSize,
        final int timeout
    ) {
        int page = 0;
        Page<T> result;

        try {
            do {
                result = fetcher.apply(new PageRequest(page, pageSize, sorting));

                if (result.getNumberOfElements() > 0) {
                    if (log.isDebugEnabled())
                        log.debug("processing batch of {}/{} items on page {}/{}", result.getNumberOfElements(), result.getTotalElements(), result.getNumber(), result.getTotalPages());

                    processor.accept(result.getContent());
                }

                if (!result.hasNext()) {
                    break;
                }

                try {
                    Thread.sleep(timeout);
                } catch (InterruptedException e) {
                    log.error("could not wait between batch processing. interrupted", e);
                }

                page++;
            } while (true);

            Optional.ofNullable(finished)
                .ifPresent(callable -> callable.accept(true));

            log.info("processed {} batches (each {} items) successfully", page, pageSize);

        } catch (Exception e) {
            log.error("exception during batch processing", e);

            Optional.ofNullable(finished)
                .ifPresent(callable -> callable.accept(false));
        }
    }
}
