package com.arzttermine.library.spring.advices.exception;

import com.arzttermine.library.web.dto.response.ErrorDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.util.NestedServletException;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
public class ArgumentExceptionAdvice
{
    @ExceptionHandler
    public ResponseEntity<List<ErrorDto>> handleMethodArgumentNotValidException(MethodArgumentNotValidException exc)
    {
        final List<ErrorDto> errors = exc.getBindingResult()
            .getFieldErrors()
            .stream()
            .map(err -> new ErrorDto("INVALID_ARGUMENT", err.getField(),err.getField() + " argument is not valid"))
            .collect(Collectors.toList());

        errors.addAll(exc.getBindingResult()
            .getGlobalErrors()
            .stream()
            .map((err) -> new ErrorDto("INVALID_ARGUMENT", err.getObjectName(),err.getObjectName() + " argument is not valid"))
            .collect(Collectors.toList()));

        return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public ResponseEntity<List<ErrorDto>> handleHttpMessageConversionException(HttpMessageConversionException exc)
    {
        final ErrorDto error = new ErrorDto("JSON_PARSER_ERROR", "request", exc.getMessage());

        return new ResponseEntity<>(Collections.singletonList(error), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public ResponseEntity<List<ErrorDto>> handleHttpMediaTypeNotSupportedException(HttpMediaTypeNotSupportedException exc)
    {
        final ErrorDto error = new ErrorDto(
            "MEDIA_TYPE_NOT_SUPPORTED",
            (null != exc.getContentType()) ? exc.getContentType().getType() : "null",
            "Request encoding/content-type is not valid"
        );

        return new ResponseEntity<>(Collections.singletonList(error), HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    @ExceptionHandler
    public ResponseEntity<List<ErrorDto>> handleHttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException exc)
    {
        final ErrorDto error = new ErrorDto("REQUEST_METHOD_NOT_SUPPORTED", exc.getMethod(), "Must be one of: " + String.join(",", exc.getSupportedMethods()));

        return new ResponseEntity<>(Collections.singletonList(error), HttpStatus.METHOD_NOT_ALLOWED);
    }

    @ExceptionHandler
    public ResponseEntity<List<ErrorDto>> handleNestedServletException(NestedServletException exc)
    {
        final ErrorDto error = new ErrorDto("INTERNAL_SERVER_ERROR", null, exc.getMessage());

        return new ResponseEntity<>(Collections.singletonList(error), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
