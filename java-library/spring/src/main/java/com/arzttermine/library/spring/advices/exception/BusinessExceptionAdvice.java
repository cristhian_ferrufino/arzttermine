package com.arzttermine.library.spring.advices.exception;

import com.arzttermine.library.suport.exception.BusinessException;
import com.arzttermine.library.suport.exception.ResponseErrorListException;
import com.arzttermine.library.web.dto.response.ErrorDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@ControllerAdvice
public class BusinessExceptionAdvice
{
    @ExceptionHandler
    public ResponseEntity<List<ErrorDto>> handleBusinessException(BusinessException exc)
    {
        final List<ErrorDto> errors = Collections.singletonList(exc.getErrorDto());

        return new ResponseEntity<>(errors, HttpStatus.valueOf(exc.getStatus().getCode()));
    }

    @ExceptionHandler
    public ResponseEntity<Collection<ErrorDto>> handleResponseErrorListException(ResponseErrorListException exc)
    {
        return new ResponseEntity<>(exc.getErrors(), HttpStatus.valueOf(exc.getStatus().getCode()));
    }
}
