# spring

This module allows you to use standardized support for spring through our applications.

Example: 
Each of our applications should respond with a standardized Error-Dto for the same exceptions etc.
You only need to include the `spring`-module dependencies and add a new `@ComponentScan` to your application configuration.

```xml
<dependency>
    <groupId>com.arzttermine.library</groupId>
    <artifactId>elasticsearch</artifactId>
    <version>${elasticsearch.version}</version>
</dependency>

<!-- not included dependency -->
<dependency>
    <groupId>com.arzttermine.library</groupId>
    <artifactId>support</artifactId>
    <version>${library-support.version}</version>
</dependency>

<!-- not included dependency -->
<dependency>
    <groupId>com.arzttermine.library</groupId>
    <artifactId>web</artifactId>
    <version>${library-web.version}</version>
</dependency>

<!-- not included dependency -->
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-web</artifactId>
    <version>${spring-boot-web.version}</version>
    <scope>provided</scope>
</dependency>
```

Example usage:

```java
@org.springframework.context.annotation.Configuration
@org.springframework.context.annotation.ComponentScans({
    @org.springframework.context.annotation.ComponentScan("com.arzttermine.library.spring"),
    @org.springframework.context.annotation.ComponentScan("com.arzttermine.service.my-service")
})
public class MySpringBootApp {
    public static void main(String[] args) {
        
    }
}
```
