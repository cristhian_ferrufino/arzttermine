package com.arzttermine.library.messaging.processor;

import com.arzttermine.library.messaging.model.TopicMessage;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.*;
import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import com.arzttermine.library.messaging.consumer.Consumer;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Set;

public class ConsumerRegisterProcessor
{
    private static final Logger log = LoggerFactory.getLogger(ConsumerRegisterProcessor.class);

    private final ApplicationContext context;
    private final ObjectMapper objectMapper;
    private final Channel amqpChannel;
    private final String queueSuffix;

    public ConsumerRegisterProcessor(ApplicationContext context, Channel amqpChannel, ObjectMapper objectMapper, String queueSuffix) {
        this.context = context;
        this.amqpChannel = amqpChannel;
        this.objectMapper = objectMapper;
        this.queueSuffix = queueSuffix;
    }

    public void bindQueues(final String defaultExchange) throws ReflectiveOperationException
    {
        // find all methods, annotated with @Consumer within specified package
        final Set<Method> types = new Reflections(new ConfigurationBuilder()
            .setUrls(ClasspathHelper.forPackage("com.arzttermine.service"))
            .setScanners(new MethodAnnotationsScanner()))
            .getMethodsAnnotatedWith(Consumer.class);

        for (Method item : types) {
            bindConsumer(defaultExchange, item);
        }
    }

    private void bindConsumer(final String defaultExchange, final Method item) throws ReflectiveOperationException
    {
        final Consumer consumer = item.getAnnotation(Consumer.class);
        final String realExchange = (consumer.exchange().isEmpty()) ? defaultExchange : consumer.exchange();

        // find eligible component which owns the consumer method (must be in the current context)
        // and get the generic java type of required message parameter
        final Object applicableComponent = context.getBean(item.getDeclaringClass());
        final JavaType type = objectMapper.getTypeFactory().constructType(item.getGenericParameterTypes()[0]);

        final String consumerRoutingKey = consumer.routingKey();
        final String consumerQueue = consumer.queue();

        // getting the actual routing key from provided message
        final String routingKey = (consumerRoutingKey.isEmpty())
            ? getMessage(type).getRoutingKey()
            : consumerRoutingKey;

        // use routing key as queue name (eg my.topic.message results in my-topic-message)
        final String defaultQueueName = String.format("%s.%s", item.getDeclaringClass().getCanonicalName(), item.getName());
        final String queue = ((consumerQueue.isEmpty()) ? defaultQueueName : consumerQueue) + queueSuffix;

        try {
            // declare/assert the queue and bind queue to channel
            amqpChannel.queueDeclare(
                queue,
                consumer.durable(),
                consumer.exclusive(),
                consumer.autoDelete(),
                new HashMap<>()
            );

            amqpChannel.queueBind(queue, realExchange, routingKey);
            amqpChannel.basicConsume(queue, new MessageProcessor(amqpChannel, objectMapper, type, item, applicableComponent));

            log.info("Messaging: " + queue + " bind to messages coming through " + realExchange);
        } catch(IOException e) {
            log.error("Messaging: Consumer could not be bind to exchange or queue", e);
        }
    }

    @SuppressWarnings("unchecked")
    private TopicMessage getMessage(final JavaType argument) throws ReflectiveOperationException
    {
        return ((Class<? extends TopicMessage>) argument.getRawClass()).newInstance();
    }
}
