package com.arzttermine.library.messaging.model;

import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class TopicMessage
{
    private boolean redelivered = false;

    public String getRoutingKey() {
        return Stream.of(getTopicKind(), getTopic(), getTopicValue())
            .filter(Objects::nonNull)
            .filter(s -> !s.isEmpty())
            .collect(Collectors.joining("."));
    }

    public boolean isRedelivered() {
        return redelivered;
    }

    public void setRedelivered(boolean redelivered) {
        this.redelivered = redelivered;
    }

    /**
     * @return String identifying the exact topic of the message
     */
    protected abstract String getTopic();

    /**
     * @return String identifying the kind of the topic
     */
    protected abstract String getTopicKind();

    /**
     * @return String identifying the topic value of the message
     */
    protected String getTopicValue() {
        return null;
    }
}
