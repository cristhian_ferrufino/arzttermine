package com.arzttermine.library.messaging.config;

import com.arzttermine.library.messaging.config.settings.MessagingConnectionSettings;
import com.arzttermine.library.messaging.processor.ConsumerRegisterProcessor;
import com.arzttermine.library.messaging.processor.MessageBus;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.Connection;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.handler.annotation.support.DefaultMessageHandlerMethodFactory;

import java.io.IOException;

public abstract class MessagingComponentConfigurer extends RabbitMQConfigurer
{
    @Autowired
    private ApplicationContext context;

    public MessageBus messageBus(ObjectMapper objectMapper, MessagingConnectionSettings settings) {
        return new MessageBus(rabbitTemplate(settings), objectMapper, settings.getExecutorService());
    }

    public ConsumerRegisterProcessor consumerProcessor(ObjectMapper objectMapper, MessagingConnectionSettings settings) throws IOException, ReflectiveOperationException {
        ConsumerRegisterProcessor processor = new ConsumerRegisterProcessor(context, amqpChannel(settings), objectMapper, settings.getQueueSuffix());

        if (settings.isEnabled()) {
            processor.bindQueues(settings.getExchange());
        }

        return processor;
    }

    protected ConnectionFactory configure(ConnectionFactory factory) {
        return factory;
    }

    @Bean
    public RabbitTemplate rabbitTemplate(MessagingConnectionSettings settings) {
        RabbitTemplate template = new RabbitTemplate(connectionFactory(settings));
        template.setExchange(settings.getExchange());

        return template;
    }

    @Bean
    public ConnectionFactory connectionFactory(MessagingConnectionSettings connectionSettings) {
        CachingConnectionFactory factory = new CachingConnectionFactory(connectionSettings.getHost());
        factory.setConnectionTimeout(connectionSettings.getConnectTimeout());
        factory.setUsername(connectionSettings.getUser());
        factory.setPassword(connectionSettings.getPassword());
        factory.setPort(connectionSettings.getPort());

        return configure(factory);
    }

    @Bean
    public Channel amqpChannel(MessagingConnectionSettings connectionSettings) throws ReflectiveOperationException, IOException {
        Connection connection = connectionFactory(connectionSettings).createConnection();
        Channel channel = connection.createChannel(true);

        if (connectionSettings.isEnabled()) {
            channel.exchangeDeclare(connectionSettings.getExchange(), "topic", true);
        }

        return channel;
    }
}
