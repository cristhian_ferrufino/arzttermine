package com.arzttermine.library.messaging.processor;

import com.arzttermine.library.messaging.model.TopicMessage;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

public class MessageBus
{
    private static final Logger log = LoggerFactory.getLogger(MessageBus.class);

    private final RabbitTemplate template;
    private final ObjectMapper objectMapper;
    private final ExecutorService executorService;

    public MessageBus(RabbitTemplate template, ObjectMapper objectMapper, ExecutorService executorService) {
        this.template = template;
        this.objectMapper = objectMapper;
        this.executorService = executorService;
    }

    public MessageBus(RabbitTemplate template, ObjectMapper objectMapper) {
        this(template, objectMapper, null);
    }

    public Future<?> sendAsync(final Collection<TopicMessage> messages)
    {
        if (messages.isEmpty())
            return CompletableFuture.completedFuture(Collections.emptyList());
        else
            return CompletableFuture.allOf(messages.stream()
                .map(this::publishMessage)
                .toArray(CompletableFuture[]::new));

    }

    public void send(final TopicMessage message)
    {
        send(message, template.getExchange(), false);
    }

    public void send(final TopicMessage message, final String exchange, final boolean redelivered)
    {
        final Message rabbitMessage;
        final String routingKey = message.getRoutingKey();
        final MessageProperties properties = new MessageProperties();
        properties.setRedelivered(redelivered);

        try {
            rabbitMessage = new Message(objectMapper.writeValueAsBytes(message), properties);
        } catch(IOException e) {
            log.error("Message " + routingKey + " could not be serialized.", e);
            return;
        }

        try {
            template.send(exchange, routingKey, rabbitMessage);
            log.info("Message " + routingKey + " published to " + exchange);
        } catch (Exception e) {
            log.error("Message " + routingKey + " could not be published through " + exchange, e);
        }
    }

    private CompletableFuture<Void> publishMessage(TopicMessage message)
    {
        if (null == executorService) {
            CompletableFuture.runAsync(() -> send(message));
        }

        return CompletableFuture.runAsync(() -> send(message), executorService);
    }
}
