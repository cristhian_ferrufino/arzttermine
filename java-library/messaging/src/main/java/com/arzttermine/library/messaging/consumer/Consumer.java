package com.arzttermine.library.messaging.consumer;

import org.springframework.transaction.annotation.Transactional;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Transactional
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Consumer
{
    String queue() default "";

    String routingKey() default "";

    String exchange() default "";

    boolean durable() default true;

    boolean exclusive() default false;

    boolean autoDelete() default false;
}
