package com.arzttermine.library.messaging.processor;

import com.arzttermine.library.messaging.model.TopicMessage;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class MessageProcessor extends DefaultConsumer
{
    private static final Logger log = LoggerFactory.getLogger(MessageProcessor.class);

    private final ObjectMapper objectMapper;
    private final JavaType bodyType;
    private final Method consumer;
    private final Object provider;

    MessageProcessor(
        Channel channel,
        ObjectMapper objectMapper,
        JavaType bodyType,
        Method consumer,
        Object methodOwner
    ) {
        super(channel);
        this.objectMapper = objectMapper;
        this.bodyType = bodyType;
        this.consumer = consumer;
        this.provider = methodOwner;
    }

    @Override
    public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException
    {
        final TopicMessage message;
        log.info("Consumer[{}]: got message about {}", consumerTag, envelope.getRoutingKey());

        try {
            message = objectMapper.readValue(body, bodyType);
        } catch (IOException e) {
            log.error("Consumer[%s]: rejecting unreadable message queried by {}", consumerTag, envelope.getRoutingKey(), e);
            reject(consumerTag, envelope, false);
            return;
        }

        message.setRedelivered(envelope.isRedeliver());

        try {
            consumer.invoke(provider, message);
        } catch (InvocationTargetException e) {
            log.error("Consumer[%s]: error while processing message queried by {}", consumerTag, envelope.getRoutingKey(), e);
            reject(consumerTag, envelope, !envelope.isRedeliver());
            return;
        } catch (IllegalAccessException e) {
            log.error("Consumer[%s]: error while trying to invoke message processor for message {}", consumerTag, envelope.getRoutingKey(), e);
            reject(consumerTag, envelope, false);
            return;
        }

        log.debug(String.format(
            "Consumer[%s]: invoked. ready to acknowledge message queried by %s",
            consumerTag,
            envelope.getRoutingKey()
        ));

        acknowledge(consumerTag, envelope);
    }

    private void acknowledge(final String consumerTag, final Envelope envelope)
    {
        try {
            getChannel().basicAck(envelope.getDeliveryTag(), false);
            getChannel().txCommit();

            log.info(String.format(
                "Consumer[%s]: acknowledged incoming message queried by %s",
                consumerTag,
                envelope.getRoutingKey()
            ));
        } catch (Exception e) {
            log.error("Consumer[%s]: error while persisting message state (ack) queried by {}", consumerTag, envelope.getRoutingKey(), e);
        }
    }

    private void reject(final String consumerTag, final Envelope envelope, final boolean requeue)
    {
        try {
            getChannel().basicReject(envelope.getDeliveryTag(), requeue);
            getChannel().txCommit();

            log.info(String.format(
                "Consumer[%s]: rejected incoming message queried by %s, requeue: %s",
                consumerTag,
                envelope.getRoutingKey(),
                Boolean.valueOf(requeue).toString()
            ));
        } catch (Exception e) {
            log.error("Consumer[%s]: error while persisting message state (reject) queried by {}", consumerTag, envelope.getRoutingKey(), e);
        }
    }
}
