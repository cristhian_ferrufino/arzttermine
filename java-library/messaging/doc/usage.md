# messaging

### Define your message
```java
public class AppointmentBookedMessage extends TopicMessage
{
    private long appointmentId;

    public long getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(long appointmentId) {
        this.appointmentId = appointmentId;
    }

    @Override
    protected String getTopic() {
        return "booked";
    }

    @Override
    protected String getTopicKind() {
        return "booking";
    }
}
```

Message bound to the `com.arzttermine.service` exchange using the routing key `booking.booked`


### Publish a message
```java
public class Example
{
    @Autowired
    private MessageBus messageBus;
    
    public void send() {
        final AppointmentBookedMessage message = new AppointmentBookedMessage();
        message.setAppointmentId(result.getAppointmentId());
        message.setTime(Instant.now());
    
        messageBus.send(message);
        
        // or preferable asynchronously:
        messageBus.sendAsync(message);
    }
}
```

### Consume a message
```java
@Component
public class Example
{
    @Consumer
    public void send(AppointmentBookedMessage message) {
        System.out.println("Appointment was booked");
    }
}
```
