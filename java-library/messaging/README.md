# messaging

This module allows you to define message consumers via annotation `@Consumer`. 
You can also submit new messages to a queue using `MessageBus`.

### Usage
```xml
<dependency>
    <groupId>com.arzttermine.library</groupId>
    <artifactId>messaging</artifactId>
    <version>${library-messaging.version}</version>
    <scope>import</scope>
</dependency>

<!-- not included dependency -->
<dependency>
    <groupId>com.arzttermine.library</groupId>
    <artifactId>support</artifactId>
    <version>${library-support.version}</version>
</dependency>
```

Define your [consumers and publish](doc/usage.md) messages

If you want to define messages in a separate module you should import this module with `provided` scope:
 ```xml
<dependency>
    <groupId>com.arzttermine.library</groupId>
    <artifactId>messaging</artifactId>
    <version>${library-messaging.version}</version>
    <scope>provided</scope>
</dependency>
```

### Docker
Run a container providing rabbit-mq including the UI:  
```
docker run -d \
   -p 5671:5671 \
   -p 5672:5672 \
   -p 4369:4369 \
   -p 25672:25672 \
   -p 15672:15672 \
   --network=services \
   --name rabbitmq \
   rabbitmq:3-management
```

UI: `localhost:15672`  
Credentials:  
guest @ guest
