package com.arzttermine.library.suport.exception;

import com.arzttermine.library.web.dto.response.ErrorDto;
import com.arzttermine.library.web.struct.HttpStatus;

public abstract class BusinessException extends RuntimeException
{
    private final String code;
    private final Object reference;
    private final String message;
    private HttpStatus status;

    protected BusinessException(String code, Object reference, String message, Object... controls) {
        this.code = code;
        this.reference = reference;
        this.message = String.format(message, controls);
        status = HttpStatus.CONFLICT;
    }

    public BusinessException(ErrorDto error, HttpStatus status, Throwable cause) {
        super(cause);
        this.code = error.getCode();
        this.reference = error.getReference();
        this.message = error.getMessage();
        this.status = status;
    }

    public BusinessException(ErrorDto error, HttpStatus status) {
        this(error, status, null);
    }

    public HttpStatus getStatus() {
        return status;
    }

    public ErrorDto getErrorDto() {
        return new ErrorDto(code, reference, message);
    }

    protected void setStatus(HttpStatus status) {
        this.status = status;
    }
}
