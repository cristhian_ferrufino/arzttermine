package com.arzttermine.library.suport.exception;

import com.arzttermine.library.web.struct.HttpStatus;

public class AccessDeniedException extends BusinessException
{
    public AccessDeniedException() {
        super("ACCESS_DENIED", "", "Access is denied");
        setStatus(HttpStatus.FORBIDDEN);
    }
}
