package com.arzttermine.library.suport;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * This helper can be used to format instances of Instant or Date to the appropriate string.
 *
 * We use UTC Timezones system wide!
 */
public final class DateFormatterUtil
{
    public static final ZoneId applicationZone = ZoneId.of("UTC");

    public static final String applicationFormat = "yyyy-MM-dd'T'HH:mmXXX";

    public static final DateTimeFormatter formatter = DateTimeFormatter
            .ofPattern(applicationFormat)
            .withZone(applicationZone);

    public static String format(Instant instant)
    {
        return formatter.format(instant);
    }

    public static String format(Date date)
    {
        return format(date.toInstant());
    }

    public static Date parse(String date) throws ParseException
    {
        return new SimpleDateFormat(applicationFormat).parse(date);
    }
}
