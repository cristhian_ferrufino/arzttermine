package com.arzttermine.library.suport.exception;

import com.arzttermine.library.web.dto.response.ErrorDto;
import com.arzttermine.library.web.struct.HttpStatus;

public class EntityNotFoundException extends BusinessException
{
    public EntityNotFoundException(String entity, Object reference) {
        super("NOT_FOUND", reference, "The entity %s identified by %s was not found", entity, String.valueOf(reference));
        setStatus(HttpStatus.NOT_FOUND);
    }

    public EntityNotFoundException(ErrorDto error) {
        super(error, HttpStatus.NOT_FOUND);
    }
}

