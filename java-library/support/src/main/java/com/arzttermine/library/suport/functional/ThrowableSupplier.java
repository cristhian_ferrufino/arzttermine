package com.arzttermine.library.suport.functional;

@FunctionalInterface
public interface ThrowableSupplier<T>
{
    T get() throws Exception;
}
