package com.arzttermine.library.suport.templating;

import com.arzttermine.library.web.struct.Language;

import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractTemplateTask implements TemplateTask {

    private Map<String, String> params = new HashMap<>();

    private Language language = Language.GERMAN;

    @Override
    public Map<String, String> getParams() {
        return this.params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    @Override
    public Language getLanguage() {
        return this.language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    @Override
    public ZoneId getZoneId() {
        return ZoneId.of("UTC");
    }

    public void addParam(String key, String value) {
        if (null != key)
            if (null != value)
                params.put(key, value);
            else
                params.put(key, "");
    }
}
