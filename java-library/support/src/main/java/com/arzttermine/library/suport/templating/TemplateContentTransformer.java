package com.arzttermine.library.suport.templating;

public interface TemplateContentTransformer
{
    String getContent(TemplateTask task);
}
