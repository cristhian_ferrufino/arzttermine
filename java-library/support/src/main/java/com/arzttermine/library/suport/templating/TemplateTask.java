package com.arzttermine.library.suport.templating;

import com.arzttermine.library.web.struct.Language;

import java.time.ZoneId;
import java.util.Map;

public interface TemplateTask {

    String getPath();

    Map<String, String> getParams();

    Language getLanguage();

    ZoneId getZoneId();
}
