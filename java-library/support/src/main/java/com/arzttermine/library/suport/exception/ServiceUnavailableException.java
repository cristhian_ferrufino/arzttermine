package com.arzttermine.library.suport.exception;

import com.arzttermine.library.web.struct.HttpStatus;

public class ServiceUnavailableException extends BusinessException
{
    public ServiceUnavailableException(String service) {
        super("SERVICE_UNAVAILABLE", service, "The requested service is not available or request timed out");
        setStatus(HttpStatus.SERVICE_UNAVAILABLE);
    }
}
