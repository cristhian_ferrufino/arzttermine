package com.arzttermine.library.suport.templating.wrapper;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

public class TemplateEngineWrapper implements WrappableTemplateEngine
{
    private TemplateEngine engine;

    public TemplateEngineWrapper(TemplateEngine engine) {
        this.engine = engine;
    }

    @Override
    public String process(final String template, final Context context) {
        return engine.process(template, context);
    }
}
