package com.arzttermine.library.suport;

import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.DateTime;
import net.fortuna.ical4j.model.TimeZone;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.parameter.Cn;
import net.fortuna.ical4j.model.property.*;
import net.fortuna.ical4j.util.SimpleHostInfo;
import net.fortuna.ical4j.util.UidGenerator;

import java.net.URISyntaxException;
import java.time.Instant;
import java.util.Date;

public final class IcalUtil {

    private final static String prodId = "-//Arzttermine.de//AtWidget Appointment//EN";

    private final static String hostInfo = "arzttermine.de";

    private final static java.util.TimeZone timeZone = TimeZone.getTimeZone("Europe/Berlin");

    /**
     * @param start
     * @param end
     * @param organiserName
     * @param organiserEmail
     * @param summary
     * @param description
     * @param location
     * @param timezoneOffset in minutes, beware that setting the timezone overrides the timezone switch which is automatically done by the calendar if there is any (eg: CEST TO MEST)
     * @return Ical String
     */
    public static final String generateIcalEventString(Instant start, Instant end, String organiserName, String organiserEmail, String summary, String description, String location, Integer timezoneOffset) {
        return generateIcalEvent(start, end, organiserName, organiserEmail, summary, description, location, timezoneOffset).toString();
    }

    /**
     *
     * @param start
     * @param end
     * @param organiserName
     * @param organiserEmail
     * @param summary
     * @param description
     * @param location
     * @param timezoneOffset in minutes, beware that setting the timezone overrides the timezone switch which is automatically done by the calendar if there is any (eg: CEST TO MEST)
     * @return Calendar entry
     */
    public static final Calendar generateIcalEvent(Instant start, Instant end, String organiserName, String organiserEmail, String summary, String description, String location, Integer timezoneOffset) {
        Calendar calendar = new Calendar();
        calendar.getProperties().add(new ProdId(prodId));
        calendar.getProperties().add(Version.VERSION_2_0);
        calendar.getProperties().add(CalScale.GREGORIAN);

        Organizer organizer = new Organizer();
        VEvent appointment = generateAppointment(start, end, summary, description, location, timezoneOffset);

        try {
            organizer.setValue(organiserEmail);
            organizer.getParameters().add(new Cn(organiserName));
            appointment.getProperties().add(organizer);
        }
        catch (URISyntaxException e) {
            // No organiser for you.
        }

        // generate unique identifier..
        UidGenerator ug = new UidGenerator(new SimpleHostInfo(hostInfo), "uidGen");
        Uid uid = ug.generateUid();
        appointment.getProperties().add(uid);
        calendar.getComponents().add(appointment);

        return calendar;
    }

    public static final VEvent generateAppointment(Instant start, Instant end, String summary, String description, String location, Integer timezoneOffset) {
        DateTime eventStart = new DateTime(start.toEpochMilli());
        eventStart.setUtc(true);

        DateTime eventEnd = new DateTime(end.toEpochMilli());
        eventEnd.setUtc(true);

        if (null != timezoneOffset) {
            int timezoneOffsetMillis = 0;

            if (timeZone.observesDaylightTime()) {
                // remove daylight switch if start is during daylight saving time and current time not
                // {@TODO for now we use hardcoded europe/berlin as timezone. Needs improvements when we support more timezones}
                Date startDate = Date.from(start);

                if (timezoneOffset == 120 && !timeZone.inDaylightTime(startDate)) {
                    timezoneOffsetMillis += timeZone.getDSTSavings();
                } else if (timezoneOffset == 60 && timeZone.inDaylightTime(startDate)) {
                    timezoneOffsetMillis += -1 * timeZone.getDSTSavings();
                }
            }

            eventStart.setTime(eventStart.getTime() + timezoneOffsetMillis);
            eventEnd.setTime(eventEnd.getTime() + timezoneOffsetMillis);
        }

        VEvent appointment = new VEvent(eventStart, eventEnd, summary);
        appointment.getProperties().add(new Description(description));
        appointment.getProperties().add(new Location(location));

        return appointment;
    }
}
