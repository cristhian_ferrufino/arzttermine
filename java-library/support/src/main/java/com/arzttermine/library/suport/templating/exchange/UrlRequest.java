package com.arzttermine.library.suport.templating.exchange;

import com.arzttermine.library.suport.exception.InvalidArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Arrays;

/**
 * The {@code toString()} method can be used to pass an url instruction through our services without relying on a specific configuration.
 * It must start with {@code PREFIX} and contain at least a type {@link UrlRequest.Type} and a path segment.
 *
 * <b>Example:</b>
 * url://FRONTEND/events/42/cancel could be transformed to:
 * https://atcalendar.arzttermine.de/events/42/cancel
 *
 * The scheme, host, port etc. can be configured for each profile in the <i>targets.yml</i> for example.
 */
public class UrlRequest
{

    private static final Logger log = LoggerFactory.getLogger(UrlRequest.class);
    private static final String PREFIX = "url://";
    private static final int PREFIX_LENGTH = PREFIX.length();

    private String[] path;
    private String[] fragments;
    private Type type = Type.FRONTEND;
    private final MultiValueMap<String, String> query;

    public UrlRequest() {
        this.query = new LinkedMultiValueMap<>();
    }

    public UrlRequest(Type type, String[] path) {
        this(type, path, new String[]{}, new LinkedMultiValueMap<>());
    }

    public UrlRequest(Type type, String[] path, String[] fragments, MultiValueMap<String, String> query) {
        Assert.notNull(type, "type must be set to be able to determine the full qualified URI");
        Assert.notNull(query, "query must be set (at least present and empty)");

        this.path = path;
        this.type = type;
        this.query = query;
        this.fragments = fragments;
    }

    /**
     * To get the string representation use {@code toString()}
     *
     * Examples:
     * url://FRONTEND:my/path#fragment?query=1
     * url://FRONTEND:my/path#fragment
     * url://FRONTEND:#fragment
     * url://FRONTEND:my/path?query=1
     * url://FRONTEND:#?query=1
     *
     * @param uriString String
     * @return UrlRequest created based on the string representation of the url
     * @throws InvalidArgumentException if urlString is not valid
     * @throws IllegalArgumentException if url type is not valid or unknown
     */
    public static UrlRequest fromString(String uriString) {
        final String relevantUri = uriString.substring(PREFIX_LENGTH);
        final String[] relevantParts = relevantUri.split(":");
        if (relevantParts.length < 2) {
            throw new InvalidArgumentException("urlString", "url must contain at least the type and path segments");
        }

        final Type urlType = Type.valueOf(relevantParts[0]);
        final String fragsAndQueryPart = relevantParts[1];

        final String[] queryDetails = fragsAndQueryPart.split("\\?");
        final String[] fragmentsDetails = queryDetails[0].split("#");
        final String[] fragments;

        if (fragmentsDetails.length > 1) {
            fragments = fragmentsDetails[1].split("/");
        } else {
            fragments = new String[0];
        }

        // pathSegments are present in front of fragments (therefore using fragmentDetails[0])
        final String[] pathSegments = fragmentsDetails[0].split("/");
        final MultiValueMap<String, String> query = new LinkedMultiValueMap<>();

        if (queryDetails.length > 1) {
            final String[] queryParams = queryDetails[1].split("&");
            for (String param : queryParams) {
                final String[] keyValue = param.split("=");
                if (keyValue.length > 1) {
                    query.add(keyValue[0], keyValue[1]);
                }
            }
        }

        return new UrlRequest(urlType, pathSegments, fragments, query);
    }

    /**
     * @param url formatted url
     * @return true if starts with {@code PREFIX}
     */
    public static boolean isUrlRequest(String url) {
        return url.length() > PREFIX_LENGTH && url.substring(0, PREFIX_LENGTH).equals(PREFIX);
    }

    public Type getType() {
        return type;
    }

    public void setType(final Type type) {
        this.type = type;
    }

    public String[] getPath() {
        return path;
    }

    public void setPath(final String[] path) {
        this.path = path;
    }

    public MultiValueMap<String, String> getQuery() {
        return query;
    }

    public String[] getFragments() {
        return fragments;
    }

    public void setFragments(final String[] fragments) {
        this.fragments = fragments;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder(PREFIX);
        builder.append(type.name()).append(":");

        for (int i = 0; i < path.length; i++) {
            builder.append(path[i]);
            if (path.length > i +1) {
                builder.append("/");
            }
        }

        if (fragments.length > 0) {
            builder.append("#");
            for (int i = 0; i < fragments.length; i++) {
                builder.append(fragments[i]);
                if (fragments.length > i +1) {
                    builder.append("/");
                }
            }
        }

        if (query.isEmpty()) {
            return builder.toString();
        }

        builder.append("?");
        query.forEach((key, values) -> values.forEach(value -> {
            try {
                builder.append(key).append("=").append(URLEncoder.encode(value, "UTF-8")).append("&");
            } catch (UnsupportedEncodingException e) {
                log.error("error encoding value: " + value, e);
                builder.append(key).append("=").append(value).append("&");
            }
        }));

        final String result = builder.toString();
        return result.substring(0, result.length() -1);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final UrlRequest that = (UrlRequest) o;

        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        if (!Arrays.equals(path, that.path)) return false;
        if (type != that.type) return false;
        return query.equals(that.query);
    }

    @Override
    public int hashCode() {
        int result = Arrays.hashCode(path);
        result = 31 * result + type.hashCode();
        result = 31 * result + query.hashCode();
        return result;
    }

    public enum Type {
        FRONTEND,
        GOOGLE_MAPS,
        S3_ATCALENDAR,
    }
}
