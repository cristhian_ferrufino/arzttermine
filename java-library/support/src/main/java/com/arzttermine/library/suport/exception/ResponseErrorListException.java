package com.arzttermine.library.suport.exception;

import com.arzttermine.library.web.dto.response.ErrorDto;
import com.arzttermine.library.web.struct.HttpStatus;

import java.util.Collection;

public class ResponseErrorListException extends RuntimeException
{
    private final Collection<ErrorDto> errors;
    private HttpStatus status = HttpStatus.CONFLICT;

    public ResponseErrorListException(Collection<ErrorDto> errors, HttpStatus status) {
        this.errors = errors;
        this.status = (null == status) ? HttpStatus.CONFLICT : status;
    }

    public ResponseErrorListException(Collection<ErrorDto> errors) {
        this(errors, HttpStatus.CONFLICT);
    }

    public Collection<ErrorDto> getErrors() {
        return errors;
    }

    public HttpStatus getStatus() {
        return status;
    }

    protected void setStatus(HttpStatus status) {
        this.status = status;
    }
}
