package com.arzttermine.library.suport.templating;

import com.arzttermine.library.suport.templating.wrapper.TemplateEngineWrapper;
import com.arzttermine.library.suport.templating.wrapper.WrappableTemplateEngine;
import com.arzttermine.library.web.struct.Language;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.time.ZoneId;
import java.util.Locale;
import java.util.Optional;

public class TemplateTransformer implements TemplateContentTransformer
{
    private static final Logger log = LoggerFactory.getLogger(TemplateTransformer.class);

    private final WrappableTemplateEngine templateEngine;

    public TemplateTransformer(TemplateEngine templateEngine) {
        this.templateEngine = new TemplateEngineWrapper(templateEngine);
    }

    public TemplateTransformer(WrappableTemplateEngine templateEngine) {
        this.templateEngine = templateEngine;
    }

    public String getContent(final TemplateTask task) {
        final Context ctx = new Context(determineLocale(task.getLanguage()));
        ctx.setVariables(task.getParams());
        ctx.getVariables().put("requestedTimeZone", Optional.ofNullable(task.getZoneId()).orElse(ZoneId.of("CET")));

        log.info("trying to process template at path {}", task.getPath());

        return templateEngine.process(task.getPath(), ctx);
    }

    private Locale determineLocale(Language language) {
        final String languageTag;

        switch (language) {
            default:
                languageTag = "de-DE";
                break;
        }

        return Locale.forLanguageTag(languageTag);
    }
}

