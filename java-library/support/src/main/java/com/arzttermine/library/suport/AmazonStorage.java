package com.arzttermine.library.suport;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;
import com.arzttermine.library.web.struct.AmazonStorageResult;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import org.apache.commons.codec.digest.DigestUtils;

public class AmazonStorage {

    private static final Logger logger = LoggerFactory.getLogger(AmazonStorage.class);

    private final String defaultBucketName;

    private final AmazonS3 s3Client;

    public AmazonStorage(AmazonS3 s3Client, String defaultBucketName) {
        this.s3Client = s3Client;
        this.defaultBucketName = defaultBucketName;
    }

    public String getDefaultBucketName() {
        return defaultBucketName;
    }

    // Convenience method to simply handle string content. Uses default bucket and content formatting.
    public AmazonStorageResult upload(String keyName, String content, String mimeType) {

        if (content == null || content.isEmpty() || mimeType == null || mimeType.isEmpty())
            return AmazonStorageResult.BAD_REQUEST;

        InputStream stream = new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8));
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentType(mimeType);
        try {
            metadata.setContentLength(IOUtils.toByteArray(stream).length);
            stream.reset();
            /*
            // TODO : MD5 encoding..... cant get amazon to play nice with this though.
            byte[] resultByte = DigestUtils.md5(stream);
            String streamMD5 = new String(Base64.getEncoder().encode(resultByte));
            metadata.setContentMD5(streamMD5);
            */
        }
        catch (IOException e) {
            logger.error("Upload parsing failed: ", e);
            return AmazonStorageResult.BAD_CONTENT;
        }
        return this.upload(this.defaultBucketName, keyName, stream, metadata);
    }

    public AmazonStorageResult upload(String bucketName, String keyName, InputStream content, ObjectMetadata metadata) {
        AmazonStorageResult resultStatus = AmazonStorageResult.UNKNOWN_FAILURE;
        try {
            PutObjectResult result = s3Client.putObject(new PutObjectRequest(bucketName, cleanAttachmentKey(keyName), content, metadata));

            resultStatus = AmazonStorageResult.OK;

        } catch (AmazonServiceException exception) {

            logger.error("Upload (Server) failed: ", exception.getErrorCode(),  exception);
            switch (exception.getStatusCode()) {
                case 400:
                    resultStatus = AmazonStorageResult.BAD_CREDENTIALS;
                    break;
                case 404:
                    resultStatus = AmazonStorageResult.BUCKET_NOT_FOUND;
                    break;
                case 500:
                    resultStatus = AmazonStorageResult.SERVICE_ERROR;
                    break;
            }

        } catch (AmazonClientException exception) {
            logger.error("Upload (Client) failed: ", exception);
            resultStatus = AmazonStorageResult.BAD_REQUEST;
        }
        return resultStatus;
    }

    public S3Object download(String keyName) {
        return this.download(this.defaultBucketName, keyName);
    }

    public S3Object download(String bucketName, String keyName) {
        S3Object content = null;
        try {
            content = s3Client.getObject(bucketName, cleanAttachmentKey(keyName));
        } catch (AmazonServiceException exception) {
            logger.error("Upload (Server) failed: ", exception);
        } catch (AmazonClientException exception) {
            logger.error("Upload (Client) failed: ", exception);
        }
        return content;
    }

    private String cleanAttachmentKey(String attachmentKey) {
        if (attachmentKey.substring(0,1).equals("/"))
            attachmentKey = attachmentKey.substring(1);

        return attachmentKey;
    }
}
