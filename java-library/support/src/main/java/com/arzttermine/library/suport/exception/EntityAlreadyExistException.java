package com.arzttermine.library.suport.exception;

import com.arzttermine.library.web.dto.response.ErrorDto;
import com.arzttermine.library.web.struct.HttpStatus;

public class EntityAlreadyExistException extends BusinessException
{
    public EntityAlreadyExistException(String entity, String reference) {
        super("ALREADY_EXISTS", reference, "The entity %s identified by %s already exists", entity, reference);
    }

    public EntityAlreadyExistException(ErrorDto error) {
        super(error, HttpStatus.CONFLICT);
    }
}
