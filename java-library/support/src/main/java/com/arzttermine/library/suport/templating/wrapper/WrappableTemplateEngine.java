package com.arzttermine.library.suport.templating.wrapper;

import org.thymeleaf.context.Context;

public interface WrappableTemplateEngine {
    String process(String template, Context context);
}
