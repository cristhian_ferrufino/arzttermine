package com.arzttermine.library.suport.exception;

import com.arzttermine.library.web.struct.HttpStatus;

public class InvalidArgumentException extends BusinessException
{
    public InvalidArgumentException(String argument, String message) {
        super("INVALID_ARGUMENT", argument, message);
        setStatus(HttpStatus.BAD_REQUEST);
    }

    public InvalidArgumentException(String argument, Object value) {
        this(argument, String.format("%s is not valid for %s", String.valueOf(value), argument));
    }
}
