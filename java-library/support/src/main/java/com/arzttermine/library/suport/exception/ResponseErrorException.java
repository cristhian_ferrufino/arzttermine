package com.arzttermine.library.suport.exception;

import com.arzttermine.library.web.dto.response.ErrorDto;
import com.arzttermine.library.web.struct.HttpStatus;

public class ResponseErrorException extends BusinessException
{
    private Object response;
    private final Throwable cause;

    public ResponseErrorException(Object response, int code) {
        super("RESPONSE_ERROR", code, "request failed with code %d", code);
        setStatus(HttpStatus.fromCode(code).orElse(HttpStatus.CONFLICT));
        this.response = response;
        this.cause = null;
    }

    public ResponseErrorException(Object response, int code, Throwable cause) {
        super("RESPONSE_ERROR", code, "request failed with code %d", code);
        setStatus(HttpStatus.fromCode(code).orElse(HttpStatus.CONFLICT));
        this.response = response;
        this.cause = cause;
    }

    public ResponseErrorException(String message, String param, int code, Throwable cause) {
        super(new ErrorDto("RESPONSE_ERROR", param, message), HttpStatus.fromCode(code).orElse(HttpStatus.CONFLICT), cause);
        this.response = null;
        this.cause = cause;
    }

    public <T> T getResponse(Class<T> expected) {
        return expected.cast(response);
    }

    @Override
    public Throwable getCause() {
        return cause;
    }
}
