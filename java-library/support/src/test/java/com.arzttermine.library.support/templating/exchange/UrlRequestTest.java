package com.arzttermine.library.support.templating.exchange;

import com.arzttermine.library.suport.templating.exchange.UrlRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class UrlRequestTest
{
    @Test
    public void toString_should_return_string_representation_of_url_request()
    {
        MultiValueMap<String, String> query = new LinkedMultiValueMap<>();
        query.add("test", "1");
        query.add("test", "2");

        String[] pathSegments = new String[]{"test", "url"};

        UrlRequest dto = new UrlRequest(UrlRequest.Type.FRONTEND, pathSegments, new String[0], query);

        String result = dto.toString();

        assertEquals("url://FRONTEND:test/url?test=1&test=2", result);
    }

    @Test
    public void toString_should_escape_query_parameter_if_necessary()
    {
        MultiValueMap<String, String> query = new LinkedMultiValueMap<>();
        query.add("key", "value with spaces");
        query.add("test", "does+it&work?");

        UrlRequest dto = new UrlRequest(UrlRequest.Type.FRONTEND, new String[0], new String[0], query);

        String result = dto.toString();

        assertEquals("url://FRONTEND:?key=value+with+spaces&test=does%2Bit%26work%3F", result);
    }

    @Test
    public void fromString_should_create_equal_request_dto()
    {
        MultiValueMap<String, String> query = new LinkedMultiValueMap<>();
        query.add("test", "1");
        query.add("test", "2");

        String[] pathSegments = new String[]{"test", "url"};

        UrlRequest dto = new UrlRequest(UrlRequest.Type.FRONTEND, pathSegments, new String[0], query);
        UrlRequest constructed = UrlRequest.fromString(dto.toString());

        assertEquals(dto, constructed);
    }

    @Test
    public void fromString_should_create_request_dto_based_on_string()
    {
        UrlRequest dto = UrlRequest.fromString("url://FRONTEND:test/url?test=1&test=2");

        assertEquals(UrlRequest.Type.FRONTEND, dto.getType());
        assertEquals("test", dto.getPath()[0]);
        assertEquals("url", dto.getPath()[1]);
        assertEquals("1", dto.getQuery().get("test").get(0));
        assertEquals("2", dto.getQuery().get("test").get(1));
    }

    @Test
    public void fromString_should_not_create_empty_path_segments()
    {
        UrlRequest dto = UrlRequest.fromString("url://FRONTEND:test/url//?test=1&test=2");

        assertEquals("test", dto.getPath()[0]);
        assertEquals("url", dto.getPath()[1]);
        assertEquals(2, dto.getPath().length);
    }

    @Test
    public void fromString_should_determine_fragments()
    {
        UrlRequest dto = UrlRequest.fromString("url://FRONTEND:test/url#frag1/frag2?test=1");

        assertEquals(UrlRequest.Type.FRONTEND, dto.getType());
        assertEquals("test", dto.getPath()[0]);
        assertEquals("url", dto.getPath()[1]);
        assertEquals("1", dto.getQuery().get("test").get(0));
        assertEquals(2, dto.getFragments().length);
        assertEquals("frag1", dto.getFragments()[0]);
        assertEquals("frag2", dto.getFragments()[1]);
    }

    @Test
    public void fromString_should_determine_fragments_without_query_params()
    {
        UrlRequest dto = UrlRequest.fromString("url://FRONTEND:test#frag1/frag2");

        assertEquals("test", dto.getPath()[0]);
        assertEquals(2, dto.getFragments().length);
        assertEquals("frag1", dto.getFragments()[0]);
        assertEquals("frag2", dto.getFragments()[1]);
    }

    @Test
    public void isUrlRequest_should_return_true_for_url_string()
    {
        boolean isurl = UrlRequest.isUrlRequest("url://something");
        assertTrue(isurl);
    }

    @Test
    public void isUrlRequest_should_not_throw_out_of_bounds_if_not_a_url()
    {
        boolean isurl = UrlRequest.isUrlRequest("url");
        assertFalse(isurl);
    }

    @Test
    public void isUrlRequest_should_not_return_true_if_only_prefix_is_set()
    {
        boolean isurl = UrlRequest.isUrlRequest("url://");
        assertFalse(isurl);
    }

    @Test
    public void isUrlRequest_should_return_false()
    {
        boolean isurl = UrlRequest.isUrlRequest("url/something");
        assertFalse(isurl);
    }
}
