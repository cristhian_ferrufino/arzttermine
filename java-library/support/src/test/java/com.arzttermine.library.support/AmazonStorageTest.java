package com.arzttermine.library.support;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;
import com.arzttermine.library.suport.AmazonStorage;
import com.arzttermine.library.web.struct.AmazonStorageResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AmazonStorageTest {

    @Mock
    AmazonS3 s3Client;

    private final String defaultBucket = "transient.arzttermine.de";

    @Test
    public void upload_Should_Succeed_If_Parameters_Are_Present() {
        String keyName = "blah";
        String bucketName = defaultBucket;
        String stringContent = "Mother died today. Or maybe, yesterday; I can't be sure.";
        InputStream streamContent = new ByteArrayInputStream(stringContent.getBytes(StandardCharsets.UTF_8));
        AmazonStorage amazonStorage = new AmazonStorage(s3Client, defaultBucket);

        when(s3Client.putObject(any())).thenAnswer(invocation -> {
            PutObjectRequest request = (PutObjectRequest)invocation.getArguments()[0];
            assertEquals(request.getBucketName(), bucketName);
            assertEquals(request.getKey(), keyName);
            PutObjectResult response = new PutObjectResult();
            return response;
        });
        AmazonStorageResult success = amazonStorage.upload(bucketName, keyName, streamContent, new ObjectMetadata());

        assertEquals(AmazonStorageResult.OK, success);
    }

    @Test
    public void download_Should_Succeed_If_File_Present() {
        String keyName = "blah";
        String bucketName = defaultBucket;

        AmazonStorage amazonStorage = new AmazonStorage(s3Client, defaultBucket);

        when(s3Client.getObject(bucketName, keyName)).thenReturn(new S3Object());
        S3Object s3Object = amazonStorage.download(bucketName, keyName);

        assertNotNull(s3Object);
    }

    @Test
    public void upload_Convenience_Does_Not_Allow_Empty_Content() {
        String keyName = "blah";
        String content = "";
        String mimeType = "text/plain";

        AmazonStorage amazonStorage = new AmazonStorage(s3Client, defaultBucket);

        AmazonStorageResult success = amazonStorage.upload(keyName, content, mimeType);

        assertEquals(AmazonStorageResult.BAD_REQUEST, success);
    }

    @Test
    public void upload_Convenience_Does_Not_Allow_Empty_MimeType() {
        String keyName = "blah";
        String content = "April is the cruellest month, breeding / Lilacs out of the dead land, mixing / Memory and desire, stirring / Dull roots with spring rain.";
        String mimeType = "";

        AmazonStorage amazonStorage = new AmazonStorage(s3Client, defaultBucket);

        AmazonStorageResult success = amazonStorage.upload(keyName, content, mimeType);

        assertEquals(AmazonStorageResult.BAD_REQUEST, success);
    }

    @Test
    public void upload_Convenience_Passes_Valid_Parameters_To_S3() {
        String keyName = "blah";
        String content = "April is the cruellest month, breeding / Lilacs out of the dead land, mixing / Memory and desire, stirring / Dull roots with spring rain.";
        String mimeType = "text/plain";

        AmazonStorage amazonStorage = new AmazonStorage(s3Client, defaultBucket);

        when(s3Client.putObject(any())).thenReturn(new PutObjectResult());

        AmazonStorageResult success = amazonStorage.upload(keyName, content, mimeType);

        assertEquals(AmazonStorageResult.OK, success);
    }


}
