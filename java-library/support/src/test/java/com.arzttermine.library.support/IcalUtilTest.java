package com.arzttermine.library.support;

import com.arzttermine.library.suport.IcalUtil;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.validate.CalendarValidatorImpl;
import net.fortuna.ical4j.validate.Validator;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class IcalUtilTest {

    @Test
    public void generate_Ical_Event_Should_Generate_Ical_Event() {

        String summary = "Bob has an appointment.";
        String description = "Bob loves taking appointments. Meetings make Bob feel important.";
        String location = "Bobs House";

        Calendar icalEvent = IcalUtil.generateIcalEvent(
            Instant.now(),
            Instant.now().plus(60, ChronoUnit.MINUTES),
            "Bob",
            "bob@awesomeco.com",
            summary,
            description,
            location,
            null
        );
        assertNotNull(icalEvent);

        Validator<Calendar> validator = new CalendarValidatorImpl();
        validator.validate(icalEvent);
        // Exception is thrown if invalid.
    }

    @Test
    public void generateAppointment_should_not_add_offset_for_daylight_saving_time()
    {
        Instant start = Instant.parse("2017-09-27T10:00:00.00Z");
        Instant end = Instant.parse("2017-09-27T11:00:00.00Z");

        VEvent event = IcalUtil.generateAppointment(start, end, "summary", "description", "location", 120);

        assertEquals(start.getEpochSecond(), event.getStartDate().getDate().getTime() / 1000);
        assertEquals(end.getEpochSecond(), event.getEndDate().getDate().getTime() / 1000);
    }

    @Test
    public void generateAppointment_should_add_offset_for_dates_not_during_daylight_saving_but_created_in_daylight_saving_time()
    {
        Instant start = Instant.parse("2017-11-11T10:00:00.00Z");
        Instant end = Instant.parse("2017-11-11T11:00:00.00Z");

        VEvent event = IcalUtil.generateAppointment(start, end, "summary", "description", "location", 120);

        assertEquals(start.getEpochSecond() + 3600, event.getStartDate().getDate().getTime() / 1000);
        assertEquals(end.getEpochSecond() + 3600, event.getEndDate().getDate().getTime() / 1000);
    }

    @Test
    public void generateAppointment_should_not_add_offset_for_dates_during_daylight_saving_but_created_not_during_daylight_saving_time()
    {
        Instant start = Instant.parse("2017-09-11T10:00:00.00Z");
        Instant end = Instant.parse("2017-09-11T11:00:00.00Z");

        VEvent event = IcalUtil.generateAppointment(start, end, "summary", "description", "location", 60);

        assertEquals(start.getEpochSecond() - 3600, event.getStartDate().getDate().getTime() / 1000);
        assertEquals(end.getEpochSecond() - 3600, event.getEndDate().getDate().getTime() / 1000);
    }
}
