package com.arzttermine.library.suport.templating;

import com.arzttermine.library.suport.templating.wrapper.WrappableTemplateEngine;
import com.arzttermine.library.web.struct.Language;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.thymeleaf.context.Context;

import java.util.Locale;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class TemplateTransformerTest
{
    private WrappableTemplateEngine engine;

    private TemplateTransformer transformer;

    @Before
    public void setUp() {
        engine = mock(WrappableTemplateEngine.class);
        transformer = new TemplateTransformer(engine);
    }

    @Test
    public void getContent_should_call_engine_with_correct_params()
    {
        TemplateTaskImpl task = new TemplateTaskImpl();
        task.addParam("key", "value");

        doAnswer(invocation -> {
            Context ctx = (Context) invocation.getArguments()[1];
            assertTrue(ctx.getVariables().containsKey("key"));
            assertEquals("value", ctx.getVariables().get("key"));

            return "result";
        }).when(engine).process(eq(task.getPath()), any());

        transformer.getContent(task);

        verify(engine, times(1)).process(eq(task.getPath()), any());
    }

    @Test
    public void getContent_should_pass_determined_locale()
    {
        TemplateTaskImpl task = new TemplateTaskImpl();

        doAnswer(invocation -> {
            Context ctx = (Context) invocation.getArguments()[1];
            assertEquals(Locale.forLanguageTag("de-DE"), ctx.getLocale());

            return "result";
        }).when(engine).process(eq(task.getPath()), any());

        transformer.getContent(task);

        verify(engine, times(1)).process(eq(task.getPath()), any());
    }

    @Test
    public void getContent_should_fallback_to_default_locale_if_language_is_unknown()
    {
        TemplateTaskImpl task = new TemplateTaskImpl();
        task.setLanguage(Language.CHINESE);

        doAnswer(invocation -> {
            Context ctx = (Context) invocation.getArguments()[1];
            assertEquals(Locale.forLanguageTag("de-DE"), ctx.getLocale());

            return "result";
        }).when(engine).process(eq(task.getPath()), any());

        transformer.getContent(task);

        verify(engine, times(1)).process(eq(task.getPath()), any());
    }

    private static final class TemplateTaskImpl extends AbstractTemplateTask
    {
        @Override
        public String getPath() {
            return "path/to/template";
        }
    }
}
