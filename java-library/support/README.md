# support

This module provides useful utilities like a DateFormatterUtil, base exceptions and more
### Usage
```xml
<dependency>
    <groupId>com.arzttermine.library</groupId>
    <artifactId>support</artifactId>
    <version>${library-support.version}</version>
</dependency>

<!-- not included dependency -->
<dependency>
    <groupId>com.arzttermine.library</groupId>
    <artifactId>web</artifactId>
    <version>${library-web.version}</version>
</dependency>
```
