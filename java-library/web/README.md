# web

This module stores common DTOs.

Feel free to add new DTOS which are used in multiple applications the same way.

### Usage
```xml
<dependency>
    <groupId>com.arzttermine.library</groupId>
    <artifactId>web</artifactId>
    <version>${library-web.version}</version>
</dependency>
```
