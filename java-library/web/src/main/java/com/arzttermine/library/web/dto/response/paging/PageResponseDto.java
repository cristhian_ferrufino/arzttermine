package com.arzttermine.library.web.dto.response.paging;

import java.util.List;

public class PageResponseDto<ElementType>
{
    private List<ElementType> elements;

    private PageDto page;

    public List<ElementType> getElements() {
        return elements;
    }

    public void setElements(List<ElementType> elements) {
        this.elements = elements;
    }

    public PageDto getPage() {
        return page;
    }

    public void setPage(PageDto page) {
        this.page = page;
    }
}
