package com.arzttermine.library.web.dto;

import com.arzttermine.library.web.struct.Currency;

public class PriceDto
{
    private int amount;
    private Currency currency = Currency.EUR;

    public PriceDto() {
    }

    public PriceDto(int amount) {
        this.amount = amount;
    }

    public PriceDto(int amount, Currency currency) {
        this.amount = amount;
        this.currency = currency;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}
