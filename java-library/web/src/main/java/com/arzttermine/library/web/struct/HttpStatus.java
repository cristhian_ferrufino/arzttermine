package com.arzttermine.library.web.struct;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

public enum HttpStatus
{
    OK(200),
    CREATED(201),
    NO_CONTENT(204),
    BAD_REQUEST(400),
    UNAUTHORIZED(401),
    PAYMENT_REQUIRED(402),
    FORBIDDEN(403),
    NOT_FOUND(404),
    CONFLICT(409),
    UNSUPPORTED_MEDIA_TYPE(415),
    INTERNAL_SERVER_ERROR(500),
    BAD_GATEWAY(502),
    SERVICE_UNAVAILABLE(503);

    private static final Collection<HttpStatus> statuses = Collections.unmodifiableCollection(Arrays.asList(HttpStatus.values()));

    private int code;

    HttpStatus(int code) {
        this.code = code;
    }

    public static Optional<HttpStatus> fromCode(int code) {
        return statuses.stream().filter(s -> s.code == code).findFirst();
    }

    public int getCode() {
        return code;
    }
}
