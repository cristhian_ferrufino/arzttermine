package com.arzttermine.library.web.struct;

public enum InsuranceType
{
    PRIVATE,
    COMPULSORY,
}
