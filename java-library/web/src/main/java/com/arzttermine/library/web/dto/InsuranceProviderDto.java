package com.arzttermine.library.web.dto;

import com.arzttermine.library.web.struct.InsuranceType;

public class InsuranceProviderDto
{
    private InsuranceType type;

    private String provider;

    public InsuranceType getType() {
        return type;
    }

    public void setType(InsuranceType type) {
        this.type = type;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }
}
