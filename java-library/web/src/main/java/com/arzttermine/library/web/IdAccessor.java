package com.arzttermine.library.web;

public interface IdAccessor
{
    long getId();
}
