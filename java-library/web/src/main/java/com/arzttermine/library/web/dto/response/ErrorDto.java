package com.arzttermine.library.web.dto.response;

public class ErrorDto
{
    private String code;

    private Object reference;

    private String message;

    public ErrorDto() {
    }

    public ErrorDto(String code, Object reference, String message)
    {
        this.code = code;
        this.reference = reference;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Object getReference() {
        return reference;
    }

    public void setReference(Object reference) {
        this.reference = reference;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
