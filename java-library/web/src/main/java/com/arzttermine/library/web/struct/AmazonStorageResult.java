package com.arzttermine.library.web.struct;

public enum AmazonStorageResult {
    OK,
    BAD_REQUEST,
    BAD_CREDENTIALS,
    BAD_CONTENT,
    BUCKET_NOT_FOUND,
    SERVICE_ERROR,
    NETWORK_DOWN,
    UNKNOWN_FAILURE
}
