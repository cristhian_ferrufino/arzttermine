# Deployments

### Prepare for deployment
When deploying your project to our EC2-Instances we need to know, how to deploy.
The deployment includes information about how to start/stop services, etc.

Here you can fine some general rules to guarantee a seamless deployment:
1. Dont store unrelated dist or cached files in the build artifact neither in your vcs
2. Let your build decide about the profile and parameters to use (eg: via environment variable, multiple buildspec.yml etc.)
3. Execute the tests during the build phase and skip them during deployment
4. Dont blow up the production machine with unnecessary files, services etc.
5. Make sure the service-role of your CodeDeploy project can access the EC2-Instance (specify IAM role when launching instance -> `ecsInstanceRole`)
6. During the deploy process your instance should be taken out from load balancer if there is any
7. If there is a traffic increase soon: Change AWS deployment config and count of running instances to make zero-downtime deployments
8. Remove dist files from build artifacts when using S3 or something else to deliver them
9. Start your application / instance in a private availability zone

##### Useful hints
- The instance will be out of service during the restart. You might want to start another machine before, if there is only one.
- The build is happening on a different machine (looks like AWS uses docker and usually sadly ubuntu 14.04).  
- The deployment is happening on the production machine. Be aware of this fact!
- [appspec.yml] `ApplicationStop` executes specified scripts from **previous** deployment if there is any
- Do not drop deployments manually
- Deployment archive will be extracted to `/opt/codedeploy-agent/deployment-root/{GUID_FOR_PIPELINE}/{DEPLOYMENT_ID}/deployment-archive`

### Staging
[Read more](staging.md)

### Setup deployment

New Repository? Dont forget to grant [at-deployment](https://github.com/at-deployment) github user (see 1Password) appropriate permissions on the repository. Consider a ready only access in our [it group](https://github.com/orgs/arzttermine/teams/it/members)

- Run new EC2 instance
    1.  Specify the IAM role with s3, code deploy and ec2 permissions
    ```
    Permissions:
    AmazonS3FullAccess
    AWSCodeDeployRole
    AmazonEC2ContainerServiceforEC2Role
    ```
    2. Create identity providers for codedeploy and ec2:
    ```
    Trusted entities
    The identity provider(s) codedeploy.amazonaws.com
    The identity provider(s) ec2.amazonaws.com
    {
      "Version": "2008-10-17",
      "Statement": [
        {
          "Sid": "AtDeployment",
          "Effect": "Allow",
          "Principal": {
            "Service": "codedeploy.amazonaws.com"
          },
          "Action": "sts:AssumeRole"
        },
        {
          "Sid": "AtDeploymentEC2",
          "Effect": "Allow",
          "Principal": {
            "Service": "ec2.amazonaws.com"
          },
          "Action": "sts:AssumeRole"
        }
      ]
    }
    ```
- Install deployment agent or use existing image with codedeploy agent (if present)
```
sudo apt-get update
sudo apt-get install ruby
cd /home/ubuntu # or any other home directory
wget https://aws-codedeploy-eu-central-1.s3.amazonaws.com/latest/install
chmod +x ./install
sudo ./install auto
sudo service codedeploy-agent status
# if not running:
sudo service codedeploy-agent start
# include it in auto start
sudo update-rc.d codedeploy-agent defaults
# log file:
cat /var/log/aws/codedeploy-agent/codedeploy-agent.log
```
If you need to run all these commands please clean up and create a new fresh AMI before making first deployments so it can be used for further instances.

You can find existing AMI's [here](https://eu-central-1.console.aws.amazon.com/ec2/v2/home?region=eu-central-1#Images:sort=desc:creationDate)
As for now we have the following available:
1. [`base-java-image`](https://eu-central-1.console.aws.amazon.com/ec2/v2/home?region=eu-central-1#Images:visibility=owned-by-me;name=base-java-image;sort=desc:creationDate) to run jar based services
2. [`base-php-image`](https://eu-central-1.console.aws.amazon.com/ec2/v2/home?region=eu-central-1#Images:visibility=owned-by-me;name=base-php-image;sort=desc:creationDate) to run php and nginx based frontend apps
2. [`base-staging-image`](https://eu-central-1.console.aws.amazon.com/ec2/v2/home?region=eu-central-1#Images:visibility=owned-by-me;name=base-staging-image;sort=desc:creationDate) to run single staging instance with php & java

All images are created with codedeploy-agent / deployment support.

#### Connect your source with CodeBuild and CodeDeploy via AWS console using Pipelines  

When creating a new pipeline, AWS console guides you through all necessary steps in a very easy way.
You can also use an existing pipeline and change the json config using the aws-cli.

Fundamental steps when [creating a new pipeline](https://eu-central-1.console.aws.amazon.com/codepipeline/home?region=eu-central-1#/create/Name) in AWS console:
1. Use github as source provider  
2. Create CodeDeploy project with same service role as used to manage the ec2 instance  
3. Use CodeBuild as build provider  
3.1. Create a new application for a global app (eg: arzttermine.de, atcalendar, ...)  
3.2. Create a new deployment group in a application for a new service-type or specific build (eg staging-notification-service/production-calendar-service, ...)  
3.3. Do not use the same deployment config for apps on the same instance
4. Create Pipeline or add your deployment/build to an existing pipeline  
