# AWS and Cloud

1. [Deployments](deploy.md)  
  1.1 Deploy a [java service](https://github.com/arzttermine/documentation/wiki/Java-in-Detail#deployment)
2. [Staging](https://github.com/arzttermine/documentation/wiki/AWS-&-Cloud#staging)


### Examples
- [PHP Deployment files](/files/deployment/php)  
- [Java Deployment files](/files/deployment/java)
- [AWS CodePipelines](pipelines)
