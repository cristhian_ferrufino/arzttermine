# documentation

[Read our Wiki](https://github.com/arzttermine/documentation/wiki)

[Deployment & Staging](aws)


### Scripts
- [Download all repositories](files/download.sh)
- [Start all service containers](files/services.sh)
- [Handling docker images](images/)
- [All about deployment & Cloudformation](files/deployment)
