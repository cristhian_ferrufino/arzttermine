#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo $DIR

if [ ! "$(docker network ls | grep services)" ]; then
    docker network create services
fi

RUNNING=$(docker ps -a)

function requiresStartup {
    if [ "$(echo $RUNNING | grep $1)" ]; then
        echo "Skipping $1 as it already exists. Run <docker start $1> if not running and observe logs"
        return 1
    else
        return 0
    fi
}

if requiresStartup rabbitmq; then
    docker run -d \
      -p 5671:5671 \
      -p 5672:5672 \
      -p 4369:4369 \
      -p 25672:25672 \
      -p 15672:15672 \
      --network=services \
      --name rabbitmq \
      rabbitmq:3-management
fi

if requiresStartup elasticsearch; then
    docker run -d \
      -p 9200:9200 \
      -p 9300:9300 \
      --network=services \
      -v /Users/tim/work/dev/documentation/files/elasticsearch/config/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml \
      --name elasticsearch \
      docker.elastic.co/elasticsearch/elasticsearch:5.4.0
fi

if requiresStartup logstash; then
    docker run -d \
        -p 4560:4560 \
        --network=services \
        -v $DIR/logstash/pipeline/:/usr/share/logstash/pipeline/ \
        -v $DIR/logstash/config/:/usr/share/logstash/config/ \
        --name logstash \
        -it docker.elastic.co/logstash/logstash:5.4.0
fi
