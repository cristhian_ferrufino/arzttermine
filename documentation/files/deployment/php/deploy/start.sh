#!/bin/bash

DIR="$(dirname "${BASH_SOURCE[0]}")"

# if you want to copy nginx configurations (assuming you have this structure: project-root/deploy/nginx):
# this is basically a good idea because you can update your nginx config with a deployment
yes | sudo cp -rf ${DIR}/nginx/* /etc/nginx/conf.d
sudo rm -rf ${DIR}/../var/cache/prod

sudo service nginx restart
sudo service php7.0-fpm restart
