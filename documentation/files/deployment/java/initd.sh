#!/bin/sh

# this is already included in the base-java-image AMI
# location: /opt/service/make_jar_service.sh
# usage: /opt/service/make_jar_service.sh {SERVICE}

SERVICE_NAME=$1
INIT_FILE="/etc/init.d/$SERVICE_NAME"

if [ -f ${INIT_FILE} ]; then
    echo "Deployed to already known environment"
    echo "Skipping init.d script creation"
else
    # copy the template from its path on the AMI
    sudo cp /etc/init.d/skeleton-java ${INIT_FILE}
    sudo sed -i "s/%SERVICE_NAME%/$SERVICE_NAME/g" ${INIT_FILE}
    sudo chown ubuntu:ubuntu ${INIT_FILE}

    echo "Created init.d script at $INIT_FILE"

    # set default runlevels etc and enable service for autostart
    sudo update-rc.d ${SERVICE_NAME} defaults

    if [ $? != 0 ]; then
        echo "WARN: Could not add $INIT_FILE to autostart!"
    fi
fi
