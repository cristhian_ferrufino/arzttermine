#!/bin/sh

### BEGIN INIT INFO
# Provides:          %SERVICE_NAME%
# Required-Start:    $local_fs
# Required-Stop:     $local_fs
# Default-Start:     2 3
# Default-Stop:      0 1 6
# Short-Description: %SERVICE_NAME%
# Description:       generic init.d/service template-script for .jar based services included in our base-java-image AMI
### END INIT INFO

SERVICE_NAME="%SERVICE_NAME%"
PATH_TO_SERVICE="/opt/service/$SERVICE_NAME"
PATH_TO_JAR="$PATH_TO_SERVICE/service.jar"
PATH_TO_PID="$PATH_TO_SERVICE/service.pid"

ERROR_LOG="$PATH_TO_SERVICE/error.log"
OUTPUT_LOG="$PATH_TO_SERVICE/output.log"

safeStartCmd() {
    echo "$SERVICE_NAME starting ..."
    java -jar -Dspring.profiles.active={TARGET_PROFILE} ${PATH_TO_JAR} 2> ${ERROR_LOG} > ${OUTPUT_LOG} &

    ERROR_CODE=$?
    if [ ${ERROR_CODE} != 0 ]; then
        echo "$SERVICE_NAME could not be started"
        exit ${ERROR_CODE}
    else
        echo $! > ${PATH_TO_PID}
        echo "$SERVICE_NAME started ..."
    fi
}

cleanStopCmd() {
    PID=$(cat ${PATH_TO_PID});
    echo "$SERVICE_NAME stopping ..."
    kill ${PID}
    echo "$SERVICE_NAME stopped ..."
    rm ${PATH_TO_PID}
}

case $1 in
    start)
        echo "Starting $SERVICE_NAME ..."
        if [ ! -f ${PATH_TO_PID} ]; then
            safeStartCmd
        else
            echo "$SERVICE_NAME is already running ..."
        fi
    ;;
    stop)
        echo "Stopping $SERVICE_NAME ..."
        if [ -f ${PATH_TO_PID} ]; then
            cleanStopCmd
        else
            echo "$SERVICE_NAME is not running ..."
        fi
    ;;
    restart)
        echo "Restarting $SERVICE_NAME ..."
        if [ -f ${PATH_TO_PID} ]; then
            cleanStopCmd
            safeStartCmd
            echo "$SERVICE_NAME restarted ..."
        else
            echo "$SERVICE_NAME is not running ..."
        fi
    ;;
    status)
        echo "Guessing status of $SERVICE_NAME by existence of $PATH_TO_PID:"
        if [ -f ${PATH_TO_PID} ]; then
            echo "$SERVICE_NAME is running ..."
        else
            echo "$SERVICE_NAME is not running ..."
        fi
    ;;
esac
