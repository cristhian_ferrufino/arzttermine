#!/bin/bash

REPOS=(
	arzttermine.de
	doctorio
	calendar-service
	profile-service
	booking-service
	payment-service
	notification-service
	widget-booking
)

for i in ${REPOS[@]}; do
	git clone git@github.com:arzttermine/${i}.git
done
