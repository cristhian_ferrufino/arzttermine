# Docker Registry

### Structure
Each image should be stored in a separate directory.
Each version of an image should also be stored in a separate directory.

Example:
`images/{image-name}/{major.patch}/Dockerfile`

Please also put a `build.sh` script into the image folder.  
See [example](spring-image/build.sh)

### Publish images

1. Make sure you are [connected to the registry](https://github.com/arzttermine/documentation/wiki/Docker#connect-to-the-registry).
2. Run `./publish.sh {image-name} {version}`
3. Follow information from output
