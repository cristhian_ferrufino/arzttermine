#!/bin/sh

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
registry=docker-registry.arzttermine.de

cd "$DIR/$1/$2"
docker build -t arzttermine/$1:$2 .
docker tag arzttermine/$1:$2 ${registry}/arzttermine/$1:$2

echo "To update the image in our registry run the following: docker push $registry/arzttermine/$1:$2"
