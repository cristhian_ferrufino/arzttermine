package com.arzttermine.service.booking.messages;

import com.arzttermine.library.messaging.model.TopicMessage;

import java.time.Instant;

public class AppointmentBookedMessage extends TopicMessage
{
    private long appointmentId;

    private long customerId;

    public long getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(long appointmentId) {
        this.appointmentId = appointmentId;
    }

    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    @Override
    protected String getTopic() {
        return "booked";
    }

    @Override
    protected String getTopicKind() {
        return "booking";
    }
}
