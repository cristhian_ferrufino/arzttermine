package com.arzttermine.service.booking.messages;

import com.arzttermine.library.messaging.model.TopicMessage;

import java.time.Instant;
import java.util.Date;

public class BookingConfirmedMessage extends TopicMessage
{
    private Date confirmedAt = Date.from(Instant.now());

    private long bookingId;

    private long customerId;

    private long practiceId;

    public Date getConfirmedAt() {
        return confirmedAt;
    }

    public void setConfirmedAt(Date confirmedAt) {
        this.confirmedAt = confirmedAt;
    }

    public long getBookingId() {
        return bookingId;
    }

    public void setBookingId(long bookingId) {
        this.bookingId = bookingId;
    }

    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(long practiceId) {
        this.practiceId = practiceId;
    }

    @Override
    protected String getTopic() {
        return "confirmed";
    }

    @Override
    protected String getTopicKind() {
        return "booking";
    }
}
