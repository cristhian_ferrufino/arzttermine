package com.arzttermine.service.booking.messages;

import com.arzttermine.library.messaging.model.TopicMessage;
import com.arzttermine.service.booking.web.struct.BookingStatus;

public class BookingCancelledMessage extends TopicMessage
{
    private boolean appointmentAvailable;
    private BookingStatus status;
    private long appointmentId;
    private long customerId;
    private long bookingId;

    public BookingStatus getStatus() {
        return status;
    }

    public void setStatus(BookingStatus status) {
        this.status = status;
    }

    public long getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(long appointmentId) {
        this.appointmentId = appointmentId;
    }

    public long getBookingId() {
        return bookingId;
    }

    public void setBookingId(long bookingId) {
        this.bookingId = bookingId;
    }

    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public boolean isAppointmentAvailable() {
        return appointmentAvailable;
    }

    public void setAppointmentAvailable(boolean appointmentAvailable) {
        this.appointmentAvailable = appointmentAvailable;
    }

    @Override
    protected String getTopic() {
        return "canceled";
    }

    @Override
    protected String getTopicKind() {
        return "booking";
    }
}
