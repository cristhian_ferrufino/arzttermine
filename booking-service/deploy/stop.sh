#!/bin/bash

DIR="$(dirname "${BASH_SOURCE[0]}")"
source ${DIR}/env

if [ -f /etc/init.d/${SERVICE_NAME} ]; then
    /etc/init.d/${SERVICE_NAME} stop
fi
