[![Build Status](https://travis-ci.com/arzttermine/booking-service.svg?token=XHsxG8wHND1xUrHu4SwY&branch=master)](https://travis-ci.com/arzttermine/booking-service)

# booking-service

[Read more](https://github.com/arzttermine/documentation/wiki/Java-in-Detail) about how to manage java applications

### Documentation

To see how to [setup java applications](https://github.com/arzttermine/documentation/wiki/Service-Setup#java--backend) follow the link.

Our API is documented using Swagger. You can access the documentation in development/default or docker profile via:
`/swagger-ui.html`

### Example documents
[see here](doc/booking.md)
