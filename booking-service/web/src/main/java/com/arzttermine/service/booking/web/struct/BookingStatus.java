package com.arzttermine.service.booking.web.struct;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public enum BookingStatus
{
    DONE,
    CREATED,
    PENDING,
    CONFIRMED,
    REJECTED,
    CANCELLED_BY_EMPLOYEE,
    CANCELLED_BY_PATIENT,
    CANCELLED_BY_PRACTICE,
    PRACTICE_NOT_REACHED,
    RECEIVED_ALTERNATIVE;

    public static final Collection<BookingStatus> released = Collections.unmodifiableCollection(Arrays.asList(
        CANCELLED_BY_PATIENT,
        CANCELLED_BY_PRACTICE,
        CANCELLED_BY_EMPLOYEE,
        RECEIVED_ALTERNATIVE,
        REJECTED,
        DONE
    ));
}
