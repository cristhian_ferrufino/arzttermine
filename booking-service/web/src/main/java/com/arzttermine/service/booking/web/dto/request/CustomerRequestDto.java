package com.arzttermine.service.booking.web.dto.request;

import com.arzttermine.service.profile.web.dto.request.CreateCustomerDto;

import javax.validation.constraints.NotNull;

public class CustomerRequestDto extends CreateCustomerDto
{
    @NotNull
    private Boolean returning = false;

    public Boolean isReturning() {
        return returning;
    }

    public void setReturning(Boolean returning) {
        this.returning = returning;
    }
}
