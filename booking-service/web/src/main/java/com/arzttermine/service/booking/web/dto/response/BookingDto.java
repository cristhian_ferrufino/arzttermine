package com.arzttermine.service.booking.web.dto.response;

import com.arzttermine.library.web.IdAccessor;
import com.arzttermine.library.web.dto.PriceDto;
import com.arzttermine.library.web.struct.Language;
import com.arzttermine.service.booking.web.dto.request.NotificationSettingsDto;
import com.arzttermine.service.booking.web.struct.BookingStatus;
import com.arzttermine.service.calendar.web.dto.response.AppointmentDto;

import java.util.Date;
import java.util.UUID;

public class BookingDto implements IdAccessor
{
    private long id;
    private UUID uuid;
    private CustomerDetailsDto customer;
    private boolean paid = false;
    private boolean paymentInitialised = false;
    private AppointmentDto appointment;
    private BookingStatus status;
    private String practiceComment;
    private PriceDto totalPrice;
    private Language language;
    private String title;
    private long treatmentTypeId;
    private NotificationSettingsDto notificationSettings;
    private Date createdAt;
    private int version;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public CustomerDetailsDto getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDetailsDto customer) {
        this.customer = customer;
    }

    public AppointmentDto getAppointment() {
        return appointment;
    }

    public void setAppointment(AppointmentDto appointment) {
        this.appointment = appointment;
    }

    public BookingStatus getStatus() {
        return status;
    }

    public void setStatus(BookingStatus status) {
        this.status = status;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public String getPracticeComment() {
        return practiceComment;
    }

    public void setPracticeComment(String practiceComment) {
        this.practiceComment = practiceComment;
    }

    public boolean isPaymentInitialised() {
        return paymentInitialised;
    }

    public void setPaymentInitialised(boolean paymentInitialised) {
        this.paymentInitialised = paymentInitialised;
    }

    public PriceDto getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(PriceDto totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getTreatmentTypeId() {
        return treatmentTypeId;
    }

    public void setTreatmentTypeId(long treatmentTypeId) {
        this.treatmentTypeId = treatmentTypeId;
    }

    public NotificationSettingsDto getNotificationSettings() {
        return notificationSettings;
    }

    public void setNotificationSettings(NotificationSettingsDto notificationSettings) {
        this.notificationSettings = notificationSettings;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(final Date createdAt) {
        this.createdAt = createdAt;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(final UUID uuid) {
        this.uuid = uuid;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }
}
