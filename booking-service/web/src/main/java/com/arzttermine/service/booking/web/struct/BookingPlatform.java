package com.arzttermine.service.booking.web.struct;

public enum BookingPlatform
{
    WIDGET,
    WEBSITE,
}
