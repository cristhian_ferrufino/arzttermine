package com.arzttermine.service.booking.web.dto.response;

import com.arzttermine.service.profile.web.dto.response.CustomerDto;

public class CustomerDetailsDto
{
    private boolean returning;
    private CustomerDto details;

    public CustomerDto getDetails() {
        return details;
    }

    public void setDetails(CustomerDto details) {
        this.details = details;
    }

    public boolean isReturning() {
        return returning;
    }

    public void setReturning(boolean returning) {
        this.returning = returning;
    }
}
