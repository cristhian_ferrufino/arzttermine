package com.arzttermine.service.booking.web.dto.response;

import com.arzttermine.library.web.dto.response.paging.PageResponseDto;

public class BookingsPageDto extends PageResponseDto<BookingDto>
{
}
