package com.arzttermine.service.booking.web.dto.request;

import org.hibernate.validator.constraints.Email;

public class NotificationSettingsDto
{
    @Email
    private String emailRecipient;

    private String smsRecipient;

    public String getEmailRecipient() {
        return emailRecipient;
    }

    public void setEmailRecipient(String emailRecipient) {
        this.emailRecipient = emailRecipient;
    }

    public String getSmsRecipient() {
        return smsRecipient;
    }

    public void setSmsRecipient(String smsRecipient) {
        this.smsRecipient = smsRecipient;
    }
}
