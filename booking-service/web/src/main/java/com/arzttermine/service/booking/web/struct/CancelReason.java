package com.arzttermine.service.booking.web.struct;

public enum CancelReason
{
    APPOINTMENT_RESCHEDULED,
    DOCTOR_REQUESTED_CANCELLATION,
    PATIENT_REQUESTED_CANCELLATION,
    BOOKING_ACCIDENT,
    BOOKING_DUPLICATE,
}
