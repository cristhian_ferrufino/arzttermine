package com.arzttermine.service.booking.web.dto.request;

import com.arzttermine.service.booking.web.struct.BookingPlatform;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.SafeHtml;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class CreateBookingDto
{
    @NotNull
    @ApiModelProperty("id of appointment to be booked")
    private Long appointmentId;

    @NotNull // for now
    @Valid
    @ApiModelProperty(
        "If supplied, a new customer will be created. " +
        "If there is already one without a password, the customer will be identified as recurring customer. " +
        "If the identified customer already has an password assigned, please continue with a login."
    )
    private CustomerRequestDto customer;

    @NotNull
    @ApiModelProperty("id of treatment type. This type may have a charge attached and requires a capture")
    private Long treatmentTypeId;

    @ApiModelProperty("additional field to track where the booking is coming from")
    private BookingPlatform platform;

    @ApiModelProperty("should be set for paid treatments, see payment-service for generating a capture")
    private String capture;

    @NotNull
    private Long practiceId;

    @Length(min = 3, max = 255)
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    private String title;

    @Valid
    @NotNull
    @ApiModelProperty("configures in which way the patient should be notified")
    private NotificationSettingsDto notificationSettings = new NotificationSettingsDto();

    public BookingPlatform getPlatform() {
        return platform;
    }

    public void setPlatform(BookingPlatform platform) {
        this.platform = platform;
    }

    public Long getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(Long appointmentId) {
        this.appointmentId = appointmentId;
    }

    public Long getTreatmentTypeId() {
        return treatmentTypeId;
    }

    public void setTreatmentTypeId(Long treatmentTypeId) {
        this.treatmentTypeId = treatmentTypeId;
    }

    public String getCapture() {
        return capture;
    }

    public void setCapture(String capture) {
        this.capture = capture;
    }

    public CustomerRequestDto getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerRequestDto customer) {
        this.customer = customer;
    }

    public NotificationSettingsDto getNotificationSettings() {
        return notificationSettings;
    }

    public void setNotificationSettings(NotificationSettingsDto notificationSettings) {
        this.notificationSettings = notificationSettings;
    }

    public boolean hasPaymentToken() {
        return null != capture;
    }

    public Long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(Long practiceId) {
        this.practiceId = practiceId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
