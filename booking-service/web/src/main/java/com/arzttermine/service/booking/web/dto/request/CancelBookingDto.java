package com.arzttermine.service.booking.web.dto.request;

import com.arzttermine.service.booking.web.struct.CancelReason;
import com.arzttermine.service.notifications.web.struct.NotificationType;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class CancelBookingDto
{
    @NotNull
    private List<NotificationType> notificationTypes = new ArrayList<>();

    @NotNull
    private Boolean appointmentAvailability = true;

    @NotNull
    private CancelReason reason;

    public List<NotificationType> getNotificationTypes() {
        return notificationTypes;
    }

    public void setNotificationTypes(List<NotificationType> notificationTypes) {
        this.notificationTypes = notificationTypes;
    }

    public Boolean getAppointmentAvailability() {
        return appointmentAvailability;
    }

    public void setAppointmentAvailability(Boolean appointmentAvailability) {
        this.appointmentAvailability = appointmentAvailability;
    }

    public CancelReason getReason() {
        return reason;
    }

    public void setReason(CancelReason reason) {
        this.reason = reason;
    }
}
