package com.arzttermine.service.booking.web.struct;

public enum PaymentState
{
    NONE,
    PENDING,
    PAID,
}
