package com.arzttermine.service.booking.web.dto.request;

import com.arzttermine.library.web.struct.Language;
import org.hibernate.validator.constraints.SafeHtml;

public class UpdateBookingDto
{
    private Language language;

    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    private String practiceComment;

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getPracticeComment() {
        return practiceComment;
    }

    public void setPracticeComment(String practiceComment) {
        this.practiceComment = practiceComment;
    }
}
