# booking

## Example request
*this example shows the request body for an paid treatment*

```json
{
	"appointmentId": 302,
	"customer": {
	    "returning": false,
		"email": "my@mail.de",
		"firstName": "someone",
		"lastName": "as example",
		"insuranceType": "PRIVATE",
		"phone": "optional phone number",
		"phoneMobile": "optional mobile number"
	},
	"notificationSettings": {
	  "smsRecipient": "if provided, sms notifications are send to this number",
	  "emailRecipient": "if provided, email notifications are send to this email instead of customer.email",
	},
	"treatmentTypeId": 1,
	"capture": "card_19dhWcJVoxDMjHTgAGOWdhcT"
}
```

`notificationsSettings` is not allowed to be null. 
But you can omit the value in the request. In that case we will send email notifications to the customers email address (which is required).
Why this way? Recurring customer may want to use different email/number for notifications.

## Example document

```json
{
  "id": 1,
  "customer": {
    "returning": false,
    "details": {
      "id": 2,
      "email": "example@test.de",
      "firstName": "first",
      "lastName": "last",
      "phone": "optional phone",
      "insurance": {
        "type": "COMPULSORY"
      },
      "language": "GERMAN",
      "enabled": true
    }
  },
  "paid": false,
  "appointment": {
    "id": 30,
    "start": "2017-02-11T14:30Z",
    "end": "2017-02-11T16:00Z",
    "duration": 90,
    "booked": true,
    "practice": {
      "id": 1,
      "name": "rgtfff",
      "location": {
        "id": 0,
        "country": "GERMANY",
        "longitude": 1000,
        "latitude": 0,
        "location": "somewhere 123",
        "zip": "12345",
        "primary": false
      },
      "medicalSpecialties": [
        "DENTIST"
      ]
    },
    "doctor": {
      "id": 1,
      "email": "my@mail.de",
      "firstName": "someone",
      "lastName": "as example",
      "language": "GERMAN",
      "enabled": true
    },
    "availableInsuranceTypes": [
      "COMPULSORY",
      "PRIVATE"
    ],
    "availableTreatmentTypes": [
      1,
      3
    ],
    "availableLanguages": [
      "GERMAN"
    ]
  },
  "status": "CREATED"
}
```
