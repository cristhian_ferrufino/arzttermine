package com.arzttermine.service.booking.service.externals.impl;

import com.arzttermine.library.client.model.RequestOptions;
import com.arzttermine.library.client.model.param.QueryParameter;
import com.arzttermine.service.booking.service.externals.AppointmentApi;
import com.arzttermine.service.calendar.client.AppointmentClient;
import com.arzttermine.service.calendar.web.dto.response.AppointmentDto;
import com.arzttermine.service.profile.security.service.TokenCache;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
@Profile("!testing")
public class DefaultAppointmentClient implements AppointmentApi
{
    @Autowired
    private AppointmentClient client;

    @Autowired
    private TokenCache tokenCache;

    @Override
    public AppointmentDto findById(UUID token, long id) {
        final RequestOptions options = new RequestOptions();
        options.setAuthorization(token);

        return client.getAppointment(id, options);
    }

    @Override
    public CompletableFuture<AppointmentDto> findByIdAsync(long id) {
        final UUID token = tokenCache.getToken();

        return CompletableFuture.supplyAsync(() -> findById(token, id));
    }

    @Override
    public Map<Long, AppointmentDto> findByIds(UUID token, Collection<Long> ids) {
        if (ids.isEmpty()) {
            return Maps.newHashMap();
        }

        final RequestOptions options = new RequestOptions();
        options.setAuthorization(token);
        options.setPaging(0, ids.size());
        ids.forEach(id -> options.addParameter(new QueryParameter("id", id)));

        return client.getAppointments(options).getElements()
            .stream().collect(Collectors.toMap(AppointmentDto::getId, Function.identity()));
    }

    @Override
    public CompletableFuture<Map<Long, AppointmentDto>> findByIdsAsync(Collection<Long> ids) {
        final UUID token = tokenCache.getToken();

        return CompletableFuture.supplyAsync(() -> findByIds(token, ids));
    }
}
