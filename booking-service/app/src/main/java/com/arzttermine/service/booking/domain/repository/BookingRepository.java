package com.arzttermine.service.booking.domain.repository;

import com.arzttermine.service.booking.domain.entity.Booking;
import com.arzttermine.service.booking.web.struct.BookingStatus;
import com.google.common.base.Strings;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Set;
import java.util.UUID;

@Repository
public interface BookingRepository extends PagingAndSortingRepository<Booking, Long>
{
    Booking findByUuid(UUID uuid);

    Set<Booking> findByAppointmentId(long appointmentId);

    Page<Booking> findByPracticeId(long practiceId, Pageable pageable);

    @Query("SELECT COUNT(b) > 0 FROM Booking b WHERE b.appointmentId = :appointment AND b.status NOT IN (:excluded)")
    boolean existBooking(@Param("appointment") long appointmentId, @Param("excluded") Collection<BookingStatus> excluded);
}
