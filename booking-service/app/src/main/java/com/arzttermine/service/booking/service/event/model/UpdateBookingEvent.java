package com.arzttermine.service.booking.service.event.model;

import com.arzttermine.service.booking.domain.entity.Booking;

public class UpdateBookingEvent
{
    private final Booking booking;

    private UpdateBookingEvent(Booking booking) {
        this.booking = booking;
    }

    public static UpdateBookingEvent of(Booking booking) {
        return new UpdateBookingEvent(booking);
    }

    public Booking getBooking() {
        return booking;
    }
}
