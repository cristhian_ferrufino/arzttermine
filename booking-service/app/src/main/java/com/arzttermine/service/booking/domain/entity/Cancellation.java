package com.arzttermine.service.booking.domain.entity;

import com.arzttermine.service.booking.support.InstantToTimestampConverter;
import com.arzttermine.service.booking.web.struct.CancelReason;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.Instant;

@Entity
@Table(name = "cancellations")
public class Cancellation
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false, insertable = false)
    private long id;

    @OneToOne
    @JoinColumn(name = "booking_id", unique = true)
    private Booking booking;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private CancelReason reason;

    @Column(name = "appointment_availability", nullable = false, updatable = false)
    private boolean appointmentAvailable = true;

    @Column(nullable = false, updatable = false)
    @Convert(converter = InstantToTimestampConverter.class)
    private Instant cancelledAt = Instant.now();

    protected Cancellation() {
    }

    public Cancellation(Booking booking, CancelReason reason, boolean appointmentAvailability) {
        this.booking = booking;
        this.reason = reason;
        this.appointmentAvailable = appointmentAvailability;
    }

    public long getId() {
        return id;
    }

    public Booking getBooking() {
        return booking;
    }

    public CancelReason getReason() {
        return reason;
    }

    public Instant getCancelledAt() {
        return cancelledAt;
    }

    public boolean isAppointmentAvailable() {
        return appointmentAvailable;
    }
}
