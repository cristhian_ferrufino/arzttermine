package com.arzttermine.service.booking.domain.entity;

import com.arzttermine.library.web.dto.PriceDto;
import com.arzttermine.library.web.struct.Currency;
import com.arzttermine.library.web.struct.InsuranceType;
import com.arzttermine.library.web.struct.Language;
import com.arzttermine.service.booking.support.InstantToTimestampConverter;
import com.arzttermine.service.booking.web.dto.request.NotificationSettingsDto;
import com.arzttermine.service.booking.web.struct.BookingPlatform;
import com.arzttermine.service.booking.web.struct.BookingStatus;
import com.arzttermine.service.booking.web.struct.PaymentState;
import com.arzttermine.service.calendar.web.dto.response.AppointmentDto;
import com.arzttermine.service.profile.web.dto.response.CustomerDto;
import com.arzttermine.service.profile.web.dto.response.TreatmentTypeDto;
import org.hibernate.validator.constraints.Length;
import org.springframework.util.Assert;

import javax.persistence.*;
import java.time.Instant;
import java.util.UUID;
import java.util.EnumSet;

@Entity
@Table(
    name = "bookings",
    indexes = {
        @Index(name = "id_booking_appointment_id", columnList = "appointment_id"),
        @Index(name = "id_booking_practice_id", columnList = "practice_id"),
        @Index(name = "id_booking_uuid_hash", columnList = "uuid")
    },
    uniqueConstraints = {
        @UniqueConstraint(name = "un_active_booking_per_appointment", columnNames = {"active", "appointment_id"})
    }
)
public class Booking
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false, insertable = false)
    private long id;

    @Column(nullable = false, updatable = false, unique = true, columnDefinition = "BINARY(16)")
    private UUID uuid;

    @Column(name = "appointment_id", nullable = false, unique = true)
    private long appointmentId;

    @Column(name = "customer_id", nullable = false, updatable = false)
    private long customerId;

    @Column(name = "practice_id", nullable = false, updatable = false)
    private long practiceId;

    @Column(name = "treatment_type_id", nullable = false, updatable = false)
    private long treatmentTypeId;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Language language;

    @Enumerated(EnumType.STRING)
    @Column(name = "insurance_type", nullable = false)
    private InsuranceType insuranceType;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private BookingStatus status;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private PaymentState payment = PaymentState.NONE;

    @Enumerated(EnumType.STRING)
    @Column(name = "source_platform", updatable = false)
    private BookingPlatform sourcePlatform;

    @Convert(converter = InstantToTimestampConverter.class)
    @Column(name = "confirmed_at")
    private Instant confirmedAt;

    @Convert(converter = InstantToTimestampConverter.class)
    @Column(name = "created_at", nullable = false, updatable = false)
    private Instant createdAt = Instant.now();

    @Convert(converter = InstantToTimestampConverter.class)
    @Column(name = "updated_at", nullable = false)
    private Instant updatedAt = Instant.now();

    @Column
    @Length(min = 3, max = 255)
    private String title;

    @Length(max = 1024)
    @Column(name = "practice_comment")
    private String practiceComment;

    @Column(name = "notification_email")
    private String notificationEmail;

    @Column(name = "notification_sms")
    private String notificationSms;

    @Column(nullable = true)
    private Boolean active = true;

    @Column(name = "returning_customer", nullable = false, updatable = false)
    private boolean returningCustomer;

    @Column(name = "total_price", updatable = false)
    private Integer totalPrice;

    @Enumerated(EnumType.STRING)
    @Column(name = "used_currency", updatable = false)
    private Currency usedCurrency;

    @Version
    private int version;

    @Transient
    private transient TreatmentTypeDto treatmentType;

    @Transient
    private transient AppointmentDto appointment;

    @Transient
    private transient CustomerDto customer;

    protected Booking() {
    }

    public Booking(Builder builder) {
        practiceId = builder.practiceId;
        appointmentId = builder.appointmentId;
        treatmentTypeId = builder.treatmentTypeId;
        returningCustomer = builder.returningCustomer;
        customerId = builder.customerId;
        language = builder.language;
        confirmedAt = builder.confirmedAt;
        status = builder.status;
        title = builder.title;
        sourcePlatform = builder.platform;
        notificationEmail = builder.emailRecipient;
        notificationSms = builder.smsRecipient;
        insuranceType = builder.insuranceType;

        if (null != builder.totalPrice) {
            totalPrice = builder.totalPrice.getAmount();
            usedCurrency = builder.totalPrice.getCurrency();
            if (builder.paid) {
                paid();
            }
        }
    }

    @PreUpdate
    public void beforeUpdate() {
        updatedAt = Instant.now();
    }

    @PrePersist
    public void beforePersist() {
        if (null == uuid) {
            uuid = UUID.randomUUID();
        }
    }

    public long getId() {
        return id;
    }

    public long getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(long appointmentId) {
        if (this.appointmentId != appointmentId) {
            this.appointment = null;
        }

        this.appointmentId = appointmentId;
    }

    public long getCustomerId() {
        return customerId;
    }

    public long getPracticeId() {
        return practiceId;
    }

    public String getTitle() {
        return title;
    }

    public Language getLanguage() {
        return language;
    }

    public BookingStatus getStatus() {
        return status;
    }

    public Instant getConfirmedAt() {
        return confirmedAt;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public Instant getUpdatedAt() {
        return updatedAt;
    }

    public int getVersion() {
        return version;
    }

    public boolean isActive() {
        return null != active && active;
    }

    public boolean isReturningCustomer() {
        return returningCustomer;
    }

    public String getNotificationEmail() {
        return notificationEmail;
    }

    public String getNotificationSms() {
        return notificationSms;
    }

    public void confirm(Instant at) {
        confirmedAt = at;
        status = BookingStatus.CONFIRMED;
    }

    public BookingPlatform getSourcePlatform() {
        return sourcePlatform;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public void setStatus(BookingStatus status) {
        this.status = status;

        if (isReleased()) {
            active = null;
        } else {
            active = true;
        }
    }

    public long getTreatmentTypeId() {
        return treatmentTypeId;
    }

    public boolean isPaid() {
        return payment.equals(PaymentState.PAID);
    }

    public boolean isPaymentInitialised() {
        return payment.equals(PaymentState.PENDING) || isPaid();
    }

    public void paid() {
        payment = PaymentState.PAID;
    }

    public void paymentInitialised() {
        Assert.state(!isPaid(), "payment cannot initialised if already paid");
        payment = PaymentState.PENDING;
    }

    public String getPracticeComment() {
        return practiceComment;
    }

    public void setPracticeComment(String practiceComment) {
        this.practiceComment = practiceComment;
    }

    /**
     * @return boolean indicating whether the booking is blocking
     *                 or already released for further usages
     */
    public boolean isReleased() {
        return null == active || BookingStatus.released.contains(status);
    }

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public Currency getUsedCurrency() {
        return usedCurrency;
    }

    public InsuranceType getInsuranceType() {
        return insuranceType;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void attachBookingData(CustomerDto customer, AppointmentDto appointment, TreatmentTypeDto treatmentType) {
        this.customer = customer;
        this.appointment = appointment;
        this.treatmentType = treatmentType;
    }

    public TreatmentTypeDto getTreatmentType() {
        return treatmentType;
    }

    public AppointmentDto getAppointment() {
        return appointment;
    }

    public CustomerDto getCustomer() {
        return customer;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder
    {
        private Long appointmentId;
        private Long practiceId;
        private Long customerId;
        private Language language;
        private Instant confirmedAt;
        private Long treatmentTypeId;
        private BookingPlatform platform;
        private boolean paid = false;
        private BookingStatus status = BookingStatus.CREATED;
        private boolean returningCustomer = false;
        private String smsRecipient;
        private String emailRecipient;
        private PriceDto totalPrice;
        private String title;
        private InsuranceType insuranceType;

        public Builder appointmentId(long appointmentId) {
            this.appointmentId = appointmentId;
            return this;
        }

        public Builder practiceId(long practiceId) {
            this.practiceId = practiceId;
            return this;
        }

        public Builder treatmentTypeId(long treatmentTypeId) {
            this.treatmentTypeId = treatmentTypeId;
            return this;
        }

        public Builder platform(BookingPlatform platform) {
            this.platform = platform;
            return this;
        }

        public Builder customerId(long customerId) {
            this.customerId = customerId;
            return this;
        }

        public Builder returningCustomer(boolean returningCustomer) {
            this.returningCustomer = returningCustomer;
            return this;
        }

        public Builder language(Language language) {
            this.language = language;
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder status(BookingStatus status) {
            this.status = status;
            return this;
        }

        public Builder paid(boolean paid) {
            this.paid = paid;
            return this;
        }

        public Builder totalPrice(PriceDto priceDto) {
            this.totalPrice = priceDto;
            return this;
        }

        public Builder confirmed(Instant confirmed) {
            this.confirmedAt = confirmed;
            return this;
        }

        public Builder insuranceType(InsuranceType type) {
            this.insuranceType = type;
            return this;
        }

        public Builder notifications(NotificationSettingsDto settings) {
            this.smsRecipient = settings.getSmsRecipient();
            this.emailRecipient = settings.getEmailRecipient();
            return this;
        }

        public Booking build() {
            Assert.notNull(treatmentTypeId, "id of treatment type must be set");
            Assert.notNull(appointmentId, "appointment id must be set");
            Assert.notNull(practiceId, "practice id must be set");
            Assert.notNull(customerId, "customer id must be set");
            Assert.notNull(status, "initial state must be set");

            if (null != confirmedAt && status != BookingStatus.CONFIRMED) {
                status = BookingStatus.CONFIRMED;
            }

            return new Booking(this);
        }
    }
}
