package com.arzttermine.service.booking.config.settings;

import com.arzttermine.service.profile.security.config.settings.RemoteTokenServicesSettings;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(
    locations = "classpath:${spring.profiles.active:default}/clients.yml",
    prefix = "remoteTokenService"
)
public class RemoteTokenClientSettings extends RemoteTokenServicesSettings
{
}
