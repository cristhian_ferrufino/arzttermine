
package com.arzttermine.service.booking.service.event;

import com.arzttermine.library.elasticsearch.model.BulkAction;
import com.arzttermine.library.elasticsearch.service.DocumentProcessor;
import com.arzttermine.library.messaging.model.TopicMessage;
import com.arzttermine.library.messaging.processor.MessageBus;
import com.arzttermine.service.booking.domain.entity.Booking;
import com.arzttermine.service.booking.messages.BookingCancelledMessage;
import com.arzttermine.service.booking.service.event.model.CancelBookingEvent;
import com.arzttermine.service.booking.web.dto.response.BookingDto;
import com.arzttermine.service.booking.web.struct.BookingStatus;
import com.arzttermine.service.notification.messages.notification.CancelNotificationsMessage;
import com.arzttermine.service.notification.messages.notification.NotificationCancellationRequestDto;
import com.arzttermine.service.notifications.web.struct.Notification;
import com.arzttermine.service.notifications.web.struct.NotificationType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionalEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class CancelBookingListener
{
    private static final Logger log = LoggerFactory.getLogger(CancelBookingListener.class);

    @Autowired
    private MessageBus messageBus;

    @Autowired
    private DocumentProcessor documentProcessor;

    @Async
    @TransactionalEventListener
    @Transactional(readOnly = true)
    public void cancelBooking(final CancelBookingEvent event)
    {
        if (event.getUpdateType() == BulkAction.ActionType.DELETE) {
            // Assumption: the result will not be null.
            final Booking booking = event.getCancellation().getBooking();

            // Send notification
            final BookingCancelledMessage message = new BookingCancelledMessage();
            message.setAppointmentId(booking.getAppointmentId());
            message.setCustomerId(booking.getCustomerId());
            message.setBookingId(booking.getId());
            message.setStatus(booking.getStatus());
            message.setAppointmentAvailable(event.getCancellation().isAppointmentAvailable());

            final CancelNotificationsMessage cancelMessages = new CancelNotificationsMessage();

            final NotificationCancellationRequestDto cancelBookingReminderRequest = new NotificationCancellationRequestDto();
            cancelBookingReminderRequest.setTemplate(Notification.BOOKING_REMINDER);
            cancelBookingReminderRequest.getParams().put("bookingId", booking.getId());
            cancelMessages.addCancellationRequest(cancelBookingReminderRequest);

            final NotificationCancellationRequestDto cancelReviewReminderRequest = new NotificationCancellationRequestDto();
            cancelReviewReminderRequest.setTemplate(Notification.REVIEW_REMINDER);
            cancelReviewReminderRequest.getParams().put("bookingId", booking.getId());
            cancelMessages.addCancellationRequest(cancelReviewReminderRequest);

            List<TopicMessage> messages = new ArrayList<>();
            messages.add(message);
            messages.add(cancelMessages);
            messageBus.sendAsync(messages);

            if (booking.getStatus() == BookingStatus.CANCELLED_BY_EMPLOYEE)
                log.info(String.format("Booking (id: %d) cancelled by Employee (id: %d)", booking.getId(), event.getUserPrincipal()));
            else if (booking.getStatus() == BookingStatus.CANCELLED_BY_PATIENT)
                log.info(String.format("Booking (id: %d) cancelled by Patient (id: %d)", booking.getAppointmentId(), event.getUserPrincipal()));
            else if (booking.getStatus() == BookingStatus.CANCELLED_BY_PRACTICE)
                log.info(String.format("Booking (id: %d) cancelled by Practice (id: %d)", booking.getAppointmentId(), booking.getPracticeId()));

            // Remove document....
            final String type = "practice-" + booking.getPracticeId();

            // Assumption: We do not have practice authority for comments as we do not need to see them in a search.
            final BookingDto bookingDto = dtoFromBooking(booking);
            final BulkAction action;
            action = BulkAction.delete(bookingDto);
            documentProcessor.execute("bookings", type, Collections.singletonList(action));
        }
    }

    private BookingDto dtoFromBooking(final Booking booking) {

        final BookingDto dto = new BookingDto();
        dto.setId(booking.getId());

        return dto;
    }
}
