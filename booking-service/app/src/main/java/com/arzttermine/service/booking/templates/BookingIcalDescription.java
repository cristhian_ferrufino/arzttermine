package com.arzttermine.service.booking.templates;


import com.arzttermine.library.suport.templating.AbstractTemplateTask;
import com.arzttermine.library.suport.templating.TemplateTask;

public class BookingIcalDescription extends AbstractTemplateTask implements TemplateTask {

    public String getPath() {
        return "ical/booking-event-description";
    }

}
