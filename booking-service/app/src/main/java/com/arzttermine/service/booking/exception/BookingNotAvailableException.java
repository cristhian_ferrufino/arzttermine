package com.arzttermine.service.booking.exception;

import com.arzttermine.library.suport.exception.BusinessException;

import java.time.Instant;
import java.util.Date;

public class BookingNotAvailableException extends BusinessException
{
    public BookingNotAvailableException(Instant start, int requiredSecondsBefore) {
        super("BOOKING_NOT_AVAILABLE", Date.from(start), "The booking must be booked %d before start of appointment", requiredSecondsBefore);
    }
}
