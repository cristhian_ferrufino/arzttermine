package com.arzttermine.service.booking.service;

import com.arzttermine.library.elasticsearch.model.BulkAction;
import com.arzttermine.library.elasticsearch.service.DocumentProcessor;
import com.arzttermine.library.messaging.processor.MessageBus;
import com.arzttermine.library.spring.data.BatchProcessingUtil;
import com.arzttermine.library.suport.exception.AccessDeniedException;
import com.arzttermine.library.suport.exception.EntityNotFoundException;
import com.arzttermine.library.suport.exception.ServiceUnavailableException;
import com.arzttermine.library.web.dto.response.paging.PageDto;
import com.arzttermine.service.booking.domain.entity.Booking;
import com.arzttermine.service.booking.domain.entity.Cancellation;
import com.arzttermine.service.booking.domain.repository.BookingRepository;
import com.arzttermine.service.booking.domain.repository.CancellationRepository;
import com.arzttermine.service.booking.exception.BookingNotActivatedException;
import com.arzttermine.service.booking.service.event.model.AppointmentBookedEvent;
import com.arzttermine.service.booking.service.event.model.CancelBookingEvent;
import com.arzttermine.service.booking.service.event.model.UpdateBookingEvent;
import com.arzttermine.service.booking.service.externals.AppointmentApi;
import com.arzttermine.service.booking.service.externals.CustomerApi;
import com.arzttermine.service.booking.service.externals.ProfileApi;
import com.arzttermine.service.booking.service.mapper.BookingMapper;
import com.arzttermine.service.booking.service.processor.AppointmentTransformer;
import com.arzttermine.service.booking.service.processor.BookingDataProcessor;
import com.arzttermine.service.booking.web.dto.request.CancelBookingDto;
import com.arzttermine.service.booking.web.dto.request.CreateBookingDto;
import com.arzttermine.service.booking.web.dto.request.UpdateBookingDto;
import com.arzttermine.service.booking.web.dto.response.BookingDto;
import com.arzttermine.service.booking.web.dto.response.BookingsPageDto;
import com.arzttermine.service.booking.web.struct.BookingStatus;
import com.arzttermine.service.calendar.web.dto.response.AppointmentDto;
import com.arzttermine.service.notification.messages.booking.BookingCancelledMessage;
import com.arzttermine.service.notifications.web.struct.NotificationType;
import com.arzttermine.service.player.client.PracticeClient;
import com.arzttermine.service.profile.security.support.SecurityContextUtil;
import com.arzttermine.service.profile.web.dto.response.CustomerDto;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import com.arzttermine.service.profile.web.dto.response.TreatmentTypeDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Service
public class BookingService
{
    private static final Logger log = LoggerFactory.getLogger(BookingService.class);

    @Autowired
    private AppointmentApi appointmentClient;

    @Autowired
    private CustomerApi customerClient;

    @Autowired
    private ProfileApi profileClient;

    @Autowired
    private AppointmentTransformer transformer;

    @Autowired
    private BookingRepository repository;

    @Autowired
    private BookingMapper bookingMapper;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private CancellationRepository cancellationRepository;

    @Autowired
    private MessageBus messageBus;

    @Autowired
    private BookingDataProcessor dataProcessor;

    @Autowired
    private DocumentProcessor documentProcessor;

    @Value("${processing.config.reindex.batchSize}")
    private int REINDEX_BATCH_SIZE = 100;

    @Value("${processing.config.reindex.delay}")
    private int REINDEX_DELAY = 30000;

    @Value("${processing.config.reindex.processDelay}")
    private int REINDEX_PROCESS_DELAY = 3000;

    @Transactional(readOnly = false)
    public BookingDto bookAppointment(final CreateBookingDto request)
    {
        final long appointmentId = request.getAppointmentId();

        final CompletableFuture<AppointmentDto> appointmentFuture = appointmentClient.findByIdAsync(appointmentId);
        final CompletableFuture<CustomerDto> customerFuture = customerClient.createCustomerAsync(request.getCustomer());
        final CompletableFuture<TreatmentTypeDto> ttFuture = profileClient.findTreatmentTypeByIdAsync(request.getTreatmentTypeId(), request.getPracticeId());
        CompletableFuture.allOf(appointmentFuture, customerFuture, ttFuture).join();

        final AppointmentDto appointment;
        final TreatmentTypeDto treatment;
        final CustomerDto customer;

        try {
            appointment = appointmentFuture.get();
            customer = customerFuture.get();
            treatment = ttFuture.get();
        } catch (InterruptedException e) {
            log.error("could not await customer, appointment or treatment future", e);
            throw new ServiceUnavailableException("booking");
        } catch (ExecutionException e) {
            log.warn("error while trying to book appointment", e.getCause());
            throw (RuntimeException) e.getCause();
        }

        // this should be done by the frontend! but nvm for now
        if (null != request.getCustomer().getPhoneMobile() && null == request.getNotificationSettings().getSmsRecipient()) {
            request.getNotificationSettings().setSmsRecipient(request.getCustomer().getPhoneMobile());
        }
        if (null != request.getCustomer().getEmail() && null == request.getNotificationSettings().getEmailRecipient()) {
            request.getNotificationSettings().setEmailRecipient(request.getCustomer().getEmail());
        }

        final Booking booking = transformer.transformAppointment(request, customer, appointment, treatment);
        eventPublisher.publishEvent(AppointmentBookedEvent.of(booking, appointment, customer, treatment));

        appointment.setBooked(true);

        return bookingMapper.toDto(booking, appointment, customer, false);
    }

    @Transactional(readOnly = true)
    public BookingDto getBooking(final long id)
    {
        final Booking booking = repository.findOne(id);
        if (null == booking) {
            throw new EntityNotFoundException("Booking", id);
        }

        assertPermissions(booking);
        dataProcessor.process(booking);

        return bookingMapper.toDto(booking, booking.getAppointment(), booking.getCustomer(), SecurityContextUtil.hasPracticeAuthority(booking.getPracticeId()));
    }

    @Transactional(readOnly = true)
    public BookingDto getBooking(final UUID id)
    {
        final Booking booking = repository.findByUuid(id);
        if (null == booking) {
            throw new EntityNotFoundException("Booking", id);
        }

        assertPermissions(booking);
        dataProcessor.process(booking);

        return bookingMapper.toDto(booking, booking.getAppointment(), booking.getCustomer(), SecurityContextUtil.hasPracticeAuthority(booking.getPracticeId()));
    }

    @Transactional(readOnly = false)
    public void cancelBooking(final long bookingId, final CancelBookingDto request)
    {
        final Booking booking = findOneNullsafe(bookingId);
        final int permission = assertPermissions(booking);

        if (booking.isReleased()) {
            throw new BookingNotActivatedException(bookingId);
        }

        final long userPrincipal = (permission < 3) ? SecurityContextUtil.userPrincipal() : 0;

        if (0 == permission) {
            booking.setStatus(BookingStatus.CANCELLED_BY_PATIENT);
        } else if (1 == permission) {
            booking.setStatus(BookingStatus.CANCELLED_BY_PRACTICE);
        } else {
            booking.setStatus(BookingStatus.CANCELLED_BY_EMPLOYEE);
        }

        final Cancellation cancellation = new Cancellation(booking, request.getReason(), request.getAppointmentAvailability());
        if (permission > 0 && !request.getNotificationTypes().isEmpty()) {
            dataProcessor.process(booking);

            // make sure notifications can be sent before saving cancellation
            request.getNotificationTypes().stream()
                .map(type -> cancellationNotification(type, booking))
                .forEach(messageBus::send);
        }

        cancellationRepository.save(cancellation);

        // Fire cancel event (delete document from elasticsearch index)
        eventPublisher.publishEvent(CancelBookingEvent.of(cancellation, userPrincipal));
    }

    @Transactional(readOnly = false)
    public void updateBooking(final long bookingId, final UpdateBookingDto update)
    {
        final Booking booking = findUpdateableNullsafe(bookingId);
        final int permission = assertPermissions(booking);

        Optional.ofNullable(update.getLanguage()).ifPresent(booking::setLanguage);

        if (permission >= 1) { // system/employee/practice
            Optional.ofNullable(update.getPracticeComment()).ifPresent(booking::setPracticeComment);
        }

        eventPublisher.publishEvent(UpdateBookingEvent.of(booking));
    }

    @Transactional(readOnly = true)
    public Set<BookingDto> getBookings(final long appointmentId)
    {
        final Set<Booking> bookings = repository.findByAppointmentId(appointmentId);

        final UUID currentToken = SecurityContextUtil.currentToken();
        final AppointmentDto appointment = appointmentClient.findById(currentToken, appointmentId);

        return bookings.stream()
            .map(b -> bookingMapper.toDto(b, appointment, true))
            .collect(Collectors.toSet());
    }

    @Transactional(readOnly = true)
    public BookingsPageDto getPracticeBookings(final long practiceId, final Pageable pageable)
    {
        final Page<Booking> bookings = repository.findByPracticeId(practiceId, pageable);

        final PageDto page = new PageDto();
        page.setCurrentPage(pageable.getPageNumber());
        page.setPageSize(pageable.getPageSize());
        page.setTotalItems(bookings.getTotalElements());
        page.setTotalPages(bookings.getTotalPages());

        final UUID currentToken = SecurityContextUtil.currentToken();
        final Map<Long, AppointmentDto> appointments = appointmentClient.findByIds(currentToken, bookings.getContent()
            .stream()
            .map(Booking::getAppointmentId)
            .distinct()
            .collect(Collectors.toSet()));

        final boolean hasPracticeAuth = SecurityContextUtil.hasPracticeAuthority(practiceId) || SecurityContextUtil.hasSystemGrants();
        final BookingsPageDto result = new BookingsPageDto();
        result.setPage(page);

        result.setElements(bookings.getContent().stream()
            .map(b -> bookingMapper.toDto(b, appointments.get(b.getAppointmentId()), hasPracticeAuth))
            .collect(Collectors.toList()));

        return result;
    }

    private Booking findUpdateableNullsafe(final long id)
    {
        final Booking booking = findOneNullsafe(id);

        if (booking.isReleased()) {
            throw new BookingNotActivatedException(id);
        }

        return booking;
    }

    private Booking findOneNullsafe(final long id)
    {
        final Booking booking = repository.findOne(id);

        if (null == booking) {
            throw new EntityNotFoundException("Booking", id);
        }

        return booking;
    }

    private int assertPermissions(final Booking booking)
    {
        if (SecurityContextUtil.hasAuthorities("ROLE_SYSTEM")) {
            return 3;
        } else if (SecurityContextUtil.hasAuthorities("ROLE_EMPLOYEE")) {
            return 2;
        } else if (SecurityContextUtil.hasPracticeAuthority(booking.getPracticeId())) {
            return 1;
        } else if (Objects.equals(booking.getCustomerId(), SecurityContextUtil.userPrincipal())) {
            return 0;
        } else {
            throw new AccessDeniedException();
        }
    }

    private BookingCancelledMessage cancellationNotification(final NotificationType type, final Booking booking)
    {
        final BookingCancelledMessage.CancellationBag bag = new BookingCancelledMessage.CancellationBag();
        bag.bookingId = booking.getId();
        bag.customerId = booking.getCustomer().getId();
        bag.customerName = booking.getCustomer().getFullName();
        bag.doctorName = booking.getAppointment().getDoctor().getFullName();
        bag.practiceLocation = booking.getAppointment().getPractice().getLocation();
        bag.practiceName = booking.getAppointment().getPractice().getName();
        bag.start = booking.getAppointment().getStart().toInstant();
        bag.treatmentType = booking.getTreatmentType().getType();

        final String target = (type.equals(NotificationType.EMAIL))
            ? booking.getNotificationEmail()
            : booking.getNotificationSms();

        return new BookingCancelledMessage(bag, type, target, booking.getLanguage());
    }

    @Transactional(readOnly = true)
    public void reindexBookings(final long practiceId) {

        log.info("Starting reindex of bookings for practice {}", practiceId);
        final Collection<BulkAction> actions = new ArrayList<>();

        BatchProcessingUtil.process(
            request -> repository.findByPracticeId(practiceId, request),
            bookings -> {

                Set<Booking> activeBookings = new HashSet<>();

                //Delete cancelled bookings, collect remaining bookings for processing
                bookings.forEach( b -> {
                    if (b.isReleased()) {
                        BookingDto bookingDto = new BookingDto();
                        bookingDto.setId(b.getId());
                        actions.add(BulkAction.delete(bookingDto));
                    } else {
                        activeBookings.add(b);
                    }
                });

                //Process and index
                dataProcessor.process(activeBookings,practiceId);
                activeBookings.stream()
                    .map(activeBooking -> bookingMapper.toDto(activeBooking,activeBooking.getAppointment(), activeBooking.getCustomer(), true))
                    .map(BulkAction::index)
                    .forEach(actions::add);

            },
            success -> {
                documentProcessor.executeAsync("bookings", "practice-" + practiceId, actions);
                log.info("Finished reindexing bookings for practice {}", practiceId);
            },
            new Sort("id"),
            REINDEX_BATCH_SIZE,
            REINDEX_DELAY
        );

    }
}
