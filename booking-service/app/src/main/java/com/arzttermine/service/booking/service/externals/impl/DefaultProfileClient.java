package com.arzttermine.service.booking.service.externals.impl;

import com.arzttermine.library.client.model.RequestOptions;
import com.arzttermine.library.client.model.param.QueryParameter;
import com.arzttermine.service.booking.service.externals.ProfileApi;
import com.arzttermine.service.calendar.web.dto.response.AppointmentDto;
import com.arzttermine.service.player.client.TreatmentClient;
import com.arzttermine.service.player.client.PracticeClient;
import com.arzttermine.service.profile.security.service.TokenCache;
import com.arzttermine.service.profile.web.dto.response.PaidFeatureAssignmentDto;
import com.arzttermine.service.profile.web.dto.response.ReviewSiteDto;
import com.arzttermine.service.profile.web.dto.response.TreatmentTypeDto;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
@Profile("!testing")
public class DefaultProfileClient implements ProfileApi
{
    @Autowired
    private TreatmentClient treatmentClient;

    @Autowired
    private PracticeClient practiceClient;

    @Autowired
    private TokenCache tokenCache;

    @Override
    public TreatmentTypeDto findTreatmentTypeById(long id, long practiceId) {
        final RequestOptions options = new RequestOptions();
        options.setAuthorization(tokenCache.getToken());
        return treatmentClient.getById(id, practiceId, options);
    }

    @Override
    public CompletableFuture<TreatmentTypeDto> findTreatmentTypeByIdAsync(long id, long practiceId) {
        return CompletableFuture.supplyAsync(() -> findTreatmentTypeById(id, practiceId));
    }

    @Override
    public Map<Long, TreatmentTypeDto> findTreatmentTypesByIds(Collection<Long> ids, long practiceId) {
        if (ids.isEmpty()) {
            return Maps.newHashMap();
        }

        final RequestOptions options = new RequestOptions();
        options.setPaging(0, ids.size());
        ids.forEach(id -> options.addParameter(new QueryParameter("id", id)));

        return treatmentClient.getTreatmentTypes(practiceId,options).getElements()
            .stream().collect(Collectors.toMap(TreatmentTypeDto::getId, Function.identity()));
    }

    @Override
    public CompletableFuture<Map<Long, TreatmentTypeDto>> findTreatmentTypesByIdsAsync(Collection<Long> ids, long practiceId) {
        return CompletableFuture.supplyAsync(() -> findTreatmentTypesByIds(ids, practiceId));
    }

    @Override
    public Set<PaidFeatureAssignmentDto> findPaidFeaturesByPracticeId(long practiceId) {
        final RequestOptions options = new RequestOptions();
        options.setAuthorization(tokenCache.getToken());
        return new HashSet<>(Arrays.asList(practiceClient.getPaidFeaturesByPracticeId(practiceId, options)));
    }

    @Override
    public ReviewSiteDto findReviewSiteByPracticeId(long practiceId) {
        final RequestOptions options = new RequestOptions();
        options.setAuthorization(tokenCache.getToken());
        return practiceClient.getReviewSiteByPracticeId(practiceId, options);
    }
}
