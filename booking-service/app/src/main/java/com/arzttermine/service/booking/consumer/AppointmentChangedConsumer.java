package com.arzttermine.service.booking.consumer;

import com.arzttermine.library.messaging.consumer.Consumer;
import com.arzttermine.library.messaging.processor.MessageBus;
import com.arzttermine.library.suport.exception.AccessDeniedException;
import com.arzttermine.library.suport.exception.ServiceUnavailableException;
import com.arzttermine.service.booking.domain.entity.Booking;
import com.arzttermine.service.booking.domain.repository.BookingRepository;
import com.arzttermine.service.booking.messages.validation.BookingNotificationProxy;
import com.arzttermine.service.booking.service.event.model.UpdateBookingEvent;
import com.arzttermine.service.booking.service.externals.ProfileApi;
import com.arzttermine.service.booking.service.processor.BookingDataProcessor;
import com.arzttermine.service.calendar.messages.AppointmentChangedMessage;
import com.arzttermine.service.notification.messages.booking.BookingMovedMessage;
import com.arzttermine.service.notifications.web.struct.NotificationType;
import com.arzttermine.service.profile.web.dto.response.PaidFeatureAssignmentDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class AppointmentChangedConsumer
{
    private static final Logger log = LoggerFactory.getLogger(AppointmentChangedConsumer.class);

    @Autowired
    private BookingRepository bookingRepository;

    @Autowired
    private MessageBus messageBus;

    @Autowired
    private BookingDataProcessor dataProcessor;

    @Autowired
    private ProfileApi profileClient;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    /**
     * Important note:
     * Currently changing an appointment means the start or end date changed.
     * As this does not include any changes related to treatment charges etc,
     * you cannot expect any payment related updates after this consumer accepted the message.
     *
     * @param message
     */
    @Consumer
    @Retryable(
        maxAttempts = 2,
        backoff = @Backoff(value = 2500, multiplier = 2),
        include = {ServiceUnavailableException.class, AccessDeniedException.class, IOException.class}
    )
    public void appointmentChanged(AppointmentChangedMessage message)
    {
        final List<Booking> bookings = bookingRepository.findByAppointmentId(message.getPrevious()).stream()
            .filter(b -> !b.isReleased())
            .collect(Collectors.toList());

        if (bookings.isEmpty()) {
            return;
        }

        final Optional<Booking> declined;

        if (null != message.getBookedByCustomerId()) {
            declined = bookings.stream().filter(b -> b.getCustomerId() == message.getBookedByCustomerId()).findFirst();
        } else if (bookings.size() == 1 && null == message.getBookedByCustomerId()) {
            // kind of failure tolerance (eg: appointment booked, but not updated in calendar-service yet)
            declined = Optional.of(bookings.get(0));
        } else {
            declined = Optional.empty();
        }

        if (!declined.isPresent()) {
            log.warn(
                "PLEASE CHECK: there are some open bookings ({}) for appointment {}, but changed appointment ({} -> {}) did not match any of these.",
                bookings.stream().map(Booking::getId).map(String::valueOf).collect(Collectors.joining(",")),
                message.getPrevious(),
                message.getPrevious(),
                message.getNewId()
            );
            return;
        }

        final Booking booking = declined.get();
        booking.setAppointmentId(message.getNewId());

        sendNotifications(booking, message.getNotificationTypes(), message.getPreviousAppointmentStart());
        eventPublisher.publishEvent(UpdateBookingEvent.of(booking));
    }

    private void sendNotifications(final Booking booking, final Collection<NotificationType> notificationTypes, final Date previousAppointmentStart) {

        if (notificationTypes.isEmpty()) {
            return;
        }

        try {
            dataProcessor.process(booking);

            final BookingMovedMessage.MovementBag bag = new BookingMovedMessage.MovementBag();
            bag.bookingId = booking.getId();
            bag.customerId = booking.getCustomerId();
            bag.customerName = booking.getCustomer().getFullName();
            bag.treatmentType = booking.getTreatmentType().getType();
            bag.practiceLocation = booking.getAppointment().getPractice().getLocation();
            bag.practiceName = booking.getAppointment().getPractice().getName();
            bag.doctorName = booking.getAppointment().getDoctor().getFullName();
            bag.start = booking.getAppointment().getStart().toInstant();
            bag.previousStart = Optional.ofNullable(previousAppointmentStart)
                .orElse(booking.getAppointment().getStart())
                .toInstant();


            final Set<PaidFeatureAssignmentDto> paidFeatures = profileClient.findPaidFeaturesByPracticeId(booking.getPracticeId());
            final BookingNotificationProxy notification = new BookingNotificationProxy(paidFeatures, messageBus);

            notification.sendAsync(notificationTypes.stream()
                .map(type -> new BookingMovedMessage(bag, type, (type.equals(NotificationType.EMAIL) ? booking.getNotificationEmail() : booking.getNotificationSms()), booking.getLanguage()))
                .collect(Collectors.toList()));

        } catch (Exception e) {
            log.error("error occurred while trying to create or submit notifications", e);
        }
    }
}
