package com.arzttermine.service.booking.service.externals;

import com.arzttermine.service.profile.web.dto.response.PaidFeatureAssignmentDto;
import com.arzttermine.service.profile.web.dto.response.ReviewSiteDto;
import com.arzttermine.service.profile.web.dto.response.TreatmentTypeDto;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

public interface ProfileApi
{
    TreatmentTypeDto findTreatmentTypeById(long id, long practiceId);

    CompletableFuture<TreatmentTypeDto> findTreatmentTypeByIdAsync(long id, long practiceId);

    Map<Long,TreatmentTypeDto> findTreatmentTypesByIds(Collection<Long> ids, long practiceId);

    CompletableFuture<Map<Long,TreatmentTypeDto>> findTreatmentTypesByIdsAsync(Collection<Long> ids, long practiceId);

    Set<PaidFeatureAssignmentDto> findPaidFeaturesByPracticeId(long practiceId);

    ReviewSiteDto findReviewSiteByPracticeId(long practiceId);
}
