package com.arzttermine.service.booking.domain.repository;

import com.arzttermine.service.booking.domain.entity.Cancellation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CancellationRepository extends JpaRepository<Cancellation, Long>
{
}
