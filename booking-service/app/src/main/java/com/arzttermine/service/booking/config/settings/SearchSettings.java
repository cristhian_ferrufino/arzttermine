package com.arzttermine.service.booking.config.settings;

import com.arzttermine.library.elasticsearch.config.settings.ElasticsearchSettings;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(
    locations = "classpath:${spring.profiles.active:default}/clients.yml",
    prefix = "search"
)
public class SearchSettings extends ElasticsearchSettings
{
}
