package com.arzttermine.service.booking.service.externals;

import com.arzttermine.service.profile.web.dto.request.CreateCustomerDto;
import com.arzttermine.service.profile.web.dto.response.CustomerDto;

import java.util.Collection;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public interface CustomerApi
{
    CustomerDto findById(UUID token, long id);

    CompletableFuture<CustomerDto> findByIdAsync(UUID token, long id);

    CompletableFuture<CustomerDto> findByIdAsync(long id);

    Map<Long,CustomerDto> findByIds(UUID token, Collection<Long> ids);

    CompletableFuture<Map<Long,CustomerDto>> findByIdsAsync(UUID token, Collection<Long> ids);

    CompletableFuture<Map<Long,CustomerDto>> findByIdsAsync(Collection<Long> ids);

    CustomerDto createCustomer(CreateCustomerDto request);

    CompletableFuture<CustomerDto> createCustomerAsync(CreateCustomerDto request);
}
