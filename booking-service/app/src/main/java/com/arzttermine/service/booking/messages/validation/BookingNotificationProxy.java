
package com.arzttermine.service.booking.messages.validation;

import com.arzttermine.library.messaging.processor.MessageBus;
import com.arzttermine.service.notification.messages.NotificationInitialisation;
import com.arzttermine.service.notification.messages.validation.AbstractNotificationValidationProxy;
import com.arzttermine.service.notifications.web.struct.Notification;
import com.arzttermine.service.notifications.web.struct.NotificationType;
import com.arzttermine.service.profile.web.dto.response.PaidFeatureAssignmentDto;

import java.util.Set;

public class BookingNotificationProxy extends AbstractNotificationValidationProxy {

    private Set<PaidFeatureAssignmentDto> paidFeatureDtos;

    public BookingNotificationProxy(final Set<PaidFeatureAssignmentDto> paidFeatureDtos, final MessageBus messageBus) {
        super(messageBus);
        this.paidFeatureDtos = paidFeatureDtos;
    }

    @Override
    protected boolean canSend(NotificationInitialisation message) {

        boolean canSend = true;

        if (message.request.getTemplate().equals(Notification.BOOKING_CONFIRMATION)) {
            canSend = paidFeatureExists("sms_booking_confirmation");
        }
        else if (message.request.getTemplate().equals(Notification.BOOKING_REMINDER)) {
            canSend = paidFeatureExists("sms_booking_reminder");
        }
        else if (message.request.getTemplate().equals(Notification.BOOKING_MOVED)) {
            canSend = paidFeatureExists("sms_booking_movement");
        }
        else if (message.request.getTemplate().equals(Notification.REVIEW_REMINDER)) {
            canSend = paidFeatureExists("sms_review_reminder");
        }

        return canSend;
    }

    @Override
    protected boolean supports(NotificationType type) {
        return NotificationType.SMS.equals(type);
    }

    private boolean paidFeatureExists(final String paidFeatureName) {
        return paidFeatureDtos.stream()
            .anyMatch(paidFeature -> paidFeature.getName().equals(paidFeatureName));
    }

}
