package com.arzttermine.service.booking;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableJpaRepositories
@EnableAspectJAutoProxy
@EnableTransactionManagement
@ComponentScans({
    @ComponentScan("com.arzttermine.library.spring"),
    @ComponentScan("com.arzttermine.service.booking")
})
public class Application
{
    public static void main(String[] args)
    {
        new SpringApplicationBuilder()
            .sources(Application.class)
            .run(args);
    }
}
