package com.arzttermine.service.booking.exception;

import com.arzttermine.library.suport.exception.BusinessException;
import com.arzttermine.library.web.struct.InsuranceType;

import java.util.Collection;
import java.util.stream.Collectors;

public class InsuranceNotBookableException extends BusinessException
{
    public InsuranceNotBookableException(InsuranceType type, Collection<InsuranceType> expected) {
        super(
            "INVALID_INSURANCE",
            type,
            "Insurance type %s is not valid. Must be one of: ",
            expected.stream().map(InsuranceType::name).collect(Collectors.joining(","))
        );
    }
}
