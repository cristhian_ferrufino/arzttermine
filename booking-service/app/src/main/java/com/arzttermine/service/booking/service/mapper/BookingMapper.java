package com.arzttermine.service.booking.service.mapper;

import com.arzttermine.library.web.dto.PriceDto;
import com.arzttermine.service.booking.domain.entity.Booking;
import com.arzttermine.service.booking.web.dto.request.NotificationSettingsDto;
import com.arzttermine.service.booking.web.dto.response.BookingDto;
import com.arzttermine.service.booking.web.dto.response.CustomerDetailsDto;
import com.arzttermine.service.calendar.web.dto.response.AppointmentDto;
import com.arzttermine.service.profile.web.dto.response.CustomerDto;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.util.Optional;

@Component
public class BookingMapper
{
    public BookingDto toDto(final Booking booking, final AppointmentDto appointment, final CustomerDto customer, final boolean hasPracticeAuthority)
    {
        final CustomerDetailsDto details = new CustomerDetailsDto();
        details.setReturning(booking.isReturningCustomer());
        details.setDetails(customer);

        final BookingDto dto = new BookingDto();
        dto.setPaymentInitialised(booking.isPaymentInitialised());
        dto.setStatus(booking.getStatus());
        dto.setAppointment(appointment);
        dto.setPaid(booking.isPaid());
        dto.setUuid(booking.getUuid());
        dto.setId(booking.getId());
        dto.setCustomer(details);
        dto.setTitle(booking.getTitle());
        dto.setLanguage(booking.getLanguage());
        dto.setTreatmentTypeId(booking.getTreatmentTypeId());
        dto.setCreatedAt(Date.from(booking.getCreatedAt()));
        dto.setVersion(booking.getVersion());
        dto.setTotalPrice(Optional.ofNullable(booking.getTotalPrice())
            .map(p -> new PriceDto(p, booking.getUsedCurrency()))
            .orElse(null));

        if (hasPracticeAuthority) {
            dto.setPracticeComment(booking.getPracticeComment());
        }

        final NotificationSettingsDto settings = new NotificationSettingsDto();
        settings.setEmailRecipient(booking.getNotificationEmail());
        settings.setSmsRecipient(booking.getNotificationSms());
        dto.setNotificationSettings(settings);

        return dto;
    }

    public BookingDto toDto(final Booking booking, final AppointmentDto appointment, final boolean hasPracticeAuthority)
    {
        return toDto(booking, appointment, new CustomerDto(), hasPracticeAuthority);
    }
}
