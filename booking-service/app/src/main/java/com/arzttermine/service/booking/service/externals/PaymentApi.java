package com.arzttermine.service.booking.service.externals;

import com.arzttermine.library.web.dto.PriceDto;
import com.arzttermine.service.booking.domain.entity.Booking;

public interface PaymentApi
{
    void payBooking(Booking booking, PriceDto price, String email, String fullName, String source);
}
