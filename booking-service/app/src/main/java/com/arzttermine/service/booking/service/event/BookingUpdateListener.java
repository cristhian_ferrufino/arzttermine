package com.arzttermine.service.booking.service.event;

import com.arzttermine.library.elasticsearch.model.BulkAction;
import com.arzttermine.library.elasticsearch.service.DocumentProcessor;
import com.arzttermine.library.messaging.processor.MessageBus;
import com.arzttermine.library.web.dto.PriceDto;
import com.arzttermine.library.web.struct.Language;
import com.arzttermine.service.booking.domain.entity.Booking;
import com.arzttermine.service.booking.service.event.model.BookingPaidEvent;
import com.arzttermine.service.booking.service.event.model.UpdateBookingEvent;
import com.arzttermine.service.booking.service.externals.AppointmentApi;
import com.arzttermine.service.booking.service.externals.CustomerApi;
import com.arzttermine.service.booking.service.mapper.BookingMapper;
import com.arzttermine.service.booking.service.processor.BookingDataProcessor;
import com.arzttermine.service.booking.web.dto.response.BookingDto;
import com.arzttermine.service.notification.messages.booking.BookingCreatedMessage;
import com.arzttermine.service.notification.messages.booking.BookingInvoiceMessage;
import com.arzttermine.service.notifications.web.struct.NotificationType;
import com.arzttermine.service.payment.web.struct.AdditionalData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionalEventListener;

import java.util.Collections;
import java.util.Optional;

@Component
public class BookingUpdateListener
{
    private static final Logger log = LoggerFactory.getLogger(BookingUpdateListener.class);

    @Autowired
    private BookingMapper mapper;

    @Autowired
    private AppointmentApi appointmentClient;

    @Autowired
    private CustomerApi customerClient;

    @Autowired
    private DocumentProcessor documentProcessor;

    @Autowired
    private MessageBus messageBus;

    @Autowired
    private BookingDataProcessor dataProcessor;

    @Async
    @TransactionalEventListener
    public void updateBooking(UpdateBookingEvent event)
    {
        final Booking booking = event.getBooking();
        dataProcessor.process(booking);

        // do not show practice related details in search
        final BookingDto dto = mapper.toDto(booking, booking.getAppointment(), booking.getCustomer(), true);
        documentProcessor.index("bookings", String.format("practice-%d", booking.getPracticeId()), dto);
    }

    /**
     * @TODO might be better to send invoices via payment-service! Due to data-lack in payment-service the invoice is published here
     * to improve this we need a unified and standardized invoice template etc.
     *
     * @param event BookingPaidEvent
     */
    @Async
    @TransactionalEventListener
    @Transactional(readOnly = false)
    public void bookingPaid(BookingPaidEvent event)
    {
        dataProcessor.process(event.getBooking());

        final BookingInvoiceMessage.InvoiceBag bag = new BookingInvoiceMessage.InvoiceBag();
        bag.payerEmailAddress = event.getPayload().getPayerEmail();
        bag.payerFullName = event.getPayload().getPayerFullName();
        bag.paidAt = event.getPayload().getPaidAt().toInstant();
        bag.createdAt = event.getPayload().getCreatedAt().toInstant();
        bag.payerId = event.getBooking().getCustomerId();
        bag.bookingId = event.getBooking().getId();
        bag.price = event.getPayload().getTotal();
        bag.transactionId = event.getPayload().getTransactionId();
        bag.paymentDetails = event.getPayload().getPaymentDetails();
        bag.articleDescription = event.getBooking().getTreatmentType().getType();
        bag.practiceName = event.getBooking().getAppointment().getPractice().getName();

        messageBus.send(new BookingInvoiceMessage(bag, event.getBooking().getLanguage()));

        // This booking required payment, so we delayed the booking confirmation delivery to the practice till we have payment details (including success)
        if (event.getBooking().isPaymentInitialised()) {

            final BookingCreatedMessage.CreatedBag createdBag = new BookingCreatedMessage.CreatedBag();
            createdBag.start = event.getBooking().getAppointment().getStart().toInstant();
            createdBag.end = event.getBooking().getAppointment().getEnd().toInstant();
            createdBag.bookingId = event.getBooking().getId();
            createdBag.practiceId = event.getBooking().getAppointment().getPractice().getId();
            createdBag.practiceName = event.getBooking().getAppointment().getPractice().getName();
            createdBag.city = event.getBooking().getAppointment().getPractice().getLocation().getCity();
            createdBag.customerName = event.getBooking().getCustomer().getFullName();
            createdBag.customerMail = event.getBooking().getNotificationEmail();
            createdBag.customerPhone = event.getBooking().getNotificationSms();
            createdBag.doctorName = event.getBooking().getAppointment().getDoctor().getFullName();
            createdBag.treatmentType = event.getBooking().getTreatmentType().getType();
            createdBag.insuranceType = event.getBooking().getInsuranceType();
            createdBag.returningPatient = event.getBooking().isReturningCustomer();
            createdBag.price = Optional.ofNullable(event.getBooking().getTotalPrice()).map(PriceDto::new).orElse(null);
            createdBag.paymentType = event.getPayload().getPaymentDetails().get(AdditionalData.DataType.METHOD_BRAND.getDetailsType());
            createdBag.paymentSuccess = event.getBooking().isPaid();

            messageBus.sendAsync(Collections.singletonList(new BookingCreatedMessage(createdBag, NotificationType.EMAIL, event.getBooking().getAppointment().getPractice().getLocation().getContactMail(), Language.GERMAN )));
        }

        documentProcessor.executeAsync(
            "bookings",
            String.format("practice-%d", event.getBooking().getPracticeId()),
            Collections.singletonList(BulkAction.update(
                mapper.toDto(event.getBooking(), event.getBooking().getAppointment(), event.getBooking().getCustomer(), true)
            ))
        );
    }
}
