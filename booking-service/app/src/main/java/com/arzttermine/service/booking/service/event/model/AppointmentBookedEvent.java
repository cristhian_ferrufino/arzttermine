package com.arzttermine.service.booking.service.event.model;

import com.arzttermine.library.elasticsearch.model.BulkAction;
import com.arzttermine.service.booking.domain.entity.Booking;
import com.arzttermine.service.calendar.web.dto.response.AppointmentDto;
import com.arzttermine.service.profile.web.dto.response.CustomerDto;
import com.arzttermine.service.profile.web.dto.response.TreatmentTypeDto;

public class AppointmentBookedEvent
{
    private final CustomerDto customer;
    private final TreatmentTypeDto treatment;
    private final AppointmentDto appointment;
    private final Booking result;
    private final BulkAction.ActionType updateType;

    protected AppointmentBookedEvent(Booking result, AppointmentDto appointment, CustomerDto customer, TreatmentTypeDto treatment, BulkAction.ActionType updateType) {
        this.result = result;
        this.appointment = appointment;
        this.customer = customer;
        this.treatment = treatment;
        this.updateType = updateType;
    }

    public static AppointmentBookedEvent of(Booking result, AppointmentDto appointment, CustomerDto customer, TreatmentTypeDto treatment) {
        return new AppointmentBookedEvent(result, appointment, customer, treatment, BulkAction.ActionType.INDEX);
    }

    public static AppointmentBookedEvent ofUpdated(Booking result, AppointmentDto appointment, CustomerDto customer, TreatmentTypeDto treatment) {
        return new AppointmentBookedEvent(result, appointment, customer, treatment, BulkAction.ActionType.UPDATE);
    }

    public static AppointmentBookedEvent ofDeleted(Booking result, AppointmentDto appointment, CustomerDto customer, TreatmentTypeDto treatment) {
        return new AppointmentBookedEvent(result, appointment, customer, treatment, BulkAction.ActionType.DELETE);
    }

    public static AppointmentBookedEvent ofNew(Booking result, AppointmentDto appointment, CustomerDto customer, TreatmentTypeDto treatment) {
        return new AppointmentBookedEvent(result, appointment, customer, treatment, BulkAction.ActionType.INDEX);
    }

    public CustomerDto getCustomer() {
        return customer;
    }

    public AppointmentDto getAppointment() {
        return appointment;
    }

    public Booking getResult() {
        return result;
    }

    public TreatmentTypeDto getTreatment() { return treatment; }

    public BulkAction.ActionType getUpdateType() { return updateType;}
}
