package com.arzttermine.service.booking.exception;

import com.arzttermine.library.suport.exception.BusinessException;
import com.arzttermine.library.web.struct.HttpStatus;

import java.util.Optional;

public class PaymentRequiredException extends BusinessException
{
    public PaymentRequiredException() {
        super("PAYMENT_REQUIRED", null, "This appointment requires a payment");
        setStatus(HttpStatus.PAYMENT_REQUIRED);
    }

    public PaymentRequiredException(String reference, String failureMessage) {
        super("PAYMENT_REQUIRED", reference, Optional.ofNullable(failureMessage).orElse("Payment failed"));
        setStatus(HttpStatus.PAYMENT_REQUIRED);
    }
}
