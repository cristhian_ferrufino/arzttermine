package com.arzttermine.service.booking.service.processor;

import com.arzttermine.library.web.struct.InsuranceType;
import com.arzttermine.library.web.struct.Language;
import com.arzttermine.service.booking.domain.entity.Booking;
import com.arzttermine.service.booking.domain.repository.BookingRepository;
import com.arzttermine.service.booking.exception.AlreadyBookedException;
import com.arzttermine.service.booking.exception.BookingNotAvailableException;
import com.arzttermine.service.booking.exception.InsuranceNotBookableException;
import com.arzttermine.service.booking.exception.LanguageNotBookableException;
import com.arzttermine.service.booking.exception.PaymentRequiredException;
import com.arzttermine.service.booking.exception.TreatmentTypeNotBookableException;
import com.arzttermine.service.booking.service.externals.PaymentApi;
import com.arzttermine.service.booking.web.dto.request.CreateBookingDto;
import com.arzttermine.service.booking.web.dto.request.CustomerRequestDto;
import com.arzttermine.service.booking.web.struct.BookingStatus;
import com.arzttermine.service.calendar.web.dto.response.AppointmentDto;
import com.arzttermine.service.profile.web.dto.response.CustomerDto;
import com.arzttermine.service.profile.web.dto.response.DoctorProfileDto;
import com.arzttermine.service.profile.web.dto.response.TreatmentTypeDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

@Component
public class AppointmentTransformer
{
    private static final Logger log = LoggerFactory.getLogger(AppointmentTransformer.class);

    @Autowired
    private BookingRepository repository;

    @Autowired
    private PaymentApi paymentClient;

    public Booking transformAppointment(final CreateBookingDto request, final CustomerDto customer, final AppointmentDto appointment, final TreatmentTypeDto treatment)
    {
        log.info("trying to transform appointment {} to booking", appointment.getId());

        final boolean exist = repository.existBooking(appointment.getId(), BookingStatus.released);

        if (exist) {
            throw new AlreadyBookedException(appointment.getId());
        }

        validateAppointment(request, customer, appointment, treatment);

        final Booking booking = repository.save(Booking.builder()
            .returningCustomer(Optional.ofNullable(request.getCustomer()).map(CustomerRequestDto::isReturning).orElse(false))
            .insuranceType(Optional.ofNullable(request.getCustomer()).map(CustomerRequestDto::getInsuranceType).orElse(null))
            .notifications(request.getNotificationSettings())
            .practiceId(request.getPracticeId())
            .treatmentTypeId(treatment.getId())
            .appointmentId(appointment.getId())
            .totalPrice(treatment.getCharge())
            .platform(request.getPlatform())
            .language(Optional.ofNullable(customer.getPreferredLanguage()).orElse(Language.GERMAN))
            .status(BookingStatus.CREATED)
            .customerId(customer.getId())
            .title(request.getTitle())
            .build());

        if (treatment.isFreeTreatment() || (!treatment.getForcePay() && !request.hasPaymentToken())) {
            return booking;
        }

        try {
            paymentClient.payBooking(
                booking,
                treatment.getCharge(),
                customer.getEmail(),
                Optional.ofNullable(request.getCustomer()).map(c ->
                    String.format("%s %s", c.getFirstName(), c.getLastName())).orElse(customer.getFullName()),
                request.getCapture()
            );

            booking.paymentInitialised();
        } catch (Exception e) {
            log.error("payment failed for booking {}", booking.getId(), e);

            if (treatment.isForcePay()) {
                throw new PaymentRequiredException("paymentFailure", e.getMessage());
            }
        }

        return booking;
    }

    private void validateAppointment(final CreateBookingDto request, final CustomerDto customer, final AppointmentDto appointment, final TreatmentTypeDto treatment)
    {
        // if doctor is there we must respect his desired offset between now and appointment start
        Optional.ofNullable(appointment.getDoctor()).map(DoctorProfileDto::getBookingTimeOffset).ifPresent(offset -> {
            final Instant start = appointment.getStart().toInstant();
            final Instant minimumDate = Instant.now().plus(offset, ChronoUnit.HOURS);

            if (start.isBefore(minimumDate)) {
                throw new BookingNotAvailableException(start, offset * 60);
            }
        });

        final boolean hasCustomer = null != request.getCustomer();

        final Language language = Optional.ofNullable((hasCustomer) ? request.getCustomer().getLanguage() : null)
            .orElse(customer.getPreferredLanguage());

        final InsuranceType insuranceType = Optional.ofNullable((hasCustomer) ? request.getCustomer().getInsuranceType() : null)
            .orElse((null != customer.getInsurance()) ? customer.getInsurance().getType() : null);

        if (!appointment.getAvailableLanguages().contains(language)) {
            throw new LanguageNotBookableException(language, appointment.getAvailableLanguages());
        }

        if (!appointment.getAvailableInsuranceTypes().contains(insuranceType)) {
            throw new InsuranceNotBookableException(insuranceType, appointment.getAvailableInsuranceTypes());
        }

        if (!appointment.getAvailableTreatmentTypes().contains(treatment.getId())) {
            throw new TreatmentTypeNotBookableException(request.getTreatmentTypeId(), appointment.getAvailableTreatmentTypes());
        }

        if (treatment.requiresPayment() && !request.hasPaymentToken()) {
            throw new PaymentRequiredException();
        }
    }
}
