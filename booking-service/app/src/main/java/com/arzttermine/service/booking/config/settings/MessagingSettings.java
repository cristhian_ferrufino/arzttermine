package com.arzttermine.service.booking.config.settings;

import com.arzttermine.library.messaging.config.settings.MessagingConnectionSettings;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(
    locations = "classpath:${spring.profiles.active:default}/clients.yml",
    prefix = "messaging"
)
public class MessagingSettings extends MessagingConnectionSettings
{
}
