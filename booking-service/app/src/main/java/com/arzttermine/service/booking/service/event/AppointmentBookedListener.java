package com.arzttermine.service.booking.service.event;

import com.arzttermine.library.elasticsearch.model.BulkAction;
import com.arzttermine.library.elasticsearch.service.DocumentProcessor;
import com.arzttermine.library.messaging.model.TopicMessage;
import com.arzttermine.library.messaging.processor.MessageBus;
import com.arzttermine.library.suport.AmazonStorage;
import com.arzttermine.library.suport.IcalUtil;
import com.arzttermine.library.suport.exception.EntityNotFoundException;
import com.arzttermine.library.suport.templating.TemplateContentTransformer;
import com.arzttermine.library.suport.templating.exchange.UrlRequest;
import com.arzttermine.library.web.dto.PriceDto;
import com.arzttermine.library.web.struct.AmazonStorageResult;
import com.arzttermine.library.web.struct.Language;
import com.arzttermine.service.booking.domain.entity.Booking;
import com.arzttermine.service.booking.messages.AppointmentBookedMessage;
import com.arzttermine.service.booking.messages.validation.BookingNotificationProxy;
import com.arzttermine.service.booking.service.event.model.AppointmentBookedEvent;
import com.arzttermine.service.booking.service.externals.ProfileApi;
import com.arzttermine.service.booking.service.mapper.BookingMapper;
import com.arzttermine.service.booking.templates.BookingIcalDescription;
import com.arzttermine.service.booking.web.dto.response.BookingDto;
import com.arzttermine.service.notification.messages.NotificationInitialisation;
import com.arzttermine.service.notification.messages.booking.BookingConfirmationMessage;
import com.arzttermine.service.notification.messages.booking.BookingCreatedMessage;
import com.arzttermine.service.notification.messages.booking.BookingReminderMessage;
import com.arzttermine.service.notification.messages.booking.ReviewReminderMessage;
import com.arzttermine.service.notification.messages.templating.ReviewSiteTemplate;
import com.arzttermine.service.notifications.web.request.NotificationAttachmentDto;
import com.arzttermine.service.notifications.web.struct.NotificationType;
import com.arzttermine.service.profile.web.dto.LocationDto;
import com.arzttermine.service.profile.web.dto.response.PaidFeatureAssignmentDto;
import com.arzttermine.service.profile.web.dto.response.ReviewSiteDto;
import com.google.common.net.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionalEventListener;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Component
public class AppointmentBookedListener
{
    private static final Logger log = LoggerFactory.getLogger(AppointmentBookedListener.class);

    private static final BookingReminder[] REMINDERS = {
        new BookingReminder(NotificationType.SMS, 1440),
        new BookingReminder(NotificationType.EMAIL, 7200)
    };

    @Autowired
    private MessageBus messageBus;

    @Autowired
    private DocumentProcessor documentProcessor;

    @Autowired
    private BookingMapper mapper;

    @Autowired
    private ProfileApi profileClient;

    @Autowired
    private TemplateContentTransformer templateProcessor;

    @Autowired
    private AmazonStorage amazonStorage;

    @Async
    @TransactionalEventListener
    @Transactional(readOnly = true)
    @Retryable(backoff = @Backoff(value = 2000, multiplier = 2))
    public void createBookingFromAppointment(final AppointmentBookedEvent event)
    {
        // Assumption: the result will not be null.
        final Booking booking = event.getResult();

        log.info(String.format("appointment with id %d was booked by customer %d", booking.getAppointmentId(), booking.getCustomerId()));

        // Send notification to customer.
        final List<TopicMessage> messages = new ArrayList<>();
        final Set<PaidFeatureAssignmentDto> paidFeatures = profileClient.findPaidFeaturesByPracticeId(booking.getPracticeId());
        final BookingNotificationProxy bookingNotification = new BookingNotificationProxy(paidFeatures, messageBus);

        ReviewSiteDto reviewSite = null;
        try {
            reviewSite = profileClient.findReviewSiteByPracticeId(booking.getPracticeId());
        }
        catch (EntityNotFoundException e) {
            // TODO We need to handle this better in the client, for now we'll handle the exception.
        }

        final AppointmentBookedMessage message = new AppointmentBookedMessage();
        message.setAppointmentId(booking.getAppointmentId());
        message.setCustomerId(booking.getCustomerId());
        messages.add(message);
        messageBus.sendAsync(messages);

        List<NotificationInitialisation> notificationMessages = customerNotifications(event, reviewSite);

        // If this booking does not have a payment associated with it, we will send the notification to the practice immediately.
        // Otherwise, we'll send it once we have the payment details (including success).
        if (! event.getResult().isPaymentInitialised())
            notificationMessages.add(practiceNotification(event));

        bookingNotification.sendAsync(notificationMessages);

        // Index in elastic.
        final String type = "practice-" + booking.getPracticeId();

        // Assumption: We do not have practice authority for comments as we do not need to see them in a search.
        final BookingDto bookingDto = mapper.toDto(booking, event.getAppointment(), event.getCustomer(), true);
        final BulkAction action;
        if (event.getUpdateType() == BulkAction.ActionType.INDEX) {
            documentProcessor.index("bookings", type, bookingDto);
        }
        else {
            if (event.getUpdateType() == BulkAction.ActionType.DELETE) {
                action = BulkAction.delete(bookingDto);
            } else {
                action = BulkAction.update(bookingDto);
            }
            documentProcessor.execute("bookings", type, Collections.singletonList(action));
        }
    }

    private List<NotificationInitialisation> customerNotifications(final AppointmentBookedEvent event, final ReviewSiteDto reviewSiteDto)
    {
        final List<NotificationInitialisation> messages = new ArrayList<>();
        final LocationDto location = event.getAppointment().getPractice().getLocation();

        final String notificationEmail = Optional.ofNullable(event.getResult().getNotificationEmail())
            .orElse(event.getCustomer().getEmail());

        final String notificationPhone = event.getResult().getNotificationSms();

        final Instant appointmentStart = event.getAppointment().getStart().toInstant();
        final Instant now = Instant.now();

        for (int i = 0; i < REMINDERS.length; i++) {
            // adding 30 minutes as we do not want to send spam, which no one really is interested in
            if (now.plus(REMINDERS[i].sendMinutesBefore + 30, ChronoUnit.MINUTES).isAfter(appointmentStart)) {
                continue;
            }

            final BookingReminderMessage.ReminderBag bag = new BookingReminderMessage.ReminderBag();
            bag.start = event.getAppointment().getStart().toInstant();
            bag.customerName = event.getCustomer().getFullName();
            bag.customerId = event.getResult().getCustomerId();
            bag.doctorName = event.getAppointment().getDoctor().getFullName();
            bag.practiceName = event.getAppointment().getPractice().getName();
            bag.practiceLocation = event.getAppointment().getPractice().getLocation();
            bag.start = event.getAppointment().getStart().toInstant();
            bag.treatmentType = event.getTreatment().getType();
            bag.created = event.getResult().getCreatedAt();
            bag.bookingId = event.getResult().getId();
            bag.sendBeforeMinutes = REMINDERS[i].sendMinutesBefore;

            messages.add(new BookingReminderMessage(bag, REMINDERS[i].type,
                (REMINDERS[i].type.equals(NotificationType.EMAIL) ? notificationEmail : notificationPhone), Language.GERMAN));
        }

        final BookingConfirmationMessage.ConfirmationBag confirmationBag = new BookingConfirmationMessage.ConfirmationBag();
        confirmationBag.start = event.getAppointment().getStart().toInstant();
        confirmationBag.customerName = event.getCustomer().getFullName();
        confirmationBag.customerId = event.getResult().getCustomerId();
        confirmationBag.doctorName = event.getAppointment().getDoctor().getFullName();
        confirmationBag.bookingId = event.getResult().getId();
        confirmationBag.practiceLocation = location;
        confirmationBag.practiceName = event.getAppointment().getPractice().getName();
        confirmationBag.treatmentType = event.getTreatment().getType();
        confirmationBag.paymentInitialised = event.getResult().isPaymentInitialised();
        confirmationBag.totalPrice = Optional.ofNullable(event.getResult().getTotalPrice()).map(PriceDto::new).orElse(null);

        ReviewReminderMessage.ReviewReminderBag reviewReminderBag = null;
        if (null != reviewSiteDto) {

            reviewReminderBag = new ReviewReminderMessage.ReviewReminderBag();
            reviewReminderBag.start = event.getAppointment().getStart().toInstant();
            reviewReminderBag.customerName = event.getCustomer().getFullName();
            reviewReminderBag.customerId = event.getResult().getCustomerId();
            reviewReminderBag.doctorName = event.getAppointment().getDoctor().getFullName();
            reviewReminderBag.bookingId = event.getResult().getId();
            reviewReminderBag.practiceLocation = location;
            reviewReminderBag.practiceName = event.getAppointment().getPractice().getName();
            reviewReminderBag.treatmentType = event.getTreatment().getType();
            reviewReminderBag.sendAfterMinutes = 1440; // Send reminder 24 hours after appointment.
            reviewReminderBag.reviewUrl = ReviewSiteTemplate.getUrl(reviewSiteDto);
        }

        if (null != notificationEmail) {
            BookingConfirmationMessage confirmationMessage = bookingConfirmationWithIcal(confirmationBag, event, notificationEmail);
            messages.add(confirmationMessage);
            if (null != reviewReminderBag)
                messages.add(new ReviewReminderMessage(reviewReminderBag, NotificationType.EMAIL, notificationEmail, event.getResult().getLanguage()));
        }

        if (null != notificationPhone) {
            messages.add(new BookingConfirmationMessage(confirmationBag, NotificationType.SMS, notificationPhone, event.getResult().getLanguage()));
            if (null != reviewReminderBag)
                messages.add(new ReviewReminderMessage(reviewReminderBag, NotificationType.SMS, notificationPhone, event.getResult().getLanguage()));
        }

        return messages;
    }

    private BookingCreatedMessage practiceNotification(final AppointmentBookedEvent event)
    {
        final BookingCreatedMessage.CreatedBag bag = new BookingCreatedMessage.CreatedBag();
        bag.bookingId = event.getResult().getId();
        bag.customerName = event.getCustomer().getFullName();
        bag.customerMail = event.getResult().getNotificationEmail();
        bag.customerPhone = event.getResult().getNotificationSms();
        bag.doctorName = event.getAppointment().getDoctor().getFullName();
        bag.start = event.getAppointment().getStart().toInstant();
        bag.end = event.getAppointment().getEnd().toInstant();
        bag.insuranceType = event.getResult().getInsuranceType();
        bag.returningPatient = event.getResult().isReturningCustomer();
        bag.treatmentType = event.getTreatment().getType();
        bag.practiceId = event.getAppointment().getPractice().getId();
        bag.practiceName = event.getAppointment().getPractice().getName();
        bag.city = event.getAppointment().getPractice().getLocation().getCity();
        bag.price = Optional.ofNullable(event.getResult().getTotalPrice()).map(PriceDto::new).orElse(null);

        String cancellationUrl = String.format("/app/calendar#/event/%d/cancel", event.getResult().getId());

        try {
            MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();
            queryParams.add("_pid", String.valueOf(event.getResult().getPracticeId()));
            queryParams.add("_tp", URLEncoder.encode(cancellationUrl, StandardCharsets.UTF_8.toString()));

            bag.cancellationUrl = new UrlRequest(
                UrlRequest.Type.FRONTEND,
                new String[]{"app", "epr"}, // entrypoint for redirects
                new String[0],
                queryParams
            );
        } catch (UnsupportedEncodingException e) {
            log.warn("could not encode cancellation url for booking", cancellationUrl);
        }

        // this message should be caught by created directories and additionally send to the main email of the practice
        BookingCreatedMessage bookingCreatedMessage = new BookingCreatedMessage(bag, NotificationType.EMAIL, event.getAppointment().getPractice().getLocation().getContactMail(), Language.GERMAN);

        return bookingCreatedMessage;
    }

    private BookingConfirmationMessage bookingConfirmationWithIcal(final BookingConfirmationMessage.ConfirmationBag confirmationBag, final AppointmentBookedEvent event, String notificationEmail) {
        BookingConfirmationMessage bookingConfirmationMessage = new BookingConfirmationMessage(confirmationBag, NotificationType.EMAIL, notificationEmail, event.getResult().getLanguage());

        String iCal = generateBookingIcalEvent(event);

        // TODO the path should be handled better, probably with a specific attachment subclass, ie. BookingCreatedIcalAttachment etc.
        String attachmentKey = "notifications/booking-confirmed/booking-"+event.getResult().getId()+".ics";
        NotificationAttachmentDto attachment = new NotificationAttachmentDto();
        attachment.setAttachmentKey(attachmentKey);
        attachment.setBucketName(amazonStorage.getDefaultBucketName());
        attachment.setMimetype(MediaType.I_CALENDAR_UTF_8.toString());
        AmazonStorageResult uploadResult = amazonStorage.upload(attachment.getAttachmentKey(), iCal, attachment.getMimetype() );

        // Currently if we cant upload the result, we're not going to worry about it. So upload failure = no ical for you.
        if (uploadResult == AmazonStorageResult.OK)
            bookingConfirmationMessage.request.addAttachment(attachment);

        return bookingConfirmationMessage;
    }

    private String generateBookingIcalEvent(AppointmentBookedEvent event) {

        // TODO: Move formatting and localisation elsewhere. Needs a better approach.
        BookingIcalDescription descriptionTemplate = new BookingIcalDescription();
        LocalDateTime start = LocalDateTime.ofInstant(event.getAppointment().getStart().toInstant(), ZoneId.systemDefault());
        final Locale locale = Locale.GERMANY;

        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm a").withLocale(locale).withZone(ZoneId.systemDefault());
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy").withLocale(locale).withZone(ZoneId.systemDefault());

        descriptionTemplate.addParam("patientFullName", event.getCustomer().getFullName());
        descriptionTemplate.addParam("patientFirstName", event.getCustomer().getFirstName());
        descriptionTemplate.addParam("patientLastName", event.getCustomer().getLastName());
        descriptionTemplate.addParam("appointmentDay", start.getDayOfWeek().getDisplayName(TextStyle.FULL, locale));
        descriptionTemplate.addParam("appointmentDate", start.format(dateFormatter));
        descriptionTemplate.addParam("appointmentTime", start.format(timeFormatter));
        descriptionTemplate.addParam("doctorTitle", event.getAppointment().getDoctor().getTitle());
        descriptionTemplate.addParam("doctorFirstName", event.getAppointment().getDoctor().getFirstName());
        descriptionTemplate.addParam("doctorLastName", event.getAppointment().getDoctor().getLastName());
        descriptionTemplate.addParam("practiceName", event.getAppointment().getPractice().getName());
        descriptionTemplate.addParam("practicePhone", event.getAppointment().getPractice().getLocation().getPhone());
        descriptionTemplate.addParam("practiceWebsite", "http://www.arzttermine.de/"); // TODO: We might actually want to use the practice's website here.
        descriptionTemplate.addParam("practiceEmail", event.getAppointment().getPractice().getLocation().getContactMail());

        String description = templateProcessor.getContent(descriptionTemplate);

        return IcalUtil.generateIcalEventString(
            event.getAppointment().getStart().toInstant(),
            event.getAppointment().getEnd().toInstant(),
            event.getAppointment().getDoctor().getTitledFullName(),
            event.getAppointment().getDoctor().getEmail(),
            event.getTreatment().getType() + " - " + event.getCustomer().getLastName(),
            description,
            event.getAppointment().getPractice().getLocation().getLocation() + ", " + event.getAppointment().getPractice().getLocation().getCity()
        );
    }

    private static final class BookingReminder {
        NotificationType type;
        int sendMinutesBefore;

        BookingReminder(NotificationType type, int sendMinutesBefore) {
            this.type = type;
            this.sendMinutesBefore = sendMinutesBefore;
        }
    }

}
