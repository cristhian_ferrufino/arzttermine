package com.arzttermine.service.booking.exception;

import com.arzttermine.library.suport.exception.BusinessException;
import com.arzttermine.library.web.struct.Language;

import java.util.Collection;
import java.util.stream.Collectors;

public class LanguageNotBookableException extends BusinessException
{
    public LanguageNotBookableException(Language language, Collection<Language> languages) {
        super(
            "LANGUAGE_NOT_SUPPORTED",
            language,
            "The appointment is not available for the requested language %s, must be one of: ",
            language,
            languages.stream().map(Language::name).collect(Collectors.joining(","))
        );
    }
}
