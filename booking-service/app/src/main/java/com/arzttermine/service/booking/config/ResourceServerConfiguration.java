package com.arzttermine.service.booking.config;

import com.arzttermine.service.player.client.TokenClient;
import com.arzttermine.service.profile.security.config.RemoteTokenServiceConfigurer;
import com.arzttermine.service.profile.security.config.ResourceServerConfigurer;
import com.arzttermine.service.profile.security.config.settings.RemoteTokenServicesSettings;
import com.arzttermine.service.profile.security.service.TokenCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.common.exceptions.BadClientCredentialsException;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import java.util.Collections;

@Configuration
public class ResourceServerConfiguration extends ResourceServerConfigurer
{
    @Autowired
    private RemoteTokenServices tokenService;

    @Value("${spring.application.name}")
    private String applicationName;

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception
    {
        resources.resourceId(applicationName);
        resources.tokenServices(tokenService);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception
    {
        http.authorizeRequests()
            .regexMatchers(
                "/error.*",
                ".*swagger-ui.*",
                "/health",
                "/.*\\.html",
                "/.*\\.css",
                "/.*\\.js",
                "/.*\\.png",
                "/.*\\.jpe?g",
                "/.*\\.woff2?",
                "/.*\\.ttf",
                "/swagger-resources.*",
                "/v.+/api-docs.*",
                "/bookings"
            )
            .permitAll()
            .and()
            .authorizeRequests()
            .antMatchers(HttpMethod.POST, "/bookings")
            .authenticated()
            .and()
            .csrf().disable()
            .cors().disable()
            .httpBasic()
            .and()
            .authorizeRequests()
            .anyRequest()
            .authenticated()
            .and()
            .sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        BasicAuthenticationFilter basicFilter = new BasicAuthenticationFilter(new HttpBasicAuthManager());
        http.addFilterBefore(basicFilter, UsernamePasswordAuthenticationFilter.class);
    }

    private static class HttpBasicAuthManager implements AuthenticationManager
    {
        @Override
        public Authentication authenticate(Authentication authentication) throws AuthenticationException {
            final String user = (String) authentication.getPrincipal();
            final String password = (String) authentication.getCredentials();

            if (!user.equals("widget-booking-client") || !password.equals("official-client")) {
                throw new BadClientCredentialsException();
            }

            return new UsernamePasswordAuthenticationToken(user, null, Collections.singletonList(new SimpleGrantedAuthority("ROLE_WIDGET")));
        }
    }

    @Configuration
    @Profile("!testing")
    public static class RemoteTokenServicesConfigration extends RemoteTokenServiceConfigurer
    {
        @Autowired
        private RemoteTokenServicesSettings tokenServicesSettings;

        @Autowired
        private TokenClient tokenClient;

        @Bean
        public RemoteTokenServices remoteTokenServices()
        {
            return super.configure(tokenServicesSettings);
        }

        @Bean
        @Primary
        public TokenCache clientTokenCache()
        {
            return super.clientTokenCache(tokenServicesSettings, tokenClient);
        }
    }
}
