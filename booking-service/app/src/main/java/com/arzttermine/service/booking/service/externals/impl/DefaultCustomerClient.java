package com.arzttermine.service.booking.service.externals.impl;

import com.arzttermine.library.client.model.RequestOptions;
import com.arzttermine.library.client.model.param.Parameter;
import com.arzttermine.library.client.model.param.QueryParameter;
import com.arzttermine.service.booking.service.externals.CustomerApi;
import com.arzttermine.service.calendar.web.dto.response.AppointmentDto;
import com.arzttermine.service.player.client.CustomerClient;
import com.arzttermine.service.profile.security.service.TokenCache;
import com.arzttermine.service.profile.web.dto.request.CreateCustomerDto;
import com.arzttermine.service.profile.web.dto.response.CustomerDto;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
@Profile("!testing")
public class DefaultCustomerClient implements CustomerApi
{
    @Autowired
    private CustomerClient client;

    @Autowired
    private TokenCache tokenCache;

    @Override
    public CustomerDto findById(UUID token, long id) {
        final RequestOptions options = new RequestOptions();
        options.setAuthorization(token);
        options.addParameter(new Parameter(Parameter.Type.PATH, "id", String.valueOf(id)));

        return client.getById(options);
    }

    @Override
    public CompletableFuture<CustomerDto> findByIdAsync(UUID token, long id) {
        return CompletableFuture.supplyAsync(() -> findById(token, id));
    }

    @Override
    public CompletableFuture<CustomerDto> findByIdAsync(long id) {
        return findByIdAsync(tokenCache.getToken(), id);
    }

    @Override
    public Map<Long, CustomerDto> findByIds(UUID token, Collection<Long> ids) {
        if (ids.isEmpty()) {
            return Maps.newHashMap();
        }

        final RequestOptions options = new RequestOptions();
        options.setAuthorization(token);
        options.setPaging(0, ids.size());
        ids.forEach(id -> options.addParameter(new QueryParameter("id", id)));

        return client.getCustomers(options).getElements()
            .stream().collect(Collectors.toMap(CustomerDto::getId, Function.identity()));
    }

    @Override
    public CompletableFuture<Map<Long, CustomerDto>> findByIdsAsync(UUID token, Collection<Long> ids) {
        return CompletableFuture.supplyAsync(() -> findByIds(token, ids));
    }

    @Override
    public CompletableFuture<Map<Long, CustomerDto>> findByIdsAsync(Collection<Long> ids) {
        final UUID token = tokenCache.getToken();

        return CompletableFuture.supplyAsync(() -> findByIds(token, ids));
    }

    @Override
    public CustomerDto createCustomer(final CreateCustomerDto request) {
        final RequestOptions options = new RequestOptions();
        options.setAuthorization(tokenCache.getToken());

        return client.create(request, options);
    }

    @Override
    public CompletableFuture<CustomerDto> createCustomerAsync(final CreateCustomerDto request) {
        return CompletableFuture.supplyAsync(() -> createCustomer(request));
    }
}
