package com.arzttermine.service.booking.exception;

import com.arzttermine.library.suport.exception.BusinessException;

public class BookingNotActivatedException extends BusinessException
{
    public BookingNotActivatedException(long id) {
        super("BOOKING_INACTIVE", id, "Booking %d is not active anymore", id);
    }
}
