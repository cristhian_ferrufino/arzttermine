package com.arzttermine.service.booking.exception;

import com.arzttermine.library.suport.exception.BusinessException;

import java.util.Collection;
import java.util.stream.Collectors;

public class TreatmentTypeNotBookableException extends BusinessException
{
    public TreatmentTypeNotBookableException(long id, Collection<Long> types) {
        super("TREATMENT_TYPE_NOT_SUPPORTED", id, "The appointment is not available for the requested treatment type %d, must be one of: ", id, types.stream()
            .map(String::valueOf)
            .collect(Collectors.joining(",")));
    }
}
