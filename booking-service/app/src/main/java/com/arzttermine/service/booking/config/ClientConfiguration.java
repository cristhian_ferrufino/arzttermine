package com.arzttermine.service.booking.config;

import com.arzttermine.library.client.config.RequestProcessorConfigurer;
import com.arzttermine.library.client.config.settings.HttpClientSettings;
import com.arzttermine.library.client.service.RequestProcessor;
import com.arzttermine.service.booking.config.settings.client.AppointmentHttpSettings;
import com.arzttermine.service.booking.config.settings.client.CustomerHttpSettings;
import com.arzttermine.service.booking.config.settings.client.PaymentHttpSettings;
import com.arzttermine.service.calendar.client.AppointmentClient;
import com.arzttermine.service.payment.client.PaymentClient;
import com.arzttermine.service.player.client.CustomerClient;
import com.arzttermine.service.player.client.PracticeClient;
import com.arzttermine.service.player.client.TokenClient;
import com.arzttermine.service.player.client.TreatmentClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.Collections;

@Configuration
@Profile("!testing")
public class ClientConfiguration extends RequestProcessorConfigurer
{
    @Bean
    public RequestProcessor requestProcessor(HttpClientSettings settings)
    {
        return super.requestSender(settings, Collections.emptyList());
    }

    @Bean
    public AppointmentClient appointmentClient(AppointmentHttpSettings settings)
    {
        return new AppointmentClient(settings);
    }

    @Bean
    public PaymentClient paymentClient(PaymentHttpSettings settings)
    {
        return new PaymentClient(settings);
    }

    @Bean
    public TokenClient tokenClient(CustomerHttpSettings settings)
    {
        return new TokenClient(settings);
    }

    @Bean
    public CustomerClient customerClient(CustomerHttpSettings settings)
    {
        return new CustomerClient(settings);
    }

    @Bean
    public TreatmentClient treatmentClient(CustomerHttpSettings settings)
    {
        return new TreatmentClient(settings);
    }

    @Bean
    public PracticeClient practiceClient(CustomerHttpSettings settings)
    {
        return new PracticeClient(settings);
    }
}
