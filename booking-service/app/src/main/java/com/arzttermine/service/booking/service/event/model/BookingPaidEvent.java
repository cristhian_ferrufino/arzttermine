package com.arzttermine.service.booking.service.event.model;

import com.arzttermine.service.booking.domain.entity.Booking;
import com.arzttermine.service.payment.messages.TransactionPaidMessage;

public class BookingPaidEvent
{
    private final Booking booking;
    private final TransactionPaidMessage payload;

    private BookingPaidEvent(Booking booking, TransactionPaidMessage payload) {
        this.booking = booking;
        this.payload = payload;
    }

    public static BookingPaidEvent of(Booking booking, TransactionPaidMessage payload) {
        return new BookingPaidEvent(booking, payload);
    }

    public Booking getBooking() {
        return booking;
    }

    public TransactionPaidMessage getPayload() {
        return payload;
    }
}
