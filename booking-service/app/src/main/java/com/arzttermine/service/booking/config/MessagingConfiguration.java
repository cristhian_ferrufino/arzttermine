package com.arzttermine.service.booking.config;

import com.arzttermine.library.messaging.config.MessagingComponentConfigurer;
import com.arzttermine.library.messaging.config.settings.MessagingConnectionSettings;
import com.arzttermine.library.messaging.processor.ConsumerRegisterProcessor;
import com.arzttermine.library.messaging.processor.MessageBus;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.concurrent.ExecutorService;

@Configuration
public class MessagingConfiguration extends MessagingComponentConfigurer
{
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ExecutorService executorService;

    @Bean
    public MessageBus messageBus(MessagingConnectionSettings settings) {
        settings.setExecutorService(executorService);

        return super.messageBus(objectMapper, settings);
    }

    @Bean
    public ConsumerRegisterProcessor consumerRegisterProcessor(MessagingConnectionSettings settings) throws Exception {
        return super.consumerProcessor(objectMapper, settings);
    }
}
