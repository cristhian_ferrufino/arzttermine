package com.arzttermine.service.booking.service.processor;

import com.arzttermine.library.suport.exception.AccessDeniedException;
import com.arzttermine.library.suport.exception.ServiceUnavailableException;
import com.arzttermine.service.booking.domain.entity.Booking;
import com.arzttermine.service.booking.service.externals.AppointmentApi;
import com.arzttermine.service.booking.service.externals.CustomerApi;
import com.arzttermine.service.booking.service.externals.ProfileApi;
import com.arzttermine.service.calendar.web.dto.response.AppointmentDto;
import com.arzttermine.service.profile.web.dto.response.CustomerDto;
import com.arzttermine.service.profile.web.dto.response.TreatmentTypeDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Component
public class BookingDataProcessor
{
    private static final Logger log = LoggerFactory.getLogger(BookingDataProcessor.class);

    @Autowired
    private CustomerApi customerClient;

    @Autowired
    private ProfileApi profileClient;

    @Autowired
    private AppointmentApi appointmentClient;

    @Retryable(
        backoff = @Backoff(delay = 2000, multiplier = 2),
        maxAttempts = 3,
        include = {
            ServiceUnavailableException.class,
            AccessDeniedException.class
        }
    )
    public void process(final Booking booking)
    {
        // load transient data if not present

        final CompletableFuture<CustomerDto> getCustomer = (booking.getCustomer() != null)
            ? CompletableFuture.completedFuture(booking.getCustomer())
            : customerClient.findByIdAsync(booking.getCustomerId());

        final CompletableFuture<TreatmentTypeDto> getTreatmentType = (booking.getTreatmentType() != null)
            ? CompletableFuture.completedFuture(booking.getTreatmentType())
            : profileClient.findTreatmentTypeByIdAsync(booking.getTreatmentTypeId(), booking.getPracticeId());

        final CompletableFuture<AppointmentDto> getAppointment = (booking.getAppointment() != null)
            ? CompletableFuture.completedFuture(booking.getAppointment())
            : appointmentClient.findByIdAsync(booking.getAppointmentId());

        CompletableFuture.allOf(getCustomer, getTreatmentType, getAppointment).join();

        try {
            booking.attachBookingData(
                getCustomer.get(),
                getAppointment.get(),
                getTreatmentType.get()
            );
        } catch (InterruptedException e) {
            log.error("could not await customer, appointment or treatment future", e);
            throw new ServiceUnavailableException("booking");
        } catch (ExecutionException e) {
            log.warn("error while trying to collect booking details", e.getCause());
            throw (RuntimeException) e.getCause();
        }
    }

    @Retryable(
        backoff = @Backoff(delay = 2000, multiplier = 2),
        maxAttempts = 3,
        include = {
            ServiceUnavailableException.class,
            AccessDeniedException.class
        }
    )
    public void process(final Collection<Booking> bookings, long practiceId) {

        Collection<Long> customerIds = bookings.stream().map(Booking::getCustomerId).collect(Collectors.toSet());
        Collection<Long> treatmentIds = bookings.stream().map(Booking::getTreatmentTypeId).collect(Collectors.toSet());
        Collection<Long> appointmentIds = bookings.stream().map(Booking::getAppointmentId).collect(Collectors.toSet());

        final CompletableFuture<Map<Long,CustomerDto>> getCustomers = customerClient.findByIdsAsync(customerIds);
        final CompletableFuture<Map<Long,TreatmentTypeDto>> getTreatmentTypes = profileClient.findTreatmentTypesByIdsAsync(treatmentIds,practiceId);
        final CompletableFuture<Map<Long,AppointmentDto>> getAppointments = appointmentClient.findByIdsAsync(appointmentIds);

        try {

            for (Booking b: bookings) {
                b.attachBookingData(
                    getCustomers.get().get(b.getCustomerId()),
                    getAppointments.get().get(b.getAppointmentId()),
                    getTreatmentTypes.get().get(b.getTreatmentTypeId())
                );
            }

        } catch (InterruptedException e) {
            log.error("could not await customer, appointment or treatment future", e);
            throw new ServiceUnavailableException("booking");
        } catch (ExecutionException e) {
            log.warn("error while trying to collect booking details", e.getCause());
            throw (RuntimeException) e.getCause();
        }

    }
}
