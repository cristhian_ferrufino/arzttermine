package com.arzttermine.service.booking.exception;

import com.arzttermine.library.suport.exception.BusinessException;

public class AlreadyBookedException extends BusinessException
{
    public AlreadyBookedException(long appointmentId) {
        super("ALREADY_BOOKED", appointmentId, "The appointment %d was already booked by another customer", appointmentId);
    }
}
