package com.arzttermine.service.booking.service.externals.impl;

import com.arzttermine.library.client.model.RequestOptions;
import com.arzttermine.library.web.dto.PriceDto;
import com.arzttermine.service.booking.domain.entity.Booking;
import com.arzttermine.service.booking.service.externals.PaymentApi;
import com.arzttermine.service.payment.client.PaymentClient;
import com.arzttermine.service.payment.web.dto.request.CreateChargeDto;
import com.arzttermine.service.payment.web.struct.PaymentMethodType;
import com.arzttermine.service.profile.security.service.TokenCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("!testing")
public class DefaultPaymentClient implements PaymentApi
{
    @Autowired
    private PaymentClient client;

    @Autowired
    private TokenCache tokenCache;

    @Override
    public void payBooking(Booking booking, PriceDto price, String email, String fullName, String source) {

        final CreateChargeDto request = new CreateChargeDto();
        request.setDescription(String.format("Buchung %d", booking.getId()));
        request.setMethodType(PaymentMethodType.CREDIT_CARD);
        request.setCustomerId(booking.getCustomerId());
        request.setBookingId(booking.getId());
        request.setCustomerFullName(fullName);
        request.setCustomerMail(email);
        request.setToken(source);
        request.setPrice(price);

        final RequestOptions options = new RequestOptions();
        options.setAuthorization(tokenCache.getToken());

        client.charge(request, options);
    }
}
