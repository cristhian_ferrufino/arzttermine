package com.arzttermine.service.booking.exception;

import com.arzttermine.library.suport.exception.BusinessException;

public class AppointmentNotAvailableException extends BusinessException
{
    public AppointmentNotAvailableException(long appointmentId) {
        super("APPOINTMENT_NOT_AVAILABLE", appointmentId, "Appointment %d is not available or disabled", appointmentId);
    }
}
