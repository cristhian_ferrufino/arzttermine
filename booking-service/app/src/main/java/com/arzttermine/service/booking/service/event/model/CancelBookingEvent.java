package com.arzttermine.service.booking.service.event.model;

import com.arzttermine.library.elasticsearch.model.BulkAction;
import com.arzttermine.service.booking.domain.entity.Cancellation;

public class CancelBookingEvent
{
    private final Cancellation cancellation;
    private final BulkAction.ActionType updateType;
    private final long userPrincipal;

    protected CancelBookingEvent(Cancellation cancellation, BulkAction.ActionType updateType, long userPrincipal) {
        this.cancellation = cancellation;
        this.updateType = updateType;
        this.userPrincipal = userPrincipal;
    }

    public static CancelBookingEvent of(Cancellation result, long userPrincipal) {
        return new CancelBookingEvent(result, BulkAction.ActionType.DELETE, userPrincipal);
    }

    public Cancellation getCancellation() {
        return cancellation;
    }

    public BulkAction.ActionType getUpdateType() { return updateType;}

    public long getUserPrincipal() {
        return userPrincipal;
    }

}

