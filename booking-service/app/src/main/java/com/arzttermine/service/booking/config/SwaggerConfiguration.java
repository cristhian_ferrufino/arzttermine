package com.arzttermine.service.booking.config;

import com.fasterxml.classmate.TypeResolver;
import com.google.common.base.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.util.concurrent.ListenableFuture;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.schema.AlternateTypeRule;
import springfox.documentation.schema.WildcardType;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static com.google.common.base.Predicates.or;
import static springfox.documentation.builders.PathSelectors.regex;
import static springfox.documentation.schema.AlternateTypeRules.newRule;

@Configuration
@EnableSwagger2
@Profile({"!testing", "!production"})
public class SwaggerConfiguration
{
    @Autowired
    private TypeResolver typeResolver;

    @Bean
    public Docket serviceApi()
    {
        AlternateTypeRule rule = newRule(
            typeResolver.resolve(ListenableFuture.class, typeResolver.resolve(ResponseEntity.class, WildcardType.class)),
            typeResolver.resolve(WildcardType.class)
        );

        return new Docket(DocumentationType.SWAGGER_2)
            .groupName("booking-service")
            .apiInfo(serviceApiInfo())
            .alternateTypeRules(rule)
            .useDefaultResponseMessages(false)
            .genericModelSubstitutes(ListenableFuture.class, ResponseEntity.class)
            .select()
            .paths(servicePaths())
            .build();
    }

    @SuppressWarnings("unchecked")
    private Predicate<String> servicePaths()
    {
        return or(regex("/bookings.*"));
    }

    private ApiInfo serviceApiInfo()
    {
        return new ApiInfoBuilder()
                .title("booking-service")
                .description("REST-API to handle bookings and to transition appointments to real events")
                .build();
    }
}
