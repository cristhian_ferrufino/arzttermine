package com.arzttermine.service.booking.consumer;

import com.arzttermine.library.messaging.consumer.Consumer;
import com.arzttermine.service.booking.domain.entity.Booking;
import com.arzttermine.service.booking.domain.repository.BookingRepository;
import com.arzttermine.service.booking.service.event.model.BookingPaidEvent;
import com.arzttermine.service.payment.messages.TransactionPaidMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
public class BookingPaidConsumer
{
    private static final Logger log = LoggerFactory.getLogger(BookingPaidConsumer.class);

    @Autowired
    private BookingRepository bookingRepository;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Consumer
    public void bookingPaid(TransactionPaidMessage message)
    {
        final Booking booking = bookingRepository.findOne(message.getBookingId());

        if (null == booking) {
            log.error("PLEASE CHECK: Transaction for booking {} was paid, but booking was not found.", message.getBookingId());
            return;
        }

        if (booking.isPaid()) {
            log.info("update on transaction: booking {} already paid", message.getBookingId());
            return;
        }

        if (!booking.isPaymentInitialised()) {
            log.warn("booking does not expect any payments, but transaction {} was paid for booking {}", message.getTransactionId(), booking.getId());
        }

        booking.paid();

        eventPublisher.publishEvent(BookingPaidEvent.of(booking, message));
        log.info("update on transaction: booking {} was paid successfully", message.getBookingId());
    }
}
