package com.arzttermine.service.booking.web.controller;

import com.arzttermine.library.suport.exception.InvalidArgumentException;
import com.arzttermine.service.booking.service.BookingService;
import com.arzttermine.service.booking.web.dto.request.CancelBookingDto;
import com.arzttermine.service.booking.web.dto.request.CreateBookingDto;
import com.arzttermine.service.booking.web.dto.request.UpdateBookingDto;
import com.arzttermine.service.booking.web.dto.response.BookingDto;
import com.arzttermine.service.booking.web.dto.response.BookingsPageDto;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Set;
import java.util.UUID;

@RestController
@RequestMapping("/bookings")
public class BookingController
{
    @Autowired
    private BookingService bookingService;

    @CrossOrigin(methods = RequestMethod.POST)
    @ApiOperation("Transition an appointment to a real event")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_CREATED, message = "appointment was booked and booking was created"),
        @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "request body is not valid, token might be invalid, ..."),
        @ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, message = "requested customer exists, but is not authorized yet"),
        @ApiResponse(code = HttpServletResponse.SC_PAYMENT_REQUIRED, message = "appointment requires payment hence you have to provide all payment details"),
        @ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, message = "the authenticated user cannot book this appointment"),
        @ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "appointment was not found or is not available"),
        @ApiResponse(code = HttpServletResponse.SC_CONFLICT, message = "appointment is not bookable, maybe its already transitioned"),
        @ApiResponse(code = HttpServletResponse.SC_SERVICE_UNAVAILABLE, message = "service is not available or timed out (eg appointment-service is down)")
    })
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("#oauth2.hasScope('booking') or hasAnyRole('ROLE_WIDGET', 'ROLE_CUSTOMER', 'ROLE_SYSTEM')")
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public BookingDto bookAppointment(@RequestBody @Valid CreateBookingDto request)
    {
        return bookingService.bookAppointment(request);
    }

    @ApiOperation("Update existing booking")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        ),
        @ApiImplicitParam(
            name = "bookingId",
            dataType = "long",
            paramType = "path",
            required = true
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_NO_CONTENT, message = "booking was updated"),
        @ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "booking or appointment was not found")
    })
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAnyRole('ROLE_CUSTOMER', 'ROLE_EMPLOYEE')")
    @RequestMapping(value = "/{bookingId}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void updateBooking(@PathVariable long bookingId, @RequestBody @Valid UpdateBookingDto request)
    {
        bookingService.updateBooking(bookingId, request);
    }

    @ApiOperation("cancels existing booking")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        ),
        @ApiImplicitParam(
            name = "bookingId",
            dataType = "long",
            paramType = "path",
            required = true
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_NO_CONTENT, message = "booking was updated"),
        @ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "booking was not found")
    })
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAnyRole('ROLE_CUSTOMER', 'ROLE_EMPLOYEE', 'ROLE_SYSTEM')")
    @RequestMapping(value = "/{bookingId}", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void cancelBooking(@PathVariable long bookingId, @RequestBody @Valid CancelBookingDto request)
    {
        bookingService.cancelBooking(bookingId, request);
    }

    @ApiOperation("Update existing booking")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        ),
        @ApiImplicitParam(
            name = "bookingId",
            dataType = "long",
            paramType = "path",
            required = true
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "booking was returned"),
        @ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "booking, appointment or customer was not found")
    })
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyRole('ROLE_SYSTEM', 'ROLE_EMPLOYEE', 'ROLE_CUSTOMER')")
    @RequestMapping(value = "/{bookingId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public BookingDto getBooking(@PathVariable String bookingId)
    {
        try {
            return bookingService.getBooking(UUID.fromString(bookingId));
        } catch (IllegalArgumentException e) {
            try {
                return bookingService.getBooking(Long.parseLong(bookingId));
            } catch (NumberFormatException e2) {
                throw new InvalidArgumentException("bookingId", bookingId);
            }
        }
    }

    @ApiOperation("Get bookings for corresponding appointment-id")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        ),
        @ApiImplicitParam(
            name = "appointmentId",
            dataType = "string",
            paramType = "path",
            required = true
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "bookings are returned"),
        @ApiResponse(code = HttpServletResponse.SC_SERVICE_UNAVAILABLE, message = "service is not available or timed out (eg appointment-service is down)")
    })
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyRole('ROLE_SYSTEM', 'ROLE_EMPLOYEE')")
    @RequestMapping(value = "/appointments/{appointmentId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Set<BookingDto> getBookings(@PathVariable long appointmentId)
    {
        return bookingService.getBookings(appointmentId);
    }

    @ApiOperation("Get bookings for corresponding practice-id")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        ),
        @ApiImplicitParam(
            name = "practiceId",
            dataType = "long",
            paramType = "path",
            required = true
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "bookings are returned"),
        @ApiResponse(code = HttpServletResponse.SC_SERVICE_UNAVAILABLE, message = "service is not available or timed out (eg appointment-service is down)")
    })
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyRole('ROLE_SYSTEM', 'ROLE_EMPLOYEE', 'ROLE_PRACTICE_' + #practiceId)")
    @RequestMapping(value = "/practices/{practiceId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public BookingsPageDto getBookingsFromPractice(@PathVariable long practiceId, Pageable pageable)
    {
        return bookingService.getPracticeBookings(practiceId, pageable);
    }

    @ApiOperation("Reindex all bookings for a practice")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        ),
        @ApiImplicitParam(
            name = "practiceId",
            dataType = "long",
            required = true,
            paramType = "path"
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_NO_CONTENT, message = "Bookings are reindexed.")
    })
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAnyRole('ROLE_EMPLOYEE', 'ROLE_SYSTEM', 'ROLE_PRACTICE_'+#practiceId)")
    @RequestMapping(value = "/practices/{practiceId}/reindex", method = RequestMethod.POST)
    public void reindexBookings(@PathVariable long practiceId)
    {
        bookingService.reindexBookings(practiceId);
    }
}
