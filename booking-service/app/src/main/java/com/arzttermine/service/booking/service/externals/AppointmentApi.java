package com.arzttermine.service.booking.service.externals;

import com.arzttermine.service.calendar.web.dto.response.AppointmentDto;

import java.util.Collection;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public interface AppointmentApi
{
    AppointmentDto findById(UUID token, long id);

    CompletableFuture<AppointmentDto> findByIdAsync(long id);

    Map<Long, AppointmentDto> findByIds(UUID token, Collection<Long> ids);

    CompletableFuture<Map<Long, AppointmentDto>> findByIdsAsync(Collection<Long> ids);
}
