USE booking;

ALTER TABLE bookings
  ADD COLUMN `uuid` BINARY(16) DEFAULT NULL;

UPDATE bookings SET uuid = unhex(replace(uuid(), '-', ''));;

ALTER TABLE bookings
  MODIFY COLUMN `uuid` BINARY(16) NOT NULL,
  ADD UNIQUE INDEX un_uuid_for_bookings (`uuid`);
