USE booking;

CREATE TABLE `cancellations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `appointment_availability` bit(1) NOT NULL DEFAULT 1,
  `cancelled_at` datetime NOT NULL DEFAULT NOW(),
  `reason` varchar(128) NOT NULL,
  `booking_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `un_cancellation_per_booking_id` (`booking_id`),
  CONSTRAINT `fk_cancellation_cancelled_booking` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;
