package com.arzttermine.service.booking.service;

import com.arzttermine.library.elasticsearch.model.BulkAction;
import com.arzttermine.library.elasticsearch.service.DocumentProcessor;
import com.arzttermine.service.booking.domain.entity.Booking;
import com.arzttermine.service.booking.domain.repository.BookingRepository;
import com.arzttermine.service.booking.service.mapper.BookingMapper;
import com.arzttermine.service.booking.service.processor.BookingDataProcessor;
import com.arzttermine.service.booking.web.struct.BookingStatus;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class BookingServiceTest
{
    @Mock
    private BookingMapper bookingMapper = new BookingMapper();

    @Mock
    private BookingRepository repository;

    @Mock
    private DocumentProcessor documentProcessor;

    @Mock
    private BookingDataProcessor dataProcessor;

    @InjectMocks
    private BookingService bookingService;


    @Test
    public void reindexBookings_Should_Reindex_Bookings()
    {
        PracticeDto practice = new PracticeDto();
        practice.setId(42L);

        Booking booking = createBooking(1,  BookingStatus.CONFIRMED);
        Booking deletedBooking = createBooking(2, BookingStatus.CANCELLED_BY_EMPLOYEE);

        doAnswer(invocation -> {
            List<BulkAction> actions = invocation.getArgumentAt(2, List.class);
            assertEquals(BulkAction.ActionType.DELETE, actions.get(0).getType());
            assertEquals(BulkAction.ActionType.INDEX, actions.get(1).getType());
            return null;
        }).when(documentProcessor).executeAsync(eq("bookings"), eq("practice-42"), anyCollectionOf(BulkAction.class));

        doAnswer(invocation -> {
            Collection<Booking> bookings = invocation.getArgumentAt(0, Collection.class);
            assertEquals(1,bookings.size());
            assertEquals(booking, bookings.iterator().next());
            return null;
        }).when(dataProcessor).process(Matchers.any(), any(Integer.class));

        when(repository.findByPracticeId(eq(42L), any(Pageable.class)))
            .thenReturn(new PageImpl<>(Arrays.asList(booking, deletedBooking)));

        bookingService.reindexBookings(42L);

        verify(dataProcessor, times(1)).process(Matchers.any(), any(Integer.class));
        verify(documentProcessor, times(1)).executeAsync(eq("bookings"), eq("practice-42"), anyCollectionOf(BulkAction.class));
    }


    private Booking createBooking(long id, BookingStatus status)
    {
        Booking spy = spy(Booking.class);

        when(spy.getId()).thenReturn(id);
        when(spy.getPracticeId()).thenReturn(42l);
        when(spy.getCustomerId()).thenReturn(42l);
        when(spy.getAppointmentId()).thenReturn(42l);
        when(spy.getTreatmentTypeId()).thenReturn(42l);
        spy.setStatus(status);

        return spy;
    }
}
