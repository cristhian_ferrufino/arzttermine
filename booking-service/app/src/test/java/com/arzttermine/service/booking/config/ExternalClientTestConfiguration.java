package com.arzttermine.service.booking.config;

import com.arzttermine.library.client.config.RequestProcessorConfigurer;
import com.arzttermine.service.booking.externals.test.TestAppointmentClient;
import com.arzttermine.service.booking.externals.test.TestCustomerClient;
import com.arzttermine.service.booking.externals.test.TestPaymentClient;
import com.arzttermine.service.booking.service.externals.AppointmentApi;
import com.arzttermine.service.booking.service.externals.CustomerApi;
import com.arzttermine.service.booking.service.externals.PaymentApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import static org.mockito.Mockito.*;

@Configuration
@Profile("testing")
public class ExternalClientTestConfiguration extends RequestProcessorConfigurer
{
    @Bean
    public AppointmentApi appointmentApi()
    {
        return spy(new TestAppointmentClient());
    }

    @Bean
    public PaymentApi paymentApi()
    {
        return spy(new TestPaymentClient());
    }

    @Bean
    public CustomerApi customerApi()
    {
        return spy(new TestCustomerClient());
    }
}
