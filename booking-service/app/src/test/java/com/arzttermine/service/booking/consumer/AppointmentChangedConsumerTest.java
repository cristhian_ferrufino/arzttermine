package com.arzttermine.service.booking.consumer;

import com.arzttermine.library.messaging.processor.MessageBus;
import com.arzttermine.service.booking.externals.test.TestAppointmentClient;
import com.arzttermine.service.booking.web.BaseIntegrationTest;
import com.arzttermine.service.booking.web.controller.BookingControllerTest;
import com.arzttermine.service.booking.web.dto.response.BookingDto;
import com.arzttermine.service.calendar.messages.AppointmentChangedMessage;
import com.arzttermine.service.calendar.web.dto.response.AppointmentDto;
import com.arzttermine.service.calendar.web.dto.response.AppointmentLocation;
import com.arzttermine.service.notifications.web.struct.NotificationType;
import com.arzttermine.service.profile.web.dto.LocationDto;
import com.arzttermine.service.profile.web.dto.response.DoctorProfileDto;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;

import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class AppointmentChangedConsumerTest extends BaseIntegrationTest
{
    @Autowired
    private MessageBus messageBus;

    @Autowired
    private TestConsumerCounter consumerCounter;

    @Test
    @Sql("/db/empty_database.sql")
    public void appointmentChanged_should_update_appointment_from_booking() throws Exception
    {
        BookingDto booking = objectMapper.readValue(mockMvc.perform(customerRequest(42L, () -> post("/bookings"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(BookingControllerTest.buildRequestDto(42))))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), BookingDto.class);

        assertEquals(42L, booking.getAppointment().getId());

        AppointmentDto newAppointment = new AppointmentDto();
        newAppointment.setId(55L);
        newAppointment.setBooked(false);
        newAppointment.setStart(Date.from(Instant.now().plusSeconds(60 * 60)));
        newAppointment.setEnd(Date.from(Instant.now().plusSeconds(65 * 60)));
        newAppointment.setDuration(5);

        TestAppointmentClient.appointment(55L, newAppointment);
        consumerCounter.flush();

        AppointmentChangedMessage message = new AppointmentChangedMessage();
        message.setBookedByCustomerId(42L);
        message.setPrevious(42L);
        message.setNewId(55L);
        messageBus.send(message);

        Thread.sleep(150);

        mockMvc.perform(employeeRequest(42L, () -> get("/bookings/appointments/55")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$[0].appointment.id", is(55)));

        mockMvc.perform(employeeRequest(42L, () -> get("/bookings/appointments/42")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*]", hasSize(0)));
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void appointmentChanged_should_send_out_moved_notifications() throws Exception
    {
        BookingDto booking = objectMapper.readValue(mockMvc.perform(customerRequest(42L, () -> post("/bookings"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(BookingControllerTest.buildRequestDto(42))))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), BookingDto.class);

        assertEquals(42L, booking.getAppointment().getId());

        makeNewAppointment(55L);
        consumerCounter.flush();

        AppointmentChangedMessage message = new AppointmentChangedMessage();
        message.setNotificationTypes(Arrays.asList(NotificationType.SMS, NotificationType.EMAIL));
        message.setBookedByCustomerId(null); // <-- note this
        message.setPrevious(42L);
        message.setNewId(55L);
        messageBus.send(message);

        Thread.sleep(150);

        mockMvc.perform(employeeRequest(42L, () -> get("/bookings/appointments/55")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$[0].appointment.id", is(55)));

        mockMvc.perform(employeeRequest(42L, () -> get("/bookings/appointments/42")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*]", hasSize(0)));

        // sms should not be send out
        assertEquals(1, consumerCounter.getMovedNotifications());
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void appointmentChanged_should_not_send_out_any_notification_if_only_requested_is_not_available() throws Exception
    {
        BookingDto booking = objectMapper.readValue(mockMvc.perform(customerRequest(42L, () -> post("/bookings"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(BookingControllerTest.buildRequestDto(42))))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), BookingDto.class);

        assertEquals(42L, booking.getAppointment().getId());

        makeNewAppointment(12345L);
        consumerCounter.flush();

        AppointmentChangedMessage message = new AppointmentChangedMessage();
        message.setNotificationTypes(Collections.singletonList(NotificationType.SMS));
        message.setBookedByCustomerId(null); // <-- note this
        message.setPrevious(42L);
        message.setNewId(12345L);
        messageBus.send(message);

        Thread.sleep(150);

        // sms should not be send out and email was not requested
        assertEquals(0, consumerCounter.getMovedNotifications());
    }

    private static void makeNewAppointment(long id)
    {
        AppointmentLocation practice = new AppointmentLocation();
        practice.setLocation(new LocationDto());

        DoctorProfileDto doctor = new DoctorProfileDto();
        doctor.setFullName("my full name");

        AppointmentDto newAppointment = new AppointmentDto();
        newAppointment.setId(id);
        newAppointment.setBooked(false);
        newAppointment.setStart(Date.from(Instant.now().plusSeconds(60 * 60)));
        newAppointment.setEnd(Date.from(Instant.now().plusSeconds(65 * 60)));
        newAppointment.setPractice(practice);
        newAppointment.setDoctor(doctor);
        newAppointment.setDuration(5);

        TestAppointmentClient.appointment(id, newAppointment);
    }
}
