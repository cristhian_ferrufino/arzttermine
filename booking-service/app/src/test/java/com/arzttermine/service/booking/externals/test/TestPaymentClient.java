package com.arzttermine.service.booking.externals.test;

import com.arzttermine.library.web.dto.PriceDto;
import com.arzttermine.service.booking.domain.entity.Booking;
import com.arzttermine.service.booking.service.externals.PaymentApi;

public class TestPaymentClient implements PaymentApi
{
    @Override
    public void payBooking(Booking booking, PriceDto price, String email, String fullName, String source) {
    }
}
