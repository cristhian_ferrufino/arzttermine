package com.arzttermine.service.booking.service.processor;

import com.arzttermine.library.web.dto.InsuranceProviderDto;
import com.arzttermine.library.web.dto.PriceDto;
import com.arzttermine.library.web.struct.InsuranceType;
import com.arzttermine.library.web.struct.Language;
import com.arzttermine.service.booking.domain.entity.Booking;
import com.arzttermine.service.booking.domain.repository.BookingRepository;
import com.arzttermine.service.booking.exception.*;
import com.arzttermine.service.booking.service.externals.PaymentApi;
import com.arzttermine.service.booking.web.dto.request.CreateBookingDto;
import com.arzttermine.service.booking.web.dto.request.CustomerRequestDto;
import com.arzttermine.service.booking.web.struct.BookingStatus;
import com.arzttermine.service.calendar.web.dto.response.AppointmentDto;
import com.arzttermine.service.calendar.web.dto.response.AppointmentLocation;
import com.arzttermine.service.profile.web.dto.LocationDto;
import com.arzttermine.service.profile.web.dto.response.CustomerDto;
import com.arzttermine.service.profile.web.dto.response.DoctorProfileDto;
import com.arzttermine.service.profile.web.dto.response.TreatmentTypeDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.Date;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AppointmentTransformerTest
{
    @Mock
    private PaymentApi paymentClient;

    @Mock
    private BookingRepository bookingRepository;

    @InjectMocks
    private AppointmentTransformer transformer;

    @Test(expected = BookingNotAvailableException.class)
    public void transformAppointment_should_throw_not_bookable_when_booking_could_not_be_booked_anymore()
    {
        DoctorProfileDto doctor = new DoctorProfileDto();
        doctor.setBookingTimeOffset(1);

        AppointmentDto appointment = new AppointmentDto();
        appointment.setAvailableLanguages(Collections.singletonList(Language.GERMAN));
        appointment.setAvailableInsuranceTypes(Collections.singletonList(InsuranceType.PRIVATE));
        appointment.setStart(Date.from(Instant.now().plus(59, ChronoUnit.MINUTES)));
        appointment.setDoctor(doctor);

        InsuranceProviderDto insurance = new InsuranceProviderDto();
        insurance.setType(InsuranceType.PRIVATE);

        CustomerDto customer = new CustomerDto();
        customer.setPreferredLanguage(Language.GERMAN);
        customer.setInsurance(insurance);

        transformer.transformAppointment(new CreateBookingDto(), customer, appointment, null);
    }

    @Test(expected = AlreadyBookedException.class)
    public void transformAppointment_should_throw_not_bookable_when_booking_is_already_reserved()
    {
        Instant now = Instant.now();
        AppointmentDto appointment = new AppointmentDto();
        appointment.setStart(Date.from(now.plusSeconds(9999)));
        appointment.setId(42L);

        when(bookingRepository.existBooking(eq(42L), any())).thenReturn(true);

        transformer.transformAppointment(new CreateBookingDto(), new CustomerDto(), appointment, null);
    }

    @Test(expected = LanguageNotBookableException.class)
    public void transformAppointment_should_throw_not_bookable_when_language_is_not_supported()
    {
        Instant now = Instant.now();
        AppointmentDto appointment = new AppointmentDto();
        appointment.setAvailableLanguages(Collections.singletonList(Language.ENGLISH));
        appointment.setStart(Date.from(now.plusSeconds(9999)));
        appointment.setId(42L);

        CustomerRequestDto cc = new CustomerRequestDto();
        cc.setLanguage(Language.GERMAN);

        CreateBookingDto dto = new CreateBookingDto();
        dto.setCustomer(cc);
        dto.setPracticeId(42l);

        transformer.transformAppointment(dto, new CustomerDto(), appointment, null);
    }

    @Test(expected = InsuranceNotBookableException.class)
    public void transformAppointment_should_report_not_unsupported_insurance_type()
    {
        Instant now = Instant.now();
        AppointmentDto appointment = new AppointmentDto();
        appointment.setAvailableLanguages(Collections.singletonList(Language.ENGLISH));
        appointment.setAvailableInsuranceTypes(Collections.singletonList(InsuranceType.PRIVATE));
        appointment.setStart(Date.from(now.plusSeconds(9999)));
        appointment.setId(42L);

        InsuranceProviderDto insurance = new InsuranceProviderDto();
        insurance.setType(InsuranceType.COMPULSORY);

        CustomerDto cc = new CustomerDto();
        cc.setPreferredLanguage(Language.ENGLISH);
        cc.setInsurance(insurance);

        transformer.transformAppointment(new CreateBookingDto(), cc, appointment, null);
    }

    @Test(expected = PaymentRequiredException.class)
    public void transformAppointment_should_report_missing_payment_auth_when_payment_is_required()
    {
        TreatmentTypeDto tt = new TreatmentTypeDto();
        tt.setCharge(new PriceDto(123));
        tt.setId(42L);
        tt.setForcePay(true);

        AppointmentDto appointment = appointmentDto(42L);

        InsuranceProviderDto insurance = new InsuranceProviderDto();
        insurance.setType(InsuranceType.COMPULSORY);

        CustomerDto customer = new CustomerDto();
        customer.setPreferredLanguage(Language.GERMAN);
        customer.setInsurance(insurance);

        CreateBookingDto cb = new CreateBookingDto();
        cb.setTreatmentTypeId(42L);
        cb.setPracticeId(42l);

        transformer.transformAppointment(cb, customer, appointment, tt);
    }

    @Test(expected = TreatmentTypeNotBookableException.class)
    public void transformAppointment_should_report_not_available_treatment()
    {
        TreatmentTypeDto tt = new TreatmentTypeDto();
        tt.setCharge(new PriceDto(123));
        tt.setId(123456L);
        tt.setForcePay(true);

        AppointmentDto appointment = appointmentDto(42L);

        InsuranceProviderDto insurance = new InsuranceProviderDto();
        insurance.setType(InsuranceType.COMPULSORY);

        CustomerDto customer = new CustomerDto();
        customer.setPreferredLanguage(Language.GERMAN);
        customer.setInsurance(insurance);

        CreateBookingDto cb = new CreateBookingDto();
        cb.setTreatmentTypeId(12345678L);
        cb.setPracticeId(42l);

        transformer.transformAppointment(cb, customer, appointment, tt);
    }

    @Test
    public void transformAppointment_should_not_report_missing_payment_auth_when_payment_is_not_forced()
    {
        TreatmentTypeDto tt = new TreatmentTypeDto();
        tt.setCharge(new PriceDto(123));
        tt.setId(42L);
        tt.setForcePay(false);

        AppointmentDto appointment = appointmentDto(42L);

        InsuranceProviderDto insurance = new InsuranceProviderDto();
        insurance.setType(InsuranceType.COMPULSORY);

        CustomerDto customer = new CustomerDto();
        customer.setPreferredLanguage(Language.GERMAN);
        customer.setInsurance(insurance);

        CreateBookingDto cb = new CreateBookingDto();
        cb.setTreatmentTypeId(42L);
        cb.setCapture("token");
        cb.setPracticeId(42l);

        doThrow(RuntimeException.class).when(paymentClient)
            .payBooking(any(), any(), anyString(), anyString(), anyString());

        when(bookingRepository.save(any(Booking.class))).thenAnswer(invocation -> {
            Booking booking = spy(Booking.builder()
                .customerId(42L)
                .practiceId(42L)
                .appointmentId(42L)
                .treatmentTypeId(1L)
                .build());

            when(booking.getId()).thenReturn(0L);
            return booking;
        });

        transformer.transformAppointment(cb, customer, appointment, tt);

        verify(paymentClient, times(1)).payBooking(any(), any(), anyString(), anyString(), eq("token"));
    }

    @Test
    public void transformAppointment_should_return_booking()
    {
        TreatmentTypeDto tt = new TreatmentTypeDto();
        tt.setType("aktuter durchfall");
        tt.setId(42L);

        AppointmentDto appointment = appointmentDto(42L);

        InsuranceProviderDto insurance = new InsuranceProviderDto();
        insurance.setType(InsuranceType.COMPULSORY);

        CustomerDto customer = new CustomerDto();
        customer.setPreferredLanguage(Language.GERMAN);
        customer.setInsurance(insurance);
        customer.setId(42L);

        CreateBookingDto cb = new CreateBookingDto();
        cb.setTreatmentTypeId(42L);
        cb.setPracticeId(42l);

        when(bookingRepository.save(any(Booking.class)))
            .thenAnswer(invocation -> invocation.getArgumentAt(0, Booking.class));

        Booking booking = transformer.transformAppointment(cb, customer, appointment, tt);

        assertEquals(42L, booking.getCustomerId());
        assertEquals(42L, booking.getAppointmentId());
        assertEquals(BookingStatus.CREATED, booking.getStatus());
        assertEquals(Language.GERMAN, booking.getLanguage());
        assertNull(booking.getConfirmedAt());
        assertFalse(booking.isReleased());
    }

    private AppointmentDto appointmentDto(Long... tts)
    {
        AppointmentLocation location = new AppointmentLocation();
        location.setLocation(new LocationDto());

        AppointmentDto appointment = new AppointmentDto();
        appointment.setPractice(location);
        appointment.setAvailableInsuranceTypes(Collections.singletonList(InsuranceType.COMPULSORY));
        appointment.setAvailableLanguages(Collections.singletonList(Language.GERMAN));
        appointment.setAvailableTreatmentTypes(Arrays.asList(tts));
        appointment.setStart(Date.from(Instant.now().plusSeconds(9999)));
        appointment.setId(42L);

        return appointment;
    }
}
