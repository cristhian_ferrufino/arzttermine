package com.arzttermine.service.booking.externals.test;

import com.arzttermine.library.web.dto.InsuranceProviderDto;
import com.arzttermine.library.web.struct.InsuranceType;
import com.arzttermine.library.web.struct.Language;
import com.arzttermine.service.booking.service.externals.CustomerApi;
import com.arzttermine.service.profile.web.dto.request.CreateCustomerDto;
import com.arzttermine.service.profile.web.dto.response.CustomerDto;

import java.util.Collection;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TestCustomerClient implements CustomerApi
{
    @Override
    public CustomerDto findById(UUID token, long id) {
        InsuranceProviderDto insurance = new InsuranceProviderDto();
        insurance.setType(InsuranceType.COMPULSORY);

        CustomerDto customer = new CustomerDto();
        customer.setEmail("random@thought.com");
        customer.setPreferredLanguage(Language.GERMAN);
        customer.setInsurance(insurance);
        customer.setFirstName("casper");
        customer.setLastName("clown");
        customer.setEnabled(true);
        customer.setId(id);

        return customer;
    }

    @Override
    public CompletableFuture<CustomerDto> findByIdAsync(UUID token, long id) {
        return CompletableFuture.completedFuture(findById(token, id));
    }

    @Override
    public CompletableFuture<CustomerDto> findByIdAsync(long id) {
        return findByIdAsync(UUID.randomUUID(), id);
    }

    @Override
    public Map<Long, CustomerDto> findByIds(UUID token, Collection<Long> ids) {
        return ids.stream().collect(Collectors.toMap(Function.identity(), e -> findById(token, e)));
    }

    @Override
    public CompletableFuture<Map<Long, CustomerDto>> findByIdsAsync(UUID token, Collection<Long> ids) {
        return CompletableFuture.completedFuture(findByIds(token,ids));
    }

    @Override
    public CompletableFuture<Map<Long, CustomerDto>> findByIdsAsync(Collection<Long> ids) {
        return findByIdsAsync(UUID.randomUUID(), ids);
    }

    @Override
    public CustomerDto createCustomer(final CreateCustomerDto request) {
        InsuranceProviderDto insurance = new InsuranceProviderDto();
        insurance.setType(InsuranceType.COMPULSORY);

        CustomerDto customer = new CustomerDto();
        customer.setEmail(request.getEmail());
        customer.setInsurance(insurance);
        customer.setId(42L);
        customer.setPreferredLanguage(request.getLanguage());
        customer.setEnabled(true);
        customer.setFirstName("casper");
        customer.setLastName("clown");

        return customer;
    }

    @Override
    public CompletableFuture<CustomerDto> createCustomerAsync(final CreateCustomerDto request) {
        return CompletableFuture.supplyAsync(() -> createCustomer(request));
    }
}
