package com.arzttermine.service.booking.web.controller;

import com.arzttermine.library.elasticsearch.service.DocumentProcessor;
import com.arzttermine.library.suport.exception.EntityNotFoundException;
import com.arzttermine.library.web.struct.Currency;
import com.arzttermine.library.web.struct.InsuranceType;
import com.arzttermine.library.web.struct.Language;
import com.arzttermine.service.booking.consumer.TestConsumerCounter;
import com.arzttermine.service.booking.externals.test.TestProfileClient;
import com.arzttermine.service.booking.service.externals.AppointmentApi;
import com.arzttermine.service.booking.service.externals.CustomerApi;
import com.arzttermine.service.booking.service.externals.PaymentApi;
import com.arzttermine.service.booking.service.externals.ProfileApi;
import com.arzttermine.service.booking.web.BaseIntegrationTest;
import com.arzttermine.service.booking.web.dto.request.CancelBookingDto;
import com.arzttermine.service.booking.web.dto.request.CreateBookingDto;
import com.arzttermine.service.booking.web.dto.request.CustomerRequestDto;
import com.arzttermine.service.booking.web.dto.request.NotificationSettingsDto;
import com.arzttermine.service.booking.web.dto.request.UpdateBookingDto;
import com.arzttermine.service.booking.web.dto.response.BookingDto;
import com.arzttermine.service.booking.web.struct.BookingPlatform;
import com.arzttermine.service.booking.web.struct.BookingStatus;
import com.arzttermine.service.booking.web.struct.CancelReason;
import com.arzttermine.service.calendar.web.dto.response.AppointmentDto;
import com.arzttermine.service.notifications.web.struct.Notification;
import com.arzttermine.service.notifications.web.struct.NotificationType;
import com.arzttermine.service.profile.web.dto.response.PaidFeatureAssignmentDto;
import com.arzttermine.service.profile.web.struct.Gender;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;

import java.util.*;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class BookingControllerTest extends BaseIntegrationTest
{
    @Autowired
    private AppointmentApi appointmentClient;

    @Autowired
    private CustomerApi customerClient;

    @Autowired
    private PaymentApi paymentClient;

    @Autowired
    private ProfileApi profileClient;

    @Autowired
    private DocumentProcessor documentProcessor;

    @Autowired
    private TestConsumerCounter testConsumerCounter;

    @Before
    public void init() throws Exception {
        Thread.sleep(100);
        testConsumerCounter.flush();
        reset(appointmentClient, customerClient, paymentClient, documentProcessor);
    }

    @After
    public void tearDown() throws Exception {
        reset(documentProcessor);
    }

    @Test
    public void bookAppointment_should_be_restricted_for_customers_only() throws Exception
    {
        mockMvc.perform(post("/bookings")
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(buildRequestDto(42))))
            .andExpect(status().isUnauthorized());

        mockMvc.perform(clientRequest(() -> post("/bookings"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(buildRequestDto(42))))
            .andExpect(status().isForbidden());
    }

    @Test
    public void bookAppointment_should_report_missing_appointment() throws Exception
    {
        AppointmentDto appointment = new AppointmentDto();
        appointment.setId(42);

        when(appointmentClient.findById(any(), eq(42L)))
            .thenThrow(new EntityNotFoundException("Appointment", 42));

        mockMvc.perform(customerRequest(42L, () -> post("/bookings"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(buildRequestDto(42))))
            .andExpect(status().isNotFound())
            .andExpect(jsonPath("$[0].code", is("NOT_FOUND")))
            .andExpect(jsonPath("$[0].reference", is(42)));
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void bookAppointment_should_return_created_booking() throws Exception
    {
        reset(customerClient, paymentClient);
        CreateBookingDto requestDto = buildRequestDto(42);

        mockMvc.perform(customerRequest(42L, () -> post("/bookings"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(requestDto)))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.id", notNullValue()))
            //.andExpect(jsonPath("$.customerId", is(42)))
            .andExpect(jsonPath("$.status", is(BookingStatus.CREATED.name())))
            .andExpect(jsonPath("$.appointment.id", is(42)))
            .andExpect(jsonPath("$.appointment.availableLanguages[0]", is(Language.GERMAN.name())));

        verify(customerClient, times(1)).createCustomerAsync(any());
        verify(paymentClient, never()).payBooking(any(), any(), anyString(), anyString(), anyString());
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void bookAppointment_should_return_payment_required_if_force_pay_is_activated_and_payment_failed() throws Exception
    {
        reset(customerClient, paymentClient);
        CreateBookingDto requestDto = buildRequestDto(42);
        requestDto.setTreatmentTypeId(TestProfileClient.FORCED_CHARGE_TREATMENT_ID);
        requestDto.setCapture("token");

        doThrow(new RuntimeException("failed")).when(paymentClient)
            .payBooking(any(), any(), any(), anyString(), eq("token"));

        mockMvc.perform(customerRequest(42L, () -> post("/bookings"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(requestDto)))
            .andExpect(status().isPaymentRequired())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$[0].code", is("PAYMENT_REQUIRED")))
            .andExpect(jsonPath("$[0].message", is("failed")));

        verify(customerClient, times(1)).createCustomerAsync(any());
        verify(paymentClient, times(1)).payBooking(any(), any(), anyString(), anyString(), eq("token"));
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void getBooking_should_return_booking_with_details() throws Exception
    {
        reset(customerClient, appointmentClient);
        CreateBookingDto requestDto = buildRequestDto(42);

        long bookingId = objectMapper.readValue(mockMvc.perform(customerRequest(42L, () -> post("/bookings"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(requestDto)))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), BookingDto.class)
            .getId();

        mockMvc.perform(employeeRequest(55L, () -> get("/bookings/" + bookingId)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id", is((int) bookingId)))
            .andExpect(jsonPath("$.appointment.id", is(42)));

        verify(customerClient, times(1)).findByIdAsync(any(), eq(42L));
        verify(appointmentClient, times(2)).findByIdAsync(42L);
        verify(documentProcessor, times(1)).index(eq("bookings"), eq("practice-42"), any());
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void getBooking_should_handle_uuid_as_secret_hash() throws Exception
    {
        reset(customerClient, appointmentClient);
        CreateBookingDto requestDto = buildRequestDto(42);

        BookingDto booking = objectMapper.readValue(mockMvc.perform(customerRequest(42L, () -> post("/bookings"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(requestDto)))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), BookingDto.class);

        mockMvc.perform(employeeRequest(55L, () -> get("/bookings/" + booking.getUuid())))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.uuid", is(booking.getUuid().toString())))
            .andExpect(jsonPath("$.id", is((int) booking.getId())));
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void bookAppointment_should_call_payment_service_for_charged_treatment_types() throws Exception
    {
        reset(customerClient, paymentClient);

        CreateBookingDto requestDto = buildRequestDto(42L);
        requestDto.setTreatmentTypeId(TestProfileClient.CHARGE_TREATMENT_ID);
        requestDto.setCapture("token");

        mockMvc.perform(customerRequest(42L, () -> post("/bookings"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(requestDto)))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.id", notNullValue()))
            .andExpect(jsonPath("$.paid", is(false)))
            .andExpect(jsonPath("$.treatmentTypeId", is(requestDto.getTreatmentTypeId().intValue())))
            .andExpect(jsonPath("$.paymentInitialised", is(true)))
            .andExpect(jsonPath("$.totalPrice.amount", is(TestProfileClient.CHARGED_TREATMENT_COSTS)))
            .andExpect(jsonPath("$.totalPrice.currency", is(Currency.EUR.name())))
            //.andExpect(jsonPath("$.customerId", is(42)))
            .andExpect(jsonPath("$.status", is(BookingStatus.CREATED.name())))
            .andExpect(jsonPath("$.appointment.id", is(42)))
            .andExpect(jsonPath("$.appointment.availableLanguages[0]", is(Language.GERMAN.name())));

        String name = String.format("%s %s", requestDto.getCustomer().getFirstName(), requestDto.getCustomer().getLastName());

        verify(customerClient, times(1)).createCustomerAsync(any());
        verify(paymentClient, times(1)).payBooking(any(), any(), eq(requestDto.getCustomer().getEmail()), eq(name), eq("token"));
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void bookAppointment_should_dispatch_message_about_created_booking() throws Exception
    {
        CreateBookingDto bookingDto = buildRequestDto(42l);
        bookingDto.setPracticeId(42l);

        testConsumerCounter.flush();

        mockMvc.perform(customerRequest(42L, () -> post("/bookings"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(bookingDto)))
            .andExpect(status().isCreated());

        Thread.sleep(200);
        // We expect two confirmation notifications, an SMS and email. Both should send.
        assertEquals(2, testConsumerCounter.getCreatedNotifications());
        // We expect two reminder notifications, 1 SMS and 1 email. All should send.
        assertEquals(2, testConsumerCounter.getReminderNotifications());
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void bookAppointment_should_not_dispatch_message_about_created_booking() throws Exception
    {
        CreateBookingDto bookingDto = buildRequestDto(42l);
        bookingDto.setPracticeId(43l);

        mockMvc.perform(customerRequest(42L, () -> post("/bookings"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(bookingDto)))
            .andExpect(status().isCreated());

        Thread.sleep(100);
        // We expect two confirmation notifications, an SMS and an email, but only the emails should send.
        assertEquals(1, testConsumerCounter.getCreatedNotifications());
        // We expect five reminder notifications, 1 SMS and 1 email, but only the email should send.
        assertEquals(1, testConsumerCounter.getReminderNotifications());
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void bookAppointment_should_send_both_review_reminder_messages() throws Exception
    {
        CreateBookingDto bookingDto = buildRequestDto(42l);
        bookingDto.setPracticeId(42l);

        mockMvc.perform(customerRequest(42L, () -> post("/bookings"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(bookingDto)))
            .andExpect(status().isCreated());

        Thread.sleep(120);
        assertEquals(2, testConsumerCounter.getReviewReminders());
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void bookAppointment_should_send_email_review_reminder_message() throws Exception
    {
        CreateBookingDto bookingDto = buildRequestDto(42l);
        bookingDto.setPracticeId(3l);

        mockMvc.perform(customerRequest(42L, () -> post("/bookings"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(bookingDto)))
            .andExpect(status().isCreated());

        Thread.sleep(120);
        assertEquals(1, testConsumerCounter.getReviewReminders());
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void updateBooking_should_check_for_permissions() throws Exception
    {
        long id = objectMapper.readValue(mockMvc.perform(customerRequest(42L, () -> post("/bookings"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(buildRequestDto(42))))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), BookingDto.class)
            .getId();

        UpdateBookingDto update = new UpdateBookingDto();
        update.setPracticeComment("try to set a comment");

        mockMvc.perform(practiceRequest(99L, 99L, () -> put("/bookings/" + id))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(update)))
            .andExpect(status().isForbidden());

        mockMvc.perform(customerRequest(55L, () -> put("/bookings/" + id))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(update)))
            .andExpect(status().isForbidden());
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void updateBooking_should_reject_comments_for_customers() throws Exception
    {
        long id = objectMapper.readValue(mockMvc.perform(customerRequest(42L, () -> post("/bookings"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(buildRequestDto(42))))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), BookingDto.class)
            .getId();

        UpdateBookingDto update = new UpdateBookingDto();
        update.setPracticeComment("try to set a comment");

        mockMvc.perform(customerRequest(42L, () -> put("/bookings/" + id))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(update)))
            .andExpect(status().isNoContent());

        mockMvc.perform(customerRequest(42L, () -> get("/bookings/" + id)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.practiceComment").doesNotExist());
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void getBooking_should_be_restricted() throws Exception
    {
        long id = objectMapper.readValue(mockMvc.perform(customerRequest(42L, () -> post("/bookings"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(buildRequestDto(42))))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), BookingDto.class)
            .getId();

        mockMvc.perform(customerRequest(42L, () -> get("/bookings/" + id)))
            .andExpect(status().isOk());
        mockMvc.perform(practiceRequest(42L, 99L, () -> get("/bookings/" + id)))
            .andExpect(status().isOk());
        mockMvc.perform(practiceRequest(99L, 42L, () -> get("/bookings/" + id)))
            .andExpect(status().isOk());

        mockMvc.perform(customerRequest(55L, () -> get("/bookings/" + id)))
            .andExpect(status().isForbidden());
        mockMvc.perform(practiceRequest(99L, 55L, () -> get("/bookings/" + id)))
            .andExpect(status().isForbidden());
        mockMvc.perform(clientRequest(() -> get("/bookings/" + id)))
            .andExpect(status().isForbidden());
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void getBooking_should_return_notification_settings() throws Exception
    {
        NotificationSettingsDto settings = new NotificationSettingsDto();
        settings.setEmailRecipient("my@example.com");
        settings.setSmsRecipient("+4917612345678");

        CreateBookingDto request = buildRequestDto(42);
        request.setNotificationSettings(settings);

        long id = objectMapper.readValue(mockMvc.perform(customerRequest(42L, () -> post("/bookings"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), BookingDto.class)
            .getId();

        mockMvc.perform(customerRequest(42L, () -> get("/bookings/" + id)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.notificationSettings.emailRecipient", is("my@example.com")))
            .andExpect(jsonPath("$.notificationSettings.smsRecipient", is("+4917612345678")));
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void updateBooking_should_update_comment_and_should_be_returned_on_get() throws Exception
    {
        long id = objectMapper.readValue(mockMvc.perform(customerRequest(42L, () -> post("/bookings"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(buildRequestDto(42))))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), BookingDto.class)
            .getId();

        UpdateBookingDto update = new UpdateBookingDto();
        update.setPracticeComment("random thoughts");

        mockMvc.perform(practiceRequest(42L, 99L, () -> put("/bookings/" + id))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(update)))
            .andExpect(status().isNoContent());

        mockMvc.perform(practiceRequest(42L, 128L, () -> get("/bookings/practices/42")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.elements[*]", hasSize(1)))
            .andExpect(jsonPath("$.elements[0].practiceComment", is("random thoughts")));
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void cancelBooking_should_be_restricted() throws Exception
    {
        long id = makeBooking();
        CancelBookingDto request = cancelRequest();

        mockMvc.perform(customerRequest(55L, () -> delete("/bookings/" + id))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isForbidden());

        mockMvc.perform(practiceRequest(99L, 55L, () -> delete("/bookings/" + id))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isForbidden());

        mockMvc.perform(clientRequest(() -> delete("/bookings/" + id))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isForbidden());
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void cancelBooking_should_cancel_booking_by_practice() throws Exception
    {
        long id = makeBooking();
        CancelBookingDto request = cancelRequest();

        mockMvc.perform(practiceRequest(42L, 128L, () -> delete("/bookings/" + id))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isNoContent());

        mockMvc.perform(customerRequest(42L, () -> get("/bookings/" + id)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.status", is(BookingStatus.CANCELLED_BY_PRACTICE.name())));
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void cancelBooking_should_cancel_booking_by_employee() throws Exception
    {
        long id = makeBooking();
        CancelBookingDto request = cancelRequest();

        mockMvc.perform(employeeRequest(100L, () -> delete("/bookings/" + id))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isNoContent());

        mockMvc.perform(customerRequest(42L, () -> get("/bookings/" + id)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.status", is(BookingStatus.CANCELLED_BY_EMPLOYEE.name())));
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void cancelBooking_should_cancel_booking_by_patient() throws Exception
    {
        long id = makeBooking();
        CancelBookingDto request = cancelRequest();

        mockMvc.perform(customerRequest(42L, () -> delete("/bookings/" + id))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isNoContent());

        mockMvc.perform(customerRequest(42L, () -> get("/bookings/" + id)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.status", is(BookingStatus.CANCELLED_BY_PATIENT.name())));
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void cancelBooking_should_send_requested_notifications_to_patient() throws Exception
    {
        long id = makeBooking();
        CancelBookingDto request = cancelRequest();
        request.setNotificationTypes(Arrays.asList(NotificationType.EMAIL, NotificationType.SMS));

        Thread.sleep(50);
        testConsumerCounter.flush();

        mockMvc.perform(practiceRequest(42L,66L, () -> delete("/bookings/" + id))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isNoContent());

        mockMvc.perform(customerRequest(42L, () -> get("/bookings/" + id)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.status", is(BookingStatus.CANCELLED_BY_PRACTICE.name())));

        Thread.sleep(100);

        assertEquals(2, testConsumerCounter.getCancellationNotifications().size());
        assertEquals((int) id, testConsumerCounter.getCancellationNotifications().get(0).request.getParams().get("bookingId"));
        assertEquals((int) id, testConsumerCounter.getCancellationNotifications().get(1).request.getParams().get("bookingId"));

        List<String> targets = testConsumerCounter.getCancellationNotifications().stream()
            .map(m -> m.request.getTarget())
            .sorted()
            .collect(Collectors.toList());

        assertEquals(Arrays.asList("lee.white@arzttermine.de", "mobile"), targets);
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void cancelBooking_should_cancel_booking_notifications() throws Exception
    {
        long id = makeBooking();
        CancelBookingDto request = cancelRequest();
        request.setNotificationTypes(Arrays.asList(NotificationType.EMAIL, NotificationType.SMS));

        Thread.sleep(50);
        testConsumerCounter.flush();

        mockMvc.perform(practiceRequest(42L,66L, () -> delete("/bookings/" + id))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isNoContent());

        mockMvc.perform(customerRequest(42L, () -> get("/bookings/" + id)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.status", is(BookingStatus.CANCELLED_BY_PRACTICE.name())));

        Thread.sleep(200);

        assertEquals(1, testConsumerCounter.getCancelMessages());
        assertEquals(2, testConsumerCounter.getNotificationCancellationRequestDtos().size());
        assertThat(testConsumerCounter.getNotificationCancellationRequestDtos().get(0).getTemplate(), anyOf(is(Notification.BOOKING_REMINDER), is(Notification.REVIEW_REMINDER)));
        assertThat(testConsumerCounter.getNotificationCancellationRequestDtos().get(1).getTemplate(), anyOf(is(Notification.BOOKING_REMINDER), is(Notification.REVIEW_REMINDER)));

        List<String> targets = testConsumerCounter.getCancellationNotifications().stream()
            .map(m -> m.request.getTarget())
            .sorted()
            .collect(Collectors.toList());

        assertEquals(Arrays.asList("lee.white@arzttermine.de", "mobile"), targets);
    }

    private CancelBookingDto cancelRequest() {
        CancelBookingDto request = new CancelBookingDto();
        request.setReason(CancelReason.BOOKING_ACCIDENT);
        request.setAppointmentAvailability(true);
        request.setNotificationTypes(Collections.emptyList());
        return request;
    }

    private long makeBooking() throws Exception {
        return objectMapper.readValue(mockMvc.perform(customerRequest(42L, () -> post("/bookings"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(buildRequestDto(42))))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), BookingDto.class)
            .getId();
    }

    public static CreateBookingDto buildRequestDto(long appointmentId)
    {
        final CustomerRequestDto ccd = new CustomerRequestDto();
        ccd.setInsuranceType(InsuranceType.COMPULSORY);
        ccd.setLanguage(Language.GERMAN);
        ccd.setEmail("lee.white@arzttermine.de");
        ccd.setGender(Gender.MALE);
        ccd.setFirstName("test");
        ccd.setLastName("test");
        ccd.setReturning(false);
        ccd.setPhoneMobile("mobile");

        CreateBookingDto request = new CreateBookingDto();
        request.setTreatmentTypeId(TestProfileClient.FREE_TREATMENT_ID);
        request.setNotificationSettings(new NotificationSettingsDto());
        request.setPlatform(BookingPlatform.WEBSITE);
        request.setAppointmentId(appointmentId);
        request.setCustomer(ccd);
        request.setPracticeId(42L);

        return request;
    }

    public static Set<PaidFeatureAssignmentDto> dummyPaidFeatures() {
        PaidFeatureAssignmentDto reviewReminderSms = new PaidFeatureAssignmentDto();
        reviewReminderSms.setPaidFeatureId(5l);
        reviewReminderSms.setName("sms_review_reminder");

        return new HashSet<>(Collections.singletonList(reviewReminderSms));
    }

}
