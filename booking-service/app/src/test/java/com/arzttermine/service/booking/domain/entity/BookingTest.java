package com.arzttermine.service.booking.domain.entity;

import com.arzttermine.library.web.dto.PriceDto;
import com.arzttermine.library.web.struct.Currency;
import com.arzttermine.service.booking.web.struct.BookingStatus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class BookingTest
{
    @Test
    public void setStatus_should_set_active_to_null_when_booking_is_release()
    {
        Booking booking = Booking.builder()
            .appointmentId(42L)
            .customerId(42L)
            .practiceId(42L)
            .treatmentTypeId(1L)
            .build();

        assertFalse(booking.isReleased());
        assertTrue(booking.isActive());

        booking.setStatus(BookingStatus.CANCELLED_BY_PRACTICE);

        assertTrue(booking.isReleased());
        assertFalse(booking.isActive());
    }

    @Test
    public void setStatus_should_set_not_set_active_to_true_when_active_is_already_null()
    {
        Booking booking = Booking.builder()
            .appointmentId(42L)
            .customerId(42L)
            .practiceId(42L)
            .treatmentTypeId(1L)
            .build();

        booking.setStatus(BookingStatus.CANCELLED_BY_PRACTICE);

        assertTrue(booking.isReleased());
        assertFalse(booking.isActive());

        booking.setStatus(BookingStatus.CONFIRMED);

        assertTrue(booking.isReleased());
        assertFalse(booking.isActive());
    }

    @Test
    public void builder_should_set_total_price_and_used_currency()
    {
        Booking booking = Booking.builder()
            .appointmentId(42L)
            .customerId(42L)
            .practiceId(42L)
            .treatmentTypeId(1L)
            .totalPrice(new PriceDto(1000, Currency.EUR))
            .build();

        assertEquals(1000, booking.getTotalPrice().intValue());
        assertEquals(Currency.EUR, booking.getUsedCurrency());
        assertFalse(booking.isPaymentInitialised()); // must be initialised manually
        assertFalse(booking.isPaid());
    }

    @Test
    public void builder_should_set_correct_payment_state()
    {
        Booking booking = Booking.builder()
            .appointmentId(42L)
            .customerId(42L)
            .practiceId(42L)
            .treatmentTypeId(1L)
            .totalPrice(new PriceDto(1000, Currency.EUR))
            .paid(true)
            .build();

        assertEquals(1000, booking.getTotalPrice().intValue());
        assertEquals(Currency.EUR, booking.getUsedCurrency());
        assertTrue(booking.isPaymentInitialised());
        assertTrue(booking.isPaid());
    }
}
