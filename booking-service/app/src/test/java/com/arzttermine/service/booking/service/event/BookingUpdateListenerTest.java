package com.arzttermine.service.booking.service.event;

import com.arzttermine.library.elasticsearch.model.BulkAction;
import com.arzttermine.library.elasticsearch.service.DocumentProcessor;
import com.arzttermine.library.web.struct.Language;
import com.arzttermine.service.booking.web.BaseIntegrationTest;
import com.arzttermine.service.booking.web.controller.BookingControllerTest;
import com.arzttermine.service.booking.web.dto.request.UpdateBookingDto;
import com.arzttermine.service.booking.web.dto.response.BookingDto;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class BookingUpdateListenerTest extends BaseIntegrationTest
{
    @Autowired
    private DocumentProcessor documentProcessor;

    @Test
    @Sql("/db/empty_database.sql")
    public void updateBooking_should_update_bookings_index() throws Exception
    {
        BookingDto booking = objectMapper.readValue(mockMvc.perform(customerRequest(42L, () -> post("/bookings"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(BookingControllerTest.buildRequestDto(42))))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), BookingDto.class);

        assertEquals(Language.GERMAN, booking.getLanguage());

        Thread.sleep(50);
        reset(documentProcessor);

        UpdateBookingDto update = new UpdateBookingDto();
        update.setLanguage(Language.ENGLISH);

        doAnswer(invocation -> {
            BulkAction action = invocation.getArgumentAt(2, BulkAction.class);
            BookingDto dto = (BookingDto) action.getDocument();

            assertEquals(Language.ENGLISH, dto.getLanguage());
            return null;
        }).when(documentProcessor).index(eq("bookings"), eq("practice-42"), any());

        mockMvc.perform(employeeRequest(42L, () -> put("/bookings/" + booking.getId()))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(update)))
            .andExpect(status().isNoContent());

        Thread.sleep(20);
        verify(documentProcessor, times(1))
            .index(eq("bookings"), eq("practice-42"), any());
    }
}
