package com.arzttermine.service.booking.consumer;

import com.arzttermine.library.messaging.consumer.Consumer;
import com.arzttermine.service.booking.messages.AppointmentBookedMessage;
import com.arzttermine.service.booking.messages.BookingConfirmedMessage;
import com.arzttermine.service.notification.messages.booking.BookingCancelledMessage;
import com.arzttermine.service.notification.messages.booking.BookingConfirmationMessage;
import com.arzttermine.service.notification.messages.booking.BookingInvoiceMessage;
import com.arzttermine.service.notification.messages.booking.BookingMovedMessage;
import com.arzttermine.service.notification.messages.booking.BookingReminderMessage;
import com.arzttermine.service.notification.messages.booking.ReviewReminderMessage;
import com.arzttermine.service.notification.messages.notification.CancelNotificationsMessage;
import com.arzttermine.service.notification.messages.notification.NotificationCancellationRequestDto;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

@Component
@Profile("testing")
public class TestConsumerCounter
{
    private final AtomicInteger invoices = new AtomicInteger(0);
    private final AtomicInteger confirmations = new AtomicInteger(0);
    private final AtomicInteger creations = new AtomicInteger(0);
    private final AtomicInteger movedNotifications = new AtomicInteger(0);
    private final AtomicInteger createdNotifications = new AtomicInteger(0);
    private final AtomicInteger reminderNotifications = new AtomicInteger(0);
    private final AtomicInteger reviewReminders = new AtomicInteger(0);
    private final AtomicInteger cancelMessages = new AtomicInteger(0);

    private final AtomicReference<List<BookingCancelledMessage>> cancellationNotifications = new AtomicReference<>(new ArrayList<>());

    private final AtomicReference<List<NotificationCancellationRequestDto>> notificationCancellationDtos = new AtomicReference<>(new ArrayList<>());

    @Consumer
    public void bookingConfirmed(BookingConfirmedMessage message)
    {
        confirmations.incrementAndGet();
    }

    @Consumer
    public void bookingCreated(AppointmentBookedMessage message)
    {
        creations.incrementAndGet();
    }

    @Consumer
    public void bookingInvoiceSent(BookingInvoiceMessage message)
    {
        invoices.incrementAndGet();
    }

    @Consumer
    public void requestedCancellationNotification(BookingCancelledMessage cancelledMessage) {
        cancellationNotifications.get().add(cancelledMessage);
    }

    @Consumer
    public void requestBookingMovedNotification(BookingMovedMessage message)
    {
        movedNotifications.incrementAndGet();
    }

    @Consumer
    public void bookingCreatedNotification(BookingConfirmationMessage message) {
        createdNotifications.incrementAndGet();
    }

    @Consumer
    public void bookingReminderNotification(BookingReminderMessage message) {
        reminderNotifications.incrementAndGet();
    }

    @Consumer
    public void reviewReminderNotification(ReviewReminderMessage message) {
        reviewReminders.incrementAndGet();
    }

    @Consumer
    public void cancelMessages(CancelNotificationsMessage message) {
        cancelMessages.incrementAndGet();
        notificationCancellationDtos.get().addAll(message.getCancellationRequests());
    }

    public void flush() {
        confirmations.set(0);
        creations.set(0);
        movedNotifications.set(0);
        createdNotifications.set(0);
        reminderNotifications.set(0);
        invoices.set(0);
        reviewReminders.set(0);
        cancelMessages.set(0);
        cancellationNotifications.get().clear();
        notificationCancellationDtos.get().clear();
    }

    public int getInvoices() {
        return invoices.intValue();
    }

    public List<BookingCancelledMessage> getCancellationNotifications() {
        return cancellationNotifications.get();
    }

    public int getConfirmations() {
        return confirmations.intValue();
    }

    public int getCreations() {
        return creations.intValue();
    }

    public int getMovedNotifications() {
        return movedNotifications.intValue();
    }

    public int getCreatedNotifications() {
        return createdNotifications.intValue();
    }

    public int getReminderNotifications() {
        return reminderNotifications.intValue();
    }

    public int getReviewReminders() {
        return reviewReminders.intValue();
    }

    public int getCancelMessages() {
        return cancelMessages.intValue();
    }

    public List<NotificationCancellationRequestDto> getNotificationCancellationRequestDtos() {
        return notificationCancellationDtos.get();
    }
}
