package com.arzttermine.service.booking.externals.test;

import com.arzttermine.library.web.dto.PriceDto;
import com.arzttermine.service.booking.service.externals.ProfileApi;
import com.arzttermine.service.profile.web.dto.response.PaidFeatureAssignmentDto;
import com.arzttermine.service.profile.web.dto.response.ReviewSiteDto;
import com.arzttermine.service.profile.web.dto.response.TreatmentTypeDto;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
@Profile("testing")
public class TestProfileClient implements ProfileApi
{
    public static final long FREE_TREATMENT_ID = 42L;
    public static final long CHARGE_TREATMENT_ID = 66L;
    public static final long FORCED_CHARGE_TREATMENT_ID = 68L;
    public static final int CHARGED_TREATMENT_COSTS = 10000;

    @Override
    public TreatmentTypeDto findTreatmentTypeById(long id, long practiceId) {
        TreatmentTypeDto tt = new TreatmentTypeDto();
        tt.setType((id == FREE_TREATMENT_ID) ? "free" : "charged");
        tt.setDoctorIds(Collections.singletonList(42L));
        tt.setPracticeId(practiceId);
        tt.setId(id);

        if (id == CHARGE_TREATMENT_ID || id == FORCED_CHARGE_TREATMENT_ID) {
            tt.setCharge(new PriceDto(CHARGED_TREATMENT_COSTS));
            if (id == FORCED_CHARGE_TREATMENT_ID) {
                tt.setForcePay(true);
            }
        }

        return tt;
    }

    @Override
    public CompletableFuture<TreatmentTypeDto> findTreatmentTypeByIdAsync(long id, long practiceId) {
        return CompletableFuture.completedFuture(findTreatmentTypeById(id, practiceId));
    }

    @Override
    public Map<Long, TreatmentTypeDto> findTreatmentTypesByIds(Collection<Long> ids, long practiceId) {
        return ids.stream().collect(Collectors.toMap(Function.identity(), e -> findTreatmentTypeById(e,practiceId)));
    }

    @Override
    public CompletableFuture<Map<Long, TreatmentTypeDto>> findTreatmentTypesByIdsAsync(Collection<Long> ids, long practiceId) {
        return CompletableFuture.completedFuture(findTreatmentTypesByIds(ids, practiceId));
    }

    @Override
    public Set<PaidFeatureAssignmentDto> findPaidFeaturesByPracticeId(long practiceId) {
        Set<PaidFeatureAssignmentDto> paidFeatures = new HashSet<>();

        if (practiceId == 42l) {
            PaidFeatureAssignmentDto paidFeature = new PaidFeatureAssignmentDto();
            paidFeature.setPaidFeatureId(1l);
            paidFeature.setName("sms_booking_confirmation");
            paidFeatures.add( paidFeature);
            paidFeature = new PaidFeatureAssignmentDto();
            paidFeature.setPaidFeatureId(2l);
            paidFeature.setName("sms_booking_reminder");
            paidFeatures.add(paidFeature);
            paidFeature = new PaidFeatureAssignmentDto();
            paidFeature.setPaidFeatureId(5l);
            paidFeature.setName("sms_review_reminder");
            paidFeatures.add(paidFeature);
        }
        else {
            PaidFeatureAssignmentDto paidFeature = new PaidFeatureAssignmentDto();
            paidFeature.setPaidFeatureId(3l);
            paidFeature.setName("online_payments");
            paidFeatures.add(paidFeature);
        }

        return paidFeatures;
    }

    @Override
    public ReviewSiteDto findReviewSiteByPracticeId(long practiceId) {
        ReviewSiteDto reviewSiteDto = new ReviewSiteDto();

        if (practiceId == 42l) {
            reviewSiteDto.setData("https://www.jameda.de/berlin/zahnaerzte/silke-kuehn/bewerten/81216972_1/");
            reviewSiteDto.setType("jameda");
        }
        else {
            reviewSiteDto.setData("ChIJPcJ7UV9OqEcRzd6F6oIxwoI");
            reviewSiteDto.setType("google");
        }

        return reviewSiteDto;
    }

}
