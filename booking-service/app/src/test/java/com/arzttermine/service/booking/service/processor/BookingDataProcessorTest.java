package com.arzttermine.service.booking.service.processor;

import com.arzttermine.service.booking.domain.entity.Booking;
import com.arzttermine.service.booking.service.externals.AppointmentApi;
import com.arzttermine.service.booking.service.externals.CustomerApi;
import com.arzttermine.service.booking.service.externals.ProfileApi;
import com.arzttermine.service.booking.web.struct.BookingStatus;
import com.arzttermine.service.calendar.web.dto.response.AppointmentDto;
import com.arzttermine.service.profile.web.dto.response.CustomerDto;
import com.arzttermine.service.profile.web.dto.response.TreatmentTypeDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class BookingDataProcessorTest
{

    @Mock
    private CustomerApi customerApi;

    @Mock
    private AppointmentApi appointmentApi;

    @Mock
    private ProfileApi profileApi;

    @InjectMocks
    private BookingDataProcessor dataProcessor;

    @Test
    public void process_should_fetch_booking_details()
    {

        CustomerDto customer1 = new CustomerDto();
        customer1.setId(1l);
        Map<Long, CustomerDto> customerMap = new HashMap<>();
        customerMap.put(1l,customer1);

        TreatmentTypeDto treatment1 = new TreatmentTypeDto();
        treatment1.setId(1l);
        Map<Long, TreatmentTypeDto> treatmentMap = new HashMap<>();
        treatmentMap.put(1l,treatment1);

        AppointmentDto appointment1 = new AppointmentDto();
        AppointmentDto appointment2 = new AppointmentDto();
        appointment1.setId(1l);
        appointment2.setId(2l);
        Map<Long, AppointmentDto> appointmentMap = new HashMap<>();
        appointmentMap.put(1l,appointment1);
        appointmentMap.put(2l,appointment2);

        when(customerApi.findByIdsAsync(customerMap.keySet())).thenReturn(CompletableFuture.supplyAsync(() -> customerMap));
        when(appointmentApi.findByIdsAsync(appointmentMap.keySet())).thenReturn(CompletableFuture.supplyAsync(() -> appointmentMap));
        when(profileApi.findTreatmentTypesByIdsAsync(treatmentMap.keySet(),42l)).thenReturn(CompletableFuture.supplyAsync(() -> treatmentMap));

        Booking booking1 = createBooking(1,  customer1.getId(), appointment1.getId(), treatment1.getId(), BookingStatus.CANCELLED_BY_PATIENT);
        Booking booking2 = createBooking(2,  customer1.getId(), appointment2.getId(), treatment1.getId(), BookingStatus.CONFIRMED);
        Collection<Booking> bookingBatch = Arrays.asList(booking1, booking2);

        dataProcessor.process(bookingBatch, 42l);

        assertEquals(customer1, booking1.getCustomer());
        assertEquals(appointment1, booking1.getAppointment());
        assertEquals(treatment1, booking1.getTreatmentType());

        assertEquals(customer1, booking2.getCustomer());
        assertEquals(appointment2, booking2.getAppointment());
        assertEquals(treatment1, booking2.getTreatmentType());

        verify(customerApi,times(1)).findByIdsAsync(eq(customerMap.keySet()));
        verify(appointmentApi,times(1)).findByIdsAsync(eq(appointmentMap.keySet()));
        verify(profileApi,times(1)).findTreatmentTypesByIdsAsync(eq(treatmentMap.keySet()),eq(42l));

    }

    private Booking createBooking(long id, long customerId, long appointmentId, long treatmentTypeId, BookingStatus status)
    {
        Booking spy = spy(Booking.class);
        when(spy.getId()).thenReturn(id);
        when(spy.getPracticeId()).thenReturn(42l);
        when(spy.getCustomerId()).thenReturn(customerId);
        when(spy.getAppointmentId()).thenReturn(appointmentId);
        when(spy.getTreatmentTypeId()).thenReturn(treatmentTypeId);
        spy.setStatus(status);

        return spy;
    }
}
