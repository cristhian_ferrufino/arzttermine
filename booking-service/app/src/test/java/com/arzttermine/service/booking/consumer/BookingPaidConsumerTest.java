package com.arzttermine.service.booking.consumer;

import com.arzttermine.library.messaging.processor.MessageBus;
import com.arzttermine.library.web.dto.PriceDto;
import com.arzttermine.service.booking.web.BaseIntegrationTest;
import com.arzttermine.service.booking.web.controller.BookingControllerTest;
import com.arzttermine.service.booking.web.dto.request.CreateBookingDto;
import com.arzttermine.service.booking.web.dto.response.BookingDto;
import com.arzttermine.service.payment.messages.TransactionPaidMessage;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;

import java.time.Instant;
import java.util.Date;
import java.util.HashMap;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class BookingPaidConsumerTest extends BaseIntegrationTest
{
    @Autowired
    private MessageBus messageBus;

    @Autowired
    private TestConsumerCounter consumerCounter;

    @Test
    @Sql("/db/empty_database.sql")
    public void bookingPaid_should_mark_booking_as_paid() throws Exception
    {
        CreateBookingDto requestDto = BookingControllerTest.buildRequestDto(42);
        long bookingId = objectMapper.readValue(mockMvc.perform(customerRequest(42L, () -> post("/bookings"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(requestDto)))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), BookingDto.class)
            .getId();

        TransactionPaidMessage message = new TransactionPaidMessage();
        message.setCreatedAt(Date.from(Instant.now()));
        message.setPaidAt(message.getCreatedAt());
        message.setBookingId(bookingId);

        messageBus.send(message);
        Thread.sleep(100);

        mockMvc.perform(employeeRequest(23L, () -> get("/bookings/appointments/42")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.[*]", hasSize(1)))
            .andExpect(jsonPath("$.[0].paid", is(true)));
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void bookingPaid_should_dispatch_invoice_notification() throws Exception
    {
        CreateBookingDto requestDto = BookingControllerTest.buildRequestDto(42);

        long bookingId = objectMapper.readValue(mockMvc.perform(customerRequest(42L, () -> post("/bookings"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(requestDto)))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), BookingDto.class)
            .getId();

        TransactionPaidMessage message = new TransactionPaidMessage();
        message.setBookingId(bookingId);
        message.setPaidAt(Date.from(Instant.now()));
        message.setCreatedAt(message.getPaidAt());
        message.setTotal(new PriceDto(1000));
        message.setPaymentDetails(new HashMap<>());

        consumerCounter.flush();
        messageBus.send(message);

        Thread.sleep(200);

        Assert.assertEquals(1, consumerCounter.getInvoices());
    }
}
