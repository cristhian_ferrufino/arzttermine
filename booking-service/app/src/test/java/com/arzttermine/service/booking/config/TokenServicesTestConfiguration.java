package com.arzttermine.service.booking.config;

import com.arzttermine.service.profile.security.service.TokenCache;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;

import static org.mockito.Mockito.*;

@Configuration
@Profile("testing")
public class TokenServicesTestConfiguration extends ResourceServerConfiguration
{
    @Bean
    public RemoteTokenServices remoteTokenServices()
    {
        return mock(RemoteTokenServices.class);
    }

    @Bean
    public TokenCache clientTokenCache()
    {
        return mock(TokenCache.class);
    }
}
