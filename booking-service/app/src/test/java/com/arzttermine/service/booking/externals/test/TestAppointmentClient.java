package com.arzttermine.service.booking.externals.test;

import com.arzttermine.library.web.struct.InsuranceType;
import com.arzttermine.library.web.struct.Language;
import com.arzttermine.service.booking.service.externals.AppointmentApi;
import com.arzttermine.service.calendar.web.dto.response.AppointmentDto;
import com.arzttermine.service.calendar.web.dto.response.AppointmentLocation;
import com.arzttermine.service.profile.web.dto.LocationDto;
import com.arzttermine.service.profile.web.dto.response.DoctorProfileDto;

import java.time.Instant;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TestAppointmentClient implements AppointmentApi
{
    private static final Map<Long, AppointmentDto> appointments = new HashMap<>();
    private final AppointmentDto defaultAppointment;

    public TestAppointmentClient() {
        AppointmentLocation location = new AppointmentLocation();
        location.setLocation(new LocationDto());
        location.setName("awesome practice");
        location.setId(42L);

        defaultAppointment = new AppointmentDto();
        defaultAppointment.setId(42L);
        defaultAppointment.setDuration(60);
        defaultAppointment.setBooked(false);
        defaultAppointment.setPractice(location);
        defaultAppointment.setDoctor(createDoctor());
        defaultAppointment.setAvailableLanguages(Arrays.asList(Language.GERMAN, Language.ENGLISH));
        defaultAppointment.setAvailableInsuranceTypes(Arrays.asList(InsuranceType.COMPULSORY, InsuranceType.PRIVATE));
        defaultAppointment.setAvailableTreatmentTypes(Arrays.asList(TestProfileClient.FREE_TREATMENT_ID, TestProfileClient.CHARGE_TREATMENT_ID, TestProfileClient.FORCED_CHARGE_TREATMENT_ID));
        defaultAppointment.setStart(Date.from(Instant.now().plusSeconds(60 * 11520)));
        defaultAppointment.setEnd(Date.from(Instant.now().plusSeconds(60 * 11580)));
        appointments.put(42L, defaultAppointment);
    }

    private DoctorProfileDto createDoctor() {
        DoctorProfileDto doctor = new DoctorProfileDto();
        doctor.setEmail("kevorkian@killkillkill.com");
        doctor.setTitle("Dr");
        doctor.setFirstName("Jack");
        doctor.setLastName("Kevorkian");
        return doctor;
    }

    public static void appointment(long id, AppointmentDto appointment) {
        appointments.put(id, appointment);
    }

    @Override
    public AppointmentDto findById(UUID token, long id) {
        return appointments.getOrDefault(id, defaultAppointment);
    }

    @Override
    public CompletableFuture<AppointmentDto> findByIdAsync(long id) {
        return CompletableFuture.supplyAsync(() -> findById(UUID.randomUUID(), id));
    }

    @Override
    public Map<Long, AppointmentDto> findByIds(UUID token, Collection<Long> ids) {
        return appointments.entrySet().stream()
            .filter(e -> ids.contains(e.getKey()))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    @Override
    public CompletableFuture<Map<Long, AppointmentDto>> findByIdsAsync(Collection<Long> ids) {
        Map<Long,AppointmentDto> map = ids.stream().collect(Collectors.toMap(Function.identity(), e -> findById(UUID.randomUUID(), e)));
        return CompletableFuture.supplyAsync(() -> map);
    }
}
