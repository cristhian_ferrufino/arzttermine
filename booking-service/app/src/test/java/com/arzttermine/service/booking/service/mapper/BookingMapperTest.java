package com.arzttermine.service.booking.service.mapper;

import com.arzttermine.library.web.dto.PriceDto;
import com.arzttermine.library.web.struct.Currency;
import com.arzttermine.service.booking.domain.entity.Booking;
import com.arzttermine.service.booking.web.dto.response.BookingDto;
import com.arzttermine.service.calendar.web.dto.response.AppointmentDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class BookingMapperTest
{
    @InjectMocks
    private BookingMapper bookingMapper;

    private Booking booking = Booking.builder().appointmentId(42L)
        .practiceId(42L).customerId(42L).treatmentTypeId(1L)
        .totalPrice(new PriceDto(1000, Currency.EUR)).build();

    @Test
    public void toDto_should_not_add_practice_comment_for_unauthorized_users()
    {
        booking.setPracticeComment("auf das schlimmste vorbereiten");

        BookingDto result = bookingMapper.toDto(booking, new AppointmentDto(), false);

        assertNull(result.getPracticeComment());
    }

    @Test
    public void toDto_should_add_practice_comment_for_granted_users()
    {
        booking.setPracticeComment("auf das schlimmste vorbereiten");

        BookingDto result = bookingMapper.toDto(booking, new AppointmentDto(), true);

        assertEquals("auf das schlimmste vorbereiten", result.getPracticeComment());
    }

    @Test
    public void toDto_should_add_total_price_dto()
    {
        BookingDto result = bookingMapper.toDto(booking, new AppointmentDto(), false);

        assertNotNull(result.getTotalPrice());
        assertEquals(1000, result.getTotalPrice().getAmount());
        assertEquals(Currency.EUR, result.getTotalPrice().getCurrency());
    }
}
