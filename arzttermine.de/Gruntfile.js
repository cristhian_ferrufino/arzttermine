module.exports = function(grunt) {
  var
    jsRoot              = 'htdocs/js/',
    jsResources         = 'resources/js/',
    jsDestination       = 'htdocs/static/js/',

    scssResources       = 'resources/scss/',
    scssDestination     = 'htdocs/static/scss/',

    cssResources        = 'resources/css/',
    cssDestination      = 'htdocs/static/css/',

    fontsResources      = 'resources/fonts/',
    fontsDestination    = 'htdocs/static/fonts/',

    vendorResources     = 'vendor/',
    vendorDestination   = 'htdocs/static/vendor/',

    bower_components    = 'bower_components/',
    node_modules        = 'node_modules/';

    oldImagesInStatic   = 'htdocs/static/img';

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    compass: {
      'main': {
        options: {
          sassDir: scssDestination,
          cssDir: cssDestination
        }
      },

      'widgets': {
        options: {
          sassDir: 'htdocs/static/widget/css',
          cssDir: 'htdocs/static/widget/css',
          imagesDir: oldImagesInStatic,
          noLineComments: true,
          relativeAssets: true,
          config: 'config.rb'
        }
      },

      'landing-pages': {
        options: {
          sass: 'htdocs/lp',
          css: 'htdocs/lp',
          noLineComments: true,
          importPath: scssDestination,
          require: 'bootstrap-sass'
        }
      }
    },

    sass: {
      'mobile-dev': {
        options: {
          style: 'expanded'
        },
        files: [{
          expand: true,
          cwd: 'resources/scss/mobile',
          src: ['mobile.sass'],
          dest: 'htdocs/css',
          ext: '.css'
        }]
      },
      'mobile-dist': {
        options: {
          style: 'compressed'
        },
        files: [{
          expand: true,
          cwd: 'resources/scss/mobile',
          src: ['mobile.sass'],
          dest: 'htdocs/css',
          ext: '.min.css'
        }]
      },
      'admin': {
        options: {
          style: 'expanded'
        },
        files: [{
          expand: true,
          cwd: scssResources,
          src: ['admin.sass'],
          dest: cssDestination,
          ext: '.css'
        }]
      }
    },

    cssmin: {
      'minify': {
        files: [
          {
            expand: true,
            cwd: cssDestination,
            src: ['*.css', '!*.min.css'],
            dest: cssDestination,
            ext: '.min.css'
          }
        ]
      },
      'widgets': {
        files: [
          {
            src: ['*.css', '!_*.css', '!*.min.css'],
            dest: 'htdocs/static/widget/css',
            expand: true,
            cwd: 'htdocs/static/widget/css',
            ext: '.min.css'
          }
        ]
      },
      admin: {
        options: {
          keepSpecialComments: '*',
          noAdvanced: true, // turn advanced optimizations off until the issue is fixed in clean-css
          report: 'min',
          selectorsMergeMode: 'ie8'
        },
        src: [
          cssDestination + 'admin.css',
          cssResources + 'bootstrap-datetimepicker.css'
        ],
        dest: cssDestination + 'admin.min.css'
      }
    },

    uglify: {
      'dev': {
        options: {
          compress: {
            drop_console: true
          }
        },
        files: [{
          expand: true,
          cwd: jsDestination,
          src: ['*.js', '!*.min.js'],
          dest: jsDestination,
          ext: '.min.js',
          extDot: 'last'
        }]
      }
    },

    concat: {
      'footer-js': {
        options: {
          separator: ";"
        },
        src: [jsDestination + '/jquery.formalize.min.js',
          jsDestination + '/at.min.js',
          jsDestination + '/calendar.min.js',
          jsDestination + '/provider.min.js'],
        dest: jsDestination + '/common.min.js'
      },

      'search-js': {
        options: {
          separator: ";"
        },
        src: [jsDestination + '/search.min.js',
          jsDestination + '/vendor/marker.min.js',
          jsDestination + '/vendor/infobox.min.js',
          jsDestination + '/map.min.js',
          jsDestination + '/vendor/jquery.transit.min.js'],
        dest: jsDestination + '/search-page.min.js'
      },

      'mobile-js': {
        options: {
          separator: ";"
        },
        src: [
          jsResources + 'vendor/twbs/ratchet/ratchet.js',
          jsResources + 'vendor/google/marker.min.js',
          jsResources + 'mobile/modernizr.js',
          jsResources + 'mobile/infobubble.js',
          jsResources + 'mobile/jquery.ui.datepicker.min.js',
          jsResources + 'mobile/mobile.js'
        ],
        dest: jsRoot + 'mobile.js'
      },

      'widgets': {
        options: {
          separator: ";"
        },
        src: ['htdocs/static/old-css/reset-min.css',
          'htdocs/static/old-css/base-min.css',
          'htdocs/static/old-css/fonts-min.css'],
        dest: 'htdocs/static/old-css/widget.concat.css'
      },

      'js-admin': {
        src: [
          node_modules + 'jquery/dist/jquery.min.js',
          vendorResources + 'twbs/bootstrap/dist/js/bootstrap.min.js',
          vendorResources + 'onokumus/metismenu/dist/metisMenu.min.js',
          jsResources + 'vendor/highcharts/highcharts.js',
          jsResources + 'moment.min.js',   // include before bootstrap-datepicker!!!
          jsResources + 'moment-lang-de.min.js',
          jsResources + 'bootstrap-datetimepicker.min.js'
        ],
        dest: jsDestination + 'admin.min.js'
      },
      'css-admin': {
        src: [
          vendorResources + 'onokumus/metismenu/dist/metisMenu.min.css',
          cssDestination + 'admin.min.css',
          cssDestination + 'font-awesome.min.css'
        ],
        dest: cssDestination + 'admin.all.min.css'
      }
    },

    copy: {
      jquery2: {  // mobile needs it there. @refactor mobile
        expand: true,
        flatten: true,
        src: [ node_modules + 'jquery/dist/jquery.js', node_modules + 'jquery/dist/jquery.min.js' ],
        dest: jsRoot
      },
      bower_components: {
        expand: true,
        cwd: bower_components,
        src: [ 'bower-chosen/*' ],
        dest: vendorDestination
      },
      bootstrapfonts: {
        expand: true,
        flatten: true,
        src: 'vendor/twbs/bootstrap/dist/fonts/*',
        dest: fontsDestination
      },
      fontsresources: {
        expand: true,
        flatten: true,
        src: fontsResources + '*',
        dest: fontsDestination
      },
      fontawesomecss: {
        expand: false,
        src: 'vendor/fortawesome/font-awesome/css/font-awesome.min.css',
        dest: cssDestination + 'font-awesome.min.css'
      },
      fontawesomefonts: {
        expand: true,
        flatten: true,
        src: 'vendor/fortawesome/font-awesome/fonts/*',
        dest: fontsDestination
      }
    },

    watch: {
      'scss': {
        files: [scssDestination + '**/*.scss', scssDestination + 'widget/dak/*.scss'],
        tasks: ['compass:main']
      },

      'css': {
        files: [cssDestination + '**/*.css', '!' + cssDestination + '/**/*.min.css'],
        tasks: ['cssmin:minify']
      },

      'js': {
        files: [jsDestination + '/at.js',
          jsDestination + '/map.js',
          jsDestination + '/calendar.js',
          jsDestination + '/search.js'],
        tasks: ['uglify', 'concat']
      },

      'widgets': {
        files: ['htdocs/static/css/reset-min.css',
          'htdocs/static/css/base-min.css',
          'htdocs/static/css/fonts-min.css',
          'htdocs/static/css/widget-main.css',
          'htdocs/static/widget/css/*.scss'],
        tasks: ['compass:widgets',
          'concat:widgets',
          'cssmin:widgets']
      },

      'mobile': {
        files: ['resources/scss/mobile/*.*',
          'resources/js/mobile/*.*'],
        tasks: ['css-mobile-dev','css-mobile-dist','js-mobile-dist']
      },

      'landing': {
        files: ['htdocs/lp/**/*.scss'],
        tasks: ['compass:landing-pages']
      }
    }
  });

  // These plugins provide necessary tasks.
  require('load-grunt-tasks')(grunt, {scope: 'devDependencies'});

  /* MOBILE *********************************************** */
  grunt.registerTask('css-mobile-dev', ['sass:mobile-dev']);
  grunt.registerTask('css-mobile-dist', ['sass:mobile-dist']);
  grunt.registerTask('js-mobile-dist', ['copy:jquery2', 'concat:mobile-js']);

  /* GLOBAL *********************************************** */
  // JS distribution task.
  grunt.registerTask('js', ['js-mobile-dist']);

  // CSS distribution task.
  grunt.registerTask('css', [
    'css-mobile-dev',
    'css-mobile-dist'
  ]);

  // Admin-CSS distribution tasks
  grunt.registerTask('css-admin', [
    'copy:fontawesomefonts',
    'copy:fontawesomecss',
    'sass:admin',
    'cssmin:admin',
    'concat:css-admin'
  ]);

  // Admin-JS distribution task.
  grunt.registerTask('js-admin', [
    'copy:jquery2',
    'copy:bower_components',
    'concat:js-admin'
  ]);

  /* ****************************************************** */
  grunt.registerTask('frontend', ['css', 'js']);
  grunt.registerTask('admin', ['css-admin', 'js-admin']);

  // Full distribution task.
  grunt.registerTask('all', ['frontend', 'admin']);

  grunt.registerTask('default', ['watch']);
};
