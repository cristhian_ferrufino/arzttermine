# Arzttermine.de

### Version
[1.0.0](../../releases/latest)

### Requirements

* node.js (for npm)
* npm
* bower
* Grunt
* Composer
* bundle

### Installation (Server)

- install ubuntu 16.04.1
(optional) - setup german keyboard layout
```sh
sudo dpkg-reconfigure keyboard-configuration
```

- disable cdrom as apt source
```sh
sudo sed -i 's/^deb cdrom/#deb cdrom/g' /etc/apt/sources.list
```

(optional) - install ssh server
```sh
sudo apt-get update
sudo apt-get install openssh-server
```

(optional) - add vimrc
```sh
echo "set nocompatible" > ~/.vimrc
```

(optional) - keychain
```sh
sudo apt-get install keychain
vi ~/.bash_profile
if [ -f ~/.bashrc ]; then . ~/.bashrc; fi
eval `keychain --eval --agents ssh id_rsa`

generate new ssh key and add it to your ssh-agent following this instructions https://help.github.com/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent/
add ssh key to github on https://github.com/settings/keys
```

- install some useful tools
```sh
sudo apt-get install software-properties-common
sudo add-apt-repository ppa:git-core/ppa
sudo apt-get update
sudo apt-get install git
sudo apt-get install unzip
sudo apt-get install curl
```

(optional) - install nfs for fileshare between vm and dev machine for example
```sh
sudo apt-get update
sudo apt-get install nfs-kernel-server
sudo sh -c "echo '/var/www/PhpstormProjects *(rw,async,insecure,all_squash,no_subtree_check,anonuid=1000,anongid=1000)' >> /etc/exports"
sudo mkdir -p /var/www/PhpstormProjects
sudo exportfs -ra
on your mac client add following mount, where "arzttermine" is the nfs server dns and "/var/www/PhpstormProjects" the shared directory on the server
mkdir /Users/simon/PhpstormProjects
sudo mount -t nfs -o soft,timeo=900,retrans=3,vers=3,proto=tcp arzttermine:/var/www/PhpstormProjects /Users/simon/PhpstormProjects
```


- install nginx
```sh
wget -O - http://nginx.org/keys/nginx_signing.key | sudo apt-key add -
echo "deb http://nginx.org/packages/mainline/ubuntu/ xenial nginx" | sudo tee -a /etc/apt/sources.list
echo "deb-src http://nginx.org/packages/mainline/ubuntu/ xenial nginx" | sudo tee -a /etc/apt/sources.list
sudo apt-get update
sudo apt-get install nginx
```

- configure nginx (add vhost)
```sh
sudo vi /etc/nginx/nginx.conf
in http block change/add following settings...
worker_processes  auto;
client_body_timeout 12;
client_header_timeout 12;
keepalive_timeout 15;
send_timeout 10;

gzip on;
gzip_disable "msie6";
gzip_vary on;
gzip_proxied any;
gzip_comp_level 6;
gzip_min_length 256;
gzip_buffers 16 8k;
gzip_http_version 1.1;
gzip_types text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript;

upstream php {
   server unix:/run/php/php7.0-fpm.sock;
}

afterwards configure your vhost...
sudo mkdir /etc/nginx/ssl
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/nginx/ssl/nginx.key -out /etc/nginx/ssl/nginx.crt
sudo vi /etc/nginx/conf.d/arzttermine.conf

#fastcgi_cache_path  /tmp/nginx-cache levels=1:2 keys_zone=img:100m inactive=60m;
#fastcgi_cache_key "$scheme$request_method$host$request_uri";
add_header X-Cache $upstream_cache_status;
add_header X-Frame-Options SAMEORIGIN;
add_header X-Content-Type-Options nosniff;
add_header X-XSS-Protection "1; mode=block";
server {
    listen 80;
    server_name  *.arzttermine arzttermine;
    location / {
        return 301 https://$host$request_uri;
    }
}
server {
    listen       443 ssl http2;
    server_name  *.arzttermine arzttermine;
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers 'EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH';
    ssl_prefer_server_ciphers on;
    ssl_certificate     /etc/nginx/ssl/nginx.crt;
    ssl_certificate_key /etc/nginx/ssl/nginx.key;
    root   /usr/share/nginx/arzttermine/htdocs;
    index  index.php;
    proxy_read_timeout 300;

    access_log  /var/log/nginx/arzttermine.access.log  main;

    rewrite ^/.well-known/caldav /dav/ permanent;

    location ~* ^/rev/(.*) {
      rewrite ^(.*?)/rev/(.*?)/(.*)$ $1/$3;
    }

    location ~* \.(?:jpg|jpeg|gif|png|ico|cur|gz|svg|svgz|mp4|ogg|ogv|webm|htc|css|js)$ {
      expires max;
      access_log off;
      add_header Cache-Control "public";
    }

    location ~* \.(?:ttf|ttc|otf|eot|woff|woff2)$ {
      add_header "Access-Control-Allow-Origin" "*";
      expires max;
      access_log off;
      add_header Cache-Control "public";
    }

    location / {
         try_files $uri /index.php$is_args$args;
    }

    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }

    location ~ ^/.+\.php(/|$) {
        fastcgi_pass php;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
#        fastcgi_cache_key "$scheme$request_method$host$request_uri";
#        fastcgi_cache img;
#        fastcgi_cache_valid any 60m;
#        fastcgi_hide_header Set-Cookie;
    }
}
sudo ln -s /var/www/PhpstormProjects/arzttermine.de /usr/share/nginx/arzttermine
sudo service nginx restart
```

- install memcached
```sh
sudo apt-get install memcached
```

- install php-fpm
```sh
sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
sudo apt-get install php7.0-fpm
```

- configure php
```sh
sudo sed -i 's/^memory_limit.*/memory_limit = -1/g' /etc/php/7.0/fpm/php.ini
sudo sed -i 's/^session.save_handler.*/session.save_handler = memcached/g' /etc/php/7.0/fpm/php.ini
sudo sed -i 's%^;session.save_path.*%session.save_path = "localhost:11211?persistent=1&amp;weight=1&amp;timeout=1&amp;retry_interval=15"%g' /etc/php/7.0/fpm/php.ini
sudo sed -i 's%^;date.timezone.*%date.timezone = Europe/Berlin%g' /etc/php/7.0/fpm/php.ini
```

- configure php-fpm
```sh
sudo vi /etc/php/7.0/fpm/pool.d/www.conf
sudo sed -i 's/^listen.owner.*/listen.owner = nginx/g' /etc/php/7.0/fpm/pool.d/www.conf
sudo sed -i 's/^listen.group.*/listen.group = nginx/g' /etc/php/7.0/fpm/pool.d/www.conf
sudo sed -i 's/^;listen.mode.*/listen.mode = 0660/g' /etc/php/7.0/fpm/pool.d/www.conf
sudo service php7.0-fpm restart
```

- install php extensions
```sh
sudo apt-get install php7.0-mysql
sudo apt-get install php7.0-gd
sudo apt-get install php7.0-intl
sudo apt-get install php7.0-curl
sudo apt-get install php7.0-sqlite3
sudo apt-get install php7.0-xml
sudo apt-get install php7.0-bcmath
sudo apt-get install php7.0-mbstring
sudo apt-get install php7.0-soap
sudo service php7.0-fpm restart
```

- install memcached C extension
```sh
git clone -b php7 --single-branch git://github.com/php-memcached-dev/php-memcached.git
sudo apt-get install pkg-config libmemcached-dev php7.0-dev
cd php-memcached/
phpize
./configure
make
sudo make install
echo "extension=memcached.so" | sudo tee /etc/php/7.0/mods-available/memcached.ini
sudo phpenmod memcached
sudo service php7.0-fpm restart
```

- install xdebug C extension
```sh
git clone git://github.com/xdebug/xdebug.git
cd xdebug/
phpize
./configure --enable-xdebug
make
sudo make install
echo "zend_extension=/usr/lib/php/20151012/xdebug.so" | sudo tee /etc/php/7.0/mods-available/xdebug.ini
sudo vi /etc/php/7.0/mods-available/xdebug.ini
xdebug.remote_enable=1
xdebug.remote_autostart=1
xdebug.remote_connect_back=1
xdebug.idekey=xdebug

sudo phpenmod xdebug
sudo service php7.0-fpm restart
```

- install mysql
```sh
sudo apt-get update
sudo apt-get install mysql-server
vi /etc/mysql/conf.d/mysql.cnf
[mysqld]
sql-mode="STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION"
sudo service mysql restart
```

- install nodejs && npm
```sh
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get install -y nodejs
or
sudo add-apt-repository -y -r ppa:chris-lea/node.js
sudo rm -f /etc/apt/sources.list.d/chris-lea-node_js-*.list
curl -s https://deb.nodesource.com/gpgkey/nodesource.gpg.key | sudo apt-key add -
echo 'deb https://deb.nodesource.com/node_6.x xenial main' | sudo tee -a /etc/apt/sources.list.d/nodesource.list
echo 'deb-src https://deb.nodesource.com/node_6.x xenial main' | sudo tee -a /etc/apt/sources.list.d/nodesource.list
sudo apt-get update
sudo apt-get install nodejs
sudo npm install -g npm@latest
```

- install composer
```sh
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer
```

- (optional) install phpunit
```sh
wget https://phar.phpunit.de/phpunit.phar && chmod +x phpunit.phar && sudo mv phpunit.phar /usr/local/bin/phpunit
```

- install bundler
```sh
sudo apt-get install bundler
```

- clone the arzttermine git repository
```sh
cd /usr/share/nginx/
git clone git@github.com:arzttermine/arzttermine.de.git
sudo chown -R $USER:nginx arzttermine.de/
```

### Installation

- import mysql database www_arzttermine_de
```sh
mysql -u root -p www_arzttermine_de < www_arzttermine_de.20160921010001.sql
```

- install symfony and set config values...
```sh
cd /usr/share/nginx/arzttermine.de
rm vendor
composer install

update db settings in config/config.php
- update value of MYSQL_ADMIN_PASSWORD

update shared path in include/bootstrap.php
- update value of SHARED_PATH

create shared dirs
sudo mkdir -p /var/www/shared
sudo chmod a+w -R /var/www/shared
```

Run:
```sh
cd /usr/share/nginx/arzttermine
bundle install
rm node_modules
npm install --prefix ./
./node_modules/.bin/bower install
./node_modules/.bin/grunt all
```

- in the /etc/hosts file of your local machine set arzttermine to ip of your server

in your browser call...
http://arzttermine/app_dev.php


### Deploy branch "twigtemplates" to staging
```sh
BRANCH=twigtemplates /usr/local/bin/cap staging deploy
```
