<?php
/**
 * @author  Naghashyan Solution, e-mail: info@naghashyan.com
 * @version 1.0
 * Default constants 
 * 
 */
date_default_timezone_set('UTC');

define("HTTP_HOST", isset($_SERVER["HTTP_HOST"]) ? $_SERVER["HTTP_HOST"] : '');

if(strrpos ( HTTP_HOST, "." )){
	define("HTTP_ROOT_HOST", substr(HTTP_HOST, strrpos ( HTTP_HOST, ".", -(strlen(HTTP_HOST) - strrpos ( HTTP_HOST, "." )+1) )));
}else{
	define("HTTP_ROOT_HOST", HTTP_HOST);
}

//---defining document root
define("DOCUMENT_ROOT", $_SERVER["DOCUMENT_ROOT"] . "/../ngs");

//---defining HOMEPAGE ROOT root
$ngsRoot = substr(DOCUMENT_ROOT, 0, strrpos(DOCUMENT_ROOT, "/htdocs"))."/ngs";

define("NGS_ROOT", $ngsRoot);

//---defining config file path
define("CONF_PATH", NGS_ROOT . "/conf");

//---defining classes paths
define("CLASSES_PATH", NGS_ROOT . "/classes");

//---defining framework path
define("FRAMEWORK_PATH", CLASSES_PATH . "/framework");

//---defining data dir path
define("DATA_DIR", NGS_ROOT . "/data");

//---defining temp dir path
define("TEMP_DIR", NGS_ROOT . "/tmp");

//---defining data bin path
define("BIN_DIR", NGS_ROOT . "/bin");

//---defining interface images dir
define("IMG_ROOT_DIR", NGS_ROOT . "/htdocs/img");
define("CSS_ROOT_DIR", NGS_ROOT . "/htdocs/css");

define("CONFIG_INI", CONF_PATH . "/config.ini");

//defining load and action directories
define("LOADS_DIR", "loads");
define("ACTIONS_DIR", "actions");
