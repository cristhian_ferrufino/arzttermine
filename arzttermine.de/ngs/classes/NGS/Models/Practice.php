<?php

namespace NGS\Models;

use Arzttermine\Application\Application;
use NGS\DAL\DTO\UsersDto;
use NGS\Managers\AssetsManager;
use NGS\Managers\LocationsManager;
use NGS\Managers\StatementsManager;
use NGS\Managers\UsersManager;

class Practice {

    /**
     * @var UsersDto[] Users
     */
    protected $members = array();

    /**
     * @var mixed Location (practice)
     */
    protected $location = null;

    /**
     * @var LocationsManager|null
     */
    private $locations_manager = null;

    /**
     * @var UsersManager|null
     */
    private $users_manager = null;

    /**
     * @var StatementsManager|null
     */
    private $statements_manager = null;

    /**
     * @var AssetsManager|null
     */
    private $assets_manager = null;

    /**
     * If provided, will attempt to load a user immediately
     *
     * @param int|null $id_locations
     */
    public function __construct($id_locations = null)
    {
        $this->locations_manager = LocationsManager::getInstance();
        $this->users_manager = UsersManager::getInstance();
        $this->statements_manager = StatementsManager::getInstance();
        $this->assets_manager = AssetsManager::getInstance();

        if (!empty($id_locations)) {
            $this->load($id_locations);
        }
    }

    /**
     * Load a location, then all members attached to it
     *
     * @param int $id_locations
     */
    public function load($id_locations)
    {
        if (intval($id_locations)) {
            if ($this->location = $this->locations_manager->getById($id_locations)) {
                $this->populate();
            }
        }
    }

    /**
     * Populate the currently loaded location with its members
     */
    private function populate()
    {
        if ($this->location) {
            $member_ids = $this->location->getMemberIds();

            foreach ((array)explode(',', $member_ids) as $id_users) {
                $this->members[] = $this->users_manager->getUserById(intval(trim($id_users)));
            }
        }
    }

    /**
     * Get a location's members
     *
     * @return UsersDto[]
     */
    public function getMembers()
    {
        return $this->members;
    }

    /**
     * Attempt to load a location from a slug. If it fails, uses the fallback
     *
     * @param int $fallback An ID to use if there is no slug
     */
    public function autoload($fallback = null)
    {
        $request = Application::getInstance()->getRequest();

        if ($praxis_slug = $request->getQuery('praxis')) {
            $this->loadBySlug($praxis_slug);
        }

        if (!$this->location && intval($fallback)) {
            $this->load($fallback);
        }
    }

    /**
     * Load the location by the provided slug
     *
     * @param string $slug
     */
    public function loadBySlug($slug)
    {
        if (!empty($slug)) {
            if ($this->location = $this->locations_manager->getBySlug($slug)) {
                $this->populate();
            }
        }
    }

    /**
     * An editor would most likely be a doctor
     *
     * @param int $editor The user id of the editor
     *
     * @return bool
     */
    public function isEditAllowed($editor)
    {
        $permission = false;

        if ($this->members) {
            foreach ($this->members as $member) {
                if ($member instanceof UsersDto && $member->getId() == $editor) {
                    $permission = true;
                    break;
                }
            }
        }

        return $permission;
    }

    /**
     * Get all statements for the current practice
     *
     * @param string $period
     *
     * @return array
     */
    public function getStatements($period = null)
    {
        if (!$period) {
            $period = date('Y-M');
        }

        return $this->statements_manager->getStatements($this->getLocation()->getId(), $period);
    }

    /**
     * Get the practice location
     *
     * @return mixed
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Get the profile picture for this location
     *
     * @return string
     */
    public function getProfile()
    {
        $url = '';

        if (isset($this->assets_manager)) {

            if ($profileAssetDto = $this->assets_manager->getProfileAsset($this->location->getId())) {
                $profileChildAssetsDtos = $this->assets_manager->getProfileChildAssets($profileAssetDto->getId());
                /**
                 * @var AssetsDto[] $profileChildAssetsDtos
                 */
                if ($profileChildAssetsDtos && $profileChildAssetsDtos[ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED]) {
                    $childAssetDto = $profileChildAssetsDtos[ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED];
                    $_id = sprintf("%09d", $childAssetDto->getParentId());
                    $path = substr($_id, 0, 3) . '/' . substr($_id, 3, 3) . '/' . substr($_id, 6, 3);
                    $profile_image_url = $GLOBALS["CONFIG"]["ASSET_URL"] . $path . '/' . $childAssetDto->getFileName();
                } else {
                    $_id = sprintf("%09d", $profileAssetDto->getId());
                    $path = substr($_id, 0, 3) . '/' . substr($_id, 3, 3) . '/' . substr($_id, 6, 3);
                    $profile_image_url = $GLOBALS["CONFIG"]["ASSET_URL"] . $path . '/' . $profileAssetDto->getFileName();
                }

                $url = $profile_image_url;
            }
        }

        return $url;
    }

    /**
     * Update a location by ID
     *
     * @param array $data As key/value = column/value pairs
     *
     * @return int Affected rows
     */
    public function update(array $data)
    {
        // As some fields are currently prohibited, just save what we're allowed to
        $available_fields = array(
            'phone',
            'www',
            'medical_specialty_ids'
        );

        return $this->locations_manager->updateLocation(intval($this->getLocation()->getId()), $available_fields, $data);
    }

    /**
     * Update/create a statement for the given time period
     *
     * @param string $period
     * @param int $statement_status
     *
     * @return int
     */
    public function updateStatement($period, $statement_status = 0)
    {
        if ($this->statements_manager->getStatements($this->getLocation()->getId(), $period)) {
            $status = $this->statements_manager->updateStatement($this->getLocation()->getId(), $period, intval($statement_status));
        } else {
            $statement = $this->statements_manager->createDto();
            $statement->setStatus($statement_status);
            $statement->setLocationId($this->getLocation()->getId());
            $statement->setPeriod($period);

            $status = $this->statements_manager->addStatement($statement);
        }

        return $status;
    }

    /**
     * Update the medical specialties for this location
     *
     * @param array $data
     *
     * @return int
     */
    public function updateMedicalSpecialties(array $data)
    {
        $data = filter_var_array($data, FILTER_SANITIZE_NUMBER_INT);

        return $this->locations_manager->setMedicalSpecialities($this->getLocation()->getId(), $data);
    }
}
