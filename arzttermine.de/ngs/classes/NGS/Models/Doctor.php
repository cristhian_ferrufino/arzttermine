<?php

namespace NGS\Models;

use Arzttermine\Application\Application;
use NGS\DAL\DTO\AssetsDto;
use NGS\DAL\DTO\LocationsDto;
use NGS\DAL\DTO\UsersDto;
use NGS\Managers\AssetsManager;
use NGS\Managers\LocationsManager;
use NGS\Managers\UsersManager;

class Doctor {

	/**
	 * @var UsersDto
	 */
	protected $doctor = null;
	
	/**
	 * @var LocationsDto[] Locations
	 */
	protected $locations = array();

	/**
	 * @var LocationsManager|null
	 */
	private $locations_manager = null;

	/**
	 * @var UsersManager|null
	 */
	private $users_manager = null;

	/**
	 * @var AssetsManager|null
	 */
	private $assets_manager = null;

	/**
	 * If provided, will attempt to load a user immediately
	 *
	 * @param int|null $id_users
	 */
	public function __construct($id_users = null) {
		$this->locations_manager = LocationsManager::getInstance();
		$this->users_manager     = UsersManager::getInstance();
		$this->assets_manager    = AssetsManager::getInstance();

		if (!empty($id_users)) {
			$this->load($id_users);
		}
	}

	/**
	 * Get a doctor's locations
	 * 
	 * @return LocationsDto[]
	 */
	public function getLocations() {
		return (array)$this->locations;
	}

	/**
	 * Get the doctor
	 *
	 * @return UsersDto
	 */
	public function getDoctor() {
		return $this->doctor;
	}

	/**
	 * Attempt to load a doctor from a slug. If it fails, uses the fallback
	 * 
	 * @param int $fallback An ID to use if there is no slug
	 */
	public function autoload($fallback = null) {
        $request = Application::getInstance()->getRequest();
        
		if ($doctor_slug = $request->getQuery('arzt')) {
			$this->loadBySlug($doctor_slug);
		}
		
		if (!$this->doctor && intval($fallback)) {
			$this->load($fallback);
		}
	}
	
	/**
	 * Load a location, then all members attached to it
	 *
	 * @param int $id_users
	 */
	public function load($id_users) {
		if (intval($id_users)) {
			if ($this->doctor = $this->users_manager->getUserById($id_users)) {
				$this->populate();
			}
		}
	}

	/**
	 * Load the location by the provided slug
	 *
	 * @param string $slug
	 */
	public function loadBySlug($slug) {
		if (!empty($slug)) {
			if ($this->doctor = $this->users_manager->getUserBySlug($slug)) {
				$this->populate();
			}
		}
	}

	/**
	 * Populate the currently loaded location with its members
	 */
	private function populate() {
		if ($this->doctor) {
			$this->locations = $this->locations_manager->getLocationsByUserId($this->doctor->getId());
		}
	}

	/**
	 * An editor would most likely be a doctor
	 *
	 * @param int $editor The user id of the editor
	 *
	 * @return bool
	 */
	public function isEditAllowed($editor) {
		$permission = false;
		
		if ($this->doctor) {
			if (intval($editor) == $this->doctor->getId()) {
				$permission = true;
			} elseif ($this->locations) {
				$editor_locations = $this->locations_manager->getLocationsByUserId($editor);
				$members = array();

				foreach ($editor_locations as $editor_location) {
					$members = array_merge($members, explode(',', $editor_location->getMemberIds()));
				}
				
				array_unique($members);
				
				if (in_array($this->getDoctor()->getId(), $members)) {
					$permission = true;
				}
			}
		}

		return $permission;
	}

	/**
	 * Load and generate the profile pic URL for this doctor
	 * 
	 * @return string
	 */
	public function getProfile() {
		$url = '';

		/**
		 * @var AssetsDto $childAssetDto
		 */
		if ($profileAssetDto = $this->assets_manager->getProfileAsset($this->getDoctor()->getId())) {
			$profileChildAssetsDtos = $this->assets_manager->getProfileChildAssets($profileAssetDto->getId());
            
			if ($profileChildAssetsDtos && $profileChildAssetsDtos[ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED]) {
				$childAssetDto = $profileChildAssetsDtos[ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED];
				$_id = sprintf("%09d", $childAssetDto->getParentId());
				$path = substr($_id, 0, 3) . '/' . substr($_id, 3, 3) . '/' . substr($_id, 6, 3);
				$profile_image_url = $GLOBALS["CONFIG"]["ASSET_URL"] . $path . '/' . $childAssetDto->getFileName();
			} else {
				$_id = sprintf("%09d", $profileAssetDto->getId());
				$path = substr($_id, 0, 3) . '/' . substr($_id, 3, 3) . '/' . substr($_id, 6, 3);
				$profile_image_url = $GLOBALS["CONFIG"]["ASSET_URL"] . $path . '/' . $profileAssetDto->getFileName();
			}

			$url = $profile_image_url;
		}

		return $url;
	}

	/**
	 * Update a doctor's profile
	 * 
	 * @param int $id_users
	 * @param array $data
	 *
	 * @return int
	 */
	public function update(array $data) {
		return $this->users_manager->update(intval($this->getDoctor()->getId()), $data);
	}

	/**
	 * Update the medical specialties for this doctor
	 * 
	 * @param array $data
	 *
	 * @return int
	 */
	public function updateMedicalSpecialties(array $data) {
		$data = filter_var_array($data, FILTER_SANITIZE_NUMBER_INT);
		
		return $this->users_manager->setMedicalSpecialities($this->getDoctor()->getId(), $data);
	}
}
