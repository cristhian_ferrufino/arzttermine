<?php

namespace NGS\Actions;

use Arzttermine\Mail\Mailing;
use NGS\DAL\DTO\UsersDto;
use NGS\Framework\AbstractAction;

abstract class BaseAction extends AbstractAction {

    /**
     * Very bad method to "secure" strings
     *
     * @deprecated
     *
     * @param mixed $var
     *
     * @return null|string
     */
    public function secure($var)
    {
        if (isset($var)) {
            return trim(addslashes(htmlspecialchars(strip_tags($var))));
        } else {
            return null;
        }
    }

    /**
     * @deprecated Use the global Profile from Application
     * 
     * @return UsersDto
     */
    public function getSessionUser()
    {
        return $this->sessionManager->getUser();
    }

    /**
     * A minimum password requirements check
     *
     * @deprecated
     *
     * @param string $password
     *
     * @return bool
     */
    public function checkPassword($password)
    {
        return (strlen($password) >= 8 && preg_match('@[A-Za-z]@', $password) && preg_match('@[0-9]@', $password));
    }

    /**
     * @param UsersDto $doctor
     */
    protected function sendProfileEditEmail(UsersDto $doctor)
    {
        $templateFilename = '/mailing/doctors/doctor-profile-edit.txt';
        $_mailing = new Mailing();
        $_mailing->type = MAILING_TYPE_TEXT;
        $_mailing->content_filename = $templateFilename;
        $_mailing->data['domain'] = $GLOBALS['CONFIG']['DOMAIN'];
        $_mailing->data['doctor_url'] = '';
        $_mailing->data['doctor_name'] = $doctor->getDoctorFullName();
        $_mailing->data['time'] = $doctor->getUpdatedAt();
        $_mailing->addAddressTo($GLOBALS["CONFIG"]["PROFILE_EDIT_EMAIL_ADDRESS"]);
        $_mailing->send();
    }
}