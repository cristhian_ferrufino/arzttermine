<?php

namespace NGS\Actions\Doctors;

use Arzttermine\Mail\Mailing;
use NGS\Actions\BaseAction;
use NGS\Managers\TempUsersDataManager;
use NGS\Managers\UsersManager;
use NGS\Security\RequestGroups;

/**
 * @author Vahagn Kirakosyan
 */
class RegisterDoctorAction extends BaseAction {

    private $usersManager = null;

    private $tempUsersDataManager = null;

    public function service()
    {

        $this->usersManager = UsersManager::getInstance($this->config, $this->args);
        $this->tempUsersDataManager = TempUsersDataManager::getInstance($this->config, $this->args);

        if (isset($_REQUEST["step"])) {
            $step = $_REQUEST["step"];
        }
        if (!isset($step) || !is_numeric($step)) {
            $step = 1;
        }

        switch ($step) {
            case '1':
                $userDatasArray = $this->checkAllFields($step);
                $_SESSION['userDatas'] = $userDatasArray;
                $tempUserDataDto = $this->setUserRequestParametersInDto($userDatasArray);
                $activation_code = substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 6);
                $tempUserDataDto->setActivationCode($activation_code);
                $id = $this->tempUsersDataManager->insertDto($tempUserDataDto);
                $_REQUEST["id"] = $id;
                $this->sendInfoEmail($tempUserDataDto);
                $this->sendRequestConfirmationEmail($tempUserDataDto);
                break;
            case '2':
                $this->checkAllFields($step);
                $retrievedTempUserDataDto = $this->tempUsersDataManager->getById($_REQUEST["id"]);

                $retrievedTempUserDataDto->setMedicalSpecialtyIds($this->secure($_REQUEST['medical_specialty_id']));
                $retrievedTempUserDataDto->setLocationCity($this->secure($_REQUEST['city']));
                $retrievedTempUserDataDto->setLocationName($this->secure($_REQUEST['practice_title']));
                $retrievedTempUserDataDto->setPhoneMobile($this->secure($_REQUEST['phone_mobile']));

                $this->tempUsersDataManager->updateByPK($retrievedTempUserDataDto);
                break;
            case '3':
                $this->checkAllFields($step);
                $retrievedTempUserDataDto = $this->tempUsersDataManager->getById($_REQUEST["id"]);

                $retrievedTempUserDataDto->setRegistrationReasons($this->secure($_REQUEST['registration_reasons']));
                $retrievedTempUserDataDto->setRegistrationFunnel($this->secure($_REQUEST['registration_funnel']));
                $retrievedTempUserDataDto->setRegistrationFunnelOther($this->secure($_REQUEST['registration_funnel_other']));

                $this->tempUsersDataManager->updateByPK($retrievedTempUserDataDto);
                break;
        }
        $_REQUEST["p"] = "registration";
        $this->redirectToLoad("doctors", "Main", array("step" => $step + 1));
    }

    private function checkAllFields($registrationStep)
    {
        switch ($registrationStep) {
            case '1':
                $gender = $this->secure(isset($_REQUEST['gender']) ? $_REQUEST['gender'] : '');

                $first_name = $this->secure($_REQUEST['first_name']);
                if (empty($first_name)) {
                    $this->fireError('first_name', 'Bitte geben Sie Ihren Vornamen ein');
                }

                $last_name = $this->secure($_REQUEST['last_name']);
                if (empty($last_name)) {
                    $this->fireError('last_name', 'Bitte geben Sie Ihren Nachnamen ein');
                }
                $full_title = $this->secure($_REQUEST['full_title']);

                $email = $this->secure($_REQUEST['email']);
                $checkEmail = $this->checkEmail($email);
                if (empty($email) || $checkEmail === -1) {
                    $this->fireError('email', 'Bitte geben Sie Ihre E-Mail-Adresse ein');
                } elseif ($checkEmail === 0 && !isset($_SESSION["userId"])) {
                    $this->fireError('email', 'Ein Account mit dieser E-Mail-Adresse besteht bereits');
                }

                $phone = $this->secure($_REQUEST['phone']);
                if (empty($phone)) {
                    $this->fireError('phone', 'Bitte geben Sie Ihre Telefonnumer ein');
                }

                return array(
                    'first_name' => $first_name,
                    'last_name'  => $last_name,
                    'title'      => $full_title,
                    'gender'     => empty($gender) ? '0' : $gender,
                    'phone'      => $phone,
                    'email'      => $email
                );
            case '2':
                $medical_specialties = $_REQUEST['medical_specialty_id'];
                if (empty($medical_specialties)) {
                    $this->fireError('medical_specialty_id', 'Bitte wählen Sie Ihr Fachgebiet', 2);
                }

                $practice_title = $this->secure($_REQUEST['practice_title']);
                if (empty($practice_title)) {
                    $this->fireError('practice_title', 'Bitte geben Sie Ihren Praxisnamen an', 2);
                }
                $city = $this->secure($_REQUEST['city']);
                if (empty($city)) {
                    $this->fireError('city', 'Bitte geben Sie Ihre Stadt an', 2);
                }

                return array($medical_specialties);
        }

        return array();
    }

    private function fireError($fieldName, $errorMessage, $step = 1)
    {
        $GLOBALS['view']->addMessage('STATUS', 'ERROR', $errorMessage);
        $_REQUEST["p"] = "registration";
        $this->redirectToLoad("doctors", "Main", array("step" => $step));
    }

    public function checkEmail($email)
    {
        if (!preg_match("/^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/", strtolower($email))) {
            return -1;
        }
        $user = $this->usersManager->getUserByEmail($email);
        $tempUser = $this->tempUsersDataManager->getByEmail($email);
        if (isset($user) || isset($tempUser)) {
            return 0;
        }

        return true;
    }

    public function setUserRequestParametersInDto($userDatas)
    {
        $tempUserDataDto = $this->tempUsersDataManager->createDto();
        $tempUserDataDto->setFirstName($userDatas['first_name']);
        $tempUserDataDto->setLastName($userDatas['last_name']);
        $tempUserDataDto->setTitle($userDatas['title']);
        $tempUserDataDto->setGender($userDatas['gender']);
        $tempUserDataDto->setEmail($userDatas['email']);
        $tempUserDataDto->setPhone($userDatas['phone']);
/*
// not used in the registration process anymore
        $passwordHashSalt = substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 6);
        $passwordHash = hash('sha256', PASSWORD_FIX_SALT . $userDatas['password'] . $passwordHashSalt);
        $tempUserDataDto->setPasswordHash($passwordHash);
        $tempUserDataDto->setPasswordHashSalt($passwordHashSalt);

        $tempUserDataDto->setLocationName($userDatas['location_name']);
        $tempUserDataDto->setLocationStreet($userDatas['location_street']);
        $tempUserDataDto->setLocationZip($userDatas['location_zip']);
        $tempUserDataDto->setLocationCity($userDatas['location_city']);
*/
        return $tempUserDataDto;
    }

    private function sendInfoEmail($userDto)
    {
        $templateFilename = '/mailing/doctors/doctor-register.txt';
        $_mailing = new Mailing();
        $_mailing->type = MAILING_TYPE_TEXT;
        $_mailing->content_filename = $templateFilename;

        $_mailing->data['domain'] = $GLOBALS['CONFIG']['DOMAIN'];
        $_mailing->data['email'] = $userDto->getEmail();
        $_mailing->data['doctor_name'] = $GLOBALS["CONFIG"]["ARRAY_GENDER"][$userDto->getGender()] . " " . $userDto->getTitle() . " " . $userDto->getFirstName() . " " . $userDto->getLastName();
        $_mailing->data['medical_specialty'] = "(Please visit for all details: " . "http://www.arzttermine.de/admin/temp_users/index.php?action=edit&id=" . $_REQUEST['id'] . "&page=1&alpha=&sort=id&direction=DESC&num_per_page=50&type=num" . ")";
        $_mailing->data['phone'] = $userDto->getPhone();
        $_mailing->data['created_at'] = date('Y-m-d H:i:s');

        $_mailing->addAddressTo($GLOBALS["CONFIG"]["SALES_EMAIL_ADDRESS"]);
        $_mailing->send();
    }

    private function sendRequestConfirmationEmail($userDto)
    {
        $templateFilename = '/mailing/doctors/doctor-request-confirmation.txt';
        $_mailing = new Mailing();
        $_mailing->type = MAILING_TYPE_TEXT;
        $_mailing->content_filename = $templateFilename;
        $_mailing->data['domain'] = $GLOBALS['CONFIG']['DOMAIN'];
        $_mailing->data['doctor_salutation_full_name'] = $GLOBALS["CONFIG"]["ARRAY_GENDER"][$userDto->getGender()] . " " . $userDto->getFirstName() . " " . $userDto->getLastName();
        $_mailing->addAddressTo($userDto->getEmail());
        $_mailing->send();
    }

    public function getRequestGroup()
    {
        return RequestGroups::$guestRequest;
    }
}
