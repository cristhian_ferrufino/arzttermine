<?php

namespace NGS\Actions\Doctors;

use Arzttermine\Application\Application;
use NGS\Managers\AssetsManager;
use NGS\Security\RequestGroups;

class UploadProfilePicAction extends BaseDoctorAction {

    public function service() {
		parent::service();

		// Only accept POST requests
		if (!$this->isPost()) {
			$this->fireError('Invalid request');
		}

		// If the IDs don't match, either the user messed with the form, or the DB is freaking out
		if (intval($this->getPost('uid')) !== intval($this->doctor->getDoctor()->getId())) {
			$this->fireError('Invalid user');
		}

		$editor = $this->getSessionUser()->getId();
		
		if ($this->doctor->isEditAllowed($editor)) {
			$assetsManager = AssetsManager::getInstance($this->config, $this->args);
			$doctorId = $this->doctor->getDoctor()->getId();
			
			$ownerType = ASSET_OWNERTYPE_USER;
			$fileExtension = end(explode('.', $_FILES['upload']['name']));
			
			if (!in_array($fileExtension, $GLOBALS["CONFIG"]["ASSET_POSSIBLE_FILEEXTENSIONS"])){
				$this->fireError("Leider wird Ihr Bildformat nicht unterstützt. (JPG, JPEG, PNG)");
			}
			
			$response = $this->checkInputFile('upload');
			
			if ($response !== 'ok') {
				$this->fireError($response);
			}
			
			if (!$assetId = $assetsManager->saveAsset($ownerType, $doctorId, $_FILES['upload'])) {
				$this->fireError("Could not save profile picture");
			}

			$asset = $assetsManager->getById($assetId);

			// Update the doctor's profile with the new asset info
			$this->doctor->update(array('profileAssetId' => $assetId, 'profileAssetFilename' => $asset->getFileName()));
			

			$_REQUEST['p'] = 'profile_pictures';
			$this->redirect($GLOBALS["CONFIG"]["DOCTORS_LOGIN_URL"] . "/profilbild/" . $this->doctor->getDoctor()->getSlug());
		} else {
			$this->fireError("You do not have permission to edit this doctor's profile");
		}
    }

    public function getRequestGroup() {
        return RequestGroups::$doctorsRequest;
    }
    
    public function checkInputFile($requestFileVariableName) {
		$error = $_FILES[$requestFileVariableName]['error'];
		//check for error
		switch ($error) {
			case UPLOAD_ERR_OK :
				return 'ok';
			case UPLOAD_ERR_INI_SIZE :
				return 'The uploaded file exceeds the upload_max_filesize directive in php.ini.';

			case UPLOAD_ERR_FORM_SIZE :
				return 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.';

			case UPLOAD_ERR_PARTIAL :
				return 'The uploaded file was only partially uploaded.';

			case UPLOAD_ERR_NO_FILE :
				return 'No file was uploaded.';

			case UPLOAD_ERR_NO_TMP_DIR :
				return 'Missing a temporary folder. Introduced in PHP 4.3.10 and PHP 5.0.3.';

			case UPLOAD_ERR_CANT_WRITE :
				return 'Failed to write file to disk. Introduced in PHP 5.1.0.';

			case UPLOAD_ERR_EXTENSION :
				return 'File upload stopped by extension. Introduced in PHP 5.2.0.';

			default :
				return 'Unknown error';
		}
	}

	private function fireError($errorMessage, $fieldName = '') {
        Application::getInstance()->addFlashMessage(array('status' => 'ERROR', 'text' => $errorMessage));
        $this->redirect($this->getCurrentUrl());
	}
    
    private function getCurrentUrl()
    {
        return $GLOBALS["CONFIG"]["DOCTORS_LOGIN_URL"] . "/profilbild?arzt=" . $this->doctor->getDoctor()->getSlug();
    }
}