<?php

namespace NGS\Actions\Doctors;

use NGS\Managers\DoctorsTreatmentTypesManager;
use NGS\Managers\TreatmentTypesManager;
use NGS\Security\RequestGroups;

class SaveMedicalSpecialtiesAction extends BaseDoctorAction {

    public function service() {
		parent::service();
		
		if (!$this->isPost()) {
			$this->fireError('Invalid request');
		}

		if (intval($this->getPost('uid')) !== intval($this->doctor->getDoctor()->getId())) {
			$this->fireError('Invalid user');
		}

		$editor = $this->getSessionUser()->getId();
		
		if ($this->doctor->isEditAllowed($editor)) {
			$medical_specialties_ids = $this->getPost('medical_specialties_ids');
			$treatment_types_ids     = $this->getPost('treatment_types');
			
			// Fail hard
			// @todo: Fix this so it doesn't have a bunch of crap in it
			if (empty($treatment_types_ids) || empty($medical_specialties_ids)) {
				$_REQUEST['p'] = 'manage_medical_specialties';
				$this->redirectToLoad("doctors", "Main", array("error" => array('error_message' => 'Please select at least 1 medical specialty and 1 treatment type'), "request" => $this->getPost()));
			}
			
			$doctorsTreatmentTypesManager = DoctorsTreatmentTypesManager::getInstance($this->config, $this->args);
			$treatmentTypesManager        = TreatmentTypesManager::getInstance($this->config, $this->args);

			// Saving doctor selected medical specialities treatment types
			$treatmentTypesDtos = $treatmentTypesManager->getByIds($treatment_types_ids);
			$doctorsTreatmentTypesManager->deleteDoctorAllTreatmentTypes($this->doctor->getDoctor()->getId());

			foreach ($treatmentTypesDtos as $ttd) {
				$medicalSpecialtyId = $ttd->getMedicalSpecialtyId();
				$doctorsTreatmentTypesManager->addTreatmentType($this->doctor->getDoctor()->getId(), $medicalSpecialtyId, $ttd->getId());
			}

			// Saving doctor selected medical specialities ids
			$this->doctor->updateMedicalSpecialties($medical_specialties_ids);

			$this->redirectToLoad("doctors", "Main", array("message" => "Profil erfolgreich gespeichert."));
		} else {
			$this->fireError("You do not have permission to edit this doctor's profile");
		}
    }

    public function getRequestGroup() {
        return RequestGroups::$doctorsRequest;
    }

	
	private function fireError($errorMessage) {
		$_REQUEST["p"] = 'manage_medical_specialties';
		$this->redirectToLoad("doctors", "Main", array('error' => array('error_message' => $errorMessage), 'form_data' => $this->getPost()));
	}

}