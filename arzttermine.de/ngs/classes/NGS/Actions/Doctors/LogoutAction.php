<?php

namespace NGS\Actions\Doctors;

use Arzttermine\Application\Application;
use NGS\Actions\BaseAction;
use NGS\Security\RequestGroups;

class LogoutAction extends BaseAction {

    public function service()
    {
        session_destroy();
        $this->redirect($GLOBALS['CONFIG']['DOCTORS_LOGIN_URL']);
    }

    /**
     * Let *anyone* log out. It doesn't matter
     * 
     * @return int
     */
    public function getRequestGroup()
    {
        return RequestGroups::$guestRequest;
    }
}