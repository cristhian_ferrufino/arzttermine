<?php

namespace NGS\Actions\Doctors;

use NGS\Security\RequestGroups;

class SaveLocationSpecialtiesAction extends BaseDoctorAction {

	public function service() {
		parent::service();

		if (!$this->isPost()) {
			$this->fireError('Invalid request');
		}

		if (intval($this->getPost('uid')) !== intval($this->location->getLocation()->getId())) {
			$this->fireError('Invalid location');
		}

		$specialties = $this->getPost('medical_specialty_ids', array());
		
		if (empty($specialties) || count($specialties) < 1) {
			// @todo: translation
			$this->fireError('Please select at least 1 medical specialty');
		}
		
		$editor = $this->getSessionUser()->getId();

		if ($this->location->isEditAllowed($editor)) {
			$this->saveSpecialties($specialties);
		} else {
			$this->fireError("You do not have permission to edit this location's profile");
		}
		
		$this->redirectToLoad("doctors", "Main", array("message" => "Profil erfolgreich gespeichert."));
	}
	
	private function saveSpecialties($specialties) {
		$medical_specialty_ids = filter_var_array($specialties, FILTER_SANITIZE_NUMBER_INT);
		
		return $this->location->update(array('medical_specialty_ids' => implode(',', $medical_specialty_ids)));
	}

	private function fireError($errorMessage) {
		$_REQUEST["p"] = 'manage_location_specialties';
		$this->redirectToLoad("doctors", "Main", array('error' => array('error_message' => $errorMessage), 'request' => $_POST));
	}

	public function getRequestGroup() {
		return RequestGroups::$doctorsRequest;
	}

}