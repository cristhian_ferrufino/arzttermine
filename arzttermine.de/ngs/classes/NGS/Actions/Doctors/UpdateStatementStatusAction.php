<?php

namespace NGS\Actions\Doctors;

use NGS\Security\RequestGroups;
use NGS\Util\MailSender;

class UpdateStatementStatusAction extends BaseDoctorAction {
	
    public function service() {
		parent::service();
		
		// Can ONLY be called via AJAX Post
		if (!$this->isPost() || !$this->isXmlHttpRequest()) {
			return;
		}
		
		$status = false;
		$email_status = false;
		
		if ($this->location->isEditAllowed($this->getSessionUser()->getId())) {
			$period = $this->getPost('period');
			$statement_status = $this->getPost('status');

			if ($status = $this->location->updateStatement($period, $statement_status)) {
				$email_status = $this->sendInfoEmail($period);
			}
		}
		
		$result = array(
			'status' => $status,
			'email'  => (int)$email_status
		);
		
		$this->sendJsonResponse($result);
    }

    public function getRequestGroup() {
        return RequestGroups::$doctorsRequest;
    }

    private function sendInfoEmail($period) {
		$user = $this->getSessionUser();
        $mailSender = new MailSender($this->config);
		
        $from       = $GLOBALS["CONFIG"]["SYSTEM_MAIL_FROM_EMAIL"];
        $recipients = array($GLOBALS["CONFIG"]["CONTACTS_EMAIL_ADDRESS"]);
        $subject    = 'Appointment review for {$period}';
		
		$html     = "{$user->getFirstName()} {$user->getLastName()} has completed his appointment review for {$period}";
        $template = SYSTEM_PATH . "/ngs/templates/mail/cms_mailing.tpl";
        $params   = array("emailMessage" => $html);
		
        return $mailSender->send($from, $recipients, $subject, $template, $params);
    }

}