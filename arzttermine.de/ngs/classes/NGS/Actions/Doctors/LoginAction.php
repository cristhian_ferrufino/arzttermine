<?php

namespace NGS\Actions\Doctors;

use Arzttermine\Application\Application;
use NGS\Actions\BaseAction;
use NGS\Managers\LoginHistoryManager;
use NGS\Managers\TempUsersDataManager;
use NGS\Managers\UsersManager;
use NGS\Security\RequestGroups;
use NGS\Security\Users\DoctorUser;
use NGS\Util\UtilFunctions;

/**
 * @author Vahagn Sookiasian
 */
class LoginAction extends BaseAction {
    
	public function service()
    {
        $view = Application::getInstance()->getView();
            
		$this->checkEmailPasswordEmpty();
		$email = $this->secure($_REQUEST['email']);
		$password = $_REQUEST['password'];
        
        $um = UsersManager::getInstance($this->config, $this->args);
		$userDto = $um->getUserByEmail($email);
        
        if (!isset($userDto)) {
            $tempUsersDataManager = TempUsersDataManager::getInstance($this->config, $this->args);
            $tempUserDataDto = $tempUsersDataManager->getByEmail($email);
            if (isset($tempUserDataDto)) {
                $hashSalt = $tempUserDataDto->getPasswordHashSalt();
                $password_hash = hash('sha256', PASSWORD_FIX_SALT . $password . $hashSalt);
                $tempUserDataDto = $tempUsersDataManager->getByEmailAndPasswordHash($email, $password_hash);
                if (isset($tempUserDataDto)) {
                    $_REQUEST['p'] = 'login';
                    $errorMessage = 'Ihr Konto wurde noch nicht bestätigt, bitte versuchen Sie es später';
                    $view->addMessage('STATUS', 'ERROR', $errorMessage);
                    $this->redirectToLoad("doctors", "Main", array("error" => true, "error_message" => $errorMessage));
                }
            }
        }
        
		$this->checkUserDtoEmpty($userDto);

		$hashSalt = $userDto->getPasswordHashSalt();
		$password_hash = hash('sha256', PASSWORD_FIX_SALT . $password . $hashSalt);
		$userDto = $um->getUserByEmailAndPasswordHash($email, $password_hash);
		$this->checkUserDtoEmpty($userDto);

		if ($userDto->getGroupId() != GROUP_DOCTOR) {
            $_REQUEST['p'] = 'login';
			$errorMessage = 'Sie sind nicht als Arzt in unserem System erfasst';
		    $view->addMessage('STATUS', 'ERROR', $errorMessage);
			$this->redirectToLoad("doctors", "Main", array("error" => true, "error_message" => $errorMessage));	
		} elseif ($userDto->getActivatedAt() == "0000-00-00 00:00:00") {
            $_REQUEST['p'] = 'login';
            $errorMessage = 'Der Account wurde noch nicht aktiviert, bitte überprüfen Sie Ihre E-Mails.';
            $view->addMessage('STATUS', 'ERROR', $errorMessage);
            $this->redirectToLoad("doctors", "Main", array("error" => true, "error_message" => $errorMessage));                  
		}
        
		$user = new DoctorUser($userDto->getId());
		$user->setUniqueId($userDto->getPasswordHash());
		$this->sessionManager->setUser($user, true, true);
        $um->setLastLoginAt($userDto->getId(),date("Y-m-d H:i:s"));
        
		// Inserting data into login_history table
		$lhm = LoginHistoryManager::getInstance($this->config, $this->args);
		$browser = UtilFunctions::getBrowser();
        $lhm->insertRow(
            $userDto->getId(),
            LOGIN_DOCTOR_TYPE,
            date('Y-m-d H:i:s'),
            $_SERVER['REMOTE_ADDR'],
            $browser['platform'],
            $browser['name'],
            $browser['version']
        );

		$this->redirect($GLOBALS['CONFIG']['DOCTORS_LOGIN_URL']);
	}

	public function checkUserDtoEmpty($userDto)
    {
        $view = Application::getInstance()->getView();
        
		if (empty($userDto)) {
            $_REQUEST['p'] = 'login';
			$errorMessage = 'Bitte überprüfen Sie Ihre E-Mail-Adresse/Passwort';
            $view->addMessage('STATUS', 'ERROR', $errorMessage);
			$this->redirectToLoad("doctors", "Main", array("error" => true, "error_message" => $errorMessage));
		}
	}

	public function checkEmailPasswordEmpty()
    {
        $view = Application::getInstance()->getView();
        
		if (empty($_REQUEST['email'])) {
            $_REQUEST['p'] = 'login';
			$errorMessage = 'Bitte tragen Sie Ihre E-Mail-Adresse ein';
            $view->addMessage('STATUS', 'ERROR', $errorMessage);
			$this->redirectToLoad("doctors", "Main", array("error" => true, "error_message" => $errorMessage));
		}
        
		if (empty($_REQUEST['password'])) {
            $_REQUEST['p'] = 'login';
			$errorMessage = 'Bitte geben Sie Ihr Passwort ein';
            $view->addMessage('STATUS', 'ERROR', $errorMessage);
			$this->redirectToLoad("doctors", "Main", array("error" => true, "error_message" => $errorMessage));
		}
	}

	public function getRequestGroup()
    {
		return RequestGroups::$guestRequest;
	}

}
