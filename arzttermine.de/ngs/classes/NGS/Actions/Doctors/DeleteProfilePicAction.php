<?php

namespace NGS\Actions\Doctors;

use NGS\Managers\AssetsManager;
use NGS\Security\RequestGroups;

/**
 * @author Vahagn Kirakosyan
 */
class DeleteProfilePicAction extends BaseDoctorAction {

    public function service() {
		parent::service();

		// Only accept POST requests
		if (!$this->isPost() || !$this->isXmlHttpRequest()) {
			$this->fireError('Invalid request');
		}

		// If the IDs don't match, either the user messed with the form, or the DB is freaking out
		if (intval($this->getPost('uid')) !== intval($this->doctor->getDoctor()->getId())) {
			$this->fireError('Invalid user');
		}

		$editor = $this->getSessionUser()->getId();
        
		if ($this->doctor->isEditAllowed($editor)) {
			$assetId = $this->getPost('aid');
			
			$am = AssetsManager::getInstance($this->config, $this->args);
			$am->deleteAssetById($assetId);

			$this->sendJsonResponse(array('aid' => $assetId));
		} else {
			$this->sendJsonResponse(array('error' => 'You are not allowed to edit this doctor\'s profile'));
		}
    }

    public function getRequestGroup() {
        return RequestGroups::$doctorsRequest;
    }

	private function fireError($errorMessage, $fieldName = '') {
		$_REQUEST["p"] = 'profile_pictures';
		$this->redirectToLoad("doctors", "Main", array('error' => array('field_name' => $fieldName, 'error_message' => $errorMessage)));
	}

}