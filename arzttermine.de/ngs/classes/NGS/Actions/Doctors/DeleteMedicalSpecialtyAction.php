<?php

namespace NGS\Actions\Doctors;

use NGS\Actions\BaseAction;
use NGS\Managers\DoctorsTreatmentTypesManager;
use NGS\Managers\UsersManager;
use NGS\Security\RequestGroups;

/**
 * @author Vahagn Kirakosyan
 */
class DeleteMedicalSpecialtyAction extends BaseAction {

    public function service() {
        $usersManager = UsersManager::getInstance($this->config, $this->args);
        $doctorsTreatmentTypesManager = DoctorsTreatmentTypesManager::getInstance($this->config, $this->args);
        $doctorId = $this->sessionManager->getUser()->getId();
        $medicalSpecialtyId = $_REQUEST['id'];
        
        $medSpecs = $doctorsTreatmentTypesManager->getMedSpecIdsArrayWithExistingTreatmentTypesByDoctorId($doctorId);
        if((isset($medSpecs[$medicalSpecialtyId]) && count($medSpecs)==1) || !$usersManager->deleteDoctorMedicalSpecialty($doctorId, $medicalSpecialtyId)){
            $_REQUEST['p'] = 'delete_medical_specialties';
            $this->redirectToLoad('doctors', 'Main', array('error'=>array('error_message'=>'You must have at least one medical specialty and one treatment type')));
        }
        $doctorsTreatmentTypesManager->deleteDoctorTreatmentTypeByMedSpecId($doctorId,$medicalSpecialtyId);
        
        $doctor = $usersManager->getUserById($doctorId);
        $this->sendProfileEditEmail($doctor);
        $this->redirect($GLOBALS["CONFIG"]["DOCTORS_LOGIN_URL"]."/fachrichtung_entfernen");
    }

    public function getRequestGroup() {
        return RequestGroups::$doctorsRequest;
    }

}