<?php

namespace NGS\Actions\Doctors;
use NGS\Managers\DoctorsTreatmentTypesManager;
use NGS\Managers\UsersAaManager;
use NGS\Managers\UsersManager;
use NGS\Security\RequestGroups;


class SaveCalendarAction extends BaseDoctorAction {

	private $usersAaManager = null;

	public function service() {
		parent::service();
		
		$this->usersAaManager = UsersAaManager::getInstance($this->config, $this->args);
		$usersManager = UsersManager::getInstance($this->config, $this->args);
		$doctorId = $this->secure($_REQUEST['user_id']);
		if (isset($_REQUEST["fixed_week"])) {
			$changeFixedWeekTo = ($this->secure($_REQUEST["fixed_week"] == "true")) ? 1 : 0;
		}
		if (isset($changeFixedWeekTo) && $changeFixedWeekTo == 1) {
			$usersManager->setUserFixedWeek($doctorId, $changeFixedWeekTo);
		}
		$loggedInUserId = $this->sessionManager->getUser()->getId();
		$dtosToBeInserted = $this->getCalendarFormatedData($loggedInUserId);
		$doctor = $usersManager->getUserById($doctorId);
		$fixedWeek = $doctor->getFixedWeek();
		//TOTO do input parameters validation		
		if ($fixedWeek == 1) {
			$deleteUserAllAvaialableTimesByUserIdAndLocationIdSql = $this->usersAaManager->deleteUserAllAvaialableTimesByUserIdAndLocationId($doctorId, true);
			$bulkDtosToBeInserted = array();
			foreach ($dtosToBeInserted as $dto) {
				$dtos = $this->usersAaManager->addDoctorFixedWeekAppointment($dto, null, null, true);
				$bulkDtosToBeInserted = array_merge($bulkDtosToBeInserted, $dtos);
			}
			$this->usersAaManager->addRows($bulkDtosToBeInserted, $deleteUserAllAvaialableTimesByUserIdAndLocationIdSql);
		} else {
			
			$startDate = $this->secure($_REQUEST['start_date']);
			if (!isset($_REQUEST['repeat_until_date'])) {
				$endDate = $this->secure($_REQUEST['end_date']);
				$deleteUserAllAvaialableTimesByUserIdAndLocationIdAndDateRageSql = $this->usersAaManager->deleteUserAllAvaialableTimesByUserIdAndLocationIdAndDateRage($doctorId, $startDate, $endDate, true);
				$this->usersAaManager->addRows($dtosToBeInserted, $deleteUserAllAvaialableTimesByUserIdAndLocationIdAndDateRageSql);
				
			} else {
				$repeatUntilDate = $this->secure($_REQUEST['repeat_until_date']);
				$repeatUntilDate = date("Y-m-d", strtotime($repeatUntilDate));
				$deleteUserAllAvaialableTimesByUserIdAndLocationIdAndDateRageSql = $this->usersAaManager->deleteUserAllAvaialableTimesByUserIdAndLocationIdAndDateRage($doctorId, $startDate, $repeatUntilDate, true);
				$bulkDtosToBeInserted = array();
				foreach ($dtosToBeInserted as $dto) {
					$dtos = $this->usersAaManager->addDoctorFixedWeekAppointment($dto, $startDate, $repeatUntilDate, true);
					$bulkDtosToBeInserted = array_merge($bulkDtosToBeInserted, $dtos);
				}

				$this->usersAaManager->addRows($bulkDtosToBeInserted, $deleteUserAllAvaialableTimesByUserIdAndLocationIdAndDateRageSql);
			}
		}
		if (isset($changeFixedWeekTo) && $changeFixedWeekTo == 0) {
			$usersManager->setUserFixedWeek($doctorId, $changeFixedWeekTo);
		}
		if (isset($_REQUEST['call_back_page'])) {
			echo $_REQUEST['call_back_page'];
		}
	}

	public function getCalendarFormatedData($loggedInUserId) {
		$calendarDataJson = $_REQUEST['calendar_data'];
		$calendarData = json_decode($calendarDataJson);
		$doctorsTreatmentTypesManager = DoctorsTreatmentTypesManager::getInstance($this->config, $this->args);
		$ret = array();
		foreach ($calendarData as $aptObject) {		
			if (empty($aptObject->treatment_type_ids)) {
				$doctorTreatmentTypesIdsArray = $doctorsTreatmentTypesManager->getDoctorTreatmentTypesIds($aptObject->user_id);
				$doctorTreatmentTypesIdsText = implode(',', $doctorTreatmentTypesIdsArray);
				$aptObject->treatment_type_ids = $doctorTreatmentTypesIdsText;
			}
			$dto = $this->usersAaManager->createDto();
			$dto->setUserId($aptObject->user_id);
			$dto->setLocationId($aptObject->location_id);
			$dto->setStartAt($aptObject->start_at);
			$dto->setEndAt($aptObject->end_at);
			$dto->setBlockInMinutes($aptObject->block_in_minutes);
			$dto->setDate($aptObject->date);
			$dto->setTreatmentTypeIds($aptObject->treatment_type_ids);
			$dto->setInsuranceTypes($aptObject->insurance_types);
			$dto->setCreatedAt(date("Y-m-d H:i:s"));
			$dto->setCreatedBy($loggedInUserId);
			$ret[] = $dto;
		}
		return $ret;
	}

    public function getRequestGroup() {
        return RequestGroups::$doctorsRequest;
    }

}