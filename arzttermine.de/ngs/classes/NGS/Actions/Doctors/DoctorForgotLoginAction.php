<?php

namespace NGS\Actions\Doctors;

use Arzttermine\Mail\Mailing;
use NGS\Actions\BaseAction;
use NGS\Managers\UsersManager;
use NGS\Security\RequestGroups;

/**
 * @author Vahagn Sookiasian
 */
class DoctorForgotLoginAction extends BaseAction {

	public function service() {
				
		$practice_name = $this->secure($_REQUEST['practice_name']);			
        $doctor_name = $this->secure($_REQUEST['doctor_name']);
        $doctor_lname = $this->secure($_REQUEST['doctor_lname']);
        
		
        if (empty($doctor_name))
        {
            $this->fireError("Bitte geben Sie Ihren Vornamen an.");
        }
        if (empty($doctor_lname))
        {
            $this->fireError("Bitte geben Sie Ihren Nachnamen an.");
        }
		if (empty($practice_name))
		{
            $this->fireError("Bitte geben Sie Ihren Praxisnamen an.");
		}
        
        $usersManager = UsersManager::getInstance($this->config, $this->args);
        $userDto = $usersManager->getUserByNameAndLNameANDPracticeName($practice_name, $doctor_name ,$doctor_lname);
        
        if(isset($userDto)){
            $this->sendInfoEmail($practice_name, $userDto->getPhone());  
            $this->sendEmailToDoctor($userDto);  
            $email = substr($userDto->getEmail(), 0,6);
            $message = 'Ihre Login-Daten wurden an Ihre E-Mail-Adresse ( "'. $email .'...",  ) gesendet. Falls Sie weitere Fragen haben oder Hilfe benötigen, kontaktieren Sie unseren kostenfreien Support: 030 / 609 840 260';
            $GLOBALS['view']->addMessage('STATUS', 'NOTICE', $message);
            $_REQUEST['p']='login';
            $this->redirectToLoad("doctors", "Main", array("message" => $message));
        }
        $this->fireError('Leider konnten wir Ihre Daten nicht finden, bitte versuchen Sie es erneut oder kontaktieren Sie unseren kostenfreien Support: 030 / 609 840 260');
	}

    private function fireError($errorMessage) {
        $GLOBALS['view']->addMessage('STATUS', 'ERROR', $errorMessage);
        $_REQUEST["forgotten"] = "login";
        $_REQUEST["p"] = "login";
        $this->redirectToLoad("doctors", "Main", array("error_message"=>$errorMessage));
    }

    private function sendInfoEmail($practice_name,$phone_number){
        $templateFilename = '/mailing/doctors/doctor-forgot-login.txt';
        $_mailing = new Mailing();
        $_mailing->type = MAILING_TYPE_TEXT;
        $_mailing->content_filename = $templateFilename;
        $_mailing->data['domain'] = $GLOBALS['CONFIG']['DOMAIN'];
        $_mailing->data['location_name'] = $practice_name;
        $_mailing->data['phone'] = $phone_number;
        $_mailing->addAddressTo($GLOBALS["CONFIG"]["SALES_EMAIL_ADDRESS"]);
        $_mailing->send();
    }
    
    private function sendEmailToDoctor($userDto){
        $templateFilename = '/mailing/doctors/forgot-login.txt';
        $_mailing = new Mailing();
        $_mailing->type = MAILING_TYPE_TEXT;
        $_mailing->content_filename = $templateFilename;
        $_mailing->data['domain'] = $GLOBALS['CONFIG']['DOMAIN'];
        $_mailing->data['doctor_name'] = $userDto->getTitle()." ".$userDto->getFirstName()." ".$userDto->getLastName();
        $_mailing->data['email'] = $userDto->getEmail();
        $_mailing->addAddressTo($userDto->getEmail());
        $_mailing->send();   
    }

	public function getRequestGroup() {
		return RequestGroups::$guestRequest;
	}

}
