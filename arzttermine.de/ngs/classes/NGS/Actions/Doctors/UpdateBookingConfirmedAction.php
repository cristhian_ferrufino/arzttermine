<?php

namespace NGS\Actions\Doctors;

use NGS\Managers\BookingsManager;
use NGS\Security\RequestGroups;

class UpdateBookingConfirmedAction extends BaseDoctorAction {

    public function service() {
		parent::service();
		
		// Can ONLY be called via AJAX Post
		if (!$this->isPost() || !$this->isXmlHttpRequest()) {
			return;
		}
		
		$status = false;
		
		if ($this->doctor->isEditAllowed($this->getSessionUser()->getId())) {
			$bm = BookingsManager::getInstance($this->config, $this->args);
			$status = $bm->updateConfirmed(intval($this->getPost('id')), intval($this->getPost('c')));
		}

		$result = array(
			'status' => (int)$status
		);
		
		$this->sendJsonResponse($result);
    }

    public function getRequestGroup() {
        return RequestGroups::$doctorsRequest;
    }

}