<?php

namespace NGS\Actions\Doctors;

use Arzttermine\Application\Application;
use NGS\Security\RequestGroups;

/**
 * @author Vahagn Sookiasian
 */
class SaveLocationAction extends BaseDoctorAction {

    public function service()
    {
        parent::service();

        // Only accept POST requests
        if (!$this->isPost()) {
            $this->fireError('uid', 'Invalid request');
        }

        if (intval($this->getPost('uid')) !== intval($this->location->getLocation()->getId())) {
            $this->fireError('uid', 'Invalid location');
        }

        // The editor is the logged in user, the doctor is the modification target
        $editor = $this->getSessionUser()->getId();

        if (!$this->location->isEditAllowed($editor)) {
            // @todo: Translation for error
            $this->fireError('uid', 'You do not have permission to edit this practice profile');
        }

        // Form
        $this->form_data = filter_var_array($this->getPost(), FILTER_SANITIZE_STRING);
        $result = $this->location->update($this->form_data);

        // Zero rows affected, or DB error
        if ($result === 0 || $result === -1) {
            // @todo: Handle zero rows affected (no update)
            // @todo: Translation for error
            $this->fireError('uid', 'Could not update practice profile at this time');
        }

        $this->redirect($this->getCurrentUrl());
    }

    /**
     * @param string $fieldName
     * @param string $errorMessage
     */
    private function fireError($fieldName, $errorMessage)
    {
        Application::getInstance()->addFlashMessage(array('status' => 'ERROR', 'text' => $errorMessage));
        $this->redirect($this->getCurrentUrl());
    }

    /**
     * @return string
     */
    private function getCurrentUrl()
    {
        return $GLOBALS["CONFIG"]["DOCTORS_LOGIN_URL"] . "/praxis?praxis=" . $this->location->getLocation()->getSlug();
    }

    /**
     * @return int
     */
    public function getRequestGroup()
    {
        return RequestGroups::$doctorsRequest;
    }
}