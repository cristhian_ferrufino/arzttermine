<?php

namespace NGS\Actions\Doctors;

use NGS\Actions\BaseAction;
use NGS\DAL\DTO\LocationsDto;
use NGS\Managers\AssetsManager;
use NGS\Managers\LocationsManager;
use NGS\Managers\UsersManager;
use NGS\Models\Doctor;
use NGS\Models\Practice;
use NGS\Security\RequestGroups;

class BaseDoctorAction extends BaseAction {

	/**
	 * @var UsersManager Singleton user manager
	 */
	protected $usersManager;

	/**
	 * @var AssetsManager Singleton asset manager
	 */
	protected $assetsManager;

	/**
	 * @var LocationsManager Singleton location manager
	 */
	protected $locationsManager;

	/**
	 * @var Doctor The current doctor we're looking at
	 */
	protected $doctor;

	/**
	 * @var Practice The current location we're looking at
	 */
	protected $location;

	/**
	 * @var array Carries request data from whatever form we might have. Used by all form pages
	 */
	protected $form_data = array();

	/**
	 * @return void
	 */
	public function service()
    {

		// Load the doctor we're looking at (NOT necessarily the session doctor)
		$this->doctor = new Doctor();
		$this->doctor->autoload($this->getSessionUser()->getId());

		// Load a location. If it's empty, try load the doctor's first location
		if ($locations = $this->doctor->getLocations()) {
			if (count($locations) && !empty($locations[0]) && $locations[0] instanceof LocationsDto) {
				$this->location = new Practice();
				$this->location->autoload($locations[0]->getId());
			}
		}
		
	}

	public function getRequestGroup()
    {
		return RequestGroups::$doctorsRequest;
	}

}