<?php

namespace NGS\Actions\Doctors;

use NGS\Actions\BaseAction;
use NGS\Managers\TempUsersDataManager;
use NGS\Managers\UsersManager;
use NGS\Security\RequestGroups;

/**
 * @author Vahagn Kirakosyan
 */
class ActivateAction extends BaseAction {

    public function service() {
        $activationCode = $_REQUEST['code'];
        if(!isset($activationCode)){
            $_REQUEST["p"] = "login";
            $this->redirectToLoad('doctors', "Main", array("error_message" => "Ungültiger Aktivierungs-Code"));
        }
        $usersManager = UsersManager::getInstance($this->config, $this->args);
        $userDto = $usersManager->getUserByActivationCode($activationCode);
        
        if(!isset($userDto)){
            $tempUsersDataManager= TempUsersDataManager::getInstance($this->config, $this->args);
            $tempUser = $tempUsersDataManager->getByActivationCode($activationCode);
            if(isset($tempUser)){
                $_REQUEST["p"] = "login";
                $this->redirectToLoad('doctors', "Main", array("error_message" => "Your account is not approved yet, please try later"));
            }else{
                $_REQUEST["p"] = "login";
                $this->redirectToLoad('doctors', "Main", array("error_message" => "Ungültiger Aktivierungs-Code"));
            }
        }
        else{
            $usersManager->activateDoctor($userDto->getId());
            $_REQUEST["p"] = "login";
            $this->redirectToLoad('doctors', "Main", array("message" => "Ihr Nutzerkonto wurde erfolgreich aktiviert. Sie können sich jetzt einloggen."));
        }
    }

    public function getRequestGroup() {
        return RequestGroups::$guestRequest;
    }

}