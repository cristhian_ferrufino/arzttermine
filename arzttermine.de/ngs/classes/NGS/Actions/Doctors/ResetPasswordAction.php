<?php

namespace NGS\Actions\Doctors;

use NGS\Actions\BaseAction;
use NGS\Managers\UsersManager;
use NGS\Security\RequestGroups;

/**
 * @author Vahagn Sookiasian
 */
class ResetPasswordAction extends BaseAction {

	public function service() {
		$password = $this->secure($_REQUEST['password']);
		$repeatPassword = $this->secure($_REQUEST['repeat_password']);
		$this->checkNewPassword($password, $repeatPassword);
        if(!$this->checkPassword($password)){
        	$this->fireError("Das Password sollte mindestes 12 Zeichen haben und mindestens einen Buchstaben sowie eine Zahl enthalten.");
        }
		$resetPasswordCode = $this->secure($_REQUEST['reset_password_code']);
		$usersManager = new UsersManager($this->config, $this->args);
		$userDto = $usersManager->getUserByResetPasswordCode($resetPasswordCode);
		if (!isset($userDto)) {
			$errorMessage = "Falscher Passwortreset-Code!";
			$GLOBALS['view']->addMessage('STATUS', 'ERROR', $errorMessage);
			$this->redirectToLoad("doctors", "Main", array("error_message" => $errorMessage));
		}
		$hashSalt = $userDto->getPasswordHashSalt();		
		$password_hash = hash('sha256', PASSWORD_FIX_SALT . $password . $hashSalt);
        if($userDto->getPasswordHash()==$password_hash){
            $this->fireError("Bitte geben Sie ein neues Passwort ein, das sich vom bisherigen unterscheidet.");    
        }

        // NM 26.02.2014.
        // The process of registering an account doesn't require explicit activation by the doctor any more.
        // Instead, a password reset link is emailed to him. The first time he gets here, the account will be activated.
        // I don't think there's a security concern here because this point can't be reached without prior backend approval 
        // from our side which leads to generating the reset_password_code in the initial email.
        if($userDto->getActivatedAt() == "0000-00-00 00:00:00"){
        	$usersManager->activateDoctor($userDto->getId());
        }
		
		$usersManager->setPasswordHash($userDto->getId(), $password_hash);
		
        $_REQUEST['p'] = 'login';
        $GLOBALS['view']->addMessage('STATUS', 'NOTICE', "Passwort wurde erfolgreich geändert.");
		$this->redirectToLoad("doctors", "Main", array("message" => "Passwort wurde erfolgreich geändert."));	
	}

    private function fireError($errorMessage) {
        $GLOBALS['view']->addMessage('STATUS', 'ERROR', $errorMessage);
        $_REQUEST["p"] = "reset_password";
        $_REQUEST['sub_pages'][0] = $_REQUEST['reset_password_code'];
        $this->redirectToLoad("doctors", "Main", array("error_message"=>$errorMessage));
    }

	public function checkNewPassword($password, $repeatPassword) {
		if ($password != $repeatPassword) {
			$this->fireError("Die Passwörter stimmen nicht überein.");
		}
		if (empty($password)) {
			$this->fireError("Bitte geben Sie ein Passwort an.");
		}
	}

	public function getRequestGroup() {
		return RequestGroups::$guestRequest;
	}

}
