<?php

namespace NGS\Actions\Doctors;

use Arzttermine\Application\Application;
use NGS\DAL\DTO\TreatmentTypesDto;
use NGS\Managers\DoctorsTreatmentTypesManager;
use NGS\Managers\TreatmentTypesManager;
use NGS\Managers\UsersEditHistoryManager;
use NGS\Managers\UsersManager;
use NGS\Security\RequestGroups;

/**
 * @author Vahagn Sookiasian
 */
class SaveDoctorProfileAction extends BaseDoctorAction {

    public function service()
    {
        parent::service();
        
        // Only accept POST requests
        if (!$this->isPost()) {
            $this->fireError('uid', 'Invalid request');
        }

        // If the IDs don't match, either the user messed with the form, or the DB is freaking out
        if (intval($this->getPost('uid')) !== intval($this->doctor->getDoctor()->getId())) {
            $this->fireError('uid', 'Invalid user');
        }

        // The editor is the logged in user, the doctor is the modification target
        $editor = $this->getSessionUser()->getId();

        if ($this->doctor->isEditAllowed($editor)) {
            $title      = $this->secure($this->getPost('title'));
            $first_name = $this->secure($this->getPost('first_name'));
            $last_name  = $this->secure($this->getPost('last_name'));
            
            $docinfo1 = $this->secure($this->getPost('docinfo1'));
            $docinfo2 = $this->secure($this->getPost('docinfo2'));
            $docinfo3 = $this->secure($this->getPost('docinfo3'));
            $docinfo5 = $this->secure($this->getPost('docinfo5'));
            $docinfo6 = $this->secure($this->getPost('docinfo6'));

            $this->checkInputTextFields(array('docinfo1' => $docinfo1, 'docinfo5' => $docinfo5, 'docinfo6' => $docinfo6, 'docinfo2' => $docinfo2, 'docinfo3' => $docinfo3));

            $currentPassword = $this->getPost('current_password');
            $newPassword = $this->getPost('new_password');
            $passwordHashSalt = $this->doctor->getDoctor()->getPasswordHashSalt();
            $passwordHash = $this->doctor->getDoctor()->getPasswordHash();

            if (!empty($newPassword)) {
                $this->usersManager = UsersManager::getInstance();
                $password_hash = hash('sha256', PASSWORD_FIX_SALT . $currentPassword . $passwordHashSalt);
                $doctorToBeChecked = $this->usersManager->getUserByIdAndHash($this->doctor->getDoctor()->getId(), $password_hash);

                if (empty($doctorToBeChecked)) {
                    $this->fireError('current_password', 'Die Eingabe Ihres aktuellen Passwortes ist leider falsch. Zum Ändern Ihres aktuellen Passwortes geben Sie bitte Ihr korrektes aktuelles Passwort ein');
                }

                if (!$this->checkPassword($newPassword)) {
                    $this->fireError('new_password', 'Das Password sollte mindestes 12 Zeichen haben und mindestens einen Buchstaben sowie eine Zahl enthalten.');
                }

                $passwordHashSalt = substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 6);
                $passwordHash = hash('sha256', PASSWORD_FIX_SALT . $newPassword . $passwordHashSalt);
            }

            $usersEditHistoryManager = UsersEditHistoryManager::getInstance($this->config, $this->args);
            $usersEditHistoryManager->insertDoctorOldInfo($this->doctor->getDoctor());

            $data = array(
                'title'            => $title,
                'firstName'        => $first_name,
                'lastName'         => $last_name,
                'docinfo1'         => $docinfo1,
                'docinfo2'         => $docinfo2,
                'docinfo3'         => $docinfo3,
                'docinfo5'         => $docinfo5,
                'docinfo6'         => $docinfo6,
                'passwordHashSalt' => $passwordHashSalt,
                'passwordHash'     => $passwordHash
            );

            $result = $this->doctor->update($data);

            // Zero rows affected, or DB error
            if ($result === 0 || $result === -1 || $result === false) {
                // @todo: Translation for error
                $this->fireError('uid', 'Could not update doctor profile at this time.');
            }

            $medical_specialties_ids = $this->getPost('medical_specialties_ids');
            $treatment_types_ids     = $this->getPost('treatment_types');

            // Fail hard
            // @todo: Fix this so it doesn't have a bunch of crap in it
            if (empty($treatment_types_ids) || empty($medical_specialties_ids)) {
                $_REQUEST['p'] = 'manage_medical_specialties';
                $this->redirectToLoad("doctors", "Main", array("error" => array('error_message' => 'Please select at least 1 medical specialty and 1 treatment type'), "request" => $this->getPost()));
            }

            $doctorsTreatmentTypesManager = DoctorsTreatmentTypesManager::getInstance($this->config, $this->args);
            $treatmentTypesManager        = TreatmentTypesManager::getInstance($this->config, $this->args);

            // Saving doctor selected medical specialities treatment types
            $treatmentTypesDtos = $treatmentTypesManager->getByIds($treatment_types_ids);
            $doctorsTreatmentTypesManager->deleteDoctorAllTreatmentTypes($this->doctor->getDoctor()->getId());
            
            /** @var TreatmentTypesDto $ttd */
            foreach ($treatmentTypesDtos as $ttd) {
                $medicalSpecialtyId = $ttd->getMedicalSpecialtyId();
                $doctorsTreatmentTypesManager->addTreatmentType($this->doctor->getDoctor()->getId(), $medicalSpecialtyId, $ttd->getId());
            }

            // Saving doctor selected medical specialities ids
            $this->doctor->updateMedicalSpecialties($medical_specialties_ids);
        } else {
            $this->fireError('uid', "You do not have permission to edit this doctor's profile");
        }

        Application::getInstance()->addFlashMessage('Profile successfully updated');
        $this->redirect($this->getCurrentUrl());
    }

    /**
     * @param string $fieldName
     * @param string $errorMessage
     */
    private function fireError($fieldName, $errorMessage)
    {
        Application::getInstance()->addFlashMessage(array('status' => 'ERROR', 'text' => $errorMessage));
        $this->redirect($this->getCurrentUrl());
    }

    /**
     * @return string
     */
    private function getCurrentUrl()
    {
        return $GLOBALS["CONFIG"]["DOCTORS_LOGIN_URL"] . "?arzt=" . $this->doctor->getDoctor()->getSlug();
    }

    /**
     * @param $textFieldsArray
     */
    public function checkInputTextFields($textFieldsArray)
    {
        $englishNumbersArray = array("zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine");
        $germanNumbersArray = array("null", "eins", "zwei", "drei", "vier", "fünf", "sechs", "sieben", "acht", "neun");
        $urlPattern = '/((https?\:\/\/)|(www[ ]*[.]{1})){1}[ -a-z0-9]+[.]{1}[ ]*[a-z]{2,4}/i';
        $emailPattern = "/([\s]*)([_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*([ ]+|)@([ ]+|)([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,}))([\s]*)/i";
        //$phoneNumberPattern = '/((00|\+)49)?[ -.\/]?(0?1[5-7 -.\/][0-9 -.\/]{1,})/';
        $mobilePhoneNumberPattern = '/(\(?030|040|069|089|((00|\+)49)\)?)[ -.\/]?([0-9- .\/]{4,12})/';
        $addressPattern1 = '/[0-9]+([ ,.\/\\\-\s]*[a-zA-ZÄäÖöÜüÀÁáÂâÈèÉéÊêÙùÚúßÇç]+){1,2}/';
        $addressPattern2 = '/([a-zA-ZÄäÖöÜüÀÁáÂâÈèÉéÊêÙùÚúßÇç]+[ ,.\/\\\-\s]*){1,2}[0-9]+/';
        foreach ($textFieldsArray as $fieldName => $fieldValue) {
            $fieldValue = str_replace($englishNumbersArray, array_keys($englishNumbersArray), $fieldValue);
            $fieldValue = str_replace($germanNumbersArray, array_keys($germanNumbersArray), $fieldValue);
            $fieldValue = str_replace(array('[dot]', '[at]'), array('.', '@'), $fieldValue);

            if (preg_match($urlPattern, $fieldValue) === 1) {
                $this->fireError($fieldName, "Bitte entschuldigen Sie, es ist nicht möglich Ihre Website Öffentlich in Ihrem Profil anzuzeigen.<br/>Für Fragen und Hilfe steht Ihnen unser Support unter Tel. 030 609 840 229 jederzeit zur Verfügung.");
            }

            if (preg_match($emailPattern, $fieldValue) === 1) {
                $this->fireError($fieldName, "Bitte entschuldigen Sie, es ist nicht möglich eine Emailadresse Öffentlich in Ihrem Profil anzuzeigen.<br/>Für Fragen und Hilfe steht Ihnen unser Support unter Tel. 030 609 840 229 jederzeit zur Verfügung.");
            }

            if (preg_match($mobilePhoneNumberPattern, $fieldValue) === 1) {
                $this->fireError($fieldName, "Bitte entschuldigen Sie, es ist nicht möglich die Telefonnummer in Ihrem Profil zu Ändern.<br/>Für Fragen und Hilfe steht Ihnen unser Support unter Tel. 030 609 840 229 jederzeit zur Verfügung.");
            }

            // Checking address existence by searching alphabet characters 
            // Proceeded or succeeded by number characters.
            // Searching by googleMaps the existence of this address in Germany.

            preg_match_all($addressPattern1, $fieldValue, $matches1);
            preg_match_all($addressPattern2, $fieldValue, $matches2);

            if (!isset($matches1[0])) {
                $matches1[0] = array();
            }

            if (!isset($matches2[0])) {
                $matches2[0] = array();
            }
            
            $addresses = array_merge($matches1[0], $matches2[0]);

            if (!empty($addresses)) {
                foreach ($addresses as $address) {
                    $_url = sprintf('http://maps.googleapis.com/maps/api/geocode/json?address=%s&language=de&sensor=false&components=country:DE', rawurlencode($address));

                    if ($_json = file_get_contents($_url)) {
                        $json = json_decode($_json);
                        if (count($json->results) > 0) {
                            for ($i = 0; $i < 3; $i++) {
                                if (@in_array('street_address', $json->results[$i]->types) && $json->results[$i]->geometry->location_type == "ROOFTOP" && strlen($address) >= 10) {
                                    // @todo: Fis, restore, or dump this. It's rubbish
                                    # $this->fireError($fieldName, "Die Eingabe von Firmenadressen ist nicht möglich. Für Fragen und Hilfe steht Ihnen unser Support unter Tel. 030 609 840 229 jederzeit zur Verfügung.");
                                    $this->sendProfileEditEmail($this->doctor->getDoctor());
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public function getRequestGroup()
    {
        return RequestGroups::$doctorsRequest;
    }
}