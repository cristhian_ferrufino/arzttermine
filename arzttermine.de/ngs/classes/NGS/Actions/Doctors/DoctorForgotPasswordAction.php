<?php

namespace NGS\Actions\Doctors;
use Arzttermine\Mail\Mailing;
use NGS\Actions\BaseAction;
use NGS\Managers\UsersManager;
use NGS\Security\RequestGroups;

/**
 * @author Vahagn Sookiasian
 */
class DoctorForgotPasswordAction extends BaseAction {

	public function service() {		
		$email = strtolower($this->secure($_REQUEST['email']));
		if (!$this->validateEmail($email)) {
	       //$this->redirectToLoad( $GLOBALS['CONFIG']['DOCTORS_LOGIN_URL'], "Main", array("message" => "Email successfully sent! we will contact you soon."));
		}
		$resetPasswordCode = substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 10);
		$usersManager = UsersManager::getInstance($this->config, $this->args);
		$userDto = $usersManager->getUserByEmail($email);
		if (!isset($userDto)) {
			$this->fireError("Diese E-Mail-Adresse ist nicht im System erfasst.");
		}elseif($userDto->getGroupId()!=3){
		    $this->fireError("Sie sind kein Arzt. Bitte, kontaktieren Sie unseren kostenfreien Support: 030 / 609 840 260");
		}	
		$usersManager->setResetPasswordCode($userDto->getId(), $resetPasswordCode);
		
        
        $templateFilename = '/mailing/doctors/forgot-password.txt';
        $_mailing = new Mailing();
        $_mailing->type = MAILING_TYPE_TEXT;
        $_mailing->content_filename = $templateFilename;
        $_mailing->data['domain'] = $GLOBALS['CONFIG']['DOMAIN'];
        $_mailing->data['url'] = $GLOBALS['protocol'].$_SERVER['HTTP_HOST'].'/'.$GLOBALS['CONFIG']['DOCTORS_LOGIN_URL'].'/passwort_zuruecksetzen/'.$resetPasswordCode;
        $_mailing->data['doctor_name'] = $userDto->getTitle()." ".$userDto->getFirstName()." ".$userDto->getLastName();
        $_mailing->addAddressTo($email);
        $_mailing->send();
        
        $message = "Anfrage zum Passwort-Reset wurde gesendet. Bitte überprüfen Sie Ihr E-Mail-Postfach $email.";
        $GLOBALS['view']->addMessage('STATUS', 'NOTICE', $message);
        $_REQUEST['p']='login';
		$this->redirectToLoad("doctors", "Main", array("message" => $message));
	}

    private function fireError($errorMessage) {
        $GLOBALS['view']->addMessage('STATUS', 'ERROR', $errorMessage);
        $_REQUEST["forgotten"] = "password";
        $_REQUEST["p"] = "login";
        $this->redirectToLoad("doctors", "Main", array("error_message"=>$errorMessage));
    }

	private function validateEmail($email) {
		return preg_match("/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/", $email);
	}

	public function getRequestGroup() {
		return RequestGroups::$guestRequest;
	}

}
