<?php

namespace NGS\Actions\Admins;

class EditEntryAction extends BaseAdminAction {
		
	public function service(){
		
		$path=$this->secure($_REQUEST['path']);
		$mapperName=$this->secure($_REQUEST['mapperName']);
		$mapper=$mapperName::getInstance();
		
		
		$id=$this->secure($_REQUEST['id']);
		$dto=$mapper->selectByPK($id);
		
		if(isset($_REQUEST['firstName']) && empty($_REQUEST['firstName'])){
			$this->redirect("ngsadmin/".$path."/edit?booking_id=".$id."&error=firstName");
		} 
		if(isset($_REQUEST['lastName']) && empty($_REQUEST['lastName'])){
			$this->redirect("ngsadmin/".$path."/edit?booking_id=".$id."&error=lastName");
		}
		
		if(isset($_REQUEST['userId']) && empty($_REQUEST['userId'])){
			$this->redirect("ngsadmin/".$path."/edit?samedi_data_id=".$id."&error=userId");
		} 
		if(isset($_REQUEST['locationId']) && empty($_REQUEST['locationId'])){
			$this->redirect("ngsadmin/".$path."/edit?samedi_data_id=".$id."&error=locationId");
		}
		
		foreach ($_REQUEST as $key => $value) {
			if(in_array($key, array_values($dto->getMapArray()))){
				$functionName='set'.ucfirst($key);
				$dto->$functionName($value);
			}
		}
		
		$updatedAt=date('Y-m-d H:i:s');
		$dto->setUpdatedAt($updatedAt);
		
		$mapper->updateByPK($dto);
		$this->redirect("ngsadmin/".$path."?editentry=success");
	}
	

}