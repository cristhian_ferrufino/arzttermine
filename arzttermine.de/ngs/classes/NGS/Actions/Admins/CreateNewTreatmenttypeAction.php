<?php

namespace NGS\Actions\Admins;

use NGS\Managers\TreatmentTypesManager;

class CreateNewTreatmenttypeAction extends BaseAdminAction {

	public function service() {
		$name_en = $_REQUEST['ntt_name_en'];
		$name_de = $_REQUEST['ntt_name_de'];
		$slug = $_REQUEST['ntt_slug'];
		$status = $_REQUEST['ntt_status'];
		$medSpecId = $_REQUEST['ntt_medspecid'];
		$treatmentTypesManager = TreatmentTypesManager::getInstance($this->config, $this->args);
		$userId = $this->getSessionUser()->getId();
		$treatmentTypesManager->addRow($status, $slug, $name_en, $name_de, $medSpecId, $userId);
		echo 'ok';
	}

}