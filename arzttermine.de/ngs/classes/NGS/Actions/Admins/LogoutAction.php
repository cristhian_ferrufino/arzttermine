<?php

namespace NGS\Actions\Admins;

use Arzttermine\Application\Application;

class LogoutAction extends BaseAdminAction {

    public function service()
    {
        session_destroy();
        $app = Application::getInstance();
        $app->redirect($app->container->get('router')->generate('login'));
    }
}