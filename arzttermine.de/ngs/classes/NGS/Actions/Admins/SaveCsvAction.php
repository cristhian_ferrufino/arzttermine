<?php

namespace NGS\Actions\Admins;

use NGS\Actions\BaseAction;
use NGS\Managers\BookingsManager;

class SaveCsvAction extends BaseAction {
		
	public function service(){
		$bookingsManager = BookingsManager::getInstance($this->config, $this->args);
		$count=$bookingsManager->getAllBookingsCount();
		$bookingsDtoArray=$bookingsManager->getBookingsFull(0,$count,'bookings_id',true);
		//$csvFieldNameArray=array('id','created_at','appointment_start_at','status','first_name','last_name','insurance_text','returning_visitor_text',
		//						'user_id','user_name','location_id','location_name','medical_specialty_text','treatmenttype_text','zip','city','comment_intern');
		$csvFieldNameArray=array('id'=>array('dto'=>'','name'=>'Id'),'created_at'=>array('dto'=>'','name'=>'CreatedAt'),
								'appointment_start_at'=>array('dto'=>'','name'=>'AppointmentStartAt'),'status'=>array('dto'=>'','name'=>'Status'),
								'first_name'=>array('dto'=>'','name'=>'FirstName'),'last_name'=>array('dto'=>'','name'=>'LastName'),
								'insurance_text'=>array('dto'=>'','name'=>'InsuranceId'),'returning_visitor_text'=>array('dto'=>'','name'=>'ReturningVisitor'),
								'user_id'=>array('dto'=>'','name'=>'UserId'),'user_name'=>array('dto'=>'User','name'=>'FirstName'),
								'location_id'=>array('dto'=>'','name'=>'LocationId'),'location_name'=>array('dto'=>'Location','name'=>'Name'),
								'medical_specialty_text'=>array('dto'=>'User','name'=>'MedicalSpecialityIds'),
								'treatmenttype_text'=>array('dto'=>'TreatmentType','name'=>'NameEn'),'zip'=>array('dto'=>'Location','name'=>'Zip'),
								'city'=>array('dto'=>'Location','name'=>'City'),'comment_intern'=>array('dto'=>'','name'=>'CommentIntern'));	
													
		$csv=fopen("/Users/naghashyan/Downloads/test.csv", 'w');
		fputcsv($csv, $csvFieldNameArray);
		foreach ($bookingsDtoArray as $bookingDto) {
			$fieldValueArray=array();
			
			foreach($csvFieldNameArray as $fieldName){
				if($fieldName=='userName'){
					$user=$bookingDto->getUser();
					$fieldValue=$user->getFirstName().' '.$user->getLastName();
				}elseif($fieldName=='locationName'){
					$location=$bookingDto->getLocation();
					$fieldValue=$location->getName();
				}else{
					$functionName='get'.ucfirst($fieldName);
					$fieldValue=$bookingDto->$functionName();
				}
				
				if(empty($fieldValue)){
					$fieldValue=" ";
				}
				$fieldValueArray[]=$fieldValue;
			}
			fputcsv($csv, $fieldValueArray);
		}
		fclose($csv);
	}
}