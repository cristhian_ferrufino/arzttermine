<?php

namespace NGS\Actions\Admins;

use Arzttermine\Integration\Integration\Samedi\Types\CategoryEventType;
use NGS\Managers\DoctorsTreatmentTypesManager;
use NGS\Managers\MedicalSpecialtiesManager;
use NGS\Managers\SamediEventTypesCachesManager;
use NGS\Managers\SamediTreatmentTypesManager;
use NGS\Managers\TreatmentTypesManager;

class UpdateDoctorTtInDoctTtTableAction extends BaseAdminAction {

	public function service() {


		$set_samedi_tt_parent_ids = intval($_REQUEST['set_samedi_tt_parent_ids']);
		$user_id = intval($_REQUEST['user_id']);
		$category_id = $this->secure($_REQUEST['category_id']);
		$practice_id = $this->secure($_REQUEST['practice_id']);
		$samediTreatmenttypesManager = SamediTreatmentTypesManager::getInstance($this->config, $this->args);
		$samediEventTypesCachesManager = SamediEventTypesCachesManager::getInstance($this->config, $this->args);
		$doctorsTreatmentTypesManager = DoctorsTreatmentTypesManager::getInstance($this->config, $this->args);
		$treatmentTypesManager = TreatmentTypesManager::getInstance($this->config, $this->args);
		$medicalSpecialtiesManager = MedicalSpecialtiesManager::getInstance($this->config, $this->args);
		
		//this is the case when admin set the parent_ids of the doctor treatmenttypes and click save button
		if ($set_samedi_tt_parent_ids == 1) {
			$arz_tt_and_samedi_tt_array = json_decode($_REQUEST['arz_tt_and_samedi_tt_array']);
			foreach ($arz_tt_and_samedi_tt_array as $arzTTAndSamediTTIdsPair) {
				$samediTTId = $arzTTAndSamediTTIdsPair[0];
				$arzTTId = $arzTTAndSamediTTIdsPair[1];
				$samediTreatmenttypesManager->setParentId($samediTTId, $arzTTId);
			}
		}

		$categoryEventTypes = $samediEventTypesCachesManager->getCategoryEventTypes($practice_id, $category_id);
		if (empty($categoryEventTypes)) {
			echo "Error: Samedi Category Event Types not exists";
			exit;
		}
		$samediEmptyParentIdTTsDtos = array();
		$samediNonEmptyParentIdTTsDtos = array();
		foreach ($categoryEventTypes as $et) {
			$sEvType = new CategoryEventType($et);
			$evId = $sEvType->getId();
			$dto = $samediTreatmenttypesManager->getById($evId);
			if (!isset($dto)) {
				$dto = $samediTreatmenttypesManager->insertRow($sEvType);
			}
			
			if ($dto->getParentId() > 0 && !isset($_REQUEST['review_tts'])) {
				
				$samediNonEmptyParentIdTTsDtos [] = $dto;
			} else {				
				$samediEmptyParentIdTTsDtos[] = $dto;
			}
		}
		
		if (empty($samediEmptyParentIdTTsDtos)) {
			$doctorsTreatmentTypesManager->deleteDoctorAllTreatmentTypes($user_id);
			foreach ($samediNonEmptyParentIdTTsDtos as $samediNonEmptyParentIdTTDto) {
				$arztTtId = $samediNonEmptyParentIdTTDto->getParentId();
				$arzTTDto = $treatmentTypesManager->getById($arztTtId);
				assert(isset($arzTTDto));
				$mSpecId = $arzTTDto->getMedicalSpecialtyId();
				$doctorsTreatmentTypesManager->addTreatmentType($user_id, $mSpecId, $arztTtId);
			}
			echo "ok";
			exit;
		} else {
			$samediTTsDtosIdsNamesArray = $this->getSamediTTsDtosIdsNamesArray($samediEmptyParentIdTTsDtos);			
			$medicalSpecialtiesByDoctorId = $medicalSpecialtiesManager->getMedicalSpecialtiesByDoctorId($user_id);
			$doctorMedSpeciesWithTTs = array();
			$doctorMedSpeciesArray = array();
			foreach ($medicalSpecialtiesByDoctorId as $medDto) {
				$doctorMedSpeciesArray [] = array($medDto->getId(), $medDto->getNameSingularDe());
				$medId = $medDto->getId();
				$doctorMedSpeciesWithTTs[] = $medicalSpecialtiesManager->getMedicalSpecialtyWithTreatmentTypesById($medId);
			}
			$arztAllTTs = $this->getArzTTsMedDtosWithTTsIdsNamesArray($doctorMedSpeciesWithTTs);
			$ret = array($samediTTsDtosIdsNamesArray, $arztAllTTs, $doctorMedSpeciesArray);
			echo json_encode($ret);
			exit;
		}
	}

	private function getArzTTsMedDtosWithTTsIdsNamesArray($arzMedTTsDtos) {

		$ret = array();
		foreach ($arzMedTTsDtos as $arzMedTTsDto) {
			$treatmentTypesDtos = $arzMedTTsDto->getTreatmentTypesDtos();
			foreach ($treatmentTypesDtos as $arzTTDto) {
				$arzTTName = $arzTTDto->getNameDe() . ' (' . $arzMedTTsDto->getNameSingularDe() . ')';
				$ret[] = array($arzTTDto->getId(), $arzTTName);
			}
		}
		return $ret;
	}

	private function getSamediTTsDtosIdsNamesArray($samediTTsDtos) {
		$ret = array();
		foreach ($samediTTsDtos as $sTTDto) {
			$sTTid = $sTTDto->getId();
			$sTTName = $sTTDto->getName();
			$sTTParentId = $sTTDto->getParentId();
			$ret[] = array($sTTid, $sTTName, $sTTParentId);
		}
		return $ret;
	}

}