<?php

namespace NGS\Actions\Admins;

use NGS\Actions\BaseAction;
use NGS\Managers\UsersManager;
use NGS\Security\RequestGroups;
use NGS\Security\Users\AdminUser;

/**
 * @author Vahagn Sookiasian
 */
class LoginAction extends BaseAction {

	private $usersManager;

	public function service() {
		$this->checkEmailPasswordEmpty();
		$email = $this->secure($_REQUEST['email']);
		$password = $this->secure($_REQUEST['password']);

		$this->usersManager = new UsersManager($this->config, $this->args);

		$userDto = $this->usersManager->getUserByEmail($email);
		$this->checkUserDtoEmpty($userDto);

		$this->checkWrongAttempts($userDto);
		
		$user_id = $userDto->getId();		
		$hashSalt = $userDto->getPasswordHashSalt();
		$password_hash = hash('sha256', PASSWORD_FIX_SALT . $password . $hashSalt);
		$userDto = $this->usersManager->getUserByEmailAndPasswordHash($email, $password_hash);
		$this->checkUserDtoEmpty($userDto, $user_id);
		
		$this->usersManager->resetWrongLoginAttempt($user_id);
		
		$user = new AdminUser($userDto->getId());
		$user->setUniqueId($userDto->getPasswordHash());
		$this->sessionManager->setUser($user, true, true);
        $_SESSION["USERID"] = $userDto->getId();
		$this->redirect("ngsadmin");
	}

	public function checkUserDtoEmpty($userDto, $id = null) {
		if ($userDto == null) {
			if ($id > 0) {
				$this->usersManager->addWrongLoginAttempt($id);
			}
			$this->redirectToLoad("admins", "Main", array("error" => true, "error_message" => "Wrong  user/password combination!"));
		}
	}
	public function checkWrongAttempts($userDto) {
		$wrongLoginAttempt = $userDto->getWrongLoginAttempt();
		if ($wrongLoginAttempt == 10)
		{
			$this->redirectToLoad("admins", "Main", array("error" => true, "error_message" => "Your account is blocked!"));						
		}				
	}

	public function checkEmailPasswordEmpty() {
		if (empty($_REQUEST['email'])) {
			$this->redirectToLoad("admins", "Main", array("error" => true, "error_message" => "please input your email!"));
		}
		if (empty($_REQUEST['password'])) {
			$this->redirectToLoad("admins", "Main", array("error" => true, "error_message" => "please input password!"));
		}
	}

	public function getRequestGroup() {
		return RequestGroups::$guestRequest;
	}

}