<?php

namespace NGS\Actions\Admins;

use NGS\Managers\DoctorsManager;

class ImportIntegrationDoctorsAction extends BaseAdminAction {
	private $doctorsManager;

	public function service() {
		$this->doctorsManager = DoctorsManager::getInstance($this->config, $this->args);
		if (isset($_REQUEST['integration_id'])) {
			$integrationId = $this->secure($_REQUEST['integration_id']);
			if (isset($integrationId) && $integrationId == 5) {
				$allSamediDoctorsDtos = $this->doctorsManager->getAllSamediDoctorsDtos();
			}
		}
		
		if (isset($_REQUEST['selected_doctors'])) {
			$doctorsToBeImport = array();
			$allSamediDoctorsDtos = $this->doctorsManager->getAllSamediDoctorsDtos();			
			$practiceAndCategoryIds = $_REQUEST['selected_doctors'];
			foreach ($practiceAndCategoryIds as $pc => $selected) {
				list($practiceId, $categoryId) = explode('^', $pc);				
				$doctorsToBeImport[] = $this->getDoctorDtoByPracticeIdAndCategoryId($allSamediDoctorsDtos, $practiceId, $categoryId);
			}
			$this->doctorsManager->insertDoctors($doctorsToBeImport, isset($_REQUEST['replace_doctors']), isset($_REQUEST['replace_locations']));
			$_REQUEST["p"] = "importdoctors";
			$this->redirectToLoad("admins", "Main", array("inserted_doctors" => $doctorsToBeImport,"request"=>$_REQUEST));
		}

		$_REQUEST["p"] = "importdoctors";
		$this->redirectToLoad("admins", "Main", array("integration_doctors" => isset($allSamediDoctorsDtos) ? $allSamediDoctorsDtos : null,"request"=>$_REQUEST));

	}

	private function getDoctorDtoByPracticeIdAndCategoryId($doctorsDtos, $practiceId, $categoryId)
    {
		foreach ($doctorsDtos as $doctorDto) {
			$samediDataDtos = $doctorDto->getSamediDatasDtos();
			foreach ($samediDataDtos as $key => $sdd) {
				$pi = $sdd->getPracticeId();
				$ci = $sdd->getCategoryId();
				if ($pi == $practiceId && $ci == $categoryId) {
					$clone = $doctorDto->getDoctorDataOnlyClone();
					$locationDtos = $doctorDto->getLocationDtos();
					$clone->addLocationDto($locationDtos[$key]);
					$clone->addSamediDataDto($sdd);
					$eventTypesDtos = $doctorDto->getSamediEventTypesCachesDtos();
					$clone->addSamediEventTypesCachesDto($eventTypesDtos[$key]);
					return $clone;
				}
			}
		}
        
        return null;
	}

}