<?php

namespace NGS\Actions\Admins;

use Arzttermine\Booking\Booking as StandardBooking;
use Arzttermine\Integration\Booking;
use NGS\Managers\BookingsManager;
use NGS\Managers\DoctorsManager;
use NGS\Managers\SamediBookingsManager;

class CancelAction extends BaseAdminAction {

	public function service() {

		// what is the assert() for if there is no assert_options() call?!?!
		// I don't know...

		$bookingManager = BookingsManager::getInstance($this->config, $this->args);
		$doctorsManager = DoctorsManager::getInstance($this->config, $this->args);
		$bookingId = $this->secure($_REQUEST['id']);
		$bookingDto = $bookingManager->getBookingByIdFull($bookingId);
		$locationDto = $bookingDto->getLocation();
		assert(isset($locationDto));
		$integrationId = $locationDto->getIntegrationId();
		assert($integrationId > 0);
		$bookingStatus = $bookingDto->getStatus();
		$bookingIntegrationStatus = $bookingDto->getIntegrationStatus();
		assert($bookingIntegrationStatus == BOOKING_INTEGRATION_STATUS_BOOKED);

		// check if the status is correct for this action
		assert(in_array($bookingStatus, Booking::getReceivedStatuses()));

		if ($integrationId == $GLOBALS['CONFIG']['INTEGRATIONS'][5]['ID']) {
			$samediBookingsManager = SamediBookingsManager::getInstance($this->config, $this->args);
			$samediBookingDto = $samediBookingsManager->getByBookingId($bookingDto->getId());
			assert(isset($samediBookingDto));
			$resultJson = $samediBookingDto->getResultJson();
			$bookingResultObject = json_decode($resultJson);
			$bookingResultObject = new \SBooking($bookingResultObject);
			$samediSideBookingId = $bookingResultObject->getId();			
			$practiceId = $bookingResultObject->getPracticeId();
			$cancelResult = $doctorsManager->cancelBookedAppointment($practiceId, $samediSideBookingId);
			if ($cancelResult) {
				$bookingManager->setStatus($bookingDto->getId(), StandardBooking::STATUS_CANCELLED_BY_PATIENT);
				$bookingManager->setIntegrationStatus($bookingDto->getId(), BOOKING_INTEGRATION_STATUS_CANCELED);
				$this->redirect("ngsadmin/samedibookings?status=ok");
			} else {
				$this->redirect("ngsadmin/samedibookings?status=error");
			}
		}		
		$this->redirect("ngsadmin/samedibookings?status=error");
		
	}

}