<?php

namespace NGS\Actions\Admins;

use NGS\Actions\BaseAction;
use NGS\Security\RequestGroups;

/**
 * @author Vahagn Sookiasian
 */
abstract class BaseAdminAction extends BaseAction {

public function getRequestGroup() {
		return RequestGroups::$adminsRequest;
	}
}