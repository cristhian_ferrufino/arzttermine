<?php

namespace NGS\Actions\Admins;

class DeleteEntryAction extends BaseAdminAction {
		
	public function service(){
		$path=$this->secure($_REQUEST['path']);
		
		foreach ($_REQUEST as $name => $value) {
			if(strpos($name,'->id')){
				$mapperName=substr($name, 0, strpos($name,'->id'))."Mapper";
				$id=$value;
			}
		}
		
		$mapper=$mapperName::getInstance();
		$mapper->deleteByPK($id);
		$this->redirect("ngsadmin/".$path."?deleteEntry=success");
	}
	

}