<?php

namespace NGS\Actions\Admins;

class NewEntryAction extends BaseAdminAction {
		
	public function service(){
		$path=$this->secure($_REQUEST['path']);
		$mapperName=$this->secure($_REQUEST['mapperName']);
		$mapper=$mapperName::getInstance();
		$dto=$mapper->createDto();
		$dtoMapArray=$dto->getMapArray();
		foreach ($dtoMapArray as $dtoFieldName) {
			$functionName='set'.ucfirst($dtoFieldName);
			$dto->$functionName(NULL);
		}
		
		if(isset($_REQUEST['firstName']) && empty($_REQUEST['firstName'])){
			$this->redirect("ngsadmin/".$path."/newentry?error=firstName");
		}
		
		if(isset($_REQUEST['lastName']) && empty($_REQUEST['lastName'])){
			$this->redirect("ngsadmin/".$path."/newentry?error=lastName");
		}
		
		if(isset($_REQUEST['userId']) && empty($_REQUEST['userId'])){
			$this->redirect("ngsadmin/".$path."/newentry?error=userId");
		} 
		if(isset($_REQUEST['locationId']) && empty($_REQUEST['locationId'])){
			$this->redirect("ngsadmin/".$path."/newentry?error=locationId");
		}
		
		foreach ($_REQUEST as $key => $value) {
			if(in_array($key, array_values($dtoMapArray))){
				$functionName='set'.ucfirst($key);
				$dto->$functionName($value);
			}
		}
		
		$createdAt=date('Y-m-d H:i:s');
		$dto->setCreatedAt($createdAt);
		
		$id=$mapper->insertDto($dto);
		
		$this->redirect("ngsadmin/".$path."?newentry=success&newId=".$id);
	
	}
	

}