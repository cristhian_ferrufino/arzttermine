<?php

namespace NGS\DAL\Mappers;

use NGS\Framework\DAL\Mappers\AbstractMapper;
use NGS\DAL\DTO\UsersDto;

class UsersMapper extends AbstractMapper {

    /**
     * @var self
     */
    private static $instance = null;

    /**
     * @var string
     */
    protected $tableName = 'users';

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @see AbstractMapper::createDto()
     */
    public function createDto()
    {
        return new UsersDto();
    }

    /**
     * Get a user given their ID and password hash. Used to check existing passwords in the event of a change
     *
     * @param string $id
     * @param string $passHash
     *
     * @return array|null
     */
    public function getUserByIdAndHash($id, $passHash)
    {
        $sql = "SELECT * from `%s` WHERE `id` = %s AND `password_hash` = '%s'";

        $query = sprintf($sql, $this->getTableName(), $id, $passHash);
        $result = $this->fetchRows($query);

        if (count($result) === 1) {
            return $result[0];
        }

        return null;
    }

    /**
     * Get a user by email and password hash. Useful for login/existing user checks
     *
     * @param string $email
     * @param string $passHash
     *
     * @return array|null
     */

    public function getUserByEmailAndPasswordHash($email, $passHash)
    {
        $sql = "SELECT * from `%s` WHERE `email` = '%s' AND `password_hash` = '%s'";

        $query = sprintf($sql, $this->getTableName(), $email, $passHash);
        $result = $this->fetchRows($query);

        if (count($result) === 1) {
            return $result[0];
        }

        return null;
    }

    /**
     * Get user by their email address
     *
     * @param string $email
     *
     * @return array|null
     */
    public function getUserByEmail($email)
    {
        $sql = "SELECT * from `%s` WHERE `email` = '%s'";

        $query = sprintf($sql, $this->getTableName(), $email);
        $result = $this->fetchRows($query);

        if (count($result) === 1) {
            return $result[0];
        }

        return null;
    }

    /**
     * Get a user if they have a password reset code
     *
     * @param string $resetPasswordCode
     *
     * @return array|null
     */
    public function getUserByResetPasswordCode($resetPasswordCode)
    {
        $sql = "SELECT * from `%s` WHERE `reset_password_code` = '%s'";

        $query = sprintf($sql, $this->getTableName(), $resetPasswordCode);
        $result = $this->fetchRows($query);

        if (count($result) === 1) {
            return $result[0];
        }

        return null;
    }

    /**
     * Get user group settings for a user
     *
     * @param int $user_id
     *
     * @return array
     */
    public function getUserGroupAccesses($user_id)
    {
        $sql
            = "SELECT `group_accesses`.`access_id` as `group_access_id`
			FROM `%s`
			LEFT JOIN `group_accesses` ON `%s`.`group_id` = `group_accesses`.`group_id`
			WHERE `%s`.`id` = %d";

        $query = sprintf($sql, $this->getTableName(), $this->getTableName(), $this->getTableName(), $user_id);

        return $this->fetchRows($query);
    }

    /**
     * As above, with no additional options
     *
     * @return array
     */
    public function getAllUsersNoLimit()
    {
        return $this->getAllUsers();
    }

    /**
     * Grab all the users with an offset and limit, and optionally order by a field
     *
     * @param int $offset
     * @param int $limit
     * @param string $orderByFieldName
     *
     * @return array
     */
    public function getAllUsers($offset = 0, $limit = 0, $orderByFieldName = '')
    {
        $sql = "SELECT `%s`.* FROM `%s` %s %s";
        $order_by = '';
        $offset_limit = '';

        if ($orderByFieldName) {
            $order_by = sprintf("ORDER BY `%s`.`%s`", $this->getTableName(), trim($orderByFieldName));
        }

        if ($limit || $offset) {
            $offset_limit = sprintf("LIMIT %d, %d", intval($offset), intval($limit));
        }

        $query = sprintf($sql, $this->getTableName(), $this->getTableName(), $order_by, $offset_limit);

        return $this->fetchRows($query);
    }

    /**
     * Get the total number of users
     *
     * @return mixed field Value
     */
    public function getAllUsersCount()
    {
        $sql = "SELECT count(`id`) AS `count`  FROM `%s`";

        $query = sprintf($sql, $this->getTableName());

        return $this->fetchField($query, 'count');
    }

    /**
     * Delete a single doctor's specialty
     *
     * @param int $doctorId
     * @param int $medicalSpecialtyId
     *
     * @return bool|int
     */
    public function deleteDoctorMedicalSpecialty($doctorId, $medicalSpecialtyId)
    {
        $doctor = $this->selectByPK($doctorId);
        $medSpecIdsString = $doctor->getMedicalSpecialtyIds();
        $medSpecIdsArray = explode(',', $medSpecIdsString);

        if (count($medSpecIdsArray) === 1) {
            return false;
        }

        $key = array_search($medicalSpecialtyId, $medSpecIdsArray);

        if ($key !== false) {
            unset($medSpecIdsArray[$key]);
        }

        $medSpecIdsString = implode(',', $medSpecIdsArray);

        return $this->updateTextField($doctorId, 'medical_specialty_ids', $medSpecIdsString);
    }

    /**
     * @param $practiceName
     * @param $userName
     * @param $userLName
     *
     * @return null
     */
    public function getUserByNameAndLNameANDPracticeName($practiceName, $userName, $userLName)
    {
        $sql
            = "SELECT `%s`.`email`
        	FROM `%s`
        	INNER JOIN `locations` ON FIND_IN_SET(`%s`.`id`,`locations`.`member_ids`) 
				AND `locations`.`name`='%s'
			WHERE `users`.`first_name` = '%s'
				AND `users`.`last_name` = '%s'";

        $query = sprintf($sql, $this->getTableName(), $this->getTableName(), $this->getTableName(), $practiceName, $userName, $userLName);

        $results = $this->fetchRows($query);

        if (count($results) === 1) {
            return $results[0];
        }

        return null;
    }

    /**
     * @param $activationCode
     *
     * @return array
     */
    public function getUserByActivationCode($activationCode)
    {
        $sql = "SELECT * FROM `%s` WHERE `activation_code`='%s'";

        $query = sprintf($sql, $this->getTableName(), $activationCode);
        $results = $this->fetchRows($query);

        return $results[0];
    }

    /**
     * Get all users by status
     *
     * @param int $status
     *
     * @return array
     */
    public function getUsersByStatus($status)
    {
        $sql = "SELECT * FROM `%s` WHERE `status`=%d";

        $query = sprintf($sql, $this->getTableName(), $status);
        $result = $this->fetchRows($query);

        return $result;
    }

    /**
     * Get all users that have no Treatment Type
     *
     * @param int $status
     *
     * @return array
     */
    public function getUsersThatNotHaveTT($status)
    {
        $sql
            = "SELECT `%s`.* 
               FROM `%s`
               LEFT JOIN doctors_treatment_types ON `%s`.id= doctors_treatment_types.`doctor_id`
               WHERE `%s`.`status`=%d
                   AND doctors_treatment_types.`doctor_id` IS NULL ";

        $query = sprintf($sql, $this->getTableName(), $this->getTableName(), $this->getTableName(), $this->getTableName(), $status);

        $result = $this->fetchRows($query);

        return $result;
    }

    /**
     * Grab all doctor users
     *
     * @return array
     */
    public function getAllDoctorsNoLimit()
    {
        $sql = "SELECT * FROM `%s` WHERE `group_id`=3 ";

        $query = sprintf($sql, $this->getTableName());

        return $this->fetchRows($query);
    }

    /**
     * @return array
     */
    public function getAllDoctorNames()
    {
        $sqlQuery = sprintf("SELECT `id`,`title`,`first_name`,`last_name` FROM `%s` WHERE `group_id`=3 ", $this->getTableName());

        return $this->fetchRows($sqlQuery);
    }

    /**
     * Get a single user by their slug
     *
     * @param string $slug
     *
     * @return array
     */
    public function getUserBySlug($slug)
    {
        $sql = "SELECT * FROM `%s` WHERE `slug`='%s' LIMIT 1;";
        $query = sprintf($sql, $this->getTableName(), mysql_real_escape_string(trim($slug)));
        $ret = $this->fetchRows($query);
        if (count($ret) === 1) {
            return $ret [0];
        } else {
            return null;
        }
    }

    /**
     * Get fixed week users
     *
     * @param int $offset
     * @param int $limit
     *
     * @return array
     */
    public function getFixedWeekUsers($offset, $limit)
    {
        $sql = "SELECT * FROM `%s` WHERE `fixed_week`=1 AND `group_id`=3 LIMIT %d, %d";
        $query = sprintf($sql, $this->getTableName(), $offset, $limit);

        return $this->fetchRows($query);
    }
}
