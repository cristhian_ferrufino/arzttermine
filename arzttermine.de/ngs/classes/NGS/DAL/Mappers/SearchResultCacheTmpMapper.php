<?php

namespace NGS\DAL\Mappers;

use NGS\DAL\DTO\SearchResultCacheDto;
use NGS\Framework\DAL\Mappers\AbstractMapper;

class SearchResultCacheTmpMapper extends AbstractMapper {

    /**
     * @var self
     */
    private static $instance = null;

    /**
     * @var string
     */
    protected $tableName = 'search_result_cache_tmp';

    /**
     * @var string
     */
    protected $originalTableName = 'search_result_cache';

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @see AbstractMapper::getTableName()
     */
    public function setTableName($tn)
    {
        $this->tableName = $tn;
    }

    public function swapTableName()
    {
        $sw = $this->tableName;
        $this->tableName = $this->originalTableName;
        $this->originalTableName = $sw;
        
        return $this;
    }

    public function deleteDoctorCaches($userId, $locationId)
    {
        $sqlQuery = sprintf("DELETE FROM `%s` WHERE `user_id`=%d AND `location_id`=%d ", $this->getTableName(), $userId, $locationId);
        $this->dbms->query($sqlQuery);
    }

    /**
     * @see AbstractMapper::getTableName()
     */
    public function getTableName()
    {
        if (isset($GLOBALS["CONFIG"]['SEARCH_RESULT_CACHE_TABLE_PREVIEW']) && $GLOBALS["CONFIG"]['SEARCH_RESULT_CACHE_TABLE_PREVIEW'] == true) {
            return 'search_result_cache_preview';
        }

        return $this->tableName;
    }

    public function truncateTempTable()
    {
        $sqlQuery = sprintf("TRUNCATE TABLE `%s`", $this->getTableName());
        $this->dbms->query($sqlQuery);
    }

    public function swapWithOriginalTable()
    {
        $sqlQuery = sprintf("RENAME TABLE `%s` TO `%s`, `%s` To `%s`, `%s` To `%s`", $this->getOriginalTableName(), 'tmptablenameforswap', $this->getTableName(), $this->getOriginalTableName(), 'tmptablenameforswap', $this->getTableName());
        $this->dbms->query($sqlQuery);
    }

    /**
     * @see AbstractMapper::getTableName()
     */
    public function getOriginalTableName()
    {
        if (isset($GLOBALS["CONFIG"]['SEARCH_RESULT_CACHE_TABLE_PREVIEW']) && $GLOBALS["CONFIG"]['SEARCH_RESULT_CACHE_TABLE_PREVIEW'] == true) {
            return 'search_result_cache_preview';
        }

        return $this->originalTableName;
    }

    public function clearDoctorData($doctorId, $fromOriginalTable)
    {
        $sqlQuery = sprintf("DELETE FROM `%s` WHERE `user_id`=%d", $fromOriginalTable ? $this->getOriginalTableName() : $this->getTableName(), $doctorId);
        $this->dbms->query($sqlQuery);
    }

    public function getSearchResultCachesByFilters($userIds, $locationIds, $_datetime_start, $_datetime_end, $insuranceId, $treatmenttypeId = null)
    {
        $ttSubQuery = '';
        if (!empty($treatmenttypeId)) {
            $ttSubQuery = sprintf(" AND FIND_IN_SET(%d,`treatmenttype_ids`)", $treatmenttypeId);
        }
        $query = sprintf(
            "SELECT * FROM `%s` WHERE `user_id` IN (%s) AND `location_id` IN (%s) AND `date`>='%s' AND `date`<='%s' AND FIND_IN_SET(%d,`insurance_ids`) %s GROUP BY `user_id`,`location_id`,`appointment` ",
            $this->getOriginalTableName(), implode(',', array_unique($userIds)), implode(',', array_unique($locationIds)), $_datetime_start, $_datetime_end, $insuranceId, $ttSubQuery
        );
        $res = $this->dbms->query($query);
        $results = $this->dbms->getResultArray($res);
        $resultArr = array();
        foreach ($results as $result) {
            $dto = $this->createDto();
            $this->initializeDto($dto, $result);
            $userId = $dto->getUserId();
            $locationId = $dto->getLocationId();
            $resultArr[$userId][$locationId][] = $dto;
        }

        return $resultArr;
    }

    /**
     * @see AbstractMapper::createDto()
     */
    public function createDto()
    {
        return new SearchResultCacheDto();
    }

    public function getNextAvailableSearchResultCachesByFilters($date_start, $userId, $locationId, $insuranceId, $treatmenttypeId)
    {
        $ttSubQuery = '';
        if (!empty($treatmenttypeId)) {
            $ttSubQuery = sprintf(" AND FIND_IN_SET(%d,`treatmenttype_ids`)", $treatmenttypeId);
        }
        $query = sprintf(
            "SELECT * FROM `%s` WHERE `user_id`=%d AND `location_id`=%d AND `date`>'%s' AND FIND_IN_SET(%d,`insurance_ids`) %s ORDER BY `appointment`",
            $this->getOriginalTableName(), $userId, $locationId, $date_start, $insuranceId, $ttSubQuery
        );

        return $this->fetchRows($query);
    }

    public function deleteByUserIdAndLocationIdAndDate($userId, $locationId, $date)
    {
        $query = sprintf("DELETE FROM `%s` WHERE `user_id`=%d AND `location_id`=%d AND `date`='%s' ", $this->getOriginalTableName(), $userId, $locationId, $date);

        return $this->dbms->query($query);
    }

    public function insertAppointmentsFromSubqueryArray($insertQueryPartsArray)
    {
        $query = sprintf(
            "INSERT INTO `%s` (`id`,`date`,`insurance_ids`,`treatmenttype_ids`,`user_id`,`location_id`,`appointment`,`last_updated_time`) values %s",
            $this->getTableName(), implode(',', $insertQueryPartsArray)
        );

        $res = $this->dbms->query($query);
        if ($res) {
            // Get inserted id.
            $result = $this->dbms->getLastInsertedId();

            return $result;
        }

        return -1;
    }
}
