<?php

namespace NGS\DAL\Mappers;

use NGS\DAL\DTO\MailingsDto;
use NGS\Framework\DAL\Mappers\AbstractMapper;

class MailingsMapper extends AbstractMapper {

    private static $instance = null;

    protected $tableName = 'mailings';

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function createDto()
    {
        return new MailingsDto();
    }
}