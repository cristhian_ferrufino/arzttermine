<?php

namespace NGS\DAL\Mappers;

use NGS\DAL\DTO\SamediPracticesDto;
use NGS\Framework\DAL\Mappers\AbstractMapper;

class SamediPracticesMapper extends AbstractMapper {

    /**
     * @var self
     */
    private static $instance = null;

    /**
     * @var string
     */
    protected $tableName = 'samedi_practices';

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @see AbstractMapper::createDto()
     */
    public function createDto()
    {
        return new SamediPracticesDto();
    }

    public function truncateTable()
    {
        $sqlQuery = sprintf("TRUNCATE TABLE `%s`", $this->getTableName());
        $this->dbms->query($sqlQuery);
    }
}