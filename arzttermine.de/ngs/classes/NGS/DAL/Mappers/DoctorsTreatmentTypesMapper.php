<?php

namespace NGS\DAL\Mappers;

use NGS\DAL\DTO\DoctorsTreatmentTypesDto;
use NGS\DAL\DTO\TreatmentTypesDto;
use NGS\Framework\DAL\Mappers\AbstractMapper;

class DoctorsTreatmentTypesMapper extends AbstractMapper {

    /**
     * @var self
     */
    private static $instance = null;

    /**
     * @var string
     */
    protected $tableName = 'doctors_treatment_types';

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @see AbstractMapper::createDto()
     */
    public function createDto()
    {
        return new DoctorsTreatmentTypesDto();
    }

    /**
     * @param int $doctorId
     *
     * @return bool
     */
    public function deleteDoctorAllTreatmentTypes($doctorId)
    {
        $sqlQuery = sprintf("DELETE FROM `%s` WHERE `doctor_id`=%d", $this->getTableName(), $doctorId);

        return $this->dbms->query($sqlQuery);
    }

    /**
     * @param int $doctorId
     * @param int $medSpecId
     *
     * @return array
     */
    public function getByDoctorIdAndMedSpecId($doctorId, $medSpecId)
    {
        $sqlQuery = sprintf("SELECT * FROM `%s` WHERE `doctor_id`=%d AND `medical_speciality_id`=%d", $this->getTableName(), $doctorId, $medSpecId);
        $rows = $this->fetchRows($sqlQuery);

        return $rows;
    }

    /**
     * @param int $doctorId
     *
     * @return array
     */
    public function getByDoctorId($doctorId)
    {
        $sqlQuery = sprintf("SELECT * FROM `%s` WHERE `doctor_id`=%d", $this->getTableName(), $doctorId);
        $rows = $this->fetchRows($sqlQuery);

        return $rows;
    }

    /**
     * @param int $doctorId
     *
     * @return mixed
     */
    public function getTtIdsByDoctorId($doctorId)
    {
        $sqlQuery = sprintf("SELECT GROUP_CONCAT(treatment_type_id) AS`tt_ids` FROM `%s` WHERE `doctor_id`=%d", $this->getTableName(), $doctorId);
        $field = $this->fetchField($sqlQuery, 'tt_ids');

        return $field;
    }

    /**
     * @param int $doctorId
     *
     * @return array
     */
    public function getDoctorAllTreatmentTypes($doctorId)
    {
        $treatmentTypesMapper = TreatmentTypesMapper::getInstance();
        $q = $treatmentTypesMapper->getAllFieldsAsQueryString();
        $query = sprintf(
            "SELECT %s FROM `%s` INNER JOIN `%s` ON `%s`.`treatment_type_id`=`%s`.`id` WHERE `doctor_id`=%d ", $q, $this->getTableName(), $treatmentTypesMapper->getTableName(), $this->getTableName(), $treatmentTypesMapper->getTableName(), $doctorId
        );
        $rows = $treatmentTypesMapper->fetchRows($query);

        return $rows;
    }

    /**
     * @param int $doctorId
     * @param int $medicalSpecialityId
     * @param int $treatmentTypeId
     *
     * @return DoctorsTreatmentTypesDto[]
     */
    public function getByUserIdMedSpecIdTTId($doctorId, $medicalSpecialityId, $treatmentTypeId)
    {
        $query = sprintf(
            "SELECT * FROM `%s` WHERE `doctor_id`=%d AND `medical_speciality_id`=%d AND `treatment_type_id`=%d ",
            $this->getTableName(), $doctorId, $medicalSpecialityId, $treatmentTypeId
        );
        $rows = $this->fetchRows($query);

        return $rows;
    }

    /**
     * @param int $doctorId
     * @param int[] $treatmentTypeIdsArray
     *
     * @return int
     */
    public function addTreatmentTypes($doctorId, $treatmentTypeIdsArray)
    {
        $treatmentTypesMapper = TreatmentTypesMapper::getInstance();
        $treatmentTypes = $treatmentTypesMapper->getByIds($treatmentTypeIdsArray);
        $query = sprintf("INSERT INTO `%s` (`doctor_id`,`medical_speciality_id`,`treatment_type_id`) VALUES ", $this->getTableName());
        
        /** @var TreatmentTypesDto $treatmentType */
        foreach ($treatmentTypes as $treatmentType) {
            $query .= sprintf("(%d,%d,%d),", $doctorId, $treatmentType->getMedicalSpecialtyId(), $treatmentType->getId());
        }
        $query = substr($query, 0, -1);
        $res = $this->dbms->query($query);
        if ($res) {
            $result = $this->dbms->getLastInsertedId();

            return $result;
        }

        return -1;
    }

    /**
     * @param int $doctorId
     * @param int[] $treatmentTypeIds
     *
     * @return int
     */
    public function addTreatmentTypesForDoctor($doctorId, $treatmentTypeIds)
    {
        $treatmentTypesMapper = TreatmentTypesMapper::getInstance();
        $treatmentTypes = $treatmentTypesMapper->getByIds($treatmentTypeIds);
        $query = sprintf("INSERT INTO `%s` (`doctor_id`, `medical_speciality_id`, `treatment_type_id`) VALUES ", $this->getTableName());

        /** @var TreatmentTypesDto $treatmentType */
        foreach ($treatmentTypes as $treatmentType) {
            $query .= sprintf("(%d,%d,%d),", $doctorId, $treatmentType->getMedicalSpecialtyId(), $treatmentType->getId());
        }
        $query = substr($query, 0, -1);
        $res = $this->dbms->query($query);
        if ($res) {
            $result = $this->dbms->getLastInsertedId();

            return $result;
        }

        return -1;
    }

    /**
     * @param int $doctorId
     * @param int $medicalSpecialtyId
     *
     * @return bool
     */
    public function deleteDoctorTreatmentTypeByMedSpecId($doctorId, $medicalSpecialtyId)
    {
        $query = sprintf("DELETE FROM `%s` WHERE `doctor_id`=%d AND `medical_speciality_id`=%d", $this->getTableName(), $doctorId, $medicalSpecialtyId);

        return $this->dbms->query($query);
    }
}