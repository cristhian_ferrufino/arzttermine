<?php

namespace NGS\DAL\Mappers;

use NGS\DAL\DTO\TreatmentTypesDto;
use NGS\Framework\DAL\Mappers\AbstractMapper;

class TreatmentTypesMapper extends AbstractMapper {

    /**
     * @var self
     */
    private static $instance = null;

    /**
     * @var string
     */
    protected $tableName = 'treatmenttypes';

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @see AbstractMapper::createDto()
     */
    public function createDto()
    {
        return new TreatmentTypesDto();
    }

    /**
     * @param $ids_array
     *
     * @return TreatmentTypesDto
     */
    public function getByIds($ids_array)
    {
        $idsImploded = implode(',', $ids_array);
        $sqlQuery = sprintf("SELECT * FROM `%s` WHERE `id` IN (%s) ORDER BY FIND_IN_SET(`id`,'%s')", $this->getTableName(), $idsImploded, $idsImploded);
        $rows = $this->fetchRows($sqlQuery);

        return $rows;
    }

    public function getNamesByIds($ids_array, $langCode)
    {
        $idsImploded = implode(',', $ids_array);
        $sqlQuery = sprintf("SELECT group_concat( name_%s SEPARATOR  ';') as `names` FROM  `%s` WHERE `id` IN (%s) ORDER BY FIND_IN_SET(`id`,'%s')", $langCode, $this->getTableName(), $idsImploded, $idsImploded);
        $names = $this->fetchField($sqlQuery, 'names');

        return explode(';', $names);
    }

    public function getDoctorAllTreatmentTypes($doctorId)
    {
        $doctorsTreatmentTypesMapper = DoctorsTreatmentTypesMapper::getInstance();
        $query = sprintf(
            "SELECT `%s`.* FROM `%s` INNER JOIN `%s` ON `%s`.`treatment_type_id`=`%s`.`id` WHERE `doctor_id`=%d ", $this->getTableName(),
            $doctorsTreatmentTypesMapper->getTableName(), $this->getTableName(), $doctorsTreatmentTypesMapper->getTableName(), $this->getTableName(), $doctorId
        );
        $rows = $this->fetchRows($query);

        return $rows;
    }
}