<?php

namespace NGS\DAL\Mappers;

use Arzttermine\Application\Application;
use Arzttermine\Event\Event;
use NGS\DAL\DTO\MedicalSpecialtiesDto;
use NGS\Framework\DAL\Mappers\AbstractMapper;

class MedicalSpecialtiesMapper extends AbstractMapper {

    /**
     * @var self
     */
    private static $instance = null;

    /**
     * @var string
     */
    protected $tableName = 'medical_specialties';

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function getAllMedicalSpecialtiesWithTreatmentTypes()
    {
        $treatmentTypesMapper = TreatmentTypesMapper::getInstance();
        $q = $this->getAllFieldsAsQueryString(true);
        $q .= ',' . $treatmentTypesMapper->getAllFieldsAsQueryString(true);
        $where = ' WHERE 1=1 ';

        $event = Application::getInstance()->container->get('dispatcher')->dispatch('medical_specialty.query_where', new Event($where));
        $where = $event->data;
        
        $query = sprintf(
            "SELECT %s 
            FROM `%s` 
            LEFT JOIN `%s` ON `%s`.`id`=`%s`.`medical_specialty_id` %s",
            $q,
            $this->getTableName(),
            $treatmentTypesMapper->getTableName(),
            $this->getTableName(),
            $treatmentTypesMapper->getTableName(),
            $where
        );
        
        $res = $this->dbms->query($query);
        $results = $this->dbms->getResultArray($res);
        $existingMedicalSpecialtiesIdsArray = array();
        
        foreach ($results as $result) {
            $treatmentTypeDto = $treatmentTypesMapper->createDto();

            if (!isset($existingMedicalSpecialtiesIdsArray[$result["medical_specialties_id"]])) {
                $medicalSpecialtyDto = $this->createDto();
                $this->initializeDto($medicalSpecialtyDto, $result, true);
                $existingMedicalSpecialtiesIdsArray[$medicalSpecialtyDto->getId()] = $medicalSpecialtyDto;
            } else {
                $medicalSpecialtyDto = $existingMedicalSpecialtiesIdsArray[$result["medical_specialties_id"]];
            }

            $treatmentTypesMapper->initializeDto($treatmentTypeDto, $result, true);
            if ($treatmentTypeDto->getId() > 0) {
                $medicalSpecialtyDto->addTreatmentTypeDto($treatmentTypeDto);
            }

            $existingMedicalSpecialtiesIdsArray[$medicalSpecialtyDto->getId()] = $medicalSpecialtyDto;
        }

        $resultArr = array_values($existingMedicalSpecialtiesIdsArray);
        
        return $resultArr;
    }

    /**
     * @see AbstractMapper::createDto()
     */
    public function createDto()
    {
        return new MedicalSpecialtiesDto();
    }

    public function getMedicalSpecialtyWithTreatmentTypesById($id)
    {
        $treatmentTypesMapper = TreatmentTypesMapper::getInstance();
        $q = $this->getAllFieldsAsQueryString(true);
        $q .= ',' . $treatmentTypesMapper->getAllFieldsAsQueryString(true);

        $where = sprintf(
            "WHERE `%s`.`id`=%d",
            $this->getTableName(),
            $id
        );

        $event = Application::getInstance()->container->get('dispatcher')->dispatch('medical_specialty.query_where', new Event($where));
        $where = $event->data;
        
        $query = sprintf(
            "SELECT %s 
            FROM `%s` 
            LEFT JOIN `%s` ON `%s`.`id`=`%s`.`medical_specialty_id` 
            %s",
            $q,
            $this->getTableName(),
            $treatmentTypesMapper->getTableName(),
            $this->getTableName(),
            $treatmentTypesMapper->getTableName(),
            $where
        );
        $res = $this->dbms->query($query);
        $results = $this->dbms->getResultArray($res);

        $medicalSpecialtyDto = $this->createDto();
        
        if (count($results)) {
            $this->initializeDto($medicalSpecialtyDto, $results[0], true);
            foreach ($results as $result) {
                if ($result["treatmenttypes_id"] > 0) {
                    $tretmentTypeDto = $treatmentTypesMapper->createDto();
                    $treatmentTypesMapper->initializeDto($tretmentTypeDto, $result, true);
                    $medicalSpecialtyDto->addTreatmentTypeDto($tretmentTypeDto);
                }
            }
        }
        
        return $medicalSpecialtyDto;
    }

    public function getMedicalSpecialtiesByDoctorId($doctorId)
    {
        $doctorsMapper = DoctorsMapper::getInstance();
        $q = $this->getAllFieldsAsQueryString();
        
        $where = sprintf(
            "WHERE `%s`.`id`=%d ",
            $doctorsMapper->getTableName(),
            $doctorId
        );

        $event = Application::getInstance()->get('dispatcher')->dispatch('medical_specialty.query_where', new Event($where));
        $where = $event->data;
        
        $query = sprintf(
            "SELECT %s 
            FROM `%s` 
            INNER JOIN `%s` ON FIND_IN_SET(`%s`.`id`,`%s`.`medical_specialty_ids`) 
            %s",
            $q,
            $this->getTableName(),
            $doctorsMapper->getTableName(),
            $this->getTableName(),
            $doctorsMapper->getTableName(),
            $where
        );

        return $this->fetchRows($query);
    }

    public function getByIds($medSpecIdsString)
    {
        $query = sprintf("SELECT * FROM `%s` WHERE `id` IN (%s)", $this->getTableName(), $medSpecIdsString);

        return $this->fetchRows($query);
    }

    public function getMedicalSpecialityIdsText($medSpecIds, $lanCode, $delimiter, $singular)
    {
        $colName = 'name_' . ($singular ? 'singular_' : 'plural_') . $lanCode;
        $query = sprintf("SELECT GROUP_CONCAT(`%s` SEPARATOR '%s') as `res` FROM `%s` WHERE FIND_IN_SET (`id` , '%s')", $colName, $delimiter, $this->getTableName(), $medSpecIds);

        return $this->fetchField($query, 'res');
    }
}