<?php

namespace NGS\DAL\Mappers;

use NGS\DAL\DTO\TempDto;
use NGS\Framework\DAL\Mappers\AbstractMapper;

class TempMapper extends AbstractMapper {

    /**
     * @var self
     */
    private static $instance = null;

    /**
     * @var string
     */
    protected $tableName = 'temp';

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @see AbstractMapper::createDto()
     */
    public function createDto()
    {
        return new TempDto();
    }
}