<?php

namespace NGS\DAL\Mappers;

use NGS\DAL\DTO\GroupsDto;
use NGS\Framework\DAL\Mappers\AbstractMapper;

class GroupsMapper extends AbstractMapper {

    /**
     * @var self
     */
    private static $instance = null;

    /**
     * @var string
     */
    protected $tableName = 'groups';

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @see AbstractMapper::createDto()
     */
    public function createDto()
    {
        return new GroupsDto();
    }

    /**
     * @param int $offset
     * @param int $limit
     * @param string $orderByFieldName
     *
     * @return GroupsDto[]
     */
    public function getAllGroupsJoinedUsers($offset, $limit, $orderByFieldName)
    {
        $sqlQuery = sprintf("SELECT `%s`.*, `users`.`first_name` as `user_first_name`, `users`.`last_name` as `user_last_name`  FROM `%s` LEFT JOIN `users` on `%s`.`updated_by` = `users`.`id` ORDER BY `%s`.`%s` LIMIT %d, %d", $this->getTableName(), $this->getTableName(), $this->getTableName(), $this->getTableName(), $orderByFieldName, $offset, $limit);

        return $this->fetchRows($sqlQuery);
    }

    /**
     * @return int
     */
    public function getAllGroupsCount()
    {
        $sqlQuery = sprintf("SELECT count(`id`) as `count`  FROM `%s`", $this->getTableName());

        return intval($this->fetchField($sqlQuery, 'count'));
    }
}