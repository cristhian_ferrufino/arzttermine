<?php

namespace NGS\DAL\Mappers;

use NGS\DAL\DTO\Integration1DatasDto;
use NGS\Framework\DAL\Mappers\AbstractMapper;

class Integration1DatasMapper extends AbstractMapper {

    /**
     * @var self
     */
    private static $instance = null;

    /**
     * @var string
     */
    protected $tableName = 'integration_1_datas';

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @see AbstractMapper::createDto()
     */
    public function createDto()
    {
        return new Integration1DatasDto();
    }
}