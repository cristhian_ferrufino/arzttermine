<?php

namespace NGS\DAL\Mappers;

use NGS\DAL\DTO\SamediNetworkDto;
use NGS\Framework\DAL\Mappers\AbstractMapper;

class SamediNetworkMapper extends AbstractMapper {

    /**
     * @var self
     */
    private static $instance = null;

    /**
     * @var string
     */
    protected $tableName = 'samedi_network';

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @see AbstractMapper::createDto()
     */
    public function createDto()
    {
        return new SamediNetworkDto();
    }

    public function truncateTable()
    {
        $sqlQuery = sprintf("TRUNCATE TABLE `%s`", $this->getTableName());
        $this->dbms->query($sqlQuery);
    }
}