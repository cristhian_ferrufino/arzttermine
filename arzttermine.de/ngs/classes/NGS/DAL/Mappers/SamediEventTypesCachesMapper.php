<?php

namespace NGS\DAL\Mappers;

use NGS\DAL\DTO\SamediEventTypesCachesDto;
use NGS\Framework\DAL\Mappers\AbstractMapper;
use Arzttermine\Integration\Integration\Samedi\Types\CategoryEventType;

class SamediEventTypesCachesMapper extends AbstractMapper {

    /**
     * @var self
     */
    private static $instance = null;

    /**
     * @var string
     */
    protected $tableName = 'samedi_event_types_caches';

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @see AbstractMapper::createDto()
     */
    public function createDto()
    {
        return new SamediEventTypesCachesDto();
    }

    /**
     * @param int $practiceId
     * @param int $categoryId
     *
     * @return CategoryEventType
     */
    public function getCategoryEventTypes($practiceId, $categoryId)
    {
        $sqlQuery = sprintf("SELECT * FROM `%s` WHERE `practice_id` = '%s' AND `category_id` = %d", $this->getTableName(), $practiceId, $categoryId);
        $rows = $this->fetchRows($sqlQuery);
        if (isset($rows) && count($rows) === 1) {
            return $rows[0];
        } else {
            return null;
        }
    }
}