<?php

namespace NGS\DAL\Mappers;

use NGS\DAL\DTO\SamediTreatmentTypesDto;
use NGS\Framework\DAL\Mappers\AbstractMapper;

class SamediTreatmentTypesMapper extends AbstractMapper {

    /**
     * @var self
     */
    private static $instance = null;

    /**
     * @var string
     */
    protected $tableName = 'samedi_treatmenttypes';

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function deleteByIds($doctorId, $ttIdsArray)
    {
        $query = sprintf("DELETE FROM `%s` WHERE `id` IN (%s) AND `user_id`=%d", $this->getTableName(), implode(',', $ttIdsArray), $doctorId);
        $res = $this->dbms->query($query);
        if ($res) {
            $result = $this->dbms->getAffectedRows();

            return $result;
        }

        return -1;
    }

    public function getByIdIncludedParentDto($id)
    {
        $treatmentTypesMapper = TreatmentTypesMapper::getInstance();
        $q = $this->getAllFieldsAsQueryString(true);
        $q .= ',' . $treatmentTypesMapper->getAllFieldsAsQueryString(true);
        $query = sprintf("SELECT %s FROM `%s` LEFT JOIN `treatmenttypes` ON `%s`.`parent_id` = `treatmenttypes`.`id` WHERE `%s`.`id` = %d ", $q, $this->getTableName(), $this->getTableName(), $this->getTableName(), $id);

        $res = $this->dbms->query($query);
        $results = $this->dbms->getResultArray($res);
        if (count($results) !== 1) {
            return null;
        }
        $result = $results [0];
        $samediTreatmentTypeDto = $this->createDto();
        $this->initializeDto($samediTreatmentTypeDto, $result, true);
        $treatmentTypeDto = $treatmentTypesMapper->createDto();
        $treatmentTypesMapper->initializeDto($treatmentTypeDto, $result, true);
        $samediTreatmentTypeDto->setBaseTreatmentTypeDto($treatmentTypeDto);

        return $samediTreatmentTypeDto;
    }

    /**
     * @see AbstractMapper::createDto()
     */
    public function createDto()
    {
        return new SamediTreatmentTypesDto();
    }
}