<?php

namespace NGS\DAL\Mappers;

use NGS\DAL\DTO\LoginHistoryDto;
use NGS\Framework\DAL\Mappers\AbstractMapper;

class LoginHistoryMapper extends AbstractMapper {

    /**
     * @var self
     */
    private static $instance = null;

    /**
     * @var string
     */
    protected $tableName = 'login_history';

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @see AbstractMapper::createDto()
     */
    public function createDto()
    {
        return new LoginHistoryDto();
    }
}