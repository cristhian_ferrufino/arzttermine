<?php

namespace NGS\DAL\Mappers;

use NGS\DAL\DTO\DoctorsDto;
use Arzttermine\Integration\Integration\Samedi\Types\CategoryEventType;

class DoctorsMapper extends UsersMapper {

    /**
     * @var self
     */
    private static $instance = null;

    /**
     * @var string
     */
    protected $tableName = 'users';

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param int $offset
     * @param int $limit
     * @param string $orderByFieldName
     * @param bool $ascending
     * @param array $searchArray
     *
     * @return DoctorsDto[]
     */
    public function getDoctorsFull($offset, $limit, $orderByFieldName, $ascending = true, array $searchArray = array())
    {
        $samediMapper = SamediDatasMapper::getInstance();
        $locationsMapper = LocationsMapper::getInstance();

        $q = $this->getAllFieldsAsQueryString(true);

        $q = $q . ',' . $samediMapper->getAllFieldsAsQueryString(true);
        $q = $q . ',' . $locationsMapper->getAllFieldsAsQueryString(true);
        $q = $q . ',' . $this->getCreatedByFieldsAsQueryString();

        $query = sprintf(
            "SELECT %s FROM `%s` inner join `%s` on `%s`.`user_id`=`%s`.`id` inner join `%s` on `%s`.`id`=`%s`.`location_id`
            left join `%s` as `created` on `%s`.`created_by`=`created`.`id` ",
            $q, $samediMapper->getTableName(), $this->getTableName(), $samediMapper->getTableName(), $this->getTableName(),
            $locationsMapper->getTableName(), $locationsMapper->getTableName(), $samediMapper->getTableName(),
            $this->getTableName(), $samediMapper->getTableName()
        );

        if (!empty($searchArray)) {
            $query .= "WHERE ";
            foreach ($searchArray as $columnName => $searchInfo) {
                if ($searchInfo['searchStyle'] != 'BETWEEN') {
                    $query .= sprintf("`samedi_datas`.`%s` %s '%s' AND ", $columnName, $searchInfo['searchStyle'], $searchInfo['value']);
                } else {
                    $query .= sprintf("`samedi_datas`.`%s` %s '%s' AND '%s' AND ", $columnName, $searchInfo['searchStyle'], $searchInfo['value1'], $searchInfo['value2']);
                }
            }
            $query = substr($query, 0, -4);
        }

        $query = $query . sprintf("ORDER BY `%s` ", $orderByFieldName);

        if ($ascending) {
            $query = $query . sprintf("LIMIT %d, %d ", $offset, $limit);
        } else {
            $query = $query . sprintf("DESC LIMIT %d, %d ", $offset, $limit);
        }

        $res = $this->dbms->query($query);
        $results = $this->dbms->getResultArray($res);
        $resultArr = array();

        foreach ($results as $result) {

            $locationDto = $locationsMapper->createDto();
            $samediDatasDto = $samediMapper->createDto();
            $dtoExists = false;

            foreach ($resultArr as $dto) {
                if ($dto->getId() === $result['users_id']) {

                    $dtoExists = true;
                    $locationsMapper->initializeDto($locationDto, $result, true);
                    $samediMapper->initializeDto($samediDatasDto, $result, true);
                    $createdBy = $samediDatasDto->getCreatedBy();
                    if (isset($createdBy)) {
                        $userDto = $this->selectByPK($createdBy);
                    }
                    $samediDatasDto->creator = $userDto;
                    $dto->addSamediDataDto($samediDatasDto);
                    $dto->addLocationDto($locationDto);
                    break;
                }
            }
            if (!$dtoExists) {
                $doctorDto = $this->createDto();
                $this->initializeDto($doctorDto, $result, true);
                $locationsMapper->initializeDto($locationDto, $result, true);
                $samediMapper->initializeDto($samediDatasDto, $result, true);

                foreach ($result as $fieldName => $fieldValue) {
                    $i = strpos($fieldName, 'createdBy');
                    if ($i !== false) {
                        $result[substr($fieldName, 10)] = $fieldValue;
                    }
                    unset($result[$fieldName]);
                }
                $createdBy = $this->createDto();
                $samediMapper->initializeDto($createdBy, $result);
                $samediDatasDto->addCreatedByDto($createdBy);
                $doctorDto->addSamediDataDto($samediDatasDto);
                $doctorDto->addLocationDto($locationDto);
                $resultArr[] = $doctorDto;
            }
        }

        return $resultArr;
    }

    public function getCreatedByFieldsAsQueryString()
    {
        $dto = $this->createDto();
        $dtoMapArray = $dto->getMapArray();
        $queryString = '';
        foreach ($dtoMapArray as $dbFieldName => $dtoFieldName) {
            $string = sprintf("`created`.`%s` as `%s_%s`,", $dbFieldName, "createdBy", $dbFieldName);
            $queryString = $queryString . $string;
        }
        $queryString = substr($queryString, 0, -1);

        return $queryString;
    }

    public function createDto()
    {
        return new DoctorsDto();
    }

    public function insertDoctorFull($doctorDto, $replace = false, $replaceLocation = false)
    {
        $doctorId = $doctorDto->getId();
        if (!isset($doctorId)) {
            $doctorDto->setId('');
        }

        $samediMapper = SamediDatasMapper::getInstance();
        $locationsMapper = LocationsMapper::getInstance();
        $locationUserConnectionsMapper = LocationUserConnectionsMapper::getInstance();
        $samediTreatmentTypesMapper = SamediTreatmentTypesMapper::getInstance();

        $samediDatas = $doctorDto->getSamediDatasDtos();
        $samediDataDto = $samediDatas[0];
        $locations = $doctorDto->getLocationDtos();
        $locationDto = $locations[0];

        $practiceId = $samediDataDto->getPracticeId();
        $categoryId = $samediDataDto->getCategoryId();

        $doctorEventTypeCaches = $doctorDto->getSamediEventTypesCachesDtos();
        $eventTypes = $doctorEventTypeCaches[0]->getEventTypes();
        $eventTypes = json_decode($eventTypes);
        $ttsToBeInserted = array();
        $ttsToBeInsertedIds = array();
        foreach ($eventTypes as $eventType) {
            $eventType = new CategoryEventType($eventType);
            $ttDto = $samediTreatmentTypesMapper->createDto();
            $ttDto->setId($eventType->getId());
            $ttDto->setName($eventType->getName());
            $ttDto->setDuration($eventType->getDurationInMinutes());
            $ttDto->setInsuranceIds($eventType->getArzttermineFormattedInsuranceIds());
            $ttDto->setCreatedAt(date("Y-m-d H:i:s"));
            $ttsToBeInserted[] = $ttDto;
            $ttsToBeInsertedIds[] = $eventType->getId();
        }
        foreach ($ttsToBeInserted as $ttDto) {
            $samediTTDto = $samediTreatmentTypesMapper->selectByPK($ttDto->getId());
            $exists = isset($samediTTDto);
            if (!$exists) {
                $samediTreatmentTypesMapper->insertDto($ttDto);
            }
        }
        $query = sprintf("SELECT * FROM `%s` WHERE `practice_id`='%s' AND `category_id`='%s' ", $samediMapper->getTableName(), $practiceId, $categoryId);
        $resultArr = $samediMapper->fetchRows($query);
        if (!empty($resultArr)) {
            $doctorId = $resultArr[0]->getUserId();
            if ($replace === true) {
                $doctorDto->setId($doctorId);
                $this->updateByPK($doctorDto);
            }
            if ($replaceLocation === true) {
                $locationDto->setId($resultArr[0]->getLocationId());
                $locationsMapper->updateByPK($locationDto);
            }

            return;
        }

        $query = sprintf("SELECT * FROM `%s` WHERE `practice_id`='%s'", $samediMapper->getTableName(), $practiceId);
        $resultArr = $samediMapper->fetchRows($query);

        if (!empty($resultArr)) {
            $doctorId = $this->insertDto($doctorDto);
            $locationId = $resultArr[0]->getLocationId();
            $existingLocationDto = $locationsMapper->selectByPK($locationId);
            $locationMemberIds = $existingLocationDto->getMemberIds();

            if (empty($locationMemberIds)) {
                $locationDto->setMemberIds($doctorId);
                $existingLocationDto->setMemberIds($doctorId);
            } else {
                $locationMemberIds .= ',' . $doctorId;
                $locationDto->setMemberIds($locationMemberIds);
                $existingLocationDto->setMemberIds($locationMemberIds);
            }

            if (!$replaceLocation) {
                $locationsMapper->updateByPK($existingLocationDto);
            } else {
                $locationDto->setId($existingLocationDto->getId());
                $locationsMapper->updateByPK($locationDto);
            }
        } else {
            $doctorId = $this->insertDto($doctorDto);
            $locationMemberIds = $locationDto->getMemberIds();

            if (empty($locationMemberIds)) {
                $locationDto->setMemberIds($doctorId);
            } else {
                $locationMemberIds .= ',' . $doctorId;
                $locationDto->setMemberIds($locationMemberIds);
            }
            $locationId = $locationsMapper->insertDto($locationDto);
        }

        $locationUserConnectionsDto = $locationUserConnectionsMapper->createDto();
        $locationUserConnectionsDto->setUserId($doctorId);
        $locationUserConnectionsDto->setLocationId($locationId);
        $locationUserConnectionsMapper->insertDto($locationUserConnectionsDto);
        $samediDataDto->setUserId($doctorId);
        $samediDataDto->setLocationId($locationId);
        $samediMapper->insertDto($samediDataDto);
    }

    public function getDoctorWithMedicalSpecialtiesAndTreatmentTypes($id)
    {
        $medicalSpecialtiesMapper = MedicalSpecialtiesMapper::getInstance();
        $treatmentTypesMapper = TreatmentTypesMapper::getInstance();

        $q = $this->getAllFieldsAsQueryString(true);
        $q .= "," . $medicalSpecialtiesMapper->getAllFieldsAsQueryString(true);
        $q .= "," . $treatmentTypesMapper->getAllFieldsAsQueryString(true);

        $query = sprintf(
            "SELECT %s FROM `%s` left join `%s` on FIND_IN_SET(`%s`.`id`,`%s`.`medical_specialty_ids`) left join `%s` on `%s`.`medical_specialty_id`=`%s`.`id` WHERE `%s`.`id`='%d' ",
            $q, $this->getTableName(), $medicalSpecialtiesMapper->getTableName(), $medicalSpecialtiesMapper->getTableName(), $this->getTableName(),
            $treatmentTypesMapper->getTableName(), $treatmentTypesMapper->getTableName(), $medicalSpecialtiesMapper->getTableName(), $this->getTableName(), $id
        );

        $res = $this->dbms->query($query);
        $results = $this->dbms->getResultArray($res);
        $doctorDto = $this->createDto();
        $this->initializeDto($doctorDto, $results[0], true);
        $insertedMedSpecIdArray = array();
        foreach ($results as $result) {
            if ($result['medical_specialties_id'] > 0 && !in_array($result['medical_specialties_id'], $insertedMedSpecIdArray)) {
                $insertedMedSpecIdArray[] = $result['medical_specialties_id'];
                $medicalSpecialtyDto = $medicalSpecialtiesMapper->createDto();
                $medicalSpecialtiesMapper->initializeDto($medicalSpecialtyDto, $result, true);
                $doctorDto->addMedicalSpecialtyDto($medicalSpecialtyDto);
            }
            if ($result['treatmenttypes_id'] > 0) {
                $treatmentTypeDto = $treatmentTypesMapper->createDto();
                $treatmentTypesMapper->initializeDto($treatmentTypeDto, $result, true);
                $medicalSpecialtyDto->addSamediTreatmentTypeDto($treatmentTypeDto);
            }
        }

        return $doctorDto;
    }

    public function getUserWithLocationsById($userId, $locationId)
    {
        $locationsMapper = LocationsMapper::getInstance();
        $q = $this->getAllFieldsAsQueryString(true);
        $q .= ',' . $locationsMapper->getAllFieldsAsQueryString(true);
        $query = sprintf(
            "SELECT %s FROM `users` 
                        INNER JOIN `location_user_connections` ON `users`.`id`=`location_user_connections`.`user_id`
                        INNER JOIN `locations` ON `locations`.`id`=`location_user_connections`.`location_id` WHERE `users`.`id`=%d ", $q, $userId
        );
        if ($locationId > 0) {
            $query .= " AND `locations`.`id`=" . intval($locationId);
        }
        $res = $this->dbms->query($query);
        $results = $this->dbms->getResultArray($res);
        $userDto = null;
        foreach ($results as $result) {
            if (!isset($userDto)) {
                $userDto = $this->createDto();
                $this->initializeDto($userDto, $result, true);
            }
            $locationDto = $locationsMapper->createDto();
            $locationsMapper->initializeDto($locationDto, $result, true);
            $userDto->addLocationDto($locationDto);
        }

        return $userDto;
    }

    /**
     * Get only a given set of users according to IDs
     *
     * @param array $user_ids
     * @param bool $force_order
     *
     * @return array
     */
    public function getUsersByIds(array $user_ids, $force_order = false)
    {
        $order_by = '';

        // Force the DB to return the IDs as we supplied them
        if ($force_order) {
            $order_by = 'ORDER BY FIELD(`id`, ' . implode(',', $user_ids) . ')';
        }

        $sql = sprintf("SELECT * FROM `%s` WHERE `id` IN (%s) %s", $this->getTableName(), implode(',', $user_ids), $order_by);
        $query = $this->dbms->query($sql);
        $users = $this->dbms->getResultArray($query);

        $results = array();

        foreach ($users as $user) {
            $dto = $this->createDto();
            $this->initializeDto($dto, $user);
            $results[$dto->getId()] = $dto;
        }

        return $results;
    }
}