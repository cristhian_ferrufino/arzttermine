<?php

namespace NGS\DAL\Mappers;

use NGS\DAL\DTO\BlackListDto;
use NGS\Framework\DAL\Mappers\AbstractMapper;

class BlackListMapper extends AbstractMapper {

    /**
     * @var self
     */
    private static $instance = null;

    /**
     * @var string
     */
    protected $tableName = 'black_list';

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @see AbstractMapper::createDto()
     */
    public function createDto()
    {
        return new BlackListDto();
    }

    public function getByEmail($email)
    {
        $sql = "SELECT * FROM `%s` WHERE FIND_IN_SET('%s',`emails`) ";
        $sqlQuery = sprintf($sql, $this->getTableName(), $email);
        $rows = $this->fetchRows($sqlQuery);
        if (!empty($rows)) {
            return $rows[0];
        }

        return null;
    }

    public function getByPhoneNumber($phone)
    {
        $sql = "SELECT * FROM `%s` WHERE FIND_IN_SET('%s',`phone_numbers`) ";
        $sqlQuery = sprintf($sql, $this->getTableName(), $phone);
        $rows = $this->fetchRows($sqlQuery);
        if (!empty($rows)) {
            return $rows[0];
        }

        return null;
    }

    public function getByIp($ip)
    {
        $sql = "SELECT * FROM `%s` WHERE FIND_IN_SET('%s',`ips`) ";
        $sqlQuery = sprintf($sql, $this->getTableName(), $ip);
        $rows = $this->fetchRows($sqlQuery);
        if (!empty($rows)) {
            return $rows[0];
        }

        return null;
    }
}