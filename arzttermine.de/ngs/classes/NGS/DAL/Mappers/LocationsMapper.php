<?php

namespace NGS\DAL\Mappers;

use NGS\DAL\DTO\LocationsDto;
use NGS\Framework\DAL\Mappers\AbstractMapper;

class LocationsMapper extends AbstractMapper {

    /**
     * @var self
     */
    private static $instance = null;

    /**
     * @var string
     */
    protected $tableName = 'locations';

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param int $offset
     * @param int $limit
     * @param string $orderByFieldName
     *
     * @return LocationsDto[]
     */
    public function getAllLocations($offset = 0, $limit = null, $orderByFieldName = '')
    {
        $query = sprintf("SELECT `%s`.* FROM `%s`", $this->getTableName(), $this->getTableName());
        
        if (!empty($orderByFieldName)) {
            $query .= sprintf(' ORDER BY `%s`.`%s`', $this->getTableName(), $orderByFieldName);
        }
        
        if (!is_null($limit)) {
            $query .= sprintf(' LIMIT %d, %d', intval($offset), intval($limit));
        }

        return $this->fetchRows($query);
    }

    /**
     * @return LocationsDto[]
     */
    public function getAllLocationsNoLimit()
    {
        return $this->getAllLocations();
    }

    /**
     * @param string $slug
     *
     * @return LocationsDto|null
     */
    public function getBySlug($slug)
    {
        $sqlQuery = sprintf("SELECT * FROM `%s` WHERE `slug`='%s'", $this->getTableName(), $slug);
        $rows = $this->fetchRows($sqlQuery);
        if (count($rows) === 1) {
            return $rows[0];
        } else {
            return null;
        }
    }

    /**
     * @return LocationsDto[]
     */
    public function getAllLocationNames()
    {
        $sqlQuery = sprintf("SELECT `id`,`name` FROM `%s` ", $this->getTableName());

        return $this->fetchRows($sqlQuery);
    }

    /**
     * @return int
     */
    public function getAllLocationsCount()
    {
        $sqlQuery = sprintf("SELECT count(`id`) as `count`  FROM `%s`", $this->getTableName());

        return intval($this->fetchField($sqlQuery, 'count'));
    }

    /**
     * @param int $locationStatus
     * @param int $userStatus
     * @param int $medicalSpecialtyId
     * @param int $doctorTreatmentTypeId
     * @param int $distance
     * @param string $lng
     * @param string $lat
     *
     * @return string|null
     */
    public function getCityByGivenFilters($locationStatus, $userStatus, $medicalSpecialtyId, $doctorTreatmentTypeId, $distance, $lng, $lat)
    {
        $doctorsMapper = DoctorsMapper::getInstance();
        $doctorsTreatmentTypesMapper = DoctorsTreatmentTypesMapper::getInstance();
        $q = $this->getAllFieldsAsQueryString(true);
        $q .= ',' . $doctorsMapper->getAllFieldsAsQueryString(true);
        if (!empty($doctorTreatmentTypeId)) {
            $q .= ',' . $doctorsTreatmentTypesMapper->getAllFieldsAsQueryString(true);
        }
        $q .= ',';

        $rad_lng = $lng / 180 * M_PI;
        $rad_lat = $lat / 180 * M_PI;

        $q
            .= "(
             6368 * SQRT(2*(1-cos(RADIANS(`lat`)) *
             cos(" . $rad_lat . ") * (sin(RADIANS(`lng`)) *
             sin(" . $rad_lng . ") + cos(RADIANS(`lng`)) *
             cos(" . $rad_lng . ")) - sin(RADIANS(`lat`)) * sin(" . $rad_lat . ")))) AS distance ";

        $query = sprintf(
            "SELECT %s FROM `%s` 
                          INNER JOIN `location_user_connections` on `location_user_connections`.`location_id` = `%s`.`id` 
                          INNER JOIN `%s` ON `%s`.`id`= `location_user_connections`.`user_id` ",
            $q, $this->getTableName(), $this->getTableName(), $doctorsMapper->getTableName(), $doctorsMapper->getTableName()
        );

        if (!empty($doctorTreatmentTypeId)) {
            $query .= sprintf(
                "LEFT JOIN `%s` ON `%s`.`doctor_id`=`%s`.`id` WHERE `%s`.`treatment_type_id`=%d AND ",
                $doctorsTreatmentTypesMapper->getTableName(), $doctorsTreatmentTypesMapper->getTableName(), $doctorsMapper->getTableName(), $doctorsTreatmentTypesMapper->getTableName(), $doctorTreatmentTypeId
            );
        } else {
            $query .= "WHERE ";
        }
        $query .= sprintf(" FIND_IN_SET( %d, `users`.`medical_specialty_ids`)  AND FIND_IN_SET(%d,`%s`.`medical_specialty_ids`) AND `%s`.`status`=%d AND `%s`.`status`=%d ", $medicalSpecialtyId, $medicalSpecialtyId, $this->getTableName(), $this->getTableName(), $locationStatus, $doctorsMapper->getTableName(), $userStatus);
        $query .= sprintf("HAVING distance<%d ", $distance);
        $query .= " ORDER BY distance ASC LIMIT 1";
        $res = $this->dbms->query($query);
        $results = $this->dbms->getResultArray($res);
        if (count($results) == 1) {
            return $results[0]['locations_city'];
        }

        return null;
    }

    /**
     * @param string[] $cityNamesArray
     *
     * @return array
     */
    public function getCitiesWithMedicalSpecialties($cityNamesArray)
    {
        $medicalSpecialtiesMapper = MedicalSpecialtiesMapper::getInstance();

        $cityNamesString = implode($cityNamesArray, "','");
        $q = $this->getAllFieldsAsQueryString(true);
        $q = $q . ',' . $medicalSpecialtiesMapper->getAllFieldsAsQueryString(true);

        $query = sprintf(
            "SELECT %s FROM `%s` LEFT JOIN 
                    `%s` ON FIND_IN_SET(`%s`.`id`,`%s`.`medical_specialty_ids`) WHERE
                    `%s`.`status` = 3 AND `%s`.`status` = 1 AND `city` IN ('%s') GROUP BY `city`,`%s`.`id`;",
            $q, $this->getTableName(), $medicalSpecialtiesMapper->getTableName(), $medicalSpecialtiesMapper->getTableName(), $this->getTableName(), $this->getTableName(),
            $medicalSpecialtiesMapper->getTableName(), $cityNamesString, $medicalSpecialtiesMapper->getTableName()
        );
        $res = $this->dbms->query($query);
        $results = $this->dbms->getResultArray($res);
        $resultArr = array();
        $existingLocationsIdsArray = array();

        foreach ($results as $result) {
            $medicalSpecialtyDto = $medicalSpecialtiesMapper->createDto();

            if (!isset($existingLocationsIdsArray[$result["locations_id"]])) {
                $locationDto = $this->createDto();
                $this->initializeDto($locationDto, $result, true);
                $existingLocationsIdsArray[$locationDto->getId()] = "exists";
            } else {
                $locationDto = $this->selectByPK($result["locations_id"]);
            }

            $medicalSpecialtiesMapper->initializeDto($medicalSpecialtyDto, $result, true);
            $locationDto->addMedicalSpecialtyDto($medicalSpecialtyDto);
            $resultArr[] = $locationDto;
        }

        return $resultArr;
    }

    /**
     * @see AbstractMapper::createDto()
     */
    public function createDto()
    {
        return new LocationsDto();
    }

    /**
     * @param int $userId
     *
     * @return LocationsDto[]
     */
    public function getLocationsByUserId($userId)
    {
        $query = sprintf(
            "SELECT * FROM `%s` inner join `location_user_connections` on `location_user_connections`.`location_id`=`%s`.`id` AND `location_user_connections`.`user_id`=%d",
            $this->getTableName(), $this->getTableName(), $userId
        );
        $rows = $this->fetchRows($query);

        return $rows;
    }

    /**
     * @param string $salesforceId
     *
     * @return int|bool
     */
    public function getIdBySalesforceId($salesforceId)
    {
        /*
         * delete the 3 last characters from $salesforceId
         * the Id stored in SF is 3 characters longer than the
         * Id that is exported in CSV export and eventually
         * stored in the database.
         */
        $salesforceId = substr($salesforceId, 0, -3);
        $query = sprintf(
            "SELECT id FROM `%s` WHERE `salesforce_id` LIKE '%s'",
            $this->getTableName(), $salesforceId
        );
        $rows = $this->fetchRows($query);

        if (is_array($rows)) {
            return $rows[0]->id;
        }

        return false;
    }

    /**
     * @param $integrationId
     * @param $locationStatus
     * @param $userStatus
     *
     * @return LocationsDto[]
     */
    public function getIntegrationLocationsWithDoctors($integrationId, $locationStatus, $userStatus)
    {
        $doctorsMapper = DoctorsMapper::getInstance();
        $q = $this->getAllFieldsAsQueryString(true);
        $q .= ',' . $doctorsMapper->getAllFieldsAsQueryString(true);
        $statusFilter = '';
        if (isset($locationStatus) && isset($userStatus)) {
            $statusFilter = sprintf("AND `locations`.`status`=%d AND `users`.`status`=%d", $locationStatus, $userStatus);
        }
        $sqlQuery = sprintf(
            "SELECT %s FROM `%s` 
                    INNER JOIN `location_user_connections` ON `location_user_connections`.`location_id`=`locations`.`id`
                    INNER JOIN `users` ON `users`.`id`=`location_user_connections`.`user_id` WHERE `integration_id`=%d %s ORDER BY `locations`.`name`", $q, $this->getTableName(), $integrationId, $statusFilter
        );
        $res = $this->dbms->query($sqlQuery);
        $results = $this->dbms->getResultArray($res);

        $resultArr = array();

        foreach ((array)$results as $result) {
            $locationDto = $this->createDto();
            $this->initializeDto($locationDto, $result, true);
            $doctorDto = $doctorsMapper->createDto();
            $doctorsMapper->initializeDto($doctorDto, $result, true);
            $locationDto->addUserDto($doctorDto);
            $locationDto->setUserDto($doctorDto);
            $resultArr[] = $locationDto;
        }

        return $resultArr;
    }

    public function getAllIntegrationsIdsArray()
    {
        $query = sprintf("SELECT `integration_id` FROM `%s` GROUP BY `integration_id` ORDER BY `integration_id`", $this->getTableName());
        $resultArr = $this->fetchRows($query);
        $ret = array();
        foreach ($resultArr as $dto) {
            $ret[] = $dto->getIntegrationId();
        }

        return $ret;
    }

    public function findAllVisibleDoctors($locationStatus, $userStatus, $excludedIntegrationIdsArray = array())
    {
        $doctorsMapper = DoctorsMapper::getInstance();
        $locationUserConnectionsMapper = LocationUserConnectionsMapper::getInstance();
        $q = $this->getAllFieldsAsQueryString(true);
        $q .= ',' . $doctorsMapper->getAllFieldsAsQueryString(true);

        $excludeIntegrationsSubquery = "";
        if (!empty($excludedIntegrationIdsArray)) {
            $excludedIntegrationIds = implode(',', $excludedIntegrationIdsArray);
            $excludeIntegrationsSubquery = sprintf(" `%s`.`integration_id` NOT IN (%s) AND ", $this->getTableName(), $excludedIntegrationIds);
        }

        $query = sprintf(
            "SELECT %s FROM `%s` 
                        LEFT JOIN `%s` ON `%s`.`location_id`= `%s`.`id`
                        LEFT JOIN `%s` ON `%s`.`user_id`= `%s`.`id`
                        WHERE %s `%s`.`status`>=%d AND `%s`.`status`>=%d ",
            $q, $this->getTableName(),
            $locationUserConnectionsMapper->getTableName(), $locationUserConnectionsMapper->getTableName(), $this->getTableName(),
            $doctorsMapper->getTableName(), $locationUserConnectionsMapper->getTableName(), $doctorsMapper->getTableName(),
            $excludeIntegrationsSubquery, $this->getTableName(), $locationStatus, $doctorsMapper->getTableName(), $userStatus
        );
        $res = $this->dbms->query($query);

        $results = $this->dbms->getResultArray($res);

        $resultArr = array();
        if (count($results) < 1) {
            return array();
        }

        foreach ($results as $result) {
            $locationDto = $this->createDto();
            $this->initializeDto($locationDto, $result, true);

            $doctorDto = $doctorsMapper->createDto();
            $doctorsMapper->initializeDto($doctorDto, $result, true);
            $locationDto->setUserDto($doctorDto);

            $resultArr[] = $locationDto;
        }

        return $resultArr;
    }

    public function getAllLocationsWithDoctors($locationStatus, $userStatus)
    {
        $doctorsMapper = DoctorsMapper::getInstance();
        $q = $this->getAllFieldsAsQueryString(true);
        $q .= ',' . $doctorsMapper->getAllFieldsAsQueryString(true);

        $sqlQuery = sprintf(
            "SELECT %s FROM `%s` 
                    INNER JOIN `location_user_connections` ON `location_user_connections`.`location_id`=`locations`.`id`
                    INNER JOIN `users` ON `users`.`id`=`location_user_connections`.`user_id` WHERE `locations`.`status`=%d AND `users`.`status`=%d", $q, $this->getTableName(), $locationStatus, $userStatus
        );

        $res = $this->dbms->query($sqlQuery);
        $results = $this->dbms->getResultArray($res);

        $resultArr = array();
        if (count($results) < 1) {
            return array();
        }

        foreach ($results as $result) {
            $locationDto = $this->createDto();
            $this->initializeDto($locationDto, $result, true);
            $doctorDto = $doctorsMapper->createDto();
            $doctorsMapper->initializeDto($doctorDto, $result, true);
            $locationDto->setUserDto($doctorDto);
            $resultArr[] = $locationDto;
        }

        return $resultArr;
    }

    public function getLocationsByStatus($status)
    {
        $query = sprintf("SELECT * FROM `%s` WHERE `status`=%d ", $this->getTableName(), $status);
        $resultArr = $this->fetchRows($query);

        return $resultArr;
    }

    /**
     * @param int $id
     * @param array $fields
     * @param array $values
     *
     * @return bool|int
     */
    public function updateLocation($id, array $fields, array $values)
    {
        if (intval($id)) {
            return $this->updateWithFields($id, $fields, $values);
        }

        return false;
    }

    public function getLocationWithUsersById($locationId)
    {
        $doctorsMapper = DoctorsMapper::getInstance();
        $q = $this->getAllFieldsAsQueryString(true);
        $q .= ',' . $doctorsMapper->getAllFieldsAsQueryString(true);
        $query = sprintf(
            "SELECT %s FROM `locations` 
                        INNER JOIN `location_user_connections` ON `location_user_connections`.`location_id`=`locations`.`id`
                        INNER JOIN `users` ON `users`.`id`=`location_user_connections`.`user_id` WHERE `locations`.`id`=%d", $q, $locationId
        );
        $res = $this->dbms->query($query);
        $results = $this->dbms->getResultArray($res);
        $locationDto = null;
        foreach ($results as $result) {
            if (!isset($locationDto)) {
                $locationDto = $this->createDto();
                $this->initializeDto($locationDto, $result, true);
            }
            $userDto = $doctorsMapper->createDto();
            $doctorsMapper->initializeDto($userDto, $result, true);
            $locationDto->addUserDto($userDto);
        }

        return $locationDto;
    }

    public function getLocationsByIds($locationIdsArray)
    {
        $query = sprintf("SELECT * FROM `%s` WHERE `id` IN (%s)", $this->getTableName(), implode(',', $locationIdsArray));
        $res = $this->dbms->query($query);
        $results = $this->dbms->getResultArray($res);
        $resultArr = array();
        foreach ($results as $result) {
            $dto = $this->createDto();
            $this->initializeDto($dto, $result);
            $resultArr[$dto->getId()] = $dto;
        }

        return $resultArr;
    }

    public function getPhoneNumbersByStatus($status)
    {
        $query = sprintf("SELECT DISTINCT phone_visible, id FROM `%s` WHERE status = %d", $this->getTableName(), $status);

        return $this->fetchRows($query);
    }
}
