<?php

namespace NGS\DAL\Mappers;

use NGS\DAL\DTO\SamediCachesDto;
use NGS\Framework\DAL\Mappers\AbstractMapper;

class SamediCachesTmpMapper extends AbstractMapper {

    /**
     * @var self
     */
    private static $instance = null;

    /**
     * @var string
     */
    protected $tableName = 'samedi_caches_tmp';

    /**
     * @var string
     */
    private $originalTableName = 'samedi_caches';

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @see AbstractMapper::getTableName()
     */
    public function setTableName($tableName)
    {
        $this->tableName = $tableName;
    }

    /**
     * @see AbstractMapper::getTableName()
     */
    public function getOriginalTableName()
    {
        return $this->originalTableName;
    }

    public function getAllSamediCaches($offset, $limit, $orderByFieldName)
    {
        $sqlQuery = sprintf("SELECT * FROM `%s` ORDER BY `%s` LIMIT %d, %d", $this->originalTableName, $orderByFieldName, $offset, $limit);

        return $this->fetchRows($sqlQuery);
    }

    public function getAllSamediCachesCount()
    {
        $sqlQuery = sprintf("SELECT count(*) as `count`  FROM `%s`", $this->originalTableName);

        return $this->fetchField($sqlQuery, 'count');
    }

    public function getDoctorAvailableAppointmentsByDateRange($user_id, $date_start, $date_end, $insurance_id, $treatmenttype_ids)
    {
        $treatmentTypesMapper = TreatmentTypesMapper::getInstance();
        $samediTreatmentTypesMapper = SamediTreatmentTypesMapper::getInstance();
        $q = $this->getAllFieldsAsQueryString(true, $this->originalTableName);
        $q .= ',' . $treatmentTypesMapper->getAllFieldsAsQueryString(true);
        $q .= ',' . $samediTreatmentTypesMapper->getAllFieldsAsQueryString(true);
        $sqlQuery = sprintf(
            "SELECT %s FROM `%s` 
             LEFT JOIN `samedi_treatmenttypes` ON FIND_IN_SET(`samedi_treatmenttypes`.`id`, `%s`.`treatmenttype_ids`)  
             LEFT JOIN `treatmenttypes` ON `samedi_treatmenttypes`.`parent_id` = `treatmenttypes`.`id`
             WHERE `%s`.`user_id` = %d AND `date`>='%s' AND `date`<='%s' ", $q, $this->originalTableName, $this->originalTableName, $this->originalTableName,
            $user_id, $date_start, $date_end, $treatmenttype_ids
        );
        if (!empty($treatmenttype_ids)) {
            $sqlQuery .= sprintf(" AND FIND_IN_SET(`samedi_treatmenttypes`.`parent_id`, '%s') ", $treatmenttype_ids);
        }
        if (!empty($insurance_id)) {
            $sqlQuery .= sprintf(" AND FIND_IN_SET('%s', `samedi_treatmenttypes`.`insurance_ids`) ", $insurance_id);
        }
        $sqlQuery .= " ORDER BY `date`, `appointment_datetime`";
        $res = $this->dbms->query($sqlQuery);
        $results = $this->dbms->getResultArray($res);
        $existingSamediCachesIdsArray = array();
        foreach ($results as $result) {
            $treatmentTypeDto = $treatmentTypesMapper->createDto();
            $samediTreatmentTypeDto = $samediTreatmentTypesMapper->createDto();
            if (!isset($existingSamediCachesIdsArray[$result["samedi_caches_id"]])) {
                $samediCachesDto = $this->createDto();
                $this->initializeDto($samediCachesDto, $result, true, $this->originalTableName);
                $existingSamediCachesIdsArray[$samediCachesDto->getId()] = $samediCachesDto;
            } else {
                $samediCachesDto = $existingSamediCachesIdsArray[$result["samedi_caches_id"]];
            }
            $treatmentTypesMapper->initializeDto($treatmentTypeDto, $result, true);

            $samediTreatmentTypesMapper->initializeDto($samediTreatmentTypeDto, $result, true);

            $samediTreatmentTypeDto->setBaseTreatmentTypeDto($treatmentTypeDto);
            $samediCachesDto->addSamediTreatmentTypeDtos($samediTreatmentTypeDto);
            $existingSamediCachesIdsArray[$samediCachesDto->getId()] = $samediCachesDto;
        }
        $resultArr = array_values($existingSamediCachesIdsArray);

        return $resultArr;
    }

    /**
     * @see AbstractMapper::createDto()
     */
    public function createDto()
    {
        return new SamediCachesDto();
    }

    public function getDoctorAllNonEmptyAvailableAppointments($user_id, $insuranceId, $treatmentTypesIds)
    {
        $sqlQuery = sprintf(
            "SELECT * FROM `%s` 
                            LEFT JOIN `treatmenttypes` ON FIND_IN_SET(`treatmenttypes`.`id`, `%s`.`treatmenttype_ids`)		
                            WHERE `user_id`=%d AND FIND_IN_SET('%s', `treatmenttypes`.`insurance_ids`)", $this->originalTableName, $this->originalTableName, $user_id, $insuranceId
        );
        if (!empty($treatmentTypesIds)) {
            $sqlQuery .= sprintf("AND FIND_IN_SET(`treatmenttypes`.`id`, '%s')", $treatmentTypesIds);
        }

        return $this->fetchRows($sqlQuery);
    }

    public function deleteBookingDayAppoitnmentsFromOriginalTable($userId, $date)
    {
        $sqlQuery = sprintf("DELETE FROM `%s` WHERE `user_id`=%d AND `date`='%s'", $this->originalTableName, $userId, $date);

        return $this->dbms->query($sqlQuery);
    }

    public function deleteDoctorAppointments($user_id, $fromOriginalTable)
    {
        $sqlQuery = sprintf("DELETE FROM `%s` WHERE `user_id`=%d", $fromOriginalTable ? $this->originalTableName : $this->getTableName(), $user_id);

        return $this->dbms->query($sqlQuery);
    }

    public function truncateTable()
    {
        $sqlQuery = sprintf("TRUNCATE TABLE `%s`", $this->getTableName());
        $this->dbms->query($sqlQuery);
    }

    public function swapWithOriginalTable()
    {
        $sqlQuery = sprintf("RENAME TABLE `%s` TO `%s`, `%s` To `%s`, `%s` To `%s`", $this->originalTableName, 'tmptablenameforswapsamedicaches', $this->getTableName(), $this->originalTableName, 'tmptablenameforswapsamedicaches', $this->getTableName());
        $this->dbms->query($sqlQuery);
    }
}