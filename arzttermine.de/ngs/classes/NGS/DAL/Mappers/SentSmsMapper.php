<?php

namespace NGS\DAL\Mappers;

use NGS\DAL\DTO\SentSmsDto;
use NGS\Framework\DAL\Mappers\AbstractMapper;

class SentSmsMapper extends AbstractMapper {

    /**
     * @var self
     */
    private static $instance = null;

    /**
     * @var string
     */
    protected $tableName = 'sent_sms';

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @see AbstractMapper::createDto()
     */
    public function createDto()
    {
        return new SentSmsDto();
    }
}