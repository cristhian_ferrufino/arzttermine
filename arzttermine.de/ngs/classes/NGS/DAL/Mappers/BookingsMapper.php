<?php

namespace NGS\DAL\Mappers;

use Arzttermine\Booking\Booking;
use Arzttermine\Calendar\Calendar;
use NGS\DAL\DTO\BookingsDto;
use NGS\Framework\DAL\Mappers\AbstractMapper;

class BookingsMapper extends AbstractMapper {

    /**
     * @var self
     */
    private static $instance = null;

    /**
     * @var string
     */
    protected $tableName = 'bookings';

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function getBookingsStartAtByFilters($userId, $locationId, $statuses, $dateStart, $dateEnd)
    {
        //add 1 day because appointment_start_at is datetime, but $dateStart, $dateEnd are dates.
        $dateEnd = Calendar::addDays($dateEnd, 1, true);
        $sqlQuery = sprintf(
            "SELECT GROUP_CONCAT(appointment_start_at) as `all_booked_times` 
            FROM `%s` 
            WHERE `user_id`=%d AND 
                `location_id`=%d AND 
                `status` IN (%s) AND 
                `appointment_start_at`>='%s' AND 
                `appointment_start_at`<='%s' ",
            $this->getTableName(),
            $userId,
            $locationId,
            implode(',', $statuses),
            $dateStart,
            $dateEnd
        );

        return $this->fetchField($sqlQuery, 'all_booked_times');
    }

    public function getBookingsMonthsByUserId($id)
    {
        $sqlQuery = sprintf(
            "SELECT DISTINCT EXTRACT(YEAR FROM appointment_start_at) AS first_name, EXTRACT(MONTH FROM appointment_start_at) AS last_name 
            FROM bookings 
            WHERE user_id = %s 
            ORDER BY appointment_start_at ;",
            $id
        );

        return $this->fetchRows($sqlQuery);
    }

    public function getAllBookingsCount($searchArray = array())
    {
        $q = '';
        if ($searchArray) {
            $q = "WHERE ";
            foreach ($searchArray as $columnName => $searchInfo) {
                if ($searchInfo['searchStyle'] != 'BETWEEN') {
                    $q .= sprintf("`%s` %s '%s' AND ", $columnName, $searchInfo['searchStyle'], $searchInfo['value']);
                } else {
                    $q .= sprintf("`%s` %s '%s' AND '%s' AND ", $columnName, $searchInfo['searchStyle'], $searchInfo['value1'], $searchInfo['value2']);
                }
            }
            $q = substr($q, 0, -4);
        }

        $sqlQuery = sprintf("SELECT count(`id`) as `count`  FROM `%s`", $this->getTableName());
        $sqlQuery .= $q;

        return $this->fetchField($sqlQuery, 'count');
    }

    public function getPatientSimilarBookings($email, $searchdateRangeStart, $searchdateRangeEnd, $medicalSpecialtyId, $docdir = false)
    {
        // If docdir than search must be done by created_at column, instead of appointment_start_at
        $timeFilterColumn = $docdir ? 'created_at' : 'appointment_start_at';

        // build the query for all statuses
        $bookingStatuses = Booking::getBookedStatuses();
        $bookingStatusesSql = Booking::getStatusesSql($bookingStatuses, 'OR');
        $bookingStatusesSql = ($bookingStatusesSql == '') ? '' : ' AND (' . $bookingStatusesSql . ')';

        $query = sprintf(
            "SELECT `%s`.* FROM `%s` 
					WHERE `email` = '%s' %s AND `%s`.`%s` >= '%s' AND `%s`.`%s` <= '%s' 
					AND `%s`.`medical_specialty_id` = %d 
					ORDER BY `appointment_start_at` DESC",
            $this->getTableName(),
            $this->getTableName(),
            $email,
            $bookingStatusesSql,
            $this->getTableName(),
            $timeFilterColumn,
            $searchdateRangeStart,
            $this->getTableName(),
            $timeFilterColumn,
            $searchdateRangeEnd,
            $this->getTableName(),
            intval($medicalSpecialtyId)
        );

        $resultArr = $this->fetchRows($query);

        return $resultArr;
    }

    public function getBookingsFull($offset, $limit, $orderByFieldName, $ascending = true, $searchArray = array())
    {
        $locationsMapper = LocationsMapper::getInstance();
        $doctorsMapper = DoctorsMapper::getInstance();
        $treatmentTypesMapper = TreatmentTypesMapper::getInstance();
        $samediBookingsMapper = SamediBookingsMapper::getInstance();
        $mailingsMapper = MailingsMapper::getInstance();

        $q = $this->getAllFieldsAsQueryString(true);

        $q = $q . ',' . $locationsMapper->getAllFieldsAsQueryString(true);
        $q = $q . ',' . $doctorsMapper->getAllFieldsAsQueryString(true);
        $q = $q . ',' . $treatmentTypesMapper->getAllFieldsAsQueryString(true);
        $q = $q . ',' . $samediBookingsMapper->getAllFieldsAsQueryString(true);
        $q = $q . ',' . $doctorsMapper->getCreatedByFieldsAsQueryString();
        $q = $q . ',count(`mailings`.`id`) as mailingsCount';

        $query = sprintf(
            "SELECT %s FROM `%s` left join `%s` on `%s`.`location_id`=`%s`.`id` left join `%s` on `%s`.`user_id`=`%s`.`id` 
            left join `%s` on `%s`.`treatmenttype_id`=`%s`.`id` left join `%s` on `%s`.`id`=`%s`.`booking_id` left join `%s` as `created` on `%s`.`created_by`=`created`.`id` 
            left join `%s` on `%s`.`source_id`=`%s`.`id`",
            $q, $this->getTableName(), $locationsMapper->getTableName(), $this->getTableName(), $locationsMapper->getTableName(),
            $doctorsMapper->getTableName(), $this->getTableName(), $doctorsMapper->getTableName(), $treatmentTypesMapper->getTableName(),
            $this->getTableName(), $treatmentTypesMapper->getTableName(), $samediBookingsMapper->getTableName(), $this->getTableName(),
            $samediBookingsMapper->getTableName(), $doctorsMapper->getTableName(), $this->getTableName(),
            $mailingsMapper->getTableName(), $mailingsMapper->getTableName(), $this->getTableName()
        );

        if (!empty($searchArray)) {
            $query .= "WHERE ";
            foreach ($searchArray as $columnName => $searchInfo) {
                if ($searchInfo['searchStyle'] != 'BETWEEN') {
                    $query .= sprintf("`bookings`.`%s` %s '%s' AND ", $columnName, $searchInfo['searchStyle'], $searchInfo['value']);
                } else {
                    $query .= sprintf("`bookings`.`%s` %s '%s' AND '%s' AND ", $columnName, $searchInfo['searchStyle'], $searchInfo['value1'], $searchInfo['value2']);
                }
            }
            $query = substr($query, 0, -4);
        }

        $query .= sprintf("GROUP BY `%s`.`id` ", $this->getTableName());
        $query .= sprintf("ORDER BY `%s` ", $orderByFieldName);
        if ($ascending) {
            $query = $query . sprintf("LIMIT %d, %d", $offset, $limit);
        } else {
            $query = $query . sprintf("DESC LIMIT %d, %d", $offset, $limit);
        }

        $res = $this->dbms->query($query);
        $results = $this->dbms->getResultArray($res);
        $resultArr = array();
        foreach ($results as $result) {

            $locationDto = $locationsMapper->createDto();
            $doctorDto = $doctorsMapper->createDto();
            $treatmentTypesDto = $treatmentTypesMapper->createDto();
            $samediBookingsDto = $samediBookingsMapper->createDto();
            $bookingDto = $this->createDto();
            $createdBy = $doctorsMapper->createDto();

            $this->initializeDto($bookingDto, $result, true);
            $locationsMapper->initializeDto($locationDto, $result, true);
            $doctorsMapper->initializeDto($doctorDto, $result, true);
            $treatmentTypesMapper->initializeDto($treatmentTypesDto, $result, true);
            $samediBookingsMapper->initializeDto($samediBookingsDto, $result, true);

            $bookingDto->addLocation($locationDto);
            $bookingDto->addUser($doctorDto);
            $bookingDto->addTreatmentType($treatmentTypesDto);
            $bookingDto->addSamediBookings($samediBookingsDto);
            $bookingDto->addMailingsCount($result["mailingsCount"]);
            foreach ($result as $fieldName => $fieldValue) {
                $i = strpos($fieldName, 'createdBy');
                if ($i !== false) {
                    $result[substr($fieldName, 10)] = $fieldValue;
                }
                unset($result[$fieldName]);
            }

            $doctorsMapper->initializeDto($createdBy, $result);
            $bookingDto->addCreatedByDto($createdBy);
            $resultArr[] = $bookingDto;
        }

        return $resultArr;
    }

    /**
     * @see AbstractMapper::createDto()
     */
    public function createDto()
    {
        return new BookingsDto();
    }

    public function getBookingByIdFull($id)
    {
        $locationsMapper = LocationsMapper::getInstance();
        $doctorsMapper = DoctorsMapper::getInstance();
        $treatmentTypesMapper = TreatmentTypesMapper::getInstance();
        $mailingsMapper = MailingsMapper::getInstance();

        $bookingDto = $this->selectByPK($id);
        
        $locationId = $bookingDto->getLocationId();
        if (isset($locationId) && $locationId != 0) {
            $locationDto = $locationsMapper->selectByPK($locationId);
        } else {
            $locationDto = null;
        }
        $doctorId = $bookingDto->getUserId();
        if (isset($doctorId) && $doctorId != 0) {
            $doctorDto = $doctorsMapper->selectByPK($doctorId);
        } else {
            $doctorDto = null;
        }
        $treatmentTypeId = $bookingDto->getTreatmenttypeId();
        if (isset($treatmentTypeId) && $treatmentTypeId != 0) {
            $treatmentTypeDto = $treatmentTypesMapper->selectByPK($treatmentTypeId);
        } else {
            $treatmentTypeDto = null;
        }

        $bookingDto->addLocation($locationDto);
        $bookingDto->addUser($doctorDto);
        $bookingDto->addTreatmentType($treatmentTypeDto);
        $createdBy = $bookingDto->getCreatedBy();
        if (isset($createdBy) && $createdBy != 0) {
            $createdByDto = $doctorsMapper->selectByPK($createdBy);
        } else {
            $createdByDto = null;
        }
        $bookingDto->creator = $createdByDto;
        $updatedBy = $bookingDto->getUpdatedBy();
        if (isset($updatedBy) && $updatedBy != 0) {
            $updatedByDto = $doctorsMapper->selectByPK($updatedBy);
        } else {
            $updatedByDto = null;
        }
        $bookingDto->update = $updatedByDto;

        $query = sprintf("SELECT * FROM `%s` WHERE `%s`.`source_id`=%d", $mailingsMapper->getTableName(), $mailingsMapper->getTableName(), $id);
        $res = $this->dbms->query($query);
        $results = $this->dbms->getResultArray($res);
        if (!empty($results)) {
            foreach ($results as $result) {
                $mailingDto = $mailingsMapper->createDto();
                $mailingsMapper->initializeDto($mailingDto, $result);
                $bookingDto->addMailing($mailingDto);
            }
        }

        return $bookingDto;
    }

    /**
     * Returns bookings by user_id.
     * If second parameter is false, returns user's all bookings.
     * If second parameter is 'upcoming', returns user's all upcoming bookings.
     * If second parameter is 'past', returns user's all past bookings.
     * 
     * @param int $userId
     * @param string $appointmentStartTime
     *
     * @return array
     */
    public function getAllBookingsByUserId($userId, $appointmentStartTime = '')
    {
        $query = sprintf("SELECT * FROM `%s` WHERE `user_id`=%d ", $this->getTableName(), $userId);

        if ($appointmentStartTime === 'upcoming') {
            $query .= sprintf(" AND `%s`.`appointment_start_at` > NOW() ", $this->getTableName());
        } elseif ($appointmentStartTime === 'past') {
            $query .= sprintf(" AND `%s`.`appointment_start_at` < NOW() ", $this->getTableName());
        }
        $resultArr = $this->fetchRows($query);

        return $resultArr;
    }

    public function getBookingsWithTreatmentTypeInDateRangeByUserId($userId, $dateFrom, $dateTo, $orderBy = "DESC")
    {
        $dateFrom .= " 00:00:00";
        $dateTo .= " 23:59:59";
        $treatmentTypesMapper = TreatmentTypesMapper::getInstance();
        $q = $this->getAllFieldsAsQueryString(true);
        $q = $q . ',' . $treatmentTypesMapper->getAllFieldsAsQueryString(true);

        $query = sprintf(
            "SELECT %s FROM `%s` LEFT JOIN `%s` on `%s`.`id`=`%s`.`treatmenttype_id` WHERE `user_id`=%d AND `%s`.`appointment_start_at` BETWEEN '%s' AND '%s' ORDER BY `appointment_start_at` %s ",
            $q, $this->getTableName(), $treatmentTypesMapper->getTableName(), $treatmentTypesMapper->getTableName(), $this->getTableName(), $userId, $this->getTableName(), $dateFrom, $dateTo, $orderBy
        );
        $res = $this->dbms->query($query);
        $results = $this->dbms->getResultArray($res);
        $resultArr = array();
        foreach ($results as $result) {
            $bookingDto = $this->createDto();
            $treatmentTypeDto = $treatmentTypesMapper->createDto();

            $this->initializeDto($bookingDto, $result, true);
            $treatmentTypesMapper->initializeDto($treatmentTypeDto, $result, true);

            $bookingDto->addTreatmentType($treatmentTypeDto);
            $resultArr[] = $bookingDto;
        }

        return $resultArr;
    }

    public function updateBookingsByEmail($oldEmail, $newEmail)
    {
        $query = sprintf("UPDATE `%s` SET `email`='%s' WHERE `email`='%s'", $this->getTableName(), $newEmail, $oldEmail);
        $res = $this->dbms->query($query);
        if ($res) {
            $result = $this->dbms->getAffectedRows();

            return $result;
        }

        return -1;
    }

    /**
     * @param int $integrationId
     * @param string $datetime format 'Y-m-d H:i:s'
     * @param array int $bookingStatuses
     *
     * @return BookingsDto[]
     */
    public function getIntegrationBookingsAfterGivenDatetime($integrationId, $datetime, $bookingStatuses)
    {
        $samediBookingsMapper = SamediBookingsMapper::getInstance();
        $locationsMapper = LocationsMapper::getInstance();

        $q = $this->getAllFieldsAsQueryString(true);
        $q = $q . ',' . $samediBookingsMapper->getAllFieldsAsQueryString(true);
        $q = $q . ',' . $locationsMapper->getAllFieldsAsQueryString(true);

        // build the query for all statuses
        $bookingStatusesSql = Booking::getStatusesSql($bookingStatuses, 'OR');
        $bookingStatusesSql = ($bookingStatusesSql == '') ? '' : ' AND (' . $bookingStatusesSql . ')';

        $query = sprintf(
            "SELECT %s 
             FROM `%s` 
             INNER JOIN `samedi_bookings` ON `%s`.`id`=`samedi_bookings`.`booking_id` 
             LEFT JOIN `locations` ON `%s`.`location_id`=`locations`.`id` 
             WHERE `locations`.`integration_id`=%d AND 
                 `%s`.`appointment_start_at` >= '%s'
                 %s",
            $q,
            $this->getTableName(),
            $this->getTableName(),
            $this->getTableName(),
            $integrationId,
            $this->getTableName(),
            $datetime,
            $bookingStatusesSql
        );

        $res = $this->dbms->query($query);
        $results = $this->dbms->getResultArray($res);
        $resultArr = array();
        foreach ($results as $result) {

            $locationDto = $locationsMapper->createDto();
            $samediBookingsDto = $samediBookingsMapper->createDto();
            $bookingDto = $this->createDto();
            $this->initializeDto($bookingDto, $result, true);
            $locationsMapper->initializeDto($locationDto, $result, true);
            $samediBookingsMapper->initializeDto($samediBookingsDto, $result, true);
            $bookingDto->addLocation($locationDto);
            $bookingDto->setIntegrationBookingDto($samediBookingsDto);
            $resultArr[] = $bookingDto;
        }

        return $resultArr;
    }

    public function getBookingsByUserIdAndStatus($userId, $status)
    {
        $query = sprintf("SELECT * FROM `%s` WHERE `user_id`=%d AND `status`=%d", $this->getTableName(), $userId, $status);
        $resultArr = $this->fetchRows($query);

        return $resultArr;
    }

    /**
     * @param int $locationId
     * @param int $status
     *
     * @return array
     */
    public function getBookingsByLocationIdAndStatus($locationId, $status)
    {
        $query = sprintf("SELECT * FROM `%s` WHERE `location_id`=%d AND `status`=%d", $this->getTableName(), $locationId, $status);
        $resultArr = $this->fetchRows($query);

        return $resultArr;
    }

    public function getBookingPeriodsAtLocation($location_id)
    {
        $periods = array();

        $sql
            = "SELECT DISTINCT
				DATE_FORMAT(appointment_start_at, '%%b-%%Y') AS periods
				FROM bookings
				WHERE location_id = %d;";

        $query = sprintf($sql, intval($location_id));

        if ($result = $this->dbms->query($query)) {
            $periods = $this->dbms->getResultArray($result);
        }

        return $periods;
    }

    /**
     * Returns bookings count by patient_id.
     * If second parameter is false, returns user's all bookings.
     * If second parameter is 'upcoming', returns user's all upcoming bookings.
     * If second parameter is 'past', returns user's all past bookings.
     *
     * @return int
     */

    public function getAllBookingsCountByPatientId($patientId, $appointmentStartTime = false)
    {
        $query = sprintf("SELECT COUNT(*) as count FROM `%s` LEFT JOIN `patients` ON `patients`.`email`=`bookings`.`email` WHERE `patients`.`id`=%d ", $this->getTableName(), $patientId);

        if ($appointmentStartTime === 'upcoming') {
            $query .= sprintf(" AND `%s`.`appointment_start_at` > NOW() ", $this->getTableName());
        } elseif ($appointmentStartTime === 'past') {
            $query .= sprintf(" AND `%s`.`appointment_start_at` < NOW() ", $this->getTableName());
        }

        $count = $this->fetchField($query, 'count');

        return $count;
    }

    /**
     * A catch-all function that will attempt to build a query to get bookings on any arbitrary fields.
     * It's not very good, but flexible enough for most purposes.
     *
     * @param array $where
     * @param array $select
     * @param array $orderby
     * @param int $limit
     * @param int $offset
     *
     * @return mixed
     */
    public function getBookings(array $where = array(), array $select = array(), array $orderby = array(), $limit = 0, $offset = 0)
    {
        $query = array();
        $sql = array();

        // Select statements
        if (!empty($select)) {
            foreach ($select as $alias => $field) {
                if (!is_numeric($alias)) {
                    $query['select'][] = sprintf("`%s` as '%s'", $field, $alias);
                } else {
                    $query['select'][] = sprintf("`%s`", $field);
                }
            }
        } else {
            $query['select'][] = '*';
        }

        // Where clauses
        foreach ($where as $condition) {
            $query['where'][] = $condition;
        }

        // Order by
        if (!empty($orderby)) {
            foreach ($orderby as $direction => $field) {
                if (!is_numeric($direction) && in_array(strtoupper($direction), array('DESC', 'ASC'))) {
                    $query['orderby'][] = sprintf("%s %s", $field, $direction);
                } else {
                    $query['orderby'][] = sprintf("%s", $field);
                }
            }
        }

        // Limit clause
        if (!empty($limit)) {
            if (intval($limit)) {
                $query['limit'] = sprintf('(%d, %d)', intval($offset), intval($limit));
            }
        }

        // Build the SQL
        $sql['select'] = sprintf('SELECT %s FROM `%s`', implode(',', $query['select']), $this->getTableName());

        if (!empty($query['where'])) {
            $sql['where'] = sprintf('WHERE %s', implode(' AND ', $query['where']));
        }

        if (!empty($query['orderby'])) {
            $sql['orderby'] = sprintf('ORDER BY %s', implode(',', $query['orderby']));
        }

        if (!empty($query['limit'])) {
            $sql['limit'] = sprintf('LIMIT %s', implode(',', $query['limit']));
        }

        return $this->fetchRows(implode(' ', $sql));
    }
}
