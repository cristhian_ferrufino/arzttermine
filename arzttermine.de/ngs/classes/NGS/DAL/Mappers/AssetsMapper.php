<?php

namespace NGS\DAL\Mappers;

use NGS\DAL\DTO\AssetsDto;
use NGS\Framework\DAL\Mappers\AbstractMapper;

class AssetsMapper extends AbstractMapper {

    /**
     * @var self
     */
    private static $instance = null;

    /**
     * @var string
     */
    protected $tableName = 'assets';

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @see AbstractMapper::createDto()
     */
    public function createDto()
    {
        return new AssetsDto();
    }

    /**
     * Return a profile pic asset from the database by userId
     *
     * @param $userId
     *
     * @return AssetsDto
     */
    public function getProfileAsset($userId)
    {
        return $this->getAssets($userId, 0, ASSET_OWNERTYPE_USER, 1);
    }

    /**
     * Get arbitrary profile asset
     *
     * @param int $owner_id
     * @param int $parent_id
     * @param int $type
     * @param int $limit
     * @param string $order_by
     *
     * @return null
     */
    public function getAssets($owner_id, $parent_id = 0, $type = ASSET_OWNERTYPE_USER, $limit = 0, $order_by = 'created_at')
    {
        if ($owner_id) {
            $sql = "SELECT * FROM `%s` WHERE `owner_id` = %d AND `parent_id` = %d AND owner_type = %d ORDER BY `%s` DESC %s";
            $query = sprintf($sql, $this->getTableName(), $owner_id, $parent_id, $type, $order_by, ($limit ? 'LIMIT ' . intval($limit) : ''));

            $result = $this->fetchRows($query);

            if (count($result) === 1) {
                return $result[0];
            }

            return $result;
        }

        return null;
    }

    /**
     * Get all profile assets by parentId
     *
     * @param $parentId
     *
     * @return array
     */
    public function getProfileChildAssets($parentId)
    {
        $sql = "SELECT * FROM `%s` WHERE `parent_id`=%d ORDER BY `size`";

        $query = sprintf($sql, $this->getTableName(), $parentId);

        $result = $this->fetchRows($query);

        return $result;
    }

    /**
     * Get all profile assets by userId
     *
     * @param int $userId
     *
     * @return array
     */
    public function getProfileAssets($userId)
    {
        return $this->getAssets($userId);
    }
}