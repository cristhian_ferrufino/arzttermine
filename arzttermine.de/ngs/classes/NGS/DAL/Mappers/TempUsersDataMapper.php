<?php

namespace NGS\DAL\Mappers;

use NGS\Framework\DAL\Mappers\AbstractMapper;
use NGS\DAL\DTO\TempUsersDataDto;

class TempUsersDataMapper extends AbstractMapper {

    /**
     * @var self
     */
    private static $instance = null;

    /**
     * @var string
     */
    protected $tableName = 'temp_users_data';

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @see AbstractMapper::createDto()
     */
    public function createDto()
    {
        return new TempUsersDataDto();
    }

    public function getByEmail($email)
    {
        $query = sprintf("SELECT * FROM `%s` WHERE `email` = '%s'", $this->getTableName(), $email);
        $result = $this->fetchRows($query);
        if (count($result) === 1) {
            return $result[0];
        }

        return null;
    }

    public function getByEmailAndPasswordHash($email, $passHash)
    {
        $query = sprintf("SELECT * FROM `%s` WHERE `email` = '%s' AND `password_hash`= '%s' ", $this->getTableName(), $email, $passHash);
        $result = $this->fetchRows($query);
        if (count($result) === 1) {
            return $result[0];
        }

        return null;
    }

    public function getByActivationCode($activationCode)
    {
        $query = sprintf("SELECT * FROM `%s` WHERE `activation_code`='%s'", $this->getTableName(), $activationCode);
        $results = $this->fetchRows($query);

        return $results[0];
    }
}