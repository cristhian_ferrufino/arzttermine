<?php

namespace NGS\DAL\Mappers;

use NGS\DAL\DTO\SamediPatientsConnectionsDto;
use NGS\Framework\DAL\Mappers\AbstractMapper;

class SamediPatientsConnectionsMapper extends AbstractMapper {

    /**
     * @var self
     */
    private static $instance = null;

    /**
     * @var string
     */
    protected $tableName = 'samedi_patients_connections';

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @see AbstractMapper::createDto()
     */
    public function createDto()
    {
        return new SamediPatientsConnectionsDto();
    }

    /**
     * @see AbstractMapper::getPKFieldName()
     */
    public function getPKFieldName()
    {
        return "patient_id";
    }
}