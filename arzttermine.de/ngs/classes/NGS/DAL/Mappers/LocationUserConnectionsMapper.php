<?php

namespace NGS\DAL\Mappers;

use NGS\DAL\DTO\LocationUserConnectionsDto;
use NGS\Framework\DAL\Mappers\AbstractMapper;

class LocationUserConnectionsMapper extends AbstractMapper {

    /**
     * @var self
     */
    private static $instance = null;

    /**
     * @var string
     */
    protected $tableName = 'location_user_connections';

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @see AbstractMapper::createDto()
     */
    public function createDto()
    {
        return new LocationUserConnectionsDto();
    }

    /**
     * @see AbstractMapper::getPKFieldName()
     */
    public function getPKFieldName()
    {
        return "user_id";
    }

    public function removeByLocationIds($locationIds)
    {
        $sql = "DELETE from `%s` WHERE `location_id` IN (%s)";
        $query = sprintf($sql, $this->getTableName(), $locationIds);
        $this->dbms->query($query);

        return true;
    }

    public function getByLocationUserIds($userId, $locationId)
    {
        $sql = "SELECT * from `%s` WHERE `user_id` = %d AND `location_id` = %d";
        $query = sprintf($sql, $this->getTableName(), $userId, $locationId);
        $result = $this->fetchRows($query);
        if (count($result) >= 1) {
            return $result[0];
        }

        return null;
    }
}