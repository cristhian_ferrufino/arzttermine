<?php

namespace NGS\DAL\Mappers;

use NGS\DAL\DTO\TerminlandBookingsDto;
use NGS\Framework\DAL\Mappers\AbstractMapper;

class TerminlandBookingsMapper extends AbstractMapper {

    /**
     * @var self
     */
    private static $instance = null;

    /**
     * @var string
     */
    protected $tableName = 'integration_2_bookings';

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @see AbstractMapper::createDto()
     */
    public function createDto()
    {
        return new TerminlandBookingsDto();
    }

    public function getByBookingId($bookingId)
    {
        $sqlQuery = sprintf("SELECT * FROM `%s` WHERE `booking_id`=%d", $this->getTableName(), $bookingId);
        $rows = $this->fetchRows($sqlQuery);
        if (count($rows) === 1) {
            return $rows[0];
        } else {
            return null;
        }
    }
}