<?php

namespace NGS\DAL\Mappers;

use NGS\DAL\DTO\StatementsDto;
use NGS\Framework\DAL\Mappers\AbstractMapper;

class StatementsMapper extends AbstractMapper {

    /**
     * @var self
     */
    private static $instance = null;

    /**
     * @var string
     */
    protected $tableName = 'statements';

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @see AbstractMapper::createDto()
     */
    public function createDto()
    {
        return new StatementsDto();
    }

    /**
     * Update a statement for a location
     *
     * @param $location_id
     * @param $period
     * @param $status
     *
     * @return int -1 for failure, anything else for rows affected
     */
    public function updateStatement($location_id, $period, $status)
    {
        $query = sprintf(
            "UPDATE `%s` SET `status` = '%s' WHERE `location_id` = %d && `monthyear` = '%s'",
            $this->getTableName(),
            (int)(!!$status),
            intval($location_id),
            $this->dbms->escape($period)
        );

        $res = $this->dbms->query($query);

        if ($res) {
            return $this->dbms->getAffectedRows();
        }

        return -1;
    }

    /**
     * Get a locations statements for a given time period. Should only ever be one
     *
     * @param $location_id
     * @param $period
     *
     * @return array
     */
    public function getStatements($location_id, $period)
    {
        $query = sprintf(
            "SELECT * FROM `%s` WHERE `location_id` = %d AND `period` = '%s' LIMIT 1",
            $this->getTableName(),
            intval($location_id),
            $this->dbms->escape($period)
        );

        return $this->fetchRows($query);
    }
}
