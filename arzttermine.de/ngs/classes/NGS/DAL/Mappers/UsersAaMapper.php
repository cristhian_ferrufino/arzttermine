<?php

namespace NGS\DAL\Mappers;

use NGS\DAL\DTO\UsersAaDto;
use NGS\Framework\DAL\Mappers\AbstractMapper;

class UsersAaMapper extends AbstractMapper {

    /**
     * @var self
     */
    private static $instance = null;

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function getByUserId($userId)
    {
        $sql = sprintf("SELECT * FROM `%s` WHERE `user_id`=%d", $this->getTableName(), $userId);

        return $this->fetchRows($sql);
    }

    /**
     * @see AbstractMapper::getTableName()
     */
    public function getTableName()
    {
        if (isset($GLOBALS["CONFIG"]['USER_AA_TABLE_TMP']) && $GLOBALS["CONFIG"]['USER_AA_TABLE_TMP'] == true) {
            return "users_aa_tmp";
        } else {
            return "users_aa";
        }
    }

    public function getByFilters($dateStart, $dateEnd, $userId, $locationIdsCommaSeparated, $insurance = null, $treatmentTypeIds = null)
    {
        $treatmentTypesMapper = TreatmentTypesMapper::getInstance();
        $q = $this->getAllFieldsAsQueryString(true);
        $q .= ',' . $treatmentTypesMapper->getAllFieldsAsQueryString(true);
        if (strpos($locationIdsCommaSeparated, ',') !== false) {
            $subQuery = sprintf("FIND_IN_SET(`location_id`, '%s')", $locationIdsCommaSeparated);
        } else {
            $subQuery = sprintf("`location_id`=%d", $locationIdsCommaSeparated);
        }

        $sqlQuery = sprintf("SELECT %s FROM `%s` LEFT JOIN `%s` ON FIND_IN_SET(`%s`.`id`,`%s`.`treatment_type_ids`) WHERE `user_id` = %d AND %s AND `date`>='%s' AND `date`<='%s' ", $q, $this->getTableName(), $treatmentTypesMapper->getTableName(), $treatmentTypesMapper->getTableName(), $this->getTableName(), $userId, $subQuery, $dateStart, $dateEnd);
        if (!empty($insurance)) {
            $sqlQuery .= " AND FIND_IN_SET('" . $insurance . "', `insurance_types`) ";
        }
        if (!empty($treatmentTypeIds)) {
            $treatmentTypeIdsArray = explode(',', $treatmentTypeIds);
            $sqlQuery .= " AND (";
            foreach ($treatmentTypeIdsArray as $key => $ttId) {
                $sqlQuery .= ($key > 0 ? ' OR ' : '') . " FIND_IN_SET('" . $ttId . "', `treatment_type_ids`) ";
            }
            $sqlQuery .= " ) ";
        }
        $sqlQuery .= " ORDER BY `date`, `start_at`";
        
        $res = $this->dbms->query($sqlQuery);
        $results = $this->dbms->getResultArray($res);

        $existingUsersAaIdsArray = array();
        foreach ($results as $result) {
            $ttDto = $treatmentTypesMapper->createDto();
            $treatmentTypesMapper->initializeDto($ttDto, $result, true);

            if (!empty($result["users_aa_id"]) && !empty($existingUsersAaIdsArray[$result["users_aa_id"]])) {
                $usersAaDto = $existingUsersAaIdsArray[$result["users_aa_id"]];
            } else {
                $usersAaDto = $this->createDto();
                $this->initializeDto($usersAaDto, $result, true);
                $existingUsersAaIdsArray[$usersAaDto->getId()] = $usersAaDto;
            }

            $usersAaDto->addTreatmentTypeDtos($ttDto);
            $existingUsersAaIdsArray[$usersAaDto->getId()] = $usersAaDto;
        }
        $resultArr = array_values($existingUsersAaIdsArray);

        return $resultArr;
    }

    /**
     * @see AbstractMapper::createDto()
     */
    public function createDto()
    {
        return new UsersAaDto();
    }

    /**
     * This function removes all the given user related data from users_aa table by user_id and location_id.
     *
     * @param type $userId
     * @param type $locationId
     * @param bool $returnOnlySql if true then the function returns only the SQL string to run later and not make any
     * change into DB. Otherwise it run the query and returns affected rows in the result.
     *
     * @return mixed type SQL or affected rows.
     */
    public function deleteUserAllAvaialableTimesByUserIdAndLocationId($userId, $returnOnlySql = false)
    {
        $q = "DELETE FROM `%s` WHERE `user_id`=%d";
        $query = sprintf($q, $this->getTableName(), $userId);
        if ($returnOnlySql) {
            return $query . ";";
        }
        $res = $this->dbms->query($query);
        if ($res) {
            $result = $this->dbms->getAffectedRows();

            return $result;
        }

        return -1;
    }

    /**
     * This function removes the given user related data from users_aa table by user_id and location_id and date range.
     *
     * @param type $userId
     * @param type $locationId
     * @param type $dateStart
     * @param type $dateEnd
     * @param bool $returnOnlySql if true then the function returns only the SQL string to run later and not make any
     * change into DB. Otherwise it run the query and returns affected rows in the result.
     *
     * @return type
     */
    public function deleteUserAllAvaialableTimesByUserIdAndLocationIdAndDateRage($userId, $dateStart, $dateEnd, $returnOnlySql = false)
    {
        $q = "DELETE FROM `%s` WHERE `user_id`=%d AND `date`>='%s' AND `date`<='%s'";
        $query = sprintf($q, $this->getTableName(), $userId, $dateStart, $dateEnd);
        if ($returnOnlySql) {
            return $query . ";";
        }
        $res = $this->dbms->query($query);
        if ($res) {
            $result = $this->dbms->getAffectedRows();

            return $result;
        }

        return -1;
    }

    /**
     *
     * @param int $docId
     * @param str $appDateTime timestamp
     * returns doctor's appointment row in which the $appTime is included
     */
    public function getByDoctorIdAndAppointmentTime($docId, $appDateTime)
    {
        $date = date('Y-m-d', strtotime($appDateTime));
        $time = date('H:i:00', strtotime($appDateTime));
        $q = "SELECT * FROM `%s` WHERE `user_id`=%d AND `date`='%s' AND `start_at`<='%s'  AND `end_at`>'%s' ";
        $query = sprintf($q, $this->getTableName(), $docId, $date, $time, $time);
        $results = $this->fetchRows($query);
        if (count($results) === 1) {
            return $results[0];
        }

        return null;
    }
}