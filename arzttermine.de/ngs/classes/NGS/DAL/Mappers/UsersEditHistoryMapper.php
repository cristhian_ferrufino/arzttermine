<?php

namespace NGS\DAL\Mappers;

use NGS\DAL\DTO\UsersEditHistoryDto;
use NGS\Framework\DAL\Mappers\AbstractMapper;

class UsersEditHistoryMapper extends AbstractMapper {

    /**
     * @var self
     */
    private static $instance = null;

    /**
     * @var string
     */
    protected $tableName = 'users_edit_history';

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @see AbstractMapper::createDto()
     */
    public function createDto()
    {
        return new UsersEditHistoryDto();
    }

    /**
     * @param int $doctorId
     *
     * @return array
     */
    public function getByDoctorId($doctorId)
    {
        $query = sprintf(
            "SELECT * FROM `%s` WHERE `user_id`=%d ORDER BY `updated_at`",
            $this->getTableName(), $doctorId
        );

        return $this->fetchRows($query);
    }

    /**
     * @param int $userId
     * @param int $limit
     *
     * @return array
     */
    public function getByUserId($userId, $limit = null)
    {
        $query = sprintf("SELECT * FROM `%s` WHERE `user_id`=%d ORDER BY `updated_at` DESC ", $this->getTableName(), $userId);
        if (isset($limit)) {
            $query .= "LIMIT " . $limit;
        }

        return $this->fetchRows($query);
    }
}