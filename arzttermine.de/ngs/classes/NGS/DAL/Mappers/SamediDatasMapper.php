<?php

namespace NGS\DAL\Mappers;

use NGS\DAL\DTO\SamediDatasDto;
use NGS\Framework\DAL\Mappers\AbstractMapper;

define('DB_TABLENAME_INTEGRATION_5_DATAS', DB_NAME . '.samedi_datas');

class SamediDatasMapper extends AbstractMapper {

    /**
     * @var self
     */
    private static $instance = null;

    /**
     * @var string
     */
    protected $tableName = 'samedi_datas';

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @see AbstractMapper::createDto()
     */
    public function createDto()
    {
        return new SamediDatasDto();
    }

    public function getAllSamediDatasJoinWithUsersAndLocations($offset, $limit, $orderByFieldName)
    {
        $sqlQuery = sprintf(
            "SELECT `%s`.*, `users`.`first_name` as `user_first_name`, `users`.`last_name` as `user_last_name` ,
             `locations`.`name` as `location_name` FROM `%s` LEFT JOIN `users` on `%s`.`user_id` = `users`.`id` 
             LEFT JOIN `locations` ON  `%s`.`location_id` =  `locations`.`id`
             LEFT JOIN `samedi_category_events` on `%s`.`category_id` = `samedi_category_events`.`category_id`
           ORDER BY `%s`.`%s` LIMIT %d, %d", $this->getTableName(), $this->getTableName(), $this->getTableName(), $this->getTableName(), $this->getTableName(), $this->getTableName(), $orderByFieldName, $offset, $limit
        );

        return $this->fetchRows($sqlQuery);
    }

    public function getAllSamediDatasCount($searchArray = array())
    {
        $q = '';
        if (!empty($searchArray)) {
            $q = " WHERE";
            foreach ($searchArray as $columnName => $searchInfo) {
                if ($searchInfo['searchStyle'] != 'BETWEEN') {
                    $q .= sprintf("`samedi_datas`.`%s` %s '%s' AND ", $columnName, $searchInfo['searchStyle'], $searchInfo['value']);
                } else {
                    $q .= sprintf("`samedi_datas`.`%s` %s '%s' AND '%s' AND ", $columnName, $searchInfo['searchStyle'], $searchInfo['value1'], $searchInfo['value2']);
                }
            }
            $q = substr($q, 0, -4);
        }
        $sqlQuery = sprintf("SELECT count(`id`) as `count`  FROM `%s`", $this->getTableName());
        $sqlQuery .= $q;

        return $this->fetchField($sqlQuery, 'count');
    }

    public function getAllSamediDatas()
    {
        $sqlQuery = sprintf("SELECT * FROM `%s`", $this->getTableName());

        return $this->fetchRows($sqlQuery);
    }

    public function getByUserIdAndLocationId($user_id, $location_id)
    {
        $sqlQuery = sprintf("SELECT * FROM `%s` WHERE `user_id`=%d AND `location_id`=%d ", $this->getTableName(), $user_id, $location_id);
        $rows = $this->fetchRows($sqlQuery);
        if (count($rows) === 1) {
            return $rows[0];
        } else {
            return null;
        }
    }

    /**
     * @param $category_id
     * @param $practice_id
     *
     * @return SamediDatasDto
     */
    public function getByCategoryIdAndPracticeId($category_id, $practice_id)
    {
        $sqlQuery = sprintf("SELECT * FROM `%s` WHERE `category_id`=%d AND `practice_id`=%d ", $this->getTableName(), $category_id, $practice_id);
        $rows = $this->fetchRows($sqlQuery);
        if (count($rows) === 1) {
            return $rows[0];
        } else {
            return null;
        }
    }
}