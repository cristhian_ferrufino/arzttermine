<?php

namespace NGS\DAL\Mappers;

use NGS\DAL\DTO\SamediBookingsDto;
use NGS\Framework\DAL\Mappers\AbstractMapper;

class SamediBookingsMapper extends AbstractMapper {

    /**
     * @var self
     */
    private static $instance = null;

    /**
     * @var string
     */
    protected $tableName = 'samedi_bookings';

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function getByBookingId($bookingId)
    {
        $sqlQuery = sprintf("SELECT * FROM `%s` WHERE `booking_id`=%d", $this->getTableName(), $bookingId);
        $rows = $this->fetchRows($sqlQuery);
        if (count($rows) === 1) {
            return $rows[0];
        } else {
            return null;
        }
    }

    public function getAllBookingsCount($searchArray = array())
    {
        $q = '';
        if (!empty($searchArray)) {
            $q = "WHERE ";
            foreach ($searchArray as $columnName => $searchInfo) {
                if ($searchInfo['searchStyle'] != 'BETWEEN') {
                    $q .= sprintf("`%s` %s '%s' AND ", $columnName, $searchInfo['searchStyle'], $searchInfo['value']);
                } else {
                    $q .= sprintf("`%s` %s '%s' AND '%s' AND ", $columnName, $searchInfo['searchStyle'], $searchInfo['value1'], $searchInfo['value2']);
                }
            }
            $q = substr($q, 0, -4);
        }

        $sqlQuery = sprintf("SELECT count(`id`) as `count`  FROM `%s`", $this->getTableName());
        $sqlQuery .= $q;

        return $this->fetchField($sqlQuery, 'count');
    }

    public function getBookingsFull($offset, $limit, $orderByFieldName, $ascending = true, $searchArray = array())
    {
        $samediTreatmentTypesMapper = SamediTreatmentTypesMapper::getInstance();
        $treatmentTypesMapper = TreatmentTypesMapper::getInstance();

        $q = $this->getAllFieldsAsQueryString(true);
        $q = $q . ',' . $treatmentTypesMapper->getAllFieldsAsQueryString(true);
        $q = $q . ',' . $samediTreatmentTypesMapper->getAllFieldsAsQueryString(true);

        $query = sprintf(
            "SELECT %s FROM `%s` left join `%s` on `%s`.`event_type_id`=`%s`.`id`
                 left join `%s` on `%s`.`id`=`%s`.`parent_id`",
            $q, $this->getTableName(), $samediTreatmentTypesMapper->getTableName(), $this->getTableName(), $samediTreatmentTypesMapper->getTableName(),
            $treatmentTypesMapper->getTableName(), $treatmentTypesMapper->getTableName(), $samediTreatmentTypesMapper->getTableName()
        );

        if (!empty($searchArray)) {

            $query .= "WHERE ";
            foreach ($searchArray as $columnName => $searchInfo) {
                if ($searchInfo['searchStyle'] != 'BETWEEN') {
                    $query .= sprintf("`%s`.`%s` %s '%s' AND ", $this->getTableName(), $columnName, $searchInfo['searchStyle'], $searchInfo['value']);
                } else {
                    $query .= sprintf("`%s`.`%s` %s '%s' AND '%s' AND ", $this->getTableName(), $columnName, $searchInfo['searchStyle'], $searchInfo['value1'], $searchInfo['value2']);
                }
            }
            $query = substr($query, 0, -4);
        }

        $query .= sprintf("GROUP BY `%s`.`id` ", $this->getTableName());
        $query .= sprintf("ORDER BY `%s` ", $orderByFieldName);
        if ($ascending) {
            $query = $query . sprintf("LIMIT %d, %d", $offset, $limit);
        } else {
            $query = $query . sprintf("DESC LIMIT %d, %d", $offset, $limit);
        }
        $res = $this->dbms->query($query);
        $results = $this->dbms->getResultArray($res);
        $resultArr = array();
        foreach ($results as $result) {
            $samediBookingDto = $this->createDto();
            $samediTreatmentTypesDto = $samediTreatmentTypesMapper->createDto();
            $treatmentTypesDto = $treatmentTypesMapper->createDto();
            $samediTreatmentTypesMapper->initializeDto($samediTreatmentTypesDto, $result, true);
            $treatmentTypesMapper->initializeDto($treatmentTypesDto, $result, true);
            $this->initializeDto($samediBookingDto, $result, true);
            $samediBookingDto->setSamediTreatmentTypeDto($treatmentTypesDto);
            $samediTreatmentTypesDto->setBaseTreatmentTypeDto($treatmentTypesDto);
            $resultArr[] = $samediBookingDto;
        }

        return $resultArr;
    }

    /**
     * @see AbstractMapper::createDto()
     */
    public function createDto()
    {
        return new SamediBookingsDto();
    }
}