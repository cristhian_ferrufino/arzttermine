<?php

namespace NGS\DAL\Mappers;

use NGS\DAL\DTO\SamediPracticeCategoriesDto;
use NGS\Framework\DAL\Mappers\AbstractMapper;

class SamediPracticeCategoriesMapper extends AbstractMapper {

    /**
     * @var self
     */
    private static $instance = null;

    /**
     * @var string
     */
    protected $tableName = 'samedi_practice_categories';

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @see AbstractMapper::createDto()
     */
    public function createDto()
    {
        return new SamediPracticeCategoriesDto();
    }

    public function truncateTable()
    {
        $sqlQuery = sprintf("TRUNCATE TABLE `%s`", $this->getTableName());
        $this->dbms->query($sqlQuery);
    }

    public function getPracticeCategoriesBySamediPracticeId($samediPracticeId)
    {
        $sqlQuery = sprintf("SELECT * FROM `%s` WHERE `samedi_practice_id`='%s'", $this->getTableName(), $samediPracticeId);

        return $this->fetchRows($sqlQuery);
    }
}