<?php

namespace NGS\DAL\DTO;

use NGS\Framework\DAL\DTO\AbstractDto;

/**
 * @method int getId
 * @method int getUserId
 * @method int getLocationId
 * @method int getCategoryId
 * @method int getPracticeId
 * @method string getCreatedAt
 * @method string getCreatedBy
 * @method string getUpdatedAt
 * @method string getUpdatedBy
 */
class SamediDatasDto extends AbstractDto {

    protected $mapArray
        = array(
            "id"          => "id",
            "user_id"     => "userId",
            "location_id" => "locationId",
            "category_id" => "categoryId",
            "practice_id" => "practiceId",
            "created_at"  => "createdAt",
            "created_by"  => "createdBy",
            "updated_at"  => "updatedAt",
            "updated_by"  => "updatedBy"
        );

    private $createdByDto = null;

    public function getMapArray()
    {
        return $this->mapArray;
    }

    public function addCreatedByDto($userDto)
    {
        $this->createdByDto = $userDto;
    }

    public function getCreatedByDto()
    {
        return $this->createdByDto;
    }
}