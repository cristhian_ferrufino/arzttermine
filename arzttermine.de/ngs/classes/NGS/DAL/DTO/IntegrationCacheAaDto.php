<?php

namespace NGS\DAL\DTO;

use NGS\Framework\DAL\DTO\AbstractDto;

/**
 * AdminDto class is extended class from AbstractDto.
 *
 * @author  Vahagn Kirakosyan
 */
class IntegrationCacheAaDto extends AbstractDto {

    // Map of DB value to Field value
    protected $mapArray = array(
                            "appointment_date" => "appointmentDate", 
                            "appointment" => "appointment",
                            "user_id" => "userId",
                            "location_id" => "locationId",
                            "insurance_id" => "insuranceId",
                            "created_at" => "createdAt",    
                            );
    // constructs class instance
    public function __construct() {
    }

    // returns map array
    public function getMapArray() {
        return $this->mapArray;
    }

}