<?php

namespace NGS\DAL\DTO;

use NGS\Framework\DAL\DTO\AbstractDto;

class MedicalSpecialtiesDto extends AbstractDto {


    private $treatmentTypesDtos=array();
    // Map of DB value to Field value
    protected $mapArray = array("id" => "id", 
                                "slug_de" => "slugDe", 
                                "slug_en" => "slugEn", 
                                "status" => "status",
                                "name_singular_de" => "nameSingularDe", 
                                "name_singular_en" => "nameSingularEn", 
                                "name_plural_de" => "namePluralDe",
                                "name_plural_en" => "namePluralEn", 
                                "created_at" => "createdAt",
                                "created_by" => "createdBy",
                                "updated_at" => "updatedAt",
                                "updated_by" => "updatedBy",
                                );

    // returns map array
    public function getMapArray() {
        return $this->mapArray;
    }

    public function addTreatmentTypeDto($treatmentTypeDto){
        $this->treatmentTypesDtos[]=$treatmentTypeDto;
    }

    /**
     * @return TreatmentTypesDto[]
     */
    public function getTreatmentTypesDtos(){
        return $this->treatmentTypesDtos;
    }
    
}