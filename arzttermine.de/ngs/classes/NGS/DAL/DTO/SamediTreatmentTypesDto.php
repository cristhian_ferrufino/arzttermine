<?php

namespace NGS\DAL\DTO;

use NGS\Framework\DAL\DTO\AbstractDto;

/**
 * Class SamediTreatmentTypesDto
 *
 * @method int getId
 * @method int getParentId
 * @method string getName
 * @method string getInsuranceIds
 * @method string getDuration
 * @method string getCreatedAt
 */
class SamediTreatmentTypesDto extends AbstractDto {

    /**
     * @var array
     */
    protected $mapArray = array(
        "id" => "id",
        "parent_id" => "parentId",
        "name" => "name",
        "insurance_ids" => "insuranceIds",
        "duration" => "duration",
        "created_at" => "createdAt"
    );

    /**
     * @var mixed
     */
    private $baseTreatmenttypeDto;

    /**
     * @return array
     */
    public function getMapArray()
    {
        return $this->mapArray;
    }

    /**
     * @param $ttDto
     */
    public function setBaseTreatmentTypeDto($ttDto)
    {
        $this->baseTreatmenttypeDto = $ttDto;
    }

    /**
     * @return mixed
     */
    public function getBaseTreatmentTypeDto()
    {
        return $this->baseTreatmenttypeDto;
    }

}