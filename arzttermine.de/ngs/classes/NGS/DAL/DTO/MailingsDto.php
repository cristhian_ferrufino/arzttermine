<?php

namespace NGS\DAL\DTO;

use NGS\Framework\DAL\DTO\AbstractDto;

class MailingsDto extends AbstractDto {

	// Map of DB value to Field value
	protected $mapArray = array("id" => "id", 
								"status" => "status",
								"type" => "type", 
								"source_type_id" => "sourceTypeId", 
								"source_id" => "sourceId",
								"mailto" => "mailto", 
								"mailcc" => "mailcc", 
								"mailbcc" => "mailbcc", 
								"subject" => "subject",
								"content_id" => "contentId",
								"content_filename" => "contentFileName",
								"body_text" => "bodyText",
								"body_html" => "bodyHtml", 
								"created_at" => "createdAt",
								"created_by" => "createdBy",
								);
								
	public function __construct() {
	}

	// returns map array
	public function getMapArray() {
		return $this->mapArray;
	}

}
