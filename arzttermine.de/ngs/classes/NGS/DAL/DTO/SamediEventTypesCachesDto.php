<?php

namespace NGS\DAL\DTO;

use NGS\Framework\DAL\DTO\AbstractDto;

/**
 * AdminDto class is extended class from AbstractDto.
 *
 * @author	Vahagn Sookiasian
 */
class SamediEventTypesCachesDto extends AbstractDto {

	// Map of DB value to Field value
	protected $mapArray = array("id" => "id", "practice_id" => "practiceId", "category_id" => "categoryId", "event_types" => "eventTypes", "created_date"=>"createdDate");
	// constructs class instance
	public function __construct() {
	}

	// returns map array
	public function getMapArray() {
		return $this->mapArray;
	}

}
