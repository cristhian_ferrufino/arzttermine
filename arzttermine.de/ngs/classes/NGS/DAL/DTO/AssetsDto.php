<?php

namespace NGS\DAL\DTO;

use NGS\Framework\DAL\DTO\AbstractDto;

/**
 * @method int getId
 * @method int getGalleryId
 * @method int getOwnerId
 * @method int getOwnerType
 * @method int getParentId
 * @method int getSize
 * @method int getFileSize
 * @method int getWidth
 * @method int getHeight
 * @method string getDescription
 * @method string getFileName
 * @method string getMimeType
 * @method string getCreatedAt
 * @method string getCreatedBy
 * @method string getUpdatedAt
 * @method string getUpdatedBy
 */
class AssetsDto extends AbstractDto {

	// Map of DB value to Field value
	protected $mapArray = array(
		"id" => "id",
		"gallery_id" => "galleryId",
		"owner_id" => "ownerId",
		"owner_type" => "ownerType", 
		"parent_id" => "parentId",
		"size" => "size",
		"filesize" => "fileSize",
		"width" => "width",
		"height" => "height",
		"description" => "description",
		"filename" => "fileName",
		"mime_type" => "mimeType",
		"created_at" => "createdAt",
		"created_by" => "createdBy",
		"updated_at" => "updatedAt",
		"updated_by" => "updatedBy"
	);
	
	// constructs class instance
	public function __construct() {
	}

	/**
	 * @return array
	 */
	public function getMapArray() {
		return $this->mapArray;
	}

}
