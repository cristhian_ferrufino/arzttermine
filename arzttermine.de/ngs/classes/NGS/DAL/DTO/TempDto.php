<?php

namespace NGS\DAL\DTO;

use NGS\Framework\DAL\DTO\AbstractDto;

/**
 * AdminDto class is extended class from AbstractDto.
 *
 * @author	Vahagn Sookiasian
 */
class TempDto extends AbstractDto {

	// Map of DB value to Field value
	protected $mapArray = array("id" => "id", "gender" => "gender", "title" => "title", "fname" => "fname",
		"lname" => "lname", "street" => "street","location_name"=>"locationName",  
		"zip" => "zip", "city" => "city", "med_spec_id" => "medSpecId"
	);

	// constructs class instance
	public function __construct() {
		
	}

	// returns map array
	public function getMapArray() {
		return $this->mapArray;
	}

}