<?php

namespace NGS\DAL\DTO;

use NGS\Framework\DAL\DTO\AbstractDto;

/**
 * AdminDto class is extended class from AbstractDto.
 *
 * @author	Vahagn Kirakosyan
 */
class LocationUserConnectionsDto extends AbstractDto {

	// Map of DB value to Field value
	protected $mapArray = array("user_id" => "userId", "location_id" => "locationId");
	// constructs class instance
	public function __construct() {
	}

	// returns map array
	public function getMapArray() {
		return $this->mapArray;
	}
}