<?php

namespace NGS\DAL\DTO;

use NGS\Framework\DAL\DTO\AbstractDto;

/**
 * Class UsersDto
 * 
 * @method int getId
 * @method string getSlug
 * @method string getStatus
 * @method int getGroupId
 * @method string getEmail
 * @method string getGender
 * @method string getTitle
 * @method string getFirstName
 * @method string getLastName
 * @method string getPhone
 * @method string getMedicalSpecialtyIds
 * @method string getPasswordHash
 * @method string getPasswordHashSalt
 * @method string getFixedWeek
 * @method int getProfileAssetId
 * @method string getResetPasswordCode
 * @method int getNewsletterSubscription
 * @method float getRatingAverage
 * @method float getRating1
 * @method float getRating2
 * @method float getRating3
 * @method float getDocinfo1
 * @method float getDocinfo2
 * @method float getDocinfo3
 * @method float getDocinfo4
 * @method float getDocinfo5
 * @method float getDocinfo6
 * @method string getInfoSeo
 * @method string getHtmlTitle
 * @method string getHtmlMetaDescription
 * @method string getHtml_meta_keywords
 * @method string getCommentIntern
 * @method string getCreatedAt
 * @method string getCreatedBy
 * @method string getActivationCode
 * @method string getActivatedAt
 * @method string getUpdatedAt
 * @method string getUpdatedBy
 * @method string getLastLoginAt
 * @method string getWrongLoginAttempt
 * @method bool getHasContract
 * @method int getLocationId
 * @method int getLocationIntegrationId
 * @method string getLocationName
 * @method string getLocationStreet
 * @method string getLocationCity
 * @method string getLocationZip
 * @method string getLocationWww
 * @method string getLocationPhone
 * @method int getGroupAccessId
 */
class UsersDto extends AbstractDto {

	// Map of DB value to Field value
	protected $mapArray = array(
		"id" => "id", 
		"slug" => "slug",
		"status" => "status",
		"group_id" => "groupId",
		"email" => "email",
		"gender" => "gender",
		"title" => "title",
		"first_name" => "firstName",
		"last_name" => "lastName",
		"phone" => "phone",
		"medical_specialty_ids" => "medicalSpecialtyIds",
		"password_hash" => "passwordHash", 
		"password_hash_salt" => "passwordHashSalt",
		"fixed_week"=>"fixedWeek",
		"profile_asset_id" => "profileAssetId",
		"profile_asset_filename" => "profileAssetFilename",
		"reset_password_code" => "resetPasswordCode",
		"newsletter_subscription" => "newsletterSubscription",
		"rating_average" => "ratingAverage",
		"rating_1" => "rating1",
		"rating_2" => "rating2",
		"rating_3" => "rating3",
		"docinfo_1" => "docinfo1",
		"docinfo_2" => "docinfo2",
		"docinfo_3" => "docinfo3",
		"docinfo_4" => "docinfo4",
		"docinfo_5" => "docinfo5",
		"docinfo_6" => "docinfo6",
		"info_seo" => "infoSeo",
		"html_title" => "htmlTitle",
		"html_meta_description" => "htmlMetaDescription",
		"html_meta_keywords" => "html_meta_keywords",
		"comment_intern" => "commentIntern",
		"created_at" => "createdAt",
		"created_by" => "createdBy",
		"activation_code" => "activationCode",
		"activated_at" => "activatedAt",
		"updated_at" => "updatedAt",
		"updated_by" => "updatedBy",
		"last_login_at" => "lastLoginAt",
		"wrong_login_attempt"=>"wrongLoginAttempt",
		"has_contract"=>"hasContract",
		"location_id" => "locationId",
		"location_integration_id"=>"locationIntegrationId",
		"location_name" => "locationName",
		"location_street" => "locationStreet",
		"location_city" => "locationCity",
		"location_zip" => "locationZip",
		"location_www" => "locationWww",
		"location_phone" => "locationPhone",
		"group_access_id"=>"groupAccessId"
	);
	
	// constructs class instance
	public function __construct() {
	}

	// returns map array
	public function getMapArray() {
		return $this->mapArray;
	}
        
	public function getDoctorFullName() {
		return trim("{$this->getTitle()} {$this->getFirstName()} {$this->getLastName()}");
   }

}
