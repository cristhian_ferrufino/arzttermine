<?php

namespace NGS\DAL\DTO;

use Arzttermine\Application\Application;
use Arzttermine\User\User;
use NGS\Managers\MedicalSpecialtiesManager;

class DoctorsDto extends UsersDto {

	private $locationsDtos = array();
	private $samediDatasDtos = array();
	private $samediEventTypesCachesDtos = array();
	private $doctorsTreatmentTypeDto = null;
	private $doctorsMedicalSpecialtyDtos = array();
	
	protected $mapArray = array(
        "id" => "id",
        "slug" => "slug",
        "status" => "status",
        "group_id" => "groupId",
        "email" => "email",
        "gender" => "gender",
		"title" => "title",
        "first_name" => "firstName",
        "last_name" => "lastName",
        "phone" => "phone",
        "medical_specialty_ids" => "medicalSpecialtyIds",
		"password_hash" => "passwordHash",
        "password_hash_salt" => "passwordHashSalt",
        "fixed_week" => "fixedWeek",
        "profile_asset_id" => "profileAssetId",
        "profile_asset_filename" => "profileAssetFilename",
		"reset_password_code" => "resetPasswordCode",
        "newsletter_subscription" => "newsletterSubscription",
		"rating_average" => "ratingAverage",
        "rating_1" => "rating1",
        "rating_2" => "rating2",
        "rating_3" => "rating3",
		"docinfo_1" => "docinfo1",
        "docinfo_2" => "docinfo2",
        "docinfo_3" => "docinfo3",
        "docinfo_4" => "docinfo4",
		"docinfo_5" => "docinfo5",
        "docinfo_6" => "docinfo6",
        "info_seo" => "infoSeo",
        "html_title" => "htmlTitle",
        "html_meta_description" => "htmlMetaDescription",
        "html_meta_keywords" => "html_meta_keywords",
		"comment_intern" => "commentIntern",
        "created_at" => "createdAt",
        "created_by" => "createdBy",
		"activation_code" => "activationCode",
        "activated_at" => "activatedAt",
        "updated_at" => "updatedAt",
		"updated_by" => "updatedBy",
        "last_login_at" => "lastLoginAt",
        "has_contract" => "hasContract"
    );
	
	public function getMapArray(){
		return $this->mapArray;
	}
	
	public function addLocationDto($locationDto){
		$this->locationsDtos[]=$locationDto;
	}
	
	public function getLocationDtos(){
		return $this->locationsDtos;
	}
	
	public function addSamediDataDto($samediDataDto){
		$this->samediDatasDtos[]=$samediDataDto;
	}
	
	public function getSamediDatasDtos(){
		return $this->samediDatasDtos;
	}

	public function addDoctorsTreatmentTypeDto($doctorsTreatmentTypeDto) {
		$this->doctorsTreatmentTypeDto[] = $doctorsTreatmentTypeDto;
	}

	public function addDoctorsMedicalSpecialtyDto($doctorsMedicalSpecialtyDto) {
		$this->doctorsMedicalSpecialtyDtos[] = $doctorsMedicalSpecialtyDto;
	}

	public function getDoctorsMedicalSpecialtyDtos() {
		return $this->doctorsMedicalSpecialtyDtos;
	}

	public function getDoctorsTreatmentTypeDto() {
		return $this->doctorsTreatmentTypeDto;
	}	

	public function addSamediEventTypesCachesDto($samediEventTypesCachesDto) {
		$this->samediEventTypesCachesDtos[] = $samediEventTypesCachesDto;
	}

	public function getSamediEventTypesCachesDtos() {
		return $this->samediEventTypesCachesDtos;
	}

    public function getDoctorFullName() {
        $ret = array($this->getTitle(), $this->getFirstName(), $this->getLastName());
        return implode(' ', $ret);
    }

   public function getInfoText($dbFieldName) {
		$mapArray = $this->getMapArray();
		if(!isset($mapArray[$dbFieldName])){
			return null;
		}
		$dtoFieldName = $mapArray[$dbFieldName];
		$functionName = 'get'.ucfirst($dtoFieldName);
		$value = $this->$functionName();
        if (!isset($value)) {
            return '';
        }
        return do_shortcode(wpautop($value));
    }
    
	public function getDoctorDataOnlyClone() {
		$ret = new DoctorsDto();
		foreach ($this->mapArray as $value) {
			$ret->$value= $this->$value;
		}
		return $ret;
	}

    /**
     * returns this objects specific html title
     *
     * @return text
     */
    public function getHtmlTitle() {
        $htmlTitle = parent::getHtmlTitle();
        if (!empty($htmlTitle)) {
            return $htmlTitle;
        }
        return $this->getHtmlDefaultTitle();
    }

    /**
     * returns this objects default html title
     *
     * @return text
     */
    function getHtmlDefaultTitle() {
        $medicalSpecialityIdsText = $this->getMedicalSpecialityIdsText(' und ');
        return $this->getDoctorFullName() .
                ' - ' .
                $medicalSpecialityIdsText .
                ' - Termin buchen';
    }

    /**
     * returns this objects specific html meta description
     *
     * @return text
     */
    function getHtmlMetaDescription() {
        $htmlMetaDescription = parent::getHtmlMetaDescription();
        if (!empty($htmlMetaDescription)) {
            return $htmlMetaDescription;
        }
        return $this->getHtmlDefaultMetaDescription();
    }

    /**
     * returns this objects default html meta description
     *
     * @return text
     */
    function getHtmlDefaultMetaDescription() {

        $medicalSpecialityIdsText = $this->getMedicalSpecialityIdsText(' und ');
        return 'Bei ' . $medicalSpecialityIdsText . ' ' . $this->getName() .
                ' sofort einen Termin bekommen. Buchen Sie jetzt Ihren Termin online auf Arzttermine.de.';
    }

    /**
     * returns this objects default html meta description
     *
     * @return text
     */
    function getMedicalSpecialityIdsText($delimiter = ', ') {
        $medicalSpecialtiesManager = MedicalSpecialtiesManager::getInstance();
        return $medicalSpecialtiesManager->getMedicalSpecialityIdsText($this->getMedicalSpecialtyIds(), Application::getInstance()->getLanguageCode(), $delimiter);
    }

     /**
     * returns this objects default html meta description
     *
     * @return text
     */
    function getMedicalSpecialityIdsLis() {
        $medicalSpecialtiesManager = MedicalSpecialtiesManager::getInstance();
        return $medicalSpecialtiesManager->getMedicalSpecialityIdsLis($this->getMedicalSpecialtyIds(), Application::getInstance()->getLanguageCode());
    }
    
    /**
     * returns this objects specific html meta keywords
     *
     * @return text
     */
    function getHtmlMetaKeywords() {
        $htmlMetaKeywords = parent::getHtmlMetaKeywords();
        if (!empty($htmlMetaKeywords)) {
            return $htmlMetaKeywords;
        }
        return $this->getHtmlDefaultMetaKeywords();
    }

    /**
     * returns this objects default html meta keywords
     *
     * @return text
     */
    function getHtmlDefaultMetaKeywords() {
        $medicalSpecialityIdsText = $this->getMedicalSpecialityIdsText(', ');
        return $this->getDoctorFullName() . ', ' . $medicalSpecialityIdsText . ', Termin buchen, Arzttermin';
    }

    /**
     * returns the rating html
     *
     * @param key
     * @param text
     * @param location_id
     * 
     * @return html
     */
    function getRatingHtml($key, $text, $location_id = '') {
        if ($location_id != '') {
            $location_id = '-l' . $location_id;
        } else {
            $location_id = '';
        }
        $_html = '<div class="rating"><div class="text">' . Application::getInstance()->_($text) . '</div><div class="stars">';
        for ($x = 0.5; $x <= 5; $x = $x + 0.5) {
            $fn = $this->mapArray[$key];
            if ($this->$fn >= $x) {
                $_checked = ' checked="checked"';
            } else {
                $_checked = '';
            }
            $_html .= '<input class="star {split:2}" type="radio" name="rating-u' . $this->getId() . $location_id . '-' . $key . '" value="' . $x . '" disabled="disabled"' . $_checked . ' />';
        }
        $_html .= '</div></div><div class="clear"></div>';
        return $_html;
    }

    /**
     *  Nebojsa: a function based on getRatingHtml(). Returning the only thing that appears to be the rating.
     *  Should return numerical value> Currently hardcoded just to have some value, otherwise is empty.
     */
    function getRating($key) {
        return 4; // $this->mapArray[$key];
    }

    /**
     * Get the first location in this doctors list
     * 
     * @return LocationsDto|null
     */
    public function getDefaultLocation()
    {
        if (!$this->locationsDtos) {
            $this->getLocationDtos();
        }
        
        return $this->locationsDtos ? $this->locationsDtos[0] : null;
    }

    /**
     * Returns the url with filter attrs
     *
     * @param array $filter_array 
     * @param bool $absolute_url
     * 
     * @return string The URL
     */
    public function getUrlWithFilter($filter_array, $absolute_url = false)
    {
        $_url = $this->getUrl($absolute_url);
        $_attrs = '';
        
        foreach ($filter_array as $filter_id => $filter_value) {
            switch ($filter_id) {
                case 'insurance_id':
                    if (isset($GLOBALS['CONFIG']['INSURANCE_SLUGS'][$filter_value])) {
                        $_attrs = '/' . $GLOBALS['CONFIG']['INSURANCE_SLUGS'][$filter_value];
                    }
                    break;
                default:
                    break;
            }
        }
        
        return Application::getInstance()->i18nTranslateUrl($_url . $_attrs);
    }

    public function getUrl($absolute_url = false) {
        if ($absolute_url) {
            $_host = $GLOBALS["CONFIG"]["URL_HTTP_LIVE"];
        } else {
            $_host = '';
        }
        if ($this->getGroupId() == GROUP_DOCTOR && $this->getSlug() != '') {
            return $_host . '/arzt/' . $this->getSlug();
        }
        return "";
    }

    public function getProfileAssetUrl($size = ASSET_GFX_SIZE_ORIGINAL, $absolute_path = false) {
		if (is_numeric($this->getProfileAssetId()) && $this->getProfileAssetId() > 0 && $this->getProfileAssetFilename() != '') {
			$url = self::getUrlByIdFilenameOwner($this->getProfileAssetId(), $this->getProfileAssetFilename(), ASSET_OWNERTYPE_USER, $size, $absolute_path);
			if ($url) {
				return $url;
			}
		}
		return getStaticUrl($GLOBALS['CONFIG']['ASSET_GFX_TYPES'][ASSET_OWNERTYPE_USER]['SIZES'][$size]['default'], $absolute_path);
	}

	static function getUrlByIdFilenameOwner($id, $filename, $ownertype, $size = ASSET_GFX_SIZE_ORIGINAL, $add_host = false) {
		if (!is_numeric($id) || $id == 0 || $filename == '')
			return '';

		$host = '';
		if ($add_host) {
			$host = $GLOBALS['CONFIG']['URL_HTTP_LIVE'];
		}
		$pathinfo = pathinfo($filename);
		$filename = $pathinfo["filename"].$GLOBALS["CONFIG"]["ASSET_GFX_TYPES"][$ownertype]["SIZES"][$size]["postfix"].'.'.$pathinfo["extension"];
		$path = self::calcPath($id);
		$url = $GLOBALS["CONFIG"]["ASSET_URL"].$path.'/'.$filename;

		return $host.$url;
	}
	
	static function calcPath($id){
		$id = sprintf("%09d",$id);
		return substr($id,0,3).'/'.substr($id,3,3).'/'.substr($id,6,3);
	}

	/**
	 * Shows $num thumbs and creates the fancybox items
	 *
	 * @param 
	 * @return array assets
	*/
	function getAssetThumbsHtml($num, $show_profile_asset=false) {		
		$u = new User($this->getId());
		return $u->getAssetThumbsHtml($num, $show_profile_asset);
	}

}