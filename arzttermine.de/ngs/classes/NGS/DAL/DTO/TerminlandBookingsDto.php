<?php

namespace NGS\DAL\DTO;

use NGS\Framework\DAL\DTO\AbstractDto;

/**
 * AdminDto class is extended class from AbstractDto.
 *
 * @author	Vahagn Sookiasian
 */
class TerminlandBookingsDto extends AbstractDto {

	// Map of DB value to Field value
	protected $mapArray = array("id" => "id", "status" => "status", "booking_id" => "bookingId", "function_id" => "functionId", "integration_id" =>"integrationId", 
															"log_text" => "logText","created_at"=>"createdAt", "created_by" => "createdBy", 
															"updated_at" => "updatedAt", "updated_by" => "updatedBy","terminland_id"=>"terminlandId","terminplanId"=>"terminplanId",
															"termin_nr"=>"terminNr","terminCode");
	// constructs class instance
	public function __construct() {
	}

	// returns map array
	public function getMapArray() {
		return $this->mapArray;
	}

}