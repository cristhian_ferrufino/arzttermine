<?php

namespace NGS\DAL\DTO;

use NGS\Framework\DAL\DTO\AbstractDto;

class TempUsersDataDto extends AbstractDto {

	protected $mapArray = array("id" => "id",
                                    "gender" => "gender",
                                    "first_name"=>"firstName", 
                                    "last_name" => "lastName", 
                                    "title" => "title",
                                    "phone" => "phone",
                                    "phone_mobile" => "phoneMobile",
									"email" => "email", 
                                    "password_hash" => "passwordHash",
                                    "password_hash_salt" => "passwordHashSalt", 
                                    "activation_code" => "activationCode",
                                    "location_name" => "locationName",
                                    "location_street" => "locationStreet",
                                    "location_zip" => "locationZip",
                                    "location_city" => "locationCity",
                                    "medical_specialty_ids" => "medicalSpecialtyIds",
                                    "treatment_type_ids" => "treatmentTypeIds",
                                    "registration_reasons" => "registrationReasons",
                                    "registration_funnel" => "registrationFunnel",
                                    "registration_funnel_other" => "registrationFunnelOther",
                                    );
	
	public function getMapArray(){
		return $this->mapArray;
	}
        
        public function getFullName() {
            $ret =array($this->getTitle(), $this->getFirstName(), $this->getLastName());
            return implode(' ', $ret);
        }
}
