<?php

namespace NGS\DAL\DTO;

use Arzttermine\Application\Application;
use Arzttermine\Location\Location;
use NGS\Framework\DAL\DTO\AbstractDto;
use NGS\Managers\MedicalSpecialtiesManager;

/**
 * @method int getId
 * @method string getSlug
 * @method int getStatus
 * @method int getIntegrationId
 * @method string getName
 * @method string getStreet
 * @method string getZip
 * @method string getCity
 * @method float getLat
 * @method float getLng
 * @method string getEmail
 * @method string getPhone
 * @method string getFax
 * @method string getFaxVisible
 * @method int getNewsletterSubscription
 * @method string getMemberIds
 * @method string getMedicalSpecialtyIds
 * @method string getPositionFactor
 * @method float getRatingAverage
 * @method float getRating1
 * @method float getRating2
 * @method float getRating3
 * @method string getInfo1
 * @method string getInfoSeo
 * @method int getPrimary_member_id
 * @method string getCommentIntern
 * @method int getProfileAssetId
 * @method string getCreatedAt
 * @method string getCreatedBy
 * @method string getUpdatedAt
 * @method string getUpdatedBy
 */
class LocationsDto extends AbstractDto {

    private $userDto=null;
    private $userDtos=null;
    private $distance=null;
    private $medicalSpecialtiesDtos=null;    
	// Map of DB value to Field value
	protected $mapArray = array(
		"id" => "id",
		"slug" => "slug",
		"status" => "status",
		"integration_id" => "integrationId",
		"name" => "name",
		"street" => "street",
		"zip" => "zip",
		"city" => "city",
		"lat" => "lat",
		"lng" => "lng",
		"www" => "www",
		"email" => "email", 
		"phone" => "phone",
		"phone_visible" => "phoneVisible",
		"fax" => "fax",
		"fax_visible" => "faxVisible",
		"newsletter_subscription" => "newsletterSubscription",
		"member_ids" => "memberIds", 
		"medical_specialty_ids" => "medicalSpecialtyIds",
		"position_factor" => "positionFactor",
		"rating_average" => "ratingAverage",
		"rating_1" => "rating1",
		"rating_2" => "rating2",
		"rating_3" => "rating3",
		"info_1" => "info1",
		"info_seo" => "infoSeo",
		"html_title" => "htmlTitle",
		"primary_member_id" => "primary_member_id",
		"html_meta_description" => "htmlMetaDescription",
		"html_meta_keywords" => "htmlMetaKeywords",
		"comment_intern" => "commentIntern",
		"profile_asset_id" => "profileAssetId",
		"created_at" => "createdAt",
		"created_by" => "createdBy",
		"updated_at" => "updatedAt",
		"updated_by" => "updatedBy"
	);
	
	// constructs class instance
	public function __construct() {
	}

	// returns map array
	public function getMapArray() {
		return $this->mapArray;
	}

    public function setUserDto($userDto) {
        $this->userDto = $userDto;
    }

    /**
     * @return UsersDto
     */
    public function getUserDto() {
        return $this->userDto;
    }
	
	public function addUserDto($userDto) {
        $this->userDtos[] = $userDto;
    }
	
	public function getUserDtos(){
		return $this->userDtos;
	}

	public function setDistance($distance){
        $this->distance=$distance;
    }

    public function getDistance() {
        return $this->distance;
    }

    public function getLatitude() {
        return $this->getLat();
    }
    
    public function getLongitude() {
        return $this->getLng();
    }
    
    public function addMedicalSpecialtyDto($medicalSpecialtyDto){
        $this->medicalSpecialtiesDtos[]=$medicalSpecialtyDto;
    }
    
    public function getCityAndZip() {
        return $this->getZip().' '.$this->getCity();
    }
    
    public function getMedicalSpecialtiesDtos(){
        return $this->medicalSpecialtiesDtos;
    }

    public function getUrl($absolute_url = false) {
        if ($absolute_url) {
            $_host = $GLOBALS["CONFIG"]["URL_HTTP_LIVE"];
        } else {
            $_host = '';
        }
        return $_host . '/praxis/' . $this->getSlug();
    }

    /**
     * Returns the url with filter attrs
     *
     * @param array array filter
     * @param bool $absolute_url
     * 
     * @return string URL
     */
    public function getUrlWithFilter($filter_array, $absolute_url = false)
    {
        $_url = $this->getUrl($absolute_url);
        $_attrs = '';
        foreach ($filter_array as $filter_id => $filter_value) {
            switch ($filter_id) {
                case 'insurance_id':
                    if (isset($GLOBALS['CONFIG']['INSURANCE_SLUGS'][$filter_value])) {
                        $_attrs = '/' . $GLOBALS['CONFIG']['INSURANCE_SLUGS'][$filter_value];
                    }
                    break;
            }
        }
        return Application::getInstance()->i18nTranslateUrl($_url . $_attrs);
    }

    /**
     * Returns the address
     *
     * @param string $delimiter
     * 
     * @return string
     */
    public function getAddress($delimiter = '<br />') {
        return $this->getStreet() . $delimiter . $this->getCityZip();
    }

    /**
     * Returns the city and zip
     *
     * @param
     * @return string
     */
    public function getCityZip() {
        return $this->getZip() . ' ' . $this->getCity();
    }

    /**
     * Returns the visible phone number
     *
     * @param
     * @return string
     */
    public function getPhoneVisible() {
        $phoneVisible = parent::getPhoneVisible();
        if (!empty($phoneVisible)) {
            return $phoneVisible;
        } else {
            return $GLOBALS['CONFIG']['PHONE_SUPPORT'];
        }
    }

    public function getProfileAssetUrl($size = ASSET_GFX_SIZE_ORIGINAL, $absolute_path = false) {
        $l = new Location($this->getId());
        return $l->getProfileAssetUrl($size, $absolute_path);
    }

    /**
     * returns this objects specific html title
     *
     * @return string
     */
    public function getHtmlTitle() {
        $htmlTitle = parent::getHtmlTitle();
        if (!empty($htmlTitle)) {
            return $htmlTitle;
        }
        return $this->getHtmlDefaultTitle();
    }

    /**
     * returns this objects default html title
     *
     * @return string
     */
    public function getHtmlDefaultTitle() {
        return
                $this->getName() .
                ' - ' .
                $this->getMedicalSpecialityIdsText(' und ') .
                ' Praxis ' .
                $this->getCityZip() .
                ' - Termin buchen';
    }

    /**
     * returns this objects default html meta description
     *
     * @param string $delimiter
     * @return string
     */
    public function getMedicalSpecialityIdsText($delimiter = ', ') {
        $medicalSpecialtiesManager = MedicalSpecialtiesManager::getInstance();
        return $medicalSpecialtiesManager->getMedicalSpecialityIdsText($this->getMedicalSpecialtyIds(), Application::getInstance()->getLanguageCode(), $delimiter);
    }

    /**
     * returns this objects default html meta description
     *
     * @return string
     */
    public function getMedicalSpecialityIdsLis() {
        $medicalSpecialtiesManager = MedicalSpecialtiesManager::getInstance();
        return $medicalSpecialtiesManager->getMedicalSpecialityIdsLis($this->getMedicalSpecialtyIds(), Application::getInstance()->getLanguageCode());
    }

    /**
     * returns this objects specific html meta description
     *
     * @return string
     */
    public function getHtmlMetaDescription() {
        $htmlMetaDescription = parent::getHtmlMetaDescription();
        if (!empty($htmlMetaDescription)) {
            return $htmlMetaDescription;
        }
        return $this->getHtmlDefaultMetaDescription();
    }

    /**
     * returns this objects default html meta description
     *
     * @return string
     */
    public function getHtmlDefaultMetaDescription() {
        return 'In der ' . $this->getName() . ' in ' . $this->getCityZip() .
                ' sofort einen Termin bekommen. Buchen Sie jetzt Ihren Termin online auf Arzttermine.de.';
    }

    /**
     * returns this objects specific html meta keywords
     *
     * @return string
     */
    function getHtmlMetaKeywords() {
        $htmlMetaKeywords = parent::getHtmlMetaKeywords();
        if (!empty($htmlMetaKeywords)) {
            return $htmlMetaKeywords;
        }
        return $this->getHtmlDefaultMetaKeywords();
    }

    /**
     * returns this objects default html meta keywords
     *
     * @return string
     */
    public function getHtmlDefaultMetaKeywords() {
        return $this->getName() . ', ' . $this->getMedicalSpecialityIdsText() . ', ' . $this->getCityZip() .
                ', Termin buchen, Arzttermin';
    }

    /**
     * @return string
     */
    public function getWww()
    {
        if (!empty($this->www)) {
            if (substr($this->www, 0, 4) !== 'http') {
                return 'http://' . $this->www;  
            }
            
            return $this->www;
        }
        
        return '';
    }

    /**
     * returns the rating html
     *
     * @param int $id
     * @param string $text
     * @param string $user_id
     *                
     * @return string
     */
    public function getRatingHtml($id, $text, $user_id = '') {
        if ($user_id != '')
            $user_id = '-l' . $user_id;
        else
            $user_id = '';
        $_html = '<div class="rating"><div class="text">' . Application::getInstance()->_($text) . '</div><div class="stars">';
        for ($x = 0.5; $x <= 5; $x = $x + 0.5) {
            $fn = $this->mapArray[$id];
            if ($this->$fn >= $x) {
                $_checked = ' checked="checked"';
            } else {
                $_checked = '';
            }
            $_html .= '<input class="star {split:2}" type="radio" name="rating' . $user_id . '-l' . $this->getId() . '-' . $id . '" value="' . $x . '" disabled="disabled"' . $_checked . ' />';
        }
        $_html .= '</div></div><div class="clear"></div>';
        return $_html;
    }

    public function getInfoText($dbFieldName) {
		$mapArray = $this->getMapArray();
		if(!isset($mapArray[$dbFieldName])){
			return null;
		}
		$dtoFieldName = $mapArray[$dbFieldName];
		$functionName = 'get'.ucfirst($dtoFieldName);
		$value = $this->$functionName();
        if (!isset($value)) {
            return '';
        }
        return nl2br(do_shortcode(wpautop($value)));
    }

    
    /**
	 * Shows $num thumbs and creates the fancybox items
	 *
	 * @param int $num
     * @param bool $show_profile_asset
     * 
	 * @return array assets
	*/
	public function getAssetThumbsHtml($num, $show_profile_asset=false) {
        $l = new Location($this->getId());
		return $l->getAssetThumbsHtml($num, $show_profile_asset);
	}
}
