<?php

namespace NGS\DAL\DTO;

use NGS\Framework\DAL\DTO\AbstractDto;

/**
 * AdminDto class is extended class from AbstractDto.
 *
 * @author	Vahagn Sookiasian
 */
class GroupAccessesDto extends AbstractDto {

	// Map of DB value to Field value
	protected $mapArray = array("group_id" => "groupId", "access_id" => "accessId");

	// returns map array
	public function getMapArray() {
		return $this->mapArray;
	}

}