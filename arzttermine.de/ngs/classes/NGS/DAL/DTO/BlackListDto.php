<?php

namespace NGS\DAL\DTO;

use NGS\Framework\DAL\DTO\AbstractDto;

/**
 * AdminDto class is extended class from AbstractDto.
 *
 * @author	Vahagn Sookiasian
 */
class BlackListDto extends AbstractDto {

	protected $mapArray = array("id" => "id",
		"emails" => "emails",
		"phone_numbers" => "phoneNumbers",
		"ips" => "ips",
		"message_de"=>"messageDe"
	);

	// constructs class instance
	public function __construct() {
		
	}

	// returns map array
	public function getMapArray() {
		return $this->mapArray;
	}

}

