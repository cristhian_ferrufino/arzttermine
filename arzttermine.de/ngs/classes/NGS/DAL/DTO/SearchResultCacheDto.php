<?php

namespace NGS\DAL\DTO;

use NGS\Framework\DAL\DTO\AbstractDto;

/**
 * Class SearchResultCacheDto
 *
 * @method int getId
 * @method int setId
 * @method string getDate
 * @method string setDate
 * @method string getInsuranceIds
 * @method string setInsuranceIds
 * @method string getTreatmenttypeIds
 * @method string setTreatmenttypeIds
 * @method int getLocationId
 * @method int setLocationId
 * @method int getUserId
 * @method int setUserId
 * @method string getAppointment
 * @method string setAppointment
 * @method string getLastUpdatedTime
 * @method string setLastUpdatedTime
 */
class SearchResultCacheDto extends AbstractDto {

    protected $mapArray
        = array(
            "id"                => "id",
            "date"              => "date",
            "insurance_ids"     => "insuranceIds",
            "treatmenttype_ids" => "treatmenttypeIds",
            "user_id"           => "userId",
            "location_id"       => "locationId",
            "appointment"       => "appointment",
            "last_updated_time" => "lastUpdatedTime"
        );

    public function __construct()
    {
    }

    public function getMapArray()
    {
        return $this->mapArray;
    }
}