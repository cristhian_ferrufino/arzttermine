<?php

namespace NGS\DAL\DTO;

use NGS\Framework\DAL\DTO\AbstractDto;

/**
 * AdminDto class is extended class from AbstractDto.
 *
 * @author  Vahagn Kirakosyan
 */
class Integration1DatasDto extends AbstractDto {

    // Map of DB value to Field value
    protected $mapArray = array(
                            "id" => "id", 
                            "user_id" => "userId",
                            "location_id" => "locationId",
                            "weekday" => "weekday",
                            "start_at" => "startAt",
                            "end_at" => "endAt",
                            "block_in_minutes" => "blockInMinutes",
                            "created_at" => "createdAt",    
                            "created_by" => "createdBy",
                            "updated_at" => "updatedAt",    
                            "updated_by" => "updatedBy",
                            );

    // returns map array
    public function getMapArray() {
        return $this->mapArray;
    }

}
