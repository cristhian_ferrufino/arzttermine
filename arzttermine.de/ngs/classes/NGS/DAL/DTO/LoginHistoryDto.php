<?php

namespace NGS\DAL\DTO;

use NGS\Framework\DAL\DTO\AbstractDto;

class LoginHistoryDto extends AbstractDto {

	// Map of DB value to Field value
	protected $mapArray = array("id" => "id", 
								"user_id"=>"userId",
								"user_type"=>"userType",
								"ip"=>"ip",
								"operating_system"=>"operatingSystem",
								"browser"=>"browser",
								"browser_version"=>"browserVersion",
								"date"=>"date"
								);
								
	public function __construct() {
	}

	// returns map array
	public function getMapArray() {
		return $this->mapArray;
	}

}
