<?php

namespace NGS\DAL\DTO;

use NGS\Framework\DAL\DTO\AbstractDto;

/**
 * AdminDto class is extended class from AbstractDto.
 *
 * @author	Vahagn Sookiasian
 */
class GroupsDto extends AbstractDto {

	// Map of DB value to Field value
	protected $mapArray = array("id" => "id", "name" => "name","created_at" => "createdAt","created_by" => "createdBy",
															"updated_at" => "updatedAt",	"updated_by" => "updatedBy",
															
															//to join with users table
															"user_first_name"=>"userFirstName","user_last_name"=>"userLastName"
															);

	// returns map array
	public function getMapArray() {
		return $this->mapArray;
	}

}