<?php

namespace NGS\DAL\DTO;

use NGS\Framework\DAL\DTO\AbstractDto;

/**
 * AdminDto class is extended class from AbstractDto.
 *
 * @author	Vahagn Sookiasian
 */
class DoctorsTreatmentTypesDto extends AbstractDto {

    // Map of DB value to Field value
    protected $mapArray = array("id" => "id", "doctor_id" => "doctorId",
        "medical_speciality_id" => "medicalSpecialityId", "treatment_type_id" => "treatmentTypeId");

    // constructs class instance
    public function __construct() {
        
    }

    // returns map array
    public function getMapArray() {
        return $this->mapArray;
    }
}