<?php

namespace NGS\DAL\DTO;

use NGS\Framework\DAL\DTO\AbstractDto;

/**
 * AdminDto class is extended class from AbstractDto.
 *
 * @author	Vahagn Sookiasian
 */
class SamediCachesDto extends AbstractDto {
	private $samediTreatmentTypeDtos = array();
	// Map of DB value to Field value
	protected $mapArray = array("id" => "id", "user_id" => "userId", "date" => "date", "appointment_datetime" => "appointmentDatetime",
		"metadata" => "metadata",
		"treatmenttype_ids" => "treatmenttypeIds",
		"created_date" => "createdDate");

	// constructs class instance
	public function __construct() {
		
	}

	// returns map array
	public function getMapArray() {
		return $this->mapArray;
	}
	
	public function getSamediTreatmentTypeDtos() {
		return $this->samediTreatmentTypeDtos;
	}
					
	public function addSamediTreatmentTypeDtos($sTtDto) {
		$this->samediTreatmentTypeDtos[] = $sTtDto;
	}

}