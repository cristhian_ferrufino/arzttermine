<?php

namespace NGS\DAL\DTO;

use NGS\Framework\DAL\DTO\AbstractDto;

/**
 * Class UsersAaDto
 *
 * @method int getId
 * @method int getUserId
 * @method int getLocationId
 * @method string getDate
 * @method getStartAt
 * @method getEndAt
 * @method int getBlockInMinutes
 * @method string getInsuranceTypes
 * @method string getInsuranceTypeIds
 * @method string getCreatedAt
 * @method string getCreatedBy
 * @method string getUpdatedAt
 * @method string getUpdatedBy
 */
class UsersAaDto extends AbstractDto {

    /**
     * @var array
     */
    private $treatmentTypeDtos = array();

    /**
     * @var array
     */
    protected $mapArray = array(
        "id" => "id",
        "user_id" => "userId",
        "location_id" => "locationId",
        "date" => "date",
        "start_at" => "startAt",
        "end_at" => "endAt",
        "block_in_minutes" => "blockInMinutes",
        "insurance_types" => "insuranceTypes",
        "treatment_type_ids" => "treatmentTypeIds",
        "created_at" => "createdAt",
        "created_by" => "createdBy",
        "updated_at" => "updatedAt",
        "updated_by" => "updatedBy"
    );

    /**
     * @var array
     */
    protected $dynamicMapArray = array("treatmenttype_names" => "treatmenttypeNames");

    /**
     * @return array
     */
    public function getMapArray()
    {
        return $this->mapArray;
    }

    /**
     * @return array
     */
    public function getDynamicMapArray()
    {
        return $this->dynamicMapArray;
    }

    /**
     * @return array
     */
    public function getTreatmentTypeDtos()
    {
        return $this->treatmentTypeDtos;
    }

    /**
     * @param $treatmentTypeDto
     */
    public function addTreatmentTypeDtos($treatmentTypeDto)
    {
        $this->treatmentTypeDtos[] = $treatmentTypeDto;
    }

}