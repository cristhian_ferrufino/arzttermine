<?php

namespace NGS\DAL\DTO;

use NGS\Framework\DAL\DTO\AbstractDto;

/**
 * AdminDto class is extended class from AbstractDto.
 *
 * @author	Vahagn Sookiasian
 */
class SamediPatientsConnectionsDto extends AbstractDto {

	// Map of DB value to Field value
	protected $mapArray = array("patient_id" => "patientId", "samedi_patient_id" => "samediPatientId");
	// constructs class instance
	public function __construct() {
	}

	// returns map array
	public function getMapArray() {
		return $this->mapArray;
	}

}