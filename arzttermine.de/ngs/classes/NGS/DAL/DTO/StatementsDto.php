<?php

namespace NGS\DAL\DTO;

use NGS\Framework\DAL\DTO\AbstractDto;

/**
 * Class StatementsDto
 * 
 * @method int getId
 * @method int getStatus
 * @method int getLocationId
 * @method string getPeriod
 * @method int setStatus
 * @method int setLocationId
 * @method string setPeriod
 */
class StatementsDto extends AbstractDto {

    // Map of DB value to Field value
    protected $mapArray = array(
		"id" => "id",
		"status" => "status",
		"location_id" => "locationId",
		"period" => "period"
    );
    // constructs class instance
    public function __construct() {
    }

    // returns map array
    public function getMapArray() {
        return $this->mapArray;
    }

}
