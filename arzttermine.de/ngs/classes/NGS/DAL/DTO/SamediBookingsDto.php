<?php

namespace NGS\DAL\DTO;

use NGS\Framework\DAL\DTO\AbstractDto;

/**
 * @method int getId
 * @method int getStatus
 * @method int getPracticeId
 * @method int getCategoryId
 * @method int getEventTypeId
 * @method string getCreatedAt
 * @method int getBookingId
 * @method string getResultJson
 * @method string getCancelableUntil
 * @method string getCreatedBy
 * @method string getUpdatedAt
 * @method string getUpdatedBy
 */
class SamediBookingsDto extends AbstractDto {

    protected $mapArray
        = array(
            "id"               => "id",
            "status"           => "status",
            "practice_id"      => "practiceId",
            "category_id"      => "categoryId",
            "event_type_id"    => "eventTypeId",
            "created_at"       => "createdAt",
            "booking_id"       => "bookingId",
            "result_json"      => "resultJson",
            "cancelable_until" => "cancelableUntil",
            "created_by"       => "createdBy",
            "updated_at"       => "updatedAt",
            "updated_by"       => "updatedBy"
        );

    private $samediTreatmentTypeDto;

    public function getMapArray()
    {
        return $this->mapArray;
    }

    public function setSamediTreatmentTypeDto($samediTreatmentTypeDto)
    {
        $this->samediTreatmentTypeDto = $samediTreatmentTypeDto;
    }

    public function getSamediTreatmentTypeDto()
    {
        return $this->samediTreatmentTypeDto;
    }
}