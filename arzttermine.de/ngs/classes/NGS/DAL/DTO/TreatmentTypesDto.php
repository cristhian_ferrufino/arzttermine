<?php

namespace NGS\DAL\DTO;

use NGS\Framework\DAL\DTO\AbstractDto;

/**
 * Class TreatmentTypesDto
 *
 * @method int getId
 * @method string getSlug
 * @method int getStatus
 * @method int getMedicalSpecialtyId
 * @method string getNameDe
 * @method string getNameEn
 * @method string getCreatedAt
 * @method string getCreatedBy
 * @method string getUpdatedAt
 * @method string getUpdatedBy
 */
class TreatmentTypesDto extends AbstractDto {

    /**
     * @var array
     */
    protected $mapArray = array(
        "id" => "id",
        "slug" => "slug",
        "status" => "status",
        "medical_specialty_id" => "medicalSpecialtyId",
        "name_de" => "nameDe",
        "name_en" => "nameEn",
        "created_at" => "createdAt",
        "created_by" => "createdBy",
        "updated_at" => "updatedAt",
        "updated_by" => "updatedBy"
    );

    /**
     * @return array
     */
    public function getMapArray()
    {
        return $this->mapArray;
    }

}