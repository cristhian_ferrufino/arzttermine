<?php

namespace NGS\DAL\DTO;

use NGS\Framework\DAL\DTO\AbstractDto;

/**
 * Class BookingsDto
 *
 * @method int getId
 * @method int getStatus
 * @method int getBackendMarkerClass
 * @method int getSourcePlatform
 * @method int getIntegrationStatus
 * @method int getIntegrationLog
 * @method int getSlug
 * @method int getFormOnce
 * @method int getUserId
 * @method int getLocationId
 * @method int getAppointmentStartAt
 * @method int getGender
 * @method int getFirstName
 * @method int getLastName
 * @method int getEmail
 * @method int getPhone
 * @method int getInsuranceId
 * @method int getReturningVisitor
 * @method int getTreatmentTypeId
 * @method int getNewsletterSubscription
 * @method int getCommentIntern
 * @method int getCreatedAt
 * @method int getCreatedBy
 * @method int getUpdatedAt
 * @method int getUpdatedBy
 * @method int getBookedLanguage
 * @method int getConfirmed
 * @method int getConfirmedAt
 */
class BookingsDto extends AbstractDto {

	private $_location = null;
	private $_user = null;
	private $_integrationBookingDto = null;
	private $_treatmentType = null;
	private $_medicalSpecialty = null;
	private $_samediBookings = null;
    private $_createdByDto=null;
    private $_mailingsCount=0;
	private $_referral=null;

    // Map of DB value to Field value
    protected $mapArray = array(
        "id" => "id",
        "status" => "status",
        "backend_marker_class" => "backendMarkerClass",
        "source_platform" => "sourcePlatform",
        "integration_status" => "integrationStatus",
        "integration_log" => "integrationLog",
        "slug" => "slug",
        "form_once" => "formOnce",
        "user_id" => "userId",
        "location_id" => "locationId",
        "appointment_start_at" => "appointmentStartAt",
        "gender" => "gender",
        "first_name" => "firstName",
        "last_name" => "lastName",
        "email" => "email",
        "phone" => "phone",
        "insurance_id" => "insuranceId",
        "returning_visitor" => "returningVisitor",
        "treatmenttype_id" => "treatmenttypeId",
        "newsletter_subscription" => "newsletterSubscription",
        "comment_intern" => "commentIntern",
        "created_at" => "createdAt",
        "created_by" => "createdBy",
        "updated_at" => "updatedAt",
        "updated_by" => "updatedBy",
        "booked_language" => "bookedLanguage",
        "confirmed" => "confirmed",
        "updated_confirmed_at" => "confirmed_at"
    );
								
	// constructs class instance
	public function __construct() {
	}

	// returns map array
	public function getMapArray() {
		return $this->mapArray;
	}
	
	public function addLocation($locationDto){
		$this->_location=$locationDto;
	}

    /**
     * @return LocationsDto
     */
    public function getLocation(){
		return $this->_location;
	}

	public function getSamediBookings() {
		return $this->_samediBookings;
	}

	public function addUser($userDto) {
		$this->_user = $userDto;
	}

    /**
     * @return UsersDto
     */
    public function getUser(){
		return $this->_user;
	}
	
	public function addTreatmentType($treatmentTypeDto){
		$this->_treatmentType=$treatmentTypeDto;
	}

	public function addSamediBookings($samediBookingsDto) {
		$this->_samediBookings = $samediBookingsDto;
	}

	public function getTreatmentType() {
		return $this->_treatmentType;
	}
	
	public function addCreatedByDto($userDto) {
        $this->_createdByDto = $userDto;
    }
    
    public function getCreatedByDto(){
        return $this->_createdByDto;
    }
    
    public function addMailingsCount($count) {
        $this->_mailingsCount = $count;
    }
    
    public function getMailingsCount(){
        return $this->_mailingsCount;
    }
	
	public function getIntegrationBookingDto() {
		return $this->_integrationBookingDto;
	}

	public function setIntegrationBookingDto($integrationBookingDto) {
		$this->_integrationBookingDto = $integrationBookingDto;
	}
	public function getReferral() {
		return $this->_referral;
	}

	public function setReferral($referral) {
		$this->_referral = $referral;
	}

	public function getMedicalSpecialty() {
		return $this->_medicalSpecialty;
	}

	public function setMedicalSpecialty($medicalSpecialty) {
		$this->_medicalSpecialty = $medicalSpecialty;
	}

}
