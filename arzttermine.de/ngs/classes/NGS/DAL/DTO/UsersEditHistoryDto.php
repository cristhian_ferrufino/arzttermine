<?php

namespace NGS\DAL\DTO;

use NGS\Framework\DAL\DTO\AbstractDto;

class UsersEditHistoryDto extends AbstractDto {

	protected $mapArray = array("id" => "id","user_id"=>"userId", "slug" => "slug", "status" => "status", 
                                    "group_id" => "groupId", "email" => "email", "gender" => "gender",
                                    "title" => "title", "first_name" => "firstName", "last_name" => "lastName", 
                                    "phone" => "phone", "medical_specialty_ids" => "medicalSpecialtyIds",
                                    "password_hash" => "passwordHash", "password_hash_salt" => "passwordHashSalt", 
                                    "fixed_week"=>"fixedWeek","profile_asset_id" => "profileAssetId",
                                    "reset_password_code" => "resetPasswordCode", "newsletter_subscription" => "newsletterSubscription",
                                    "rating_average" => "ratingAverage", "rating_1" => "rating1", "rating_2" => "rating2", "rating_3" => "rating3",
                                    "docinfo_1" => "docinfo1", "docinfo_2" => "docinfo2", "docinfo_3" => "docinfo3", "docinfo_4" => "docinfo4",
                                    "docinfo_5" => "docinfo5", "docinfo_6" => "docinfo6", "info_seo" => "infoSeo", "html_title" => "htmlTitle",
                                    "html_meta_description" => "htmlMetaDescription", "html_meta_keywords" => "html_meta_keywords",
                                    "comment_intern" => "commentIntern", "created_at" => "createdAt", "created_by" => "createdBy",
                                    "activation_code" => "activationCode", "activated_at" => "activatedAt", "updated_at" => "updatedAt",
                                    "updated_by" => "updatedBy", "last_login_at" => "lastLoginAt");

	public function __construct() {

	}
	
	public function getMapArray(){
		return $this->mapArray;
	}
}