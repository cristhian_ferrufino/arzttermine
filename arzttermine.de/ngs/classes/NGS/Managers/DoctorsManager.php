<?php

namespace NGS\Managers;

use Arzttermine\Booking\Appointment;
use Arzttermine\Booking\Booking;
use Arzttermine\Calendar\Calendar;
use Arzttermine\Core\Configuration;
use Arzttermine\Integration\Integration\Samedi\Connection\SamediAdapter;
use Arzttermine\Integration\Integration\Samedi\Types\CategoryEventType;
use Arzttermine\Integration\Integration\Samedi\Types\SamediPatient;
use Arzttermine\Integration\Integration\Samedi\Types\SBooking;
use Arzttermine\Integration\Integration\Samedi\Types\SNetworkContact;
use Arzttermine\Integration\Integration\Samedi\Types\SPatient;
use Arzttermine\Integration\Integration\Samedi\Types\SPractice;
use Arzttermine\Integration\Integration\Samedi\Types\SPracticeCategory;
use Arzttermine\Integration\Integration\Samedi\Types\STime;

use NGS\DAL\DTO\SamediTreatmentTypesDto;
use NGS\DAL\Mappers\DoctorsMapper;
use NGS\Framework\AbstractManager;
use NGS\Util\UtilFunctions;

class DoctorsManager extends AbstractManager {

    /**
     * @var singleton instance of class
     */
    private static $instance = null;

    private $usersManager;

    private $samediCachesTmpManager;

    private $samediEventTypesCachesManager;

    private $samediDatasManager;

    private $samediAdapter;

    private $doctorsMapper;

    /**
     * Initializes DB mappers
     *
     * @param object $config
     * @param object $args
     *
     * @return
     */
    function __construct($config = array(), $args = array())
    {
        $this->config = $config;
        $this->args = $args;
        $this->usersManager = UsersManager::getInstance($this->config, $this->args);
        $this->samediCachesTmpManager = SamediCachesTmpManager::getInstance($this->config, $this->args);
        $this->samediDatasManager = SamediDatasManager::getInstance($this->config, $this->args);
        $this->samediAdapter = SamediAdapter::getInstance();
        $this->samediEventTypesCachesManager = SamediEventTypesCachesManager::getInstance($this->config, $this->args);
        $this->doctorsMapper = DoctorsMapper::getInstance();
    }

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Checks if samedi doctor insurance type is corresponding to Arzttermine insurance.
     *
     * @param CategoryEventType $et
     * @param int $insuranceId 0,1,2
     *
     * @return boolean Returns TRUE if given event type insurance is corresponding to Arzttermine insurance type., otherwise FALSE
     */
    public static function isSamediEventTypeInsuranceMatchToSystemInsuranceId($subTtDto, $insuranceId)
    {
        $insuranceIds = $subTtDto->getInsuranceIds();
        $insuranceIdsArray = explode(',', $insuranceIds);

        return in_array($insuranceId, $insuranceIdsArray);
    }

    /**
     * Format the given treatmenttypes and return the formated String representing the treatmenttypes.
     *
     * @param type $eventTypes
     * @param string $returnFormat
     *
     * @return string
     */
    public static function getEventTypesNamesString($eventTypes)
    {
        if (!is_array($eventTypes)) {
            $eventTypes = json_decode($eventTypes);
        }
        if (!empty($eventTypes)) {
            $names = "";

            foreach ($eventTypes as $eventType) {
                $eventType = new CategoryEventType($eventType);
                $names[] = $eventType->getName();
            }
            $names = implode(', ', $names);

            return $names;
        }

        return null;
    }

    public static function getTreatmentTypeNamesString($ttDtos)
    {

        if (!empty($ttDtos)) {
            $names = "";

            foreach ($ttDtos as $ttDto) {
                $names[] = $ttDto->getName();
            }
            $names = implode('&#013;', $names);

            return $names;
        }

        return null;
    }

    public function insertDoctors($doctorsDtos, $replaceDoctors, $replaceLocations)
    {
        foreach ($doctorsDtos as $doc) {
            $this->insertDoctorFull($doc, $replaceDoctors, $replaceLocations);
        }
    }

    public function insertDoctorFull($doctorDto, $replaceDoctor, $replaceLocation)
    {
        return $this->doctorsMapper->insertDoctorFull($doctorDto, $replaceDoctor, $replaceLocation);
    }

    /**
     * @param int $userId
     * @param int $locationId
     * @param string $datetime
     *
     * @return array
     */
    public function getTreatmentTypesForGivenTime($userId, $locationId, $datetime)
    {
        $date = Calendar::getDate($datetime);
        $appointments = $this->getAvailableAppointments($date, $date, $userId, $locationId);
        $ap = $this->findAppointmentByDateTime($appointments, $datetime);

        if (!empty($ap)) {

            $ttDtos = $ap->getTreatmenttypeDtos();

            return array(
                "tt_names" => self::getSamediTreatmentTypesArztterminNamesArray($ttDtos),
                "tt_insurances" => $this->getEventTypesInsurancesArray($ttDtos)
            );
        }

        return array();
    }

    /**
     * Returns user available appointmetns for a given period and filtered by treatmenttype ids
     * (Note that treatmenttype_ids filtering is not currently working... Need to make relaeion between samedi and local treatmentypes ids)
     *
     * @param string $date_start
     * @param string $date_end
     * @param int $userId
     * @param int $locationId
     * @param int $insurance_id
     * @param string $treatmenttype_ids Treatmenttype ids joined by comma ex. "1,8,24"
     *
     * @return bool or array(date=>array(Appointment))
     */
    public function getAvailableAppointments($date_start, $date_end, $userId, $locationId, $insurance_id = null, $treatmenttype_ids = null)
    {
        // return appointments from cache
        $cachesDtos = $this->samediCachesTmpManager->getDoctorAvailableAppointmentsByDateRange($userId, $date_start, $date_end, $insurance_id, $treatmenttype_ids);

        return $this->convertSamediCacheDtosToAppointmentObjects($cachesDtos);
    }

    private function convertSamediCacheDtosToAppointmentObjects($caches)
    {
        $appointments = array();
        foreach ($caches as $cacheDto) {
            $appointments[$cacheDto->getDate()][] = $this->getDayAppointmentFromSamediCacheDto($cacheDto);
        }

        return $appointments;
    }

    private function getDayAppointmentFromSamediCacheDto($cacheDto)
    {
        $datetime = $cacheDto->getAppointmentDatetime();
        $metadata = $cacheDto->getMetadata();
        $samediTreatmenttypeDtos = $cacheDto->getSamediTreatmentTypeDtos();

        return new Appointment($datetime, $metadata, $samediTreatmenttypeDtos);
    }

    /**
     * @param $datetime this is timestamp for app start datetiem
     */
    private function findAppointmentByDateTime($appointments, $datetime)
    {
        foreach ($appointments as $ap_array) {
            foreach ($ap_array as $ap) {
                if ($ap->getDateTime() == $datetime) {
                    return $ap;
                }
            }
        }

        return null;
    }

    private static function getSamediTreatmentTypesArztterminNamesArray($ttDtos)
    {
        $ret = array();
        $arztterminTTIdsArray = array();
        foreach ($ttDtos as $ttDto) {
            $baseTreatmenttypeDto = $ttDto->getBaseTreatmenttypeDto();
            $insuranceIds = $ttDto->getInsuranceIds();
            $insuranceIdsArray = explode(',', $insuranceIds);
            $insNameArray = array();
            foreach ($insuranceIdsArray as $insId) {
                $insNameArray [] = $GLOBALS['CONFIG']['INSURANCES'][intval($insId)];
            }
            if (!in_array($baseTreatmenttypeDto->getId(), $arztterminTTIdsArray)) {
                $arztterminTTIdsArray[] = $baseTreatmenttypeDto->getId();
                $ret[$ttDto->getId()] = $baseTreatmenttypeDto ? $baseTreatmenttypeDto->getNameDe() : $ttDto->getName();
            }
        }

        return $ret;
    }

    /**
     * to show in select
     * array("event_type_id"=>"name", ...)
     */
    public static function getEventTypesInsurancesArray($ttDtos)
    {
        $ret = array();
        foreach ($ttDtos as $ttDto) {
            $ret[$ttDto->getId()] = $ttDto->getInsuranceIds();
        }

        return $ret;
    }

    /**
     * this function used to get find the user selected treatment type
     * from the given time appointment.
     *
     * @return CategoryEventType or null if there is no given treadment type in the appointment
     */
    public function findTreatmentTypeForGivenApptTime($userId, $locationId, $datetime, $integrationSpecificTtId)
    {
        $date = Calendar::getDate($datetime);
        $appointments = $this->getAvailableAppointments($date, $date, $userId, $locationId);

        /** @var Appointment $ap */
        $ap = $this->findAppointmentByDateTime($appointments, $datetime);
        if ($ap != null) {
            $ttDtos = $ap->getTreatmenttypeDtos();

            return $this->findTtById($ttDtos, $integrationSpecificTtId);
        }

        return null;
    }

    /**
     *  find the samedi_treatmenttype for the given list by given samedi_treatmenttype_id
     *
     * @param array $ttDtos
     * @param int $ttId
     *
     * @return SamediTreatmentTypesDto
     */
    private static function findTtById($ttDtos, $ttId)
    {
        foreach ($ttDtos as $ttDto) {
            if ($ttDto->getId() == $ttId) {
                return $ttDto;
            }
        }

        return null;
    }

    /**
     * Fetches appointments from Samedi and stores them in the samedi cache
     * (Note that treatmenttype_ids filtering is not currently working... Need to make relaeion between samedi and local treatmentypes ids)
     *
     * @param string $date_start
     * @param string $date_end
     * @param int $userId
     * @param int $locationId
     *
     * @return bool or array(date=>array(Appointment))
     */
    public function updateAvailableAppointments($date_start, $date_end, $userId, $locationId)
    {
        // update the appointments from samedi
        $data = $this->samediDatasManager->getByUserIdAndLocationId($userId, $locationId);
        if (!isset($data)) {
            return;
        }
        $categoryId = $data->getCategoryId();
        $practiceId = $data->getPracticeId();
        $eventTypes = $this->getPracticeCategoryEventTypes($practiceId, $categoryId);
        $appointments = $this->samediAdapter->getAvailableAppointments($date_start, $date_end, $categoryId, $practiceId, $eventTypes);
        $this->samediCachesTmpManager->saveDoctorAppointments($userId, $appointments);

        return;
    }

    /**
     * Returns the given doctor EventTypes in samedi eventType format
     *
     * @param type $practiceId
     * @param type $categoryId
     * @param type $cacheOnly
     *
     * @return boolean
     */
    public function getPracticeCategoryEventTypes($practiceId, $categoryId, $cacheOnly = false)
    {
        if (!$cacheOnly) {
            $eventTypes = $this->samediEventTypesCachesManager->getCategoryEventTypes($practiceId, $categoryId);

            return $eventTypes;
        }
        $eventTypes = $this->samediAdapter->getPracticeCategoryEventTypes($practiceId, $categoryId);

        if ($eventTypes != false) {
            $this->samediEventTypesCachesManager->setCatagoryEventTypes($practiceId, $categoryId, $eventTypes);

            return true;
        } else {
            return false;
        }
    }

    /**
     * Caches given samedi user event type in DB
     *
     * @param int $userId
     * @param int $locationId
     *
     * @return bool TRUE if scsseed to cache FALSE otherwise
     */
    public function cacheDoctorEventTypes($userId, $locationId)
    {
        $data = $this->samediDatasManager->getByUserIdAndLocationId($userId, $locationId);
        if (!isset($data)) {
            return false;
        }
        $categoryId = $data->getCategoryId();
        $practiceId = $data->getPracticeId();

        return $this->getPracticeCategoryEventTypes($practiceId, $categoryId, true);
    }

    /**
     * Returns the next available time
     *
     * @ param datetime start
     * @ param datetime end
     * @ param UserId
     * @ param LocationId
     * @ param insurance_id
     * @ param $treatmenttype_ids( optional)
     *
     * @ return Appointment || bool
     */
    public function getNextAvailableAppointment($datetime_start, $datetime_end, $userId, $locationId, $insurance_id, $treatmenttype_ids = null)
    {
        $appointments = $this->getAvailableAppointments($datetime_start, $datetime_end, $userId, $locationId, $insurance_id, $treatmenttype_ids);
        if (!empty($appointments)) {
            $ap = reset($appointments);

            return current($ap);
        } else {
            return null;
        }
    }

    /**
     * @param int $booking_id
     * @param int $userProfileId
     *
     * @return array|bool
     */
    public function bookAppointment($booking_id, $userProfileId)
    {

        $bookingsManager = BookingsManager::getInstance($this->config, $this->args);
        $bookingDto = $bookingsManager->getBookingById($booking_id);
        $eventTypeId = $bookingDto->getTreatmenttypeId();

        //changing booking's treatmenttype_id in arzttermine bookings table to arzttermin treatmenttype_id insted of samedi_treatmenttype_id
        $samediTreatmentTypesManager = SamediTreatmentTypesManager::getInstance($this->config, $this->args);
        $samediTreatmentDto = $samediTreatmentTypesManager->getById($eventTypeId);
        $bookingsManager->setBookingTreatmenttypeId($booking_id, $samediTreatmentDto->getParentId());

        // check for invalid status
        if ($bookingDto->getIntegrationStatus() == BOOKING_INTEGRATION_STATUS_BOOKED || $bookingDto->getIntegrationStatus() == BOOKING_INTEGRATION_STATUS_CANCELED) {
            if ($bookingDto->getIntegrationStatus() == BOOKING_INTEGRATION_STATUS_BOOKED) {
                return array(100, 'This booking status is already BOOKED');
            }
            if ($bookingDto->getIntegrationStatus() == BOOKING_INTEGRATION_STATUS_CANCELED) {
                return array(100, 'This booking status is CANCELED');
            }
        }
        $samediDataDto = $this->samediDatasManager->getByUserIdAndLocationId($bookingDto->getUserId(), $bookingDto->getLocationId());
        $samediBookingsManager = SamediBookingsManager::getInstance($this->config, $this->args);

        $practiceId = $samediDataDto->getPracticeId();
        $categoryId = $samediDataDto->getCategoryId();
        $samediBookingId = $samediBookingsManager->addBooking(
            \Arzttermine\Integration\Booking::STATUS_ERROR,
            $booking_id,
            $practiceId,
            $categoryId,
            $eventTypeId,
            $userProfileId
        );
        
        $bookingRes = $this->book($bookingDto, $samediDataDto, $eventTypeId);
        if (!is_array($bookingRes)) {
            if (!($bookingRes === true)) {
                $sbooking = new SBooking($bookingRes);
                $cancelableUntilDateTimeObj = date_create_from_format('Y-m-d?H:i:sP', $sbooking->getCancelableUntil());
                $cancelableUntilDateTime = null;
                if ($cancelableUntilDateTimeObj !== false) {
                    $cancelableUntilDateTime = date_format($cancelableUntilDateTimeObj, 'Y-m-d H:i:s');
                }
                $samediBookingsManager->setBookingCancelableUntil($samediBookingId, $cancelableUntilDateTime);
            }
            
            $samediBookingsManager->setBookingResultJson($samediBookingId, $bookingRes);
            $samediBookingsManager->setBookingStatus($samediBookingId, \Arzttermine\Integration\Booking::STATUS_OK);
        } else {
            $samediBookingsManager->setBookingResultJson($samediBookingId, $bookingRes[1]);
            $samediBookingsManager->setBookingStatus(
                $samediBookingId,
                \Arzttermine\Integration\Booking::STATUS_ERROR
            );
        }
        
        $bookingDateTime = $bookingDto->getAppointmentStartAt();
        $this->updateBookingDayAppointmentsFromOriginalTable($bookingDto->getUserId(), $bookingDto->getLocationId(), $bookingDateTime);
        $samediBookingDto = $samediBookingsManager->getById($samediBookingId);
        if ($samediBookingDto->getStatus() == \Arzttermine\Integration\Booking::STATUS_OK) {
            $bookingsManager->setIntegrationStatus($booking_id, BOOKING_INTEGRATION_STATUS_BOOKED);

            return true;
        } else {
            $bookingsManager->setIntegrationStatus($booking_id, BOOKING_INTEGRATION_STATUS_ERROR);

            return $bookingRes;
        }
    }

    /**
     * Book an appointment
     * Returns samedi booking object or false if booking failed
     *
     * @param Booking
     *
     * @return book object or array(errorCode, errorDescription)
     */
    private function book($bookingDto, $samediDataDto, $integrationSpecificTtId)
    {

        $bookingTime = $bookingDto->getAppointmentStartAt();
        list($stime, $integrationSpecificTtDto) = $this->getDoctorSTimeAndTtDto($samediDataDto, $bookingTime, $integrationSpecificTtId, $bookingDto->getInsuranceId());
        if (!isset($stime) || !isset($integrationSpecificTtDto)) {
            return array(100, "Der von Ihnen gewählte Termin ist leider nicht mehr verfügbar. Bitte buchen Sie einen anderen Termin.");
        }
        if (Configuration::get('samedi_real_booking_enable') != 1) {
            return true;
        }
        $userData = new SamediPatient($bookingDto);
        $patient = $this->samediAdapter->addPatient($userData);
        $p = new SPatient($patient);
        $booking = $this->samediAdapter->book($samediDataDto->getPracticeId(), $integrationSpecificTtId, $samediDataDto->getCategoryId(), $stime, $p);
        if (!$booking) {
            return array(intval($this->samediAdapter->getLastError()), $this->samediAdapter->getLastError());
        }

        return $booking;
    }

    /**
     * this function is used in bookAppointment  function to get the doctor stime and seventType to use them in booking function
     *
     * @param $dateObj should be php DateTime object
     */
    private function getDoctorSTimeAndTtDto($samediDataDto, $bookingTime, $integrationSpecificTtId, $insuranceId)
    {
        $date = Calendar::getDate($bookingTime);
        $dayAppointments = $this->getAvailableAppointments($date, $date, $samediDataDto->getUserId(), $samediDataDto->getLocationId(), $insuranceId);
        if (isset($dayAppointments) && $dayAppointments !== false) {
            reset($dayAppointments);
            $aps = current($dayAppointments);
            foreach ($aps as $ap) {
                if ($ap->getDateTime() == $bookingTime) {
                    $samediTime = json_decode($ap->getMetadata());
                    $ttDtos = $ap->getTreatmenttypeDtos();
                    foreach ($ttDtos as $ttDto) {
                        if ($ttDto->getId() == $integrationSpecificTtId) {
                            return array(new STime($samediTime), $ttDto);
                        }
                    }
                }
            }
        }

        return array(null, null);
    }

    /**
     * Update given user's remote available appointments for a booking day only
     *
     * @param int $doctorId
     * @param int $locationId
     * @param string $datetime
     */
    public function updateBookingDayAppointmentsFromOriginalTable($doctorId, $locationId, $datetime)
    {
        $configuration = Configuration::getInstance();
        $val = $configuration->optionGet(SamediAdapter::SAMEDI_CHANGED_DOCTORS_IDS_KEY);
        $configuration->optionSet(SamediAdapter::SAMEDI_CHANGED_DOCTORS_IDS_KEY, $val . $doctorId . ' ', true);

        $date = Calendar::getDate($datetime);
        $this->samediCachesTmpManager->deleteBookingDayAppoitnmentsFromOriginalTable($doctorId, $date);

        $data = $this->samediDatasManager->getByUserIdAndLocationId($doctorId, $locationId);
        if (!isset($data)) {
            return;
        }
        $categoryId = $data->getCategoryId();
        $practiceId = $data->getPracticeId();
        $eventTypes = $this->getPracticeCategoryEventTypes($practiceId, $categoryId);
        $appointments = $this->samediAdapter->getAvailableAppointments($date, $date, $categoryId, $practiceId, $eventTypes);
        $this->samediCachesTmpManager->saveDoctorAppointments($doctorId, $appointments, true);
    }

    public function getBookedAppointment($bookId)
    {
        return $this->samediAdapter->getBookedAppointment($bookId);
    }

    public function getAllPendingBookedAppointments($offset, $limit)
    {
        return $this->samediAdapter->getAllPendingBookedAppointments($offset, $limit);
    }

    /**
     * Returns the given doctor EventTypes in samedi eventType format
     *
     * @param bookingId
     *
     * @return boolean (true = success, everything else: error message)
     */
    public function cancelBookedAppointment($bookingId)
    {
        $bookingManager = BookingsManager::getInstance($this->config, $this->args);
        $bookingDto = $bookingManager->getBookingByIdFull($bookingId);
        $locationDto = $bookingDto->getLocation();
        if (!isset($locationDto)) {
            return "Location is not available. Location Id: " . $bookingDto->getLocationId();
        }
        $integrationId = $locationDto->getIntegrationId();
        if ($integrationId <= 0) {
            return "Integration Error: IntegrationId: " . $integrationId;
        }
        $bookingStatus = $bookingDto->getStatus();
        $bookingIntegrationStatus = $bookingDto->getIntegrationStatus();
        if ($bookingIntegrationStatus != BOOKING_INTEGRATION_STATUS_BOOKED) {
            return "Integration booking status is not booked! Integration book status: " . $bookingIntegrationStatus;
        }
        if (!in_array($bookingStatus, Booking::getReceivedStatuses())) {
            return "Booking status is not booked! Book status: " . $bookingStatus;
        }
        if ($integrationId == $GLOBALS['CONFIG']['INTEGRATIONS'][5]['ID']) {
            $samediBookingsManager = SamediBookingsManager::getInstance($this->config, $this->args);
            $samediBookingDto = $samediBookingsManager->getByBookingId($bookingDto->getId());

            if (!isset($samediBookingDto)) {
                return "Diese Ernennung nicht in unserem System vorhanden sind.";
            }
            $cancelableUntilDate = $samediBookingDto->getCancelableUntil();
            if (date('Y-m-d H:i:s') <= $cancelableUntilDate) {
                $resultJson = $samediBookingDto->getResultJson();
                $bookingResultObject = json_decode($resultJson);
                $bookingResultObject = new SBooking($bookingResultObject);
                $samediSideBookingId = $bookingResultObject->getId();
                $practiceId = $bookingResultObject->getPracticeId();

                if (Configuration::get('samedi_real_booking_enable') != 1) {
                    $cancelResult = true;
                } else {
                    $cancelResult = $this->samediAdapter->cancelBookedAppointment($practiceId, $samediSideBookingId);
                }
                if ($cancelResult === true) {
                    $userDto = $bookingDto->getUser();
                    $dateTime = $bookingDto->getAppointmentStartAt();
                    $this->updateBookingDayAppointmentsFromOriginalTable($userDto->getId(), $locationDto->getId(), $dateTime);
                    $bookingManager->setStatus($bookingDto->getId(), Booking::STATUS_ABORTED);
                    $bookingManager->setIntegrationStatus($bookingDto->getId(), BOOKING_INTEGRATION_STATUS_CANCELED);

                    return true;
                } else {
                    return $this->samediAdapter->getLastError();
                }
            } else {
                return "You could cancel until $cancelableUntilDate";
            }
        }

        return false;
    }

    /**
     * returns array() of SPractice objects
     */
    public function getAllSamediPractices()
    {

        return $this->samediAdapter->getReferrers();
    }

    public function getSamediPracticeCategories($sPractice)
    {
        return $this->samediAdapter->getPracticeCategories($sPractice->getId());
    }

    /**
     * get all doctors, used in class.api
     **/
    public function getAllDoctorsNoLimit()
    {
        return DoctorsMapper::getInstance()->getAllUsersNoLimit();
    }

    /**
     * this function used in import doctors action from admin panel to fetch]
     * all samedi doctors dtos to import them into the system
     */
    public function getAllSamediDoctorsDtos()
    {
        $doctorsDtos = array();
        $locationsManager = LocationsManager::getInstance($this->config, $this->args);

        $samediPracticesManager = SamediPracticesManager::getInstance($this->config, $this->args);
        $practices = $samediPracticesManager->selectAll();

        $samediNetworkManager = SamediNetworkManager::getInstance($this->config, $this->args);
        $networkContactsDtos = $samediNetworkManager->selectAll();

        $samediPracticeCategoriesManager = SamediPracticeCategoriesManager::getInstance($this->config, $this->args);

        foreach ($practices as $practice) {
            $sPractice = new SPractice(json_decode($practice->getData()));

            $practiceCategories = $samediPracticeCategoriesManager->getPracticeCategoriesBySamediPracticeId($sPractice->getId());

            foreach ($practiceCategories as $prCatDto) {
                $sPracticeCategory = new SPracticeCategory(json_decode($prCatDto->getData()));
                $eventTypes = $this->getPracticeCategoryEventTypes($sPractice->getId(), $sPracticeCategory->getId());

                $samediEventTypesCachesDto = $this->samediEventTypesCachesManager->createSamediEventTypesDtoFromPracticeIdAndCategoryIdAndEventTypes($sPractice->getId(), $sPracticeCategory->getId(), $eventTypes);

                $snc = $this->fetchDoctorDetailsFromContactsByDoctorTitle($networkContactsDtos, $sPractice->getId(), $sPracticeCategory->getName());

                $doctorDto = $this->findDoctorByPracticeId($doctorsDtos, $sPracticeCategory->getId());
                $addNewDoctor = false;
                if (!isset($doctorDto)) {
                    $addNewDoctor = true;
                    if ($snc != null) {
                        $locationDto = $locationsManager->createLocationDtoFromSNetworkContact($snc);
                        $doctorDto = $this->createDoctorDtoFromSNetworkContact($snc);
                    } else {
                        $locationDto = $locationsManager->createLocationDtoFromSPractice($sPractice);
                        $doctorDto = $this->createDoctorDtoFromSamediPracticeCategory($sPracticeCategory);
                    }
                }
                if ($doctorDto) {
                    $doctorDto->addLocationDto($locationDto);
                    $samediDataDto = $this->samediDatasManager->createSamediDataDtoFromSPracticeAndSPracticeCategory($sPractice, $sPracticeCategory);
                    $doctorDto->addSamediDataDto($samediDataDto);
                    $doctorDto->addSamediEventTypesCachesDto($samediEventTypesCachesDto);
                    if ($addNewDoctor) {
                        $doctorsDtos[] = $doctorDto;
                    }
                }
            }
        }

        return $doctorsDtos;
    }

    /**
     * returns SNetworkContact object if doctor found in the contact list , null otherwose
     */
    private function fetchDoctorDetailsFromContactsByDoctorTitle($networkContactsDtos, $practiceId, $title)
    {

        foreach ($networkContactsDtos as $ncDto) {
            $snc = new SNetworkContact(json_decode($ncDto->getData()));
            if ($snc->getCanRefer() == 'true' && $snc->getHealerPracticeId() == $practiceId) {
                $healerTitle = trim($snc->getHealerTitle());
                $healerFirstName = trim($snc->getHealerFirstName());
                $healerLastName = trim($snc->getHealerLastName());
                if ((empty($healerFirstName) || strpos($title, $healerFirstName) !== false) &&
                    (empty($healerLastName) || strpos($title, $healerLastName) !== false) &&
                    (empty($healerTitle) || strpos($title, $healerTitle) !== false)
                ) {
                    return $snc;
                }
            }
        }

        return null;
    }

    private function findDoctorByPracticeId($doctorsDtos, $categoryId)
    {
        foreach ($doctorsDtos as $doctor) {
            $sdds = $doctor->getSamediDatasDtos();
            if (!is_array($sdds) || empty($sdds)) {
                continue;
            }
            $sdd = $sdds[0];
            if ($sdd->getCategoryId() == $categoryId) {
                return $doctor;
            }
        }

        return null;
    }

    private function createDoctorDtoFromSNetworkContact(SNetworkContact $snc)
    {
        assert(isset($snc));
        $doctorDto = $this->doctorsMapper->createDto();
        $doctorDto->setFirstName($snc->getHealerFirstName());
        $doctorDto->setLastName($snc->getHealerLastName());
        $doctorDto->setTitle($snc->getHealerTitle());
        $doctorDto->setEmail($snc->getHealerEmail());
        $doctorDto->setGender($snc->getHealerGender() == 'female' ? 1 : 0);
        $doctorDto->setPhone($snc->getHealerPhone());
        $slug = UtilFunctions::createSlugFromString($snc->getHealerTitle() . ' ' . $snc->getHealerFirstName() . ' ' . $snc->getHealerLastName());
        $doctorDto->setSlug($slug);

        return $doctorDto;
    }

    private function createDoctorDtoFromSamediPracticeCategory($sPracticeCategory)
    {
        if (!isset($sPracticeCategory)) {
            return false;
        }
        $doctorDto = $this->doctorsMapper->createDto();
        list($title, $name, $lName) = $this->getDoctorNameAndLastNameByPracticeCategoryName($sPracticeCategory->getName());
        $doctorDto->setTitle($title);
        $doctorDto->setFirstName($name);
        $doctorDto->setLastName($lName);
        $slug = UtilFunctions::createSlugFromString($title . ' ' . $name . ' ' . $lName);
        $doctorDto->setSlug($slug);

        return $doctorDto;
    }

    private static function getDoctorNameAndLastNameByPracticeCategoryName($pcName)
    {
        $pcName = UtilFunctions::replaceStringNonAzAndSpacesCharsToSpaces($pcName);
        $pcName = UtilFunctions::convertToSingleSpaceString($pcName);
        $pcName = trim($pcName);
        if (substr_count($pcName, ' ') === 0) {
            return array('', $pcName, '');
        }
        if (substr_count($pcName, ' ') === 1) {
            $name_lastName_array = explode(' ', $pcName);
            if (strlen($name_lastName_array[0]) > 2) {
                return array('', $name_lastName_array[0], $name_lastName_array[1]);
            }

            return array($name_lastName_array[0], '', $name_lastName_array[1]);
        }
        if (substr_count($pcName, ' ') === 2) {
            return explode(' ', $pcName);
        } else {
            $tnlnArray = explode(' ', $pcName);
            $lname = end($tnlnArray);
            $name = prev($tnlnArray);
            $titleArray = array_slice($tnlnArray, 0, -2);

            return array(implode(' ', $titleArray), $name, $lname);
        }
    }

    public function createEmtpyDoctorDto()
    {
        return $this->doctorsMapper->createDto();
    }
}
