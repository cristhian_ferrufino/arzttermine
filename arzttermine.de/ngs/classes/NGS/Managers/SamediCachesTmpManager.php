<?php

namespace NGS\Managers;

use NGS\DAL\Mappers\SamediCachesTmpMapper;
use NGS\Framework\AbstractManager;

class SamediCachesTmpManager extends AbstractManager {

	private $samediCachesTmpMapper;

	/**
	 * @var singleton instance of class
	 */
	private static $instance = null;

	/**
	 * Initializes DB mappers
	 *
	 * @param object $config
	 * @param object $args
	 * @return
	 */
	function __construct($config = array(), $args = array()) {
		$this->samediCachesTmpMapper = SamediCachesTmpMapper::getInstance();
		$this->config = $config;
		$this->args = $args;
	}

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

	public function getAllSamediCaches($offset, $limit, $orderByFieldName) {
		return $this->samediCachesTmpMapper->getAllSamediCaches($offset, $limit, $orderByFieldName);
	}

	public function getAllSamediCachesCount() {
		return $this->samediCachesTmpMapper->getAllSamediCachesCount();
	}

	public function getDoctorAvailableAppointmentsByDateRange($user_id, $date_start, $date_end, $insurance_id, $treatmenttype_ids) {
		return $this->samediCachesTmpMapper->getDoctorAvailableAppointmentsByDateRange($user_id, $date_start, $date_end, $insurance_id, $treatmenttype_ids);
	}

	public function getDoctorAllNonEmptyAvailableAppointments($user_id, $insuranceId, $treatmentTypesIds) {
		return $this->samediCachesTmpMapper->getDoctorAllNonEmptyAvailableAppointments($user_id, $insuranceId, $treatmentTypesIds);
	}

	public function saveDoctorAppointments($doctor_id, $dateAppointments = '', $onOriginalTable = false) {
		if (is_array($dateAppointments)) {
			if ($onOriginalTable) {
				$otn = $this->samediCachesTmpMapper->getOriginalTableName();
				$tn = $this->samediCachesTmpMapper->getTableName();
				$this->samediCachesTmpMapper->setTableName($otn);
			}
			foreach ($dateAppointments as $aptDate => $appointments) {
				foreach ($appointments as $appointment) {
					list($datetime, $metadata, $treatmenttypeIds) = $appointment;
					$dto = $this->samediCachesTmpMapper->createDto();
					$dto->setUserId($doctor_id);
					$dto->setDate($aptDate);
					$dto->setAppointmentDatetime($datetime);
					$dto->setMetadata($metadata);
					$dto->setTreatmenttypeIds($treatmenttypeIds);
					$dto->setCreatedDate(date('Y-m-d H:i:s'));
					$this->samediCachesTmpMapper->insertDto($dto);
				}
			}
			if ($onOriginalTable) {
				$this->samediCachesTmpMapper->setTableName($tn);
			}
		}
	}

	public function deleteDoctorAppointments($doctor_id, $fromOriginalTable = true) {		
		return $this->samediCachesTmpMapper->deleteDoctorAppointments($doctor_id, $fromOriginalTable);
	}
	
	public function deleteBookingDayAppoitnmentsFromOriginalTable($userId, $date){	
		return $this->samediCachesTmpMapper->deleteBookingDayAppoitnmentsFromOriginalTable($userId, $date);
	}
	
	
	
	public function truncateTable() {
		$this -> samediCachesTmpMapper -> truncateTable();
	}
	public function swapWithOriginalTable() {
		$this -> samediCachesTmpMapper -> swapWithOriginalTable();
	}
	

}