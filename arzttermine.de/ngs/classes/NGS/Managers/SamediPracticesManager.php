<?php

namespace NGS\Managers;

use NGS\DAL\Mappers\SamediPracticesMapper;
use NGS\Framework\AbstractManager;

class SamediPracticesManager extends AbstractManager {

    private $samediPracticesMapper;

    /**
     * @var singleton instance of class
     */
    private static $instance = null;

    /**
     * Initializes DB mappers
     *
     * @param object $config
     * @param object $args
     * @return
     */
    function __construct($config = array(), $args = array()) {
        $this->samediPracticesMapper = SamediPracticesMapper::getInstance();
        $this->config = $config;
        $this->args = $args;
    }

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function selectAll() {
        return $this->samediPracticesMapper->selectAll();
    }

    public function addRow($data) {
        $dto = $this->samediPracticesMapper->createDto();
        $dto->setData($data);         
        return $this->samediPracticesMapper->insertDto($dto);
    }

    
	public function truncateTable() {
        $this->samediPracticesMapper->truncateTable();
	}
}