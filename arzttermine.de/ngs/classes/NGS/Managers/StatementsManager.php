<?php

namespace NGS\Managers;

use NGS\DAL\Mappers\StatementsMapper;
use NGS\Framework\AbstractManager;

class StatementsManager extends AbstractManager {

    private $statementsMapper;

    /**
     * @var singleton instance of class
     */
    private static $instance = null;

    /**
     * Initializes DB mappers
     *
     * @param object $config
     * @param object $args
     * @return
     */
    function __construct($config = array(), $args = array()) {
        $this->statementsMapper = StatementsMapper::getInstance();
        $this->config = $config;
        $this->args = $args;
    }

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

	/**
	 * @return StatementsDto
	 */
	public function createDto(){
        return $this->statementsMapper->createDto();
    }

    public function addStatement($statementDto){
        return $this->statementsMapper->insertDto($statementDto);
    }

    public function updateTextField($id, $fieldName, $fieldValue){
        return $this->statementsMapper->updateTextField($id, $fieldName, $fieldValue);
    }
    
	public function getStatementsByUserIdAndMonthyear($user_id, $monthyear) {
        return $this->statementsMapper->getStatementsByUserIdAndMonthyear($user_id, $monthyear);
    }
	
    public function updateStatement($user_id, $monthyear, $status){
        return $this->statementsMapper->updateStatement($user_id, $monthyear, $status);
    }

	
	public function getStatements($location_id, $period = null) {
		if (!$period) {
			$period = date('Y-M');
		}
		
		return $this->statementsMapper->getStatements($location_id, $period);
	}

}