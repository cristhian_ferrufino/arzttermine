<?php

namespace NGS\Managers;

use NGS\DAL\Mappers\SamediTreatmentTypesMapper;
use NGS\Framework\AbstractManager;
use Arzttermine\Integration\Integration\Samedi\Types\CategoryEventType;

class SamediTreatmentTypesManager extends AbstractManager {

	/**
	 * @var singleton instance of class
	 */
	private static $instance = null;
	private $samediTreatmentTypesMapper;

	/**
	 * Initializes DB mappers
	 *
	 * @param object $config
	 * @param object $args
	 * @return
	 */
	function __construct($config = array(), $args = array()) {
		$this->samediTreatmentTypesMapper = SamediTreatmentTypesMapper::getInstance();
		$this->config = $config;
		$this->args = $args;
	}

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

	public function getById($id) {
		return $this->samediTreatmentTypesMapper->selectByPK($id);
	}

	public function getByIdIncludedParentDto($id) {
		return $this->samediTreatmentTypesMapper->getByIdIncludedParentDto($id);
	}

	public function setParentId($id, $parentId = null) {
		$this->samediTreatmentTypesMapper->updateNumericField($id, 'parent_id', $parentId);
	}

	public function insertRow($eventType, $parentId = null) {
		$dto = $this->samediTreatmentTypesMapper->createDto();
		$dto->setId($eventType->getId());
		$dto->setDuration($eventType->getDurationInMinutes());
		$dto->setInsuranceIds($eventType->getArzttermineFormattedInsuranceIds());
		$dto->setCreatedAt(date('Y-m-d H:i:s'));
		if (isset($parentId) && $parentId > 0) {
			$dto->setParentId($parentId);
		}
		$this->samediTreatmentTypesMapper->insertDto($dto);
		return $dto;
	}

	public function isDoctorSamediTTsSetuped($practice_id, $category_id) {
		$samediEventTypesCachesManager = SamediEventTypesCachesManager::getInstance($this->config, $this->args);
		$categoryEventTypes = $samediEventTypesCachesManager->getCategoryEventTypes($practice_id, $category_id);
		foreach ($categoryEventTypes as $et) {
			$sEvType = new CategoryEventType($et);
			$evId = $sEvType->getId();
			$dto = $this->getById($evId);
			if (!isset($dto)) {
				return false;
			}
		}
		return true;
	}

	public function isDoctorSamediTTsParentIdsSetuped($practice_id, $category_id) {
		$samediEventTypesCachesManager = SamediEventTypesCachesManager::getInstance($this->config, $this->args);
		$categoryEventTypes = $samediEventTypesCachesManager->getCategoryEventTypes($practice_id, $category_id);
		foreach ($categoryEventTypes as $et) {
			$sEvType = new CategoryEventType($et);
			$evId = $sEvType->getId();
			$dto = $this->getById($evId);
			if (!isset($dto)) {
				return false;
			}
			if (!($dto->getParentId() > 0)) {
				return false;
			}
		}
		return true;
	}

	public function isDoctorTTsSetupedInDoctorTtTable($userId, $practice_id, $category_id) {
		$doctorsTreatmentTypesManager = DoctorsTreatmentTypesManager::getInstance($this->config, $this->args);
		$docTTs = $doctorsTreatmentTypesManager->getByDoctorId($userId);
		$samediEventTypesCachesManager = SamediEventTypesCachesManager::getInstance($this->config, $this->args);
		$categoryEventTypes = $samediEventTypesCachesManager->getCategoryEventTypes($practice_id, $category_id);
		$paretnTTIdsArray = array();
		foreach ($categoryEventTypes as $et) {
			$sEvType = new CategoryEventType($et);
			$evId = $sEvType->getId();
			$dto = $this->getById($evId);
			if (!isset($dto)) {
				return false;
			}
			if (!($dto->getParentId() > 0)) {
				return false;
			}
			$paretnTTIdsArray [] = $dto->getParentId();
		}
		$paretnTTIdsArray = array_unique($paretnTTIdsArray);
		foreach ($paretnTTIdsArray as $parentTTId) {
			$samediTTParentInGivenDocTTDtos = $this->isSamediTTParentInGivenDocTTDtos($parentTTId, $docTTs);
			if (!$samediTTParentInGivenDocTTDtos)
				return false;
		}
		return true;
	}

	private function isSamediTTParentInGivenDocTTDtos($parentTTId, $docTTs) {
		foreach ($docTTs as $docTTDto) {
			if ($docTTDto->getTreatmentTypeId() == $parentTTId) {
				return true;
			}
		}
		return false;
	}

}