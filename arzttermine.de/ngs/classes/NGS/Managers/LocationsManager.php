<?php

namespace NGS\Managers;

use Arzttermine\Location\Location;
use Arzttermine\Maps\GoogleMap;
use Arzttermine\User\User;
use Arzttermine\Integration\Integration\Samedi\Types\SNetworkContact;
use Arzttermine\Integration\Integration\Samedi\Types\SPractice;

use NGS\DAL\DTO\LocationsDto;
use NGS\DAL\Mappers\LocationsMapper;
use NGS\Framework\AbstractManager;
use NGS\Util\UtilFunctions;

class LocationsManager extends AbstractManager {

    /**
     * @var LocationsMapper
     */
    private $locationsMapper;

    /**
     * @var self singleton instance of class
     */
    private static $instance = null;

    /**
     * Initializes DB mappers
     *
     * @param array $config
     * @param array $args
     */
    public function __construct($config = array(), $args = array())
    {
        $this->locationsMapper = LocationsMapper::getInstance();
        $this->config = $config;
        $this->args = $args;
    }

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param int $locationId
     *
     * @return LocationsDto
     */
    public function getById($locationId)
    {
        return $this->locationsMapper->selectByPK($locationId);
    }

    /**
     * @param string $slug
     *
     * @return LocationsDto
     */
    public function getBySlug($slug)
    {
        return $this->locationsMapper->getBySlug($slug);
    }

    /**
     * @return array
     */
    public function selectAll()
    {
        return $this->locationsMapper->selectAll();
    }

    /**
     * @param int $offset
     * @param int $limit
     * @param string $orderByFieldName
     *
     * @return LocationsDto[]
     */
    public function getAllLocations($offset, $limit, $orderByFieldName)
    {
        return $this->locationsMapper->getAllLocations($offset, $limit, $orderByFieldName);
    }

    /**
     * Get all locations' names
     * 
     * @todo Remove. Useless
     * 
     * @return array
     */
    public function getAllLocationNames()
    {
        return $this->locationsMapper->getAllLocationNames();
    }

    /**
     * get all locations, used in class.api
     *
     * @return LocationsDto[]
     **/
    public function getAllLocationsNoLimit()
    {
        return LocationsMapper::getInstance()->getAllLocationsNoLimit();
    }

    /**
     * @return int
     */
    public function getAllLocationsCount()
    {
        return $this->locationsMapper->getAllLocationsCount();
    }

    /**
     * @param int $id
     * @param array $fields
     * @param array $values
     *
     * @return int
     */
    public function updateLocation($id, $fields, $values)
    {
        return $this->locationsMapper->updateWithFields($id, $fields, $values);
    }

    /**
     * @param SPractice $sPractice
     *
     * @return LocationsDto
     */
    public function createLocationDtoFromSPractice(SPractice $sPractice)
    {
        $dto = $this->locationsMapper->createDto();
        $dto->setName($sPractice->getName());
        $dto->setIntegrationId(5);
        return $dto;
    }

    /**
     * @param int $integrationId
     * @param bool $filterByStatus
     *
     * @return LocationsDto[]
     */
    public function getIntegrationLocationsWithDoctors($integrationId, $filterByStatus = true)
    {
        $locationStatus = null;
        $userStatus = null;
        if ($filterByStatus) {
            $locationStatus = Location::STATUS_VISIBLE_APPOINTMENTS;
            $userStatus = User::STATUS_VISIBLE_APPOINTMENTS;
        }
        return $this->locationsMapper->getIntegrationLocationsWithDoctors($integrationId , $locationStatus, $userStatus);
    }


    /**
     * @param int[] $excludedIntegrationIdsArray
     *
     * @return LocationsDto[]
     */
    public function findAllVisibleDoctors($excludedIntegrationIdsArray = array())
    {
        return $this->locationsMapper->findAllVisibleDoctors(2, 2, $excludedIntegrationIdsArray);
    }

    /**
     * @param SNetworkContact $snk
     *
     * @return LocationsDto
     */
    public function createLocationDtoFromSNetworkContact(SNetworkContact $snk)
    {
        $dto = $this->locationsMapper->createDto();
        $dto->setName($snk->getHealerPracticeName());
        $dto->setStreet($snk->getHealerStreet());
        $dto->setZip($snk->getHealerZip());
        $dto->setCity($snk->getHealerCity());
        $dto->setLat($snk->getHealerLatitude());
        $dto->setLng($snk->getHealerLongitude());
        $dto->setWww($snk->getHealerHomepage());
        $slug = UtilFunctions::createSlugFromString($snk->getHealerPracticeName());
        $dto->setSlug($slug);
        $dto->setIntegrationId(5);
        return $dto;
    }

    /**
     * @param $userId
     *
     * @return LocationsDto[]
     */
    public function getLocationsByUserId($userId)
    {
        return $this->locationsMapper->getLocationsByUserId($userId);
    }

    /**
     * @return LocationsDto
     */
    public function createDto()
    {
        return $this->locationsMapper->createDto();
    }

    /**
     * @param int $locationStatus
     * @param int $userStatus
     * @param int $medicalSpecialtyId
     * @param int $doctorTreatmentTypeId
     * @param int $distance
     * @param float $lng
     * @param float $lat
     *
     * @return array
     */
    public function getCityByGivenFilters($locationStatus, $userStatus, $medicalSpecialtyId, $doctorTreatmentTypeId, $distance, $lng , $lat)
    {
        return $this->locationsMapper->getCityByGivenFilters($locationStatus, $userStatus, $medicalSpecialtyId, $doctorTreatmentTypeId, $distance, $lng , $lat);
    }

    /**
     * @param LocationsDto $locationDto
     *
     * @return int
     */
    public function addLocation($locationDto)
    {
        return $this->locationsMapper->insertDto($locationDto);
    }

    /**
     * @param int $id
     * @param string $fieldName
     * @param string $fieldValue
     *
     * @return int
     */
    public function updateTextField($id, $fieldName, $fieldValue)
    {
        return $this->locationsMapper->updateTextField($id, $fieldName, $fieldValue);
    }

    /**
     * @param $cityNamesArray
     *
     * @return array
     */
    public function getCitiesWithMedicalSpecialties($cityNamesArray)
    {
        return $this->locationsMapper->getCitiesWithMedicalSpecialties($cityNamesArray);
    }

    /**
     * @param int $locationId
     * @param int[] $medicalSpecialtiesArr
     *
     * @return int
     */
    public function addMedicalSpecialties($locationId, $medicalSpecialtiesArr)
    {
        $locationDto = $this->locationsMapper->selectByPK($locationId);
        $locationDto->setMedicalSpecialtyIds(implode(',', $medicalSpecialtiesArr));

        return $this->locationsMapper->updateByPK($locationDto);
    }

    /**
     * Returns all locations Dtos
     *
     * @return array
     */
    public function getAllIntegrationsIdsArray()
    {
        return $this->locationsMapper->getAllIntegrationsIdsArray();
    }

    /**
     * @param int $id
     * @param string $salesforceId
     *
     * @return int
     */
    public function setSalesforceId($id, $salesforceId)
    {
        return $this->locationsMapper->updateTextField($id, 'salesforce_id', $salesforceId);
    }

    /**
     * @param int $status
     *
     * @return LocationsDto[]
     */
    public function getLocationsByStatus($status)
    {
        return $this->locationsMapper->getLocationsByStatus($status);
    }

    /**
     * @param string $salesforceId
     *
     * @return bool
     */
    public function getIdBySalesforceId($salesforceId)
    {
        return $this->locationsMapper->getIdBySalesforceId($salesforceId);
    }

    /**
     * @param int $locationStatus
     * @param int $userStatus
     *
     * @return LocationsDto[]
     */
    public function getAllLocationsWithDoctors($locationStatus, $userStatus)
    {
        return $this->locationsMapper->getAllLocationsWithDoctors($locationStatus, $userStatus);
    }

    /**
     * @param string $street
     * @param string $zip
     * @param string $city
     *
     * @return array
     */
    public function reverseGeocode($street, $zip, $city)
    {
        $coords = GoogleMap::getGeoCoords("{$street}, {$zip} {$city}");
        
        return array(
            'lng' => $coords['lng'],
            'lat' => $coords['lat']
        );
    }

    /**
     * Inserts $tempUserDataDto field values that correspond to location into locationDto
     *
     * @param object $tempUserData
     *
     * @return LocationsDto.
     */
    public function insertTempUserDataIntoDto($tempUserData)
    {
        $locationDto = $this->locationsMapper->createDto();
        $locationDto->setName($tempUserData->getLocationName());
        $locationDto->setStreet($tempUserData->getLocationStreet());
        $locationDto->setZip($tempUserData->getLocationZip());
        $locationDto->setCity($tempUserData->getLocationCity());
        $locationDto->setWww($tempUserData->getLocationWww());
        $slug = UtilFunctions::createSlugFromString($locationDto->getName());
        $locationDto->setSlug($slug);
        $address = $locationDto->getStreet().','.$locationDto->getZip().' '.$locationDto->getCity();
        $coords = GoogleMap::getGeoCoords($address);
        $locationDto->setLat($coords["lat"]);
        $locationDto->setLng($coords["lng"]);
        return $locationDto;
    }

    /**
     * @param int $locationId
     *
     * @return LocationsDto|null
     */
    public function getLocationWithUsersById($locationId)
    {
        return $this->locationsMapper->getLocationWithUsersById($locationId);
    }

    /**
     * @param int[] $locationIdsArray
     *
     * @return LocationsDto[]
     */
    public function getLocationsByIds($locationIdsArray)
    {
        return $this->locationsMapper->getLocationsByIds($locationIdsArray);
    }

    /**
     * @param int $status
     *
     * @return array
     */
    public function getPhoneNumbersByStatus($status)
    {
        return $this->locationsMapper->getPhoneNumbersByStatus($status);
    }

}
