<?php

namespace NGS\Managers;

use NGS\DAL\Mappers\UsersEditHistoryMapper;
use NGS\Framework\AbstractManager;

class UsersEditHistoryManager extends AbstractManager {

	private $usersEditHistoryMapper;

	/**
	 * @var singleton instance of class
	 */
	private static $instance = null;

	/**
	 * Initializes DB mappers
	 *
	 * @param object $config
	 * @param object $args
	 * @return
	 */
	function __construct($config = array(), $args = array()) {
		$this->usersEditHistoryMapper = UsersEditHistoryMapper::getInstance();
		$this->config = $config;
		$this->args = $args;
	}

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }
    
    public function insertDoctorOldInfo($doctor){
        $history = $this->usersEditHistoryMapper->getByDoctorId($doctor->getId());
        if(count($history)==10){
            $idToBeDeleted = $history[0]->getId();
            $this->usersEditHistoryMapper->deleteByPK($idToBeDeleted);
            
        }
        $dto = $this->convertDoctor($doctor);
        return $this->usersEditHistoryMapper->insertDto($dto);
  
    }
    
    public function convertDoctor($doctor){
        $dto = $this->usersEditHistoryMapper->createDto();
        $dto->setUserId($doctor->getId());
        $arr = $dto->getMapArray();
        unset($arr['id']);
        unset($arr['user_id']);
        foreach ($arr as $dtoFieldName) {
            $setFuncName = 'set'.ucfirst($dtoFieldName);
            $getFuncName = 'get'.ucfirst($dtoFieldName);
            $dto->$setFuncName($doctor->$getFuncName());
        }
        return $dto;
    }
    
    /**
     * Returns all edit history for specific user
     * @param type $userId
     * @param int $limit if set, then returns specified number of history
     * @return array $userEditHistorys
     */
    public function getByUserId($userId, $limit=null) {
        return $this->usersEditHistoryMapper->getByUserId($userId, $limit);
    }
    
    public function getById($id){
        return $this->usersEditHistoryMapper->selectByPK($id);
    }
        
}