<?php

namespace NGS\Managers;

use Arzttermine\Booking\Appointment;
use Arzttermine\Calendar\Calendar;
use Arzttermine\Location\Location;
use Arzttermine\User\User;
use Arzttermine\Core\Configuration;
use NGS\DAL\Mappers\SearchResultCacheTmpMapper;
use NGS\Framework\AbstractManager;
use NGS\Util\ProviderRow;

class SearchResultCacheTmpManager extends AbstractManager {

    /**
     * @var SearchResultCacheTmpMapper
     */
    private $searchResultCacheTmpMapper;

	/**
	 * @var self singleton instance of class
	 */
	private static $instance = null;

	/**
	 * Initializes DB mappers
	 *
	 * @param array $config
	 * @param array $args
	 */
	public function __construct($config = array(), $args = array())
    {
		$this->searchResultCacheTmpMapper = SearchResultCacheTmpMapper::getInstance();
		$this->config = $config;
		$this->args = $args;
	}

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

	public function updateByPK($row, $fromOriginalTable = false)
    {
		if ($fromOriginalTable) {
			$otn = $this->searchResultCacheTmpMapper->getOriginalTableName();
			$tn = $this->searchResultCacheTmpMapper->getTableName();
			$this->searchResultCacheTmpMapper->setTableName($otn);
		}
		$this->searchResultCacheTmpMapper->updateByPK($row);
        
		if ($fromOriginalTable) {
			$this->searchResultCacheTmpMapper->setTableName($tn);
		}
	}

	public function getById($id)
    {
		return $this->searchResultCacheTmpMapper->selectByPK($id);
	}

	public function getDoctorWithItsLocations($doctorId, $insurance_id)
    {
		return $this->searchResultCacheTmpMapper->getDoctorWithItsLocations($doctorId, $insurance_id);
	}

	public function getByDoctorIdAndLocationId($doctorId, $locationId, $insurance_id)
    {
		return $this->searchResultCacheTmpMapper->getByDoctorIdAndLocationId($doctorId, $locationId, $insurance_id);
	}

	public function getLocationDoctors($locationId, $insurance_id)
    {
		return $this->searchResultCacheTmpMapper->getLocationDoctors($locationId, $insurance_id);
	}

	public function deleteDoctorCaches($userId, $locationId)
    {
		$this->searchResultCacheTmpMapper->deleteDoctorCaches($userId, $locationId);
	}

	public function getByUserIdAndLocationIdAndInsurance($userId, $locationId, $insuranceId)
    {
		return $this->searchResultCacheTmpMapper->getByUserIdAndLocationIdAndInsurance($userId, $locationId, $insuranceId);
	}

	public function addRows($userId, $locationId, $allAvailableAppointments, $integrationId, $insertToOriginalTable=false)
    {
		$doctorsTreatmentTypesManager = DoctorsTreatmentTypesManager::getInstance($this->config, $this->args);
		$treatment_type_ids = $doctorsTreatmentTypesManager->getTtIdsByDoctorId($userId);
		if (!is_array($allAvailableAppointments)) {
			return;
		}
		if ($insertToOriginalTable) {
			$this->searchResultCacheTmpMapper->swapTableName();
		}
		$insertQueryPartsArray = array();
		foreach ($allAvailableAppointments as $date => $appointments) {
			foreach ($appointments as $appointment) {
				$dto = $this->searchResultCacheTmpMapper->createDto();
				$dto->setId('');
				$dto->setDate($date);
				$dto->setUserId($userId);
				$dto->setLocationId($locationId);
				$dto->setAppointment($appointment->getDatetime());
				$dto->setLastUpdatedTime(date('Y-m-d H:i:s', time()));

				/*
				 * right now only samedi appointments have set the insuranceIds (and not only insuranceId) at this stage
				 * so we use the insuranceId as a default
				 */
				$insuranceIds = $appointment->getInsuranceIds();
				if (!empty($insuranceIds)) {
					$dto->setInsuranceIds($insuranceIds);
				} else {
					$dto->setInsuranceIds($appointment->getInsuranceId());
				}

				switch ($integrationId) {
					case 1:
						$ttDtos = $appointment->getTreatmenttypeDtos();
						$ttIdsArray = array();
						foreach ($ttDtos as $ttDto) {
							$ttIdsArray[] = $ttDto->getId();
						}
						$treatment_type_ids = implode(',', $ttIdsArray);
						$dto->setTreatmenttypeIds($treatment_type_ids);
						$insertQueryPartsArray[] = sprintf("('','%s','%s','%s',%d,%d,'%s','%s')",$dto->getDate(),$dto->getInsuranceIds(),$dto->getTreatmenttypeIds(),$dto->getUserId(),$dto->getLocationId(),$dto->getAppointment(),$dto->getLastUpdatedTime());
						break;
					case 2:
						$dto->setTreatmenttypeIds($treatment_type_ids);
						$insertQueryPartsArray[] = sprintf("('','%s','%s','%s',%d,%d,'%s','%s')",$dto->getDate(),$dto->getInsuranceIds(),$dto->getTreatmenttypeIds(),$dto->getUserId(),$dto->getLocationId(),$dto->getAppointment(),$dto->getLastUpdatedTime());
						break;
					case 5:
						$samediTtDtos = $appointment->getTreatmenttypeDtos();
						foreach ($samediTtDtos as $samediTtDto) {
							$dtoToBeInserted = clone $dto;
							$dtoToBeInserted->setInsuranceIds($samediTtDto->getInsuranceIds());
							$ttDto = $samediTtDto->getBaseTreatmenttypeDto();
							$dtoToBeInserted->setTreatmenttypeIds($ttDto->getId());
							$insertQueryPartsArray[] = sprintf("('','%s','%s','%s',%d,%d,'%s','%s')",$dtoToBeInserted->getDate(),$dtoToBeInserted->getInsuranceIds(),$dtoToBeInserted->getTreatmenttypeIds(),$dtoToBeInserted->getUserId(),$dtoToBeInserted->getLocationId(),$dtoToBeInserted->getAppointment(),$dtoToBeInserted->getLastUpdatedTime());
						}
						break;
					default:
						break;
				}
			}
		}
		//preventing mysql crash because of very big query
		$offset = 0;
		$insertPerQuery = 5000;
		while ($offset < count($insertQueryPartsArray)) {
			$insertQueryPartsArraySliced = array_slice($insertQueryPartsArray, $offset, $insertPerQuery);
			$this->searchResultCacheTmpMapper->insertAppointmentsFromSubqueryArray($insertQueryPartsArraySliced);
			$offset += $insertPerQuery;
		}
		if ($insertToOriginalTable) {
			$this->searchResultCacheTmpMapper->swapTableName();
		}
	}

	public function truncateTempTable()
    {
		$this->searchResultCacheTmpMapper->truncateTempTable();
	}

	public function swapWithOriginalTable()
    {
		$this->searchResultCacheTmpMapper->swapWithOriginalTable();
	}

	public function clearDoctorData($doctorId, $fromOriginalTable)
    {
	    $this->searchResultCacheTmpMapper->clearDoctorData($doctorId, $fromOriginalTable);
	}

	/**
	 * Calculates and sets the search result caches for each given ProviderRow
	 * 
	 * @param ProviderRow[] $visibleProviders
	 * @param string $date_start
	 * @param string $date_end
	 * @param int $insuranceId
	 * @param int $treatmenttypeId
	 * @param int $firstWeek
	 * @param bool $enableFilter
	 *
	 * @return ProviderRow[]
	 */
	public function getProviderRowsByFilters(&$visibleProviders, $date_start, $date_end, $insuranceId, $treatmenttypeId, $firstWeek = 0, $enableFilter = true)
    {
		// get all userIds and locationIds for the cache query
		$userIds = array();
		$locationIds = array();
		foreach ($visibleProviders as $visibleProvider) {
			$userIds[] = $visibleProvider->getUser()->getId();
			$locationIds[] = $visibleProvider->getLocation()->getId();
		}
		// set all data from the cache
		// returns an array with results[userId][locationId]
		$searchResultCacheDtos = $this->searchResultCacheTmpMapper->getSearchResultCachesByFilters($userIds, $locationIds, $date_start, $date_end, $insuranceId, $treatmenttypeId);

		foreach ($visibleProviders as $visibleProvider) {
			$user = $visibleProvider->getUser();
			$userId = $user->getId();
			$location = $visibleProvider->getLocation();
			$locationId = $location->getId();

			$userResultCaches = array();
			if (isset($searchResultCacheDtos[$userId][$locationId])) {
				$userResultCaches = $searchResultCacheDtos[$userId][$locationId];
			}
			if ($enableFilter) {
				$userResultCaches = self::filterAppointments($userResultCaches, $user, $location, $date_start, $date_end, $firstWeek);
			}
			if (empty($userResultCaches)) {
				$userResultCaches = $this->searchResultCacheTmpMapper->getNextAvailableSearchResultCachesByFilters($date_start, $userId, $locationId, $insuranceId, $treatmenttypeId);
				if ($enableFilter) {
					$userResultCaches = self::filterAppointments($userResultCaches, $user, $location, $date_start, $date_end, $firstWeek);
				}
				
				if (isset($userResultCaches[0])) {
					$userResultCaches = array($userResultCaches[0]);
				} else {
					$userResultCaches = array();
				}
			}
			$visibleProvider->setSearchResultCaches($userResultCaches);
		}
		return $visibleProviders;
	}

	/**
	 * Return an array of Appointments
	 * 
	 * @param User $user
	 * @param Location $location
	 * @param string $datetime_start
	 * @param string $datetime_end
	 * @param int $insuranceId
	 * @param int $treatmenttypeId
	 * @param int $firstWeek
	 * @param bool $enableFilter
	 * @param string $format ('Appointment' | 'datetime')
	 *
	 * @return Appointment[]
	 **/
	public function getAvailableAppointments($user, $location, $datetime_start, $datetime_end, $insuranceId, $treatmenttypeId, $firstWeek = 0, $enableFilter = true, $format = 'Appointment')
    {
        $provider = new ProviderRow();
        $provider
            ->setUser($user)
            ->setLocation($location);
        $providers = array($provider);

        $this->getProviderRowsByFilters($providers, $datetime_start, $datetime_end, $insuranceId, $treatmenttypeId, $firstWeek, $enableFilter);
        $appointmentsArray = array();

        $searchResultCaches = $provider->getSearchResultCaches();
		foreach ($searchResultCaches as $searchResultCache){
			$date = $searchResultCache->getDate();
			if ($format=='datetime'){
				$appointmentsArray[$date][] = $searchResultCache->getAppointment();
			} else {
				$appointment = ProviderRow::convertSearchResultCacheToAppointment($searchResultCache);
				$appointmentsArray[$date][] = $appointment;
			}
		}
        
		return $appointmentsArray;
	}

	static function filterAppointments($userResultCaches, $user, $location, $date_start, $date_end, $firstWeek)
    {
		$userResultCachesArrayWithDateKeys = array();
		foreach ($userResultCaches as $userResultCache) {
			$date = $userResultCache->getDate();
			$userResultCachesArrayWithDateKeys[$date][] = $userResultCache;
		}
		//FILTER INT1 APTS FOR NEXT X DAYS HIDING THE APPOINTMENTS
		if ($location->getIntegrationId() == 1) {
			$daysNumberToHide = intval(Configuration::get('hide_all_int1_doctors_apts_for_next_days'));
			$todayDate = date('Y-m-d');
			for ($i = 0; $i < $daysNumberToHide; $i++) {
				$dateToHide = date('Y-m-d', strtotime($todayDate . " +" . $i . " day"));
				unset($userResultCachesArrayWithDateKeys[$dateToHide]);
			}
		}

		//FILTER FOR NEXT X DAYS HIDING THE APPOINTMENTS BY USER		
		$userHideNextDaysAppts = preg_replace('/\s+/', '', Configuration::get('users_hide_next_days_appts'));
		$userHideNextDaysApptsArray = explode(',', $userHideNextDaysAppts);

		$usersIdsAndDaysToHideArrayMap = array();
		foreach ($userHideNextDaysApptsArray as $value) {
			list($userId, $daysToHide) = explode('->', $value);
			$usersIdsAndDaysToHideArrayMap[$userId] = $daysToHide;
		}
		
		if (array_key_exists($user->getId(), $usersIdsAndDaysToHideArrayMap)) {
			$todayDate = date('Y-m-d');
			$daysNumberToHide = intval($usersIdsAndDaysToHideArrayMap[$user->getId()]);
			for ($i = 0; $i < $daysNumberToHide; $i++) {
				$dateToHide = date('Y-m-d', strtotime($todayDate . " +" . $i . " day"));
				unset($userResultCachesArrayWithDateKeys[$dateToHide]);
			}
		}

		// EXEPTION FOR HOLIDAYS AND WEEKEND
		$dates = array_keys($userResultCachesArrayWithDateKeys);
		foreach ($dates as $_date) {
			if (!Calendar::isWorkingDay($_date)) {
				unset($userResultCachesArrayWithDateKeys[$_date]);
			}
		}

		foreach ($dates as $_date) {
			if (Calendar::isRegionalHoliday($_date, $location->getCity(), $location->getZip())) {
				unset($userResultCachesArrayWithDateKeys[$_date]);
			}
		}

		//current day app after current time minutes
		$currentTime = time();
		if (date('H:i', $currentTime) < SHOW_NEXT_DAY_APPS_AFTER_THIS_TIME) {
			//means calendar is showing same day appts
			$todayDate = date('Y-m-d', $currentTime);

			if (array_key_exists($todayDate, $userResultCachesArrayWithDateKeys)) {
				if ($location->getIntegrationId() == 1) {
					unset($userResultCachesArrayWithDateKeys [$todayDate]);
				} else {
					$todayUserResultCaches = $userResultCachesArrayWithDateKeys[$todayDate];
					$showAppointmentsAfterThisTime = date('H:i', $currentTime + CURRENT_DAY_APP_SHOW_AFTER_MINUTES * 60);
					$userResultCachesArrayWithDateKeys [$todayDate] =
							self::filterWorkingDayAppointmentsByTime($todayUserResultCaches, $showAppointmentsAfterThisTime);
				}
			}
		}

		//  working day apps after specified time NEXT_WORKING_APTS_FOR_NOT_WORKING_HOURS_AFTER
		 if ($location->getIntegrationId() == 1 && $firstWeek == 1) {
			if (!empty($userResultCachesArrayWithDateKeys)) {

				foreach ($userResultCachesArrayWithDateKeys as $date => $userResultCachesArray) {
					$wd = date('w', strtotime($date));
					if ($wd == 0 || $wd == 6) {
                        continue;
                    }

					if (!Calendar::isInWorkingHours()) {
						$nextWorkingDay = $date_start;
						while ($nextWorkingDay < $date_end) {
							if (Calendar::isWorkingDay($nextWorkingDay)) {
								if (isset($userResultCachesArrayWithDateKeys [$nextWorkingDay])) {
									$userResultCachesArrayWithDateKeys [$nextWorkingDay] = self::filterWorkingDayAppointmentsByTime(
											$userResultCachesArray, NEXT_WORKING_APTS_FOR_NOT_WORKING_HOURS_AFTER);
								}
								break;
							} else {
								$nextWorkingDay = Calendar::addDays($nextWorkingDay, 1);
							}
						}
					}
					break;
				}
			}
		}

		$filteredResultCaches = array();
		foreach ($userResultCachesArrayWithDateKeys as $userResultCaches) {
			$filteredResultCaches = array_merge($filteredResultCaches, $userResultCaches);
		}
		return $filteredResultCaches;
	}

	private static function filterWorkingDayAppointmentsByTime($userResultCaches, $timeStr)
    {
		$ret = array();
        
		foreach ($userResultCaches as $userResultCache) {
			$dateTime = $userResultCache->getAppointment();
			if (!is_numeric($dateTime)) {
				$dateTime = strtotime($dateTime);
			}
			$apTime = date('H:i', $dateTime);
			if ($apTime > $timeStr) {
				$ret[] = $userResultCache;
			}
		}
        
		return $ret;
	}
	
	public function deleteByUserIdAndLocationIdAndDate($userId, $locationId, $date)
    {
		$this->searchResultCacheTmpMapper->deleteByUserIdAndLocationIdAndDate($userId, $locationId, $date);
	}
}
