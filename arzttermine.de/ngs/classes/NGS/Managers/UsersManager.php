<?php

namespace NGS\Managers;

use NGS\DAL\DTO\DoctorsDto;
use NGS\DAL\Mappers\DoctorsMapper;
use NGS\DAL\Mappers\UsersMapper;
use NGS\Framework\AbstractManager;
use NGS\Util\UtilFunctions;

class UsersManager extends AbstractManager {

    /**
     * @var singleton instance of class
     */
    private static $instance = null;

    private $usersMapper;

    private $doctorsMapper;

    /**
     * Initializes DB mappers
     *
     * @param object $config
     * @param object $args
     *
     * @return
     */
    function __construct($config = array(), $args = array())
    {
        $this->usersMapper = UsersMapper::getInstance();
        $this->doctorsMapper = DoctorsMapper::getInstance();
        $this->config = $config;
        $this->args = $args;
    }

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function getFixedWeekUsers($offset, $limit)
    {

        return $this->usersMapper->getFixedWeekUsers($offset, $limit);
    }

    public function getUserByEmailAndPasswordHash($email, $passHash)
    {
        return $this->usersMapper->getUserByEmailAndPasswordHash($email, $passHash);
    }

    public function getUserByEmail($email)
    {
        return $this->usersMapper->getUserByEmail($email);
    }

    /**
     * @param $id
     *
     * @return DoctorsDto
     */
    public function getUserById($id)
    {
        return $this->doctorsMapper->selectByPK($id);
    }

    public function getUserByIdAndHash($id, $hash)
    {
        return $this->usersMapper->getUserByIdAndHash($id, $hash);
    }

    public function getUserByResetPasswordCode($resetPasswordCode)
    {
        return $this->usersMapper->getUserByResetPasswordCode($resetPasswordCode);
    }

    public function setUserProfileFields($userId, $docinfo1, $docinfo2, $docinfo3, $docinfo5, $docinfo6, $passwordHashSalt, $passwordHash)
    {
        $dto = $this->usersMapper->selectByPK($userId);
        $dto->setDocinfo1($docinfo1);
        $dto->setDocinfo2($docinfo2);
        $dto->setDocinfo3($docinfo3);
        $dto->setDocinfo5($docinfo5);
        $dto->setDocinfo6($docinfo6);
        $dto->setPasswordHashSalt($passwordHashSalt);
        $dto->setPasswordHash($passwordHash);
        $dto->setUpdatedAt(date('Y-m-d H:i:s'));

        if ($this->usersMapper->updateByPK($dto) == 1) {
            return $dto;
        }

        return false;
    }

    /**
     *
     * @param type $fixedWeek (0 or 1)
     */

    public function setUserFixedWeek($userId, $fixedWeek)
    {
        $this->usersMapper->updateNumericField($userId, 'fixed_week', $fixedWeek);
    }

    public function getUserGroupAccessesArrayByUserId($user_id)
    {
        $userGroupAccessesDtos = $this->usersMapper->getUserGroupAccesses($user_id);
        $ret = array();
        foreach ($userGroupAccessesDtos as $userGroupAccessesDto) {
            $ret[] = $userGroupAccessesDto->getGroupAccessId();
        }

        return $ret;
    }

    public function getAllUsers($offset, $limit, $orderByFieldName)
    {
        return $this->usersMapper->getAllUsers($offset, $limit, $orderByFieldName);
    }

    public function getAllUsersCount()
    {
        return $this->usersMapper->getAllUsersCount();
    }

    public function getDoctorsFull($offset, $limit, $orderByFieldName, $ascending = true, array $searchArray = array())
    {
        return $this->doctorsMapper->getDoctorsFull($offset, $limit, $orderByFieldName, $ascending, $searchArray);
    }

    public function addWrongLoginAttempt($id)
    {
        $user = $this->usersMapper->selectByPk($id);
        assert(isset($user));
        $this->usersMapper->updateNumericField($id, 'wrong_login_attempt', $user->getWrongLoginAttempt() + 1);
    }

    public function resetWrongLoginAttempt($id)
    {
        $this->usersMapper->updateNumericField($id, 'wrong_login_attempt', 0);
    }

    public function setUserHasContract($id, $at)
    {
        $this->usersMapper->updateNumericField($id, 'has_contract', $at);
    }

    public function setUserProfileAssetId($id, $at)
    {
        $this->usersMapper->updateNumericField($id, 'profile_asset_id', $at);
    }

    public function setProfileAssetFilename($id, $at)
    {
        $this->usersMapper->updateTextField($id, 'profile_asset_filename', $at);
    }

    public function setMedicalSpecialities($id, $medicalSpecialitiesIdsArray)
    {
        return $this->usersMapper->updateTextField($id, 'medical_specialty_ids', implode(',', $medicalSpecialitiesIdsArray));
    }

    public function insertDoctorFull($doctorDto)
    {
        return $this->doctorsMapper->insertDoctorFull($doctorDto);
    }

    public function setResetPasswordCode($id, $passwordResetCode)
    {
        return $this->usersMapper->updateTextField($id, 'reset_password_code', $passwordResetCode);
    }

    public function setPasswordHash($id, $passwordHash)
    {
        return $this->usersMapper->updateTextField($id, 'password_hash', $passwordHash);
    }

    public function getDoctorWithMedicalSpecialtiesAndTreatmentTypes($id)
    {
        return $this->doctorsMapper->getDoctorWithMedicalSpecialtiesAndTreatmentTypes($id);
    }

    public function updateByPK($dto)
    {
        return $this->usersMapper->updateByPK($dto);
    }

    public function addUser($userDto)
    {
        return $this->usersMapper->insertDto($userDto);
    }

    public function updateTextField($id, $fieldName, $fieldValue)
    {
        return $this->usersMapper->updateTextField($id, $fieldName, $fieldValue);
    }

    public function setActivationCode($id, $activationCode)
    {
        return $this->usersMapper->updateTextField($id, 'activation_code', $activationCode);
    }

    public function activateDoctor($id)
    {
        return $this->usersMapper->updateTextField($id, 'activated_at', date("Y-m-d H:i:s"));
    }

    public function deleteUserById($id)
    {
        return $this->usersMapper->deleteByPK($id);
    }

    public function setLastLoginAt($id, $date)
    {
        return $this->usersMapper->updateTextField($id, 'last_login_at', $date);
    }

    public function deleteDoctorMedicalSpecialty($doctorId, $medicalSpecialtyId)
    {
        return $this->usersMapper->deleteDoctorMedicalSpecialty($doctorId, $medicalSpecialtyId);
    }

    public function getUserByNameAndLNameANDPracticeName($practiceName, $userName, $userLName)
    {
        return $this->usersMapper->getUserByNameAndLNameANDPracticeName($practiceName, $userName, $userLName);
    }

    public function getUserByActivationCode($activationCode)
    {
        return $this->usersMapper->getUserByActivationCode($activationCode);
    }

    /**
     * @param string $slug
     *
     * @return UsersDto
     */
    public function getUserBySlug($slug)
    {
        return $this->usersMapper->getUserBySlug($slug);
    }

    /**
     *
     * @param type $tempUserDataDto
     * inserts $tempUserDataDto field values
     * that correspond to user into userDto.
     *
     * @return userDto
     */
    public function insertTempUserDataIntoDto($tempUserData)
    {
        $userDto = $this->usersMapper->createDto();
        $userDto->setFirstName($tempUserData->getFirstName());
        $userDto->setLastName($tempUserData->getLastName());
        $userDto->setTitle($tempUserData->getTitle());
        $userDto->setPhone($tempUserData->getPhone());
        $userDto->setGender($tempUserData->getGender());
        $userDto->setEmail($tempUserData->getEmail());
        $userDto->setPasswordHash($tempUserData->getPasswordHash());
        $userDto->setPasswordHashSalt($tempUserData->getPasswordHashSalt());
        $userDto->setActivationCode($tempUserData->getActivationCode());
        //setting slug

        $title = $userDto->getTitle();
        if (!empty($title)) {
            $slug = UtilFunctions::createSlugFromString($title . ' ' . $userDto->getFirstName() . ' ' . $userDto->getLastName());
        } else {
            $slug = UtilFunctions::createSlugFromString($userDto->getFirstName() . ' ' . $userDto->getLastName());
        }
        $userDto->setSlug($slug);
        $userDto->setGroupId(GROUP_DOCTOR);
        $userDto->setMedicalSpecialtyIds($tempUserData->getMedicalSpecialtyIds());

        return $userDto;
    }

    /**
     * gets the user from user_edit_history table by id
     *
     * @param int type $editHistoryId
     *
     * @return object $userDto
     */
    public function getUserFromEditHistory($editHistoryId)
    {
        $usersEditHistoryManager = UsersEditHistoryManager::getInstance($this->config, $this->args);
        $userEditHistoryDto = $usersEditHistoryManager->getById($editHistoryId);
        $mapArray = $userEditHistoryDto->getMapArray();
        unset($mapArray['id']);
        unset($mapArray['user_id']);

        $userDto = $this->createDto();
        $userDto->setId($userEditHistoryDto->getUserId());
        foreach ($mapArray as $dtoFieldName) {
            $setFuncName = 'set' . ucfirst($dtoFieldName);
            $getFuncName = 'get' . ucfirst($dtoFieldName);
            $userDto->$setFuncName($userEditHistoryDto->$getFuncName());
        }

        return $userDto;
    }

    public function createDto()
    {
        return $this->usersMapper->createDto();
    }

    public function getUsersByStatus($status)
    {
        return $this->usersMapper->getUsersByStatus($status);
    }

    public function getUsersThatNotHaveTT($status)
    {
        return $this->usersMapper->getUsersThatNotHaveTT($status);
    }

    public function getAllDoctorsNoLimit()
    {
        return $this->usersMapper->getAllDoctorsNoLimit();
    }

    public function getAllDoctorNames()
    {
        return $this->usersMapper->getAllDoctorNames();
    }

    public function getUserWithLocationsById($userId, $locationId)
    {
        return $this->doctorsMapper->getUserWithLocationsById($userId, $locationId);
    }

    public function getUsersByIds(array $user_ids = array(), $force_order = false)
    {
        return $this->doctorsMapper->getUsersByIds($user_ids, $force_order);
    }

    /**
     * Catch-all update for setting user fields. Hacky but necessary, as the gateway paradigm is a total mess here.
     *
     * @param int $id_users
     * @param array $data Use the qualified/mapped names (not the original) as keys
     *
     * @return int
     */
    public function update($id_users, array $data)
    {
        if (!empty($id_users) && !empty($data)) {

            // For some reason we create a dto and then update
            if ($dto = $this->usersMapper->selectByPK(intval($id_users))) {
                foreach ($data as $key => $value) {
                    // Hacky as hell way to force values
                    $dto->$key = $value;
                }
            }

            if (empty($data['updatedAt'])) {
                $dto->setUpdatedAt(date('Y-m-d H:i:s'));
            }

            return $this->usersMapper->updateByPK($dto);
        }

        return -1;
    }
}