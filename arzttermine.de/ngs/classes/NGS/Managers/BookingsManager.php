<?php

namespace NGS\Managers;

use Arzttermine\Booking\Booking;
use Arzttermine\Core\Configuration;
use NGS\DAL\DTO\BookingsDto;
use NGS\DAL\Mappers\BookingsMapper;
use NGS\DAL\Mappers\MailingsMapper;
use NGS\Framework\AbstractManager;
use NGS\Util\MailSender;

class BookingsManager extends AbstractManager {

	/**
	 * @var self
	 */
	private static $instance = null;

	/**
	 * @param array $config
	 * @param array $args
	 */
	protected function __construct($config = array(), $args = array()) {
		$this->bookingsMapper = BookingsMapper::getInstance();
		$this->config = $config;
		$this->args = $args;
	}

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param int $id
     *
     * @return BookingsDto
     */
    public function getBookingById($id) {
		return $this->bookingsMapper->selectByPK($id);
	}

    public function getBookingBySalesforceId($salesforceId) {
        return $this->bookingsMapper->selectByField("salesforce_id", $salesforceId);
    }

	public function setIntegrationStatus($id, $integrationStatus) {
		return $this->bookingsMapper->updateNumericField($id, 'integration_status', $integrationStatus);
	}
	
	public function setBookingTreatmenttypeId($id, $tt_id) {
		return $this->bookingsMapper->updateNumericField($id, 'treatmenttype_id', $tt_id);
	}

    public function updateFields($id, $fieldsArr) {
        return $this->bookingsMapper->updateFields($id, $fieldsArr);
    }

    public function setSalesforceId($id, $salesforceId) {
        return $this->bookingsMapper->updateTextField($id, 'salesforce_id', $salesforceId);
    }

    public function deleteBySalesforceId($salesforceId) {
        return $this->bookingsMapper->deleteByCustomKey('salesforce_id', $salesforceId);
    }

	public function getAllBookingsCount($searchArray=false) {
		return $this->bookingsMapper->getAllBookingsCount($searchArray);
	}

	public function getIntegrationBookingsAfterGivenDatetime($integrationId, $datetime, $bookingStatuses) {
		return $this->bookingsMapper->getIntegrationBookingsAfterGivenDatetime($integrationId, $datetime, $bookingStatuses);
	}

	public function setStatus($id, $status) {
		return $this->bookingsMapper->updateNumericField($id, 'status', $status);
	}
	
	public function setAppointmentStartAt($id, $value) {
		return $this->bookingsMapper->updateTextField($id, 'appointment_start_at', $value);
	}
	
	public function setUserId($id, $value) {
		return $this->bookingsMapper->updateNumericField($id, 'user_id', $value);
	}
	
	public function setLocationId($id, $value) {
		return $this->bookingsMapper->updateNumericField($id, 'location_id', $value);
	}

    public function updateConfirmed($id, $bookingConfirmed) {
        $this->bookingsMapper->setCurrentTimestamp($id, 'updated_confirmed_at');
        return $this->bookingsMapper->updateNumericField($id, 'confirmed', $bookingConfirmed);
    }

	public function getBookingsFull($offset, $limit, $orderByFieldName, $ascending = true,$searchArray=false) {
		return $this->bookingsMapper->getBookingsFull($offset, $limit, $orderByFieldName, $ascending,$searchArray);
	}
	
	/**
	 * 
	 * @param string $email patient email
	 * @param string $appointmentStartAt Datetime of appointment start by this format 'Y-m-d H:i:s'
	 * @return $bookingDtos Returns similar bookings for patient
	 */
	public function getPatientSimilarBookings($email, $appointmentStartAt, $medicalSpecialtyId) {
		$docdir = false;
		if($appointmentStartAt=='docdir'){
			$docdir = true;
			$appointmentStartAt = date('Y-m-d H:i:s');
		}
		$similar_booking_within_days_number = Configuration::get('similar_booking_within_days_number');
		$searchdateRangeStart = date('Y-m-d', strtotime("-" . $similar_booking_within_days_number . " day", strtotime($appointmentStartAt)));
		if ($searchdateRangeStart < date('Y-m-d')) {
			$searchdateRangeStart = date('Y-m-d');
		}
		$searchdateRangeEnd = date('Y-m-d', strtotime("+" . $similar_booking_within_days_number . " day", strtotime($appointmentStartAt)));		
		return $this->bookingsMapper->getPatientSimilarBookings($email, $searchdateRangeStart, $searchdateRangeEnd,$medicalSpecialtyId, $docdir);
	}

	public function getBookingByIdFull($id) {
		return $this->bookingsMapper->getBookingByIdFull($id);
	}
	
	public function createMailingDto(){
		$mailingsMapper=MailingsMapper::getInstance();
		return $mailingsMapper->createDto();
	}
	
	public function sendMail($from, $recipients, $subject, $template, $params ,$mailingDto){
		$mailingsMapper=MailingsMapper::getInstance();
		$mailSender = new MailSender($this->config);
		$mailSent=$mailSender->send($from, $recipients, $subject, $template, $params);
		if($mailSent){
			$mailingsMapper->insertDto($mailingDto);
		}
		return $mailSent;
	}
	
    /**
     * 
     * @param type $userId
     * @param type $locationId
     * @param string $dateStart
     * @param string $dateEnd
     * @return type
     */
	  public function getBookedTimesByFilters($userId, $locationId, $dateStart, $dateEnd) {          
        $statuses = Booking::getBookedStatuses();
        $bookedTimes = $this->bookingsMapper->getBookingsStartAtByFilters($userId, $locationId, $statuses, $dateStart, $dateEnd);
        if (!empty($bookedTimes)) {
            return explode(',', $bookedTimes);
        } else {
            return array();
        }
    }

	public function updateBookingsByEmail($oldEmail, $newEmail){
		if($oldEmail==$newEmail){
			return null;
		}
		return $this->bookingsMapper->updateBookingsByEmail($oldEmail, $newEmail);
	}

    public function getBookingsMonthsByUserId($userId){
        $BookingsMonths =  $this->bookingsMapper->getBookingsMonthsByUserId($userId);
        $monthsArray= array();
        foreach ($BookingsMonths as $month) {
            $str_time = $month->getLastName().'/1/'.$month->getFirstName();
            array_push($monthsArray, date("M-Y",strtotime($str_time)));
        }
        return $monthsArray;
    }


	public function getBookingsWithTreatmentTypeInDateRangeByUserId($userId, $dateFrom, $dateTo, $orderBy="DESC"){
		$bookings = $this->bookingsMapper->getBookingsWithTreatmentTypeInDateRangeByUserId($userId, $dateFrom, $dateTo, $orderBy);
		return $bookings;
	}
	
	public function getBookingsByUserIdAndStatus($userId, $status){
		return $this->bookingsMapper->getBookingsByUserIdAndStatus($userId, $status);
	}
	
	public function getBookingsByLocationIdAndStatus($locationId, $status){
		return $this->bookingsMapper->getBookingsByLocationIdAndStatus($locationId, $status);
	}

	/**
     * Returns bookings by user_id.
     * If second parameter is false, returns user's all bookings.
     * If second parameter is 'upcoming', returns user's all upcoming bookings.
     * If second parameter is 'past', returns user's all past bookings.
     * @return
     */
	public function getAllBookingsCountByPatientId($patientId, $appointmentStartTime=false){
		return $this->bookingsMapper->getAllBookingsCountByPatientId($patientId, $appointmentStartTime); 
	}
	
	/**
	 * @param $location_id
	 * @param $month
	 *
	 * @return BookingsDto[]
	 */
	public function getUpcomingBookingsForLocation($location_id, $month) {
		$conditions = array();
		
		if (intval($location_id)) {
			$conditions[] = 'location_id = ' . intval($location_id);
		}
		
		if (!empty($month)) {
			$conditions[] = "appointment_start_at >= TIMESTAMP('" . date('Y.m.d', strtotime("first day of {$month}")) . "')";
			$conditions[] = "appointment_start_at <= TIMESTAMP('" . date('Y.m.d', strtotime("last day of {$month}" )) . "', '23:59:59')";
		}
		
		return $this->getBookings($conditions);
	}

	public function getBookingPeriodsAtLocation($location_id) {
		$periods = array();
		
		if ($result = $this->bookingsMapper->getBookingPeriodsAtLocation($location_id)) {
			$exclude = date('M-Y');
			foreach ($result as $period) {
				if (!($period['periods'] == $exclude)) {
					$periods[] = $period['periods'];
				}
			}
		}
		
		return $periods;
	}
	
	/**
	 * Catch-all
	 * 
	 * @param array $where
	 * @param array $select
	 * @param array $orderby
	 * @param int   $limit
	 * @param int   $offset
	 *
	 * @return BookingsDto[]
	 */
	public function getBookings(array $where = array(), array $select = array(), array $orderby = array(), $limit = 0, $offset = 0) {
		return $this->bookingsMapper->getBookings($where, $select, $orderby, $limit, $offset);
	}

}
