<?php

namespace NGS\Managers;

use NGS\DAL\Mappers\SamediNetworkMapper;
use NGS\Framework\AbstractManager;

class SamediNetworkManager extends AbstractManager {

    private $samediNetworkMapper;

    /**
     * @var singleton instance of class
     */
    private static $instance = null;

    /**
     * Initializes DB mappers
     *
     * @param object $config
     * @param object $args
     * @return
     */
    function __construct($config = array(), $args = array()) {
        $this->samediNetworkMapper = SamediNetworkMapper::getInstance();
        $this->config = $config;
        $this->args = $args;
    }

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function selectAll() {
        return $this->samediNetworkMapper->selectAll();
    }

    public function addRow($data) {
        $dto = $this->samediNetworkMapper->createDto();
        $dto->setData($data);         
        return $this->samediNetworkMapper->insertDto($dto);
    }

    
	public function truncateTable() {
        $this->samediNetworkMapper->truncateTable();
	}
}