<?php

namespace NGS\Managers;

use NGS\DAL\Mappers\LoginHistoryMapper;
use NGS\Framework\AbstractManager;

class LoginHistoryManager extends AbstractManager {

    /**
     * @var singleton instance of class
     */
    private static $instance = null;

	private $loginHistoryMapper;
    /**
     * Initializes DB mappers
     *
     * @param object $config
     * @param object $args
     * @return
     */
    function __construct($config = array(), $args = array()) {
        $this->loginHistoryMapper = LoginHistoryMapper::getInstance();
        $this->config = $config;
        $this->args = $args;
    }

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }
	
	public function insertRow($userId, $userType, $date, $ip, $os, $browserName, $browserVersion){
		return $this->loginHistoryMapper->insertValues(
				array('user_id','user_type','date','ip','operating_system', 'browser', 'browser_version'),
				array($userId, $userType, $date, $ip, $os, $browserName,$browserVersion));
	}

}