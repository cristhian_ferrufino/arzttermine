<?php

namespace NGS\Managers;
use Arzttermine\Application\Application;
use NGS\DAL\DTO\TreatmentTypesDto;
use NGS\DAL\Mappers\TreatmentTypesMapper;
use NGS\Framework\AbstractManager;

class TreatmentTypesManager extends AbstractManager {

    /**
     * @var singleton instance of class
     */
    private static $instance = null;
    private $treatmentTypesMapper;

    /**
     * Initializes DB mappers
     *
     * @param object $config
     * @param object $args
     * @return
     */
    function __construct($config = array(), $args = array()) {
        $this->treatmentTypesMapper = TreatmentTypesMapper::getInstance();
        $this->config = $config;
        $this->args = $args;
    }

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param $id
     *
     * @return TreatmentTypesDto
     */
    public function getById($id) {
        return $this->treatmentTypesMapper->selectByPK($id);
    }

    public function getByIds($ids_array) {
        return $this->treatmentTypesMapper->getByIds($ids_array);
    }

    /**
     * @param $fieldName
     * @param $fieldValue
     *
     * @return TreatmentTypesDto
     */
    public function getByFieldName($fieldName, $fieldValue) {
        return $this->treatmentTypesMapper->selectByField($fieldName, $fieldValue);
    }
    
    public function getNamesByIds($ids_array, $langCode = 'de') {
        return $this->treatmentTypesMapper->getNamesByIds($ids_array, $langCode);
    }
    
	public function selectAll() {
		return $this->treatmentTypesMapper->selectAll();
	}

    public function getDoctorAllTreatmentTypes($doctorId){
        return $this->treatmentTypesMapper->getDoctorAllTreatmentTypes($doctorId);
    }

	public function addRow($status, $slug, $name_en, $name_de, $medSpecId, $createdBy) {
		$dto = $this->treatmentTypesMapper->createDto();
		$dto->setSlug($slug);
		$dto->setStatus($status);
		$dto->setNameEn($name_en);
		$dto->setNameDe($name_de);
		$dto->setMedicalSpecialtyId($medSpecId);
		$dto->setCreatedAt(date('Y-m-d H:i:s'));
		$dto->setCreatedBy($createdBy);
		return $this->treatmentTypesMapper->insertDto($dto);
	}

}