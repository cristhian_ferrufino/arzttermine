<?php

namespace NGS\Managers;

use Arzttermine\Booking\Appointment;
use Arzttermine\Calendar\Calendar;
use Arzttermine\User\User;
use NGS\DAL\DTO\UsersAaDto;
use NGS\DAL\Mappers\UsersAaMapper;
use NGS\Framework\AbstractManager;

class UsersAaManager extends AbstractManager
{
    /**
     * @var self
     */
    private static $instance = null;

    /**
     * @var UsersAaMapper
     */
    private $usersAaMapper;

    /**
     * @var BookingsManager
     */
    private $bookingsManager;

    /**
     * @param array $config
     * @param array $args
     */
    protected function __construct($config = array(), $args = array())
    {
        $this->usersAaMapper   = UsersAaMapper::getInstance();
        $this->config          = $config;
        $this->args            = $args;
        $this->bookingsManager = BookingsManager::getInstance($config, $args);
    }

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param $dto
     *
     * @return int
     */
    public function addRow($dto)
    {
        return $this->usersAaMapper->insertDto($dto);
    }

    /**
     * @param mixed $dtos
     * @param string $preSql
     * @param string $postSql
     *
     * @return bool
     */
    public function addRows($dtos, $preSql = "", $postSql = "")
    {
        return $this->usersAaMapper->insertDtos($dtos, true, $preSql, $postSql);
    }

    /**
     * @param mixed $dto
     * @param string $startDate
     * @param string $tillDate
     * @param bool   $returnOnlyDtos
     *
     * @return array
     */
    public function addDoctorFixedWeekAppointment($dto, $startDate = '', $tillDate = '', $returnOnlyDtos = false)
    {
        if (empty($dto)) {
            return array();
        }

        $baseDate = $dto->getDate();
        $weekday = date('w', strtotime($baseDate)); //0 sunday, 1 monday, ... , 6 saturday
        $baseDate = $this->getFirstComingDateByWeekday($weekday);
        $fillFutureWeeksCount = intval($GLOBALS['CONFIG']['CALENDAR_MAX_DAYS_AHEAD'] / 7);
        if ($returnOnlyDtos) {
            $ret = array();
        }
        for ($weekNumber = 0; $weekNumber <= $fillFutureWeeksCount; $weekNumber++) {
            $currentWeekDate = $this->getFuturWeekDateByWeeksNumber($baseDate, $weekNumber);
            if (!empty($tillDate) && !empty($startDate) && ($currentWeekDate < $startDate || $currentWeekDate > $tillDate)) {
                continue;
            }
            $dto->setDate($currentWeekDate);
            if ($returnOnlyDtos) {
                $dto->setId('');
                $ret [] = clone $dto;
            } else {
                $dto->setId('');
                $this->addRow($dto);
            }
        }
        if ($returnOnlyDtos) {
            return $ret;
        }

        return array();
    }

    /**
     * @param string $date
     * @param int $weeksNumber
     *
     * @return bool|string
     */
    private static function getFuturWeekDateByWeeksNumber($date, $weeksNumber)
    {
        if ($weeksNumber == 0) {
            return $date;
        }
        return date('Y-m-d', strtotime("+$weeksNumber week", strtotime($date)));
    }

    /**
     * @param $weekday
     *
     * @return bool|string
     */
    public static function getFirstComingDateByWeekday($weekday)
    {
        switch ($weekday) {
            case 1:
                $dayName = 'monday';
                break;
            case 2:
                $dayName = 'tuesday';
                break;
            case 3:
                $dayName = 'wednesday';
                break;
            case 4:
                $dayName = 'thursday';
                break;
            case 5:
                $dayName = 'friday';
                break;
        }

        return date('Y-m-d', strtotime("next " . $dayName, strtotime("yesterday")));
    }

    /**
     * @return UsersAaDto
     */
    public function createEmptyDto()
    {
        return $this->usersAaMapper->createDto();
    }

    /**
     * @param string $dateStart
     * @param string $dateEnd
     * @param int $user_id
     * @param int $location_id
     * @param string $insurance
     * @param string $treatmentTypeIds
     *
     * @return array
     */
    public function getAvailableAppointments($dateStart, $dateEnd, $user_id, $location_id, $insurance = null, $treatmentTypeIds = null)
    {
        $appointments = array();
        $rows = $this->getAvailableAppointmentsNotExtracted($dateStart, $dateEnd, $user_id, $location_id, $insurance, $treatmentTypeIds);

        foreach ($rows as $row) {
            $rowAppointments = $this->extractRowToAppointments($row);
            if (isset($appointments[$row->getDate()])) {
                $appointments[$row->getDate()] = array_merge($appointments[$row->getDate()], $rowAppointments);
            } else {
                $appointments[$row->getDate()] = $rowAppointments;
            }
        }

        $user = new User($user_id);
        if (!$user->getFixedWeek()) {
            $appointments = $this->subtractBookedAppointmentsFromAppointments(
                $appointments, $dateStart, $dateEnd, $user_id, $location_id
            );
        }

        return $appointments;
    }

    /**
     * @param string $dateStart
     * @param string $dateEnd
     * @param int $doctorId
     * @param array $locationIdsArray
     * @param string $insurance
     * @param string $treatmentTypeIds
     *
     * @return UsersAaDto[]
     */
    public function getAvailableAppointmentsNotExtracted($dateStart, $dateEnd, $doctorId, $locationIdsArray, $insurance = null, $treatmentTypeIds = null)
    {
        $insuranceType = $this->getInsuranceTypeByIndex($insurance);

        if (is_array($locationIdsArray)) {
            $locationIdsString = implode(',', $locationIdsArray);
        } else {
            $locationIdsString = $locationIdsArray;
        }
        
        $dtos = $this->usersAaMapper->getByFilters($dateStart, $dateEnd, $doctorId, $locationIdsString, $insuranceType, $treatmentTypeIds);
        
        return $dtos;
    }

    /**
     * @param int $insuranceIndex
     *
     * @return string
     */
    public function getInsuranceTypeByIndex($insuranceIndex)
    {
        switch ($insuranceIndex) {
            case 1:
                return 'private';
            case 2:
                return 'public';
            case 3:
                return 'cash';
        }
        
        return '';
    }

    /**
     * @param UsersAaDto
     */
    public function updateByPK($dto)
    {
        $this->usersAaMapper->updateByPK($dto);
    }

    /**
     * @param int
     * 
     * @return UsersAaDto[]
     */
    public function getByUserId($userId)
    {
        return $this->usersAaMapper->getByUserId($userId);
    }

    /**
     * @param string $insuranceType
     *
     * @return int
     */
    public function getInsuranceIndexByType($insuranceType)
    {
        switch ($insuranceType) {
            case 'private':
                return 1;
            case 'public':
                return 2;
            case 'cash':
                return 3;
        }
        
        return 0;
    }

    /**
     * Arzttermin integration doctors are inputting their available times in following format.
     * date, startTime, endTime, BlockInMinuts
     * That's why every inputted row in the table can be extracted to many appointments,
     * depending srartTime, EndTime and BlockInMinutes values.
     * This function extract that table row to many appointments and returns array of appointments.
     *
     * @param mixed $rowDto
     *
     * @return array of Appointment.
     */
    private function extractRowToAppointments($rowDto)
    {
        $ret                 = array();
        $index               = 0;
        $startAt             = $rowDto->getStartAt();
        $endAt               = $rowDto->getEndAt();
        $blockInMinutes      = intval($rowDto->getBlockInMinutes());
        $startAtSeconds      = Calendar::time2Seconds($startAt);
        $endAtSeconds        = Calendar::time2Seconds($endAt);
        $treatmetTypeDtos    = $rowDto->getTreatmentTypeDtos();
        $insuranceTypesArray = explode(",", $rowDto->getInsuranceTypes());
        $insuranceIdsArray   = array();
        
        foreach ($insuranceTypesArray as $insuranceType) {
            $insuranceIdsArray[] = $this->getInsuranceIndexByType($insuranceType);
        }
        $insuranceIds = implode(',', $insuranceIdsArray);
        if ($blockInMinutes == 0) {
            return array();
        }
        
        while ($startAtSeconds + $index * $blockInMinutes * 60 < $endAtSeconds) {
            $timeStamp = ($startAtSeconds + ($index * $blockInMinutes * 60));
            $dateTime  = strtotime($rowDto->getDate()) + $timeStamp;
            // IMPORTANT: here we use the insuranceIds STRING, not array
            $apTs = new Appointment(date('Y-m-d H:i:s', $dateTime), null, $treatmetTypeDtos);
            $apTs->setInsuranceIds($insuranceIds);
            $ret[] = $apTs;
            $index++;
        }
        return $ret;
    }

    /**
     * @param array $appointments
     * @param string $dateStart
     * @param string $dateEnd
     * @param int $doctorId
     * @param int $locationId
     *
     * @return mixed
     */
    private function subtractBookedAppointmentsFromAppointments($appointments, $dateStart, $dateEnd, $doctorId, $locationId)
    {
        $bookingsTimesInDateRange = $this->bookingsManager->getBookedTimesByFilters($doctorId, $locationId, $dateStart, $dateEnd);
        foreach ($bookingsTimesInDateRange as $bookingTimeDate) {
            list($dateToBeunset, $indexToBeUnset) = $this->findAppointmentByDate($appointments, $bookingTimeDate);
            if ($indexToBeUnset >= 0) {
                unset($appointments[$dateToBeunset][$indexToBeUnset]);
            }
        }
        return $appointments;
    }

    /**
     * @param array $appointmentsArray
     * @param string $searchDateTime
     *
     * @return array|int
     */
    private function findAppointmentByDate($appointmentsArray, $searchDateTime)
    {
        foreach ($appointmentsArray as $date => $appointments) {
            foreach ($appointments as $index => $appointment) {
                $appointmentdateTime = $appointment->getDateTime();
                if ($appointmentdateTime == $searchDateTime) {
                    return array($date, $index);
                }
            }
        }
        return -1;
    }

    /**
     * @param int $id
     * @param mixed $value
     *
     * @return int
     */
    public function setInsuranceTypesByPK($id, $value)
    {
        return $this->usersAaMapper->updateTextField($id, 'insurance_types', $value);
    }

    /**
     * @param int $id
     * @param mixed $value
     *
     * @return int
     */
    public function setTreatmentTypeIdsByPK($id, $value)
    {
        return $this->usersAaMapper->updateTextField($id, 'treatment_type_ids', $value);
    }

    /**
     * @return UsersAaDto
     */
    public function createDto()
    {
        return $this->usersAaMapper->createDto();
    }

    /**
     * @param int $userId
     * @param bool $returnOnlySql
     *
     * @return mixed
     */
    public function deleteUserAllAvaialableTimesByUserIdAndLocationId($userId, $returnOnlySql = false)
    {
        return $this->usersAaMapper->deleteUserAllAvaialableTimesByUserIdAndLocationId($userId, $returnOnlySql);
    }

    /**
     * @param int $userId
     * @param string $dateStart
     * @param string $dateEnd
     * @param bool $returnOnlySql
     *
     * @return mixed
     */
    public function deleteUserAllAvaialableTimesByUserIdAndLocationIdAndDateRage($userId, $dateStart, $dateEnd, $returnOnlySql = false)
    {
        return $this->usersAaMapper->deleteUserAllAvaialableTimesByUserIdAndLocationIdAndDateRage(
            $userId, $dateStart, $dateEnd, $returnOnlySql
        );
    }

    /**
     * @param int    $docId
     * @param string $appDateTime
     *
     * @return UsersAaDto Doctor's appointment row in which the $appTime is included
     */
    public function getByDoctorIdAndAppointmentTime($docId, $appDateTime)
    {
        return $this->usersAaMapper->getByDoctorIdAndAppointmentTime($docId, $appDateTime);
    }

    /**
     * @param int $id
     * @param string $startAt
     */
    public function setStartAtByPK($id, $startAt)
    {
        $this->usersAaMapper->updateTextField($id, 'start_at', $startAt);
    }

    /**
     * @param int $id
     * @param string $endAt
     */
    public function setEndAtByPK($id, $endAt)
    {
        $this->usersAaMapper->updateTextField($id, 'end_at', $endAt);
    }

    /**
     * @param int $id
     */
    public function deleteRowByPK($id)
    {
        $this->usersAaMapper->deleteByPK($id);
    }

}