<?php

namespace NGS\Managers;

use NGS\DAL\Mappers\SamediPracticeCategoriesMapper;
use NGS\Framework\AbstractManager;

class SamediPracticeCategoriesManager extends AbstractManager {
    
    private $samediPracticeCategoriesMapper;

    /**
     * @var singleton instance of class
     */
    private static $instance = null;

    /**
     * Initializes DB mappers
     *
     * @param object $config
     * @param object $args
     * @return
     */
    function __construct($config = array(), $args = array()) {
        $this->samediPracticeCategoriesMapper = SamediPracticeCategoriesMapper::getInstance();
        $this->config = $config;
        $this->args = $args;
    }

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function selectAll() {
        return $this->samediPracticeCategoriesMapper->selectAll();
    }

    public function getPracticeCategoriesBySamediPracticeId($samediPracticeId) {
        return $this->samediPracticeCategoriesMapper->getPracticeCategoriesBySamediPracticeId($samediPracticeId);
    }

    public function addRow($samediPracticeId, $data) {
        $dto = $this->samediPracticeCategoriesMapper->createDto();
        $dto->setData($data);
        $dto->setSamediPracticeId($samediPracticeId);
        return $this->samediPracticeCategoriesMapper->insertDto($dto);
    }

    public function truncateTable() {
        $this->samediPracticeCategoriesMapper->truncateTable();
    }

}