<?php

namespace NGS\Managers;

use NGS\DAL\Mappers\Integration1DatasMapper;
use NGS\Framework\AbstractManager;

class Integration1DatasManager extends AbstractManager {

    private $integration1DatasMapper;

    /**
     * @var singleton instance of class
     */
    private static $instance = null;

    /**
     * Initializes DB mappers
     *
     * @param object $config
     * @param object $args
     * @return
     */
    function __construct($config = array(), $args = array()) {
        $this->integration1DatasMapper = Integration1DatasMapper::getInstance();
        $this->config = $config;
        $this->args = $args;
    }

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function selectAll(){
        return  $this->integration1DatasMapper->selectAll();
    }
    
    public function addAvailableAppointment($integration1DataDto){
        return $this->integration1DatasMapper->insertDto($integration1DataDto);
    }
    
    public function createDto(){
        return $this->integration1DatasMapper->createDto();
    }
}