<?php

namespace NGS\Managers;

use Arzttermine\Media\Asset;
use NGS\DAL\DTO\AssetsDto;
use NGS\DAL\Mappers\AssetsMapper;
use NGS\Framework\AbstractManager;

class AssetsManager extends AbstractManager {

	private $assetsMapper;

	/**
	 * @var singleton instance of class
	 */
	private static $instance = null;

	/**
	 * Initializes DB mappers
	 *
	 * @param object $config
	 * @param object $args
	 * @return
	 */
	function __construct($config = array(), $args = array()) {
		$this->assetsMapper = AssetsMapper::getInstance();
		$this->config = $config;
		$this->args = $args;
	}

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

	/**
	 * @param int
	 * @return AssetsDto
	 */
	public function getById($id) {
		return $this->assetsMapper->selectByPK($id);
	}

	/**
	 * @param $userId
	 *
	 * @return AssetsDto
	 */
	public function getProfileAsset($userId) {
		return $this->assetsMapper->getProfileAsset($userId);
	}

	public function getProfileChildAssets($parentId) {
		$ret = $this->assetsMapper->getProfileChildAssets($parentId);
		if ($ret && is_array($ret)) {
			return $this->putArrayKeyToAssetSize($ret);
		}
		return null;
	}

	// @todo: WARNING - potential overwrite of data here if asset size is identical
	private function putArrayKeyToAssetSize($assets) {
		$ret = array();
		foreach ($assets as $asset) {
			$ret[$asset->getSize()] = $asset;
		}
		return $ret;
	}
    
    public function saveAsset($ownerType, $ownerId, $filesUpload, $saveFromLocalFile=false){
		$asset = new Asset();
        $data = array();
        $data['gallery_id']=0;
        return $asset->saveNewParent($ownerType, $ownerId, $data, $filesUpload, $saveFromLocalFile);
    }
    
    public function getProfileAssets($userId) {
        return $this->assetsMapper->getProfileAssets($userId);
    }
    
    public function deleteAssetById($id){
        $asset = new Asset($id);
        $asset->deleteWithChildren();
    }
    
    public function setCreatedAt($assetId){
        $this->assetsMapper->updateTextField($assetId,'created_at',date("Y-m-d H:i:s"));
    }
	
	public function getLocationProfile($id) {
		return $this->assetsMapper->getAssets($id, 0, ASSET_OWNERTYPE_LOCATION, 1);
	}
}