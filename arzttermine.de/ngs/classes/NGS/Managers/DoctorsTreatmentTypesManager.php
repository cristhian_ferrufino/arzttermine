<?php

namespace NGS\Managers;

use NGS\DAL\Mappers\DoctorsTreatmentTypesMapper;
use NGS\Framework\AbstractManager;

class DoctorsTreatmentTypesManager extends AbstractManager {

    private $doctorsTreatmentTypesMapper;

    /**
     * @var singleton instance of class
     */
    private static $instance = null;

    /**
     * Initializes DB mappers
     *
     * @param object $config
     * @param object $args
     * @return
     */
    function __construct($config = array(), $args = array()) {
        $this->doctorsTreatmentTypesMapper = DoctorsTreatmentTypesMapper::getInstance();
        $this->config = $config;
        $this->args = $args;
    }

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

	public function addTreatmentType($doctorId, $medicalSpecialityId, $treatmentTypeId) {
		$dtos = $this->doctorsTreatmentTypesMapper->getByUserIdMedSpecIdTTId($doctorId, $medicalSpecialityId, $treatmentTypeId);
		if (empty($dtos)) {
			$this->doctorsTreatmentTypesMapper->insertValues(array("doctor_id", "medical_specialty_id", "treatment_type_id"), array($doctorId, $medicalSpecialityId, $treatmentTypeId));
			return true;
		}
		return false;
	}

	public function addTreatmentTypes($doctorIds, $medicalSpecialtyIds, $treatmentTypeIds) {
		return $this->doctorsTreatmentTypesMapper->addTreatmentTypes($doctorIds, $medicalSpecialtyIds, $treatmentTypeIds);
	}
	
	public function addTreatmentTypesForDoctor($doctorId, $treatmentTypeIds) {
		return $this->doctorsTreatmentTypesMapper->addTreatmentTypesForDoctor($doctorId, $treatmentTypeIds);
	}

    public function deleteDoctorAllTreatmentTypes($doctorId) {
        $this->doctorsTreatmentTypesMapper->deleteDoctorAllTreatmentTypes($doctorId);
    }

    public function getByDoctorIdAndMedSpecId($doctorId, $medSpecId) {        
        return $this->doctorsTreatmentTypesMapper->getByDoctorIdAndMedSpecId($doctorId, $medSpecId);
    }

    public function getTreatmentTypesIdsArrayByDoctorIdAndMedSpecId($doctorId, $medSpecId) {
        
        $dtos = $this->getByDoctorIdAndMedSpecId($doctorId, $medSpecId);
        
        $ret = array();
        foreach ($dtos as $dto) {
            $ret [] = $dto->getTreatmentTypeId();
        }
        return $ret;
    }
    
    public function getByDoctorId($doctorId){
        return $this->doctorsTreatmentTypesMapper->getByDoctorId($doctorId); 
    }

    public function getTtIdsByDoctorId($doctorId) {
        return $this->doctorsTreatmentTypesMapper->getTtIdsByDoctorId($doctorId);
    }

    public function getMedSpecIdsArrayWithExistingTreatmentTypesByDoctorId($doctorId) {
        $dtos = $this->getByDoctorId($doctorId);
        $ret=array();
        foreach ($dtos as $dto) {
            $ret[$dto->getMedicalSpecialityId()][]=$dto->getTreatmentTypeId();
        }
        return $ret;
    }
    
    public function getDoctorAllTreatmentTypes($doctorId){
        return $this->doctorsTreatmentTypesMapper->getDoctorAllTreatmentTypes($doctorId);
    }
    
    public function deleteDoctorTreatmentTypeByMedSpecId($doctorId,$medicalSpecialtyId){
        return $this->doctorsTreatmentTypesMapper->deleteDoctorTreatmentTypeByMedSpecId($doctorId,$medicalSpecialtyId);
    }

    public function getDoctorTreatmentTypesIds($doctorId) {
        $ret = array();
        $medSpecIdsArrayWithExistingTreatmentTypes =  $this->getMedSpecIdsArrayWithExistingTreatmentTypesByDoctorId($doctorId);
        foreach ($medSpecIdsArrayWithExistingTreatmentTypes  as $ttIdsArray) {
            $ret = array_merge($ret, $ttIdsArray);
        }
        return array_unique($ret);
    }
}