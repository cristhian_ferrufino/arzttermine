<?php

namespace NGS\Managers;
use NGS\DAL\Mappers\GroupsMapper;
use NGS\Framework\AbstractManager;

class GroupsManager extends AbstractManager {

	private $groupsMapper;

	/**
	 * @var singleton instance of class
	 */
	private static $instance = null;

	/**
	 * Initializes DB mappers
	 *
	 * @param object $config
	 * @param object $args
	 * @return
	 */
	function __construct($config = array(), $args = array()) {
		$this->groupsMapper = GroupsMapper::getInstance();
		$this->config = $config;
		$this->args = $args;
	}

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

	public function getAllGroupsJoinedUsers($offset, $limit, $orderByFieldName) {
		return $this->groupsMapper->getAllGroupsJoinedUsers($offset, $limit, $orderByFieldName);
	}

	public function getAllGroupsCount() {
		return $this->groupsMapper->getAllGroupsCount();
	}

}