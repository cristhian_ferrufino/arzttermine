<?php

namespace NGS\Managers;

use NGS\DAL\Mappers\SamediPatientsConnectionsMapper;
use NGS\Framework\AbstractManager;

class SamediPatientsConnectionsManager extends AbstractManager {

	private $samediPatientsConnectionsMapper;

	/**
	 * @var singleton instance of class
	 */
	private static $instance = null;

	/**
	 * Initializes DB mappers
	 *
	 * @param object $config
	 * @param object $args
	 * @return
	 */
	function __construct($config = array(), $args = array()) {
		$this->patientsMapper = SamediPatientsConnectionsMapper::getInstance();
		$this->config = $config;
		$this->args = $args;
	}

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

	public function getSamediPatientIdByPatientId($patientId) {
		$dto = $this->samediPatientsConnectionsMapper->selectByPK($patientId);
		return $dto->getSamediPatientId();
	}

}