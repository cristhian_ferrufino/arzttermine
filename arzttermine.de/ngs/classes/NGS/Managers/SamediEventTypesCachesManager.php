<?php

namespace NGS\Managers;

use NGS\DAL\DTO\SamediEventTypesCachesDto;
use NGS\DAL\Mappers\SamediEventTypesCachesMapper;
use NGS\Framework\AbstractManager;
use Arzttermine\Integration\Integration\Samedi\Types\CategoryEventType;

class SamediEventTypesCachesManager extends AbstractManager {

    /**
     * @var singleton instance of class
     */
    private static $instance = null;

    private $samediEventTypesCachesMapper;

    /**
     * Initializes DB mappers
     *
     * @param object $config
     * @param object $args
     *
     * @return
     */
    function __construct($config = array(), $args = array())
    {
        $this->samediEventTypesCachesMapper = SamediEventTypesCachesMapper::getInstance();
        $this->config = $config;
        $this->args = $args;
    }

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function selectAll()
    {
        return $this->samediEventTypesCachesMapper->selectAll();
    }

    /**
     * @param $practiceId
     * @param $categoryId
     *
     * @return CategoryEventType[]
     */
    public function getCategoryEventTypes($practiceId, $categoryId)
    {
        $eventTypesDto = $this->samediEventTypesCachesMapper->getCategoryEventTypes($practiceId, $categoryId);
        $eventTypes = null;
        if (isset($eventTypesDto)) {
            $etjson = $eventTypesDto->getEventTypes();
            $eventTypes = json_decode($etjson);
        }

        return $eventTypes;
    }

    public function setCatagoryEventTypes($practiceId, $categoryId, $eventTypes)
    {

        $eventTypesDto = $this->samediEventTypesCachesMapper->getCategoryEventTypes($practiceId, $categoryId);
        $update = true;
        if (!isset($eventTypesDto)) {
            $update = false;
            $eventTypesDto = $this->samediEventTypesCachesMapper->createDto();
        }
        $eventTypes = json_encode($eventTypes);
        $eventTypesDto->setPracticeId($practiceId);
        $eventTypesDto->setCategoryId($categoryId);
        $eventTypesDto->setEventTypes($eventTypes);
        $eventTypesDto->setCreatedDate(date('Y-m-d H:i:s'));
        if ($update) {
            return $this->samediEventTypesCachesMapper->updateByPk($eventTypesDto);
        } else {
            return $this->samediEventTypesCachesMapper->insertDto($eventTypesDto);
        }
    }

    public function createSamediEventTypesDtoFromPracticeIdAndCategoryIdAndEventTypes($practiceId, $categoryId, $eventTypes)
    {
        $dto = $this->samediEventTypesCachesMapper->createDto();
        $dto->setPracticeId($practiceId);
        $dto->setCategoryId($categoryId);
        $dto->setEventTypes(json_encode($eventTypes));

        return $dto;
    }
}