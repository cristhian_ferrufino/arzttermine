<?php

namespace NGS\Managers;
use NGS\DAL\Mappers\MedicalSpecialtiesMapper;
use NGS\Framework\AbstractManager;

class MedicalSpecialtiesManager extends AbstractManager {

    /**
     * @var singleton instance of class
     */
    private static $instance = null;

    /**
     * Initializes DB mappers
     *
     * @param object $config
     * @param object $args
     * @return
     */
    function __construct($config = array(), $args = array()) {
        $this->medicalSpecialtiesMapper = MedicalSpecialtiesMapper::getInstance();
        $this->config = $config;
        $this->args = $args;
    }

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }
    
    public function getAllMedicalSpecialtiesWithTreatmentTypes(){
        return $this->medicalSpecialtiesMapper->getAllMedicalSpecialtiesWithTreatmentTypes();
    }
    
    public function getMedicalSpecialtyWithTreatmentTypesById($id){
        return $this->medicalSpecialtiesMapper->getMedicalSpecialtyWithTreatmentTypesById($id);
    }
    
    public function getMedicalSpecialtiesByDoctorId($doctorId){
        return $this->medicalSpecialtiesMapper->getMedicalSpecialtiesByDoctorId($doctorId);       
    }
    
	public function getById($id){
        return $this->medicalSpecialtiesMapper->selectByPK($id);       
    }
    
    public function getMedicalSpecialityIdsText($medSpecIds, $lanCode = 'de', $delimiter = ', ', $singular=true)
    {
        return $this->medicalSpecialtiesMapper->getMedicalSpecialityIdsText($medSpecIds, $lanCode , $delimiter , $singular);
    }
    
    public function getMedicalSpecialityIdsLis($medSpecIds, $lanCode = 'de', $singular=true)
    {        
        $medicalSpecialityIdsText = $this->medicalSpecialtiesMapper->getMedicalSpecialityIdsText($medSpecIds, $lanCode , "^", $singular);
        $medicalSpecialityIdsText  = explode("^", $medicalSpecialityIdsText  );        
        $retHTML = "";
        foreach ($medicalSpecialityIdsText as $mText) {
        $retHTML .=     "<li>" .$mText."</li>";
        }
        return $retHTML ;
    }
    
    public function getByIds($medSpecIdsString){
        return $this->medicalSpecialtiesMapper->getByIds($medSpecIdsString);
    }
	
	public function getAllMedicalSpecialties(){
		return $this->medicalSpecialtiesMapper->selectAll();
	}

}