<?php

namespace NGS\Managers;

use NGS\DAL\Mappers\BlackListMapper;
use NGS\Framework\AbstractManager;

class BlackListManager extends AbstractManager {

	/**
	 * @var singleton instance of class
	 */
	private static $instance = null;

	/**
	 * Initializes DB mappers
	 *
	 * @return
	 */
	function __construct() {
		$this->cmsVersionsMapper = BlackListMapper::getInstance();
	}

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

	public function getPatientBlackListedMessage($email, $phoneNumber, $ip) {
		$blackListDtoByEmail = $this->cmsVersionsMapper->getByEmail($email);
		if (!empty($blackListDtoByEmail)) {	
			//var_dump('email');
			return $blackListDtoByEmail->getMessageDe();
		}

		$blackListDtoByPhone = $this->cmsVersionsMapper->getByPhoneNumber($phoneNumber);
		if (!empty($blackListDtoByPhone)) {
			//var_dump('phone');
			return $blackListDtoByPhone->getMessageDe();
		}

		if (!empty($ip)) {
			$blackListDtoByIp = $this->cmsVersionsMapper->getByIp($ip);
			if (!empty($blackListDtoByIp)) {
				//var_dump('ip');
				return $blackListDtoByIp->getMessageDe();
			}
		}
		return false;
	}

}