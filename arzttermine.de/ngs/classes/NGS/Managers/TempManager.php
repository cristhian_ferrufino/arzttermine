<?php

namespace NGS\Managers;

use NGS\DAL\Mappers\TempMapper;
use NGS\Framework\AbstractManager;

class TempManager extends AbstractManager {

	private $tempMapper;

	/**
	 * @var singleton instance of class
	 */
	private static $instance = null;

	/**
	 * Initializes DB mappers
	 *
	 * @param object $config
	 * @param object $args
	 * @return
	 */
	function __construct($config = array(), $args = array()) {
		$this->tempMapper = TempMapper::getInstance();
		$this->config = $config;
		$this->args = $args;
	}

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }
	
	public function selectAll()
	{
		return $this->tempMapper->selectAll();
	}

}