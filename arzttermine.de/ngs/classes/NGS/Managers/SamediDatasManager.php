<?php

namespace NGS\Managers;

use NGS\DAL\Mappers\SamediDatasMapper;
use NGS\Framework\AbstractManager;
use Arzttermine\Integration\Integration\Samedi\Types\SPractice;
use Arzttermine\Integration\Integration\Samedi\Types\SPracticeCategory;

class SamediDatasManager extends AbstractManager {

    /**
     * @var self
     */
    private static $instance = null;

    private $samediDatasMapper;

    /**
     * @param array $config
     * @param array $args
     */
    function __construct($config = array(), $args = array())
    {
        $this->samediDatasMapper = SamediDatasMapper::getInstance();
        $this->config = $config;
        $this->args = $args;
    }

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function getSamediDataById($id)
    {
        return $this->samediDatasMapper->selectByPK($id);
    }

    public function getAllSamediDatasJoinWithUsersAndLocations($offset, $limit, $orderByFieldName)
    {
        return $this->samediDatasMapper->getAllSamediDatasJoinWithUsersAndLocations($offset, $limit, $orderByFieldName);
    }

    public function getAllSamediDatas()
    {
        return $this->samediDatasMapper->getAllSamediDatas();
    }

    public function getByUserIdAndLocationId($user_id, $location_id)
    {
        return $this->samediDatasMapper->getByUserIdAndLocationId($user_id, $location_id);
    }

    /**
     * @param $category_id
     * @param $practice_id
     *
     * @return \NGS\DAL\DTO\SamediDatasDto
     */
    public function getByCategoryIdAndPracticeId($category_id, $practice_id)
    {
        return $this->samediDatasMapper->getByCategoryIdAndPracticeId($category_id, $practice_id);
    }

    public function getAllSamediDatasCount($searchArray = false)
    {
        return $this->samediDatasMapper->getAllSamediDatasCount($searchArray);
    }

    public function createSamediDataDtoFromSPracticeAndSPracticeCategory(SPractice $sPractice, SPracticeCategory $sPracticeCategory)
    {
        $dto = $this->samediDatasMapper->createDto();
        $dto->setPracticeId($sPractice->getId());
        $dto->setCategoryId($sPracticeCategory->getId());

        return $dto;
    }
}