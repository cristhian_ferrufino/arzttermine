<?php

namespace NGS\Managers;

use Arzttermine\Application\Application;
use NGS\DAL\Mappers\TerminlandBookingsMapper;
use NGS\Framework\AbstractManager;

class TerminlandBookingsManager extends AbstractManager {

	/**
	 * @var singleton instance of class
	 */
	private static $instance = null;

	private $terminlandBookingsMapper;

	/**
	 * Initializes DB mappers
	 *
	 * @param object $config
	 * @param object $args
	 * @return
	 */
	function __construct($config = array(), $args = array()) {
		$this->terminlandBookingsMapper = TerminlandBookingsMapper::getInstance();
		$this->config = $config;
		$this->args = $args;
	}

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

	public function getById($id) {
		return $this->terminlandBookingsMapper->selectByPK($id);
	}

	public function getByBookingId($bookingId) {
		return $this->terminlandBookingsMapper->getByBookingId($bookingId);
	}

	public function setBookingStatus($id, $status) {
		return $this->terminlandBookingsMapper->updateNumericField($id, 'status', $status);
	}

	public function addCancelBooking($status) {
        $profile = Application::getInstance()->getCurrentUser();

		$dto = $this->terminlandBookingsMapper->createDto();
		$dto->setFunctionId(\Arzttermine\Integration\Integration\Terminland\Booking::FUNCTION_CANCEL);
		$dto->setStatus($status);
		$dto->setIntegrationId(2);
		$dto->setLogText("Terminland->cancelBooking();\n");
		$dto->setCreatedAt(date('Y-m-d H:i:s'));
		$dto->setCreatedBy($profile->getId());
		return $this->terminlandBookingsMapper->insertDto($dto);
	}

}