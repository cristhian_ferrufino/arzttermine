<?php

namespace NGS\Managers;

use NGS\DAL\Mappers\LocationUserConnectionsMapper;
use NGS\Framework\AbstractManager;

class LocationUserConnectionsManager extends AbstractManager {

    /**
     * @var singleton instance of class
     */
    private static $instance = null;

    /**
     * Initializes DB mappers
     *
     * @param object $config
     * @param object $args
     * @return
     */
    function __construct($config = array(), $args = array()) {
        $this->locationUserConnectionsMapper = LocationUserConnectionsMapper::getInstance();
        $this->config = $config;
        $this->args = $args;
    }
	
	 public function selectAll(){
        return  $this->locationUserConnectionsMapper->selectAll();
    }

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

	public function addConnection($userId, $locationId) {
		if (!$this->isConnected($userId, $locationId)) {
			$dto = $this->locationUserConnectionsMapper->createDto();
			$dto->setUserId($userId);
			$dto->setLocationId($locationId);
			$this->locationUserConnectionsMapper->insertDto($dto);
		}
	}

	public function isConnected($userId, $locationId) {
		$dto = $this->locationUserConnectionsMapper->getByLocationUserIds($userId, $locationId);
		return isset($dto);
	}

	public function removeByLocationIds($locationIds){			
			if (is_array($locationIds))
			{
				$locationIds = implode(',', $locationIds);
			}
        $this->locationUserConnectionsMapper->removeByLocationIds($locationIds);
        
    }
    
}