<?php

namespace NGS\Managers;

use Arzttermine\Location\Location;
use Arzttermine\User\User;
use NGS\Framework\AbstractManager;

class AvailableAppointmentsManager extends AbstractManager {

    /**
     * @var singleton instance of class
     */
    private static $instance = null;

    /**
     * @param object $config
     * @param object $args
     * @return
     */
    function __construct($config, $args) {
        $this->config = $config;
        $this->args = $args;
    }

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function getAvailableAppointments($datetimeStart, $datetimeEnd, $userId, $locationId, $medicalSpecialtyId = null, $format='Appointment'){           
    	$provider = new Provider();
        $user = new User($userId);
        $location = new Location($locationId);
        $privateAppointments = $provider->getAvailableAppointments($datetimeStart, $datetimeEnd, $user, $location, 1, $medicalSpecialtyId, $format);
        $publicAppointments = $provider->getAvailableAppointments($datetimeStart, $datetimeEnd, $user, $location, 2, $medicalSpecialtyId, $format);
        $day = $datetimeStart;
        $validDates = array();
        while($day < $datetimeEnd){
            $validDates[] = date("Y-m-d",$day);
            $day += 86400;
        }
        foreach ($validDates as $date) {
            $privateAppointmentsForDate = $privateAppointments[$date];      
            if(isset($privateAppointmentsForDate)){
                if(get_class($privateAppointmentsForDate[0])=='Appointment'){
                    $timestamp = false;
                }else{
                    $timestamp = null;
                }
                
                foreach ($privateAppointmentsForDate as $appointment) {
                    $appointment->setInsurance(1);
                    if($timestamp===false){
                    	// WARNING: The appointments are stored in a timestamp array here!!!
                        $appointments[$date][strtotime($appointment->getDateTime())] = $appointment;
                    }
                }
            }
            $publicAppointmentsForDate = $publicAppointments[$date];
            if(isset($publicAppointmentsForDate)){
                if(get_class($publicAppointmentsForDate[0])=='Appointment'){
                    $timestamp = false;
                }else{
                    $timestamp = null;
                }
                foreach ($publicAppointmentsForDate as $appointment) {
                    
                    if($timestamp===false){
                    	// WARNING: The appointments are stored in a timestamp array here!!!
                    	$appTime = strtotime($appointment->getDateTime());
                    }

                    // why are the public appointments handled different that the privates???
                    if(isset($appointments[$date][$appTime])){
                        $appointments[$date][$appTime]->setInsurance(2);
                    }else{
                        $appointment->setInsurance(2);
                        $appointments[$date][$appTime] = $appointment;
                    }
                }
                
            }
        }
        return $appointments;
    }
    
}