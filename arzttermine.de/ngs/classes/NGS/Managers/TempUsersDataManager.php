<?php

namespace NGS\Managers;

use NGS\DAL\Mappers\TempUsersDataMapper;
use NGS\Framework\AbstractManager;

class TempUsersDataManager extends AbstractManager {

    private $tempUsersDataMapper;

    /**
     * @var singleton instance of class
     */
    private static $instance = null;

    /**
     * Initializes DB mappers
     *
     * @param object $config
     * @param object $args
     * @return
     */
    function __construct($config = array(), $args = array()) {
        $this->tempUsersDataMapper = TempUsersDataMapper::getInstance();
        $this->config = $config;
        $this->args = $args;
    }

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function createDto(){
        return $this->tempUsersDataMapper->createDto();
    }

    public function insertDto($dto){
        return $this->tempUsersDataMapper->insertDto($dto);
    }

    public function updateByPK($dto){
        return $this->tempUsersDataMapper->updateByPK($dto);
    }

    public function getById($id){
        return $this->tempUsersDataMapper->selectByPK($id);
    }

    public function getByEmail($email){
        return $this->tempUsersDataMapper->getByEmail($email);
    }

    public function getByEmailAndPasswordHash($email, $passHash){
        return $this->tempUsersDataMapper->getByEmailAndPasswordHash($email, $passHash);
    }

    public function deleteById($id){
        return $this->tempUsersDataMapper->deleteByPK($id);
    }

    /**
     *
     * @param type $id
     * selects the temp_users_data by $id and moves
     * corresponding data to user, location and doctors_treatment_types
     * tables. After that, it deletes that row from temp_users_data table
     * @return $userDto that was added to users table
     */
    public function activateById($id){
        $usersManager = UsersManager::getInstance($this->config, $this->args);
        $doctorsTreatmentTypesManager = DoctorsTreatmentTypesManager::getInstance($this->config, $this->args);
        $tempUserData = $this->tempUsersDataMapper->selectByPK($id);

        //inserting corresponding data into users table
        $userDto = $usersManager->insertTempUserDataIntoDto($tempUserData);
        $userDto->setCreatedAt(date("Y-m-d H:i:s"));
        $userDto->setActivatedAt(date("Y-m-d H:i:s"));
        $userDto->setUpdatedAt(date("Y-m-d H:i:s"));
        $userDto->setLastLoginAt(date("Y-m-d H:i:s"));
        $userId = $usersManager->addUser($userDto);
        $userDto->setId($userId);

        //inserting medical specialties and treatment types data into doctor_treatment_types table
        $treatmentTypeIdsString = $tempUserData->getTreatmentTypeIds();
        $doctorsTreatmentTypesManager->addTreatmentTypesForDoctor($userId, explode(',', $treatmentTypeIdsString));

        //inserting location data into locations table.
        $locationName = $tempUserData->getLocationName();
        if(!empty($locationName)){
            $locationsManager = LocationsManager::getInstance($this->config, $this->args);
            $locationUserConnectionsManager = LocationUserConnectionsManager::getInstance($this->config, $this->args);
            $locationDto = $locationsManager->insertTempUserDataIntoDto($tempUserData);
            $locationDto->setMemberIds($userId);
            $locationDto->setMedicalSpecialtyIds($tempUserData->getMedicalSpecialtyIds());
            $locationDto->setCreatedAt(date("Y-m-d H:i:s"));
            $locationId = $locationsManager->addLocation($locationDto);
            $locationUserConnectionsManager->addConnection($userId,$locationId);
        }
        $this->tempUsersDataMapper->deleteByPK($id);
        return $userDto;
    }

    public function getByActivationCode($activationCode){
        return $this->tempUsersDataMapper->getByActivationCode($activationCode);
    }

    public function setActivationCode($id, $activationCode){
        return $this -> tempUsersDataMapper -> updateTextField($id, 'activation_code', $activationCode);
    }
}
