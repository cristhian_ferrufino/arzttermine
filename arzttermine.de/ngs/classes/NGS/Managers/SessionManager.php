<?php

namespace NGS\Managers;

use NGS\Framework\AbstractSessionManager;
use NGS\Security\RequestGroups;
use NGS\Security\UserGroups;
use NGS\Security\Users\AdminUser;
use NGS\Security\Users\DoctorUser;
use NGS\Security\Users\GuestUser;
use NGS\Security\Users\PatientUser;

class SessionManager extends AbstractSessionManager {

	private $user = null;
	private $config;

    public function __construct($config) {
        if (!session_id()) {
            session_start();
        }
        
        $this->config = $config;
    }

    /**
     * @abstract
     * @access
     * @return object for $this->user
     */
    public function getUser() {

		if ($this->user != null) {
			return $this->user;
		}
		$this->user = new GuestUser();
		try {
			if (isset($_SESSION["ut"]) && isset($_SESSION["uh"]) && isset($_SESSION["ud"])) {
				if ($_SESSION["ut"] == UserGroups::$ADMIN) {
					$user = new AdminUser($_SESSION["ud"]);
				} else if ($_SESSION["ut"] == UserGroups::$DOCTOR) {
					$user = new DoctorUser($_SESSION["ud"]);
				} else if ($_SESSION["ut"] == UserGroups::$PATIENT) {
					$user = new PatientUser($_SESSION["ud"]);
				}
				if ($user) {
					$user->setUniqueId($_SESSION["uh"]);
				}
			}
		} catch (\Exception $e) {
			
		}

        if (isset($user) && $user->validate()) {
            $this->user = $user;
        }
        return $this->user;
    }

    /**
     * @abstract
     * @access
     * @return object for $this->user
     */
    public function getSessionUserId() {
        $sessionUser = $this->getUser();
        if (!($sessionUser instanceof GuestUser)) {
            return $sessionUser->getId();
        }
        return null;
    }
    
     /**
     * @abstract
     * @access
     * @return object for $this->user
     */
    public function getSessionUserLevel() {
        $sessionUser = $this->getUser();       
        return $sessionUser->getLevel();
    }
    
    /**
     * Return a thing based on $request, $user parameters
     * @abstract
     * @access
     * @param $request, $user
     * @return bool
     */
	public function validateRequest($request, $user) {
		if ($request->getRequestGroup() == RequestGroups::$adminsRequest) {
			if ($user->getLevel() == UserGroups::$ADMIN) {
				return true;
			}
		} elseif ($request->getRequestGroup() == RequestGroups::$doctorsRequest) {
			if ($user->getLevel() == UserGroups::$DOCTOR || $user->getLevel() == UserGroups::$ADMIN) {
				return true;
			}
		} elseif ($request->getRequestGroup() == RequestGroups::$patientsRequest) {
			if ($user->getLevel() == UserGroups::$PATIENT) {
				return true;
			}
		} elseif ($request->getRequestGroup() == RequestGroups::$guestRequest) {
			return true;
		}
		return false;
	}


}
