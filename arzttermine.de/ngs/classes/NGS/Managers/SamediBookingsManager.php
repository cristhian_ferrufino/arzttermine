<?php

namespace NGS\Managers;

use NGS\DAL\Mappers\SamediBookingsMapper;
use NGS\Framework\AbstractManager;

class SamediBookingsManager extends AbstractManager {

	/**
	 * @var singleton instance of class
	 */
	private static $instance = null;

	private $samediBookingsMapper;

	/**
	 * Initializes DB mappers
	 *
	 * @param object $config
	 * @param object $args
	 * @return
	 */
	function __construct($config = array(), $args = array()) {
		$this->samediBookingsMapper = SamediBookingsMapper::getInstance();
		$this->config = $config;
		$this->args = $args;
	}

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

	public function getById($id) {
		return $this->samediBookingsMapper->selectByPK($id);
	}
	
	public function getByBookingId($bookingId) {
		return $this->samediBookingsMapper->getByBookingId($bookingId);
	}

	public function addBooking($status, $booking_id, $practiceId, $categoryId, $eventTypeId,$loggedInUserId) {
		$dto = $this->samediBookingsMapper->createDto();
		$dto->setStatus($status);
		$dto->setBookingId($booking_id);
		$dto->setPracticeId($practiceId);
		$dto->setCategoryId($categoryId);
		$dto->setEventTypeId($eventTypeId);
		$dto->setCreatedBy($loggedInUserId);
		return $this->samediBookingsMapper->insertDto($dto);
	}

	public function setBookingStatus($id, $status) {
		return $this->samediBookingsMapper->updateNumericField($id, 'status', $status);
	}

	public function setBookingCancelableUntil($id, $datetime) {
		return $this->samediBookingsMapper->updateTextField($id, 'cancelable_until', $datetime);
	}

	public function setBookingResultJson($id, $sBoking) {
		return $this->samediBookingsMapper->updateTextField($id, 'result_json', json_encode($sBoking));
	}
	
	public function getBookingsFull($offset, $limit, $orderByFieldName, $ascending = true, $searchArray = false) {
		return $this->samediBookingsMapper->getBookingsFull($offset, $limit, $orderByFieldName, $ascending, $searchArray);
	}
	
	public function getAllBookingsCount($searchArray = false) {
		return $this->samediBookingsMapper->getAllBookingsCount($searchArray);
	}
	
		public function setEventTypeId($id, $value) {
		return $this->samediBookingsMapper->updateTextField($id, 'event_type_id', $value);
	}
	
	public function setPracticeId($id, $value) {
		return $this->samediBookingsMapper->updateTextField($id, 'practice_id', $value);
	}
	
	public function setCategoryId($id, $value) {
		return $this->samediBookingsMapper->updateTextField($id, 'category_id', $value);
	}

}