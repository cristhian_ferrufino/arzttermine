<?php

namespace NGS\Loads\Main;
use NGS\Loads\BaseLoad;

/**
 *
 * @author Levon Naghashyan
 *
 */

class RowsPerPageLoad extends BaseLoad {

	public function load() {
		$total_elements_cout = intval($this->args['total_elements_count']);
		$allOptions = explode(',', $this->config['admin_rows_per_page_options']);
		$itemsPerPage = intval($this->config['admin_default_row_per_page']);
		if (isset($this->args['itemsPerPage'])) {
			$itemsPerPage = intval($this->args['itemsPerPage']);
		}
		$paramsArray = $this->args['paramsArray'];
		$urlParamName = $this->args['param_name'];
		$this->initRowPerPage($itemsPerPage, $allOptions, $paramsArray, $urlParamName, $total_elements_cout);
	}

	/**
	 * @param $itemsPerPage selected option
	 * @param $allOptions all options
	 * @param $paramsArray parameters array to put in elements' href
	 * @param string $urlParamName row per page parameter name to put in elements' href
	 * @return boolean
	 */
	public function initRowPerPage($itemsPerPage, $allOptions, $paramsArray, $paramName, $itemsCount) {
		$this->addParam("itemsPerPage", $itemsPerPage);
		$this->addParam("allOptions", $allOptions);
		$paramsUrl = "?";
		if (isset($paramsArray)) {
			foreach ($paramsArray as $key => $value) {
				$paramsUrl .= $key . '=' . $value . '&';
			}
		}		
		$this->addParam("paramsUrl", $paramsUrl);
		$this->addParam("paramName", $paramName);
		$this->addParam("itemsCount", $itemsCount);
		return true;
	}

	public function getDefaultLoads($args) {
		$loads = array();
		return $loads;
	}

	public function getTemplate() {
		return "main/rows_per_page.tpl";
	}

}