<?php

namespace NGS\Loads\Main;

use NGS\Loads\BaseLoad;

/**
 *
 * @author Levon Naghashyan
 *
 */

class PagingLoad extends BaseLoad {

	public function load() {
		$page = 1;		
		if (isset($this->args['current_page_number'])) {
			$page = intval($this->args['current_page_number']);
		}		
		$total_elements_cout = intval($this->args['total_elements_count']);
		$limit = intval($this->args['limit']);
		$itemsPerPage = intval($this->config['admin_default_row_per_page']);
		if (isset($this->args['itemsPerPage'])) {
			$itemsPerPage = intval($this->args['itemsPerPage']);
		}
		$paramsArray = $this->args['paramsArray'];
		$param_name = $this->args['param_name'];
		$this->initPaging($page, $total_elements_cout, $itemsPerPage, $limit, $this->config['admin_users_pages_limit'], $paramsArray, $param_name);
	}

	/*
	 * @param $paramsArray parameters array to put in elements' href
	* @param $paginationParamName pagination parameter name to put in href url
	*/
	public function initPaging($page, $itemsCount, $itemsPerPage, $limit, $pagesShowed, $paramsArray = null, $urlParamName = 'p') {
		// 1 ,      301 ,      20

		$pageCount = ceil($itemsCount / $limit);

		$centredPage = ceil($pagesShowed / 2);
		$pStart = 0;
		if (($page - $centredPage) > 0) {
			$pStart = $page - $centredPage;
		}
		if (($page + $centredPage) >= $pageCount) {
			$pEnd = $pageCount;
			if (($pStart - ($page + $centredPage - $pageCount)) > 0) {
				$pStart = $pStart - ($page + $centredPage - $pageCount);
			}
		} else {
			$pEnd = $pStart + $pagesShowed;
			if ($pageCount < $pagesShowed) {
				$pEnd = $pageCount;
			}
		}

		$this->addParam("itemsPerPage", $itemsPerPage);
		$this->addParam("pageCount", $pageCount);
		$this->addParam("page", $page);
		$this->addParam("pStart", $pStart);
		$this->addParam("pEnd", $pEnd);

		$paramsUrl = "?";
		if (isset($paramsArray)) {
			foreach ($paramsArray as $key => $value) {
				$paramsUrl .= $key . '=' . $value . '&';
			}
		}		
		$this->addParam("paramsUrl", $paramsUrl);
		$this->addParam("paginationParamName", $urlParamName);
		return true;
	}
	
	public function getDefaultLoads($args) {
		$loads = array();
		return $loads;
	}

	public function getTemplate() {
		return "main/paging.tpl";
	}

}