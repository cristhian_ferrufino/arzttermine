<?php

namespace NGS\Loads\Admins\TableViewBaseLoads;
use NGS\Loads\Admins\BaseAdminLoad;

/**
 *
 * <p><i>This is an exmaple of Load file.</i></p>
 * This file is using for outputing template(*.tpl) files.
 *
 * <p>In this file identified nested loads to help separatly controll each part of view.</p>
 * Identifing nested load using in tpl file- "ngs_nested_example" property you can load custom solutions in NgsNestedExampleLoad.calss.php file
 *
 * <p><b>NgsExampleLoad</b> class is extended class from <b>AbstractLoad.</b></p>
 *
 * @author  Naghashyan Solutions, e-mail: info@naghashyan.com
 * @version 1.0
 * @package load
 */
abstract class TableViewHeadBaseLoad extends BaseAdminLoad {

	public $offset;
	public $limit;
	public static $URL_PARAMS_NAMES = array('pg', 'rpp');

	public function load() {
		$this->limit = $this->config['admin_default_row_per_page'];
		if (isset($_REQUEST['rpp'])) {
			$this->limit = $_REQUEST['rpp'];
		}
		$page = 1;
		if (isset($_REQUEST['pg'])) {
			$page = $_REQUEST['pg'];
		}
		$this->offset = ($page - 1) * $this->limit;
		
		//put messages into params
		$this->addParam('message', $this->getMessage());
		
	}

	/**
	 * Return a thing based on $args parameter
	 * @abstract
	 * @access
	 * @param $args
	 * @return $loads
	 */
	public function getDefaultLoads($args) {

		$pageingUrlParamName = self::$URL_PARAMS_NAMES[0];
		$rowPerPageUrlParamName = self::$URL_PARAMS_NAMES[1];


		$loads = array();
		//paging
		$loadKey = 'paging';
		$loads[$loadKey]["load"] = "loads/main/PagingLoad";
		 
		if (isset($_REQUEST['pg'])) {
			$loads[$loadKey]["args"]["current_page_number"] = $_REQUEST[$pageingUrlParamName];
		}
		$loads[$loadKey]["args"]["total_elements_count"] = $this->getTotalRowsCount();
		$loads[$loadKey]["args"]["limit"] = isset($_REQUEST[$rowPerPageUrlParamName]) ? $_REQUEST[$rowPerPageUrlParamName] : $this->config['admin_default_row_per_page'];

		if (isset($_REQUEST[self::$URL_PARAMS_NAMES[1]])) {
			$loads[$loadKey]["args"]["itemsPerPage"] = $_REQUEST[$rowPerPageUrlParamName];
		}
		$paramsArray = $this->getAllUrlParams();
		if (isset($paramsArray[$pageingUrlParamName])) {
			unset($paramsArray[$pageingUrlParamName]);
		}
		$loads[$loadKey]["args"]["paramsArray"] = $paramsArray;
		$loads[$loadKey]["args"]["param_name"] = $pageingUrlParamName;
		$loads[$loadKey]["loads"] = array();

		//rows_per_page
		$loadKey = 'rows_per_page';
		$loads[$loadKey]["load"] = "loads/main/RowsPerPageLoad";
		if (isset($_REQUEST[self::$URL_PARAMS_NAMES[1]])) {
			$loads[$loadKey]["args"]["itemsPerPage"] = $_REQUEST[$rowPerPageUrlParamName];
		}
		$paramsArray = $this->getAllUrlParams();				
		if (isset($paramsArray[$rowPerPageUrlParamName])) {
			unset($paramsArray[$rowPerPageUrlParamName]);
		}
		$loads[$loadKey]["args"]["total_elements_count"] = $this->getTotalRowsCount();
		$loads[$loadKey]["args"]["paramsArray"] = $paramsArray;
		$loads[$loadKey]["args"]["param_name"] = $rowPerPageUrlParamName;		
		$loads[$loadKey]["loads"] = array();
		return $loads;
	}

	abstract protected function getTotalRowsCount();

	protected function getMessage()
	{
		return null;	
	}
	 
	
	public function getAllUrlParams() {
		$urlParams = array();
		foreach (self::$URL_PARAMS_NAMES as $paramName) {
			if (isset($_REQUEST[$paramName])) {
				$urlParams[$paramName] = $this->secure($_REQUEST[$paramName]);
			}
		}
		return $urlParams;
	}

	
}