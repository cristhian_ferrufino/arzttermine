<?php

namespace NGS\Loads\Admins\TableViewBaseLoads;

use NGS\Loads\Admins\BaseAdminLoad;

/**
 *
 * <p><i>This is an exmaple of Load file.</i></p>
 * This file is using for outputing template(*.tpl) files.
 *
 * <p>In this file identified nested loads to help separatly controll each part of view.</p>
 * Identifing nested load using in tpl file- "ngs_nested_example" property you can load custom solutions in NgsNestedExampleLoad.calss.php file
 *
 * <p><b>NgsExampleLoad</b> class is extended class from <b>AbstractLoad.</b></p>
 *
 * @author  Naghashyan Solutions, e-mail: info@naghashyan.com
 * @version 1.0
 * @package load
 */
abstract class TableViewBodyBaseLoad extends BaseAdminLoad {
	
	protected $headLoad; 
	protected $footerLoad;				
		
	public function load()
	{
		$this->headLoad = $this->args['headLoad'];
		$this->footerLoad = $this->args['footerLoad'];
		
		if(isset($_REQUEST['direction'])){
			$this->args['direction']=$_REQUEST['direction'];
			$this->addParam("direction",$this->args['direction']);
		}
		if(isset($_REQUEST['order_by'])){
			$this->args['orderBy']=$_REQUEST['order_by'];
			$this->addParam("orderBy",$this->args['orderBy']);
		}
		
		$requestString='';
		if(isset($_REQUEST['search']) and !empty($_REQUEST['search'])){
			$this->args['search']=$this->secure($_REQUEST['search']);
			if(!empty($_POST)){
				$requestString='&search=yes';
				foreach ($_POST as $key => $value) {
					$requestString.='&'.$key.'='.$value;
				}
			}else{
				
				foreach ($_GET as $key => $value) {
					if($key!='direction' && $key!='order_by'){
						$requestString.='&'.$key.'='.$value;
					}
				}
			}
		}
		

		$limit=$this->headLoad->limit;
		$offset=$this->headLoad->offset;
		$pageNumber=$offset/$limit+1;
		$requestString.='&rpp='.$limit.'&pg='.$pageNumber;

		if(!empty($requestString)){
			$this->addParam('requestString',$requestString);
		}
		
		if(isset($_REQUEST['mapperName'])){
			$this->args['mapperName']=$_REQUEST['mapperName'];
		}

		if(isset($this->args['search'])){
			$this->addParam("search",$this->args['search']);
		}
		
		$visibleColumns = $this->getVisibleFieldsNames();
		$this->addParam("columns", $visibleColumns);
		$rows = $this->getTableDataRows();
		$this->addParam("rows", $rows);
		$fieldsDisplayValues = $this->getFieldsDisplayValues();
		$this->addParam("fieldsDisplayValues", $fieldsDisplayValues);	
		$this->addParam('searchArray',$this->getSearchArray());
	}
	
	abstract function getVisibleFieldsNames();
	
	
	/**
	* return type should be following
	* array('fieldName' => array('value'=>'DisplayValue'))
	*/
	function getFieldsDisplayValues()
	{
		return array();
	}
	
	abstract function getTableDataRows();
	
}