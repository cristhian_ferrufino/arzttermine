<?php

namespace NGS\Loads\Admins;
use NGS\Framework\Dispatcher;
use NGS\Managers\BookingsManager;
use NGS\Util\UtilFunctions;

/**
 *
 * <p><i>This is an exmaple of Load file.</i></p>
 * This file is using for outputing template(*.tpl) files.
 *
 * <p>In this file identified nested loads to help separatly controll each part of view.</p>
 * Identifing nested load using in tpl file- "ngs_nested_example" property you can load custom solutions in NgsNestedExampleLoad.calss.php file
 *
 * <p><b>NgsExampleLoad</b> class is extended class from <b>AbstractLoad.</b></p>
 *
 * @author  Naghashyan Solutions, e-mail: info@naghashyan.com
 * @version 1.0
 * @package load
 */
class HomeLoad extends BaseAdminLoad {

	public function load() {
		$page = 'home';
		if (!empty($_REQUEST["p"])) {
			$page = $_REQUEST["p"];
		}
		if (!empty($_REQUEST["sub_pages"])) {
			$sub_pages = $_REQUEST["sub_pages"];
		}
		
        $loadKey = "content_right";
		$loadClassName = $this->getLoadClassNameByLoadKey($loadKey);
		$load["load"] = Dispatcher::buildClassName('loads', 'admins', $loadClassName);
        
		$this->args["mainLoad"] = &$this;
		$load["args"] = $this->args;
		$load["args"]["page"] = $page;
		if (isset($sub_pages)) {
			$load["args"]["sub_pages"] = $sub_pages;
		}

		$load["loads"] = array();
		$this->nest("content_right", $load);
        $this->getMailMessage();
        
        if(isset($_REQUEST['newentry'])){
            $id=$_REQUEST['newId'];
            $tableName=ucfirst(substr($_REQUEST['p'],0,-1));
            $this->addParam('newentry',$_REQUEST['newentry']);
            $this->addParam('newId',$id);
            $this->addParam('tableName',$tableName);
        }
        
        if(isset($_REQUEST["editentry"])){
            $this->addParam('editentry',$_REQUEST['editentry']);
        }
        
        if(isset($_REQUEST["deleteEntry"])){
            $this->addParam('deleteEntry',$_REQUEST['deleteEntry']);
        }

	}
    
    public function getMailMessage(){
        if(isset($_REQUEST["send_mail"])){
             $bookingManager=BookingsManager::getInstance($this->config, $this->args);
            $name=$_REQUEST["form_data"]["name"];
            $to=$_REQUEST["form_data"]["email"];
            $subject=$_REQUEST["form_data"]["subject"];
            $message=$_REQUEST["form_data"]["body"];
            $template = SYSTEM_PATH . "/ngs/templates/admins/samedibookings/email/email_content.tpl";
            $from=$GLOBALS["CONFIG"]["SYSTEM_MAIL_FROM_EMAIL"];
            $bookingId=$_REQUEST["id"];
            $bookingDto=$bookingManager->getBookingByIdFull($bookingId);
            $mailingDto=$bookingManager->createMailingDto();
            $mailingDto->setStatus(1);
            $mailingDto->setType(1);
            $mailingDto->setSourceTypeId(1);
            $mailingDto->setSourceId($bookingId);
            $mailingDto->setMailto('"'.$name.'" <'.$to.'>');
            $mailingDto->setSubject($subject);
            $mailingDto->setBodyText($message);
            $mailingDto->setCreatedAt(date('Y-m-d H:i:s'));
            $mailingDto->setCreatedBy($this->user->getId());
            
            $appointmentWeekDayIndex=(date('w', strtotime($bookingDto->getAppointmentStartAt())));
            $appointmentWeekDay=$GLOBALS["CONFIG"]["CALENDAR_WEEKDAY_TEXTS"][$appointmentWeekDayIndex];
            $sendMail=$bookingManager->sendMail($from, array($to), $subject, $template ,array("bookingDto"=>$bookingDto,'genderArray'=>$GLOBALS["CONFIG"]["ARRAY_GENDER"],"appointmentWeekDay"=>$appointmentWeekDay),$mailingDto);
            $mailStatus=$sendMail?'success':'error';
            $this->addParam("mailStatus",$mailStatus);
        }
    }

	/**
	 * Return a thing based on $args parameter
	 * @abstract
	 * @access
	 * @param $args
	 * @return $loads
	 */
	public function getDefaultLoads($args) {

		$loads = array();

		$loadKey = "menu_left";
        $loadClassName = UtilFunctions::underscoreToCamelCase($loadKey . 'Load');
		$loads[$loadKey]["load"] = Dispatcher::buildClassName('loads', 'admins', $loadClassName);
        
		$this->args["mainLoad"] = &$this;
		$loads[$loadKey]["args"] = $this->args;
		$loads[$loadKey]["loads"] = array();

		return $loads;
	}

	/**
	 * Using for validation whether as certain load with nesting
	 *
	 * Return a thing based on $namespace, $load parameters
	 * @abstract
	 * @access
	 * @param $namespace, $load
	 * @return true
	 */
	public function isValidLoad($namespace, $load) {
		return true;
	}

	/**
	 * Identify view tempalte
	 *
	 * @abstract
	 * @access
	 * @param
	 * @return  *.tpl file
	 */
	public function getTemplate() {
		return "admins/home.tpl";
	}

	public function isMain() {
		return true;
	}

}