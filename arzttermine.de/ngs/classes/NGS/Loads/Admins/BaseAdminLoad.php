<?php

namespace NGS\Loads\Admins;

use NGS\Loads\BaseLoad;
use NGS\Managers\UsersManager;
use NGS\Security\RequestGroups;

/**
 *
 * <p><i>This is an exmaple of Load file.</i></p>
 * This file is using for outputing template(*.tpl) files.
 *
 * <p>In this file identified nested loads to help separatly controll each part of view.</p>
 * Identifing nested load using in tpl file- "ngs_nested_example" property you can load custom solutions in NgsNestedExampleLoad.calss.php file
 *
 * <p><b>NgsExampleLoad</b> class is extended class from <b>AbstractLoad.</b></p>
 *
 * @author  Naghashyan Solutions, e-mail: info@naghashyan.com
 * @version 1.0
 * @package load
 */
abstract class BaseAdminLoad extends BaseLoad {

    private $usersManager;

    public function initialize($smarty, $sessionManager, $config, $loadMapper, $args)
    {
        parent::initialize($smarty, $sessionManager, $config, $loadMapper, $args);
        setlocale(LC_TIME, 'de_DE');
        $this->usersManager = UsersManager::getInstance($config, $args);
        $this->addParam("admin", $this->getAdmin());
        $this->addParam("sess_admin", $this->user);
    }

    public function getAdmin()
    {
        return $this->usersManager->getUserById($this->user->getId());
    }

    public function getSearchArray()
    {
        $searchArray = array();
        if (isset($this->args['search']) && $this->args['search'] == 'yes') {

            $mapperName = $this->secure($_REQUEST['mapperName']);

            $mapper = $mapperName::getInstance();
            $dto = $mapper->createDto();
            $searchStyleArray = array('=', '>', '>=', '<', '<=', '!=', '=', '!=', 'LIKE', 'BETWEEN');
            $searchArray = array();
            $requestString = '';

            foreach ($dto->getMapArray() as $key => $value) {
                if ((isset($_REQUEST[$value]) && (!empty($_REQUEST[$value]) && isset($_REQUEST[$value . "_searchStyle"]) && !is_null($_REQUEST[$value . "_searchStyle"]))
                     || (empty($_REQUEST[$value]) && isset($_REQUEST[$value . "_searchStyle"]) && (($_REQUEST[$value . "_searchStyle"] == 6) || ($_REQUEST[$value . "_searchStyle"] == 7) || ($_REQUEST[$value . "_searchStyle"] == 'on'))))
                ) {
                    $requestString .= '&' . $key . '=' . $_REQUEST[$value];

                    if ($_REQUEST[$value . "_searchStyle"] == 8) {
                        $searchArray[$key] = array('value' => '%' . $_REQUEST[$value] . '%', 'searchStyle' => $searchStyleArray[$_REQUEST[$value . "_searchStyle"]]);
                    } elseif ($_REQUEST[$value . "_searchStyle"] == 'on') {
                        $searchArray[$key] = array('value' => $_REQUEST[$value], 'searchStyle' => $searchStyleArray[6]);
                    } elseif ($_REQUEST[$value . "_searchStyle"] == 9) {
                        $searchArray[$key] = array('value1' => $_REQUEST[$value], 'value2' => $_REQUEST[$value . '1'], 'searchStyle' => $searchStyleArray[$_REQUEST[$value . '_searchStyle']]);
                    } else {
                        $searchArray[$key] = array('value' => $_REQUEST[$value], 'searchStyle' => $searchStyleArray[$_REQUEST[$value . "_searchStyle"]]);
                    }
                }
            }
        }

        return $searchArray;
    }

    /** Identify permissions for users in this action file.
     *
     * Using for exmaple:
     * $adminsRequest variable is giving permission to  actions,loads file as a Administrator.
     * $guestRequest variable is giving permission to the actions,loads as a Guest User.
     *
     * The logic for this user is written in SessionManager.class.php file.
     *
     * @abstract
     * @access
     *
     * @param
     *
     * @return $guestRequest
     */

    public function getRequestGroup()
    {
        return RequestGroups::$adminsRequest;
    }

    public function onNoAccess()
    {
        $this->redirect("ngsadmin/login");
    }
}