<?php

namespace NGS\Loads\Admins;

use NGS\Framework\Dispatcher;
use NGS\Util\UtilFunctions;

class ContentRightLoad extends BaseAdminLoad {

    public function load()
    {
        $subPath = "";
        if (isset($this->args['sub_pages']) && !empty($this->args['sub_pages'])) {
            $subPath = '\\' . implode('\\', array_map(function (&$a) { return ucfirst($a); }, $this->args['sub_pages']));
        }

        $a = array('head', 'footer', 'body');
        foreach ($a as $key => $loadKey) {
            $loadClassName = $this->getLoadClassNameByLoadKey($loadKey);
            $load['load'] = Dispatcher::buildClassName('loads', 'Admins\\RightSides\\' . UtilFunctions::underscoreToCamelCase($this->args['page']) . $subPath, $loadClassName);
            if (class_exists($load['load'])) {
                $this->args["mainLoad"] = &$this;
                if ($key === 2) {
                    $this->args["headLoad"] = &$this->head;
                    $this->args["footerLoad"] = &$this->footer;
                }
                $load["args"] = $this->args;
                $load["loads"] = array();
                $this->$loadKey = $this->nest("content_right_$loadKey", $load);
            } else {
                $this->nest("content_right_$loadKey", $this->getEmptyLoadAttr());
            }
        }
    }

    public function getEmptyLoadAttr()
    {
        $load["load"] = Dispatcher::buildClassName('loads', '', 'EmptyLoad');
        $this->args["mainLoad"] = & $this;
        $load["args"] = $this->args;
        $load["loads"] = array();

        return $load;
    }

    /**
     * Return a thing based on $args parameter
     *
     * @abstract
     * @access
     *
     * @param $args
     *
     * @return $loads
     */
    public function getDefaultLoads($args)
    {
        $loads = array();

        return $loads;
    }

    /**
     * Using for validation whether as certain load with nesting
     *
     * Return a thing based on $namespace, $load parameters
     *
     * @abstract
     * @access
     *
     * @param $namespace , $load
     *
     * @return true
     */
    public function isValidLoad($namespace, $load)
    {
        return true;
    }

    /**
     * Identify view tempalte
     *
     * @abstract
     * @access
     *
     * @param
     *
     * @return  *.tpl file
     */
    public function getTemplate()
    {
        return "admins/content_right.tpl";
    }

    /*public  function getRequestGroup(){
     return RequestGroups::$adminRequest;
     }*/

    public function isMain()
    {
        return true;
    }
}