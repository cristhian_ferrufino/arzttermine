<?php

namespace NGS\Loads\Admins;
use NGS\Framework\Dispatcher;
use NGS\Loads\BaseLoad;
use NGS\Security\RequestGroups;

/**
 *
 * <p><i>This is an exmaple of Load file.</i></p>
 * This file is using for outputing template(*.tpl) files.
 *
 * <p>In this file identified nested loads to help separatly controll each part of view.</p>
 * Identifing nested load using in tpl file- "ngs_nested_example" property you can load custom solutions in NgsNestedExampleLoad.calss.php file
 *
 * <p><b>NgsExampleLoad</b> class is extended class from <b>AbstractLoad.</b></p>
 *
 * @author  Naghashyan Solutions, e-mail: info@naghashyan.com
 * @version 1.0
 * @package load
 */
class MainLoad extends BaseLoad {

	public function load() {

	}

	/**
	 * Return a thing based on $args parameter
	 * @abstract
	 * @access
	 * @param $args
	 * @return $loads
	 */
	public function getDefaultLoads($args) {
		$contentJsLoad = "home";

		if (!empty($_REQUEST["p"]) && $_REQUEST["p"] == "login") {
			$contentJsLoad = $_REQUEST["p"];
		}

        $loads = array();
        $loads["content"]["load"] = Dispatcher::buildClassName('loads', 'admins', ucfirst($contentJsLoad) . 'Load');
		$this->args["mainLoad"] = &$this;
		$loads["content"]["args"] = $this -> args;
		$loads["content"]["loads"] = array();
		$this->addParam("contentLoad", $contentJsLoad);
		if ($contentJsLoad != 'login') {
			$mailingLoadKey = "mailing";
			$mailingLoadClassName = $this->getLoadClassNameByLoadKey($mailingLoadKey);
			$loads[$mailingLoadKey]["load"] = "NGS\\Loads\\Admins\\RightSides\\SamediBookings\\Email\\" . $mailingLoadClassName;
			$loads[$mailingLoadKey]["args"] = $this -> args;
			$loads[$mailingLoadKey]["loads"] = array();
			$this -> nest('mailing', $loads[$mailingLoadKey]);
			$this -> addParam('mailing', 'yes');
		}
		
		return $loads;
	}

	/**
	 * Using for validation whether as certain load with nesting
	 *
	 * Return a thing based on $namespace, $load parameters
	 * @abstract
	 * @access
	 * @param $namespace, $load
	 * @return true
	 */
	public function isValidLoad($namespace, $load) {
		return true;
	}

	/**
	 * Identify view tempalte
	 *
	 * @abstract
	 * @access
	 * @param
	 * @return  *.tpl file
	 */
	public function getTemplate() {
		return "admins/main.tpl";
	}

	/** Identify permissions for users in this action file.
	 *
	 * Using for exmaple:
	 * $adminsRequest variable is giving permission to  actions,loads file as a Administrator.
	 * $guestRequest variable is giving permission to the actions,loads as a Guest User.
	 *
	 * The logic for this user is written in SessionManager.class.php file.
	 *
	 * @abstract
	 * @access
	 * @param
	 * @return $guestRequest
	 */

	public function getRequestGroup() {
		return RequestGroups::$guestRequest;
	}

	/*public  function getRequestGroup(){
	 return RequestGroups::$adminRequest;
	 }*/

	public function isMain() {
		return true;
	}

}