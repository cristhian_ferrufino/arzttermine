<?php

namespace NGS\Loads\Admins;
use NGS\Loads\BaseLoad;
use NGS\Security\RequestGroups;

/**
 *
 * <p><i>This is an exmaple of Load file.</i></p>
 * This file is using for outputing template(*.tpl) files.
 *
 * <p>In this file identified nested loads to help separatly controll each part of view.</p>
 * Identifing nested load using in tpl file- "ngs_nested_example" property you can load custom solutions in NgsNestedExampleLoad.calss.php file
 *
 * <p><b>NgsExampleLoad</b> class is extended class from <b>AbstractLoad.</b></p>
 *
 * @author  Naghashyan Solutions, e-mail: info@naghashyan.com
 * @version 1.0
 * @package load
 */
class LoginLoad extends BaseLoad {

	public function load() {
					
		if (isset($this->args["error"]))
		{			
			$this->addParam("error", "true");
			$this->addParam("error_message", $this->args["error_message"]);
		}
		$page = "";
		if(isset($_REQUEST['p'])){
			$page = $_REQUEST['p'];
		}
		if ($page == "forgot_login")
		{
			$this->addParam("forgot_login", "true");
		}
		if ($page == "forgot_password")
		{
			$this->addParam("forgot_password", "true");
		}		
	}

	/**
	 * Return a thing based on $args parameter
	 * @abstract
	 * @access
	 * @param $args
	 * @return $loads
	 */
	public function getDefaultLoads($args) {
		$loads = array();
		return $loads;
	}

	/**
	 * Using for validation whether as certain load with nesting
	 *
	 * Return a thing based on $namespace, $load parameters
	 * @abstract
	 * @access
	 * @param $namespace, $load
	 * @return true
	 */
	public function isValidLoad($namespace, $load) {
		return true;
	}

	/**
	 * Identify view tempalte
	 *
	 * @abstract
	 * @access
	 * @param
	 * @return  *.tpl file
	 */
	public function getTemplate() {
		return "admins/login.tpl";
	}

	/** Identify permissions for users in this action file.
	 *
	 * Using for exmaple:
	 * $adminsRequest variable is giving permission to  actions,loads file as a Administrator.
	 * $guestRequest variable is giving permission to the actions,loads as a Guest User.
	 *
	 * The logic for this user is written in SessionManager.class.php file.
	 *
	 * @abstract
	 * @access
	 * @param
	 * @return $guestRequest
	 */

	public function getRequestGroup() {		
		return RequestGroups::$guestRequest;
	}

	public function isMain() {
		return true;
	}

}