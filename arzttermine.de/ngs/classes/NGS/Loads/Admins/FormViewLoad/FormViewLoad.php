<?php

namespace NGS\Loads\Admins\FormViewLoad;

use NGS\Loads\Admins\BaseAdminLoad;

abstract class FormViewLoad extends BaseAdminLoad {
	
	public function load(){
		$formInfo=$this->getVisibleNamesAndInputTypes();
		$selectDisplayValues=$this->getSelectDisplayValues();
		
		if (isset($_REQUEST['error'])){
			$error=$_REQUEST['error'];
			$this->addParam("error",$error);
		}
        if(isset($this->args["error"])){
            $this->addParam("error",$this->args["error"]);
        }
        if(isset($this->args["request"])){
            $this->addParam("request",$this->args["request"]);
        }
		$this->addParam("formInfo", $formInfo);
		$this->addParam('selectDisplayValues', $selectDisplayValues);
	}
	
	abstract function getVisibleNamesAndInputTypes();
	
	
	function getSelectDisplayValues(){
		return array();
	}
}