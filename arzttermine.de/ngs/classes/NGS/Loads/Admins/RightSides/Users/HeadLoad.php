<?php

namespace NGS\Loads\Admins\RightSides\Users;

use NGS\Loads\Admins\TableViewBaseLoads\TableViewHeadBaseLoad;
use NGS\Managers\UsersManager;

/**
 *
 * <p><i>This is an exmaple of Load file.</i></p>
 * This file is using for outputing template(*.tpl) files.
 *
 * <p>In this file identified nested loads to help separatly controll each part of view.</p>
 * Identifing nested load using in tpl file- "ngs_nested_example" property you can load custom solutions in NgsNestedExampleLoad.calss.php file
 *
 * <p><b>NgsExampleLoad</b> class is extended class from <b>AbstractLoad.</b></p>
 *
 * @author  Naghashyan Solutions, e-mail: info@naghashyan.com
 * @version 1.0
 * @package load
 */
class HeadLoad extends TableViewHeadBaseLoad {

	public function load() {
			parent::load();
	}

	public function getTotalRowsCount() {
		$usersManager = UsersManager::getInstance($this->config, $this->args);
		return $usersManager->getAllUsersCount();
	}

	/**
	 * Identify view tempalte
	 *
	 * @abstract
	 * @access
	 * @param
	 * @return  *.tpl file
	 */
	public function getTemplate() {
		return "admins/users/head.tpl";
	}


}