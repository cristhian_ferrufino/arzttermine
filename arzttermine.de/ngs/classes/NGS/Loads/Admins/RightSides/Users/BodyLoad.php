<?php

namespace NGS\Loads\Admins\RightSides\Users;

use NGS\Loads\Admins\TableViewBaseLoads\TableViewBodyBaseLoad;
use NGS\Managers\UsersManager;

/**
 *
 * <p><i>This is an exmaple of Load file.</i></p>
 * This file is using for outputing template(*.tpl) files.
 *
 * <p>In this file identified nested loads to help separatly controll each part of view.</p>
 * Identifing nested load using in tpl file- "ngs_nested_example" property you can load custom solutions in NgsNestedExampleLoad.calss.php file
 *
 * <p><b>NgsExampleLoad</b> class is extended class from <b>AbstractLoad.</b></p>
 *
 * @author  Naghashyan Solutions, e-mail: info@naghashyan.com
 * @version 1.0
 * @package load
 */
class BodyLoad extends TableViewBodyBaseLoad {

	public function load() {
		parent::load();
	}
	
	public function getVisibleFieldsNames()
	{
		return array("id" => "ID", "status" => "Status", "email" => "Email", "groupId" => "Group", "gender" => "Gender",
								 "title" => "TITLE", "firstName" => "First Name", "lastName" => "Last Name", "phone" => "Phone", 
								 "medicalSpecialtyIds" => "Medical Specialities", "createdAt" => "Created At", "createdBy" => "Created By", 
								 "activatedAt" => "Activated At", "updatedAt" => "Updated At", "updatedBy" => "Updated By", "lastLoginAt" => "Last Login");			
	}
	
	public function getTableDataRows()
	{		 
		$usersManager = UsersManager::getInstance($this->config, $this->args);		
		$allUsers = $usersManager->getAllUsers($this->headLoad->offset, $this->headLoad->limit, 'id');
		return $allUsers;
	}
	
	function getFieldsDisplayValues()
	{
		
		$arr=array('status'=>
		array(
			
			USER_STATUS_DRAFT =>$GLOBALS["CONFIG"]['USER_STATUS'][USER_STATUS_DRAFT],
			USER_STATUS_ACTIVE =>$GLOBALS["CONFIG"]['USER_STATUS'][USER_STATUS_ACTIVE],
			USER_STATUS_VISIBLE =>$GLOBALS["CONFIG"]['USER_STATUS'][USER_STATUS_VISIBLE],
			USER_STATUS_VISIBLE_APPOINTMENTS =>$GLOBALS["CONFIG"]['USER_STATUS'][USER_STATUS_VISIBLE_APPOINTMENTS],
		),
			'gender'=>$GLOBALS["CONFIG"]["ARRAY_GENDER"],
			'group'=> 
		array(
			GROUP_ADMIN =>$GLOBALS["CONFIG"][GROUP_ADMIN],
			GROUP_USER =>$GLOBALS["CONFIG"][GROUP_USER],
			GROUP_DOCTOR =>$GLOBALS["CONFIG"][GROUP_DOCTOR],
		)
		
		);
		
		return $arr;		
	}

	/**
	 * Return a thing based on $args parameter
	 * @abstract
	 * @access
	 * @param $args
	 * @return $loads
	 */
	public function getDefaultLoads($args) {

		$loads = array();

		return $loads;
	}

	/**
	 * Identify view tempalte
	 *
	 * @abstract
	 * @access
	 * @param
	 * @return  *.tpl file
	 */
	public function getTemplate() {
		return "admins/users/body.tpl";
	}

	
}