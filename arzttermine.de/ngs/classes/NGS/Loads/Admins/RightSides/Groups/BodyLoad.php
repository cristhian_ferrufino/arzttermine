<?php

namespace NGS\Loads\Admins\RightSides\Groups;

use NGS\Loads\Admins\TableViewBaseLoads\TableViewBodyBaseLoad;
use NGS\Managers\GroupsManager;

/**
 *
 * <p><i>This is an exmaple of Load file.</i></p>
 * This file is using for outputing template(*.tpl) files.
 *
 * <p>In this file identified nested loads to help separatly controll each part of view.</p>
 * Identifing nested load using in tpl file- "ngs_nested_example" property you can load custom solutions in NgsNestedExampleLoad.calss.php file
 *
 * <p><b>NgsExampleLoad</b> class is extended class from <b>AbstractLoad.</b></p>
 *
 * @author  Naghashyan Solutions, e-mail: info@naghashyan.com
 * @version 1.0
 * @package load
 */
class BodyLoad extends TableViewBodyBaseLoad {

	public function load() {
		parent::load();
	}
	
	public function getVisibleFieldsNames()
	{
		return array("id" => "ID", "name" => "Name", "createdAt" => "Created At", "userFirstName" => "Created By Name", "userLastName" => "Created By Last Name");			
	}
	
	public function getTableDataRows()
	{		 
		$groupsManager = GroupsManager::getInstance($this->config, $this->args);		
		$allGroups = $groupsManager->getAllGroupsJoinedUsers($this->headLoad->offset, $this->headLoad->limit, 'id');
		return $allGroups;
	}
	
	

	/**
	 * Return a thing based on $args parameter
	 * @abstract
	 * @access
	 * @param $args
	 * @return $loads
	 */
	public function getDefaultLoads($args) {

		$loads = array();

		return $loads;
	}

	/**
	 * Identify view tempalte
	 *
	 * @abstract
	 * @access
	 * @param
	 * @return  *.tpl file
	 */
	public function getTemplate() {
		return "admins/groups/body.tpl";
	}

	
}
