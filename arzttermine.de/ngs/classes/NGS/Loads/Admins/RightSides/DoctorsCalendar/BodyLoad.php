<?php

namespace NGS\Loads\Admins\RightSides\DoctorsCalendar;

use Arzttermine\Integration\Integration;
use NGS\Framework\Dispatcher;
use NGS\Loads\Admins\BaseAdminLoad;
use NGS\Managers\LocationsManager;

class BodyLoad extends BaseAdminLoad {

    public $selectedIntegrationId = 0;
    public $selectedLocationId = 0;
    public $selectedDoctorId = 0;
    
    public function load()
    {
        $locationsManager = LocationsManager::getInstance($this->config, $this->args);

        // Integration information
        // ------------------------------------------------------------------------
        $allIntegrationsIdsArray = $locationsManager->getAllIntegrationsIdsArray();
        $integrationsNamesArray = array();
        $availableIntegrationsIdsArray = array();
        
        foreach ($allIntegrationsIdsArray as $intId) {
            $integration_id = intval($intId);
            if ($integration = Integration::getIntegration($integration_id)) {
                $integrationsNamesArray[] = $integration->getName();
                $availableIntegrationsIdsArray[] = $integration_id;
            }
        }
        
        $this->addParam("integrationsIdsArray", $availableIntegrationsIdsArray);
        $this->addParam("integrationsNamesArray", $integrationsNamesArray);

        $selectedIntegrationId = intval($this->getRequestParam('integration_id', $availableIntegrationsIdsArray[0]));
        
        $this->selectedIntegrationId = $selectedIntegrationId;
        $this->addParam("selectedIntegrationId", $selectedIntegrationId);

        // ------------------------------------------------------------------------
        
        $locationsIdNameArrayMap = array();
        $doctorsIdNameArrayMap = array();

        foreach ($locationsManager->getIntegrationLocationsWithDoctors($selectedIntegrationId) as $locationDto) {
            $locationsIdNameArrayMap[$locationDto->getId()] = $locationDto->getName();
            $doctorsIdNameArrayMap[$locationDto->getId()][$locationDto->getUserDto()->getId()] = $locationDto->getUserDto()->getDoctorFullName();
        }

        $this->addParam("locationsIds", array_keys($locationsIdNameArrayMap));
        $this->addParam("locationsNames", $locationsIdNameArrayMap);
        $locationsNamesIdsArray = array();
        foreach ($locationsIdNameArrayMap as $locationId => $locationName) {
            $locationsNamesIdsArray[] = array("label" => $locationName, "value" => $locationId);
        }
        $this->addParam("locationsNamesIdsJson", json_encode($locationsNamesIdsArray));
        reset($locationsIdNameArrayMap);
        
        $this->selectedLocationId = key($locationsIdNameArrayMap);
        if (empty($this->selectedLocationId)) {
            return;
        }

        $this->selectedLocationId = intval($this->getRequestParam('location_id', $this->selectedLocationId));
        $this->addParam("selectedLocationId", $this->selectedLocationId);

        $doctors_ids = !empty($doctorsIdNameArrayMap[$this->selectedLocationId]) ? array_keys($doctorsIdNameArrayMap[$this->selectedLocationId]) : array();
        $this->addParam("doctorsIds", $doctors_ids);
        
        $doctors_names = !empty($doctorsIdNameArrayMap[$this->selectedLocationId]) ? $doctorsIdNameArrayMap[$this->selectedLocationId] : array();
        $this->addParam("doctorsNames", $doctors_names);

        reset($doctorsIdNameArrayMap[$this->selectedLocationId]);
        $this->selectedDoctorId = key($doctorsIdNameArrayMap[$this->selectedLocationId]);

        if (isset($_REQUEST['user_id'])) {
            $this->selectedDoctorId = intval($_REQUEST['user_id']);
        }
        
        $this->addParam("selectedDoctorId", $this->selectedDoctorId);
    }

    /**
     * @param array $args
     *
     * @return array
     */
    public function getDefaultLoads($args)
    {
        $loads = array();
        if (isset($this->selectedDoctorId)) {
            $loads["calendar"]["load"] = Dispatcher::buildClassName('loads', 'Doctors', 'CalendarLoad');
            $this->args["parentLoad"] = & $this;

            $loads["calendar"]["args"] = $this->args;
            $loads["calendar"]["loads"] = array();
            $this->addParam("contentLoad", 'calendar');
        }

        return $loads;
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        return "admins/doctorscalendar/body.tpl";
    }
}
