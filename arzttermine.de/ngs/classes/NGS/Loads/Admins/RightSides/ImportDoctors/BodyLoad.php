<?php

namespace NGS\Loads\Admins\RightSides\ImportDoctors;

use NGS\Loads\Admins\TableViewBaseLoads\TableViewBodyBaseLoad;
use NGS\Managers\DoctorsManager;
use NGS\Managers\SamediDatasManager;
use NGS\Managers\SamediTreatmentTypesManager;

/**
 *
 * <p><i>This is an exmaple of Load file.</i></p>
 * This file is using for outputing template(*.tpl) files.
 *
 * <p>In this file identified nested loads to help separatly controll each part of view.</p>
 * Identifing nested load using in tpl file- "ngs_nested_example" property you can load custom solutions in NgsNestedExampleLoad.calss.php file
 *
 * <p><b>NgsExampleLoad</b> class is extended class from <b>AbstractLoad.</b></p>
 *
 * @author  Naghashyan Solutions, e-mail: info@naghashyan.com
 * @version 1.0
 * @package load
 */
class BodyLoad extends TableViewBodyBaseLoad {

    public function load()
    {
        parent::load();

        $doctorsManager = DoctorsManager::getInstance($this->config, $this->args);
        $this->addParam("doctorsManager", $doctorsManager);

        if (isset($this->args['integration_doctors'])) {
            $doctorsDtos = $this->args['integration_doctors'];
            $samediDatasDtosInSpecificArrayMap = $this->getSamediDatasDtosInSpecificArrayMap();

            $this->addParam("doctorsDtos", $doctorsDtos);
            $this->addParam("samediDatasDtosInSpecificArrayMap", $samediDatasDtosInSpecificArrayMap);
        }
        if (isset($this->args['inserted_doctors'])) {
            $this->addParam("doctorsDtos", $this->args['inserted_doctors']);
            $this->addParam("afterInsert", 1);
        }
        // lol
        // @todo: Remove
        if (isset($this->args['request'])) {
            $this->addParam("request", $this->args['request']);
        } else {
            $this->addParam('request', array());
        }
        
        $samediTreatmentTypesManager = SamediTreatmentTypesManager::getInstance($this->config, $this->args);
        $this->addParam("samediTreatmentTypesManager", $samediTreatmentTypesManager);
    }

    private function getSamediDatasDtosInSpecificArrayMap()
    {
        $samediDatasManager = SamediDatasManager::getInstance($this->config, $this->args);
        $allSamediDatas = $samediDatasManager->getAllSamediDatas();
        $ret = array();
        foreach ($allSamediDatas as $sd) {
            $ret[$sd->getPracticeId()][$sd->getCategoryId()] = $sd;
        }

        return $ret;
    }

    public function getVisibleFieldsNames()
    {
        $arr = array('Doctor', 'Location', 'Practice Id', 'Category Id', 'Event Types', 'update doctor tts');

        /*	=>'firstName',
         =>'LocationsDtos->name'
         =>'SamediDataDto->practiceId'
         =>'SamediEventTypesCachesDto->eventTypes'*/

        return $arr;
    }

    public function getTableDataRows()
    {

        return array();
    }

    /**
     * Return a thing based on $args parameter
     *
     * @abstract
     * @access
     *
     * @param $args
     *
     * @return $loads
     */
    public function getDefaultLoads($args)
    {

        $loads = array();

        return $loads;
    }

    /**
     * Identify view tempalte
     *
     * @abstract
     * @access
     *
     * @param
     *
     * @return  *.tpl file
     */
    public function getTemplate()
    {
        return "admins/importdoctors/body.tpl";
    }
}
