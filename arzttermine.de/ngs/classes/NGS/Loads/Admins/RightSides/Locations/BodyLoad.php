<?php

namespace NGS\Loads\Admins\RightSides\Integrations;

use NGS\Loads\Admins\TableViewBaseLoads\TableViewBodyBaseLoad;
use NGS\Managers\LocationsManager;

/**
 *
 * <p><i>This is an exmaple of Load file.</i></p>
 * This file is using for outputing template(*.tpl) files.
 *
 * <p>In this file identified nested loads to help separatly controll each part of view.</p>
 * Identifing nested load using in tpl file- "ngs_nested_example" property you can load custom solutions in NgsNestedExampleLoad.calss.php file
 *
 * <p><b>NgsExampleLoad</b> class is extended class from <b>AbstractLoad.</b></p>
 *
 * @author  Naghashyan Solutions, e-mail: info@naghashyan.com
 * @version 1.0
 * @package load
 */
class BodyLoad extends TableViewBodyBaseLoad {

	public function load() {
		parent::load();
		
	}
	
	public function getVisibleFieldsNames()
	{
		return array("id" => "ID", "slug" => "Slug", "status" => "Status", "integrationId" => "Integration Id", "name" => "Name",
															"street" => "Street", "zip" => "Zip", "city" => "City", "lat" => "Latitude",
															"lng" => "Longitude", "www" => "WWW", "email" => "Email", 
															"phone" => "Phone", "phoneVisible" => "Phone Visible",
															"fax" => "Fax", "faxVisible" => "faxVisible",
															"newsletterSubscription" => "Newsletter Subscription", "memberIds" => "Member Ids", 
															"medicalSpecialtyIds" => "Medical Specialty Ids",
															"positionFactor" => "Position Factor", "ratingAverage" => "Rating Average","rating1" => "Rating 1",
															"rating2" => "Rating 2","rating3" => "Rating 3",
															"info1" => "Info 1","infoSeo" => "Info Seo","htmlTitle" => "Html Title",
															"htmlMetaDescription" => "Html Meta Description","htmlMetaKeywords" => "Html Meta Keywords",
															"commentIntern" => "Comment Intern","profileAssetId" => "Profile Asset Id","createdAt" => "Created At",
															"createdBy" => "Created By","updatedAt" => "Updated At","updatedBy" => "Updated By");			
	}
	
	public function getTableDataRows()
	{
		$locationsManager = LocationsManager::getInstance($this->config, $this->args);	
		$allLocations = $locationsManager->getAllLocations($this->headLoad->offset, $this->headLoad->limit, 'id');	
		return $allLocations;
	}
	
	/**
	* return type should be following
	* array('fieldName' => array('value'=>'DisplayValue'))
	*/
	function getFieldsDisplayValues()
	{
		$arr=array('status'=>
		array(
			LOCATION_STATUS_DRAFT => $GLOBALS["CONFIG"]["LOCATION_STATUS"][LOCATION_STATUS_DRAFT],
			LOCATION_STATUS_ACTIVE => $GLOBALS["CONFIG"]["LOCATION_STATUS"][LOCATION_STATUS_ACTIVE],
			LOCATION_STATUS_VISIBLE => $GLOBALS["CONFIG"]["LOCATION_STATUS"][LOCATION_STATUS_VISIBLE],
			LOCATION_STATUS_VISIBLE_APPOINTMENTS => $GLOBALS["CONFIG"]["LOCATION_STATUS"][LOCATION_STATUS_VISIBLE_APPOINTMENTS]
		)
					
		);
		
		return $arr;		
	}

	/**
	 * Return a thing based on $args parameter
	 * @abstract
	 * @access
	 * @param $args
	 * @return $loads
	 */
	public function getDefaultLoads($args) {

		$loads = array();

		return $loads;
	}

	/**
	 * Identify view tempalte
	 *
	 * @abstract
	 * @access
	 * @param
	 * @return  *.tpl file
	 */
	public function getTemplate() {
		return "admins/locations/body.tpl";
	}

	
}
