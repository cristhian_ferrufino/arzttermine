<?php

namespace NGS\Loads\Admins\RightSides\Integrations\Samedi;

use NGS\Loads\Admins\TableViewBaseLoads\TableViewHeadBaseLoad;
use NGS\Managers\SamediDatasManager;

/**
 *
 * <p><i>This is an exmaple of Load file.</i></p>
 * This file is using for outputing template(*.tpl) files.
 *
 * <p>In this file identified nested loads to help separatly controll each part of view.</p>
 * Identifing nested load using in tpl file- "ngs_nested_example" property you can load custom solutions in NgsNestedExampleLoad.calss.php file
 *
 * <p><b>NgsExampleLoad</b> class is extended class from <b>AbstractLoad.</b></p>
 *
 * @author  Naghashyan Solutions, e-mail: info@naghashyan.com
 * @version 1.0
 * @package load
 */
class HeadLoad extends TableViewHeadBaseLoad {

    public function load()
    {
        parent::load();
        if (isset($_REQUEST['search'])) {
            $search = $this->secure($_REQUEST['search']);
            $this->addParam('search', $search);
        }
        $searchInfo = $this->getVisibleNamesAndInputTypes();
        if (isset($_REQUEST['error'])) {
            $error = $_REQUEST['error'];
            $this->addParam("error", $error);
        }

        $this->addParam("searchInfo", $searchInfo);
        $this->addParam('selectDisplayValues', null); // Something vanished and wasn't fixed here
        $this->addParam('path', 'integrations/samedi');
        $this->addParam('mapperName', 'SamediDatasMapper');
    }

    public function getVisibleNamesAndInputTypes()
    {
        $arr = array(
            "id"         => array('name' => 'ID', 'type' => 'text'),
            "userId"     => array('name' => 'Arzt', 'type' => 'text'),
            "locationId" => array('name' => 'Praxis', 'type' => 'text'),
            "categoryId" => array('name' => 'Category ID', 'type' => 'text'),
            "practiceId" => array('name' => 'Practice ID', 'type' => 'text'),
        );

        $array = array();
        $request = array_merge($_POST, $_GET);
        foreach ($request as $key => $value) {
            if (in_array($key, array_keys($arr)) && isset($request[$key]) && (!empty($request[$key]) && isset($request[$key . "_searchStyle"]) && !is_null($request[$key . "_searchStyle"]))
                || (empty($request[$key]) && isset($request[$key . "_searchStyle"]) && (($request[$key . "_searchStyle"] == 6) || ($request[$key . "_searchStyle"] == 7)))
            ) {
                $array[$key] = $arr[$key];
                $array[$key]['value'] = $request[$key];
                if (isset($request[$key . '1'])) {
                    $array[$key]['value1'] = $request[$key . '1'];
                }
                $array[$key]['searchStyle'] = $request[$key . '_searchStyle'];
            }
        }

        return $array;
    }

    public function getSelectDisplayValues()
    {
    }

    public function getTotalRowsCount()
    {
        $samediDatasManager = SamediDatasManager::getInstance($this->config, $this->args);
        $searchArray = $this->getSearchArray();

        return $samediDatasManager->getAllSamediDatasCount($searchArray);
    }

    /**
     * Identify view tempalte
     *
     * @abstract
     * @access
     *
     * @param
     *
     * @return  *.tpl file
     */
    public function getTemplate()
    {
        return "admins/integrations/samedi/head.tpl";
    }
}
