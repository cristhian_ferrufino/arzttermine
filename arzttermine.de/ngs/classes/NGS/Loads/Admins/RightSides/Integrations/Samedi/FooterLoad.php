<?php

namespace NGS\Loads\Admins\RightSides\Integrations\Samedi;

use NGS\Loads\Admins\TableViewBaseLoads\TableViewFooterBaseLoad;

class FooterLoad extends TableViewFooterBaseLoad {

    /**
     * Return a thing based on $args parameter
     *
     * @abstract
     * @access
     *
     * @param $args
     *
     * @return $loads
     */
    public function getDefaultLoads($args)
    {

        $loads = array();

        return $loads;
    }

    /**
     * Identify view tempalte
     *
     * @abstract
     * @access
     *
     * @param
     *
     * @return  *.tpl file
     */
    public function getTemplate()
    {
        return "admins/integrations/samedi/footer.tpl";
    }
}
