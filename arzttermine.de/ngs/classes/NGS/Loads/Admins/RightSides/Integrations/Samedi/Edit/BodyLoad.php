<?php

namespace NGS\Loads\Admins\RightSides\Integrations\Samedi\Edit;

use NGS\Loads\Admins\FormViewLoad\FormViewLoad;
use NGS\Managers\SamediDatasManager;

class BodyLoad extends FormViewLoad {

    public function load()
    {
        parent::load();
        $samediDataId = intval($this->getGet('samedi_data_id'));
        $samediDatasManager = SamediDatasManager::getInstance($this->config, $this->args);
        $samediData = $samediDatasManager->getSamediDataById($samediDataId);
        if (isset($_REQUEST['error'])) {
            $error = $_REQUEST['error'];
            $this->addParam("error", $error);
        }
        $adminId = $this->getAdmin()->getId();
        
        $this->addParam('updatedBy', $adminId);
        $this->addParam('samediData', $samediData);
        $this->addParam('path', 'integrations/samedi');
        $this->addParam('mapperName', 'SamediDatasMapper');
    }

    public function getVisibleNamesAndInputTypes()
    {
        $arr = array(
            "id"         => array('name' => 'ID'),
            "userId"     => array('name' => 'Arzt', 'type' => 'text'),
            "locationId" => array('name' => 'Praxis', 'type' => 'text'),
            "categoryId" => array('name' => 'Category ID', 'type' => 'text'),
            "practiceId" => array('name' => 'Practice ID', 'type' => 'text'),
            "createdAt"  => array('name' => 'Erstellungsdatum'),
            "createdBy"  => array('name' => 'Erstellt von'),
            "updatedAt"  => array('name' => 'Letztes Update'),
            "updatedBy"  => array('name' => 'Letzter Bearbeiter'),
        );

        return $arr;
    }

    public function getTemplate()
    {
        return "admins/integrations/samedi/edit/body.tpl";
    }
}
