<?php

namespace NGS\Loads\Admins\RightSides\Integrations\Samedi;

use NGS\Loads\Admins\FormViewLoad\FormViewLoad;
use NGS\Managers\SamediDatasManager;

class BodyLoad extends FormViewLoad {
	
	public function load() {
		parent::load();
		$samediDataId=$_REQUEST['samedi_data_id'];
		$samediDatasManager=SamediDatasManager::getInstance($this->config, $this->args);
		$samediData=$samediDatasManager->getSamediDataById($samediDataId);
		$this->addParam('samediData',$samediData);
		$this->addParam('path','integrations/samedi');
	}
	
	public function getVisibleNamesAndInputTypes(){
		
		$arr=array(
			"id"=>array('name'=>'ID'),
			"userId"=>array('name'=>'Arzt'),
			"locationId"=> array('name'=>'Praxis'),
			"categoryId"=> array('name'=>'Category ID'),
			"practiceId"=> array('name'=>'Practice ID'),
			"createdAt"=>array('name'=>'Erstellungsdatum'),
			"createdBy"=>array('name'=>'Erstellt von'),
			"updatedAt"=>array('name'=>'Letztes Update'),
			"updatedBy"=>array('name'=>'Letzter Bearbeiter'),
		);
		
		return $arr;
	}
	
	
	public function getTemplate() {
		return "admins/integrations/samedi/delete/body.tpl";
	}
}