<?php

namespace NGS\Loads\Admins\RightSides\Integrations\Samedi\Search;

use NGS\Loads\Admins\SearchViewLoad\SearchViewLoad;

class BodyLoad extends SearchViewLoad {
	
	public function load() {
		parent::load();
		$this->addParam('path','integrations/samedi');
		$this->addParam('mapperName','SamediDatasMapper');
	}
	
	public function getVisibleNamesAndInputTypes(){
		
		$arr=array(
			"id"=>array('name'=>'ID', 'type'=>'text'),
			"userId"=>array('name'=>'Arzt', 'type'=>'text'),
			"locationId"=> array('name'=>'Praxis','type'=>'text'),
			"categoryId"=> array('name'=>'Category ID', 'type'=>'text'),
			"practiceId"=>array('name'=>'Practice ID','type'=>'text'),
		);
		
		return $arr;
	}
	
	public function getSelectDisplayValues(){
		
	}
	
	public function getTemplate() {
		return "admins/integrations/samedi/search/body.tpl";
	}
}
