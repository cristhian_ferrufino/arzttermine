<?php

namespace NGS\Loads\Admins\RightSides\Integrations\Samedi;

use NGS\Loads\Admins\TableViewBaseLoads\TableViewBodyBaseLoad;
use NGS\Managers\UsersManager;

class BodyLoad extends TableViewBodyBaseLoad {

    public function getVisibleFieldsNames()
    {
        return array(
            "id"         => "ID",
            "userId"     => "Doctor",
            "locationId" => "Location",
            "categoryId" => "Category ID",
            "practiceId" => "Practice ID",
            "createdBy"  => "Created By"
        );
    }

    public function getTableDataRows()
    {
        $usersManager = UsersManager::getInstance($this->config, $this->args);

        $searchArray = $this->getSearchArray();

        if (isset($this->args['orderBy'])) {

            $pieces = preg_split('/(?=[A-Z])/', $this->args['orderBy']);

            for ($i = 0; $i < count($pieces); $i++) {
                $pieces[$i] = lcfirst($pieces[$i]);
            }
            $orderBy = implode('_', $pieces);

            switch ($this->args['direction']) {
                case 'asc':
                    $ascending = true;
                    break;
                case 'desc':
                    $ascending = false;
                    break;
                default:
                    $ascending = true;
                    break;
            }
            $allDoctors = $usersManager->getDoctorsFull($this->headLoad->offset, $this->headLoad->limit, $orderBy, $ascending, $searchArray);
        } else {
            $allDoctors = $usersManager->getDoctorsFull($this->headLoad->offset, $this->headLoad->limit, 'samedi_datas_id', false, $searchArray);
        }

        return $allDoctors;
    }

    /**
     * Return a thing based on $args parameter
     *
     * @abstract
     * @access
     *
     * @param $args
     *
     * @return $loads
     */
    public function getDefaultLoads($args)
    {
        $loads = array();

        return $loads;
    }

    /**
     * Identify view tempalte
     *
     * @abstract
     * @access
     *
     * @param
     *
     * @return  *.tpl file
     */
    public function getTemplate()
    {
        return "admins/integrations/samedi/body.tpl";
    }
}
