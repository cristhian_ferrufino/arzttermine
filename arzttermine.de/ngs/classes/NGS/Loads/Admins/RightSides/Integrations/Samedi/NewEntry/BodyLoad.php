<?php

namespace NGS\Loads\Admins\RightSides\Integrations\Samedi\NewEntry;

use NGS\Loads\Admins\FormViewLoad\FormViewLoad;

class BodyLoad extends FormViewLoad {
	
	public function load() {
		parent::load();
		$adminId=$this->getAdmin()->getId();
		
		$this->addParam('createdBy',$adminId);
		$this->addParam('path','integrations/samedi');
		$this->addParam('mapperName','SamediDatasMapper');
	}
	
	public function getVisibleNamesAndInputTypes(){
		
		$arr=array(
			"userId"=>array('name'=>'Arzt', 'type'=>'text'),
			"locationId"=> array('name'=>'Praxis','type'=>'text'),
			"categoryId"=> array('name'=>'Category ID', 'type'=>'text'),
			"practiceId"=>array('name'=>'Practice ID','type'=>'text'),
		);
		
		return $arr;
	}
	
	public function getSelectDisplayValues(){
		
	}
	
	public function getTemplate() {
		return "admins/integrations/samedi/newentry/body.tpl";
	}
}
