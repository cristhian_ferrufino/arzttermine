<?php

namespace NGS\Loads\Admins\RightSides\SamediBookings\Cancel;

use Arzttermine\Booking\Booking;
use NGS\Loads\Admins\FormViewLoad\FormViewLoad;
use NGS\Managers\BookingsManager;

class BodyLoad extends FormViewLoad {
	
	public function load() {
		parent::load();
		$bookingManager=BookingsManager::getInstance($this->config, $this->args);
		$bookingId=$_REQUEST['booking_id'];
		$bookingDto=$bookingManager->getBookingByIdFull($bookingId);
		
		$this->addParam('booking',$bookingDto);
	}
	
	public function getVisibleNamesAndInputTypes(){
		
		$arr=array(
			"id"=>array('name'=>'ID'),
			"status"=>array('name'=>'Status'),
			"integrationStatus"=> array('name'=>'Integration Booking Status'),
			"userId"=> array('name'=>'Arzt'),
			"locationId"=>array('name'=>'Praxis'),
			"appointmentStartAt"=> array('name'=>'Termin'),
			"gender"=>array('name'=>'F/M'),
			"firstName"=>array('name'=>'Vorname'),
			"lastName"=>array('name'=>'NachName'),
			"phone"=>array('name'=>'Telefon'),
			"insuranceId"=>array('name'=>'Versicherung'),
			"returningVisitor"=>array('name'=>'Dem Arzt bekannt'),
			"treatmenttypeId"=> array('name'=>'Behandlungsgrund'),
			"createdAt"=>array('name'=>'Erstellungsdatum'),
			"createdBy"=>array('name'=>'Erstellt von'),

		);
		
		return $arr;
	}
	
	public function getSelectDisplayValues(){
		return array(
		
		'status'=> Booking::getStatusTextArray(),
		"integrationStatus"=>array(
			BOOKING_INTEGRATION_STATUS_OPEN => $GLOBALS["CONFIG"]["BOOKING_INTEGRATION_STATUS"][BOOKING_INTEGRATION_STATUS_OPEN],
			BOOKING_INTEGRATION_STATUS_BOOKED => $GLOBALS["CONFIG"]["BOOKING_INTEGRATION_STATUS"][BOOKING_INTEGRATION_STATUS_BOOKED],
			BOOKING_INTEGRATION_STATUS_ERROR => $GLOBALS["CONFIG"]["BOOKING_INTEGRATION_STATUS"][BOOKING_INTEGRATION_STATUS_ERROR],
			BOOKING_INTEGRATION_STATUS_CANCELED => $GLOBALS["CONFIG"]["BOOKING_INTEGRATION_STATUS"][BOOKING_INTEGRATION_STATUS_CANCELED]
		),
			
		"returningVisitor" => $GLOBALS["CONFIG"]["ARRAY_NULL_NO_YES"],
		'gender'=>$GLOBALS["CONFIG"]["ARRAY_GENDER"],
		"insuranceId" => $GLOBALS["CONFIG"]["INSURANCES"],
		);
		
	}
	
	public function getTemplate() {
		return "admins/samedibookings/cancel/body.tpl";
	}
}