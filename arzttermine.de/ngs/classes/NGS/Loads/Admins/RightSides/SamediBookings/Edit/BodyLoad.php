<?php

namespace NGS\Loads\Admins\RightSides\SamediBookings\Edit;

use Arzttermine\Booking\Booking;
use NGS\Loads\Admins\FormViewLoad\FormViewLoad;
use NGS\Managers\BookingsManager;

class BodyLoad extends FormViewLoad {

    public function load()
    {
        parent::load();
        $bookingManager = BookingsManager::getInstance($this->config, $this->args);
        $bookingId = intval($this->getGet('booking_id'));
        $bookingDto = $bookingManager->getBookingByIdFull($bookingId);
        
        if (isset($_REQUEST['error'])) {
            $error = $_REQUEST['error'];
            $this->addParam('error', $error);
        }
        $adminId = $this->getAdmin()->getId();

        $this->addParam('updatedBy', $adminId);
        $this->addParam('booking', $bookingDto);
        $this->addParam('path', 'samedibookings');
        $this->addParam('mapperName', 'BookingsMapper');
    }

    public function getVisibleNamesAndInputTypes()
    {
        $arr = array(
            "id"                 => array('name' => 'ID', 'dto' => 'booking'),
            "status"             => array('name' => 'Status', 'type' => 'select', 'dto' => 'booking'),
            "backendMarkerClass" => array('name' => 'BackendMarker', 'type' => 'select', 'dto' => 'booking'),
            "sourcePlatform"     => array('name' => 'Plattform', 'dto' => 'booking'),
            "language"           => array('name' => 'Sparche'),
            "appointmentStartAt" => array('name' => 'Termin', 'dto' => 'booking'),
            "integrationStatus"  => array('name' => 'Integration Booking Status', 'dto' => 'booking'),
            "gender"             => array('name' => 'F/M', 'type' => 'select', 'dto' => 'booking'),
            "firstName"          => array('name' => 'Vorname', 'type' => 'text', 'dto' => 'booking'),
            "lastName"           => array('name' => 'NachName', 'type' => 'text', 'dto' => 'booking'),
            "email"              => array('name' => 'E-Mail', 'type' => 'text', 'dto' => 'booking'),
            "phone"              => array('name' => 'Telefon', 'type' => 'text', 'dto' => 'booking'),
            "insuranceId"        => array('name' => 'Versicherung', 'type' => 'select', 'dto' => 'booking'),
            "returningVisitor"   => array('name' => 'Dem Arzt bekannt', 'dto' => 'booking'),
            "treatmenttypeId"    => array('name' => 'Behandlungsgrund',),
            "userId"             => array('name' => 'Arzt'),
            "locationId"         => array('name' => 'Praxis'),
            "commentIntern"      => array('name' => 'Kommentar (intern)', 'type' => 'textarea', 'dto' => 'booking'),
            "integrationLog"     => array('name' => 'Integration-Log', 'type' => 'textarea', 'dto' => 'booking'),
            "createdAt"          => array('name' => 'Erstellungsdatum', 'dto' => 'booking'),
            "createdBy"          => array('name' => 'Erstellt von'),
            "updatedAt"          => array('name' => 'Letztes Update', 'dto' => 'booking'),
            "updatedBy"          => array('name' => 'Letzter Bearbeiter'),
        );

        return $arr;
    }

    public function getSelectDisplayValues()
    {
        return array(

            'status'             => Booking::getStatusTextArray(),
            "integrationStatus"  => array(
                BOOKING_INTEGRATION_STATUS_OPEN     => $GLOBALS["CONFIG"]["BOOKING_INTEGRATION_STATUS"][BOOKING_INTEGRATION_STATUS_OPEN],
                BOOKING_INTEGRATION_STATUS_BOOKED   => $GLOBALS["CONFIG"]["BOOKING_INTEGRATION_STATUS"][BOOKING_INTEGRATION_STATUS_BOOKED],
                BOOKING_INTEGRATION_STATUS_ERROR    => $GLOBALS["CONFIG"]["BOOKING_INTEGRATION_STATUS"][BOOKING_INTEGRATION_STATUS_ERROR],
                BOOKING_INTEGRATION_STATUS_CANCELED => $GLOBALS["CONFIG"]["BOOKING_INTEGRATION_STATUS"][BOOKING_INTEGRATION_STATUS_CANCELED]
            ),

            "returningVisitor"   => $GLOBALS["CONFIG"]["ARRAY_NULL_NO_YES"],
            'gender'             => $GLOBALS["CONFIG"]["ARRAY_GENDER"],
            "insuranceId"        => $GLOBALS["CONFIG"]["INSURANCES"],
            "backendMarkerClass" => array('green' => 'Grün', 'blue' => 'Blau', 'orange' => 'Orange', 'red' => 'Rot')
        );
    }

    public function getTemplate()
    {
        return "admins/samedibookings/edit/body.tpl";
    }
}