<?php

namespace NGS\Loads\Admins\RightSides\SamediBookings;

use Arzttermine\Booking\Booking;
use NGS\Loads\Admins\TableViewBaseLoads\TableViewBodyBaseLoad;
use NGS\Managers\SamediBookingsManager;

/**
 *
 * <p><i>This is an exmaple of Load file.</i></p>
 * This file is using for outputing template(*.tpl) files.
 *
 * <p>In this file identified nested loads to help separatly controll each part of view.</p>
 * Identifing nested load using in tpl file- "ngs_nested_example" property you can load custom solutions in NgsNestedExampleLoad.calss.php file
 *
 * <p><b>NgsExampleLoad</b> class is extended class from <b>AbstractLoad.</b></p>
 *
 * @author  Naghashyan Solutions, e-mail: info@naghashyan.com
 * @version 1.0
 * @package load
 */
class BodyLoad extends TableViewBodyBaseLoad {

	public function load() {
		parent::load();
	}
	/**
	 * Return a thing based on $args parameter
	 * @abstract
	 * @access
	 * @param $args
	 * @return $loads
	 */
	 
	public function getVisibleFieldsNames()
	{	
		return array(	
					"id" => "ID","status" => "Status", "bookingId" => "Arzttermin Booking ID",
					"practiceId" => "Samedi Practice ID", "categoryId" => "Samedi Doctor ID",
					"eventTypeId" => "Samedi Event Type ID", "cancelableUntil" => "Cancelable Until",					
					"createdAt" => "Post Time", "createdBy" => "Created By"
					);			
	}
	 
	 public function getTableDataRows(){
	 	
	 	$samediBookingsManager = SamediBookingsManager::getInstance($this->config, $this->args);
		
		$searchArray=$this->getSearchArray();
		if(isset($this->args['orderBy'])) {
			
			$pieces=preg_split('/(?=[A-Z])/',$this->args['orderBy']);
			
			for ($i=0; $i <count($pieces) ; $i++) { 
				$pieces[$i]=lcfirst($pieces[$i]);
			}
			$orderBy=implode('_', $pieces);
			//var_dump($orderBy);exit;
			switch ($this->args['direction']) {
				case 'asc':
					$ascending=true;
					break;
				case 'desc':
					$ascending=false;
					break;
				default:
					$ascending=true;
					break;
			}
			$allBookings = $samediBookingsManager->getBookingsFull($this->headLoad->offset, $this->headLoad->limit, $orderBy,$ascending,$searchArray);
		} else {
			$allBookings = $samediBookingsManager->getBookingsFull($this->headLoad->offset, $this->headLoad->limit, 'samedi_bookings_id', false,$searchArray);
		}
		return $allBookings;
	}

	function getFieldsDisplayValues(){
	 	$arr = array(
			'status'=>
				array(
					0 => Booking::findStatusText(Booking::STATUS_RECEIVED),
					1 => Booking::findStatusText(Booking::STATUS_INVALID)
				)
		);
		return $arr;
	}

	public function getDefaultLoads($args) {
		$loads = array();
		return $loads;
	}

	/**
	 * Identify view tempalte
	 *
	 * @abstract
	 * @access
	 * @param
	 * @return  *.tpl file
	 */
	public function getTemplate() {
		return "admins/samedibookings/body.tpl";
	}
}
