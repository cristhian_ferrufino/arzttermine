<?php

namespace NGS\Loads\Admins\RightSides\SamediBookings\Email;

use NGS\Loads\Admins\BaseAdminLoad;
use NGS\Managers\BookingsManager;

class MailingLoad extends BaseAdminLoad {

	public function load() {
		$bookingsManager = BookingsManager::getInstance($this->config, $this->args);
		
		if(isset($_REQUEST["booking_id_for_email"])){
			$bookingId=$_REQUEST["booking_id_for_email"];
			
			$bookingDto=$bookingsManager->getBookingByIdFull($bookingId);
			$this->addParam('bookingDto',$bookingDto);
			
			$date = $bookingDto->getAppointmentStartAt();
			$appointmentWeekDayIndex=(date('w', strtotime($date)));
			$appointmentWeekDay=$GLOBALS["CONFIG"]["CALENDAR_WEEKDAY_TEXTS"][$appointmentWeekDayIndex];
			$this->addParam('appointmentWeekDay',$appointmentWeekDay);
			$genderArray=$GLOBALS["CONFIG"]["ARRAY_GENDER"];
			$this->addParam('genderArray',$genderArray);
		}
	}
	
	public function getDefaultLoads($args) {
			
		$loads = array();

		return $loads;
	}


	function getTemplate() {
		return "admins/samedibookings/email/mailing.tpl";
	}

	
}
