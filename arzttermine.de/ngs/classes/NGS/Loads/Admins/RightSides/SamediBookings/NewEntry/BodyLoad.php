<?php

namespace NGS\Loads\Admins\RightSides\SamediBookings\NewEntry;

use Arzttermine\Booking\Booking;
use NGS\Loads\Admins\FormViewLoad\FormViewLoad;

class BodyLoad extends FormViewLoad {

    public function load()
    {
        parent::load();
        $adminId = $this->getAdmin()->getId();

        $this->addParam('createdBy', $adminId);
        $this->addParam('path', 'samedibookings');
        $this->addParam('mapperName', 'BookingsMapper');
    }

    public function getVisibleNamesAndInputTypes()
    {

        $arr = array(
            "status"             => array('name' => 'Status', 'type' => 'select'),
            "backendMarkerClass" => array('name' => 'BackendMarker', 'type' => 'select'),
            "termin"             => array('name' => 'Termin'),
            "gender"             => array('name' => 'F/M', 'type' => 'select'),
            "firstName"          => array('name' => 'Vorname', 'type' => 'text'),
            "lastName"           => array('name' => 'NachName', 'type' => 'text'),
            "email"              => array('name' => 'E-Mail', 'type' => 'text'),
            "phone"              => array('name' => 'Telefon', 'type' => 'text'),
            "insuranceId"        => array('name' => 'Versicherung', 'type' => 'select'),
            "returningVisitor"   => array('name' => 'Dem Arzt bekannt', 'value' => '%'),
            "treatmenttypeId"    => array('name' => 'Behandlungsgrund'),
            "locationId"         => array('name' => 'Praxis', 'value' => 'n/a'),
            "commentIntern"      => array('name' => 'Kommentar (intern)', 'type' => 'textarea'),
        );

        return $arr;
    }

    public function getSelectDisplayValues()
    {
        return array(

            'status'             => Booking::getStatusTextArray(),
            'gender'             => $GLOBALS["CONFIG"]["ARRAY_GENDER"],
            "insuranceId"        => $GLOBALS["CONFIG"]["INSURANCES"],
            "backendMarkerClass" => array('green' => 'Grün', 'blue' => 'Blau', 'orange' => 'Orange', 'red' => 'Rot')
        );
    }

    public function getTemplate()
    {
        return "admins/samedibookings/newentry/body.tpl";
    }
}