<?php

namespace NGS\Loads\Admins\RightSides\SamediBookings\Search;

use Arzttermine\Booking\Booking;
use NGS\Loads\Admins\SearchViewLoad\SearchViewLoad;

class BodyLoad extends SearchViewLoad {
	
	public function load() {
		parent::load();
	}
	
	public function getVisibleNamesAndInputTypes(){
		
		$arr=array(
			"id"=>array('name'=>'ID','type'=>'text'),
			"status"=>array('name'=>'Status', 'type'=>'select'),
			"bookingId"=> array('name'=>'Booking Id','type'=>'text')
		);
		
		return $arr;
	}
	
	public function getSelectDisplayValues(){
		return array(

				'status'=>
				array(
						0 => Booking::findStatusText(Booking::STATUS_RECEIVED),
						1 => Booking::findStatusText(Booking::STATUS_INVALID)
				)
		);
		
	}
	
	public function getTemplate() {
		return "admins/samedibookings/search/body.tpl";
	}
}