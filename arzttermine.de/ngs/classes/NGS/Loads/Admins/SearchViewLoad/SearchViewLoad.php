<?php

namespace NGS\Loads\Admins\SearchViewLoad;

use NGS\Loads\Admins\BaseAdminLoad;

abstract class SearchViewLoad extends BaseAdminLoad {
	
	public function load(){
		$searchInfo=$this->getVisibleNamesAndInputTypes();
		$selectDisplayValues=$this->getSelectDisplayValues();
		
		if (isset($_REQUEST['error'])){
			$error=$_REQUEST['error'];
		}else{
			$error='';
		}
		
		$this->addParam("error",$error);
		$this->addParam("searchInfo", $searchInfo);
		$this->addParam('selectDisplayValues', $selectDisplayValues);
	}
	
	abstract function getVisibleNamesAndInputTypes();
	
	
	function getSelectDisplayValues(){
		return array();
	}
}