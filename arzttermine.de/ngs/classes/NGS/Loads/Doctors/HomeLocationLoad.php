<?php

namespace NGS\Loads\Doctors;

use Arzttermine\Application\Application;
use NGS\DAL\DTO\AssetsDto;
use NGS\DAL\DTO\UsersDto;
use NGS\Managers\MedicalSpecialtiesManager;
use NGS\Managers\AssetsManager;

class HomeLocationLoad extends BaseDoctorLoad {

    public function load()
    {
        // Monkeypatch used in doctors/main.tpl
        define('LOAD_NEW_DESIGN', 'true');

        // If there is no current location, we should redirect to the user profile page
        if (!$this->location->getLocation()) {
            Application::getInstance()->redirect($GLOBALS['CONFIG']['DOCTORS_LOGIN_URL']);
        }

        // Load the specialties for locations
        $this->loadLocationSpecialities();

        $this->addParam('location', $this->location);
        $this->addParam('locations', $this->doctor->getLocations());
        $this->addParam('doctors', $this->location->getMembers());
        
        // Load all available specialties
        $this->loadAllLocationSpecialties();

        // Load specialties relevant to only the location
        $this->addParam("locationsMedSpecIdsArray", explode(",", $this->location->getLocation()->getMedicalSpecialtyIds()));

        $this->loadLocationDoctors();

        $doctors = $this->location->getMembers();
        
        /** @var UsersDto[] $doctors */
        foreach ($doctors as &$doctor) {
            $doctor->profileUrl = $this->getProfile($doctor->getId());
        }
    }

    /**
     * Load in specialties associated with the location attached to the loaded user (probably a doctor)
     *
     * @return void
     */
    protected function loadLocationSpecialities()
    {
        // Medical specialities manager
        $msm = MedicalSpecialtiesManager::getInstance($this->config, $this->args);
        $specialties = array();

        if ($this->location) {
            foreach (explode(',', $this->location->getLocation()->getMedicalSpecialtyIds()) as $speciality_id) {
                $specialties[] = $msm->getById($speciality_id);
            }
        }

        $this->addParam('specialties', $specialties);
    }

    /**
     * Load in specialties associated with the locations attached to the loaded user (probably a doctor)
     *
     * @return void
     */
    protected function loadAllLocationSpecialties()
    {
        // Medical specialities manager
        $msm = MedicalSpecialtiesManager::getInstance($this->config, $this->args);

        // All specialties (for JS selection)
        $all_specialities = $msm->getAllMedicalSpecialtiesWithTreatmentTypes();
        $this->addParam("all_specialties", $all_specialities);
    }

    /**
     * Load in specialties associated with the location attached to the loaded user (probably a doctor)
     *
     * @return void
     */
    protected function loadLocationDoctors()
    {
        // Medical specialities manager
        $msm = MedicalSpecialtiesManager::getInstance($this->config, $this->args);
        $specialties = array();

        if ($this->location->getLocation()) {
            foreach ($this->location->getMembers() as $doctor) {
                foreach (explode(',', $this->location->getLocation()->getMedicalSpecialtyIds()) as $specialty_id) {
                    $specialties[$doctor->getId()][] = $msm->getById($specialty_id);
                }
            }
        }
    }

    /**
     * Awful and bad code copy/paste job just for this page. IGNORE PLEASE
     *
     * @param int $id_doctors
     *
     * @return string
     */
    public function getProfile($id_doctors)
    {
        $url = '';

        $am = AssetsManager::getInstance();

        /**
         * @var AssetsDto $childAssetDto
         */
        if ($profileAssetDto = $am->getProfileAsset($id_doctors)) {
            $profileChildAssetsDtos = $am->getProfileChildAssets($profileAssetDto->getId());

            if ($profileChildAssetsDtos && $profileChildAssetsDtos[ASSET_GFX_SIZE_THUMBNAIL]) {
                $childAssetDto = $profileChildAssetsDtos[ASSET_GFX_SIZE_THUMBNAIL];
                $_id = sprintf("%09d", $childAssetDto->getParentId());
                $path = substr($_id, 0, 3) . '/' . substr($_id, 3, 3) . '/' . substr($_id, 6, 3);
                $profile_image_url = $GLOBALS["CONFIG"]["ASSET_URL"] . $path . '/' . $childAssetDto->getFileName();
            } else {
                $_id = sprintf("%09d", $profileAssetDto->getId());
                $path = substr($_id, 0, 3) . '/' . substr($_id, 3, 3) . '/' . substr($_id, 6, 3);
                $profile_image_url = $GLOBALS["CONFIG"]["ASSET_URL"] . $path . '/' . $profileAssetDto->getFileName();
            }

            $url = $profile_image_url;
        }

        return $url;
    }

    /**
     * @param array $args
     *
     * @return array
     */
    public function getDefaultLoads($args)
    {
        return array();
    }

    /**
     * @param string $namespace
     * @param string $load
     *
     * @return bool
     */
    public function isValidLoad($namespace, $load)
    {
        return true;
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        return "doctors/home-location.tpl";
    }

    /**
     * @return bool
     */
    public function isMain()
    {
        return true;
    }

    /**
     * Load in specialties associated with the locations attached to the loaded user (probably a doctor)
     *
     * @return void
     */
    protected function loadLocationSpecialties()
    {
        // Medical specialities manager
        $msm = MedicalSpecialtiesManager::getInstance($this->config, $this->args);

        // All specialties (for JS selection)
        $specialities = $msm->getAllMedicalSpecialtiesWithTreatmentTypes();
        $this->addParam("specialties", $specialities);
    }
}
