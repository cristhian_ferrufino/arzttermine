<?php

namespace NGS\Loads\Doctors;

use NGS\Loads\BaseLoad;
use NGS\Security\RequestGroups;
use Arzttermine\Cms\CmsContent;
use Arzttermine\MedicalSpecialty\MedicalSpecialty;

class RegistrationLoad extends BaseLoad {

    public function load()
    {
        // Monkeypatch used in doctors/main.tpl
        define('LOAD_NEW_DESIGN', 'true');

        //load the AGB text from the database
        $agbText = '';
        $content = new CmsContent();
        if ($content->loadByUrl('/agb') && $content->isValid()) {
            $agbText = $content->getActiveVersion()->getContent1();
        }
        $this->addParam("agbText", $agbText);

        if ($this->getRegistrationStep() == 1 && !isset($_SESSION['userId'])) {
            session_destroy();
        }
        if ($this->getRegistrationStep() == 2) {
            $this->addParam("medicalSpecialtiesOptions", MedicalSpecialty::getHtmlOptions());
        }
        if ($this->getRegistrationStep() == 4) {
            $this->addParam("isFinished", true);
        }
        if ($this->getRegistrationStep() > 3) {
            session_destroy();
        }
        if (isset($this->args["error_message"])) {
            $this->addParam("error_message", $this->args["error_message"]);
        }
        if (!isset($this->args["request"])) {
            $this->args["request"] = $this->getEmptyFormData();
        }

        $this->addParam('request', $this->args["request"]);
    }

    /**
     * @return int
     */
    public function getRegistrationStep()
    {
        if (!isset($this->args["step"])) {
            $step = 1;
        } else {
            $step = $this->args["step"];
        }

        return $step;
    }

    /**
     * @return array
     */
    private function getEmptyFormData()
    {
        return array("first_name" => "", "last_name" => "", 'gender' => "", "full_title" => "", "practice_title" => "", "street_name" => "", "street_number" => "", "post_code" => "", "city" => "", "email" => "", "password" => "", "confirm_password" => "");
    }

    /**
     * @param array $args
     *
     * @return array
     */
    public function getDefaultLoads($args)
    {
        $loads = array();

        return $loads;
    }

    /**
     * @param string $namespace
     * @param string $load
     *
     * @return bool
     */
    public function isValidLoad($namespace, $load)
    {
        return true;
    }

    /**
     * Identify view tempalte
     *
     * @return string
     */
    public function getTemplate()
    {
        switch ($this->getRegistrationStep()) {
            case '1':
                return "doctors/registration_step1.tpl";
            case '2':
                return "doctors/registration_step2.tpl";
            case '3':
                return "doctors/registration_step3.tpl";
            case '4':
                return "doctors/registration_step3.tpl";
        }
        return '';
    }

    /**
     * @return int
     */
    public function getRequestGroup()
    {
        return RequestGroups::$guestRequest;
    }
}
