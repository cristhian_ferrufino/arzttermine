<?php

namespace NGS\Loads\Doctors;

use NGS\DAL\DTO\DoctorsDto;
use NGS\Managers\MedicalSpecialtiesManager;

class ManageMedicalSpecialtiesLoad extends BaseDoctorLoad {

    public function load() {
		
        if (isset($this->args['error'])) {
            $this->addParam('error', $this->args['error']);
        }
		
        $msm = MedicalSpecialtiesManager::getInstance($this->config, $this->args);
        
		$allMedicalSpecialtiesWithTreatmentTypes = $msm->getAllMedicalSpecialtiesWithTreatmentTypes();
		$doctorDtoWithMedSpec = $this->usersManager->getDoctorWithMedicalSpecialtiesAndTreatmentTypes($this->doctor->getDoctor()->getId());
        $doctorsMedSpecIdsArray = $this->getDoctorsMedSpecIdsArray($doctorDtoWithMedSpec);
        
		$this->addParam('doctor', $this->doctor);
		$this->addParam('locations', $this->doctor->getLocations());
        $this->addParam("specialties", $doctorsMedSpecIdsArray);
        $this->addParam('doctorWithMedSpec', $doctorDtoWithMedSpec);
		$this->addParam("all_specialties", $allMedicalSpecialtiesWithTreatmentTypes);
    }
    
	
    protected function getDoctorsMedSpecIdsArray(DoctorsDto $doctorDto)
    {
        $specialties = array();
		
        if ($msids = $doctorDto->getMedicalSpecialtyIds()) {
            foreach (explode(',', $msids) as $msid) {
                $specialties[] = $msid;
            }
            array_filter($specialties);
        }
		
        return $specialties;
    }

	
    /**
     * Return a thing based on $args parameter
	 * 
     * @param $args
	 * 
     * @return array
     */
    public function getDefaultLoads($args) {
        return array();
    }

	
    /**
     * Using for validation whether as certain load with nesting
     * Return a thing based on $namespace, $load parameters
	 * 
     * @param $namespace
	 * @param $load
	 * 
     * @return bool
     */
    public function isValidLoad($namespace, $load) {
        return true;
    }

	
    /**
     * Identify view template
     *
     * @return string *.tpl file
     */
    public function getTemplate() {
        return "doctors/manage_medical_specialties.tpl";
    }

}
