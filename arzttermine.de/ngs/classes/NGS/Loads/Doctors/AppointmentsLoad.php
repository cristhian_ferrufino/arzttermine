<?php

namespace NGS\Loads\Doctors;

use NGS\Framework\Dispatcher;

class AppointmentsLoad extends BaseDoctorLoad {
    
    public function load() {
        // Monkeypatch used in doctors/main.tpl
        define('LOAD_NEW_DESIGN', 'true');

        $this->addParam('doctor', $this->doctor);
        $this->addParam('locations', $this->doctor->getLocations());
    }

    public function getWeek(){
		$week = $this->getRequestParam('woche', date('W', time()));
		
        $this->addParam("week", $week);
		
        return $week;
    }
   
    /**
     * Return a thing based on $args parameter
	 * 
     * @param array $args
	 * 
     * @return array $loads
     */
    public function getDefaultLoads($args) {
		$loads = array();
        $loads["calendar"]["load"] = Dispatcher::buildClassName('loads', 'Doctors', 'CalendarLoad');
        $this->args["parentLoad"] = &$this;
        
		$loads["calendar"]["args"] = $this->args;
		$loads["calendar"]["loads"] = array();
		$this->addParam("contentLoad", 'calendar');
		return $loads;
    }

    /**
     * Using for validation whether as certain load with nesting
	 * 
     * @param $namespace
	 * @param $load
	 * 
     * @return bool true
     */
    public function isValidLoad($namespace, $load) {
        return true;
    }

    /**
     * Identify view tempalte
     *
     * @return string *.tpl file
     */
    public function getTemplate() {
        return "doctors/appointments.tpl";
    }

}
