<?php

namespace NGS\Loads\Doctors;

use NGS\Managers\UsersEditHistoryManager;

class ManageProfileLoad extends BaseDoctorLoad {

    public function load()
    {
        $this->form_data = array();

        if (isset($this->args['form_data'])) {
            $this->form_data = $this->args['form_data'];
        }

        if (!empty($this->args['error'])) {
            $this->addParam('error', $this->args['error']);
        }

        if ($this->isPost() && $id_users = intval($this->getPost('uid'))) {
            $uehm = UsersEditHistoryManager::getInstance($this->config, $this->args);

            if ($userEditHistory = $uehm->getById(intval($id_users))) {
                $this->form_data['docinfo1'] = $userEditHistory->getDocinfo1();
                $this->form_data['docinfo2'] = $userEditHistory->getDocinfo2();
                $this->form_data['docinfo3'] = $userEditHistory->getDocinfo3();
                $this->form_data['docinfo5'] = $userEditHistory->getDocinfo5();
                $this->form_data['docinfo6'] = $userEditHistory->getDocinfo6();
            }
        }

        if (!empty($this->form_data)) {
            $this->addParam('form_data', $this->form_data);
        }

        $this->addParam('doctor', $this->doctor);
        $this->addParam('locations', $this->doctor->getLocations());
    }


    /**
     * @return array
     */
    public function getDefaultLoads()
    {
        return array();
    }


    /**
     * Using for validation whether as certain load with nesting
     *
     * @return bool
     */
    public function isValidLoad()
    {
        return true;
    }


    /**
     * Identify view template
     *
     * @return string
     */
    public function getTemplate()
    {
        return 'doctors/manage_profile.tpl';
    }

}
