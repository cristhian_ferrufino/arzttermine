<?php

namespace NGS\Loads\Doctors;

use Arzttermine\Application\Application;
use NGS\Framework\AbstractSessionManager;
use NGS\Loads\BaseLoad;
use NGS\Managers\AssetsManager;
use NGS\Managers\LocationsManager;
use NGS\Managers\UsersManager;
use NGS\Models\Doctor;
use NGS\Models\Practice;
use NGS\Security\RequestGroups;

abstract class BaseDoctorLoad extends BaseLoad {

    /**
     * @var UsersManager Singleton user manager
     */
    protected $usersManager;

    /**
     * @var AssetsManager Singleton asset manager
     */
    protected $assetsManager;

    /**
     * @var LocationsManager Singleton location manager
     */
    protected $locationsManager;

    /**
     * @var Doctor The current doctor we're looking at
     */
    protected $doctor;

    /**
     * @var Practice The current location we're looking at
     */
    protected $location;

    /**
     * @var array Carries request data from whatever form we might have. Used by all form pages
     */
    protected $form_data = array();

    /**
     * Pseudo constructor
     *
     * @param \SmartyBC $smarty
     * @param AbstractSessionManager $sessionManager
     * @param array $config
     * @param array $loadMapper
     * @param array $args
     *
     * @return void
     */
    public function initialize($smarty, AbstractSessionManager $sessionManager, $config, $loadMapper, $args)
    {
        parent::initialize($smarty, $sessionManager, $config, $loadMapper, $args);

        // Load managers 
        $this->usersManager = UsersManager::getInstance($config, $args);
        $this->assetsManager = AssetsManager::getInstance($config, $args);
        $this->locationsManager = LocationsManager::getInstance($config, $args);

        // Load the doctor we're looking at (NOT necessarily the session doctor)
        $this->doctor = new Doctor();
        $this->doctor->autoload($this->sessionManager->getUser()->getId());

        // Load a location. If it's empty, try load the doctor's first location
        $this->location = new Practice();
        $location_id = null;

        if ($locations = $this->doctor->getLocations()) {
            if (count($locations) && !empty($locations[0])) {
                $location_id = $locations[0]->getId();
            }
        }

        $this->location->autoload($location_id);

        $page = isset($_REQUEST['p']) ? $_REQUEST['p'] : 1;

        // Page param
        $this->addParam("page", $page);

        setlocale(LC_TIME, 'de_DE');
    }

    /**
     * Identify permissions for users in this action file.
     *
     * @return int
     */
    public function getRequestGroup()
    {
        return RequestGroups::$doctorsRequest;
    }

    /**
     * Handler for invalid access
     */
    public function onNoAccess()
    {
        $this->redirect($GLOBALS['CONFIG']['DOCTORS_LOGIN_URL'] . '/login');
    }
}