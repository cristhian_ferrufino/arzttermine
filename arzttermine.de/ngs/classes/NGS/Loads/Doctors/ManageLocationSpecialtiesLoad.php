<?php

namespace NGS\Loads\Doctors;

use NGS\Managers\MedicalSpecialtiesManager;

class ManageLocationSpecialtiesLoad extends BaseDoctorLoad {

	public function load() {
		// If there is no current location, we should redirect to the user profile page
		if (!$this->location->getLocation()) {
			$this->redirect($GLOBALS['CONFIG']['DOCTORS_LOGIN_URL']);
		}
		
		// Load all available specialties
		$this->loadAllLocationSpecialties();
		
		// Load specialties relevant to only the location
		$this->addParam("locationsMedSpecIdsArray", explode(",", $this->location->getLocation()->getMedicalSpecialtyIds()));
		
		if(isset($this->args['error'])){
			$this->addParam('error', $this->args['error']);
		}

		$this->addParam('location', $this->location);
		$this->addParam('doctors', $this->location->getMembers());
		$this->addParam('locations', $this->doctor->getLocations());
	}

	/**
	 * Load in specialties associated with the locations attached to the loaded user (probably a doctor)
	 *
	 * @return void
	 */
	protected function loadAllLocationSpecialties() {
		// Medical specialities manager
		$msm = MedicalSpecialtiesManager::getInstance($this->config, $this->args);

		// All specialties (for JS selection)
		$all_specialities = $msm->getAllMedicalSpecialtiesWithTreatmentTypes();
		$this->addParam("all_specialties", $all_specialities);
	}

	/**
	 * Load in specialties associated with the locations attached to the loaded user (probably a doctor)
	 *
	 * @return void
	 */
	protected function loadLocationSpecialties() {
		// Medical specialities manager
		$msm = MedicalSpecialtiesManager::getInstance($this->config, $this->args);

		// All specialties (for JS selection)
		$specialities = $msm->getAllMedicalSpecialtiesWithTreatmentTypes();
		$this->addParam("specialties", $specialities);
	}

	/**
	 * Return a thing based on $args parameter
	 * 
	 * @param $args
	 * @return array $loads
	 */
	public function getDefaultLoads($args) {
		return array();
	}

	/**
	 * Using for validation whether as certain load with nesting
	 *
	 * Return a thing based on $namespace, $load parameters
	 * 
	 * @param $namespace
	 * @param $load
	 * @return bool
	 */
	public function isValidLoad($namespace, $load) {
		return true;
	}

	/**
	 * Identify view tempalte
	 *
	 * @return string *.tpl file
	 */
	public function getTemplate() {
		return "doctors/manage_location_specialties.tpl";
	}

}