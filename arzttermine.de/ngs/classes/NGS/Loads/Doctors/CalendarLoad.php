<?php

namespace NGS\Loads\Doctors;

use NGS\Framework\DAL\DTO\AbstractDto;
use NGS\Loads\BaseLoad;
use NGS\Managers\BookingsManager;
use NGS\Managers\LocationsManager;
use NGS\Managers\TreatmentTypesManager;
use NGS\Managers\UsersAaManager;
use NGS\Managers\UsersManager;
use NGS\Security\UserGroups;
use NGS\Util\UtilFunctions;

class CalendarLoad extends BaseLoad {

    public function load()
    {
        $userLevel = $this->sessionManager->getSessionUserLevel();
        if ($userLevel == UserGroups::$ADMIN) {
            $userId = $this->args["parentLoad"]->selectedDoctorId;
            $locationId = $this->args["parentLoad"]->selectedLocationId;
            $integrationId = $this->args["parentLoad"]->selectedIntegrationId;
            $this->addParam("location_id", $locationId);
            $this->addParam("integration_id", $integrationId);
            $this->addParam('is_admin', 1);
        } else {
            $this->addParam('is_admin', 0);
            $userId = $this->sessionManager->getSessionUserId();
        }
        
        $locationsManager = LocationsManager::getInstance($this->config, $this->args);
        $usersManager = UsersManager::getInstance($this->config, $this->args);
        $userDto = $usersManager->getUserById($userId);
        $locationsDto = $locationsManager->getLocationsByUserId($userId);
        
        if (!empty($locationsDto) && isset($userDto)) {
            $locationIds = array();
            foreach ($locationsDto as $locationDto) {
                $locationIds[] = $locationDto->getId();
            }
            
            $usersAaManager = UsersAaManager::getInstance($this->config, $this->args);
            $mondayDate = $this->getCalendarMondayDateToShow();
            $fridayDate = date('Y-m-d', strtotime($mondayDate . ' +4 days'));
            if ($userDto->getFixedWeek() == 1) {
                $nextMondayDate = date('Y-m-d', strtotime($mondayDate . ' +7 days'));
                $nextFridayDate = date('Y-m-d', strtotime($nextMondayDate . ' +4 days'));
                $availableAppointmentsNotExtracted = $usersAaManager->getAvailableAppointmentsNotExtracted($nextMondayDate, $nextFridayDate, $userId, $locationIds);
            } else {
                $availableAppointmentsNotExtracted = $usersAaManager->getAvailableAppointmentsNotExtracted($mondayDate, $fridayDate, $userId, $locationIds);
            }
            
            $validDates = array($mondayDate);
            for ($i = 1; $i <= 4; $i++) {
                $validDates[] = date('Y-m-d', strtotime($mondayDate . ' +' . $i . ' days'));
            }
            
            $allAppointmentsNotExtractedConvertedToJson = AbstractDto::dtosToJSON($availableAppointmentsNotExtracted);
            $availableAppointments = $allAppointmentsNotExtractedConvertedToJson;
            $treatmentTypesManager = TreatmentTypesManager::getInstance($this->config, $this->args);
            $doctorAllTreatmentTypes = $treatmentTypesManager->getDoctorAllTreatmentTypes($userId);
            $doctorAllTreatmentTypeIds = array();
            $doctorAllTreatmentTypeNames = array();
            foreach ($doctorAllTreatmentTypes as $tt) {
                $doctorAllTreatmentTypeIds[] = $tt->getId();
                $doctorAllTreatmentTypeNames[] = $tt->getNameDe();
            }
            
            //geting doctor bookings
            $bookingsManager = BookingsManager::getInstance($this->config, $this->args);
            $bookings = $bookingsManager->getBookingsWithTreatmentTypeInDateRangeByUserId($userId, $mondayDate, $fridayDate);

            $this->addParam("bookingsJson", json_encode($bookings));
            $this->addParam("doctorAllTreatmentTypeIds", implode(',', $doctorAllTreatmentTypeIds));
            $this->addParam("doctorAllTreatmentTypeNames", implode(',', $doctorAllTreatmentTypeNames));
            $this->addParam('appointmentsJson', $availableAppointments);
            $this->addParam("validDates", $validDates);
            $this->addParam("user_id", $userId);
            $this->addParam("location_ids", json_encode($locationIds));
            $this->addParam('fixed_week', $userDto->getFixedWeek());
        } else {
            $this->addParam('no_access_error', "Sorry you are not registered in any location.");
        }
        
        $unitDivHeight = 5;
        $calendarTimesFrom = $GLOBALS["CONFIG"]["CALENDAR_VISIBLE_TIMES_FROM"];
        $calendarTimesTo = $GLOBALS["CONFIG"]["CALENDAR_VISIBLE_TIMES_TO"];
        $calendarVisibleTimes = array();
        $defaultIncrement = $GLOBALS["CONFIG"]["APPOINTMENT_DEFAULT_INCREMENT"];
        $time = $calendarTimesFrom;
        
        while ($time <= $calendarTimesTo) {
            $calendarVisibleTimes[] = $time;
            $time = UtilFunctions::addMinutesToTime($time, 60);
        }
        
        $this->addParam('calendarTimesFrom', $calendarTimesFrom);
        $this->addParam('calendarEndTime', $calendarVisibleTimes[count($calendarVisibleTimes) - 1]);
        array_pop($calendarVisibleTimes);
        $this->addParam('unitDivHeight', $unitDivHeight);
        $this->addParam('calendarVisibleTimes', $calendarVisibleTimes);
        $this->addParam('defaultIncrement', $defaultIncrement);
        $this->addParam("monday_date", $mondayDate);
        $this->addParam("next_monday_date", date('Y-m-d', strtotime($mondayDate . ' +7 days')));
        $this->addParam("previous_monday_date", date('Y-m-d', strtotime($mondayDate . ' -7 days')));
    }

    public function getCalendarMondayDateToShow()
    {
        $retMondayDate = date('Y-m-d', strtotime('monday this week'));
        if (isset($_REQUEST["monday_date"])) {
            $mondayDate = $_REQUEST["monday_date"];
            $date = \DateTime::createFromFormat('Y-m-d', $mondayDate);

            if ($date !== false && date('w', strtotime($mondayDate)) == 1) {

                $retMondayDate = $mondayDate;
            }
        }

        return $retMondayDate;
    }

    /**
     * Using for validation whether as certain load with nesting
     *
     * Return a thing based on $namespace, $load parameters
     *
     * @abstract
     * @access
     *
     * @param $namespace , $load
     *
     * @return true
     */
    public function isValidLoad($namespace, $load)
    {
        return true;
    }

    /**
     * Identify view tempalte
     *
     * @abstract
     * @access
     *
     * @param
     *
     * @return  *.tpl file
     */
    public function getTemplate()
    {
        return "doctors/calendar.tpl";
    }
}