<?php

namespace NGS\Loads\Doctors;

use NGS\Managers\AssetsManager;

class ProfilePicturesLoad extends BaseDoctorLoad {

    public function load() {
    // Monkeypatch used in doctors/main.tpl
    define('LOAD_NEW_DESIGN', 'true');

        $assetsManager = AssetsManager::getInstance($this->config, $this->args);
        $assets = $assetsManager->getProfileAssets($this->doctor->getDoctor()->getId());
        $userAssets = array();
        
		// Stupid kludge because of inconsistent return types
		if (count($assets) == 1) {
			$assets = array($assets);
		}
		
		foreach ($assets as $asset) {
			$userAssets[$asset->getId()] = $this->initDoctorAsset($asset);
		}
		
		// Clear out dud URLs
		array_filter($userAssets);
		
        $this->addParam('userAssets', $userAssets);
		
        $error = isset($this->args['error']) ? $this->args['error'] : '';
		
        if (isset($error)) {            
            $this->addParam('error', $error);
        }
		
		$this->addParam('doctor', $this->doctor);
		$this->addParam('locations', $this->doctor->getLocations());
    }

	public function initDoctorAsset($profileAssetDto) {
		if ($profileAssetDto) {
			$paid = $profileAssetDto->getId();
			$profileChildAssetsDtos = $this->assetsManager->getProfileChildAssets($paid);
			
			if ($profileChildAssetsDtos && !empty($profileChildAssetsDtos[ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED])) {
				$childAssetDto = $profileChildAssetsDtos[ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED];
				$_id = sprintf("%09d", $childAssetDto->getParentId());
				$path = substr($_id, 0, 3) . '/' . substr($_id, 3, 3) . '/' . substr($_id, 6, 3);
				$profile_image_url = $GLOBALS["CONFIG"]["ASSET_URL"] . $path . '/' . $childAssetDto->getFileName();
			} else {
				$_id = sprintf("%09d", $profileAssetDto->getId());
				$path = substr($_id, 0, 3) . '/' . substr($_id, 3, 3) . '/' . substr($_id, 6, 3);
				$profile_image_url = $GLOBALS["CONFIG"]["ASSET_URL"] . $path . '/' . $profileAssetDto->getFileName();
			}

			return $profile_image_url;
		}
		
		return '';
	}

    /**
     * Return a thing based on $args parameter
	 * 
     * @param $args
     * @return array
     */
    public function getDefaultLoads($args) {
        return array();
    }

    /**
     * Using for validation whether as certain load with nesting
	 * 
     * @param $namespace
	 * @param $load
     * @return bool
     */
    public function isValidLoad($namespace, $load) {
        return true;
    }

    /**
     * Identify view tempalte
     *
     * @return string *.tpl file
     */
    public function getTemplate() {
        return "doctors/profile_pictures.tpl";
    }

}
