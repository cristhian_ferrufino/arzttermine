<?php

namespace NGS\Loads\Doctors;

use NGS\DAL\DTO\DoctorsDto;
use NGS\DAL\DTO\LocationsDto;
use NGS\Managers\UsersEditHistoryManager;
use NGS\Managers\MedicalSpecialtiesManager;
use NGS\Models\Doctor;
use NGS\Models\Practice;

class HomeLoad extends BaseDoctorLoad {

    public function load()
    {
        // Monkeypatch used in doctors/main.tpl
        define('LOAD_NEW_DESIGN', 'true');

        $usersEditHistoryManager = UsersEditHistoryManager::getInstance($this->config, $this->args);
        $userEditHistory = $usersEditHistoryManager->getByUserId($this->doctor->getDoctor()->getId(), 5);

        $msm = MedicalSpecialtiesManager::getInstance($this->config, $this->args);

        $allMedicalSpecialtiesWithTreatmentTypes = $msm->getAllMedicalSpecialtiesWithTreatmentTypes();
        $doctorDtoWithMedSpec = $this->usersManager->getDoctorWithMedicalSpecialtiesAndTreatmentTypes($this->doctor->getDoctor()->getId());
        $doctorsMedSpecIdsArray = $this->getDoctorsMedSpecIdsArray($doctorDtoWithMedSpec);

        $locations = $this->doctor->getLocations();
        $this->addParam('doctor', $this->doctor);
        $this->addParam('locations', $locations);
        $this->addParam('doctors', $this->getAllDoctors($locations));
        $this->addParam("specialties", $doctorsMedSpecIdsArray);
        $this->addParam('userEditHistory', $userEditHistory);
        $this->addParam("all_specialties", $allMedicalSpecialtiesWithTreatmentTypes);
    }

    protected function getAllDoctors($locations = array())
    {
        $doctors = array();
        
        /** @var LocationsDto[] $locations */
        foreach ($locations as $location) {
            $_location = new Practice($location->getId());
            $_doctors = $_location->getMembers();
            foreach ($_doctors as $_doctor) {
                $doctors[$_doctor->getId()] = $_doctor;
            }
        }
        
        return $doctors;
    }
    
    /**
     * @param DoctorsDto $doctorDto
     *
     * @return array
     */
    protected function getDoctorsMedSpecIdsArray(DoctorsDto $doctorDto)
    {
        $specialties = array();

        if ($msids = $doctorDto->getMedicalSpecialtyIds()) {
            foreach (explode(',', $msids) as $msid) {
                $specialties[] = $msid;
            }
            array_filter($specialties);
        }

        return $specialties;
    }

    /**
     * @param array $args
     *
     * @return array
     */
    public function getDefaultLoads($args)
    {
        return array();
    }

    /**
     * @param string $namespace
     * @param string $load
     *
     * @return bool
     */
    public function isValidLoad($namespace, $load)
    {
        return true;
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        return "doctors/home.tpl";
    }

    /**
     * @return bool
     */
    public function isMain()
    {
        return true;
    }
}
