<?php

namespace NGS\Loads\Doctors;
use NGS\Managers\AssetsManager;
use NGS\Managers\MedicalSpecialtiesManager;

/**
 *
 * <p><i>This is an exmaple of Load file.</i></p>
 * This file is using for outputing template(*.tpl) files.
 *
 * <p>In this file identified nested loads to help separatly controll each part of view.</p>
 * Identifing nested load using in tpl file- "ngs_nested_example" property you can load custom solutions in NgsNestedExampleLoad.calss.php file
 *
 * <p><b>NgsExampleLoad</b> class is extended class from <b>AbstractLoad.</b></p>
 *
 * @author  Naghashyan Solutions, e-mail: info@naghashyan.com
 * @version 1.0
 * @package load
 */
class LocationDoctorsLoad extends BaseDoctorLoad {

	public function load() {
		if (!$this->location->getLocation()) {
			$this->redirect($GLOBALS['CONFIG']['DOCTORS_LOGIN_URL']);
		}
		
		$this->loadLocationDoctors();

		$doctors = (array)$this->location->getMembers();
		
		foreach ($doctors as &$doctor) {
			$doctor->profileUrl = $this->getProfile($doctor->getId());
		}
		
		$this->addParam('location', $this->location);
		$this->addParam('locations', $this->doctor->getLocations());
		$this->addParam('doctors', $this->location->getMembers());
	}
	
	

	/**
	 * Load in specialties associated with the location attached to the loaded user (probably a doctor)
	 *
	 * @return void
	 */
	protected function loadLocationDoctors() {
		// Medical specialities manager
		$msm = MedicalSpecialtiesManager::getInstance($this->config, $this->args);
		$specialties = array();
		
		if ($this->location->getLocation()) {
			foreach($this->location->getMembers() as $doctor) {
				foreach(explode(',', $this->location->getLocation()->getMedicalSpecialtyIds()) as $specialty_id) {
					$specialties[$doctor->getId()][] = $msm->getById($specialty_id);
				}
			}
		}
		
		$this->addParam('specialties', $specialties);
	}

	/**
	 * Awful and bad code copy/paste job just for this page. IGNORE PLEASE
	 *
	 * @return string
	 */
	public function getProfile($id_doctors) {
		$url = '';
		
		$am = AssetsManager::getInstance();
		
		/**
		 * @var AssetsDto $childAssetDto
		 */
		if ($profileAssetDto = $am->getProfileAsset($id_doctors)) {
			$profileChildAssetsDtos = $am->getProfileChildAssets($profileAssetDto->getId());

			if ($profileChildAssetsDtos && $profileChildAssetsDtos[ASSET_GFX_SIZE_THUMBNAIL]) {
				$childAssetDto = $profileChildAssetsDtos[ASSET_GFX_SIZE_THUMBNAIL];
				$_id = sprintf("%09d", $childAssetDto->getParentId());
				$path = substr($_id, 0, 3) . '/' . substr($_id, 3, 3) . '/' . substr($_id, 6, 3);
				$profile_image_url = $GLOBALS["CONFIG"]["ASSET_URL"] . $path . '/' . $childAssetDto->getFileName();
			} else {
				$_id = sprintf("%09d", $profileAssetDto->getId());
				$path = substr($_id, 0, 3) . '/' . substr($_id, 3, 3) . '/' . substr($_id, 6, 3);
				$profile_image_url = $GLOBALS["CONFIG"]["ASSET_URL"] . $path . '/' . $profileAssetDto->getFileName();
			}

			$url = $profile_image_url;
		}

		return $url;
	}

	/**
	 * Return a thing based on $args parameter
	 * @abstract
	 * @access
	 * @param $args
	 * @return $loads
	 */
	public function getDefaultLoads($args) {
		$loads = array();
		return $loads;
	}

	/**
	 * Using for validation whether as certain load with nesting
	 *
	 * Return a thing based on $namespace, $load parameters
	 * @abstract
	 * @access
	 * @param $namespace, $load
	 * @return true
	 */
	public function isValidLoad($namespace, $load) {
		return true;
	}

	/**
	 * Identify view tempalte
	 *
	 * @abstract
	 * @access
	 * @param
	 * @return  *.tpl file
	 */
	public function getTemplate() {
		return "doctors/location_doctors.tpl";
	}

	public function isMain() {
		return true;
	}

}
