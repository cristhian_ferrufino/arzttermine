<?php

namespace NGS\Loads\Doctors;

use Arzttermine\Calendar\Calendar;
use NGS\Actions\Doctors\SaveCalendarAction;
use NGS\DAL\DTO\LocationsDto;
use NGS\Loads\BaseLoad;
use NGS\Managers\SearchResultCacheTmpManager;
use NGS\Managers\UsersManager;
use NGS\Util\ProviderRow;

class CalendarPreviewLoad extends BaseLoad {

    public function load()
    {
        if ($_REQUEST['current_week_preview'] == 1) {
            $GLOBALS["CONFIG"]['USER_AA_TABLE_TMP'] = true;
            $saveCalendarAction = new SaveCalendarAction();
            $saveCalendarAction->initialize($this->sessionManager, $this->config, $this->loadMapper, $this->args);
            $saveCalendarAction->service();
        }

        $usersManager = UsersManager::getInstance($this->config, $this->args);
        $userDto = $usersManager->getUserWithLocationsById($_REQUEST['user_id'], 0);
        $locationDtos = $userDto->getLocationDtos();

        $startDate = $this->secure($_REQUEST['start_date']);
        $CALENDAR_TIMES_SHOW_MORE_LINK_AFTER_HOW_MANY_TIMES = $GLOBALS['CONFIG']['CALENDAR_TIMES_SHOW_MORE_LINK_AFTER_HOW_MANY_TIMES'];
        //show unlimited times for a day without adding "mehr" link on calendar
        $GLOBALS['CONFIG']['CALENDAR_TIMES_SHOW_MORE_LINK_AFTER_HOW_MANY_TIMES'] = 1000;
        $daysNumberToShow = 5;
        $headerHtml = Calendar::getCalendarDaysHtml($startDate, $daysNumberToShow);
        $timesHtml = array();

        $GLOBALS["CONFIG"]['SEARCH_RESULT_CACHE_TABLE_PREVIEW'] = true;
        $searchResultCacheTmpManager = SearchResultCacheTmpManager::getInstance($this->config, $this->args);
        if ($_REQUEST['current_week_preview'] == 1) {
            // NOTE: This was missing the second parameter 26.02.2014
            $searchResultCacheTmpManager->clearDoctorData($userDto->getId(), '');
        }
        $visibleLocationsAndUsers = array();

        /** @var LocationsDto[] $locationDtos */
        foreach ($locationDtos as $locationDto) {
            if ($_REQUEST['current_week_preview'] == 1) {
                ProviderRow::cacheDoctorSearchResultInfo($userDto->getId(), $locationDto->getId());
            }
            $provider = new ProviderRow();
            $provider->setUser($userDto)->setLocation($locationDto);
            $visibleLocationsAndUsers[] = $provider;
        }
        foreach ($GLOBALS["CONFIG"]['INSURANCES'] as $insurance_id => $insurance_name) {
            $providerRows = $searchResultCacheTmpManager->getProviderRowsByFilters($visibleLocationsAndUsers, $startDate, $daysNumberToShow, $insurance_id, null, 1);
            foreach ($providerRows as $providerRow) {
                $location = $providerRow->getLocation();
                $timesHtml[$location->getId()][$insurance_id] = $providerRow->getDaysTimesHtml($startDate, $daysNumberToShow, $insurance_id, null);
            }
            $this->addParam("timesHtml", $timesHtml);
        }
        
        $GLOBALS["CONFIG"]['SEARCH_RESULT_CACHE_TABLE_PREVIEW'] = false;
        $this->addParam("headerHtml", $headerHtml);
        $this->addParam("insurances", $GLOBALS["CONFIG"]['INSURANCES']);

        $this->addParam("fixed_week", $userDto->getFixedWeek());
        $this->addParam("next_monday_date", date('Y-m-d', strtotime($startDate . ' +7 days')));
        $this->addParam("previous_monday_date", date('Y-m-d', strtotime($startDate . ' -7 days')));
        $GLOBALS['CONFIG']['CALENDAR_TIMES_SHOW_MORE_LINK_AFTER_HOW_MANY_TIMES'] = $CALENDAR_TIMES_SHOW_MORE_LINK_AFTER_HOW_MANY_TIMES;
        $GLOBALS["CONFIG"]['USER_AA_TABLE_TMP'] = false;
        $this->addParam("locations", $locationDtos);
    }

    /**
     * @param string $namespace
     * @param string $load
     *
     * @return bool
     */
    public function isValidLoad($namespace, $load)
    {
        return true;
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        return "doctors/calendar_preview.tpl";
    }
}