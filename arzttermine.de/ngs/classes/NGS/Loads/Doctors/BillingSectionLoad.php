<?php

namespace NGS\Loads\Doctors;

use NGS\Managers\BookingsManager;

class BillingSectionLoad extends BaseDoctorLoad {
	
    public function load() {
		$bm = BookingsManager::getInstance($this->config, $this->args);

		$periods = $bm->getBookingPeriodsAtLocation($this->location->getLocation()->getId());
		$most_recent_period = end($periods);
		reset($periods);
		
		$period = $this->getRequestParam('period', $most_recent_period);
		$upcoming_bookings = $bm->getUpcomingBookingsForLocation($this->location->getLocation()->getId(), $period);
		
		$statement_completed = false;
		
		if ($current_statements = $this->location->getStatements($period)) {
			if (!empty($current_statements[0])) {
				$statement_completed = (bool)$current_statements[0]->getStatus();
			}
		}
		
		$this->addParam('completed', $statement_completed);
		$this->addParam('period', $period);
		$this->addParam('periods', $periods);
		$this->addParam('upcoming_bookings', $this->sortBookings($upcoming_bookings));
		
		$this->addParam('location', $this->location);
		$this->addParam('locations', $this->doctor->getLocations());
		$this->addParam('doctors', $this->location->getMembers());
	}

	/**
	* Will sort bookings according to year/month/day
	* 
	* @param array $bookings
	* 
	* @return array
	*/
	protected function sortBookings($bookings) {
		$sorted = array();
		
		foreach ($bookings as $booking) {
			$sorted[strftime('%d', strtotime($booking->getAppointmentStartAt()))][] = $booking;
		}
		
		return $sorted;
	}

    /**
     * Using for validation whether as certain load with nesting
	 * 
     * @param $namespace
	 * @param $load
	 * 
     * @return bool
     */
    public function isValidLoad($namespace, $load) {
        return true;
    }

    /**
     * Identify view tempalte
     *
     * @return string *.tpl file
     */
    public function getTemplate() {
        return "doctors/billing_section.tpl";
    }

}
