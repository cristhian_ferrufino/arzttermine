<?php

namespace NGS\Loads\Doctors;
use NGS\Managers\AvailableAppointmentsManager;
use NGS\Managers\BookingsManager;
use NGS\Managers\LocationsManager;

/**
 *
 * <p><i>This is an exmaple of Load file.</i></p>
 * This file is using for outputing template(*.tpl) files.
 *
 * <p>In this file identified nested loads to help separatly controll each part of view.</p>
 * Identifing nested load using in tpl file- "ngs_nested_example" property you can load custom solutions in NgsNestedExampleLoad.calss.php file
 *
 * <p><b>NgsExampleLoad</b> class is extended class from <b>AbstractLoad.</b></p>
 *
 * @author  Naghashyan Solutions, e-mail: info@naghashyan.com
 * @version 1.0
 * @package load
 */
class AppointmentsHistoryLoad extends BaseDoctorLoad {

    public function load() {
        $userDto = $this->sessionManager->getUser();
        $userId = $userDto->getId();
		
        $locationsManager = LocationsManager::getInstance($this->config, $this->args);
        $locationDto = $locationsManager->getLocationByUserId($userId);
        if(isset($locationDto)){
            $locationId = $locationDto->getId();

            list($dateFrom,$dateTo,$orderBy) = $this->validateDates();
            $availableAppointmentsManager = AvailableAppointmentsManager::getInstance($this->config, $this->args);
            $availableAppointments = $availableAppointmentsManager->getAvailableAppointments(strtotime($dateFrom), strtotime($dateTo), $userId, $locationId);
            $validDates = $this->getDatesBetween(strtotime($dateFrom), strtotime($dateTo),$orderBy);

            $dateFrom = date("Y-m-d H:i:s",strtotime($dateFrom));
            $dateTo = date("Y-m-d H:i:s",strtotime($dateTo)+86399);
            $bookingsManager = BookingsManager::getInstance($this->config, $this->args);
            $doctorsPastBookings = $bookingsManager->getBookingsWithTreatmentTypeInDateRangeByUserId($userId, $dateFrom, $dateTo, $orderBy);

            $mergedArray = array();
            foreach ($validDates as $date){
                if(isset($availableAppointments[$date])){
                    if(isset($doctorsPastBookings[$date])){
                        $arr = $doctorsPastBookings[$date]+$availableAppointments[$date];
                    }else{
                        $arr = $availableAppointments[$date];
                    }
                }else{
                    if(isset($doctorsPastBookings[$date])){
                        $arr = $doctorsPastBookings[$date];
                    }else{
                        $arr = array();
                    }
                }

                ksort($arr);
                $mergedArray[$date] = $arr;
            }
            $this->addParam("appointments", $availableAppointments);
            $this->addParam("doctorsPastBookings",$doctorsPastBookings);
            $this->addParam("validDates", $validDates);
            $this->addParam("mergedArray",$mergedArray);
        }else{
            $this->addParam('no_access_error',"Sorry you are not registered in any location.");
        }
    }

    public function validateDates(){    
        $dateTo=date("d.m.Y",time());
        if(isset($_POST["date_to"]) && !empty($_POST["date_to"]) && strtotime($_POST["date_to"])<time()){
            $dateTo = date("d.m.Y",strtotime($_POST["date_to"]));
        }
        
        $dateToTimestamp = strtotime($dateTo);
        if (isset($_POST["date_from"]) && strtotime($_POST["date_from"])<=$dateToTimestamp){
            $dateFrom = date("d.m.Y", strtotime($_POST["date_from"]));
        }else{
            $dateFrom = date("d.m.Y", $dateToTimestamp-4*7*86400);
        }
        
        $orderBy="DESC";
        if(isset($_POST["order_by"]) && $_POST["order_by"]=="asc"){
            $orderBy="ASC";
        }
        
        $this->addParam("orderBy",$orderBy);
        $this->addParam("dateFrom", $dateFrom);
        $this->addParam("dateTo", $dateTo);
        return array($dateFrom, $dateTo, $orderBy);
    }
    
    public function getDatesBetween($startDate, $endDate, $order="DESC"){
        $validDates = array();
        switch ($order) {
            case "ASC":
                $day = $startDate;
                while($day <= $endDate){
                    if(date("w",$day)!=0 && date("w",$day)!=6){
                        $validDates[] = date("Y-m-d",$day);
                    }
                    $day += 86400;
                } 
                
                break;
            
            case "DESC":
                $day = $endDate;
                while ($day >= $startDate) {
                    if(date("w",$day)!=0 && date("w",$day)!=6){
                        $validDates[] = date("Y-m-d",$day);
                    }
                    $day-=86400;
                }
                break;
        }
        return $validDates;
          
    }
    /**
     * Return a thing based on $args parameter
     * @abstract
     * @access
     * @param $args
     * @return $loads
     */
    public function getDefaultLoads($args) {
        $loads = array();
        return $loads;
    }

    /**
     * Using for validation whether as certain load with nesting
     *
     * Return a thing based on $namespace, $load parameters
     * @abstract
     * @access
     * @param $namespace, $load
     * @return true
     */
    public function isValidLoad($namespace, $load) {
        return true;
    }

    /**
     * Identify view tempalte
     *
     * @abstract
     * @access
     * @param
     * @return  *.tpl file
     */
    public function getTemplate() {
        return "doctors/appointments_history.tpl";
    }

}