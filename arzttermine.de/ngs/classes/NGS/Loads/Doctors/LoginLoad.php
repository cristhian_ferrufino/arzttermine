<?php

namespace NGS\Loads\Doctors;

use NGS\Loads\BaseLoad;
use NGS\Security\RequestGroups;

/**
 *
 * <p><i>This is an exmaple of Load file.</i></p>
 * This file is using for outputing template(*.tpl) files.
 *
 * <p>In this file identified nested loads to help separatly controll each part of view.</p>
 * Identifing nested load using in tpl file- "ngs_nested_example" property you can load custom solutions in NgsNestedExampleLoad.calss.php file
 *
 * <p><b>NgsExampleLoad</b> class is extended class from <b>AbstractLoad.</b></p>
 *
 * @author  Naghashyan Solutions, e-mail: info@naghashyan.com
 * @version 1.0
 * @package load
 */
class LoginLoad extends BaseLoad {

    public function load()
    {
        // Monkeypatch used in doctors/main.tpl
        define('LOAD_NEW_DESIGN', 'true');

        if (isset($this->args['error_message'])) {
            $this->addParam('error_message', $this->args['error_message']);
        }
        
        if (isset($this->args['message'])) {
            $this->addParam('message', $this->args['message']);
        }

        $page = '';
        if (isset($_REQUEST['p'])) {
            $page = filter_var($_REQUEST['p'], FILTER_SANITIZE_STRING);
        }
        
        if ($page === 'forgot_login') {
            $this->addParam('forgot_login', 'true');
        }
        
        if ($page === 'forgot_password') {
            $this->addParam('forgot_password', 'true');
        }
        
        $this->addParam('email', filter_var($this->getPost('email', ''), FILTER_SANITIZE_STRING));
    }

    /**
     * Return a thing based on $args parameter
     * 
     * @param array $args
     * 
     * @return array
     */
    public function getDefaultLoads($args)
    {
        $loads = array();
        return $loads;
    }

    /**
     * Using for validation whether as certain load with nesting
     *
     * Return a thing based on $namespace, $load parameters
     *
     * @param string $namespace
     * @param string $load
     * 
     * @return bool
     */
    public function isValidLoad($namespace, $load) {
        return true;
    }

    /**
     * Identify view tempalte
     *
     * @return string
     */
    public function getTemplate()
    {
        return 'doctors/login.tpl';
    }

    /**
     * Identify permissions for users in this action file.
     *
     * Using for exmaple:
     * $adminsRequest variable is giving permission to  actions,loads file as a Administrator.
     * $guestRequest variable is giving permission to the actions,loads as a Guest User.
     *
     * The logic for this user is written in SessionManager.class.php file.
     *
     * @return int
     */
    public function getRequestGroup()
    {
        return RequestGroups::$guestRequest;
    }
}
