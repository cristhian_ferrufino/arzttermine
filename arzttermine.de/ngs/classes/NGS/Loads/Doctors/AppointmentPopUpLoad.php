<?php

namespace NGS\Loads\Doctors;

use NGS\Loads\BaseLoad;
use NGS\Managers\LocationsManager;
use NGS\Managers\UsersAaManager;

class AppointmentPopUpLoad extends BaseLoad {

    public function load()
    {
        $user_id = intval($this->getRequestParam('user_id'));
        $column_id = intval($this->getRequestParam('column_id'));
        $location_id = intval($this->getRequestParam('location_id'));
        $block_in_minutes = intval($this->getRequestParam('block_in_minutes'));
        $start_date = $this->secure($this->getRequestParam('start_date'));
        $start_at = $this->secure($this->getRequestParam('start_at'));
        $end_at = $this->secure($this->getRequestParam('end_at'));
        $selectedInsuranceTypesString = $this->secure($this->getRequestParam("selected_insurance_types"));

        $locationsManager = LocationsManager::getInstance($this->config, $this->args);
        $locationsDto = $locationsManager->getLocationsByUserId($user_id);
        $location_ids_array = array();
        $location_names = array();

        foreach ($locationsDto as $locationDto) {
            $location_ids_array[] = $locationDto->getId();
            $location_names [] = $locationDto->getName();
        }

        if (!$block_in_minutes) {
            $block_in_minutes = (strtotime($end_at) - strtotime($start_at)) / 60;
        }

        setlocale(LC_TIME, 'de_DE');
        $weekdays = array();
        for ($i = 0; $i < 5; $i++) {
            $weekdays[] = strftime("%a", strtotime($start_date) + $i * 86400);
        }
        $date = date("Y-m-d", strtotime($start_date) + 86400 * ($column_id - 1));
        $selectedWeekday = strftime("%a", strtotime($date));

        $weekday = date('w', strtotime($date));
        $this->addParam('weekday', $weekday);
        $appointmentDurationOptions = $GLOBALS["CONFIG"]["APPOINTMENT_DURATION_OPTIONS"];
        $appointmentDurationOptions = array_combine($appointmentDurationOptions, $appointmentDurationOptions);

        $ttNamesString = $this->secure($this->getRequestParam("doctor_tt_names"));
        $ttNamesArray = array();
        if (!empty($ttNamesString)) {
            $ttNamesArray = explode(",", $ttNamesString);
        }

        $ttIdsString = $this->secure($this->getRequestParam("doctor_tt_ids"));
        $ttIdsArray = array();
        if (!empty($ttIdsString)) {
            $ttIdsArray = explode(',', $ttIdsString);
        }

        $selectedTtIdsString = $this->secure($this->getRequestParam("selected_tt_ids"));
        $selectedTtIdsArray = array();
        if (!empty($selectedTtIdsString)) {
            $selectedTtIdsArray = explode(',', $selectedTtIdsString);
        }

        $usersAaManager = UsersAaManager::getInstance($this->config, $this->args);
        $insurances = $GLOBALS["CONFIG"]["INSURANCES"];
        $allInsuranceTypes = array();
        foreach ($insurances as $insuranceId => $insuranceName) {
            $allInsuranceTypes[] = $usersAaManager->getInsuranceTypeByIndex($insuranceId);
        }

        if (!isset($selectedInsuranceTypesString)) {
            $selectedInsuranceTypesArray = $allInsuranceTypes;
        } else {
            $selectedInsuranceTypesArray = explode(",", $selectedInsuranceTypesString);
        }

        $is_admin = $this->getRequestParam("is_admin");

        $this->addParam('is_admin', $is_admin);
        $this->addParam('ttIdsArray', $ttIdsArray);
        $this->addParam('ttNamesArray', $ttNamesArray);
        $this->addParam("selectedTtIdsArray", $selectedTtIdsArray);
        $this->addParam("allInsuranceTypes", $allInsuranceTypes);
        $this->addParam("allInsuranceNames", array_values($GLOBALS["CONFIG"]["INSURANCES"]));
        $this->addParam("selectedInsuranceTypesArray", $selectedInsuranceTypesArray);
        $this->addParam("appointmentDurationOptions", $appointmentDurationOptions);
        $this->addParam('start_at', $start_at);
        $this->addParam('end_at', $end_at);
        $this->addParam('block_in_minutes', $block_in_minutes);
        $this->addParam('columnId', $column_id);
        $this->addParam('date', $date);
        $this->addParam('weekdays', $weekdays);
        $this->addParam('selectedWeekday', $selectedWeekday);
        $this->addParam('location_ids', $location_ids_array);
        $this->addParam('location_names', $location_names);

        $this->addParam('selected_location_id', isset($_REQUEST['location_id']) ? $location_id : isset($location_ids_array[0]) ? $location_ids_array[0] : 0);
    }

    public function getTemplate()
    {
        return "doctors/appointment_pop_up.tpl";
    }
}