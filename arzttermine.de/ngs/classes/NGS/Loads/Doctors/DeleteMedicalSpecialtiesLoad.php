<?php

namespace NGS\Loads\Doctors;

use NGS\Managers\MedicalSpecialtiesManager;

/**
 * @author  Vahagn Kirakosyan
 */
 
class DeleteMedicalSpecialtiesLoad extends BaseDoctorLoad {

    public function load() {
        if(isset($this->args['error'])){
            $this->addParam('error', $this->args['error']);
        }
        $medicalSpecialtiesManager = MedicalSpecialtiesManager::getInstance($this->config, $this->args);
        $doctorId = $this->sessionManager->getUser()->getId();
        $medicalSpecialties = $medicalSpecialtiesManager->getMedicalSpecialtiesByDoctorId($doctorId);
        $this->addParam("medicalSpecialties", $medicalSpecialties);
    }
    
    /**
     * Return a thing based on $args parameter
     * @abstract
     * @access
     * @param $args
     * @return $loads
     */
    public function getDefaultLoads($args) {
        $loads = array();
        return $loads;
    }

    /**
     * Using for validation whether as certain load with nesting
     *
     * Return a thing based on $namespace, $load parameters
     * @abstract
     * @access
     * @param $namespace, $load
     * @return true
     */
    public function isValidLoad($namespace, $load) {
        return true;
    }

    /**
     * Identify view tempalte
     *
     * @abstract
     * @access
     * @param
     * @return  *.tpl file
     */
    public function getTemplate() {
        return "doctors/delete_medical_specialties.tpl";
    }

}