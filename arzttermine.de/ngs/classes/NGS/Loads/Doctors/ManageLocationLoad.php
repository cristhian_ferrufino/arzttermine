<?php

namespace NGS\Loads\Doctors;
use NGS\Managers\MedicalSpecialtiesManager;

/**
 *
 * <p><i>This is an exmaple of Load file.</i></p>
 * This file is using for outputing template(*.tpl) files.
 *
 * <p>In this file identified nested loads to help separatly controll each part of view.</p>
 * Identifing nested load using in tpl file- "ngs_nested_example" property you can load custom solutions in NgsNestedExampleLoad.calss.php file
 *
 * <p><b>NgsExampleLoad</b> class is extended class from <b>AbstractLoad.</b></p>
 *
 * @author  Naghashyan Solutions, e-mail: info@naghashyan.com
 * @version 1.0
 * @package load
 */
class ManageLocationLoad extends BaseDoctorLoad {
	
	public function load() {
		// If there is no current location, we should redirect to the user profile page
		if (!$this->location->getLocation()) {
			$this->redirect($GLOBALS['CONFIG']['DOCTORS_LOGIN_URL']);
		}
		
		// Load the specialties for locations
		$this->loadLocationSpecialities();
		
        $this->form_data = array();

        if (isset($this->args['form_data'])) {
            $this->form_data = $this->args['form_data'];
        }

        if (!empty($this->args['error'])) {
            $this->addParam('error', $this->args['error']);
        }

        $this->addParam('form_data', $this->form_data);
		$this->addParam('location', $this->location);
		$this->addParam('locations', $this->doctor->getLocations());
		$this->addParam('doctors', $this->location->getMembers());
	}

	/**
	 * Load in specialties associated with the location attached to the loaded user (probably a doctor)
	 * 
	 * @return void
	 */
	protected function loadLocationSpecialities() {
		// Medical specialities manager
		$msm = MedicalSpecialtiesManager::getInstance($this->config, $this->args);
		$specialties = array();

		if ($this->location->getLocation()) {
			foreach(explode(',', $this->location->getLocation()->getMedicalSpecialtyIds()) as $speciality_id) {
				$specialties[] = $msm->getById($speciality_id);
			}
		}
		
		$this->addParam('specialties', $specialties);
	}

	/**
	 * Return a thing based on $args parameter
	 * 
	 * @param $args
	 * @return array $loads
	 */
	public function getDefaultLoads($args) {
		return array();
	}

	/**
	 * Using for validation whether as certain load with nesting
	 * Return a thing based on $namespace, $load parameters
	 * 
	 * @param $namespace
	 * @param $load
	 * @return bool
	 */
	public function isValidLoad($namespace, $load) {
		return true;
	}

	/**
	 * Identify view tempalte
	 *
	 * @return string *.tpl file
	 */
	public function getTemplate() {
		return "doctors/manage_location.tpl";
	}
}