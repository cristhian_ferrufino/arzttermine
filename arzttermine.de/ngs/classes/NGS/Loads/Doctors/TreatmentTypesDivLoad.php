<?php

namespace NGS\Loads\Doctors;

use NGS\Loads\BaseLoad;
use NGS\Managers\DoctorsTreatmentTypesManager;
use NGS\Managers\MedicalSpecialtiesManager;

class TreatmentTypesDivLoad extends BaseLoad {

    public function load() {        
        $medicalSpecialtiesManager = MedicalSpecialtiesManager::getInstance($this -> config, $this -> args);
        $medicalSpecialtyId = $_REQUEST["med_spec_id"];
        $medicalSpecialtyDto = $medicalSpecialtiesManager -> getMedicalSpecialtyWithTreatmentTypesById($medicalSpecialtyId);
        $treatmentTypeIdsArray=$this->getTreatmentTypeIdsArray($medicalSpecialtyDto);
        $this->addParam("treatmentTypeIdsArray",$treatmentTypeIdsArray);
        $this -> addParam("medicalSpecialtyDto", $medicalSpecialtyDto);
        
        $doctor = $this->sessionManager->getUser();
        if(method_exists($doctor, "getId")){
            $doctorsTreatmentTypesManager = DoctorsTreatmentTypesManager::getInstance($this -> config, $this -> args);
            $treatmentTypesIdsArrayByDoctorIdAndMedSpecId = $doctorsTreatmentTypesManager->getTreatmentTypesIdsArrayByDoctorIdAndMedSpecId($doctor ->getId(), $medicalSpecialtyId);
            $this->addParam("treatmentTypesIdsArrayByDoctorIdAndMedSpecId",$treatmentTypesIdsArrayByDoctorIdAndMedSpecId );   
        }else{
            if(isset($_REQUEST['ttIds']) && !empty($_REQUEST['ttIds'])){
                $treatmentTypesIdsArrayByDoctorIdAndMedSpecId = explode(',', $_REQUEST['ttIds']);
            }else{
                $treatmentTypesIdsArrayByDoctorIdAndMedSpecId = array();
            }
            $this->addParam("treatmentTypesIdsArrayByDoctorIdAndMedSpecId",$treatmentTypesIdsArrayByDoctorIdAndMedSpecId);      
        }
       
    }

    protected function getTreatmentTypeIdsArray($medicalSpecialtyDto) {
        $arr=array();
        foreach ($medicalSpecialtyDto->getTreatmentTypesDtos() as $treatmentTypeDto) {
            $arr[]=$treatmentTypeDto->getId();
        }
        return $arr;
    }

    public function getTemplate() {
        return "doctors/treatment_types_div.tpl";
    }

}