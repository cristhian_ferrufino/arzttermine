<?php

namespace NGS\Loads\Doctors;
use NGS\Loads\BaseLoad;
use NGS\Managers\UsersManager;
use NGS\Security\RequestGroups;

/**
 *
 * <p><i>This is an exmaple of Load file.</i></p>
 * This file is using for outputing template(*.tpl) files.
 *
 * <p>In this file identified nested loads to help separatly controll each part of view.</p>
 * Identifing nested load using in tpl file- "ngs_nested_example" property you can load custom solutions in NgsNestedExampleLoad.calss.php file
 *
 * <p><b>NgsExampleLoad</b> class is extended class from <b>AbstractLoad.</b></p>
 *
 * @author  Naghashyan Solutions, e-mail: info@naghashyan.com
 * @version 1.0
 * @package load
 */
class ResetPasswordLoad extends BaseLoad {

	public function load() {
		// Monkeypatch used in doctors/main.tpl
        define('LOAD_NEW_DESIGN', 'true');
        
		if (isset($_REQUEST['sub_pages'][0])) {
			$resetPasswordCode = $_REQUEST['sub_pages'][0];
			$usersManager = new UsersManager($this->config, $this->args);
			$userDto = $usersManager->getUserByResetPasswordCode($resetPasswordCode);
			if (!isset($userDto)) {
				$_REQUEST["p"] = "forgot_password";
				$this->redirectToLoad("doctors", "Login", array("error_message" => "Code falsch!"));

			}
			$this->addParam("password_reset_code", $resetPasswordCode);
			if (isset($this->args['error_message'])) {
				$this->addParam("error_message", $this->args['error_message']);
			}

		} else {
			$_REQUEST["p"] = "forgot_password";
			$this->redirectToLoad("doctors", "Login", array("error_message" => "Code falsch!"));
		}
	}

	/**
	 * Return a thing based on $args parameter
	 * @abstract
	 * @access
	 * @param $args
	 * @return $loads
	 */
	public function getDefaultLoads($args) {
		$loads = array();
		return $loads;
	}

	/**
	 * Using for validation whether as certain load with nesting
	 *
	 * Return a thing based on $namespace, $load parameters
	 * @abstract
	 * @access
	 * @param $namespace, $load
	 * @return true
	 */
	public function isValidLoad($namespace, $load) {
		return true;
	}

	/**
	 * Identify view tempalte
	 *
	 * @abstract
	 * @access
	 * @param
	 * @return  *.tpl file
	 */
	public function getTemplate() {

		return "doctors/reset_password.tpl";
	}

	/** Identify permissions for users in this action file.
	 *
	 * Using for exmaple:
	 * $adminsRequest variable is giving permission to  actions,loads file as a Administrator.
	 * $guestRequest variable is giving permission to the actions,loads as a Guest User.
	 *
	 * The logic for this user is written in SessionManager.class.php file.
	 *
	 * @abstract
	 * @access
	 * @param
	 * @return $guestRequest
	 */

	public function getRequestGroup() {
		return RequestGroups::$guestRequest;
	}

}
