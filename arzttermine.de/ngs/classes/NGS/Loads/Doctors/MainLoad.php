<?php

namespace NGS\Loads\Doctors;

use NGS\Framework\Dispatcher;
use NGS\Loads\BaseLoad;
use NGS\Security\RequestGroups;
use NGS\Security\UserGroups;

class MainLoad extends BaseLoad {

	public function load() {
		if ($this->sessionManager->getSessionUserLevel() == UserGroups::$ADMIN) {
			$_REQUEST["p"] = 'login';
		}
	}

	/**
	 * Return a thing based on $args parameter
	 * @abstract
	 * @access
	 * @param $args
	 * @return $loads
	 */
	public function getDefaultLoads($args) {
		
		// Available actions in the form of /<area>/<action>/<additional>...
		// e.g. /aerzte/account_bearbeitung
		$loadMappingArr = array(
			// Account
			'account'                => 'home',
			'account_bearbeitung'    => 'manage_profile',
			'profilbild'             => 'profile_pictures',
			'registrierung'          => 'registration',
			'passwort_zuruecksetzen' => 'reset_password',
			'abrechnung_abschnitt'   => 'billing_section',
			
			// Specialties
			'fachrichtung_verwalten' => 'manage_medical_specialties',
			'fachrichtung_entfernen' => 'delete_medical_specialties',
			
			// Appointments
			'termine'                => 'appointments',
			'termin_rueckblick'      => 'appointments_history',
			'termin_erstellen'       => 'create_appointment',
			
			// Locations
			'praxis'                 => 'home_location',
			'praxis_aerzte'          => 'location_doctors',
			'praxis_bearbeitung'     => 'manage_location',
			'praxis_fachrichtung'    => 'manage_location_specialties',
		);

		$contentJsLoad = "home";
		
		if (!empty($_REQUEST["p"])) {
			if (isset($loadMappingArr[$_REQUEST["p"]])) {
				$contentJsLoad = $loadMappingArr[$_REQUEST["p"]];
			} else {
				$contentJsLoad = $_REQUEST["p"];
			}
		}
		
		$loads = array();
		$loads["content"]["load"] = Dispatcher::buildClassName('loads', 'doctors', ucfirst($contentJsLoad) . 'Load');
		$this->args["mainLoad"] = &$this;
		$loads["content"]["args"] = $this->args;
		$loads["content"]["loads"] = array();
		$this->addParam("contentLoad", $contentJsLoad);
		
		return $loads;
	}

	/**
	 * Using for validation whether as certain load with nesting
	 *
	 * Return a thing based on $namespace, $load parameters
	 * @abstract
	 * @access
	 * @param $namespace, $load
	 * @return bool
	 */
	public function isValidLoad($namespace, $load) {
		return true;
	}

	/**
	 * Identify view tempalte
	 *
	 * @abstract
	 * @access
	 * @param
	 * @return  *.tpl file
	 */
	public function getTemplate() {
		return "doctors/main.tpl";
	}

	/** Identify permissions for users in this action file.
	 *
	 * Using for exmaple:
	 * $adminsRequest variable is giving permission to  actions,loads file as a Administrator.
	 * $guestRequest variable is giving permission to the actions,loads as a Guest User.
	 *
	 * The logic for this user is written in SessionManager.class.php file.
	 *
	 * @abstract
	 * @access
	 * @param
	 * @return $guestRequest
	 */

	public function getRequestGroup() {
		return RequestGroups::$guestRequest;
	}

	public function isMain() {
		return true;
	}

}
