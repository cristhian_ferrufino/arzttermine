<?php

namespace NGS\Loads;

use NGS\Framework\AbstractLoad;
use NGS\Security\RequestGroups;
use NGS\Security\UserGroups;
use NGS\Util\UtilFunctions;

/**
 *
 * <p><i>This is an exmaple of Load file.</i></p>
 * This file is using for outputing template(*.tpl) files.
 *
 * <p>In this file identified nested loads to help separatly controll each part of view.</p>
 * Identifing nested load using in tpl file- "ngs_nested_example" property you can load custom solutions in NgsNestedExampleLoad.calss.php file
 *
 * <p><b>NgsExampleLoad</b> class is extended class from <b>AbstractLoad.</b></p>
 *
 * @author  Naghashyan Solutions, e-mail: info@naghashyan.com
 * @version 1.0
 * @package load
*/
abstract class BaseLoad extends AbstractLoad {

	protected $user;

	public function initialize($smarty, $sessionManager, $config, $loadMapper, $args) {
		parent::initialize($smarty, $sessionManager, $config, $loadMapper, $args);
		$this->addParam('arzconfig', $GLOBALS["CONFIG"]);
        
		$this->user = $this->sessionManager->getUser();
		if ($this->user->getLevel() != UserGroups::$GUEST) {
			$this->addParam('userId', $this->user->getId());
		}
		
		$requestString=$this->getRequestString();
		if($requestString){
			$this->addParam('requestString',$requestString);
		}

	}
	
	public function getRequestString(){
		$requestString='';
		if(isset($_REQUEST['search']) and !empty($_REQUEST['search'])){
			$this->args['search']=$this->secure($_REQUEST['search']);
			if(!empty($_POST)){
				$requestString='&search=yes';
				foreach ($_POST as $key => $value) {
					$requestString.='&'.$key.'='.$value;
				}
			}else{
				foreach ($_GET as $key => $value) {
					if($key!='rpp' && $key!='pg'){
						$requestString.='&'.$key.'='.$value;
					}
				}
			}
		}else {
			if(!empty($_REQUEST['order_by'])){
				$requestString.='&order_by='.$_REQUEST['order_by'];
				$requestString.='&direction='.$_REQUEST['direction'];
			}
		}

		return $requestString;
	}

	public function getCustomer() {
		return $this->user;
	}

	public function secure($var) {
		if (isset($var)) {
			return trim(htmlspecialchars(strip_tags($var)));
		} else {
			return null;
		}
	}

	public function isValidLoad($namespace, $load) {
		return true;
	}

	public function getLoadClassNameByLoadKey($loadKey) {
        return UtilFunctions::underscoreToCamelCase($loadKey . 'Load');
	}

	public function getRequestGroup() {
		return RequestGroups::$guestRequest;
	}
	
	public function isMain() {
		return false;
	}

}