<?php

namespace NGS\Framework;

abstract class AbstractManager {

    /**
     * @var array
     */
    public $config = array();
    
    /**
     * @var array
     */
    public $args = array();
    
}