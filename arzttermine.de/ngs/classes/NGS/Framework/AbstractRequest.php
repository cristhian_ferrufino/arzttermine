<?php

namespace NGS\Framework;

use Arzttermine\Application\Application;
use NGS\Framework\Exceptions\NoAccessException;

/**
 * <p><b>AbstractRequest class</b> is a base class for all action classes.
 * The child of this class is <b>AbstractAction.class.php,AbstractLoad.class.php</b> files. </p>
 *
 * @author  Naghashyan Solutions, e-mail: info@naghashyan.com
 * @version 1.0
 * @package framework
 */
abstract class AbstractRequest {

    protected $config = array();

    protected $args = array();

    /**
     * @var AbstractSessionManager
     */
    protected $sessionManager;

    protected $loadMapper;

    protected $requestGroup;

    /**
     * Return a thing based on $config parameter
     *
     * @abstract
     * @access
     *
     * @param $config
     *
     * @return
     */
    public static function notFoundHandler($config)
    {
        Application::getInstance()->forward(Application::getInstance()->container->get('router')->generate('not_found'));
    }

    /**
     * Return a thing based on $sessionManager, $config, $loadMapper, $args parameters
     *
     * @param $sessionManager
     * @param array $config
     * @param $loadMapper
     * @param array $args
     */
    public function initialize($sessionManager, $config, $loadMapper, $args)
    {
        $this->sessionManager = $sessionManager;
        $this->loadMapper = $loadMapper;
        $this->config = $config;
        $this->args = $args;
    }

    /**
     * Detect whether or not the current request was made by an AJAX call or not
     *
     * @return bool
     */
    public final function isXmlHttpRequest()
    {
        return Application::getInstance()->getRequest()->isXmlHttpRequest();
    }

    /**
     * Retrieve a member of the $_POST superglobal
     *
     * If no $key is passed, returns the entire $_POST array.
     *
     * @param string $key
     * @param mixed $default Default value to use if key not found
     *
     * @return mixed Returns null if key does not exist
     */
    public function getPost($key = null, $default = null)
    {
        return Application::getInstance()->getRequest()->getRequest($key, $default);
    }

    /**
     * Strangely named, retrieves a member of the $_GET superglobal
     *
     * If no $key is passed, returns the entire $_GET array.
     *
     * @param string $key
     * @param mixed $default Default value to use if key not found
     *
     * @return mixed Returns null if key does not exist
     */
    public function getGet($key = null, $default = null)
    {
        return Application::getInstance()->getRequest()->getQuery($key, $default);
    }

    /**
     * Retrieve a parameter
     *
     * Retrieves a parameter from the instance. If a
     * parameter matching the $key is not found, default is returned.
     *
     * @param string $key
     * @param mixed $default Default value to use if key not found
     *
     * @return mixed
     */
    public function getRequestParam($key, $default = null)
    {
        return Application::getInstance()->getRequest()->getParam($key, $default);
    }

    /**
     * Return a thing based on $requestGroup parameter
     *
     * @abstract
     * @access
     *
     * @param $requestGroup
     *
     * @return
     */
    public function setRequestGroup($requestGroup)
    {
        $this->requestGroup = $requestGroup;
    }

    /**
     * Return a thing based on $parameter
     *
     * @abstract
     * @access
     *
     * @param $parameter
     *
     * @return
     */
    public function getRequestGroup()
    {
        return $this->requestGroup;
    }

    /**
     * Return a thing based on $dispatcher parameter
     *
     * @abstract
     * @access
     *
     * @param $dispatcher
     *
     * @return object
     */
    public function setDispatcher($dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * Send JSON data back to the client
     *
     * @param array $data
     *
     * @return void
     */
    public final function sendJsonResponse($data = array())
    {
        header('Content-Type: application/json');
        echo json_encode($data, JSON_FORCE_OBJECT);
        exit;
    }

    /**
     * Return a thing based on $package, $load, $args, $statusCode parameters
     *
     * @abstract
     * @access
     *
     * @param $package , $load, $args, $statusCode
     *
     * @return integer|babyclass
     */
    public function redirectToLoad($package, $load, $args, $statusCode = 200)
    {
        if ($statusCode > 200 && $statusCode < 300) {
            header("HTTP/1.1 $statusCode Exception");
        }
        $wrapperLoad = $this->getWrapperLoad();
        if ($wrapperLoad) {
            $loadArr = array();
            $loadArr["load"] = Dispatcher::buildClassName('loads', $package, $load . "Load");
            $loadArr["args"] = $args;
            $jsLoad = strtolower($load);
            $wrapperLoad->addParam($this->getNameSpace() . "Load", $jsLoad);
            $wrapperLoad->nest($this->getNameSpace(), $loadArr, false);
        } else {
            if (!$this->isMain()) {
                $jsLoad = strtolower($load);
                echo('<input type="hidden" id="redirectedLoad" name="redirectedLoad" value="' . $jsLoad . '"/>');
            }
            $this->dispatcher->loadPage($package, $load, $args);
            exit();
        }
    }

    /**
     * @return bool
     */
    protected function getWrapperLoad()
    {
        return false;
    }

    /**
     * @return bool
     */
    public function isMain()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function toCache()
    {
        return false;
    }

    /**
     * @return bool
     */
    public function onNoAccess()
    {
        return false;
    }

    /**
     * Get the request verb from the server
     *
     * @return string
     */
    protected final function getRequestMethod()
    {
        return Application::getInstance()->getRequest()->getMethod();
    }

    /**
     * Check if our request is a POST request
     *
     * @return string
     */
    protected final function isPost()
    {
        return Application::getInstance()->getRequest()->isPost();
    }

    /**
     * Check if our request is a GET request
     *
     * @return string
     */
    protected final function isGet()
    {
        return Application::getInstance()->getRequest()->isGet();
    }

    /**
     * @throws Exceptions\NoAccessException
     */
    protected function cancel()
    {
        throw new NoAccessException("Load canceled request ");
    }

    /**
     * @param string $url
     * @param bool $isSecure Unused
     *
     * @deprecated Use Application::redirect
     */
    protected function redirect($url, $isSecure = false)
    {
        $protocol = "http";
        if ($isSecure) {
            $protocol = "https";
        }
        
        header("Location: " . $protocol . "://" . HTTP_HOST . "/$url");
        exit();

    }
}