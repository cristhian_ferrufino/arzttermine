<?php

namespace NGS\Framework\DAL\Mappers;

use NGS\Util\DB\DBMSFactory;

abstract class AbstractMapper {

    protected $dbms;

    /**
     * @var string
     */
    protected $tableName = '';

    /**
     * Initializes DBMS pointer.
     */
    public function __construct()
    {
        $this->dbms = DBMSFactory::getDBMS();
    }

    /**
     * Updates tables text field's value by primary key
     *
     * @param mixed $id The PK
     * @param string $fieldName - the name of filed which must be updated
     * @param string $fieldValue - the new value of field
     * @param bool $esc - if true the field value will be escaped
     *
     * @return int Affected rows count or -1 if something goes wrong
     */
    public function updateTextField($id, $fieldName, $fieldValue, $esc = true)
    {
        $sql = sprintf(
            "UPDATE `%s` SET `%s` = '%s' WHERE `%s` = %d ",
            $this->getTableName(),
            $fieldName,
            $esc ? $this->dbms->escape($fieldValue) : $fieldValue,
            $this->getPKFieldName(),
            $id
        );

        $res = $this->dbms->query($sql);
        if ($res) {
            $result = $this->dbms->getAffectedRows();

            return $result;
        }

        return -1;
    }

    /**
     * The child class must implemet this method to return table name.
     *
     * @return string
     */
    public function getTableName()
    {
        return $this->tableName;
    }

    /**
     * The child class must implemet this method to return primary key field name.
     *
     * @return string
     */
    public function getPKFieldName()
    {
        return 'id';
    }

    /**
     * Update a table in the DB all at once, filtering the input array (useful for forms)
     *
     * @param int $id The PK
     * @param array $fieldNames This is a list of valid fields to update
     * @param array $fieldValues This is a data dump of field data as key => value pairs. Uses $fields for reference
     * @param bool $esc If fields should be escaped
     *
     * @return int Affected rows count or -1 if something goes wrong
     */
    public function updateWithFields($id, $fieldNames, $fieldValues, $esc = true)
    {
        // Filter out the fields we're not allowed to update
        $validFields = array_intersect_key($fieldValues, array_flip($fieldNames));

        return $this->updateFields($id, $validFields, $esc);
    }

    /**
     * Updates tables numeric and text field's value by primary key
     *
     * @param string $id - the unique identifier of table
     * @param array $fields - array that key is fieldName and value is fieldValue
     * @param bool $esc
     *
     * @return int affected rows count or -1 if something goes wrong
     */
    public function updateFields($id, $fields, $esc = true)
    {
        $result = -1;

        // Aggregate SET statements
        $set = array(); // of strings

        foreach ((array)$fields as $fieldName => $fieldValue) {
            if ($fieldValue == "CURRENT_TIMESTAMP") {
                $set[] = sprintf("`%s` = %s", $fieldName, $fieldValue);
            } else {
                $set[] = sprintf("`%s` = '%s'", $fieldName, ($esc) ? $this->dbms->escape($fieldValue) : $fieldValue);
            }
        }

        $q = sprintf(
            "UPDATE `%s` SET %s WHERE `%s` = %d;",
            $this->getTableName(),
            implode(',', $set),
            $this->getPKFieldName(),
            $id
        );

        $res = $this->dbms->query($q);

        if ($res) {
            $result = $this->dbms->getAffectedRows();
        }

        return $result;
    }

    /**
     * Updates tables numeric field's value by primary key
     *
     * @param mixed $id - the unique identifier of table
     * @param string $fieldName - the name of filed which must be updated
     * @param string $fieldValue - the new value of field
     *
     * @return affected rows count or -1 if something goes wrong
     */
    public function updateNumericField($id, $fieldName, $fieldValue)
    {
        // Create query.
        $q = sprintf("UPDATE `%s` SET `%s` = %d WHERE `%s` = %d ", $this->getTableName(), $fieldName, $fieldValue, $this->getPKFieldName(), $id);
        // Execute query.
        $res = $this->dbms->query($q);
        if ($res) {
            $result = $this->dbms->getAffectedRows();

            return $result;
        }

        return -1;
    }

    /**
     * Sets field value NULL.
     *
     * @param mixed $id
     * @param string $fieldName
     *
     * @return int
     */
    public function setNull($id, $fieldName)
    {
        $sql = sprintf(
            "UPDATE `%s` SET `%s` = NULL WHERE `%s` = %d ",
            $this->getTableName(),
            $fieldName,
            $this->getPKFieldName(),
            $id
        );

        return $this->executeUpdate($sql);
    }

    /**
     * Executes Update/Delete queries.
     *
     * @param string $sql
     *
     * @return int
     */
    protected function executeUpdate($sql)
    {
        $res = $this->dbms->query($sql);
        if ($res) {
            $result = $this->dbms->getAffectedRows();

            return $result;
        }

        return -1;
    }

    /**
     * Deletes the row by primary key
     *
     * @param string $id - the unique identifier of table
     *
     * @return int rows count or -1 if something goes wrong
     */
    public function deleteByPK($id)
    {
        $sql = sprintf(
            "DELETE FROM `%s` WHERE `%s` = %d ",
            $this->getTableName(),
            $this->getPKFieldName(),
            $id
        );

        $res = $this->dbms->query($sql);
        if ($res) {
            $result = $this->dbms->getAffectedRows();

            return $result;
        }

        return -1;
    }

    /**
     * Deletes the row by custom key
     *
     * @param string $fieldName - the unique identifier of table
     * @param mixed $fieldValue
     * @param bool $esc [optional] - shows if the textual values must be escaped before setting to DB
     *
     * @return int rows count or -1 if something goes wrong
     */
    public function deleteByCustomKey($fieldName, $fieldValue, $esc = true)
    {
        //check if $fieldValue is string and print it accordingly in the query
        if (is_string($fieldValue)) {
            $sql = sprintf(
                "DELETE FROM `%s` WHERE `%s` = '%s' ",
                $this->getTableName(),
                $fieldName,
                $esc ? $this->dbms->escape($fieldValue) : $fieldValue
            );
        } else {
            $sql = sprintf(
                "DELETE FROM `%s` WHERE `%s` = %d ",
                $this->getTableName(),
                $fieldName,
                $fieldValue
            );
        }

        $res = $this->dbms->query($sql);
        if ($res) {
            $result = $this->dbms->getAffectedRows();

            return $result;
        }

        return -1;
    }

    /**
     * Inserts new row in table.
     *
     * @param array $fields - the field names, which must be inserted
     * @param array $values - field values
     * @param bool $esc [optional] - shows if the textual values must be escaped before setting to DB
     *
     * @return int id or -1 if something goes wrong
     */
    public function insertValues(array $fields, array $values, $esc = true)
    {
        if (empty($fields) || empty($values) || count($fields) !== count($values)) {
            return -1;
        }

        $sql = sprintf("INSERT INTO `%s` SET ", $this->getTableName());
        $set = array();

        for ($i = 0; $i < count($fields); $i++) {
            $set[] = sprintf(" `%s` = '%s'", $fields[$i], $esc ? $this->dbms->escape($values[$i]) : $values[$i]);
        }
        $sql .= implode(',', $set);

        $res = $this->dbms->query($sql);
        if ($res) {
            $result = $this->dbms->getLastInsertedId();

            return $result;
        }

        return -1;
    }

    /**
     * @param mixed $dto
     * @param bool $esc
     * @param bool $returnQuery
     *
     * @return int
     */
    public function updateByPK($dto, $esc = true, $returnQuery = false)
    {
        if ($dto == null) {
            return -1;
        }

        $getPKFunc = self::getCorrespondingFunctionName($dto->getMapArray(), $this->getPKFieldName(), "get");
        $pk = $dto->$getPKFunc();

        if (!isset($pk)) {
            return -1;
        }

        $dto_fields = array_values($dto->getMapArray());
        $db_fields = array_keys($dto->getMapArray());

        $sql = sprintf("UPDATE `%s` SET ", $this->getTableName());
        $update = array();

        for ($i = 0; $i < count($dto_fields); $i++) {
            $functionName = "get" . ucfirst($dto_fields[$i]);
            if ($val = $dto->$functionName()) {
                if (is_int($val)) {
                    $update[] = sprintf(" `%s` = %d", $db_fields[$i], $val);
                } else {
                    if ($val == "CURRENT_TIMESTAMP()") {
                        $update[] = sprintf(" `%s` = %s", $db_fields[$i], $esc ? $this->dbms->escape($val) : $val);
                    } else {
                        $update[] = sprintf(" `%s` = '%s'", $db_fields[$i], $esc ? $this->dbms->escape($val) : $val);
                    }
                }
            }
        }
        $sql .= implode(',', $update);

        $sql .= sprintf(" WHERE `%s` = %d ", $this->getPKFieldName(), $dto->$getPKFunc());
        if ($returnQuery) {
            return $sql;
        }
        
        return $this->executeUpdate($sql);
    }

    /**
     * @param array $mapArray
     * @param string $itemName
     * @param string $prefix
     *
     * @return string
     */
    private static function getCorrespondingFunctionName($mapArray, $itemName, $prefix = "set")
    {
        $valueOfMap = null;

        if (array_key_exists($itemName, $mapArray)) {
            $valueOfMap = $mapArray[$itemName];
        }

        if (isset($valueOfMap)) {
            $functionName = $prefix . ucfirst($valueOfMap);

            return $functionName;
        }

        return null;
    }

    /**
     * @param mixed $dtos
     * @param bool $esc
     * @param string $preSql
     * @param string $postSql
     *
     * @return bool
     */
    public function insertDtos($dtos, $esc = true, $preSql = "", $postSql = "")
    {
        $bulkUpdateQuery = $preSql;
        foreach ($dtos as $dto) {
            $bulkUpdateQuery .= $this->insertDto($dto, $esc, true) . ";";
        }
        $bulkUpdateQuery .= $postSql;

        return $this->dbms->multiQuery($bulkUpdateQuery);
    }

    /**
     * @param mixed $dto
     * @param bool $esc
     * @param bool $returnQueryOnly
     *
     * @return int
     */
    public function insertDto($dto, $esc = true, $returnQueryOnly = false)
    {
        if ($dto == null) {
            return -1;
        }

        $dto_fields = array_values($dto->getMapArray());
        $db_fields = array_keys($dto->getMapArray());

        $sql = sprintf("INSERT INTO `%s` SET ", $this->getTableName());
        $set = array();

        for ($i = 0; $i < count($dto_fields); $i++) {
            $functionName = "get" . ucfirst($dto_fields[$i]);
            if ($val = $dto->$functionName()) {
                if (is_int($val)) {
                    $set[] = sprintf(" `%s` = %d", $db_fields[$i], $val);
                } else {
                    if ($val == "CURRENT_TIMESTAMP") {
                        $set[] = sprintf(" `%s` = %s", $db_fields[$i], $esc ? $this->dbms->escape($val) : $val);
                    } else {
                        $set[] = sprintf(" `%s` = '%s'", $db_fields[$i], $esc ? $this->dbms->escape($val) : $val);
                    }
                }
            }
        }
        $sql .= implode(',', $set);

        if ($returnQueryOnly) {
            return $sql;
        }

        $res = $this->dbms->query($sql);
        if ($res) {
            $result = $this->dbms->getLastInsertedId();

            return $result;
        }

        return -1;
    }

    /**
     * Selects all entries from table
     *
     * @return array
     */
    public function selectAll()
    {
        $sql = sprintf("SELECT * FROM `%s`", $this->getTableName());

        return $this->fetchRows($sql);
    }

    /**
     * Executes the query and returns an array of corresponding DTOs
     *
     * @param string $sqlQuery
     *
     * @return array
     */
    protected function fetchRows($sqlQuery)
    {
        $res = $this->dbms->query($sqlQuery);

        $resultArr = array();
        if ($res && $this->dbms->getResultCount($res) > 0) {
            $results = $this->dbms->getResultArray($res);
            foreach ($results as $result) {
                $dto = $this->createDto();
                $this->initializeDto($dto, $result);

                $resultArr[] = $dto;
            }
        }

        return $resultArr;
    }

    /**
     * The child class must implement this method
     * to return an instance of corresponding DTO class.
     *
     * @return mixed
     */
    public abstract function createDto();

    /**
     * Calls stripslashes method to unescape magic quotes.
     *
     * @param mixed $dto
     * @param array $dbData
     * @param bool $prefix
     * @param string $tableName
     *
     * @return void
     */
    public function initializeDto($dto, $dbData, $prefix = false, $tableName = null)
    {
        $mapArray = $dto->getMapArray();

        if ($dbData == null) {
            return;
        }

        if ($prefix != false) {

            $prefix = !empty($tableName) ? $tableName : $this->getTableName();
            foreach ($dbData as $dbDataKey => $dbDataValue) {
                if (strpos($dbDataKey, $prefix) === 0) {
                    $functionName = self::getCorrespondingFunctionName($mapArray, substr($dbDataKey, strlen($prefix) + 1));
                    if (strlen($functionName) != 0) {
                        // Call function and initialize item based on data.
                        $dto->$functionName($dbDataValue);
                    }
                }
            }

            return;
        }

        // Get keys for dbData.
        $dbDataKeys = array_keys($dbData);

        foreach ($dbDataKeys as $keyName) {
            // Create function based on key name.
            $functionName = self::getCorrespondingFunctionName($mapArray, $keyName);
            if (strlen($functionName) != 0) {
                // Call function and initialize item based on data.
                $data = $dbData[$keyName];
                $dto->$functionName($data);
            }
        }
    }

    /**
     * Selects from table by primary key and returns corresponding DTO
     *
     * @param int $id
     *
     * @return mixed
     */
    public function selectByPK($id)
    {
        if (is_int($id)) {
            $sqlQuery = sprintf("SELECT * FROM `%s` WHERE `%s` = %d ", $this->getTableName(), $this->getPKFieldName(), $id);
        } else {
            $sqlQuery = sprintf("SELECT * FROM `%s` WHERE `%s` = '%s' ", $this->getTableName(), $this->getPKFieldName(), $id);
        }

        $resultArr = $this->fetchRows($sqlQuery);

        if (count($resultArr) === 1) {
            return $resultArr[0];
        } else {
            return null;
        }
    }

    /**
     * Selects from table by field and returns corresponding DTO
     *
     * @param mixed $fieldValue
     * @param string $fieldName
     *
     * @return mixed
     */
    public function selectByField($fieldName, $fieldValue)
    {
        if (is_int($fieldValue)) {
            $sqlQuery = sprintf("SELECT * FROM `%s` WHERE `%s` = %d ", $this->getTableName(), $fieldName, $fieldValue);
        } else {
            $sqlQuery = sprintf("SELECT * FROM `%s` WHERE `%s` = '%s' ", $this->getTableName(), $fieldName, $fieldValue);
        }

        $resultArr = $this->fetchRows($sqlQuery);
        if (count($resultArr) === 1) {
            return $resultArr[0];
        } else {
            return null;
        }
    }

    /**
     * Sets the given field value to mysql CURRENT_TIMESTAMP()
     *
     * @param int $id - primary key value
     * @param string $fieldName - mysql field name
     *
     * @return int
     */
    public function setCurrentTimestamp($id, $fieldName)
    {
        $sql = sprintf("UPDATE `%s` SET `%s` = CURRENT_TIMESTAMP() WHERE `%s` = %d ", $this->getTableName(), $fieldName, $this->getPKFieldName(), $id);

        return $this->executeUpdate($sql);
    }

    /**
     * @param bool $prefix
     * @param string $tableName
     *
     * @return string
     */
    public function getAllFieldsAsQueryString($prefix = false, $tableName = null)
    {
        if (is_null($tableName)) {
            $tableName = $this->getTableName();
        }

        $dto = $this->createDto();
        $dtoMapArray = $dto->getMapArray();

        $queryString = '';

        if ($prefix == false) {
            foreach ($dtoMapArray as $dbFieldName => $dtoFieldName) {
                $string = sprintf("`%s`.`%s` as `%s`,", $tableName, $dbFieldName, $dbFieldName);
                $queryString = $queryString . $string;
            }
        } else {
            foreach ($dtoMapArray as $dbFieldName => $dtoFieldName) {
                $string = sprintf("`%s`.`%s` as `%s_%s`,", $tableName, $dbFieldName, $tableName, $dbFieldName);
                $queryString = $queryString . $string;
            }
        }

        $queryString = substr($queryString, 0, -1);

        return $queryString;
    }

    /**
     * @param string $sqlQuery
     * @param string $fieldName
     *
     * @return mixed
     */
    protected function fetchField($sqlQuery, $fieldName)
    {
        $res = $this->dbms->query($sqlQuery);

        if ($res && $this->dbms->getResultCount($res) > 0) {
            $results = $this->dbms->getResultArray($res);
            $fieldValue = $results[0][$fieldName];
            if (isset($fieldValue)) {
                return $fieldValue;
            }
        }

        return null;
    }
}