<?php

namespace NGS\Framework\DAL\DTO;

/**
 * <p><b>AbstractDto class</b> is a base class for all dto classes.</p>
 * <p><b>Data transfer object (DTO)</b> is using to transfer data between software application subsystems.
 * DTOs are often used in conjunction with data access objects to retrieve data from a database.
 * </p>
 * <p>
 * <p>To creating data transfer object (DTO) that holds all data that is required for the remote call.</p>
 * It also template class for all DTO (data transfer object) class types and defines mandatory  members and functions to be implemented by child classes.
 *
 * @author  Naghashyan Solutions, e-mail: info@naghashyan.com
 * @version 1.0
 * @package framework
 */
abstract class AbstractDto {

    protected $mapArray;

    /**
     * Sets fields at creation time if you so desire
     */
    public function __construct($fields = array())
    {
        if (!empty($fields)) {
            $this->setFields($fields);
        }
    }

    /**
     * Set all fields at once. Key/value pair arrays only
     *
     * @param array $fields
     *
     * @return void
     */
    public function setFields($fields = array())
    {
        if (!empty($fields)) {
            foreach ((array)$fields as $field => $value) {
                $this->setField($field, $value);
            }
        }
    }

    /**
     * Set a single field by key
     *
     * @param string $field
     * @param mixed $value
     *
     * @return void
     */
    public function setField($field, $value)
    {
        if (in_array($field, array_keys($this->mapArray))) {
            $this->{$this->mapArray[$field]} = $value;
        }
    }

    public static function dtosToJSON($dtos, $includedDynamicMapArray = false, $htmlSpecialChar = false)
    {

        if (empty($dtos)) {
            return "[]";
        }
        $ret = "[";
        foreach ($dtos as $dto) {
            $json = self::dtoToJSON($dto, $includedDynamicMapArray = false, $htmlSpecialChar);
            $ret .= $json . ',';
        }
        $ret = substr($ret, 0, -1) . ']';

        return $ret;
    }

    public static function dtoToJSON($dto, $includedDynamicMapArray = false, $htmlSpecialChar = false)
    {
        $josn_text = "{";
        $mapArray = $dto->getMapArray();
        foreach ($mapArray as $key => $value) {
            $josn_text .= '"' . $key . '"' . ':' . '"' . ($htmlSpecialChar == true ? htmlspecialchars($dto->$value) : $dto->$value) . '"' . ',';
        }
        if ($includedDynamicMapArray) {
            $dynamicMapArray = $dto->getDynamicMapArray();
            foreach ($dynamicMapArray as $key => $value) {
                $josn_text .= '"' . $key . '"' . ':' . '"' . ($htmlSpecialChar == true ? htmlspecialchars($dto->$value) : $dto->$value) . '"' . ',';
            }
        }
        $len = strlen($josn_text);
        if ($len > 1) {
            $josn_text = substr($josn_text, 0, -1);
        }
        $josn_text .= "}";

        return $josn_text;
    }

    public static function dtosToArray($dtos, $includedDynamicMapArray = false)
    {

        $ret = array();
        foreach ($dtos as $dto) {
            $ret [] = self::dtoToArray($dto, $includedDynamicMapArray = false);
        }

        return $ret;
    }

    public static function dtoToArray($dto, $includedDynamicMapArray = false)
    {
        $ret = array();
        $mapArray = $dto->getMapArray();
        foreach ($mapArray as $key => $value) {
            $ret [$key] = $dto->$value;
        }
        if ($includedDynamicMapArray) {
            $dynamicMapArray = $dto->getDynamicMapArray();
            foreach ($dynamicMapArray as $key => $value) {
                $ret [$key] = $dto->$value;
            }
        }

        return $ret;
    }

    /**
     * Returns an assoc array, which maps DB fields into DTO members.
     * The member names should correspond to the following formats:
     *        array(
     *                "id" => "id",
     *                "user_id" => "userId",
     *                "UserName" => "userName",
     *                "LAST_TIME" => "lastTime"
     *            )
     *
     * @return assoc array
     */
    public function getMapArray()
    {
        return $this->mapArray;
    }

    public function getDynamicMapArray()
    {
        return array();
    }

    /**
     * Overloads getter and setter methods.
     *
     * @param object $m - method name, should start either by "set" or by "get" prefix
     * @param object $a - for setter methods $a[0] will be used as new value for property
     *
     * @return
     */
    public function __call($m, $a)
    {
        // retrieving the method type (setter or getter)
        $type = substr($m, 0, 3);

        // retrieving the field name
        $fieldName = self::lowerFirstLetter(substr($m, 3));

        if ($type == 'set') {
            $this->$fieldName = $a[0];
        } elseif ($type == 'get' && isset($this->$fieldName)) {
            return $this->$fieldName;
        }

        return null;
    }

    /**
     * The first letter of input string changes to Lower case.
     * The transformation will be performed only ASCII alphabetical symbols,
     * otherwise the same text will be returned without any changes.
     *
     * @param String $str - input string, that should be lowercased
     *
     * @return - string
     */
    private static function lowerFirstLetter($str)
    {
        $first = substr($str, 0, 1);
        $asciiValue = ord($first);
        if ($asciiValue >= 65 && $asciiValue <= 90) {
            $asciiValue += 32;

            return chr($asciiValue) . substr($str, 1);
        }

        return $str;
    }

    public function toJSON($includedDynamicMapArray = false, $htmlSpecialChar = false)
    {

        return self::dtoToJSON($this, $includedDynamicMapArray = false, $htmlSpecialChar);
    }

    public function toArray($includedDynamicMapArray = false)
    {
        return self::dtoToArray($this, $includedDynamicMapArray = false);
    }
}