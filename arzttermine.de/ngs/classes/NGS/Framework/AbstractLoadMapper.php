<?php

namespace NGS\Framework;

/**
 * <p><b>AbstractLoadMapper class</b> is a base class for all mapper classes.</p>
 * 
 * @author  Naghashyan Solutions, e-mail: info@naghashyan.com
 * @version 1.0
 * @package framework
*/
abstract class  AbstractLoadMapper{

    /**
     * @var array
     */
    private $params;
    
    /**
     * @var array
     */
    protected $config;

    /**
     * @var \SmartyBC
     */
    protected $smarty;
	
  /**
   * Return a thing based on $config parameter
   * @abstract  
   * @access
   * @param $config 
   * @return 
  */
	public function __construct($config = array()){
		$this->config = $config;
	}
	
  /**
   * Return a thing based on $smarty  parameter
   * @abstract  
   * @access
   * @param $smarty 
   * @return 
  */
  
	public function setSmarty($smarty){
		$this->smarty = $smarty;
	}
	
  /**
   * @abstract  
   * @access
   * @param
   * @return 
  */
	//public abstract function initUrlConfig();
	
  /**
   * Return a thing based on $parameter
   * @abstract  
   * @access
   * @param $parameter 
   * @return integer|babyclass
  */	
	public abstract function getCurrentLoads();
	
  /**
   * Return a thing based on $url, $matches $parameters
   * @abstract  
   * @access
   * @param $url, $matches
   * @return 
  */
	public abstract function getDynamicLoad($url, $matches);
	
  /**
   * Return a thing based on $exCode parameter
   * @abstract  
   * @access
   * @param $exCode 
   * @return $param
  */

	public abstract function notFoundHandler($exCode);

	public function getParam($name){
		$param = null;
		if(!$this->params){
			$ns = $this->smarty->get_template_vars("ns");
			$param = $ns[$name];
		}
		else{
			$param = $this->params[$name];
		}
		
		return $param;
	}
	
  /**
   * Return a thing based on $name, $value parameters
   * @abstract  
   * @access
   * @param $name, $value
   * @return $ns[$name]
  */
	public function setParam($name, $value){
		$ns = $this->smarty->get_template_vars("ns");
		$ns[$name] = $value;
	}
	
  /**
   * Return a thing based on $nm parameter
   * @abstract  
   * @access
   * @param $nm 
   * @return 
  */	
	public abstract function __get($nm);

  /**
   * Return a thing based on $name, $arguments  parameters
   * @abstract  
   * @access
   * @param $name, $arguments 
   * @return
  */
	public abstract function __call($name, $arguments);

  /**
   * Return a thing based on $parameter
   * @abstract  
   * @access
   * @param $params 
   * @return integer|babyclass
  */		
	public function setParams($params){
		$this->params = $params;
	}
}