<?php

namespace NGS\Framework;

use NGS\DAL\DTO\UsersDto;
use NGS\Security\Users\AuthenticateUser;

abstract class AbstractSessionManager {

    protected $args;

    protected $dispatcher;

    /**
     * @param array $args
     */
    public function setArgs(&$args)
    {
        $this->args = $args;
    }

    /**
     * @param $dispatcher
     */
    public function setDispatcher($dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * @return UsersDto
     */
    public abstract function getUser();

    /**
     * @param $request
     * @param $user
     */
    public abstract function validateRequest($request, $user);

    /**
     * @param $user
     * @param bool $remember
     * @param bool $useDomain
     */
    public function setUser($user, $remember = false, $useDomain = true)
    {
        $sessionTimeout = $remember ? 2078842581 : 0;
        $domain = false;

        if ($useDomain) {
            if (HTTP_ROOT_HOST === HTTP_HOST) {
                $domain = "." . HTTP_HOST;
            } else {
                $domain = HTTP_ROOT_HOST;
            }
        }

        $cookieParams = $user->getCookieParams();
        foreach ($cookieParams as $key => $value) {
            setcookie($key, $value, $sessionTimeout, "/", $domain);
        }

        $sessionParams = $user->getSessionParams();
        foreach ($sessionParams as $key => $value) {
            $_SESSION[$key] = $value;
        }
    }

    /**
     * @param $user
     */
    public function updateUserUniqueId($user)
    {
        if (HTTP_ROOT_HOST === HTTP_HOST) {
            $domain = "." . HTTP_HOST;
        } else {
            $domain = HTTP_ROOT_HOST;
        }

        $cookieParams = $user->getCookieParams();

        setcookie("uh", $cookieParams["uh"], null, "/", $domain);
    }

    /**
     * @param AuthenticateUser $user
     * @param bool $useDomain
     */
    public function removeUser(AuthenticateUser $user, $useDomain = true)
    {
        $sessionTimeout = time() - 42000;

        $domain = false;

        if ($useDomain) {
            if (HTTP_ROOT_HOST === HTTP_HOST) {
                $domain = "." . HTTP_HOST;
            } else {
                $domain = HTTP_ROOT_HOST;
            }
        }

        $cookieParams = $user->getCookieParams();

        foreach ($cookieParams as $key => $value) {
            setcookie($key, '', $sessionTimeout, "/", $domain);
        }

        $this->deleteSession();
    }

    /**
     * 
     */
    private function deleteSession()
    {
        $_SESSION = array();

        if (isset($_COOKIE[session_name()])) {
            @setcookie(session_name(), '', time() - 42000, '/');
        }
        session_destroy();
    }
}