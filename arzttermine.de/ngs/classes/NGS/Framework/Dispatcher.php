<?php

namespace NGS\Framework;

use Arzttermine\Application\Application;
use NGS\Framework\Exceptions\ClientException;
use NGS\Framework\Exceptions\NoAccessException;
use NGS\Framework\Exceptions\RedirectException;
use NGS\Managers\SessionManager;
use NGS\Util\DB\DBMSFactory;
use NGS\Util\NgsSmarty;
use NGS\Util\UtilFunctions;

//this part should be refactored, please create a method that will require the given path and then will instantiate the corresponding instance
//after that it should return the object of the class, in case of any problems it should return a null.

/**
 * <p><b>Dispatcher class</b> is a base class for initilize configuration and database connection.</p>
 * <p>The main purpose of this this file is dispatching requests in the project.</p>
 * 
 * @author  Naghashyan Solutions, e-mail: info@naghashyan.com
 * @version 1.0
 * @package framework
 */
class Dispatcher {

    public $loadsPackage;
    
    protected $config = array();
    protected $args = array();
    protected $toCache = false;
    protected $smarty;
    
    private $isAjax = false;

    /**
		 * <p>In the  <b>_construct()</b> we are defining basic goals.</p>
		 * <p><b>$package</b> is a folder name(for our custom case it is named "main") that we are create our class folder.</p>  
		 * <p><b>$command</b> is a defauld load name,that should be loaded at the first time,when you run it in you web browser.</p>
		 * 
     */
    public function __construct() {
        // @todo: Trash this
        if (defined("CONFIG_INI")) {
            $this->config = parse_ini_file(CONFIG_INI);
        }
        // Initilize db connection
        if (defined("DB_FACTORY")) {
            DBMSFactory::init($this->config);
        }

        // Initialize load mapper. For some reason always a Load type
        $mapper = $this->buildClassName('loads', '', 'LoadMapper');
        $this->loadMapper = new $mapper($this->config);
        $this->sessionManager =  new SessionManager($this->config, $this->args);
        
        $this->smarty = new NgsSmarty($this->config, $this->config["VERSION"]);
        
        $command = "";
        $args = array();
        
        if (!empty($_REQUEST['_url']) && preg_match_all("/(\/([^\/]+))/", $_REQUEST["_url"], $matches)) {
            $package = array_shift($matches[2]);
            $command = array_shift($matches[2]);
            $args = $matches[2];
            if (isset($args[count($args) - 1])) {
                if (preg_match("/(.+?)\.ajax/", $args[count($args) - 1], $matches1)) {
                    $this->isAjax = true;
                    $args[count($args) - 1] = $matches1[1];
                }
            }
            $package = str_replace("_", "/", $package);
        } else {
            $app = Application::getInstance();
            $app->forward($app->container->get('router')->generate('not_found'));
        }
        
        $this->sessionManager->setDispatcher($this);
        
        $this->dispatch($package, $command, $args);
    }

    /**
     * Return a thing based on $parameter
     * @abstract  
     * @access
     * @param $isAjax 
     * @return integer|babyclass
     */
    public function setIsAjax($isAjax) {
        $this->isAjax = $isAjax;
    }

    /**
     * Return a thing based on $parameter
     * @abstract  
     * @access
     * @param $parameter 
     * @return integer|babyclass
     */
    public function isAjax() {
        return $this->isAjax;
    }

    /**
		 * <p>In the <b>dispatch()</b> function you are controlling the request.</p>
		 * <p>Actions are requested via a special URL patterns "http://host/dyn/actionpackage_actionInnerPackage/do_action_name".</p>
     * <p>For example, for requesting NgsExampleAction.class.php the "http://host/do_ngs_example" URL should be used.</p>
		 * <p><b>"do_"</b> should be cuted and left "ngs_example".After using ucfirst() function we get "Ngs_example".</p>
		 * <p>Using preg_replace() function we get "NgsExample".</p>
		 * 
     * Return a thing based on $package, $command, $args parameters
     * @abstract  
     * @access
     * @param $package, $command, $args
     * @return
     */
    public function dispatch($package, $command, &$args) {
        if (isset($_REQUEST["show_not_found"])) {
            $this->loadMapper->notFoundHandler(0);
            return;
        }

        $this->args = &$args;
        if ($command == "") {
            $command = "default";
        }
        $isCommand = false;
        if (strripos($command, "do_") === 0) {
            $isCommand = true;
            $command = substr($command, 3);
        }
        $command = ucfirst($command);
        $command = preg_replace("/_(\w)/e", "strtoupper('\\1')", $command);
        try {
            if ($command) {
                if ($isCommand) {
                    $this->doAction($package, $command);
                } else {
                    $this->loadPage($package, $command);
                }
            }
        } catch (ClientException $ex) {
            $errorArr = $ex->getErrorParams();
            $ret = "[{";
            if (is_array($errorArr)) {
                $delim = "";

                foreach ($errorArr as $key => $value) {
                    $ret .= $delim;
                    $ret .= "'" . $key . "': {code: '" . $value["code"] . "', message: '" . $value["message"] . "'}";
                    $delim = ",";
                }
            }
            $ret .= "}]";

            header("HTTP/1.0 403 Forbidden");
            echo($ret);
            exit();
        } catch (RedirectException $ex) {
            $this->redirect($ex->getRedirectTo());
        } catch (NoAccessException $ex) {
            $this->showNotFound($ex->getCode());
        } catch (\Exception $ex) {
            $this->showNotFound(NoAccessException::$NOT_FOUND);
        }
    }
    
    public static function buildClassName($type, $package, $class)
    {
        if (class_exists($class)) {
            return $class;
        }
        
        $class = UtilFunctions::underscoreToCamelCase($class);
        
        // e.g NGS\Loads\Doctors\AppointmentsLoad
        //     NGS\Actions\Patients\LoginAction
        return 'NGS\\' . ucfirst($type) . '\\' . ($package ? ucfirst($package) . '\\' : '') . $class;
    }

    /**
		 * <p><b>loadPage()</b>function handling load files.</p>
		 * <p>In this case $loadName is defining for example "NgsExample" concatenate with "Load" word and we get "NgsExampleLoad".</p>
		 *
     * Return a thing based on $package, $command, $args parameters
     * @abstract  
     * @access
     * @param $package, $command, $args
     * @return
     */
    public function loadPage($package, $command, $args = false) {

        $loadName = $command . "Load";
        $className = self::buildClassName('loads', $package, $loadName);
        
        try {
            $loadObj = new $className();

            if ($args) {
                $this->args = array_merge($this->args, $args);
            }
            
            $loadObj->initialize($this->smarty, $this->sessionManager, $this->config, $this->loadMapper, $this->args);
            $loadObj->setDispatcher($this);
            
            if ($this->validateRequest($loadObj)) {
                $this->toCache = $loadObj->toCache();
                if (!$this->toCache) {
                    $this->dontCache();
                }
                $loads = $this->loadMapper->getCurrentLoads();
                $loadObj->service($loads); //passing arguments
                $params = $loadObj->getParams();
                
                if ($templateName = $loadObj->getTemplate()) {
                    $this->displayResult($templateName, $params);
                }
                
                return;
            }

            if ($loadObj->onNoAccess(NoAccessException::$NO_ACCESS)) {
                return;
            }
        } catch (NoAccessException $ex) {
            if ($loadObj->onNoAccess($ex->getCode())) {
                return;
            }
        }


        $this->showNotFound(NoAccessException::$NOT_FOUND);
    }

    /**
		 * <p><b>doAction()</b>function handling action files.</p>
		 * <p>In this case $actionName is defining for example "NgsExample" concatenate with "Action" word and we get "NgsExampleAction".</p>
		 * 
     * Return a thing based on $package, $action parameters
     * @abstract  
     * @access
     * @param $package, $action
     * @return
     */
    private function doAction($package, $action) {
        $actionName = $action . "Action";
        $className = self::buildClassName('actions', $package, $actionName);
        
        try {
            $actionObj = new $className();
            $actionObj->initialize($this->sessionManager, $this->config, $this->loadMapper, $this->args);
            $actionObj->setDispatcher($this);
            if ($this->validateRequest($actionObj)) {
                $this->toCache = $actionObj->toCache();
                if (!$this->toCache) {
                    $this->dontCache();
                }
                $actionObj->service();
                return;
            }

            if ($actionObj->onNoAccess(NoAccessException::$NO_ACCESS)) {
                return;
            }
        } catch (NoAccessException $ex) {
            if ($actionObj->onNoAccess($ex->getCode())) {
                return;
            }
        }

        $this->showNotFound(NoAccessException::$NOT_FOUND);
    }

    /**
		 * <p>In the <b>validateRequest($request)</b> function we are defining users role for the page.</p>
		 * <p>See full description in the <b>SessionManager.class.php</b> file</p>
		 *
     * Return a thing based on $request parameters
     * @abstract  
     * @access
     * @param $request
     * @return false
     */
    private function validateRequest($request) {    	
        $user = $this->sessionManager->getUser();				
        if ($user->validate()) {       	

            if ($this->sessionManager->validateRequest($request, $user)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Return a thing based on $dispatcherIndex, $params parameters
     * @abstract  
     * @access
     * @param $dispatcherIndex, $params
     * @return
     */
    private function displayResult($dispatcherIndex, $params) {
        if (!$this->toCache) {
            $this->dontCache();
        }
        if ($this->isAjax) {
            $ret = $this->createJSON($params);
            echo($ret);
        } else {
            $this->loadMapper->setSmarty($this->smarty);
            $this->smarty->assign("ns", $params);
            $this->smarty->assign("pm", $this->loadMapper);
            $this->smarty->display($dispatcherIndex);
        }
    }

    /**
     * Return a thing based on $arr parameter
     * @abstract  
     * @access
     * @param $arr 
     * @return $ret
     */
    private function createJSON($arr) {
        $ret = "[{";
        if (is_array($arr)) {
            $delim = "";
            if (isset($arr["inc"])) {
                foreach ($arr["inc"] as $key => $value) {
                    $ret .= $delim;
                    $innerObj = $this->createJSON($value);
                    $ret .= $key . ":" . $innerObj;
                    $delim = ",";
                }
                unset($arr["inc"]);
            }

            foreach ($arr as $key => $value) {
                $ret .= $delim;
                $ret .= $key . ":" . $value;
                $delim = ",";
            }
        }
        $ret .= "}]";
        return $ret;
    }

    /**
		 * <p>The <b>redirect()</b> function using for redirect in some cases to the another page.</p>
		 *
     * Return a thing based on $url, $isSecure parameters
     * @abstract  
     * @access
     * @param $url, $isSecure 
     * @return
     */
    public function redirect($url, $isSecure = false) {
        $protocol = "http";
        if ($isSecure) {
            $protocol = "https";
        }
        
        header("Location: " . $protocol . "://" . $_SERVER[HTTP_HOST] . "/$url");
    }

    /**
		 * <p>The <b>showNotFound()</b> function using for get <i>"404 Not Found"<i> page.</p> 
		 *
     * Return a thing based on $code parameter
     * @abstract  
     * @access
     * @param $code 
     * @return
     */
    protected function showNotFound($code = 0)
    {
        $app = Application::getInstance();
        $app->forward($app->container->get('router')->generate('not_found'));
    }

    /**
     * Return a thing based on $parameter
     * @abstract  
     * @access
     * @param $parameter 
     * @return
     */
    protected function dontCache() {
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
        header("Pragma: no-cache"); // HTTP/1.0
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
    }

}