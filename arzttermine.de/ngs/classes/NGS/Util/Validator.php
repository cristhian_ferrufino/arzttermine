<?php

namespace NGS\Util;

use NGS\Framework\Exceptions\ClientException;

/**
 * Validator class contains utility functions for validating received paramaeteres. 
 * 
 * @author  Naghashyan Solutions, e-mail: info@naghashyan.com
 * @version 1.0
 */
class Validator {
	
	/**
	 * Validates parameter by length.
	 * 
	 * @param object $str
	 * @param object $paramName - necessary for constructiong error message
	 * @param object $allowEmpty - wheter allow text to be empty or not
	 * @param object $minChars - min allowed characters count
	 * @param object $maxChars - max allowed characters count
	 * @return 
	 */
	public static function validateLength($str, $paramName, $allowEmpty, $minChars, $maxChars)
	{
		if( !isset($str) ){
			if($allowEmpty){
				return "";
			}
			throw new ClientException($paramName." is missing.");
		}
		
		$str = trim($str);
		if( $str == '' ){
			if($allowEmpty){
				return "";
			}
			throw new ClientException($paramName." is empty.");
		}
	
		if( strlen($str) < $minChars ){
			throw new ClientException($paramName." should have at least $minChars chars.");
		}
		
		if( strlen($str) > $maxChars ){
			throw new ClientException($paramName." has too many chars (".strlen($str)."). Maximum allowed is $maxChars.");
		}
		
		return $str;
	}
	
	/**
	 * Escapes the input string
	 * 
	 * @param object $str
	 * @return 
	 */
	public static function escape($str)
	{
		//escape HTML special symbols
		$str = htmlspecialchars($str);
	
		return $str;
	}
	
	public static function stripTags($text)
	{
		if( empty($text)  ){
			return "";
		}
		return strip_tags($text, '<b><i><p><a><strong><title>');
	}
	
	public static function checkEmailAddress($email) {
        // First, we check that there's one @ symbol, and that the lengths are right
        if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email)) {
            // Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
            return false;
        }
        // Split it into sections to make life easier
        $email_array = explode("@", $email);
        $local_array = explode(".", $email_array[0]);
        for ($i = 0; $i < sizeof($local_array); $i++) {
            if (!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])) {
                return false;
            }
        }
        if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) {
            // Check if domain is IP. If not, it should be valid domain name
            $domain_array = explode(".", $email_array[1]);
            if (sizeof($domain_array) < 2) {
                return false;
                // Not enough parts to domain
            }
            for ($i = 0; $i < sizeof($domain_array); $i++) {
                if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])) {
                    return false;
                }
            }
        }
        return true;
    }

}