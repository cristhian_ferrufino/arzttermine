<?php

namespace NGS\Util;

class UtilFunctions {

    /**
     * @param $string
     * @param int $maxLength
     *
     * @return mixed
     */
    public static function createSlugFromString($string, $maxLength = 100)
    {
        $string = substr(strtolower(preg_replace('/[^a-zA-Z0-9 ]/s', '', $string)), 0, $maxLength);

        return preg_replace("/\s+/", '-', $string);
    }

    /**
     * @param string $time in 'H:i' format
     * @param int $minutes
     *
     * @return string
     */
    public static function addMinutesToTime($time, $minutes)
    {
        return date("H:i", strtotime('+' . $minutes . ' minutes', strtotime($time)));
    }

    /**
     * @param $string
     *
     * @return mixed
     */
    public static function replaceStringNonAzAndSpacesCharsToSpaces($string)
    {
        return preg_replace('/[^a-zA-Z ]/s', ' ', $string);
    }

    /**
     * @param $string
     *
     * @return mixed
     */
    public static function convertToSingleSpaceString($string)
    {
        return preg_replace('/ +/', ' ', $string);
    }

    /**
     * @return array
     */
    public static function getBrowser()
    {
        $u_agent = $_SERVER['HTTP_USER_AGENT'];
        $bname = 'Unknown';
        $platform = 'Unknown';
        $ub = '';

        //First get the platform?
        if (preg_match('/linux/i', $u_agent)) {
            $platform = 'Linux';
        } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
            $platform = 'Mac OS';
        } elseif (preg_match('/windows|win32/i', $u_agent)) {
            $platform = 'Windows';
        }

        // Next get the name of the useragent yes seperately and for good reason
        if (preg_match('/MSIE/i', $u_agent) && !preg_match('/Opera/i', $u_agent)) {
            $bname = 'Internet Explorer';
            $ub = "MSIE";
        } elseif (preg_match('/Firefox/i', $u_agent)) {
            $bname = 'Mozilla Firefox';
            $ub = "Firefox";
        } elseif (preg_match('/Chrome/i', $u_agent)) {
            $bname = 'Google Chrome';
            $ub = "Chrome";
        } elseif (preg_match('/Safari/i', $u_agent)) {
            $bname = 'Apple Safari';
            $ub = "Safari";
        } elseif (preg_match('/Opera/i', $u_agent)) {
            $bname = 'Opera';
            $ub = "Opera";
        } elseif (preg_match('/Netscape/i', $u_agent)) {
            $bname = 'Netscape';
            $ub = "Netscape";
        }

        // finally get the correct version number
        $known = array('Version', $ub, 'other');
        $pattern = '#(?<browser>' . join('|', $known) .
                   ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        if (!preg_match_all($pattern, $u_agent, $matches)) {
            // we have no matching number just continue
        }

        // see how many we have
        $i = count($matches['browser']);
        if ($i != 1) {
            //we will have two since we are not using 'other' argument yet
            //see if version is before or after the name
            if (strripos($u_agent, "Version") < strripos($u_agent, $ub)) {
                $version = $matches['version'][0];
            } else {
                $version = $matches['version'][1];
            }
        } else {
            $version = $matches['version'][0];
        }

        // check if we have a number
        if ($version == null || $version == "") {
            $version = "?";
        }

        return array(
            'userAgent' => $u_agent,
            'name'      => $bname,
            'version'   => $version,
            'platform'  => $platform,
            'pattern'   => $pattern
        );
    }

    /**
     * @param $string
     * @param bool $first_char_caps
     *
     * @return mixed
     */
    public static function underscoreToCamelCase($string, $first_char_caps = true)
    {
        if ($first_char_caps) {
            $string[0] = strtoupper($string[0]);
        }

        $func = create_function('$c', 'return strtoupper($c[1]);');

        return preg_replace_callback('/_([a-z])/', $func, $string);
    }
}