<?php

namespace NGS\Util;

class MailSender {

    private $config;

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function send($from, $recipients, $subject, $template, $params = array(), $separate = false)
    {
        $smarty = new NgsSmarty($this->config["static"]);
        $smarty->assign("ns", $params);
        $message = $smarty->fetch($template);
        
        $headers = "";
        $headers .= 'MIME-Version: 1.0' . "\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\n";

        $headers .= "From: $from" . "\n";

        // multiple recipients
        if ($separate) {
            $to = "";
            foreach ($recipients as $recipient) {
                $to .= "$recipient";
                mail($to, $subject, $message, $headers);
            }
        } else {
            $to = "";
            foreach ($recipients as $recipient) {
                $to .= "$recipient" . ',';
            }

            return mail($to, $subject, $message, $headers);
        }
        
        return false;
    }
}