<?php

namespace NGS\Util\DB;

/**
 * ImprovedDBMS class uses MySQL Improved Extension to access DB.
 * This class provides full transaction support instead of DBMS class.
 *
 * @author  Naghashyan Solutions, e-mail: info@naghashyan.com
 * @version 1.0
 */
class ImprovedDBMS {

	/**
	 * Singleton instance of class
	 */
	private static $instance = NULL;

	/**
	 * Object which represents the connection to a MySQL Server
	 */
	private $link;

	/**
	 * DB configuration properties
	 */
	private static $db_host;
	private static $db_user;
	private static $db_pass;
	private static $db_name;
	private static $db_port;
	private static $db_socket;


	/**
	 * Tries to connect to a MySQL Server
	 */
	private function __construct() {
		$this->link = new \mysqli(self::$db_host, self::$db_user, self::$db_pass, self::$db_name, self::$db_port, self::$db_socket);
		mysqli_set_charset($this->link, $GLOBALS["CONFIG"]["SYSTEM_DB_CHARSET"]);
		if (!$this->link) {
			printf("Connect failed: %s\n", mysqli_connect_error());
			exit();
		}

	}

	/**
	 * Initializes MySQL Server connection properties
	 *
	 * @param object $config - associative array, which contains properties
	 * @return
	 */
	public static function init() {
		self::$db_host = $GLOBALS["CONFIG"]["MYSQL_ADMIN_HOST"];
		self::$db_user = $GLOBALS["CONFIG"]["MYSQL_ADMIN_USER"];
		self::$db_pass = $GLOBALS["CONFIG"]["MYSQL_ADMIN_PASSWORD"];
		self::$db_name = $GLOBALS["CONFIG"]["MYSQL_ADMIN_DATABASE"];
		self::$db_port = 3306;
		self::$db_socket = $GLOBALS["CONFIG"]["MYSQL_ADMIN_SOCKET"];
	
	}

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

	/**
	 *	Turns off auto-commiting database modifications
	 */
	public function startTransaction() {
		mysqli_autocommit($this->link, FALSE);
	}

	/**
	 * Commits the current transaction
	 */
	public function commitTransaction() {
		mysqli_commit($this->link);
		mysqli_autocommit($this->link, TRUE);
	}

	/**
	 * Rollback the current transaction
	 */
	public function rollbackTransaction() {
		mysqli_rollback($this->link);
		mysqli_autocommit($this->link, TRUE);
	}

	/////////////////////////// common functions /////////////////////////

	/**
	 * Returns TRUE on success or FALSE on failure.
	 * For SELECT, SHOW, DESCRIBE or EXPLAIN will return a result object.
	 */
	public function query($q) {
		mysqli_query($this->link, "SET character_set_results = 'utf8', character_set_client = 'utf8',
								character_set_connection = 'utf8',
								character_set_database = 'utf8', character_set_server = 'utf8'");
		
		$result = mysqli_query($this->link, $q);
		return $result;
	}
    
     /**
     * Returns TRUE on success or FALSE on failure.
     * For SELECT, SHOW, DESCRIBE or EXPLAIN will return a result object.
     */
    public function multiQuery($q) {
        $this->link->query("SET character_set_results = 'utf8', character_set_client = 'utf8',
        character_set_connection = 'utf8',
        character_set_database = 'utf8', character_set_server = 'utf8'");
        $result = $this->link->multi_query($q);
        if ($result) { 
            $i = 0; 
            do { 
                $i++; 
            } while ($this->link->next_result()); 
        } 
        if ($this->link->errno) { 
            echo "Batch execution prematurely ended on statement $i.\n"; 
            exit;
        } 
        return $result;
    }

    /**
	 * Frees the memory associated with the result object,
	 * which was returnd by query() function.
	 *
	 * @param object $result - object returnd by query() function
	 * @return
	 */
	public function freeResult($result) {
		mysqli_free_result($result);
	}

	/**
	 * Returns the auto generated id used in the last query
	 *
	 * @return The value of the AUTO_INCREMENT field that was updated by the previous query.
	 * Returns zero if there was no previous query on the connection or
	 * if the query did not update an AUTO_INCREMENT value.
	 */
	public function getLastInsertedId() {
		return mysqli_insert_id($this->link);
	}

	/**
	 * Gets the number of affected rows in a previous MySQL operation
	 * An integer greater than zero indicates the number of rows affected or retrieved.
	 * Zero indicates that no records where updated for an UPDATE statement,
	 * no rows matched the WHERE clause in the query or that no query has yet been executed.
	 * -1 indicates that the query returned an error.
	 */
	public function getAffectedRows() {
		return mysqli_affected_rows($this->link);
	}

	/**
	 * Fetch a result row as an associative array
	 */
	public function getResultArray($res) {
		$results = array();
		if ($res) {
			while ($t = mysqli_fetch_assoc($res)) {
				$results[] = $t;
			}
			return $results;
		} else {
			die();
		}
	}

	/**
	 * Gets the number of rows in a result
	 */
	public function getResultCount($res) {
		if ($res) {
			return mysqli_num_rows($res);
		}
		return false;
	}

	/**
	 * Escapes special characters in a string for use in a SQL statement,
	 * taking into account the current charset of the connection
	 *
	 * @return an escaped string.
	 */
	public function escape($str, $trim = false) {
		if ($trim) {
			$str = trim($str);
		}
		return function_exists('mysqli_real_escape_string') ? mysqli_real_escape_string($this->link, $str) : $str;
	}

}
