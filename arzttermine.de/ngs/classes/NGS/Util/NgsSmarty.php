<?php

namespace NGS\Util;

use Arzttermine\Application\Application;

class NgsSmarty extends \SmartyBC {

    /**
     * constructor
     * reading Smarty config and setting up smarty environment accordingly
     */
    public function __construct($config, $version = 0)
    {
        parent::__construct();

        $defaults = array(
            "TEMPLATE_DIR"      => '',
            "COMPILE_DIR"       => '',
            "PLUGIN_DIR"        => '',
            "DEBUG"             => false,
            "FORCE_COMPILE"     => false,
            "COMPILE_CHECK"     => false
        );

        $configuration = array_merge($defaults, $GLOBALS['CONFIG']['SMARTY']);

        $this->debugging  = $configuration['DEBUG'];
        $this->compile_check = $configuration['COMPILE_CHECK'];
        $this->force_compile = $configuration['FORCE_COMPILE'];
        $this->error_reporting = E_ALL & ~E_NOTICE;

        // Add the AT template path too. Hardcoded and nasty
        $this->setCompileDir($configuration['COMPILE_DIR'])
             ->setTemplateDir(SYSTEM_PATH . '/ngs/templates')
             ->addTemplateDir($configuration['TEMPLATE_DIR'])
             ->setPluginsDir($configuration['PLUGIN_DIR'])
             ->addPluginsDir(SYSTEM_PATH . '/include/lib/smarty/plugins/');
        
        $protocol = "http://";
        if (Application::getInstance()->isSecure()) {
            $protocol = "https://";
        }

        $this->assign("STATIC_URL", $_SERVER["HTTP_HOST"])
             ->assign("SITE_URL", $_SERVER["HTTP_HOST"])
             ->assign("STATIC_PATH", $protocol.$_SERVER["HTTP_HOST"])
             ->assign("PROTOCOL", $protocol)
             ->assign("SITE_PATH", $protocol.$_SERVER["HTTP_HOST"])
             ->assign("VERSION", $version);

        // Add the AT variables to the NGS templates.
        $this->assignGlobal("this", Application::getInstance()->getView());
        $this->assignGlobal("profile", isset($GLOBALS['profile']) ? $GLOBALS['profile'] : '');
        $this->assignGlobal("app", Application::getInstance());
    }

    /**
     * Returns the url for a static asset
     *
     * @param string $name
     * @param bool $absolute_url
     * @param bool $add_timestamp
     *
     * @return string
     **/
    public function getStaticUrl($name, $absolute_url = false, $add_timestamp = false)
    {
        $url = '';

        if ($absolute_url) {
            $url .= $GLOBALS['CONFIG']['URL_HTTP_LIVE'];
        }

        $url .= $name;

        if ($add_timestamp) {
            $_filename = $GLOBALS['CONFIG']['STATIC_PATH'] . "../" . $name;
            if (file_exists($_filename)) {
                $url .= '?'.filemtime($_filename);
            }
        }

        return $url;
    }
}