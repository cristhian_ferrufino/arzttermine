<?php

namespace NGS\Util;

use Arzttermine\Application\Application;
use Arzttermine\Booking\Appointment;
use Arzttermine\Calendar\Calendar;
use Arzttermine\Integration\Integration;
use Arzttermine\Location\Location;
use Arzttermine\User\User;
use Arzttermine\Core\Configuration;
use NGS\DAL\DTO\SearchResultCacheDto;
use NGS\Managers\LocationsManager;
use NGS\Managers\SearchResultCacheTmpManager;

class ProviderRow {

    /**
     * @var SearchResultCacheDto[]
     */
    public $searchResultCaches = array();

    /**
     * @var
     */
    private $user;

    /**
     * @var
     */
    private $location;

    /**
     * @var
     */
    private $medicalSpecialtyId;

    /**
     * @var
     */
    private $treatmentTypeId;

    /**
     * sql result array from the search
     */
    private $searchResultData = array();

    /**
     * @param $userId
     * @param $locationId
     */
    public static function cacheDoctorSearchResultInfo($userId, $locationId)
    {
        ini_set('memory_limit', '500M');
        $searchResultCacheTmpManager = SearchResultCacheTmpManager::getInstance();
        $location = new Location($locationId);
        $user = new User($userId);
        //updating user/location profile asset filenames
        $location->updateProfileAssetFilename();
        $user->updateProfileAssetFilename();
        $integrationId = $location->getIntegrationId();
        if ($integrationId != 8 && $integrationId != 9 && $integrationId != 10) {
            $allAvailableAppointments = self::getAllAvailableAppointments($userId, $locationId, $integrationId);
            // store all available appointments to table "search_result_cache_tmp"
            // for this doctor
            $searchResultCacheTmpManager->addRows($userId, $locationId, $allAvailableAppointments, $integrationId);
        }
    }

    /**
     * @param $userId
     * @param $locationId
     * @param $integrationId
     *
     * @return bool|mixed
     */
    private static function getAllAvailableAppointments($userId, $locationId, $integrationId)
    {
        $ret = array();
        $user = new User($userId);
        
        if ($user->getStatus() == User::STATUS_VISIBLE_APPOINTMENTS) {
            $_integration = Integration::getIntegration($integrationId);
            $datetime_start = Calendar::getFirstPossibleDateTimeForSearch();
            $datetime_end = Calendar::getLastPossibleDateTimeForSearch();
            if (!isset($_integration) || !$_integration) {
                return false;
            }
            $ret = $_integration->getAvailableAppointments($datetime_start, $datetime_end, $userId, $locationId);
        }

        return $ret;
    }

    /**
     * @return mixed
     */
    public function getMedicalSpecialtyId()
    {
        return $this->medicalSpecialtyId;
    }

    /**
     * @param $medicalSpecialtyId
     *
     * @return $this
     */
    public function setMedicalSpecialtyId($medicalSpecialtyId)
    {
        $this->medicalSpecialtyId = $medicalSpecialtyId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTreatmentTypeId()
    {
        return $this->treatmentTypeId;
    }

    /**
     * @param $treatmentTypeId
     *
     * @return $this
     */
    public function setTreatmentTypeId($treatmentTypeId)
    {
        $this->treatmentTypeId = $treatmentTypeId;

        return $this;
    }

    /**
     * @param $searchResultCaches
     *
     * @return $this
     */
    public function setSearchResultCaches($searchResultCaches)
    {
        $this->searchResultCaches = $searchResultCaches;

        return $this;
    }

    /**
     * these methods get their data from the searchResultData array *******************
     */

    /**
     * Return the complete array or just a part of the searchResultData array
     *
     * @param key
     *
     * @return mixed
     */
    public function getSearchResultData($key = null)
    {
        if ($key != null && !isset($this->searchResultData[$key])) {
            return null;
        } else {
            return $this->searchResultData[$key];
        }
    }

    /**
     * @param $data
     *
     * @return $this
     */
    public function setSearchResultData($data)
    {
        $this->searchResultData = $data;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDistance()
    {
        return $this->searchResultData['distance'];
    }

    /**
     * Get the data for an appointments block
     *
     * This method returns as many days as in date_days. It skips weekdays so the date_end should be recalculated
     * in all calling methods if necessary.
     *
     * @param string $date_start
     * @param string $date_end
     * @param int $insuranceId
     * @param int $medicalSpecialtyId
     * @param int $treatmenttypeId (optional)
     * @param string $widgetSlug (optional)
     *
     * @return \StdClass StdClass->type = string (
     *                    'no-available-appointments'
     *                    'no-selected-insurance',
     *                    'custom-message',
     *                    'appointments'
     *                          ),
     *         StdClass->days[date] array Appointment,
     *         StdClass->days[date]->date string
     *         StdClass->customMessage = string
     **/
    public function getAppointmentsBlock($date_start, $date_end, $insuranceId, $medicalSpecialtyId, $treatmenttypeId = null, $widgetSlug = '')
    {
        $app = Application::getInstance();
        $appointmentsBlock = array();

        $user = $this->getUser();
        $userId = $user->getId();
        $location = $this->getLocation();
        $locationId = $location->getId();

        if (!is_numeric($insuranceId) || $insuranceId < 1) {
            $appointmentsBlock['type'] = 'no-selected-insurance';

            return $appointmentsBlock;
        }

        // Check if there's a custom message stored against this doctor
        $customMessageFormula = Configuration::get('doctors_calendar_custom_message');
        $customMessage = self::isCustomMessageDoctor($userId, $insuranceId, $customMessageFormula);
        if ($customMessage !== false) {
            $appointmentsBlock['type'] = 'custom-message';
            $appointmentsBlock['customMessage'] = $customMessage;

            return $appointmentsBlock;
        }

        /**
         * integration8/docdir **************************************
         **/
        if ($location->getIntegrationId() == 8) {
            if ($user->getStatus() != USER_STATUS_VISIBLE_APPOINTMENTS) {
                $appointmentsBlock['type'] = 'no-available-appointments';

                return $appointmentsBlock;
            }

            // Seems unnecessary now, but we'll be doing this moving forward
            $_appointment = new Appointment();

            $appointmentsBlock['type'] = 'integration_8';
            $appointmentsBlock['url'] = $_appointment->getUrl($userId, $locationId, $insuranceId, $medicalSpecialtyId, (isset($treatmenttypeId) ? $treatmenttypeId : ''));
            $appointmentsBlock['text'] = $app->_('Freie Termine anfragen');
            $appointmentsBlock['customMessage'] = $app->_('Wir kümmern uns darum und rufen Sie zurück!');

            return $appointmentsBlock;
        }

        /**
         * integration_9 *********************************************
         * return a block of customTexted appointments
         **/
        if ($location->getIntegrationId() == 9) {
            $daysToHide = array();

            if ($location->getStatus() != LOCATION_STATUS_VISIBLE_APPOINTMENTS) {
                $appointmentsBlock['type'] = 'no-available-appointments';

                return $appointmentsBlock;
            }
            $appointmentsBlock['type'] = 'integration_9';
            $appointmentsBlock['widgetSlug'] = $widgetSlug;

            // fill the days with no appointments to make it easier for the template to iterate
            $days = Calendar::getDatesInAreaWithFilter($date_start, $date_end);

            // filter some days out
            $daysNumberToHide = intval(Configuration::get('hide_all_int9_doctors_apts_for_next_days'));
            if ($daysNumberToHide > 0) {
                $daysToHide = Calendar::getDatesInArea($date_start, Calendar::addDays($date_start, $daysNumberToHide));
            }

            foreach ($days as $date) {
                $appointmentsForOneDay = array();

                if (
                    !in_array($date, $daysToHide)
                    &&
                    Calendar::isWorkingDay($date)
                ) {
                    $appointment = new Appointment($date . ' 08:00:00');
                    $appointment->setDateTimeEnd($date . ' 10:00:00');
                    $appointment->setCustomText('8 - 10');
                    $appointmentsForOneDay[] = $appointment;
                    $appointment = new Appointment($date . ' 10:00:00');
                    $appointment->setDateTimeEnd($date . ' 14:00:00');
                    $appointment->setCustomText('10 - 14');
                    $appointmentsForOneDay[] = $appointment;
                    $appointment = new Appointment($date . ' 16:00:00');
                    $appointment->setDateTimeEnd($date . ' 20:00:00');
                    $appointment->setCustomText('16 - 20');
                    $appointmentsForOneDay[] = $appointment;

                    // set the appointments to the coresponding settings to be used in the template
                    /** @var Appointment $appointment */
                    foreach ($appointmentsForOneDay as &$appointment) {
                        $appointment
                            ->setUserId($userId)
                            ->setLocationId($locationId)
                            ->setInsuranceId($insuranceId)
                            ->setMedicalSpecialtyId($medicalSpecialtyId)
                            ->setTreatmenttypeId($treatmenttypeId);
                    }
                }
                // store the day's date
                $appointmentsBlock['days'][$date]['date'] = $date;
                $appointmentsBlock['days'][$date]['date_text'] = date('d.m.', strtotime($date));
                $appointmentsBlock['days'][$date]['day_text'] = Calendar::getWeekdayTextByDateTime($date, true);
                // store the appointments
                $appointmentsBlock['days'][$date]['appointments'] = $appointmentsForOneDay;
            }

            return $appointmentsBlock;
        }

        $searchResultCaches = $this->getSearchResultCaches();
        $days_array = array();

        /**
         * CONDITION until we removed NGS stuff
         *
         * @todo: refactor all "if (!empty($searchResultCaches)) {" blocks
         * to not use the searchResultCaches but only the days_array with the Appointment Objects
         */
        if ($location->getIntegrationId() == 10) {
            $integration = $location->getIntegration();
            // @todo: change ALL integrations to match this new parameters (incl. medicalSpecialty) or better: provide an array with filters[insuranceId, medicalSpecialtyId, treatmentTypeId]
            $days_array = $integration->getAvailableAppointments($date_start, $date_end, $userId, $locationId, $insuranceId, $treatmenttypeId, $medicalSpecialtyId, 'Appointment');
        }

        /**
         * no-available-appointments **************************************************
         **/
        if (empty($days_array) && empty($searchResultCaches)) {
            $appointmentsBlock['type'] = 'no-available-appointments';

            return $appointmentsBlock;
        }

        /**
         * next-available-appointments ************************************************
         **/
        if (!empty($searchResultCaches)) {
            if (count($searchResultCaches) === 1) {
                $searchResultCache = $searchResultCaches[0];
                $date = $searchResultCache->getDate();
                if ($date > $date_end) {
                    $appointmentsBlock['appointment'] = self::convertSearchResultCacheToAppointment($searchResultCache);
                    $appointmentsBlock['type'] = 'next-available-appointment';

                    return $appointmentsBlock;
                }
            }
        }

        /**
         * appointments ***************************************************************
         **/
        // Normal Appointments block (int1,2,5)

        $appointmentsBlock['type'] = 'appointments';

        $appointment = new Appointment(); // Used to generate the URL
        $found_appointments = array();

        if (!empty($searchResultCaches)) {
            // Sort the hits we have and just take the data we need
            foreach ($searchResultCaches as $found_appointment) {
                $appointment->setDatetime($found_appointment->getAppointment());
                $found_appointments[$found_appointment->getDate()][] = array(
                    'url' => $appointment->getUrl($userId, $locationId, $insuranceId, $medicalSpecialtyId, $treatmenttypeId),
                    'time_text' => $appointment->getTimeText($found_appointment->getAppointment())
                );
            }

            // Loop through each available day in the query (usually about 20 days)
            
            foreach (Calendar::getDatesInAreaWithFilter($date_start, $date_end) as $date) {
                // Set the output data for this. We need labels etc
                $appointmentsBlock['days'][$date]['date_text'] = date('d.m.', strtotime($date));
                $appointmentsBlock['days'][$date]['day_text']  = Calendar::getWeekdayTextByDateTime($date, true);

                // If we also found appointments above, add them
                if (isset($found_appointments[$date])) {
                    $appointmentsBlock['days'][$date]['appointments'] = $found_appointments[$date];
                }
            }
        }

        return $appointmentsBlock;
    }

    /**
     * Returns the User
     * If the User is not set, than try to get the user_id from the searchResultData array
     * and initialize it and store it
     *
     * @return User
     */
    public function getUser()
    {
        $user = $this->user;
        if (!is_object($user)) {
            $user_id = 0;
            if (isset($this->searchResultData['user_id'])) {
                $user_id = $this->searchResultData['user_id'];
            }
            $user = new User($user_id);
            $this->setUser($user);
        }

        return $user;
    }

    /**
     * @param $user
     *
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Returns the Location
     * If the Location is not set, than try to get the location_id from the searchResultData array
     * and initialize it and store it
     *
     * @return Location
     */
    public function getLocation()
    {
        $location = $this->location;
        if (!is_object($location)) {
            $location_id = 0;
            if (isset($this->searchResultData['location_id'])) {
                $location_id = $this->searchResultData['location_id'];
            }
            $location = new Location($location_id);
            $this->setLocation($location);
        }

        return $location;
    }

    /**
     * @param $location
     *
     * @return $this
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * @param $userId
     * @param $insuranceId
     * @param $cmsCustomMessageFormula
     *
     * @return bool|string
     */
    static function isCustomMessageDoctor($userId, $insuranceId, $cmsCustomMessageFormula)
    {
        $doctors_calendar_custom_message = $cmsCustomMessageFormula;
        if (!empty($doctors_calendar_custom_message)) {
            $doctors_calendar_custom_message_array = explode('^', $doctors_calendar_custom_message);
            foreach ($doctors_calendar_custom_message_array as $value) {
                $seperatorChar = '->';
                $firstSeperatorPos = strpos($value, $seperatorChar);
                $secondSeperatorPos = strpos($value, $seperatorChar, $firstSeperatorPos + strlen($seperatorChar));
                $customMessageUserId = substr($value, 0, $firstSeperatorPos);
                $customMessageInsuranceId = substr($value, $firstSeperatorPos + strlen($seperatorChar), $secondSeperatorPos - ($firstSeperatorPos + strlen($seperatorChar)));
                if ($userId == $customMessageUserId && $insuranceId == $customMessageInsuranceId) {
                    $customCaseMessage = trim(trim(substr($value, $secondSeperatorPos + strlen($seperatorChar))), '"');

                    return Application::getInstance()->_($customCaseMessage);
                }
            }
        }

        return false;
    }

    /**
     * @return SearchResultCacheDto[]
     */
    public function getSearchResultCaches()
    {
        return $this->searchResultCaches;
    }

    /**
     * @param SearchResultCacheDto $searchResultCache
     *
     * @return Appointment
     */
    static function convertSearchResultCacheToAppointment($searchResultCache)
    {
        return new Appointment($searchResultCache->getAppointment());
    }

    /**
     * return the next available appointment text
     * Used in mobile search result list.
     *
     * @return Appointment text (like Do 07.01.2014, 10:45) or empty string on failure
     **/
    public function getNextAvailableAppointmentText()
    {
        $appointment = $this->getNextAvailableAppointment();
        if (!$appointment->isValid()) {
            return '';
        }
        if (date('Y-m-d', strtotime($appointment->getDateTime())) == date('Y-m-d', strtotime('+1 day'))) {
            return "Morgen um " . $appointment->getTimeText() . " Uhr";
        } else {
            return $appointment->getDateTimeText(true);
        }
    }

    /**
     * return the next available appointment
     *
     * @return Appointment
     **/
    public function getNextAvailableAppointment()
    {
        $searchResultCaches = $this->getSearchResultCaches();
        if (!isset($searchResultCaches[0])) {
            return new Appointment();
        }

        return self::convertSearchResultCacheToAppointment($searchResultCaches[0]);
    }

    /**
     * Crappy kludge designed to get our message
     *
     * @todo Remove
     */
    public function getMobileDoctorCustomMessage($userId, $insuranceId)
    {
        return self::mobileDoctorIsCustomMessage($userId, $insuranceId);
    }

    /**
     * @param $userId
     * @param $insuranceId
     *
     * @return bool|string
     */
    static function mobileDoctorIsCustomMessage($userId, $insuranceId)
    {
        return self::isCustomMessageDoctor($userId, $insuranceId, Configuration::get('mobile_doctors_calendar_custom_message'));
    }
}
