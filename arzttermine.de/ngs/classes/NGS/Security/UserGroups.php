<?php

namespace NGS\Security;

/**
 * Contains definitions for all participant roles in system.
 *
 * @author  Naghashyan Solutions, e-mail: info@naghashyan.com
 * @version 1.0
 * @package security
 */
class UserGroups {

	/**
	 * @var System administrator
	 */
	public static $ADMIN = 0;

	/**
	 *
	 */
	public static $PATIENT = 2;

	/**
	 *
	 */
	public static $DOCTOR = 4;

	/**
	 * @var Non authorized user with minimum privileges
	 */
	public static $GUEST = 6;

}