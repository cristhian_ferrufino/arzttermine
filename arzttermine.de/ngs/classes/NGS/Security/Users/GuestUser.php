<?php

namespace NGS\Security\Users;
use NGS\Framework\AbstractUser;
use NGS\Security\UserGroups;

/**
 * User object for non authorized users.
 * 
 * @author  Naghashyan Solutions, e-mail: info@naghashyan.com
 * @version 1.0
 * @package security.users
 */
class GuestUser extends AbstractUser {

	/**
	 * Constructs GUEST user object 
	 */
	public function __construct(){
		$this->setSessionParam("ut", UserGroups::$GUEST);
	}
	
  /**
   * Returns user's level
   * @abstract  
   * @access
   * @param 
   * @return int 
   */ 
	   
	public function getLevel(){	
		return $this->getSessionParam("ut");
	}
	
  /**
	 * There is no validation needed for GUEST users
	 * 
   * @abstract  
   * @access
   * @param 
   * @return true 
   */ 

	public function validate(){
		return true;
	}
}