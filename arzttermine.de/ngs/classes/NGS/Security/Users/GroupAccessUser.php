<?php

namespace NGS\Security\Users;

use NGS\Managers\UsersManager;

abstract class GroupAccessUser extends AuthenticateUser {

    protected $groupId;

    protected $groupAccesses;

    private $usersManager;

    public function __construct($id)
    {
        parent::__construct($id);
        $this->usersManager = UsersManager::getInstance();
        $this->groupId = $this->usersManager->getUserById($this->getId())->getGroupId();
        $this->groupAccesses = $this->usersManager->getUserGroupAccessesArrayByUserId($this->getId());
    }

    public function validate()
    {
        $userDto = $this->usersManager->getUserByIdAndHash($this->id, $this->uniqueId);
        if ($userDto) {
            return true;
        }

        return false;
    }

    public function getGroupId()
    {

        return $this->groupId;
    }

    public function getGroupAccesses()
    {

        return $this->groupAccesses;
    }

    public function checkPrivilege($actionName)
    {
        switch ($actionName) {
            case 'user_view':
            case 'user_gallery_admin':
                if ($this->groupId == ADMIN_GROUP_ADMIN) {
                    return true;
                }
            case 'user_gfx_new':
            case 'user_gallery_new':
            case 'user_profile_edit':
                if (isset($data['user_id'])) {
                    return $this->id == $data['user_id'];
                }
            // GROUP ACCESSES ********************************
            default:
                return in_array($actionName, $this->groupAccesses);
        }
    }
}