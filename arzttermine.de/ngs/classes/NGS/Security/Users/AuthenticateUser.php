<?php

namespace NGS\Security\Users;

use NGS\Framework\AbstractUser;

abstract class AuthenticateUser extends AbstractUser {

    /**
     * @var - unique identifier per session
     */
    protected $uniqueId;

    /**
     * @var - user's invariant identifier
     */
    protected $id;

    public function __construct($id)
    {
        $this->setId($id);
    }

    public function setId($id)
    {
        $this->id = $id;
        $this->setSessionParam("ud", $id);
    }

    public function setUniqueId($uniqueId)
    {
        $this->uniqueId = $uniqueId;
        $this->setSessionParam("uh", $uniqueId);
    }

    public function getUniqueId()
    {
        return $this->getSessionParam("uh");
    }

    public function getId()
    {
        return $this->getSessionParam("ud");
    }

    /**
     * Returns ADMIN level.
     *
     * @return int
     */
    public function getLevel()
    {
        return $this->getSessionParam("ut");
    }
}