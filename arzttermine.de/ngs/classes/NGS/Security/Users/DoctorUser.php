<?php

namespace NGS\Security\Users;

use NGS\Managers\UsersManager;
use NGS\Security\UserGroups;

/**
	* User class for system administrators.
	* 
	* @author  Naghashyan Solution, e-mail: info@naghashyan.com
	* @version 1.0
	* @package security.users
	*/
class DoctorUser extends GroupAccessUser {
	private $usersManager;
		/**
		* Creates en instance of admin user class and
		* initializes class members necessary for validation. 
		* 
		* @param object $adminId
		* @return 
		*/
	public function __construct($id){
		parent::__construct($id);
		$this->setSessionParam("ut", UserGroups::$DOCTOR);
		$this->usersManager = UsersManager::getInstance();
	}

	public function validate(){
		$userDto = $this->usersManager->getUserByIdAndHash($this->id, $this->uniqueId);
		if($userDto){
			if($userDto->getActivatedAt() != "0000-00-00 00:00:00"){
				return true;
			}
		}

		return false;
	}
	
}