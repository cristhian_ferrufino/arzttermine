<?php

namespace NGS\Security\Users;

use NGS\Security\UserGroups;

class AdminUser extends GroupAccessUser {

    public function __construct($id)
    {
        parent::__construct($id);
        $this->setSessionParam("ut", UserGroups::$ADMIN);
    }
}