{*}
    <!-- 
        Template used for client-side password validation on doctor and patient registration pages, 
        as well as on password reset pages.

        There should be only one element with the id="password" in the including page.
        Also, the form containing the password input field should have one button with type="submit", 
        either as a <button> or as an <input>.
    -->
{/*}

<link rel="stylesheet" type="text/css" href="{$STATIC_PATH}/ngs/css/register-password.css">

<input id="password" type="password" name="password" value="" required/>
<div id="password-validator-container">
    <div class="password-validator">
        <div>Ihr Passwort sollte...</div>
        <div id="has-letters"><div class="bullet"></div>Buchstaben beinhalten</div>
        <div id="has-numbers"><div class="bullet"></div>Ziffern beinhalten</div>
        <div id="has-enough-chars"><div class="bullet"></div>mind. 8 Zeichen lang sein</div>
{*}
Removed: Data security expert Dr. Graeber didn't like the wording and adviced to remove it completely
        <div id="has-all-valid"><div class="bullet"></div>Klasse! Dies ist ein sehr sicheres Passwort.</div>
{/*}
    </div>
</div>
<script>
    {literal}
    $(document).ready(function() {
        setSubmitButtonDisabled(true);
    });

    function setSubmitButtonDisabled(isDisabled) {
        // somewhere it's <button>, somewhere <input>
        var submit = $("#password").parents("form").find("*[type='submit']");
        if (isDisabled) {
            submit.attr("disabled", "true");
        }
        else {
            submit.removeAttr("disabled");
        }
    }

    function isPasswordValid() {
        var isValid = true;
        var pass = $("#password").val();

        if (pass.match( /[A-Za-z]/ )) {
            $("#has-letters").removeClass("invalid");
            $("#has-letters").addClass("valid");
        }
        else {
            $("#has-letters").addClass("invalid");
            $("#has-letters").removeClass("valid");
            isValid = false;
        }

        if (pass.match( /[0-9]/ )) {
            $("#has-numbers").removeClass("invalid");
            $("#has-numbers").addClass("valid");
        }
        else {
            $("#has-numbers").addClass("invalid");
            $("#has-numbers").removeClass("valid");
            isValid = false;
        }

        if (pass.length >= 8) {
            $("#has-enough-chars").removeClass("invalid");
            $("#has-enough-chars").addClass("valid");
        }
        else {
            $("#has-enough-chars").addClass("invalid");
            $("#has-enough-chars").removeClass("valid");
            isValid = false;
        }
        return isValid;
    }

    // Checks whether the password meets our criteria: has letter, has number, has 8 chars.
    function validatePassword() {
        var pass = $("#password").val();

        if (pass.length === 0) {
            $(".password-validator > div[id^=has-]").removeClass("invalid valid");
            $("#password-validator-container").removeClass("valid");
            setSubmitButtonDisabled(true);
            return;
        }

        var isValid = isPasswordValid();

        if (isValid) {
            $("#password-validator-container").addClass("valid");
            var form = $("#password").parents("form");
            setSubmitButtonDisabled(false);
        }
        else {
            $("#password-validator-container").removeClass("valid");
            setSubmitButtonDisabled(true);
        }

    }
    $("#password").keyup(function(){
        validatePassword();
    });
    $("#password").on("focus", function(){
       var password = $("#password");
       var container = $("#password-validator-container");
       container.show().offset({top: password.offset().top - container.outerHeight() - 15, left: password.offset().left})
    });
    $("#password").on("blur", function(){
        var container = $("#password-validator-container");

        if(isPasswordValid()) {
            container.hide();
        }
    });

    {/literal}
</script>
