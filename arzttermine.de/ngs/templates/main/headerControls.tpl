<script type="text/javascript"  src="{$STATIC_PATH}/ngs/js/lib/jquery_1.9.min.js" ></script>
<script type="text/javascript" src="{$STATIC_PATH}/ngs/js/lib/jquery-ui.js"></script>
<script type="text/javascript" src="{$STATIC_PATH}/ngs/js/lib/jquery.unparam.js"></script>
<script type="text/javascript"  src="{$STATIC_PATH}/ngs/js/main-jQ.js" ></script>
<script type="text/javascript" src="{$STATIC_PATH}/static/jquery/i18n/jquery.ui.datepicker-de.js"></script>
<script type="text/javascript" src="{$STATIC_PATH}/static/jquery/plugins/jquery-ui-timepicker-addon-0.5.min.js"></script>
<link rel="stylesheet" href="{$STATIC_PATH}/static/jquery/plugins/jquery-ui-timepicker-addon-0.5.css"/>
<!-- LET'S TRY AND REMOVE THIS 
<link rel="stylesheet" href="{$STATIC_PATH}/static/jquery/layout/smothness/jquery.ui.datepicker.css"/>
-->


<script type="text/javascript">
{literal}
	var ngs = {};
{/literal}
	var ngs_URL = "{$STATIC_URL}";
	var site_PATH = "{$SITE_PATH}";
	var STATIC_URL = "{$STATIC_URL}";
	var STATIC_PATH = "{$STATIC_PATH}";	
	
  function _sendRequest(_url, postData, callBack) {
	 $.ajax({
	 url : _url,
	 type : 'GET',
	 data : postData,
	 context : document.body
	 }).done( function(data) {
	 callBack(data,postData);
	 });
	}
</script>
