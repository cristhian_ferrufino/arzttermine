{if $ns.pageCount>1}
<td valign="top" colspan="2">
<div class="tablenav-pages" id="f_pageingBox">
	More Items:
	<div id="pageBox">
		
		{if $ns.page > 1}
		<a id="f_prev" href="{$ns.paramsUrl}{$ns.paginationParamName}={$ns.page-1}{if isset($ns.requestString)}{$ns.requestString}{/if}" class="prev-btn navbutton">&lt;&lt;</a>
		{else}
		<span class="prev-btn ">&lt;&lt;</span>
		{/if}
		 
		{if $ns.pStart+1>1}
			<a class="f_pagenum" id="tplPage_1" href="{$ns.paramsUrl}{$ns.paginationParamName}=1{if isset($ns.requestString)}{$ns.requestString}{/if}">1</a>
			{if $ns.pStart+1>2}
				<span>...</span>
			{/if}
		{/if}
		
		{section name=pages loop=$ns.pEnd start=$ns.pStart}
		{if ($ns.page) != ($smarty.section.pages.index+1)}
		<a class="page-numbers" id="tplPage_{$smarty.section.pages.index+1}" href="{$ns.paramsUrl}{$ns.paginationParamName}={$smarty.section.pages.index+1}{if isset($ns.requestString)}{$ns.requestString}{/if}">{$smarty.section.pages.index+1}</a>
		{else}
		<span class="page-numbers current">{$smarty.section.pages.index+1}</span>
		{/if}
		{/section}
		{if $ns.pageCount > $ns.pEnd}
			{if $ns.pageCount > $ns.pEnd + 1}
				<span >...</span>
			{/if}
			<a class="page-numbers" id="tplPage_{$ns.pageCount}" href="{$ns.paramsUrl}{$ns.paginationParamName}={$ns.pageCount}{if isset($ns.requestString)}{$ns.requestString}{/if}">{$ns.pageCount}</a>
		{/if}
		
		
		{if $ns.page == $ns.pageCount}
		<span class="next-btn ">&gt;&gt;</span>
		{else}		
		<a id="f_next" href="{$ns.paramsUrl}{$ns.paginationParamName}={$ns.page+1}{if isset($ns.requestString)}{$ns.requestString}{/if}" class="next-btn navbutton">&gt;&gt;</a>
		{/if}
		<div style="float:right;">
            <form action="" method="post" class="paging">
				{if $ns.page > 1}
				<a href="{$ns.paramsUrl}{$ns.paginationParamName}={$ns.page-1}{if isset($ns.requestString)}{$ns.requestString}{/if}">«</a>
				{else}
				<a class="disabled">«</a>
				{/if}
				<select name="pagination">
					{section name=pages loop=$ns.pageCount start=$ns.pStart}
						<option value="{$smarty.section.pages.index+1}" {if ($smarty.section.pages.index+1)==$ns.page}selected='selected'{/if}>
							{math equation="(page-1)*itemsPerPage+1" page=$smarty.section.pages.index+1 itemsPerPage=$ns.itemsPerPage}-
							{math equation="page*itemsPerPage" page=$smarty.section.pages.index+1 itemsPerPage=$ns.itemsPerPage}
                        </option>
					{/section}
				</select>
                <a class="button-secondary">OK</a>
				{if $ns.page == $ns.pageCount}
				<a class="disabled">»</a>
				{else}
				<a href="{$ns.paramsUrl}{$ns.paginationParamName}={$ns.page+1}{if isset($ns.requestString)}{$ns.requestString}{/if}">»</a>
				{/if}
			</form>		
		</div>
	</div>
	<input type="hidden" id="f_curPage" value="{$ns.page}">
	<input type="hidden" id="f_pageCount" value="{$ns.pageCount}">
</div>
</td>
{/if}
{literal}
<script>
    $(document).ready(function() {
        var form = $("form.paging");
        form.find('select[name="pagination"]').change(function() {
            var page = $(this).val();
            form.find("a.button-secondary").attr("href", "{/literal}{$ns.paramsUrl}{$ns.paginationParamName}=" + page + "{if isset($requestString)}{$requestString}{/if}"+{if isset($ns.referral_type)}"&referral_type={$ns.referral_type}"{/if}{literal});
        });
    });
</script>
{/literal}