<td>{$ns.itemsCount}</td>
<td>
    <span> Rows per page:</span>
    <form method="post" style="display:inline;" class="rows_per_page">
        <select name="rpp">
            {foreach from=$ns.allOptions  item=rpp}
                <option value="{$rpp}" {if $rpp == $ns.itemsPerPage}selected='selected'{/if}>{$rpp}</option>
            {/foreach}
        </select>
        <a class="button-secondary">Go</a>
    </form>
</td>

{literal}
<script>
    $(document).ready(function() {
        var form = $("form.rows_per_page");
        form.find('select[name="rpp"]').change(function() {
            var rowsPerPage = $(this).val();
            form.find("a.button-secondary").attr("href", "{/literal}{$ns.paramsUrl}{$ns.paramName}=" + rowsPerPage + "{if isset($requestString)}{$requestString}{/if}"+{if isset($ns.referral_type)}"&referral_type={$ns.referral_type}"{/if}{literal});
        });
    });
</script>
{/literal}