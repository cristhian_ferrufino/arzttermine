{if $ns.medicalSpecialtyDto->getTreatmentTypesDtos()|count > 0}
<div class="specialty-treatments" id="treatment-type-{$ns.medicalSpecialtyDto->getId()}">
    <h4>{$ns.medicalSpecialtyDto->getNameSingularDe()} Behandlungsarten</h4>
    <ul>
    {foreach from=$ns.medicalSpecialtyDto->getTreatmentTypesDtos() item=treatmentTypeDto}
        {assign var="isChecked" 
                value=in_array($treatmentTypeDto->getId(), $ns.treatmentTypesIdsArrayByDoctorIdAndMedSpecId)}
        <li>
            <label>
              <input type="checkbox" name="treatment_types[]" value="{$treatmentTypeDto->getId()}" {if $isChecked}checked{/if}>
              {$treatmentTypeDto->getNameDe()}
            </label>
        </li>
    {/foreach}
    </ul>
</div>
{/if}
