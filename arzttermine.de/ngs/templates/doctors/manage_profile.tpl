{include "doctors/header.tpl"}

<div id="main" class="middle-responsiv profile-edit doctor profile">
	<div id="header">
        <div class="left profile-title">{$doctor->getDoctor()->getTitle()} {$doctor->getDoctor()->getFirstName()} {$doctor->getDoctor()->getLastName()}</div>
		<div id="optinal-function">
            <span class="arrow"></span>
            <ul id="item-list" style="display: none;">
				{foreach from=$locations item=location_item}
                    <li class="first locationName"><span>{$location_item->getName()}</span></li>
				{/foreach}
				<li><a href="/kontakt" target="_blank">Kontakt</a></li>
				<li class="last">{if isset($ns.userId) and $ns.userId > 0}<a href="//{$SITE_URL}/dyn/{$ns.arzconfig.DOCTORS_LOGIN_URL}/do_logout">abmelden</a>{/if}</li>
			</ul>
		</div>
	</div>
	<div id="doc-manager-edit" class="doc_border">
		<div id="edit-left_side">
			{include "doctors/left_panel.tpl"}
		</div>
		
		<div id="doc-content-main" class="doc_border">
			<h3>Arztprofil bearbeiten</h3>
		{if isset($ns.error)}
			<div class="error">{$ns.error.error_message}</div>
		{/if}
			<form id="form-profile-edit" action="{$SITE_PATH}/{$ns.arzconfig.DOCTORS_LOGIN_URL}/do_speichern_aerzte_profil/{$doctor->getDoctor()->getSlug()}" method="post">
				{* ID of the doctor we're editing *}
				<input type="hidden" name="uid" value="{$doctor->getDoctor()->getId()}" />
				<table class="info_table" border="0">
                    <tr>
                        <td class="info_label" >Arzt</td>
                        <td class="info_value">
                        {$doctor->getDoctor()->getTitle()} {$doctor->getDoctor()->getFirstName()} {$doctor->getDoctor()->getLastName()}
                        </td>
                    </tr>
                    <tr>
                        <td class="info_label">Praxisname</td>
                        <td class="info_value">
	                        <ul>
                        {foreach from=$locations item=location}
								<li>{$location->getName()}</li>
						{/foreach}
	                        </ul>
                        </td>
                    </tr>
                    <tr>
                        <td class="info_label">Anschrift</td>
                        <td class="info_value">
						{foreach from=$locations item=location}
							{$location->getStreet()}<br/>
							{$location->getZip()}, {$location->getCity()}<br/><br/>
							{$location->getPhone()}<br/>
								<a href="{$location->getWww()}" target="_blank">{$location->getWww()}</a>
								<hr style="border: solid #808080 1px"/>
						{/foreach}
                        </td>
                    </tr>
                    <tr>
                        <td class="info_label">Philosophie</td>
                        <td id="docinfo1_value" class="info_value">
                            <textarea name="docinfo1">{if isset($form_data.docinfo1)}{$form_data.docinfo1}{else}{$doctor->getDoctor()->getDocinfo1()}{/if}</textarea><div class="edit_icon"></div></li>
                        </td>
                    </tr>
                    <tr>
                        <td class="info_label">Fachrichtung</td>
                        <td id="docinfo5_value" class="info_value">
                            <textarea name="docinfo5">{if isset($form_data.docinfo5)}{$form_data.docinfo5}{else}{$doctor->getDoctor()->getDocinfo5()}{/if}</textarea>
                            <div class="edit_icon"></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="info_label">Leistungsspektrum</td>
                        <td id="docinfo6_value" class="info_value">
                            <textarea name="docinfo6">{if isset($form_data.docinfo6)}{$form_data.docinfo6}{else}{$doctor->getDoctor()->getDocinfo6()}{/if}</textarea>
                            <div class="edit_icon"></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="info_label">Ausbildung</td>
                        <td id="docinfo2_value" class="info_value">
                            <textarea name="docinfo2">{if isset($form_data.docinfo2)}{$form_data.docinfo2}{else}{$doctor->getDoctor()->getDocinfo2()}{/if}</textarea>
                            <div class="edit_icon"></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="info_label">Sprachen</td>
                        <td id="docinfo3_value" class="info_value">
                            <textarea name="docinfo3">{if isset($form_data.docinfo3)}{$form_data.docinfo3}{else}{$doctor->getDoctor()->getDocinfo3()}{/if}</textarea>
                            <div class="edit_icon"></div>
                        </td>
                    </tr>
					<tr>
                        <td class="info_label">Passwort:</td>
                        <td id="password_value" class="info_value">
							Passwort ändern: <input name="change_password" type="checkbox" id="change_password_checkbox" style="top:-2px;" {if isset($form_data.change_password)}checked{/if}><br/>
							Aktuell passwort: <input class="password" type="password" name="current_password" style="margin-left:10px" {if !isset($form_data.change_password)}disabled=""{/if}><br/>
							Neu Passwort:<input class="password" type="password" name="new_password" style="margin-left:29px" {if !isset($form_data.change_password)}disabled=""{/if}>
                            <div class="edit_icon"></div>
							<script>
								$(function() {
									$("#change_password_checkbox").on('change', function(e) {
										e.preventDefault();
										if ($(this).is(":checked")) {
											$(".password").removeAttr('disabled');
										} else {
											$(".password").attr('disabled', 'disabled');
										}
									});	
								});
							</script>
                        </td>
                    </tr>
                </table>
				<input type="submit" class="btn_green submit button large" id="edit-submit" value="Speichern"/>
				<input type="reset" class="new-grey-button reset button large" id="edit-reset" value="Verwerfen"/>
			</form>
		</div>
	</div>
</div>
                                