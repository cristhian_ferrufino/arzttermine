{include "doctors/header.tpl"}

<div id="main" class="middle-responsiv location-profile-edit location profile manage-specialties">
	<div id="header">
		<div class="left profile-title">{$location->getLocation()->getName()}</div>
		<div id="optinal-function">
			<span class="arrow"></span>

			<ul id="item-list" style="display: none;">
				{foreach from=$locations item=location_item}
					<li class="first locationName"><span>{$location_item->getName()}</span></li>
				{/foreach}
				<li><a href="/kontakt" target="_blank">Kontakt</a></li>
				<li class="last">{if isset($ns.userId) and $ns.userId > 0}<a href="//{$SITE_URL}/dyn/{$ns.arzconfig.DOCTORS_LOGIN_URL}/do_logout">abmelden</a>{/if}</li>
			</ul>
		</div>
	</div>
	<div id="doc-manager-edit" class="doc_border">
		<div id="edit-left_side">
			{include "doctors/left_panel_location.tpl"}
		</div>

		<div id="doc-content-main" class="doc_border">
			<form style="width:800px" action="//{$SITE_URL}/{$ns.arzconfig.DOCTORS_LOGIN_URL}/do_praxis_fachrichtung/{$location->getLocation()->getSlug()}" method="post">
				<input type="hidden" name="uid" value="{$location->getLocation()->getId()}" />
				
				<h3>Fachrichtungen und Behandlungsarten</h3>
				{if isset($ns.error)}
					<div class="error">
						{$ns.error.error_message}
					</div>
				{/if}
				<div class="grey_bold">Fachrichtungen Bearbeiten</div>
				<table class="grey_normal treatment_specialties">
					{foreach from=array_chunk($all_specialties, 2) item=rowItem}
						<tr>
							{foreach from=$rowItem item=specialty}
								<td>
									<label>
										<input type="checkbox" name="medical_specialties_ids[]" value="{$specialty->getId()}" {if in_array($specialty->getId(), $locationsMedSpecIdsArray)}checked{/if} />
										{$specialty->getNameSingularDe()}
									</label>
								</td>
							{/foreach}
						</tr>
					{/foreach}
				</table>
				
				<input style="margin:40px auto 20px auto; display:block;" class="button large btn_green" type="submit" value="Speichern" />

			</form>
		</div>
	</div>
</div>