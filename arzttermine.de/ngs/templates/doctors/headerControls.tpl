<!-- NGS Theme Styles -->
<link rel="stylesheet" type="text/css" href="{$STATIC_PATH}/ngs/css/style.css" />
<link rel="stylesheet" type="text/css" href="{$STATIC_PATH}/ngs/css/doctors/style.css" />
<link rel="stylesheet" type="text/css" media="all" href="{$STATIC_PATH}/ngs/fancybox/jquery.fancybox-1.3.4.css" />
{include "main/headerControls.tpl"}

<!--script to fix placeholder in IE-->
<script src="{$STATIC_PATH}/ngs/js/lib/jquery.html5-placeholder-shim.js"></script>
<script src="{$PROTOCOL}code.jquery.com/ui/1.10.1/jquery-ui.js"></script>
<script src="{$STATIC_PATH}/ngs/js/lib/jquery.raty.min.js"></script>
