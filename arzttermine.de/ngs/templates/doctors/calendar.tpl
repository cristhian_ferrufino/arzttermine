{$this->addJsInclude('https://code.jquery.com/ui/1.11.0/jquery-ui.min.js')}
{$this->addJsInclude('/ngs/js/calendar.js')}

{if !isset($ns.no_access_error)}
    <div id="calendar_body">
		{if $ns.fixed_week==0}
			<div>Termine wiederholen bis: <input readonly id="repeat_until" type="text" /></div>
		{/if}
	    <div class="calendar-controls">
		    <input class="tab" type="button" value="Kalender speichern" id="save_calendar"/>
            <form autocomplete="off">
                {if $ns.is_admin == 1}
                    <div>Show Bookings<input type="checkbox" id="show_bookings_checkbox" checked /></div>
                {/if}
                <div>
                    <span>Feste Termine Woche </span>
                    <input id="fixed_week" name="fixed_week" type="checkbox" {if $ns.fixed_week==1}checked="checked"{/if}>
                </div>
            </form>
		</div>

        <div class="week_buttons_container">
            {if $ns.is_admin == 1}
                {assign var=next_href value="?integration_id=`$ns.integration_id`&location_id=`$ns.location_id`&user_id=`$ns.user_id`&monday_date=`$ns.next_monday_date`"}
                {assign var=prev_href value="?integration_id=`$ns.integration_id`&location_id=`$ns.location_id`&user_id=`$ns.user_id`&monday_date=`$ns.previous_monday_date`"}
            {else}
                {assign var=next_href value="?monday_date=`$ns.next_monday_date`"}
                {assign var=prev_href value="?monday_date=`$ns.previous_monday_date`"}
            {/if}
            <a class="previous_week" href="{$prev_href}"></a>
            <a class="next_week" href="{$next_href}"></a>
        </div>
		<div id="calendar" class="clearfix">
			<div class="calendar_column" id="calendar_column_0">
			{assign var=time value=$ns.calendarTimesFrom}
			{assign var=line_height value=60} {* 60 units = 60 minutes *}
				<div class="calendar_weekday"></div>
				{foreach from = $ns.calendarVisibleTimes item=time}
                    {* No line height breaks the backend *}
                    {if $ns.is_admin == 1}
					<div class="calendar_left_times" style="height:{$line_height}px;line-height:{$line_height}px">
						{$time}
					</div>
                    {else}
                    <div class="calendar_left_times">
                        {$time}
                    </div>
                    {/if}
				{/foreach}
			</div>
		  
        {foreach from = $ns.validDates item=date key=column_index}
			<div class="calendar_column" id="calendar_column_{$column_index+1}">
				<div class="calendar_weekday">                   
                    {strftime("%d %b<br/>%A",strtotime($date))}
				</div>
				{foreach from = $ns.calendarVisibleTimes item=time key=row_index}
                    {* No line height breaks the backend *}
                    {if $ns.is_admin == 1}
                    <div class="calendar_time_block" style="height:{$line_height}px;line-height:{$line_height}px"></div>
                    {else}
                    <div class="calendar_time_block"></div>
                    {/if}
                {/foreach}
			</div>
        {/foreach}
		</div>
	</div>

    <script type="text/javascript">
		var site_path = '{$SITE_PATH}',
		    start_date = '{$ns.validDates.0}',
		    next_monday_date = "{$ns.next_monday_date}",
		    previous_monday_date = "{$ns.previous_monday_date}",
		    end_date = '{$ns.validDates[count($ns.validDates)-1]}',
		    unit_block_height = '{$ns.unitDivHeight}',
		    appointments_json = {if $ns.appointmentsJson}{$ns.appointmentsJson}{else} { } {/if},
		    calendar_start_time = '{$ns.calendarVisibleTimes.0}',
		    calendar_end_time = '{$ns.calendarEndTime}',
		    user_id = {$ns.user_id},
		    location_ids = {if $ns.location_ids}{$ns.location_ids}{else}[]{/if},
		    default_increment = {$ns.defaultIncrement},
		    doctorAllTreatmentTypeIds = '{$ns.doctorAllTreatmentTypeIds}',
		    doctorAllTreatmentTypeNames = '{$ns.doctorAllTreatmentTypeNames}',
		    bookings_json = {if $ns.bookingsJson}{$ns.bookingsJson}{else} { } {/if},
		    is_admin = '{$ns.is_admin}';
	</script>

    {if $ns.is_admin}
        <script type="text/javascript" src='{$STATIC_PATH}/ngs/js/calendar.js'></script>
    {/if}
{else}
    {if $ns.is_admin}
        <div class="error">Dieser Arzt ist für keine Praxis registriert.</div>
    {else}
        <div class="error">Sie sind derzeit mit keiner Praxis verbunden, bitte kontaktieren Sie den Support.</div>
    {/if}
{/if}


