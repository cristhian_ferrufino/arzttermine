{include "doctors/header.tpl"}
<script>
    $(document).ready(function() {
        $('.toggle_height')
                .click(function(){
                    if(!$(this).hasClass('close_toggle')){
                        $(this).addClass('close_toggle');
                        $(this).parent().css('height','auto');
                    }
                    else{
                        $(this).removeClass('close_toggle');
                        $(this).parent().css('height','16px');
                    }
                }
        );
    });
</script>
<div id="main" class="appointment_history middle-responsiv profile-edit profile">
    <div id="header">
        <div class="left profile-title">Termin-Rückblick</div>
    {include "doctors/log_out.tpl"}
    </div>
    <div id="doc-manager-edit" class="doc_border">
        <div id="edit-left_side">
        {include "doctors/left_panel.tpl"}
        </div>
    {if isset($no_access_error)}
        <div class="error">{$no_access_error}</div>
        {else}
        <form action="{$SITE_PATH}/{$ns.arzconfig.DOCTORS_LOGIN_URL}/termin_rueckblick" method="post">
            <div id="doc-content-main" class="doc_border">
                <div class="filter_date_container">
                    Von:
                    <input id="date_from" type="text" name="date_from" style="width: 100px" value="{$dateFrom}"/>
                    Bis:
                    <input id="date_to" type="text" name="date_to" style="width: 100px" value="{$dateTo}"/>
                    <input type="submit" value="Termine zeigen">
                    <div style="float: right;padding: 4px ">
                        <span class="grey_normal">Sortieren nach:</span>
                        <select name="order_by">
                            <option value="asc" {if $orderBy=="ASC"}selected="selected"{/if}>aufsteigend</option>
                            <option value="desc" {if $orderBy=="DESC"}selected="selected"{/if}>absteigend</option>
                        </select>
                    </div>
                </div>
                {foreach from=$validDates item=date}
                    <div class="appointments_day_container" >
                        {if !empty($mergedArray.$date)}
                            <div class="toggle_height"></div>
                        {/if}
                        <div class="appointments_daytime">
                            {strftime(" %A %d %b %Y",strtotime($date))}
                        </div>
                        {if !empty($mergedArray.$date)}
                            <div class="apointments_counter">
                                (<span class="available_counter">{count($appointments.$date)}</span> Verfügbar
                                <span class="available_counter">{count($doctorsPastBookings.$date)}</span> Gebucht
                                )
                            </div>
                        {/if}
                        <ul>
                            {foreach from=$mergedArray.$date item = bookapp}
                                <li>
                                    {if $bookapp instanceof "BookingsDto"}
                                        {assign var=booking value=$bookapp}
                                        <span class="apointment_daytime">{date("H:i", strtotime($booking->getAppointmentStartAt()))}</span><span class="appointment_detail"> - {$booking->getTreatmentType()->getNameDe()}, {$booking->getFirstName()} {$booking->getLastName()},
                                Patienten Telefonnummer: {$booking->getPhone()}
                            </span>
                                        {else}
                                        {assign var=appointment value=$bookapp}
                                        {assign var=insurances value=$appointment->getInsurances()}
                                        {if strtotime($date)<time()}
                                            <span class="apointment_daytime">{$appointment->getTimeText()}</span>
                            <span class="appointment_detail"> - Verfügbar(
                                {foreach from=$insurances item=insuranceId name=insurances}
                                    {$ns.arzconfig.INSURANCES.$insuranceId}{if not $smarty.foreach.insurances.last}, {/if}
                                {/foreach}
                                )</span>
                                            {else}
                                            <span class="appointment_detail"><a target="_blank" href="{$ns.arzconfig.URL_HTTPS_LIVE}/termin?u={$user->getId()}&l={$location->getId()}&m={$medicalSpecialtyId}&i={$insuranceId}&s={$appointment->getDateTimeUrlAttr()}">{$appointment->getTimeText()}</a> - Verfügbar</span>
                                        {/if}
                                    {/if}
                                </li>
                            {/foreach}
                        </ul>
                    </div>
                {/foreach}
            </div>
        </form>
    {/if}
    </div>
</div>
