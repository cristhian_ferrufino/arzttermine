{assign var="template_body_class" value="two-cols-with-form"}
{assign var="additional_body_classes" value="doctor-registration"}
{assign var="use_minimal_footer" value=true}
{assign var="isSales" value=true}
{assign var="hasFullHeader" value="false"}
{include 'header.tpl'}

  <div class="container">
    <main>
      <div class="already-registered">Sind Sie bereits registriert? <a href="/aerzte/login">Hier einloggen.</a></div>
      <h1>Registrierung</h1>

      <section id="form">
        <form id="booking-form" class="{if isset($error_class)}{$error_class}{/if}" action="{$SITE_PATH}/{$ns.arzconfig.DOCTORS_LOGIN_URL}/do_registrieren_aerzte" method="post" enctype="application/x-www-form-urlencoded" >
            <div class="form-element {if isset($error_class)}{$error_class}{/if}" data-error-message="Bitte wählen Sie Ihre Anrede aus.">
              <div id="gender" class="radio-group">
                <h4>Anrede</h4>
                <input name="gender"  type="radio" id="gender-1" value="1" {if isset($smarty.request['gender']) && $smarty.request['gender'] === "1"}checked{/if}><label for="gender-1">Frau</label>
                <input name="gender"  type="radio" id="gender-2" value="0" {if isset($smarty.request['gender']) && $smarty.request['gender'] === "0"}checked{/if}><label for="gender-2">Herr</label>
              </div>
            </div>
            <div class="form-element {if isset($error_class)}{$error_class}{/if}" data-error-message="Bitte geben Sie Ihren Vornamen ein.">
              <label>Titel</label>
              <input type="text" name="full_title" value="{if isset($smarty.request['full_title'])}{$smarty.request['full_title']}{/if}">
            </div>
            <div class="form-element {if isset($error_class)}{$error_class}{/if}" data-error-message="Bitte geben Sie Ihren Vornamen ein.">
              <label>Vorname</label>
              <input type="text" name="first_name" value="{if isset($smarty.request['first_name'])}{$smarty.request['first_name']}{/if}">
            </div>
            <div class="form-element {if isset($error_class)}{$error_class}{/if}" data-error-message="Bitte geben Sie Ihren Nachnamen ein.">
              <label>Nachname</label>
              <input type="text" name="last_name" value="{if isset($smarty.request['last_name'])}{$smarty.request['last_name']}{/if}">
            </div>
            <div class="form-element {if isset($error_class)}{$error_class}{/if}" data-error-message="Bitte geben Sie Ihre E-Mail Adresse ein.">
              <label>E-Mail</label>
              <input type="email" name="email" value="{if isset($smarty.request['email'])}{$smarty.request['email']}{/if}">
            </div>
            <div class="form-element {if isset($error_class)}{$error_class}{/if}" data-error-message="Bitte geben Sie eine gültige Telefonnummer ein.">
              <label>Telefon</label>
              <input type="tel" name="phone" value="{if isset($smarty.request['phone'])}{$smarty.request['phone']}{/if}">
            </div>
            <div class="button-wrapper"><button type="submit">Registrieren</button></div>
        </form>
      </section>

      <section id="info-block">
        <div class="orange-bar">
          <div class="datetime"><span class="light">Erstellen Sie Ihr</span> kostenfreies Profil!</div>
        </div>
        <div class="infobox advantages">
          <h4><em>Gute Gründe</em> für Ihre <em>risikofreie Registrierung:</em></h4>
          <ul>
            <li>Darstellung Ihrer Praxis im exklusiven Umfeld.</li>
            <li>Generierung gezielter Anfragen entsprechend Ihrer Spezialisierungen.</li>
            <li>Effizienzsteigerung durch qualifizierte Patientenanfragen.</li>
            <li>Bedarfsgerechte Neupatientenakquise.</li>
            <li>Moderne und patientenfreundliche Außenwirkung.</li>
          </ul>
        </div>
        <section class="infobox infobox-grey-light cta-phone">
          <h4><em>Haben Sie noch Fragen?</em></h4>
          <p>Wir sind 24 Stunden kostenfrei für Sie erreichbar:</p>
          {include file="phone-numbers/phone-sales.tpl"}
        </section>
      </section>

  </main>
</div>

{include 'footer.tpl'}
