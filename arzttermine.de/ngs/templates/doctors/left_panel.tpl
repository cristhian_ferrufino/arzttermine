
{* Cache this value for readability *}
{assign var=login_url value="{$SITE_PATH}/{$ns.arzconfig.DOCTORS_LOGIN_URL}"}
{assign var=profile_pic value=$doctor->getProfile()}
{assign var=locations value=$locations}

<div id="doctor_dashboard_left_panel" class="dashboard_left_panel">
	<a id="link_avatar" href="{$login_url}/profilbild">
		
		<img id="left_panel_image" class="profile_image" src="{$SITE_PATH}{if $profile_pic}{$profile_pic}{else}/ngs/img/default_avatar.jpg"{/if}" width="140" alt="Dein Profilbild" />
        <span id="avatar_tooltip" style="display:none">Lade deine Profilbilder</span>
	</a>
	<ul id="edit_menu">
		<li class="first btn-profile_view"><a href="{$login_url}/account/{$doctor->getDoctor()->getSlug()}" class="button medium new-grey-button profile_view ">Profil anschauen</a></li>
		<li class="btn-profile_edit"><a href="{$login_url}/account_bearbeitung/{$doctor->getDoctor()->getSlug()}" class="button medium new-grey-button profile_edit ">Profil bearbeiten</a></li>

	{if $locations && $locations[0]->getIntegrationId() == 1}
		<li class="last"><a href="{$login_url}/termine" class="button medium new-grey-button profile_edit ">Termine verwalten</a></li>
	{/if}

		<li class="last btn-fachrichtung_verwalten"><a href="{$login_url}/fachrichtung_verwalten/{$doctor->getDoctor()->getSlug()}" class="button medium new-grey-button profile_edit">Fachrichtung &amp; Behandlungsgrund</a></li>

	{if $locations}
		<li class="last btn-praxis_edit"><a href="{$login_url}/praxis" class="button medium new-grey-button profile_edit">Praxisprofil</a></li>
	{/if}
	</ul>
</div>
{* Handles the hover for the avatar *}
{if !$profile_pic}
<script>
	$(function() {
		var $avatar_tooltip = $('#avatar_tooltip');

		$('#link_avatar').hover(
			function() { $avatar_tooltip.show(); },
			function() { $avatar_tooltip.hide(); }
		);
	});
</script>
{/if}
