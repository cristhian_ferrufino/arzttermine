
{assign var="template_body_class" value="profile-page"}
{assign var="additional_body_classes" value="doctor doctor-account"}
{include 'header.tpl'}

{* Cache this value for readability *}
{assign var=login_url value="{$SITE_PATH}/{$ns.arzconfig.DOCTORS_LOGIN_URL}"}
{assign var=profile_pic value=$doctor->getProfile()}
{assign var=locations value=$locations}

{$this->addJsInclude($this->getStaticUrl('js/doctor-profile.js'))}

<div class="container">
    <main>
        <section class="doctor-summary" data-doctor-slug="{$doctor->getDoctor()->getSlug()}">
            <div class="headline justified-blocks">
                <h1>{$doctor->getDoctor()->getTitle()} {$doctor->getDoctor()->getFirstName()} {$doctor->getDoctor()->getLastName()}</h1>
                {if isset($ns.userId) and  $ns.userId > 0}
                <a class="logout" href="//{$SITE_URL}/dyn/{$ns.arzconfig.DOCTORS_LOGIN_URL}/do_logout">abmelden</a>
                {/if}
            </div>
            <nav>
                <ul>
                    <li class="active"><a href="{$login_url}/account/{$doctor->getDoctor()->getSlug()}">Mein Arztprofil</a></li>
                    {if $locations && $locations[0]->getIntegrationId() == 1}
                    <li><a href="{$login_url}/termine">Termine verwalten</a></li>
                    {/if}
                    {if $locations}
                    <li><a href="{$login_url}/praxis">Praxisprofil</a></li>
                    {/if}
                </ul>
            </nav>
        </section>

            
        <section>
            <form action="{$SITE_PATH}/{$ns.arzconfig.DOCTORS_LOGIN_URL}/do_speichern_aerzte_profil?arzt={$doctor->getDoctor()->getSlug()}" method="post">
                <div class="profile-photo">
                    <img src="{$SITE_PATH}{if $profile_pic}{$profile_pic}{else}/ngs/img/default_avatar.jpg{/if}" alt="Profilbild">
                    <a href="{$login_url}/profilbild">Profilbilder bearbeiten</a>
                </div>
    
                <div class="text-data name">
                    <h3>Name</h3>
                    <div>
                        <input type="text" name="title" value="{$doctor->getDoctor()->getTitle()}" placeholder="Anrede" />
                        <input type="text" name="first_name" value="{$doctor->getDoctor()->getFirstName()}" placeholder="Vorname" />
                        <input type="text" name="last_name" value="{$doctor->getDoctor()->getLastName()}" placeholder="Nachname" />
                    </div>
                </div>
    
                <div class="text-data praxen">
                    <h3>Praxen</h3>
                    <div>
                    {if $locations}
                        <ul>
                        {foreach from=$locations item=location}
                            <li>
                            <p class="praxis-name">{$location->getName()}</p>
                            <p>{$location->getStreet()}</p>
                            <p>{$location->getZip()}, {$location->getCity()}</p>
                            <p>{$location->getPhone()}</p>
                            <p><a href="{$location->getWww()}" target="_blank">{$location->getWww()}</a></p>
                            </li>
                        {/foreach}
                        </ul>
                    {else}
                        <p>Your account needs to be registered to a practice before it can be used. Please call our Account Management or email to have the issue resolved: 0800 2222 133</p>
                    {/if}
                    </div>
                </div>
    
                <div class="text-data">
                    <h3>Aerzte der Praxen</h3>
                    <div>
                        <select id="doctor-select">
                            {foreach $doctors as $doctor_item}
                                <option value="{$doctor_item->getSlug()}" {if $doctor->getDoctor()->getId() == $doctor_item->getId()}selected{/if}>
                                    {$doctor_item->getTitle()} {$doctor_item->getFirstName()} {$doctor_item->getLastName()}
                                </option>
                            {/foreach}
                        </select>
                    </div>
                </div>
    
                <div class="text-data">
                    <h3>Philosophie</h3>
                    <div class="editable-container">
                        <textarea name="docinfo1">{$doctor->getDoctor()->getDocinfo1()}</textarea>
                    </div>
                </div>
                
                <div class="text-data">
                    <h3>Fachrichtung</h3>
                    <div class="editable-container specialties">
                        {foreach from=array_chunk($all_specialties, 2) item=rowItem}
                        {foreach from=$rowItem item=specialty}
                        {if in_array($specialty->getId(), $specialties)}
                        <label>
                            <input type="checkbox" name="medical_specialties_ids[]" value="{$specialty->getId()}" checked>
                            {assign var="specialty_selected" value="true"}
                            {$specialty->getNameSingularDe()}
                        </label>
                        {/if}
                        {/foreach}
                        {/foreach}
    
                        {if isset($specialty_selected)}
                        <a role="button" class="toggle-specialties">Show <span class="show-more">more</span><span class="show-less">less</span></a>
                        {/if}
    
                        <div class="unselected-specialties {if !isset($specialty_selected)}visible{/if}">
                            {foreach from=array_chunk($all_specialties, 2) item=rowItem}
                            {foreach from=$rowItem item=specialty}
                            {if !in_array($specialty->getId(), $specialties)}
                            <label>
                                <input type="checkbox" name="medical_specialties_ids[]" value="{$specialty->getId()}">
                                {$specialty->getNameSingularDe()}
                            </label>
                            {/if}
                            {/foreach}
                            {/foreach}
                        </div>
                    </div>
                </div>
                
                <div class="text-data">
                    <h3>Leistungsspektrum</h3>
                    <div class="editable-container" id="treatments">
                        
                    </div>
                </div>
                
                <div class="text-data">
                    <h3>Ausbildung</h3>
                    <div class="editable-container">
                        <textarea name="docinfo2">{$doctor->getDoctor()->getDocinfo2()}</textarea>
                    </div>
                </div>
                
                <div class="text-data">
                    <h3>Sprachen</h3>
                    <div class="editable-container">
                        <textarea name="docinfo3">{$doctor->getDoctor()->getDocinfo3()}</textarea>
                    </div>
                </div>
    
                <div class="text-data">
                    <h3>Feste Termine Woche</h3>
                    <div>{if $doctor->getDoctor()->getFixedWeek()==1}Ja{else}Nein{/if}</div>
                </div>
                    
                <div class="text-data">
                    <h3>Passwort</h3>
                    <div class="change-password">
                        <input class="password" type="password" name="current_password" placeholder="Aktuelles Passwort">
                        <input class="password" type="password" name="new_password" placeholder="Neues Passwort">
                    </div>
                </div>
    
                <div class="buttons-container">
                    <button class="save">Save</button>
                    <button class="reset">Reset</button>
                </div>
    
                <input type="hidden" name="uid" value="{$doctor->getDoctor()->getId()}" />
            </form>
        </section>
        
    </main>
</div>

<script>
    var doctorsMedSpecIdsArray = {json_encode($specialties)} || [];
    var docAccountApiUrl = "//{$SITE_URL}/dyn/{$ns.arzconfig.DOCTORS_LOGIN_URL}/";
    var docAccountUrl = '{$ns.arzconfig.DOCTORS_LOGIN_URL}';
</script>

{include 'footer.tpl'}

