{* Cache this value for readability *}
{assign var=login_url value="{$SITE_PATH}/{$ns.arzconfig.DOCTORS_LOGIN_URL}"}
{assign var=profile_pic value=$location->getProfile()}
{assign var=slug value=$location->getLocation()->getSlug()}

<div id="doctor_dashboard_left_panel" class="dashboard_left_panel">
	<img id="left_panel_image" class="profile_image" src="{$SITE_PATH}{if $profile_pic}{$profile_pic}{else}/ngs/img/default_avatar.jpg"{/if}" width="140" alt="Praxis Profilbild" />
	<ul id="edit_menu">
		<li class="first btn-praxis_edit"><a href="{$login_url}/praxis_bearbeitung/{$slug}" class="button medium new-grey-button profile_edit ">Profil bearbeiten</a></li>
		<li class="first btn-praxis_location-view"><a href="{$login_url}/praxis/{$slug}" class="button medium new-grey-button profile_view">Profil anschauen</a></li>
		<li class="last btn-fachrichtung_verwalten"><a href="{$login_url}/praxis_fachrichtung/{$slug}" class="button medium new-grey-button profile_edit">Fachrichtung &amp; Behandlungsarten</a></li>
		<li class="last btn-fachrichtung_verwalten"><a href="{$login_url}/praxis_aerzte/{$slug}" class="button medium new-grey-button view_doctors">Alle Ärzte der Praxis</a></li>
		<li class="last btn-arzt_edit"><a href="{$login_url}/account" class="button medium new-grey-button profile_edit">Mein Arztprofil</a></li>
	</ul>
</div>