{* Set the current location *}
{assign var=location_id value=$location->getLocation()->getId()}

{include "doctors/header.tpl"}
<div id="main" class="middle-responsiv location-profile-edit profile-edit location doctor profile">
	<div id="header">
		<div class="left profile-title">{$location->getLocation()->getName()}</div>
		{assign var=locations value=$locations}
		<div id="optinal-function">
			<span class="arrow"></span>

			<ul id="item-list" style="display: none;">
				{foreach from=$locations item=location_item}
					<li class="first locationName"><span>{$location_item->getName()}</span></li>
				{/foreach}
				<li><a href="/kontakt" target="_blank">Kontakt</a></li>
				<li class="last">{if isset($ns.userId) and $ns.userId > 0}<a href="//{$SITE_URL}/dyn/{$ns.arzconfig.DOCTORS_LOGIN_URL}/do_logout">abmelden</a>{/if}</li>
			</ul>
		</div>
	</div>
	<div id="doc-manager-edit" class="doc_border">
		<div id="edit-left_side">
			{include "doctors/left_panel_location.tpl"}
		</div>

		<div id="doc-content-main" class="doc_border">
			<h3>Praxisprofil bearbeiten</h3>
			{if isset($ns.error)}<div class="error">{$ns.error.error_message}</div>{/if}
			
			<form id="form-profile-edit" action="{$SITE_PATH}/{$ns.arzconfig.DOCTORS_LOGIN_URL}/do_speichern_praxis/{$location->getLocation()->getSlug()}" method="post">
				<input type="hidden" name="uid" value="{$location_id}" />
				<table class="info_table" border="0">
					<tr>
						<td class="info_label">Praxisname</td>
						<td class="info_value">
							<span>{$location->getLocation()->getName()}</span>
						</td>
					</tr>

					<tr>
						<td class="info_label">Anschrift</td>
						<td class="info_value">
							<span>{$location->getLocation()->getStreet()}</span>
						</td>
					</tr>

					<tr>
						<td class="info_label">PLZ</td>
						<td class="info_value">
							<span>{$location->getLocation()->getZip()}</span>
						</td>
					</tr>

					<tr>
						<td class="info_label">Ort</td>
						<td class="info_value">
							<span>{$location->getLocation()->getCity()}</span>
						</td>
					</tr>

					<tr>
						<td class="info_label">Telefon</td>
						<td class="info_value">
							<input type="text" name="phone" class="text" value="{$location->getLocation()->getPhone()}" />
							<div class="edit_icon"></div>
						</td>
					</tr>

					<tr>
						<td class="info_label">Website</td>
						<td class="info_value">
							<input type="text" name="www" class="text" value="{$location->getLocation()->getWww()}" />
							<div class="edit_icon"></div>
						</td>
					</tr>

					<tr>
						<td class="info_label">Fachrichtung</td>
						<td class="info_value specialties">
							<ul>
							{foreach $specialties as $specialty}
								<li>{$specialty->getNameSingularDe()}</li>
							{/foreach}
							</ul>
							{* @todo: Check translation *}
							<a href="{$SITE_PATH}/{$ns.arzconfig.DOCTORS_LOGIN_URL}/praxis_fachrichtung/{$location->getLocation()->getSlug()}">Fachrichtungen bearbeiten</a>
						</td>
					</tr>
					
				</table>
				<input type="submit" class="btn_green submit button large" id="edit-submit" value="Speichern"/>
				<input type="reset" class="new-grey-button reset button large" id="edit-reset" value="Verwerfen"/>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		$('#praxen').change(function() {
			window.location = '{$SITE_PATH}/{$ns.arzconfig.DOCTORS_LOGIN_URL}/praxis_bearbeitung/' + this.options[this.selectedIndex].value;
		});
	});
</script>