{include "doctors/header.tpl"}
<script>
	function _sendRequest(_url, postData, callBack) {
		if (_url && postData) {
			callBack = callBack || $.noop;

			$.ajax({
				url : _url,
				type : 'GET',
				data : postData,
				context : document.body
			})
			.done(function(data) {
				callBack(data);
			});
		}
	}
	
	function showTreatmentTypesDiv(data){
		treatmentTypesDiv=document.getElementById("treatment_types");
		treatmentTypesDiv.innerHTML+=data;
	}

	function removeTreatmentTypesDiv(id) {
		treatmentTypeDiv = document.getElementById("treatment_type_" + id);
		treatmentTypeDiv.parentNode.removeChild(treatmentTypeDiv);
	}

	var doctorsMedSpecIdsArray = {json_encode($specialties)} || [];

	for (var i=0;i < doctorsMedSpecIdsArray.length; i++) {
		_sendRequest("//{$SITE_URL}/dyn/{$ns.arzconfig.DOCTORS_LOGIN_URL}/treatment_types_div?med_spec_id=" + doctorsMedSpecIdsArray[i], doctorsMedSpecIdsArray[i], showTreatmentTypesDiv);
	}

	$(function() {
		$('.treatment_specialties').find('input').on('change', (function() {
			if (this.checked) {
				_sendRequest(
						"//{$SITE_URL}/dyn/{$ns.arzconfig.DOCTORS_LOGIN_URL}/treatment_types_div?med_spec_id=" + this.value,
						this.value,
						showTreatmentTypesDiv
				);
			} else {
				removeTreatmentTypesDiv(this.value);
			}
		}));
	});
</script>
<div id="main" class="middle-responsiv profile-edit profile manage-specialties">
	<div id="header">
		{assign var=locations value=$ns.locations}
        <div class="left profile-title">{$doctor->getDoctor()->getTitle()} {$doctor->getDoctor()->getFirstName()} {$doctor->getDoctor()->getLastName()}</div>
        <div id="optinal-function">
            <span class="arrow"></span>

            <ul id="item-list" style="display: none;">
				{foreach from=$locations item=location}
                    <li class="first locationName"><span>{$location->getName()}</span></li>
				{/foreach}
                <li><a href="/kontakt" target="_blank">Kontakt</a></li>
                <li class="last">{if isset($ns.userId) and $ns.userId > 0}<a href="//{$SITE_URL}/dyn/{$ns.arzconfig.DOCTORS_LOGIN_URL}/do_logout">abmelden</a>{/if}</li>
            </ul>
        </div>
	</div>
	<div id="doc-manager-edit" class="doc_border">
		<div id="edit-left_side">
			{include "doctors/left_panel.tpl"}
		</div>
		
		<div id="doc-content-main" class="doc_border">
			<form style="width:800px" action="//{$SITE_URL}/{$ns.arzconfig.DOCTORS_LOGIN_URL}/do_fachrichtung_verwalten/{$doctor->getDoctor()->getSlug()}" method="post">
				<input type="hidden" name="uid" value="{$doctor->getDoctor()->getId()}" />
				<h3>Fachrichtungen und Behandlungsarten</h3>
                {if isset($ns.error)}
                    <div class="error">
                        {$ns.error.error_message}
                    </div>
                {/if}
				<div class="grey_bold">Fachrichtungen Bearbeiten</div>
				<table class="grey_normal treatment_specialties">
					{foreach from=array_chunk($all_specialties, 2) item=rowItem}
						<tr>
							{foreach from=$rowItem item=specialty}
							<td>
								<label>
									<input type="checkbox" name="medical_specialties_ids[]" value="{$specialty->getId()}" {if in_array($specialty->getId(), $specialties)}checked{/if} />
									{$specialty->getNameSingularDe()}
								</label>
							</td>
							{/foreach}
						</tr>
					{/foreach}
				</table>
                
				<div style="margin-top:50px;" class="grey_bold" >Fachrichtungen Bearbeiten</div>
                
				<div id="treatment_types"></div>
                <input style="margin:0 auto; display:block;" class="button large btn_green" type="submit" value="Speichern" /><br />

            </form>
		</div>
	</div>
</div>