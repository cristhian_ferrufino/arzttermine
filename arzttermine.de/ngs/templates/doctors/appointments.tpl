
{assign var="template_body_class" value="profile-page"}
{assign var="additional_body_classes" value="doctor doctor-account"}
{include 'header.tpl'}

{* Cache this value for readability *}
{assign var=login_url value="{$SITE_PATH}/{$ns.arzconfig.DOCTORS_LOGIN_URL}"}
{assign var=profile_pic value=$doctor->getProfile()}
{assign var=locations value=$locations}

{$this->addJsInclude($this->getStaticUrl('js/doctor-profile.js'))}

<link rel="stylesheet" type="text/css" href="/static/css/doc-calendar.min.css?cv={$cacheVersion}">

<div class="container">
    <main>
        <section class="doctor-summary" data-doctor-slug="{$doctor->getDoctor()->getSlug()}">
            <div class="headline justified-blocks">
                <h1>{$doctor->getDoctor()->getTitle()} {$doctor->getDoctor()->getFirstName()} {$doctor->getDoctor()->getLastName()}</h1>
                {if isset($ns.userId) and  $ns.userId > 0}
                <a class="logout" href="//{$SITE_URL}/dyn/{$ns.arzconfig.DOCTORS_LOGIN_URL}/do_logout">abmelden</a>
                {/if}
            </div>
            <nav>
                <ul>
                    <li><a href="{$login_url}/account/{$doctor->getDoctor()->getSlug()}">Profil anschauen</a></li>
                    {*}<li><a href="{$login_url}/account_bearbeitung/{$doctor->getDoctor()->getSlug()}">Profil bearbeiten</a></li>{/*}
                    {if $locations && $locations[0]->getIntegrationId() == 1}
                    <li class="active"><a href="{$login_url}/termine">Termine verwalten</a></li>
                    {/if}
                    {if $locations}
                    <li><a href="{$login_url}/praxis">Praxisprofil</a></li>
                    {/if}
                </ul>
            </nav>
        </section>

        <h2>Anstehende Termine</h2>
        {nest ns=calendar}

    </main>
</div>


{include 'footer.tpl'}

