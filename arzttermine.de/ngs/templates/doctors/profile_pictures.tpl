{assign var="template_body_class" value="profile-page"}
{assign var="additional_body_classes" value="doctor doctor-account photo-upload"}
{include 'header.tpl'}

{* Cache this value for readability *}
{assign var=login_url value="{$SITE_PATH}/{$ns.arzconfig.DOCTORS_LOGIN_URL}"}
{assign var=profile_pic value=$doctor->getProfile()}
{assign var=locations value=$locations}

{$this->addJsInclude($this->getStaticUrl('js/doctor-profile.js'))}

<div class="container">
    <main>
        <section class="doctor-summary" data-doctor-slug="{$doctor->getDoctor()->getSlug()}">
            <div class="headline justified-blocks">
                <h1>{$doctor->getDoctor()->getTitle()} {$doctor->getDoctor()->getFirstName()} {$doctor->getDoctor()->getLastName()}</h1>
                {if isset($ns.userId) and  $ns.userId > 0}
                <a class="logout" href="//{$SITE_URL}/dyn/{$ns.arzconfig.DOCTORS_LOGIN_URL}/do_logout">abmelden</a>
                {/if}
            </div>
            <nav>
                <ul>
                    <li class="active"><a href="{$login_url}/account/{$doctor->getDoctor()->getSlug()}">Mein Arztprofil</a></li>
                    {if $locations && $locations[0]->getIntegrationId() == 1}
                    <li><a href="{$login_url}/termine">Termine verwalten</a></li>
                    {/if}
                    {if $locations}
                    <li><a href="{$login_url}/praxis">Praxisprofil</a></li>
                    {/if}
                </ul>
            </nav>
        </section>

		
				<section class="profile-images">
				{foreach from=$userAssets key=assetId item=profileImageUrl}
				{if $profileImageUrl}
						<div class="profile-image-wrapper">
								<img class="profile-image" src="{$SITE_PATH}{$profileImageUrl}" />
								<div class="actions" data-aid="{$assetId}">
										<a href="{$SITE_PATH}/dyn/{$ns.arzconfig.DOCTORS_LOGIN_URL}/do_delete_profile_pic/{$doctor->getDoctor()->getSlug()}">
		                    delete
										</a>
		                <a href="{$SITE_PATH}/dyn/{$ns.arzconfig.DOCTORS_LOGIN_URL}/do_make_profile_pic/{$doctor->getDoctor()->getSlug()}">
		                    select
										</a>
								</div>
						</div>
				{/if}
				{/foreach}
						<a id="upload_pic_button" role="button">Neues Foto hochladen</a>
						<div id="upload_pic">
							<form id="form-profile-edit" action="{$SITE_PATH}/{$ns.arzconfig.DOCTORS_LOGIN_URL}/do_laden_pic?arzt={$doctor->getDoctor()->getSlug()}" method="post" enctype="multipart/form-data">
									<input name="uid" id="uid" type="hidden" value="{$doctor->getDoctor()->getId()}" />
									<input class="button" type="file" name="upload">
									<div class="buttons-container">
			                <button type="submit" id="edit-submit" class="save">Speichern</button>
			                <button type="reset" id="edit-reset" class="reset">Verwerfen</button>
			            </div>
							</form>
						</div>
				</section>



    </main>
</div>

<script type="text/javascript">
	var uid = {$doctor->getDoctor()->getId()};
</script>
{include 'footer.tpl'}

