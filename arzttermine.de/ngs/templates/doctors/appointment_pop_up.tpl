<form class="popUpForm">
    <ul>
        <li><a href="#general_info_tab">Allgemein</a></li>
        <li><a href="#insurance_types_tab">Versicherungsart</a></li>
			{if $ns.is_admin == 1}
        <li><a href="#treatment_types_tab">Behandlungsgrunde</a></li>
			{/if}		
		
			{if count($ns.location_ids) > 1}
        <li><a href="#locations_tab">Praxen</a></li>
			{/if}
    </ul>
    <table width="100%" style="text-align: left" id="general_info_tab">
        <tr>
            <td>
                <div class="termin-date">
                Termin-Datum:
                </div>
            </td>
            <td class="termin-date">
                <div class="termin-date">
                <span>{strftime("%d %b  / %A",strtotime($ns.date))} </span>
                <input type="hidden" name="date" value="{$ns.date}">
                </div>
            </td>
        </tr>
        <tr class="input_rows">
            <td class="input_label">
                Startzeit:
            </td>
            <td>
                <input type="text" name="start_at" value="{$ns.start_at}" class="calendar_time_input">
            </td>
        </tr>
        <tr class="input_rows">
            <td class="input_label">
                Endzeit:
            </td>
            <td>
                <input type="text" name="end_at" value="{$ns.end_at}" class="calendar_time_input">
            </td>
        </tr>
        <tr class="input_rows">
            <td class="input_label">
                Blöcke in Min:
            </td>
            <td>
                {html_options name="block_in_minutes" options=$ns.appointmentDurationOptions selected=$ns.block_in_minutes}
            </td>
        </tr>
        <tr class="input_rows">
            <td class="input_label" valign="top">
                Wochentage:
            </td>
            <td>
                {foreach from=$ns.weekdays item=weekday key=index}
                    <div style="float:left;">
                        <input style="top: -4px; left:-24px;" name="weekday" type="checkbox" {if $weekday==$ns.selectedWeekday}checked="checked"{/if} value="{$index+1}"/><br/>
                        <span class="weekday">{$weekday}</span>
                    </div>
                {/foreach}
            </td>
        </tr>
    </table>
    <div id="insurance_types_tab">
       
                {foreach from=$ns.allInsuranceTypes key=index item=insuranceType}
                    <div>
                        <input style="left:0px;" type="checkbox" value="{$insuranceType}" name="insurance_types" {if in_array($insuranceType,$ns.selectedInsuranceTypesArray)}checked="checked"{/if}>
                        {$ns.allInsuranceNames.$index}
                    </div>
                {/foreach}
          
    </div>
	{if $ns.is_admin == 1}		
		<div id="treatment_types_tab">        
			
					{foreach from=$ns.ttIdsArray key=index item=ttId}
						<div>
							<input style="left:0px;" type="checkbox" value="{$ttId}" name="treatment_type_id" {if in_array($ttId,$ns.selectedTtIdsArray)}checked="checked"{/if}>
							{$ns.ttNamesArray.$index}
						</div>
					{/foreach}
				
		</div> 
	{/if}
	{if count($ns.location_ids) > 1}
		<div id="locations_tab">        
					<select name="location_id" style="max-width:200px">
						{html_options values=$ns.location_ids selected=$ns.selected_location_id output=$ns.location_names}
					</select>
		</div> 
	{else}
		<input name="location_id" type="hidden" value="{$ns.selected_location_id}"/>
	{/if}
</form>

