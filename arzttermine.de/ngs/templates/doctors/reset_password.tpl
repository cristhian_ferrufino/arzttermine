{assign var="template_body_class" value="two-cols-with-form"}
{assign var="additional_body_classes" value="doctor-registration"}
{assign var="use_minimal_footer" value=true}
{assign var="isSales" value=true}
{include 'header.tpl'}
  <div class="container">
    <main>
        <h1>Passwort zurücksetzen</h1>

        {*}
        {if isset($ns.error_message)}
                <div class="error login_error_message">
                    {$ns.error_message}
                </div>
        {/if}
        {if isset($ns.message)}
                <div class="login_message">
                    {$ns.message}
                </div>
        {/if}
        {/*}
        <section id="form">
        	<form action="//{$SITE_URL}/{$ns.arzconfig.DOCTORS_LOGIN_URL}/do_passwort_zuruecksetzen" method="post">	   
                <div class="form-element">
                    <label>Passwort<label>
                    {include "register-password.tpl"}
                </div>
                <div class="form-element">
                    <label>Passwort bestätigen</label>
                    <input id="repeat_password" type="password" name="repeat_password" required/>
                </div>
                <input id="reset_password_code" type="hidden" name="reset_password_code" value="{$ns.password_reset_code}"/>
                <div class="button-wrapper"><button type="submit">Speichern</button></div>
            </form>
        </section>

        <section id="info-block"  class="infobox infobox-grey-light cta-phone">
            <h4><em>Haben Sie noch Fragen?</em></h4>
            <p>Wir sind 24 Stunden kostenfrei für Sie erreichbar:</p>
            {include file="phone-numbers/phone-sales.tpl"}
        </section>

      </main>
  </div>

{include 'footer.tpl'}

