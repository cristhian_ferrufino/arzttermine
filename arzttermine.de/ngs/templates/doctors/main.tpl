{* Just a monkeypatch of NGS to use the new design. Not beutiful, but servers the purpose until this gets refactored. *}
{if $smarty.const.LOAD_NEW_DESIGN === 'true'}
	{nest ns=content}
{else}

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="robots" content="noindex, nofollow" />
        <meta http-equiv="X-UA-Compatible" content="IE=9" />
		<title>Ärzte | Arzttermine.de</title>
		{include file="doctors/headerControls.tpl"}
		
	</head>
	<body class="js">
{include file="js/tagmanager/tagmanager.tpl"}
		<div id="page-main">
			<input type="hidden" id="initialLoad" name="initialLoad" value="main" />
			<input type="hidden" id="contentLoad" value="{$ns.contentLoad}" />
			{*<div class="page-container" >*}
				{nest ns=content}
			{*</div>*}
			{include file="main/footer.tpl"}
		</div>
	</body>
</html>
{/if}