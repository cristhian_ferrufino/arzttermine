{include "doctors/header.tpl"}
<div id="main" class="middle-responsiv profile-edit profile">

    <div id="header">
		{assign var=locations value=$ns.locations}
        <div class="left profile-title">Fachrichtung löschen</div>
        <div id="optinal-function">
            <span class="arrow"></span>
            <ul id="item-list" style="display: none;">
            {foreach from=$locations item=location}
				<li class="first locationName"><span>{$location->getName()}</span></li>
			{/foreach}
            <li><a href="#lorem" target="_blank">Ansicht</a></li>
            <li><a href="#save">Speichern</a></li>
            <li><a href="#hilfe">Hilfe</a></li>
            <li class="last">{if isset($ns.userId) and  $ns.userId > 0}<a href="//{$SITE_URL}/dyn/{$ns.arzconfig.DOCTORS_LOGIN_URL}/do_logout">abmelden</a>{/if}</li>
            </ul>
        </div>
    </div>
	<div id="doc-manager-edit" class="doc_border">
		<div id="edit-left_side">
			{include "doctors/left_panel.tpl"}
		</div>
		<div id="doc-content-main" class="doc_border" style="border:none">
                    {if isset($ns.error)}
                        <div class="error">
                            {$ns.error.error_message}
                        </div>
                    {/if}
			<ul id="general-information" style="border-bottom:none">
				<li class="list_head">
					<h4>Fachrichtungen</h4>
				</li>
				{foreach from=$ns.medicalSpecialties item=medicalSpecialty}
					<li class="delete-specialties">
						{$medicalSpecialty->getNameSingularDe()}
						<a href="{$SITE_PATH}/{$ns.arzconfig.DOCTORS_LOGIN_URL}/do_fachrichtung_loschen?id={$medicalSpecialty->getId()}" class="delete" style="float: right" ></a>
					</li>
				{/foreach}
			</ul>
		</div>
	</div>
</div>
<script>
	window.onload = function(){
		var deleteLinks = document.getElementsByClassName('delete');
		for (var i=0;i<deleteLinks.length;i++){
			deleteLink = deleteLinks[i];
			deleteLink.onclick = function(){

			}
		}
	}	
</script>
