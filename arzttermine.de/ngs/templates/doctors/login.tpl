{assign var="template_body_class" value="two-cols-with-form"}
{assign var="additional_body_classes" value="doctor-registration doctor-login"}
{assign var="use_minimal_footer" value=true}
{assign var="isSales" value=true}
{include 'header.tpl'}

  <div class="container">
    <main>
      <div class="already-registered">Sie haben noch keinen Ärzte-Zugang? <a href="/aerzte/registrierung">Hier registrieren.</a></div>
      <h1>Ärzte-Management</h1>

      <section id="form">
        <form id="booking-form" class="{if isset($smarty.request.forgotten)}hidden{/if}" action="//{$SITE_URL}/{$ns.arzconfig.DOCTORS_LOGIN_URL}/do_anmelden" method="post" enctype="application/x-www-form-urlencoded" >
          <div class="form-element">
            <label>E-Mail-Adresse</label>
            <input type="email" name="email" tabindex="1" value="{$email}">
            <a role="button" tabindex="4" href="#">Ich habe meinen Login-Namen / E-Mail-Adresse vergessen</a>
          </div>
          <div class="form-element">
            <label>Passwort</label>
            <input type="password" tabindex="2" name="password">
            <a role="button" tabindex="5" href="#">Ich habe mein Passwort vergessen</a>
          </div>
          <div class="button-wrapper"><input type="submit" tabindex="3" value="Anmelden"></div>
        </form>  

        <form id="forgot-email" class="{if !isset($smarty.request.forgotten) || (isset($smarty.request.forgotten) && $smarty.request.forgotten != "login")}hidden{/if}" action="//{$SITE_URL}/{$ns.arzconfig.DOCTORS_LOGIN_URL}/do_anmelden_vergessen" method="post">
          <div class="form-element">
            <label>Vorname<label>
            <input type="text" name="doctor_name">
          </div>
          <div class="form-element">
            <label>Nachname<label>
            <input type="text" name="doctor_lname">
          </div>
          <div class="form-element">
            <label>Praxisname<label>
            <input type="text" name="practice_name">
          </div>
          <div class="button-wrapper"><input type="submit" value="Login anfordern"></div>
        </form>

        <form id="forgot-password" class="{if !isset($smarty.request.forgotten) || (isset($smarty.request.forgotten) && $smarty.request.forgotten != "password")}hidden{/if}" action="//{$SITE_URL}/{$ns.arzconfig.DOCTORS_LOGIN_URL}/do_passwort_vergessen" method="post">
          <div class="form-element">
            <label>E-Mail-Adresse<label>
            <input type="email" name="email">
          </div>
          <div class="button-wrapper"><input class="btn_green button large center" id="btn-pw_submit" type="submit" value="Passwort anfordern"></div>
        </form>

      </section>

      <section id="info-block"  class="infobox infobox-grey-light cta-phone">
        <h4><em>Haben Sie noch Fragen?</em></h4>
        <p>Wir sind 24 Stunden kostenfrei für Sie erreichbar:</p>
        {include file="phone-numbers/phone-sales.tpl"}
      </section>
	  </main>
  </div>

{*
<div id="main" class="middle-responsiv login">
	<div id="login-maks">
		<div id="header">
			<h1 class="greenblue ta_center">Ärzte-Management</h1>
			 {if isset($ns.error_message)}
			<div class="error login_error_message">
				{$ns.error_message}
			</div>
			{/if}
			{if isset($ns.message)}
			<div class="login_error_message">
				{$ns.message}
			</div>
			{/if}
			<div id="support-box">
				<h6>Support-Telefon</h6>
				<span class="tel greenblue"><b>0800/2222 133</b></span>
				<span class="small-gray">(kostenfrei)</span>
			</div>
		</div>
		<form id="login_form"  name="login_form doc_border" style="text-align: center" action="//{$SITE_URL}/{$ns.arzconfig.DOCTORS_LOGIN_URL}/do_anmelden" method="post">
			<fieldset>
				<div class="login_form">
					<input id="email" class="input_text sp-login" type="email" name="email" placeholder="E-Mail Adresse" autofocus required />
					<input id="password" class="input_password sp-login" type="password" name="password" placeholder="Passwort" required/>
				</div>
			</fieldset>
			<fieldset id="actions">
					<input id="login-button" class="large button btn_green" type="submit" value="Anmelden"/>
					<a class="large button new-grey-button" href="{$SITE_PATH}/{$ns.arzconfig.DOCTORS_LOGIN_URL}/registrierung">Registrieren</a>
				{if not $ns.forgot_login}
					<p id="login-link"  class="lost-link"><span class="forgot_email greenblue click">Ich habe meinen Login-Namen / E-Mail-Adresse vergessen</span>.</p>
					{ *<a class="lost-login sp-login click" href="{$SITE_PATH}/{$ns.arzconfig.DOCTORS_LOGIN_URL}/forgot_login">Login-Namen vergessen</a></p>* }
				{/if}
				{if not $ns.forgot_password}
					<p class="lost-link"><span class="forgot_password greenblue click">Ich kenne mein Passwort nicht mehr</span>.</p>
					{ *<a class="lost-password sp-login click" href="{$SITE_PATH}/{$ns.arzconfig.DOCTORS_LOGIN_URL}/forgot_password">Passwort nicht mehr</a></p>* }
				{/if}
			</fieldset>
		</form>
		<div id="forgot-area">
			<div id="forgot_email" class="login_form">
				<h4>Login/ E-Mail vergessen:</h4>
				<p><span>Bitte geben Sie Ihren Praxisnamen und Ihre Telefonnummer an, wir senden Ihnen die Zugangsdaten erneut per E-Mail.</span></p>
				<form id="forgot_email_form" name="login_form" style="text-align: center" action="//{$SITE_URL}/{$ns.arzconfig.DOCTORS_LOGIN_URL}/do_anmelden_vergessen" method="post">
					<p><input id="doctor_name" type="text" name="doctor_name" placeholder="Vornamen" />
                        <input id="doctor_lname" type="text" name="doctor_lname" placeholder="Nachnamen" />                    
                    </p>
                    <p><input id="practice_name" type="text" name="practice_name" placeholder="Praxis" /></p>
					<!--p><input id="phone_number" type="text" name="phone_number" placeholder="Telefonnummer" /></p-->
					<button id="btn-mail_submit" class="center btn_green button large" type="submit" value="Login anfordern">Login anfordern</button>
				</form>
			</div>
			<div id="forgot_password" class="login_form">
				<h4>Passwort vergessen:</h4>
				<p>Bitte geben Sie Ihre E-Mail-Adresse ein, damit wir Ihnen ein neues Passwort zusenden können.</p>
				<form id="forgot_password_form" name="login_form" style="text-align: center" action="//{$SITE_URL}/{$ns.arzconfig.DOCTORS_LOGIN_URL}/do_passwort_vergessen" method="post">
					<p><input id="email" type="text" name="email" placeholder="E-Mail-Adresse" /></p>
					<button class="btn_green button large center" id="btn-pw_submit" type="submit" value="Passwort anfordern" >Passwort anfordern</button>
				</form>
			</div>
		</div>
		
		<!-- forgot password -->
		{ *{if $ns.forgot_password}
		
		{/if}* }
		<!-- forgot login -->
		{ *{if isset($ns.forgot_login)}
		
		{/if}* }
	</div>	{ * END OF LOGIN-MASK  * }
	<div id="help-info" class="middle">
		<p>
			<b>Sie sind noch nicht bei Arzttermine.de?</b><br/>
			Kontaktieren Sie uns <a href="/kontakt">einfach online.</a><br/>
			Auch telefonisch sind wir für Sie jederzeit erreichbar: <span class="tel greenblue">0800/2222 133</span>.
		</p>
	</div>
</div>

*}

{include 'footer.tpl'}

