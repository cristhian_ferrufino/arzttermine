{assign var="template_body_class" value="two-cols-with-form"}
{assign var="additional_body_classes" value="doctor-registration confirmation"}
{assign var="use_minimal_footer" value=true}
{assign var="isSales" value=true}
{assign var="hasFullHeader" value="false"}
{include 'header.tpl'}

  <div class="container">
    <main>
      {if isset($smarty.request.gender)}
        {if $smarty.request.gender == 1}
          {assign var="gender_title" value="Frau"}
        {else}
          {assign var="gender_title" value="Herr"}
        {/if}
      {/if}
      <h1>Herzlichen Glückwunsch, {if isset($gender_title)}{$gender_title}{/if} {if isset($smarty.request.full_title)}{$smarty.request.full_title}{/if} {if isset($smarty.request.last_name)}{$smarty.request.last_name}{/if}!</h1>
      <section id="form">
        <form id="booking-form" class="{if isset($error_class)}{$error_class}{/if}" action="{$SITE_PATH}/{$ns.arzconfig.DOCTORS_LOGIN_URL}/do_registrieren_aerzte?step=2&id={$smarty.request.id}" method="post" enctype="application/x-www-form-urlencoded" >
            <h4>Sie haben sich erfolgreich registriert!</h4>
            <p>In Kürze wird Sie ein Mitarbeiter persönlich kontaktieren, um mit Ihnen die Details zu klären.</p>
            <p>Nutzen Sie die Zeit bis dahin, um Ihr Profil zu pflegen.</p>

            <div class="form-element {if isset($error_class)}{$error_class}{/if}" data-error-message="Bitte wählen Sie Ihre Anrede aus.">
              <label>Ihr Fachgebiet</label>
              <select name="medical_specialty_id" >
                <option value="">Bitte wählen</option>
                {html_options options=$medicalSpecialtiesOptions}
              </select>
            </div>

            <div class="form-element {if isset($error_class)}{$error_class}{/if}" data-error-message="Bitte geben Sie Ihren Praxisnamen ein.">
              <label>Praxisname</label>
              <input type="text" name="practice_title" value="{if isset($smarty.request['practice_title'])}{$smarty.request['practice_title']}{/if}">
            </div>

            <div class="form-element {if isset($error_class)}{$error_class}{/if}" data-error-message="Bitte geben Sie Ihre Stadt ein.">
              <label>Stadt</label>
              <input type="text" name="city" value="{if isset($smarty.request['city'])}{$smarty.request['city']}{/if}">
            </div>

            <div class="form-element {if isset($error_class)}{$error_class}{/if}" data-error-message="Bitte geben Sie eine gültige Mobilnummer ein.">
              <label>Mobilnummer</label>
              <input type="tel" name="phone_mobile" value="{if isset($smarty.request['phone_mobile'])}{$smarty.request['phone_mobile']}{/if}">
            </div>

            <div class="button-wrapper"><button type="submit">Weiter</button></div>
        </form>

      </section>

      <section id="info-block"  class="infobox infobox-grey-light cta-phone">
          <h4><em>Haben Sie noch Fragen?</em></h4>
          <p>Wir sind 24 Stunden kostenfrei für Sie erreichbar:</p>
          {include file="phone-numbers/phone-sales.tpl"}
      </section>

  </main>
</div>


{include 'footer.tpl'}

