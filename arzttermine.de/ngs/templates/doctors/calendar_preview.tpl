<div style="margin:10px;">
	<div style="float:left">
		<a id="cp_preview_link" href="javascript:void(0);" date="{$ns.previous_monday_date}">vorherige Woche</a>
	</div>    
	<div id="cp_next_link" style="float:right" date="{$ns.next_monday_date}">
		<a href="javascript:void(0);">nächste Woche</a>
	</div>
	<div style="clear: both"></div>
</div>
<div style="clear: both"></div>
<div id="ic_contact_tabs">
    <ul>
        {foreach from=$ns.insurances item=insurance_name key=insurance_id name=cl} 
            <li><a href="#tab_insurance_{$insurance_id}">{$insurance_name}</a></li>
			{/foreach}
    </ul>
    {foreach from=$ns.insurances item=insurance_name key=insurance_id name=cl} 
        <div id="tab_insurance_{$insurance_id}">
            <div class="profile-doctor german" style="position: relative; left: -13px; font-family: tahoma,helvetica,arial">
                <div id="bd">
                    <div class="container_1">
                        <div class="calendar-times-wrapper">
                            <div class="provider-list static-header">
                                <div class="provider-list-header">
                                    <div date_days="5" date_start="2013-06-07" class="calendar-container">
                                        <div class="calendar-slide odd">
                                            {$ns.headerHtml}
                                        </div>
                                    </div>
                                </div>
								{foreach from=$ns.locations item=location}
									{assign var=location_id value=$location->getId()}
									<h3>{$location->getName()}</h3>
                                <div class="provider-row odd first last">
                                    <div class="times-container times-days-5">
                                        <div class="times-slide odd">
                                            {$ns.timesHtml.$location_id.$insurance_id}
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
								{/foreach}
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {/foreach}
</div>
