
{include "doctors/header.tpl"}

<div id="main" class="middle-responsiv location location-doctors doctor profile">
	<div id="header">
		<div class="left profile-title">{$location->getLocation()->getName()}</div>
		<div id="optinal-function">
			<span class="arrow"></span>

			<ul id="item-list" style="display: none;">
				{foreach from=$locations item=location_item}
					<li class="first locationName"><span>{$location_item->getName()}</span></li>
				{/foreach}
				<li><a href="/kontakt" target="_blank">Kontakt</a></li>
				<li class="last">{if isset($ns.userId) and $ns.userId > 0}<a href="//{$SITE_URL}/dyn/{$ns.arzconfig.DOCTORS_LOGIN_URL}/do_logout">abmelden</a>{/if}</li>
			</ul>
		</div>
	</div>
	<div id="doc-manager-edit" class="doc_border">
		<div id="edit-left_side">
			{include "doctors/left_panel_location.tpl"}
		</div>

		<div id="doc-content-main" class="doc_border">
			<h3>Alle Ärzte der Praxis</h3>
			<div id="doctors-at-practice" class="clearfix">
				{foreach $doctors as $doctor}
				<div class="doctor-at-practice">
					<div class="wrap clearfix">
						<div class="profile-pic">
							<img src="{$SITE_PATH}{$doctor->profileUrl}" width="140" alt="" />
						</div>
						<div class="details">
							<span class="doctor-name">{$doctor->getTitle()} {$doctor->getFirstName()} {$doctor->getLastName()}</span>
							<div class="specialties">
						{if !empty($specialties[$doctor->getId()])}
							{foreach $specialties[$doctor->getId()] as $specialty}
								<span class="specialty">{$specialty->getNameSingularDe()}</span>
							{/foreach}
						{/if}
							</div>
							<div class="actions">
								<a href="{$SITE_PATH}/arzt/{$doctor->getSlug()}">Arztprofil ansehen</a>
								<a href="{$SITE_PATH}/{$ns.arzconfig.DOCTORS_LOGIN_URL}/account_bearbeitung/{$doctor->getSlug()}">Arztprofil bearbeiten</a>
							</div>
						</div>
					</div>
				</div>
				{/foreach}
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		$('#praxen').change(function() {
			window.location = '{$SITE_PATH}/{$ns.arzconfig.DOCTORS_LOGIN_URL}/praxis/' + this.options[this.selectedIndex].value;
		});
	});
</script>