{include "doctors/header.tpl"}
<div id="main" class="billing-section middle-responsiv profile-edit profile">
	<div id="header">
		<div class="left profile-title">{$location->getLocation()->getName()}</div>
		<div id="optinal-function">
			<span class="arrow"></span>

			<ul id="item-list" style="display: none;">
				{foreach from=$locations item=location_item}
					<li class="first locationName"><span>{$location_item->getName()}</span></li>
				{/foreach}
				<li><a href="/kontakt" target="_blank">Kontakt</a></li>
				<li class="last">{if isset($ns.userId) and $ns.userId > 0}<a href="//{$SITE_URL}/dyn/{$ns.arzconfig.DOCTORS_LOGIN_URL}/do_logout">abmelden</a>{/if}</li>
			</ul>
		</div>
	</div>
    <div id="doc-manager-edit" class="doc_border">
        <div id="edit-left_side">
			{include "doctors/left_panel_location.tpl"}
        </div>
	    
        <div id="doc-content-main" class="doc_border">
            <h3>Abrechnung Abschnitt</h3>
        {if isset($ns.error)}
	        <div class="error">{$ns.error.error_message}</div>
        {/if}
            <div id="top_billing">
                <div id="current_month">{$period}</div>
                <div id="status">STATUS: <span>{if $completed}In review{else}Ready for review{/if}</span></div>
                <div id="month_select_container">
	                <select id="select_month">
	                {html_options values=$periods output=$periods selected=$period}
	                </select>
                </div>
            </div>
	        
	        {foreach from=$upcoming_bookings key=day item=booking_days}
	        <div class="billing_date">
		        <div class="billing_day">
			        {strftime("%a", strtotime({$day|cat:'-'|cat:$period}))}
		        </div>
		        <div class="billing_num">
			        {$day}
		        </div>
	        </div>
	        <table class="infoTable" border="0">
		        {foreach from=$booking_days item=booking}
			        <tr>
				        <td class="info_label">
					        {date("H:i", strtotime($booking->getAppointmentStartAt()))}
				        </td>
				        <td class="info_value">
					        <span class="booking_patient">{$booking->getGenderText()} {$booking->getFirstName()} {$booking->getLastName()}</span>
					        {assign var=isConfirmed value=$booking->getConfirmed()}
					        <div class="booking_status clearfix">
					        {if $completed}
						        <label>{if $isConfirmed}Confirmed{else}No show{/if}</label>
					        {else}
						        <label><input type="radio" class="status" data-booking-id="{$booking->getId()}" name="status[{$booking->getId()}]" value="1"{if $isConfirmed} checked{/if}>Confirmed</label>
						        <label><input type="radio" class="status" data-booking-id="{$booking->getId()}" name="status[{$booking->getId()}]" value="0"{if !$isConfirmed} checked{/if}>No show</label>
					        {/if}
					        </div>
				        </td>
			        </tr>
		        {/foreach}
	        </table>
			{/foreach}
	        {if !$completed}
            <a id="review_completed_btn" class="button medium new-grey-button" href="#">Review completed</a>
	        {/if}
        </div>
    </div>
</div>

<script>
	$(document).ready(function() {
		$('#select_month').on('change', function() {
			window.location.replace('{$SITE_PATH}/{$ns.arzconfig.DOCTORS_LOGIN_URL}/abrechnung_abschnitt/{$location->getLocation()->getSlug()}?period=' + $(this).val());
		});

		{if !$completed}
		$('.status').on('change', function() {
			var $this = $(this);
			
			$.post(
				'{$SITE_PATH}/{$ns.arzconfig.DOCTORS_LOGIN_URL}/do_update_confirmed/{$location->getLocation()->getSlug()}',
				{
					id: $this.data('booking-id'),
					c: $this.val(),
					uid: {$location->getLocation()->getId()}
				}
			);
		});
		
		//show confirm window when submitting the review
		$('#review_completed_btn').on('click', function(e) {
			e.preventDefault();
			
			if (confirm("Are you sure you are done reviewing the statement for {$period}?")) {
				$.post(
					'{$SITE_PATH}/{$ns.arzconfig.DOCTORS_LOGIN_URL}/do_update_statement_status/{$location->getLocation()->getSlug()}',
					{
						status: 1,
						period: '{$period}',
						uid: {$location->getLocation()->getId()}
					}
				);
			}
		});
		{/if}
	});
</script>