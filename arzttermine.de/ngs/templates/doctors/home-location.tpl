{assign var="template_body_class" value="profile-page"}
{assign var="additional_body_classes" value="doctor doctor-account praxis"}
{include 'header.tpl'}

{$this->addJsInclude($this->getStaticUrl('js/doctor-profile.js'))}

{* Set the current location *}
{assign var=location_id value=$location->getLocation()->getId()}


<div class="container">
    <main>
        <section class="doctor-summary" >
            <div class="headline justified-blocks">
                <h1>{$location->getLocation()->getName()}</h1>
                {if isset($ns.userId) and  $ns.userId > 0}
                <a class="logout" href="//{$SITE_URL}/dyn/{$ns.arzconfig.DOCTORS_LOGIN_URL}/do_logout">abmelden</a>
                {/if}
            </div>
            <nav>
                <ul>
                    {* Cache this value for readability *}
                    {assign var=login_url value="{$SITE_PATH}/{$ns.arzconfig.DOCTORS_LOGIN_URL}"}
                    {assign var=profile_pic value=$location->getProfile()}
                    {assign var=slug value=$location->getLocation()->getSlug()}
                    <li><a href="{$login_url}/account">Mein Arztprofil</a></li>
                    {if $locations && $locations[0]->getIntegrationId() == 1}
                        <li><a href="{$login_url}/termine">Termine verwalten</a></li>
                    {/if}
                    <li class="active"><a href="{$login_url}/praxis/{$slug}">Praxisprofil</a></li>
                </ul>
            </nav>
        </section>

        <section>
            <form action="{$SITE_PATH}/{$ns.arzconfig.DOCTORS_LOGIN_URL}/do_speichern_praxis/{$location->getLocation()->getSlug()}" method="post">
                <div class="profile-photo">
                    <img src="{$SITE_PATH}{if $profile_pic}{$profile_pic}{else}/ngs/img/default_avatar.jpg{/if}" alt="Praxis Profilbild">
                </div>

                <div class="text-data">
                    <h3>Praxisname</h3>
                    <div>
                        <select id="praxen">
                            {foreach from=$locations item=location_item}
                                <option value="{$location_item->getSlug()}" {if $location_id == $location_item->getId()}selected{/if}>{$location_item->getName()}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>

                <div class="text-data">
                    <h3>Anschrift</h3>
                    <div>{$location->getLocation()->getStreet()}</div>
                </div>

                <div class="text-data">
                    <h3>PLZ</h3>
                    <div>{$location->getLocation()->getZip()}</div>
                </div>

                <div class="text-data">
                    <h3>Ort</h3>
                    <div>{$location->getLocation()->getCity()}</div>
                </div>

                <div class="text-data">
                    <h3>Telefon</h3>
                    <div class="editable-container">
                        <input type="text" name="phone" value="{$location->getLocation()->getPhone()}">
                    </div>
                </div>

                <div class="text-data">
                    <h3>Website</h3>
                    <div class="editable-container">
                        <input type="text" name="www" value="{$location->getLocation()->getWww()}">
                    </div>
                </div>

                <div class="text-data">
                    <h3>Fachrichtung</h3>
                    <div class="editable-container specialties">
                        {foreach from=array_chunk($all_specialties, 2) item=rowItem}
                        {foreach from=$rowItem item=specialty}
                        {if in_array($specialty->getId(), $locationsMedSpecIdsArray)}
                        <label>
                            <input type="checkbox" name="medical_specialties_ids[]" value="{$specialty->getId()}" checked>
                            {assign var="specialty_selected" value="true"}
                            {$specialty->getNameSingularDe()}
                        </label>
                        {/if}
                        {/foreach}
                        {/foreach}

                        {if isset($specialty_selected)}
                        <a role="button" class="toggle-specialties">Show <span class="show-more">more</span><span class="show-less">less</span></a>
                        {/if}

                        <div class="unselected-specialties {if !isset($specialty_selected)}visible{/if}">
                            {foreach from=array_chunk($all_specialties, 2) item=rowItem}
                            {foreach from=$rowItem item=specialty}
                            {if !in_array($specialty->getId(), $locationsMedSpecIdsArray)}
                            <label>
                                <input type="checkbox" name="medical_specialties_ids[]" value="{$specialty->getId()}">
                                {$specialty->getNameSingularDe()}
                            </label>
                            {/if}
                            {/foreach}
                            {/foreach}
                        </div>
                    </div>
                </div>

                <div class="text-data">
                    <h3>Alle Ärzte der Praxis</h3>
                    <div>
                    {foreach $doctors as $praxisDoctor}
                        <section class="praxis-doctor">
                            <img src="{$SITE_PATH}{$praxisDoctor->profileUrl}" alt="{$praxisDoctor->getTitle()} {$praxisDoctor->getFirstName()} {$praxisDoctor->getLastName()}">
                            <h4>{$praxisDoctor->getTitle()} {$praxisDoctor->getFirstName()} {$praxisDoctor->getLastName()}</h4>
                            {if !empty($specialties[$praxisDoctor->getId()])}
                                {foreach $specialties[$praxisDoctor->getId()] as $specialty}
                                <span>{$specialty->getNameSingularDe()}</span>
                                {/foreach}
                            {/if}
                            <a href="{$SITE_PATH}/arzt/{$praxisDoctor->getSlug()}">Arztprofil ansehen</a>
                            <a href="{$SITE_PATH}/{$ns.arzconfig.DOCTORS_LOGIN_URL}?arzt={$praxisDoctor->getSlug()}">Arztprofil bearbeiten</a>
                        </section>  
                    {/foreach}
                    </div>                    
                </div>
                <input type="hidden" name="uid" value="{$location_id}" />
                
                <div class="buttons-container">
                    <button class="save">Save</button>
                    <button class="reset">Reset</button>
                </div>

            </form>
        </section>
        
    </main>
</div>

<script>
    var doctorsMedSpecIdsArray = {json_encode($specialties)} || [];
    var docAccountApiUrl = "//{$SITE_URL}/dyn/{$ns.arzconfig.DOCTORS_LOGIN_URL}/";
    var docAccountUrl = '{$ns.arzconfig.DOCTORS_LOGIN_URL}';
</script>

{include 'footer.tpl'}
