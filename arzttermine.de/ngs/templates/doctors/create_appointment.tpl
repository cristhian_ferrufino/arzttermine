{include "doctors/header.tpl"}
<div id="main" class="create-appointment middle-responsiv profile-edit profile">
    <div id="header">
        <div class="left profile-title">Termin-Verwaltung</div>
{include "doctors/log_out.tpl"}
    </div>
	<div id="doc-manager-edit" class="doc_border">
		<div id="edit-left_side">
{include "doctors/left_panel.tpl"}
		</div>
		<div id="doc-content-main" class="doc_border">
{if isset($no_access_error)}
            <div class="error">{$no_access_error}</div>
{else}
    {if isset($error)}
            <div class="error">{$error.error_message}</div>
    {/if}
            <h4 class="subHeader">Termin erstellen</h4>
			<div class="grey_normal">
				<form action="{$SITE_PATH}/{$ns.arzconfig.DOCTORS_LOGIN_URL}/do_ertellen_termin" method="post">
					<table class="create_appointment_table" width="100%">
						<tr>
							<td></td>
							<td>Startzeit</td>
							<td>Endzeit</td>
							<td>Dauer</td>
							<td>Datum</td>
						</tr>
						<tr class="termin_row">
							<td class="grey_bold">Termin:</td>
							<td>
								<input type="text" placeholder="hh:mm" name="start_time" style="width: 100px" {if isset($ns.request.start_time)}value="{$ns.request.start_time}"{/if}/>
							</td>
							<td>
								<input type="text" placeholder="hh:mm" name="end_time" style="width: 100px" {if isset($ns.request.end_time)}value="{$ns.request.end_time}"{/if}/>
							</td>
							<td>
								<input type="text" placeholder="mm" name="duration" style="width: 100px" {if isset($ns.request.duration)}value="{$ns.request.duration}"{/if}/>
							</td>
							<td>
								<input id="appointment_date" type="text" name="date" style="width: 100px" {if isset($ns.request.date)}value="{$ns.request.date}"{/if}/>
							</td>
						</tr>
						<tr>
							<td class="grey_bold" valign="top">Behandlungen:</td>
							<td valign="top" colspan="2">
    {foreach from=$treatmentTypes item=treatmentType}
                                <input type="checkbox" {if !isset($ns.request) || in_array($treatmentType->getId(),$ns.request.treatment_types)}checked="checked"{/if}  name="treatment_types[]" value="{$treatmentType->getId()}"/>{$treatmentType->getNameDe()}<br />
    {/foreach}
							</td>
                            <td class="grey_bold" valign="top">Versicherung:</td>
							<td valign="top" colspan="2">
    {foreach from=$ns.arzconfig.INSURANCES item=insurance key=id}
								<input type="checkbox" {if !isset($ns.request) || in_array($id,$ns.request.insurance_types)}checked="checked"{/if} name="insurance_types[]" value="{$id}"/>{$insurance}<br />
    {/foreach}
							</td>
						</tr>
						<tr>
							<td colspan="5" align="center">
								<input class="btn_green" type="submit" value="Speichern" />
								<input class="btn_gray_new" type="button" value="Erstellen anderen Termin"/>
							</td>
						</tr>
					</table>
				</form>
			</div>	
{/if}
		</div>
	</div>
</div>
