<ul id="nav-02">
{if $ns.sess_admin->checkPrivilege('user_admin')}
	<li><a href="/admin/users/index.php"><img src="{$STATIC_PATH}/static/adm/icon-users.png" width="24" height="24" alt=""/>Users</a></li>
{/if}
{if $ns.sess_admin->checkPrivilege('user_admin')}
	<li><a href="/admin/temp_users/index.php"><img src="{$STATIC_PATH}/static/adm/icon-users.png" width="24" height="24" alt=""/>Temp Users</a></li>
{/if}
{if $ns.sess_admin->checkPrivilege('group_admin')}
	<li><a href="/admin/groups/index.php"><img src="{$STATIC_PATH}/static/adm/icon-groups.png" width="24" height="24" alt=""/>Groups</a></li>
{/if}
{if $ns.sess_admin->checkPrivilege('cms_admin')}
	<li><a href="/admin/cms/index.php"><img src="{$STATIC_PATH}/static/adm/icon-cms.png" width="24" height="24" alt=""/>CMS</a></li>
{/if}
{if $ns.sess_admin->checkPrivilege('media_admin')}
	<li><a href="admin/media/index.php"><img src="{$STATIC_PATH}/static/adm/icon-media.png" width="24" height="24" alt=""/>Mediathek</a></li>
{/if}
{if $ns.sess_admin->checkPrivilege('location_admin')}
	<li><a href="/admin/locations/index.php"><img src="{$STATIC_PATH}/static/adm/icon-world.png" width="24" height="24" alt=""/>Praxen</a></li>
{/if}
{if $ns.sess_admin->checkPrivilege('booking_admin')}
	<li><a href="/admin/bookings/index.php"><img src="{$STATIC_PATH}/static/adm/icon-booking.png" width="24" height="24" alt=""/>Buchungen</a></li>
{/if}
{if $ns.sess_admin->checkPrivilege('bookingoffer_admin')}
    <li><a href="/admin/booking-offers/index.php"><img src="{$STATIC_PATH}/static/adm/icon-booking-offer.png" width="24" height="24" alt=""/>BookingOffers</a></li>
{/if}
{if $ns.sess_admin->checkPrivilege('coupon_admin')}
    <li><a href="/admin/coupons/index.php"><img src="{$STATIC_PATH}/static/adm/icon-coupon.png" width="24" height="24" alt=""/>Gutscheine</a></li>
    <li><a href="/admin/coupons-used/index.php"><img src="{$STATIC_PATH}/static/adm/icon-coupon.png" width="24" height="24" alt=""/>Gutscheine (Used)</a></li>
{/if}
{if $ns.sess_admin->checkPrivilege('insurance_admin')}
	<li><a href="/admin/insurances/index.php"><img src="{$STATIC_PATH}/static/adm/icon-shield.png" width="24" height="24" alt=""/>Versicherungen</a></li>
{/if}
{if $ns.sess_admin->checkPrivilege('medicalspecialty_admin')}
	<li><a href="/admin/medicalspecialties/index.php"><img src="{$STATIC_PATH}/static/adm/icon-medicalspecialties.png" width="24" height="24" alt=""/>Fachrichtungen</a></li>
{/if}
{if $ns.sess_admin->checkPrivilege('treatmenttype_admin')}
	<li><a href="/admin/treatmenttypes/index.php"><img src="{$STATIC_PATH}/static/adm/icon-treatmenttypes.png" width="24" height="24" alt=""/>Behandlungsarten</a></li>
{/if}
{if $ns.sess_admin->checkPrivilege('appointment_admin')}
	<li><a href="/ngsadmin/appointments"><img src="{$STATIC_PATH}/static/adm/icon-appointment.png" width="24" height="24" alt=""/>Termine</a></li>
{/if}
{if $ns.sess_admin->checkPrivilege('contact_admin')}
	<li><a href="/admin/contacts/index.php"><img src="{$STATIC_PATH}/static/adm/icon-contact.png" width="24" height="24" alt=""/>Kontakt</a></li>
{/if}
{if $ns.sess_admin->checkPrivilege('mailing_admin')}
	<li><a href="/admin/mailings/index.php"><img src="{$STATIC_PATH}/static/adm/icon-mailing.png" width="24" height="24" alt=""/>Mailings</a></li>
{/if}
{if $ns.sess_admin->checkPrivilege('login_history_admin')}
	<li><a href="/admin/login_history/index.php"><img src="{$STATIC_PATH}/static/adm/icon-login-history.png" width="24" height="24" alt=""/>Login History</a></li>
{/if}
{if $ns.sess_admin->checkPrivilege('message_admin')}
	<li><a href="/ngsadmin/messages"><img src="{$STATIC_PATH}/static/adm/icon-blog.png" width="24" height="24" alt=""/>Messageboard</a></li>
{/if}
{if $ns.sess_admin->checkPrivilege('blog_admin')}
	<li><a href="/admin/news/index.php"><img src="{$STATIC_PATH}/static/adm/icon-news.png" width="24" height="24" alt=""/>News</a></li>
{/if}
{if $ns.sess_admin->checkPrivilege('integration_admin')}
	<li><a href="/admin/integrations.php"><img src="{$STATIC_PATH}/static/adm/icon-integrations.png" width="24" height="24" alt=""/>Integrationen</a></li>
{/if}
{if $ns.sess_admin->checkPrivilege('searchresulttext_admin')}
	<li><a href="/admin/searchresulttexts/index.php"><img src="{$STATIC_PATH}/static/adm/icon-search.png" width="24" height="24" alt=""/>Suchergebnis-SEO</a></li>
{/if}
{if $ns.sess_admin->checkPrivilege('import_admin_doctors')}
	<li><a href="/ngsadmin/import_doctors"><img src="{$STATIC_PATH}/static/adm/import-icon.png" width="24" height="24" alt=""/>Importieren Arzte</a></li>
{/if}
{if $ns.sess_admin->checkPrivilege('booking_admin')}
	<li><a href="/ngsadmin/samedi_bookings"><img src="{$STATIC_PATH}/static/adm/icon-booking.png" width="24" height="24" alt=""/>Samedi Buchungen</a></li>
{/if}
{if $ns.sess_admin->checkPrivilege('integration_admin')}
	<li><a href="/ngsadmin/integrations"><img src="{$STATIC_PATH}/static/adm/icon-integrations.png" width="24" height="24" alt=""/>Samedi Integrationen</a></li>
{/if}
{if $ns.sess_admin->checkPrivilege('integration_admin')}
	<li><a href="/ngsadmin/doctors_calendar"><img src="{$STATIC_PATH}/static/adm/calendar-icon.png" width="24" height="24" alt=""/>Doctors Calendars</a></li>
{/if}
{if $ns.sess_admin->checkPrivilege('group_admin')}
	<li><a href="/ngsadmin/cms_config"><img src="{$STATIC_PATH}/static/adm/cms_config.png" width="24" height="24" alt=""/>CMS Config</a></li>
{/if}
</ul>
