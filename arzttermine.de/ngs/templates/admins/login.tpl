{include "admins/header.tpl"}
<div class="page-wrapper">
	<div id="logo">
		<a title="Home Arzttermine.de Arzt &amp; Zahnarzt Termine Berlin Frankfurt Hamburg Munchen" href="/"> <img width="216" height="60" alt="Home Arzttermine.de Arzt &amp; Zahnarzt Termine" src="/static/img/logo.png" /> </a>
	</div>
	<div id="hotline-top" style="display:block">
		<span class="hotline-text">Wir helfen Ihnen gerne:</span>
		<img class="ico-phone" height="28" alt="Phone-Icon" src="/static/img/icons/icon_phone.png">
		<span class="at_grbl" title="Wir helfen Ihnen gerne bei der Auswahl Ihres Arztes oder der Buchung des Wunschtermins (24h / kostenfrei)">0800 / 2222 133</span>
	</div>

	{if isset($ns.error) and $ns.error=='true'}
	<div class="flash" id="flash">
		<div class="error" style="left: 0px;">
			{$ns.error_message}
			<a class="hide" style="cursor: pointer" onclick="document.getElementById('flash').style.display='none'">X</a>
		</div>
	</div>
	{/if}
	<div id="bd">
		<div class="container_1">
			<div class="grid_1 content padding-top padding-left-right">
				<div id="login" class="section">
					<form id="login_form" action="/dyn/admins/do_login" method="post" name="login_form">
						<p>
							<label for="email"> E-Mail:
								<br>
								<input id="email" class="input_text" type="text" name="email" placeholder="E-Mail Address">
							</label>
						</p>
						<p style="clear:both;padding-top:1em;">
							<label for="password"> Password:
								<br>
								<input id="password" class="input_password" type="password" maxlength="20" name="password" placeholder="Password">
							</label>
						</p>
						<p class="field submit">
							<input class="input_submit button medium orange" type="submit" value="Login">
						</p>
						<div class="clear"></div>
					</form>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>