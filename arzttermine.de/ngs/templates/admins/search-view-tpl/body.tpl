<form enctype="multipart/form-data" name="editform" action='http://{$SITE_URL}/ngsadmin/{$path}?search=yes' method="post">
	<table class="widefat" style="width: 730px">
		<tr class="columns">
			<th colspan="3">Suchen</th>
		</tr>

		{foreach from=$searchInfo key=name item=info}
		<tr class="search_key">
			<th> {$info.name} </th>

			{if $info.type==select}
			<td>
			<div style="text-align: right;">
				<input type="checkbox" name="{$name}_searchStyle" {if isset($info.searchStyle)}checked="checked"{/if}
				onclick="if (this.checked) {literal}{{/literal}document.getElementById('{$name}').style.display='block';{literal}}{/literal}else
				{literal}{{/literal}document.getElementById('{$name}').style.display='none';{literal}}{/literal}" />
			</div></td>
			<td>
			<div  id="{$name}" {if isset($info.searchStyle)}style="display: block;"{else}style="display: none;"{/if}>
				<select name="{$name}">
					{if isset($selectDisplayValues)}
					{if not empty ($selectDisplayValues.$name)}
					{if $name==backendMarkerClass}
					<option value="" selected="selected"></option>
					{/if}
					{foreach from=$selectDisplayValues.$name key=option item=optionValue}
					<option value="{$option}" {if isset($info.value) && $info.value==$option}selected='selected'{/if}>{$optionValue}</option>
					{/foreach}
					{/if}
					{/if}
				</select>
			</div></td>
			{else}
			<td>
			<select size="1" name="{$name}_searchStyle" onchange="if(this.value==6 || this.value==7){literal}{{/literal}document.getElementById('{$name}').style.display='none'; {literal}}{/literal}
			else{literal}{{/literal}document.getElementById('{$name}').style.display='block'; {literal}}{/literal}
			if(this.value==9){literal}{{/literal}document.getElementById('{$name}1').style.display='block'; {literal}}{/literal}
			else{literal}{{/literal}document.getElementById('{$name}1').style.display='none'; {literal}}{/literal}" >

				<option value="0" {if isset($info.searchStyle) and $info.searchStyle==0}selected='selected'{/if}>=</option>
				<option value="1" {if isset($info.searchStyle) and $info.searchStyle==1}selected='selected'{/if}>></option>
				<option value="2" {if isset($info.searchStyle) and $info.searchStyle==2}selected='selected'{/if}>>=</option>
				<option value="3" {if isset($info.searchStyle) and $info.searchStyle==3}selected='selected'{/if}><</option>
				<option value="4" {if isset($info.searchStyle) and $info.searchStyle==4}selected='selected'{/if}><=</option>
				<option value="5" {if isset($info.searchStyle) and $info.searchStyle==5}selected='selected'{/if}>!=</option>
				<option value="6" {if isset($info.searchStyle) and $info.searchStyle==6}selected='selected'{/if}>EMPTY</option>
				<option value="7" {if isset($info.searchStyle) and $info.searchStyle==7}selected='selected'{/if}>NOT EMPTY</option>
				<option value="8" {if isset($info.searchStyle) and $info.searchStyle==8}selected='selected'{/if}>LIKE</option>
				<option value="9" {if isset($info.searchStyle) and $info.searchStyle==9}selected='selected'{/if}>BETWEEN</option>
			</select></td>
			<td>
			<div id="{$name}" {if isset($info.searchStyle) && ($info.searchStyle==6 || $info.searchStyle==7)}style="display: none"{/if}>
				<input type="{$info.type}" name="{$name}" class="form_text"
				{if isset($info.value)}value="{$info.value}"{/if}
				{if $info.type==textarea}style="width:300px;height:60px;"{/if} />
			</div>
			<div id="{$name}1"  {if isset($info.searchStyle) && $info.searchStyle==9}style="display: block"{else}style="display: none"{/if}>
				<input type="{$info.type}" name="{$name}1" class="form_text"
				{if isset($info.value1)}value="{$info.value1}"{/if}
				{if $info.type==textarea}style="width:300px;height:60px;"{/if} />
			</div></td>
			{/if}

		</tr>
		{/foreach}
		<tr class="form_buttons">
			<td colspan="3">
			<div>
				<input type="submit" class="button-primary" name="send_button" value="Suchen">
			</div>
			<div>
				<a class="button-secondary" href="http://{$SITE_URL}/ngsadmin/{$path}">Abbrechen</a>
			</div>
			<input name="path" type="hidden" value="{$path}" />
			<input name="mapperName" type="hidden" value="{$mapperName}" />
			</td>
		</tr>
	</table>
</form>
