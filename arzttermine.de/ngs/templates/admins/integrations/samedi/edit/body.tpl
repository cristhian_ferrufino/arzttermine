{if isset($error)}
	<div id="flash" class="flash">
		<div class="error" style="left: 0px;">
			Invalid Entries.
			<a class="hide" style="cursor: pointer" onclick="document.getElementById('flash').style.display='none'">X</a>
		</div>
	</div>
{/if}
<div id='content-right'>
	<form enctype="multipart/form-data" name="editform" action='//{$SITE_URL}/dyn/admins/do_edit_entry' method="post">
		<table class="widefat">
			<tr class="columns">
				<th colspan="2">Eintrag bearbeiten </th>
			</tr>
			<input type="hidden" name="id" value="{$samediData->id}" />
			{foreach from=$formInfo key=name item=info}
			<tr class="search_key">
				<th>
					{$info.name}
				</th>
				<td>
					{if isset($info.type)}
						{if $info.type==select}
							<{$info.type} name="{$name}">
								{if not empty ($selectDisplayValues.$name)}
									{foreach from=$selectDisplayValues.$name key=option item=optionValue}
										<option value="{$option}">{$optionValue}</option>
									{/foreach}
								{/if}
							</{$info.type}>
						{elseif $info.type==textarea}
							<input type="{$info.type}" class="form_textarea" style="width:300px;height:60px;" name="{$name}" value="{$samediData->$name}"/>
						{else}
							{if isset($error) AND $name=='firstName' AND $error=='firstName'}
								<input type="{$info.type}" class="form_text input_error" size="50" name="{$name}"/>
								<div class="field_error">Firstname is required.</div>
							{elseif isset($error) AND $name=='lastName' AND $error=='lastName'}
								<input type="{$info.type}" class="form_text input_error" size="50" name="{$name}"/>
								<div class="field_error">Lastname is required.</div>
							{elseif isset($error) AND $name=='userId' AND $error=='userId'}
								<input type="{$info.type}" class="form_text input_error" size="50" name="{$name}"/>
								<div class="field_error">UserId is required.</div>
							{elseif isset($error) AND $name=='locationId' AND $error=='locationId'}
								<input type="{$info.type}" class="form_text input_error" size="50" name="{$name}"/>
								<div class="field_error">LocationId is required.</div>
							{else}
								<input type="{$info.type}" class="form_text" size="50" name="{$name}" value="{$samediData->$name}"/>
							{/if}
						{/if}
					{else}
						{$samediData->$name}
					{/if}
					
				</td>
			</tr>
			{/foreach}
			<tr class="form_buttons">
				<td colspan="2">
					<div><input type="submit" class="button-primary" name="send_button" value="Speichern"></div>
					<div><a class="button-secondary" href="http://{$SITE_URL}/ngsadmin/{$path}">Abbrechen</a></div>
					<input name="path" type="hidden" value="{$path}" />
					<input name="mapperName" type="hidden" value="{$mapperName}" />
					<input name="updatedBy" type="hidden" value="{$updatedBy}" />
				</td>
			</tr>
		</table>
	</form>
</div>