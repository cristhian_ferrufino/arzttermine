<div id='content-right'>
	<form enctype="multipart/form-data" name="editform" action='http://{$SITE_URL}/dyn/admins/do_delete_entry' method="post">
		<table class="widefat">
			<tr class="headers">
				<th colspan="15">Soll dieser Eintrag wirklich gelöscht werden?</th>
			</tr>
			<input type="hidden" name="SamediDatas->id" value="{$samediData->id}" />
			{foreach from=$formInfo key=name item=info}
			<tr class="search_key">
				<th>
					{$info.name}
				</th>
				<td>
					{$samediData->$name}
				</td>
			</tr>
			{/foreach}
			<tr class="form_buttons">
				<td colspan="2">
					<div><input type="submit" class="button-primary" name="send_button" value="Ok"></div>
					<div><a class="button-secondary" href="http://{$SITE_URL}/ngsadmin/{$path}">Abbrechen</a></div>
					<input name="path" type="hidden" value="{$path}" />
				</td>
			</tr>
		</table>
	</form>
</div>