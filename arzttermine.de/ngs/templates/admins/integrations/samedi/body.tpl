<table class="widefat">
	<tbody>
		<tr class="columns">
			
			<th class="nowrap">
				<a href="http://{$SITE_URL}/ngsadmin/integrations/samedi/newentry">
					<img class="button-action" title="Neuer Eintrag" src="/static/adm/document_new.png">
				</a>
				<a href="http://{$SITE_URL}/ngsadmin/integrations/samedi/search">
					<img class="button-action" title="Suchen" src="/static/adm/search.png">
				</a>
			</th>

			{foreach from=$columns key=columnName item=columnTitle}
			
			<th class="nowrap">
				{assign var=columnNameUpperCase value=ucfirst({$columnName})}
						{if !isset($direction)}
							<a title="Order by" href="http://{$SITE_URL}/ngsadmin/integrations/samedi?order_by=samediDatas{$columnNameUpperCase}&direction=asc{if isset($requestString)}{$requestString}{/if}"> {$columnTitle}</a>
							
						{elseif $direction==asc and isset($orderBy) and $orderBy=="samediDatas{$columnNameUpperCase}"}
							<a title="Order by" href="http://{$SITE_URL}/ngsadmin/integrations/samedi?order_by=samediDatas{$columnNameUpperCase}&direction=desc{if isset($requestString)}{$requestString}{/if}"> {$columnTitle}</a>
						{elseif $direction==desc and isset($orderBy) and $orderBy=="samediDatas{$columnNameUpperCase}"}
							<a title="Order by" href="http://{$SITE_URL}/ngsadmin/integrations/samedi?order_by=samediDatas{$columnNameUpperCase}&direction=asc{if isset($requestString)}{$requestString}{/if}"> {$columnTitle}</a>
						{elseif isset($orderBy) and  $orderBy!="samediDatas{$columnNameUpperCase}"}
							<a title="Order by" href="http://{$SITE_URL}/ngsadmin/integrations/samedi?order_by=samediDatas{$columnNameUpperCase}&direction=asc{if isset($requestString)}{$requestString}{/if}"> {$columnTitle}</a>
						{/if} 
						
						{if isset($orderBy) and $orderBy=="samediDatas{$columnNameUpperCase}"}
							{if $direction==asc}
								<img class="direction" src="/static/adm/asc.gif">
							{elseif $direction==desc}
								<img class="direction" src="/static/adm/desc.gif">
							{/if}
						{/if} 
			{/foreach}
		</tr>

		{foreach from=$rows item=row}
		{assign var="locations" value=$row->getLocationDtos()}
		{assign var="samediDatas" value=$row->getSamediDatasDtos()}
		{section name=data loop=$samediDatas}
		
		<tr class="alternate">
			<td class="nowrap">
				<a href="http://{$SITE_URL}/ngsadmin/integrations/samedi/edit?samedi_data_id={$samediDatas[data]->id}">
					<img class="button-action" title="Eintrag bearbeiten" src="/static/adm/document_edit.png">
				</a>
				<a href="http://{$SITE_URL}/ngsadmin/integrations/samedi/delete?samedi_data_id={$samediDatas[data]->id}">
					<img class="button-action" title="Eintrag löschen" src="/static/adm/delete.png">
				</a>
			</td>
			<td class="nowrap">
				{$samediDatas[data]->id}
			</td>
			<td class="nowrap">
				<a target="_blank" href="/arzt/{$row->slug}">{$row->title} {$row->firstName} {$row->lastName}</a>
			</td>
			<td class="nowrap">
				<a target="_blank" href="/praxis/{$locations[data]->slug}">{$locations[data]->name}</a>
			</td>
			<td class="nowrap">
				{$samediDatas[data]->categoryId}
			</td>
			<td class="nowrap">
				{$samediDatas[data]->practiceId}
			</td>
			<td class="nowrap">
				{assign var=createdBy value=$samediDatas[data]->getCreatedByDto()}
				{if isset($createdBy->id) and $createdBy->getId()!=null}
					{$createdBy->firstName} {$createdBy->lastName}
				{else}
				n/a
				{/if}
			</td>
		</tr>
		
		
		{/section}
		{/foreach}
		
		<tr class="columns">
			
			<th class="nowrap">
				<a href="http://{$SITE_URL}/ngsadmin/integrations/samedi/newentry">
					<img class="button-action" title="Neuer Eintrag" src="/static/adm/document_new.png">
				</a>
				<a href="http://{$SITE_URL}/ngsadmin/integrations/samedi/search">
					<img class="button-action" title="Suchen" src="/static/adm/search.png">
				</a>
			</th>

			{foreach from=$columns key=columnName item=columnTitle}
			
			<th class="nowrap">
				{assign var=columnNameUpperCase value=ucfirst({$columnName})}
						{if !isset($direction)}
							<a title="Order by" href="http://{$SITE_URL}/ngsadmin/integrations/samedi?order_by=samediDatasId&direction=asc{if isset($requestString)}{$requestString}{/if}"> {$columnTitle}</a>
							
						{elseif $direction==asc and isset($orderBy) and $orderBy=="samediDatas{$columnNameUpperCase}"}
							<a title="Order by" href="http://{$SITE_URL}/ngsadmin/integrations/samedi?order_by=samediDatas{$columnNameUpperCase}&direction=desc{if isset($requestString)}{$requestString}{/if}"> {$columnTitle}</a>
						{elseif $direction==desc and isset($orderBy) and $orderBy=="samediDatas{$columnNameUpperCase}"}
							<a title="Order by" href="http://{$SITE_URL}/ngsadmin/integrations/samedi?order_by=samediDatas{$columnNameUpperCase}&direction=asc{if isset($requestString)}{$requestString}{/if}"> {$columnTitle}</a>
						{elseif isset($orderBy) and  $orderBy!="samediDatas{$columnNameUpperCase}"}
							<a title="Order by" href="http://{$SITE_URL}/ngsadmin/integrations/samedi?order_by=samediDatas{$columnNameUpperCase}&direction=asc{if isset($requestString)}{$requestString}{/if}"> {$columnTitle}</a>
						{/if} 
						
						{if isset($orderBy) and $orderBy=="samediDatas{$columnNameUpperCase}"}
							{if $direction==asc}
								<img class="direction" src="/static/adm/asc.gif">
							{elseif $direction==desc}
								<img class="direction" src="/static/adm/desc.gif">
							{/if}
						{/if} 
			{/foreach}
		</tr>
	</tbody>
</table>