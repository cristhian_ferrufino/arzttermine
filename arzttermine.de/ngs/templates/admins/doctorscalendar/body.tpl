<script src="{$STATIC_PATH}/ngs/js/admin/doctorscalendar/body.js"></script>
<form autocomplete="off">
    <select id="f_selected_integration">
        {html_options values=$ns.integrationsIdsArray selected=$ns.selectedIntegrationId output=$ns.integrationsNamesArray}
    </select>
    <select id="f_selected_location" style="max-width:200px">
        {html_options values=$ns.locationsIds selected=$ns.selectedLocationId output=$ns.locationsNames}
    </select>
    <select id="f_selected_doctor" style="max-width:200px">
        {html_options values=$ns.doctorsIds selected=$ns.selectedDoctorId output=$ns.doctorsNames}
    </select>
	<br/><label for="search_location">Search Location By Name: </label><input id="search_location" />
</form>
{if isset($ns.selectedDoctorId)}
    {nest ns=calendar}
{/if}
{literal}
	<script>
    	var availableTags = {/literal}{$ns.locationsNamesIdsJson}{literal};
	</script>
{/literal}
<style>
  .ui-autocomplete {
    max-height: 200px;
    overflow-y: auto;
    /* prevent horizontal scrollbar */
    overflow-x: hidden;
  }
  /* IE 6 doesn't support max-height
   * we use height instead, but this forces the menu to always be this tall
   */
  * html .ui-autocomplete {
    height: 200px;
  }
</style>