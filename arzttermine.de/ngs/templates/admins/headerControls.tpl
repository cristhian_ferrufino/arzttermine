<!-- NGS Theme Styles -->
{if $ns.contentLoad!='login'}
<link rel="stylesheet" type="text/css" href="{$STATIC_PATH}/ngs/css/admins/style.css" />
{else}
<link rel="stylesheet" type="text/css" href="{$STATIC_PATH}/ngs/css/admins/login.css" />
{/if}
{include "main/headerControls.tpl"}

<script>

function _sendRequest(_url, postData, callBack) {
	 $.ajax({
	 url : _url,
	 type : 'POST',
	 data : postData,
	 context : document.body
	 }).done( function(data) {
	 callBack(data,postData);
	 });
	}
</script>