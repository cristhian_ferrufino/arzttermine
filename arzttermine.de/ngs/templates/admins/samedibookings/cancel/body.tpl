<div id='content-right'>
	<form enctype="multipart/form-data" name="editform" action='http://{$SITE_URL}/dyn/admins/do_cancel' method="post">
		<table class="widefat">
			<tr class="headers">
				<th colspan="15"></th>
			</tr>
			<input type="hidden" name="id" value="{$booking->id}" />
			{foreach from=$formInfo key=name item=info}
			<tr class="search_key">
				<th>
					{$info.name}
				</th>
				<td>
					{if $name==userId}
						{assign var="user" value=$booking->getUser()}
						{if isset($user)}
						{$user->title} {$user->firstName} {$user->lastName}
						{else}
						n/a
						{/if}
						{continue}
					{/if}
					
					{if $name==locationId}
						{assign var="location" value=$booking->getLocation()}
						{if isset($location)}
						{$location->name}
						{else}
						n/a
						{/if}
						{continue}
					{/if}
					
					{if $name==createdBy}
						{assign var="creator" value=$booking->creator}
						{if isset($creator)}
						{$creator->firstName} {$creator->lastName}
						{/if}
						{continue}
					{/if}
					
					{if not empty ($selectDisplayValues.$name)}
						{$selectDisplayValues.$name[{$booking->$name}]}
					{else}
						{$booking->$name}
					{/if}
					
					
				</td>
			</tr>
			{/foreach}
			<tr class="form_buttons">
				<td colspan="2">
					<div><input type="submit" class="button-primary" name="send_button" value="Speichern"></div>
					<div><a class="button-secondary" href="http://{$SITE_URL}/ngsadmin/samedibookings">Abbrechen</a></div>
				</td>
			</tr>
		</table>
	</form>
</div>