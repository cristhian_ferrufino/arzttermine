{if isset($bookingDto)}
<div id="fancybox-overlay" style="background-color: rgb(119, 119, 119); opacity: 0.7; cursor: pointer; height: 1294px;display: block;"></div>
<div id="fancybox-wrap" style="width: 644px; height: auto; top: 20px; left: 603px; display: block;">
	<div id="fancybox-outer">
		<div id="fancybox-bg-n" class="fancybox-bg"></div>
		<div id="fancybox-bg-ne" class="fancybox-bg"></div>
		<div id="fancybox-bg-e" class="fancybox-bg"></div>
		<div id="fancybox-bg-se" class="fancybox-bg"></div>
		<div id="fancybox-bg-s" class="fancybox-bg"></div>
		<div id="fancybox-bg-sw" class="fancybox-bg"></div>
		<div id="fancybox-bg-w" class="fancybox-bg"></div>
		<div id="fancybox-bg-nw" class="fancybox-bg"></div>
		<div id="fancybox-content" style="border-width: 10px; width: 624px; height: auto;">
			<div style="width:auto;height:auto;overflow: auto;position:relative;">
			<title> - </title>
				<div id="custom-doc">
					<div id="bd" class="iframe_full">
						<div id="content-main">
							<form method="post" action="/ngsadmin/samedibookings" name="editform" enctype="multipart/form-data">
							
							<input type="hidden" name="id" value="{$bookingDto->id}">
							
							<table class="widefat">
								<tr class="columns">
									<th colspan="2">Bestätigungsmail</th>
								</tr>
								<tr>
									<th>Buchungs-ID</th>
									<td>{$bookingDto->id}</td>
								</tr>
								<tr id="form_row_name">
									<th>Name</th>
									<td>
										<input type="text" id="form_data_name" class="form_text" name="form_data[name]" value="{$bookingDto->getFirstName()} {$bookingDto->getLastName()}" size="50">
									</td>
								</tr>
								<tr id="form_row_email">
									<th>E-Mail</th>
									<td>
										<input type="text" id="form_data_email" class="form_text" name="form_data[email]" value="{$bookingDto->getEmail()}" size="50">
									</td>
								</tr>
								<tr id="form_row_subject">
									<th>Subject</th>
									<td>
										{assign var="location" value=$bookingDto->getLocation()}
										{assign var="doctor" value=$bookingDto->getUser()}
										<input type="text" id="form_data_subject" class="form_text" name="form_data[subject]" size="50"
										value=" Terminbestätigung für {$doctor->getTitle()} {$doctor->getFirstName()} {$doctor->getLastName()}">
									</td>
								</tr>
								<tr id="form_row_body">
									<th>Mailtext</th>
									<td>
										<textarea id="form_data_body" class="form_textarea" name="form_data[body]" style="width:500px;height:300px;">
{include "admins/samedibookings/email/email_content.tpl"}
										</textarea>
									</td>
								</tr>
								<tr class="form_buttons">
									<td colspan="2">
										<div><input type="submit" value="Abschicken" name="send_mail" class="button-primary"></div>
									</td>
								</tr>
							</table>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<a id="fancybox-close" style="display: inline;" href='{$SITE_PATH}/ngsadmin/samedibookings'></a>
	</div>
</div>
{/if}
