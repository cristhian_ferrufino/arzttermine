{assign var="location" value=$ns.bookingDto->getLocation()}
{assign var="doctor" value=$ns.bookingDto->getUser()}
Sehr geehrte {$ns.genderArray[$ns.bookingDto->getGenderText()]} {$ns.bookingDto->getFirstName()} {$ns.bookingDto->getLastName()},
											
Vielen Dank für die Terminbuchung über Arzttermine.de.
Hiermit bestätigen wir Ihren Termin in der Praxis:
											
{$doctor->getTitle()} {$doctor->getFirstName()} {$doctor->getLastName()}
{$location->getStreet()} {$location->getZip()} {$location->getCity()}
											
Datum: {$ns.appointmentWeekDay} {date("d.m.Y",strtotime($ns.bookingDto->getAppointmentStartAt()))}
Uhrzeit: {date("H:i",strtotime($ns.bookingDto->getAppointmentStartAt()))} Uhr
											
Sollten Sie diesen Termin nicht wahrnehmen können, bitte wir Sie um eine Absage. 
Antworten Sie dafür einfach auf diese Email mit dem Betreff "Absage" oder rufen Sie uns 
kostenfrei unter der Nummer 08002222133 an.
							
Mit freundlichen Grüßen,
Ihr Arzttermine.de Team
support@arzttermine.de