<table class="widefat">
		<tbody>
			<tr class="columns">
				<th class="nowrap">
					<a href="http://{$SITE_URL}/ngsadmin/samedibookings/newentry">
						<img class="button-action" title="Neuer Eintrag" src="/static/adm/document_new.png">
					</a>
					<a href="http://{$SITE_URL}/ngsadmin/samedibookings/search">
						<img class="button-action" title="Suchen" src="/static/adm/search.png">
					</a>
					<a {*href="http://{$SITE_URL}/dyn/admins/do_save_csv"*} onclick="alert('not implemented')">
						<img class="button-action" title="Liste als CSV herunterladen" src="/static/adm/download.gif">
					</a>
				</th>
				
				{foreach from=$columns key=columnName item=columnTitle} 
					<th class="nowrap">
						
							{assign var=tableName value="samedi_bookings"}
						
							
						{assign var=columnNameUpperCase value=ucfirst({$columnName})}
						{if !isset($direction)}
							<a title="Order by" href="http://{$SITE_URL}/ngsadmin/samedibookings?order_by={$tableName}{$columnNameUpperCase}&direction=asc{if isset($requestString)}{$requestString}{/if}"> {$columnTitle}</a>
						{elseif $direction==asc and isset($orderBy) and $orderBy=="bookings{$columnNameUpperCase}"}
							<a title="Order by" href="http://{$SITE_URL}/ngsadmin/samedibookings?order_by={$tableName}{$columnNameUpperCase}&direction=desc{if isset($requestString)}{$requestString}{/if}"> {$columnTitle}</a>
						{elseif $direction==desc and isset($orderBy) and $orderBy=="bookings{$columnNameUpperCase}"}
							<a title="Order by" href="http://{$SITE_URL}/ngsadmin/samedibookings?order_by={$tableName}{$columnNameUpperCase}&direction=asc{if isset($requestString)}{$requestString}{/if}"> {$columnTitle}</a>
						{elseif isset($orderBy) and $orderBy!="bookings{$columnNameUpperCase}"}
							<a title="Order by" href="http://{$SITE_URL}/ngsadmin/samedibookings?order_by={$tableName}{$columnNameUpperCase}&direction=asc{if isset($requestString)}{$requestString}{/if}"> {$columnTitle}</a>
						{/if}
						
						{if isset($orderBy) and $orderBy=="{$columnNameUpperCase}"}
							{if $direction==asc}
								<img class="direction" src="/static/adm/asc.gif">
							{elseif $direction==desc}
								<img class="direction" src="/static/adm/desc.gif">
							{/if}
						{/if} 
					</th>
				{/foreach} 
			</tr>
			
			
			{foreach from=$rows item=row}
			
			
				<tr class="alternate">
		
					<td class="nowrap">
						<a href="http://{$SITE_URL}/ngsadmin/samedibookings/edit?booking_id={$row->id}">
							<img class="button-action" title="Eintrag bearbeiten" src="/static/adm/document_edit.png">
						</a>
						<a href="http://{$SITE_URL}/ngsadmin/samedibookings/delete?booking_id={$row->id}">
							<img class="button-action" title="Eintrag löschen" src="/static/adm/delete.png">
						</a>						
					</td>
					{foreach from=$columns key=columnName item=k}	
					<td class="nowrap">	
						
						{assign var="fname" value="get`$columnName|ucfirst`"}						
						
						{assign var="fieldValue" value = $row->$fname()}
						
						{if $columnName=='eventTypeId'}
							{assign var="fieldValue" value = $row->getSamediTreatmentTypeDto()->getNameDe()}
						{/if}
					
						
						{if not empty($ns.fieldsDisplayValues)}
							{if isset($ns.fieldsDisplayValues.$columnName)}
								{if isset($ns.fieldsDisplayValues.$columnName.$fieldValue)}
									{$ns.fieldsDisplayValues.$columnName.$fieldValue}									
									{continue}
								{/if}								
							{/if}									
						{/if}					
						{$fieldValue}			
						</td> 
					{/foreach}
					
				</tr>
			{/foreach}
			
			<tr class="columns">
				<th class="nowrap">
					<a href="http://{$SITE_URL}/ngsadmin/samedibookings/newentry">
						<img class="button-action" title="Neuer Eintrag" src="/static/adm/document_new.png">
					</a>
					<a href="http://{$SITE_URL}/ngsadmin/samedibookings/search">
						<img class="button-action" title="Suchen" src="/static/adm/search.png">
					</a>
					<a {*href="http://{$SITE_URL}/dyn/admins/do_save_csv"*} onclick="alert('not implemented')">
						<img class="button-action" title="Liste als CSV herunterladen" src="/static/adm/download.gif">
					</a>
				</th>
				
				{foreach from=$columns key=columnName item=columnTitle} 
					<th class="nowrap">
							
						{assign var=columnNameUpperCase value=ucfirst({$columnName})}
						{if !isset($direction)}
							<a title="Order by" href="http://{$SITE_URL}/ngsadmin/samedibookings?order_by={$columnNameUpperCase}&direction=asc{if isset($requestString)}{$requestString}{/if}"> {$columnTitle}</a>
						{elseif $direction==asc and isset($orderBy) and $orderBy=="bookings{$columnNameUpperCase}"}
							<a title="Order by" href="http://{$SITE_URL}/ngsadmin/samedibookings?order_by={$columnNameUpperCase}&direction=desc{if isset($requestString)}{$requestString}{/if}"> {$columnTitle}</a>
						{elseif $direction==desc and isset($orderBy) and $orderBy=="bookings{$columnNameUpperCase}"}
							<a title="Order by" href="http://{$SITE_URL}/ngsadmin/samedibookings?order_by={$columnNameUpperCase}&direction=asc{if isset($requestString)}{$requestString}{/if}"> {$columnTitle}</a>
						{elseif isset($orderBy) and $orderBy!="bookings{$columnNameUpperCase}"}
							<a title="Order by" href="http://{$SITE_URL}/ngsadmin/samedibookings?order_by={$columnNameUpperCase}&direction=asc{if isset($requestString)}{$requestString}{/if}"> {$columnTitle}</a>
						{/if}
						
						{if isset($orderBy) and $orderBy=="{$columnNameUpperCase}"}
							{if $direction==asc}
								<img class="direction" src="/static/adm/asc.gif">
							{elseif $direction==desc}
								<img class="direction" src="/static/adm/desc.gif">
							{/if}
						{/if} 
					</th>
				{/foreach} 
			</tr>
				
		</tbody>
</table>