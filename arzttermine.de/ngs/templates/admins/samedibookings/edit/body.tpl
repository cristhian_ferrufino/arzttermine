{if isset($error)}
	<div class="flash" id="flash">
		<div class="error" style="left: 0px;">
			Invalid Entries.
			<a class="hide" style="cursor: pointer" onclick="document.getElementById('flash').style.display='none'">X</a>
		</div>
	</div>
{/if}
<div id='content-right'>
	<form enctype="multipart/form-data" name="editform" action='http://{$SITE_URL}/dyn/admins/do_edit_entry' method="post">
		<table class="widefat">
			<tr class="columns">
				<th colspan="2">Eintrag bearbeiten </th>
			</tr>
			<input type="hidden" name="id" value="{$booking->id}" />
			{foreach from=$formInfo key=name item=info}
			<tr class="search_key">
				<th>
					{$info.name}
				</th>
				<td>
				{if isset($info.dto) and $info.dto==booking}
					{if isset($info.type)}
						{if $info.type==select}
							<{$info.type} name="{$name}">
								{if not empty ($selectDisplayValues.$name)}
									{if $name==backendMarkerClass}
										<option value="" selected="selected"></option>
									{/if}
									{foreach from=$selectDisplayValues.$name key=option item=optionValue}
										
										{if $booking->$name==$option}
											<option value="{$option}" selected="selected">{$optionValue}</option>
										{else}
											<option value="{$option}" >{$optionValue}</option>
										{/if}
									{/foreach}
								{/if}
							</{$info.type}>
						{elseif $info.type==textarea}
							<input type="{$info.type}" class="form_textarea" style="width:300px;height:60px;" value="{$booking->$name}" name="{$name}"/>
						{else}
							{if isset($error) AND $name=='firstName' AND $error=='firstName'}
								<input type="{$info.type}" class="form_text input_error" size="50" name="{$name}"/>
								<div class="field_error">Firstname is required.</div>
							{elseif isset($error) AND $name=='lastName' AND $error=='lastName'}
								<input type="{$info.type}" class="form_text input_error" size="50" name="{$name}"/>
								<div class="field_error">Lastname is required.</div>
							{elseif isset($error) AND $name=='userId' AND $error=='userId'}
								<input type="{$info.type}" class="form_text input_error" size="50" name="{$name}"/>
								<div class="field_error">UserId is required.</div>
							{elseif isset($error) AND $name=='locationId' AND $error=='locationId'}
								<input type="{$info.type}" class="form_text input_error" size="50" name="{$name}"/>
								<div class="field_error">LocationId is required.</div>
							{else}
								<input type="{$info.type}" class="form_text" size="50" value="{$booking->$name}" name="{$name}"/>
							{/if}							
					  	{/if}
					{else}
						{if not empty ($selectDisplayValues.$name)}	
							{$selectDisplayValues.$name[$booking->$name]}
						{else}
							{$booking->$name}
						{/if}
					{/if}
				
					{if isset($info.value)}
						{$info.value}
		
					{/if}
				{/if}
				
				{if $name==treatmenttypeId}
					{assign var="treatmentType" value=$booking->getTreatmentType()}
					{if isset($treatmentType)}
						{$treatmentType->name}
					{/if}
				{/if}
				
				{if $name==userId}
					{assign var="user" value=$booking->getUser()}
					{if isset($user)}
					<a target="_blank" href="{$SITE_PATH}/arzt/{$user->slug}">{$user->title} {$user->firstName} {$user->lastName}</a>
					{else}
					n/a
					{/if}
				{/if}
				
				{if $name==locationId}
					{assign var="location" value=$booking->getLocation()}
					{if isset($location)}
					<a target="_blank" href="{$SITE_PATH}/praxis/{$location->slug}">{$location->name}</a>
					{else}
					n/a
					{/if}
				{/if}
				
				{if $name==createdBy}
					{assign var="creator" value=$booking->creator}
					{if isset($creator)}
					{$creator->firstName} {$creator->lastName}
					{/if}
				{/if}
				
				{if $name==updatedBy}
					{assign var="update" value=$booking->update}
					{if isset($update)}
					{$update->firstName} {$update->lastName}
					{else}
					n/a
					{/if}
				{/if}
				</td>
			</tr>
			{/foreach}
			<tr class="form_buttons">
				<td colspan="2">
					<div><input type="submit" class="button-primary" name="send_button" value="Speichern"></div>
					<div><a class="button-secondary" href="http://{$SITE_URL}/ngsadmin/samedibookings">Abbrechen</a></div>
					<input name="path" type="hidden" value="{$path}" />
					<input name="mapperName" type="hidden" value="{$mapperName}" />
					<input name="updatedBy" type="hidden" value="{$updatedBy}" />
				</td>
			</tr>
		</table>
	</form>
</div>