<div id='content-right'>
	<form enctype="multipart/form-data" name="editform" action='http://{$SITE_URL}/dyn/admins/do_delete_entry' method="post">
		<table class="widefat">
			<tr class="headers">
				<th colspan="15">Soll dieser Eintrag wirklich gelöscht werden?</th>
			</tr>
			<input type="hidden" name="Bookings->id" value="{$booking->id}" />
			{foreach from=$formInfo key=name item=info}
			<tr class="search_key">
				<th>
					{$info.name}
				</th>
				<td>
					{if not empty ($selectDisplayValues.$name)}
						{$selectDisplayValues.$name[{$booking->$name}]}
					{else}
						{$booking->$name}
					{/if}
					
					{if $name==userId}
						{assign var="user" value=$booking->getUser()}
						{if isset($user)}
						{$user->title} {$user->firstName} {$user->lastName}
						{else}
						n/a
						{/if}
					{/if}
					
					{if $name==locationId}
						{assign var="location" value=$booking->getLocation()}
						{if isset($location)}
						{$location->name}
						{else}
						n/a
						{/if}
					{/if}
					
					{if $name==createdBy}
						{assign var="creator" value=$booking->creator}
						{if isset($creator)}
						{$creator->firstName} {$creator->lastName}
						{/if}
					{/if}
				</td>
			</tr>
			{/foreach}
			<tr class="form_buttons">
				<td colspan="2">
					<div><input type="submit" class="button-primary" name="send_button" value="Ok"></div>
					<div><a class="button-secondary" href="http://{$SITE_URL}/ngsadmin/samedibookings">Abbrechen</a></div>
					<input name="path" type="hidden" value="{$path}" />
				</td>
			</tr>
		</table>
	</form>
</div>