{if isset($doctorsDtos)}
	<form action="http://{$SITE_URL}/dyn/admins/do_import_integration_doctors" method="post">
		<table class="widefat">
			<tbody>
				<tr class="columns">
					<th class="nowrap"></th>

					{foreach from=$columns item=columnName}
						<th class="nowrap"> {if $columnName=="integrationId"}
							{assign var=tableName value="locations"}
							{else}
								{assign var=tableName value="bookings"}
								{/if}

									{assign var=columnNameUpperCase value=ucfirst({$columnName})}
	{if !isset($direction)} <a title="Order by" href="http://{$SITE_URL}/ngsadmin/samedibookings?order_by={$tableName}{$columnNameUpperCase}&direction=asc{if isset($requestString)}{$requestString}{/if}"> {$columnName}</a> {elseif $direction==asc and isset($orderBy) and $orderBy=="bookings{$columnNameUpperCase}"} <a title="Order by" href="http://{$SITE_URL}/ngsadmin/samedibookings?order_by={$tableName}{$columnNameUpperCase}&direction=desc{if isset($requestString)}{$requestString}{/if}"> {$columnName}</a> {elseif $direction==desc and isset($orderBy) and $orderBy=="bookings{$columnNameUpperCase}"} <a title="Order by" href="http://{$SITE_URL}/ngsadmin/samedibookings?order_by={$tableName}{$columnNameUpperCase}&direction=asc{if isset($requestString)}{$requestString}{/if}"> {$columnName}</a> {elseif isset($orderBy) and $orderBy!="bookings{$columnNameUpperCase}"} <a title="Order by" href="http://{$SITE_URL}/ngsadmin/samedibookings?order_by={$tableName}{$columnNameUpperCase}&direction=asc{if isset($requestString)}{$requestString}{/if}"> {$columnName}</a> {/if}

	{if isset($orderBy) and $orderBy=="bookings{$columnNameUpperCase}"}
{if $direction==asc} <img class="direction" src="/static/adm/asc.gif"> {elseif $direction==desc} <img class="direction" src="/static/adm/desc.gif"> {/if}
{/if} </th>
{/foreach}
</tr>

{foreach from=$doctorsDtos item=doctorDto}			
	{assign var="locationsDtos" value=$doctorDto->getLocationDtos()}
	{assign var="samediDataDtos" value=$doctorDto->getSamediDatasDtos()}
	{assign var="samediEventTypesCachesDtos" value=$doctorDto->getSamediEventTypesCachesDtos()}

	{assign var="locationsCount" value=$locationsDtos|@count}			
	{for $i=0 to $locationsCount-1}

		<tr 
		{if isset($ns.samediDatasDtosInSpecificArrayMap.{$samediDataDtos.$i->getPracticeId()}.{$samediDataDtos.$i->getCategoryId()})} style="background:#FFAA55"{/if}
		>				
		<td>

			{if (not isset($ns.afterInsert)) and isset($doctorDto->lastName)}
				<input name="selected_doctors[{$samediDataDtos.$i->getPracticeId()}^{$samediDataDtos.$i->getCategoryId()}]" type="checkbox" />
			{/if}
		</td>
		<td>{if isset($doctorDto->title)}{$doctorDto->getTitle()}{/if} {$doctorDto->getFirstName()} {if isset($doctorDto->lastName)}{$doctorDto->getLastName()}{/if}</td>
		<td> {$locationsDtos.$i->getName()} </td>
		<td> {$samediDataDtos.$i->getPracticeId()} </td>
		<td> {$samediDataDtos.$i->getCategoryId()} </td>
		<td> {$doctorsManager->getEventTypesNamesString($samediEventTypesCachesDtos.$i->getEventTypes())} </td>
		<td>
			{if isset($ns.samediDatasDtosInSpecificArrayMap.{$samediDataDtos.$i->getPracticeId()}.{$samediDataDtos.$i->getCategoryId()})}
				{assign var="samedi_data_dto" value=$ns.samediDatasDtosInSpecificArrayMap.{$samediDataDtos.$i->getPracticeId()}.{$samediDataDtos.$i->getCategoryId()}}
				{assign var="isDoctorSamediTTsSetuped" value=$samediTreatmentTypesManager->isDoctorSamediTTsSetuped($samediDataDtos.$i->getPracticeId(), $samediDataDtos.$i->getCategoryId())}
				{assign var="isDoctorSamediTTsParentIdsSetuped" value=$samediTreatmentTypesManager->isDoctorSamediTTsParentIdsSetuped($samediDataDtos.$i->getPracticeId(), $samediDataDtos.$i->getCategoryId())}
				{assign var="isDoctorTTsSetupedInDoctorTtTable" value=$samediTreatmentTypesManager->isDoctorTTsSetupedInDoctorTtTable($samedi_data_dto->getUserId(),$samediDataDtos.$i->getPracticeId(), $samediDataDtos.$i->getCategoryId())}				
				{if $isDoctorSamediTTsSetuped}
					{if !$isDoctorSamediTTsParentIdsSetuped || !$isDoctorTTsSetupedInDoctorTtTable}
						<a class="update_doctor_tts_links" user_id="{$samedi_data_dto->getUserId()}" 
						   title="{if !$isDoctorSamediTTsParentIdsSetuped}samedi tt parent ids is not setuped for this doctor!{/if}&#013;{if !$isDoctorTTsSetupedInDoctorTtTable}doctors tts are not setupped in doctors_tt table!{/if}"
						   practice_id="{$samediDataDtos.$i->getPracticeId()}" 
						   category_id="{$samediDataDtos.$i->getCategoryId()}"  
						   href="javascript:void(0);" style="color:red">Set Doctor TTs</a>
					{else}
						<a class="review_doctor_tts_links" user_id="{$samedi_data_dto->getUserId()}" 
						   practice_id="{$samediDataDtos.$i->getPracticeId()}" category_id="{$samediDataDtos.$i->getCategoryId()}"  
						   href="javascript:void(0);" style="color:green">Review Doctor TTs</a>
					{/if} 
				{else}
					<span style='color:red'>please import doctor again to fix the missing TTs in samedi_tt table!</span>
				{/if}
			{/if}
		</td>				
	</tr>
{/for}
{/foreach}

			

		</tbody>
	</table>
	{if (not isset($ns.afterInsert))}
	<p>
	<label for="replace_doctors">Replace existing doctors data</label>
	<input name="replace_doctors" id="replace_doctors" type="checkbox" />
	</p>
	<p>
		<label for="replace_locations">Replace existing locations data</label>
		<input name="replace_locations" id="replace_locations" type="checkbox" />
	</p>

	<p>
	<input type="submit" value="import selected doctors"/>
	</p>
	{/if}
</form>
{/if}
	<script>
		{literal}
	//review doctor treatmenttypes to setup the parent treatmenttypes		
	$('.review_doctor_tts_links').click(function() {		
				var user_id = $(this).attr('user_id');
				var category_id = $(this).attr('category_id');
				var practice_id = $(this).attr('practice_id');
				_sendRequest(site_PATH + '/dyn/admins/do_update_doctor_tt_in_doct_tt_table',
						'review_tts=1'+ '&user_id=' + user_id + '&category_id=' + category_id + '&practice_id=' + practice_id, 
						update_doctor_tt_callback.bind(new Array(user_id, category_id, practice_id)));

			});			
			
			
			$('.update_doctor_tts_links').click(function() {
				var user_id = $(this).attr('user_id');
				var category_id = $(this).attr('category_id');
				var practice_id = $(this).attr('practice_id');
				_sendRequest(site_PATH + '/dyn/admins/do_update_doctor_tt_in_doct_tt_table',
						'user_id=' + user_id + '&category_id=' + category_id + '&practice_id=' + practice_id, update_doctor_tt_callback.bind(new Array(user_id, category_id, practice_id)));
			});
			function update_doctor_tt_callback(data, postParams)
			{


				var postParamsObject = $.unparam(postParams);

				if (data === 'ok')
				{
					$('#id_head_form').trigger('submit');
				}
				else {
					var jsonDec = $.parseJSON(data);
					var samediTTIdAndNamesArray = jsonDec [0];
					var arzTTIdAndNamesArray = jsonDec [1];
					var allMedSpecies = jsonDec [2];
					if (allMedSpecies.length===0){
						alert('Please set the doctor medical specialty!');
						return;
					}
					var html = "";
					for (var i = 0; i < samediTTIdAndNamesArray.length; i++) {
						html += getTTSetupLineHtml(samediTTIdAndNamesArray[i], arzTTIdAndNamesArray);
					}

					var createNewTTHtml = "<div style='clear:both;text-align:right;margin-top:50px'><a id='create_new_tt_link' href='javascript:void(0);'>create new Treatmenttype<a/></div>";
					html += createNewTTHtml;
					$('<div title="Setup Treatmenttypes Relations" id="tt_setup_dialog" style="padding:20px">' + html + '</div>').dialog({
						width: 600,
						height: 500,
						modal: true,
						buttons: [{
								text: "Save",
								click: function() {
									var arzTTAndSamediTTArray = new Array();
									$('.arzTTAndSamediTTUnit').each(function() {
										var samedi_tt_id = $(this).attr('samedi_tt_id');
										var arzt_tt_id = $(this).find("select").val();
										arzTTAndSamediTTArray.push(new Array(samedi_tt_id, arzt_tt_id));
									});
									_sendRequest(site_PATH + '/dyn/admins/do_update_doctor_tt_in_doct_tt_table',
											'user_id=' + postParamsObject.user_id + '&category_id=' +
											postParamsObject.category_id + '&practice_id=' +
											postParamsObject.practice_id + '&set_samedi_tt_parent_ids=1&arz_tt_and_samedi_tt_array=' + JSON.stringify(arzTTAndSamediTTArray), update_tt_callback);
									$(this).dialog("close");
								}
							},
							{
								text: "Cancel",
								click: function() {
									$(this).dialog("close");
								}
							}
						],
						close: function() {
							$(this).remove();
						}
					});
					$('#create_new_tt_link').click(function() {

						var dialogContentHTML = "<div style='padding:20px'>";
						dialogContentHTML += "<p>";
						dialogContentHTML += "<span>status</span>";
						dialogContentHTML += "<select id='ntt_status'><option value=0>draft</option><option value=1>active</option></select>";
						dialogContentHTML += "</p>";
						dialogContentHTML += "<p>";
						dialogContentHTML += "<span>Medical Specialty</span>";
						dialogContentHTML += "<select id='ntt_medspecid'>";
						for (var i = 0; i < allMedSpecies.length; i++) {
							var medId = allMedSpecies[i][0];
							var medName = allMedSpecies[i][1];
							dialogContentHTML += "<option value='" + medId + "'>" + medName + "</option>";
						}
						dialogContentHTML += "</select>";
						dialogContentHTML += "</p>";
						dialogContentHTML += "<p>";
						dialogContentHTML += "<span>slug</span>";
						dialogContentHTML += "<input id='ntt_slug' type='text'/>";
						dialogContentHTML += "</p>";
						dialogContentHTML += "<p>";
						dialogContentHTML += "<span>name_de</span>";
						dialogContentHTML += "<input id='ntt_name_de' type='text'/>";
						dialogContentHTML += "</p>";
						dialogContentHTML += "<p>";
						dialogContentHTML += "<span>name_en</span>";
						dialogContentHTML += "<input id='ntt_name_en' type='text'/>";
						dialogContentHTML += "</p>";
						dialogContentHTML += "</div>";
						//dialogContentHTML += ''
						$('<div title="Create New Treatmenttype">' + dialogContentHTML + '</div>').dialog(
								{
									modal: true,
									width: 500,
									buttons: [{
											text: "Save",
											click: function() {
												_sendRequest(site_PATH + '/dyn/admins/do_create_new_treatmenttype',
														'status=' + $("#ntt_status").val() + '&ntt_medspecid=' + $("#ntt_medspecid").val()
														+ '&ntt_slug=' + $("#ntt_slug").val() + '&ntt_name_de=' + $("#ntt_name_de").val()
														+ '&ntt_name_en=' + $("#ntt_name_en").val(), create_new_tt_callback);
												$(this).dialog("close");
											}
										},
										{
											text: "Cancel",
											click: function() {
												$(this).dialog("close");
											}
										}
									],
									close: function() {
										$(this).remove();
									}
								});
						function create_new_tt_callback()
						{
							$('#tt_setup_dialog').dialog("close");
							_sendRequest(site_PATH + '/dyn/admins/do_update_doctor_tt_in_doct_tt_table',
									'user_id=' + postParamsObject.user_id + '&category_id=' + postParamsObject.category_id + '&practice_id='
									+ postParamsObject.practice_id, update_doctor_tt_callback.bind(new Array(postParamsObject.user_id, postParamsObject.category_id, postParamsObject.practice_id)));
						}
					});
				}
				function update_tt_callback(data)
				{
					if (data === 'ok')
					{
						$('#id_head_form').trigger('submit');
					}
				}
				function getTTSetupLineHtml(samediTTIdandNamePair, artAllTTsIdsNamesArray) {
					var sTTId = samediTTIdandNamePair[0];
					var sTTName = samediTTIdandNamePair[1];
					var sTTParentId = samediTTIdandNamePair[2];
					var ret = "<div class='arzTTAndSamediTTUnit' style='clear:both' samedi_tt_id='" + sTTId + "'>";
					var sTTSpan = "<div style='min-width:300px;max-width:300px;float:left'>" + sTTName + "</div>";
					var arztTTSelect = "<select style='max-width:250px;float:left'>";
					for (var i = 0; i < artAllTTsIdsNamesArray.length; i++) {
						var arzTTId = artAllTTsIdsNamesArray[i][0];
						var arzTTName = artAllTTsIdsNamesArray[i][1];
						arztTTSelect += "<option value='" + arzTTId + "' "+
					(parseInt(sTTParentId)===parseInt(arzTTId)?"selected='selected'":'')
					+">" + arzTTName + "</option>";
					}
					arztTTSelect += "</select>";
					ret += sTTSpan + arztTTSelect + '</div>';
					return  ret;
				}
			}
		{/literal}
	</script>

