<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />        
        <title>Arzttermine.de Backend</title>

        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0" />
        
        {include "admins/headerControls.tpl"}
    </head>
    <body class="js {if $ns.contentLoad!='login'}admin{/if}">
    <script type='text/javascript'>
        var AT = AT || { };

        AT.q=[];
        AT._defer=function(f){
            AT.q.push(f);
        };
    </script>
    
        <div id="page-main">
            <input type="hidden" id="initialLoad" name="initialLoad" value="main" />
            <input type="hidden" id="contentLoad" value="{$ns.contentLoad}" />
            <div class="page-container" >
                {nest ns=content}
            </div>
            <div id="nav-footer">
                <div class="nav left"></div>
                <div class="nav right">
                    <a rel="nofollow" href="/agb">AGB</a> | <a title="Arzttermine Imprint" href="/impressum">Impressum</a>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        {include "admins/footer.tpl"}
    
    <script>
        $(document).ready(function(){
            $.each(AT.q,function(index,f){
                $(f);
            });
        })
    </script>
    </body>
</html>