{if isset($search) and $search=='yes'}{include "admins/search-view-tpl/body.tpl"}{/if}
{block name="admin_table_view_header"}
{if isset($ns.message)}
	<div class="flash" id="flash">
		<div class="error" style="left: 0px;">
			{$ns.message}
			<a class="hide" style="cursor: pointer" onclick="document.getElementById('flash').style.display='none'">X</a>
		</div>
	</div>
{/if}
	
	<table class="widefat tablenav" style="width: 730px">
		
			
		
		<tr class="columns_small">
			<th>Number</th>
			<th>View</th>
		</tr>
		<tr>
			{nest ns=rows_per_page}
		</tr>
		<tr>
			{nest ns=paging}
		</tr>
	</table>
{/block}