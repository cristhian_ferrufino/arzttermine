{block name="admin_table_view"}
	<table class="widefat">
		<tbody>
			<tr class="columns">
	
				<th class="nowrap">
					<a href="">
						<img class="button-action" title="Neuer Eintrag" src="/static/adm/document_new.png">
					</a>
					<a href="">
						<img class="button-action" title="Suchen" src="/static/adm/search.png">
					</a>
				</th>
				
				{foreach from=$columns item=columnTitle} 
					<th class="nowrap"> 
						<a title="Order by" href="/admin/users"> {$columnTitle}
						{*}<img class="direction" src="/static/adm/desc.gif">{*} </a> 
					</th>
				{/foreach} 
			</tr>
			
			
			{foreach from=$rows item=row}
				<tr class="alternate">
						
					<td class="nowrap">
						<a href="">
							<img class="button-action" title="Eintrag bearbeiten" src="/static/adm/document_edit.png">
						</a>
						<a href="">
							<img class="button-action" title="Eintrag löschen" src="/static/adm/delete.png">
						</a>
					</td>
					
					{foreach from=$columns key=columnName item=k}	
					<td class="nowrap">	
							{assign var="fname" value="get`$columnName|ucfirst`"}
							{assign var="fieldValue" value = $row->$fname()}
							{if not empty($ns.fieldsDisplayValues)}
							  {if isset($ns.fieldsDisplayValues.$columnName)}
									{if isset($ns.fieldsDisplayValues.$columnName.$fieldValue)}
										{$ns.fieldsDisplayValues.$columnName.$fieldValue}
										{continue} 
									{/if}								
								{/if}
							{/if}													
						{$fieldValue}
						</td> 
					{/foreach}
					
				</tr>
			{/foreach}
			
			<tr class="columns">
	
				<th class="nowrap">
					<a href="">
						<img class="button-action" title="Neuer Eintrag" src="/static/adm/document_new.png">
					</a>
					<a href="">
						<img class="button-action" title="Suchen" src="/static/adm/search.png">
					</a>
				</th>
				
				{foreach from=$columns item=columnTitle} 
					<th class="nowrap"> 
						<a title="Order by" href="/admin/users"> {$columnTitle}
						{*}<img class="direction" src="/static/adm/desc.gif">{*} </a> 
					</th>
				{/foreach} 
			</tr>
		
		</tbody>
	</table>
{/block}