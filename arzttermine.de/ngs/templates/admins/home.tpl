{include "admins/header.tpl"}
<div class="page-wrapper">
	{if isset($mailStatus)}
	{if $mailStatus=='success'}
	<div class="flash" id="flash">
		<div class="notice" style="display: block;">
			Mail sent successfully.
			<a class="hide" style="cursor: pointer" onclick="document.getElementById('flash').style.display='none'">X</a>
		</div>
	</div>
	{elseif $mailStatus=='error'}
	<div class="flash" id="flash">
		<div class="error" style="left: 0px;">
			Error occured while sending the mail.
			<a class="hide" style="cursor: pointer" onclick="document.getElementById('flash').style.display='none'">X</a>
		</div>
	</div>
	{/if}
	{/if}
	{if isset($newentry)}
	<div class="flash" id="flash">
		<div class="notice" style="display: block;">
			{$tableName} has been successfully created with ID {$newId}.
			<a class="hide" style="cursor: pointer" onclick="document.getElementById('flash').style.display='none'">X</a>
		</div>
	</div>
	{/if}
	{if isset($editentry)}
	<div class="flash" id="flash">
		<div class="notice" style="display: block;">
			Edited successfully.
			<a class="hide" style="cursor: pointer" onclick="document.getElementById('flash').style.display='none'">X</a>
		</div>
	</div>
	{/if}
	{if isset($deleteEntry)}
	<div class="flash" id="flash">
		<div class="notice" style="display: block;">
			Deleted successfully.
			<a class="hide" style="cursor: pointer" onclick="document.getElementById('flash').style.display='none'">X</a>
		</div>
	</div>
	{/if}
	<div id="logo">
		<a title="Home Arzttermine.de Arzt &amp; Zahnarzt Termine Berlin Frankfurt Hamburg Munchen" href=""><img width="216" height="60" alt="Home Arzttermine.de Arzt &amp; Zahnarzt Termine" src="/static/img/logo.png"></a>
	</div>
	<div id="bd">
		<div id="content-left" class="padding-top">
			{nest ns=menu_left}
		</div>
		<div class="padding-top content-right" >
			{nest ns=content_right}
		</div>
		<div class="clear"></div>
	</div>

	<!-- END: page-wrapper -->
</div>