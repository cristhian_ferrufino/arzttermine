ALTER TABLE bookings ADD (updated_confirmed_at TIMESTAMP, confirmed TINYINT(1) DEFAULT 1) ;

CREATE TABLE IF NOT EXISTS `statements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` tinyint(2) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `monthyear` TEXT NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

ALTER TABLE  `statements` CHANGE  `user_id`  `location_id` INT( 11 ) NOT NULL;
ALTER TABLE  `statements` CHANGE  `monthyear`  `period` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT  'In the form Y-M'

