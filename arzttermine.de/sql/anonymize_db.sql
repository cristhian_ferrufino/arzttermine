-- use this script to anonymize the data from production and prepare for development use

-- anonymize some data
TRUNCATE TABLE `address_geo_datas`;
TRUNCATE TABLE `bookings`;
TRUNCATE TABLE `samedi_bookings`;
TRUNCATE TABLE `booking_offers`;
TRUNCATE TABLE `mailings`;
TRUNCATE TABLE `sent_sms`;
TRUNCATE TABLE `contacts`;
TRUNCATE TABLE `patients`;
TRUNCATE TABLE `referrals`;
TRUNCATE TABLE `patient_referrals`;
TRUNCATE TABLE `login_history`;
TRUNCATE TABLE `users_edit_history`;
UPDATE `users` SET email='', password_hash='', password_hash_salt='', reset_password_code='' WHERE email NOT LIKE '%arzttermine.de';

-- prevent booking real appointments for samedi integration
UPDATE `cms_config` SET `value` = '0' WHERE `cms_config`.`var` = "samedi_real_booking_enable";
