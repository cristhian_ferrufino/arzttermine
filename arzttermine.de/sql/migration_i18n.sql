ALTER TABLE `medical_specialties` CHANGE `slug` `slug_de` VARCHAR( 250 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '';
ALTER TABLE `medical_specialties` CHANGE `name` `name_singular_de` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE `medical_specialties` CHANGE `search_res_name` `name_plural_de` VARCHAR( 50 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

ALTER TABLE `medical_specialties` ADD `slug_en` VARCHAR( 250 ) NOT NULL DEFAULT '' AFTER `slug_de`;
ALTER TABLE `medical_specialties` ADD `name_singular_en` VARCHAR( 255 ) NOT NULL DEFAULT '' AFTER `name_singular_de`;
ALTER TABLE `medical_specialties` ADD `name_plural_en` VARCHAR( 50 ) NOT NULL DEFAULT '' AFTER `name_plural_de`;

ALTER TABLE `insurances` CHANGE `slug` `slug_de` VARCHAR( 250 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '';
ALTER TABLE `insurances` CHANGE `name` `name_de` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE `insurances` ADD `slug_en` VARCHAR( 250 ) NOT NULL DEFAULT '' AFTER `slug_de`;
ALTER TABLE `insurances` ADD `name_en` VARCHAR( 255 ) NOT NULL AFTER `name_de`;


ALTER TABLE `treatmenttypes` CHANGE `name` `name_de` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE `treatmenttypes` ADD `name_en` VARCHAR( 255 ) NOT NULL AFTER `name_de`;

ALTER TABLE `bookings` ADD `booked_language` VARCHAR( 2 ) NOT NULL AFTER `comment_intern`;
