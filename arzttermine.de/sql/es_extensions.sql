
ALTER TABLE `insurances` ADD `slug_es` VARCHAR(255) NOT NULL;
ALTER TABLE `insurances` ADD `name_es` VARCHAR(255) NOT NULL;

ALTER TABLE `languages` ADD `language_es` VARCHAR(255) NOT NULL;

ALTER TABLE `medical_specialties` ADD `slug_es` VARCHAR(255) NOT NULL;
ALTER TABLE `medical_specialties` ADD `name_singular_es` VARCHAR(255) NOT NULL;
ALTER TABLE `medical_specialties` ADD `name_plural_es` VARCHAR(255) NOT NULL;

ALTER TABLE `neighborhoods` ADD `name_es` VARCHAR(255) NOT NULL;

ALTER TABLE `treatmenttypes` ADD `name_es` VARCHAR(255) NOT NULL;