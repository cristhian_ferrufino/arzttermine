<?php 
	
	
	
	/* FACEBOOK START */
	require('src/facebook.php');
	$app_id = "317241091706226";
	$app_secret = "62f0c549cf73156907ec0e30ea29bf93";
	$facebook = new Facebook(array(
	'appId' => $app_id,
	'secret' => $app_secret,
	'cookie' => true
	));
	$signed_request = $facebook->getSignedRequest();
	function parsePageSignedRequest() {
	    if (isset($_REQUEST['signed_request'])) {
	      $encoded_sig = null;
	      $payload = null;
	      list($encoded_sig, $payload) = explode('.', $_REQUEST['signed_request'], 2);
	      $sig = base64_decode(strtr($encoded_sig, '-_', '+/'));
	      $data = json_decode(base64_decode(strtr($payload, '-_', '+/'), true));
	      return $data;
	    }
	    return false;
	}
	
	
	
	
	/* FACEBOOK END */
	function getCurrentSite() {
		$currentSite = $_SERVER['SCRIPT_URI'];
		return $currentSite;
	}	
	function setDocVars() {
		 
		
				$arztName = "Zahnarzttermine";
				$medical_specialty_id = 1;
				$ort		= "deiner Nähe";
				$phone		= "08002222133";
				$phone_read	= "0800 / 2222133";
				$meta_desc 	= "";
				$meta_key 	= "";	
				  			
		return (array("arztName" => $arztName,"ort" => $ort,"special_ID" => $medical_specialty_id, "meta_desc" =>$meta_desc, "meta_key" =>$meta_key, "phone" =>$phone, "phone_read"=>$phone_read));
	}
	/* ######################################################### */
	/* REDIRECT AFTER SEARCH >> by ZIPCODE and SPECIAL_ID */	
	$docVars = setDocVars();	
/*	if( ($_SERVER['REQUEST_METHOD'] === "POST") && ($_POST["send"]) ){	  
	   header('Location: http://www.arzttermine.de/suche?a=search&form%5Blocation%5D='.$_POST["search_zipcode"].'&form%5Bmedical_specialty_id%5D='.$docVars['special_ID'].'&form%5Binsurance_id%5D=1');  
	}	*/	
?>

<!DOCTYPE html>
  <head>
    <meta charset="utf-8">
    <title><?php echo $docVars['arztName']; ?> in <?php echo $docVars['ort']; ?> | Arzttermine.de</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Buche schnell und einfach Arzttermine in <?php echo $docVars['ort']; ?>. Bei uns findest du erstklassige Ärzte zur passenden Zeit">
    <meta name="google-site-verification" content="-_gDcYcXHUlbYQmupgk9TXfVas8FKkoheX64LT10NgA" />
    
    <link href="layout/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
    </style>
    <link href="layout/css/bootstrap-responsive.min.css" rel="stylesheet">
	<link href="layout/css/screen.css" rel="stylesheet">
	<meta http-equiv="cleartype" content="on">
	
	<link rel="shortcut icon" href="layout/ico/favicon.ico">
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="layout/ico/apple-touch-icon-144-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="layout/ico/apple-touch-icon-114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="layout/ico/apple-touch-icon-72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="layout/ico/apple-touch-icon-57-precomposed.png">
	
	<meta property="og:title" content="Arzttermine finden | Arzttermine.de" />
	<meta property="og:url" content="http://www.arzttermine.de/fb/terminfinder/" />
	<meta property="og:site_name" content="Arzttermine finden | Arzttermine.deg"/>
	<meta property="og:type" content="website"/>
	
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  </head>
  <body>
    <section class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="/" onclick="_gaq.push(['_trackEvent', 'Logo_Top', 'click'])"><img src="layout/img/logo-arzttermine.png" alt="Arzttermine.de - schnell. einfach. online" /></a>
          <div id="bekanntAus"></div>
          <p id="hotline">Telefonisch buchen: <span onclick="_gaq.push(['_trackEvent', '<?php echo $docVars['ort']; ?> Hotline', 'click'])"><?php echo $docVars['phone_read']; ?></span></p>
        </div>
      </div>
    </section>
    <section id="content_main" class="container">
      <div class="hero-unit">
        <h1>Freie Arzttermine finden und Wunschtermin buchen..</h1>
        <p class="top_text">Arzttermine.de bietet dir einen schnellen und einfachen Überblick über freie Arzttermine in deiner Umgebung und hilft dir, den passenden Spezialisten zur gewünschten Zeit zu finden.</p>
        <div id="zip-form">
        	<p class="search_text">1. Fachrichtung* auswählen:</p>
        	<select name="specialtySelect" id="specialty">
        		<option value="">...bitte Fachrichtung wählen...</option>
        		<option value="1">Zahnarzt</option>
        		<option value="13">Kinderzahnarzt</option>
        		<option value="3">Frauenarzt / Gynäkologe</option>
        	</select>
        	<p class="search_text">2. Postleitzahl eingeben:</p>        	
        	<p>
        	  <input id="zipSearch" maxlength="5" type="text" name="search_zipcode" value="" class="pull-left zip-code_search" />
        	  <a id="zip-submit" style="height: 51px !important; padding: 0 14px !important; line-height: 49px !important;" class="btn btn-primary btn-large" onclick="_gaq.push(['_trackEvent', 'Facebook_PLZ-Search', 'click'])" target="_blank" href="#">Termin finden</a></p>
        </div>
      </div>           
      <div class="row" id="info">
        <div class="span4">
          <p class="eyecatch"><strong>SCHNELL.</strong></p> 
          <h2>In Sekunden zum Wunschtermin...</h2>
           <p class="text">Mit Arzttermine.de findst du jederzeit freie Arzttermine in deiner Nähe.</p>
        </div>
        <div class="span4">
          <p class="eyecatch"><strong>EINFACH.</strong></p>  
          <h2>...ohne langes Warten...</h2>
          <p class="text">Gib einfach deine Postleitzahl ein und vergleiche bequem erstklassige Ärzte.</p>       
       </div>
        <div class="span4">
          <p class="eyecatch"><strong>ONLINE.</strong></p>
          <h2>… von zuhause und unterwegs!</h2>
          <p class="text">Buche dein Arzttermin – von zuhause, im Büro oder von Unterwegs!</p>          
        </div>
      </div>    
    </section> 
    <footer id="footer">
		<div id="footer_content">
			<div id="footer_navigation">
				<h5 class="fontFam2">Weitere Informationen</h5>
				<ul>
					<li><a href="http://www.arzttermine.de/news" title="News - Arzttermine.de">News</a></li>
					<li><a href="http://www.arzttermine.de/presse" title="Presse - Arzttermine.de">Presse</a></li>
					<li><a href="http://www.arzttermine.de/kontakt" title="Kontakt - Arzttermine.de">Kontakt</a></li>
					<li><a href="http://www.arzttermine.de/impressum" title="Impressum - Arzttermine.de">Impressum</a></li>
					<li><a href="http://www.arzttermine.de/datenschutz" title="Datenschutz - Arzttermine.de">Datenschutz</a></li>
					<li><a href="http://www.arzttermine.de/agb" title="AGB - Arzttermine.de">AGB</a></li>
				</ul>
			</div>
			<div id="footer_hinweis">
				<p>* Weitere Fachrichtungen und Ärzte in deiner Nähe werden in Kürze freigeschaltet.</p>
				<p>Bei <b>Fragen und Anregungen</b> bitten wir dich eine<br>E-Mail an <a href="mailto:support@arzttermine.de">support@arzttermine.de</a> zu senden.</p>
				<p>Wir werden deine Anfrage umgehend bearbeiten!</p>
				<p>Alternativ kannst du uns auch telefonisch unter der kostenlosen Rufnummer <b><a href="tel:08002222133">0800 / 2222133</a></b> erreichen.
				Unsere Mitarbeiter freuen sich rund um die Uhr auf deine Anfragen!</p>
			</div>
		</div>
    </footer>
    <div id="thx" style="display:none;">Vielen Dank für die Verwendung unserer Arzt-Suche!</div>
	<div id="error" style="display:none;">Bitte wähle eine Fachrichtung aus und trage eine Postleitzahl ein.</div>
    <script type="text/javascript" src="layout/js/jquery.min.js"></script>
    <script src="layout/js/special/bootstrap-transition.js"></script>    
    <script src="layout/js/special/bootstrap-collapse.js"></script>
    <script src="layout/js/main.js"></script>
    <script type="text/javascript">    
	    var _gaq = _gaq || [];
	    _gaq.push(['_setAccount', 'UA-27059894-4']);
	    _gaq.push(['_setDomainName', 'arzttermine.de']);
	    _gaq.push(['_setAllowLinker', true]);
	    _gaq.push(['_gat._anonymizeIp']);
	    _gaq.push(['_trackPageview']);
	      (function() {
	        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	      })();	
    </script>
  </body>
</html>
