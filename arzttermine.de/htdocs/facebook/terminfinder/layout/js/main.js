$(document).ready(function () {	
/* START */


$.searchBox();
$.sendSearch();

$("#specialty").focus();

/* ENDE */		
});

/* ##########################################################################################
########################################################################################## */
function debug(message) {
	console.log(message);
}

$.searchBox = function() {
	 if ($('#zipSearch') != false) {
	 	$('#zipSearch').attr('value', 'Plz 12345');
	 }
	 if ($('#zipSearch') != false) {
       $('#zipSearch').on({
         'focus': function () {
           if ($(this).attr('value') == 'Plz 12345') {
             $(this).attr('value', '');
           }
         },
         'blur': function () {
           if ($(this).attr('value') == '') {
             $(this).attr('value', 'Plz 12345');
           }
         }
       });
     }
}
$.getSelection = function() {
	return $("#specialty").val();
}

/* PageLoader */
$.sendSearch = function() {
	$("#zip-submit").on('click', function(event) {
		event.preventDefault();
		var regex = /^([0-9]{5})$/;
		var error = false;
		var specialtyID = $.getSelection();
		var plz 		= $("#zipSearch").val();
		var link 		= "http://www.arzttermine.de/suche?a=search&form%5Blocation%5D="+plz+"&form%5Bmedical_specialty_id%5D="+specialtyID+"&form%5Binsurance_id%5D=2"; 
		
		/* ERROR - PLZ fünfstellig */
		if(specialtyID ==''){ $("#error").text("Bitte wähle eine Fachrichtung aus."); error = true; } 
		if(!regex.test(plz) || (plz == 'Plz 12345') || (plz == '') ){ $("#error").text("Bitte gib eine fünfstellige Postleitzahl an."); error = true; }
		
				
		if ( error == false){		
				$("#thx").fadeIn(500);				
				$('#thx').delay(5500).fadeOut(400);
				window.open(link);
		}
		else {
			$("#error").fadeIn(500);				
			$('#error').delay(2200).fadeOut(400);
		}
	});
	
}