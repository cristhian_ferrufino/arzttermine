<?php
  error_reporting(0);
  if ($_GET['page']) {
    $page = $_GET['page'];
  }
  else {
    $page = "home";
  }
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>arzttermine.de</title>

    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <link href="../css/mobile.css" rel="stylesheet">

    <script src="../static/jquery/jquery-1.9.min.js"></script>
    <script src="../static/js/vendor/modernizr.js"></script>
    <script src="../js/vendor/twbs/ratchet/ratchet.js"></script>
  </head>
  <body>

    <header class="bar bar-nav">
      <a href="/" class="logo"></a>
      <?php if ($page !== "home"): ?>
        <a class="btn btn-link btn-nav pull-left" href="?page=home">
          <span class="fa fa-chevron-left"></span>
          Zurück
        </a>
        <a class="btn btn-link btn-nav pull-right" href="?page=map">
          Karte
        </a>
      <?php endif; ?>
    </header>

    <div class="content">
      <?php include ($page.'.php'); ?>

      <div class="call-now">
        <a class="btn btn-outline btn-secondary" href="tel:+498002222133">
          <span class="fa fa-phone"></span>
          0800 - 2222133
        </a>
      </div>

      <footer>
        <table>
          <tr>
            <td><a class="" href="?page=imprint">Impressum</a></td>
            <td><a class="" href="#">Datenschutz</a></td>
            <td><a class="" href="#">AGB</a></td>
            <td><a class="" href="#">Klassische Version</a></td>
          </tr>
        </table>

        <!--
        <a class="primary-links" href="tel:+498002222133">
          <span class="fa fa-phone"></span>
          0800 - 2222133
        </a> -->
        <a class="primary-links" href="mailto:support@arzttermine.de">
          <span class="fa fa-envelope"></span>
          support@arzttermine.de
        </a>
      </footer>

    </div><!-- .content -->

  </body>
</html>
