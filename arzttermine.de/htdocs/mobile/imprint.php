<div class="content-padded">
  <h1>Impressum</h1>

  <h2>docbiz UG (haftungsbeschränkt)</h2>
  <p>
    Sitz der Gesellschaft<br />
    Rosenthaler Straße 51<br />
    10178 Berlin, Deutschland
  </p>

  <h2>Rechtliches</h2>
  <p>
    Geschäftsführer: Bjoern Keune<br/>
    USt.-IdNr: DE271206218<br />
    HRB: 126290 Amtsgericht Charlottenburg
  </p>

  <h2>Postanschrift</h2>
  <p>
    docbiz UG (haftungsbeschränkt)<br />
    Rosenthaler Straße 51<br>
    10178 Berlin
  </p>

  <h2>Kontakt</h2>
  <p>
    <a href="tel:+4930609840210" class="btn btn-outlined btn-small"><span class="fa fa-phone"></span>&nbsp;+49 (0)30 6098402-10</a>
    <a href="mailto:info@arzttermine.de" class="btn btn-outlined btn-small"><span class="fa fa-envelope"></span>&nbsp;info@arzttermine.de</a>
  </p>
  <p class="content-centered">
    <span class="fa fa-fax"></span>&nbsp;+49 (0)30 6098402-99
  </p>


</div>
