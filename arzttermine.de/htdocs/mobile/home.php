<?php include 'home.js.inc'; ?>
<form method="post" action="index.php?page=list" class="search-form">

  <p class="introduction">Wählen Sie Ihre Stadt und die Fachrichtung. Wir finden Ihren Arzt. Sie können direkt online einen Termin buchen.</p>

  <fieldset>
    <div class="search-form-row location">
      <span class="fa fa-home input-label"></span>
      <select id="form_location" name="form[location]">
        <option class="prompt" value="">Ort / PLZ</option>
        <option value="currentLocation">Aktueller Standort</option>
        <option value="Berlin">Berlin</option>
        <option value="Bremen">Bremen</option>
        <option value="Dresden">Dresden</option>
        <option value="Dortmund">Dortmund</option>
        <option value="Düsseldorf">Düsseldorf</option>
        <option value="Essen">Essen</option>
        <option value="Frankfurt">Frankfurt</option>
        <option value="Hamburg">Hamburg</option>
        <option value="Hannover">Hannover</option>
        <option value="Köln">Köln</option>
        <option value="Leipzig">Leipzig</option>
        <option value="München">München</option>
        <option value="Nürnberg">Nürnberg</option>
        <option value="Stuttgart">Stuttgart</option>
        <option value="Koblenz">Koblenz</option>
      </select>
    </div>
    <div class="search-form-row location">
      <span class="input-label"></span>
      <input type='text' name="exact-location" autocapitalize='off' autocomplete='off' autocorrect='off' id='form_exact_location' placeholder='Ihr Standort' />
    </div>

    <div class="search-form-row">
      <span class="fa fa-stethoscope input-label"></span>
      <select id="form_medical_specialty_id" name="form[medical_specialty_id]">
        <option class='prompt' value="">Fachrichtung wählen</option>
        <option value="1">Zahnarzt</option>
        <option value="28">Akupunkteur</option>
        <option value="22">Allergologe</option>
        <option value="2">Allgemeinarzt / Hausarzt</option>
        <option value="25">Anästhesist</option>
        <option value="68">Andrologie</option>
        <option value="79">Anthroposophische Medizin</option>
        <option value="40">ästhetische Medizin</option>
        <option value="6">Augenarzt</option>
        <option value="66">Bioresonanztherapie</option>
        <option value="38">Chirurg</option>
        <option value="37">Diabetologe</option>
        <option value="70">Endokrinologie</option>
        <option value="41">Ergotherapie</option>
        <option value="78">Ernährungsmedizin</option>
        <option value="63">Facharzt für rehabilitative und physikalische Medizin</option>
        <option value="23">Facharzt für Venenleiden</option>
        <option value="3">Frauenarzt / Gynäkologe</option>
        <option value="64">Gastroenterologe</option>
        <option value="14">Gefäßchirurg</option>
        <option value="71">Geriatrie / Altersheilkunde</option>
        <option value="45">Hausbesuch</option>
        <option value="5">Hautarzt / Dermatologe</option>
        <option value="26">Heilpraktiker</option>
        <option value="62">Heilpraktiker für Psychotherapie</option>
        <option value="4">HNO-Arzt</option>
        <option value="19">Homöopath</option>
        <option value="65">Hypnosetherapeut</option>
        <option value="30">Implantologe</option>
        <option value="20">Internist</option>
        <option value="24">Isopathe</option>
        <option value="35">Kardiologe</option>
        <option value="12">Kieferorthopäde</option>
        <option value="34">Kinderarzt</option>
        <option value="36">Kinderwunsch / Reproduktionsmedizin</option>
        <option value="13">Kinderzahnarzt</option>
        <option value="80">Komplementäre Onkologie</option>
        <option value="81">Logopädie</option>
        <option value="32">Massage</option>
        <option value="48">MRT-Diagnostik</option>
        <option value="15">Mund-Kiefer-Gesichtschirurg</option>
        <option value="67">Naturheilkunde</option>
        <option value="46">Neurochirurg</option>
        <option value="11">Neurologe</option>
        <option value="17">Oralchirurg</option>
        <option value="7">Orthopäde</option>
        <option value="77">Osteologie</option>
        <option value="31">Pflanzenheilkunde</option>
        <option value="33">Physiotherapeut</option>
        <option value="10">Plastische / Ästhetische Chirurgie</option>
        <option value="43">Pneumologe / Lungenfacharzt</option>
        <option value="75">Präventivmedizin</option>
        <option value="69">Proktologie / Erkrankungen des Enddarms</option>
        <option value="21">Psychiater</option>
        <option value="82">Psychologe</option>
        <option value="83">Psychologischer Psychotherapeut</option>
        <option value="29">Psychosomatiker</option>
        <option value="8">Psychotherapeut</option>
        <option value="18">Radiologie</option>
        <option value="39">Refraktive Chirurgie / Augenoperationen</option>
        <option value="73">Rheumatologie</option>
        <option value="74">Schlafmedizin</option>
        <option value="42">Schmerztherapie</option>
        <option value="76">Sportmedizin</option>
        <option value="27">Traditionelle chinesische Medizin</option>
        <option value="9">Urologe</option>
        <option value="47">Wirbelsäulenchirurg</option>
      </select>
    </div>

    <div class="search-form-row">
      <span class="fa fa-credit-card input-label"></span>
      <select id="form_insurance_id" name="form[insurance_id]">
        <option value="2">Gesetzlich versichert</option>
        <option value="1">Privat versichert</option>
      </select>
    </div>

    <button id="form_submit" type="submit" class="btn btn-primary btn-block">
      <span class="fa fa-search"></span>
      Termin finden
    </button>
  </fieldset>

</form>