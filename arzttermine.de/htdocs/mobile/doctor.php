<div class="doctor">

  <ul class="table-view doctor-details card">
    <li class="table-view-cell media">
      <img class="media-object pull-left" src="http://m.arzttermine.de/asset/000/377/533/DR%20Rainer%20Niess%20(2)_4.jpg" width="60">
      <div class="media-body">
        <h1>Dr. Rainer Niess-Schnarrenberger</h1>
        <span class="badge badge-primary">Zahnarzt</span>
        <span class="badge badge-primary">Traumatologe</span>
      </div>
    </li>
  </ul>

  <div class="doctor-address">
    <h3>Zahnarztpraxis am Ku'Damm</h3>
    <p>
      Kurfürstendamm 35<br />
      10719 Berlin-Charlottenburg
    </p>
    <p class="phone">
      <a href="tel:+4930609835116" class="btn btn-outlined btn-small">
        <span class="fa fa-phone"></span>
        030 - 6098 35 116
      </a>
    </p>
  </div>

  <div class="doctor-appointments">
    <ul class="table-view">
      <li class="table-view-divider">Montag, 5. Januar 2015</li>
      <li class="table-view-cell">
        <a href="?page=booking" class="btn btn-primary btn-small">12:00</a>
        <a href="?page=booking" class="btn btn-primary btn-small">13:15</a>
        <a href="?page=booking" class="btn btn-primary btn-small">14:15</a>
        <a href="?page=booking" class="btn btn-primary btn-small">15:35</a>
        <a href="?page=booking" class="btn btn-primary btn-small">15:55</a>
        <a href="?page=booking" class="btn btn-primary btn-small">17:00</a>
      </li>
      <li class="table-view-divider">Dienstag, 6. Januar 2015</li>
      <li class="table-view-cell">
        <a href="?page=booking" class="btn btn-primary">12:00</a>
        <a href="?page=booking" class="btn btn-primary">17:00</a>
      </li>
      <li class="table-view-divider">Donnerstag, 8. Januar 2015</li>
      <li class="table-view-cell">
        <a href="?page=booking" class="btn btn-primary">12:00</a>
      </li>
    </ul>
  </div>

  <div class="content-padded doctor-more-appointments">
    <a class="btn btn-primary btn-outlined">Weitere Termine anzeigen&nbsp;<span class="fa fa-chevron-down"></span></a>
  </div>

  <div class="content-padded">
    <h2>Philosophie</h2>
    <p>Die schönste Visitenkarte eines Menschen sind strahlend weiße und gesunde Zähne.</p>
    <p>Der individuelle Mensch steht hierbei im Mittelpunkt unseres Denkens und Handelns.</p>
    <p>Wir offerieren Ihnen in unserer Zahnarztpraxis hochmoderne Zahnheilkunde, die individuell auf jeden einzelnen Menschen persönlich zugeschnittene ganzheitliche Behandlungskonzepte anbietet.</p>
    <p>In unserer Zahnarztpraxis am Kurfürstendamm 35 ist jeder Patient ein VIP-Patient. Wir freuen uns auf Sie!</p>
  </div>

  <div class="content-padded">
    <h2>Spezialisierung</h2>
    <p>Highend-Dentistry – Ästhetische Zahnheilkunde umfaßt neueste und wirksamste Methoden der Zahnmedizin.</p>
    <ul>
      <li>ganzheitliche Zahnmedizin</li>
      <li>Veneers und Luminiers</li>
      <li>Bleaching</li>
      <li>Prophylaxe</li>
      <li>Lasertherapien</li>
      <li>Perfekte ästhetische Rekonstruktionen</li>
      <li>Zahnschiene- Invisalign</li>
      <li>Kiefergelenksdiagnostik</li>
      <li>Füllungstherapien</li>
      <li>Minimalinvasive Implantologie- gestützt auf die computernavigierte Methode von Nobel Biocare, welche lebenslange Garantie auf Implantatsysteme bietet</li>
      <li>Parodontosebehandlungen</li>
      <li>Angstpatient</li>
      <li>nachhaltige Behandlungskonzepte</li>
    </ul>
  </div>

  <div class="content-padded">
    <h2>Behandlungsmöglichkeiten</h2>
    <ul>
      <li>Ästhetische Zahnmedizin</li>
      <li>Parodontosebehandlungen</li>
      <li>Prothetik</li>
      <li>minimalinvasive Füllungstherapie</li>
      <li>Kiefergelenksdiagnostik</li>
      <li>Hypnosetherapie bei Angstpatienten</li>
      <li>Endodontie</li>
      <li>Implantatprothetik</li>
    </ul>
    <p>Die kontinuierliche Teilnahme an Fortbildungen unseres Teams in Deutschland und Europa und jahrelange Berufserfahrung, sowie höchste Qualität und ein eigenes Zahnlabor runden das Profil ab.</p>
  </div>

  <div class="content-padded">
    <h2>Ausbildung</h2>
    <p>
      Geboren bei Ulm wuchs ich in Stuttgart auf und machte 1966 Abitur am Friedrich Schiller Gymnasium in Fellbach.<br>
      Nach der Offiziersausbildung bei der Bundeswehr begann ich 1967 mein Studium an der Christians Albrecht Universität in Kiel und legte dort das Physikum ab.<br>
      Das klinische Studium setzte ich  an der Albrecht Ludwigs Universität in Freiburg for, wo ich 1972 das Examen machte und 1973 meine Promotion abschloss. Es folgten acht Jahre mit verschiedenen zahnärztlichen Verwendungen und Führungsaufgaben als Sanitätsoffizier bei Luftwaffe und den Gebirgsjägern in verschiedenen Standorten zwischen Husum und Bad Reichenhall .Auch eine Kommandierung nach Beja in Portugal gehörte zum Einsatzbereich.
    </p>
    <p>
      Meine erste Niederlassung in eigener Praxis war 1981 in Diessen am Ammersee. 1985 wechselte ich nach  Garmisch Partenkirchen, wo ich eine große Praxis am Marienplatz übernahm.<br>
      Seit 1993 bin ich in Berlin, da ich zukunftsdenkend eine vernetzte und spezialisierte Praxis aufbauen wollte, was inzwischen bei anderen Kollegen auch schon angekommen ist.<br>
      Zahnmedizin im dritten Jahrtausend macht mir Spaß, wir sind erfolgreich und wir haben unglaublich nette Patienten.<br>
      Außerdem fühle ich mich in Berlin sauwohl.<br>
      Also werde ich Ihnen in der Praxis noch lange erhalten bleiben.
    </p>
  </div>

  <div class="content-padded content-centered">
    <p>Telefonisch buchen:</p>
    <a href="tel:+4930609835116" class="btn btn-outline">
      <span class="fa fa-phone"></span>
      030 - 6098 35 116
    </a>
  </div>

</div>
