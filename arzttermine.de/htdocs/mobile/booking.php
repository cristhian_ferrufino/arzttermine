<div class="booking">

  <ul class="table-view card">
    <li class="table-view-cell media">
      <img class="media-object pull-left" src="http://m.arzttermine.de/asset/000/377/533/DR%20Rainer%20Niess%20(2)_4.jpg" width="60">
      <div class="media-body">
        <h1>Dr. Rainer Niess-Schnarrenberger</h1>
        <p>Montag, 05.01.2015<br />Um 12:15 Uhr</p>
      </div>
    </li>
  </ul>

  <form method="POST" action="?page=confirmation">
    <fieldset>

      <div class="radio">
        <span><input type="radio" value="1" name="salutation" />Herr</span>
        <span><input type="radio" value="2" name="salutation" />Frau</span>
      </div>

      <input type="text" placeholder="Vorname">
      <input type="text" placeholder="Nachname">
      <input type="email" placeholder="E-Mail-Adresse">
      <input type="text" placeholder="Telefon">

      <div class="radio radio-block">
        <span><input type="radio" value="2" name="insurance_type" />Gesetzlich versichert</span>
        <span><input type="radio" value="1" name="insurance_type" />Privat versichert</span>
      </div>

      <div class="radio">
        <label for="known_patient">Sind Sie Patient/in bei Dr. Railer Niess-Schnarrenberger?</label>
        <span><input type="radio" value="2" name="known_patient" />ja</span>
        <span><input type="radio" value="1" name="known_patient" />nein</span>
      </div>

      <div>
        <select>
          <option>Behandlungsgrund (optional)</option>
          <option>Vorsorge</option>
          <option>Nachsorge</option>
        </select>
      </div>

      <div>
        <input type="text" placeholder="Empfehlungscode (optional)">
      </div>

      <div class="checkbox">
        <label for="agb">
          <input type="checkbox" id="agb" />
          Ich stimme der Verwendung meiner Daten zur Vermittlung meines Termins unter den in der <a target="_blank" href="/datenschutz">Datenschutzerklärung</a> genannten Bedingungen zu. Arzttermine.de darf insbesondere meine Daten an den Arzt weiterleiten und dieser rückübermitteln, ob ich den Termin wahrgenommen habe.
        </label>
      </div>

      <div class="checkbox">
        <div>
          <label for="newsletter">
            <input type="checkbox" id="newsletter" />
            Newsletter abonnieren
          </label>
        </div>
      </div>

      <button type="submit" class="btn btn-primary btn-block">Termin buchen</button>

    </fieldset>
  </form>

</div>
