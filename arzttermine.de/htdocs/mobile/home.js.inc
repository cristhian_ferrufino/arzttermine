<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>

<script type='text/javascript'>
var AT = AT || {};

AT.q=[];
AT._defer=function(f){
    AT.q.push(f);
};

//create an array of objects of cities
var AllCities = [
    {
        id: 1,
        name: "Berlin",
        specialties: [1, 5, 10, 12, 13, 17, 18, 19, 26, 27, 28, 29, 30, 31, 32],
        lat: 52.519171,
        lng: 13.4060912
    },
    {
        id: 2,
        name: "Frankfurt",
        specialties: [1, 2, 3, 4, 5, 6, 7, 9, 10, 12, 13, 30],
        lat: 50.1109221,
        lng: 8.6821267
    },
    {
        id: 3,
        name: "Hamburg",
        specialties: [1, 2, 3, 5, 9, 12, 13, 15, 17, 30],
        lat: 53.5510846,
        lng: 9.9936818
    },
    {
        id: 4,
        name: "Köln",
        specialties: [1, 2, 3, 4, 7, 10, 11, 12, 13, 17, 19, 30],
        lat: 50.937531,
        lng: 6.9602786
    },
    {
        id: 5,
        name: "München",
        specialties: [1, 3, 4, 5, 6, 10, 12, 13],
        lat: 48.1366069,
        lng: 11.5770851
    },
    {
        id: 6,
        name: "Bremen",
        specialties: [1, 12, 13, 17, 30],
        lat: 53.078965,
        lng: 8.801765
    },
    {
        id: 7,
        name: "Dresden",
        specialties: [1, 12, 13, 15],
        lat: 51.050365,
        lng: 13.737309
    },
    {
        id: 8,
        name: "Düsseldorf",
        specialties: [1, 2, 5, 13, 15, 20],
        lat: 51.227733,
        lng: 6.773455
    },
    {
        id: 9,
        name: "Essen",
        specialties: [1, 5, 13, 22, 23],
        lat: 51.455628,
        lng: 7.011568
    },
    {
        id: 10,
        name: "Hannover",
        specialties: [1, 3, 6, 10, 12, 13, 30],
        lat: 52.37554,
        lng: 9.731719
    },
    {
        id: 11,
        name: "Leipzig",
        specialties: [1, 13, 15],
        lat: 51.340639,
        lng: 12.377472
    },
    {
        id: 12,
        name: "Nürnberg",
        specialties: [1, 13, 30],
        lat: 49.452022,
        lng: 11.076745
    },
    {
        id: 13,
        name: "Stuttgart",
        specialties: [14, 1, 4, 17],
        lat: 48.775339,
        lng: 9.181738
    },
    {
        id: 14,
        name: "Dortmund",
        specialties: [1],
        lat: 51.513376,
        lng: 7.465333
    }
];

function getDistance( lat1, lon1, lat2, lon2, unit ) {
    var radlat1  = Math.PI * lat1/180,
        radlat2  = Math.PI * lat2/180,
        theta    = lon1-lon2,
        radtheta = Math.PI * theta/180,
        dist;

    dist = Math.acos(Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta)) * 180/Math.PI * 60 * 1.1515;

    if (unit == "K") {
        dist = dist * 1.609344;
    }

    if (unit == "N") {
        dist = dist * 0.8684;
    }

    return dist;
}

function update_form() {
    var select_form_location = $('#form_location').val(),
        select_options = {"Berlin":{"Zahnarzt":"1","Akupunkteur":"28","Allergologe":"22","Allgemeinarzt \/ Hausarzt":"2","An\u00e4sthesist":"25","Andrologie":"68","Anthroposophische Medizin":"79","\u00e4sthetische Medizin":"40","Augenarzt":"6","Bioresonanztherapie":"66","Chirurg":"38","Diabetologe":"37","Ergotherapie":"41","Ern\u00e4hrungsmedizin":"78","Facharzt f\u00fcr Venenleiden":"23","Frauenarzt \/ Gyn\u00e4kologe":"3","Gastroenterologe":"64","Gef\u00e4\u00dfchirurg":"14","Hausbesuch":"45","Hautarzt \/ Dermatologe":"5","Heilpraktiker":"26","Heilpraktiker f\u00fcr Psychotherapie":"62","HNO-Arzt":"4","Hom\u00f6opath":"19","Implantologe":"30","Internist":"20","Isopathe":"24","Kardiologe":"35","Kieferorthop\u00e4de":"12","Kinderarzt":"34","Kinderwunsch \/ Reproduktionsmedizin":"36","Kinderzahnarzt":"13","Komplement\u00e4re Onkologie":"80","Massage":"32","MRT-Diagnostik":"48","Mund-Kiefer-Gesichtschirurg":"15","Naturheilkunde":"67","Neurochirurg":"46","Oralchirurg":"17","Orthop\u00e4de":"7","Pflanzenheilkunde":"31","Physiotherapeut":"33","Plastische \/ \u00c4sthetische Chirurgie":"10","Pneumologe \/ Lungenfacharzt":"43","Pr\u00e4ventivmedizin":"75","Proktologie \/ Erkrankungen des Enddarms":"69","Psychiater":"21","Psychologe":"82","Psychologischer Psychotherapeut":"83","Psychosomatiker":"29","Psychotherapeut":"8","Radiologie":"18","Refraktive Chirurgie \/ Augenoperationen":"39","Schlafmedizin":"74","Schmerztherapie":"42","Sportmedizin":"76","Traditionelle chinesische Medizin":"27","Urologe":"9","Wirbels\u00e4ulenchirurg":"47"},"Bremen":{"Zahnarzt":"1","Implantologe":"30","Kieferorthop\u00e4de":"12","Kinderzahnarzt":"13","Oralchirurg":"17"},"Dortmund":{"Zahnarzt":"1","Heilpraktiker f\u00fcr Psychotherapie":"62","Implantologe":"30"},"Dresden":{"Zahnarzt":"1","\u00e4sthetische Medizin":"40","Plastische \/ \u00c4sthetische Chirurgie":"10"},"D\u00fcsseldorf":{"Zahnarzt":"1","Allgemeinarzt \/ Hausarzt":"2","Hautarzt \/ Dermatologe":"5","Implantologe":"30","Kinderzahnarzt":"13","Plastische \/ \u00c4sthetische Chirurgie":"10","Psychiater":"21","Psychotherapeut":"8","Radiologie":"18","Refraktive Chirurgie \/ Augenoperationen":"39","Urologe":"9"},"Essen":{"Zahnarzt":"1","Allgemeinarzt \/ Hausarzt":"2"},"Frankfurt":{"Zahnarzt":"1","Allgemeinarzt \/ Hausarzt":"2","Andrologie":"68","Augenarzt":"6","Diabetologe":"37","Endokrinologie":"70","Frauenarzt \/ Gyn\u00e4kologe":"3","Hautarzt \/ Dermatologe":"5","Heilpraktiker":"26","HNO-Arzt":"4","Hom\u00f6opath":"19","Implantologe":"30","Internist":"20","Kieferorthop\u00e4de":"12","Kinderarzt":"34","Kinderzahnarzt":"13","Mund-Kiefer-Gesichtschirurg":"15","Neurologe":"11","Oralchirurg":"17","Orthop\u00e4de":"7","Physiotherapeut":"33","Plastische \/ \u00c4sthetische Chirurgie":"10","Psychiater":"21","Refraktive Chirurgie \/ Augenoperationen":"39","Rheumatologie":"73","Urologe":"9"},"Hamburg":{"Zahnarzt":"1","Allgemeinarzt \/ Hausarzt":"2","\u00e4sthetische Medizin":"40","Augenarzt":"6","Chirurg":"38","Frauenarzt \/ Gyn\u00e4kologe":"3","Gef\u00e4\u00dfchirurg":"14","Geriatrie \/ Altersheilkunde":"71","Hautarzt \/ Dermatologe":"5","Heilpraktiker":"26","Hypnosetherapeut":"65","Implantologe":"30","Internist":"20","Kardiologe":"35","Kieferorthop\u00e4de":"12","Kinderarzt":"34","Kinderzahnarzt":"13","Neurochirurg":"46","Neurologe":"11","Oralchirurg":"17","Orthop\u00e4de":"7","Plastische \/ \u00c4sthetische Chirurgie":"10","Pneumologe \/ Lungenfacharzt":"43","Psychotherapeut":"8","Radiologie":"18","Refraktive Chirurgie \/ Augenoperationen":"39","Traditionelle chinesische Medizin":"27","Urologe":"9","Wirbels\u00e4ulenchirurg":"47"},"Hannover":{"Zahnarzt":"1","Frauenarzt \/ Gyn\u00e4kologe":"3","Heilpraktiker":"26","Implantologe":"30","Kieferorthop\u00e4de":"12","Kinderzahnarzt":"13","Mund-Kiefer-Gesichtschirurg":"15","Oralchirurg":"17","Plastische \/ \u00c4sthetische Chirurgie":"10"},"K\u00f6ln":{"Zahnarzt":"1","Akupunkteur":"28","Allergologe":"22","Allgemeinarzt \/ Hausarzt":"2","Chirurg":"38","Ern\u00e4hrungsmedizin":"78","Frauenarzt \/ Gyn\u00e4kologe":"3","HNO-Arzt":"4","Implantologe":"30","Internist":"20","Kardiologe":"35","Kieferorthop\u00e4de":"12","Kinderwunsch \/ Reproduktionsmedizin":"36","Kinderzahnarzt":"13","Naturheilkunde":"67","Neurologe":"11","Orthop\u00e4de":"7","Osteologie":"77","Physiotherapeut":"33","Plastische \/ \u00c4sthetische Chirurgie":"10","Pneumologe \/ Lungenfacharzt":"43","Psychiater":"21","Psychosomatiker":"29","Psychotherapeut":"8","Schlafmedizin":"74","Schmerztherapie":"42","Sportmedizin":"76","Traditionelle chinesische Medizin":"27"},"Leipzig":{"Zahnarzt":"1","Allgemeinarzt \/ Hausarzt":"2","\u00e4sthetische Medizin":"40","Augenarzt":"6","Hautarzt \/ Dermatologe":"5","Kinderarzt":"34","Kinderzahnarzt":"13","Mund-Kiefer-Gesichtschirurg":"15","Orthop\u00e4de":"7","Plastische \/ \u00c4sthetische Chirurgie":"10","Psychotherapeut":"8","Radiologie":"18","Urologe":"9"},"M\u00fcnchen":{"Zahnarzt":"1","Akupunkteur":"28","Allergologe":"22","Allgemeinarzt \/ Hausarzt":"2","\u00e4sthetische Medizin":"40","Chirurg":"38","Facharzt f\u00fcr rehabilitative und physikalische Medizin":"63","Frauenarzt \/ Gyn\u00e4kologe":"3","Hautarzt \/ Dermatologe":"5","Heilpraktiker":"26","HNO-Arzt":"4","Hom\u00f6opath":"19","Implantologe":"30","Internist":"20","Kardiologe":"35","Kieferorthop\u00e4de":"12","Kinderarzt":"34","Kinderzahnarzt":"13","MRT-Diagnostik":"48","Mund-Kiefer-Gesichtschirurg":"15","Naturheilkunde":"67","Oralchirurg":"17","Orthop\u00e4de":"7","Pflanzenheilkunde":"31","Plastische \/ \u00c4sthetische Chirurgie":"10","Psychotherapeut":"8","Radiologie":"18","Refraktive Chirurgie \/ Augenoperationen":"39","Traditionelle chinesische Medizin":"27","Urologe":"9"},"N\u00fcrnberg":{"Zahnarzt":"1","\u00e4sthetische Medizin":"40","Implantologe":"30","Kieferorthop\u00e4de":"12","Kinderzahnarzt":"13","Oralchirurg":"17","Plastische \/ \u00c4sthetische Chirurgie":"10","Refraktive Chirurgie \/ Augenoperationen":"39"},"Stuttgart":{"Zahnarzt":"1","Allgemeinarzt \/ Hausarzt":"2","\u00e4sthetische Medizin":"40","Gef\u00e4\u00dfchirurg":"14","HNO-Arzt":"4","Implantologe":"30","Kieferorthop\u00e4de":"12","Kinderzahnarzt":"13","Plastische \/ \u00c4sthetische Chirurgie":"10","Refraktive Chirurgie \/ Augenoperationen":"39"},"Koblenz":{"Allgemeinarzt \/ Hausarzt":"2","An\u00e4sthesist":"25","Internist":"20","Orthop\u00e4de":"7","Psychotherapeut":"8","Schmerztherapie":"42","Urologe":"9"}} || {},
        $medical_specialty_id = $('#form_medical_specialty_id');

    $medical_specialty_id.empty().append($('<option class="prompt" value="">...bitte Fachrichtung wählen...</option>'));

    for (var options in select_options[select_form_location]) {
        if (select_options[select_form_location].hasOwnProperty(options)) {
            $medical_specialty_id.append($('<option value="' + select_options[select_form_location][options] + '">' + options + '</option>'));
        }
    }

    $('#form_medical_specialty_id').val('');
}

$(function() {
    $('#form_location').change(function() {
        //clear exact location when changing city
        $('#form_exact_location').val('');

        //check if user selected to use current location
        if ($('#form_location').val() == "currentLocation") {
            locate_user();
        } else {
            update_form();
        }

        return false;
    });
});


//$(document).ready(function() {
function locate_user() {
    //get users location if geolocation exists
    if (navigator.geolocation) {
        //show a masked spinner until getting the user's location
        navigator.geolocation.getCurrentPosition(function(position) {
                $('#spinner').remove();
                Location = position;
                //get latitude longitude of user
                var currentLat = Location.coords.latitude,
                    currentLng = Location.coords.longitude,
                    unit = "K",
                    distances = [];


                //get formatted address of user's current location
                var geocoder = new google.maps.Geocoder();
                var latLng = new google.maps.LatLng(currentLat, currentLng);
                geocoder.geocode( { 'latLng': latLng}, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[1]) {
                            //assign the location field to the formatted address
                            $('#form_exact_location').val(results[0].formatted_address)
                        }
                    }
                });

                //for each city calculate the distance of the city to the user
                AllCities.forEach(function( city ) {
                    var distance = getDistance(
                        currentLat, currentLng,
                        city["lat"], city["lng"],
                        unit
                    );
                    //push the name and the distance of each city to a new array of objects
                    distances.push({
                        name: city["name"],
                        distance: distance
                    });

                }, this);

                //sort the new array of cities by distances
                var sortedDistances = distances.sort(function(a,b) { return (a.distance > b.distance) ? 1 : ((b.distance > a.distance) ? -1 : 0); } );

                //set the value of the dropdown to the nearest city of the user's position
                $('#form_location').val(sortedDistances[0].name).change();
                //update medical specialties select
                update_form();
            },
            function () {
                //if geolocation failed remove the spinner
                $('#spinner').remove();
                alert("Geolocation unavailable. Please check your system settings");
                $("#form_location").val(0);
                $("#form_medical_specialty_id").attr('disabled', 'disabled').fadeTo('fast', 0.5);
            });
    }
}

function doBounce(element, times, distance, speed) {
    for (var i = 0; i < times; i++) {
        element.animate({ marginLeft: '-=' + distance }, speed)
            .animate({ marginLeft: '+=' + distance }, speed);
    }
}

function toggleEnabled($elm, is_enabled) {
    is_enabled = !!is_enabled;

    if (is_enabled) {
        $elm.attr('disabled', 'disabled');
        $elm.fadeTo('fast', 0.5);
    } else if(!is_enabled) {
        $elm.removeAttr('disabled');
        $elm.fadeTo('fast', 1);
    }
}

function enable($elm) {
    toggleEnabled($elm, false);
}

function disable($elm) {
    toggleEnabled($elm, true);
}

$(function() {
    var $locations_select = $('#form_location'),
        $insurances_select = $('#form_insurance_id'),
        $specialties_select = $('#form_medical_specialty_id');

    // on load of the web page disable all inputs except the Location select (cities-select)
    if (!$locations_select.val()) {
        disable($('#form_exact_location, #form_medical_specialty_id, #form_insurance_id'));
        locate_user();
    }

    //when changing the cities-select
    $locations_select.change(function() {
        //check if user has not selected a real value
        if(!$locations_select.val()) {
            disable($('#form_exact_location, #form_medical_specialty_id'));
            if ($insurances_select.val()) {
                disable($insurances_select);
            }
        } else {
            enable($('#form_exact_location, #form_medical_specialty_id'));
            if ($insurances_select.val()){
                enable($insurances_select);
            }
        }
    });

    //when changing the specialties-select
    $specialties_select.change(function() {
        //check if user has not selected a real value
        if (!$specialties_select.val()) {
            if (!$insurances_select.val()) {
                disable($insurances_select);
            }
        } else {
            if ($insurances_select.val()) {
                enable($insurances_select);
            }
        }
    });

    $('#form_submit').click(function(e) {
        if (!$locations_select.val()) {
            doBounce($locations_select, 4, '4px', 30);
            e.preventDefault();
        } else if (!$specialties_select.val()) {
            doBounce($specialties_select, 4, '4px', 30);
            e.preventDefault();
        } else if (!$insurances_select.val()) {
            doBounce($insurances_select, 4, '4px', 30);
            e.preventDefault();
        }
    });
});

</script>
