<div class="list">

  <div class="content-padded">
    <h1>1903 Zahnärzte in Berlin haben Termine für Sie:</h1>
  </div>

  <ul class="table-view">
    <li class="table-view-cell media">
      <a class="navigate-right" href="?page=doctor">
        <img class="media-object pull-left" src="http://m.arzttermine.de/asset/000/377/533/DR%20Rainer%20Niess%20(2)_4.jpg" width="60">
        <div class="media-body">
          <h2>Dr. Rainer Niess-Schnarrenberger</h2>
          <p class="address">
            Kurfürstendamm 131b<br />
            10247 Kaiserslautern-West
          </p>
          <div class="appointment">
            <h3>Nächster Termin:</h3>
            <p>Mi, 05.12.2015 um 12:00</p>
          </div>
          <p class="distance">
            <span class="badge">5,87 km</span>
          </p>
        </div>
      </a>
    </li>

    <li class="table-view-cell media">
      <a class="navigate-right" href="?page=doctor">
        <img class="media-object pull-left" src="http://m.arzttermine.de/asset/000/377/533/DR%20Rainer%20Niess%20(2)_4.jpg" width="60">
        <div class="media-body">
          <h2>Dr. Rainer Niess-Schnarrenberger</h2>
          <p class="address">
            Kurfürstendamm 131b<br />
            10247 Kaiserslautern-West
          </p>
          <div class="appointment">
            <h3>Nächster Termin:</h3>
            <p>Mi, 05.12.2015 um 12:00</p>
          </div>
          <p class="distance">
            <span class="badge">5,87 km</span>
          </p>
        </div>
      </a>
    </li>

    <li class="table-view-cell media">
      <a class="navigate-right" href="?page=doctor">
        <img class="media-object pull-left" src="http://m.arzttermine.de/asset/000/377/533/DR%20Rainer%20Niess%20(2)_4.jpg" width="60">
        <div class="media-body">
          <h2>Dr. Rainer Niess-Schnarrenberger</h2>
          <p class="address">
            Kurfürstendamm 131b<br />
            10247 Kaiserslautern-West
          </p>
          <div class="appointment">
            <h3>Nächster Termin:</h3>
            <p>Mi, 05.12.2015 um 12:00</p>
          </div>
          <p class="distance">
            <span class="badge">5,87 km</span>
          </p>
        </div>
      </a>
    </li>

  </ul>

</div>
