<?php
/**
 * api
 * 
 * After redesign only the widgets are still using this api
 * Everything else is using the class.api.php logic
 *
 * @author Axel Kuzmik <axel@kuzmik.de>
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/../include/bootstrap.php';

use Arzttermine\Application\Application;
use Arzttermine\Calendar\Calendar;
use Arzttermine\Location\Location;
use Arzttermine\User\User;
use Arzttermine\Widget\WidgetFactory;
use NGS\Managers\SearchResultCacheTmpManager;
use NGS\Util\ProviderRow;

$app = Application::getInstance();
$view = $app->getView();
$request = $app->getRequest();

if ($widget_slug = $request->getParam('widget')) {
    try {
        $app->setWidgetContainer(WidgetFactory::getWidgetContainer($widget_slug))->setIsWidget(true);
    } catch (\Exception $e) {
        // Ignore
    }
}

// START ****************************************************
$action = getParam('a','');

switch ($action) {
	/* *******************************************
	 * PROVIDER
	 * ******************************************* */
        case 'get-all-times':		
		$date_days =  intval(getParam('date_days'));
		
				// Sanitize to a max days!
		if (!is_numeric($date_days) || $date_days > $GLOBALS['CONFIG']['CALENDAR_MAX_DAYS_ON_ONE_PAGE']) $date_days = $GLOBALS['CONFIG']['CALENDAR_MAX_DAYS_ON_ONE_PAGE'];
		if ($date_days < 1) $date_days = 1;

		$date_start = getParam('date_start');
		if (empty($date_start)) {
			$date_start = Calendar::getFirstPossibleDateTimeForSearch();
		}
		$index = getParam('index', 0);		
		if (preg_match("/^(\d{4})-(\d{2})-(\d{2})$/", $index)) {
			$days_count = Calendar::dateDiffDaysCount($date_start, $index);
			$index = $days_count / intval($date_days);
		}
		$index = intval($index);
		if ($index > 0 && (($index * $date_days) < $GLOBALS['CONFIG']['CALENDAR_MAX_DAYS_AHEAD'])) {
			$_add_days = $index * $date_days;
			$date_start = Calendar::addDays($date_start, $_add_days, true);
		}
		$ret = array();
		//calendar header start
		$date_end = Calendar::addDays($date_start, ($date_days + 1));
		$view->setRef('date_start', $date_start);
		$view->setRef('date_end', $date_end);
		$view->setRef('date_days', $date_days);
		$ret[] = Calendar::getCalendarDaysHtml($date_start, $date_days);

		$userIds = getParam('user_ids');
		$locationIds = getParam('location_ids');
		$widgetSlug = getParam('widget');
		// @todo: optimize this sanitation (if that is one at all...)
		if (!empty($widgetSlug)){
			foreach ($userIds as $i => $userId) {
				if(intval($userId)<1){
					unset($userIds[$i]);
					unset($locationIds[$i]);
				}
			}
		}
		$medicalSpecialtyId = getParam('medical_specialty_id');
		$treatmentTypeId = getParam('treatmenttype_id');
		$insuranceId = getParam('insurance_id');

		$providers = array();
		foreach ($userIds as $parameterIndex => $userId) {
			$provider = new ProviderRow();
			$user = new User($userId);
			$provider->setUser($user);
			$location = new Location($locationIds[$parameterIndex]);
			$provider->setLocation($location);
			$provider->setMedicalSpecialtyId($medicalSpecialtyId);
			$provider->setTreatmentTypeId($treatmentTypeId);
			$providers[$parameterIndex] = $provider;
		}
		$searchResultCacheTmpManager = SearchResultCacheTmpManager::getInstance();
		$providers = $searchResultCacheTmpManager->getProviderRowsByFilters($providers, $date_start, $date_end, $insuranceId, $treatmentTypeId, $firstWeek = 0);
		foreach ($providers as $provider) {
			$view->setRef('appointmentsBlock', $provider->getAppointmentsBlock($date_start, $date_end, $insuranceId, $medicalSpecialtyId, $treatmentTypeId, $widgetSlug));
			$ret[] = $view->fetch('appointment/_appointments-block-w.tpl');
		}
		if ($index > 0){
			//for prev button enable
			  $ret[] = '1';
		}else{
			//for prev button disable
			  $ret[] = '0';
		}                
	   if (($index+1) * $date_days < intval($GLOBALS['CONFIG']['CALENDAR_MAX_DAYS_AHEAD'])){
		  //for next button enable
			  $ret[] = '1';
		}else{
			//for next button disable
			  $ret[] = '0';
		}
		$ret[] = $index;
        
        Application::getInstance()->container->get('request')->json($ret)->send();
		break;
}
