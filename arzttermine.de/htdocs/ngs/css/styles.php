<?php
	require_once("../../conf/constants.php");
	$conf = parse_ini_file(CONF_PATH."/config.ini");
	$protocol = "http://";
	if(isset($_SERVER["HTTPS"])){
		$protocol = "https://";	
	}
	$static_path = $protocol.$conf['static'];
    
    header('Content-Type: text/css');
