/**
* @fileoverview
* @class core class of the framework which is responsible for ajax requests and delivery of responses
* work with loads and actions which are for supporting same structure as backend has
* @author Naghashyan Solutions, e-mail: info@naghashyan.com
* @version 1.0
*/

Object.extend(Framework, {
    ajaxLoader: null,
    pageManager: null,
    toolbarManager: null,
    loadtracking: true,
    
    setCurrentmanager: function(currentManager){
        this.currentManager = currentManager;
    },
    
    load: function(loadName, params, dynContainer){
        var load = this.getLoad(loadName);
        if (!params || typeof(params) == "undefined") {
            params = {};
        }
        load.setParams(params);
        load.setDynContainer(dynContainer);
        load.load();
    },
    
    action: function(actionName, params){
        var action = this.actionFactory.getAction(actionName);
        action.setParams(params);
        action.action();
    },
    
    getLoad: function(loadName){
        return this.loadFactory.getLoad(loadName);
    },
    
    main: function(){
        this.defaultParams = {};
        //		this.defaultParams.userId = false;
        //		if($("defaultUserId") && $("defaultUserId").value != ""){
        //			this.defaultParams.userId = $("defaultUserId").value;
        //		}
        
	
        
        this.loadManager = new Framework.LoadManager(this, null, new Framework.UrlFormater());
        this.ajaxLoader = new Framework.AjaxLoader(SITE_URL + "/dyn/", "ajax_loader", this.loadManager, this.defaultParams);
        this.loadFactory = new Framework.LoadFactory(this.ajaxLoader);
        this.actionFactory = new Framework.ActionFactory(this.ajaxLoader);
       
        this.ajaxLoader.start(this.loadFactory, null);
       
        
        Framework.DialogUtility.init("warningDialog", "f_close", "errorDialog", "f_close", "f_confirmDialog");
        //this.thumbsMarunInitialLoadnager = new Framework.ThumbsManager("thumbnailsBody");
        if ($("disableLoadTracking")) {
            this.ajaxLoader.disableLoadtracking();
        }
        this.runInitialLoad();
        
        
    },
    
    nestLoad: function(loadName, params){
        var load = this.loadFactory.getLoad(loadName);
        if (params) {
            load.setParams(params);
        }
        this.ajaxLoader.afterLoad(load);
        load.initPagging();
        
    },
    
    runInitialLoad: function(){
        var initialLoadElem = $("initialLoad");
        if (initialLoadElem && initialLoadElem.value != "") {
            this.nestLoad(initialLoadElem.value);
        }
    },
    
    getDefaultParams: function(){
        return this.defaultParams;
    }
    
});

document.observe("dom:loaded", function() {
  Framework.main(Framework);
});