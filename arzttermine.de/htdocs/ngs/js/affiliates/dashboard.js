$(document).ready(function() {
	if ($('#dh_select_page').length > 0) {
		$('#dh_select_page').change(function() {
			var page = $(this).val();
			$(location).attr('href', site_PATH + '/affiliates/dashboard?' + $.param(getParams()));
		});
	}
	$('#dh_search_button').click(function() {
		$(location).attr('href', site_PATH + '/affiliates/dashboard?' + $.param(getParams()));
	});

	$('#dh_search_text').keypress(function(e) {
		if (e.which === 13)
		{
			$(location).attr('href', site_PATH + '/affiliates/dashboard?' + $.param(getParams()));
		}
	});
	function doSearch() {
		
	}
	function getParams()
	{
		var search_text = $('#dh_search_text').val();
		var search_tables_and_fialds_names = $('#dh_selected_field_to_search').val();
		var page = 1;
		if ($('#dh_select_page').length > 0) {
			page = $('#dh_select_page').val();
		}
		return {'page': page, 'search_text': search_text, 'search_tables_and_fialds_names': search_tables_and_fialds_names}

	}

});
