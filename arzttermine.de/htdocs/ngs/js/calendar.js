AT._defer(
    (function() {
        var calendarChanged = 0;
        
        function _sendRequest(_url, postData) {
            if (_url && postData) {
        
                $.ajax({
                    url: _url,
                    type: 'post',
                    data: postData,
                    context: document.body
                })
                .done(function (data) {
                    location.reload();
                });
            }
        }
        
        function getPopup(data, callBack) {
            if (data) {
                callBack = callBack || $.noop;
        
                $.ajax({
                    url: '/aerzte/data?type=calendar_appointment',
                    type: 'get',
                    data: data,
                    context: document.body
                })
                .done(function (t) {
                    callBack(t);
                });
            }
        }
        
        /* Methods for IE8 because it's completely trash */
        if (!Function.prototype.bind) {
            Function.prototype.bind = function (oThis) {
                if (typeof this !== "function") {
                    // closest thing possible to the ECMAScript 5 internal IsCallable function
                    throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
                }
        
                var aArgs = Array.prototype.slice.call(arguments, 1),
                    fToBind = this,
                    fNOP = function () {
                    },
                    fBound = function () {
                        return fToBind.apply(this instanceof fNOP && oThis
                            ? this
                            : oThis,
                            aArgs.concat(Array.prototype.slice.call(arguments)));
                    };
        
                fNOP.prototype = this.prototype;
                fBound.prototype = new fNOP();
        
                return fBound;
            };
        }
        
        if (!Array.prototype.indexOf) {
            Array.prototype.indexOf = function (obj, start) {
                for (var i = (start || 0), j = this.length; i < j; i++) {
                    if (this[i] === obj) {
                        return i;
                    }
                }
                return -1;
            }
        }
        
        function parseISO8601(dateStringInRange) {
            var isoExp = /^\s*(\d{4})-(\d\d)-(\d\d)\s*$/,
                date = new Date(NaN), month,
                parts = isoExp.exec(dateStringInRange);
        
            if(parts) {
                month = +parts[2];
                date.setFullYear(parts[1], month - 1, parts[3]);
                if(month != date.getMonth() + 1) {
                    date.setTime(NaN);
                }
            }
            return date;
        }
        /* ======================================================================================================================= */
        
        $(document).ready(function () {
            initCalendar();
        });
        
        var oneMinuteHeight = 1;
        
        function initCalendar() {
            
            //checkbox for show/hide booking blocks in calendar
            $("#show_bookings_checkbox").change(function () {
                var bookingBlocks = $("#calendar").find('div[block_type="booking"]');
                if ($(this).is(":checked")) {
                    bookingBlocks.show();
                } else {
                    bookingBlocks.hide();
                }
            });
            
            //adding doctors existing appointment blocks to calendar
            for (var i = 0; i < appointments_json.length; i++) {
                addAppointmentBlock(appointments_json[i]);
            }
            //adding doctor booking blocks to calendar(if logged in with admin)
            if (is_admin === '1') {
                for (var j = 0; j < bookings_json.length; j++) {
                    addBookingBlock(bookings_json[j]);
                }
            }
            //doctor fixed week edit
            var fixedWeekCheck = $('#fixed_week');
            fixedWeekCheck.click(function () {
                var fixedWeekCheckBox = $(this);
                var fixedWeekValue = fixedWeekCheckBox.is(":checked");
                var dialogContent = "";
                if (fixedWeekValue) {
                    dialogContent = "<div class='dialogContent'><span>!</span>Kalender in 'statische Termine' umstellen?</div>";
                } else {
                    dialogContent = "<div class='dialogContent'><span>!</span>Kalender in 'dynamische Termine' umstellen?</div>";
                }
        
                $(dialogContent).dialog({
                    modal: true,
                    buttons: [
                        {
                            text: "Ja",
                            click: function () {
                                saveCalendar("", fixedWeekValue);
                                $(this).remove();
                            }
                        },
                        {
                            text: "Nein",
                            click: function () {
                                if (fixedWeekValue) {
                                    fixedWeekCheckBox.prop("checked", false);
                                } else {
                                    fixedWeekCheckBox.prop("checked", true);
                                }
                                $(this).remove();
                            }
                        }
                    ],
                    close: function () {
                        $(this).remove();
                    },
                    width: 568,
                    title: "Achtung",
                    draggable: false
                });
            });
            //adding new appointment block functionality
            $("#calendar").mousedown(function (e) {
                var target = $(e.target);
                if (target.hasClass("calendar_time_block")) {
                    var appointmentBlock = $('<div class="calendar_appointment_block"></div>');
                    var calendarColumn = target.closest('.calendar_column');
                    calendarColumn.append(appointmentBlock);
                    var divTop = e.pageY;
                    appointmentBlock.css("top", divTop - $(this).offset().top + "px");
                    var $body = $('body');
                    $body.mousemove(function (e) {
                        if (typeof document.getSelection == 'function') {
                            document.getSelection().removeAllRanges();
                        }
                        if (e.pageY - divTop > 0 && parseInt(appointmentBlock.css("top"), 10) + parseInt(appointmentBlock.css("height"), 10) < $("#calendar").height()) {
                            appointmentBlock.css("height", e.pageY - divTop + "px");
                        }
                    });
                    $body.mouseup(function () {
                        if (appointmentBlock.height() < default_increment) {
                            appointmentBlock.height(default_increment);
                        }
                        var start_at = roundTimeByDefaultIncrement(getTimeByTop(parseInt(appointmentBlock.css("top"), 10)));
                        var end_at = roundTimeByDefaultIncrement(getTimeByTop(parseInt(appointmentBlock.css("top"), 10) + parseInt(appointmentBlock.css("height"), 10)));
                        var columnIdString = calendarColumn.attr('id');
                        var columnId = parseInt(columnIdString.substring(columnIdString.lastIndexOf("_") + 1), 10);
                        $body.unbind('mousemove').unbind('mouseup');
                        var params = {
                            start_at: start_at,
                            end_at: end_at,
                            start_date: start_date,
                            column_id: columnId,
                            user_id: user_id,
                            doctor_tt_names: doctorAllTreatmentTypeNames,
                            doctor_tt_ids: doctorAllTreatmentTypeIds,
                            selected_tt_ids: doctorAllTreatmentTypeIds,
                            is_admin: is_admin
                        };
        
                        getPopup(params, newAppointmentPopUp.bind(appointmentBlock));
                    });
                }
            });
        
            $("#save_calendar").click(saveCalendar.bind('', '', ''));
        
            $("#repeat_until").datepicker({
                dateFormat: 'dd.mm.yy',
                defaultDate: "+1w",
                minDate: new Date(next_monday_date)
            });
        }
        
        function saveCalendar(pageForCallback, changeFixedWeekTo) {
            calendarChanged = 0;
            var appointmentsArray = getAllAppointmentsJson();
            var repeatUntilDate = $("#repeat_until").val();
            var postData = {'calendar_data': JSON.stringify(appointmentsArray), "user_id": user_id, "start_date": start_date, "end_date": end_date, "call_back_page": pageForCallback};
            if (typeof repeatUntilDate !== 'undefined' && repeatUntilDate.length > 0) {
                postData.repeat_until_date = repeatUntilDate;
            }
            if (changeFixedWeekTo.length !== 0) {
                postData.fixed_week = changeFixedWeekTo;
            }
            _sendRequest('/aerzte/termine', postData);
        }
        
        /**
         * showing a form in a dialog for creating new appointment
         * @param {string} data from ajax response
         */
        
        function newAppointmentPopUp(data) {
            var appointmentBlock = $(this);
            var dialogContent = "<div>" + data + "</div>";
            var dialogBox = $(dialogContent).dialog({
                modal: true,
                buttons: [
                    {
                        text: "Termin speichern",
                        "class": "cta-button",
                        click: function () {
                            $(this).dialog("close");
                            var form = $(this).find("form");
                            saveApp(form);
                            appointmentBlock.remove();
                        }
                    },
                    {
                        text: "Entfernen",
                        click: function () {
                            $(this).dialog("close");
                            appointmentBlock.remove();
                        }
                    }
                ],
                close: function () {
                    appointmentBlock.remove();
                    $(this).remove();
                },
                width: 568,
                title: "Termin eintragen",
                dialogClass: "appointment-dialog",
                draggable: false
            });
            if (is_admin === '1') {
                $(".ui-dialog").width(780);
            }
            dialogBox.tabs();
        }
        
        //adding appointment block corresponding to new/edit form
        function saveApp(form) {
            calendarChanged = 1;
            var start_at = form.find('[name="start_at"]').val();
            var end_at = form.find('[name="end_at"]').val();
            if (start_at > end_at || start_at < calendar_start_time || end_at > calendar_end_time) {
                return false;
            }
            if (!checkTimeFormat(start_at)) {
                return false;
            }
            if (!checkTimeFormat(end_at)) {
                return false;
            }
            var block_in_minutes = form.find('[name="block_in_minutes"]').val();
            var date = form.find('[name="date"]').val();
            var columnId = getColumnIdByDate(date);
            var checkedWeekdays = form.find('[name="weekday"]:checked');
            var checkedTtIds = form.find('[name="treatment_type_id"]:checked');
            var checkedInsuranceIds = form.find('[name="insurance_types"]:checked');
            var location_id = form.find('[name="location_id"]').val();
            if ((getMinutesBetweenTimes(start_at, end_at) % block_in_minutes) !== 0) {
                end_at = addMinutesToTime(end_at, (block_in_minutes - getMinutesBetweenTimes(start_at, end_at)) % block_in_minutes);
            }
            var columnIds = [];
            checkedWeekdays.each(function () {
                if ($(this).val() != columnId) {
                    columnIds.push($(this).val());
                }
            });
            var ttIds = [];
            checkedTtIds.each(function () {
                ttIds.push($(this).val());
            });
            if (ttIds.length === 0 && is_admin === '1') {
                return false;
            }
            ttIds = ttIds.join();
        
            var insurance_types = [];
            checkedInsuranceIds.each(function () {
                insurance_types.push($(this).val());
            });
            if (insurance_types.length === 0) {
                return false;
            }
            insurance_types = insurance_types.join();
            var appointment_json = '';
            for (var i = 0; i < columnIds.length; i++) {
                var _date = getDateByColumnId(parseInt(columnIds[i], 10));
                appointment_json = createAppointmentJson(start_at, end_at, block_in_minutes, _date, ttIds, insurance_types, location_id);
                if (!checkIntersection(appointment_json, getAllAppointmentsJson())) {
                    addAppointmentBlock(appointment_json);
                }
            }
            appointment_json = createAppointmentJson(start_at, end_at, block_in_minutes, date, ttIds, insurance_types, location_id);
            if (!checkIntersection(appointment_json, getAllAppointmentsJson())) {
                addAppointmentBlock(appointment_json);
            } else {
                return false;
            }
            return true;
        }
        
        /**
         * showing a form in a dialog for editing existing appointment
         * @param {string} data from ajax response
         */
        function editAppointmentPopUp(data) {
            var appointmentBlock = $(this);
            var dialogContent = "<div>" + data + "</div>";
            var dialogBox = $(dialogContent).dialog({
                modal: true,
                buttons: [
                    {
                        text: "Termin speichern",
                        "class": "cta-button",
                        click: function () {
                            $(this).dialog("close");
                            var form = $(this).find("form");
                            var oldAppointmentBlock = appointmentBlock.remove();
                            if (!saveApp(form)) {
                                addAppointmentBlock(getAppointmentJsonFromAppointmentBlock(oldAppointmentBlock));
                            }
                        }
                    },
                    {
                        text: "Entfernen",
                        click: function () {
                            calendarChanged = 1;
                            $(this).dialog("close");
                            appointmentBlock.remove();
                        }
                    }
                ],
                close: function () {
                    $(this).remove();
                },
                width: 780,
                title: "Termin bearbeiten",
                dialogClass: "appointment-dialog",
                draggable: false
            });
            dialogBox.tabs();
        }
        
        function setDivBackgroundColorByLocationId(jQueryDivObject, location_id) {
            // Values from _colors.scss
            var colors = ["#0090D6", '#F5780A', '#F5F5F5'];
            var color = colors[location_ids.indexOf(location_id)];
            jQueryDivObject.css('background-color', color);
        }
        
        
        function addAppointmentBlock(appointment) {
            var weekday_height = $('.calendar_weekday').outerHeight();
            
            // Start, end at ----------------------------------
            var start_at = appointment.start_at;
            var startAtArr = start_at.split(':');
            if (startAtArr.length === 3) {
                start_at = startAtArr[0] + ':' + startAtArr[1];
            }
            var end_at = appointment.end_at;
            var endAtArr = end_at.split(':');
            if (endAtArr.length === 3) {
                end_at = endAtArr[0] + ':' + endAtArr[1];
            }
            
            // Durations --------------------------------------
            var duration_label = "";
            var block_in_minutes = appointment.block_in_minutes || getMinutesBetweenTimes(start_at, end_at);
            if (block_in_minutes >= 60) {
                duration_label = " [" + (block_in_minutes / 60) + " Std]";
            } else {
                duration_label = " [" + block_in_minutes + " Min]";
            }
            
            var date = appointment.date;
            var ttIds = appointment.treatment_type_ids;
            var insurance_types = appointment.insurance_types;
            var columnId = getColumnIdByDate(appointment.date);
            var location_id = appointment.location_id;
            var $calendarColumn = $("#calendar_column_" + columnId);
            var minutes = getMinutesBetweenTimes(start_at, end_at);
            var blockDivHeight = minutes * oneMinuteHeight;
            var blockDivTop = getMinutesBetweenTimes(start_at, calendar_start_time) * oneMinuteHeight + weekday_height;
            
            var $appointmentBlock = $('<div class="calendar_appointment_block checkered">')
                .css({
                    top: (blockDivTop) + "px",
                    height: blockDivHeight + "px",
                    position: "absolute"
                })
                .append(
                    $('<div class="calendar_remove_parent">X</div>').click(function(e) {
                        e.stopPropagation();
                        $(this).parent().remove();
                    }))
                .append('<div class="appointment_block_header">' + start_at + '-' + end_at + duration_label + '</div>')
                .appendTo($calendarColumn);
        
            // Add data attributes to the appointment block element :S
            addDataToAppointmentBlock($appointmentBlock, start_at, end_at, block_in_minutes, date, ttIds, insurance_types, location_id);
            
            // Drag/drop "config" -------------------------------
            var xStep = $calendarColumn.find(".calendar_time_block").outerWidth();
            var yStep = 15; // 15mins
        
            // AppointmentBlock events --------------------------
            $appointmentBlock
                .click(function () {
                    var params = {
                        start_at: start_at,
                        end_at: end_at,
                        block_in_minutes: appointment.block_in_minutes || block_in_minutes,
                        start_date: start_date,
                        column_id: columnId,
                        user_id: user_id,
                        doctor_tt_names: doctorAllTreatmentTypeNames,
                        doctor_tt_ids: doctorAllTreatmentTypeIds,
                        selected_tt_ids: ttIds,
                        selected_insurance_types: insurance_types,
                        location_id: location_id,
                        is_admin: is_admin
                    };
                    getPopup(params, editAppointmentPopUp.bind(this));
                })
                .resizable({
                    containment: "parent",
                    handles: "n, s",
                    resize: function () {
                        $appointmentBlock.css('z-index', 5);
                    },
                    stop: function () {
                        var $this = $(this);
                        var top = $this.css("top");
                        var height = $this.css("height");
                        var _startAt = roundTimeByDefaultIncrement(getTimeByTop(top));
                        var _endAt = roundTimeByDefaultIncrement(getTimeByTop(parseInt(top, 10) + parseInt(height, 10)));
                        var block_in_minutes = getMinutesBetweenTimes(_startAt, _endAt);
                        var oldAppointmentBlock = $this.remove();
                        var appointment_json = createAppointmentJson(_startAt, _endAt, block_in_minutes, date, ttIds, insurance_types, location_id);
                        if (checkIntersection(appointment_json, getAllAppointmentsJson()) || parseInt(top, 10) < parseInt($(".calendar_weekday").outerHeight(), 10)) {
                            addAppointmentBlock(getAppointmentJsonFromAppointmentBlock(oldAppointmentBlock));
                        } else {
                            addAppointmentBlock(appointment_json);
                            calendarChanged = 1;
                        }
                    }
                })
                .draggable({
                    containment: $("#calendar"),
                    revert: "valid",
                    snap: ".calendar_time_block",
                    grid: [xStep, yStep],
                    start: function () {
                        $appointmentBlock.css('z-index', 5);
                    },
                    stop: function (e, ui) {
                        var $this = $(this);
                        var columnsCountToRight = parseInt((ui.position.left - ui.originalPosition.left) / xStep, 10);
                        columnId = parseInt(columnId + columnsCountToRight, 10);
                        var _date = getDateByColumnId(columnId);
                        var top = $this.css("top");
                        var height = $this.css("height");
                        var _startAt = roundTimeByDefaultIncrement(getTimeByTop(top));
                        var _endAt = roundTimeByDefaultIncrement(getTimeByTop(parseInt(top, 10) + parseInt(height, 10)));
                        var oldAppointmentBlock = $this.remove();
                        var appointment_json = createAppointmentJson(_startAt, _endAt, block_in_minutes, _date, ttIds, insurance_types, location_id);
                        if (checkIntersection(appointment_json, getAllAppointmentsJson()) || parseInt(top, 10) < parseInt($(".calendar_weekday").outerHeight(), 10)) {
                            addAppointmentBlock(getAppointmentJsonFromAppointmentBlock(oldAppointmentBlock));
                        } else {
                            addAppointmentBlock(appointment_json);
                            calendarChanged = 1;
                        }
                    }
                });
        
            setDivBackgroundColorByLocationId($appointmentBlock, location_id);
            
            return $appointmentBlock;
        }
        
        /**
         * @param {json} booking_json
         * @returns {bookingBlock} div
         */
        function addBookingBlock(booking_json) {
            var appointmentStartAt = booking_json["appointmentStartAt"];
            var appointmentDate = appointmentStartAt.split(" ")[0];
            var appointmentStartTime = appointmentStartAt.split(" ")[1];
            var columnId = getColumnIdByDate(appointmentDate);
            var $calendarColumn = $("#calendar_column_" + columnId);
            var blockDivTop = getMinutesBetweenTimes(appointmentStartTime, calendar_start_time) * oneMinuteHeight + 48;
            var blockDivHeight = 20 * oneMinuteHeight;
            var bookingBlock = 
                $('<div class="calendar_booking_block checkered"></div>')
                    .css({
                        top: blockDivTop + "px",
                        height: blockDivHeight + "px",
                        backgroundColor: "#9E0101",
                        zIndex: 10,
                        fontWeight: "bold"
                    })
                    .attr("block_type", "booking")
                    .append("<span>" + appointmentStartTime + "</span>");
            
            bookingBlock.tooltip({
                items: "div",
                content: getBookingBlockTooltipContent(booking_json),
                position: {
                    my: "center bottom-5",
                    at: "center top"
                }
            });
        
            $calendarColumn.append(bookingBlock);
        }
        
        /*
         * 
         * @param {json} booking_json
         * @returns {String} div content string, corresponding to booking_json data
         */
        function getBookingBlockTooltipContent(booking_json) {
            var firstName = booking_json["firstName"];
            var lastName = booking_json["lastName"];
            var email = booking_json["email"];
            return "<div>First Name:" + firstName + "<br>Last Name:" + lastName + "<br>Email:" + email + " </div>";
        }
        
        /*
         * 
         * @param {String} time1 in "hh:mm:ss" format
         * @param {type} time2 in "hh:mm:ss" format
         * @returns {int} minutes between time1 and time2(always positive)
         */
        function getMinutesBetweenTimes(time1, time2) {
            var hour1 = parseInt(time1.split(":")[0], 10);
            var minute1 = parseInt(time1.split(":")[1], 10);
            var hour2 = parseInt(time2.split(":")[0], 10);
            var minute2 = parseInt(time2.split(":")[1], 10);
            return Math.abs(60 * (hour2 - hour1) + minute2 - minute1);
        }
        
        /*
         * 
         * @param {int/String} top relative to calendar 
         * @returns {String} corresponding time in calendar(format:"hh:mm:ss")
         */
        function getTimeByTop(top) {
            var height = parseInt(top, 10) - parseInt($(document).find("div.calendar_weekday").outerHeight(), 10);
            var startHour = parseInt(calendar_start_time.split(":")[0], 10);
            var startMinute = parseInt(calendar_start_time.split(":")[1], 10);
            var minutes = startMinute + parseInt(height / oneMinuteHeight, 10);
            var hour = startHour + Math.floor(minutes / 60);
            var minute = startMinute + minutes % 60;
            minute -= minute % 5;
        
            if (minute < 10) {
                minute = "0" + minute;
            }
            if (hour < 10) {
                hour = "0" + hour;
            }
            return hour + ":" + minute;
        }
        
        function saveCalendarCallback(page) {
            location.href = page;
        }
        
        /*
         * @param {String} time in "hh:mm:ss" format
         * @param {String/Int} minutes
         * @returns {String} corresponding time(format:"hh:mm:ss"), after adding minutes to time
         */
        function addMinutesToTime(time, minutes) {
            var hour = parseInt(time.split(":")[0], 10);
            var minute = parseInt(time.split(":")[1], 10);
            var inMinutes = 60 * hour + minute + parseInt(minutes, 10);
            hour = Math.floor(inMinutes / 60);
            minute = inMinutes % 60;
            if (minute < 10) {
                minute = "0" + minute;
            }
            if (hour < 10) {
                hour = "0" + hour;
            }
            return hour + ":" + minute;
        }
        
        /*
         * @param {Stirng/Int} columnId(1,2,3,4,5)
         * @returns {String} date, corresponding to current calendars columnId
         */
        function getDateByColumnId(columnId) {
            var startDate = new Date(parseISO8601(start_date));
            startDate.setDate(startDate.getDate() + columnId - 1);
            var year = startDate.getFullYear();
            var month = parseInt(startDate.getMonth(), 10) + 1;
            if (month < 10) {
                month = "0" + month;
            }
            var day = startDate.getDate();
            if (day < 10) {
                day = "0" + day;
            }
            return year + "-" + month + "-" + day;
        }
        
        /**
         * @param  date string
         * @returns type int (1 or 2 or 3 or 4 or 5)
         */
        function getColumnIdByDate(date) {
            var dateJs = new Date(parseISO8601(date));
            return dateJs.getDay();
        }
        
        function createAppointmentJson(start_at, end_at, block_in_minutes, date, ttIds, insurance_types, location_id) {
            return {
                start_at: start_at,
                end_at: end_at,
                block_in_minutes: block_in_minutes,
                date: date,
                treatment_type_ids: ttIds,
                insurance_types: insurance_types,
                location_id: location_id,
                user_id: user_id
            };
        }
        
        /**
         * @param {String} time (in "hh:mm:ss" format)
         * @returns {String} rounded time in "hh:mm:ss" format
         */
        function roundTimeByDefaultIncrement(time) {
            var hour = parseInt(time.split(":")[0], 10);
            var minute = parseInt(time.split(":")[1], 10);
            minute = Math.round(minute / default_increment) * default_increment;
            hour += Math.floor(minute / 60);
            minute = minute % 60;
            if (hour < 10) {
                hour = "0" + hour;
            }
            if (minute < 10) {
                minute = "0" + minute;
            }
            return hour + ":" + minute;
        }
        
        /**
         * @param {json} appointment_json
         * @param {Array} all_apointments_json
         * @returns {Boolean} true if appointment_json has intersection with one or more
         * appointments from all_apointments_json
         */
        function checkIntersection(appointment_json, all_apointments_json) {
            var date = appointment_json.date;
            var columnId = getColumnIdByDate(date);
            var start_at = appointment_json.start_at;
            var _startMinutes = convertTimeToMinutes(start_at);
            var end_at = appointment_json.end_at;
            var _endMinutes = convertTimeToMinutes(end_at);
            for (var index in all_apointments_json) {
                var _columnId = getColumnIdByDate(all_apointments_json[index]["date"]);
                if (columnId === _columnId) {
                    var startMinutes = convertTimeToMinutes(all_apointments_json[index].start_at);
                    var endMinutes = convertTimeToMinutes(all_apointments_json[index].end_at);
                    if ((startMinutes < _endMinutes && startMinutes > _startMinutes) ||
                        (endMinutes < _endMinutes && endMinutes > _startMinutes) ||
                        (_startMinutes < endMinutes && _startMinutes > startMinutes) ||
                        (_endMinutes < endMinutes && _endMinutes > startMinutes) ||
                        (_endMinutes === endMinutes && _startMinutes === startMinutes)) {
                        return true;
                    }
                }
            }
            return false;
        }
        
        function convertTimeToMinutes(time) {
            var hour = parseInt(time.split(":")[0], 10);
            var minute = parseInt(time.split(":")[1], 10);
            return 60 * hour + minute;
        }
        
        function addDataToAppointmentBlock(appointmentBlock, start_at, end_at, block_in_minutes, date, ttIds, insurance_types, location_id) {
            appointmentBlock.attr({
                start_at: start_at,
                end_at: end_at,
                block_in_minutes: block_in_minutes,
                date: date,
                treatment_type_ids: ttIds,
                insurance_types: insurance_types,
                location_id: location_id,
                user_id: user_id
            });
        }
        
        function getAllAppointmentsJson() {
            var appointmentsArray = [];
            $("#calendar").find("div.calendar_appointment_block").each(function () {
                var start_at = $(this).attr("start_at");
                var end_at = $(this).attr("end_at");
                var block_in_minutes = $(this).attr("block_in_minutes");
                var date = $(this).attr("date");
                var ttIds = $(this).attr("treatment_type_ids");
                var insurance_types = $(this).attr("insurance_types");
                var location_id = $(this).attr("location_id");
                var appointment_json = createAppointmentJson(start_at, end_at, block_in_minutes, date, ttIds, insurance_types, location_id);
                appointmentsArray.push(appointment_json);
            });
            return appointmentsArray;
        }
        
        function getAppointmentJsonFromAppointmentBlock(appointmentBlock) {
            var start_at = appointmentBlock.attr("start_at");
            var end_at = appointmentBlock.attr("end_at");
            var block_in_minutes = appointmentBlock.attr("block_in_minutes");
            var date = appointmentBlock.attr("date");
            var ttIds = appointmentBlock.attr("treatment_type_ids");
            var insurance_types = appointmentBlock.attr("insurance_types");
            var location_id = appointmentBlock.attr("location_id");
            return createAppointmentJson(start_at, end_at, block_in_minutes, date, ttIds, insurance_types, location_id);
        }
        
        function checkTimeFormat(time) {
            var timecomponents = time.split(':');
            if (timecomponents.length === 2) {
                if ((parseInt(timecomponents[0], 10) < 24 && parseInt(timecomponents[0], 10) > 0) &&
                    (parseInt(timecomponents[1], 10) < 60 && parseInt(timecomponents[1], 10) >= 0)) {
                    return true;
                }
            }
            return false;
        }
    })
);