$(document).ready(function() {
	$(".datetimepicker").datetimepicker({
		showSecond: true,
		showWeek: true,
		timeFormat: 'hh:mm:ss',
		dateFormat: 'yy-mm-dd'
	});
	$("#check_uncheck_all").click(function(){
		var isChecked = $(this).is(":checked");
		if(isChecked){
			$('input[name="accessible_medical_specialty_ids[]"]').prop('checked',true);
		}else{
			$('input[name="accessible_medical_specialty_ids[]"]').prop('checked',false);
		}
	});
	$("#search_doctors").bind("keydown", function(event) {
		if (event.keyCode === $.ui.keyCode.TAB &&
				$(this).data("ui-autocomplete").menu.active) {
			event.preventDefault();
		}
	}).autocomplete({
		minLength: 0,
		source: function(request, response) {
			response($.ui.autocomplete.filter(
					doctorsNamesIdsJson, extractLast(request.term)));
		},
		focus: function() {
			// prevent value inserted on focus
			return false;
		},
		select: function(event, ui) {
			var terms = split(this.value);
			// remove the current input
			terms.pop();
			// add the selected item
			terms.push(ui.item.value);
			// add placeholder to get the comma-and-space at the end
			terms.push("");
			this.value = terms.join(", ");
			return false;
		}
	});
	$("#search_locations").bind("keydown", function(event) {
		if (event.keyCode === $.ui.keyCode.TAB &&
				$(this).data("ui-autocomplete").menu.active) {
			event.preventDefault();
		}
	}).autocomplete({
		minLength: 0,
		source: function(request, response) {
			response($.ui.autocomplete.filter(
					locationsNamesIdsJson, extractLast(request.term)));
		},
		focus: function() {
			// prevent value inserted on focus
			return false;
		},
		select: function(event, ui) {
			var terms = split(this.value);
			// remove the current input
			terms.pop();
			// add the selected item
			terms.push(ui.item.value);
			// add placeholder to get the comma-and-space at the end
			terms.push("");
			this.value = terms.join(", ");
			return false;
		}
	});
});

function split(val) {
	return val.split(/,\s*/);
}
function extractLast(term) {
	return split(term).pop();
}



