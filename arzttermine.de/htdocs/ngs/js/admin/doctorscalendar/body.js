$(document).ready(function() {
    $('#f_selected_integration').change(function()
    {
        intId = $('#f_selected_integration').val();
        $(location).attr('href', '?integration_id=' + intId);
    });
    $('#f_selected_location').change(function()
    {
        intId = $('#f_selected_integration').val();
        locId = $('#f_selected_location').val();
        intId = $('#f_selected_integration').val();
        $(location).attr('href', '?integration_id=' + intId+'&location_id=' + locId);
    });
    $('#f_selected_doctor').change(function()
    {
        intId = $('#f_selected_integration').val();
    locId = $('#f_selected_location').val();
    docId = $('#f_selected_doctor').val();
        $(location).attr('href', '?integration_id=' + intId + '&' + 'location_id=' + locId + '&' + 'user_id=' + docId);
    });
	$("#search_location").autocomplete({
		source: availableTags,
		select: function(event, ui) {
			var selectedLocation = ui.item;
			locId = selectedLocation.value;
			intId = $('#f_selected_integration').val();
			$(location).attr('href', '?integration_id=' + intId+'&location_id=' + locId);
			return false;
		}
	});
});
