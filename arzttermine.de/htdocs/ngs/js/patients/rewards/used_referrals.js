$(document).ready(function() {
	$(".order_by").click(function() {
		var order_by_field_name = $(this).attr("order_by");
		var order_by_direction = $(this).attr("direction");
		_sendRequest('/dyn/patients_referrals/used_referrals', 'page='+currentPage+'&order_by=' + order_by_field_name + '&direction=' + order_by_direction, orderByCallback);
	});

	$('#rewards_content_page_1').find('a[go_to_page]').click(function() {
		var go_to_page = $(this).attr('go_to_page');
		_sendRequest('/dyn/patients_referrals/used_referrals', 'page=' + go_to_page+'&order_by='+currentOrderBy+'&direction='+currentDirection, usedReferralsRequestCallback);
	});

});

function orderByCallback(response) {
	$("#rewards_content_page_1").html(response);
}

function usedReferralsRequestCallback(response) {
	$('#rewards_content_page_1').html(response);
}

