$(document).ready(function() {
	$(".order_by").click(function() {
		$('#rft1_order_by').val($(this).attr("order_by"));
		$('#rft1_order_direction').val($(this).attr("direction"));
		_sendRequest('/dyn/patients_referrals/referred_friends_table1', getPageUrlParams(), callBack);
	});

	$('#referred_friends_table1').find('a[go_to_page]').click(function() {		
		_sendRequest('/dyn/patients_referrals/referred_friends_table1', getPageUrlParams(), callBack);
	});
	$('#rft1_paging_select').change(function() {
		_sendRequest('/dyn/patients_referrals/referred_friends_table1', getPageUrlParams(), callBack);
	});

	$(".has-tooltip").tooltip({
		tooltipClass: "patient-account-table",
		position: {
			my: "center bottom-15",
			at: "center top",
			using: function(position, feedback) {
				$(this).css(position);
				$("<div>")
						.addClass("arrow")
						.addClass(feedback.vertical)
						.addClass(feedback.horizontal)
						.appendTo(this);
			}
		}
	});

});

function callBack(response) {
	
	$("#referred_friends_table1").html(response);
}


function getPageUrlParams() {
	var page = $('#rft1_paging_select').val();
	var order_by = $('#rft1_order_by').val();
	var direction = $('#rft1_order_direction').val();
	var ret = {'page': page, 'order_by': order_by, 'direction': direction};	
	return $.param(ret);

}



