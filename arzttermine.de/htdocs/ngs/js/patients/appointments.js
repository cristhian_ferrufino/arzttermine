$(function() {
    var $popup = $('#referral_tooltip-loadtime');

    function _sendRequest(_url, postData, callback) {
        if (_url && postData) {
            callback = callback || $.noop;

            $.ajax({
                url: _url,
                type: 'post',
                data: postData,
                context: document.body
            })
            .done(function (data) {
                callback(data);
            });
        }
    }
    
    var cancelResult = function(response) {
        var $this        = $(this),
            status       = response.status,
            type         = $this.data('booking_type'),
            $messageDiv  = $('<div class="message"></div>'),
            bookingsContainer;
        
        if (type == 'appointment_request') {
            bookingsContainer = $('#appointment_request_bookings_container');
        } else if (type == 'upcoming') {
            bookingsContainer = $('#upcoming_bookings_container');
        }
        
        bookingsContainer.prepend($messageDiv);
        
        if (status == 'success') {
            $messageDiv.html('Dieser Termin wurde bereits abgesagt, möchten Sie einen neuen Termin <a target="_blank" href="/">buchen</a>?');
            $this
                .val('erneut buchen')
                .off('click')
                .on('click', function(e) {
                    e.preventDefault();
                    window.open('/');
                })
                .closest('tr').find('td.booking_status').html(response.booking_status);
        } else {
            $messageDiv.html(response.message);
        }

        $popup.fadeOut();
        location.reload();
    };
    
	$('input.cancel').on('click', function(e) {
        e.preventDefault();
        
        var $this = $(this),
		    bookingStatus    = $this.data('booking_status'),
		    integrationId    = $this.data('integration_id'),
		    timestamp        = parseInt($this.data('timestamp'), 10),
		    currentTimestamp = parseInt(Math.round(new Date().getTime() / 1000), 10);

		if (bookingStatus == 4) {
			alert("Dieser Termin wurde bereits abgesagt.");
		} else if (bookingStatus == 5) {
			alert("Leider ist bei der Buchung ein Fehler aufgetreten.");
		} else if (timestamp - currentTimestamp < 86400 && integrationId == 1) {
			alert("Da der Termin in weniger als 24 Stunden stattfindet bitten wir Sie sich an unseren Kundenservice zu wenden:  0800 2222 133");
		} else {
			var doctorFullName = $this.data('doctor_name'),
			    date = $this.data('date'),
			    time = $this.data('time'),
			    answer = confirm('Sind Sie sicher, dass Sie Ihren Termin mit ' + doctorFullName + ' am ' + date + ' um ' + time + ' Uhr absagen möchten?');
            
            // Compile the data to send via AJAX
            var data = $.extend({ }, $this.data(), { type: 'cancel_appointment' });
            
			if (answer) {
                $popup.fadeIn();
				_sendRequest('/patienten/data', data, cancelResult.bind(this));
			}
		}
	});
});