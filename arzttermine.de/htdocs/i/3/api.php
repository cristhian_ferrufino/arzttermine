<?php
/**
 * api for simplimed
 *
 * @author Axel Kuzmik <axel@kuzmik.de>
 * 
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/../include/bootstrap.php';

// START ****************************************************
$action = getParam('action','');
$data = getParam('data');
if (!is_array($data)) $data = array();

switch ($action) {
	/* *******************************************
	 * PROVIDER
	 * ******************************************* */
	case 'upload-appointments':
		/*
		 * Appointments come with this syntax
		 * 20120319174229;C;{3C29F407-76DA-457D-ADAF-42C03308B049};20.03.2012;20.03.2012;14:15:00;15:30:00;;;;;;;;;;;;;;113#
		*/
		$format = getParam('format','parse');
		$user_id = getParam('user_id');
		$location_id = getParam('location_id');

		if ($format=='print_r') {
			echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head><body><pre>';
			print_r($_html);
			echo '</pre></body></html>';
		} elseif ($format=='json_debug') {
			echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head><body>';
			echo str_replace('{"id"','<br />{"id"',json_encode($_html));
			echo '</body></html>';
		} elseif ($format=='json') {
			header('Content-Type: application/json');
			echo json_encode($_html);
			exit;
		} elseif ($format=='html') {
			header('Content-Type: text/html');
			$_target_path = "/www/www.arzttermine.de/tmp/simplimed/";
			$_target_path = $_target_path . date('Ymd-His_') . basename( sanitize_file_name($_FILES['upload']['name']));
			if (move_uploaded_file($_FILES['upload']['tmp_name'], $_target_path)) {
				echo '<span style="color:green;">The file has been uploaded</span><br />filename: '.basename( $_FILES['upload']['name']).'<br />filesize: '.filesize($_target_path).' bytes<br />user_id: '.$user_id.'<br />location_id: '.$location_id;
			} else{
				echo "There was an error uploading the file, please try again!";
			}
			echo '<br />'.$_FILES['upload']['tmp_name'];
			echo '<br />'.$_target_path;
			print_r($_FILES);
			exit;
		} elseif ($format=='parse') {
			$filename = $_FILES['upload']['name'];
			$tmp_filename = $_FILES['upload']['tmp_name'];
			if (is_readable($tmp_filename)) {
				$_content = file_get_contents($tmp_filename);
				// parse the simplimed_user_id and simplimed_location_id
				if (preg_match("/([a-z0-9]+)+_([a-z0-9]+)/i", $filename, $matches)) {
					$_simplimed_location_id = $matches[1];
					$_simplimed_user_id = $matches[2];
				}
#				print_r($_FILES);
#				echo $_content;
			}

			exit;
		} elseif ($format=='error') {
			header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
			exit;
		}
		break;
	case 'get-bookings':
		include_once $GLOBALS['CONFIG']['INTEGRATIONS'][3]['CLASS_FILENAME'];
		$simplimed_user_id = getParam('simplimed_user_id');
		$simplimed_password = getParam('simplimed_password');
		if (!Integration_3_Data::checkCredentials($simplimed_user_id, $simplimed_password)) {
			echo "ERROR: AUTHENTICATION";
		} else {
			echo "A;{C6518B14-841A-4EF4-8361-0A7B08B85D66};06.11.2011;06.11.2011;10:15:00;11:15:00;Blutentnahme;Herrn;;Anton;Muster; Berliner Allee 25;41464;Neuss;;23.12.1985;;(0171) 565656;anton_m@gmx.de;Test;197#";
			exit;
		}
		break;
}