<?php

use Arzttermine\User\User;
use NGS\Managers\PatientsManager;

function get_data_user_id($data = 0)
{
    $id = $data['user_id'];
    $user_type_id = $data['user_type'];
    $_html = '';
    if ($id) {
        switch ($user_type_id) {
            case LOGIN_DOCTOR_TYPE:
                $_user = new User($id);
                if ($_user->isValid()) {
                    $_link = $GLOBALS['CONFIG']['URL_HTTP_LIVE'] . '/' . $_user->getSlug();
                    $_html .= '<a href="' . $_link . '" target="_blank">' . $_user->getName() . '</a>';
                } else {
                    $_html .= '<span class="red">wrong id: ' . $id . '</span><br />';
                }
                break;
            case LOGIN_PATIENT_TYPE:
                $patientsManager = PatientsManager::getInstance();
                $patient = $patientsManager->getPatientById($id);
                if ($patient) {
                    $_html .= $patient->getFirstName() . ' ' . $patient->getLastName();
                } else {
                    $_html .= '<span class="red">wrong id: ' . $id . '</span><br />';
                }
                break;
            default:
                break;
        }
    }

    return $_html;
}

function get_user_type($data = 0)
{
    $user_type_id = $data['user_type'];
    $user_type = $GLOBALS['CONFIG']['USER_LOGIN_TYPES'][$user_type_id];
    if (isset($user_type)) {
        return $user_type;
    }

    return 'n/a';
}

function get_user_ip($data = 0)
{
    $ip = $data['ip'];
    if (isset($ip)) {
        return $ip;
    }

    return 'n/a';
}

function get_user_operating_system($data = 0)
{
    $os = $data['operating_system'];
    if (isset($os)) {
        return $os;
    }

    return 'n/a';
}

function get_user_browser($data = 0)
{
    $browser = $data['browser'];
    if (isset($browser)) {
        return $browser;
    }

    return 'n/a';
}

function get_user_browser_version($data = 0)
{
    $browser_version = $data['browser_version'];
    if (isset($browser_version)) {
        return $browser_version;
    }

    return 'n/a';
}

$MENU_ARRAY = array(
    "id"               => array("sql" => 1, "name" => "ID", "show" => 1, "edit" => 0, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1),
    "user_id"          => array("sql" => 1, "name" => 'User', "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_user_id", "para" => 1)),
    "user_type"        => array("sql" => 1, "name" => 'User Type', "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_user_type", "para" => 1)),
    "ip"               => array("sql" => 1, "name" => 'IP', "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_user_ip", "para" => 1)),
    "operating_system" => array("sql" => 1, "name" => 'Operating System', "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_user_operating_system", "para" => 1)),
    "browser"          => array("sql" => 1, "name" => 'Browser', "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_user_browser", "para" => 1)),
    "browser_version"  => array("sql" => 1, "name" => 'Browser Version', "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_user_browser_version", "para" => 1)),
    "date"             => array("sql" => 1, "name" => 'Login Date', "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1)
);

$BUTTON = array(
    "delete" => array("func" => "location_admin_delete", "alt_text" => $app->_('Löschen'), "script" => $_SERVER['PHP_SELF'], "gfx" => "delete.png", "action" => "action=delete", "th" => 0, "td" => 1),
    "search" => array("func" => "location_admin_search", "alt_text" => $app->_('Suchen'), "script" => $_SERVER['PHP_SELF'], "gfx" => "search.png", "action" => "action=search", "th" => 1, "td" => 0)
);

$TABLE_CONFIG = array(
    "table_name"  => DB_TABLENAME_LOGIN_HISTORY,
    "alpha_index" => "name",
);
$URL_PARA = array(
    "page"         => 1,
    "alpha"        => '',
    "sort"         => 0,
    "direction"    => 'DESC',
    "num_per_page" => 25,
    "type"         => 'num'
);