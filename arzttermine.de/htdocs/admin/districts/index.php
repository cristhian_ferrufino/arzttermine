<?php
/**
 * Admin: Districts
 *
 * @author Axel Kuzmik <axel@kuzmik.de>
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/../include/bootstrap.php';
require_once "config.php";

/* ************************************************************************************* */

use Arzttermine\Admin\Admin;
use Arzttermine\Application\Application;
use Arzttermine\Maps\GoogleMap;

$admin = new Admin();
$app = Application::getInstance();
$view = $app->getView();
$profile = $app->getCurrentUser();

$view->disableCaching();

/* ************************************************************************************* */

if (!$profile->checkPrivilege('district_admin')) {
    $view->setRef('ContentRight', $app->static_file('access_denied.html'));
    $view->setRef('ContentLeft', $admin->get_navigation());
    $view->display('admin/content_left_right.tpl');
    exit;
}
/* ************************************************************************************* */
function get_list($URL_PARA, $search = 0)
{
    global $admin;

    $_html = $admin->admin_get_list(0, $search, $URL_PARA);

    return $_html;
}

/* ************************************************************************************* */
function new_one($action, $form_data, $URL_PARA)
{
    global $profile, $admin, $view, $app;

    // 1. set defaults
    $form_data["created_by"] = $profile->getId();
    $form_data["created_at"] = $app->now();

    // 2. check
    if (1 > mb_strlen(sanitize_title($form_data["name"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
        $view->addMessage('admin_message_name', 'ERROR', $app->static_text('Name is missing'));
    }
    if (1 > mb_strlen(sanitize_title($form_data["city"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
        $view->addMessage('admin_message_city', 'ERROR', $app->static_text('Name of the city is missing'));
    }

    // if there are checkboxes than set the keys to make sure that deselected checkboxes are beeing set
    $form_data = $admin->set_unselected_checkbox_values($form_data);

    // 3. exit on errors
    if ($view->countMessages()) {
        $view->addMessage('STATUS', 'ERROR', $app->_('Bitte prüfen Sie Ihre Eingaben.'));
        $view->setRef('ContentRight', form_change($action, 0, $URL_PARA, $form_data));

        return $action;
    }
    // 4. sanitize
    $form_data["name"] = sanitize_title($form_data["name"]);
    $form_data["city"] = sanitize_title($form_data["city"]);

    // get the geocode
    $address = $form_data['city'] . ',' . $form_data['name'];
    $coords = GoogleMap::getGeoCoords($address);
    if (!empty($coords)) {
        $form_data['lat'] = $coords['lat'];
        $form_data['lng'] = $coords['lng'];
    } else {
        unset($form_data['lat']);
        unset($form_data['lng']);
    }

    $_id = $admin->admin_set_new($form_data);
    if ($_id > 0) {
        $view->addMessage('STATUS', 'NOTICE', $app->_('The district was created with ID ' . $_id . '.<br /><a href="/admin/districts/index.php?action=edit&id=' . $_id . '">Edit District</a>'));
        $action = 'list';
    } elseif ($_id == -1) {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('the_entry_allready_exists'));
        $view->setRef('ContentRight', form_new($action, $URL_PARA, $form_data));
        $action = 'list';
    } elseif ($_id == -2) {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('name_of_primary_key_is_empty'));
        $view->setRef('ContentRight', form_new($action, $URL_PARA, $form_data));
        $action = 'list';
    } else {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('the_entry_could_not_be_created'));
        $action = 'list';
    }

    return $action;
}

/* ************************************************************************************* */
function change_one($action, $id, $form_data, $URL_PARA)
{
    global $profile, $admin, $view, $app;

    // 1. set defaults
    $form_data["updated_by"] = $profile->getId();
    $form_data["updated_at"] = $app->now();

    // 2. check
    if (1 > mb_strlen(sanitize_title($form_data["name"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
        $view->addMessage('admin_message_name', 'ERROR', $app->static_text('Name is missing'));
    }
    if (1 > mb_strlen(sanitize_title($form_data["city"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
        $view->addMessage('admin_message_city', 'ERROR', $app->static_text('City is missing'));
    }

    // if there are checkboxes than set the keys to make sure that deselected checkboxes are beeing set
    $form_data = $admin->set_unselected_checkbox_values($form_data);

    // 3. exit on errors
    if ($view->countMessages()) {
        $view->addMessage('STATUS', 'ERROR', $app->_('Bitte prüfen Sie Ihre Eingaben.'));
        $view->setRef('ContentRight', form_change($action, $id, $URL_PARA, $form_data));

        return $action;
    }
    // 4. sanitize
    $form_data["name"] = sanitize_title($form_data["name"]);
    $form_data["city"] = sanitize_title($form_data["city"]);

    // if there are checkboxes than set the keys to make sure that deselected checkboxes are beeing set
    $form_data = $admin->set_unselected_checkbox_values($form_data);

    // get the geocode
    $address = $form_data['city'] . ',' . $form_data['name'];
    $coords = GoogleMap::getGeoCoords($address);
    if (!empty($coords)) {
        $form_data['lat'] = $coords['lat'];
        $form_data['lng'] = $coords['lng'];
    } else {
        unset($form_data['lat']);
        unset($form_data['lng']);
    }

    $_result = $admin->admin_set_update($id, $form_data);

    if ($_result == 1) {
        $view->addMessage('STATUS', 'NOTICE', $app->static_text('action_was_successfully'));
        $action = 'list';
    } elseif ($_result == -1) {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('the_entry_allready_exists'));
        $view->setRef('ContentRight', form_change($action, $id, $URL_PARA, $form_data));
        $action = 'list';
    } elseif ($_result == -2) {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('name_of_primary_key_is_empty'));
        $view->setRef('ContentRight', form_change($action, $id, $URL_PARA, $form_data));
        $action = 'list';
    } else {
        $view->addMessage('STATUS', 'WARNING', $app->static_text('no_data_has_been_updated'));
        $view->setRef('ContentRight', form_change($action, $id, $URL_PARA, $form_data));
        $action = 'list';
    }

    return $action;
}

/* ************************************************************************************* */
function ask4delete($id, $URL_PARA)
{
    global $admin;

    $_html = $admin->admin_get_confirm_table($id, $URL_PARA, 'delete');

    return $_html;
}

/* ************************************************************************************* */
function delete_one($id)
{
    global $admin;

    return $admin->admin_set_delete($id);
}

/* ************************************************************************************* */
function form_new($action, $URL_PARA, $error_data = 0)
{
    global $admin, $view;

    $_html = $admin->admin_get_form($action, 0, $error_data, $URL_PARA);

    $view->setFocus('#form_data_name');

    return $_html;
}

/* ************************************************************************************* */
function form_change($action, $id, $URL_PARA, $error_data = 0)
{
    global $admin, $view;

    $_html = $admin->admin_get_form($action, $id, $error_data, $URL_PARA);

    $view->setFocus('#form_data_name');

    return $_html;
}

/* ************************************************************************** */
function show_search_form($action, $search_data, $URL_PARA)
{
    global $admin;

    return $admin->admin_get_form($action, 0, $search_data, $URL_PARA);
}

/* ************************************************************************************** */
/* Anfragen auswerten ******************************************************************* */
//URL_PARA neu bestimmen
foreach ($URL_PARA as $key => $val) {
    if (isset($_REQUEST[$key])) {
        $URL_PARA[$key] = $_REQUEST[$key];
    }
}
$action = getParam("action", 'list');
$form_data = getParam("form_data");

switch ($action) {
    case "edit":
        if (!$profile->checkPrivilege('district_admin_edit')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        } else {
            $id = getParam("id");
            if (is_array($form_data)) {
                $view->setRef('ContentRight', change_one($action, $id, $form_data, $URL_PARA));
                $action = 'list';
            } else {
                $view->setRef('ContentRight', form_change($action, $id, $URL_PARA));
            }
        }
        break;
    case "delete":
        if (!$profile->checkPrivilege('district_admin_delete')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        } else {
            if (!isset($_REQUEST["confirm_id"])) {
                $id = getParam("id");
                $view->setRef('ContentRight', ask4delete($id, $URL_PARA));
            } else {
                $confirm_id = getParam("confirm_id");
                if (delete_one($confirm_id)) {
                    $view->addMessage('STATUS', 'NOTICE', $app->static_text('the_entry_was_deleted_successfully'));
                } else {
                    $view->addMessage('STATUS', 'ERROR', $app->static_text('the_entry_could_not_be_deleted'));
                }
                $action = 'list';
            }
        }
        break;
    case "search":
        if (!$profile->checkPrivilege('district_admin_search')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        }
        if (isset($_REQUEST["send_button"])) {
            $URL_PARA["page"] = 1;
            $URL_PARA["alpha"] = '';
        }
        $view->setRef('ContentRight', show_search_form($action, $form_data, $URL_PARA));
        if (is_array($form_data)) {
            $view->append('ContentRight', get_list($URL_PARA, $form_data));
        }
        break;
    case "new":
        if (!$profile->checkPrivilege('district_admin_new')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        }
        if (is_array($form_data)) {
            $action = new_one($action, $form_data, $URL_PARA);
        } else {
            $view->setRef('ContentRight', form_new($action, $URL_PARA));
        }
        break;
}

if ($action == 'list') {
    if ($profile->checkPrivilege('district_admin_view')) {
        $view->setRef('ContentRight', get_list($URL_PARA));
    }
}

$view->setHeadTitle('Districts Admin');
$view->setRef('ContentLeft', $admin->get_navigation());
$view->display('admin/content_left_right.tpl');
