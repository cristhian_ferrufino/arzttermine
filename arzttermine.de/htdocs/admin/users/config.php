<?php

use Arzttermine\Application\Application;
use Arzttermine\Security\Group;
use Arzttermine\Booking\Booking;
use Arzttermine\Media\Asset;
use Arzttermine\MedicalSpecialty\MedicalSpecialty;
use Arzttermine\MedicalSpecialty\TreatmentType;
use Arzttermine\User\User;
use NGS\Managers\BookingsManager;
use NGS\Managers\MedicalSpecialtiesManager;
use NGS\Managers\DoctorsTreatmentTypesManager;

function get_group_box()
{
    $sql = Application::getInstance()->getSql();

    $results = array();

    $sql->query('SELECT id, name FROM ' . DB_TABLENAME_GROUPS);
    $num = $sql->num_rows();
    for ($i = 1; $i <= $num; $i++) {
        $sql->next_record();
        $results[$sql->f("id")] = $sql->f("name");
    }

    return $results;
}

function get_data_group($data = 0)
{
    $group = new Group();

    return $group->findKey('id="' . $data["group_id"] . '"', 'name');
}

function get_first_name($data = 0)
{
    $_user = new User($data['id']);

    return '<a href="' . $_user->url_profile_start . '" target="_blank">' . $_user->getFullName() . '</a>';
}

function get_photos_name($data = 0)
{
    $_gfx = new Gfx();
    $_total = $_gfx->count('owner_id=' . $data['id'] . ' AND parent_id=0 AND owner_type="' . $GLOBALS['CONFIG']['GFX_OWNERTYPES']['USER'] . '"');
    if ($_total == 0) {
        return '<span class="red">' . $_total . '</span>';
    } else {
        $_gfxs = $_gfx->findObjects('owner_id=' . $data['id'] . ' AND size="' . GFX_SIZE_THUMBNAIL . '" AND owner_type="' . $GLOBALS['CONFIG']['GFX_OWNERTYPES']['USER'] . '"');
        if (!empty($_gfxs)) {
            $_html_gfx = '<ul class="user-profiles_thumb">';
            foreach ($_gfxs as $_gfx) {
                $_html_gfx .= '<li><img src="' . $_gfx->url . '" title="" /></li>';
            }
            $_html_gfx .= '</ul>';
        }
        $_html = '<span class="green">' . $_total . ' <img src="' . getStaticUrl('adm/icon_images.png') . '" style="vertical-align:text-bottom;" class="tooltip" title="' . htmlentities($_html_gfx) . '" /></span>';

        return $_html;
    }
}

function get_activated_at($data = 0)
{
    return ($data['activated_at'] == '0000-00-00 00:00:00' ?
        '<span class="red">Nein</span>' :
        '<span class="green">Ja (' . $data['activated_at'] . ')</span>');
}

function get_data_status($data = 0)
{
    if (isset($GLOBALS['CONFIG']['USER_STATUS'][$data['status']])) {
        return $GLOBALS['CONFIG']['USER_STATUS'][$data['status']];
    } else {
        return 'n/a';
    }
}

function get_data_gender($data = 0)
{
    return !empty($GLOBALS['CONFIG']['ARRAY_GENDER'][$data['gender']]) ? $GLOBALS['CONFIG']['ARRAY_GENDER'][$data['gender']] : '';
}

function get_fixed_week($data = 0)
{
    return $GLOBALS['CONFIG']['FIXED_WEEK'][$data['fixed_week']];
}

function get_data_assets($data = 0)
{
    $_asset = new Asset();
    $_total = $_asset->count('parent_id=0 AND owner_id=' . $data['id'] . ' AND owner_type=' . ASSET_OWNERTYPE_USER);
    if ($_total == 0) {
        return '<span class="red">' . $_total . '</span>';
    } else {
        $_assets = $_asset->findObjects('owner_id=' . $data['id'] . ' AND size="' . ASSET_GFX_SIZE_THUMBNAIL . '" AND owner_type=' . ASSET_OWNERTYPE_USER);
        if (!empty($_assets)) {
            $_html_assets = '<ul class="asset-thumb">';
            foreach ($_assets as $_asset) {
                $_html_assets .= '<li><img src="' . $_asset->getUrl() . '" title="" /></li>';
            }
            $_html_assets .= '</ul>';
        }
        $_html = '<span class="green">' . $_total . ' <img src="' . getStaticUrl('adm/icon-images.png') . '" style="vertical-align:text-bottom;" class="tooltip" title="' . htmlspecialchars($_html_assets) . '" /></span>';

        return $_html;
    }
}

function get_form_medical_specialty_ids($data = 0)
{
    $html = '';
    $ids = $data['medical_specialty_ids'];
    $ids = explode(',', $ids);
    $medical_specialty = new MedicalSpecialty();
    $medical_specialties = $medical_specialty->getMedicalSpecialties();
    $html_rows = '';
    foreach ($medical_specialties as $medical_specialty) {
        $class = '';
        $class = ($class == "alternate") ? "" : "alternate";
        $html_rows .= '<tr class="' . $class . '"><td width="20"><input type="checkbox" id="form_data_medical_specialty_ids_' . $medical_specialty->getId() . '" value="' . $medical_specialty->getId() . '" name="form_data[medical_specialty_ids][]" ' . (in_array($medical_specialty->getId(), $ids) ? " checked" : "") . '></td><td><label for="form_data_medical_specialty_ids_' . $medical_specialty->getId() . '">' . $medical_specialty->getName() . '</label></td></tr>';
    }
    $html .= '<table id="medical_specialties" class="widefat" align="center" width="100%">';
    $html .= '<tr class="headers"><th colspan="2">Fachrichtungen</th></tr>';
    $html .= $html_rows;
    $html .= "</table>\n";

    return $html;
}

function get_data_medical_specialty_ids($data = 0)
{
    $medical_specialty = new MedicalSpecialty();

    return getCategoriesText($data['medical_specialty_ids'], $medical_specialty->getMedicalSpecialtyValues(), '<br />');
}

function get_form_treatment_types($data = 0)
{
    // Something went wrong. Don't proceeed
    if (!isset($data['id'])) {
        return '';
    }
    
    // Container for queries
    $ms   = new MedicalSpecialty();
    $tt   = new TreatmentType();
    $user = new User($data['id']);

    $all_medical_specialties = $ms->getMedicalSpecialties();
    $all_treatment_types = array();
    
    // Get the TT, but sort them by parent ID.
    // We do it this way because we want the TT ID in the key
    /** @var TreatmentType $treatment_type */
    foreach ($tt->findObjects() as $treatment_type) {
        $all_treatment_types[$treatment_type->getMedicalSpecialtyId()][$treatment_type->getId()] = $treatment_type;
    }
    
    $user_ms = $user->getMedicalSpecialties();
    $user_tt = $user->getTreatmentTypes();

    $html      = '';
    $html_rows = '';
    
    foreach ($user_ms as $ms) {
        $treatment_types = $ms->getTreatmentTypes();
        
        $html_rows .= '<tr data-msid="' . $ms->getId() . '"><td width="20"><div>';
        $html_rows .= '<h3>' . $ms->getName() . '</h3>';
        
        $html_rows .= '<ul>';
        
        foreach ($treatment_types as $tt) {
            // The user has the TT if the array key is in the user's TT array
            $checked = array_key_exists($tt->getId(), $user_tt) ? ' checked' : '';
            
            $html_rows .= '<li style="list-style: none;"><input type="checkbox" id="form_data_treatment_type_ids_' . $tt->getId() . '" value="' . $tt->getId() . '" name="form_data[treatment_type_ids][' . $ms->getId() . '][]" ' . $checked . '><span>' . $tt->getName() . '</span></li>';
        }
        $html_rows .= '</ul></div></td></tr>';
    }
    $html .= '<table class="widefat" align="center" width="100%" id="treatment_types">';
    $html .= '<thead><tr class="headers"><th colspan="2">Treatment Types</th></tr></thead>';
    $html .= '<tbody>';
    $html .= $html_rows;
    $html .= "</tbody></table>\n";
    
    // Convert the objects into arrays for the JS
    $ms_json = array();
    /** @var MedicalSpecialty $ms */
    foreach ($all_medical_specialties as $ms) {
        $ms_json[$ms->getId()] = $ms->toArray();
    }
    $ms_json = json_encode($ms_json);
    
    $tt_json = array();
    foreach ($all_treatment_types as $msid => $tt_group) {
        /** @var TreatmentType $tt */
        foreach ($tt_group as $ttid => $tt) {
            $tt_json[$msid][$ttid] = $tt->toArray(); 
        }
    }
    $tt_json = json_encode($tt_json);

    $selected_tt_json = array();
    foreach($selected_tt_json as $tt) {
        $selected_tt_json[$tt->getId()] = $tt->toArray();
    }
    $selected_tt_json = json_encode($selected_tt_json);
    
    $html
        .= <<<SCRIPT

        <script>
        AT._defer(function(){

            var all_ms = {$ms_json},
                all_tt = {$tt_json},
                selected_tt = {$selected_tt_json};
                
            // Tables
            var \$ms_container = \$('#medical_specialties'),
                \$tt_container = \$('#treatment_types');
            
            var addTreatmentTypes = function(msid) {
                // If there's no TT for this MS, just return
                if (all_tt[msid] === undefined) {
                    return;
                }
                
                var html = '<tr data-msid="' + msid + '"><td width="20"><div><h3>' + all_ms[msid]['name'] + '</h3><ul>';
                
                for (tt in all_tt[msid]) {
                    if (all_tt[msid].hasOwnProperty(tt)) {
                        html += '<li style="list-style: none;"><input type="checkbox" name="form_data[treatment_type_ids][' + msid + '][]" value="' + tt + '" id="form_data_treatment_type_ids_' + tt + '"';
                        
                        // Returns index of hit, so -1 means "not found"
                        if ($.inArray(tt, selected_tt) > -1) {
                            html += ' checked="checked"';
                        }
                        
                        html += '><span>' + all_tt[msid][tt]['name'] + '</span></li>';
                    }
                }
                
                html += '</ul></div></td></tr>';
                
                \$tt_container.find('tbody').append(html);
            };
            
            var removeTreatmentTypes = function(msid) {
                \$tt_container.find('tr').filter('[data-msid="' + msid + '"]').remove();
            }
        
            \$('#form_row_medical_specialty_ids').find('input').change(function() {
                var \$this = \$(this);
                // The checked value is the *new* value
                if (\$this.is(':checked')) {
                    addTreatmentTypes(\$this.val());
                } else {
                    removeTreatmentTypes(\$this.val());
                }
            });
        });
        </script>
SCRIPT;

    return $html;
}

function get_data_last_name($data = 0)
{
    if (!empty($data['slug']) && !empty($data['last_name'])) {
        return '<a href="' . Application::getInstance()->container->get('router')->generate('doctor_profile_no_insurance', array('slug' => $data['slug'])) . '" target="_blank">' . $data['last_name'] . '</a>';   
    }

    return $data['last_name'];
}

function get_form_googlepreview($data = 0)
{
    $_user = new User($data['id']);
    $title = '';
    $description = '';
    $url_base = '';
    if ($_user->isValid()) {
        $title = $_user->getHtmlDefaultTitle();
        $description = $_user->getHtmlDefaultMetaDescription();
        $url_base = $_user->getPermalink();
        $url_base = str_replace($_user->getSlug(), '', $url_base);
        $url_base = str_replace('http://', '', $url_base);
    }

    $html
        = <<<EOF
<div id="googlepreview" class="google-result">
	<div class="title"></div>
	<div class="url"></div>
	<div class="description"></div>
</div>
<script type="text/javascript">
var AT = AT || {};
AT._defer(function(){

var input_html_title_default = '$title';
var input_html_meta_description_default = '$description';

function update_googlepreview() {
	var input_html_title = $('#form_data_html_title').val();
	if (input_html_title.length < 1) { input_html_title = input_html_title_default; }
	$('#googlepreview .title').html(input_html_title);

	var input_html_meta_description = $('#form_data_html_meta_description').val();
	if (input_html_meta_description.length < 1) { input_html_meta_description = input_html_meta_description_default; }
	$('#googlepreview .description').html(input_html_meta_description);

	var input_slug = $('#form_data_slug').val();
	if (input_slug.length < 1) { input_slug = ''; }
	$('#googlepreview .url').html('$url_base' + input_slug);
};

$(document).ready(function() {
	update_googlepreview();

	$('#form_data_html_title').keyup(function() {
		update_googlepreview();
		return false;
	});
	$('#form_data_html_meta_description').keyup(function() {
		update_googlepreview();
		return false;
	});
	$('#form_data_slug').keyup(function() {
		update_googlepreview();
		return false;
	});
});

});
</script>
EOF;

    return $html;
}

function get_open_bookings_count($data = 0)
{
    $bookingsManager = BookingsManager::getInstance();
    $userId = $data["id"];
    $newBookingsForUser = $bookingsManager->getBookingsByUserIdAndStatus($userId, Booking::STATUS_NEW);
    if (count($newBookingsForUser) == 0) {
        return count($newBookingsForUser);
    } else {
        $href = $GLOBALS['CONFIG']['URL_HTTP_LIVE'] . '/admin/bookings/index.php?action=search&open_bookings=1&form_data[user_id_SEARCH_STYLE]=0&form_data[user_id]=' . $userId;

        return '<a href="' . $href . '" target="_blank">' . count($newBookingsForUser) . '</a>';
    }
}

function get_data_has_contract($data = 0)
{
    return $GLOBALS["CONFIG"]["ARRAY_NO_YES"][$data['has_contract']];
}

function get_email_form($data = 0)
{
    $email = empty($data['email']) ? '' : $data['email'];
    return '<input type="text" id="form_data_email" class="form_text" name="form_data[email]" value="' . $email .'" autocomplete="off">';
}

// The ratings elements are number elements, as the 0,5 step system results in accidental in 0 ratings
// ===================================================================================================
function get_rating_average($data = 0)
{
    $rating = isset($data['rating_average']) ? $data['rating_average'] : 0;
    return '<input name="form_data[rating_average]" type="number" max="5.0" min="0.0" step="0.1" value="' . $rating . '"/>';
}

function get_rating_1($data = 0)
{
    $rating = isset($data['rating_1']) ? $data['rating_1'] : 0;
    return '<input name="form_data[rating_1]" type="number" max="5.0" min="0.0" step="0.1" value="' . $rating . '"/>';
}

function get_rating_2($data = 0)
{
    $rating = isset($data['rating_2']) ? $data['rating_2'] : 0;
    return '<input name="form_data[rating_2]" type="number" max="5.0" min="0.0" step="0.1" value="' . $rating . '"/>';
}

function get_rating_3($data = 0)
{
    $rating = isset($data['rating_3']) ? $data['rating_3'] : 0;
    return '<input name="form_data[rating_3]" type="number" max="5.0" min="0.0" step="0.1" value="' . $rating . '"/>';
}

$MENU_ARRAY = array(
    "id"                      => array("sql" => 1, "name" => "ID", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "open_bookings"           => array("sql" => 0, "name" => "Open Bookings", "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_open_bookings_count", "para" => 1), "list_function" => array("name" => "get_open_bookings_count", "para" => 1)),
    "html_title"              => array("sql" => 1, "name" => 'HTML-Title', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("template" => "admin/_form_text_counter.tpl")),
    "html_meta_description"   => array("sql" => 1, "name" => 'HTML-Meta-Description', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("template" => "admin/_form_text_counter.tpl")),
    "html_meta_keywords"      => array("sql" => 1, "name" => 'HTML-Meta-Keywords', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("template" => "admin/_form_text_counter.tpl")),
    "googlepreview"           => array("sql" => 0, "name" => 'GooglePreview', "show" => 0, "edit" => 1, "new" => 1, "search" => 0, "sort" => 0, "nowrap" => 0, "form_input" => array("type" => "function", "name" => "get_form_googlepreview", "para" => 1)),
    "status"                  => array("sql" => 1, "name" => "Status", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "select", "values" => $GLOBALS['CONFIG']['USER_STATUS']), "list_function" => array("name" => "get_data_status", "para" => 1)),
    "gender"                  => array("sql" => 1, "name" => "Geschlecht", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_gender", "para" => 1), "form_input" => array("type" => "select", "values" => $GLOBALS['CONFIG']['ARRAY_GENDER'])),
    "title"                   => array("sql" => 1, "name" => 'Titel', "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "first_name"              => array("sql" => 1, "name" => $app->static_text('first_name'), "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "last_name"               => array("sql" => 1, "name" => $app->static_text('last_name'), "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_last_name", "para" => 1)),
    "phone"                   => array("sql" => 1, "name" => "Telefon (privat)", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "slug"                    => array("sql" => 1, "name" => "Permalink", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "group_id"                => array("sql" => 1, "name" => $app->static_text('group'), "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "select", "values" => get_group_box()), "list_function" => array("name" => "get_data_group", "para" => 1)),
    "email"                   => array("sql" => 1, "name" => $app->static_text('email'), "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "function", "name" => "get_email_form", "para" => 1)),
    "password"                => array("sql" => 0, "name" => $app->static_text('password'), "show" => 0, "edit" => 1, "new" => 1, "search" => 0, "sort" => 0, "nowrap" => 1, "form_input" => array("type" => "password")),
    "pw2"                     => array("sql" => 0, "name" => $app->static_text('repeat_password'), "show" => 0, "edit" => 1, "new" => 1, "search" => 0, "sort" => 0, "nowrap" => 0, "form_input" => array("type" => "password")),
    "fixed_week"              => array("sql" => 1, "name" => "Fixed Week", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_fixed_week", "para" => 1), "form_input" => array("type" => "select", "values" => $GLOBALS['CONFIG']['FIXED_WEEK'])),
    "photos"                  => array("sql" => 0, "name" => "Fotos", "show" => 1, "edit" => 0, "new" => 0, "search" => 0, "sort" => 0, "nowrap" => 1, "list_function" => array("name" => "get_data_assets", "para" => 1)),
    "newsletter_subscription" => array("sql" => 1, "name" => "Newsletter", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "checkbox", "values" => array(1 => '&nbsp;'))),
    "rating_average"          => array("sql" => 1, "name" => "Rating &#216;", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "function", "name" => "get_rating_average", "para" => 1)),
    "rating_1"                => array("sql" => 1, "name" => "Rating 1", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "function", "name" => "get_rating_1", "para" => 1)),
    "rating_2"                => array("sql" => 1, "name" => "Rating 2", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "function", "name" => "get_rating_2", "para" => 1)),
    "rating_3"                => array("sql" => 1, "name" => "Rating 3", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "function", "name" => "get_rating_3", "para" => 1)),
    "medical_specialty_ids"   => array("sql" => 1, "name" => "Fachrichtungen", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_medical_specialty_ids", "para" => 1), "form_input" => array("type" => "function", "name" => "get_form_medical_specialty_ids", "para" => 1)),
    "treatment_types"         => array("sql" => 1, "name" => "Treatment Types", "show" => 0, "edit" => 1, "new" => 1, "search" => 0, "sort" => 0, "nowrap" => 1, "list_function" => array("name" => "", "para" => 1), "form_input" => array("type" => "function", "name" => "get_form_treatment_types", "para" => 1)),
    "docinfo_1"               => array("sql" => 1, "name" => 'Info Philosophy', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => ' style="width:300px;height:60px;"')),
    "docinfo_5"               => array("sql" => 1, "name" => 'Info Spezialisierung', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => ' style="width:300px;height:60px;"')),
    "docinfo_6"               => array("sql" => 1, "name" => 'Info Leistungsspektrum', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => ' style="width:300px;height:60px;"')),
    "docinfo_2"               => array("sql" => 1, "name" => 'Info Ausbildung', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => ' style="width:300px;height:60px;"')),
    "docinfo_3"               => array("sql" => 1, "name" => 'Info Sprachen', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => ' style="width:300px;height:60px;"')),
    "docinfo_4"               => array("sql" => 1, "name" => 'Info Auszeichnungen', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => ' style="width:300px;height:60px;"')),
    "info_seo"                => array("sql" => 1, "name" => 'SEO-TEXT', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => ' style="width:300px;height:60px;"')),
    "comment_intern"          => array("sql" => 1, "name" => 'Kommentar <span style="color:#ff0000;">(intern)', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => ' style="width:300px;height:60px;"')),
    "has_contract"            => array("sql" => 1, "name" => "Mit Vertrag", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "select", "values" => $GLOBALS['CONFIG']['ARRAY_NO_YES']), "list_function" => array("name" => "get_data_has_contract", "para" => 1)),
    "activated_at"            => array("sql" => 1, "name" => "Aktiv", "show" => 1, "edit" => 1, "new" => 1, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "datetime"), "list_function" => array("name" => "get_activated_at", "para" => 1)),
    "created_at"              => array("sql" => 1, "name" => $app->static_text('created_at'), "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "created_by"              => array("sql" => 1, "name" => $app->static_text('created_by'), "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "updated_at"              => array("sql" => 1, "name" => $app->static_text('updated_at'), "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "updated_by"              => array("sql" => 1, "name" => $app->static_text('updated_by'), "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "last_login_at"           => array("sql" => 1, "name" => $app->static_text('last_login_at'), "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
);

$BUTTON = array(
    "new"    => array("func" => "user_admin_new", "alt_text" => $app->_('Neu'), "script" => $_SERVER['PHP_SELF'], "gfx" => "document_new.png", "action" => "action=new", "th" => 1, "td" => 0),
    "edit"   => array("func" => "user_admin_edit", "alt_text" => $app->_('Bearbeiten'), "script" => $_SERVER['PHP_SELF'], "gfx" => "document_edit.png", "action" => "action=edit", "th" => 0, "td" => 1),
    "delete" => array("func" => "user_admin_delete", "alt_text" => $app->_('Löschen'), "script" => $_SERVER['PHP_SELF'], "gfx" => "delete.png", "action" => "action=delete", "th" => 0, "td" => 1),
    "search" => array("func" => "user_admin_search", "alt_text" => $app->_('Suchen'), "script" => $_SERVER['PHP_SELF'], "gfx" => "search.png", "action" => "action=search", "th" => 1, "td" => 0),
);

$TABLE_CONFIG = array(
    "table_name"  => DB_TABLENAME_USERS,
    "alpha_index" => "last_name",
);

$URL_PARA = array(
    "page"         => 1,
    "alpha"        => '',
    "sort"         => 'id',
    "direction"    => 'DESC',
    "num_per_page" => 50,
    "type"         => "num"
);
