<?php

use Arzttermine\User\User;

/********************************************************
 * file: config.inc
 * comment: enthaelt alle globalen einstellungen
 *********************************************************/
function get_status_name($data = 0)
{
    if (isset($GLOBALS['CONFIG']['BLOGPOST_STATUS'][$data['status']])) {
        return $GLOBALS['CONFIG']['BLOGPOST_STATUS'][$data['status']];
    } else {
        return 'n/a';
    }
}

function get_title_name($data = 0)
{
    $_link = $GLOBALS['CONFIG']['URL_HTTP_LIVE'] . '/news/' . $data['slug'];
    $_title = $data['title'];

    return '<a href="' . $_link . '" target="_blank">' . $_title . '</a>';
}

function get_created_by_name($data = 0)
{
    $_user = new User($data['created_by']);
    if (!$_user->isValid()) {
        return 'n/a';
    }

    return $_user->getFullName();
}

function get_select_created_by()
{
    $_users = array();
    $user = new User();
    $users = $user->findObjects('1=1 ORDER BY last_name');
    /** @var User[] $users */
    foreach ($users as $_user) {
        $_users[$_user->getId()] = $_user->getFullName();
    }

    return $_users;
}

function get_updated_by_name($data = 0)
{
    $_user = new User($data['updated_by']);
    if (!$_user->isValid()) {
        return 'n/a';
    }

    return $_user->getFullName();
}

$MENU_ARRAY = array(
    "id"           => array("sql" => 1, "name" => "ID", "show" => 1, "edit" => 0, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "status"       => array("sql" => 1, "name" => "Status", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "select", "values" => $GLOBALS['CONFIG']['BLOGPOST_STATUS']), "list_function" => array("name" => "get_status_name", "para" => 1)),
    "topic"        => array("sql" => 1, "name" => "Topic", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0),
    "title"        => array("sql" => 1, "name" => "Titel", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "list_function" => array("name" => "get_title_name", "para" => 1)),
    "slug"         => array("sql" => 1, "name" => "Permalink", "show" => 0, "edit" => 1, "new" => 1, "search" => 0, "sort" => 0, "nowrap" => 1),
    "published_at" => array("sql" => 1, "name" => "Datum", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "datetime"), "init_value" => $app->now()),
    "body"         => array("sql" => 1, "name" => "Teaser/Text", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "editor_tinymce")),
    "created_at"   => array("sql" => 1, "name" => "Erstellungsdatum", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "none")),
    "created_by"   => array("sql" => 1, "name" => "Author", "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_created_by_name", "para" => 1), "list_function" => array("name" => "get_created_by_name", "para" => 1)),
    "updated_at"   => array("sql" => 1, "name" => "Letztes Update", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "updated_by"   => array("sql" => 1, "name" => "Letzter Bearbeiter", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_updated_by_name", "para" => 1), "list_function" => array("name" => "get_updated_by_name", "para" => 1)),
);
$BUTTON = array(
    "new"    => array("func" => "blog_admin_new", "alt_text" => $app->static_text('new_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "document_new.png", "action" => "action=new", "th" => 1, "td" => 0),
    "edit"   => array("func" => "blog_admin_edit", "alt_text" => $app->static_text('edit_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "document_edit.png", "action" => "action=edit", "th" => 0, "td" => 1),
    "delete" => array("func" => "blog_admin_delete", "alt_text" => $app->static_text('delete_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "delete.png", "action" => "action=delete", "th" => 0, "td" => 1),
    "search" => array("func" => "blog_admin_search", "alt_text" => $app->static_text('search'), "script" => $_SERVER['PHP_SELF'], "gfx" => "search.png", "action" => "action=search", "th" => 1, "td" => 0),
);
$TABLE_CONFIG = array(
    "table_name"  => DB_TABLENAME_BLOGPOSTS,
    "alpha_index" => "title",
);
$URL_PARA = array(
    'page'         => 1,
    'alpha'        => '',
    'sort'         => 0,
    'direction'    => 'DESC',
    'num_per_page' => 25,
    'type'         => 'num'
);