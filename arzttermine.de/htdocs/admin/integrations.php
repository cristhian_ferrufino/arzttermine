<?php
/**
 * Admin: Dashboard
 *
 * @author Axel Kuzmik <axel@kuzmik.de>
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/../include/bootstrap.php';

/* ************************************************************************************* */

use Arzttermine\Admin\Admin;
use Arzttermine\Application\Application;

$admin = new Admin();
$app = Application::getInstance();
$view = $app->getView();
$profile = $app->getCurrentUser();

/* ************************************************************************************* */

$view->addHeadHtml(
    '<style type="text/css">
    <!--
    .postbox table td {
        padding:0.2em 0.4em;
        font-weight:normal;
    }
    .postbox table th {
        font-weight:bold;
    }
    .logo-border {
        border: 1px solid #bbbbbb;
        padding: 2px;
        display:block;
    }
    -->
    </style>
    '
);

if (!$profile->checkPrivilege('system_internal_group') || !$profile->checkPrivilege('integration_admin')) {
    $view->ContentRight = $app->static_file('access_denied.html');
    $view->ContentLeft = $admin->get_navigation();
    $view->display('admin/content_left_right.tpl');
    exit;
}

function show_button($access, $url, $icon)
{
    global $profile;
    if ($profile->checkPrivilege($access)) {
        return '<a href="' . $url . '"><img src="' . $icon . '" alt="" /></a>';
    }

    return '';
}

$_html
    = '<div id="users" class="postbox" >
<h3 class="hndle"><img src="' . getStaticUrl('adm/icon-integrations.png') . '" alt="" /><span>Integrationen</span></h3>
<div class="inside field">
<table style="display:inline; margin-left:10px;">';
foreach ($GLOBALS['CONFIG']['INTEGRATIONS'] as $_integration) {
    $link_start = isset($_integration['URL_ADMIN']) ? '<a href="' . $_integration['URL_ADMIN'] . '" class="logo-border">' : '';
    $link_end = isset($_integration['URL_ADMIN']) ? '</a>' : '';
    $img = isset($_integration['URL_LOGO']) ? '<img src="' . getStaticUrl($_integration['URL_LOGO']) . '" alt="" />' : '';

    $_html .= '<tr><td>' . $link_start . $img . $link_end . '</td><td>';
    if (isset($_integration['URL_ADMIN'])) {
        $_html .= '<a href="' . $_integration['URL_ADMIN'] . '">';
    }
    $_html .= '<tr><td>' . $link_start . $_integration['NAME'] . $link_end . '</td></tr>';
}
$_html
    .= '</table>
	</div>
	<div class="clear"></div>
</div>';

$view->setRef('ContentRight', $_html);

$view->setHeadTitle('Integrationen');
$view->setRef('ContentLeft', $admin->get_navigation());
$view->display('admin/content_left_right.tpl');