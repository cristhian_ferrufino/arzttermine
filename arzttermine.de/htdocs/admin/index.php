<?php
/**
 * Admin: Dashboard
 *
 * @author Axel Kuzmik <axel@kuzmik.de>
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/../include/bootstrap.php';

/* ************************************************************************************* */

use Arzttermine\Admin\Admin;
use Arzttermine\Admin\Message;
use Arzttermine\Application\Application;
use Arzttermine\Blog\Blog;
use Arzttermine\Booking\Booking;
use Arzttermine\Mail\Contact;
use Arzttermine\User\User;

$admin = new Admin();
$app = Application::getInstance();
$view = $app->getView();
$profile = $app->getCurrentUser();

$view->disableCaching();

/* ************************************************************************************* */

$view->addHeadHtml(
    '<style type="text/css">
    <!--
    .postbox table td {
        padding:0.2em 0.4em;
        font-weight:normal;
    }
    .postbox table th {
        font-weight:bold;
    }
    -->
    </style>
    '
);

if (!$profile->checkPrivilege('system_internal_group')) {
    $view->ContentRight = $app->static_file('access_denied.html');
    $view->ContentLeft = $admin->get_navigation();
    $view->display('admin/content_left_right.tpl');
    exit;
}

function show_button($access, $url, $icon)
{
    global $profile;
    if ($profile->checkPrivilege($access)) {
        return '<a href="' . $url . '"><img src="' . $icon . '" alt="" /></a>';
    }

    return '';
}

$_html = '';

/**
 * ********************************************************
 * Bookings
 */
if ($profile->checkPrivilege('booking_admin')) {
    $_html
        .= '<div id="users" class="postbox" >
	<h3 class="hndle"><img src="' . getStaticUrl('adm/icon-booking.png') . '" alt="" /><span>Buchungen</span></h3>';
    $i = 0;
    $_booking = new Booking();
    $_bookings_total = $_booking->count('1=1');
    $_bookings_new_total = $_booking->count('status=' . Booking::STATUS_NEW);

    $_html
        .= '<div class="inside field">
		<div class="field_header">
		<table style="display:inline; margin-left:10px;">
			<tr><th colspan="2">Buchungen: <span class="green taller b">' . $_bookings_total . '</span></th></tr>
			<tr><td colspan="2"><span class="orange smaller b">Neue Buchungen: </span>' . $_bookings_new_total . '</td></tr>';

    /** @var Booking[] $_bookings */
    $_bookings = $_booking->findObjects('status=' . Booking::STATUS_NEW . ' ORDER BY created_at DESC LIMIT 10');
    if (!empty($_bookings)) {
        $_html .= '<tr><td colspan="3"><span class="orange smaller b">Die letzten 10 neuen Buchungen:</span></td></tr>';
        foreach ($_bookings as $_booking) {
            $_location = $_booking->getLocation();
            if ($_location->isValid()) {
                $_location_html = 'Praxis: <a href="' . $_location->getUrl() . '">' . $_location->getName() . '</a> / ' . $_location->getCityZip();
            } else {
                $_location_html = '<span class="red">Unbekannte Praxis</span>';
            }
            $_user = $_booking->getUser();
            if ($_user->isValid()) {
                $_user_html = 'Arzt: <a href="' . $_user->getAdminEditUrl() . '">' . $_user->getName() . '</a>';
            } else {
                $_user_html = '<span class="red">Unbekannter Arzt</span>';
            }
            $_html
                .= '<tr>
			<th style="white-space:nowrap;">' . human_time_diff(strtotime($_booking->getCreatedAt())) . '</th>
			<td><a href="' . $_booking->getAdminEditUrl() . '">' . $_booking->getName() . '</a></td>
			<td>' . $_location_html . '<br />' . $_user_html . '<br />Datum: ' . $_booking->getAppointment()->getDateTimeText() . '</td>
			</tr>';
        }
    }
    $_html
        .= '
			</table>
		</div>
		</div>
		<div class="clear"></div>
	</div>';
}

/**
 * ********************************************************
 * CONTACT
 */
if ($profile->checkPrivilege('contact_admin')) {
    $_html
        .= '<div id="contacts" class="postbox" >
	<h3 class="hndle"><img src="' . getStaticUrl('adm/icon-contact.png') . '" alt="" /><span>Kontakte</span></h3>';
    $i = 0;
    foreach ($GLOBALS['CONFIG']['CMS_LOCALE'] as $lang => $value) {
        $i++;
        $_contact = new Contact();
        $_num_contacts = $_contact->count('1=1');

        $_html .= '<div class="inside field' . ($i >= sizeof($GLOBALS['CONFIG']['CMS_LOCALE']) ? '' : '_section') . '">
			<div class="field_header">
			<table style="display:inline; margin-left:10px;">
				<tr><th>Kontakte: <span class="green taller b">' . $_num_contacts . '</span></th></tr>';

        $_contact = $_contact->findObject('1=1 ORDER BY created_at DESC');
        if ($_contact->isValid()) {
            $_html .= '<tr><th>Last Message: </th><td><a href="' . $_contact->getAdminEditUrl() . '">' . $_contact->getSubject(20) . '</a></td></tr>
			<tr><td colspan="2"><span class="orange smaller b">' . human_time_diff(strtotime($_contact->getCreatedAt())) . ' / ' . $_contact->getCreatedAt() . '</span></td></tr>
			<tr><td colspan="2"><span class="smaller">' . $_contact->getMessage(20) . '</span></td></tr>';
        }
        $_html
            .= '
				</table>
			</div>
			</div>';
    }
    $_html .= '<div class="clear"></div></div>';
}

/**
 * ********************************************************
 * Online Users
 */
if ($profile->checkPrivilege('user_admin')) {
    $_html
        .= '<div id="users" class="postbox" >
	<h3 class="hndle"><img src="' . getStaticUrl('adm/icon-users.png') . '" alt="" /><span>Users</span></h3>';
    $i = 0;
    $_user = new User();
    $_users_total = $_user->count('1=1');
    $_users_online_total = $_user->count('last_login_at >= (NOW() - INTERVAL 1 DAY)');

    $_html
        .= '<div class="inside field">
		<div class="field_header">
		<table style="display:inline; margin-left:10px;">
			<tr><th colspan="2">Users: <span class="green taller b">' . $_users_total . '</span></th></tr>
			<tr><td colspan="2"><span class="orange smaller b">User eingeloggt in den letzten 24 Stunden: </span>' . $_users_online_total . '</td></tr>';

    $_users = $_user->findObjects('1=1 ORDER BY last_login_at DESC LIMIT 10');
    if (!empty($_users)) {
        $_html .= '<tr><td colspan="2"><span class="orange smaller b">Die letzten 10 eingeloggten User:</span></td></tr>';
        /** @var User[] $_users */
        foreach ($_users as $_user) {
            $_html .= '<tr><th style="white-space:nowrap;">' . human_time_diff(strtotime($_user->getLastLoginAt())) . '</th><td>' . $_user->getFullname() . '</td></tr>';
        }
    }
    $_html
        .= '
			</table>
		</div>
		</div>
		<div class="clear"></div>
	</div>';
}

$view->setRef('ContentRight', $_html)
    ->setRef('ContentLeft', $admin->get_navigation())
    ->addHeadTitle('Dashboard')
    ->display('admin/content_left_right.tpl');