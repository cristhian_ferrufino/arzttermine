<?php

use Arzttermine\MedicalSpecialty\MedicalSpecialty;
use Arzttermine\User\User;

function get_data_created_by($data = 0)
{
    $_user = new User($data['created_by']);
    if (!$_user->isValid()) {
        return 'n/a';
    }

    return $_user->getFullName();
}

function get_data_created_at($data = 0)
{
    return mysql2date('d.m.Y H:i:s', $data['created_at'], false);
}

function get_data_status($data = 0)
{
    if (isset($GLOBALS['CONFIG']['TREATMENTTYPE_STATUS'][$data['status']])) {
        return $GLOBALS['CONFIG']['TREATMENTTYPE_STATUS'][$data['status']];
    } else {
        return 'n/a';
    }
}

function get_data_medical_specialty_id($data = 0)
{
    return MedicalSpecialty::getNameById($data['medical_specialty_id']);
}

function get_values_medical_specialty_id($data = 0)
{
    return MedicalSpecialty::getHtmlOptions(true);
}

$MENU_ARRAY = array(
    "id"                   => array("sql" => 1, "name" => "ID", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "status"               => array("sql" => 1, "name" => "Status", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "select", "values" => $GLOBALS['CONFIG']['TREATMENTTYPE_STATUS']), "list_function" => array("name" => "get_data_status", "para" => 1)),
    "medical_specialty_id" => array("sql" => 1, "name" => "Fachrichtung", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_medical_specialty_id", "para" => 1), "form_input" => array("type" => "select", "values" => get_values_medical_specialty_id())),
    "name_de"              => array("sql" => 1, "name" => "Name de", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "name_en"              => array("sql" => 1, "name" => "Name en", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "slug"                 => array("sql" => 1, "name" => "Permalink", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 0, "nowrap" => 1),
    "created_at"           => array("sql" => 1, "name" => "Erstellungsdatum", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "none")),
    "created_by"           => array("sql" => 1, "name" => "Erstellt von", "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_created_by", "para" => 1), "list_function" => array("name" => "get_data_created_by", "para" => 1)),
    "updated_at"           => array("sql" => 1, "name" => "Letztes Update", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "updated_by"           => array("sql" => 1, "name" => "Letzter Bearbeiter", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_updated_by", "para" => 1), "list_function" => array("name" => "get_data_updated_by", "para" => 1)),
);

$BUTTON = array(
    "new"    => array("func" => "treatmenttype_admin_new", "alt_text" => $app->static_text('new_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "document_new.png", "action" => "action=new", "th" => 1, "td" => 0),
    "edit"   => array("func" => "treatmenttype_admin_edit", "alt_text" => $app->static_text('edit_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "document_edit.png", "action" => "action=edit", "th" => 0, "td" => 1),
    "delete" => array("func" => "treatmenttype_admin_delete", "alt_text" => $app->static_text('delete_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "delete.png", "action" => "action=delete", "th" => 0, "td" => 1),
    "search" => array("func" => "treatmenttype_admin_search", "alt_text" => $app->static_text('search'), "script" => $_SERVER['PHP_SELF'], "gfx" => "search.png", "action" => "action=search", "th" => 1, "td" => 0),
    #	"csv"			=> array("func"=>"treatmenttype_admin_csv","alt_text"=>'Liste als CSV herunterladen',"script"=>$_SERVER['PHP_SELF'],"gfx"=>"download.gif","action"=>"action=csv","th"=>1,"td"=>0),
);
$TABLE_CONFIG = array(
    "table_name"  => DB_TABLENAME_TREATMENTTYPES,
    "alpha_index" => "name",
);
$URL_PARA = array(
    'page'         => 1,
    'alpha'        => '',
    "sort"         => 0,
    "direction"    => 'DESC',
    "num_per_page" => 25,
    "type"         => 'num'
);