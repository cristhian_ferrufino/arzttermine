<?php
/**
 * Admin: BookingOffers
 *
 * @author Axel Kuzmik <axel@kuzmik.de>
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/../include/bootstrap.php';
require_once "config.php";

/* ************************************************************************************* */

use Arzttermine\Admin\Admin;
use Arzttermine\Application\Application;
use Arzttermine\Coupon\Coupon;

$admin = new Admin();
$app = Application::getInstance();
$view = $app->getView();
$profile = $app->getCurrentUser();

$view->disableCaching();

/* ************************************************************************************* */

if (!$profile->checkPrivilege('coupon_admin')) {
    $view->setRef('ContentRight', $app->static_file('access_denied.html'));
    $view->setRef('ContentLeft', $admin->get_navigation());
    $view->display('admin/content_left_right.tpl');
    exit;
}

function prepare($form_data)
{
    global $profile;

    $form_data["created_by"] = $profile->getEmail();

    $form_data['restrictions']['accessible_medical_specialty_ids'] = implode(',', $form_data['specialties']);

    return $form_data;
}

/* ************************************************************************************* */
function get_list($URL_PARA, $search = 0)
{
    global $admin;

    $_html = $admin->admin_get_list(0, $search, $URL_PARA);

    return $_html;
}

/* ************************************************************************************* */
function new_one($action, $form_data, $URL_PARA)
{
    global $app, $view;

    $form_data = prepare($form_data);

    if ($view->countMessages()) {
        $view->addMessage('STATUS', 'ERROR', $app->_('Bitte prüfen Sie Ihre Eingaben.'));
        $view->setRef('ContentRight', form_change($action, 0, $URL_PARA, $form_data));

        return $action;
    }

    $coupon = new Coupon();
    $coupon
        ->setCode($form_data['code'])
        ->setActive((bool)$form_data['active'])
        ->setOfferType($form_data['offer_type'])
        ->setNote($form_data['note'])
        ->setRestrictions($form_data['restrictions'])
        ->setType($form_data['type'])
        ->setCreatedBy($form_data['created_by'])
        ->setCreatedAt($app->now())
        ->saveNewObject();

    if ($coupon->isValid()) {
        $view->addMessage('STATUS', 'NOTICE', $app->static_text('action_was_successfully'));
        $action = 'list';
    } else {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('the_entry_could_not_be_created'));
        $action = 'list';
    }

    return $action;
}

/* ************************************************************************************* */
function change_one($action, $id, $form_data, $URL_PARA)
{
    global $app, $view;

    // 1. set defaults
    $form_data = prepare($form_data);

    $coupon = new Coupon($id);
    // @todo: More specific error message
    if (!$coupon->isValid()) {
        $view->addMessage('STATUS', 'ERROR', $app->_('Bitte prüfen Sie Ihre Eingaben.'));
    }

    // 3. exit on errors
    if ($view->countMessages()) {
        $view->addMessage('STATUS', 'ERROR', $app->_('Bitte prüfen Sie Ihre Eingaben.'));
        $view->setRef('ContentRight', form_change($action, $id, $URL_PARA, $form_data));

        return $action;
    }

    $_result = $coupon
        ->setCode($form_data['code'])
        ->setActive((bool)$form_data['active'])
        ->setOfferType($form_data['offer_type'])
        ->setNote($form_data['note'])
        ->setRestrictions($form_data['restrictions'])
        ->setType($form_data['type'])
        ->setUpdatedBy($form_data['created_by'])
        ->setUpdatedAt($app->now())
        ->save();

    if ($_result == 1) {
        $view->addMessage('STATUS', 'NOTICE', $app->static_text('action_was_successfully'));
        $action = 'list';
    } elseif ($_result == -1) {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('the_entry_allready_exists'));
        $view->setRef('ContentRight', form_change($action, $id, $URL_PARA, $form_data));
        $action = 'list';
    } elseif ($_result == -2) {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('name_of_primary_key_is_empty'));
        $view->setRef('ContentRight', form_change($action, $id, $URL_PARA, $form_data));
        $action = 'list';
    } else {
        $view->addMessage('STATUS', 'WARNING', $app->static_text('no_data_has_been_updated'));
        $view->setRef('ContentRight', form_change($action, $id, $URL_PARA, $form_data));
        $action = 'list';
    }

    return $action;
}

/* ************************************************************************************* */
function ask4delete($id, $URL_PARA)
{
    global $admin;

    $_html = $admin->admin_get_confirm_table($id, $URL_PARA, 'delete');

    return $_html;
}

/* ************************************************************************************* */
function delete_one($id)
{
    global $admin;

    return $admin->admin_set_delete($id);
}

/* ************************************************************************************* */
function form_new($action, $URL_PARA, $error_data = 0)
{
    global $admin;

    $_html = $admin->admin_get_form($action, 0, $error_data, $URL_PARA);

    return $_html;
}

/* ************************************************************************************* */
function form_change($action, $id, $URL_PARA, $error_data = 0)
{
    global $admin;

    $_html = $admin->admin_get_form($action, $id, $error_data, $URL_PARA);

    return $_html;
}

/* ************************************************************************** */
function show_search_form($action, $search_data, $URL_PARA)
{
    global $admin;

    return $admin->admin_get_form($action, 0, $search_data, $URL_PARA);
}

/* ************************************************************************************** */
/* Anfragen auswerten ******************************************************************* */
//URL_PARA neu bestimmen
foreach ($URL_PARA as $key => $val) {
    if (isset($_REQUEST[$key])) {
        $URL_PARA[$key] = $_REQUEST[$key];
    }
}
$action = getParam("action", 'list');
$form_data = getParam("form_data");

switch ($action) {
    case "edit":
        if (!$profile->checkPrivilege('coupon_admin_edit')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        } else {
            $id = getParam("id");
            if (is_array($form_data)) {
                $action = change_one($action, $id, $form_data, $URL_PARA);
            } else {
                $view->setRef('ContentRight', form_change($action, $id, $URL_PARA));
            }
        }
        break;
    case "delete":
        if (!$profile->checkPrivilege('coupon_admin_delete')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        } else {
            if (!isset($_REQUEST["confirm_id"])) {
                $id = getParam("id");
                $view->setRef('ContentRight', ask4delete($id, $URL_PARA));
            } else {
                $confirm_id = getParam("confirm_id");
                if (delete_one($confirm_id)) {
                    $view->addMessage('STATUS', 'NOTICE', $app->static_text('the_entry_was_deleted_successfully'));
                } else {
                    $view->addMessage('STATUS', 'ERROR', $app->static_text('the_entry_could_not_be_deleted'));
                }
                $action = 'list';
            }
        }
        break;
    case "new":
        if (!$profile->checkPrivilege('coupon_admin_new')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        }
        if (is_array($form_data)) {
            $action = new_one($action, $form_data, $URL_PARA);
        } else {
            $view->setRef('ContentRight', form_new($action, $URL_PARA));
        }
        break;
}

if ($action == 'list') {
    if ($profile->checkPrivilege('coupon_admin_view')) {
        $view->setRef('ContentRight', get_list($URL_PARA));
    }
}

$view->setHeadTitle('Gutscheine Admin');
$view->setRef('ContentLeft', $admin->get_navigation());
$view->display('admin/content_left_right.tpl');
