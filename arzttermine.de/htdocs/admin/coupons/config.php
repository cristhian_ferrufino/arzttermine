<?php

use Arzttermine\MedicalSpecialty\MedicalSpecialty;
use Arzttermine\Coupon\Coupon;

// Display field: offer_type
// --------------------------------------------------------------------------------------------------------------
function get_data_offer_type($data = 0)
{
    $types = $GLOBALS['CONFIG']['REFERRAL_PRICE_TYPES'];

    if (isset($types[$data['offer_type']])) {
        return $types[$data['offer_type']]['company'] . " (" . $types[$data['offer_type']]['price'] . ") ";
    } else {
        return 'N/A';
    }
}

// Display field: type
// --------------------------------------------------------------------------------------------------------------
function get_data_type($data = 0)
{
    if (isset($data['type'])) {
        if (intval($data['type']) === Coupon::TYPE_SYSTEM) {
            return 'Gutschein';
        } elseif (intval($data['type']) === Coupon::TYPE_USER) {
            return 'Referral';
        }
    }

    return 'n/a';
}

// Form field: offer_type
// --------------------------------------------------------------------------------------------------------------
function get_form_offer_type($data = 0)
{
    $types = $GLOBALS['CONFIG']['REFERRAL_PRICE_TYPES'];
    $html = '<select name="form_data[offer_type]">';
    
    $selected = $data['offer_type'];

    foreach ($types as $code => $type) {
        $html .= '<option value="' . $code . '"' . ($selected == $code ? ' selected' : '') . '>' . $type['company'] . ($type['price'] ? " ({$type['price']}) " : '') . '</option>';
    }

    $html .= '</select>';

    return $html;
}

// Form field: type
// --------------------------------------------------------------------------------------------------------------
function get_form_type($data = 0)
{
    $html = '';

    if (is_array($data) && !isset($data['type'])) {
        $data['type'] = Coupon::TYPE_SYSTEM;
    }

    $html .= '<label><input type="radio" name="form_data[type]" value="' . Coupon::TYPE_SYSTEM . '"' .
             (intval($data['type']) == Coupon::TYPE_SYSTEM ? ' checked' : '') . '>&nbsp;System (default)</label>';

    $html .= '<label><input type="radio" name="form_data[type]" value="' . Coupon::TYPE_USER . '"' .
             (intval($data['type']) == Coupon::TYPE_USER ? ' checked' : '') . '>&nbsp;Referral</label>';

    return $html;
}

// Form field: restrictions
// --------------------------------------------------------------------------------------------------------------
function get_form_restrictions($data = 0)
{
    $restrictions = array();
    $html = '';

    if (!empty($data['restrictions']) && !is_array($data['restrictions'])) {
        $restrictions = json_decode($data['restrictions'], true);
        // The decode failed, so revert the restrictions
        if (!is_array($restrictions)) {
            $restrictions = array();
        }
    }

    // accessible_has_contract
    $accessible_has_contract = !isset($restrictions['accessible_has_contract']) ? 1 : intval($restrictions['accessible_has_contract']);
    $html .= '<div>Accessible for has contract&nbsp;';
    $html .= '<select name="form_data[restrictions][accessible_has_contract]">';
    $html .= '<option value="0"' . ($accessible_has_contract === 0 ? ' selected' : '') . '>No</option>';
    $html .= '<option value="1"' . ($accessible_has_contract === 1 ? ' selected' : '') . '>Yes</option>';
    $html .= '</select>';
    $html .= '</div>';

    // accessible_has_no_contract
    $accessible_has_no_contract = !isset($restrictions['accessible_has_no_contract']) ? 1 : intval($restrictions['accessible_has_no_contract']);
    $html .= '<div>Accessible for has no contract&nbsp;';
    $html .= '<select name="form_data[restrictions][accessible_has_no_contract]">';
    $html .= '<option value="0"' . ($accessible_has_no_contract === 0 ? ' selected' : '') . '>No</option>';
    $html .= '<option value="1"' . ($accessible_has_no_contract === 1 ? ' selected' : '') . '>Yes</option>';
    $html .= '</select>';
    $html .= '</div>';

    // accessible_mobile
    $accessible_mobile = !isset($restrictions['accessible_mobile']) ? 1 : intval($restrictions['accessible_mobile']);

    $html .= '<div>Accessible on mobile site&nbsp;';
    $html .= '<select name="form_data[restrictions][accessible_mobile]">';
    $html .= '<option value="0"' . ($accessible_mobile === 0 ? ' selected' : '') . '>No</option>';
    $html .= '<option value="1"' . ($accessible_mobile === 1 ? ' selected' : '') . '>Yes</option>';
    $html .= '</select>';
    $html .= '</div>';

    // non_accessible_user_ids
    $non_accessible_user_ids = !isset($restrictions['non_accessible_user_ids']) ? '' : $restrictions['non_accessible_user_ids'];
    $html .= '<div>Non accessible with doctors&nbsp;';
    $html .= '<input type="text" name="form_data[restrictions][non_accessible_user_ids]" value="' . $non_accessible_user_ids . '"/>';
    $html .= '</div>';

    // search_locations
    $search_locations = !isset($restrictions['search_locations']) ? '' : $restrictions['search_locations'];
    $html .= '<div>Non accessible with locations&nbsp;';
    $html .= '<input type="text" name="form_data[restrictions][search_locations]" value="' . $search_locations . '"/>';
    $html .= '</div>';

    // accessible_from
    $accessible_from = !isset($restrictions['accessible_from']) ? '' : $restrictions['accessible_from'];
    $html .= '<div>Accessible from&nbsp;';
    $html .= '<input type="date" name="form_data[restrictions][accessible_from]" value="' . $accessible_from . '" class="datetimepicker" />';
    $html .= '</div>';

    // accessible_until
    $accessible_until = !isset($restrictions['accessible_until']) ? '' : $restrictions['accessible_until'];
    $html .= '<div>Accessible until&nbsp;';
    $html .= '<input type="date" name="form_data[restrictions][accessible_until]" value="' . $accessible_until . '" class="datetimepicker" />';
    $html .= '</div>';

    // accessible_medical_specialty_ids
    $medical_specialties = MedicalSpecialty::getHtmlOptions(true);
    $accessible_medical_specialty_ids = empty($restrictions['accessible_medical_specialty_ids']) ? array() : explode(',', $restrictions['accessible_medical_specialty_ids']);

    $html .= '<div>Accessible with medical specialties&nbsp;<br/>';
    $html .= '<input type="checkbox" id="check_uncheck_all"/> check/uncheck all<br/>';
    $html .= '<div class="specialties">';
    foreach ($medical_specialties as $msid => $specialty) {
        $html .= '<label><input type="checkbox" name="form_data[specialties][]" value="' . intval($msid) . '" ' .
                 (in_array($msid, $accessible_medical_specialty_ids) ? ' checked' : '') . '/>&nbsp;' . $specialty . '</label>';
    }
    $html .= '</div>';
    $html .= '</div>';

    // Hacky styling
    $html .= '<style type="text/css">.widefat div{margin:12px auto}.specialties label{float:none;text-align:left;margin:6px auto}</style>';

    // Check/uncheck button
    $html .= '<script type="text/javascript">$(function(){$("#check_uncheck_all").click(function(){$(".specialties input").prop("checked", this.checked)})});</script>';

    return $html;
}

function get_data_active($data = 0)
{
    return $GLOBALS["CONFIG"]["ARRAY_NO_YES"][$data['active']];
}

// CONFIG
// ==============================================================================================================
$MENU_ARRAY = array(
    "id"           => array("sql" => 1, "name" => "ID", "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "code"         => array("sql" => 1, "name" => "Code", "show" => 1, "edit" => 1, "new" => 1, "search" => 0, "sort" => 1, "nowrap" => 1),
    "active"       => array("sql" => 1, "name" => "Aktiv", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "select", "values" => $GLOBALS['CONFIG']['ARRAY_NO_YES']), "list_function" => array("name" => "get_data_active", "para" => 1)),
    "offer_type"   => array("sql" => 1, "name" => "Offer Type", "show" => 1, "edit" => 1, "new" => 1, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "function", "name" => "get_form_offer_type", "para" => 1), "list_function" => array("name" => "get_data_offer_type", "para" => 1)),
    "note"         => array("sql" => 1, "name" => 'Kommentar <span style="color:#ff0000;">(intern)</span>', "show" => 1, "edit" => 1, "new" => 1, "search" => 0, "sort" => 0, "nowrap" => 1),
    "restrictions" => array("sql" => 1, "name" => "Restrictions", "show" => 0, "edit" => 1, "new" => 1, "search" => 0, "sort" => 0, "nowrap" => 0, "form_input" => array("type" => "function", "name" => "get_form_restrictions", "para" => 1)),
    "type"         => array("sql" => 1, "name" => "Type", "show" => 1, "edit" => 1, "new" => 1, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "function", "name" => "get_form_type", "para" => 1), "list_function" => array("name" => "get_data_type", "para" => 1)),
    "created_by"   => array("sql" => 1, "name" => "Erstellt von", "show" => 1, "edit" => 0, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "created_at"   => array("sql" => 1, "name" => "Erstellt am", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"))
);

$BUTTON = array(
    "new"    => array("func" => "coupon_admin_new", "alt_text" => $app->static_text('new_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "document_new.png", "action" => "action=new", "th" => 1, "td" => 0),
    "edit"   => array("func" => "coupon_admin_edit", "alt_text" => $app->static_text('edit_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "document_edit.png", "action" => "action=edit", "th" => 0, "td" => 1),
    "delete" => array("func" => "coupon_admin_delete", "alt_text" => $app->static_text('delete_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "delete.png", "action" => "action=delete", "th" => 0, "td" => 1)
);

$TABLE_CONFIG = array(
    "table_name"  => DB_TABLENAME_COUPONS,
    "alpha_index" => "code",
);

$URL_PARA = array(
    'page'         => 1,
    'alpha'        => '',
    "sort"         => 0,
    "direction"    => 'DESC',
    "num_per_page" => 50,
    "type"         => 'num'
);
