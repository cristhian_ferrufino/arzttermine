<?php

use Arzttermine\Booking\Booking;
use Arzttermine\User\User;

function get_data_reviewed_at($data = 0)
{
    return mysql2date('d.m.Y H:i:s', $data['reviewed_at'], false);
}

function get_data_user_id($data = 0)
{
    $userId = $data['user_id'];
    $_user = new User($userId);
    if (!$_user->isValid()) {
        return 'n/a';
    }
    if ($_user->hasProfile()) {
        return '<a href="' . $_user->getUrl() . '" target="_blank">' . $_user->getName() . '</a>';
    } else {
        return $_user->getName();
    }
}

function get_data_booking_id($data = 0)
{
    $bookingId = $data['booking_id'];
    $booking = new Booking($bookingId);
    if (!$booking->isValid()) {
        return 'n/a';
    }

    return '<a href="' . $booking->getAdminEditUrl() . '" target="_blank">' . $booking->getId() . '</a>';
}

$MENU_ARRAY = array(
    "id"            => array("sql" => 1, "name" => "ID", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "user_id"       => array("sql" => 1, "name" => "Arzt", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_user_id", "para" => 1), "form_input" => array("type" => "none")),
    "booking_id"    => array("sql" => 1, "name" => "BookingID", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "list_function" => array("name" => "get_data_booking_id", "para" => 1)),
    "patient_email" => array("sql" => 1, "name" => "Patient eMail", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "patient_name"  => array("sql" => 1, "name" => "Patient Name", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "allow_sharing" => array("sql" => 1, "name" => "Allow Sharing", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "select", "values" => $GLOBALS['CONFIG']['ARRAY_NO_YES'])),
    "rating_1"      => array("sql" => 1, "name" => "Rating1", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "rating_2"      => array("sql" => 1, "name" => "Rating2", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "rating_3"      => array("sql" => 1, "name" => "Rating3", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "rate_text"     => array("sql" => 1, "name" => "RatingText", "show" => 0, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "rated_at"      => array("sql" => 1, "name" => "Rated at", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "approved_at"   => array("sql" => 1, "name" => "Approved at", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "datetime")),
);

$BUTTON = array(
    "new"    => array("func" => "review_admin_new", "alt_text" => $app->static_text('new_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "document_new.png", "action" => "action=new", "th" => 1, "td" => 0),
    "edit"   => array("func" => "review_admin_edit", "alt_text" => $app->static_text('edit_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "document_edit.png", "action" => "action=edit", "th" => 0, "td" => 1),
    "delete" => array("func" => "review_admin_delete", "alt_text" => $app->static_text('delete_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "delete.png", "action" => "action=delete", "th" => 0, "td" => 1),
    "cancel" => array("func" => "review_admin_cancel", "alt_text" => $app->static_text('cancel_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "cancel.png", "action" => "action=cancel", "th" => 0, "td" => 1),
    "search" => array("func" => "review_admin_search", "alt_text" => $app->static_text('search'), "script" => $_SERVER['PHP_SELF'], "gfx" => "search.png", "action" => "action=search", "th" => 1, "td" => 0),
    "csv"    => array("func" => "review_admin_csv", "alt_text" => 'Liste als CSV herunterladen', "script" => $_SERVER['PHP_SELF'], "gfx" => "download.gif", "action" => "action=csv", "th" => 1, "td" => 0),
);
$TABLE_CONFIG = array(
    "table_name"  => DB_TABLENAME_REVIEWS,
    "alpha_index" => "patient_email",
);
$URL_PARA = array(
    'page'         => 1,
    'alpha'        => '',
    "sort"         => 0,
    "direction"    => 'DESC',
    "num_per_page" => 50,
    "type"         => 'num'
);
