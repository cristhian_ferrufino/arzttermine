<?php

use Arzttermine\Application\Application;
use Arzttermine\Insurance\Insurance;
use Arzttermine\Location\District;
use Arzttermine\MedicalSpecialty\MedicalSpecialty;
use Arzttermine\User\User;

function get_data_created_by($data = 0)
{
    $_user = new User($data['created_by']);
    if (!$_user->isValid()) {
        return 'n/a';
    }

    return $_user->getFullName();
}

function get_data_created_at($data = 0)
{
    return mysql2date('d.m.Y H:i:s', $data['created_at'], false);
}

function get_data_updated_by($data = 0)
{
    $_user = new User($data['updated_by']);
    if (!$_user->isValid()) {
        return 'n/a';
    }

    return $_user->getFullName();
}

function get_data_status($data = 0)
{
    if (isset($GLOBALS['CONFIG']['SEARCH_RESULT_TEXT_STATUS'][$data['status']])) {
        return $GLOBALS['CONFIG']['SEARCH_RESULT_TEXT_STATUS'][$data['status']];
    } else {
        return 'n/a';
    }
}

function get_data_permalink($data = 0)
{
    if (isset($data['permalink'])) {
        return '<a href="' . $GLOBALS['CONFIG']['URL_HTTP_LIVE'] . $data['permalink'] . '">' . $data['permalink'] . '</a>';
    }

    return $data['permalink'];
}

function get_data_medical_specialty_id($data = 0)
{
    return MedicalSpecialty::getNameById($data['medical_specialty_id']);
}

function get_values_medical_specialty_id()
{
    return MedicalSpecialty::getHtmlOptions(true);
}

function get_data_district_id($data = 0)
{
    return District::getNameById($data['district_id']);
}

function get_data_insurance_id($data = 0)
{
    return Insurance::getNameById($data['insurance_id']);
}

function get_values_insurance_id()
{
    return Insurance::getHtmlOptions(true);
}

$MENU_ARRAY = array(
    "id"                    => array("sql" => 1, "name" => "ID", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "status"                => array("sql" => 1, "name" => "Status", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "select", "values" => $GLOBALS['CONFIG']['SEARCH_RESULT_TEXT_STATUS']), "list_function" => array("name" => "get_data_status", "para" => 1)),
    "permalink"             => array("sql" => 1, "name" => "Permalink", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_permalink", "para" => 1)),
    "medical_specialty_id"  => array("sql" => 1, "name" => "Fachrichtung", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_medical_specialty_id", "para" => 1), "form_input" => array("type" => "select", "values" => get_values_medical_specialty_id())),
    "insurance_id"          => array("sql" => 1, "name" => "Versicherung", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_insurance_id", "para" => 1), "form_input" => array("type" => "select", "values" => get_values_insurance_id())),
    "address_search"        => array("sql" => 1, "name" => "Adresssuchtext", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "district_id"           => array("sql" => 1, "name" => "Ortsteil", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_district_id", "para" => 1), "form_input" => array("type" => "select", "values" => District::getHtmlOptionsWithCity(true, $app->_('...optional...')))),
    "html_title"            => array("sql" => 1, "name" => 'HTML-Title', "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("template" => "admin/_form_text_counter.tpl")),
    "html_meta_description" => array("sql" => 1, "name" => 'HTML-Meta-Description', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("template" => "admin/_form_text_counter.tpl")),
    "html_meta_keywords"    => array("sql" => 1, "name" => 'HTML-Meta-Keywords', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("template" => "admin/_form_text_counter.tpl")),
    "content_1"             => array("sql" => 1, "name" => "Content", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "editor_tinymce", "values" => "")),
    "mf_average"            => array("sql" => 1, "name" => 'Bewertung', "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "mf_total"              => array("sql" => 1, "name" => 'Teilnehmer', "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "created_at"            => array("sql" => 1, "name" => "Erstellungsdatum", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "none")),
    "created_by"            => array("sql" => 1, "name" => "Erstellt von", "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_created_by", "para" => 1), "list_function" => array("name" => "get_data_created_by", "para" => 1)),
    "updated_at"            => array("sql" => 1, "name" => "Letztes Update", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "updated_by"            => array("sql" => 1, "name" => "Letzter Bearbeiter", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_updated_by", "para" => 1), "list_function" => array("name" => "get_data_updated_by", "para" => 1)),
);

$BUTTON = array(
    "new"    => array("func" => "searchresulttext_admin_new", "alt_text" => $app->static_text('new_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "document_new.png", "action" => "action=new", "th" => 1, "td" => 0),
    "edit"   => array("func" => "searchresulttext_admin_edit", "alt_text" => $app->static_text('edit_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "document_edit.png", "action" => "action=edit", "th" => 0, "td" => 1),
    "delete" => array("func" => "searchresulttext_admin_delete", "alt_text" => $app->static_text('delete_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "delete.png", "action" => "action=delete", "th" => 0, "td" => 1),
    "search" => array("func" => "searchresulttext_admin_search", "alt_text" => $app->static_text('search'), "script" => $_SERVER['PHP_SELF'], "gfx" => "search.png", "action" => "action=search", "th" => 1, "td" => 0),
    //	"csv"			=> array("func"=>"searchresulttext_admin_csv","alt_text"=>'Liste als CSV herunterladen',"script"=>$_SERVER['PHP_SELF'],"gfx"=>"download.gif","action"=>"action=csv","th"=>1,"td"=>0),
);
$TABLE_CONFIG = array(
    "table_name"  => DB_TABLENAME_SEARCHRESULTTEXTS,
    "alpha_index" => "name",
);
$URL_PARA = array(
    'page'         => 1,
    'alpha'        => '',
    "sort"         => 0,
    "direction"    => 'DESC',
    "num_per_page" => 25,
    "type"         => 'num'
);