<?php

function get_data_autoload($data = 0)
{
    return $GLOBALS["CONFIG"]["ARRAY_NO_YES"][$data['autoload']];
}

function get_data_option_value($data = 0)
{
    $option_value = $data['option_value'];
    if (!empty($option_value)) {
        $option_value = mb_substr($option_value, 0, 20, $GLOBALS['CONFIG']['SYSTEM_CHARSET']);

        if (mb_strlen($data['option_value'], $GLOBALS['CONFIG']['SYSTEM_CHARSET']) > 20) {
            $option_value .= '...';
        }
    }

    return $option_value;
}

// CONFIG
// ==============================================================================================================
$MENU_ARRAY = array(
    "id"           => array("sql" => 1, "name" => "ID", "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "option_name"  => array("sql" => 1, "name" => "OptionName", "show" => 1, "edit" => 1, "new" => 1, "search" => 0, "sort" => 1, "nowrap" => 1),
    "option_value" => array("sql" => 1, "name" => "OptionValue", "show" => 1, "edit" => 1, "new" => 1, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "textarea", "optional" => ' cols="60" rows="10" style="width:300px;"'), "list_function" => array("name" => "get_data_option_value", "para" => 1)),
    "autoload"     => array("sql" => 1, "name" => "Autoload", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "select", "values" => $GLOBALS['CONFIG']['ARRAY_NO_YES']), "list_function" => array("name" => "get_data_autoload", "para" => 1)),
);

$BUTTON = array(
    "new"    => array("func" => "configuration_admin_new", "alt_text" => $app->static_text('new_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "document_new.png", "action" => "action=new", "th" => 1, "td" => 0),
    "edit"   => array("func" => "configuration_admin_edit", "alt_text" => $app->static_text('edit_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "document_edit.png", "action" => "action=edit", "th" => 0, "td" => 1),
    "delete" => array("func" => "configuration_admin_delete", "alt_text" => $app->static_text('delete_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "delete.png", "action" => "action=delete", "th" => 0, "td" => 1)
);

$TABLE_CONFIG = array(
    "table_name"  => DB_TABLENAME_CONFIGURATION,
    "alpha_index" => "option_name",
);

$URL_PARA = array(
    'page'         => 1,
    'alpha'        => '',
    "sort"         => 0,
    "direction"    => 'DESC',
    "num_per_page" => 50,
    "type"         => 'num'
);
