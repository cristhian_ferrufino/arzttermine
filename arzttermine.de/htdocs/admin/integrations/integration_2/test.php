<?php
/**
 * Test
 *
 * @author Axel Kuzmik <axel@kuzmik.de>
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/../include/bootstrap.php';
require_once "config.php";

/* ************************************************************************************* */

use Arzttermine\Admin\Admin;
use Arzttermine\Application\Application;
use Arzttermine\Core\DateTime;
use Arzttermine\Integration\Integration\Terminland\Integration;
use Arzttermine\Integration\Integration\Terminland\Data;

$admin = new Admin();
$app = Application::getInstance();
$view = $app->getView();
$profile = $app->getCurrentUser();

/* ************************************************************************************* */
if (!$profile->checkPrivilege('integration_admin')) {
    $view->setRef('ContentRight', $app->static_file('access_denied.html'));
    $view->setRef('ContentLeft', $admin->get_navigation());
    $view->display('admin/content_left_right.tpl');
    exit;
}

// **********************************************************************
// START ****************************************************************
$action = getParam("a", "profile");
$form = getParam("form");

$template = 'profile';
$_result = 'Need Action!';

switch ($action) {
    // ************************************************
    case 'getAvailableAppointmentsTerminland':
        // calendar-times
        $startDateTime = new DateTime(getParam('date_start'));
        $endDateTime = new DateTime(getParam('date_end'));

        $_integration = new Integration();
        $insurance_id = getParam('insurance_id', '1');
        $integrationData = new Data(getParam('id'));
        if ($integrationData->isValid()) {
            $_result = $_integration->getAvailableAppointmentsTerminland($startDateTime, $endDateTime, $integrationData, $insurance_id, 'debug');
        } else {
            echo "unvalid integrationDataID!";
        }
        break;
    case 'getNextAvailableAppointmentsTerminland':
        // calendar-times
        $date_start = getParam('date_start');
        $date_start = max(date('Y-m-d', strtotime($date_start)), date('Y-m-d', strtotime('tomorrow')));

        $_integration = new Integration();
        $insurance_id = getParam('insurance_id', '1');
        $terminland_id = getParam('terminland_id', 'arzttermine.de');
        $terminplan_id = getParam('terminplan_id', 'tp1');
        $_result = $_integration->getNextAvailableAppointmentsTerminland($date_start, $date_end, $insurance_id, $terminland_id, $terminplan_id, 'debug');
        break;
    case 'getTerminlandTypes':
        // wsdl result
        $_integration = new Integration();
        $terminland_id = getParam('terminland_id', 'arzttermine.de');
        $terminplan_id = getParam('terminplan_id', 'tp1');
        $_result = $_integration->getTerminlandTypes($terminland_id, $terminplan_id, 'debug');
        break;
    case 'getTerminlandFunctions':
        // wsdl functions / documentation
        $_result = 'https://www.terminland.de/arzttermine.de/tlsoap/default.asmx';
        break;
}
// ********************************************************************

if ($template == "profile") {
    $view->setRef('ContentRight', '<div style="padding:0 1em 1em 1em;">' . $_result . '</div>');
}

$view->setHeadTitle('Terminland Admin');
$view->setRef('ContentLeft', '&nbsp;DEBUG Terminland');
$view->display('admin/content_left_right.tpl');