<?php

use Arzttermine\Location\Location;
use Arzttermine\User\User;

function get_data_created_by($data = 0)
{
    $_user = new User($data['created_by']);
    if (!$_user->isValid()) {
        return 'n/a';
    }

    return $_user->getFullName();
}

function get_data_created_at($data = 0)
{
    return mysql2date('d.m.Y H:i:s', $data['created_at'], false);
}

function get_data_updated_by($data = 0)
{
    $_user = new User($data['updated_by']);
    if (!$_user->isValid()) {
        return 'n/a';
    }

    return $_user->getFullName();
}

function get_data_user_id($data = 0)
{
    $_user = new User($data['user_id']);
    if (!$_user->isValid()) {
        return 'n/a';
    }
    if ($_user->hasProfile()) {
        return '<a href="' . $_user->getUrl() . '" target="_blank">' . $_user->getName() . '</a>';
    } else {
        return $_user->getName();
    }
}

function get_data_location_id($data = 0)
{
    $_location = new Location($data['location_id']);
    if (!$_location->isValid()) {
        return 'n/a';
    }

    return '<a href="' . $_location->getUrl() . '" target="_blank">' . $_location->getName() . '</a>';
}

function get_data_test($data = 0)
{
    $_html = '<a href="' . $GLOBALS['CONFIG']['INTEGRATIONS'][2]['URL_TEST'] . '?a=getAvailableAppointmentsTerminland&id=' . $data['id'] . '&date_start=' . date('Y-m-d', strtotime('tomorrow')) . '&date_end=' . date('Y-m-d', strtotime('tomorrow + 3 days')) . '" target="_blank">getAvailableAppointmentsTerminland()</a>';
/*
 * getNextAvailableAppointmentsTerminland is not available at the moment
 * @todo: need to check if we still need this basic call
 *
 * $_html .= '<br /><a href="' . $GLOBALS['CONFIG']['INTEGRATIONS'][2]['URL_TEST'] . '?a=getNextAvailableAppointmentsTerminland&terminland_id=' . $data['terminland_id'] . '&terminplan_id=' . $data['terminplan_id'] . '&date_start=' . date('Y-m-d') . '" target="_blank">getNextAvailableAppointmentsTerminland()</a>';
 */
    return $_html;
}

$MENU_ARRAY = array(
    "id"            => array("sql" => 1, "name" => "ID", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "user_id"       => array("sql" => 1, "name" => "Arzt", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_user_id", "para" => 1)),
    "location_id"   => array("sql" => 1, "name" => "Praxis", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_location_id", "para" => 1)),
    "terminland_id" => array("sql" => 1, "name" => "TerminLandID", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0),
    "terminplan_id" => array("sql" => 1, "name" => "TerminPlanID", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0),
    "test"          => array("sql" => 0, "name" => "Test", "show" => 1, "edit" => 0, "new" => 0, "search" => 0, "sort" => 0, "nowrap" => 0, "list_function" => array("name" => "get_data_test", "para" => 1)),
    "created_at"    => array("sql" => 1, "name" => "Erstellungsdatum", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "none")),
    "created_by"    => array("sql" => 1, "name" => "Erstellt von", "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_created_by", "para" => 1), "list_function" => array("name" => "get_data_created_by", "para" => 1)),
    "updated_at"    => array("sql" => 1, "name" => "Letztes Update", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "updated_by"    => array("sql" => 1, "name" => "Letzter Bearbeiter", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_updated_by", "para" => 1), "list_function" => array("name" => "get_data_updated_by", "para" => 1)),
);

$BUTTON = array(
    "new"    => array("func" => "integration_admin_new", "alt_text" => $app->static_text('new_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "document_new.png", "action" => "action=new", "th" => 1, "td" => 0),
    "edit"   => array("func" => "integration_admin_edit", "alt_text" => $app->static_text('edit_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "document_edit.png", "action" => "action=edit", "th" => 0, "td" => 1),
    "delete" => array("func" => "integration_admin_delete", "alt_text" => $app->static_text('delete_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "delete.png", "action" => "action=delete", "th" => 0, "td" => 1),
    "search" => array("func" => "integration_admin_search", "alt_text" => $app->static_text('search'), "script" => $_SERVER['PHP_SELF'], "gfx" => "search.png", "action" => "action=search", "th" => 1, "td" => 0),
    "csv"    => array("func" => "integration_admin_csv", "alt_text" => 'Liste als CSV herunterladen', "script" => $_SERVER['PHP_SELF'], "gfx" => "download.gif", "action" => "action=csv", "th" => 1, "td" => 0),
);
$TABLE_CONFIG = array(
    "table_name"  => DB_TABLENAME_INTEGRATION_2_DATAS,
    "alpha_index" => "terminland_id",
);
$URL_PARA = array(
    'page'         => 1,
    'alpha'        => '',
    "sort"         => 0,
    "direction"    => 'DESC',
    "num_per_page" => 25,
    "type"         => 'num'
);