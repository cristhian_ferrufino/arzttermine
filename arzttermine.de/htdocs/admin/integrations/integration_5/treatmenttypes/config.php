<?php

use Arzttermine\User\User;
use Arzttermine\MedicalSpecialty\TreatmentType;

function get_data_created_by($data = 0)
{
    $_user = new User($data['created_by']);
    if (!$_user->isValid()) {
        return 'n/a';
    }

    return '<a href="'.$_user->getAdminEditUrl().'" target="_blank">'.$_user->getFullName().'</a>';
}

function get_data_parent_id($data = 0)
{
    $treatmenttype = new TreatmentType($data['parent_id']);
    $name = '';
    if ($treatmenttype->isValid()) {
        $name = $treatmenttype->getName() . ' ['.$treatmenttype->getId().']';
    }
    return $name;
}


$MENU_ARRAY = array(
    "id"            => array("sql" => 1, "name" => "ID", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "name"          => array("sql" => 1, "name" => "Name", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "parent_id"     => array("sql" => 1, "name" => "AT Name [ParentID]", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_parent_id", "para" => 1), "form_input" => array("type" => "function", "name" => "get_data_parent_id", "para" => 1)),
    "duration"      => array("sql" => 1, "name" => "Duration", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "insurance_ids" => array("sql" => 1, "name" => "AT InsuranceIDs", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "created_at"    => array("sql" => 1, "name" => "Erstellungsdatum", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "none")),
);

$BUTTON = array(
    "new"    => array("func" => "integration_admin_new", "alt_text" => $app->static_text('new_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "document_new.png", "action" => "action=new", "th" => 1, "td" => 0),
    "edit"   => array("func" => "integration_admin_edit", "alt_text" => $app->static_text('edit_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "document_edit.png", "action" => "action=edit", "th" => 0, "td" => 1),
    "delete" => array("func" => "integration_admin_delete", "alt_text" => $app->static_text('delete_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "delete.png", "action" => "action=delete", "th" => 0, "td" => 1),
    "search" => array("func" => "integration_admin_search", "alt_text" => $app->static_text('search'), "script" => $_SERVER['PHP_SELF'], "gfx" => "search.png", "action" => "action=search", "th" => 1, "td" => 0),
    "csv"    => array("func" => "integration_admin_csv", "alt_text" => 'Liste als CSV herunterladen', "script" => $_SERVER['PHP_SELF'], "gfx" => "download.gif", "action" => "action=csv", "th" => 1, "td" => 0),
);
$TABLE_CONFIG = array(
    "table_name"  => DB_TABLENAME_INTEGRATION_5_TREATMENTTYPES,
    "alpha_index" => "name",
);
$URL_PARA = array(
    'page'         => 1,
    'alpha'        => '',
    "sort"         => 0,
    "direction"    => 'DESC',
    "num_per_page" => 25,
    "type"         => 'num'
);