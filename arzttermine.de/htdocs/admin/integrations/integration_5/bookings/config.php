<?php

use Arzttermine\User\User;
use Arzttermine\Booking\Booking;
use Arzttermine\MedicalSpecialty\TreatmentType;
use Arzttermine\Integration\Integration\Samedi\Booking as SamediBooking;
use Arzttermine\Integration\Integration\Samedi\TreatmentType as SamediTreatmentType;

function get_data_created_by($data = 0)
{
    $_user = new User($data['created_by']);
    if (!$_user->isValid()) {
        return 'n/a';
    }

    return '<a href="'.$_user->getAdminEditUrl().'" target="_blank">'.$_user->getFullName().'</a>';
}

function get_data_created_at($data = 0)
{
    return mysql2date('d.m.Y H:i:s', $data['created_at'], false);
}

function get_data_updated_by($data = 0)
{
    $_user = new User($data['updated_by']);
    if (!$_user->isValid()) {
        return 'n/a';
    }

    return '<a href="'.$_user->getAdminEditUrl().'" target="_blank">'.$_user->getFullName().'</a>';
}

function get_data_status($data = 0)
{
    $status_text = SamediBooking::getStatusText($data['status']);

    return $status_text;
}

function get_data_booking_id($data = 0)
{
    $booking = new Booking($data['booking_id']);
    if (!$booking->isValid()) {
        return 'n/a';
    }
    return '<a href="' . $booking->getAdminEditUrl() . '" target="_blank">' . $booking->getId() . '</a>/'.$booking->getStatusText();
}

function get_data_event_type_id($data = 0)
{
    $samediTreatmentType = new SamediTreatmentType($data['event_type_id']);
    $treatmentType = new TreatmentType($samediTreatmentType->getParentId());
    return $samediTreatmentType->getName() . ' = ' . $treatmentType->getName();
}


$MENU_ARRAY = array(
    "id"          => array("sql" => 1, "name" => "ID", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "status"      => array("sql" => 1, "name" => "Status", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_status", "para" => 1), "form_input" => array("type" => "function", "name" => "get_data_status", "para" => 1)),
    "booking_id"  => array("sql" => 1, "name" => "AT BookingID", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_booking_id", "para" => 1), "form_input" => array("type" => "function", "name" => "get_data_booking_id", "para" => 1)),
    "category_id" => array("sql" => 1, "name" => "Samedi Category = Doctor", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "practice_id" => array("sql" => 1, "name" => "Samedi Practice", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "event_type_id" => array("sql" => 1, "name" => "Samedi EventTypeID = AT TT", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_event_type_id", "para" => 1), "form_input" => array("type" => "function", "name" => "get_data_event_type_id", "para" => 1)),
    "cancelable_until"  => array("sql" => 1, "name" => "Cancelable until", "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "result_json" => array("sql" => 1, "name" => "SamediResultJson", "show" => 0, "edit" => 1, "new" => 0, "search" => 1, "sort" => 0, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => ' style="width:300px;height:60px;"')),
    "created_at"  => array("sql" => 1, "name" => "Erstellungsdatum", "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "created_by"  => array("sql" => 1, "name" => "Erstellt von", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_created_by", "para" => 1), "list_function" => array("name" => "get_data_created_by", "para" => 1)),
    "updated_at"  => array("sql" => 1, "name" => "Letztes Update", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "updated_by"  => array("sql" => 1, "name" => "Letzter Bearbeiter", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_updated_by", "para" => 1), "list_function" => array("name" => "get_data_updated_by", "para" => 1)),
);

$BUTTON = array(
    "new"    => array("func" => "integration_admin_new", "alt_text" => $app->static_text('new_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "document_new.png", "action" => "action=new", "th" => 1, "td" => 0),
    "edit"   => array("func" => "integration_admin_edit", "alt_text" => $app->static_text('edit_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "document_edit.png", "action" => "action=edit", "th" => 0, "td" => 1),
    "delete" => array("func" => "integration_admin_delete", "alt_text" => $app->static_text('delete_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "delete.png", "action" => "action=delete", "th" => 0, "td" => 1),
    "search" => array("func" => "integration_admin_search", "alt_text" => $app->static_text('search'), "script" => $_SERVER['PHP_SELF'], "gfx" => "search.png", "action" => "action=search", "th" => 1, "td" => 0),
    "csv"    => array("func" => "integration_admin_csv", "alt_text" => 'Liste als CSV herunterladen', "script" => $_SERVER['PHP_SELF'], "gfx" => "download.gif", "action" => "action=csv", "th" => 1, "td" => 0),
);
$TABLE_CONFIG = array(
    "table_name"  => DB_TABLENAME_INTEGRATION_5_BOOKINGS,
    "alpha_index" => "practice_id",
);
$URL_PARA = array(
    'page'         => 1,
    'alpha'        => '',
    "sort"         => 0,
    "direction"    => 'DESC',
    "num_per_page" => 25,
    "type"         => 'num'
);