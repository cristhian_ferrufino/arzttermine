<?php
/**
 * Admin: Integration 10
 *
 * @author Axel Kuzmik <axel@kuzmik.de>
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/../include/bootstrap.php';
require_once "config.php";

/* ************************************************************************************* */

use Arzttermine\Admin\Admin;
use Arzttermine\Application\Application;
use Arzttermine\Location\Location;
use Arzttermine\User\User;

$admin = new Admin();
$app = Application::getInstance();
$view = $app->getView();
$profile = $app->getCurrentUser();

$view->disableCaching();

/* ************************************************************************************* */

if (!$profile->checkPrivilege('integration_admin')) {
    $view->setRef('ContentRight', $app->static_file('access_denied.html'));
    $view->setRef('ContentLeft', $admin->get_navigation());
    $view->display('admin/content_left_right.tpl');
    exit;
}
/* ************************************************************************************* */
function get_list($URL_PARA, $search = 0)
{
    global $admin;

    $_html = $admin->admin_get_list(0, $search, $URL_PARA);

    return $_html;
}

/* ************************************************************************************* */
function new_one($action, $form_data, $URL_PARA)
{
    global $profile, $app, $admin, $view;

    // 1. set defaults
    $form_data["created_by"] = $profile->getId();
    $form_data["created_at"] = $app->now();

    // 2. check
    $_user = new User($form_data["user_id"]);
    if (!$_user->isValid()) {
        $view->addMessage('admin_message_user_id', 'ERROR', $app->static_text('Die ArztID ist nicht gültig'));
    }
    $_location = new Location($form_data["location_id"]);
    if (!$_location->isValid()) {
        $view->addMessage('admin_message_location_id', 'ERROR', $app->static_text('Die PraxisID ist nicht gültig'));
    }

    // if there are checkboxes than set the keys to make sure that deselected checkboxes are beeing set
    $form_data = $admin->set_unselected_checkbox_values($form_data);

    // 3. exit on errors
    if ($view->countMessages()) {
        $view->addMessage('STATUS', 'ERROR', $app->_('Bitte prüfen Sie Ihre Eingaben.'));
        $view->setRef('ContentRight', form_change($action, 0, $URL_PARA, $form_data));

        return $action;
    }
    // 4. sanitize
    //$form_data["first_name"] = sanitize_title($form_data["first_name"]);

    $_id = $admin->admin_set_new($form_data);
    if ($_id > 0) {
        $view->addMessage('STATUS', 'NOTICE', $app->static_text('Die Integration wurde erfolgreich mit der ID ' . $_id . ' angelegt.<br /><a href="/admin/integrations/integration_10/index.php?action=edit&id=' . $_id . '">Integration bearbeiten</a>'));
        $action = 'list';
    } elseif ($_id == -1) {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('the_entry_allready_exists'));
        $view->setRef('ContentRight', form_new($action, $URL_PARA, $form_data));
        $action = 'list';
    } elseif ($_id == -2) {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('name_of_primary_key_is_empty'));
        $view->setRef('ContentRight', form_new($action, $URL_PARA, $form_data));
        $action = 'list';
    } else {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('the_entry_could_not_be_created'));
        $action = 'list';
    }

    return $action;
}

/* ************************************************************************************* */
function change_one($action, $id, $form_data, $URL_PARA)
{
    global $profile, $app, $admin, $view;

    // 1. set defaults
    $form_data["updated_by"] = $profile->getId();
    $form_data["updated_at"] = $app->now();

    // 2. check
    $_user = new User($form_data["user_id"]);
    if (!$_user->isValid()) {
        $view->addMessage('admin_message_user_id', 'ERROR', $app->static_text('Die ArztID ist nicht gültig'));
    }
    $_location = new Location($form_data["location_id"]);
    if (!$_location->isValid()) {
        $view->addMessage('admin_message_location_id', 'ERROR', $app->static_text('Die PraxisID ist nicht gültig'));
    }

    // if there are checkboxes than set the keys to make sure that deselected checkboxes are beeing set
    $form_data = $admin->set_unselected_checkbox_values($form_data);

    // 3. exit on errors
    if ($view->countMessages()) {
        $view->addMessage('STATUS', 'ERROR', $app->_('Bitte prüfen Sie Ihre Eingaben.'));
        $view->setRef('ContentRight', form_change($action, $id, $URL_PARA, $form_data));

        return $action;
    }
    // 4. sanitize
    //$form_data["first_name"] = sanitize_title($form_data["first_name"]);

    // if there are checkboxes than set the keys to make sure that deselected checkboxes are beeing set
    $form_data = $admin->set_unselected_checkbox_values($form_data);

    $_result = $admin->admin_set_update($id, $form_data);
    if ($_result == 1) {
        $view->addMessage('STATUS', 'NOTICE', $app->static_text('action_was_successfully'));
        $action = 'list';
    } elseif ($_result == -1) {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('the_entry_allready_exists'));
        $view->setRef('ContentRight', form_change($action, $id, $URL_PARA, $form_data));
        $action = 'list';
    } elseif ($_result == -2) {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('name_of_primary_key_is_empty'));
        $view->setRef('ContentRight', form_change($action, $id, $URL_PARA, $form_data));
        $action = 'list';
    } else {
        $view->addMessage('STATUS', 'WARNING', $app->static_text('no_data_has_been_updated'));
        $view->setRef('ContentRight', form_change($action, $id, $URL_PARA, $form_data));
        $action = 'list';
    }

    return $action;
}

/* ************************************************************************************* */
function ask4delete($id, $URL_PARA)
{
    global $admin;

    $_html = $admin->admin_get_confirm_table($id, $URL_PARA, 'delete');

    return $_html;
}

/* ************************************************************************************* */
function delete_one($id)
{
    global $admin;

    return $admin->admin_set_delete($id);
}

/* ************************************************************************************* */
function form_new($action, $URL_PARA, $error_data = 0)
{
    global $admin, $view;

    $_html = $admin->admin_get_form($action, 0, $error_data, $URL_PARA);

    $view->setFocus('#form_data_first_name');

    return $_html;
}

/* ************************************************************************************* */
function form_change($action, $id, $URL_PARA, $error_data = 0)
{
    global $admin, $view;

    $_html = $admin->admin_get_form($action, $id, $error_data, $URL_PARA);

    $view->setFocus('#form_data_first_name');

    return $_html;
}

/* ************************************************************************** */
function show_search_form($action, $search_data, $URL_PARA)
{
    global $admin;

    return $admin->admin_get_form($action, 0, $search_data, $URL_PARA);
}

/* ************************************************************************** */
function update_appointments($id)
{
    global $app, $view;

    $data = new \Arzttermine\Integration\Integration\Charly\Data($id);
    if ($data->isValid() && $data->updateAvailableAppointments(null, null, false, false)) {
        $view->addMessage('STATUS', 'NOTICE', $app->_('Appointments have been updated'));
    } else {
        $view->addMessage('STATUS', 'ERROR', $app->_('Something went wrong'));
    }

    return true;
}

/* ************************************************************************************** */
/* Anfragen auswerten ******************************************************************* */
//URL_PARA neu bestimmen
foreach ($URL_PARA as $key => $val) {
    if (isset($_REQUEST[$key])) {
        $URL_PARA[$key] = $_REQUEST[$key];
    }
}
$action = getParam("action", 'list');
$form_data = getParam("form_data");

switch ($action) {
    case "edit":
        if (!$profile->checkPrivilege('integration_admin_edit')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        } else {
            $id = getParam("id");
            if (is_array($form_data)) {
                $action = change_one($action, $id, $form_data, $URL_PARA);
            } else {
                $view->setRef('ContentRight', form_change($action, $id, $URL_PARA));
            }
        }
        break;
    case "delete":
        if (!$profile->checkPrivilege('integration_admin_delete')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        } else {
            if (!isset($_REQUEST["confirm_id"])) {
                $id = getParam("id");
                $view->setRef('ContentRight', ask4delete($id, $URL_PARA));
            } else {
                $confirm_id = getParam("confirm_id");
                if (delete_one($confirm_id)) {
                    $view->addMessage('STATUS', 'NOTICE', $app->static_text('the_entry_was_deleted_successfully'));
                } else {
                    $view->addMessage('STATUS', 'ERROR', $app->static_text('the_entry_could_not_be_deleted'));
                }
                $action = 'list';
            }
        }
        break;
    case "search":
        if (!$profile->checkPrivilege('integration_admin_search')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        }
        if (isset($_REQUEST["send_button"])) {
            $URL_PARA["page"] = 1;
            $URL_PARA["alpha"] = '';
        }
        $view->setRef('ContentRight', show_search_form($action, $form_data, $URL_PARA));
        if (is_array($form_data)) {
            $view->append('ContentRight', get_list($URL_PARA, $form_data));
        }
        break;
    case "new":
        if (!$profile->checkPrivilege('integration_admin_new')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        }
        if (is_array($form_data)) {
            $action = new_one($action, $form_data, $URL_PARA);
        } else {
            $view->setRef('ContentRight', form_new($action, $URL_PARA));
        }
        break;
    case "update":
        if (!$profile->checkPrivilege('integration_admin_delete')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        }
        $id = getParam("id");
        update_appointments($id);
        $action = 'list';
        break;
    case "csv":
        if (!$profile->checkPrivilege('integration_admin_csv')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        }
        show_csv();
        exit;
        break;
}

if ($action == 'list') {
    if ($profile->checkPrivilege('integration_admin_view')) {
        $view->setRef('ContentRight', get_list($URL_PARA));
    }
}

$view->setHeadTitle('Integration 10 / Charly Admin');
// add the integration navi
$view->setRef('integrationNavigationActive', 'data');
$view->prepend('ContentRight', $view->fetch('admin/integrations/integration_10/_top-navigation.tpl'));
$view->setRef('ContentLeft', $admin->get_navigation());
$view->display('admin/content_left_right.tpl');
