<?php
function get_data_dens_ids($data = 0)
{
    $_user = new \Arzttermine\User\User($data['user_id']);
    if (!$_user->isValid()) {
        return 'n/a';
    }
    if ($_user->hasProfile()) {
        return '<a href="' . $_user->getUrl() . '" target="_blank">' . $_user->getName() . '</a>';
    } else {
        return $_user->getName();
    }
}

function get_data_request($data = 0)
{
    return '<pre>' . htmlentities($data['request']) . '</pre>';
}

function get_data_response($data = 0)
{
    return '<pre>' . htmlentities($data['response']) . '</pre>';
}

$MENU_ARRAY = array(
    "id"         => array("sql" => 1, "name" => "id", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0),
    "status"     => array("sql" => 1, "name" => "Status", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0),
    "dens_ids"   => array("sql" => 1, "name" => "DENS-IDs", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0),
    "ip_address" => array("sql" => 1, "name" => "IP-Address", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "request"    => array("sql" => 1, "name" => "Request", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_request", "para" => 1)),
    "response"   => array("sql" => 1, "name" => "Response", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_response", "para" => 1)),
    "created_at" => array("sql" => 1, "name" => "Erstellungsdatum", "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "none")),
);

$BUTTON = array(
#	"new"			=> array("func"=>"integration_admin_new","alt_text"=>$app->static_text('new_item'),"script"=>$_SERVER['PHP_SELF'],"gfx"=>"document_new.png","action"=>"action=new","th"=>1,"td"=>0),
"edit"   => array("func" => "integration_admin_edit", "alt_text" => $app->static_text('edit_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "document_edit.png", "action" => "action=edit", "th" => 0, "td" => 1),
#	"delete"		=> array("func"=>"integration_admin_delete","alt_text"=>$app->static_text('delete_item'),"script"=>$_SERVER['PHP_SELF'],"gfx"=>"delete.png","action"=>"action=delete","th"=>0,"td"=>1),
"search" => array("func" => "integration_admin_search", "alt_text" => $app->static_text('search'), "script" => $_SERVER['PHP_SELF'], "gfx" => "search.png", "action" => "action=search", "th" => 1, "td" => 0),
#	"csv"			=> array("func"=>"integration_admin_csv","alt_text"=>'Liste als CSV herunterladen',"script"=>$_SERVER['PHP_SELF'],"gfx"=>"download.gif","action"=>"action=csv","th"=>1,"td"=>0),
);
$TABLE_CONFIG = array(
    "table_name"  => DB_TABLENAME_INTEGRATION_6_LOGS,
    "alpha_index" => "dens_ids",
);
$URL_PARA = array(
    'page'         => 1,
    'alpha'        => '',
    "sort"         => 0,
    "direction"    => 'DESC',
    "num_per_page" => 25,
    "type"         => 'num'
);