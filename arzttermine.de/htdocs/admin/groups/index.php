<?php
/**
 * Admin: Users
 *
 * @author Axel Kuzmik <axel@kuzmik.de>
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/../include/bootstrap.php';
require_once "config.php";

/* ************************************************************************************* */

use Arzttermine\Admin\Admin;
use Arzttermine\Application\Application;

$admin = new Admin();
$app = Application::getInstance();
$view = $app->getView();
$profile = $app->getCurrentUser();

$view->disableCaching();

/* ************************************************************************************* */

if (!$profile->checkPrivilege('group_admin')) {
    $view->setRef('ContentRight', $app->static_file('access_denied.html'));
    $view->setRef('ContentLeft', $admin->get_navigation());
    $view->display('admin/content_left_right.tpl');
    exit;
}

/* ************************************************************************************* */
function get_list($URL_PARA, $search = 0)
{
    global $admin;

    $_html = $admin->admin_get_list(0, $search, $URL_PARA);

    return $_html;
}

/* ************************************************************************************* */
function new_one($action, $form_data, $URL_PARA)
{
    global $profile, $admin, $view, $CONFIG, $app;

    $form_data["updated_by"] = $profile->getId();
    $form_data["updated_at"] = $app->now();

    foreach ($CONFIG['GROUP_ACCESS'] as $access_id => $accesses) {
        if (isset($form_data["access_id"][$access_id])) {
            $tmp_access_id[] = $access_id;
        }
        foreach ($accesses as $sub_access_id) {
            if (isset($form_data["access_id"][$access_id . '_' . $sub_access_id])) {
                $tmp_access_id[] = $access_id . '_' . $sub_access_id;
            }
        }
    }

    if (!isset($form_data["name"]) || $form_data["name"] == '') {
        $view->addMessage('STATUS', 'WARNING', $app->static_text('need_name'));
        $view->setRef('ContentRight', form_change($action, $id, $URL_PARA, $form_data));

        return $action;
    }

    unset($form_data["access_id"]);

    $_id = $admin->admin_set_new($form_data);
    if ($_id > 0) {
        if (!empty($tmp_access_id) && is_array($tmp_access_id)) {
            foreach ($tmp_access_id as $access_id) {
                $app->getSql()->query('INSERT INTO ' . DB_TABLENAME_GROUP_ACCESSES . ' (group_id, access_id) VALUES (' . $_id . ',"' . $access_id . '");');
            }
        }

        $view->addMessage('STATUS', 'NOTICE', $app->static_text('action_was_successfully'));
        $action = 'list';
    } elseif ($_id == -1) {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('the_entry_allready_exists'));
        $view->setRef('ContentRight', form_new($action, $URL_PARA, $form_data));
        $action = 'list';
    } elseif ($_id == -2) {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('name_of_primary_key_is_empty'));
        $view->setRef('ContentRight', form_new($action, $URL_PARA, $form_data));
        $action = 'list';
    } else {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('the_entry_could_not_be_created'));
        $view->setRef('ContentRight', form_new($action, $URL_PARA, $form_data));
        $action = 'list';
    }

    return $action;
}

/* ************************************************************************************* */
function change_one($action, $id, $form_data, $URL_PARA)
{
    global $profile, $admin, $view, $CONFIG, $app;

    $form_data["updated_by"] = $profile->getId();
    $form_data["updated_at"] = $app->now();

    foreach ($CONFIG['GROUP_ACCESS'] as $access_id => $accesses) {
        if (isset($form_data["function_id"][$access_id])) {
            $tmp_access_id[] = $access_id;
        }
        foreach ($accesses as $sub_access_id) {
            if (isset($form_data["access_id"][$access_id . '_' . $sub_access_id])) {
                $tmp_access_id[] = $access_id . '_' . $sub_access_id;
            }
        }
    }
    unset($form_data["access_id"]);
    $_result = $admin->admin_set_update($id, $form_data);
    if ($_result == 1) {
        /* first delete all old entries */
        $app->getSql()->query('DELETE FROM ' . DB_TABLENAME_GROUP_ACCESSES . ' WHERE group_id = ' . $form_data["id"] . ';');
        if (!empty($tmp_access_id) && is_array($tmp_access_id)) {
            foreach ($tmp_access_id as $access_id) {
                $app->getSql()->query('INSERT INTO ' . DB_TABLENAME_GROUP_ACCESSES . ' (group_id, access_id) VALUES (' . $form_data["id"] . ',"' . $access_id . '");');
            }
        }

        $view->addMessage('STATUS', 'NOTICE', $app->static_text('action_was_successfully'));
        $action = 'list';
    } elseif ($_result == -1) {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('the_entry_allready_exists'));
        $view->setRef('ContentRight', form_change($action, $id, $URL_PARA, $form_data));
        $action = 'list';
    } elseif ($_result == -2) {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('name_of_primary_key_is_empty'));
        $view->setRef('ContentRight', form_change($action, $id, $URL_PARA, $form_data));
        $action = 'list';
    } else {
        $view->addMessage('STATUS', 'WARNING', $app->static_text('no_data_has_been_updated'));
        $view->setRef('ContentRight', form_change($action, $id, $URL_PARA, $form_data));
        $action = 'list';
    }

    return $action;
}

/* ************************************************************************************* */
function ask4delete($id, $URL_PARA)
{
    global $admin;

    $_html = $admin->admin_get_confirm_table($id, $URL_PARA, 'delete');

    return $_html;
}

/* ************************************************************************************* */
function delete_one($id)
{
    global $admin, $app;

    $_result = $admin->admin_set_delete($id);
    if ($_result) {
        $app->getSql()->query('DELETE FROM ' . DB_TABLENAME_GROUP_ACCESSES . ' WHERE group_id = "' . $id . '";');
        $app->getSql()->query('DELETE FROM ' . DB_TABLENAME_USERS . ' WHERE group_id = "' . $id . '";');
    }

    return $_result;
}

/* ************************************************************************************* */
function form_new($action, $URL_PARA, $error_data = 0)
{
    global $admin;

    $_html = $admin->admin_get_form($action, 0, $error_data, $URL_PARA);

    return $_html;
}

/* ************************************************************************************* */
function form_change($action, $id, $URL_PARA, $error_data = 0)
{
    global $admin;

    $_html = $admin->admin_get_form($action, $id, $error_data, $URL_PARA);

    return $_html;
}

/* ************************************************************************** */
function show_search_form($action, $search_data, $URL_PARA)
{
    global $admin;

    return $admin->admin_get_form($action, 0, $search_data, $URL_PARA);
}

/* ************************************************************************************** */
/* Anfragen auswerten ******************************************************************* */
//URL_PARA neu bestimmen
foreach ($URL_PARA as $key => $val) {
    if (isset($_REQUEST[$key])) {
        $URL_PARA[$key] = $_REQUEST[$key];
    }
}
$action = getParam("action", 'list');
$form_data = getParam("form_data");

switch ($action) {
    case "edit":
        if (!$profile->checkPrivilege('group_admin_edit')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        } else {
            $id = getParam("id");
            if (is_array($form_data)) {
                $view->setRef('ContentRight', change_one($action, $id, $form_data, $URL_PARA));
                $action = 'list';
            } else {
                $view->setRef('ContentRight', form_change($action, $id, $URL_PARA));
            }
        }
        break;
    case "delete":
        if (!$profile->checkPrivilege('group_admin_delete')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        } else {
            if (!isset($_REQUEST["confirm_id"])) {
                $id = getParam("id");
                $view->setRef('ContentRight', ask4delete($id, $URL_PARA));
            } else {
                $confirm_id = getParam("confirm_id");
                if (delete_one($confirm_id)) {
                    $view->addMessage('STATUS', 'NOTICE', $app->static_text('the_entry_was_deleted_successfully'));
                } else {
                    $view->addMessage('STATUS', 'ERROR', $app->static_text('the_entry_could_not_be_deleted'));
                }
                $action = 'list';
            }
        }
        break;
    case "search":
        if (!$profile->checkPrivilege('group_admin_search')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        }
        if (isset($_REQUEST["send_button"])) {
            $URL_PARA["page"] = 1;
            $URL_PARA["alpha"] = '';
        }
        $view->setRef('ContentRight', show_search_form($action, $form_data, $URL_PARA));
        if (is_array($form_data)) {
            $view->append('ContentRight', get_list($URL_PARA, $form_data));
        }
        break;
    case "new":
        if (!$profile->checkPrivilege('group_admin_new')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        }
        if (is_array($form_data)) {
            $action = new_one($action, $form_data, $URL_PARA);
        } else {
            $view->setRef('ContentRight', form_new($action, $URL_PARA));
        }
        break;
}

if ($action == 'list') {
    if ($profile->checkPrivilege('group_admin_view')) {
        $view->setRef('ContentRight', get_list($URL_PARA));
    }
}

$view->setHeadTitle('Groups Admin');
$view->setRef('ContentLeft', $admin->get_navigation());
$view->display('admin/content_left_right.tpl');
