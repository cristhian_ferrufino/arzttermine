<?php

use Arzttermine\Application\Application;
use Arzttermine\User\User;

/********************************************************
 * file: config.inc
 * comment: enthaelt alle globalen einstellungen
 *********************************************************/
$SCRIPT_INDEX = "index.html";
$error_msg = "";

function funcsecbox($data = 0)
{
    global $CONFIG;

    $sql = Application::getInstance()->getSql();

    if (isset($data['id'])) {
        $sql->query('SELECT access_id FROM ' . DB_TABLENAME_GROUP_ACCESSES . ' WHERE group_id=' . $data["id"] . ';');
        while ($sql->next_record()) {
            $tmp_access[$sql->f("access_id")] = 1;
        }
    }
    $_html = '';
    foreach ($CONFIG['GROUP_ACCESS'] as $access_id => $accesses) {
        $_html .= '<table class="widefat" align="center" width="100%">';
        $_html .= '<tr class="headers"><th colspan="2">' . $access_id . '</th></tr>';
        $_class = '';
        foreach ($accesses as $sub_access_id) {
            $_class = ($_class == "alternate") ? "" : "alternate";
            $_html .= '<tr class="' . $_class . '"><td>' . $sub_access_id . '</td><td width="20"><input type="checkbox" value="1" name="form_data[access_id][' . $access_id . '_' . $sub_access_id . ']" ' . (isset($tmp_access[$access_id . '_' . $sub_access_id]) ? " checked" : "") . '></td></tr>';
        }
        $_html .= "</table>\n";
    }

    return $_html;
}

function get_updated_by_name($data = 0)
{
    $_user = new User($data['updated_by']);
    if (!$_user->isValid()) {
        return 'n/a';
    }

    return $_user->getFullName();
}

$MENU_ARRAY = array(
    "id"         => array("sql" => 1, "name" => "ID", "show" => 1, "edit" => 0, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1),
    "name"       => array("sql" => 1, "name" => $app->static_text('name'), "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "updated_at" => array("sql" => 1, "name" => $app->static_text('updated_at'), "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "updated_by" => array("sql" => 1, "name" => $app->static_text('updated_by'), "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_updated_by_name", "para" => 1), "list_function" => array("name" => "get_updated_by_name", "para" => 1)),
    "access"     => array("sql" => 1, "name" => $app->static_text('access'), "show" => 0, "edit" => 1, "new" => 1, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "function", "name" => "funcsecbox", "para" => 1)),
);

$BUTTON = array(
    "new"    => array("func" => "group_admin_new", "alt_text" => $app->static_text('new_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "document_new.png", "action" => "action=new", "th" => 1, "td" => 0),
    "edit"   => array("func" => "group_admin_edit", "alt_text" => $app->static_text('edit_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "document_edit.png", "action" => "action=edit", "th" => 0, "td" => 1),
    "delete" => array("func" => "group_admin_delete", "alt_text" => $app->static_text('delete_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "delete.png", "action" => "action=delete", "th" => 0, "td" => 1),
    "search" => array("func" => "group_admin_search", "alt_text" => $app->static_text('search'), "script" => $_SERVER['PHP_SELF'], "gfx" => "search.png", "action" => "action=search", "th" => 1, "td" => 0),
);

$TABLE_CONFIG = array(
    "db_name"     => "admin",
    "table_name"  => DB_TABLENAME_GROUPS,
    "alpha_index" => "name",
);
$URL_PARA = array(
    "page"         => 1,
    "alpha"        => '',
    "sort"         => 0,
    "direction"    => 'ASC',
    "num_per_page" => 25,
    "type"         => 'num'
);