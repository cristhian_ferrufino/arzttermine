<?php

use Arzttermine\User\User;
use Arzttermine\Widget\Container;

function get_data_created_by($data = 0)
{
    $_user = new User($data['created_by']);
    if (!$_user->isValid()) {
        return 'n/a';
    }

    return $_user->getFullName();
}

function get_data_updated_by($data = 0)
{
    $_user = new User($data['updated_by']);
    if (!$_user->isValid()) {
        return 'n/a';
    }

    return $_user->getFullName();
}

function get_data_status($data = 0)
{
    if (isset($GLOBALS['CONFIG']['WIDGET_STATUS'][$data['status']])) {
        return $GLOBALS['CONFIG']['WIDGET_STATUS'][$data['status']];
    } else {
        return 'n/a';
    }
}

function get_data_slug($data = 0)
{
    $_widget = new Container($data['id']);
    if ($_widget->isValid()) {
        $_link = '<a href="' . $_widget->getDefaultUrl() . '">' . $_widget->getSlug() . '</a>';
    } else {
        $_link = $data['slug'];
    }

    return $_link;
}

function get_data_api_user_ids($data = 0)
{
    $links = '';
    $user_ids = explode(',', $data['api_user_ids']);
    if (!empty($user_ids)) {
        foreach ($user_ids as $user_id) {
            $user = new User($user_id);
            if ($user->isValid()) {
                $links .= (empty($links) ? '' : '<br />') . '<a href="'.$user->getAdminEditUrl().'">' . $user->getName() . '</a>';
            }
        }
    }

    return $links;
}


$MENU_ARRAY = array(
    "id"             => array("sql" => 1, "name" => 'ID', "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "status"         => array("sql" => 1, "name" => 'Status', "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "select", "values" => $GLOBALS['CONFIG']['WIDGET_STATUS']), "list_function" => array("name" => "get_data_status", "para" => 1)),
    "name"           => array("sql" => 1, "name" => 'Name', "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "slug"           => array("sql" => 1, "name" => 'Slug', "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_slug", "para" => 1)),
    "pattern"        => array("sql" => 1, "name" => 'Pattern (ID)', "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "html_head"      => array("sql" => 1, "name" => 'html_head', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => ' style="width:300px;height:60px;"')),
    "html_body"      => array("sql" => 1, "name" => 'html_body', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => ' style="width:300px;height:60px;"')),
    "comment_intern" => array("sql" => 1, "name" => 'Kommentar <span style="color:#ff0000;">(intern)', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => ' style="width:300px;height:60px;"')),
    "views"          => array("sql" => 1, "name" => 'Views', "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "api_user_ids"   => array("sql" => 1, "name" => 'API UserIds', "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_api_user_ids", "para" => 1)),
    "created_at"     => array("sql" => 1, "name" => "Erstellungsdatum", "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "created_by"     => array("sql" => 1, "name" => "Erstellt von", "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_created_by", "para" => 1), "list_function" => array("name" => "get_data_created_by", "para" => 1)),
    "updated_at"     => array("sql" => 1, "name" => "Letztes Update", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "updated_by"     => array("sql" => 1, "name" => "Letzter Bearbeiter", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_updated_by", "para" => 1), "list_function" => array("name" => "get_data_updated_by", "para" => 1)),
);

$BUTTON = array(
    "new"    => array("func" => "widget_admin_new", "alt_text" => $app->static_text('new_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "document_new.png", "action" => "action=new", "th" => 1, "td" => 0),
    "edit"   => array("func" => "widget_admin_edit", "alt_text" => $app->static_text('edit_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "document_edit.png", "action" => "action=edit", "th" => 0, "td" => 1),
    "delete" => array("func" => "widget_admin_delete", "alt_text" => $app->static_text('delete_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "delete.png", "action" => "action=delete", "th" => 0, "td" => 1),
    "search" => array("func" => "widget_admin_search", "alt_text" => $app->static_text('search'), "script" => $_SERVER['PHP_SELF'], "gfx" => "search.png", "action" => "action=search", "th" => 1, "td" => 0),
);
$TABLE_CONFIG = array(
    "table_name"  => DB_TABLENAME_WIDGETS,
    "alpha_index" => "slug",
);
$URL_PARA = array(
    'page'         => 1,
    'alpha'        => '',
    "sort"         => 0,
    "direction"    => 'DESC',
    "num_per_page" => 25,
    "type"         => 'num'
);