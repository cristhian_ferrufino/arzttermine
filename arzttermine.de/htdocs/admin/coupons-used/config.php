<?php

use Arzttermine\Booking\Booking;
use Arzttermine\MedicalSpecialty\MedicalSpecialty;
use Arzttermine\Coupon\Coupon;
use Arzttermine\Coupon\UsedCoupon;

// Display field: booking_id
// --------------------------------------------------------------------------------------------------------------
function get_data_booking($data = 0)
{
    if (isset($data['booking_id'])) {
        $booking = new Booking(intval($data['booking_id']));

        return $booking->getLocation()->getName();
    }

    return 'N/A';
}

// Display field: coupon_id
// --------------------------------------------------------------------------------------------------------------
function get_data_coupon($data = 0)
{
    if (isset($data['coupon_id'])) {
        $coupon = new Coupon(intval($data['coupon_id']));
        $types = $GLOBALS['CONFIG']['REFERRAL_PRICE_TYPES'];

        if (isset($types[$coupon->getOfferType()])) {
            $type = $types[$coupon->getOfferType()];

            return "{$type['company']} ({$type['price']})/[{$coupon->getCode()}]";
        }
    }

    return 'N/A';
}

// Display field: medical_specialty_id
// --------------------------------------------------------------------------------------------------------------
function get_data_medical_specialty($data = 0)
{
    if (isset($data['medical_specialty_id'])) {
        $ms = new MedicalSpecialty(intval($data['medical_specialty_id']));

        return $ms->getName();
    }

    return 'N/A';
}

// Display field: status
// --------------------------------------------------------------------------------------------------------------
function get_data_status($data = 0)
{
    if (isset($data['status']) && UsedCoupon::$STATUS_TEXTS[$data['status']]) {
        return UsedCoupon::$STATUS_TEXTS[$data['status']];
    }

    return 'N/A';
}

// Form field: status
// --------------------------------------------------------------------------------------------------------------
function get_form_status($data = 0)
{
    $html = '<select name="form_data[status]">';

    $selected = $data['status'];

    foreach (UsedCoupon::$STATUS_TEXTS as $code => $label) {
        $html .= '<option value="' . $code . '"' . ($selected == $code ? ' selected' : '') . '>' . $label . '</option>';
    }

    $html .= '</select>';

    return $html;
}

// CONFIG
// ==============================================================================================================
$MENU_ARRAY = array(
    "email"                => array("sql" => 1, "name" => "Email", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "booking_id"           => array("sql" => 1, "name" => "Booking with", "show" => 1, "edit" => 0, "new" => 1, "search" => 1, "sort" => 0, "nowrap" => 1, "list_function" => array("name" => "get_data_booking", "para" => 1)),
    "coupon_id"            => array("sql" => 1, "name" => 'Coupon code', "show" => 1, "edit" => 0, "new" => 1, "search" => 1, "sort" => 0, "nowrap" => 1, "list_function" => array("name" => "get_data_coupon", "para" => 1)),
    "medical_specialty_id" => array("sql" => 1, "name" => "Medical specialty", "show" => 1, "edit" => 0, "new" => 1, "search" => 1, "sort" => 0, "nowrap" => 0, "list_function" => array("name" => "get_data_medical_specialty", "para" => 1)),
    "status"               => array("sql" => 1, "name" => "Status", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_status", "para" => 1), "form_input" => array("type" => "function", "name" => "get_form_status", "para" => 1))
);

$BUTTON = array(
    "edit"   => array("func" => "coupon_admin_edit", "alt_text" => $app->static_text('edit_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "document_edit.png", "action" => "action=edit", "th" => 0, "td" => 1),
    "delete" => array("func" => "coupon_admin_delete", "alt_text" => $app->static_text('delete_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "delete.png", "action" => "action=delete", "th" => 0, "td" => 1),
    "search" => array("func" => "coupon_admin_search", "alt_text" => $app->static_text('search'), "script" => $_SERVER['PHP_SELF'], "gfx" => "search.png", "action" => "action=search", "th" => 1, "td" => 0)
);

$TABLE_CONFIG = array(
    "table_name"  => DB_TABLENAME_USED_COUPONS,
    "alpha_index" => "email",
);

$URL_PARA = array(
    'page'         => 1,
    'alpha'        => '',
    "sort"         => 0,
    "direction"    => 'DESC',
    "num_per_page" => 50,
    "type"         => 'num'
);
