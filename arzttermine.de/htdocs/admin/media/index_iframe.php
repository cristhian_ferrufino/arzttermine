<?php
/**
 * Admin: Media iframe
 *
 * @author Axel Kuzmik <axel@kuzmik.de>
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/../include/bootstrap.php';
require_once "config.php";

/* ************************************************************************************* */

use Arzttermine\Admin\Admin;
use Arzttermine\Application\Application;
use Arzttermine\Media\Media;

$admin = new Admin();
$app = Application::getInstance();
$view = $app->getView();
$profile = $app->getCurrentUser();

$view->disableCaching();

/* ************************************************************************************* */

if (!$profile->checkPrivilege('media_admin')) {
    $view->setRef('ContentRight', $app->static_file('access_denied.html'));
    $view->setRef('ContentLeft', $admin->get_navigation());
    $view->display('admin/content_left_right.tpl');
    exit;
}

/* ************************************************************************************* */
function get_list($URL_PARA, $search = 0)
{
    global $admin;

    $_html = $admin->admin_get_list_gfx_iframe(0, $search, $URL_PARA);

    return $_html;
}

/* ************************************************************************** */
function show_search_form($action, $search_data, $URL_PARA)
{
    global $admin;

    return $admin->admin_get_form($action, 0, $search_data, $URL_PARA);
}

function media_send_to_editor($html)
{
    ?>
    <script type="text/javascript">
        /* <![CDATA[ */
        var win = window.dialogArguments || opener || parent || top;
        win.send_to_editor('<?php echo addslashes($html); ?>');
        /* ]]> */
    </script>
    <?php
    exit;
}

function image_add_caption($html, $id, $caption, $title, $align, $url, $size, $alt = '')
{

    $id = (0 < (int)$id) ? 'attachment_' . $id : '';

    if (!preg_match('/width="([0-9]+)/', $html, $matches)) {
        return $html;
    }

    $width = $matches[1];

    $html = preg_replace('/(class=["\'][^\'"]*)align(none|left|right|center)\s?/', '$1', $html);
    if (empty($align)) {
        $align = 'none';
    }

    $shcode = '[caption id="' . $id . '" align="align' . $align
              . '" width="' . $width . '" caption="' . addslashes($caption) . '"]' . $html . '[/caption]';

    return $shcode;
}

/**
 * calc the img-tag
 *
 * @param $id
 * @param $form
 *
 * @return $html
 */
function image_media_send_to_editor($id, $form)
{
    $_media = new Media($id);
    if (!$_media->valid()) {
        return '';
    }

    $url = isset($form['url']) ? $form['url'] : '';
    $align = !empty($form['align']) ? $form['align'] : 'none';
    $size = !empty($form['image-size']) ? $form['image-size'] : 'full';
    $alt = !empty($form['image_alt']) ? $form['image_alt'] : '';
    $title = $alt;

    $hwstring = '';
    if (isset($form['width'])) {
        $hwstring .= 'width="' . intval($form['width']) . '" ';
    }
    if (isset($form['height'])) {
        $hwstring .= 'height="' . intval($form['height']) . '" ';
    }

    $class = 'align' . esc_attr($align) . ' size-' . esc_attr($size) . ' wp-image-' . $id;
    $html = '<img src="' . esc_attr($_media->getUrl()) . '" alt="' . esc_attr($alt) . '" title="' . esc_attr($title) . '" ' . $hwstring . 'class="' . $class . '" />';

    /*
     // for galleries, etc
        $rel = ( $url == get_attachment_link($id) );
        $rel = $rel ? ' rel="attachment wp-att-' . esc_attr($form_id).'"' : '';
    */
    if ($url) {
        $html = '<a href="' . esc_attr($url) . "\"{$form['rel']}>{$html}</a>";
    }

    #$html = image_add_caption($html, $id, $caption, $title, $align, $url, $size, $alt);

    return $html;
}

/*
 * *************************************************************************
 */
foreach ($URL_PARA as $key => $val) {
    if (isset($_REQUEST[$key])) {
        $URL_PARA[$key] = $_REQUEST[$key];
    }
}
$action = getParam("action", "list");
$form_data = getParam("form_data");

switch ($action) {
    case 'image_media_send_to_editor':
        $_html = image_media_send_to_editor(getParam('id'), getParam('form'));
        // send the html via javascript and exit();
        media_send_to_editor($_html);
        break;

    case "search":
        if (!$profile->checkPrivilege('media_admin_search')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        }
        if (isset($_REQUEST["send_button"])) {
            $URL_PARA["page"] = 1;
        }
        
        if (is_array($form_data)) {
            $view->setRef('ContentRight', show_search_form($action, $form_data, $URL_PARA) . get_list($URL_PARA, $form_data));
        } else {
            $view->setRef('ContentRight', show_search_form($action, $form_data, $URL_PARA));
        }
        break;
}

if ($action == "list") {
    if ($profile->checkPrivilege('media_admin_view')) {
        $view->ContentMain = get_list($URL_PARA);
    }
}

$view->setHeadTitle('Media Admin');
$view->display('admin/iframe_full.tpl');