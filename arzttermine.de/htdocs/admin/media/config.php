<?php

use Arzttermine\User\User;

function list_category($sql_array)
{
    $tmp_index = $sql_array["category"];

    return ($GLOBALS['CONFIG']['MEDIA_CATEGORIES'][$tmp_index]["name"]);
}

function form_category()
{
    $tmp_array = array();
    foreach ($GLOBALS['CONFIG']['MEDIA_CATEGORIES'] as $key => $category) {
        $tmp_array[$key] = $GLOBALS['CONFIG']['MEDIA_CATEGORIES'][$key]["name"];
    }

    return $tmp_array;
}

function list_size($sql_array)
{
    $size = $sql_array["filesize"];
    $sizes = array(' B', ' KB', ' MB', ' GB', ' TB');
    $ext = $sizes[0];
    for ($i = 1; (($i < count($sizes)) && ($size >= 1024)); $i++) {
        $size = $size / 1024;
        $ext = $sizes[$i];
    }

    return round($size, 2) . $ext;
}

function form_updated_by($sql_array)
{
    $_user = new User($sql_array["updated_by"]);
    if ($_user->isValid()) {
        $_username = $_user->getFullName();
    } else {
        $_username = 'n/a';
    }

    return $_username;
}

function form_created_by($sql_array)
{
    $_user = new User($sql_array["created_by"]);
    if ($_user->isValid()) {
        $_username = $_user->getFullName();
    } else {
        $_username = 'n/a';
    }

    return $_username;
}

function list_image($sql_array)
{
    /* get the template to display the image */
    $filename = explode(".", $sql_array["filename"]);
    $ext = array_pop($filename);
    $tmp_webfilename = $GLOBALS['CONFIG']['MEDIA_CATEGORIES'][$sql_array["category"]]["url_path"] . $sql_array["filename"];

    switch ($ext) {
        case "swf":
            $HTML_IMAGE
                = "
<SCRIPT LANGUAGE=\"JavaScript\">
<!--
  if (document.all) { // internet explorer
    if (navigator.appVersion.indexOf(\"Mac\") == -1 && navigator.appVersion.indexOf(\"3.1\") == -1)
    { // IE auf PC und nicht unter Win3.1
      document.write('<OBJECT CLASSID=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" codebase=\"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=4,0,2,0\" WIDTH=\"" . $sql_array["width"] . "\" HEIGHT=\"" . $sql_array["height"] . "\" NAME=\"sw\" ID=\"sw\">');
      document.write('  <PARAM NAME=\"Movie\" VALUE=\"$tmp_webfilename\">');
      document.write('  <PARAM NAME=\"quality\" VALUE=\"high\">');
      if (navigator.mimeTypes && navigator.mimeTypes[\"application/x-shockwave-flash\"] && navigator.mimeTypes[\"application/x-shockwave-flash\"].enabledPlugin && navigator.plugins && navigator.plugins[\"Shockwave Flash\"])
      {}
      else { // Flash 2 oder kein Flash
        document.write('Sorry, you need the Flash-Plugin to see the Flashmovie!');
      }
      document.write('</OBJECT>');
    } else { // IE auf Mac oder Win3.1
      document.write('You need the Flash-Plugin to see the Flashmovie!');
    }
  } else {
    if (navigator.appName == \"Netscape\") { // Netscape
      if (navigator.mimeTypes && navigator.mimeTypes[\"application/x-shockwave-flash\"] && navigator.mimeTypes[\"application/x-shockwave-flash\"].enabledPlugin && navigator.plugins && navigator.plugins[\"Shockwave Flash\"])
      { // Flash 3 oder hoeher
        document.write('<EMBED SRC=\"$tmp_webfilename\" WIDTH=\"" . $sql_array["width"] . "\" HEIGHT=\"" . $sql_array['height'] . "\" QUALITY=\"high\" PLUGINSPAGE=\"http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash\" TYPE=\"application/x-shockwave-flash\"></EMBED>')
      } else { // Flash 2 oder kein Flash
        document.write('You need the Flash-Plugin to see the Flashmovie!');
      }
    } else { // andere browser
      document.write('You need the Flash-Plugin to see the Flashmovie!');
    }
  }
// -->
</SCRIPT><noscript>You need the Flash-Plugin to see the Flashmovie!</noscript>
";
            break;
        case "mp3":
            $HTML_IMAGE = '<img src="' . getStaticUrl('adm/media_mimetype_mp3.png') . '" title="' . $sql_array["filename"] . '" alt="' . $sql_array["filename"] . '"></a>';
            break;
        case "mov":
        case "avi":
            $HTML_IMAGE = '<img src="' . getStaticUrl('adm/media_mimetype_avi.png') . '" title="' . $sql_array["filename"] . '" alt="' . $sql_array["filename"] . '"></a>';
            break;
        case "css":
            $HTML_IMAGE = '<img src="' . getStaticUrl('adm/media_mimetype_css.png') . '" title="' . $sql_array["filename"] . '" alt="' . $sql_array["filename"] . '"></a>';
            break;
        case "js":
            $HTML_IMAGE = '<img src="' . getStaticUrl('adm/media_mimetype_js.png') . '" title="' . $sql_array["filename"] . '" alt="' . $sql_array["filename"] . '"></a>';
            break;
        case "pdf":
            $HTML_IMAGE = '<img src="' . getStaticUrl('adm/media_mimetype_pdf.png') . '" title="' . $sql_array["filename"] . '" alt="' . $sql_array["filename"] . '"></a>';
            break;
        case "doc":
        case "docx":
            $HTML_IMAGE = '<img src="' . getStaticUrl('adm/media_mimetype_doc.png') . '" title="' . $sql_array["filename"] . '" alt="' . $sql_array["filename"] . '"></a>';
            break;
        case "ppt":
            $HTML_IMAGE = '<img src="' . getStaticUrl('adm/media_mimetype_ppt.png') . '" title="' . $sql_array["filename"] . '" alt="' . $sql_array["filename"] . '"></a>';
            break;
        case "xls":
        case "xlsx":
            $HTML_IMAGE = '<img src="' . getStaticUrl('adm/media_mimetype_xls.png') . '" title="' . $sql_array["filename"] . '" alt="' . $sql_array["filename"] . '"></a>';
            break;
        case "ico":
        case "gif":
        case "jpg":
        case "jpeg":
        case "png":
        default:
            $HTML_IMAGE = '<img src="' . $tmp_webfilename . '" title="' . $sql_array["filename"] . '" alt="' . $sql_array["filename"] . '"></a>';
            break;
    }

    return $HTML_IMAGE;
}

/* DESCRIPTION: each row equals one row in the db
		"sql": [0]|1 = the data of this row comes [not] from the db
		"name": the description of this row
		"show": [0]|1 = [don't] show this entry in the navigation
		"edit": [0]|1 = [don't] show this entry in the property dialog
		"new": [0]|1 = [don't] show this entry in the new entry dialog
		"sort": [0]|1 = [don't] allow to sort by this row
		"nowrap": [0]|1 = [doesn't] includes a "nowrap" in the navigation for this row
		"list_function": here you can define a new function to display the data in the main navigation dialog
			- "name": the id of the function
			- "para": [0]|1 = serves the data of this db-row to the function
		"form_input": here you can define a new function to display the data in the main property dialog
			- "type": [text"|"textarea"|"radio"|"checkbox"|"select"|"button"|"file"|"password"|"function"|"none"]
			- "values": name of the array or function which serves the data
) */
$MENU_ARRAY = array(
    "id"                           => array("sql" => 1, "name" => "ID", "show" => 0, "show_iframe" => 0, "edit" => 0, "new" => 0, "search" => 0, "sort" => 0, "nowrap" => 0),
    "filename"                     => array("sql" => 1, "name" => "Filename", "show" => 1, "show_iframe" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1),
    "category"                     => array("sql" => 1, "name" => "Verzeichnis", "show" => 1, "show_iframe" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "list_category", "para" => 1), "form_input" => array("type" => "select", "values" => form_category())),
    "filesize"                     => array("sql" => 1, "name" => "Dateigröße", "show" => 1, "show_iframe" => 1, "edit" => 0, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "list_size", "para" => 1)),
    "width"                        => array("sql" => 1, "name" => "Breite", "show" => 1, "show_iframe" => 1, "edit" => 0, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1),
    "height"                       => array("sql" => 1, "name" => "Höhe", "show" => 1, "show_iframe" => 1, "edit" => 0, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1),
    "label"                        => array("sql" => 1, "name" => "Caption", "show" => 1, "show_iframe" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "description"                  => array("sql" => 1, "name" => "Description", "show" => 1, "show_iframe" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "tags"                         => array("sql" => 1, "name" => "Tags", "show" => 1, "show_iframe" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "mimetype"                     => array("sql" => 1, "name" => "MimeType", "show" => 1, "show_iframe" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "created_at"                   => array("sql" => 1, "name" => "Erstellungsdatum", "show" => 0, "show_iframe" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "created_by"                   => array("sql" => 1, "name" => "Ersteller", "show" => 0, "show_iframe" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "form_created_by", "para" => 1)),
    "updated_at"                   => array("sql" => 1, "name" => "Letztes Update", "show" => 1, "show_iframe" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "updated_by"                   => array("sql" => 1, "name" => "Letzter Bearbeiter", "show" => 1, "show_iframe" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "form_updated_by", "para" => 1), "list_function" => array("name" => "form_updated_by", "para" => 1)),
    "file_upload"                  => array("sql" => 0, "name" => "Dateiupload", "show" => 0, "show_iframe" => 0, "edit" => 1, "new" => 1, "search" => 0, "sort" => 0, "nowrap" => 0, "form_input" => array("type" => "file")),
    "tip_upload_max_filesize_text" => array("sql" => 1, "name" => '<p>Medien können max. ' . ini_get("upload_max_filesize") . ' groß sein.</p>', "show" => 0, "show_iframe" => 0, "edit" => 1, "new" => 1, "search" => 0, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "none")),
);
/* DESCRIPTION: use this array to include function-buttons in the navigation
		"func": security-id,
		"gfx": the image-name of the button,
		"alt_text": the alt-text of the button,
		"script": address to the script,
		"action": parameter,
		"th": display this button in the first row as a main-function,
		"td": display this button in each row
) */
$BUTTON = array(
    "new"    => array("func" => "media_admin_new", "gfx" => "document_new.png", "alt_text" => "Neuer Eintrag", "script" => $_SERVER["PHP_SELF"], "action" => "action=new", "th" => 1, "td" => 0),
    "edit"   => array("func" => "media_admin_edit", "gfx" => "document_edit.png", "alt_text" => "Bearbeiten", "script" => $_SERVER["PHP_SELF"], "action" => "action=edit", "th" => 0, "td" => 1),
    "delete" => array("func" => "media_admin_delete", "gfx" => "delete.png", "alt_text" => "Löschen", "script" => $_SERVER["PHP_SELF"], "action" => "action=delete", "th" => 0, "td" => 1),
    "search" => array("func" => "media_admin_search", "gfx" => "search.png", "alt_text" => "Suchen", "script" => $_SERVER["PHP_SELF"], "action" => "action=search", "th" => 1, "td" => 0),
);
/* DESCRIPTION: main configuration settings
		"db_name": the databasename,
		"table_name": the tablename,
		"alpha_index": if type==alpha than sort by this,
) */
$TABLE_CONFIG = array(
    "db_name"     => "admin",
    "table_name"  => DB_TABLENAME_MEDIAS,
    "alpha_index" => "filename",
);
/* DESCRIPTION: this array will be used by default for navigation
		"start": navigation startvalue,
		"substart": if type==alpha, this is the substart-value,
		"sort": sort by which column,
		"direction": sortdirection,
		"num_per_page": number of rows to display,
		"type": visual display type: (num|num_select|alpha),
) */
$URL_PARA = array(
    "page"         => 1,
    "alpha"        => '',
    "sort"         => 'filename',
    "direction"    => 0,
    "num_per_page" => 25,
    "type"         => 'alpha',
);