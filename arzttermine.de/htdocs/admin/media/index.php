<?php
/**
 * Admin: Media
 *
 * @author Axel Kuzmik <axel@kuzmik.de>
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/../include/bootstrap.php';
require_once "config.php";

/* ************************************************************************************* */

use Arzttermine\Admin\Admin;
use Arzttermine\Application\Application;

$admin = new Admin();
$app = Application::getInstance();
$view = $app->getView();
$profile = $app->getCurrentUser();

$view->disableCaching();

/* ************************************************************************************* */

if (!$profile->checkPrivilege('media_admin')) {
    $view->setRef('ContentRight', $app->static_file('access_denied.html'));
    $view->setRef('ContentLeft', $admin->get_navigation());
    $view->display('admin/content_left_right.tpl');
    exit;
}

/* ************************************************************************************* */
function get_list($URL_PARA, $search = 0)
{
    global $admin;

    $_html = $admin->admin_get_list_gfx(0, $search, $URL_PARA);

    return $_html;
}

/* ************************************************************************************* */
function new_one($action, $form_data, $URL_PARA)
{
    global $app, $profile, $admin, $view, $TABLE_CONFIG;

    /* the name of the uploaded file */
    $form_data["filename"] = $_FILES["form_data"]["name"]["file_upload"];
    if ($form_data["filename"] == "") {
        $view->addMessage('STATUS', 'WARNING', $app->static_text('need_uploadfile'));
        $view->setRef('ContentRight', form_new($action, $URL_PARA, $form_data));

        return false;
    }
    /* check if the image has the correct fileextension and characters */
    if (!preg_match('/^[a-zA-Z\d_-]+\.(ico|gif|jpg|jpeg|png|swf|mp3|mov|avi|mpg|css|js|pdf|xls|xlsx|doc|docx|ppt)$/i', $form_data["filename"])) {
        $view->addMessage('STATUS', 'WARNING', $app->static_text('wrong_extension'));
        $view->setRef('ContentRight', form_new($action, $URL_PARA, $form_data));

        return false;
    }

    /* check if there is a file with that category and the same name */
    $app->getSql()->query('SELECT * FROM ' . $TABLE_CONFIG["table_name"] . ' WHERE category=' . $form_data["category"] . ' AND filename="' . $form_data["filename"] . '";');
    if ($app->getSql()->num_rows() > 0) {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('there_is_allready_a_file_in_this_category'));
        $view->setRef('ContentRight', form_new($action, $URL_PARA, $form_data));

        return false;
    }
    if (!admin_save_file_fs($_FILES["form_data"]["tmp_name"]["file_upload"], $GLOBALS['CONFIG']['MEDIA_CATEGORIES'][$form_data["category"]]["file_path"] . $form_data["filename"])) {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('could_not_save_the_media'));
        $view->setRef('ContentRight', form_new($action, $URL_PARA, $form_data));

        return false;
    }
    $form_data["tags"] = sanitize_tags($form_data["tags"]);
    $form_data["created_by"] = $profile->getId();
    $form_data["created_at"] = $app->now();
    $form_data["updated_by"] = $profile->getId();
    $form_data["updated_at"] = $app->now();
    $form_data["filesize"] = filesize($GLOBALS['CONFIG']['MEDIA_CATEGORIES'][$form_data["category"]]["file_path"] . $form_data["filename"]);

    /* try to get the size (supported by getimagesize()) */
    $img_array = @getimagesize($GLOBALS['CONFIG']['MEDIA_CATEGORIES'][$form_data["category"]]["file_path"] . $form_data["filename"]);
    if (is_array($img_array)) {
        $form_data["width"] = $img_array[0];
        $form_data["height"] = $img_array[1];
        $form_data["mimetype"] = $img_array['mime'];
    } else {
        $form_data["width"] = 0;
        $form_data["height"] = 0;
        $form_data["mimetype"] = '';
    }

    $_id = $admin->admin_set_new($form_data);
    if ($_id > 0) {

        calc_url_para($URL_PARA, $form_data);

        $view->addMessage('STATUS', 'NOTICE', $app->static_text('action_was_successfully'));
        $action = 'list';
    } elseif ($_id == -1) {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('the_entry_allready_exists'));
        $view->setRef('ContentRight', form_new($action, $URL_PARA, $form_data));
        $action = 'list';
    } elseif ($_id == -2) {
        /* delete the file from the fs */
        admin_delete_file_fs($GLOBALS['CONFIG']['MEDIA_CATEGORIES'][$form_data["category"]]["file_path"] . $form_data["filename"]);

        $view->addMessage('STATUS', 'ERROR', $app->static_text('name_of_primary_key_is_empty'));
        $view->setRef('ContentRight', form_new($action, $URL_PARA, $form_data));
        $action = 'list';
    } else {
        /* delete the file from the fs */
        admin_delete_file_fs($GLOBALS['CONFIG']['MEDIA_CATEGORIES'][$form_data["category"]]["file_path"] . $form_data["filename"]);

        $view->addMessage('STATUS', 'ERROR', $app->static_text('the_entry_could_not_be_created'));
        $view->setRef('ContentRight', form_new($action, $URL_PARA, $form_data));
        $action = 'list';
    }

    return $action;
}

/* ************************************************************************************* */
function change_one($action, $id, $form_data, $URL_PARA)
{
    global $profile, $admin, $view, $TABLE_CONFIG, $app;
    $sql = $app->getSql();

    /* the name of the uploaded file */
    $form_data["upload_filename"] = $_FILES["form_data"]["name"]["file_upload"];

    /* check if there is a file with that category and the same name */
    /* if there is a new file than take that filename */
    if ($form_data["upload_filename"] != "") {
        $tmp_filename_check = $form_data["upload_filename"];
    } else {
        $tmp_filename_check = $form_data["filename"];
    }

    /* check if the image has the correct fileextension and characters */
    if (!preg_match('/^[a-zA-Z\d_-]+\.(ico|gif|jpg|jpeg|png|swf|mp3|mov|avi|mpg|css|js|pdf|xls|xlsx|doc|docx|ppt)$/i', $tmp_filename_check)) {
        $view->addMessage('STATUS', 'WARNING', $app->static_text('wrong_extension'));
        $view->setRef('ContentRight', form_change($action, $id, $URL_PARA, $form_data));

        return false;
    }

    $sql->query('SELECT * FROM ' . $TABLE_CONFIG["table_name"] . ' WHERE category=' . $form_data["category"] . ' AND filename="' . $tmp_filename_check . '" AND id!=' . $id . ';');
    if ($sql->num_rows() > 0) {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('there_is_allready_a_file_in_this_category'));
        $view->setRef('ContentRight', form_change($action, $id, $URL_PARA, $form_data));

        return false;
    }
    /* check if category, filename have changed or if there was an upload */
    $sql->query('SELECT * FROM ' . $TABLE_CONFIG["table_name"] . ' WHERE id=' . $id . ';');
    if ($sql->num_rows() == 0) {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('could_not_find_media'));

        return false;
    }
    $sql->next_record();
    $old_category = $sql->f("category");
    $old_filename = $sql->f("filename");
    /* if there is an upload than store the file ************** */
    if ($form_data["upload_filename"] != "") {
        /* this is the new filename */
        $form_data["filename"] = $form_data["upload_filename"];
        if (!admin_save_file_fs($_FILES["form_data"]["tmp_name"]["file_upload"], $GLOBALS['CONFIG']['MEDIA_CATEGORIES'][$form_data["category"]]["file_path"] . $form_data["filename"])) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('could_not_save_the_media'));
            $view->setRef('ContentRight', form_change($action, $id, $URL_PARA, $form_data));

            return false;
        }
        /* if the old file has an another filename or resists in another category than delete it. */
        /* otherwise it was overwritten and we are not alowed to delete the file at this point */
        if (($old_category != $form_data["category"]) || ($old_filename != $form_data["filename"])) {
            /* delete the file from the fs */
            admin_delete_file_fs($GLOBALS['CONFIG']['MEDIA_CATEGORIES'][$old_category]["file_path"] . $old_filename);
        }
        /* the category or the filename have changed ************** */
    } elseif (($old_category != $form_data["category"]) || ($old_filename != $form_data["filename"])) {
        /* save the old file under the new address and name */
        if (!admin_save_file_fs($GLOBALS['CONFIG']['MEDIA_CATEGORIES'][$old_category]["file_path"] . $old_filename, $GLOBALS['CONFIG']['MEDIA_CATEGORIES'][$form_data["category"]]["file_path"] . $form_data["filename"])) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('could_not_save_the_media'));
        } else {
            /* delete the old file from the fs */
            admin_delete_file_fs($GLOBALS['CONFIG']['MEDIA_CATEGORIES'][$old_category]["file_path"] . $old_filename);
        }
    }

    $form_data["tags"] = sanitize_tags($form_data["tags"]);
    $form_data["updated_by"] = $profile->getId();
    $form_data["updated_at"] = $app->now();
    $form_data["filesize"] = filesize($GLOBALS['CONFIG']['MEDIA_CATEGORIES'][$form_data["category"]]["file_path"] . $form_data["filename"]);

    /* try to get the size (supported by getimagesize()) */
    $img_array = @getimagesize($GLOBALS['CONFIG']['MEDIA_CATEGORIES'][$form_data["category"]]["file_path"] . $form_data["filename"]);
    if (is_array($img_array)) {
        $form_data["width"] = $img_array[0];
        $form_data["height"] = $img_array[1];
        $form_data["mimetype"] = $img_array['mime'];
    } else {
        $form_data["width"] = 0;
        $form_data["height"] = 0;
        $form_data["mimetype"] = '';
    }

    $_result = $admin->admin_set_update($id, $form_data);
    if ($_result == 1) {

        calc_url_para($URL_PARA, $form_data);

        $view->addMessage('STATUS', 'NOTICE', $app->static_text('action_was_successfully'));
        $action = 'list';
    } elseif ($_result == -1) {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('the_entry_allready_exists'));
        $view->setRef('ContentRight', form_change($action, $id, $URL_PARA, $form_data));
        $action = 'list';
    } elseif ($_result == -2) {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('name_of_primary_key_is_empty'));
        $view->setRef('ContentRight', form_change($action, $id, $URL_PARA, $form_data));
        $action = 'list';
    } else {
        $view->addMessage('STATUS', 'WARNING', $app->static_text('no_data_has_been_updated'));
        $view->setRef('ContentRight', form_change($action, $id, $URL_PARA, $form_data));
        $action = 'list';
    }

    return $action;
}

/* ************************************************************************************* */
function delete_one($id)
{
    global $admin, $view, $TABLE_CONFIG, $app;
    $sql = $app->getSql();

    /* get the db-date before we delete */
    $sql->query('SELECT category,filename FROM ' . $TABLE_CONFIG["table_name"] . ' WHERE id=' . $id . ';');
    if ($sql->num_rows() == 0) {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('could_not_find_media'));

        return false;
    }
    $sql->next_record();
    $tmp_category = $sql->f("category");
    $tmp_filename = $sql->f("filename");

    /* delete the file in the db */
    $_result = $admin->admin_set_delete($id);
    if ($_result) {
        /* delete the file from the fs */
        admin_delete_file_fs($GLOBALS['CONFIG']['MEDIA_CATEGORIES'][$tmp_category]["file_path"] . $tmp_filename);
    }

    return $_result;
}

/* ************************************************************************************* */
function ask4delete($id, $URL_PARA)
{
    global $admin;

    $_html = $admin->admin_get_confirm_table($id, $URL_PARA, 'delete');

    return $_html;
}

/* ************************************************************************************* */
function form_new($action, $URL_PARA, $error_data = 0)
{
    global $admin;

    $_html = $admin->admin_get_form($action, 0, $error_data, $URL_PARA);

    return $_html;
}

/* ************************************************************************************* */
function form_change($action, $id, $URL_PARA, $error_data = 0)
{
    global $admin;

    $_html = $admin->admin_get_form($action, $id, $error_data, $URL_PARA);

    return $_html;
}

/* ************************************************************************** */
function show_search_form($action, $search_data, $URL_PARA)
{
    global $admin;

    return $admin->admin_get_form($action, 0, $search_data, $URL_PARA);
}

/* ************************************************************************************** */
/* OUT: ok: TRUE/error: FALSE */
function admin_save_file_fs($source, $destination)
{
    $tmp_result = copy($source, $destination);
    @chmod($destination, 0644);

    return $tmp_result;
}

/* ************************************************************************************** */
function admin_delete_file_fs($filename)
{
    return unlink($filename);
}

/* ************************************************************************************** */
/* calc the parameters to show the last edited or new entry */
function calc_url_para($_url_para, $form_data)
{
    global $TABLE_CONFIG, $app;
    $sql = $app->getSql();

    if (!is_array($_url_para)) {
        return false;
    }

    /* try to get the id of the file */
    $sql->query('SELECT id FROM ' . $TABLE_CONFIG["table_name"] . ' WHERE category=' . $form_data["category"] . ' AND filename="' . $form_data["filename"] . '";');
    if ($sql->num_rows() == 0) {
        return false;
    }
    $sql->next_record();
    $id = $sql->f("id");

    /* alphanumeric navigation */
    if ($_url_para["type"] == "alpha") {
        if (isset($form_data["filename"])) {
            $_url_para["alpha"] = substr($form_data["filename"], 0, 1);

            $sql_txt = 'SELECT id FROM ' . $TABLE_CONFIG["table_name"] . ' WHERE ' . $TABLE_CONFIG["alpha_index"] . ' LIKE "' . $_url_para["alpha"] . '%" ORDER BY ' . $_url_para["sort"];
            $sql->query($sql_txt);
            $tmp_counter = 0;
            while ($sql->next_record()) {
                if (isset($sql->Record[0])) {
                    $tmp_id = $sql->Record[0];
                    $tmp_counter++;
                    if ($tmp_id == $id) {
                        break;
                    }
                }
            }
            $_url_para["page"] = ceil($tmp_counter / $_url_para["num_per_page"]);
            $GLOBALS['URL_PARA']["page"] = $_url_para["page"];
            $GLOBALS['URL_PARA']["alpha"] = $_url_para["alpha"];
        }
    } else {
        /* numeric navigation */
        $sql_txt = 'SELECT id FROM ' . $TABLE_CONFIG["table_name"] . ' ORDER BY ' . $_url_para["sort"];
        $sql->query($sql_txt);
        $tmp_counter = 0;
        while ($sql->next_record()) {
            if (isset($sql->Record[0])) {
                $tmp_id = $sql->Record[0];
                $tmp_counter++;
                if ($tmp_id == $id) {
                    break;
                }
            }
        }
        $_url_para["page"] = ceil($tmp_counter / $_url_para["num_per_page"]);
        $GLOBALS['URL_PARA']["page"] = $_url_para["page"];
    }

    return true;
}

/* ************************************************************************************** */
/* Anfragen auswerten ******************************************************************* */
foreach ($URL_PARA as $key => $val) {
    if (isset($_REQUEST[$key])) {
        $URL_PARA[$key] = $_REQUEST[$key];
    }
}
$action = getParam("action", 'list');
$form_data = getParam("form_data");

switch ($action) {
    case "edit":
        if (!$profile->checkPrivilege('media_admin_edit')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        } else {
            $id = getParam("id");
            if (is_array($form_data)) {
                $view->setRef('ContentRight', change_one($action, $id, $form_data, $URL_PARA));
                $action = 'list';
            } else {
                $view->setRef('ContentRight', form_change($action, $id, $URL_PARA));
            }
        }
        break;
    case "delete":
        if (!$profile->checkPrivilege('media_admin_delete')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        } else {
            if (!isset($_REQUEST["confirm_id"])) {
                $id = getParam("id");
                $view->setRef('ContentRight', ask4delete($id, $URL_PARA));
            } else {
                $confirm_id = getParam("confirm_id");
                if (delete_one($confirm_id)) {
                    $view->addMessage('STATUS', 'NOTICE', $app->static_text('the_entry_was_deleted_successfully'));
                } else {
                    $view->addMessage('STATUS', 'ERROR', $app->static_text('the_entry_could_not_be_deleted'));
                }
                $action = 'list';
            }
        }
        break;
    case "search":
        if (!$profile->checkPrivilege('media_admin_search')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        }
        if (isset($_REQUEST["send_button"])) {
            $URL_PARA["page"] = 1;
            $URL_PARA["alpha"] = '';
        }
        $view->setRef('ContentRight', show_search_form($action, $form_data, $URL_PARA));
        if (is_array($form_data)) {
            $view->append('ContentRight', get_list($URL_PARA, $form_data));
        }
        break;
    case "new":
        if (!$profile->checkPrivilege('media_admin_new')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        }
        if (is_array($form_data)) {
            $action = new_one($action, $form_data, $URL_PARA);
        } else {
            $view->setRef('ContentRight', form_new($action, $URL_PARA));
        }
        break;
}

if ($action == 'list') {
    if ($profile->checkPrivilege('media_admin_view')) {
        $view->setRef('ContentRight', get_list($URL_PARA));
    }
}

$view->setHeadTitle('Media Admin');
$view->setRef('ContentLeft', $admin->get_navigation());
$view->display('admin/content_left_right.tpl');
