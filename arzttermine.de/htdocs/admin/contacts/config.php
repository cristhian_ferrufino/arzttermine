<?php

use Arzttermine\User\User;

/********************************************************
 * file: config.inc
 * comment: enthaelt alle globalen einstellungen
 *********************************************************/
$SCRIPT_INDEX = "index.html";
$error_msg = "";

function get_created_by_name($data = 0)
{
    $_user = new User($data['created_by']);
    if (!$_user->isValid()) {
        return 'n/a';
    }

    return $_user->getFullName();
}

$MENU_ARRAY = array(
    "id"         => array("sql" => 1, "name" => "ID", "show" => 1, "edit" => 0, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "name"       => array("sql" => 1, "name" => $app->static_text('name'), "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0),
    "email"      => array("sql" => 1, "name" => $app->static_text('email'), "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "subject"    => array("sql" => 1, "name" => $app->static_text('subject'), "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0),
    "message"    => array("sql" => 1, "name" => $app->static_text('message'), "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "optional" => ' cols="60" rows="10" style="width:300px;"')),
    "created_at" => array("sql" => 1, "name" => $app->static_text('created_at'), "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "none")),
    "created_by" => array("sql" => 1, "name" => $app->static_text('created_by'), "show" => 0, "edit" => 0, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_created_by_name", "para" => 1), "list_function" => array("name" => "get_created_by_name", "para" => 1)),
);
$BUTTON = array(
#	"new"			=> array("func"=>"contact_admin_new","alt_text"=>$app->static_text('new_item'),"script"=>$_SERVER['PHP_SELF'],"gfx"=>"document_new.png","action"=>"action=new","th"=>1,"td"=>0),
"edit"   => array("func" => "contact_admin_edit", "alt_text" => $app->static_text('edit_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "document_edit.png", "action" => "action=edit", "th" => 0, "td" => 1),
"delete" => array("func" => "contact_admin_delete", "alt_text" => $app->static_text('delete_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "delete.png", "action" => "action=delete", "th" => 0, "td" => 1),
"search" => array("func" => "contact_admin_search", "alt_text" => $app->static_text('search'), "script" => $_SERVER['PHP_SELF'], "gfx" => "search.png", "action" => "action=search", "th" => 1, "td" => 0),
);
$TABLE_CONFIG = array(
    "table_name"  => DB_TABLENAME_CONTACTS,
    "alpha_index" => "name",
);
$URL_PARA = array(
    'page'         => 1,
    'alpha'        => '',
    "sort"         => 0,
    "direction"    => 'DESC',
    "num_per_page" => 50,
    "type"         => 'num'
);