<?php

use Arzttermine\Booking\Booking;
use Arzttermine\Booking\Offer;
use Arzttermine\Location\Location;
use Arzttermine\User\User;

function get_data_created_by($data = 0)
{
    $_user = new User($data['created_by']);
    if (!$_user->isValid()) {
        return 'n/a';
    }

    return $_user->getFullName();
}

function get_data_created_at($data = 0)
{
    return mysql2date('d.m.Y H:i:s', $data['created_at'], false);
}

function get_data_updated_by($data = 0)
{
    $_user = new User($data['updated_by']);
    if (!$_user->isValid()) {
        return 'n/a';
    }

    return $_user->getFullName();
}

function get_data_status($data = 0)
{
    $status_texts = Offer::getStatusTextArray();
    if (isset($status_texts[$data['status']])) {
        return $status_texts[$data['status']];
    } else {
        return 'n/a';
    }
}

function get_data_user_id($data = 0)
{
    $userId = $data['user_id'];
    $_user = new User($userId);
    if (!$_user->isValid()) {
        return 'n/a';
    }
    if ($_user->hasProfile()) {
        return '[' . $_user->getId() . '] <a href="' . $_user->getUrl() . '" target="_blank">' . $_user->getName() . '</a>';
    } else {
        return '[' . $_user->getId() . '] ' . $_user->getName();
    }
}

function get_data_booking($data = 0)
{
    $bookingId = $data['booking_id'];
    $booking = new Booking($bookingId);
    if (!$booking->isValid()) {
        return 'unvalid BookingID';
    }

    return '<a href="/terminanfrage/' . $booking->getSlug() . '" target="_blank">Offers</a> | <a href="' . $booking->getAdminEditUrl() . '" target="_blank">ID: ' . $booking->getId() . '</a> ' . $booking->getName() . ' / ' . $booking->getEmail();
}

function get_data_location_id($data = 0)
{
    $locationId = $data['location_id'];
    $location = new Location($locationId);
    if (!$location->isValid()) {
        return '';
    }
    $length = 30;
    $name = mb_substr($location->getName(), 0, $length, $GLOBALS['CONFIG']['SYSTEM_CHARSET']);
    if (mb_strlen($location->getName(), $GLOBALS['CONFIG']['SYSTEM_CHARSET']) > $length) {
        $name .= '...';
    }

    return '[' . $location->getId() . '] <a href="' . $location->getUrl() . '" target="_blank">' . $name . '</a>';
}

function get_data_appointment_start_at($data = 0)
{
    $text = $data['appointment_start_at'];
    if ($data['appointment_start_at'] == '1970-01-01 00:00:00') {
        $text = 'Terminanfrage';
    }

    return $text;
}

function get_data_expires_at($data = 0)
{
    $now = new DateTime("now");
    $expiresAt = new DateTime($data['expires_at']);
    if ($now > $expiresAt) {
        $color = '#ff0000';
    } else {
        $color = '#5AA419';
    }

    return '<span style="color:' . $color . '">' . $data['expires_at'] . '</span>';
}

function get_data_action($data = 0)
{
    $mailIcon = getStaticUrl('ico/email.png');
    if ($data['mail_send_at']) {
        $mailIcon = getStaticUrl('ico/email_open.png');
    }

    return '<a href="' . $_SERVER['PHP_SELF'] . '?action=send_booking_offers_email&id=' . $data['id'] . '"><img src="' . $mailIcon . '" alt="" title="Send booking offers email" class="button-action" /></a>';
}

function get_data_booked_in_practice($data = 0)
{
    if (!isset($data['booked_in_practice'])) {
        $data['booked_in_practice'] = 0;
    }

    return $GLOBALS['CONFIG']['ARRAY_NO_YES'][$data['booked_in_practice']];
}

$MENU_ARRAY = array(
    "id"                   => array("sql" => 1, "name" => "ID", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "slug"                 => array("sql" => 1, "name" => "Slug", "show" => 1, "edit" => 0, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1),
    "status"               => array("sql" => 1, "name" => "Status", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "select", "values" => Offer::getStatusTextArray()), "list_function" => array("name" => "get_data_status", "para" => 1)),
    "action"               => array("sql" => 0, "name" => "Action", "show" => 1, "edit" => 0, "new" => 0, "search" => 0, "sort" => 0, "nowrap" => 1, "list_function" => array("name" => "get_data_action", "para" => 1)),
    "booking"              => array("sql" => 1, "name" => "Booking", "show" => 1, "edit" => 0, "new" => 0, "search" => 0, "sort" => 0, "nowrap" => 1, "list_function" => array("name" => "get_data_booking", "para" => 1)),
    "booking_id"           => array("sql" => 1, "name" => "BookingID", "show" => 0, "edit" => 1, "new" => 1, "search" => 0, "sort" => 1, "nowrap" => 1),
    "appointment_start_at" => array("sql" => 1, "name" => "Terminvorschlag", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "datetime"), "list_function" => array("name" => "get_data_appointment_start_at", "para" => 1)),
    "expires_at"           => array("sql" => 1, "name" => "Ablaufdatum", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "datetime"), "list_function" => array("name" => "get_data_expires_at", "para" => 1)),
    "booked_in_practice"   => array("sql" => 1, "name" => "BookedInPractice", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "select", "values" => $GLOBALS['CONFIG']['ARRAY_NO_YES']), "list_function" => array("name" => "get_data_booked_in_practice", "para" => 1)),
    "user_id"              => array("sql" => 1, "name" => "Arzt", "show" => 1, "edit" => 1, "new" => 1, "search" => 0, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_user_id", "para" => 1)),
    "location_id"          => array("sql" => 1, "name" => "Praxis", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_location_id", "para" => 1)),
    "mail_send_at"         => array("sql" => 1, "name" => "Mail send", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "comment_intern"       => array("sql" => 1, "name" => 'Kommentar <span style="color:#ff0000;">(intern)', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => ' style="width:300px;height:60px;"')),
    "comment_patient"      => array("sql" => 1, "name" => 'Kommentar Patient', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => 'readonly style="width:300px;height:60px;"')),
    "answered_at"          => array("sql" => 1, "name" => "Geantwortet", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "list_function" => array("name" => "get_data_answered_at", "para" => 1)),
    "created_at"           => array("sql" => 1, "name" => "Erstellungsdatum", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "created_by"           => array("sql" => 1, "name" => "Erstellt von", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_created_by", "para" => 1), "list_function" => array("name" => "get_data_created_by", "para" => 1)),
    "updated_at"           => array("sql" => 1, "name" => "Letztes Update", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "updated_by"           => array("sql" => 1, "name" => "Letzter Bearbeiter", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_updated_by", "para" => 1), "list_function" => array("name" => "get_data_updated_by", "para" => 1)),
);

$BUTTON = array(
    "new"    => array("func" => "bookingoffer_admin_new", "alt_text" => $app->static_text('new_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "document_new.png", "action" => "action=new", "th" => 1, "td" => 0),
    "edit"   => array("func" => "bookingoffer_admin_edit", "alt_text" => $app->static_text('edit_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "document_edit.png", "action" => "action=edit", "th" => 0, "td" => 1),
    "delete" => array("func" => "bookingoffer_admin_delete", "alt_text" => $app->static_text('delete_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "delete.png", "action" => "action=delete", "th" => 0, "td" => 1),
    "cancel" => array("func" => "bookingoffer_admin_cancel", "alt_text" => $app->static_text('cancel_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "cancel.png", "action" => "action=cancel", "th" => 0, "td" => 1),
    "search" => array("func" => "bookingoffer_admin_search", "alt_text" => $app->static_text('search'), "script" => $_SERVER['PHP_SELF'], "gfx" => "search.png", "action" => "action=search", "th" => 1, "td" => 0),
);
$TABLE_CONFIG = array(
    "table_name"  => DB_TABLENAME_BOOKING_OFFERS,
    "alpha_index" => "last_name",
);
$URL_PARA = array(
    'page'         => 1,
    'alpha'        => '',
    "sort"         => 0,
    "direction"    => 'DESC',
    "num_per_page" => 50,
    "type"         => 'num'
);
