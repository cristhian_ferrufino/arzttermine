<?php
/**
 * Admin: Planners
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/../include/bootstrap.php';

/* ************************************************************************************* */

use Arzttermine\Admin\Admin;
use Arzttermine\Application\Application;
use Arzttermine\Appointment\Planner;
use Arzttermine\Appointment\AppointmentGeneratorService;
use Arzttermine\DataTransformer\PlannerBlockAppointments;
use Arzttermine\User\User;
use Arzttermine\Location\Location;

$admin = new Admin();
$app = Application::getInstance();

$view    = $app->getView();
$profile = $app->getCurrentUser();
$request = $app->getRequest();

$view->disableCaching();

/* ************************************************************************************* */

if (!$profile->checkPrivilege('integration_admin')) {
    $view->setRef('ContentRight', $app->static_file('access_denied.html'));
    $view->setRef('ContentLeft', $admin->get_navigation());
    $view->display('admin/content_left_right.tpl');
    exit;
}

/* ************************************************************************************* */

// Bootstrap planner
$planner = new Planner();
$location = new Location($request->getQuery('location_id', 0));
$user = new User($request->getQuery('user_id', 0));

$planner = $planner->getForUserLocation($user, $location);
$locations = $location->findObjects('integration_id = 1 ORDER BY name'); // @todo: Fix the hardcoded-ness
$users = $location->getMembers();

$view->setRefs(array(
    'doctor' => $user,
    'practice' => $location,
    'users' => $users,
    'locations' => $locations,
    'planner' => $planner
));

// Save the planner if we need to
if ($request->isPost()) {
    $payload = $request->getRequest('payload');
    $blocks = json_decode($payload, true); // Everything is wrapped in here to avoid input var count errors

    $planner_blocks = PlannerBlockAppointments::toBlocks($blocks);

    $planner
        ->clearBlocks()
        ->setBlocks($planner_blocks)
        ->saveBlocks();

    // Set and save appointment rules
    $planner
        ->setLeadIn($request->getRequest('lead_in'))
        ->setSearchPeriod($request->getRequest('search_period'))
        ->save(array('lead_in', 'search_period'));
    
    // Generate if needed
    if ($request->getRequest('generate')) {
        AppointmentGeneratorService::getInstance()->clear($planner)->generate($planner);
    }
}

$view
    ->setHeadTitle('Planner Admin')
    ->setRef('blocks', PlannerBlockAppointments::toFullCalendar($planner->getBlocks())) // Set it here to prevent double-load
    ->setRef('ContentLeft', $admin->get_navigation())
    ->setRef('ContentRight', $view->fetch('admin/planner/planner.tpl'))
    ->display('admin/content_left_right.tpl');
