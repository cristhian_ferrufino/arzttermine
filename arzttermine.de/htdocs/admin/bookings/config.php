<?php

use Arzttermine\Booking\Booking;
use Arzttermine\Calendar\Calendar;
use Arzttermine\Integration\Integration;
use Arzttermine\Location\Location;
use Arzttermine\MedicalSpecialty\TreatmentType;
use Arzttermine\User\User;
use NGS\Managers\BookingsManager;

function get_data_created_by($data = 0)
{
    $_user = new User($data['created_by']);
    if (!$_user->isValid()) {
        return '';
    }

    return $_user->getFullName();
}

function get_data_created_at($data = 0)
{
    return mysql2date('d.m.Y H:i:s', $data['created_at'], false);
}

function get_data_updated_by($data = 0)
{
    $_user = new User($data['updated_by']);
    if (!$_user->isValid()) {
        return '';
    }

    return $_user->getFullName();
}

function get_data_status($data = 0)
{
    $status_texts = Booking::getStatusTextArray();
    if (isset($status_texts[$data['status']])) {
        return $status_texts[$data['status']];
    }
    
    return '';
}

function get_data_flexible_time($data = 0)
{
    if (isset($data['flexible_time']) &&
        isset($GLOBALS['CONFIG']['ARRAY_NO_YES'][$data['flexible_time']])
    ) {
        return $GLOBALS['CONFIG']['ARRAY_NO_YES'][$data['flexible_time']];
    }
    
    return '';
}

function get_data_flexible_location($data = 0)
{
    if (isset($data['flexible_location']) &&
        isset($GLOBALS['CONFIG']['ARRAY_NO_YES'][$data['flexible_location']])
    ) {
        return $GLOBALS['CONFIG']['ARRAY_NO_YES'][$data['flexible_location']];
    }
    
    return '';
}

function get_data_backend_marker($data = 0)
{
    if (isset($data['backend_marker']) &&
        isset($GLOBALS['CONFIG']['BACKEND_MARKER_CLASSES'][$data['backend_marker']])) {
        return $GLOBALS['CONFIG']['BACKEND_MARKER_CLASSES'][$data['backend_marker']];
    }
    
    return '';
}

function get_data_user_id($data = 0)
{
    $userId = $data['user_id'];
    $_user = new User($userId);
    if (!$_user->isValid()) {
        return '';
    }
    if ($_user->hasProfile()) {
        //getting open bookings for user
        $bookingsManager = BookingsManager::getInstance();
        $newBookingsForUser = $bookingsManager->getBookingsByUserIdAndStatus($userId, Booking::STATUS_NEW);

        return '[' . $_user->getId() . '] <a href="' . $_user->getUrl() . '" target="_blank">' . $_user->getName() .
               '</a>  Open Bookings: <a href="?open_bookings=1&action=search&form_data[user_id_SEARCH_STYLE]=0&form_data[user_id]=' . $userId . '">' . count($newBookingsForUser) . '</a>';
    } else {
        return $_user->getName();
    }
}

function get_data_location_id($data = 0)
{
    if (isset($data['location_id'])) {
        $locationId = $data['location_id'];
        $location = new Location($locationId);
        if (!$location->isValid()) {
            return '';
        }
        //getting open bookings for location
        $bookingsManager = BookingsManager::getInstance();
        $newBookingsForLocation = $bookingsManager->getBookingsByLocationIdAndStatus($locationId, Booking::STATUS_NEW);

        $length = 30;
        $name = mb_substr($location->getName(), 0, $length, $GLOBALS['CONFIG']['SYSTEM_CHARSET']);
        if (mb_strlen($location->getName(), $GLOBALS['CONFIG']['SYSTEM_CHARSET']) > $length) {
            $name .= '...';
        }

        return '[' . $location->getId() . '] <a href="' . $location->getUrl() . '" target="_blank">' . $name . '</a>  Open Bookings: <a href="?open_bookings=1&action=search&form_data[location_id_SEARCH_STYLE]=0&form_data[location_id]=' . $locationId . '">' . count($newBookingsForLocation) . '</a>';   
    }
    
    return '';
}

function get_data_gender($data = 0)
{
    if (isset($GLOBALS['CONFIG']['ARRAY_GENDER'][$data['gender']])) { 
        return $GLOBALS['CONFIG']['ARRAY_GENDER'][$data['gender']];
    }
    
    return '';
}

function get_data_appointment_start_at($data = 0)
{
    $text = $data['appointment_start_at'];
    if ($data['appointment_start_at'] == '1970-01-01 00:00:00') {
        $text = 'Terminanfrage';
    }

    return $text;
}

function get_data_insurance_id($data = 0)
{
    return $GLOBALS['CONFIG']['INSURANCES'][$data['insurance_id']];
}

function get_data_returning_visitor($data = 0)
{
    if (isset($data['returning_visitor']) && 
        isset($GLOBALS['CONFIG']['ARRAY_NULL_NO_YES'][$data['returning_visitor']])
    ) {
        return $GLOBALS['CONFIG']['ARRAY_NULL_NO_YES'][$data['returning_visitor']];   
    }
    
    return '';
}

function get_data_integration_action($data = 0)
{
    $_integration = Integration::getIntegrationByLocationId($data['location_id']);

    if (!class_implements($_integration, 'IntegrationInterface')) {
        return 'Error';
    }
    $_html = $_integration->getName();
    if ($_integration->hasAdminManualMailing()) {
        $_booking = new Booking($data['id']);
        $_total_mails = 0;
        if ($_booking->isValid()) {
            $_total_mails = $_booking->countBookingMailings();
        }
        $_icon = 'ico/email.png';
        if ($_total_mails > 0) {
            $_icon = 'ico/email_add.png';
        }
        $_html .= '<img style="cursor:pointer" onclick="$(this).siblings().not(\'.cancel_booking\').toggle()" src="' . getStaticUrl($_icon) . '" alt="" title="Verschickte Mails: ' . $_total_mails . '" class="button-action" />';
        $_html
            .= '<div style="display:none" class="mail_links">
		                <div onclick="$(this).parent().hide();" class="close">x</div>
		                <a href="' . $_SERVER['PHP_SELF'] . '?action=integration_mail&booking_id=' . $data['id'] . '&template=admin-booking-new.txt&option=new" class="integration_mail fancybox.iframe">new booking</a>
                        <a href="' . $_SERVER['PHP_SELF'] . '?action=integration_mail&booking_id=' . $data['id'] . '&template=admin-booking-generic.txt&option=cannot-reach" class="integration_mail fancybox.iframe">Patient Cannot be reached by phone</a>
                        <a href="' . $_SERVER['PHP_SELF'] . '?action=integration_mail&booking_id=' . $data['id'] . '&template=admin-booking-generic.txt&option=cannot-place" class="integration_mail fancybox.iframe">Appointment cannot be placed</a>
                        <a href="' . $_SERVER['PHP_SELF'] . '?action=integration_mail&booking_id=' . $data['id'] . '&template=admin-booking-generic.txt&option=cancel" class="integration_mail fancybox.iframe">Cancel Appointment</a>
                  </div>';
    }
    if ($_integration->hasAdminManualBooking()) {
        $_html
            .= '
		<a class="cancel_booking" href="' . $_SERVER['PHP_SELF'] . '?action=integration_book&booking_id=' . $data['id'] . '" target="_blank" onClick="return confirm(\'Soll der Termin in das AIS gebucht werden?\');"><img src="' . getStaticUrl('adm/integration_book.png') . '" alt="" /></a>
<!--		<a href="' . $_SERVER['PHP_SELF'] . '?action=integration_cancel&booking_id=' . $data['id'] . '" target="_blank"><img src="' . getStaticUrl('adm/integration_cancel.png') . '" alt="" /></a>-->
		';
    }

    return $_html;
}

function get_data_integration_status($data = 0)
{
    if (isset($data['integration_status']) &&
        isset($GLOBALS['CONFIG']['BOOKING_INTEGRATION_STATUS'][$data['integration_status']])
    ) {
        return $GLOBALS['CONFIG']['BOOKING_INTEGRATION_STATUS'][$data['integration_status']];
    }
    
    return '';
}

function get_data_integration_data($data = 0)
{
    if (isset($data['id'])) {
        $_booking = new Booking($data['id']);
        if ($_booking->isValid()) {
            return $_booking->getIntegrationBookingHtml();
        }
    }
        
    return '';
}

function get_data_treatmenttype($data = 0)
{
    $_html = '';
    
    if (isset($data['treatmenttype_id'])) {
        $_tt = new TreatmentType($data['treatmenttype_id']);
        if ($_tt->isValid()) {
            $_html = $_tt->getName();
        }   
    }

    return $_html;
}

function get_user_has_contract($data = 0)
{
    $user = new User($data["user_id"]);

    return $GLOBALS["CONFIG"]["ARRAY_NO_YES"][$user->getHasContract()];
}

function get_salesforce_updated_at($data = 0)
{
    global $admin, $URL_PARA;

    if (Calendar::isValidDateTime($data['salesforce_updated_at'])) {
        $html = '<span class="green">' . $data['salesforce_updated_at'] . '</span>';
    } else {
        $html = '<a href="' . $_SERVER['PHP_SELF'] . '?action=salesforce_push&id=' . $data['id'] . $admin->admin_get_url_para($URL_PARA) . '"><span class="red">Push booking</span></a>';
    }

    return $html;
}

function get_data_booking_offer($data = 0)
{
    $html = '';
    $booking = new Booking($data['id']);
    if ($booking->isValid()) {
        $url = sprintf('/admin/booking-offers/index.php?action=new&form_data[booking_id]=%d&form_data[user_id]=%d&form_data[location_id]=%d', $booking->getId(), $booking->getUserId(), $booking->getLocationId());
        $html = '<a href="' . $url . '">Create Booking Offer</a>';
    }

    return $html;
}
function get_data_salesforce_id($data = 0)
{
    return '<a href="https://eu1.salesforce.com/'.$data['salesforce_id'].'" target="_blank">'.$data['salesforce_id'].'</a>';
}

$MENU_ARRAY = array(
    "id"                       => array("sql" => 1, "name" => "ID", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "status"                   => array("sql" => 1, "name" => "Status", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "select", "values" => Booking::getStatusTextArray()), "list_function" => array("name" => "get_data_status", "para" => 1)),
    "appointment_start_at"     => array("sql" => 1, "name" => "Termin", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "list_function" => array("name" => "get_data_appointment_start_at", "para" => 1)),
    "salesforce_id"            => array("sql" => 1, "name" => "SalesforceID", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "list_function" => array("name" => "get_data_salesforce_id", "para" => 1)),
    "salesforce_updated_at"    => array("sql" => 1, "name" => "SalesforceUpdatedAt", "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 0, "nowrap" => 1, "form_input" => array("type" => "none"), "list_function" => array("name" => "get_salesforce_updated_at", "para" => 1)),
    "insurance_id"             => array("sql" => 1, "name" => "Versicherung", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_insurance_id", "para" => 1), "form_input" => array("type" => "select", "values" => $GLOBALS['CONFIG']['INSURANCES'])),
    "gender"                   => array("sql" => 1, "name" => "F/M", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_gender", "para" => 1), "form_input" => array("type" => "select", "values" => $GLOBALS['CONFIG']['ARRAY_GENDER'])),
    "first_name"               => array("sql" => 1, "name" => "Vorname", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "last_name"                => array("sql" => 1, "name" => "Nachname", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "phone"                    => array("sql" => 1, "name" => "Telefon", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "email"                    => array("sql" => 1, "name" => "E-Mail", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "location_id"              => array("sql" => 1, "name" => "Praxis", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_location_id", "para" => 1), "list_function" => array("name" => "get_data_location_id", "para" => 1)),
    "treatmenttype_id"         => array("sql" => 1, "name" => "Behandlungsgrund", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_treatmenttype", "para" => 1), "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_treatmenttype", "para" => 1)),
    "returning_visitor"        => array("sql" => 1, "name" => "Dem Arzt bekannt", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_returning_visitor", "para" => 1), "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_returning_visitor", "para" => 1)),
    "integration_action"       => array("sql" => 0, "name" => "Integration", "show" => 1, "edit" => 0, "new" => 0, "search" => 0, "sort" => 0, "nowrap" => 1, "list_function" => array("name" => "get_data_integration_action", "para" => 1)),
    "integration_status"       => array("sql" => 1, "name" => 'Integration Status', "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_integration_status", "para" => 1), "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_integration_status", "para" => 1)),
    "has_contract"             => array("sql" => 1, "name" => "Mit Vertrag", "show" => 1, "edit" => 0, "new" => 0, "search" => 0, "sort" => 0, "nowrap" => 1, "form_input" => array("type" => "none"), "list_function" => array("name" => "get_user_has_contract", "para" => 1)),
    "user_id"                  => array("sql" => 1, "name" => "Arzt", "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_user_id", "para" => 1), "list_function" => array("name" => "get_data_user_id", "para" => 1)),
    "flexible_time"            => array("sql" => 1, "name" => "Flexible Zeit", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_flexible_time", "para" => 1), "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_flexible_time", "para" => 1)),
    "flexible_location"        => array("sql" => 1, "name" => "Flexibler Ort", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_flexible_location", "para" => 1), "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_flexible_location", "para" => 1)),
    "booked_language"          => array("sql" => 1, "name" => "Sprache", "show" => 0, "edit" => 1, "new" => 0, "search" => 1, "sort" => 0, "nowrap" => 1, "form_input" => array("type" => "none")),
    "integration_booking_data" => array("sql" => 0, "name" => 'Integration Booking Data', "show" => 0, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_function" => array("name" => "get_data_integration_data", "para" => 1), "form_input" => array("type" => "none")),
    "integration_log"          => array("sql" => 1, "name" => 'Integration-Log', "show" => 0, "edit" => 1, "new" => 0, "search" => 1, "sort" => 0, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => ' style="width:300px;height:60px;"')),
    "backend_marker"           => array("sql" => 1, "name" => "BackendMarker", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "select", "values" => $GLOBALS['CONFIG']['BACKEND_MARKER_CLASSES']), "list_function" => array("name" => "get_data_backend_marker", "para" => 1)),
    "source_platform"          => array("sql" => 1, "name" => "Plattform", "show" => 0, "edit" => 1, "new" => 0, "search" => 1, "sort" => 0, "nowrap" => 1, "form_input" => array("type" => "none")),
    "comment_intern"           => array("sql" => 1, "name" => 'Kommentar <span style="color:#ff0000;">(intern)', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => ' style="width:300px;height:60px;"')),
    "comment_patient"          => array("sql" => 1, "name" => 'Kommentar Patient', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => 'readonly style="width:300px;height:60px;"')),
    "booking_offer"            => array("sql" => 0, "name" => "Create BookingOffer", "show" => 1, "edit" => 0, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_booking_offer", "para" => 1)),
    "created_at"               => array("sql" => 1, "name" => "Erstellungsdatum", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "created_by"               => array("sql" => 1, "name" => "Erstellt von", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_created_by", "para" => 1), "list_function" => array("name" => "get_data_created_by", "para" => 1)),
    "updated_at"               => array("sql" => 1, "name" => "Letztes Update", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "updated_by"               => array("sql" => 1, "name" => "Letzter Bearbeiter", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_updated_by", "para" => 1), "list_function" => array("name" => "get_data_updated_by", "para" => 1)),
);

$BUTTON = array(
    "new"                  => array("func" => "booking_admin_new", "alt_text" => $app->static_text('new_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "document_new.png", "action" => "action=new", "th" => 1, "td" => 0),
    "edit"                 => array("func" => "booking_admin_edit", "alt_text" => $app->static_text('edit_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "document_edit.png", "action" => "action=edit", "th" => 0, "td" => 1),
    "delete"               => array("func" => "booking_admin_delete", "alt_text" => $app->static_text('delete_item'), "script" => $_SERVER['PHP_SELF'], "gfx" => "delete.png", "action" => "action=delete", "th" => 0, "td" => 1),
    //	"cancel"				=> array("func"=>"booking_admin_cancel","alt_text"=>$app->static_text('cancel_item'),"script"=>$_SERVER['PHP_SELF'],"gfx"=>"cancel.png","action"=>"action=cancel","th"=>0,"td"=>1),
    "search"               => array("func" => "booking_admin_search", "alt_text" => $app->static_text('search'), "script" => $_SERVER['PHP_SELF'], "gfx" => "search.png", "action" => "action=search", "th" => 1, "td" => 0),
    "csv"                  => array("func" => "booking_admin_csv", "alt_text" => 'Liste als CSV herunterladen', "script" => $_SERVER['PHP_SELF'], "gfx" => "download.gif", "action" => "action=csv", "th" => 1, "td" => 0),
    //	"backend_marker"	=> array("tr"=>1,"th"=>0,"td"=>0,"template_key"=>"tr_class","function"=>array("name"=>"get_data_backend_marker_key","para"=>1)),
    "backend_marker"       => array("tr" => 1, "th" => 0, "td" => 0, "template_key" => "tr_class"),
);
$TABLE_CONFIG = array(
    "table_name"  => DB_TABLENAME_BOOKINGS,
    "alpha_index" => "last_name",
);
$URL_PARA = array(
    'page'         => 1,
    'alpha'        => '',
    "sort"         => 0,
    "direction"    => 'DESC',
    "num_per_page" => 50,
    "type"         => 'num'
);
