<?php
/**
 * Admin: Bookings
 *
 * @author Axel Kuzmik <axel@kuzmik.de>
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/../include/bootstrap.php';
require_once "config.php";

/* ************************************************************************************* */

use Arzttermine\Admin\Admin;
use Arzttermine\Application\Application;
use Arzttermine\Booking\Booking;
use Arzttermine\Cms\CmsContent;
use Arzttermine\Mail\Mailing;
use Arzttermine\MedicalSpecialty\TreatmentType;
use NGS\DAL\DTO\UsersDto;
use NGS\Managers\DoctorsManager;
use NGS\Managers\UsersManager;

$admin = new Admin();
$app = Application::getInstance();
$view = $app->getView();
$profile = $app->getCurrentUser();

$view->disableCaching();

/* ************************************************************************************* */

if (!$profile->checkPrivilege('booking_admin')) {
    $view->setRef('ContentRight', $app->static_file('access_denied.html'));
    $view->setRef('ContentLeft', $admin->get_navigation());
    $view->display('admin/content_left_right.tpl');
    exit;
}
/* ************************************************************************************* */
function get_list($URL_PARA, $search = 0)
{
    global $admin;

    $_html = $admin->admin_get_list(0, $search, $URL_PARA);
    $_html
        .= '<script type="text/javascript">
//<![CDATA[
$(document).ready(function() {
	$(".integration_mail").fancybox({
		maxWidth	: 800,
		maxHeight	: 600,
		fitToView	: false,
		width		: \'70%\',
		height		: \'70%\',
		autoSize	: false,
		closeClick	: false,
		openEffect	: \'none\',
		closeEffect	: \'none\'
	});
});
//]]>
</script>';

    return $_html;
}

/* ************************************************************************************* */
function new_one($action, $form_data, $URL_PARA)
{
    global $profile, $admin, $view, $app;

    // 1. set defaults
    $form_data["created_by"] = $profile->getId();
    $form_data["created_at"] = $app->now();

    // 2. check
    if (1 > mb_strlen(sanitize_title($form_data["first_name"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
        $view->addMessage('admin_message_first_name', 'ERROR', $app->static_text('Es muss ein Vorname angegeben werden'));
    }
    if (1 > mb_strlen(sanitize_title($form_data["last_name"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
        $view->addMessage('admin_message_last_name', 'ERROR', $app->static_text('Es muss ein Nachname angegeben werden'));
    }

    // if there are checkboxes than set the keys to make sure that deselected checkboxes are beeing set
    $form_data = $admin->set_unselected_checkbox_values($form_data);

    // 3. exit on errors
    if ($view->countMessages()) {
        $view->addMessage('STATUS', 'ERROR', $app->_('Bitte prüfen Sie Ihre Eingaben.'));
        $view->setRef('ContentRight', form_change($action, $id, $URL_PARA, $form_data));

        return $action;
    }
    // 4. sanitize
    $form_data["first_name"] = sanitize_title($form_data["first_name"]);
    $form_data["last_name"] = sanitize_title($form_data["last_name"]);

    $_id = $admin->admin_set_new($form_data);
    if ($_id > 0) {
        $view->addMessage('STATUS', 'NOTICE', $app->static_text('Die Buchung wurde erfolgreich mit der ID ' . $_id . ' angelegt.<br /><a href="/admin/bookings/index.php?action=edit&id=' . $_id . '">Buchung bearbeiten</a>'));
        $action = 'list';
    } elseif ($_id == -1) {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('the_entry_allready_exists'));
        $view->setRef('ContentRight', form_new($action, $URL_PARA, $form_data));
        $action = 'list';
    } elseif ($_id == -2) {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('name_of_primary_key_is_empty'));
        $view->setRef('ContentRight', form_new($action, $URL_PARA, $form_data));
        $action = 'list';
    } else {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('the_entry_could_not_be_created'));
        $action = 'list';
    }

    return $action;
}

/* ************************************************************************************* */
function change_one($action, $id, $form_data, $URL_PARA)
{
    global $profile, $admin, $view, $app;

    // 1. set defaults
    $form_data["updated_by"] = $profile->getId();
    $form_data["updated_at"] = $app->now();

    // 2. check
    if (1 > mb_strlen(sanitize_title($form_data["first_name"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
        $view->addMessage('admin_message_first_name', 'ERROR', $app->static_text('Es muss ein Vorname angegeben werden'));
    }
    if (1 > mb_strlen(sanitize_title($form_data["last_name"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
        $view->addMessage('admin_message_last_name', 'ERROR', $app->static_text('Es muss ein Nachname angegeben werden'));
    }

    // if there are checkboxes than set the keys to make sure that deselected checkboxes are beeing set
    $form_data = $admin->set_unselected_checkbox_values($form_data);

    // 3. exit on errors
    if ($view->countMessages()) {
        $view->addMessage('STATUS', 'ERROR', $app->_('Bitte prüfen Sie Ihre Eingaben.'));
        $view->setRef('ContentRight', form_change($action, $id, $URL_PARA, $form_data));

        return $action;
    }
    // 4. sanitize
    $form_data["first_name"] = sanitize_title($form_data["first_name"]);
    $form_data["last_name"] = sanitize_title($form_data["last_name"]);

    // if there are checkboxes than set the keys to make sure that deselected checkboxes are beeing set
    $form_data = $admin->set_unselected_checkbox_values($form_data);

    $_result = $admin->admin_set_update($id, $form_data);

    if ($_result == 1) {
        $view->addMessage('STATUS', 'NOTICE', $app->static_text('action_was_successfully'));
        $action = 'list';
    } elseif ($_result == -1) {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('the_entry_allready_exists'));
        $view->setRef('ContentRight', form_change($action, $id, $URL_PARA, $form_data));
        $action = 'list';
    } elseif ($_result == -2) {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('name_of_primary_key_is_empty'));
        $view->setRef('ContentRight', form_change($action, $id, $URL_PARA, $form_data));
        $action = 'list';
    } else {
        $view->addMessage('STATUS', 'WARNING', $app->static_text('no_data_has_been_updated'));
        $view->setRef('ContentRight', form_change($action, $id, $URL_PARA, $form_data));
        $action = 'list';
    }

    return $action;
}

/* ************************************************************************************* */
function ask4delete($id, $URL_PARA)
{
    global $admin;

    $_html = $admin->admin_get_confirm_table($id, $URL_PARA, 'delete');

    return $_html;
}

/* ************************************************************************************* */
function ask4cancel($id, $URL_PARA)
{
    global $admin;
    $_html = $admin->admin_get_confirm_table($id, $URL_PARA, 'cancel');

    return $_html;
}

/* ************************************************************************************* */
function delete_one($id)
{
    global $admin;

    return $admin->admin_set_delete($id);
}

/* ************************************************************************************* */
function cancel_one($id)
{
    return DoctorsManager::getInstance()->cancelBookedAppointment($id);
}

/* ************************************************************************************* */
function form_new($action, $URL_PARA, $error_data = 0)
{
    global $admin, $view;

    $_html = $admin->admin_get_form($action, 0, $error_data, $URL_PARA);

    $view->setFocus('#form_data_first_name');

    return $_html;
}

/* ************************************************************************************* */
function form_change($action, $id, $URL_PARA, $error_data = 0)
{
    global $admin, $view;

    $_html = $admin->admin_get_form($action, $id, $error_data, $URL_PARA);

    $view->setFocus('#form_data_first_name');

    return $_html;
}

/* ************************************************************************** */
function show_search_form($action, $search_data, $URL_PARA)
{
    global $admin;

    return $admin->admin_get_form($action, 0, $search_data, $URL_PARA);
}

/* ************************************************************************************* */
function form_integration_mail($booking_id, $url_para)
{
    global $admin, $view;
    $_booking = new Booking($booking_id);
    if (!$_booking->isValid()) {
        $_html = 'Ungültige BuchungsID';
    } else {
        $main_body = '';
        $subject = '';

        if (getParam('option') == "new") {
            $main_body = CmsContent::getCmsContent('/mailing/integrations/' . $_booking->getIntegration()->getId() . '/admin-booking-new-content.txt')->getContent();
            $subject = CmsContent::getCmsContent('/mailing/integrations/' . $_booking->getIntegration()->getId() . '/admin-booking-new-content.txt')->getTitle();
        } else if (getParam('option') == "cannot-reach") {
            $main_body = CmsContent::getCmsContent('/mailing/integrations/' . $_booking->getIntegration()->getId() . '/booking-cannot-reach.html')->getContent();
            $subject = CmsContent::getCmsContent('/mailing/integrations/' . $_booking->getIntegration()->getId() . '/booking-cannot-reach.html')->getTitle();
        } else if (getParam('option') == "cannot-place") {
            $main_body = CmsContent::getCmsContent('/mailing/integrations/' . $_booking->getIntegration()->getId() . '/admin-booking-cannot-place-content.txt')->getContent();
            $subject = CmsContent::getCmsContent('/mailing/integrations/' . $_booking->getIntegration()->getId() . '/admin-booking-cannot-place-content.txt')->getTitle();
        } else if (getParam('option') == "cancel") {
            $main_body = CmsContent::getCmsContent('/mailing/integrations/' . $_booking->getIntegration()->getId() . '/admin-booking-cancel.txt')->getContent();
            $subject = CmsContent::getCmsContent('/mailing/integrations/' . $_booking->getIntegration()->getId() . '/admin-booking-cancel.txt')->getTitle();
        }
        $view->setRef('booking', $_booking);
        $view->setRef('admin_hidden', $admin::admin_get_hidden($url_para));
        $_data = array();
        $_data['user_name'] = $_booking->getUser()->getName();
        $_data['location_name'] = $_booking->getLocation()->getName();
        $_body = $main_body;
        $view->setRef('user_name', $_booking->getUser()->getName());
        $view->setRef('booking_salutation_full_name', $_booking->getSalutationName());
        $view->setRef('location_name', $_booking->getLocation()->getName());
        $view->setRef('location_address', $_booking->getLocation()->getAddress(', '));
        $view->setRef('appointment_date', $_booking->getAppointment()->getWeekdayDateText());
        $view->setRef('appointment_time', $_booking->getAppointment()->getTimeText());
        $view->setRef('template_filename', '/mailing/integrations/' . $_booking->getIntegration()->getId() . '/' . getParam('template'));
        $view->setRef('option', getParam('option'));
        $view->setRef('body', $_body);
        $_subject = Mailing::arrayReplace($subject, $_data);
        $view->setRef('title', $_subject);
        $view->setRef('subject', $_subject);
        $_html = $view->fetch('admin/booking/_integration_mail_iframe_p1.tpl');
    }

    return $_html;
}

/* ************************************************************************** */
function show_csv()
{
    $_booking = new Booking();
    $_bookings = $_booking->findObjects('1=1 ORDER BY created_at ASC');
    if (!empty($_bookings)) {
        $_html = 'id,created_at,appointment_start_at,status,first_name,last_name,email,phone,insurance_text,returning_visitor_text,user_id,user_name,location_id,location_name,medical_specialty_text,treatmenttype_text,PLZ,Stadt,comment_intern' . "\n";

        /** @var Booking $_booking */
        foreach ($_bookings as $_booking) {
            $_location = $_booking->getLocation();
            $_user = $_booking->getUser();
            $ttName = TreatmentType::getNameStatic($_booking->getTreatmenttypeId());
            $_html .= '"' . $_booking->getId() . '","' .
                      sanitize_csv_field($_booking->getCreatedAt()) . '","' .
                      sanitize_csv_field($_booking->getAppointment()->getDateTime()) . '","' .
                      sanitize_csv_field($_booking->getStatusText(true)) . '","' .
                      sanitize_csv_field($_booking->getFirstName()) . '","' .
                      sanitize_csv_field($_booking->getLastName()) . '","' .
                      sanitize_csv_field($_booking->getEmail()) . '","' .
                      sanitize_csv_field($_booking->getPhone()) . '","' .
                      sanitize_csv_field($_booking->getInsuranceText()) . '","' .
                      sanitize_csv_field($_booking->getReturningVisitorText()) . '","' .
                      sanitize_csv_field($_user->getId()) . '","' .
                      sanitize_csv_field($_user->getName()) . '","' .
                      sanitize_csv_field($_location->getId()) . '","' .
                      sanitize_csv_field($_location->getName()) . '","' .
                      sanitize_csv_field($_user->getMedicalSpecialityIdsText('|')) . '","' .

                      sanitize_csv_field($ttName) . '","' .
                      sanitize_csv_field($_location->getZip()) . '","' .
                      sanitize_csv_field($_location->getCity()) . '","' .
                      sanitize_csv_field(preg_replace("/(\r\n|\n\r|\r|\n)/", " ", $_booking->getCommentIntern())) . '"' . "\n";
        }
        // use iso-8859-1 instead of utf-8 ONLY because ms excel on mac os
        // messes up the encoding for csv import
        // import should be then with "Windows (ANSI)" or "Windows Latin 1"
        header("Content-Type: text/csv; charset=iso-8859-1");
        header("Content-Disposition: attachment; filename=bookings-" . date('Ymd_His') . '-iso-8859-1.csv');
        header("Pragma: no-cache");
        header("Expires: 0");
        echo utf8_decode($_html);
        exit;
    }
    echo 'ERROR: No available cases';
    exit;
}

/* ************************************************************************************* */
function integration_book()
{
    $_booking_id = getParam('booking_id');
    $_booking = new Booking($_booking_id);
    if (!$_booking->isValid()) {
        $_html = 'Unvalid booking_id';
    } else {
        if ($_booking->integrationBook() !== true) {
            $_html = 'Booking failed';
        } else {
            $_html = 'Booking OK';
        }
    }

    return $_html;
}

/* ************************************************************************************* */
function integration_mail($id, $form_data)
{
    $_content = CmsContent::getCmsContent($form_data['template_filename']);
    $_data = array();
    $_data['user_name'] = $form_data['user_name'];
    $_data['location_name'] = $form_data['location_name'];
    $_data['booking_salutation_full_name'] = $form_data['booking_salutation_full_name'];
    $_data['location_address'] = $form_data['location_address'];
    $_data['appointment_date'] = $form_data['appointment_date'];
    $_data['appointment_time'] = $form_data['appointment_time'];
    $_data['domain'] = $GLOBALS['CONFIG']['DOMAIN'];
    $_data['body'] = nl2br($form_data['body']);
    $_body = $_content->getContent();
    $_body = Mailing::arrayReplace($_body, $_data);
    $_mailing = new Mailing();
    $_mailing->type = MAILING_TYPE_TEXT;
    $_mailing->source_id = $id;
    $_mailing->source_type_id = MAILING_SOURCE_TYPE_ID_BOOKING;
    $_mailing->from = $GLOBALS['CONFIG']["BOOKINGS_EMAIL_ADDRESS"];
    $_mailing->from_name = $GLOBALS['CONFIG']["BOOKINGS_EMAIL_NAME"];
    $_mailing->body_text = $_body;
    $_mailing->subject = $form_data['subject'];
    $_mailing->addAddressTo($form_data['email'], $form_data['name']);

    return $_mailing->sendPlain();
}

/* ************************************************************************************** */
/* Anfragen auswerten ******************************************************************* */
//URL_PARA neu bestimmen
foreach ($URL_PARA as $key => $val) {
    if (isset($_REQUEST[$key])) {
        $URL_PARA[$key] = $_REQUEST[$key];
    }
}
$action = getParam("action", 'list');
$form_data = getParam("form_data");

switch ($action) {
    case "edit":
        if (!$profile->checkPrivilege('booking_admin_edit')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        } else {
            $id = getParam("id");
            if (is_array($form_data)) {

                if ($GLOBALS['CONFIG']['SALESFORCE_ENABLE']) {
                    /*
                     * after editting the booking, the relative Salesforce Patienten record should also be editted
                     * create a new instance of salesforce_interface
                     * login to salesforce account
                     * call upsertBooking with required parameters
                     */
                    try {
                        $salesforce_interface = new SalesforceInterface();

                        $dm = UsersManager::getInstance();
                        $doctor_name = '';

                        /** @var UsersDto $doctor */
                        if (!empty($form_data['user_id']) && $doctor = $dm->getUserById($form_data['user_id'])) {
                            $doctor_name = "{$doctor->getTitle()} {$doctor->getFirstName()} {$doctor->getLastName()}";
                        }

                        $salesforce_interface->upsertBooking(
                            $id,
                            $form_data['gender'],
                            $form_data['first_name'],
                            $form_data['last_name'],
                            $form_data['email'],
                            $form_data['phone'],
                            null,
                            null,
                            $form_data['insurance_id'],
                            null,
                            null,
                            null,
                            $form_data['status'],
                            intval($form_data['returning_visitor']),
                            $doctor_name
                        );
                        // save the status that this booking was pushed to Salesforce
                        $booking = new Booking($id);
                        if ($booking->isValid()) {
                            $booking->setSalesforceUpdatedAt($app->now());
                            $booking->save('salesforce_updated_at');
                        }
                    } catch (Exception $e) {
                        /*
                         * the user is redirected back to the edit form and a
                         * notice message appears. No edits are applied in the AT database
                        */
                        $view->setRef('ContentRight', form_change($action, $id, $URL_PARA));
                        $view->setHeadTitle('Buchungen Admin');
                        $view->setRef('ContentLeft', $admin->get_navigation());
                        $view->addMessage('STATUS', 'NOTICE', $app->static_text('Something went wrong, please try again.'));
                        $view->display('admin/content_left_right.tpl');
                        exit();
                    }
                }
                $action = change_one($action, $id, $form_data, $URL_PARA);
            } else {
                $view->setRef('ContentRight', form_change($action, $id, $URL_PARA));
            }
        }
        break;
    case "delete":
        if (!$profile->checkPrivilege('booking_admin_delete')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        } else {
            if (!isset($_REQUEST["confirm_id"])) {
                $id = getParam("id");
                $view->setRef('ContentRight', ask4delete($id, $URL_PARA));
            } else {
                $confirm_id = getParam("confirm_id");
                try {
                    if ($GLOBALS['CONFIG']['SALESFORCE_ENABLE']) {
                        /*before deleting the booking, the relative Salesforce Patienten record should also be deleted*/
                        $salesforce_interface = new SalesforceInterface();
                        $salesforce_interface->deletePatientenById($confirm_id);
                    }
                    /* if all went weel in SF delete the record in AT */
                    if (delete_one($confirm_id)) {
                        $view->addMessage('STATUS', 'NOTICE', $app->static_text('the_entry_was_deleted_successfully'));
                    } else {
                        $view->addMessage('STATUS', 'ERROR', $app->static_text('the_entry_could_not_be_deleted'));
                    }
                } catch (Exception $e) {
                    /*
                     * the user is redirected to the booking list and a
                     * notice message appears. The delete action is not applied in the AT database
                    */
                    $view->addMessage('STATUS', 'NOTICE', $app->static_text('Something went wrong, please try again.'));
                }
                $action = 'list';
            }
        }
        break;
    case "cancel":
        if (!$profile->checkPrivilege('booking_admin_cancel')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        } else {
            if (!isset($_REQUEST["confirm_id"])) {
                $id = getParam("id");
                $view->setRef('ContentRight', ask4cancel($id, $URL_PARA));
            } else {
                $confirm_id = getParam("confirm_id");
                $res = cancel_one($confirm_id);
                if ($res === true) {
                    $view->addMessage('STATUS', 'NOTICE', $app->static_text('the_entry_was_canceled_successfully'));
                } else {
                    $view->addMessage('STATUS', 'ERROR', $app->static_text('the_entry_could_not_be_canceled' . ' (' . $res . ')'));
                }
                $action = 'list';
            }
        }
        break;

    case "search":
        if (!$profile->checkPrivilege('booking_admin_search')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        }
        if (isset($_REQUEST["send_button"])) {
            $URL_PARA["page"] = 1;
            $URL_PARA["alpha"] = '';
        }
        $view->setRef('ContentRight', show_search_form($action, $form_data, $URL_PARA));
        if (is_array($form_data)) {
            $view->append('ContentRight', get_list($URL_PARA, $form_data));
        }
        break;
    case "new":
        if (!$profile->checkPrivilege('booking_admin_new')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        }
        if (is_array($form_data)) {
            $action = new_one($action, $form_data, $URL_PARA);
        } else {
            $view->setRef('ContentRight', form_new($action, $URL_PARA));
        }
        break;
    case "integration_book":
        if (!$profile->checkPrivilege('booking_admin_integration_book')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        }
        $view->setRef('ContentRight', integration_book());
        break;
    case "integration_cancel":
        if (!$profile->checkPrivilege('booking_admin_integration_cancel')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        }
        if (is_array($form_data)) {
            $action = integration_cancel($action, $form_data, $URL_PARA);
        } else {
            $view->setRef('ContentRight', form_integration_cancel($action, $URL_PARA));
        }
        break;
    case "integration_mail":
        if (!$profile->checkPrivilege('booking_admin_integration_book')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        }
        if (is_array($form_data)) {
            $_result = integration_mail(getParam("id"), $form_data);
            if ($_result) {
                // avoid reload: redirect
                $_url_para = $admin->admin_get_url_para($URL_PARA);
                $app->redirect('/admin/bookings/index.php?' . $_url_para, array('status' => 'NOTICE', 'text' => 'Die Nachricht wurde verschickt'));
                exit;
            } else {
                $view->addMessage('STATUS', 'ERROR', 'Die Nachricht konnt nicht verschickt werden');
            }
            $action = 'list';
        } else {
            $view->setRef('ContentMain', form_integration_mail(getParam('booking_id'), $URL_PARA));
            $view->display('admin/iframe_full.tpl');
            exit;
        }
        break;
    case "salesforce_push":
        if (!$profile->checkPrivilege('booking_admin_edit')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        } else {
            $id = getParam("id");

            if ($GLOBALS['CONFIG']['SALESFORCE_ENABLE']) {
                $booking = new Booking($id);
                if ($booking->isValid()) {
                    if ($booking->createInSalesforce()) {
                        $view->addMessage('STATUS', 'NOTICE', 'The booking was successfully created in salesforce');
                    } else {
                        $view->addMessage('STATUS', 'ERROR', 'The booking could not be created in salesforce');
                    }
                }
            } else {
                $view->addMessage('STATUS', 'ERROR', 'The environment is configured to not use salesforce so the booking was not created in salesforce');
            }
            $action = 'list';
        }
        break;
    case "csv":
        if (!$profile->checkPrivilege('booking_admin_csv')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        }
        show_csv();
        exit;
        break;
}

if ($action == 'list') {
    if ($profile->checkPrivilege('booking_admin_view')) {
        $view->setRef('ContentRight', get_list($URL_PARA));
    }
}

$view->setHeadTitle('Buchungen Admin');
$view->setRef('ContentLeft', $admin->get_navigation());
$view->display('admin/content_left_right.tpl');
