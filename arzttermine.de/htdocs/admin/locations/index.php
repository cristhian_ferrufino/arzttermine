<?php
/**
 * Admin: Locations
 *
 * @author Axel Kuzmik <axel@kuzmik.de>
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/../include/bootstrap.php';
require_once "config.php";

/* ************************************************************************************* */

use Arzttermine\Admin\Admin;
use Arzttermine\Application\Application;
use Arzttermine\Maps\GoogleMap;
use Arzttermine\Location\Location;
use Arzttermine\User\User;
use Arzttermine\Integration\IntegrationConnection;

$admin = new Admin();
$app = Application::getInstance();
$view = $app->getView();
$profile = $app->getCurrentUser();

$view->disableCaching();

/* ************************************************************************************* */

if (!$profile->checkPrivilege('location_admin')) {
    $view->setRef('ContentRight', $app->static_file('access_denied.html'));
    $view->setRef('ContentLeft', $admin->get_navigation());
    $view->display('admin/content_left_right.tpl');
    exit;
}
/* ************************************************************************************* */
function get_list($URL_PARA, $search = 0)
{
    global $admin;

    $_html = $admin->admin_get_list(0, $search, $URL_PARA);

    return $_html;
}

/* ************************************************************************************* */
function new_one($action, $form_data, $URL_PARA)
{
    global $profile, $admin, $view, $app;

    // 1. set defaults
    $form_data["created_by"] = $profile->getId();
    $form_data["created_at"] = $app->now();

    // 2. check
    if (1 > mb_strlen(sanitize_title($form_data["name"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
        $view->addMessage('admin_message_name', 'ERROR', $app->static_text('Es muss ein Name angegeben werden'));
    }
    if (1 > mb_strlen(sanitize_slug($form_data["slug"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
        $view->addMessage('admin_message_slug', 'ERROR', $app->static_text('Der Permalink ist nicht gültig!'));
    } else {
        $_slug = sanitize_slug($form_data["slug"]);
        $_location = new Location();
        $_total = $_location->count('slug="' . sqlsave($_slug) . '"');
        if ($_total > 0) {
            $view->addMessage('admin_message_slug', 'ERROR', 'Der Permalink ist bereits vergeben!');
        }
    }

    // if there are checkboxes than set the keys to make sure that deselected checkboxes are beeing set
    $form_data = $admin->set_unselected_checkbox_values($form_data);

    $_medical_specialty_ids = '';
    if (isset($form_data['medical_specialty_ids']) && is_array($form_data['medical_specialty_ids'])) {
        $_medical_specialty_ids = implode(',', array_keys($form_data['medical_specialty_ids']));
    }
    $form_data['medical_specialty_ids'] = $_medical_specialty_ids;

    // 3. exit on errors
    if ($view->countMessages()) {
        $view->addMessage('STATUS', 'ERROR', $app->_('Bitte prüfen Sie Ihre Eingaben.'));
        $view->setRef('ContentRight', form_change($action, 0, $URL_PARA, $form_data));

        return $action;
    }
    // 4. sanitize
    $form_data["slug"] = sanitize_slug($form_data["slug"]);
    $form_data["name"] = sanitize_text_field($form_data["name"]);
    $form_data["street"] = sanitize_text_field($form_data["street"]);
    $form_data["city"] = sanitize_text_field($form_data["city"]);
    $form_data["zip"] = sanitize_text_field($form_data["zip"]);
    $form_data["email"] = sanitize_email($form_data["email"]);
    $form_data["www"] = sanitize_url($form_data["www"]);
    $form_data["phone"] = sanitize_text_field($form_data["phone"]);
    $form_data["fax"] = sanitize_text_field($form_data["fax"]);
    $form_data["info_1"] = sanitize_textarea_field($form_data["info_1"]);
    $form_data["comment_intern"] = sanitize_textarea_field($form_data["comment_intern"]);
    $form_data['member_ids'] = preg_replace('/[^\d\,]/', '', $form_data['member_ids']);

    // get the geocode
    $_address = $form_data['street'] . ',' . $form_data['zip'] . ' ' . $form_data['city'];
    $_coords = GoogleMap::getGeoCoords($_address);
    if (!empty($_coords)) {
        $form_data['lat'] = $_coords['lat'];
        $form_data['lng'] = $_coords['lng'];
    } else {
        $form_data['lat'] = '';
        $form_data['lng'] = '';
    }

    $id = $admin->admin_set_new($form_data);
    if ($id > 0) {
        /* first delete all old entries */
        $wrong_ids = array();
        $integrationConnection = new IntegrationConnection();
        $integrationConnection->deleteLocationConnections($id);
        if ($form_data['member_ids'] != '') {
            $user_ids = explode(',', $form_data['member_ids']);
            if (is_array($user_ids) && !empty($user_ids)) {
                foreach ($user_ids as $user_id) {
                    $user = new User($user_id);
                    if (!$user->isValid() || !$integrationConnection->connect($user_id, $id, false)) {
                        $wrong_ids[] = $user_id;
                    }
                }
            }
        }
        if (!empty($wrong_ids)) {
            $view->addMessage('STATUS', 'WARNING', 'Die Ärzte mit den IDs ' . implode(',', $wrong_ids) . ' existieren nicht.');
        }
        $view->addMessage('STATUS', 'NOTICE', $app->static_text('Die Location wurde erfolgreich mit der ID ' . $id . ' angelegt.<br /><a href="/admin/locations/index.php?action=edit&id=' . $id . '">Location bearbeiten</a>'));
        $action = 'list';
    } elseif ($id == -1) {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('the_entry_allready_exists'));
        $view->setRef('ContentRight', form_new($action, $URL_PARA, $form_data));
        $action = 'list';
    } elseif ($id == -2) {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('name_of_primary_key_is_empty'));
        $view->setRef('ContentRight', form_new($action, $URL_PARA, $form_data));
        $action = 'list';
    } else {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('the_entry_could_not_be_created'));
        $action = 'list';
    }

    return $action;
}

/* ************************************************************************************* */
function change_one($action, $id, $form_data, $URL_PARA)
{
    global $profile, $admin, $view, $app;

    // 1. set defaults
    $form_data["updated_by"] = $profile->getId();
    $form_data["updated_at"] = $app->now();

    // 2. check
    if (1 > mb_strlen(sanitize_title($form_data["name"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
        $view->addMessage('admin_message_name', 'ERROR', $app->static_text('Der Name ist nicht gültig'));
    }
    if (1 > mb_strlen(sanitize_slug($form_data["slug"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
        $view->addMessage('admin_message_slug', 'ERROR', $app->static_text('Der Permalink ist nicht gültig!'));
    } else {
        $_slug = sanitize_slug($form_data["slug"]);
        $_location = new Location();
        $_total = $_location->count('slug="' . sqlsave($_slug) . '" AND id!=' . $id);
        if ($_total > 0) {
            $view->addMessage('admin_message_slug', 'ERROR', 'Der Permalink ist bereits vergeben!');
        }
    }

    // if there are checkboxes than set the keys to make sure that deselected checkboxes are beeing set
    $form_data = $admin->set_unselected_checkbox_values($form_data);

    $_medical_specialty_ids = '';
    if (isset($form_data['medical_specialty_ids']) && is_array($form_data['medical_specialty_ids'])) {
        $_medical_specialty_ids = implode(',', array_keys($form_data['medical_specialty_ids']));
    }
    $form_data['medical_specialty_ids'] = $_medical_specialty_ids;

    // 3. exit on errors
    if ($view->countMessages()) {
        $view->addMessage('STATUS', 'ERROR', $app->_('Bitte prüfen Sie Ihre Eingaben.'));
        $view->setRef('ContentRight', form_change($action, $id, $URL_PARA, $form_data));

        return $action;
    }
    // 4. sanitize
    //$form_data["slug"] = sanitize_slug($form_data["slug"]);
    $form_data["name"] = sanitize_text_field($form_data["name"]);
    $form_data["street"] = sanitize_text_field($form_data["street"]);
    $form_data["city"] = sanitize_text_field($form_data["city"]);
    $form_data["zip"] = sanitize_text_field($form_data["zip"]);
    $form_data["email"] = sanitize_email($form_data["email"]);
    $form_data["www"] = sanitize_url($form_data["www"]);
    $form_data["phone"] = sanitize_text_field($form_data["phone"]);
    $form_data["fax"] = sanitize_text_field($form_data["fax"]);
    $form_data["info_1"] = sanitize_textarea_field($form_data["info_1"]);
    $form_data["comment_intern"] = sanitize_textarea_field($form_data["comment_intern"]);
    $form_data['member_ids'] = preg_replace('/[^\d\,]/', '', $form_data['member_ids']);

    // get the geocode
    $_address = $form_data['street'] . ',' . $form_data['zip'] . ' ' . $form_data['city'];
    $_coords = GoogleMap::getGeoCoords($_address);
    if (!empty($_coords)) {
        $form_data['lat'] = $_coords['lat'];
        $form_data['lng'] = $_coords['lng'];
    } else {
        $form_data['lat'] = '';
        $form_data['lng'] = '';
    }

    // if there are checkboxes than set the keys to make sure that deselected checkboxes are beeing set
    $form_data = $admin->set_unselected_checkbox_values($form_data);

    $_result = $admin->admin_set_update($id, $form_data);
    if ($_result == 1) {
        // update the profile_asset_filename
        $location = new Location($id);
        if ($location->isValid()) {
            $location->updateProfileAssetFilename();
        }

        /* first delete all old entries */
        $wrong_ids = array();
        $integrationConnection = new IntegrationConnection();
        $integrationConnection->deleteLocationConnections($id);
        if ($form_data['member_ids'] != '') {
            $user_ids = explode(',', $form_data['member_ids']);
            if (is_array($user_ids) && !empty($user_ids)) {
                foreach ($user_ids as $user_id) {
                    $user = new User($user_id);
                    if (!$user->isValid() || !$integrationConnection->connect($user_id, $id, false)) {
                        $wrong_ids[] = $user_id;
                    }
                }
            }
        }
        if (!empty($wrong_ids)) {
            $view->addMessage('STATUS', 'WARNING', 'Die Ärzte mit den IDs ' . implode(',', $wrong_ids) . ' existieren nicht.');
        }
        $view->addMessage('STATUS', 'NOTICE', $app->static_text('action_was_successfully'));
        $action = 'list';
    } elseif ($_result == -1) {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('the_entry_allready_exists'));
        $view->setRef('ContentRight', form_change($action, $id, $URL_PARA, $form_data));
        $action = 'list';
    } elseif ($_result == -2) {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('name_of_primary_key_is_empty'));
        $view->setRef('ContentRight', form_change($action, $id, $URL_PARA, $form_data));
        $action = 'list';
    } else {
        $view->addMessage('STATUS', 'WARNING', $app->static_text('no_data_has_been_updated'));
        $view->setRef('ContentRight', form_change($action, $id, $URL_PARA, $form_data));
        $action = 'list';
    }

    return $action;
}

/* ************************************************************************************* */
function ask4delete($id, $URL_PARA)
{
    global $admin;

    $_html = $admin->admin_get_confirm_table($id, $URL_PARA, 'delete');

    return $_html;
}

/* ************************************************************************************* */
function delete_one($id)
{
    global $admin;

    return $admin->admin_set_delete($id);
}

/* ************************************************************************************* */
function form_new($action, $URL_PARA, $error_data = 0)
{
    global $admin, $view;

    $_html = $admin->admin_get_form($action, 0, $error_data, $URL_PARA);

    $view->setFocus('#form_data_name');

    return $_html;
}

/* ************************************************************************************* */
function form_change($action, $id, $URL_PARA, $error_data = 0)
{
    global $admin, $view;

    $_html = $admin->admin_get_form($action, $id, $error_data, $URL_PARA);

    $location = new Location($id, 'id');
    if ($location->isValid()) {
        $_html .= get_gallery($location, 'location', ASSET_OWNERTYPE_LOCATION);
    }

    $view->setFocus('#form_data_name');

    return $_html;
}

/* ************************************************************************** */
function show_search_form($action, $search_data, $URL_PARA)
{
    global $admin;

    return $admin->admin_get_form($action, 0, $search_data, $URL_PARA);
}

/* ************************************************************************** */
function show_csv()
{
    global $admin, $view, $MENU_ARRAY;

    header("Content-Type: text/csv; charset=utf-8");
    header("Content-Disposition: attachment; filename=locations-users-" . date('Ymd_His') . '.csv');
    header("Pragma: no-cache");
    header("Expires: 0");
    // its utf8 content. sizeof doesnt work here! So let the browser do the calculation :)
    // header("Content-Length: ".sizeof($_html));
    Location::echoExportProviders();
    exit;
}

/* ************************************************************************************** */
/* Anfragen auswerten ******************************************************************* */
//URL_PARA neu bestimmen
foreach ($URL_PARA as $key => $val) {
    if (isset($_REQUEST[$key])) {
        $URL_PARA[$key] = $_REQUEST[$key];
    }
}
$action = getParam("action", 'list');
$form_data = getParam("form_data");

switch ($action) {
    case "edit":
        if (!$profile->checkPrivilege('location_admin_edit')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        } else {
            $id = getParam("id");
            if (is_array($form_data)) {
                $action = change_one($action, $id, $form_data, $URL_PARA);
            } else {
                $view->setRef('ContentRight', form_change($action, $id, $URL_PARA));
            }
        }
        break;
    case "delete":
        if (!$profile->checkPrivilege('location_admin_delete')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        } else {
            if (!isset($_REQUEST["confirm_id"])) {
                $id = getParam("id");
                $view->setRef('ContentRight', ask4delete($id, $URL_PARA));
            } else {
                $confirm_id = getParam("confirm_id");
                if (delete_one($confirm_id)) {
                    $view->addMessage('STATUS', 'NOTICE', $app->static_text('the_entry_was_deleted_successfully'));
                } else {
                    $view->addMessage('STATUS', 'ERROR', $app->static_text('the_entry_could_not_be_deleted'));
                }
                $action = 'list';
            }
        }
        break;
    case "search":
        if (!$profile->checkPrivilege('location_admin_search')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        }
        if (isset($_REQUEST["send_button"])) {
            $URL_PARA["page"] = 1;
            $URL_PARA["alpha"] = '';
        }
        $view->setRef('ContentRight', show_search_form($action, $form_data, $URL_PARA));
        if (is_array($form_data)) {
            $view->append('ContentRight', get_list($URL_PARA, $form_data));
        }
        break;
    case "new":
        if (!$profile->checkPrivilege('location_admin_new')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        }
        if (is_array($form_data)) {
            $action = new_one($action, $form_data, $URL_PARA);
        } else {
            $view->setRef('ContentRight', form_new($action, $URL_PARA));
        }
        break;
    case "csv":
        if (!$profile->checkPrivilege('location_admin_csv')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        }
        show_csv();
        exit;
        break;
}

if ($action == 'list') {
    if ($profile->checkPrivilege('location_admin_view')) {
        $view->setRef('ContentRight', get_list($URL_PARA));
    }
}

$view->setHeadTitle('Praxen Admin');
$view->setRef('ContentLeft', $admin->get_navigation());
$view->display('admin/content_left_right.tpl');
