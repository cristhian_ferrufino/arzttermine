<?php

use Arzttermine\Booking\Booking;
use Arzttermine\Integration\Integration;
use Arzttermine\Location\Location;
use Arzttermine\Media\Asset;
use Arzttermine\MedicalSpecialty\MedicalSpecialty;
use Arzttermine\User\User;
use NGS\Managers\BookingsManager;

function get_data_created_by($data = 0)
{
    $_user = new User($data['created_by']);
    if (!$_user->isValid()) {
        return 'n/a';
    }

    return $_user->getFullName();
}

function get_data_created_at($data = 0)
{
    return mysql2date('d.m.Y H:i:s', $data['created_at'], false);
}

function get_data_updated_by($data = 0)
{
    $_user = new User($data['updated_by']);
    if (!$_user->isValid()) {
        return 'n/a';
    }

    return $_user->getFullName();
}

function get_data_status($data = 0)
{
    if (isset($GLOBALS['CONFIG']['LOCATION_STATUS'][$data['status']])) {
        return $GLOBALS['CONFIG']['LOCATION_STATUS'][$data['status']];
    } else {
        return 'n/a';
    }
}

function get_data_country_code($data = 0)
{
    if (isset($GLOBALS['CONFIG']['LOCATION_COUNTRY_CODES'][$data['country_code']])) {
        return $GLOBALS['CONFIG']['LOCATION_COUNTRY_CODES'][$data['country_code']];
    } else {
        return 'n/a';
    }
}

function get_data_name($data = 0)
{
    if ($data['slug'] != '') {
        $_link = $GLOBALS['CONFIG']['URL_HTTP_LIVE'] . '/praxis/' . $data['slug'];
        $_name = $data['name'];

        return '<a href="' . $_link . '" target="_blank">' . $_name . '</a>';
    }

    return $data['name'];
}

function get_data_assets($data = 0)
{
    $_asset = new Asset();
    $_total = $_asset->count('parent_id=0 AND owner_id=' . $data['id'] . ' AND owner_type=' . ASSET_OWNERTYPE_LOCATION);
    if ($_total == 0) {
        return '<span class="red">' . $_total . '</span>';
    } else {
        $_assets = $_asset->findObjects('owner_id=' . $data['id'] . ' AND size="' . ASSET_GFX_SIZE_THUMBNAIL . '" AND owner_type=' . ASSET_OWNERTYPE_LOCATION);
        $_html_assets = '';
        if (!empty($_assets)) {
            $_html_assets .= '<ul class="asset-thumb">';
            /** @var Asset[] $_assets */
            foreach ($_assets as $_asset) {
                $_html_assets .= '<li><img src="' . $_asset->getUrl() . '" title="" /></li>';
            }
            $_html_assets .= '</ul>';
        }
        $_html = '<span class="green">' . $_total . ' <img src="' . getStaticUrl('adm/icon-images.png') . '" style="vertical-align:text-bottom;" class="tooltip" title="' . htmlspecialchars($_html_assets) . '" /></span>';

        return $_html;
    }
}

function get_data_member_ids($data = 0)
{
    $_html = '';
    if ($data['member_ids'] != '') {
        $_ids = explode(',', $data['member_ids']);
        foreach ($_ids as $_id) {
            $_user = new User($_id);
            $_html .= empty($_html) ? '' : '<br />';
            if ($_user->isValid()) {
                $_html .= $_user->getName() . ' (<a href="' . $_user->getAdminEditUrl() . '" target="_blank">edit</a>';
                if ($_user->hasProfile()) {
                    $_html .= ' <a href="' . $_user->getUrl('url') . '" target="_blank"> / view</a>';
                }
                $_html .= ' <a href="/' . $data['integration_id'] . '" target="_blank"> / termine</a>)';
            } else {
                $_html .= '<span class="red">wrong id: ' . $_id . '</span>';
            }
        }

        return $_html;
    }

    return '';
}

function get_form_medical_specialty_ids($data = 0)
{
    $_html = '';
    $_ids = $data['medical_specialty_ids'];
    $_ids = explode(',', $_ids);
    $_medical_specialty = new MedicalSpecialty();
    $_medical_specialties = $_medical_specialty->getMedicalSpecialties();
    $_html_rows = '';
    /** @var MedicalSpecialty[] $_medical_specialties */
    foreach ($_medical_specialties as $_medical_specialty) {
        $_class = '';
        $_class = ($_class == "alternate") ? "" : "alternate";
        $_html_rows .= '<tr class="' . $_class . '"><td width="20"><input type="checkbox" id="form_data_medical_specialty_ids_' . $_medical_specialty->getId() . '" value="1" name="form_data[medical_specialty_ids][' . $_medical_specialty->getId() . ']" ' . (in_array($_medical_specialty->getId(), $_ids) ? " checked" : "") . '></td><td><label for="form_data_medical_specialty_ids_' . $_medical_specialty->getId() . '">' . $_medical_specialty->getName() . '</label></td></tr>';
    }
    $_html .= '<table class="widefat" align="center" width="100%">';
    $_html .= '<tr class="headers"><th colspan="2">Fachrichtungen</th></tr>';
    $_html .= $_html_rows;
    $_html .= "</table>\n";

    return $_html;
}

function get_data_medical_specialty_ids($data = 0)
{
    $_medical_specialty = new MedicalSpecialty();

    return getCategoriesText($data['medical_specialty_ids'], $_medical_specialty->getMedicalSpecialtyValues(), '<br />');
}

function get_form_integration_id($data = 0)
{
    $_integration_id = $data['integration_id'];
    $_html = '<select id="form_data_integration_id" name="form_data[integration_id]">';
    foreach ($GLOBALS['CONFIG']['INTEGRATIONS'] as $_id => $_integrations_data) {
        $_html .= '<option value="' . $_id . '"' . ($_id == $_integration_id ? ' selected="selected"' : '') . '>' . $_integrations_data['NAME'] . ' [' . $_id . ']</option>';
    }
    $_html .= '</select>';

    return $_html;
}

function get_data_integration_id($data = 0)
{
    if (!empty($data['integration_id'])) {
        return Integration::findName($data['integration_id']);
    }

    return '<span class="red">n/a</span>';
}

function get_form_html_title($data = 0)
{
    global $admin;

    $_html = '';
    $_location = new Location($data['id']);
    if ($_location->isValid()) {
        $_html = '<span>' . $_location->getHtmlDefaultTitle() . '</span><br />';
    }
    $_html .= $admin->admin_template('admin/_form_text_counter.tpl', 'html_title', $data['html_title'], null, null, null, null, null);

    return $_html;
}

function get_form_googlepreview($data = 0)
{
    $_location = new Location($data['id']);
    $title = '';
    $description = '';
    $url_base = '';
    
    if ($_location->isValid()) {
        $title = $_location->getHtmlDefaultTitle();
        $description = $_location->getHtmlDefaultMetaDescription();
        $url_base = $_location->getPermalink();
        $url_base = str_replace($_location->getSlug(), '', $url_base);
        $url_base = str_replace('http://', '', $url_base);
    }

    $_html
        = <<<EOF
<div id="googlepreview" class="google-result">
	<div class="title"></div>
	<div class="url"></div>
	<div class="description"></div>
</div>
<script type="text/javascript">
AT._defer(function(){

var input_html_title_default = '$title';
var input_html_meta_description_default = '$description';

function update_googlepreview() {
	var input_html_title = $('#form_data_html_title').val();
	if (input_html_title.length < 1) { input_html_title = input_html_title_default; }
	$('#googlepreview .title').html(input_html_title);

	var input_html_meta_description = $('#form_data_html_meta_description').val();
	if (input_html_meta_description.length < 1) { input_html_meta_description = input_html_meta_description_default; }
	$('#googlepreview .description').html(input_html_meta_description);

	var input_slug = $('#form_data_slug').val();
	if (input_slug.length < 1) { input_slug = ''; }
	$('#googlepreview .url').html('$url_base' + input_slug);
};

$(document).ready(function() {
	update_googlepreview();

	$('#form_data_html_title').keyup(function() {
		update_googlepreview();
		return false;
	});
	$('#form_data_html_meta_description').keyup(function() {
		update_googlepreview();
		return false;
	});
	$('#form_data_slug').keyup(function() {
		update_googlepreview();
		return false;
	});
});
});
</script>
EOF;

    return $_html;
}

function get_open_bookings_count($data = 0)
{
    $bookingsManager = BookingsManager::getInstance();
    $locationId = $data["id"];
    $newBookingsForLocation = $bookingsManager->getBookingsByLocationIdAndStatus($locationId, Booking::STATUS_NEW);
    if (count($newBookingsForLocation) == 0) {
        return count($newBookingsForLocation);
    } else {
        $href = $GLOBALS['CONFIG']['URL_HTTP_LIVE'] . '/admin/bookings/index.php?action=search&open_bookings=1&form_data[location_id_SEARCH_STYLE]=0&form_data[location_id]=' . $locationId;

        return '<a href="' . $href . '" target="_blank">' . count($newBookingsForLocation) . '</a>';
    }
}

function get_data_salesforce_id($data = 0)
{
    return '<a href="https://eu1.salesforce.com/'.$data['salesforce_id'].'" target="_blank">'.$data['salesforce_id'].'</a>';
}

// The ratings elements are number elements, as the 0,5 step system results in accidental in 0 ratings
// ===================================================================================================
function get_rating_average($data = 0)
{
    $rating = isset($data['rating_average']) ? $data['rating_average'] : 0;
    return '<input name="form_data[rating_average]" type="number" max="5.0" min="0.0" step="0.1" value="' . $rating . '"/>';
}

function get_rating_1($data = 0)
{
    $rating = isset($data['rating_1']) ? $data['rating_1'] : 0;
    return '<input name="form_data[rating_1]" type="number" max="5.0" min="0.0" step="0.1" value="' . $rating . '"/>';
}

function get_rating_2($data = 0)
{
    $rating = isset($data['rating_2']) ? $data['rating_2'] : 0;
    return '<input name="form_data[rating_2]" type="number" max="5.0" min="0.0" step="0.1" value="' . $rating . '"/>';
}

function get_rating_3($data = 0)
{
    $rating = isset($data['rating_3']) ? $data['rating_3'] : 0;
    return '<input name="form_data[rating_3]" type="number" max="5.0" min="0.0" step="0.1" value="' . $rating . '"/>';
}

$MENU_ARRAY = array(
    "id"                      => array("sql" => 1, "name" => "ID", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "open_bookings"           => array("sql" => 0, "name" => "Open Bookings", "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_open_bookings_count", "para" => 1), "list_function" => array("name" => "get_open_bookings_count", "para" => 1)),
    "html_title"              => array("sql" => 1, "name" => 'HTML-Title', "show" => 0, "edit" => 1, "new" => 1, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("template" => "admin/_form_text_counter.tpl")),
    "html_meta_description"   => array("sql" => 1, "name" => 'HTML-Meta-Description', "show" => 0, "edit" => 1, "new" => 1, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("template" => "admin/_form_text_counter.tpl")),
    "html_meta_keywords"      => array("sql" => 1, "name" => 'HTML-Meta-Keywords', "show" => 0, "edit" => 1, "new" => 1, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("template" => "admin/_form_text_counter.tpl")),
    "googlepreview"           => array("sql" => 0, "name" => 'GooglePreview', "show" => 0, "edit" => 1, "new" => 1, "search" => 0, "sort" => 0, "nowrap" => 0, "form_input" => array("type" => "function", "name" => "get_form_googlepreview", "para" => 1)),
    "status"                  => array("sql" => 1, "name" => "Status", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "select", "values" => $GLOBALS['CONFIG']['LOCATION_STATUS']), "list_function" => array("name" => "get_data_status", "para" => 1)),
    "integration_id"          => array("sql" => 1, "name" => "Integration", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "function", "name" => "get_form_integration_id", "para" => 1), "list_function" => array("name" => "get_data_integration_id", "para" => 1)),
    "salesforce_id"           => array("sql" => 1, "name" => "Salesforce ID", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_salesforce_id", "para" => 1)),
    "position_factor"         => array("sql" => 1, "name" => "PFaktor", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "name"                    => array("sql" => 1, "name" => "Name", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_name", "para" => 1)),
    "slug"                    => array("sql" => 1, "name" => "Permalink", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "street"                  => array("sql" => 1, "name" => "Straße", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "city"                    => array("sql" => 1, "name" => "Ort", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "zip"                     => array("sql" => 1, "name" => "PLZ", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "country_code"            => array("sql" => 1, "name" => "Land", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "form_input" => array("type" => "select", "values" => $GLOBALS['CONFIG']['LOCATION_COUNTRY_CODES']), "list_function" => array("name" => "get_data_country_code", "para" => 1)),
    "lat"                     => array("sql" => 1, "name" => "lat", "show" => 0, "edit" => 1, "new" => 0, "search" => 1, "sort" => 0, "nowrap" => 1, "form_input" => array("type" => "none")),
    "lng"                     => array("sql" => 1, "name" => "lng", "show" => 0, "edit" => 1, "new" => 0, "search" => 1, "sort" => 0, "nowrap" => 1, "form_input" => array("type" => "none")),
    "email"                   => array("sql" => 1, "name" => "E-Mail", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "www"                     => array("sql" => 1, "name" => "www", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    #	"profile_asset_id"	=> array("sql"=>1,"name" => "profile_asset_id", "show"=>0, "edit"=>0, "new"=>0, "search"=>1, "sort"=>1,"nowrap"=>1),
    "phone"                   => array("sql" => 1, "name" => "Telefon [vorwahl / nummer]", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "fax"                     => array("sql" => 1, "name" => "Fax", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "phone_visible"           => array("sql" => 1, "name" => "Telefon (sichtbar) [vorwahl / nummer]", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "fax_visible"             => array("sql" => 1, "name" => "Fax (sichtbar)", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "member_ids"              => array("sql" => 1, "name" => "Ärzte", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 0, "nowrap" => 1, "list_function" => array("name" => "get_data_member_ids", "para" => 1)),
    "primary_member_id"       => array("sql" => 1, "name" => "erste Arzt", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 0, "nowrap" => 1),
    "photos"                  => array("sql" => 0, "name" => "Fotos", "show" => 1, "edit" => 0, "new" => 0, "search" => 0, "sort" => 0, "nowrap" => 1, "list_function" => array("name" => "get_data_assets", "para" => 1)),
    "newsletter_subscription" => array("sql" => 1, "name" => "Newsletter", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "checkbox", "values" => array(1 => '&nbsp;'))),
    "rating_average"          => array("sql" => 1, "name" => "Rating &#216;", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "function", "name" => "get_rating_average", "para" => 1)),
    "rating_1"                => array("sql" => 1, "name" => "Rating 1", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "function", "name" => "get_rating_1", "para" => 1)),
    "rating_2"                => array("sql" => 1, "name" => "Rating 2", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "function", "name" => "get_rating_2", "para" => 1)),
    "rating_3"                => array("sql" => 1, "name" => "Rating 3", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "function", "name" => "get_rating_3", "para" => 1)),
    "medical_specialty_ids"   => array("sql" => 1, "name" => "Fachrichtungen", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_medical_specialty_ids", "para" => 1), "form_input" => array("type" => "function", "name" => "get_form_medical_specialty_ids", "para" => 1)),
    "info_1"                  => array("sql" => 1, "name" => "Beschreibung", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => ' style="width:300px;height:60px;"')),
    "info_seo"                => array("sql" => 1, "name" => "SEO-TEXT", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => ' style="width:300px;height:60px;"')),
    "comment_intern"          => array("sql" => 1, "name" => 'Kommentar <span style="color:#ff0000;">(intern)', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => ' style="width:300px;height:60px;"')),
    "created_at"              => array("sql" => 1, "name" => "Erstellungsdatum", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "none")),
    "created_by"              => array("sql" => 1, "name" => "Erstellt von", "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_created_by", "para" => 1), "list_function" => array("name" => "get_data_created_by", "para" => 1)),
    "updated_at"              => array("sql" => 1, "name" => "Letztes Update", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "updated_by"              => array("sql" => 1, "name" => "Letzter Bearbeiter", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_updated_by", "para" => 1), "list_function" => array("name" => "get_data_updated_by", "para" => 1)),
);

$BUTTON = array(
    "new"    => array("func" => "location_admin_new", "alt_text" => $app->_('Neu'), "script" => $_SERVER['PHP_SELF'], "gfx" => "document_new.png", "action" => "action=new", "th" => 1, "td" => 0),
    "edit"   => array("func" => "location_admin_edit", "alt_text" => $app->_('Bearbeiten'), "script" => $_SERVER['PHP_SELF'], "gfx" => "document_edit.png", "action" => "action=edit", "th" => 0, "td" => 1),
    "delete" => array("func" => "location_admin_delete", "alt_text" => $app->_('Löschen'), "script" => $_SERVER['PHP_SELF'], "gfx" => "delete.png", "action" => "action=delete", "th" => 0, "td" => 1),
    "search" => array("func" => "location_admin_search", "alt_text" => $app->_('Suchen'), "script" => $_SERVER['PHP_SELF'], "gfx" => "search.png", "action" => "action=search", "th" => 1, "td" => 0),
    "csv"    => array("func" => "location_admin_csv", "alt_text" => 'Liste als CSV herunterladen', "script" => $_SERVER['PHP_SELF'], "gfx" => "download.gif", "action" => "action=csv", "th" => 1, "td" => 0),
);
$TABLE_CONFIG = array(
    "table_name"  => DB_TABLENAME_LOCATIONS,
    "alpha_index" => "name",
);
$URL_PARA = array(
    'page'         => 1,
    'alpha'        => '',
    "sort"         => 0,
    "direction"    => 'DESC',
    "num_per_page" => 50,
    "type"         => 'num'
);
