<?php
/**
 * Admin: Users
 *
 * @author Axel Kuzmik <axel@kuzmik.de>
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/../include/bootstrap.php';
require_once "config.php";

/* ************************************************************************************* */

use Arzttermine\Admin\Admin;
use Arzttermine\Application\Application;
use Arzttermine\Mail\Mailing;
use NGS\Managers\TempUsersDataManager;
use NGS\Managers\UsersManager;

$admin = new Admin();
$app = Application::getInstance();
$view = $app->getView();
$profile = $app->getCurrentUser();

$view->disableCaching();

/* ************************************************************************************* */

$tempUsersDataManager = TempUsersDataManager::getInstance();

if (!$profile->checkPrivilege('user_admin')) {
    $view->setRef('ContentRight', $app->static_file('access_denied.html'));
    $view->setRef('ContentLeft', $admin->get_navigation());
    $view->display('admin/content_left_right.tpl');
    exit;
}
/* ************************************************************************************* */
function get_list($URL_PARA, $search = 0)
{
    global $admin;

    $_html = $admin->admin_get_list(0, $search, $URL_PARA);

    return $_html;
}

/* ************************************************************************************* */
function change_one($action, $id, $form_data, $URL_PARA)
{
    global $admin, $view, $app;

    if (1 > mb_strlen(sanitize_title($form_data["last_name"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
        $view->addMessage('admin_message_last_name', 'ERROR', $app->static_text('Bitte geben Sie einen Nachnamen an'));
    }

    // if there are checkboxes than set the keys to make sure that deselected checkboxes are beeing set
    $form_data = $admin->set_unselected_checkbox_values($form_data);

    $_medical_specialty_ids = '';
    if (isset($form_data['medical_specialty_ids']) && is_array($form_data['medical_specialty_ids'])) {
        $_medical_specialty_ids = implode(',', array_keys($form_data['medical_specialty_ids']));
    }

    $form_data['medical_specialty_ids'] = $_medical_specialty_ids;
    $locationInfo = array(
        'location_name' => $form_data['location_name'], 'location_street' => $form_data['location_street'],
        'location_zip'  => $form_data['location_zip'], 'location_city' => $form_data['location_city']
    );

    //shows error if one or more location fields are empty
    //doesn't show error if all location fields are empty
    if (count(array_filter($locationInfo)) > 0) {
        foreach ($locationInfo as $fieldName => $input) {
            if (empty($input)) {
                $view->addMessage('admin_message_' . $fieldName, 'ERROR', $app->static_text('All location fields are mandatory'));
            }
        }
    }
    if (!isset($form_data['medical_specialty_ids']) || !isset($form_data['treatment_type_ids'])) {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('Please input at least 1 medical specialty and 1 treatment type'));
        $view->setRef('ContentRight', form_change($action, $id, $URL_PARA, $form_data));

        return $action;
    } elseif ($view->countMessages()) {
        $view->addMessage('STATUS', 'ERROR', $app->_('Bitte prüfen Sie Ihre Eingaben.'));
        $view->setRef('ContentRight', form_change($action, $id, $URL_PARA, $form_data));

        return $action;
    }
    // 4. sanitize
    $form_data["title"] = sanitize_title($form_data["title"]);
    $form_data["first_name"] = sanitize_title($form_data["first_name"]);
    $form_data["last_name"] = sanitize_title($form_data["last_name"]);
    $form_data["location_name"] = sanitize_title($form_data["location_name"]);
    $form_data["location_street"] = sanitize_title($form_data["location_street"]);
    $form_data["location_zip"] = sanitize_title($form_data["location_zip"]);
    $form_data["location_city"] = sanitize_title($form_data["location_city"]);

    $_result = $admin->admin_set_update($id, $form_data);
    if ($_result == 1) {
        $view->addMessage('STATUS', 'NOTICE', $app->static_text('action_was_successfully'));
        $action = 'list';
    } elseif ($_result == -1) {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('the_entry_allready_exists'));
        $view->setRef('ContentRight', form_change($action, $id, $URL_PARA, $form_data));
        $action = 'list';
    } elseif ($_result == -2) {
        $view->addMessage('STATUS', 'ERROR', $app->static_text('name_of_primary_key_is_empty'));
        $view->setRef('ContentRight', form_change($action, $id, $URL_PARA, $form_data));
        $action = 'list';
    } else {
        $view->addMessage('STATUS', 'WARNING', $app->static_text('no_data_has_been_updated'));
        $view->setRef('ContentRight', form_change($action, $id, $URL_PARA, $form_data));
        $action = 'list';
    }

    return $action;
}

/* ************************************************************************************* */
function ask4delete($id, $URL_PARA)
{
    global $admin;

    $_html = $admin->admin_get_confirm_table($id, $URL_PARA, 'delete');

    return $_html;
}

/* ************************************************************************************* */
function ask4activate($id, $URL_PARA)
{
    global $admin;

    $_html = $admin->admin_get_confirm_table($id, $URL_PARA, 'activate');

    return $_html;
}

/* ************************************************************************************* */
function form_change($action, $id, $URL_PARA, $error_data = 0)
{
    global $admin, $view;

    $_html = $admin->admin_get_form($action, $id, $error_data, $URL_PARA);

    $view->setFocus('#form_data_email');

    return $_html;
}

/* ************************************************************************** */
function show_search_form($action, $search_data, $URL_PARA)
{
    global $admin;

    return $admin->admin_get_form($action, 0, $search_data, $URL_PARA);
}

/**
 * @param \NGS\DAL\DTO\UsersDto $userDto
 */
function sendActivationEmail($userDto)
{
    $templateFilename = '/mailing/doctors/doctor-activation.txt';
    $_mailing = new Mailing();
    $_mailing->type = MAILING_TYPE_TEXT;
    $_mailing->content_filename = $templateFilename;
    $_mailing->data['domain'] = $GLOBALS['CONFIG']['DOMAIN'];

    // $_mailing->data['url'] = $GLOBALS['protocol'].$_SERVER['HTTP_HOST'].'/'.$GLOBALS['CONFIG']['DOCTORS_LOGIN_URL'].'/do_aktivieren?code='.$activation_code;

    // NM 26.02.2014.
    // we send reset_password because we don't require the doc to provide password on account registration
    // the account will be activated when the doc resets the password for the first time (actually, it's not resetting, but setting the password)
    $resetPasswordCode = substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 10);
    $usersManager = UsersManager::getInstance();
    $usersManager->setResetPasswordCode($userDto->getId(), $resetPasswordCode);
    $_mailing->data['url'] = $GLOBALS['protocol'] . $_SERVER['HTTP_HOST'] . '/' . $GLOBALS['CONFIG']['DOCTORS_LOGIN_URL'] . '/passwort_zuruecksetzen/' . $resetPasswordCode;

    $_mailing->data['doctor_name'] = $userDto->getTitle() . " " . $userDto->getFirstName() . " " . $userDto->getLastName();
    $_mailing->addAddressTo($userDto->getEmail());
    $_mailing->send();
}

/* ************************************************************************************** */
/* Anfragen auswerten ******************************************************************* */
//URL_PARA neu bestimmen
foreach ($URL_PARA as $key => $val) {
    if (isset($_REQUEST[$key])) {
        $URL_PARA[$key] = $_REQUEST[$key];
    }
}
$action = getParam("action", 'list');
$form_data = getParam("form_data");

switch ($action) {
    case "edit":
        if (!$profile->checkPrivilege('user_admin_edit')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        } else {
            $id = getParam("id");
            if (is_array($form_data)) {
                $action = change_one($action, $id, $form_data, $URL_PARA);
            } else {
                $view->setRef('ContentRight', form_change($action, $id, $URL_PARA));
            }
        }
        break;
    case "delete":
        if (!$profile->checkPrivilege('user_admin_delete')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        } else {
            if (!isset($_REQUEST["confirm_id"])) {
                $id = getParam("id");
                $view->setRef('ContentRight', ask4delete($id, $URL_PARA));
            } else {
                $idToBeDeleted = getParam("confirm_id");
                if (isset($idToBeDeleted)) {
                    $result = $tempUsersDataManager->deleteById($idToBeDeleted);
                    if ($result == -1) {
                        $view->addMessage('STATUS', 'ERROR', $app->static_text('the_entry_could_not_be_deleted'));
                    } else {
                        $view->addMessage('STATUS', 'NOTICE', $app->static_text('the_entry_was_deleted_successfully'));
                    }
                }
                $action = 'list';
            }
        }
        break;
    case "search":
        if (getParam('view') == 'employees_list' && $profile->checkPrivilege('user_admin_employees')) {
            show_list_employees($form_data, $URL_PARA);
            exit;
        }
        if (getParam('view') == 'employees_list_gfx' && $profile->checkPrivilege('user_admin_employees')) {
            show_list_employees_gfx($form_data, $URL_PARA);
            exit;
        }
        if (!$profile->checkPrivilege('user_admin_search')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        }
        if (isset($_REQUEST["send_button"])) {
            $URL_PARA["page"] = 1;
            $URL_PARA["alpha"] = '';
        }
        $view->setRef('ContentRight', show_search_form($action, $form_data, $URL_PARA));
        if (is_array($form_data)) {
            $view->append('ContentRight', get_list($URL_PARA, $form_data));
        }
        break;
    case 'activate':
        if (!$profile->checkPrivilege('user_admin_activate')) {
            $view->addMessage('STATUS', 'ERROR', $app->static_text('access_denied'));
            $action = 'list';
            continue;
        } else {
            if (!isset($_REQUEST["confirm_id"])) {
                $id = getParam("id");
                $view->setRef('ContentRight', ask4activate($id, $URL_PARA));
            } else {
                $idToBeActivated = getParam("confirm_id");
                $newUser = $tempUsersDataManager->activateById($idToBeActivated);
                sendActivationEmail($newUser);
                $view->addMessage('STATUS', 'NOTICE', $app->static_text('the_entry_was_activated_successfully'));
                $action = 'list';
            }
        }
        break;
}

if ($action == 'list') {
    if ($profile->checkPrivilege('user_admin_view')) {
        $view->setRef('ContentRight', get_list($URL_PARA));
    }
}

$view->setHeadTitle('Users Admin');
$view->setRef('ContentLeft', $admin->get_navigation());
$view->display('admin/content_left_right.tpl');
