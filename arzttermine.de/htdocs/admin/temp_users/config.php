<?php

use Arzttermine\MedicalSpecialty\MedicalSpecialty;
use Arzttermine\User\User;
use NGS\Managers\MedicalSpecialtiesManager;
use NGS\Managers\TempUsersDataManager;
use NGS\Managers\TreatmentTypesManager;

function get_first_name($data = 0)
{
    $_user = new User($data['id']);

    return '<a href="' . $_user->url_profile_start . '" target="_blank">' . $_user->getFirstName() . '</a>';
}

function get_data_gender($data = 0)
{
    if (isset($GLOBALS['CONFIG']['ARRAY_GENDER'][$data['gender']])) {
        return $GLOBALS['CONFIG']['ARRAY_GENDER'][$data['gender']];
    };

    return 'n/a';
}

function get_form_medical_specialty_ids($data = 0)
{
    $_html = '';
    $_ids = $data['medical_specialty_ids'];
    $_ids = explode(',', $_ids);
    $_medical_specialty = new MedicalSpecialty();
    $_medical_specialties = $_medical_specialty->getMedicalSpecialties();
    $_html_rows = '';
    /** @var MedicalSpecialty[] $_medical_specialties */
    foreach ($_medical_specialties as $_medical_specialty) {
        $_class = '';
        $_class = ($_class == "alternate") ? "" : "alternate";
        $_html_rows .= '<tr class="' . $_class . '"><td width="20"><input type="checkbox" onchange="addOrRemoveTreatmentTypes(' . $_medical_specialty->getId() . ')" id="form_data_medical_specialty_ids_' . $_medical_specialty->getId() . '" value="1" name="form_data[medical_specialty_ids][' . $_medical_specialty->getId() . ']" ' . (in_array($_medical_specialty->getId(), $_ids) ? " checked" : "") . '></td><td><label for="form_data_medical_specialty_ids_' . $_medical_specialty->getId() . '">' . $_medical_specialty->getName() . '</label></td></tr>';
    }
    $_html .= '<table class="widefat" align="center" width="100%">';
    $_html .= '<tr class="headers"><th colspan="2">Fachrichtungen</th></tr>';
    $_html .= $_html_rows;
    $_html .= "</table>\n";

    return $_html;
}

function get_data_medical_specialty_ids($data = 0)
{
    $_medical_specialty = new MedicalSpecialty();

    return getCategoriesText($data['medical_specialty_ids'], $_medical_specialty->getMedicalSpecialtyValues(), '<br />');
}

function get_form_treatment_types($data = 0)
{
    $medicalSpecialtiesManager = MedicalSpecialtiesManager::getInstance();
    $tempUsersDataManager = TempUsersDataManager::getInstance();
    $treatmentTypesManager = TreatmentTypesManager::getInstance();

    $_html = '';
    $temp_user_id = $data['id'];
    $tempUser = $tempUsersDataManager->getById($temp_user_id);
    $existingTreatmentTypes = $treatmentTypesManager->getByIds(explode(',', $tempUser->getTreatmentTypeIds()));
    $medSpecIdsArrayWithExistingTreatmentTypes = array();
    /** @var \NGS\DAL\DTO\TreatmentTypesDto[] $existingTreatmentTypes */
    foreach ($existingTreatmentTypes as $treatmentType) {
        $medSpecIdsArrayWithExistingTreatmentTypes[$treatmentType->getMedicalSpecialtyId()][] = $treatmentType->getId();
    }
    $_ids = $data['medical_specialty_ids'];
    $_ids = explode(',', $_ids);
    /**
     * @todo implement a function to get medical specialities by ids array.
     */
    foreach ($_ids as $id) {
        $medicalSpecialties[] = $medicalSpecialtiesManager->getMedicalSpecialtyWithTreatmentTypesById($id);
    }

    $allMedicalSpecialties = $medicalSpecialtiesManager->getAllMedicalSpecialtiesWithTreatmentTypes();
    foreach ($allMedicalSpecialties as $medicalSpecialty) {
        $medicalSpecialtiesJson[$medicalSpecialty->getId()] = $medicalSpecialty;
    }
    $allMedicalSpecialtiesWithTreatmentTypes = array();
    foreach ($allMedicalSpecialties as $medicalSpecialty) {
        $allMedicalSpecialtiesWithTreatmentTypes[$medicalSpecialty->getId()] = $medicalSpecialty->getTreatmentTypesDtos();
    }
    $_html_rows = '';
    /** @var \NGS\DAL\DTO\MedicalSpecialtiesDto[] $medicalSpecialties */
    foreach ($medicalSpecialties as $medicalSpecialty) {
        $_class = '';
        $_class = ($_class == "alternate") ? "" : "alternate";
        $_html_rows .= '<tr class="' . $_class . '" id="treatment_types_for_medical_specialty_' . $medicalSpecialty->getId() . '"><td width="20"><div>';
        $_html_rows .= '<h3>' . $medicalSpecialty->getNameSingularDe() . '</h3>';
        $treatmentTypes = $medicalSpecialty->getTreatmentTypesDtos();
        $_html_rows .= '<ul>';
        foreach ($treatmentTypes as $treatmentType) {
            $checked = (
            isset($medSpecIdsArrayWithExistingTreatmentTypes[$medicalSpecialty->getId()])
            &&
            in_array($treatmentType->getId(), $medSpecIdsArrayWithExistingTreatmentTypes[$medicalSpecialty->getId()])
                ?
                " checked"
                :
                ""
            );
            $_html_rows .= '<li style="list-style: none;"><input type="checkbox" id="form_data_treatment_type_ids_' . $treatmentType->getId() . '" value="' . $medicalSpecialty->getId() . '" name="form_data[treatment_type_ids][' . $treatmentType->getId() . ']" ' . $checked . ' ><span>' . $treatmentType->getNameDe() . '</span></li>';
        }
        $_html_rows .= '</ul></div></td></tr>';
    }
    $_html .= '<table class="widefat" align="center" width="100%" id="treatment_types">';
    $_html .= '<tr class="headers"><th colspan="2">Treatment Types</th></tr>';
    $_html .= $_html_rows;
    $_html .= "</table>\n";
    $_html
        .= '<script>                         
                var allMedicalSpecialtiesWithTreatmentTypes=' . json_encode($allMedicalSpecialtiesWithTreatmentTypes) . ';
                var medicalSpecialtiesJson=' . json_encode($medicalSpecialtiesJson) . ';
                var medSpecIdsArrayWithExistingTreatmentTypes=' . json_encode($medSpecIdsArrayWithExistingTreatmentTypes) . ';
                function addOrRemoveTreatmentTypes(medicalSpecId){
                    var element=document.getElementById("form_data_medical_specialty_ids_"+medicalSpecId);
                   
                    if (typeof(element) != "undefined" && element != null){
                            if(element.checked){
                                var tr=createTreatmentTypesSectionByMedicalSpecialtyId(medicalSpecId);
                                var treatmentTypes=document.getElementById("treatment_types");
                                var newRow=treatmentTypes.insertRow(-1);
                                newRow.setAttribute("id","treatment_types_for_medical_specialty_"+medicalSpecId);
                                newRow.setAttribute("class","alternate");
                                newRow.innerHTML=tr;
                                
                            }else{
                                var row=document.getElementById("treatment_types_for_medical_specialty_"+medicalSpecId);
                                row.parentNode.removeChild(row);
                            }
                        }
                    }   
                
                
                function createTreatmentTypesSectionByMedicalSpecialtyId(id){
                    var string= "<td width=\"20\"><div><h3>"+medicalSpecialtiesJson[id]["nameSingularDe"]+"</h3><ul>";
                    for (var i=0;i<allMedicalSpecialtiesWithTreatmentTypes[id].length;i++){
                        string+="<li style=\"list-style: none;\"><input type=\"checkbox\" name=\"form_data[treatment_type_ids][";
                        string+=allMedicalSpecialtiesWithTreatmentTypes[id][i]["id"];
                        string+="]\" value="+id+" id=\"form_data_treatment_type_ids_";
                        string+=allMedicalSpecialtiesWithTreatmentTypes[id][i]["id"];
                        string+="\"";
                        
                        if(typeof(medSpecIdsArrayWithExistingTreatmentTypes[id])!="undefined" && medSpecIdsArrayWithExistingTreatmentTypes[id].indexOf(allMedicalSpecialtiesWithTreatmentTypes[id][i]["id"])!=-1){
                            string+="checked=\"checked\"";
                        }
                        string+="><span>";
                        string+=allMedicalSpecialtiesWithTreatmentTypes[id][i]["nameDe"];
                        string+="</span></li>";
                    }
                    string+="</ul></div></td>";
                    return string;
                }
               
              </script>';

    return $_html;
}

function get_data_last_name($data = 0)
{
    if (isset($data['slug']) && $data['slug'] != '') {
        $_link = $GLOBALS['CONFIG']['URL_HTTP_LIVE'] . '/' . $data['slug'];
        $_name = $data['last_name'];

        return '<a href="' . $_link . '" target="_blank">' . $_name . '</a>';
    }

    return $data['last_name'];
}

$MENU_ARRAY = array(
    "id"                    => array("sql" => 1, "name" => "ID", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "title"                 => array("sql" => 1, "name" => 'Titel', "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "first_name"            => array("sql" => 1, "name" => $app->static_text('first_name'), "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "last_name"             => array("sql" => 1, "name" => $app->static_text('last_name'), "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_last_name", "para" => 1)),
    "gender"                => array("sql" => 1, "name" => "Geschlecht", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_gender", "para" => 1), "form_input" => array("type" => "select", "values" => $GLOBALS['CONFIG']['ARRAY_GENDER'])),
    "email"                 => array("sql" => 1, "name" => $app->static_text('email'), "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
    "medical_specialty_ids" => array("sql" => 1, "name" => "Fachrichtungen", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_medical_specialty_ids", "para" => 1), "form_input" => array("type" => "function", "name" => "get_form_medical_specialty_ids", "para" => 1)),
    "treatment_types"       => array("sql" => 1, "name" => "Treatment Types", "show" => 0, "edit" => 1, "new" => 1, "search" => 0, "sort" => 0, "nowrap" => 1, "list_function" => array("name" => "", "para" => 1), "form_input" => array("type" => "function", "name" => "get_form_treatment_types", "para" => 1)),
    "location_name"         => array("sql" => 1, "name" => 'Praxis', "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "location_street"       => array("sql" => 1, "name" => 'Street', "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "location_zip"          => array("sql" => 1, "name" => 'Post Code', "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
    "location_city"         => array("sql" => 1, "name" => 'City', "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
);
$BUTTON = array(
    //"new"			=> array("func"=>"user_admin_new","alt_text"=>$app->_('Neu'),"script"=>$_SERVER['PHP_SELF'],"gfx"=>"document_new.png","action"=>"action=new","th"=>1,"td"=>0),
    "edit"     => array("func" => "user_admin_edit", "alt_text" => $app->_('Bearbeiten'), "script" => $_SERVER['PHP_SELF'], "gfx" => "document_edit.png", "action" => "action=edit", "th" => 0, "td" => 1),
    "activate" => array("func" => "user_admin_activate", "alt_text" => $app->_('Activate'), "script" => $_SERVER['PHP_SELF'], "gfx" => "version_activate.png", "action" => "action=activate", "th" => 0, "td" => 1),
    "delete"   => array("func" => "user_admin_delete", "alt_text" => $app->_('Löschen'), "script" => $_SERVER['PHP_SELF'], "gfx" => "delete.png", "action" => "action=delete", "th" => 0, "td" => 1),
    "search"   => array("func" => "user_admin_search", "alt_text" => $app->_('Suchen'), "script" => $_SERVER['PHP_SELF'], "gfx" => "search.png", "action" => "action=search", "th" => 1, "td" => 0),
);
$TABLE_CONFIG = array(
    "table_name" => DB_TABLENAME_TEMP_USERS_DATA,
);
$URL_PARA = array(
    "page"         => 1,
    "alpha"        => '',
    "sort"         => 'id',
    "direction"    => 'DESC',
    "num_per_page" => 50,
    "type"         => "num"
);