<?php 
// get this from arzttermine.de config.php
$CONFIG_REFERRALS = array(
	"REFERRAL_PRICE_TYPES" => array(
	    "RXQTy9x0" => array(
	        "company" => "Amazon",
	        "price" => "15 Euro",
	        "image" => "amazon_coupon_15.png"
	    ),
	    "d8rGcgBa" => array(
	        "company" => "Amazon",
	        "price" => "20 Euro",
	        "image" => "amazon_coupon_20.png"
	    ),
	    "RVB3Pb4L" => array(
	        "company" => "Amazon",
	        "price" => "25 Euro",
	        "image" => "amazon_coupon_25.png"
	    ),
	    "0o1YHZ3L" => array(
	        "company" => "iTunes",
	        "price" => "15 Euro",
	        "image" => "itunes_coupon_15.png"
	    ),
	    "rL0pudHz" => array(
	        "company" => "iTunes",
	        "price" => "25 Euro",
	        "image" => "itunes_coupon_25.png"
	    ),
	    "NjGXD7f9" => array(
	        "company" => "Spotify",
	        "price" => "1 Monat",
	        "image" => "spotify_coupon_1.png"
	    ),
	    "PZ5eaRSB" => array(
	        "company" => "Spotify",
	        "price" => "2 Monat",
	        "image" => "spotify_coupon_2.png"
	    )
	)
)
?>