<?php /* INCLUDE SPACIFIC FILES */
include_once("../data/config.php");
$priceType = $_REQUEST["type"];
$priceInfo = $CONFIG_REFERRALS["REFERRAL_PRICE_TYPES"][$priceType];
if(isset($priceInfo)){
    $ref_company = $priceInfo["company"];
    $ref_price = $priceInfo["price"];
    $ref_image = $priceInfo["image"]; 
}

include_once("../../layout/header.html");
/* get Coupon-Code */
$codeParm =  preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '-', $_GET['rc']);
$additionalLI = '<li><a id="guidlines-link" target="_blank" href="http://www.arzttermine.de/facebook-empfehlung-teilnahmebedingungen">Teilnahmebedingungen</a></loi>';
?>
<meta name="description" content="Arzttermine online buchen. Lade jetzt Deine Freunde ein." />
<title>Arzttermine online - Lade jetzt Deine Freunde ein.</title>

<link rel="stylesheet" href="../data/css/recommand.css" media="all" />

<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-27059894-1']);
  _gaq.push(['_gat._anonymizeIp']);
  _gaq.push(['_trackPageview']);
  _gaq.push(['_setSiteSpeedSampleRate', 100]);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</head>
<body class="landingpage specialpage patientenpage">
	<div id="topContainer">
		<div class="middle-responsiv">
			<a id="logo" href="http://www.arzttermine.de" title="Arzttermine.de | Schnell,einfach und online zu Ihrem Wunschtermin."><img src="../data/img/logo-arzttermine.png" alt="Arzttermine.de Logo" width="175"></a>
			<p id="hotline">kostenfreie Hotline: <span class="tel at_grbl">0800 / 2222 133</span></p>
		</div>
	</div>	
	<div class="texture bigBox">
		<div class="middle subdiv">
			<h1 style="font-family: tahoma, helvetica, arial; text-align: center; color: #5294a3;">Gesund sein lohnt sich wieder</h1>
			<div class="imageBox">
				<img src="../data/img/single_polaroid.png" alt="Freunde" style="margin-left: 4%;" />
					<img src="../data/img/arrow.png" alt="Ergibt" style="margin-left: -35px; margin-right: 15px;" />
					<img src="../data/img/price/<?php echo$ref_image ?>" alt="<?php echo$ref_company ?> Gutschein" />
					<div id="loadingArea" style="display: none;"><img class="loadingImage" src="http://magazin.arzttermine.de/wp-content/themes/Backstreet_new/images/loader_black.gif" alt="Ladeanimation">...Seite wird geladen</div>
					<div class="at_widget">
						<input id="at_widget-input" type="text" name="city" value="" placeholder="Ort / PLZ" required autocomplete autofocus>
						<a href="http://www.arzttermine.de/suche?a=search&form%5Bmedical_specialty_id%5D=1&rc=<?php echo $codeParm; ?>&form%5Binsurance_id%5D=2&form%5Blocation%5D=" value="" class="btn at_widget-submit">Kostenfrei Termin finden</a>
					</div>					
			</div>			
		</div>
	</div>
	<div class="middle-responsiv step_area" style="min-height: 190px;">
		<div id="step_1" class="step_box">
			<h3 class="at_grbl"><span class="step_1 step-image"></span>Arzt finden</h3>
			<p>Finden Sie jetzt auf Arzttermine.de Ihren nächsten Arzttermin bequem und kostenfrei!</p>
		</div>
		<div class="arrow arrow_1"></div>
		<div id="step_2" class="step_box">
			<h3 class="at_grbl"><span class="step_2 step-image"></span>Termin wahrnehmen</h3>
			<p>Nehmen Sie Ihren Termin wahr und profitieren von unseren kompetenten Ärzten!</p>
		</div>
		<div class="arrow arrow_2"></div>
		<div id="step_3" class="step_box">
			<h3 class="at_grbl"><span class="step_3 step-image"></span>Prämie bekommen</h3>
			<?php if($ref_company !='Spotify') { ?>
			<p>Nach dem Arztbesuch können Sie sich die Prämie sichern. Wir senden Ihnen einen Gutschein im Wert von <b><?php echo$ref_price; ?></b>, mit dem Sie bei <?php echo$ref_company; ?> shoppen gehen können.</p>
			<?php } else { ?>
				<p>Nach dem Arztbesuch können Sie sich die Prämie sichern und <b><?php echo$ref_price; ?> <?php echo$ref_company; ?> Premium nutzen.		
			<?php } ?>
		</div>		
	</div>
	<div id="terms_cond" class="middle-responsiv">
		<?php include_once("../data/direct_terms-conditions.html"); ?>
	</div>
	<?php include_once("../../layout/footer.html");   ?>
	<script type="text/javascript">
		$(document).ready(function () {
			$('#guidlines-link').on("click", function(e){
				e.preventDefault();
				$('#terms_cond').fadeToggle(800);
			});
		});
	</script>
	<script type='text/javascript'>
		$(document).ready(function () {
			$('.at_widget .at_widget-submit').on('click',function(e) { e.preventDefault(); atWidget($(this)); });
			$('.at_widget #at_widget-input').keypress(function(e) { if(event.which == 13){ e.preventDefault(); atWidget($('.at_widget .at_widget-submit'));} });
			
			function atWidget(test){									
				$("#loadingArea").fadeIn(300);
				var searchValue	= $('.at_widget #at_widget-input').val();
				var originHref	= test.attr('href');											
				if(searchValue != ''){
					$('.loadingImage').fadeIn(50);
					location.href	= originHref+searchValue;							
				}else {
					$('.at_widget #at_widget-input').addClass('error');
				}						
			}
		});					
	</script>
</body>
</html>