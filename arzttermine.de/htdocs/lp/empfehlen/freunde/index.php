<?php /* INCLUDE SPACIFIC FILES */
include_once("../data/config.php");
$priceType = $_REQUEST["type"];
$priceInfo = $CONFIG_REFERRALS["REFERRAL_PRICE_TYPES"][$priceType];
if(isset($priceInfo)){
    $ref_company = $priceInfo["company"];
    $ref_price = $priceInfo["price"];
    $ref_image = $priceInfo["image"]; 
}

include_once("../../layout/header.html");
$additionalLI = '<li><a id="guidlines-link" target="_blank" href="http://www.arzttermine.de/facebook-empfehlung-teilnahmebedingungen">Teilnahmebedingungen</a></loi>';
?>
<meta name="description" content="Arzttermine online buchen. Lade jetzt Deine Freunde ein." />
<title>Arzttermine online - Lade jetzt Deine Freunde ein.</title>

<link rel="stylesheet" href="../data/css/recommand.css" media="all" />

</head>
<body class="landingpage specialpage patientenpage">
	<div id="topContainer">
		<div class="middle-responsiv">
			<a id="logo" href="http://www.arzttermine.de" title="Arzttermine.de | Schnell,einfach und online zu Ihrem Wunschtermin."><img src="../data/img/logo-arzttermine.png" alt="Arzttermine.de Logo" width="175"></a>
			<p id="hotline">kostenfreie Hotline: <span class="tel at_grbl">0800 / 2222 133</span></p>
		</div>
	</div>	
	<div class="texture bigBox">
		<div class="middle subdiv">
			<h1 style="font-family: tahoma, helvetica, arial; text-align: center; color: #5294a3;">Lade jetzt Deine Freunde ein.</h1>
			<div class="imageBox">
				<img src="../data/img/couple.png" alt="Freunde" />
				<img src="../data/img/result.png" alt="Ergibt" />
				<img src="../data/img/price/<?php echo$ref_image ?>" alt="<?php echo$ref_company ?> Gutschein" />
			</div>
		</div>
	</div>
	<div class="middle-responsiv step_area" style="min-height: 175px;">
		<div id="step_1" class="step_box">
			<h3 class="at_grbl"><span class="step_1 step-image"></span>Service Freunden empfehlen</h3>
			<p>Erzähle deinen Freunden von Arzttermine.de und empfehlen Ihnen, dass auch sie bei Arzttermine.de ihren Termin buchen.</p>
		</div>
		<div class="arrow arrow_1"></div>
		<div id="step_2" class="step_box">
			<h3 class="at_grbl"><span class="step_2 step-image"></span>Arzt finden und Termin buchen</h3>
			<p>Deine Freunde müssen jetzt nur noch einen Termin bei einem teilnehmenden Arzt buchen und zu dem Termin erscheinen.</p>
		</div>
		<div class="arrow arrow_2"></div>
		<div id="step_3" class="step_box">
			<h3 class="at_grbl"><span class="step_3 step-image"></span>Termin wahrnehmen &amp; Prämie bekommen</h3>
			<?php if($ref_company !='Spotify') { ?>
				<p>Sobald deine Freunde den Termin wahrnehmen, kannst du die Prämie abräumen und mit einem Gutschein im Wert von <b><?php echo$ref_price; ?></b> bei <?php echo$ref_company; ?> shoppen gehen.</p>
			<?php } else { ?>
				<p>Nach dem Arztbesuch kannst du dir die Prämie sichern und <b><?php echo$ref_price; ?> <?php echo$ref_company; ?> Premium</b> nutzen.</p>
			<?php } ?>
		</div>		
	</div>
	<iframe src="http://www.arzttermine.de/patient_empfehlung?type=<?php echo $_GET['type']; ?>"></iframe>
	<div id="terms_cond" class="middle-responsiv">
		<?php include_once("../data/friend_terms-conditions.html"); ?>
	</div>
	<?php include_once("../../layout/footer.html");   ?>
	<script type="text/javascript">
		$(document).ready(function () {
			$('#guidlines-link').on("click", function(e){
				e.preventDefault();
				$('#terms_cond').fadeToggle(800);
			});
		});
	</script>
</body>
</html>