function roll() {
	$(".arrow.right").click();
}

$(document).ready(function(){
	var c = function(o) {
		if (o instanceof jQuery && o.length == 1) 
			console.log(o.get(0));
		else
			console.log(o);
	}

	for(var i = 0; i < quotes.length; i++) {
		// Structure of a carousel element.
		// <li>
		// 	<a>
		// 		<div class='img'><img></div>
		// 		<div class='text'>
		//			<p class='quote'></p>
		//			<p class='doc-name'></p>
		//		</div>
		// 	</a>
		// </li>
		var li = $("<li>");

		var wrapper = $("<a>").attr({
			href: quotes[i].doctorProfileLink,
			title: quotes[i].doctorName,
			target: "_blank"
		});
		li.append(wrapper);

		var imgDiv = $("<div class='img'>").appendTo(wrapper);
		var img = $("<img>").attr({
			src: "docs-images/" + quotes[i].doctorImage,
			alt: quotes[i].doctorName
		});
		imgDiv.append(img);

		var text = $("<div class='text'>").appendTo(wrapper);
		text.append($("<p class='quote'>").html(quotes[i].doctorQuoteText));
		text.append($("<p class='doc-name'>").html(quotes[i].doctorName));

		$("#what-others-say ul").append(li);
	}

	$("nav").sticky({
		topSpacing: 0,
		className: "fixed"
	});

	$("nav a").on("click", function() {
		var target = $($(this).attr("href"));
		$("html,body").animate({scrollTop: target.offset().top - 80});

		return false;
	});

	// Delegate .transition() calls to .animate()
	// if the browser can't do CSS transitions.
	if (!$.support.transition)
    	$.fn.transition = $.fn.animate;


    $("#your-free-profile .arrow").on("click", function() {
    	var active = $("#your-free-profile li.active");
    	var toNext = $(this).hasClass("right");

    	if (toNext) {
	    	var next = active.next();
				if (next.length === 0) {
					next = $("#your-free-profile li").eq(0);
				}
				active.fadeOut().removeClass("active");
				next.addClass("active").fadeIn();
    	} else {
    		var prev = active.prev();
				if (prev.length === 0) {
					prev = $("#your-free-profile li").eq($("#your-free-profile li").length - 1);
				}
				active.fadeOut().removeClass("active");
				prev.addClass("active").fadeIn();
    	}

    	var ind = $("#your-free-profile .overlay.active").index();
    	$("#your-free-profile .carousel-display-point > div").removeClass("active").eq(ind).addClass("active")
    });



	var wrapper = $("#what-others-say .carousel").find(".carousel-wrapper");
	var ul = wrapper.find("ul");
	var lis = ul.find("li");

	var pos = 0;
	var delta = 460;
	var duration = 1000;
	var easing = "";
	
	lis.each(function(ind, el){
		ind > 1 ? $(el).addClass("inactive") : $(el).addClass("active");
		$(el).css({left: pos});
		pos += delta;
	});
	
	var x1 = 0;
	var x2 = delta;

	var left2 = -delta;
	var left1 = -delta*2;

	var right1 = delta + delta;
	var right2 = delta + delta*2;

	$("#what-others-say .carousel .arrow").on("click", function(){

		var active1 = ul.find(".active").eq(0);
		var active2 = ul.find(".active").eq(1);

		if (active1.position().left > active2.position().left) {
			var tmp = active1;
			active1 = active2;
			active2 = tmp;
		}

		if (active1.is(":animated") || active1.hasClass("animated")) {
			return;
		}

		if (active2.is(":animated")|| active2.hasClass("animated")) {
			return;
		}

		var toNext = $(this).hasClass("right");

		if (toNext) {
			var next1 = active2.next();
			if (next1.length === 0) {
				next1 = lis.eq(0);
			}

			var next2 = next1.next();

			next1.css({left: right1}).addClass("animated").stop().transition({left: x2}, duration, easing, function(){
				next1.removeClass("animated");
			});
			active1.addClass("animated").stop().transition({left: left2}, duration, easing, function(){
				active1.removeClass("animated");
			});
			active2.addClass("animated").stop().transition({left: x1}, duration, easing, function(){
				active2.removeClass("animated");
			});

			next1.removeClass("inactive").addClass("active");
			active1.removeClass("active").addClass("inactive");
		} else {
			var prev2 = active1.prev();
			if (prev2.length === 0) {
				prev2 = lis.last().last();
			}
			
			var prev1 = prev2.prev();

			prev2.addClass("animated").css({left: left2}).stop().transition({left: x1}, duration, easing, function(){
				prev2.removeClass("animated");
			});
			active1.addClass("animated").stop().transition({left: x2}, duration, easing, function(){
				active1.removeClass("animated");
			});
			active2.addClass("animated").stop().transition({left: right1}, duration, easing, function(){
				active2.removeClass("animated");
			});

			prev2.removeClass("inactive").addClass("active");
			active2.removeClass("active").addClass("inactive");

		}
	});

	setInterval(roll, 5000);

});
