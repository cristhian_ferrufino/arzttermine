var quotes = [
	{
		doctorImage: "praxis-mrt-apparategemeinschaft-helle-mitte-arzttermine.jpg",
		doctorName: "MRT Apparategemeinschaft Helle Mitte",
		doctorProfileLink: "http://www.arzttermine.de/praxis/mrt-apparategemeinschaft-helle-mitte/gkv",
		doctorQuoteText: "Die Kollegen bei arzttermine.de sind sehr kompetent. Wir waren überrascht über die durchaus große Nachfrage bei arzttermine.de und die sehr gute Qualität, in der wir die Patienten vermittelt bekommen."
	},
	{
		doctorImage: "dr-eva-dudik-frauenarzt-arzttermine.jpg",
		doctorName: "Dr. Eva Dudik",
		doctorProfileLink: "http://www.arzttermine.de/arzt/dr-eva-dudik",
		doctorQuoteText: "Bei Arzttermine.de spürt man die gute Stimmung sogar durch's Telefon. Freundlichkeit und Kompetenz des Teams vermitteln mir und meinen Mitarbeiterinnen Vertrauen. Bei vielen Unternehmen werden hohle Phrasen gedroschen. Arzttermine.de macht hier einen angenehmen Unterschied. Es steckt Substanz hinter den getroffenen Zusagen. Ein Partner, auf den man sich verlassen kann."
	},

	{
		doctorImage: "dr-christian-windelen-frauenarzt-koeln-arzttermine.jpg",
		doctorName: "Dr. Christian Windelen",
		doctorProfileLink: "http://www.arzttermine.de/arzt/dr-christian-windelen/pkv",
		doctorQuoteText: "Ein guter Service für unsere Patienten ist uns sehr wichtig. Dazu gehört heutzutage auch die Möglichkeit der Terminvergabe online. Wir schätzen die Zusammenarbeit mit Arzttermine.de, da wir unsere Terminplanung optimieren können und unsere Patienten zufriedener sind."
	},

	{
		doctorImage: "dr-gerd-bade-arzttemine.png",
		doctorName: "Dr. med. Gerd Bade",
		doctorProfileLink: "http://www.arzttermine.de/arzt/dr-gerd-bade/gkv",
		doctorQuoteText: "Ich schätze an arzttermine.de, dass ich so Lücken füllen kann, gerade bei kurzfristig abgesagten Terminen."
	},


	{
		doctorImage: "dr-christian-hubert-dental-carre-muenchen-zahnarzt-arzttermine.jpg",
		doctorName: "Dr. Christian Hubert",
		doctorProfileLink: "http://www.arzttermine.de/arzt/dr-christian-hubert/gkv",
		doctorQuoteText: "Unsere Zusammenarbeit mit arzttermine.de läuft reibungslos. Die Kollegen sind kompetent und wir schätzen den modernen Außenauftritt für unsere Patienten."
	},

	{
		doctorImage: "HNO-dr-floettmann-arzttermine.jpg",
		doctorName: "Dr. med. Thomas Flöttmann",
		doctorProfileLink: "http://www.arzttermine.de/arzt/dr-med-thomas-floettmann/pkv",
		doctorQuoteText: "Wir waren gespannt auf 'Arzttermine'. Die Mitarbeiter sind sehr freundlich und sehr hilfsbereit, die Probleme zu beheben, die sich vor allem aus unserer Praxisstruktur mit zwei Standorten ergeben. Jetzt freuen wir uns über jeden neuen Patienten."
	},

	{
		doctorImage: "thomas-peschke-orthopaedie-am-rathaus-steglitz-arzttermine.jpg",
		doctorName: "Thomas M. Peschke",
		doctorProfileLink: "http://www.arzttermine.de/arzt/thomas-m-peschke/gkv",
		doctorQuoteText: "Ich hätte nicht erwartet, daß Arzttermine.de mir eine derart hohe Zahl von Neu-Patienten, Kasse wie privat, bringen würde! Ein wirklich gutes, innovatives Konzept."
	},

	{
		doctorImage: "dr-martina-ulrich-hautarzt-berlin-arzttermine.jpg",
		doctorName: "Dr. med. Martina Ulrich",
		doctorProfileLink: "http://www.arzttermine.de/arzt/dr-med-martina-ulrich/pkv",
		doctorQuoteText: "Die effiziente und kurzfristige Terminierung durch Arzttermine.de erzeugt positive Effekte für alle Beteiligten: Die Patienten sind zufrieden, dass man sich unmittelbar um sie kümmert, unsere Praxis gewinnt Neupatienten und die Telefonleitung wird spürbar entlastet."
	}
];
