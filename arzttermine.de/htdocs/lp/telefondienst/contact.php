<?php

require_once $_SERVER['DOCUMENT_ROOT'] . "/../include/config.php";
require_once $_SERVER['DOCUMENT_ROOT'] . '/../include/bootstrap.php';
require_once $GLOBALS['CONFIG']['SYSTEM_CLASS_BASIC'];
require_once $GLOBALS['CONFIG']['SYSTEM_CLASS_MAILING'];

$result = array('data' => 'fail'); // Assume fail

if (isset($_POST)) {
	$post = filter_var_array($_POST, FILTER_SANITIZE_STRING);
	
	// Prevent anything messing with our return package
	if (isset($post['result'])) {
		unset($post['result']);
	}
	
	extract($post);
	
	$message = 
		"Good news, we might have a new client. <br/><br/>" .
		"Name: {$name} <br/>" . 
		"Specialty: {$specialty} <br/>" .
		"Phone: {$phone} <br/>" .
		"Email: {$email} <br/>";
	
	$_mailing = new Mailing();
	$_mailing->type = MAILING_TYPE_TEXT;
	$_mailing->subject = "Someone's interested in Fachtelefondienst";
	$_mailing->body_text = $message;
	$_mailing->addAddressTo('bk@arzttermine.de');
	
	if ($_mailing->sendPlain(false)) {
		$result['data'] = 'ok';
	}
}

echo json_encode($result);