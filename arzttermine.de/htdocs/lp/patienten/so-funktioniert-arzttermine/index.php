<?php 
/* INCLUDE SPACIFIC FILES */
include_once("../../layout/header.html");  


?>

<meta name="description" content="Arztbesuche können so einfach sein. Mit unserer Hilfe finden Sie Ihren Wunschtermin in Ihrer Nähe.<br/>Kostenfrei, online und sicher." />
<title>So funktioniert Arzttermine.de</title>
<link rel="stylesheet" href="css/patientenpage.css" media="all" />
<link href="../../static//css/bootstrap/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../../static//css/bootstrap/bootstrap-grid.min.css" rel="stylesheet" type="text/css">
<link href="../../static/css/bootstrap/bootstrap-reboot.css" rel="stylesheet" type="text/css">


<script src="js/jquery.flexslider-min.js"></script>
<script src="js/jquery.patientpage.js"></script>


	
</head>
<body class="landingpage specialpage patientenpage">
	<div id="topContainer">
		<div class="middle-responsiv">
			<a id="logo" href="http://www.arzttermine.de" title="Arzttermine.de | Schnell,einfach und online zu Ihrem Wunschtermin."><img src="//www.arzttermine.de/static/images/assets/logo-2.png" alt="Arzttermine.de Logo" width="212"></a>
			<p id="hotline">kostenfreie Hotline: <span class="tel at_grbl">0800 / 2222 133</span></p>
		</div>
	</div>
	<div class="flexslider hero">
		<ul class="slides">
			<li>
				<div class="slideContent">
					<h2 class="text-center">Unkompliziert &amp; einfach.</h2>
					<p class="text-center">Arztbesuche können so einfach sein. Mit unserer Hilfe finden Sie sofort den nächsten Arzttermin in Ihrer Nähe.<br/>Kostenfrei, online und sicher.</p>					
				</div>
				<img src="img/hero-1.jpg"/ alt="Kopfbild 1">
			</li>
			<li>
				<div class="slideContent">
					<h2 class="text-center">Vertrauen in Qualität.</h2>
					<p class="text-center">Unsere Experten stehen Ihnen in Gesundheitsfragen fachlich zur Seite. Lassen Sie sich von unseren erfahrenen Ärzten aus den verschiedensten Fachbereichen überzeugen.</p>					
				</div>
				<img src="img/hero-2.jpg" alt="Kopfbild 2"/>
			</li>
		</ul>
	</div>
	<div class="modules">
		<div class="center-fix">
			<div class="module">
				<h3><img src="img/icon_easy.jpg" />Einfach.</h3>
				<p>Mit nur wenigen Klicks zu Ihrem Wunschtermin. Ohne Anmeldung und garantiert kostenfrei.</p>
			</div>
			<div class="module">
				<h3><img src="img/icon_online.jpg" />Online.</h3>				
				<p>Ob von zu Hause, im Büro oder unterwegs: online oder mit  unserer mobilen App haben Sie jederzeit Zugriff auf Arzttermine.de.</p>
			</div>
			<div class="module">
				<h3><img src="img/icon_safe.jpg" />Sicher.</h3>
				<p>Vertrauen Sie unseren geprüften Ärzten. Das Leistungsprofil hilft Ihnen bei der Suche.</p>
			</div>
		</div>
	</div>
	<div class="middle-hero">
		<!--<img class="healthImage" src="img/middle-hero.jpg" alt="Arzttermine - Gesundheit" />-->
		<div class="center-fix">
			<div class="half">
				<div>
					<h2>Gesund leben.</h2>
					<p>Das Leben genießen, fit und gesund durch den Alltag gehen. Bestimmen Sie Ihr Leben selbst und entscheiden Sie, was gut für Sie ist. Mit unserem Service helfen wir Ihnen dabei, Ihre Gesundheit zu stärken und Ihren Arztbesuch leichter zu machen. Vertrauen Sie unserer Erfahrung und profitieren Sie von unserem Service.</p>
				</div>
				<a class="at_btn infoBtn">Arzt finden</a>
			</div>
		</div>
		<!--<a href="" class="arrow center-fix"> <img src="img/more_arrow.png" /></a>-->
	</div>
	<div class="main-content">
		<div class="center-fix">
			<div class="info">
				<div class="half picture"><img src="img/small1.jpg" alt="Top Beratung durch die besten Ärzte ihres Fachs." style="box-shadow: 0px 0px 14px #999;" /></div>
				<div class="half">
					<h3 style="padding-top: 15px;">Die besten Ärzte.<br/>Top Beratung.</h3>
					<p>Unsere erstklassigen Ärzte sind geprüft, diskret und professionell. Ob Zahnarzt, Frauenarzt oder Ärzte anderer Fachrichtungen – ein Kurzprofil der Praxis verschafft Ihnen einen Überblick über das Leistungsspektrum der Ärzte.</p>
					<a class="at_btn infoBtn" href="http://www.arzttermine.de/?utm_source=patientPage_jetzt-ausprobieren">jetzt ausprobieren</a>
				</div>
			</div>
			<div class="info">
				<div class="half">
					<h3 style="padding-top: 25px;">Nie wieder lange Wartezeiten.</h3>
					<p>Zeit ist kostbar. Verschwenden Sie diese nicht mit unnötigen Wartezeiten. Mit uns erhalten Sie Ihren individuellen Wunschtermin direkt und ohne Wartezeit. So bleibt Ihnen mehr Zeit für die schönen Dinge des Lebens.</p>
					
				</div>
				<div class="half picture"><img src="img/small4.jpg" alt="Nie wieder lange Wartezeiten." style="box-shadow: 0px 0px 14px #999;" /></div>
			</div>
			<div class="info">
				<div class="half">
					<h3 style="padding-top: 15px;">Direkt in Ihrer Nähe.</h3>
					<p>Ob Sie zu Hause sind, unterwegs oder auf der Arbeit: bei uns finden Sie den richtigen Arzt – direkt in Ihrer Nähe. Das spart Zeit und Wege. Wählen Sie ganz einfach Ihre Stadt und buchen Sie bequem über unseren Kalender den nächsten freien Termin. Online oder mit unserer mobilen App.</p>
					<a class="at_btn infoBtn" href="http://www.arzttermine.de/?utm_source=patientPage_arzt-finden">Arzt in der Nähe finden</a>
				</div>
				<div class="half picture"><img src="img/small2.jpg" alt="Finden Sie die besten Ärzt in Ihrer Nähe" /></div>
			</div>
			<!--<div class="info">
				<div class="half picture"><img src="img/small3.jpg" width="200" alt="Gesunde Ernährung ist ein wichtiger Bestandteil des Lebens." /></div>
				<div class="half">
					<h3 style="padding-top: 35px;">Gesundheit ist mehr.</h3>
					<p>Gesundheit ist Lebensgefühl, ist körperliches und seelisches Wohlbefinden, ist innere Ausgeglichenheit und Lebensfreude. Ein regelmäßiger Arztbesuch fördert Ihr Wohlbefinden und macht Sie fit für den Alltag.</p>					
				</div>
			</div>-->
		</div>
	</div>	
	
	<?php include_once("../../layout/footer.html");   ?>
		
	<script type="text/javascript">
		$(document).ready(function () {	
			$('.flexslider').flexslider({
				controlNav: false,
				directionNav: false
			});
		});
	</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
    <script src="https://use.fontawesome.com/f7e3e96c18.js"></script>
</body>
</html>
