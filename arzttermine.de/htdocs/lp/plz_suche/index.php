<?php
switch($_GET['city']) {
	case "berlin": {
		$searchfield_text 	= "Zahnarzt in Berlin finden:";
		$emotiondeck_image	= "berlin_brandenburger-tor.jpg";
		$footer_tpl 		= "neighborhood_berlin.html";
		$hotline			= "030 / 5770 224 47";
		break;
	}
	case "frankfurt": {
		$searchfield_text 	= "Zahnarzt in Frankfurt finden:";
		$emotiondeck_image	= "frankfurt_skyline.jpg";
		$footer_tpl 		= "neighborhood_frankfurt.html";
		$hotline			= "069 / 3487 974 47";
		break;
	}
	default: {
		$searchfield_text 	= "Zahnarzt finden in:";
		$emotiondeck_image	= "berlin_skyline.jpg";
		$footer_tpl			= "neighborhood_cities.html";
		$hotline			= "0800 / 2222 133";
		break;
	}
}
?>
<!doctype html>
<html lang="de" xmlns:og="http://ogp.me/ns#"xmlns:og="http://ogp.me/ns#">
<head>
<meta charset="utf-8">
	<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"/>
	<title>Zahnarzt finden | Arzttermine.de</title>
	<link href='/static/img/favicon.ico' rel='shortcut icon' />
		<link rel="stylesheet" href="css/pure_v0.2.0.min.css" />
		<link rel="stylesheet" href="css/screen.css" />
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&language=de"></script>
<script type="text/javascript" src="js/smart-search.js"></script>
<script type="text/javascript" src="js/jquery_tel.js"></script>
</head>
<body class="no-js ">
	<!-- HEADER -->
	<header id="header" role="banner" class="pure-g">
		<div class="middle-responsiv">
		<div class="site-logo pure-u-1-3">
			<img src="http://www.arzttermine.de/static/img/logo.png" alt="Arzttermine.de - Logo" />
		</div>
		<div class="header-hotline pure-u-1-3">
			<p class="description">Kostenfrei telefonisch buchen</p>
			<p class="phone_nbr at_grbl"><span class="tel"><?php echo $hotline; ?></span></p>
		</div>
		<div class="ekomi-logo pure-u-1-3">
			<a href="https://www.ekomi.de/bewertungen-arzttermine.html" target="_blank" title="Getestet und ausgezeichnet durch eKomi">
			<p>Ausgezeichneter Kundenservice</p>
			<img id="ekomi-stars" src="img/stars.png" alt="eKomi Sterne" />
			<img id="ekomi-seal" src="img/ekomi_silver_seal.png" alt="eKomi Siegel" /></a>
		</div>
	</header>
	<!-- HEADER END-->
	
	<section id="emotiondeck" class="middle-responsiv">
		<div id="emotiondeck-image"><img src="img/<?php echo $emotiondeck_image; ?>" alt="Berlin by Night" class="responsiv" />
			<div id="emotiondeck-benefits" class="pure-g">
				<div class="pure-u-1-3">
					<h4>Erstklassige Ärzte</h4>
				</div>
				<div class="pure-u-1-3">
					<h4>In meiner Stadt</h4>
				</div>
				<div class="pure-u-1-3">
					<h4>Kostenfrei ohne Anmeldung</h4>
				</div>
			</div>
		</div>
		<div id="desktop-search" class="emotiondeck-search" style="display: block;">
			<h3 class="at_grbl"><?php echo $searchfield_text; ?></h3>
			<input class="at_widget-input" type="text" name="city" value="" placeholder="PLZ / Straße" required="" autocomplete="" autofocus="">
			<a href="http://www.arzttermine.de/suche?a=search&amp;form%5Bmedical_specialty_id%5D=1&amp;form%5Binsurance_id%5D=2&amp;form%5Blocation%5D=" value="" class="btn at_widget-submit" >weiter</a>
		</div>
	</section>
		<!-- JUST MOBILE -->
		<section id="mobile-search" style="display: none;">
			<div class="emotiondeck-search">
				<h3 class="at_grbl"><?php echo $searchfield_text; ?></h3>
				<input class="at_widget-input" type="text" name="city" value="" placeholder="PLZ / Straße" required="" autocomplete="" autofocus="">
				<a href="http://www.arzttermine.de/suche?a=search&amp;form%5Bmedical_specialty_id%5D=1&amp;form%5Binsurance_id%5D=2&amp;form%5Blocation%5D=" value="" class="btn at_widget-submit" >weiter</a>
			</div>
		</section>
		<!-- JUST MOBILE END -->
	<section id="steps" class="middle-responsiv">
		<div class="pure-g" style="min-height: 190px;">
			<div id="step_1" class="step_box pure-u-1-3">
				<h3 class="at_grbl"><span class="step_1 step-image"></span>PLZ / Straße eintragen</h3>
				<p>Finden Sie ganz einfach Zahnärzte in Ihrer Stadt.</p>
			</div>
			<div id="step_2" class="step_box pure-u-1-3">
				<h3 class="at_grbl"><span class="step_2 step-image"></span>Arzt auswählen </h3>
				<p>Suchen Sie sich einen Facharzt ganz in Ihrer Nähe aus.</p>
			</div>
			<div id="step_3" class="step_box pure-u-1-3">
				<h3 class="at_grbl"><span class="step_3 step-image"></span>Wunschtermin buchen</h3>
				<p>Vereinbaren Sie Ihren Wunschtermin direkt online.</p>
			</div>
		</div>
	</section>
	<section id="hotline-area" class="middle-responsiv" style="display: none;">
		<p class="phone_nbr at_grbl"><span class="tel"><?php echo $hotline; ?></span></p>
	</section>
	<section id="neighborhoods" class="middle-responsiv">
		<?php	include($footer_tpl); ?>
	</section>
	<footer id="footer">
		<div class="middle-responsiv">
			<ul id="meta_navigation">
				<li class="first"><a href="/Kontakt" rel="nofollow">Kontakt</a></li>
				<li><a href="/Datenschutz" rel="nofollow">Datenschutz</a></li>
				<li><a href="/AGB" rel="nofollow">AGB</a></li>
				<li class="last"><a href="/Impressum" rel="nofollow">Impressum</a></li>
				<li class="sm_btn fb"><a href="https://www.facebook.com/Arzttermine"><span>Facebook</span></a></li>
				<li class="sm_btn gp"><a href="https://plus.google.com/u/0/114071819318557679351"><span>Google+</span></a></li>
			</ul>
		</div>
	</footer>
	<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-27059894-1']);
	_gaq.push(['_setDomainName', 'arzttermine.de']);
	_gaq.push(['_gat._anonymizeIp']);
	_gaq.push(['_trackPageview']);
	_gaq.push(['_trackPageLoadTime']);
	_gaq.push(['_setSiteSpeedSampleRate', 100]);
	
	(function() {
	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#emotiondeck-benefits").find("h4").prepend('<span class="icon_checkmark">');
			$(".phone_nbr").prepend('<img src="img/icon_phone.png" width="32" alt="" class="icon_phone" />');
		});
	</script>
</body>
</html>