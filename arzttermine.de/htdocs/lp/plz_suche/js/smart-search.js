$(document).ready(function() {
	/*initiate the autocomplete service*/
    $('.at_widget-input').each(function(index,element) {
        var input = element;
        var options = {
            componentRestrictions: {country: "de"}
        };
        var autocomplete = new google.maps.places.Autocomplete(input, options);
        /*bind click event to submit link. When clicking on the link append to the href the location filled in the search field.
        * the location may be one from the Google suggestions, or the value submitted
        * */
        $(".at_widget-submit").click(function(event){
            event.preventDefault();
            window.location.href = "http://www.arzttermine.de/suche?a=search&form%5Bmedical_specialty_id%5D=1&form%5Binsurance_id%5D=2&form%5Blocation%5D="+$(".at_widget-input").val();
        });
        $(".at_widget-input").keypress(function (event) {
            if (event.keyCode == 13) {
                window.location.href = "http://www.arzttermine.de/suche?a=search&form%5Bmedical_specialty_id%5D=1&form%5Binsurance_id%5D=2&form%5Blocation%5D=" + $(".at_widget-input").val();
            }
        });
    });
    /* OTHER STUFF */
    $("#meta_navigation a, #neighborhoods a").click(function() {
        $(this).target = "_blank";
        window.open($(this).prop('href'));
        return false;
    });
    /* MOBILE STUFF */
    var deviceAgent = navigator.userAgent.toLowerCase();
    var isTouchDevice =
            (deviceAgent.match(/(iphone|ipod|ipad)/) ||
                    deviceAgent.match(/(android)/)  ||
                    deviceAgent.match(/(iemobile)/) ||
                    deviceAgent.match(/iphone/i) ||
                    deviceAgent.match(/ipad/i) ||
                    deviceAgent.match(/ipod/i) ||
                    deviceAgent.match(/blackberry/i) ||
                    deviceAgent.match(/bada/i));
    if( isTouchDevice != null ){
        var tel = $('.header-hotline').find(".tel").text().replace("/","").replace(" ","").substring(1);
        $('.phone_nbr .tel').attr('title', '+49'+tel);
        $('.phone_nbr .tel').hreftel();
    }		
});