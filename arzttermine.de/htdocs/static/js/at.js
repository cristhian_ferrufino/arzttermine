// Arzttermine Generic
var AT = AT || {};

AT._defer(
    (function(_, $, doc, win, und) {

        // Calendar transformations for resize event. Makes them easier to see
        // ========================================================================================
        var $calendars = $('.calendar'),
            $calendar = $calendars.eq(0);

        // Checks the size of the calendars. Assumption: all calendars on a page are of the same width.
        // At certain breakpoints, add/remove classes. 
        // Used for different layout of messages with buttons (integration 8 or next available appointment).
        var setCalendarsSize = function() {
            if ($calendar.length) {
                var width = $calendar.width();

                if (width > 670) {
                    $calendars.removeClass('medium small').addClass('large');
                } else if (width < 670 && width > 500) {
                    $calendars.removeClass('small large').addClass('medium');
                } else if (width < 500) {
                    $calendars.removeClass('medium large').addClass('small');
                }
            }
        };

        var resizeTimer;
        $(win).resize(function() {
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(setCalendarsSize, 25);
        });
        
        // Prefill empty form elements
        // @todo: Currently hardcoded. Make dynamic or fix :/
        // ========================================================================================
        var $form      = $('form'),
            $specialty = $form.find('[name="form[medical_specialty_id]"]'),
            $insurance = $form.find('[name="form[insurance_id]"]'),
            $location  = $form.find('[name="form[location]"]');
        
        $form.submit(function() {
            if (!$specialty.val()) {
                $specialty.val(1); // Zahnarzt
            }

            if (!$insurance.val()) {
                $insurance.val(2); // GKV
            }

            if (!$location.val()) {
                $location.val('Berlin');
            }
        });

        // Placeholder styling cases
        // ========================================================================================
        var selectPlaceholder = function() {
            var $this = $(this);

            if ($this.val() == '') {
                $this.addClass('placeholder');
            } else {
                $this.removeClass('placeholder');
                $this.find('.select-placeholder').remove();
            }
        };
        
        // Doctor login page - toggling different forms
        // ========================================================================================
        $('.doctor-login input[name=email] + a').click(function(){
            $('form').hide();
            $('#forgot-email').show();
        });
        $('.doctor-login input[name=password] + a').click(function(){
            $('form').hide();
            $("#forgot-password").show();
        });

        // Help tooltips - toggling tooltips
        // ========================================================================================
        $('a.help').click(function(e){
            $(this).parent().next('.help-tooltip').toggle();
            e.preventDefault();
        });

        // onLoad
        // ========================================================================================
        $(function() {
            $('select').change(selectPlaceholder).change();

            // Initially set the classes for the calendar width.
            setCalendarsSize();

            // Don't jump to the top of the page if the .active button is clicked.
            $('.insurance-selector-button').on('click', function(e) {
                if ($(this).hasClass('active')) {
                    e.preventDefault();
                }
            });
        });

    }(AT, jQuery, document, window))
);
