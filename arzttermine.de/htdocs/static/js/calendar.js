var AT = AT || {};

AT._defer(function(){
(function(_, $, doc, win, und) {

    // Full width of the calendar
    var calendarWidth = 0;

    // Column width inside the container
    var colWidth = 1;

    // The value for which the calendar is scrolled when the arrows are clicked.
    var step = 0;
    
    // Cache appointments we've requested previously
    // @todo: Use localStorage if available
    // var appointmentCache = {};
    
    var DIRECTION = {
        RIGHT: 1,
        LEFT: -1
    };
    
    // Calendar scroll offset
    var currX = 0;

    _.Calendar = {
         // Receives date in ISO format ("yyyy-mm-dd") and adds toAdd number of days to it.
        addDaysToTextDate: function(date, toAdd) {
            toAdd = toAdd || 14; // Set the default
            var newDate = new Date(date.replace(/\-/g, '/'));

            newDate.setDate(newDate.getDate() + parseInt(toAdd, 10));

            var day = newDate.getDate();
            day = day > 9 ? day : "0" + day;

            var month = newDate.getMonth() + 1;
            month = month > 9 ? month : "0" + month;

            newDate = newDate.getFullYear() + "-" + month + "-" + day;

            return newDate;
        },

        /**
         *  Final preparations for the ajax call.
         *  In the callback, append the new HTML, update the metadata and execute the provided user callback.
         */
        fetchAppointments: function(dateStart, dateEnd, insuranceId, specialtyId, callback) {
            if (!dateStart || !dateEnd) {
                return;
            }
            
            var providers = [],
                providersString;
            
            callback = callback || $.noop;

            // Loop all scrollable calendars, take their user and location ids, generate the providers string.
            // TODO: cache this!
            $(".calendar.scrollable").each(function() {
                var $this = $(this);
                providers.push($this.data("user-id") + "|" + $this.data("location-id"));
            });

            // Remove the trailing comma or the API returns an error.
            // TODO: optimize or ask Axel to make the API a bit more flexible.
            providersString = providers.join(',');

            // When the necessary params are ready, call the API.
            $.ajax({
                url: apiUrl + "/available-appointments.json",
                data: {
                    date_start: dateStart,
                    date_end: dateEnd,
                    insurance_id: insuranceId,
                    medical_specialty_id: specialtyId,
                    providers: providersString,
                    widget: win.widget || ''
                },
                success: function (response) {
                    var providers = $("#providers");

                    // Update the metadata.
                    providers.data("date-start", response.date_start);
                    providers.data("date-end", response.date_end);
                    providers.data("date-days", response.days);

                    // There's an error, so just ignore for now
                    if (response.error !== und) {
                        // @todo Notify client
                        return;
                    }
                    
                    // Loop scrollable calendars. Based on the user and location ids, 
                    // take the new content from the response and append it.
                    $(".calendar.scrollable").each(function() {
                        var $this      = $(this);
                        var userId     = $this.data("user-id");
                        var locationId = $this.data("location-id");
                        var html       = response.providers[userId + "|" + locationId].html;

                        $this.find(".wrapper").append(html);
                    });

                    self.setWrappersWidth();
                    
                    if (typeof callback === 'function') {
                        callback.call();
                    }
                }
            });
        },

        // Set the wrapper width so we can calculate scroll values
        // @todo: Figure out why we need to set widths individually
        setWrappersWidth: function() {
            var $calendar_wrapper = $(".calendar .wrapper");

            // @todo: This, but more efficiently.
            step = 3 * $calendar_wrapper.find('.col-date').eq(0).outerWidth(true);
            
            $calendar_wrapper.each(function() {
                var $this = $(this),
                    $columns = $this.find(".col-date");
                
                colWidth = $columns.eq(0).outerWidth(true);
                $this.width(colWidth * $columns.length);
            });
        },
        
        // Scroll the calendar left or right
        scrollCalendar: function(direction, $calendar) {
            direction = direction || DIRECTION.RIGHT;
            
            var $wrapper = $calendar.find('.wrapper');

            // If the previous scrolling is not finished yet => abort.
            if (!$wrapper.length || $wrapper.is(":animated")) {
                return;
            }
            
            if (direction === DIRECTION.RIGHT) {
                // Moving away from the start, enable the left arrow.
                $('.calendar .arrow.left').removeClass('disabled');

                // If the arrow of a "next-available-appointment" type of calendar is clicked,
                // it's the same as clicking the message button itself => scroll to the next available appointment.
                var $nextAvailableAppointmentButton = $calendar.find("a[data-next-appointment-date]");
                if ($nextAvailableAppointmentButton.length != 0 && !$calendar.hasClass('hide-action')) {
                    $nextAvailableAppointmentButton.click();
                    return;
                }

                var wrapperWidth = $wrapper.width();

                // If there is no more available days in the calendar, we need to call the API.
                // Otherwise, if there is enough data, just scroll for the value of step.
                if (calendarWidth - currX + step > wrapperWidth) {
                    var dateStart = $("#providers").data("date-end"); // The end becomes the new start
                    dateStart = self.addDaysToTextDate(dateStart, 1);

                    var dateEnd = self.addDaysToTextDate(dateStart, 14);

                    // Gather the data needed for the API call.
                    // TODO: check whether the insurance and specialty IDs are per page or per provider!
                    var insuranceId = $calendar.data("insurance-id");
                    var specialtyId = $calendar.data("specialty-id");

                    // Once we have the ajax response and the new HTML is appended, scroll the calendar.
                    self.fetchAppointments(dateStart, dateEnd, insuranceId, specialtyId, function() {
                        currX -= step;
                        self.translateWrappers();
                    });
                } else {
                    currX -= step;
                    self.translateWrappers();
                }
            } else if (direction === DIRECTION.LEFT) {
                // Scroll by the value of step, or 0 (if step would take us before the calendar's start).
                currX += step;
                if (currX > 0) {
                    currX = 0;
                }

                self.translateWrappers();
            }
        },

        // Translates the calendar wrappers.
        // If the browser supports 3d, then transform3d is used. Otherwise, just plain animate.
        // Note: jquery.transit does not use 3d, just 2d translations - so no GPU acceleration, which sucks on iPads.
        translateWrappers: function() {
            var $wrapper = $('.calendar.scrollable > .wrapper');

            if (self.has3d) {
                $wrapper.css({ 
                        'position': 'relative',
                        '-webkit-transition': 'all 0.3s linear',
                           '-moz-transition': 'all 0.3s linear',
                            '-ms-transition': 'all 0.3s linear',
                             '-o-transition': 'all 0.3s linear',
                                'transition': 'all 0.3s linear',
                        '-webkit-transform': 'translate3d(' + currX + 'px,0,0)',
                           '-moz-transform': 'translate3d(' + currX + 'px,0,0)',
                            '-ms-transform': 'translate3d(' + currX + 'px,0,0)',
                             '-o-transform': 'translate3d(' + currX + 'px,0,0)',
                                'transform': 'translate3d(' + currX + 'px,0,0)'
                    });
                $wrapper.on('transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd', function() { $(doc).trigger('AT:Calendar:scrollend'); });
            } else {
                $wrapper.animate({ marginLeft: currX }, 300, function() { $(doc).trigger('AT:Calendar:scrollend'); });
            }
        },

        // Checks whether the browser has transform3d capabilities.
        // To be evaluated on init only.
        // TODO: consider moving it to the AT namespace (instead of AT.Calendar).
        // https://gist.github.com/lorenzopolidori/3794226
        has3d: (function(){
            var el = document.createElement('p'),
            has3d,
            transforms = {
                'webkitTransform':'-webkit-transform',
                'OTransform':'-o-transform',
                'msTransform':'-ms-transform',
                'MozTransform':'-moz-transform',
                'transform':'transform'
            };
         
            // Add it to the body to get the computed style
            document.body.insertBefore(el, null);
         
            for(var t in transforms){
                if( el.style[t] !== undefined ){
                    el.style[t] = 'translate3d(1px,1px,1px)';
                    has3d = win.getComputedStyle(el).getPropertyValue(transforms[t]);
                }
            }
         
            document.body.removeChild(el);
         
            return (has3d !== undefined && has3d.length > 0 && has3d !== "none");
        })(),

        // Decides whether to show "show next appt", depending on the position of the appt (on or off screen).
        updateShowNextApptButton: function($wrappers, offset) {
            $('.calendar .arrow.left').removeClass('disabled');
            
            $wrappers.each(function(){
                // if it's next-available-appointment, decide whether to show the shortcut button or not
                var $calendar = $(this).parents('.calendar');
                if ($calendar.hasClass('type-next-available-appointment')) {
                    var $firstAppt = $calendar.find('.appointment');
                    
                    if ($firstAppt.length > 0) {
                        // It can happen that the wrapper is not wide enough, the next appt is placed 
                        // below the calendar until the wrapper is widened enough. 

                        // Calendar div bottom-right
                        var calRight = $calendar.get(0).getBoundingClientRect().right;
                        var calBottom = $calendar.get(0).getBoundingClientRect().bottom;

                        // First appointment div top-left
                        var apptLeft = offset || $firstAppt.get(0).getBoundingClientRect().left - step;
                        var apptTop = $firstAppt.get(0).getBoundingClientRect().top;

                        if (apptTop < calBottom && apptLeft < calRight) {
                            $calendar.addClass('hide-action');
                            // $calendar.removeClass("exception");
                        }
                    }
                }
            });
        },
        
        showNextAppointment: function() {
            var $calendar = $(this).closest('.calendar'),
                $providers = $('#providers');
            
            var currDateStart = $providers.data("date-start"),
                currDateEnd = $providers.data("date-end");
            currDateStart = self.addDaysToTextDate(currDateStart, 1)

            // Pivot date is the one that has the first available appointment.
            var pivotDate = $(this).find("a").data("next-appointment-date");

            // If the pivot date is outside of the [dateStart, dateEnd] period => ask for more data.
            // This can happen if there are more calendars on the page, and another calendar asked for more 
            // date before this call.
            // Otherwise, just scroll to the pivotDate.
            // if (!(new Date(currDateStart) < new Date(pivotDate) && new Date(currDateEnd) > new Date(pivotDate))) {
            if (!_.datesHelper.inRange(pivotDate, currDateStart, currDateEnd)) {
                var dateStart = new Date(currDateEnd) > new Date(pivotDate) ? currDateEnd : pivotDate;
                // The end date for ajax will be two weeks after the pivot date or the start date (whichever is later).
                // Two weeks (14 days) is value used on the server side as a default. Should be a constant.
                var dateEnd = _.datesHelper.compare(pivotDate, dateStart) > 0 
                            ? self.addDaysToTextDate(pivotDate, 14) 
                            : self.addDaysToTextDate(dateStart, 14);

                var insuranceId = $calendar.data("insurance-id");
                var specialtyId = $calendar.data("specialty-id");

                self.fetchAppointments(dateStart, dateEnd, insuranceId, specialtyId, function() {
                    // The HTML is already in, now we have to calculate how much the calendar should be scrolled.
                    // The date column that contains the first appointment should be at the calendar's left visible edge.
                    var startCol = $calendar.find(".col-date[data-date=" + pivotDate + "]");

                    // Formula: index of the column * columnWidth * -1 (-1 because of the scrolling direction)
                    var left = -1 * $calendar.find(".col-date").index(startCol) * colWidth;

                    currX = left;

                    self.translateWrappers();

                    // This calendar is no longer in an exception state.
                    // @todo: many other edge cases should be checked here...
                    // $calendar.removeClass("exception").find(".action").remove();
                });
            } else {
                var startCol = $calendar.find(".col-date[data-date=" + pivotDate + "]");

                // Formula: index of the column * columnWidth * -1 (-1 because of the scrolling direction)
                var left = -1 * $calendar.find(".col-date").index(startCol) * colWidth;

                currX -= step;

                self.translateWrappers();
            }
        },

        // "MORE" APPOINTMENTS BUTTON
        // When the "more" button is hovered/tapped, we take its hidden content,
        // copy it and append to an absolutely positioned layer. The layer has to be outside
        // of the .calendar because of the hidden overflow.
        showMoreAppointments: function() {
            var $this = $(this);

            var layer = $(".layer");
            layer.find(".popup-container.appointments").remove();

            var popup = $('<div class="popup-container appointments" style="position:absolute;"></div>');
            popup.html($this.find(".appointments").html()).appendTo(layer);

            var offset = $this.offset();
            var top = offset.top - 11;
            var left = offset.left - 6;

            popup.css({ top: top, left: left }).show();
            popup.one("mouseleave", function() {
                popup.remove();
            });
        },

        // Bind all the event listeners for the module
        setListeners: function() {
            $(doc)
                .on('AT:Calendar:initialized', self.setWrappersWidth)
                .on('mouseenter click', '.calendar .more', self.showMoreAppointments)
                .on('click', '.type-next-available-appointment .message', self.showNextAppointment);

            $(doc).on('AT:Calendar:scrollend', function(){
                self.updateShowNextApptButton($(".calendar.scrollable .wrapper"));
            });

            $(doc).on('touchstart click', '.calendar .arrow.right', function(e) { 
                // Touch overrides click on tablets. Otherwise, the response is very slow.
                // http://stackoverflow.com/a/13214114
                e.stopPropagation(); 
                e.preventDefault();
                self.scrollCalendar(DIRECTION.RIGHT, $(this).closest('.calendar')); 
            });

            $(doc).on('touchstart click', '.calendar .arrow.left', function(e) { 
                // Touch overrides click on tablets. Otherwise, the response is very slow.
                // http://stackoverflow.com/a/13214114
                e.stopPropagation(); 
                e.preventDefault();
                self.scrollCalendar(DIRECTION.LEFT, $(this).closest('.calendar')); 
            });
        },

        initialize: function() {
            // Set some important UI numbers
            calendarWidth = $(".calendar").eq(0).width();
            
            // IE8 shim-implementation for Date.toISOString() (http://stackoverflow.com/a/11440625)
            if (!Date.prototype.toISOString) {
                // Here we rely on JSON serialization for dates because it matches 
                // the ISO standard. However, we check if JSON serializer is present 
                // on a page and define our own .toJSON method only if necessary
                if (!Date.prototype.toJSON) {
                    Date.prototype.toJSON = function () {
                        function formatNumber(n) {
                            // Format integers to have at least two digits.
                            return n < 10 ? '0' + n : n;
                        }

                        return this.getUTCFullYear()   + '-' +
                            formatNumber(this.getUTCMonth() + 1) + '-' +
                            formatNumber(this.getUTCDate())      + 'T' +
                            formatNumber(this.getUTCHours())     + ':' +
                            formatNumber(this.getUTCMinutes())   + ':' +
                            formatNumber(this.getUTCSeconds())   + 'Z';
                    };
                }

                Date.prototype.toISOString = Date.prototype.toJSON;
            }

            self.setListeners();
            
            $(doc).trigger('AT:Calendar:initialized');
        }
    };

    _.datesHelper = {
        convert:function(d) {
            // Converts the date in d to a date-object. The input can be:
            //   a date object: returned without modification
            //  an array      : Interpreted as [year,month,day]. NOTE: month is 0-11.
            //   a number     : Interpreted as number of milliseconds
            //                  since 1 Jan 1970 (a timestamp) 
            //   a string     : Any format supported by the javascript engine, like
            //                  "YYYY/MM/DD", "MM/DD/YYYY", "Jan 31 2009" etc.
            //  an object     : Interpreted as an object with year, month and date
            //                  attributes.  **NOTE** month is 0-11.
            return (
                d.constructor === Date ? d :
                d.constructor === Array ? new Date(d[0],d[1],d[2]) :
                d.constructor === Number ? new Date(d) :
                d.constructor === String ? new Date(d) :
                typeof d === "object" ? new Date(d.year,d.month,d.date) :
                NaN
            );
        },

        compare:function(a,b) {
            // Compare two dates (could be of any type supported by the convert
            // function above) and returns:
            //  -1 : if a < b
            //   0 : if a = b
            //   1 : if a > b
            // NaN : if a or b is an illegal date
            // NOTE: The code inside isFinite does an assignment (=).
            return (
                isFinite(a=this.convert(a).valueOf()) &&
                isFinite(b=this.convert(b).valueOf()) ?
                (a>b)-(a<b) :
                NaN
            );
        },

        inRange:function(d,start,end) {
            // Checks if date in d is between dates in start and end.
            // Returns a boolean or NaN:
            //    true  : if d is between start and end (inclusive)
            //    false : if d is before start or after end
            //    NaN   : if one or more of the dates is illegal.
            // NOTE: The code inside isFinite does an assignment (=).
           return (
                isFinite(d=this.convert(d).valueOf()) &&
                isFinite(start=this.convert(start).valueOf()) &&
                isFinite(end=this.convert(end).valueOf()) ?
                start <= d && d <= end :
                NaN
            );
        }
    };

    var self = _.Calendar;

    // @todo: We should just detect this somehow
    $(function() {
        _.Calendar.initialize();
    });

}(AT, jQuery, document, window))

});