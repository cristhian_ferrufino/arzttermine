// Source for the Reporting (in dashboard) section
;(function($, win, doc, und) {

    var $statement = $('#statement');
    
    // For displaying icons
    // @todo Better
    var statusClasses = {
        1: 'status-yes',
        2: 'status-maybe',
        4: 'status-yes',
        8: 'status-maybe'
    };
    
    // Send the current report to the server
    var save = function() {
        var bookings = [];

        // Get booking data and create an array
        $statement.find('tr.booking').each(function() {
            var $booking = $(this);
            
            bookings.push({
                booking_id: $booking.data('booking-id'),
                status: $booking.find('input.reporting-status:checked').val(),
                appointment_start_at: $booking.find('input.moved-to').val(),
                returning_visitor: !!$booking.find('.reporting-returning-visitor').is(':checked')
            });
        });
        
        $.ajax({
            url: win.location,
            type: 'post',
            data: {
                payload: JSON.stringify(bookings) // We need to JSONify this to ensure we don't get param count problems
            }
        })
        .done(function () {
            win.location.href = win.location.href;
        });
    };

    // Event listeners
    // --------------------------------------------------------------------------------------------------------------
    $('.buttons').on('click', '.button', function(e) {
        e.preventDefault();

        var $this = $(this);

        if ($this.data('busy')) {
            return;
        }
        
        $this
            .data('busy', true)
            .addClass('disabled');

        save();
    });
    
    // Detect changes to the status radios to toggle dates
    $('td.status')
        .on('click', 'input[type=radio]', function() {
            var $this = $(this);
            $this.parents('tr').find('.moved-to').toggle(parseInt($this.val(), 10) === 4);
        })

    // Update status icons when the inputs are updated
        .on('click', 'input[type=radio]', function() {
            var $this = $(this),
                status = parseInt($this.val(), 10);
            
            $this.parents('tr').find('.booking-status')
                .removeClass('status-yes status-no status-maybe')
                .addClass(
                    status === 2 ? 'status-no' :
                        status === 4 ? 'status-maybe' : 
                            'status-yes'
                );
        }).click();

    // onLoad
    // --------------------------------------------------------------------------------------------------------------
    $(function() {
        $statement.find('.datetimepicker').datetimepicker();
    });

}(jQuery, window, document));