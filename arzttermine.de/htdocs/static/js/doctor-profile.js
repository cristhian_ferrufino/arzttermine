// Arzttermine Generic
var AT = AT || {};

AT._defer(
    (function(_, $, doc, win, und) {

        $(function() {
            $(doc).on('change', 'select.hyperlink', function() {
                win.location = this.options[this.selectedIndex].value;
            });
        });
        
        $('.account-dashboard .toggle-specialties').click(function(){
            var $this = $(this),
                $unselected = $('.unselected-specialties'),
                $showMore = $this.find('.show-more'),
                $showLess = $this.find('.show-less');

            $unselected.toggle();
            $showMore.toggle();
            $showLess.toggle();

            // Move the newly selected to top
            if ($showMore.is(':visible')) {
                var $newSelected = $unselected.find('input:checked');
                $newSelected.each(function() {
                    $(this).parents('label').insertBefore('a.toggle-specialties');
                });
            } else if ($showLess.is(':visible')) {
            // Move the deselected to the togglable div
                var $newUnselected = $unselected.parents('.specialties').find('> label > input:not(:checked)');
                $newUnselected.each(function () {
                    $(this).parents('label').prependTo($unselected);
                });
            }
        });

        // Copied from NGS =============================================================================================
        
        $('#upload_pic_button').on('click', function() {
            $('#upload_pic').show();
        });
        
        $('.actions').find('a').on('click', function(e) {
            e.preventDefault();
            
            $.post(
                $(this).attr('href'),
                {
                    uid: uid,
                    aid: $(this).parent().data('aid')
                },
                function() {
                    location.reload();
                }
            )
        });

        $('input[name*=medical_specialties_ids]').on('change', (function() {
            if (this.checked) {
                _sendRequest(
                        docAccountApiUrl + "?type=treatment_types&msid=" + this.value + '&arzt=' + $('.summary').data('doctor-slug'),
                        this.value,
                        showTreatmentTypesDiv
                );
            } else {
                removeTreatmentTypesDiv(this.value);
            }
        }));

        function showTreatmentTypesDiv(data){
            $('#treatments').append(data);
        }

        function removeTreatmentTypesDiv(id) {
            $("#treatment-type-" + id).remove();
        }

        // This function is copied like ten goddamn times.
        // @todo: Seek and Destroy
        function _sendRequest(_url, postData, callBack) {
            if (_url && postData) {
                callBack = callBack || $.noop;

                $.ajax({
                    url : _url,
                    type : 'GET',
                    data : postData,
                    context : doc.body
                })
                .done(function(data) {
                    callBack(data);
                });
            }
        }
        if (typeof doctorsMedSpecIdsArray !== 'undefined') {
            for (var i=0;i < doctorsMedSpecIdsArray.length; i++) {
                _sendRequest(
                    docAccountApiUrl, 
                    {
                        type: 'treatment_types',
                        msid: doctorsMedSpecIdsArray[i],
                        arzt: $('.summary').data('doctor-slug')
                    },
                    showTreatmentTypesDiv
                );
            }
        }

    }(AT, jQuery, document, window))
);
