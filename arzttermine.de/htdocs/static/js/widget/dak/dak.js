var AT = AT || {};

AT._defer(function(){
    (function(_, $, doc, win, und) {
        _.Maps.placeMarkers = function () {
            $(doc).triggerHandler('AT:Maps:markersCreated');
        },

        _.Search.paginate = function() {},

        // If the screen is smaller than the mobile responsive breakpoint,
        // prevent map initialization by hiding its container.
        (function () {
          // 30*16 because (30em*16px)
          if ($('body').width() <= 30*16) {
            $('#search-map').remove();
          }
        })(),

        $('#map-toggler').on('click', function (argument) {
          $('#search').toggleClass('expanded');
          $('#results-list').toggleClass('pushed');
          
          // If the map hasn't been init'd yet, add its container and initialize.
          if ($('#search-map').length == 0) {
            $('#search').prepend($('<div id="search-map"></div>'));
            _.Maps.initialize();
          }
        })
    }(AT, jQuery, document, window))
});
