/*
 * Define global Dak object for encapsulation 
 */
if(typeof Dak === 'undefined') {
	var Dak = {};
}

/*
 * get Cookie function
 */
Dak.getCookie = function(name) {
	var cookieTemp = '',
	cookieName = '',
	allCookies = document.cookie.split(';');
	for (var i=0; i<allCookies.length; i++) {
		cookieTemp = allCookies[i].split('=');
		cookieName = cookieTemp[0].replace(/^\s+|\s+$/g,'');
		if (cookieName == name) {
			if (cookieTemp.length > 1) {
				return unescape(cookieTemp[1].replace(/^\s+|\s+$/g,'')); 	 
			}
			break;
		}
	}
	return null;
};

/*
 * set body font size on init depending on cookie
 */
Dak.setCookieBodySize = function() {
	var cookie = Dak.getCookie("dak-font-scale");
	if(cookie!=null&&cookie!=""&&!isNaN(cookie)) {
		document.lastChild.style.fontSize = parseInt(cookie)+"%";
	}
};

/*
 * add class to html if js is enabled
 */
Dak.setJSEnabledClass = function() {
	document.lastChild.className += "js-enabled";
};

Dak.setCookieBodySize();
Dak.setJSEnabledClass();