$nav-height: 1.8;

body > header {
  background: $color-at-white;
  position: relative;
  width: 100%;
  @include clearfix;
  overflow: visible;
  @include rem-fallback(margin-bottom, 3);

  .top-bar {
    width: 100%;
    position: absolute;
    @include rem-fallback(height, $nav-height);
    @include rem-fallback(line-height, $nav-height);
    background: $color-at-blue;
    text-align: right;
  }

  nav {
    @include inline-block;
    
    ul {
      list-style-type: none;
      @include rem-fallback(font-size, 0.75);

      & > li {
        @include inline-block;
        @include rem-fallback(padding, 0, 1);

        & > a { 
          text-transform: uppercase;
          color: $color-at-white; 
          font-weight: 700;
        }
    
        &.active {
          background: $color-at-white;
          a { color: $color-at-grey-darker; }
        }

        &:hover {
          .top-bar-popup {
            display: block;
          }
        }
      }
    }

    .top-bar-popup {
      display: none;
      position: absolute;
      margin-top: -2px;
      text-align: left;
      @include rem-fallback(width, 14);
      background: $color-at-white;
      border-top: 3px solid $color-at-blue;
      box-shadow: 0 2px 2px $color-at-grey-darker;
      @include rem-fallback(padding, 1);
      z-index: 1;

      h4 {
        color: $color-at-blue;
        text-transform: uppercase;
        @include font-lato;
      }
      h5 {
        font-weight: 700;
        text-transform: uppercase;
        @include font-lato;
      }

      a {
        color: $color-at-blue;
      }

      form {
        label {
          display: block;
          text-transform: uppercase;
        }

        input[type=password],
        input[type=text] {
          @include rem-fallback(margin-right, 0.5);
          @include rem-fallback(line-height, 1);
          @include font16;
          display: block;
          width: 100%;
        }

        input[type=submit] {
          @include flat-button($color-at-blue, large);
          display: block;
          margin: auto;
          @include rem-fallback(margin-top, 0.5);
        }

        a {
          display: block;
          text-align: center;
          @include font10;
        }
      }

      section {
        border-top: 1px solid $color-at-grey;
        @include rem-fallback(margin-top, 0.5);
        @include rem-fallback(padding-top, 0.5);
      }

      a[role=button],
      button {
        @include flat-button($color-at-blue, large);
        display: block;
        margin: auto;
        @include rem-fallback(margin-top, 0.5);
        a { color: $color-at-white; }
      }
    }

  }

  div.phone-container {
    text-transform: lowercase;
    @include inline-block;
    @include font12;
    @include rem-fallback(line-height, 1.15);
    @include rem-fallback(margin-right, .5);
    text-align: right;
    color: $color-at-white; 

    a {
      color: $color-at-white; 
      @include font14;
      font-weight: 700;

      &:before {
        font-size: 100%;
        position: relative;
        top: 1px;
        margin-right: 1px;
      }
    }
  }

  .logo {
    position: relative;
    @include rem-fallback(top, 0.5);
    @include rem-fallback(margin-bottom, 0.5);
    width: 215px;
    height: 70px;
    background-size: auto 100%;

    background: none;
    @include rem-fallback(top, 2);

    a {
      display: block;
      width: 100%;
    }
  }

  .breadcrumbs {
    @include span-columns(12);
    @include rem-fallback(margin-top, 1);
    @include font14;

    li {
      display: inline;
    }

    li {
      &:before {
       content: "\a0>\a0";
      }

      &:first-child {
        &:before {
          content: "";
        }
      }

      &:last-child {
        font-weight: 700;
      }
    }
  }
}
