<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/../include/bootstrap.php';

use Arzttermine\Statistic\Statistic;

$statistic = new Statistic();

?><!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html class="no-js" lang="en"><!--<![endif]--><!-- BEGIN HEAD --><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Dashboard</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport">
<meta content="" name="description">
<meta content="" name="author">
<meta name="MobileOptimized" content="320">
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="css/font-awesome.css" rel="stylesheet" type="text/css">
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/style-responsive.css" rel="stylesheet" type="text/css">
<!-- END THEME STYLES -->
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed page-sidebar-closed">
<!-- BEGIN HEADER -->
<div class="header navbar navbar-inverse navbar-fixed-top">
	<!-- BEGIN TOP NAVIGATION BAR -->
	<div class="header-inner">
		<!-- BEGIN LOGO -->
		<a class="navbar-brand" href="/dashboard/index.php" style="margin:0; padding:2px;">
		<img src="/static/img/logo.png" alt="Arzttermine.de" class="img-responsive" style="height:38px;">
		</a>
		<!-- END LOGO -->
		<!-- BEGIN TOP NAVIGATION MENU -->
	<!-- END TOP NAVIGATION MENU -->
</div>
<!-- END TOP NAVIGATION BAR -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN SIDEBAR -->
<!-- END SIDEBAR -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title">Dashboard</h3>
				</div>
			</div>
			<!-- END PAGE HEADER-->

			<!-- BEGIN OVERVIEW STATISTIC BARS-->
			<div class="row stats-overview-cont">
				<div class="col-md-2 col-sm-4">
					<div class="stats-overview stat-block">
						<div class="details">
							<div class="title">Bookings <small>today</small></div>
							<div class="numbers"><?php echo $statistic->count('bookings_today'); ?></div>
							<div class="progress"><span style="width: 100%;" class="progress-bar progress-bar-danger" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></span></div>
						</div>
					</div>
				</div>
				<div class="col-md-2 col-sm-4">
					<div class="stats-overview stat-block">
						<div class="details">
							<div class="title">Bookings assigned <small>today</small></div>
							<div class="numbers"><?php echo $statistic->count('bookings_today_received'); ?></div>
							<div class="progress"><span style="width: 100%;" class="progress-bar progress-bar-danger" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></span></div>
						</div>
					</div>
				</div>
				<div class="col-md-2 col-sm-4">
					<div class="stats-overview stat-block">
						<div class="details">
							<div class="title">Bookings invalid <small>today</small></div>
							<div class="numbers"><?php echo $statistic->count('bookings_today_invalid'); ?></div>
							<div class="progress"><span style="width: 100%;" class="progress-bar progress-bar-danger" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></span></div>
						</div>
					</div>
				</div>
				<div class="col-md-2 col-sm-4">
					<div class="stats-overview stat-block">
						<div class="details">
							<div class="title">Bookings Insurances</div>
							<div class="numbers">
								<?php echo $statistic->count('bookings_today_insurance_private'); ?> <small>private</small>
								/
								<?php echo $statistic->count('bookings_today_insurance_public'); ?> <small>public</small>
							</div>
							<div class="progress" style="background-color:#D9534F;"><span style="width: <?php echo $statistic->count('bookings_today_insurance_private_percent'); ?>%;" class="progress-bar progress-bar-success" aria-valuenow="<?php echo $statistic->count('bookings_today_insurance_private_percent'); ?>" aria-valuemin="0" aria-valuemax="100"></span></div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="stats-overview stat-block">
						<div class="details">
							<div class="title">Bookings Integrations</div>
							<div class="numbers">
								<?php echo $statistic->count('bookings_today_integration_integrated'); ?> <small>integrated</small>
								/
								<?php echo $statistic->count('bookings_today_integration_not_integrated'); ?> <small>not integrated</small>
								/
								<?php echo $statistic->count('bookings_today_integration_inquery'); ?> <small>inquery</small>
								/
								<?php echo $statistic->count('bookings_today_integration_telephone'); ?> <small>telephone</small>
								</div>
							<div class="progress"><span style="width: 100%;" class="progress-bar progress-bar-danger" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></span></div>
						</div>
					</div>
				</div>
			</div>
			<!-- END OVERVIEW STATISTIC BARS-->
			<div class="clearfix"></div>
			<div class="row">
				<div class="col-md-6 col-sm-12">
					<!-- BEGIN PORTLET-->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption"><i class="fa fa-bar-chart"></i>Bookings per day</div>
						</div>
						<div class="portlet-body">
							<div style="display: none;" id="statistics_bookings_day_loading">
								<img src="img/loading.gif" alt="loading">
							</div>
							<div style="display: block;" id="statistics_bookings_day_content" class="display-none">
								<div style="padding: 0px; position: relative;" id="statistics_bookings_day" class="chart">
									<canvas height="300" width="878" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 878px; height: 300px;" class="flot-base"></canvas>
									<canvas height="300" width="878" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 878px; height: 300px;" class="flot-overlay"></canvas>
<script type="text/javascript">
	function getBookingsDayToday() {
		return [<?php echo $statistic->getJSArray('bookings_day_today'); ?>];
	}
	function getBookingsDayYesterday() {
		return [<?php echo $statistic->getJSArray('bookings_day_yesterday'); ?>];
	}
	function getBookingsDayLastWeek() {
		return [<?php echo $statistic->getJSArray('bookings_day_lastweek'); ?>];
	}
</script>
								</div>
							</div>
						</div>
					</div>
					<!-- END PORTLET-->
				</div>
				<div class="col-md-6 col-sm-12">
					<!-- BEGIN PORTLET-->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption"><i class="fa fa-bar-chart"></i>Bookings per day <small>last 12 weeks</small></div>
						</div>
						<div class="portlet-body">
							<div style="display: none;" id="statistics_bookings_days_year_loading">
								<img src="img/loading.gif" alt="loading">
							</div>
							<div style="display: block;" id="statistics_bookings_days_year_content" class="display-none">
								<div style="padding: 0px; position: relative;" id="statistics_bookings_days_year" class="chart">
									<canvas height="300" width="878" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 878px; height: 300px;" class="flot-base"></canvas>
									<canvas height="300" width="878" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 878px; height: 300px;" class="flot-overlay"></canvas>
<script type="text/javascript">
	function getBookingsDaysYear() {
		return [<?php echo $statistic->getJSArray('bookings_days_year'); ?>];
	}
</script>
								</div>
							</div>
						</div>
					</div>
					<!-- END PORTLET-->
				</div>
				<div class="col-md-6 col-sm-12">
					<!-- BEGIN PORTLET-->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption"><i class="fa fa-bar-chart"></i>Bookings per week</div>
						</div>
						<div class="portlet-body">
							<div style="display: none;" id="statistics_bookings_weeks_loading">
								<img src="img/loading.gif" alt="loading">
							</div>
							<div style="display: block;" id="statistics_bookings_weeks_content" class="display-none">
								<div style="padding: 0px; position: relative;" id="statistics_bookings_weeks" class="chart">
									<canvas height="300" width="878" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 878px; height: 300px;" class="flot-base"></canvas>
									<canvas height="300" width="878" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 878px; height: 300px;" class="flot-overlay"></canvas>
<script type="text/javascript">
	function getBookingsWeeks() {
		return [<?php echo $statistic->getJSArray('bookings_weeks'); ?>];
	}
	function getBookingsWeeksXAxis() {
		return [<?php echo $statistic->getJSArray('bookings_weeks_xaxis'); ?>];
	}
</script>
								</div>
							</div>
						</div>
					</div>
					<!-- END PORTLET-->
				</div>
				<div class="col-md-6 col-sm-12">
					<!-- BEGIN PORTLET-->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption"><i class="fa fa-bar-chart"></i>Bookings per month</div>
						</div>
						<div class="portlet-body">
							<div style="display: none;" id="statistics_bookings_months_loading">
								<img src="img/loading.gif" alt="loading">
							</div>
							<div style="display: block;" id="statistics_bookings_months_content" class="display-none">
								<div style="padding: 0px; position: relative;" id="statistics_bookings_months" class="chart">
									<canvas height="300" width="878" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 878px; height: 300px;" class="flot-base"></canvas>
									<canvas height="300" width="878" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 878px; height: 300px;" class="flot-overlay"></canvas>
<script type="text/javascript">
	function getBookingsMonths() {
		return [<?php echo $statistic->getJSArray('bookings_months'); ?>];
	}
	function getBookingsMonthsXAxis() {
		return [<?php echo $statistic->getJSArray('bookings_months_xaxis'); ?>];
	}
</script>
								</div>
							</div>
						</div>
					</div>
					<!-- END PORTLET-->
				</div>
				<div class="clearfix"></div>
<!--
			<div class="row ">
				<div class="col-md-6 col-sm-6">
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption"><i class="fa fa-bell"></i> Recent Orders</div>
						</div>
						<div class="portlet-body">
							<div class="table-scrollable">
								<table class="table table-striped table-bordered table-hover">
								<thead>
								<tr>
									<th>From</th>
									<th>Contact</th>
									<th>Amount</th>
									<th></th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<td>
										<a href="#">Ikea</a>
									</td>
									<td>Elis Yong</td>
									<td>4560.60$
										<span class="label label-warning label-sm">Paid</span>
									</td>
									<td>
										<a href="#" class="btn btn-default btn-xs">View</a>
									</td>
								</tr>
								</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-bell"></i>Recent Orders
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-scrollable">
								<table class="table table-striped table-bordered table-hover">
								<thead>
								<tr>
									<th>From</th>
									<th>Contact</th>
									<th>Amount</th>
									<th></th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<td><a href="#">Ikea</a></td>
									<td>Elis Yong</td>
									<td>4560.60$
										<span class="label label-warning label-sm">Paid</span>
									</td>
									<td>
										<a href="#" class="btn btn-default btn-xs">View</a>
									</td>
								</tr>
								</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix">
			-->
			</div>

<!-- DOCTORS ******************************************************************** -->
			<div class="clearfix"></div>
			<!-- BEGIN OVERVIEW STATISTIC BARS-->
			<div class="row stats-overview-cont">
				<div class="col-md-2 col-sm-4">
					<div class="stats-overview stat-block">
						<div class="details">
							<div class="title">Doctors <small>all</small></div>
							<div class="numbers"><?php echo $statistic->count('doctors_all'); ?></div>
							<div class="progress"><span style="width: 100%;" class="progress-bar progress-bar-success" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></span></div>
						</div>
					</div>
				</div>
				<div class="col-md-2 col-sm-4">
					<div class="stats-overview stat-block">
						<div class="details">
							<div class="title">Doctors <small>with contract</small></div>
                            <div class="numbers">
                                <?php echo $statistic->count('doctors_signed'); ?>
                            </div>
                            <div class="title"><small>draft / active / visible / visible + appointments</small></div>
                            <div class="numbers">
                                <?php echo $statistic->count('doctors_signed_draft'); ?>
                                /
                                <?php echo $statistic->count('doctors_signed_active'); ?>
                                /
                                <?php echo $statistic->count('doctors_signed_visible'); ?>
                                /
                                <?php echo $statistic->count('doctors_signed_visible_appointments'); ?>
                            </div>
							<div class="progress"><span style="width: 100%;" class="progress-bar progress-bar-success" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></span></div>
						</div>
					</div>
				</div>
				<div class="col-md-2 col-sm-4">
					<div class="stats-overview stat-block">
						<div class="details">
							<div class="title">Practises</div>
							<div class="numbers"><?php echo $statistic->count('locations_all'); ?></div>
							<div class="progress"><span style="width: 100%;" class="progress-bar progress-bar-success" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></span></div>
						</div>
					</div>
				</div>
			</div>
			<!-- END OVERVIEW STATISTIC BARS-->
			<div class="clearfix"></div>

<!-- REVIEWS ******************************************************************** -->
			<div class="clearfix"></div>
			<!-- BEGIN OVERVIEW STATISTIC BARS-->
			<div class="row stats-overview-cont">
				<div class="col-md-2 col-sm-4">
					<div class="stats-overview stat-block">
						<div class="details">
							<div class="title">Reviews <small>today</small></div>
							<div class="numbers"><?php echo $statistic->count('reviews_today'); ?></div>
							<div class="progress"><span style="width: 100%;" class="progress-bar progress-bar-info" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></span></div>
						</div>
					</div>
				</div>
				<div class="col-md-2 col-sm-4">
					<div class="stats-overview stat-block">
						<div class="details">
							<div class="title">Reviews <small>all / approved</small></div>
							<div class="numbers"><?php echo $statistic->count('reviews_all'); ?> / <?php echo $statistic->count('reviews_all_approved'); ?></div>
							<div class="progress"><span style="width: 100%;" class="progress-bar progress-bar-info" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></span></div>
						</div>
					</div>
				</div>
				<div class="col-md-2 col-sm-4">
					<div class="stats-overview stat-block">
						<div class="details">
							<div class="title">Reviews average <small>over all</small></div>
							<div class="numbers"><?php echo $statistic->count('reviews_average_all'); ?> / 5</div>
							<div class="progress"><span style="width: 100%;" class="progress-bar progress-bar-info" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></span></div>
						</div>
					</div>
				</div>
			</div>
			<!-- END OVERVIEW STATISTIC BARS-->
			<div class="clearfix"></div>

			<div class="row ">
				<div class="col-md-6 col-sm-12">
					<!-- BEGIN PORTLET-->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption"><i class="fa fa-bar-chart"></i>Reviews per day <small>last 12 weeks</small></div>
						</div>
						<div class="portlet-body">
							<div style="display: none;" id="statistics_reviews_days_year_loading">
								<img src="img/loading.gif" alt="loading">
							</div>
							<div style="display: block;" id="statistics_reviews_days_year_content" class="display-none">
								<div style="padding: 0px; position: relative;" id="statistics_reviews_days_year" class="chart">
									<canvas height="300" width="878" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 878px; height: 300px;" class="flot-base"></canvas>
									<canvas height="300" width="878" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 878px; height: 300px;" class="flot-overlay"></canvas>
<script type="text/javascript">
	function getReviewsDaysYear() {
		return [<?php echo $statistic->getJSArray('reviews_days_year'); ?>];
	}
</script>
								</div>
							</div>
						</div>
					</div>
					<!-- END PORTLET-->
				</div>
			</div>

		</div>
	</div>
</div>
<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="footer"></div>
<!-- END FOOTER -->

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<script src="js/jquery-1.js" type="text/javascript"></script>
<script src="js/bootstrap.js" type="text/javascript"></script>

<!-- graph -->
<script src="js/jquery_003.js" type="text/javascript"></script>
<script src="js/jquery.flot.time.js" type="text/javascript"></script>
<!-- dynamic graph on page resize -->
<script src="js/jquery_013.js" type="text/javascript"></script>

<script src="js/index.js" type="text/javascript"></script>

<script>
jQuery(document).ready(function() {    
   Index.initCharts();
});
</script>

</body>
</html>
