var Index = function () {

	return {
        initCharts: function () {
            if (!jQuery.plot) {
                return;
            }

            var data = [];

            function showTooltip(title, pos_x, pos_y, value_x, value_y) {
                $('<div id="tooltip" class="chart-tooltip"><div class="date">' + title + '<\/div><div class="label label-success">' + value_y + '<\/div><div class="label label-danger">' + value_x + '<\/div><\/div>').css({
                    position: 'absolute',
                    display: 'none',
                    top: pos_y - 100,
                    width: 80,
                    left: pos_x - 40,
                    border: '0px solid #ccc',
                    padding: '2px 6px',
                    'background-color': '#fff',
                }).appendTo("body").fadeIn(200);
            }

            // bookings per day ************************************
            var bookingsDayToday = getBookingsDayToday();
            var bookingsDayYesterday = getBookingsDayYesterday();
            var bookingsDayLastWeek = getBookingsDayLastWeek();

            if ($('#statistics_bookings_day').size() != 0) {

                $('#statistics_bookings_day_loading').hide();
                $('#statistics_bookings_day_content').show();

                var plot_statistics = $.plot($("#statistics_bookings_day"), [{
                        data: bookingsDayToday,
                        label: "today"
                    }, {
                        data: bookingsDayYesterday,
                        label: "yesterday"
                    }, {
                        data: bookingsDayLastWeek,
                        label: "last week"
                    }
                ], {
                    series: {
                        lines: {
                            show: true,
                            lineWidth: 2,
                            fill: true,
                            fillColor: {
                                colors: [{
                                        opacity: 0.05
                                    }, {
                                        opacity: 0.01
                                    }
                                ]
                            }
                        },
                        points: {
                            show: true
                        },
                        shadowSize: 2
                    },
                    grid: {
                        hoverable: true,
                        clickable: true,
                        tickColor: "#eee",
                        borderWidth: 0
                    },
                    colors: ["#d12610", "#5CB85C", "#37b7f3"],
                    xaxis: {
                        ticks: 11,
                        tickDecimals: 0
                    },
                    yaxis: {
                        ticks: 11,
                        tickDecimals: 0
                    },
                    legend: {
                        position: "nw"
                    }
                });

                // https://github.com/flot/flot/blob/master/API.md
                var previousPoint = null;
                $("#statistics_bookings_day").bind("plothover", function (event, pos, item) {
                    $("#x").text(pos.x.toFixed(2));
                    $("#y").text(pos.y.toFixed(2));
                    if (item) {
                        if (previousPoint != item.dataIndex) {
                            previousPoint = item.dataIndex;

                            $("#tooltip").remove();
                            var x = item.datapoint[0].toFixed(2),
                                y = item.datapoint[1].toFixed(2);

                            showTooltip(item.series.label, item.pageX, item.pageY, Math.ceil(x) + ':00h', Math.ceil(y));
                        }
                    } else {
                        $("#tooltip").remove();
                        previousPoint = null;
                    }
                });
            }

            // bookings per day / last weeks ************************************
            var bookingsDaysYear = getBookingsDaysYear();

            if ($('#statistics_bookings_days_year').size() != 0) {

                $('#statistics_bookings_days_year_loading').hide();
                $('#statistics_bookings_days_year_content').show();

                var plot_statistics = $.plot($("#statistics_bookings_days_year"), [{
                        data: bookingsDaysYear,
                        label: "bookings per day"
                    }
                ], {
                    series: {
                        lines: {
                            show: true,
                            lineWidth: 1,
                            fill: true,
                            fillColor: {
                                colors: [{
                                        opacity: 0.15
                                    }, {
                                        opacity: 0.01
                                    }
                                ]
                            }
                        },
                        points: {
                            show: false
                        },
                        shadowSize: 2
                    },
                    grid: {
                        hoverable: true,
                        clickable: true,
                        tickColor: "#eee",
                        borderWidth: 0
                    },
                    colors: ["#d12610", "#5CB85C", "#37b7f3"],
                    xaxis: {
                        mode: "time",
                        timeformat: "%d.%m.%Y"
                    },
                    yaxis: {
                        ticks: 11,
                        tickDecimals: 0
                    },
                    legend: {
                        position: "nw"
                    }
                });

                // https://github.com/flot/flot/blob/master/API.md
                var previousPoint = null;
                $("#statistics_bookings_days_year").bind("plothover", function (event, pos, item) {
                    $("#x").text(pos.x.toFixed(2));
                    $("#y").text(pos.y.toFixed(2));
                    if (item) {
                        if (previousPoint != item.dataIndex) {
                            previousPoint = item.dataIndex;

                            $("#tooltip").remove();
                            var x = item.datapoint[0].toFixed(2),
                                y = item.datapoint[1].toFixed(2);

                            // cast to int!
                            var day = new Date(Math.floor( x ));
                            var datetext = day.getDate() + '.' + (day.getMonth() + 1) + '.' + day.getFullYear();
                            showTooltip(item.series.label, item.pageX, item.pageY, datetext, Math.ceil(y));
                        }
                    } else {
                        $("#tooltip").remove();
                        previousPoint = null;
                    }
                });
            }

            // bookings per week ************************************
            var bookingsWeeks = getBookingsWeeks();
            var bookingsWeeksXAxis = getBookingsWeeksXAxis();

            if ($('#statistics_bookings_weeks').size() != 0) {

                $('#statistics_bookings_weeks_loading').hide();
                $('#statistics_bookings_weeks_content').show();

                var plot_statistics = $.plot($("#statistics_bookings_weeks"), [{
                        data: bookingsWeeks,
                        label: "bookings per week"
                    }
                ], {
                    series: {
                        lines: {
                            show: true,
                            lineWidth: 1,
                            fill: true,
                            fillColor: {
                                colors: [{
                                        opacity: 0.15
                                    }, {
                                        opacity: 0.01
                                    }
                                ]
                            }
                        },
                        points: {
                            show: true
                        },
                        shadowSize: 2
                    },
                    grid: {
                        hoverable: true,
                        clickable: true,
                        tickColor: "#eee",
                        borderWidth: 0
                    },
                    colors: ["#d12610", "#5CB85C", "#37b7f3"],
                    xaxis: {
                    	ticks: bookingsWeeksXAxis
                    },
                    yaxis: {
                        ticks: 11,
                        tickDecimals: 0
                    },
                    legend: {
                        position: "nw"
                    }
                });

                // https://github.com/flot/flot/blob/master/API.md
                var previousPoint = null;
                $("#statistics_bookings_weeks").bind("plothover", function (event, pos, item) {
                    $("#x").text(pos.x.toFixed(2));
                    $("#y").text(pos.y.toFixed(2));
                    if (item) {
                        if (previousPoint != item.dataIndex) {
                            previousPoint = item.dataIndex;

                            $("#tooltip").remove();
                            var x = item.datapoint[0].toFixed(2),
                                y = item.datapoint[1].toFixed(2);

                            // hack to get the xaxis value
                            var text = bookingsWeeksXAxis[item.dataIndex].toString().split(",");
                            text = text[1];
                            showTooltip(item.series.label, item.pageX, item.pageY, text, Math.ceil(y));
                        }
                    } else {
                        $("#tooltip").remove();
                        previousPoint = null;
                    }
                });
            }
            
            // bookings per month ************************************
            var bookingsMonths = getBookingsMonths();
            var bookingsMonthsXAxis = getBookingsMonthsXAxis();

            if ($('#statistics_bookings_months').size() != 0) {

                $('#statistics_bookings_months_loading').hide();
                $('#statistics_bookings_months_content').show();

                var plot_statistics = $.plot($("#statistics_bookings_months"), [{
                        data: bookingsMonths,
                        label: "bookings per month"
                    }
                ], {
                    series: {
                        lines: {
                            show: true,
                            lineWidth: 1,
                            fill: true,
                            fillColor: {
                                colors: [{
                                        opacity: 0.15
                                    }, {
                                        opacity: 0.01
                                    }
                                ]
                            }
                        },
                        points: {
                            show: true
                        },
                        shadowSize: 2
                    },
                    grid: {
                        hoverable: true,
                        clickable: true,
                        tickColor: "#eee",
                        borderWidth: 0
                    },
                    colors: ["#d12610", "#5CB85C", "#37b7f3"],
                    xaxis: {
                    	ticks: bookingsMonthsXAxis
                    },
                    yaxis: {
                        ticks: 11,
                        tickDecimals: 0
                    },
                    legend: {
                        position: "nw"
                    }
                });

                // https://github.com/flot/flot/blob/master/API.md
                var previousPoint = null;
                $("#statistics_bookings_months").bind("plothover", function (event, pos, item) {
                    $("#x").text(pos.x.toFixed(2));
                    $("#y").text(pos.y.toFixed(2));
                    if (item) {
                        if (previousPoint != item.dataIndex) {
                            previousPoint = item.dataIndex;

                            $("#tooltip").remove();
                            var x = item.datapoint[0].toFixed(2),
                                y = item.datapoint[1].toFixed(2);

                            // hack to get the xaxis value
                            var text = bookingsMonthsXAxis[item.dataIndex].toString().split(",");
                            text = text[1];
                            showTooltip(item.series.label, item.pageX, item.pageY, text, Math.ceil(y));
                        }
                    } else {
                        $("#tooltip").remove();
                        previousPoint = null;
                    }
                });
            }

            // reviews per day / last weeks ************************************
            var reviewsDaysYear = getReviewsDaysYear();

            if ($('#statistics_reviews_days_year').size() != 0) {

                $('#statistics_reviews_days_year_loading').hide();
                $('#statistics_reviews_days_year_content').show();

                var plot_statistics = $.plot($("#statistics_reviews_days_year"), [{
                        data: reviewsDaysYear,
                        label: "reviews per day"
                    }
                ], {
                    series: {
                        lines: {
                            show: true,
                            lineWidth: 1,
                            fill: true,
                            fillColor: {
                                colors: [{
                                        opacity: 0.15
                                    }, {
                                        opacity: 0.01
                                    }
                                ]
                            }
                        },
                        points: {
                            show: false
                        },
                        shadowSize: 2
                    },
                    grid: {
                        hoverable: true,
                        clickable: true,
                        tickColor: "#eee",
                        borderWidth: 0
                    },
                    colors: ["#37b7f3"],
                    xaxis: {
                        mode: "time",
                        timeformat: "%d.%m.%Y"
                    },
                    yaxis: {
                        ticks: 11,
                        tickDecimals: 0
                    },
                    legend: {
                        position: "nw"
                    }
                });

                // https://github.com/flot/flot/blob/master/API.md
                var previousPoint = null;
                $("#statistics_reviews_days_year").bind("plothover", function (event, pos, item) {
                    $("#x").text(pos.x.toFixed(2));
                    $("#y").text(pos.y.toFixed(2));
                    if (item) {
                        if (previousPoint != item.dataIndex) {
                            previousPoint = item.dataIndex;

                            $("#tooltip").remove();
                            var x = item.datapoint[0].toFixed(2),
                                y = item.datapoint[1].toFixed(2);

                            // cast to int!
                            var day = new Date(Math.floor( x ));
                            var datetext = day.getDate() + '.' + (day.getMonth() + 1) + '.' + day.getFullYear();
                            showTooltip(item.series.label, item.pageX, item.pageY, datetext, Math.ceil(y));
                        }
                    } else {
                        $("#tooltip").remove();
                        previousPoint = null;
                    }
                });
            }

        },

    };

}();