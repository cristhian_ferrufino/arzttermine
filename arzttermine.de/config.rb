# Remove the random string appended to sprite
# http://stackoverflow.com/questions/14173242/remove-the-random-string-appended-to-sprite-filename-with-compass-sass
asset_cache_buster :none

# Make a copy of sprites with a name that has no uniqueness of the hash.
on_sprite_saved do |filename|
  if File.exists?(filename)
    FileUtils.mv filename, filename.gsub(%r{-s[a-z0-9]{10}\.png$}, '.png')
  end
end

# Replace in stylesheets generated references to sprites
# by their counterparts without the hash uniqueness.
on_stylesheet_saved do |filename|
  if File.exists?(filename)
    css = File.read filename
    File.open(filename, 'w+') do |f|
      f << css.gsub(%r{-s([a-z0-9]{10})\.png}, '.png?v\1')
    end
  end
end
# END - Remove the random string appended to sprite