<?php

namespace Arzttermine\Blog;

use Arzttermine\Application\Application;

class Blog {

    /**
     * @var array
     */
    protected $posts = array();

    /**
     * @var array
     */
    protected $authors = array();

    /**
     * Loads all blogposts into $this->posts
     *
     * @param int $offset
     * @param int $row_count
     * @param bool $show_admin_view
     *
     * @return int
     */
    public function loadPosts($offset = 0, $row_count = 0, $show_admin_view = false)
    {
        $posts = array();
        $post = new Post();
        $sql_published = '';

        if (!$show_admin_view) {
            $sql_published = ' AND published_at < now() AND status=' . Post::STATUS_PUBLISHED;
        }

        $sql_limit = ' LIMIT ' . $offset . ',' . $row_count;
        $post_ids = $post->findKeys('1=1 ' . $sql_published . ' ORDER BY published_at DESC ' . $sql_limit);

        if (!empty($post_ids) && is_array($post_ids)) {
            foreach ($post_ids as $id) {
                $_post = new Post($id);
                if ($_post->isValid()) {
                    $posts[] = $_post;
                }
            }
        }

        $this->setPosts($posts);

        return count($posts);
    }

    /**
     * Counts the number of loaded blogposts
     *
     * @return int
     */
    public function countLoadedPosts()
    {
        return count($this->getPosts());
    }

    /**
     * Counts the number of all blogposts
     *
     * @param bool $count_unpublished
     *
     * @return int
     */
    public function countPosts($count_unpublished = false)
    {
        $post = new Post();
        $sql_published = '';

        if (!$count_unpublished) {
            $sql_published = ' AND published_at < now() AND status=' . Post::STATUS_PUBLISHED;
        }

        $num = $post->count('1=1 ' . $sql_published);

        return (int)$num;
    }

    /**
     * Returns the title for this blog
     *
     * @return string
     */
    public function getTitle()
    {
        return Application::getInstance()->static_text('blog_title');
    }

    /**
     * @param array $authors
     *
     * @return self
     */
    public function setAuthors(array $authors)
    {
        $this->authors = $authors;

        return $this;
    }

    /**
     * @return array
     */
    public function getAuthors()
    {
        return $this->authors;
    }

    /**
     * @param array $posts
     *
     * @return self
     */
    public function setPosts(array $posts)
    {
        $this->posts = $posts;

        return $this;
    }

    /**
     * @return array
     */
    public function getPosts()
    {
        return $this->posts;
    }
}