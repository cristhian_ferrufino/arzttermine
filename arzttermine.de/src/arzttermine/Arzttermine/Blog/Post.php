<?php

namespace Arzttermine\Blog;

use Arzttermine\Application\Application;
use Arzttermine\Core\Basic;
use Arzttermine\User\User;

class Post extends Basic {

    const STATUS_DRAFT = 0;
    const STATUS_PUBLISHED = 1;

    /**
     * @var string
     */
    protected $tablename = DB_TABLENAME_BLOGPOSTS;

    /**
     * @var string
     */
    protected $slug;

    /**
     * @var string
     */
    protected $status;

    /**
     * @var string
     */
    protected $topic;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $body;

    /**
     * @var Post
     */
    protected $prev = '';

    /**
     * @var Post
     */
    protected $next = '';

    /**
     * @var string
     */
    protected $created_at;

    /**
     * @var string
     */
    protected $created_by;

    /**
     * @var string
     */
    protected $updated_at = '';

    /**
     * @var string
     */
    protected $updated_by = '';

    /**
     * @var string
     */
    protected $published_at = '';

    /**
     * @var string
     */
    protected $url;

    /**
     * @var array
     */
    protected $comments;

    /**
     * @var string
     */
    protected $author = '';

    /**
     * @var array
     */
    protected $all_keys = array(
        "id",
        "slug",
        "status",
        "topic",
        "title",
        "body",
        "created_at",
        "created_by",
        "updated_at",
        "updated_by",
        "published_at"
    );

    /**
     * @var array
     */
    protected $secure_keys = array(
        "slug",
        "status",
        "topic",
        "title",
        "body",
        "created_at",
        "created_by",
        "updated_at",
        "updated_by",
        "published_at"
    );

    /**
     * Calc some vars
     *
     * @return true
     */
    public function calcVars()
    {
        $this->setUrl('/news/' . $this->getSlug());

        $user = new User($this->created_by);

        if ($user->isValid()) {
            $this->author = $user->getFullName();
        }

        return true;
    }

    /**
     * @return string
     */
    public function getAdminEditUrl()
    {
        return '/administration/news?action=edit&id=' . $this->getId();
    }

    /**
     * @param string $url
     *
     * @return self
     */
    public function setUrl($url)
    {
        $this->url = trim($url);

        return $this;
    }

    /**
     * @param bool $absolute
     * @param bool $secure
     *
     * @return string
     */
    public function getUrl($absolute = false, $secure = false)
    {
        return ($absolute ? ($secure ? $GLOBALS['CONFIG']['URL_HTTPS_LIVE'] : $GLOBALS['CONFIG']['URL_HTTP_LIVE']) : '') . $this->url;
    }

    /**
     * @param Post $next
     *
     * @return self
     */
    public function setNext(Post $next)
    {
        $this->next = $next;

        return $this;
    }

    /**
     * @return Post
     */
    public function getNext()
    {
        return $this->next;
    }

    /**
     * @param Post $prev
     *
     * @return self
     */
    public function setPrev(Post $prev)
    {
        $this->prev = $prev;

        return $this;
    }

    /**
     * @return Post
     */
    public function getPrev()
    {
        return $this->prev;
    }

    /**
     * @param string $updated_at
     *
     * @return self
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param string $updated_by
     *
     * @return self
     */
    public function setUpdatedBy($updated_by)
    {
        $this->updated_by = $updated_by;

        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedBy()
    {
        return $this->updated_by;
    }

    /**
     * @param string $published_at
     *
     * @return self
     */
    public function setPublishedAt($published_at)
    {
        $this->published_at = $published_at;

        return $this;
    }

    /**
     * @return string
     */
    public function getPublishedAt()
    {
        return $this->published_at;
    }

    /**
     * @param string $body
     *
     * @return self
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param array $comments
     *
     * @return self
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * @return array
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param string $slug
     *
     * @return self
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $status
     *
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Shows if the blogpost is public and can be shown
     *
     * @return string
     */
    public function isPublished()
    {
        if ($this->status != BLOGPOST_STATUS_PUBLISHED) {
            return false;
        }

        if ($this->published_at == '0000-00-00 00:00:00') {
            return false;
        }

        $now = Application::getInstance()->now('timestamp');
        $then = mysql2date('U', $this->published_at);

        return !($now < $then);
    }

    /**
     * Get extended entry info (<!--more-->).
     *
     * There should not be any space after the second dash and before the word
     * 'more'. There can be text or space(s) after the word 'more', but won't be
     * referenced.
     *
     * The returned array has 'main' and 'extended' keys. Main has the text before
     * the <code><!--more--></code>. The 'extended' key has the content after the
     * <code><!--more--></code> comment.
     *
     * @since 1.0.0
     *
     * @param string $post Post content.
     *
     * @return array Post before ('main') and after ('extended').
     */
    public function get_extended($post)
    {
        // Match the new style more links
        if (preg_match('/<!--more(.*?)?-->/', $post, $matches)) {
            list($main, $extended) = explode($matches[0], $post, 2);
        } else {
            $main = $post;
            $extended = '';
        }

        // Strip leading and trailing whitespace
        $main = preg_replace('/^[\s]*(.*)[\s]*$/', '\\1', $main);
        $extended = preg_replace('/^[\s]*(.*)[\s]*$/', '\\1', $extended);

        return array('main'     => $main,
                     'extended' => $extended
        );
    }

    /**
     * Renders this blogpost for html output
     *
     * @param bool $only_teaser
     * @param bool $add_teaser_image
     *
     * @return string
     */
    public function getBlogPostHtml($only_teaser = false, $add_teaser_image = false)
    {
        $content = $this->get_extended($this->body);

        if ($only_teaser) {
            $content['main'] .= '<a href="' . $this->getUrl() . '" class="more-link">Weiterlesen</a>';
        }

        if ($add_teaser_image) {
            if ($this->countGfxs() > 0) {
                // Add the gfx only if there not already one
                if (false === strpos($content['main'], '[gfx')) {
                    $_content['main'] = '[gfx]' . $content['main'];
                }
            }
        }

        // add the news_id to let the shortcode know where we are
        $content['main'] = str_replace('[gfx', '[gfx news=' . $this->id, $content['main']);
        $content['extended'] = str_replace('[gfx', '[gfx news=' . $this->id, $content['extended']);
        $html = do_shortcode(wpautop($content['main']));

        if (!$only_teaser && $content['extended'] != '') {
            $html .= do_shortcode(wpautop($content['extended']));
        }

        return $html;
    }

    /**
     * Renders this blogpost for html output and filters out some bad things
     *
     * @return string
     */
    public function getRssBlogPostHtml()
    {
        $content = str_replace('[gfx', '[gfx news=' . $this->id, $this->body);
        $content = preg_replace('/<!--more(.*?)?-->/', '', $content);
        $html = do_shortcode(wpautop($content));

        return str_replace(']]>', ']]&gt;', $html);
    }

    /**
     * Renders a excerpt for this blogpost for html output
     *
     * @param int $excerpt_length
     *
     * @return string
     */
    public function getExcerpt($excerpt_length = 55)
    {
        return getExcerpt($this->body, $excerpt_length);
    }

    /**
     * Returns the url to this blogpost
     *
     * @param bool $absolute_url
     *
     * @return string a href
     */
    public function getUrlComments($absolute_url = false)
    {
        return $this->getUrl($absolute_url) . '#comments';
    }

    /**
     * Returns the title for this blogpost
     *
     * @param int $length
     *
     * @return string
     */
    public function getTitle($length = 0)
    {
        if (isset($this->title)) {
            if ($length > 0) {
                $_title = mb_substr($this->title, 0, $length, $GLOBALS['CONFIG']['SYSTEM_CHARSET']);

                if (mb_strlen($this->title, $GLOBALS['CONFIG']['SYSTEM_CHARSET']) > $length) {
                    $_title .= '...';
                }

                return $_title;
            } else {
                return $this->title;
            }
        }

        return '';
    }

    /**
     * Returns the tags for this blogpost
     *
     * @return string a href
     */
    public function getTags()
    {
        if (isset($this->tags)) {
            return $this->tags;
        }

        return '';
    }

    /**
     * Returns the urlencoded tags for this blogpost
     *
     * @return string
     */
    public function getTagsUrlencoded()
    {
        $tags = $this->getTags();
        $_array = explode(',', $tags);

        // go through and urlencode
        foreach ($_array as $row => $tag) {
            $_array[$row] = urlencode(trim($tag));
        }

        $tags = implode(',', $_array);

        return $tags;
    }

    /**
     * Returns the topic for this newspost
     *
     * @param int $word_count
     *
     * @return string
     */
    public function getTopic($word_count = 0)
    {
        if (isset($this->topic)) {
            if ($word_count > 0) {
                return getExcerpt($this->topic, $word_count);
            } else {
                return $this->topic;
            }
        }

        return '';
    }

    /**
     * Returns the author for this blogpost
     *
     * @return string a href
     */
    public function getAuthor()
    {
        if (isset($this->author)) {
            return $this->author;
        }

        return '';
    }

    /**
     * Checks if the blogpost is published depending on the published_at date
     * This does NOT check if the status is published or draft!
     *
     * @return false=not published | true=is published
     */
    public function isPublishedNow()
    {
        return (mysql2date('U', $this->published_at) < Application::getInstance()->now('timestamp'));
    }

    /**
     * Returns the human time diff until the blogpost will be published
     * Will return '' if the blogpost is allready published
     *
     * @return string human time diff
     */
    public function getPublishedAtHumanTimeDiff()
    {
        if ($this->isPublishedNow()) {
            return '';
        }

        return human_time_diff(mysql2date('U', $this->published_at), Application::getInstance()->now('timestamp'));
    }

    /**
     * Loads the previous and next blogpost links and
     * stores them in $this->next and $this->prev
     *
     * @return bool false=error | true=success
     */
    public function loadPrevNextBlogPostLinks()
    {
        $prev_id = $this->findKey(
            'status=' . BLOGPOST_STATUS_PUBLISHED .
            ' AND published_at < now() ' .
            ' AND published_at < "' . $this->getPublishedAt() . '" ' .
            ' AND id != ' . $this->getId() .
            ' ORDER BY published_at DESC ' .
            ' LIMIT 1 '
        );

        $previous_blogpost = new Post($prev_id);
        $this->setPrev($previous_blogpost);

        $next_id = $this->findKey(
            'status=' . BLOGPOST_STATUS_PUBLISHED .
            ' AND published_at < now() ' .
            ' AND published_at > "' . $this->getPublishedAt() . '" ' .
            ' AND id != ' . $this->getId() .
            ' ORDER BY published_at ASC ' .
            ' LIMIT 1 '
        );

        $next_blogpost = new Post($next_id);
        $this->setNext($next_blogpost);

        return true;
    }

    /**
     * Returns the social bookmarks code
     *
     * @return string
     */
    public function getSocialBookmarksCode()
    {
        return '<div class="social_bookmarks" href="' . $this->getPermalink() . '"></div>';
    }

    /**
     * Returns the permalink
     *
     * @return string
     */
    public function getPermalink()
    {
        return $this->getUrl('url', true);
    }
}