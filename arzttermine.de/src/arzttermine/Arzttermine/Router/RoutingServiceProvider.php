<?php

namespace Arzttermine\Router;

use Arzttermine\DependencyInjection\ServiceProviderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\EventListener\RouterListener;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Router;

class RoutingServiceProvider implements ServiceProviderInterface {

    /**
     * {@inheritdoc}
     */
    public function register(ContainerInterface $container)
    {
        $options = array();
        $config = $container->getParameter('router.resource');
        
        if ($container->hasParameter('router.cache_dir') &&  file_exists($container->getParameter('router.cache_dir'))) {
            $options['cache_dir'] = $container->getParameter('router.cache_dir');
        }

        $context = new RequestContext();
        $context->fromRequest($container->get('request'));
        
        $container->set('request_context', $context);

        if (file_exists($config)) {
            $route_loader = new RouteLoader($config);

            $router = new Router($route_loader, $config, $options, $context);
            $router->getContext()->setBaseUrl('');

            $container->set('router', $router);
        } else {
            throw new \Exception('Router does not exist');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function boot(ContainerInterface $container)
    {
        /** @var \Symfony\Component\EventDispatcher\EventDispatcher $dispatcher */
        $dispatcher = $container->get('dispatcher');
        
        $dispatcher->addSubscriber(new RouterListener(
            $container->get('router'),
            $container->get('request_stack'),
            $container->get('request_context'),
            $container->has('logger') ? $container->get('logger') : null
        ));
    }
}