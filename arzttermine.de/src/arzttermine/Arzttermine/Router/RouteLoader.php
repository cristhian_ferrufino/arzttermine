<?php

namespace Arzttermine\Router;

use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Routing\Loader\PhpFileLoader;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\RouteCollection;

class RouteLoader extends Loader {

    /**
     * Routing configuration file
     * 
     * @var string
     */
    private $configuration = '';

    /**
     * @param string $routing_configuration
     */
    public function __construct($routing_configuration)
    {
        $this->configuration = trim($routing_configuration);
    }
    
    /**
     * @param mixed $resource
     * @param string $type
     * 
     * @return RouteCollection
     */
    public function load($resource, $type = null)
    {
        $collection = new RouteCollection();
        
        // Load the CMS cache if it exists
        try {
            $cache_locator = new FileLocator(CACHE_PATH);
            $cache_loader = new PhpFileLoader($cache_locator);
            $collection->addCollection($cache_loader->load('cms_routing.php'));
        } catch (\Exception $e) {
            // Ignore
        }

        if (!empty($this->configuration)) {
            try {
                $locator = new FileLocator(dirname($this->configuration));
                $loader = new YamlFileLoader($locator);
                $collection->addCollection($loader->load($resource, 'yaml'));
            } catch (\Exception $e) {
                // Ignore any exception
            }
        }
        
        return $collection;
    }

    /**
     * @param mixed $resource
     * @param string $type
     *
     * @return bool
     */
    public function supports($resource, $type = null)
    {
        return $type === 'arzttermine_db_cms';
    }
    
}