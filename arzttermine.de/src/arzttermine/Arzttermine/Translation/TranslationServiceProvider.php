<?php

namespace Arzttermine\Translation;

use Arzttermine\DependencyInjection\ServiceProviderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Translation\Loader\ArrayLoader;
use Symfony\Component\Translation\Loader\YamlFileLoader;
use Symfony\Component\Translation\Translator;

class TranslationServiceProvider implements ServiceProviderInterface {

    /**
     * @var ContainerInterface
     */
    private $container;
    
    /**
     * {@inheritdoc}
     */
    public function register(ContainerInterface $container)
    {
        /** @var Translator $translator */
        $translator = new Translator($container->getParameter('translator.default_locale'));
        
        // Add a default loader for this service
        $translator->addLoader('yaml', new YamlFileLoader());
        $translator->addLoader('array', new ArrayLoader());
        
        // German is our default (hardcoded)
        $translator->setFallbackLocales(array('de'));
        
        $translation_sources = $container->getParameter('translator.sources');
        
        foreach ($translation_sources as $type => $source) {
            foreach ($source as $locale => $catalogue) {
                if (file_exists($catalogue)) {
                    if (strtolower(pathinfo($catalogue, PATHINFO_EXTENSION)) === 'php') {
                        $catalogue = include $catalogue;
                    }
                    
                    $translator->addResource($type, $catalogue, $locale);
                }
            }
        }
        
        $container->set('translator', $translator);

        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function boot(ContainerInterface $container)
    {
        if ($container->has('dispatcher')) {
            /** @var \Symfony\Component\EventDispatcher\EventDispatcher $dispatcher */
            $dispatcher = $container->get('dispatcher');
            $dispatcher->addListener(KernelEvents::REQUEST, array($this, 'onKernelRequest'), 17);
        }
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        
        // The locale will come from the query string OR request attributes
        $locale = $request->query->get('lang', $request->attributes->get('_locale'));

        // If it exists, save it
        if ($locale) {
            $request->getSession()->set('_locale', $locale);
        } else {
            // Try and load it from the session, or use the default
            $locale = $request->getSession()->get('_locale', $this->container->getParameter('translator.default_locale'));
        }
        
        $request->setLocale($locale);
        
        // Workaround for translator not reading from Request
        $this->container->get('translator')->setLocale($locale);
    }

}