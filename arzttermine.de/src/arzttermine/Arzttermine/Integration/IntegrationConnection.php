<?php

namespace Arzttermine\Integration;

use Arzttermine\Core\BasicNoIndex;
use Arzttermine\Location\Location;
use Arzttermine\User\User;

class IntegrationConnection extends BasicNoIndex {

    /**
     * @var string
     */
    protected $tablename = DB_TABLENAME_LOCATION_USER_CONNECTIONS;

    /**
     * @var int
     */
    protected $id = 0;

    /**
     * @var array
     */
    protected $all_keys
        = array(
            "user_id",
            "location_id",
        );

    /**
     * @var array
     */
    protected $secure_keys
        = array(
            "user_id",
            "location_id",
        );

    /**
     * Adds a user and location connections
     *
     * @param int $user_id
     * @param int $location_id
     * @param bool $validateData
     *
     * @return array $wrongIds
     */
    public function connect($user_id, $location_id, $validateData = true)
    {
        if ($validateData) {
            $user = new User($user_id);
            if (!$user->isValid()) return false;
            $location = new Location($location_id);
            if (!$location->isValid()) return false;
        }
        $data = array('user_id' => $user_id, 'location_id' => $location_id);
        return ($this->saveNew($data) != false) ? true : false;
    }

    /**
     * Checks if user and location fit by its integration_data
     *
     * @param int $user_id
     * @param int $location_id
     *
     * @return bool
     */
    public function isConnected($user_id, $location_id)
    {
        $user_id = intval($user_id);
        if (!is_numeric($user_id)) return false;
        $location_id = intval($location_id);
        if (!is_numeric($location_id)) return false;

        return $this->count("user_id={$user_id} AND location_id={$location_id}") > 0;
    }

    /**
     * Deletes all connections to a location
     *
     * @param int $location_id
     *
     * @return bool
     */
    public function deleteLocationConnections($location_id)
    {
        if (!is_numeric($location_id)) return false;

        return $this->deleteWhere('location_id='.$location_id);
    }
}
