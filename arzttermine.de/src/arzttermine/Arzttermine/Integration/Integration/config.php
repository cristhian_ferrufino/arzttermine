<?php

/**
 * NAME:                this name is uploaded to Salesforce
 * APPOINTMENT_ENQUIRY: these integrations have no concrete appointments
 */

$GLOBALS['CONFIG']['INTEGRATIONS'][1] = array(
    'ID'                   => 1,
    'NAME'                 => 'Sprechzeiten',
    'URL_BASE'             => '/sprechzeiten',
    'URL_ADMIN'            => '/admin/planners/index.php',
    'CLASS_NAME'           => '\Arzttermine\Integration\Integration\Arzttermine\Integration',
    'URL_LOGO'             => 'adm/integration_arzttermine-sprechzeiten.png',
    'ADMIN_MANUAL_BOOKING' => false,
    'ADMIN_MANUAL_MAILING' => true
);

$GLOBALS['CONFIG']['INTEGRATIONS'][2] = array(
    'ID'                        => 2,
    'NAME'                      => 'Terminland',
    'URL_BASE'                  => '/terminland',
    'URL_ADMIN'                 => '/admin/integrations/integration_2/index.php',
    'URL_TEST'                  => '/admin/integrations/integration_2/test.php',
    'CLASS_NAME'                => '\Arzttermine\Integration\Integration\Terminland\Integration',
    'URL_LOGO'                  => 'adm/integration_terminland.png',
    'ADMIN_MANUAL_BOOKING'      => true,
    'ADMIN_MANUAL_MAILING'      => true,
    'CACHE_ACTIVATE'            => true,
    'CACHE_UNVALID_TIME'        => '2 HOUR',
    // sql DATE_SUB-syntax
    'AUTOMATIC_BOOKING_PROCESS' => true,
    // The booking process is automated, so show print link, etc
);

$GLOBALS['CONFIG']['INTEGRATIONS'][5] = array(
    'ID'                            => 5,
    'NAME'                          => 'Samedi',
    'URL_BASE'                      => '/samedi',
    'URL_ADMIN'                     => '/admin/integrations/integration_5/index.php',
    'CLASS_NAME'                    => '\Arzttermine\Integration\Integration\Samedi\Integration',
    'URL_LOGO'                      => 'adm/integration_samedi.png',
    'ADMIN_MANUAL_BOOKING'          => true,
    'ADMIN_MANUAL_MAILING'          => true,
    'CACHE_ACTIVE'                  => true,
    'CACHE_VALID_MINUTES'           => '125',
    'EVENT_TYPES_CACHE_VALID_HOURS' => '5',
    // The booking process is automated, so show print link, etc
    'AUTOMATIC_BOOKING_PROCESS'     => true,
    // samedi appointments are connected to the insurance
    'DISALLOW_INSURANCE_SWITCH_IN_BOOKING_PROCESS'  => true
);

$GLOBALS['CONFIG']['INTEGRATIONS'][8] = array(
    'ID'                   => 8,
    'NAME'                 => 'Terminanfrage',
    'CLASS_NAME'           => '\Arzttermine\Integration\Integration\Terminanfrage\Integration',
    'ADMIN_MANUAL_BOOKING' => false,
    'ADMIN_MANUAL_MAILING' => true
);

$GLOBALS['CONFIG']['INTEGRATIONS'][9] = array(
    'ID'                   => 9,
    'NAME'                 => 'Sprechzeiten - Terminblock',
    'CLASS_NAME'           => '\Arzttermine\Integration\Integration\Terminblock\Integration',
    'ADMIN_MANUAL_BOOKING' => false,
    'ADMIN_MANUAL_MAILING' => true
);

$GLOBALS['CONFIG']['INTEGRATIONS'][10] = array(
    'ID'                                              => 10,
    'NAME'                                            => 'Charly',
    'URL_BASE'                                        => '/charly',
    'URL_ADMIN'                                       => '/admin/integrations/integration_10/index.php',
    'CLASS_NAME'                                      => '\Arzttermine\Integration\Integration\Charly\Integration',
    'URL_LOGO'                                        => 'adm/integration_charly.png',
    'ADMIN_MANUAL_BOOKING'                            => true,
    'ADMIN_MANUAL_MAILING'                            => true,
    /**
     * the ics files are stored in this directory in the form "user_id-location_id"
     * by charly's webdav upload process.
     * No trailing slash.
     */
    'WEBDAV_DOCUMENT_ROOT'                            => '/www/webdav.charly.arzttermine.de/htdocs/user',
    'HTPASSWD_FILENAME'                               => '/www/webdav.charly.htpasswd',
    /**
     * calculate this number of days ahead of CALENDAR_MAX_DAYS_AHEAD
     * in the cache table
     */
    'DAYS_TO_CALCULATE_AFTER_CALENDAR_MAX_DAYS_AHEAD' => 7
);