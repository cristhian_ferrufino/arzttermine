<?php

namespace Arzttermine\Integration\Integration\Terminland;

use Arzttermine\Application\Application;
use Arzttermine\Appointment\AppointmentService;
use Arzttermine\Appointment\Provider;
use Arzttermine\Appointment\Appointment;
use Arzttermine\Booking\Booking as PatientBooking;
use Arzttermine\Calendar\Calendar;
use Arzttermine\Integration\Cache as IntegrationCoreCache;
use Arzttermine\Integration\Integration as IntegrationCore;
use Arzttermine\Location\Location;
use Arzttermine\User\User;
use Arzttermine\Core\DateTime;
use Arzttermine\Core\Configuration;
use Arzttermine\Mail\SMS;

/**
 * Integration 2: TerminLand
 *
 * @author Axel Kuzmik <axel@kuzmik.de>
 *
 * Examples for possible SOAP-Requests:
 * https://www.terminland.de/arzttermine.de/tlsoap/default.asmx
 *
 */
class Integration extends IntegrationCore {

    const SOAP_USER_ID = 'arzttermine.de';
    const SOAP_PW = 'vice4termine';
    const SOAP_PROTOCOL = 'https';
    const SOAP_DOMAIN = 'www.terminland.de';
    const SOAP_PATH = '/tlsoap/default.asmx';

    /**
     * @var array
     */
    public static $intisoapmap = array();

    /**
     * @var int
     */
    protected $id = 2;

    /**
     * @var int
     */
    protected $user_id;

    /**
     * @var int
     */
    protected $location_id;

    /**
     * @var \SOAPClient
     */
    private $client;

    /**
     * @var int
     */
    private $countLastUpdatedAppointments;

    /**
     * Returns the status id
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @inheritdoc
     */
    public function processApiRequest()
    {
        return;
    }

    /**
     * Checks if user and location fit by its integration_data
     *
     * @param int $user_id
     * @param int $location_id
     *
     * @return bool
     */
    public function isConnected($user_id, $location_id)
    {
        $integration_data = new Data();

        return $integration_data->count('user_id=' . $user_id . ' AND location_id=' . $location_id) > 0;
    }

    /**
     * @inheritdoc
     */
    public function getAvailableAppointments(Provider $provider, $format = '')
    {
        $appointments = array();

        // Warning: convert the timestamps back to datetimes!
        $integration_data = new Data();
        $data = $integration_data->findObject('user_id=' . $provider->getUserId() . ' AND location_id=' . $provider->getLocationId());

        if (!$data->isValid()) {
            return $appointments;
        }

        $integration_cache = new IntegrationCoreCache();
        // @todo Convert parameter use to Provider
        $date_range = $provider->getDateRange();
        $start = reset($date_range);
        $end = end($date_range);
        
        $appointments = $integration_cache->getAvailableAppointments(
            $start->getDate(),
            $end->getDate(),
            $provider->getUserId(),
            $provider->getLocationId(),
            $provider->getInsuranceId()
        );

        return $appointments;
    }

    /**
     * @inheritdoc
     */
    public function getNextAvailableAppointment(Provider $provider, $format = '')
    {
        return array();
    }

    /**
     * Book an appointment
     *
     * @param PatientBooking $booking
     *
     * @return bool
     */
    public function bookAppointment(PatientBooking $booking)
    {
        $profile = Application::getInstance()->getCurrentUser();

        // check for invalid status
        if ($booking->getIntegrationStatus() == BOOKING_INTEGRATION_STATUS_BOOKED) {
            $this->setLastErrorMessage('This booking status is already BOOKED');

            return false;
        }

        if ($booking->getIntegrationStatus() == BOOKING_INTEGRATION_STATUS_CANCELED) {
            $this->setLastErrorMessage('This booking status is CANCELED');

            return false;
        }

        // Get terminland_id and terminplan_id
        $integration_data = new Data();
        /** @var Data $data */
        $data = $integration_data->findObject('user_id=' . $booking->getUserId() . ' AND location_id=' . $booking->getLocationId());
        if (!$data->isValid()) {
            $booking->saveStatus(PatientBooking::STATUS_INVALID, true);
            $this->setLastErrorMessage('There is no data in integration data about this doctor');

            return false;
        }

        // Store all integration booking informations
        // NOTE: Chaining broke here due to some bug presumably in PHP
        $integration_booking = new Booking();
        $integration_booking->setBookingId($booking->getId());
        $integration_booking->setStatus(Booking::STATUS_ERROR);
        $integration_booking->setFunctionId(Booking::FUNCTION_BOOK);
        $integration_booking->setTerminlandId($data->getTerminlandId());
        $integration_booking->setTerminplanId($data->getTerminplanId());
        $integration_booking->setCreatedAt(Application::getInstance()->now());
        $integration_booking->setCreatedBy($profile->getId());
        $integration_booking->addLogText(get_class($this) . "->bookAppointment();\n");

        if (!$integration_booking->saveNewObject()) {
            $booking->saveStatus(PatientBooking::STATUS_INVALID, true);
            $this->setLastErrorMessage('Can not save the booking in the system');

            return false;
        }
        
        // Book!
        $this->bookAppointmentTerminland($booking, $integration_booking, $data->getTerminlandId(), $data->getTerminplanId());
        $result = $integration_booking->save();
        
        if (!$result) {
            $booking->saveStatus(PatientBooking::STATUS_INVALID, true);
            $this->setLastErrorMessage('Can not save the booking in the system');

            return false;
        }

        // Store integration status in booking object
        if ($integration_booking->getStatus() == Booking::STATUS_OK) {
            $booking->saveIntegrationStatus(BOOKING_INTEGRATION_STATUS_BOOKED);

            /**
             * @todo: remove the appointment from table appointments here
             */

            SMS::sendBookingConfirmationSms($booking);
            PatientBooking::sendBookingEmail($booking, 'booking-new-confirmed.html');

            $booking->saveStatus(PatientBooking::STATUS_RECEIVED, true);

            return true;
        }

        $booking->saveIntegrationStatus(BOOKING_INTEGRATION_STATUS_ERROR);
        $booking->saveStatus(PatientBooking::STATUS_INVALID, true);

        return false;
    }

    /**
     * Book the appointment
     *
     * @param PatientBooking $booking
     * @param Booking $integration_booking
     * @param string $terminland_id
     * @param string $terminplan_id
     *
     * @return bool
     */
    public function bookAppointmentTerminland(PatientBooking $booking, Booking &$integration_booking, $terminland_id, $terminplan_id)
    {
        // Book only in production
        if ($GLOBALS['CONFIG']['SYSTEM_ENVIRONMENT'] != 'production') {
            $integration_booking->addLogText(get_class($this) . "->bookAppointmentTerminland();\nNo Terminland booking performed as not in production environment\n");
            $integration_booking->setStatus(Booking::STATUS_OK);

            return true;
        }

        $integration_booking->addLogText(get_class($this) . "->bookAppointmentTerminland();\n");
        $html = '';
        $initSoapResult = $this->initSoap($terminland_id, true);

        if ($initSoapResult !== true) {
            return false;
        }

        try {
            $xml = '<ns1:TerminReservieren2>' .
                   '<ns1:Terminplan>' . $terminplan_id . '</ns1:Terminplan>' .
                   '<ns1:Suchparameter><ns1:string>' . self::getInsuranceTextTerminland($booking->getInsuranceId()) . '</ns1:string></ns1:Suchparameter>' .
                   '<ns1:DatumZeit_UTC>' . Calendar::dateTime2UTC(
                       Calendar::LocalDateTime2UTC(
                           $booking->getAppointment()->getDateTime()
                       )
                   ) . '</ns1:DatumZeit_UTC>' .
                   '<ns1:ReservierungDauer>60</ns1:ReservierungDauer>' .
                   '</ns1:TerminReservieren2>';
            $result = $this->client->TerminReservieren2(new \SoapVar($xml, XSD_ANYXML));
            $errorCode = 0;
            $errorDescription = '';

            if (is_object($result)) {
                $html .= "<b>Request:</b><br />" . htmlspecialchars($this->client->__getLastRequest()) . "\n";
                $html .= "<br /><br /><b>Response:</b><br />" . htmlspecialchars($this->client->__getLastResponse()) . "\n";

                $status = $result->Status->Data;

                if ($status == '200') {
                    if (is_numeric($result->TerminNr)) {
                        $integration_booking->setTerminNr($result->TerminNr);

                        $xml = '<ns1:TerminBuchen2 xmlns="wsCalendar3">' .
                               '<ns1:TerminNr>' . $result->TerminNr . '</ns1:TerminNr>' .
                               '<ns1:TerminFelder>' .
                               '<ns1:tl_Feld2>' .
                               '<ns1:FeldName>Name</ns1:FeldName>' .
                               '<ns1:Value>' . $booking->getLastName() . '</ns1:Value>' .
                               '</ns1:tl_Feld2>' .
                               '<ns1:tl_Feld2>' .
                               '<ns1:FeldName>Vorname</ns1:FeldName>' .
                               '<ns1:Value>' . $booking->getFirstName() . '</ns1:Value>' .
                               '</ns1:tl_Feld2>' .
                               '<ns1:tl_Feld2>' .
                               '<ns1:FeldName>E-Mail</ns1:FeldName>' .
                               '<ns1:Value>' . $booking->getEmail() . '</ns1:Value>' .
                               '</ns1:tl_Feld2>' .
                               '<ns1:tl_Feld2>' .
                               '<ns1:FeldName>Telefon</ns1:FeldName>' .
                               '<ns1:Value>' . $booking->getPhone() . '</ns1:Value>' .
                               '</ns1:tl_Feld2>' .
                               '<ns1:tl_Feld2>' .
                               '<ns1:FeldName>Versicherung</ns1:FeldName>' .
                               '<ns1:Value>' . self::getInsuranceTextTerminland($booking->getInsuranceId()) . '</ns1:Value>' .
                               '</ns1:tl_Feld2>' .
                               '</ns1:TerminFelder>' .
                               '</ns1:TerminBuchen2>';

                        $result = $this->client->TerminBuchen2(new \SoapVar($xml, XSD_ANYXML));

                        if (is_object($result)) {
                            $status = $result->Status->Data;
                            if ($status == '200') {
                                if ($result->TerminCode != '') {
                                    $integration_booking->setStatus(Booking::STATUS_OK);
                                    $integration_booking->setTerminCode($result->TerminCode);
                                } else {
                                    $errorCode = $status;
                                    $errorDescription = "TerminCode is empty";
                                }
                            } else {
                                $errorCode = $status;
                                $errorDescription = $result->Status->Description;
                            }
                        } else {
                            $errorCode = 100;
                            $errorDescription = "TerminBuchen2 doesn't response";
                        }
                    } else {
                        $errorCode = 100;
                        $errorDescription = "TerminNr is not numeric";
                    }
                } else {
                    $errorCode = $status;
                    $errorDescription = "Dieser Termin wurde bereits vergeben, bitte wählen Sie einen anderen aus.";
                }
            } else {
                $errorCode = 100;
                $errorDescription = "TerminReservieren2 doesn't response";
            }
        } catch (\SoapFault $fault) {
            $errorCode = $fault->faultcode;
            $errorDescription = $fault->faultstring;
        }

        $html .= "<b>Request:</b><br />" . htmlspecialchars($this->client->__getLastRequest()) . "\n";
        $html .= "<br /><br /><b>Response:</b><br />" . htmlspecialchars($this->client->__getLastResponse()) . "\n";

        $integration_booking->addLogText($html);

        if ($errorDescription != '') {
            $integration_booking->setStatus(Booking::STATUS_ERROR);
            $this->setLastErrorCode($errorCode);
            $this->setLastErrorMessage($errorDescription);

            return false;
        }

        return true;
    }

    /**
     * Fetchs available appointments for one provider from terminland,
     * deletes ALL old appointments for that provider
     * and stores them in table appointments
     *
     * @param User $user
     * @param Location $location
     * @param DateTime $startDateTime
     * @param DateTime $endDateTime
     *
     * @return self
     *
     */
    public function updateAvailableAppointments($user, $location, $startDateTime, $endDateTime)
    {
        $this->setCountLastUpdatedAppointments(0);

        $integrationData = new Data();
        /** @var Data $data */
        $integrationData = $integrationData->findObject('user_id=' . $user->getId() . ' AND location_id=' . $location->getId());

        if (!$integrationData->isValid()) {
            return $this;
        }

        /*
         * fetch appointments for pkv and gkv separately
         */
        $appointmentsPrivate = $this->getAvailableAppointmentsTerminland($startDateTime, $endDateTime, $integrationData, INSURANCE_TYPE_PRIVATE);
        $this->addCountLastUpdatedAppointments(count($appointmentsPrivate));
        $appointmentsPublic = $this->getAvailableAppointmentsTerminland($startDateTime, $endDateTime, $integrationData, INSURANCE_TYPE_PUBLIC);
        $this->addCountLastUpdatedAppointments(count($appointmentsPublic));

        /*
         * Save the appointments
         */
        $appointmentService = AppointmentService::getInstance();
        $appointmentService->flush($user, $location);
        $appointmentService->save($appointmentsPrivate);
        $appointmentService->save($appointmentsPublic);

        return $this;
    }

    /**
     * Returns all times for a date period
     *
     * The terminland api allows queries for 30 days per request only
     * so this is a wrapper for the 30 day limit!
     * This call will eventually call the TerminlandAPI multiple times
     * (via getAvailableAppointmentsTerminlandSingleCall())
     *
     * @param DateTime $startDateTime
     * @param DateTime $endDateTime
     * @param Data $integrationData
     * @param int $insurance_id
     * @param string $output
     *
     * @return array Appointment || bool false on error
     */
    public function getAvailableAppointmentsTerminland($startDateTime, $endDateTime, $integrationData, $insurance_id, $output = 'array')
    {
        if ($output == 'array') {
            $appointments = array();
        } else {
            $appointments = '';
        }

        $dates = Calendar::getDatesInRange($startDateTime, $endDateTime);

        if ($dates) {
            $start = clone $startDateTime;
            $start->setTime(0, 0, 0);
            $end = clone $endDateTime;
            $end->setTime(23, 59, 59);

            // circle the 30 day limit
            $days = 0;
            foreach ($dates as $date) {
                $days++;
                if ($days == 1) {
                    $start = clone $date;
                    $start->setTime(0, 0, 0);
                }

                $end = clone $date;
                $end->setTime(23, 59, 59);

                if ($days == 30) {
                    $period_appointments = $this->getAvailableAppointmentsTerminlandSingleCall($start, $end, $integrationData, $insurance_id, $output);
                    if ($period_appointments) {
                        if ($output == 'array' && is_array($period_appointments) && !empty($period_appointments)) {
                            $appointments = $appointments + $period_appointments;
                        } else {
                            $appointments .= $period_appointments;
                        }
                    }
                    $days = 0;
                }
            }

            // Finalize if there are open days
            if ($days > 0) {
                $period_appointments = $this->getAvailableAppointmentsTerminlandSingleCall($start, $end, $integrationData, $insurance_id, $output);
                if ($period_appointments) {
                    if ($output == 'array' && is_array($period_appointments) && !empty($period_appointments)) {
                        $appointments = $appointments + $period_appointments;
                    } else {
                        $appointments .= $period_appointments;
                    }
                }
            }
        }

        return $appointments;
    }

    /**
     * Returns all times for a date period but not more than 30 days per call
     * The terminland api allows queries for 30 days per request only
     *
     * @param DateTime $startDateTime
     * @param DateTime $endDateTime
     * @param Data $integrationData
     * @param int $insurance_id
     * @param string $output
     *
     * @return array|bool|string
     */
    public function getAvailableAppointmentsTerminlandSingleCall($startDateTime, $endDateTime, $integrationData, $insurance_id, $output = 'array')
    {
        $appointments = array();

        $terminland_convert_practice_times_to_private_array = explode(',', Configuration::get('terminland_convert_practice_times_to_private'));
        $terminland_convert_practice_times_to_public_array = explode(',', Configuration::get('terminland_convert_practice_times_to_public'));

        if ((in_array($integrationData->getTerminlandId(), $terminland_convert_practice_times_to_private_array) && $insurance_id == INSURANCE_TYPE_PUBLIC) ||
            (in_array($integrationData->getTerminlandId(), $terminland_convert_practice_times_to_public_array) && $insurance_id == INSURANCE_TYPE_PRIVATE)
        ) {
            return array();
        }

        if (is_array($this->initSoap($integrationData->getTerminlandId(), true))) {
            return false;
        }
        try {
            // suchparameter -> string ist zwingend anzugeben
            $xml = self::getXMLforAASoapRequest($startDateTime, $endDateTime, $insurance_id, $integrationData->getTerminplanId());
            $result = $this->client->FreieTermine2(new \SoapVar($xml, XSD_ANYXML));

            if (in_array($integrationData->getTerminlandId(), $terminland_convert_practice_times_to_private_array) && $insurance_id == INSURANCE_TYPE_PRIVATE) {
                $xml = self::getXMLforAASoapRequest($startDateTime, $endDateTime, INSURANCE_TYPE_PUBLIC, $integrationData->getTerminplanId());
                $resultToMerge = $this->client->FreieTermine2(new \SoapVar($xml, XSD_ANYXML));
                $result = self::mergeAAResults($result, $resultToMerge);
            }

            if (in_array($integrationData->getTerminlandId(), $terminland_convert_practice_times_to_public_array) && $insurance_id == INSURANCE_TYPE_PUBLIC) {
                $xml = self::getXMLforAASoapRequest($startDateTime, $endDateTime, INSURANCE_TYPE_PRIVATE, $integrationData->getTerminplanId());
                $resultToMerge = $this->client->FreieTermine2(new \SoapVar($xml, XSD_ANYXML));
                $result = self::mergeAAResults($result, $resultToMerge);
            }

            if (self::checkAAResultIsValid($result)) {
                $dateTimes = $result->Termine_UTC->string;
                foreach ($dateTimes as $dateTime) {
                    /*
                     * We get the DateTimes in UTC ("Z") from Terminland but need to convert them into
                     * our TimeZone. So convert it back.
                     */
                    $startDateTime = new DateTime($dateTime, new \DateTimeZone('UTC'));
                    $startDateTime->setTimezone(new \DateTimeZone($GLOBALS['CONFIG']['SYSTEM_TIMEZONE']));
                    $appointment = new Appointment();

                    /*
                     * Set only the user_id and location_id and not the Objects for memory safety
                     */
                    $appointment
                        ->setUserId($integrationData->getUser()->getId())
                        ->setLocationId($integrationData->getLocation()->getId())
                        ->setDate($startDateTime->getDate())
                        ->setInsuranceIds($insurance_id)
                        ->setStartTime($startDateTime);

                    $appointments[] = $appointment;
                }
            }
        } catch (\SoapFault $fault) {
            return false;
        }

        switch ($output) {
            case 'debug':
                $html = "<b>RequestHeaders:</b><br />" . str_replace("\n", "<br />\n", htmlspecialchars($this->client->__getLastRequestHeaders())) . "\n";
                $html .= "<br /><br /><b>Request:</b><br />" . htmlspecialchars($this->client->__getLastRequest()) . "\n";
                $html .= "<br /><br /><b>ResponseHeaders:</b><br />" . str_replace("\n", "<br />\n", htmlspecialchars($this->client->__getLastResponseHeaders())) . "\n";
                $html .= "<br /><br /><b>Response:</b><br />" . htmlspecialchars($this->client->__getLastResponse()) . "<br /><br /><hr />\n";

                return $html;

            case 'array':
            default:
                return $appointments;
        }
    }

    /**
     * Inits the Soap client
     *
     * @param string $terminland_id aka system_id
     * @param bool $set_header
     *
     * @return bool
     */
    private function initSoap($terminland_id, $set_header = false)
    {
        if (isset(self::$intisoapmap[$terminland_id])) {
            $this->client = self::$intisoapmap[$terminland_id];

            return true;
        }

        ini_set('soap.wsdl_cache_enabled', '0');

        $url = self::SOAP_PROTOCOL . '://' . self::SOAP_DOMAIN . '/' . $terminland_id . self::SOAP_PATH . '?WSDL';

        try {
            $this->client = @new \SoapClient($url, array(
                    'login'        => self::SOAP_USER_ID,
                    'password'     => self::SOAP_PW,
                    'trace'        => 1,
                    'exceptions'   => 1,
                    'soap_version' => SOAP_1_1,
                    'features'     => SOAP_SINGLE_ELEMENT_ARRAYS
                )
            );

            if ($set_header) {
                $headers = new \SoapHeader(
                    'wsCalendar3',
                    'tl_Authentifizierung2',
                    array(
                        'TLNamespace' => self::SOAP_USER_ID,
                        'Login'       => self::SOAP_USER_ID,
                        'Password'    => self::SOAP_PW
                    )
                );

                $this->client->__setSoapHeaders($headers);
            }

            self::$intisoapmap[$terminland_id] = $this->client;

            return true;
        } catch (\SoapFault $fault) {
            // if this runs in a console show a warning
            if (function_exists('show_warning')) {
                show_warning('initSoap failed with wsdl url:'.$url);
                show_warning('faultcode: '.$fault->faultcode);
                show_warning('faultstring: '.$fault->faultstring);
            }
            // These two properties are undocumented and invisible. *facepalm*
            return array(
                $fault->faultcode,
                $fault->faultstring
            );
        }
    }

    /**
     * @param DateTime $startDateTime
     * @param DateTime $endDateTime
     * @param int $insurance_id
     * @param string $terminplan_id
     *
     * @return string
     */
    private static function getXMLforAASoapRequest($startDateTime, $endDateTime, $insurance_id, $terminplan_id)
    {
        // terminland needs UTC format but with the "Z" timezone identifier. For php "UTC" equals "Z" so all good.
        // but make sure to format the output to terminland with "format('Ymd\THis\Z')"
        $startDateTime->setTimezone(new \DateTimeZone('UTC'));
        $endDateTime->setTimezone(new \DateTimeZone('UTC'));

        return '<ns1:FreieTermine2>' .
               '<ns1:Terminplan>' . $terminplan_id . '</ns1:Terminplan>' .
               '<ns1:Suchparameter><ns1:string>' . self::getInsuranceTextTerminland(strval($insurance_id)) . '</ns1:string></ns1:Suchparameter>' .
               '<ns1:DatumVon_UTC>' . $startDateTime->format('Ymd\THis\Z') . '</ns1:DatumVon_UTC>' .
               '<ns1:DatumBis_UTC>' . $endDateTime->format('Ymd\THis\Z') . '</ns1:DatumBis_UTC>' .
               '<ns1:Termine_UTC/>' .
               '</ns1:FreieTermine2>';
    }

    /**
     * Returns the insurance text
     *
     * @param int $insurance_id
     *
     * @return string
     */
    private static function getInsuranceTextTerminland($insurance_id = null)
    {
        switch ($insurance_id) {
            case INSURANCE_TYPE_PRIVATE:
                $_string = 'Privat';
                break;
            case INSURANCE_TYPE_PUBLIC:
            default:
                $_string = 'Kasse';
        }

        return $_string;
    }

    /**
     * @param object $result1
     * @param object $result2
     *
     * @return array
     */
    private static function mergeAAResults($result1, $result2)
    {
        if (!self::checkAAResultIsValid($result1) && self::checkAAResultIsValid($result2)) {
            return $result2;
        }

        if (!self::checkAAResultIsValid($result2) && self::checkAAResultIsValid($result1)) {
            return $result1;
        }

        if (!self::checkAAResultIsValid($result1) && !self::checkAAResultIsValid($result2)) {
            return null;
        }

        $tl_times = $result2->Termine_UTC->string;
        foreach ($tl_times as $tl_time) {
            $result1->Termine_UTC->string[] = $tl_time;
        }

        $result1->Termine_UTC->string = array_unique($result1->Termine_UTC->string);

        return $result1;
    }

    /**
     * @param object $result
     *
     * @return bool
     */
    private static function checkAAResultIsValid($result)
    {
        if (is_object($result) && isset($result->Status) && isset($result->Status->Data)) {
            $status = $result->Status->Data;
            if ($status == '200') {
                if (isset($result->Termine_UTC) && isset($result->Termine_UTC->string)) {
                    $tl_times = $result->Termine_UTC->string;
                }

                if (isset($tl_times) && is_array($tl_times)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Cancel a booking.
     * Note that we cannot automatically cancel Terminland appointments
     * So here we only document the cancelation
     *
     * @param PatientBooking $booking
     *
     * @return bool
     */
    public function cancelBooking(PatientBooking $booking)
    {
        $integration_booking = new Booking();
        $integration_booking->setFunctionId(Booking::FUNCTION_CANCEL);
        $integration_booking->setStatus(Booking::STATUS_ERROR);
        $integration_booking->addLogText(get_class($this) . "->cancelBooking();\n");

        if (!$integration_booking->saveNewObject()) {
            return false;
        }

        return true;
    }

    /**
     * Cancel the appointment
     *
     * @param string $termin_nr
     * @param string $terminland_id
     *
     * @return string
     */
    public function cancelAppointmentTerminland($termin_nr, $terminland_id)
    {
        // Book only in production!
        if (is_array($this->initSoap($terminland_id, true))) {
            return false;
        }

        try {
            $xml = '<ns1:TerminStornieren2><ns1:TerminNr>' . $termin_nr . '</ns1:TerminNr></ns1:TerminStornieren2>';

            $result = $this->client->TerminStornieren2(new \SoapVar($xml, XSD_ANYXML));

            if (is_object($result)) {
                $status = $result->Status->Data;
                if ($status == '200') {
                    return true;
                } else {
                    return $result->Status->Description;
                }
            }
        } catch (\SoapFault $fault) {
            return $fault->faultcode . ':' . $fault->faultstring;
        }

        return 'Unknown Error';
    }

    /**
     * Returns all integration booking objects in an array for the given booking
     *
     * @param PatientBooking $booking
     *
     * @return array
     */
    public function getIntegrationBookings(PatientBooking $booking)
    {
        $integration_booking = new Booking();

        return $integration_booking->findObjects('booking_id=' . $booking->getId() . ' ORDER BY created_at DESC;');
    }

    /**
     * Returns all types for the terminland soap requests
     *
     * @param string $terminland_id
     *
     * @return bool|string
     */
    public function getTerminlandTypes($terminland_id = self::SOAP_USER_ID)
    {
        if (is_array($this->initSoap($terminland_id, false))) {
            return false;
        }

        try {
            return '<pre>' . var_export($this->client->__getTypes(), true) . '</pre>';
        } catch (\SoapFault $fault) {
            return false;
        }
    }

    /**
     * @return int
     */
    public function getCountLastUpdatedAppointments()
    {
        return $this->countLastUpdatedAppointments;
    }

    /**
     * @param int $countLastUpdatedAppointments
     * @return self
     */
    public function setCountLastUpdatedAppointments($count)
    {
        $this->countLastUpdatedAppointments = $count;
        return $this;
    }

    /**
     * @param int $countLastUpdatedAppointments
     * @return self
     */
    public function addCountLastUpdatedAppointments($count)
    {
        $this->countLastUpdatedAppointments += $count;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function hasAppointmentEnquiry()
    {
        return false;
    }
}
