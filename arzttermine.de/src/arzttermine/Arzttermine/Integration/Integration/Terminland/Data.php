<?php

namespace Arzttermine\Integration\Integration\Terminland;

use Arzttermine\Core\Basic;
use Arzttermine\Location\Location;
use Arzttermine\User\User;

class Data extends Basic {

    /**
     * @var string
     */
    protected $tablename = DB_TABLENAME_INTEGRATION_2_DATAS;

    /**
     * @var int
     */
    protected $id = 2;

    /**
     * @var int
     */
    protected $user_id;

    /**
     * @var int
     */
    protected $location_id;

    /**
     * @var string
     */
    protected $terminland_id = '';

    /**
     * tpl1, tpl2, etc. But could also be different!
     *
     * @var string
     */
    protected $terminplan_id = '';

    /**
     * @var string
     */
    protected $created_at = '';

    /**
     * @var string
     */
    protected $created_by = '';

    /**
     * @var string
     */
    protected $updated_at = '';

    /**
     * @var string
     */
    protected $updated_by = '';

    /**
     * @var array
     */
    protected $all_keys
        = array(
            "id",
            "user_id",
            "location_id",
            "terminland_id",
            "terminplan_id",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by"
        );

    /**
     * @var array
     */
    protected $secure_keys
        = array(
            "id",
            "user_id",
            "location_id",
            "terminland_id",
            "terminplan_id",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by"
        );

    /**
     * @return int
     */
    public function getLocationId()
    {
        return $this->location_id;
    }

    /**
     * @return Location
     */
    public function getLocation()
    {
        return new Location($this->location_id);
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return new User($this->user_id);
    }

    /**
     * @return string
     */
    public function getTerminlandId()
    {
        return $this->terminland_id;
    }

    /**
     * @return string
     */
    public function getTerminplanId()
    {
        return $this->terminplan_id;
    }
}