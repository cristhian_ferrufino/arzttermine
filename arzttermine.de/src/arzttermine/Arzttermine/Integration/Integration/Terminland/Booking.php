<?php

namespace Arzttermine\Integration\Integration\Terminland;

use Arzttermine\Integration\Booking as IntegrationBookingCore;

class Booking extends IntegrationBookingCore {

    /**
     * @var string
     */
    protected $tablename = DB_TABLENAME_INTEGRATION_2_BOOKINGS;

    /**
     * @var int
     */
    protected $integration_id = 2;

    /**
     * @var string
     */
    protected $termin_nr = '';

    /**
     * @var string
     */
    protected $termin_code = '';

    /**
     * @var string
     */
    protected $terminland_id = '';

    /**
     * @var string
     */
    protected $terminplan_id = '';

    /**
     * @var array
     */
    protected $all_keys
        = array(
            "id",
            "status",
            "booking_id",
            "function_id",
            "integration_id",
            "log_text",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by",
            'terminland_id',
            'terminplan_id',
            'termin_nr',
            'termin_code'
        );

    /**
     * @var array
     */
    protected $secure_keys
        = array(
            "status",
            "log_text",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by",
            'terminland_id',
            'terminplan_id',
            'termin_nr',
            'termin_code'
        );

    /**
     * Returns the terminland_id
     *
     * @return string
     */
    public function getTerminlandId()
    {
        return $this->terminland_id;
    }

    /**
     * Returns the terminplan_id
     *
     * @return string
     */
    public function getTerminplanId()
    {
        return $this->terminplan_id;
    }

    /**
     * @param string $terminplan_id
     *
     * @return self
     */
    public function setTerminplanId($terminplan_id)
    {
        $this->terminplan_id = $terminplan_id;

        return $this;
    }

    /**
     * @param string $terminland_id
     *
     * @return self
     */
    public function setTerminlandId($terminland_id)
    {
        $this->terminland_id = $terminland_id;

        return $this;
    }

    /**
     * @param string $termin_code
     *
     * @return self
     */
    public function setTerminCode($termin_code)
    {
        $this->termin_code = $termin_code;

        return $this;
    }

    /**
     * @return string
     */
    public function getTerminCode()
    {
        return $this->termin_code;
    }

    /**
     * @param string $termin_nr
     *
     * @return self
     */
    public function setTerminNr($termin_nr)
    {
        $this->termin_nr = $termin_nr;

        return $this;
    }

    /**
     * @return string
     */
    public function getTerminNr()
    {
        return $this->termin_nr;
    }

    /**
     * Returns status information in Html
     *
     * @return string
     */
    public function getStatusHtml()
    {
        if ($this->getStatus() == Booking::STATUS_OK) {
            $html = '<span class="green">status=OK</span><br />' . "\n" .
                    'created_at=' . $this->getCreatedAt() . "<br />\n" .
                    'termin_nr=' . $this->getTerminNr() . "<br />\n" .
                    'termin_code=' . $this->getTerminCode() . "<br />\n";
        } else {
            $html = '<span class="red">status=ERROR</span><br />' . "\n" .
                    'created_at=' . $this->getCreatedAt() . "<br />\n";
        }

        return $html;
    }
}