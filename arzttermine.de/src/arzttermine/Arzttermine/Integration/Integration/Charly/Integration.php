<?php

namespace Arzttermine\Integration\Integration\Charly;

use Arzttermine\Appointment\Provider;
use Arzttermine\Booking\Booking;
use Arzttermine\Integration\Appointment;
use Arzttermine\Integration\Integration as IntegrationCore;

class Integration extends IntegrationCore {

    /**
     * @var int
     */
    protected $id = 10;
    
    /**
     * @var int
     */
    protected $user_id;

    /**
     * @var int
     */
    protected $location_id;

    /**
     * Checks if user and location fit by its integration_data
     *
     * @param int $user_id
     * @param int $location_id
     *
     * @return bool
     */
    public function isConnected($user_id, $location_id)
    {
        $integration_data = new Data();

        return $integration_data->count('user_id=' . $user_id . ' AND location_id=' . $location_id) > 0;
    }

    /**
     * Migrate the appointments from ics to db.
     * This should be called periodically in a cron job.
     *
     * Loops thru all Integration_10_Datas (Providers)
     *
     * @param bool $output (default: false)
     *
     * @return bool
     */
    public static function updateAvailableAppointments($output = false)
    {
        // loop through all providers
        $integrationData = new Data();
        $datas = $integrationData->findObjects();
        if (!empty($datas)) {
            /** @var Data $data */
            foreach ($datas as $data) {
                $data->updateAvailableAppointments(null, null, true, $output);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getAvailableAppointments(Provider $provider, $format = '')
    {
        $integrationAppointment = new Appointment();

        return $integrationAppointment->getAvailableAppointments($provider, $format);
    }

    /**
     * {@inheritdoc}
     */
    public function getNextAvailableAppointment(Provider $provider, $format = '')
    {
        $integrationAppointment = new Appointment();

        return $integrationAppointment->getNextAvailableAppointment($provider, $format);
    }

    /**
     * {@inheritdoc}
     */
    public function getAppointmentsBlock(Provider $provider, $widget_slug = '')
    {
        $date_range = $provider->getDateRange();
        
        $this->getAvailableAppointments(
            reset($date_range),
            end($date_range),
            $provider->getUserId(),
            $provider->getLocationId(),
            $provider->getInsuranceId(),
            $provider->getTreatmentTypeId(),
            $provider->getMedicalSpecialtyId(),
            'Appointment'
        );
    }

    /**
     * Cannot cancel a Charly booking at this stage
     *
     * {@inheritdoc}
     */
    public function cancelBooking(Booking $booking)
    {
        return true;
    }
}
