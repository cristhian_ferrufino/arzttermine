<?php

namespace Arzttermine\Integration\Integration\Charly;

use Arzttermine\Application\Application;
use Arzttermine\Calendar\Calendar;
use Arzttermine\Core\Basic;
use Arzttermine\Integration\Appointment;
use DateInterval;
use DateTime;

class Data extends Basic {

    /**
     * @var string
     */
    protected $tablename = DB_TABLENAME_INTEGRATION_10_DATAS;

    /**
     * @var int
     */
    protected $user_id;

    /**
     * @var int
     */
    protected $location_id;

    /**
     * @var string
     */
    protected $filename = '';

    /**
     * @var string
     */
    protected $last_hash = '';

    /**
     * @var string
     */
    protected $last_hash_updated_at = '';

    /**
     * @var string
     */
    protected $webdavaccount_id = '';

    /**
     * @var string
     */
    protected $week_schedule_json = '';

    /**
     * @var string
     */
    protected $created_at = '';

    /**
     * @var string
     */
    protected $created_by = '';

    /**
     * @var string
     */
    protected $updated_at = '';

    /**
     * @var string
     */
    protected $updated_by = '';

    /**
     * @var array
     */
    protected $all_keys
        = array(
            "id",
            "name",
            "user_id",
            "location_id",
            "filename",
            "last_hash",
            "last_hash_updated_at",
            "webdavaccount_id",
            "week_schedule_json",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by"
        );

    /**
     * @var array
     */
    protected $secure_keys
        = array(
            "id",
            "name",
            "user_id",
            "location_id",
            "filename",
            "webdavaccount_id",
            "week_schedule_json",
        );

    /**
     * The current file hash gets set by loadNewHash()
     *
     * @var string
     */
    protected $new_hash = '';

    /**
     * The vcalendar object
     *
     * @var mixed
     */
    private $vcalendar;

    /**
     * @param string $last_hash
     *
     * @return self
     */
    public function setLastHash($last_hash)
    {
        $this->last_hash = $last_hash;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastHash()
    {
        return $this->last_hash;
    }

    /**
     * @param string $new_hash
     *
     * @return self
     */
    public function setNewHash($new_hash)
    {
        $this->new_hash = $new_hash;

        return $this;
    }

    /**
     * @return string
     */
    public function getNewHash()
    {
        return $this->new_hash;
    }

    /**
     * @param string $webdavaccount_id
     *
     * @return self
     */
    public function setWebdavaccountId($webdavaccount_id)
    {
        $this->webdavaccount_id = $webdavaccount_id;

        return $this;
    }

    /**
     * @return string
     */
    public function getWebdavaccountId()
    {
        return $this->webdavaccount_id;
    }

    /**
     * @return string
     */
    public function getWeekScheduleJson()
    {
        return $this->week_schedule_json;
    }

    /**
     * Returns the user_id
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Returns the location_id
     *
     * @return int
     */
    public function getLocationId()
    {
        return $this->location_id;
    }

    /**
     * Returns the filename
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Returns the filepath to the ics file.
     *
     * @return string
     */
    public function getFilepath()
    {
        $webdavaccount = $this->getWebdavAccount();
        if (!$webdavaccount->isValid()) {
            return false;
        }

        return $webdavaccount->getDirpath() . '/' . $this->getFilename();
    }

    /**
     * Returns the Integration_10_Webdavaccount
     *
     * @return Account
     */
    public function getWebdavAccount()
    {
        return new Account($this->getWebdavaccountId());
    }

    /**
     * Calculate the appointments and store them in the db
     * a) load the ics file
     * b) check if it has changed (if not dont process again)
     * c) delete all appointments for this provider
     * d) calculate new appointments for the given date range
     *
     * @param string $dateStart
     * @param string $dateEnd
     * @param bool $checkFileChange
     * @param bool $output
     *
     * @return bool
     */
    public function updateAvailableAppointments($dateStart = null, $dateEnd = null, $checkFileChange = true, $output = false)
    {
        // get the ics file
        $filepath = $this->getFilepath();
        if (!is_readable($filepath)) {
            if ($output) {
                echo "ERROR: Can't open filepath: " . $filepath . "\n";
            }

            return false;
        }

        // check if we need to parse the file (the hash changed)
        if ($checkFileChange && !$this->fileHasChanged()) {
            if ($output) {
                echo "Skipping filepath: " . $filepath . "\n";
            }

            return true;
        }

        // parse the file and migrate the appointments
        if ($output) {
            echo "Start migrating filepath: " . $filepath . " with weekScheduleData from integration_10_Data(id:" . $this->getId() . ")\n";
        }

        if ($dateStart == null) {
            $dateStart = Calendar::getFirstPossibleDateTimeForSearch();
        }
        if ($dateEnd == null) {
            $dateEnd = Calendar::getLastPossibleDateTimeForSearch();
        }

        $dates = Calendar::getDatesInAreaWithFilter($dateStart, $dateEnd);
        if (empty($dates)) {
            return false;
        }

        // get the weekly schedule for this provider
        $weekArray = $this->getWeekScheduleData();
        if (empty($weekArray)) {
            return false;
        }

        // init the vcalendar
        if (!$this->loadVCalendar()) {
            return false;
        }

        $appointment = new Appointment();
        // @todo: add status or use a tmp switch table instead of just delete the current appointments
        $appointment->delete($this->getUserId(), $this->getLocationId());

        // loop the days that we need
        foreach ($dates as $date) {

            // A) get the (blocked) events from the ics files for this day
            $dateArray = Calendar::getDateTimeArray($date);
            $blockedEventsForADate = $this->getVCalendarEventsForADay($dateArray['year'], $dateArray['month'], $dateArray['day']);

            // B) get the free blocks for this (week) day
            // loop through each set of filters (insurace, medicalSpecialty, etc)
            foreach ($weekArray as $dataSet) {
                if (is_object($dataSet)) {
                    // let's get out the calculator...
                    $this->updateAvailableAppointmentsByDate(
                         $date,
                             $blockedEventsForADate,
                             $this->getUserId(),
                             $this->getLocationId(),
                             !empty($dataSet->insurance_ids) ? $dataSet->insurance_ids : array(),
                             !empty($dataSet->medical_specialty_ids) ? $dataSet->medical_specialty_ids : array(),
                             !empty($dataSet->treatment_type_ids) ? $dataSet->treatment_type_ids : array(),
                             !empty($dataSet->timeblock_in_minutes) ? $dataSet->timeblock_in_minutes : null,
                             !empty($dataSet->schedule_plan) ? $dataSet->schedule_plan : null
                    );
                }
            }
        }

        if ($output) {
            echo "End migrating filepath: " . $filepath . "\n";
        }
        // everything went well => store the last_hash and set the last_hash_updated_at
        $this->saveHash();

        return true;
    }

    /**
     * Calculate the appointments and store them in the db
     *
     * @param string $date (datetime)
     * @param array $blockedEventsForADate
     * @param int $userId
     * @param int $locationId
     * @param array $insuranceIds
     * @param array $medicalSpecialtyIds
     * @param array $treatmentTypeIds
     * @param string $timeblockInMinutes
     * @param \stdClass $schedulePlanObject (1->Monday array StdObject->start,end) / maps 1:1 from week_schedule_json
     *
     * @return bool
     */
    public function updateAvailableAppointmentsByDate($date, $blockedEventsForADate, $userId, $locationId, $insuranceIds, $medicalSpecialtyIds, $treatmentTypeIds, $timeblockInMinutes, $schedulePlanObject)
    {
        $weekday = Calendar::getWeekdayISO8601($date);
        if ($weekday < 1 || $weekday > 7) {
            return false;
        }

        if (empty($schedulePlanObject)) {
            return false;
        }

        // if there is no plan for this weekday than thats fine
        if (!isset($schedulePlanObject->{$weekday})) {
            return true;
        }

        if (empty($timeblockInMinutes)) {
            return false;
        }

        $schedulePlan = $schedulePlanObject->{$weekday};

        foreach ($schedulePlan as $schedule) {
            $freeStartString = $schedule->start;
            $freeEndString = $schedule->end;
            // filter out bad syntax
            if (!preg_match('/\d\d:\d\d/', $freeStartString) || !preg_match('/\d\d:\d\d/', $freeEndString)) {
                continue;
            }

            $freeEnd = new DateTime($date . ' ' . $freeEndString);
            $start = new DateTime($date . ' ' . $freeStartString);
            $end = new DateTime($date . ' ' . $freeStartString);
            $end->add(new DateInterval('PT' . $timeblockInMinutes . 'M'));

            while (($start < $freeEnd) && ($end <= $freeEnd)) {
                $overlaps = self::timesOverlap($start, $end, $blockedEventsForADate);
                if ($overlaps === false) {
                    // add appointment
                    /**
                     * debug
                     * echo $start->format('Y-m-d H:i:s')."---".$end->format('Y-m-d H:i:s')."---".$freeEnd->format('Y-m-d H:i:s')."\n";
                     */
                    $availableAppointment = new Appointment();
                    $availableAppointment
                        ->setStartAt($start->format('Y-m-d H:i:s'))
                        ->setUserId($userId)
                        ->setLocationId($locationId)
                        ->setInsuranceIds($insuranceIds)
                        ->setMedicalSpecialtyIds($medicalSpecialtyIds)
                        ->setTreatmenttypeIds($treatmentTypeIds)
                        ->saveNew();

                    // increase the next appointment block
                    $start->add(new DateInterval('PT' . $timeblockInMinutes . 'M'));
                    $end->add(new DateInterval('PT' . $timeblockInMinutes . 'M'));
                } else {
                    /**
                     * adjust the new appointment block to the end of the last blocker
                     * for the next cyclus
                     */
                    /** @var \DateTime $overlaps */
                    $start = clone $overlaps;
                    $end = clone $overlaps;
                    $end->add(new DateInterval('PT' . $timeblockInMinutes . 'M'));
                }
            }
        }

        return true;
    }

    /**
     * Checks if the start and end overlap in one of the blockedEvents for a date.
     * If it overlaps it returns the end DateTime Object of the blocker
     *
     * @return bool
     *
     * @param DateTime Object start
     * @param DateTime Object end
     * @param array ("start"=>datetime, "end"=>datetime)
     *
     * @return DateTime
     */
    public function timesOverlap($start, $end, $blockedEventsForADate)
    {
        if (!is_array($blockedEventsForADate) || empty($blockedEventsForADate)) {
            return false;
        }
        foreach ($blockedEventsForADate as $blockedEventForADate) {
            $blockedStart = new DateTime($blockedEventForADate['start']);
            $blockedEnd = new DateTime($blockedEventForADate['end']);

            /**
             * Equal edge matchings are ok (10:00 - 12:00 with a blocker from 09:00 - 10:00 is still ok)
             * The inverse logic is shorter so watch the "!". It just checks if the blocker
             * is completly before the start and completly after the end instead of checking
             * parts of it before, in or after.
             */
            if (
            !(
                ($start <= $blockedStart) && ($end <= $blockedStart)
                || ($start >= $blockedEnd) && ($end >= $blockedEnd)
            )
            ) {
                return $blockedEnd;
            }
        }

        return false;
    }

    /**
     * Returns the stdClass in decoded json
     *
     * stdClass indices are ISO-8601 numeric representation of
     * the day of the week as in php date('N'): 1:monday - 7: sunday
     *
     * @return \stdClass
     */
    public function getWeekScheduleData()
    {
        return json_decode($this->week_schedule_json);
    }

    /**
     * ICS File related *********************************************
     */

    /**
     * Loads and initializes the vcalendar object
     *
     * @return bool
     */
    private function loadVCalendar()
    {
        $config = array(
            "unique_id" => "Arzttermine.de",
            // set Your unique id
            "directory" => dirname($this->getFilepath()),
            "filename"  => basename($this->getFilepath())
        );
        $v = new \vcalendar($config); // create a new calendar instance
        $v->setProperty("method", "PUBLISH"); // required of some calendar software
        $v->setProperty("x-wr-calname", "Arzttermin"); // required of some calendar software
        $v->setProperty("X-WR-CALDESC", "Ihr Arzttermin"); // required of some calendar software
        $tz = "Europe/Berlin";
        $v->setProperty("X-WR-TIMEZONE", $tz); // required of some calendar software

        $v->parse();
        $v->sort();

        // store the object
        $this->vcalendar = $v;

        return true;
    }

    /**
     * Returns a minified data array for all events on a specific day
     *
     * @param string $year
     * @param string $month
     * @param string $day
     *
     * @return array (start=>datetime,end=>datetime))
     */
    private function getVCalendarEventsForADay($year, $month, $day)
    {
        $events = array();

        $eventArray = $this->vcalendar->selectComponents($year, $month, $day); // select components occuring this day
        if (!empty($eventArray)) {
            // (including components with recurrence pattern)
            foreach ($eventArray as $yearArray) {
                foreach ($yearArray as $monthArray) {
                    foreach ($monthArray as $dailyEventsArray) {
                        /** @var \vevent $vevent */
                        foreach ($dailyEventsArray as $vevent) {
                            // make sure we are using the current day and not the recurring start
                            // so dont use dtstart BUT x-current-dtstart AND x-current-dtend
                            $current_dtstart
                                = $vevent->getProperty("x-current-dtstart"); // if member of a recurrence set (2nd occurence etc)
                            // returns array( "x-current-dtstart", <(string) date("Y-m-d [H:i:s][timezone/UTC offset]")>)
                            $start = $current_dtstart[1];
                            $current_dtend = $vevent->getProperty("x-current-dtend");
                            $end = $current_dtend[1];
                            // add this event to the list
                            $events[] = array(
                                'start' => $start,
                                'end'   => $end
                            );
                        }
                    }
                }
            }
        }

        return $events;
    }

    /**
     * Loads the new_hash (current md5 hash)
     *
     * @return string
     */
    public function loadNewHash()
    {
        $filepath = $this->getFilepath();
        if (!is_readable($filepath)) {
            return false;
        }
        $md5Hash = md5_file($filepath);
        if (!$md5Hash) {
            return false;
        }
        $this->new_hash = $md5Hash;

        return $this->new_hash;
    }

    /**
     * Checks if the file has changed by its md5 hash
     *
     * @return bool
     */
    private function fileHasChanged()
    {
        if (empty($this->new_hash) || $this->new_hash == '') {
            if (!$this->loadNewHash()) {
                return false;
            }
        }

        return ($this->new_hash != $this->last_hash);
    }

    /**
     * Store the md5 hash and set the updated_at
     *
     * The md5 hash needs to be set before this call (i.e. filenameHasChanged())
     *
     * @return bool
     */
    private function saveHash()
    {
        // save the result
        $this->last_hash = $this->new_hash;
        $this->last_hash_updated_at = Application::getInstance()->now();
        $this->save(
             array(
                 'last_hash',
                 'last_hash_updated_at'
             )
        );
    }
}
