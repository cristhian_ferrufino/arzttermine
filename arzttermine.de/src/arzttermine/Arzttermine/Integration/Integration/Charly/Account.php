<?php

namespace Arzttermine\Integration\Integration\Charly;

use Arzttermine\Core\Basic;

class Account extends Basic {

    /**
     * @var string
     */
    protected $tablename = DB_TABLENAME_INTEGRATION_10_WEBDAVACCOUNTS;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $webdav_id;

    /**
     * @var string
     */
    protected $webdav_hashed_password;

    /**
     * @var string
     */
    protected $created_at;

    /**
     * @var string
     */
    protected $created_by;

    /**
     * @var string
     */
    protected $updated_at;

    /**
     * @var string
     */
    protected $updated_by;

    /**
     * @var array
     */
    protected $all_keys
        = array(
            "id",
            "name",
            "webdav_id",
            "webdav_hashed_password",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by"
        );

    /**
     * @var array
     */
    protected $secure_keys
        = array(
            "id",
            "name",
            "webdav_id",
            "webdav_hashed_password",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by"
        );
    
    /**
     * @return string
     */
    public function getAdminEditUrl()
    {
        return '/admin/integrations/integration_10/webdavaccounts/index.php?action=edit&id=' . $this->getId();
    }

    /**
     * Deletes this item and removes the webdav directory
     *
     * @return bool
     */
    public function delete()
    {
        if (!$this->rmDirpath()) {
            return false;
        }

        return parent::delete();
    }

    /**
     * Returns the name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the webdav_id
     *
     * @return string
     */
    public function getWebdavId()
    {
        return $this->webdav_id;
    }

    /**
     * Returns the webdav hashed password
     *
     * @return string
     */
    public function getWebdavHashedPassword()
    {
        return $this->webdav_hashed_password;
    }

    /**
     * @param string $webdav_hashed_password
     *
     * @return self
     */
    public function setWebdavHashedPassword($webdav_hashed_password)
    {
        $this->webdav_hashed_password = $webdav_hashed_password;

        return $this;
    }

    /**
     * @param string $webdav_id
     *
     * @return self
     */
    public function setWebdavId($webdav_id)
    {
        $this->webdav_id = $webdav_id;

        return $this;
    }

    /**
     * Returns the dirpath to the ics directory (without ending slash)
     *
     * @return string
     */
    public function getDirpath()
    {
        return $GLOBALS['CONFIG']['INTEGRATIONS'][10]['WEBDAV_DOCUMENT_ROOT'] . '/' . $this->getWebdavId();
    }

    /**
     * Creates the dir for holding the ics files
     *
     * @return bool
     */
    public function mkDirpath()
    {
        $basedir = $GLOBALS['CONFIG']['INTEGRATIONS'][10]['WEBDAV_DOCUMENT_ROOT'] . '/';
        $dirpath = $basedir . sanitize_file_name($this->getWebdavId());

        // check if the dir already exists
        if (is_dir($dirpath)) {
            return false;
        }

        return @mkdir($dirpath);
    }

    /**
     * Removes the dir including the ics files
     *
     * @return bool
     */
    public function rmDirpath()
    {
        $basedir = $GLOBALS['CONFIG']['INTEGRATIONS'][10]['WEBDAV_DOCUMENT_ROOT'] . '/';
        $dirpath = $basedir . sanitize_file_name($this->getWebdavId());

        if ($dirpath == '' || $dirpath == '/' || !is_dir($dirpath)) {
            return false;
        }

        // remove all files in the dir
        $files = array_diff(
            scandir($dirpath), array(
                '.',
                '..'
            )
        );
        foreach ($files as $file) {
            // only delete files in exactly this directory. Not recursive!
            if (is_file("$dirpath/$file")) {
                @unlink("$dirpath/$file");
            }
        }

        return @rmdir($dirpath);
    }

    /**
     * Renames the dir for holding the ics files
     *
     * @param string $oldWebdavId
     * @param string $newWebdavId
     *
     * @return bool
     */
    public static function renameDirpath($oldWebdavId, $newWebdavId)
    {
        $oldDirpath = $GLOBALS['CONFIG']['INTEGRATIONS'][10]['WEBDAV_DOCUMENT_ROOT'] . '/' . sanitize_file_name($oldWebdavId);
        $newDirpath = $GLOBALS['CONFIG']['INTEGRATIONS'][10]['WEBDAV_DOCUMENT_ROOT'] . '/' . sanitize_file_name($newWebdavId);
        // check if the dir already exists
        if (is_dir($newDirpath)) {
            return false;
        }

        return rename($oldDirpath, $newDirpath);
    }

    /**
     * htpasswd handling ************************************************************
     */

    /**
     * Returns the password as a hash as used in a htpasswd file
     *
     * @param string $password
     *
     * @return string
     */
    public static function getHtpasswdPasswordHash($password)
    {
        return '{SHA}' . base64_encode(sha1($password, true));
    }

    /**
     * Change the password for an account.
     * Password can not be empty.
     *
     * @param password
     *
     * @return bool
     */
    public function changePassword($password)
    {
        if ($password === '') {
            return false;
        }

        $this->setWebdavHashedPassword(self::getHtpasswdPasswordHash($password));

        return $this->save(array('webdav_hashed_password')) !== false;
    }

    /**
     * Returns the htpasswd filename
     *
     * @return string
     */
    public static function getHtpasswdFilename()
    {
        return $GLOBALS['CONFIG']['INTEGRATIONS'][10]['HTPASSWD_FILENAME'];
    }

    /**
     * Saves the array given by loadHtpasswd
     *
     * @return bool
     */
    public static function saveHtpasswdFile()
    {
        $result = true;
        $filename = self::getHtpasswdFilename();
        $array = self::getHtpasswdArray();

        ignore_user_abort(true);
        $fp = fopen($filename, "w+");
        if (flock($fp, LOCK_EX)) {
            while (list($u, $p) = each($array)) {
                fputs($fp, "$u:$p\n");
            }
            flock($fp, LOCK_UN); // release the lock
        } else {
            $result = false;
        }
        fclose($fp);
        ignore_user_abort(false);

        return $result;
    }

    /**
     * Returns the array from the db
     *
     * @return array
     */
    public static function getHtpasswdArray()
    {
        $webdavaccount = new self();

        $items = array();
        $webdavaccounts = $webdavaccount->findObjects();
        if (!empty($webdavaccounts)) {
            /** @var Account[] $webdavaccounts */
            foreach ($webdavaccounts as $webdavaccount) {
                $id = $webdavaccount->getWebdavId();
                $pw = $webdavaccount->getWebdavHashedPassword();
                $items[$id] = $pw;
            }
        }

        return $items;
    }
}
