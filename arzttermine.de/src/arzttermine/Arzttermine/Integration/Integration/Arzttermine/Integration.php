<?php

namespace Arzttermine\Integration\Integration\Arzttermine;

use Arzttermine\Appointment\Planner;
use Arzttermine\Appointment\Provider;
use Arzttermine\Calendar\Calendar;
use Arzttermine\Core\DateTime;
use Arzttermine\Integration\Integration as IntegrationCore;
use NGS\Managers\UsersAaManager;

class Integration extends IntegrationCore {

    /**
     * This must be set or the integration fails
     *
     * @var int
     */
    protected $id = 1;

    /**
     * {@inheritdoc}
     */
    public function getAppointmentsBlock(Provider $provider, $widget_slug = '')
    {
        $appointments_block = parent::getAppointmentsBlock($provider, $widget_slug);
        
        // Load the Planner associated with this provider (Integration 1 only) and apply filtering rules
        $planner = new Planner();
        $planner = $planner->getForUserLocation($provider->getUser(), $provider->getLocation());
        
        $today = new DateTime();
        $lead_in = $planner->getLeadIn();
        $max_date = $today->addDays($planner->getSearchPeriod());
        
        // Clear data from the front of the appointments blocks
        if ($lead_in) {
            foreach ($appointments_block['days'] as $date => $blocks) {
                // Shave off only the number of lead in days
                if (!$lead_in--) {
                    break;
                }
                
                // Uh we need to adjust the number of appointments we found...
                $cleared = count($appointments_block['days'][$date]['appointments']);
                $appointments_block['appointment_count'] -= $cleared;
                
                // Clear 
                $appointments_block['days'][$date]['appointments'] = array();
            }
        }
        
        // Only apply the search period if it's non-zero. We don't want to clear EVERYTHING
        if ($planner->getSearchPeriod()) {
            // Clear data after the max search date
            foreach ($appointments_block['days'] as $date => $blocks) {
                $appointment_date = new DateTime($date);
                if ($appointment_date > $max_date) {
                    // Uh we need to adjust the number of appointments we found...
                    $cleared = count($appointments_block['days'][$date]['appointments']);
                    $appointments_block['appointment_count'] -= $cleared;
                    
                    $appointments_block['days'][$date]['appointments'] = array();
                }
                unset($appointment_date);
            }
        }
        
        return $appointments_block;
    }
    
    /**
     * @inheritdoc
     */
    public function getAvailableAppointments(Provider $provider, $format = '')
    {
        // @todo Refactor to remove usersAA and to use Provider
        $date_range = $provider->getDateRange();
        return UsersAaManager::getInstance()->getAvailableAppointments(
            Calendar::getDate(reset($date_range)),
            Calendar::getDate(end($date_range)),
            $provider->getUserId(),
            $provider->getLocationId(),
            $provider->getInsuranceId()
        );
    }
}
