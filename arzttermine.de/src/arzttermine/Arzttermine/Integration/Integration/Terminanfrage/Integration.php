<?php

namespace Arzttermine\Integration\Integration\Terminanfrage;

use Arzttermine\Appointment\Appointment;
use Arzttermine\Appointment\Provider;
use \Arzttermine\Integration\Integration as IntegrationCore;
use Arzttermine\Core\Configuration;
use Arzttermine\Application\Application;

/**
 * Integration Class
 *
 * @author Axel Kuzmik <axel@kuzmik.de>
 */
class Integration extends IntegrationCore {

    /**
     * @var int
     */
    protected $id = 8;

    /**
     * @inheritdoc
     */
    public function getAvailableAppointments(Provider $provider, $format = '')
    {
        return;
    }

    /**
     * @inheritdoc
     */
    public function getNextAvailableAppointment(Provider $provider, $format = '')
    {
        return;
    }

    /**
     * @inheritdoc
     */
    public function getAppointmentsBlock(Provider $provider, $widget_slug = '')
    {
        $app = Application::getInstance();
        $appointments = array();
        
        if ($provider->getUser()->getStatus() != USER_STATUS_VISIBLE_APPOINTMENTS) {
            $appointments['type'] = 'no-available-appointments';

            return $appointments;
        }
        
        $appointment = new Appointment();
        $appointment
            ->setUser($provider->getUser())
            ->setLocation($provider->getLocation())
            ->setStartTime('1970-01-01')
            ->setEndTime('1970-01-01');
        
        $appointments = array(
            'type' => 'integration_8',
            'url'  => $appointment->getUrl($provider),
            'text' => $app->_('Freie Termine anfragen'),
            'customMessage' => $app->_('Wir kümmern uns darum und rufen Sie zurück!')
        );

        return $appointments;
    }

    /**
     * @inheritdoc
     */
    public function processApiRequest()
    {
        return;
    }

    /**
     * @inheritdoc
     */
    public function hasAppointmentEnquiry()
    {
        return true;
    }
}

