<?php

namespace Arzttermine\Integration\Integration\Samedi;

use Arzttermine\Core\DateTime;
use Arzttermine\Application\Application;
use Arzttermine\Calendar\Calendar;
use Arzttermine\Appointment\AppointmentService;
use Arzttermine\Integration\Integration\Samedi\Connection\SamediAdapter;
use Arzttermine\Appointment\Appointment as ArzttermineAppointment;

/**
 * This service is designed to handle appointments for samedi doctors and practices
 */
class IntegrationService {

    /**
     * @var self
     */
    protected static $instance;

    /**
     * @var Integration
     */
    protected $integration;

    /**
     * @var int
     */
    private $countLastUpdatedAppointments;

    /**
     * Prevent direct instantiation
     */
    protected function __construct()
    {
        $this->integration = new Integration();
    }

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @return Integration
     */
    public function getIntegration()
    {
        return $this->integration;
    }

    /**
     * EVENT_TYPES ********************************************************
     */

    /**
     * Fetchs event types for one provider from samedi,
     * deletes the old event types for that provider
     * and updates/stores them in table samedi_event_types
     *
     * @param Data $integrationData
     *
     * @return self
     *
     */
    public function updateEventTypes(Data $integrationData)
    {
        if (!$integrationData->isValid()) {
            return $this;
        }
        $app = Application::getInstance();

        $samediAdapter = SamediAdapter::getInstance();
        $eventTypes = $samediAdapter->getPracticeCategoryEventTypes(
            $integrationData->getPracticeId(),
            $integrationData->getCategoryId()
        );

        if ($eventTypes !== false) {
            // sucess => update
            $eventType = new EventType();
            /**
             * @var EventType $eventType
             */
            $eventType = $eventType->findObject('practice_id="'.$integrationData->getPracticeId().'" AND category_id='.$integrationData->getCategoryId());
            $eventType
                ->setPracticeId($integrationData->getPracticeId())
                ->setCategoryId($integrationData->getCategoryId())
                ->setEventTypes($eventTypes)
                ->setCreatedAt($app->now());

            if ($eventType->isValid()) {
                // update
                $eventType->save();
            } else {
                // insert
                $eventType->saveNewObject();
            }
        }

        return $this;
    }

    /**
     * APPOINTMENTS ********************************************************
    */
    /**
     * Fetchs available appointments for one provider from samedi,
     * deletes ALL old appointments for that provider
     * and stores them in table integration_5_appointments AND appointments
     *
     * @param Data $integrationData
     * @param DateTime $startDateTime
     * @param DateTime $endDateTime
     *
     * @return self
     *
     */
    public function updateAvailableAppointments(Data $integrationData, $startDateTime, $endDateTime)
    {
        $app = Application::getInstance();

        $this->setCountLastUpdatedAppointments(0);

        if (!$integrationData->isValid()) {
            return $this;
        }

        $samediAppointmentDataArray = $this->getAvailableAppointmentsSamedi($startDateTime, $endDateTime, $integrationData);

        /*
         * Store all samedi appointments into the integration_5_appointments table.
         * Not sure if we realy need to store this table as we transfer the appointments
         * to arzttermine appointments tables right away
         *
         */
        $samediAppointment = new Appointment();
        $samediAppointment->deleteWhere('user_id='.$integrationData->getUserId(). ' AND location_id='.$integrationData->getLocationId());

        foreach ($samediAppointmentDataArray as $startAtDatetime => $samediAppointmentData) {
            $samediAppointment = new Appointment();
            $samediAppointment
                ->setUserId($integrationData->getUserId())
                ->setLocationId($integrationData->getLocationId())
                ->setStartAt($startAtDatetime)
                ->setSamediMetadata($samediAppointmentData['metadata'])
                ->setSamediTreatmenttypeIds($samediAppointmentData['treatmenttype_ids'])
                ->setCreatedAt($app->now())
                ->saveNewObject();
        }

        /*
         * Transform Samedi appointments to Arzttermine appointments table
        */
        $appointments = array();
        // loop by each appointment datetime (it's important to have distinct datetimes here)
        foreach ($samediAppointmentDataArray as $startAtDatetime => $samediAppointmentData) {

            $appointmentAttributes = array();
            $startDateTime = new DateTime($startAtDatetime);

            // we have to create appointments for each samedi treatmenttype
            // as they could have different insurance_ids
            $samedi_treatmenttype_ids = explode(',', $samediAppointmentData['treatmenttype_ids']);
            if (!empty($samedi_treatmenttype_ids)) {

                /**
                 * Result after the loop:
                 * e.g.:
                 * Array (
                 *  [5] => Array (
                 *      [0] => 1
                 *      [1] => 2
                 *      )
                 *  [6] => Array (
                 *      [0] => 1
                 *      [1] => 2
                 *      )
                 * )
                 */
                foreach ($samedi_treatmenttype_ids as $samedi_treatmenttype_id) {
                    $samediTreatmenttype = new TreatmentType($samedi_treatmenttype_id);
                    if (!$samediTreatmenttype->isValid()) continue;

                    // build a multidim array to consolidate all appointments
                    // as our schema has csv data for insurance_ids and treatmenttype_ids (FIND_IN_SET())
                    $treatmenttype_id = $samediTreatmenttype->getParentId();
                    $appointmentAttributes[$treatmenttype_id] = array();
                    $insurance_ids = $samediTreatmenttype->getInsuranceIds();
                    foreach ($insurance_ids as $insurance_id) {
                        if (!in_array($insurance_id, $appointmentAttributes[$treatmenttype_id])) {
                            $appointmentAttributes[$treatmenttype_id][] = $insurance_id;
                            // sort them!
                            asort($appointmentAttributes[$treatmenttype_id]);
                        }
                    }
                }

                /**
                 * Transform the insurance_ids to a string assigned to each treatmenttype
                 * Array(
                 *  [5] => '1,2',
                 *  [6] => '1,2'
                 * )
                 */
                $appointmentAttributesWithInsuranceStrings = array();
                foreach ($appointmentAttributes as $treatmenttype_id => $insurance_ids) {
                    foreach ($insurance_ids as $insurance_id) {
                        $insurance_ids_string = isset($appointmentAttributesWithInsuranceStrings[$treatmenttype_id]) ? $appointmentAttributesWithInsuranceStrings[$treatmenttype_id] : '';
                        $insurance_ids_string .= ($insurance_ids_string == '' ? '' : ',') . $insurance_id;
                        $appointmentAttributesWithInsuranceStrings[$treatmenttype_id] = $insurance_ids_string;
                    }
                }

                /**
                 * Final consolidated array
                 * Array( [string $insurances] => $treatmenttype_ids, ... )
                 * e.g.:
                 * Array (
                 *  ['1,2'] => Array (
                 *      [0] => 5
                 *      [1] => 6
                 *      )
                 * )
                 */
                $insurancesStringArray = array();
                foreach ($appointmentAttributesWithInsuranceStrings as $treatmenttype_id => $insurance_ids_string) {
                    $insurancesStringArray[$insurance_ids_string][] = $treatmenttype_id;
                }

                /**
                 * create the appointments
                 */
                foreach ($insurancesStringArray as $insurance_ids_string => $treatmenttype_ids) {
                    $appointment = new ArzttermineAppointment();
                    $appointment
                        ->setUserId($integrationData->getUserId())
                        ->setLocationId($integrationData->getLocationId())
                        ->setDate($startDateTime->getDate())
                        ->setStartTime($startDateTime->getDateTime())
                        ->setInsuranceIds(explode(',', $insurance_ids_string))
                        ->setTreatmentTypeIds($treatmenttype_ids);

                    $appointments[] = $appointment;
                }
            }
        }

        /*
         * Save the appointments
         */
        $appointmentService = AppointmentService::getInstance();
        $appointmentService->flush($integrationData->getUser(), $integrationData->getLocation());
        $appointmentService->save($appointments);
        $this->addCountLastUpdatedAppointments(count($appointments));

        return $this;
    }

    /**
     * Returns all times for a date period
     *
     * The samedi api allows queries for 30 days per request only
     * so this is a wrapper for the 30 day limit!
     * This call will eventually call the TerminlandAPI multiple times
     * (via getAvailableAppointmentsSamediSingleCall())
     *
     * @param DateTime $startDateTime
     * @param DateTime $endDateTime
     * @param Data $integrationData
     *
     * @return array Appointment || bool false on error
     */
    public function getAvailableAppointmentsSamedi($startDateTime, $endDateTime, $integrationData)
    {
        $appointments = array();
        $dates = Calendar::getDatesInRange($startDateTime, $endDateTime);

        if ($dates) {
            $start = clone $startDateTime;
            $start->setTime(0, 0, 0);
            $end = clone $endDateTime;
            $end->setTime(23, 59, 59);

            // circle the 30 day limit
            $days = 0;
            foreach ($dates as $date) {
                $days++;
                if ($days == 1) {
                    $start = clone $date;
                    $start->setTime(0, 0, 0);
                }

                $end = clone $date;
                $end->setTime(23, 59, 59);

                if ($days == 30) {
                    $period_appointments = $this->getAvailableAppointmentsSamediSingleCall($start, $end, $integrationData);
                    if ($period_appointments) {
                        if (is_array($period_appointments) && !empty($period_appointments)) {
                            $appointments = $appointments + $period_appointments;
                        }
                    }
                    $days = 0;
                }
            }

            // Finalize if there are open days
            if ($days > 0) {
                $period_appointments = $this->getAvailableAppointmentsSamediSingleCall($start, $end, $integrationData);
                if ($period_appointments) {
                    if (is_array($period_appointments) && !empty($period_appointments)) {
                        $appointments = $appointments + $period_appointments;
                    }
                }
            }
        }

        return $appointments;
    }

    /**
     * Returns all times for a date period but not more than 30 days per call
     * The Samedi api allows queries for 30 days per request only (todo: is that right? Even if not, this works...)
     *
     * @param DateTime $startDateTime
     * @param DateTime $endDateTime
     * @param Data $integrationData
     *
     * @return array
     */
    public function getAvailableAppointmentsSamediSingleCall($startDateTime, $endDateTime, $integrationData)
    {
        $samediAdapter = SamediAdapter::getInstance();
        $eventType = EventType::getEventTypeFor(
            $integrationData->getPracticeId(),
            $integrationData->getCategoryId()
        );
        $eventTypes = $eventType->getEventTypes();

        $appointments = $samediAdapter->getAvailableAppointments(
            $startDateTime,
            $endDateTime,
            $integrationData->getCategoryId(),
            $integrationData->getPracticeId(),
            $eventTypes
        );

        return $appointments;
    }

    /**
     * NETWORKS ********************************************************
     */
    /**
     * Syncs the samedi contacts list (network list)
     *
     * @return
     */
    public static function syncNetwork()
    {
        $samediAdapter = SamediAdapter::getInstance();
        $networks = $samediAdapter->getNetwork(0, 500);

        if (is_array($networks)) {
            $network = new Network();
            $network->truncate();

            /**
             * @var Network $network
             */
            foreach ($networks as $network) {
                $network = new Network();
                $network
                    ->setData(json_encode($network))
                    ->saveNewObject();
            }
        }
        return;
    }

    /**
     * ********************************************************
     */
    /**
     * @return int
     */
    public function getCountLastUpdatedAppointments()
    {
        return $this->countLastUpdatedAppointments;
    }

    /**
     * @param int $count
     * @return self
     */
    public function setCountLastUpdatedAppointments($count)
    {
        $this->countLastUpdatedAppointments = $count;
        return $this;
    }

    /**
     * @param int $count
     * @return self
     */
    public function addCountLastUpdatedAppointments($count)
    {
        $this->countLastUpdatedAppointments += $count;
        return $this;
    }

}