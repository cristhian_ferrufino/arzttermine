<?php

namespace Arzttermine\Integration\Integration\Samedi\Connection;

class HttpGetRequest {

	private $reqHeaders;
	private $reqCookies;
	private $reqParams;
	private $timeout;
	private $isSecure;
	private $userName;
	private $password;
	private $post;
	private $postData;
	private $ignoreCertificate;
	private $resHeaders;
	private $resCookies;
	private $body;

	public function __construct() {
		$this->reqHeaders = array();
		$this->reqCookies = array();
		$this->timeout = null;
		$this->isSecure = false;
		$this->resHeaders = null;
		$this->resCookies = null;
		$this->body = null;
		$this->post = false;
		$this->ignoreCertificate = true;
	}

	public function setPostDataArray($dataArr) {
		$stringData = "";
		foreach ($dataArr as $key => $value) {
			$stringData .= ($key) . '=' . urlencode($value) . '&';
		}
		$stringData = rtrim($stringData, '&');
		$this->setPostData($stringData);
	}

	public function setPostData($data) {
		if ($data && !empty($data)) {
			$this->postData = $data;
			$this->post = 1;
		} else {
			$this->post = 0;
		}
	}

	public function setHeaders($headers) {
		$this->reqHeaders = $headers;
	}

	public function setCookies($cookies) {
		$this->reqCookies = $cookies;
	}

	public function setParams($params) {
		$this->reqParams = $params;
	}

	public function setTimeout($timeout) {
		$this->timeout = $timeout;
	}

	public function useSSL($userName, $password) {
		$this->isSecure = true;
		$this->userName = $userName;
		$this->password = $password;
	}

	public function ignoreCertificate() {
		$this->ignoreCertificate = true;
	}

	/*@desc for initiating http get request
	 *@access public
	 *@param url of requesting host
	 *@return true if all is OK, false if thereare errors
	 */
	public function request($url, $requestAction = null) {
		$curl = curl_init();
		$headers = $this->reqHeaders;

		if (!empty($this->reqCookies)) {
			$headers["Cookie"] = $this->reqCookies;
		}

		$reqHeaders = array();
		foreach ($headers as $key => $value) {
			$reqHeaders[] = "$key: $value";
		}

		curl_setopt($curl, CURLOPT_HTTPHEADER, $reqHeaders);
		curl_setopt($curl, CURLOPT_HEADER, TRUE);

		if ($requestAction === "DELETE") {
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
		}

		// set URL
		if ($this->reqParams && count($this->reqParams) > 0) {
			$url .= "?";
			foreach ($this->reqParams as $key => $value) {
				$url .= urlencode($key) . "=" . urlencode($value) . "&";
			}
		}
		curl_setopt($curl, CURLOPT_URL, $url);

		if ($this->isSecure) {
			curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($curl, CURLOPT_USERPWD, $this->userName . ":" . $this->password);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
		}
		if ($this->ignoreCertificate) {
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
		}

		if ($this->post) {
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $this->postData);			
		}

		curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);

		// Set timeout
		if ($this->timeout) {
			curl_setopt($curl, CURLOPT_TIMEOUT, $this->Timeout);
		}

		// write content in $doc
		$response = curl_exec($curl);
		$httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		// close connection
		curl_close($curl);

		$this->parse_http_response($response);

		$status = false;
		// there is no "status" field anymore. Looks like samedi changed the response format.
		// so we need to check the http status code and transfer that one to the old "OK"-Code: "2"
		switch ($httpCode) {
			case 200:
			case 301:
			case 302:
				$status = 2;
				break;
		}
		return $status;
		// return $this->resHeaders['status'][0];
	}

	function parse_http_response($string) {

		$headers = array();
		$content = '';
		$str = strtok($string, "\n");
		$h = null;
		while ($str !== false) {
			if ($h and trim($str) === '') {
				$h = false;
				continue;
			}
			if ($h !== false and false !== strpos($str, ':')) {
				$h = true;
				list($headername, $headervalue) = explode(':', trim($str), 2);
				$headername = strtolower($headername);
				$headervalue = ltrim($headervalue);
				if (isset($headers[$headername]))
					$headers[$headername] .= ',' . $headervalue;
				else
					$headers[$headername] = $headervalue;
			}
			if ($h === false) {
				$content .= $str . "\n";
			}
			$str = strtok("\n");
		}
		$this->resHeaders = $headers;
		if ($headers && isset($headers['set-cookie'])) {
			$this->resCookies = $headers['set-cookie'];
		}
		$this->body = trim($content);
	}

	public function getHeaders() {
		return $this->resHeaders;
	}

	public function getBody() {
		return $this->body;
	}

	public function getCookies() {
		return $this->resCookies;
	}

}