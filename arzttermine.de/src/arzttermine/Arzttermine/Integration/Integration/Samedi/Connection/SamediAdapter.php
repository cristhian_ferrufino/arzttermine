<?php

namespace Arzttermine\Integration\Integration\Samedi\Connection;

use Arzttermine\Core\Configuration;
use Arzttermine\Core\DateTime;
use Arzttermine\Integration\Integration\Samedi\Types\CategoryEventType;
use Arzttermine\Integration\Integration\Samedi\Types\SamediPatient;
use Arzttermine\Integration\Integration\Samedi\Types\SPatient;
use Arzttermine\Integration\Integration\Samedi\Types\STime;

class SamediAdapter  {
    /**
     * @var string
     */
	private $protocol;
    /**
     * @var string
     */
	private $practiceId;
    /**
     * @var string
     */
    private $practiceLogin;
    /**
     * @var string
     */
    private $practicePasword;
    /**
     * @var string
     */
	private $lastErrorStatus;

	private static $instance;

	const SAMEDI_API_PROTOCOL = 'https';
	const SAMEDI_API_ROOT_URL = 'app.samedi.de';
	const SAMEDI_API_COOKIE_DATA_KEY = 'samedi_api_cookie_data';
	const SAMEDI_CHANGED_DOCTORS_IDS_KEY = 'samedi_changed_doctors_ids';

	private function __construct() {
		$this->practiceLogin = $GLOBALS['CONFIG']['INTEGRATION_SAMEDI_PRACTICE_LOGIN'];
        $this->practicePasword = $GLOBALS['CONFIG']['INTEGRATION_SAMEDI_PRACTICE_PASSWORD'];
        $this->practiceId = $GLOBALS['CONFIG']['INTEGRATION_SAMEDI_PRACTICE_ID'];

		$this->protocol = self::SAMEDI_API_PROTOCOL.'://';
		$this->url = self::SAMEDI_API_ROOT_URL;
	}

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

	public function login() {
		$action = "/login";
		$url = $this->protocol . $this->url . $action;
		$q = new HttpGetRequest();
		$q->setPostData(
			array(
				"user[pseudonym]" => $this->practiceLogin,
				"user[password]" => $this->practicePasword
			)
		);
		$status = $q->request($url);
		if ($status == 2 || $status == 3) {
			$cookieParams = $q->getCookies();

            $configuration = Configuration::getInstance();
            $configuration
                ->optionSet(self::SAMEDI_API_COOKIE_DATA_KEY, $cookieParams)
                ->optionSave(self::SAMEDI_API_COOKIE_DATA_KEY, $cookieParams);

			return true;
		} else {
			$headers = $q->getHeaders();
			$this->lastErrorStatus = $headers['status'];
			return false;
		}
	}

	/**
	 * Returns true if connection is ready, false if not logged in.
	 */
	public function isConnected() {
		$action = "";
		$params = "/account.json";
		$url = $this->protocol . $this->url . $action . $params;
		$q = new HttpGetRequest();
		$cookieParams = $this->getSavedCookieParams();
		$q->setCookies($cookieParams);
		$statusCode = $q->request($url);
		return $statusCode == 2;
	}

	/**
	 * Read the previously saved cookie params from file and retrun it as string.
	 */
	private function getSavedCookieParams() {
            return Configuration::get(self::SAMEDI_API_COOKIE_DATA_KEY);
	}

	/**
	 * * @param $loginFirst if true then function try to login to Samdi first and then continue.
	 *			  otherwise will work with previuse session. By defauut it's false.
	 * get Referers practices of our samedi practice.
	 * Return format is following
	 * array('practiceId '=> 'PracticeName', .... )
	 */
	public function getNetwork($offset, $limit, $loginFirst = false) {
		if ($loginFirst) {
			if (!$this->login()) {
				return array();
			}
		}
		$action = "/api/contacts/v1/";
		$params = $this->practiceId."/contacts/search.json?namespace=network_v2&start=$offset&limit=$limit";
		$url = $this->protocol . $this->url . $action . $params;
		$q = new HttpGetRequest();
		$q->setCookies($this->getSavedCookieParams());
		$statusCode = $q->request($url);
		if ($statusCode == 2) {
			$json = $q->getBody();
			$practices = json_decode($json);
			return $practices->root ;
		} else {
			if (!$this->isConnected() && !$loginFirst) {
				return $this->getReferrers(true);
			}
			$headers = $q->getHeaders();
			$this->lastErrorStatus = $headers['status'];
			return array();
		}
	}

	/**
	 * * @param $loginFirst if true then function try to login to Samdi first and then continue.
	 *			  otherwise will work with previuse session. By defauut it's false.
	 * get Referers practices of our samedi practice.
	 * Return format is following
	 * array('practiceId '=> 'PracticeName', .... )
	 */
	public function getReferrers($loginFirst = false) {
		if ($loginFirst) {
			if (!$this->login()) {
				return false;
			}
		}
		$action = "/practices";
		$params = "/".$this->practiceId."/referrers.json";
		$url = $this->protocol . $this->url . $action . $params;
		$q = new HttpGetRequest();
		$cookieParams = $this->getSavedCookieParams();
		$q->setCookies($cookieParams);
		$statusCode = $q->request($url);
		if ($statusCode == 2) {
			$json = $q->getBody();
			$practices = json_decode($json);
			return $practices->root ;
		} else {
			if (!$this->isConnected() && !$loginFirst) {
				return $this->getReferrers(true);
			}
			$headers = $q->getHeaders();
			$this->lastErrorStatus = $headers['status'];
			return false;
		}
	}

    /**
     * @param null $practiceId
     * @param bool $loginFirst
     *
     * @return array
     */
    public function getPracticeCategories($practiceId = null, $loginFirst = false) {
		if ($loginFirst) {
			if (!$this->login()) {
				return array();
			}
		}
		if ($practiceId === null) {
			$practiceId = $this->practiceId;
		}
		$action = "/practices";
		$params = "/" . $practiceId . "/calendar/categories.json";
		$url = $this->protocol . $this->url . $action . $params;
		$q = new HttpGetRequest();
		$cookieParams = $this->getSavedCookieParams();
		$q->setCookies($cookieParams);
		$statusCode = $q->request($url);
		if ($statusCode == 2) {
			$json = $q->getBody();
			$categories = json_decode($json);
			return $categories->root;
		} else {
			if (!$this->isConnected() && !$loginFirst) {
				return $this->getPracticeCategories($practiceId, true);
			}
			$headers = $q->getHeaders();
			$this->lastErrorStatus = $headers['status'];
			return array();
		}
	}

	/**
	 * @param $practiceId practice id for which getting the event types
	 * @param $categoryId category id for which getting the event types,
	 * 				by default it's null and it means will return all event types for all categories.
	 * @param $forSelect return type will have less details,
	 * 				sometimes it used for filling the event types in the listbox. By default it's true
	 * @param $loginFirst if true then function try to login to Samdi first and then continue.
	 *			  otherwise will work with previuse session. By defauut it's false.
	 *
	 * Returns practice's all Event Types.	
	 */
	public function getPracticeCategoryEventTypes($practiceId = null, $categoryId = null, $forSelect = true, $loginFirst = false) {
		if ($loginFirst) {
			if (!$this->login()) {
				return false;
			}
		}

		if ($practiceId === null) {
			$practiceId = $this->practiceId;
		}
		$action = "/practices";
		if ($forSelect) {
			$params = "/" . $practiceId . "/calendar/event_types/for_select.json";
		} else {
			$params = "/" . $practiceId . "/calendar/event_types.json";
		}
		if ($categoryId) {
			$params .= "?event_category_id=" . $categoryId;
		}
		$url = $this->protocol . $this->url . $action . $params;
		$q = new HttpGetRequest();
		$cookieParams = $this->getSavedCookieParams();
		$q->setCookies($cookieParams);
		$statusCode = $q->request($url);
		if ($statusCode == 2) {
			$json = $q->getBody();
			$eventTypes = json_decode($json);
			$ret = $eventTypes->root;
			return $ret;
		} else {
			if (!$this->isConnected() && !$loginFirst) {
				return $this->getPracticeCategoryEventTypes($practiceId, $categoryId, $forSelect, true);
			}
			$headers = $q->getHeaders();
			$this->lastErrorStatus = $headers['status'];
			return false;
		}
	}

	/**
	 *
	 * * @param $loginFirst if true then function try to login to Samdi first and then continue.
	 *			  otherwise will work with previuse session. By defauut it's false.
	 * Returns times for specified date, for given $practiceId, $eventTypeId, $eventCategoryId
	 * Return format is following
	 * array(TimeToken, ...)
	 */
	public function getDaysForSpecificMonth($practiceId = null, $eventTypeId, $eventCategoryId, $Y, $M, $loginFirst = false) {
		if ($loginFirst) {
			if (!$this->login()) {
				return false;
			}
		}
		if ($practiceId === null) {
			$practiceId = $this->practiceId;
		}
		$action = "/practices";
		$params = "/" . $practiceId . "/calendar/event_types/" . $eventTypeId . "/availability/" . $Y . "/" . $M . ".json?event_category_id=" . $eventCategoryId;
		$url = $this->protocol . $this->url . $action . $params;
		$q = new HttpGetRequest();
		$cookieParams = $this->getSavedCookieParams();
		$q->setCookies($cookieParams);
		$statusCode = $q->request($url);
		if ($statusCode == 2) {
			$json = $q->getBody();
			$days = json_decode($json);
			$ret = $days->root;
			return $ret;
		} else {
			if (!$this->isConnected() && !$loginFirst) {
				return $this->getDaysForSpecificMonth($practiceId, $eventTypeId, $eventCategoryId, $Y, $M, true);
			}
			return false;
		}
	}

	/**
	 * @param DateTime $startDate
	 * @param DateTime $endDate
	 * @param $categoryId
	 * @param $practiceId
	 * @param string[] $eventTypes
	 * @return array
	 *
	 * Returning format is following
	 * array(timestamp => array($eventType, $time, eventType, $time), ...)
	 */
	public function getAvailableAppointments(DateTime $startDate, DateTime $endDate, $categoryId, $practiceId, $eventTypes) {
		$grouped_times = array();

		// todo: raise an error if the eventTypes is empty or it will fail silent
		if (!empty($eventTypes)) {
			foreach ($eventTypes as $eventType) {
				$eventType = new CategoryEventType($eventType);
				$times = $this->getTimesForSpecificPeriod($practiceId, $eventType, $categoryId, $startDate, $endDate);
				if ($times !== false) {
					foreach ($times as $time) {
						$stime = new STime($time);
						$t = date_create_from_format('Y-m-d?H:i:sP', $stime->getTime());
						$aptDateTime =$t->format('Y-m-d H:i:s');
						if (isset($grouped_times[$aptDateTime ])) {
							// in case this appointment datetime has more than just one possible treatmenttype
							$grouped_times[$aptDateTime ]['treatmenttype_ids'] .= ','.$eventType->getId();
						} else {
							$grouped_times[$aptDateTime ]['metadata'] = json_encode($time);
							$grouped_times[$aptDateTime ]['treatmenttype_ids'] = $eventType->getId();
						}
					}
				}
			}
			ksort($grouped_times);
		}
		return $grouped_times;
	}

	/**
	 * @param null $practiceId
	 * @param CategoryEventType $eventType
	 * @param $categoryId
	 * @param DateTime $startDate
	 * @param DateTime $endDate
	 * @param bool $loginFirst
	 * @return bool
	 */
	public function getTimesForSpecificPeriod($practiceId = null, CategoryEventType $eventType, $categoryId, DateTime $startDate, DateTime $endDate, $loginFirst = false) {
		if ($loginFirst) {
			if (!$this->login()) {
				return false;
			}
		}
		if ($practiceId === null) {
			$practiceId = $this->practiceId;
		}

		$action = "/api/calendar/v1";
		$params = "/" . $practiceId . "/availabilities/times?";
		$params .= "event_types[0][event_type_id]=" . $eventType->getId();
		$params .= "&event_types[0][duration]=" . $eventType->getDuration();
		$params .= "&event_types[0][insurance_company]=" . $eventType->getInsuranceCompanyConstraints();
		$params .= "&event_types[0][event_category_id]=" . $categoryId . "&";
		$params .= "from=".$startDate->format("Y-m-d");
		$params .= "&to=" .$endDate->format("Y-m-d") . "T23:59:59";

		$url = $this->protocol . $this->url . $action . $params;
		$q = new HttpGetRequest();
		$cookieParams = $this->getSavedCookieParams();
		$q->setCookies($cookieParams);
		$statusCode = $q->request($url);
		if ($statusCode == 2) {
			$json = $q->getBody();
			$times = json_decode($json);
			$ret = $times->root;
			return $ret;
		} else {
			if (!$this->isConnected() && !$loginFirst) {
				return $this->getTimesForSpecificPeriod($practiceId, $eventType, $categoryId, $startDate, $endDate, true);
			}
			$headers = $q->getHeaders();
			$this->lastErrorStatus = $headers['status'];
			return false;
		}
	}

	/**
	 * @param $loginFirst if true then function try to login to Samdi first and then continue.
	 *			  otherwise will work with previuse session. By defauut it's false.
	 * Returns times for specified date, for given $practiceId, $eventTypeId, $eventCategoryId
	 * Return format is following
	 * array(TimeToken, ...)
	 */
	public function getTimesForSpecificDay($practiceId = null, $eventTypeId, $eventCategoryId, $Y, $M, $D, $loginFirst = false) {
		if ($loginFirst) {
			if (!$this->login()) {
				return false;
			}
		}
		if ($practiceId === null) {
			$practiceId = $this->practiceId;
		}

		$action = "/practices";
		$params = "/" . $practiceId . "/calendar/event_types/" . $eventTypeId . "/times/" . $Y . "/" . $M . "/" . $D . ".json?event_category_id=" . $eventCategoryId;
		$url = $this->protocol . $this->url . $action . $params;
		$q = new HttpGetRequest();
		$cookieParams = $this->getSavedCookieParams();
		$q->setCookies($cookieParams);
		$statusCode = $q->request($url);
		if ($statusCode == 2) {
			$json = $q->getBody();
			$times = json_decode($json);
			$ret = $times->root;
			return $ret;
		} else {
			if (!$this->isConnected() && !$loginFirst) {
				return $this->getTimesForSpecificDay($practiceId, $eventTypeId, $eventCategoryId, $Y, $M, $D, true);
			}
			$headers = $q->getHeaders();
			$this->lastErrorStatus = $headers['status'];
			return false;
		}
	}

	/**
	 * Books appointment for Master the practice
	 * Returns times for specified date, for given $practiceId, $eventTypeId, $eventCategoryId
	 * Return format is following
	 * array(TimeToken, ...)
	 *
	 * @param $practiceId
	 * @param $eventTypeId
	 * @param $eventCategoryId
	 * @param STime $time
	 * @param SPatient $patient
	 * @param bool $loginFirst if true then function try to login to Samdi first and then continue.
	 *			otherwise will work with previuse session. By default it's false.
	 * @return bool|mixed
     */
	public function book($practiceId , $eventTypeId, $eventCategoryId, STime $time, SPatient $patient, $loginFirst = false) {
		if ($loginFirst) {
			if (!$this->login()) {
				return false;
			}
		}
		$postData = array(
            "event[event_category_id]" => $eventCategoryId,
            "event[event_type_id]" => $eventTypeId,
            "event[starts_at]" => $time->getTime(),
            "event[token]" => $time->getToken(),
            "event[attendant][xref]" => $this->practiceLogin,
            "event[attendant][id]" => $patient->getId()
        );

		$action = "/practices";
		if ($practiceId == null) {
			$practiceId = $this->practiceId;
		}
		$params = "/" . $practiceId . "/calendar/events.json";
		$url = $this->protocol . $this->url . $action . $params;
		$q = new HttpGetRequest();
		$cookieParams = $this->getSavedCookieParams();
		$q->setCookies($cookieParams);
		$q->setPostData($postData);
		$statusCode = $q->request($url);
		if ($statusCode == 2) {
			$json = $q->getBody();
			$book = json_decode($json);
			return $book;
		} else {
			if (!$this->isConnected() && !$loginFirst) {
				return $this->book($practiceId, $eventTypeId, $eventCategoryId, $time, $patient, true);
			}
			$headers = $q->getHeaders();
			$this->lastErrorStatus = $headers['status'];
			return false;
		}
	}

	public function getAllPendingBookedAppointments($offset, $limit, $loginFirst = false) {
		if ($loginFirst) {
			if (!$this->login()) {
				return array();
			}
		}
		$action = "/practices";
		$params = "/" . $this->practiceId . "/calendar/referrals/outgoing.json?start=" . $offset . "&limit=" . $limit;		
		$url = $this->protocol . $this->url . $action . $params;		
		$q = new HttpGetRequest();
		$cookieParams = $this->getSavedCookieParams();
		$q->setCookies($cookieParams);
		$statusCode = $q->request($url);
		if ($statusCode == 2) {
			$json = $q->getBody();
			$bookings = json_decode($json);
			$ret = $bookings->root;
			return $ret;
		} else {
			if (!$this->isConnected() && !$loginFirst) {
				return $this->getAllPendingBookedAppointments($offset, $limit, true);
			}
			$headers = $q->getHeaders();
			$this->lastErrorStatus = $headers['status'];
			return array();
		}
	}
	
	public function getBookedAppointment($bookId, $loginFirst = false) {
		if ($loginFirst) {
			if (!$this->login()) {
				return false;
			}
		}
		$action = "/api/calendar/v1";
		$params = "/" . $this->practiceId . "/events/" . $bookId . ".json";
		$url = $this->protocol . $this->url . $action . $params;
		$q = new HttpGetRequest();
		$cookieParams = $this->getSavedCookieParams();
		$q->setCookies($cookieParams);
		$statusCode = $q->request($url);
		if ($statusCode == 2) {
			$json = $q->getBody();
			$booked = json_decode($json);
			return $booked;
		} else {
			if (!$this->isConnected() && !$loginFirst) {
				return $this->getBookedAppointment($bookId, true);
			}
			$headers = $q->getHeaders();
			$this->lastErrorStatus = $headers['status'];
			return false;
		}
	}

	/**
	* @return true of false 
	*/
	public function cancelBookedAppointment($practiceId, $bookId, $loginFirst = false) {
		if ($loginFirst) {
			if (!$this->login()) {
				return false;
			}
		}
		$action = "/practices";
		
		$params = "/" . $practiceId . "/calendar/events/" . $bookId . ".json";
		
		$url = $this->protocol . $this->url . $action . $params;
		$q = new HttpGetRequest();
		$cookieParams = $this->getSavedCookieParams();
		$q->setCookies($cookieParams);
		$statusCode = $q->request($url, "DELETE");
		if ($statusCode == 2) {
			$json = $q->getBody();
			$cancel = json_decode($json);
			return $cancel->success;
		} else {
			if (!$this->isConnected() && !$loginFirst) {
				return $this->cancelBookedAppointment($practiceId,$bookId, true);
			}
			$headers = $q->getHeaders();
			$this->lastErrorStatus = $headers['status'];
			return false;
		}
	}

	
	public function getLastError() {
		return $this->lastErrorStatus;
	}

	/**
	 * Returns patients for Master practice
	 */
	public function getPatients($offset, $limit, $loginFirst = false) {
		if ($loginFirst) {
			if (!$this->login()) {
				return false;
			}
		}
		$action = "/practices";
		$params = "/" . $this->practiceId . "/patients.json?namespace=patients&start=" . $offset . "&limit=" . $limit;
		$url = $this->protocol . $this->url . $action . $params;
		$q = new HttpGetRequest();
		$cookieParams = $this->getSavedCookieParams();
		$q->setCookies($cookieParams);
		$statusCode = $q->request($url);
		if ($statusCode == 2) {
			$json = $q->getBody();
			$patients = json_decode($json);
			$ret = $patients->root;
			return $ret;
		} else {
			if (!$this->isConnected() && !$loginFirst) {
				return $this->getPatients($offset, $limit, true);
			}
			$headers = $q->getHeaders();
			$this->lastErrorStatus = $headers['status'];
			return false;
		}
	}

	public function deletePatitnet() {
		//https://www.samedi.de/practices/'.$this->practiceId.'/patients/p9f17e81a99.json
		//Request Method:DELETE
		//{"success":true}

	}

	/**
	 * @param SamediPatient $patient
	 * @param bool $loginFirst
	 * @return bool|array
     */
	public function addPatient(SamediPatient $patient, $loginFirst = false) {

		if ($loginFirst) {
			if (!$this->login()) {
				return false;
			}
		}
		$postData = array("patient[data]" => "JSON:23373:" . json_encode($patient));

		$action = "/practices";

		$params = "/" . $this->practiceId . "/patients.json";
		$url = $this->protocol . $this->url . $action . $params;
		$q = new HttpGetRequest();
		$cookieParams = $this->getSavedCookieParams();
		$q->setCookies($cookieParams);
		$q->setPostData($postData);
		$statusCode = $q->request($url);
		if ($statusCode == 2) {
			$json = $q->getBody();
			$patient = json_decode($json);
			return $patient->data;
		} else {
			if (!$this->isConnected() && !$loginFirst) {
				return $this->addPatient($patient, true);
			}
			$headers = $q->getHeaders();
			$this->lastErrorStatus = $headers['status'];
			return false;
		}

	}

}