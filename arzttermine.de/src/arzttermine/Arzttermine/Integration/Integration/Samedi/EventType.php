<?php

namespace Arzttermine\Integration\Integration\Samedi;

use Arzttermine\Core\Basic;
use Arzttermine\Integration\Integration\Samedi\Types\CategoryEventType;

class EventType extends Basic {

    /**
     * @var string
     */
    protected $tablename = DB_TABLENAME_INTEGRATION_5_EVENTTYPES;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $practice_id = '';

    /**
     * @var int
     */
    protected $category_id = '';

    /**
     * @var string
     */
    protected $event_types = '';

    /**
     * @var string
     */
    protected $created_at = '';

    /**
     * @var array
     */
    protected $all_keys
        = array(
            "id",
            "practice_id",
            "category_id",
            "event_types",
            "created_at"
        );

    /**
     * @var array
     */
    protected $secure_keys
        = array(
            "practice_id",
            "category_id",
            "event_types",
        );

    /**
     * @return string
     */
    public function getPracticeId()
    {
        return $this->practice_id;
    }

    /**
     * @param string $practice_id
     * @return self
     */
    public function setPracticeId($practice_id)
    {
        $this->practice_id = $practice_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getCategoryId()
    {
        return $this->category_id;
    }

    /**
     * @param int $category_id
     * @return self
     */
    public function setCategoryId($category_id)
    {
        $this->category_id = $category_id;
        return $this;
    }

    /**
     * This comes from Samedi API and should contain an CategoryEventType array
     * @return CategoryEventType[]
     */
    public function getEventTypes()
    {
        return json_decode($this->event_types);
    }

    /**
     * @param CategoryEventType[]
     * @return self
     */
    public function setEventTypes($event_types)
    {
        $this->event_types = json_encode($event_types);
        return $this;
    }

    /**
     * @param $practiceId
     * @param $categoryId
     * @return EventType|bool|object
     */
    public static function getEventTypeFor($practiceId, $categoryId) {
        $eventType = new self();
        $eventType = $eventType->findObject('practice_id="'.$practiceId.'" AND category_id='.$categoryId);
        return $eventType;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param string $created_at
     * @return self
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
        return $this;
    }
}