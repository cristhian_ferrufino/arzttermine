<?php

namespace Arzttermine\Integration\Integration\Samedi;

use Arzttermine\Application\Application;
use Arzttermine\Appointment\Provider;
use Arzttermine\Appointment\Appointment as ArzttermineAppointment;
use Arzttermine\Booking\Booking as PatientBooking;
use Arzttermine\Core\Configuration;
use Arzttermine\MedicalSpecialty\TreatmentType as ArzttermineTreatmenttype;
use Arzttermine\Integration\Integration as IntegrationCore;
use Arzttermine\Integration\Integration\Samedi\Connection\SamediAdapter;
use Arzttermine\Integration\Integration\Samedi\Types\SamediPatient;
use Arzttermine\Integration\Integration\Samedi\Types\SPatient;
use Arzttermine\Integration\Integration\Samedi\Types\STime;
use Arzttermine\Integration\Integration\Samedi\Types\SBooking;
use Arzttermine\Mail\SMS;
use NGS\Managers\DoctorsManager;

class Integration extends IntegrationCore
{

    /**
     * @var int
     */
    protected $id = 5;

    /**
     * @var int
     */
    protected $user_id;

    /**
     * @var int
     */
    protected $location_id;

    /**
     * @inheritdoc
     */
    public function processApiRequest()
    {
        return;
    }

    /**
     * Checks if user and location fit by its integration_data
     *
     * @param int $user_id
     * @param int $location_id
     *
     * @return bool
     */
    public function isConnected($user_id, $location_id)
    {
        $samediData = new Data();
        return ($samediData->count('user_id=' . $user_id . ' AND location_id=' . $location_id) > 0) ?
            true : false;
    }

    /**
     * @inheritdoc
     */
    public function getAvailableAppointments(Provider $provider, $format = '')
    {
        return DoctorsManager::getInstance()->getAvailableAppointments($datetime_start, $datetime_end, $user_id, $location_id, $insurance_id, $treatmenttype_ids);
    }

    /**
     * @inheritdoc
     */
    public function getNextAvailableAppointment(Provider $provider, $format = '')
    {
        return;
    }

    /**
     * Book an appointment
     *
     * @param PatientBooking $booking
     *
     * @return bool
     */
    public function bookAppointment(PatientBooking $booking)
    {
        $status = $this->bookSamediAppointment($booking);

        // save the status and push to Salesforce
        $booking->saveStatus($status, true);

        if ($status == PatientBooking::STATUS_RECEIVED) {
            SMS::sendBookingConfirmationSms($booking);
            PatientBooking::sendBookingEmail($booking, 'booking-new-confirmed.html');
            return true;
        }
        return false;
    }

    /**
     * Book an appointment via the samedi api
     *
     * @param PatientBooking $booking
     *
     * @return int PatientBooking::status
     */
    public function bookSamediAppointment(PatientBooking $booking)
    {
        $app = Application::getInstance();

        // check for invalid status
        if ($booking->getIntegrationStatus() == BOOKING_INTEGRATION_STATUS_BOOKED || $booking->getIntegrationStatus() == BOOKING_INTEGRATION_STATUS_CANCELED) {
            if ($booking->getIntegrationStatus() == BOOKING_INTEGRATION_STATUS_BOOKED) {
                $this->setLastErrorMessage('This booking status is already BOOKED');
                return PatientBooking::STATUS_INVALID;
            }
            if ($booking->getIntegrationStatus() == BOOKING_INTEGRATION_STATUS_CANCELED) {
                $this->setLastErrorMessage('This booking status is CANCELED');
                return PatientBooking::STATUS_INVALID;
            }
        }

        // prepare the data we need
        $samediData = new Data();
        $samediData = $samediData->getByLocationAndUser($booking->getLocation(), $booking->getUser());
        if (!$samediData->isValid()) {
            $this->setLastErrorMessage('Internal Error.');
            return PatientBooking::STATUS_INVALID;
        }

        // prepare the samedi appointment which will give us the possible samedi treatmenttypes (event_types)
        // that we need to remap from the arzttermine treatmenttype
        $samediAppointment = new Appointment();
        /**
         * @var Appointment $samediAppointment
         */
        $samediAppointment = $samediAppointment->findObject(
            'location_id='.$booking->getLocationId().
            ' AND user_id='.$booking->getUserId().
            ' AND start_at="'.$booking->getAppointmentStartAt().'"'
        );
        if (!$samediAppointment->isValid()) {
            $this->setLastErrorMessage('Der von Ihnen gewählte Termin ist leider nicht mehr verfügbar. Bitte buchen Sie einen anderen Termin.');
            return PatientBooking::STATUS_INVALID;
        }
        // get the Samedi TreatmentTypeId (=eventType)
        $samediTreatmentType = $this->mapArzttermineTreatmentTypeToSamediTreatmentType($booking->getTreatmentType(), $samediAppointment);
        if (!$samediTreatmentType->isValid()) {
            $this->setLastErrorMessage('Der von Ihnen gewählte Behandlungsgrund ist nicht verfügbar. Bitte kontaktieren Sie den Support.');
            return PatientBooking::STATUS_INVALID;
        }

        // store a new integration samedi booking
        $samediBooking = new Booking();
        // save with error status which will stay as a default
        $samediBooking
            ->setStatus(Booking::STATUS_ERROR)
            ->setBookingId($booking->getId())
            ->setEventTypeId($samediTreatmentType->getId()) // a bit confusing but correct. Our TreatmentType model reflects the Samedi Event Type
            ->setPracticeId($samediData->getPracticeId())
            ->setCategoryId($samediData->getCategoryId())
            ->setCreatedBy($app->getCurrentUser()->getId())
            ->setCreatedAt($app->now())
            ->saveNewObject();

        if (!$samediBooking->isValid()) {
            $this->setLastErrorMessage('Internal Error.');
            return PatientBooking::STATUS_INVALID;
        }

        // make the booking via the samedi api
        $samediAdapterBooking = $this->bookSamediAdapterAppointment($booking, $samediData, $samediTreatmentType, $samediAppointment);
        // store the results
        $cancelableUntilDateTime = null;
        if ($samediAdapterBooking !== false) {
            if (!($samediAdapterBooking === true)) {
                $sbooking = new SBooking($samediAdapterBooking);
                $cancelableUntilDateTimeObj = date_create_from_format('Y-m-d?H:i:sP', $sbooking->getCancelableUntil());
                if ($cancelableUntilDateTimeObj !== false) {
                    $cancelableUntilDateTime = date_format($cancelableUntilDateTimeObj, 'Y-m-d H:i:s');
                }
            }
            $result_json = $samediAdapterBooking;
            $status_integration_booking = \Arzttermine\Integration\Booking::STATUS_OK;
            $status_booking_integration = BOOKING_INTEGRATION_STATUS_BOOKED;
            $status_booking = PatientBooking::STATUS_RECEIVED;
        } else {
            $result_json = $samediAdapterBooking[1];
            $status_integration_booking = \Arzttermine\Integration\Booking::STATUS_ERROR;
            $status_booking_integration = BOOKING_INTEGRATION_STATUS_ERROR;
            $status_booking = PatientBooking::STATUS_INVALID;
        }
        // now store 3!!! status
        $samediBooking
            ->setCancelableUntil($cancelableUntilDateTime)
            ->setResultJson(json_encode($result_json))
            ->setStatus($status_integration_booking)
            ->save(array('status','result_json','cancelable_until'));

        // store the integration status in the bookings table (this is redundant data but very helpful)
        $booking->saveIntegrationStatus($status_booking_integration);

        // todo: remove the booked appointment from the integration_5_appointments table
        // and reload all appointments as because of the booking other appointments could have been changed

        return $status_booking;
    }

    /**
     * 1. Add a new patient via the samedi API
     * 2. Add a booking for that patient
     *
     * Returns samedi booking object or false if booking failed
     *
     * @param PatientBooking $booking
     * @param Data $samediData
     * @param TreatmentType $samediTreatmentType
     * @param Appointment $samediAppointment
     *
     * @return Object|bool
     */
    private function bookSamediAdapterAppointment(PatientBooking $booking, Data $samediData, TreatmentType $samediTreatmentType, Appointment $samediAppointment)
    {
        if (Configuration::get('samedi_real_booking_enable') != 1) {
            return true;
        }

        $samediAdapter = SamediAdapter::getInstance();
        if ($samediAdapter->login() === false) {
            $this->setLastErrorMessage('Es ist ein interner Fehler aufgetreten. Bitte kontaktieren Sie den Support.');
            return false;
        }

        // 1. Add the patient via the samedi api
        $userData = new SamediPatient($booking);
        $patient = $samediAdapter->addPatient($userData);
        if ($patient === false) {
            $this->setLastErrorMessage('Es ist ein interner Fehler aufgetreten. Bitte kontaktieren Sie den Support.');
            return false;
        }

        // 2. get the STime object which is needed to book via samedi
        $samediMetaData = json_decode($samediAppointment->getSamediMetadata());
        $sTime = new STime($samediMetaData);

        $samediPatient = new SPatient($patient);
        $samediBooking = $samediAdapter->book(
            $samediData->getPracticeId(),
            $samediTreatmentType->getId(),
            $samediData->getCategoryId(),
            $sTime,
            $samediPatient
        );
        if (!$samediBooking) {
            $this->setLastErrorMessage($samediAdapter->getLastError());
            return false;
        }
        return $samediBooking;
    }

    /**
     * Map a TreatmentType to a samedi EventType.
     * We have to find the correct EventType via the samedi appointment where all
     * possible EventTypeIds are stored
     *
     * @param ArzttermineTreatmentType $arzttermineTreatmentType
     * @param Appointment $samediAppointment
     *
     * @return TreatmentType
     */
    public function mapArzttermineTreatmentTypeToSamediTreatmentType(ArzttermineTreatmentType $arzttermineTreatmentType, Appointment $samediAppointment)
    {
        $samediTreatmentType = new TreatmentType();

        $possibleTreatmentTypes = $samediAppointment->getSamediTreatmenttypeIds();
        if (empty($possibleTreatmentTypes)) {
            return $samediTreatmentType;
        }
        $idString = implode(' OR id=', $possibleTreatmentTypes);

        return $samediTreatmentType->findObject('(id='.$idString.') AND parent_id='.$arzttermineTreatmentType->getId());
    }

    /**
     * Cancel a booking
     *
     * @param PatientBooking $booking
     *
     * @return bool
     */
    public function cancelBooking(PatientBooking $booking)
    {
        return DoctorsManager::getInstance()->cancelBookedAppointment($booking->getId());
    }

    /**
     * Returns all integration booking objects in an array for the given booking
     *
     * @param PatientBooking $booking
     *
     * @return array
     */
    public function getIntegrationBookings(PatientBooking $booking)
    {
        $integration_booking = new Booking();

        return $integration_booking->findObjects('booking_id=' . $booking->getId() . ' ORDER BY created_at DESC;');
    }

    /**
     * @inheritdoc
     */
    public function hasAppointmentEnquiry()
    {
        return false;
    }

    /**
     * Return all Samedi treatmenttypes for a samedi appointment
     *
     * @param ArzttermineAppointment $arzttermineAppointment
     * @return Treatmenttype[]
     */
    public function getSamediTreatmentTypesForAppointment(ArzttermineAppointment $arzttermineAppointment)
    {
        $treatmenttypes = array();
        $samediAppointment = new Appointment();
        $samediAppointments = $samediAppointment->findObjects(
            'location_id='.$arzttermineAppointment->getLocationId().
            ' AND user_id='.$arzttermineAppointment->getUserId().
            ' AND start_at="'.$arzttermineAppointment->getStartTime()->getDateTime().'"'
        );
        if (is_array($samediAppointments) && !empty($samediAppointments)) {
            /**
             * Catch all samedi treatment types
             * @var Appointment $samediAppointment
             */
            foreach ($samediAppointments as $samediAppointment) {
                $treatmenttypes = $treatmenttypes + $samediAppointment->getSamediTreatmenttypes();
            }
        }
        return $treatmenttypes;
    }

    /**
     * Return all Arzttermine treatmenttypes for a samedi appointment
     *
     * @param ArzttermineAppointment $arzttermineAppointment
     * @return ArzttermineTreatmenttype[]
     */
    public function getTreatmentTypesForAppointment(ArzttermineAppointment $arzttermineAppointment)
    {
        $treatmenttypes = array();
        $samediAppointment = new Appointment();
        $samediAppointments = $samediAppointment->findObjects(
            'location_id='.$arzttermineAppointment->getLocationId().
            ' AND user_id='.$arzttermineAppointment->getUserId().
            ' AND start_at="'.$arzttermineAppointment->getStartTime()->getDateTime().'"'
        );
        if (is_array($samediAppointments) && !empty($samediAppointments)) {
            /**
             * Catch all samedi treatment types
             * @var Appointment $samediAppointment
             */
            foreach ($samediAppointments as $samediAppointment) {
                $treatmenttypes = $treatmenttypes + $samediAppointment->getArzttermineTreatmenttypes();
            }
        }
        return $treatmenttypes;
    }

    /**
     * 1. Check if the treatmenttype exists (for samedi we always need a treatmenttype)
     * 2. Check if the treatmenttype has an linked samedi treatmenttype
     * 3. Check if the appointment's insurance fits the linked samedi treatmenttype's insurance
     *
     * @inheritdoc
     */
    public function appointmentIsValid(ArzttermineAppointment $arzttermineAppointment)
    {
        $result = false;

        $insurance_ids = $arzttermineAppointment->getInsuranceIds();
        if (empty($insurance_ids)) {
            $this->setLastErrorMessage('Es muss eine Versicherungsart ausgewählt werden!');
            return false;
        }

        $arzttermineTreatmentTypes = $arzttermineAppointment->getTreatmentTypes();
        if (empty($arzttermineTreatmentTypes)) {
            $this->setLastErrorMessage('Es muss eine Behandlungsart ausgewählt werden!');
            return false;
        }

        /**
         * Check if the arzttermine treatmenttype fits with the samedi treatmenttype
         */
        $samediTreatmentTypes = $this->getSamediTreatmentTypesForAppointment($arzttermineAppointment);
        if (empty($samediTreatmentTypes)) {
            $this->setLastErrorMessage('Der Termin konnte nicht gefunden werden!');
            return false;
        }

        foreach ($arzttermineTreatmentTypes as $arzttermineTreatmentType) {
            $arzttermineTreatmentTypeId = $arzttermineTreatmentType->getId();

            foreach ($samediTreatmentTypes as $samediTreatmentType) {
                $parentArzttermineTreatmentType = $samediTreatmentType->getParentTreatmentType();
                if ($arzttermineTreatmentTypeId === $parentArzttermineTreatmentType->getId()) {

                    // the treatmenttype fits
                    // now check the insurance
                    $samediInsurancesIds = $samediTreatmentType->getInsuranceIds();
                    foreach ($samediInsurancesIds as $samediInsuranceId) {
                        if (in_array($samediInsuranceId, $arzttermineAppointment->getInsuranceIds())) {
                            $result = true;
                        }
                    }
                }
            }
        }

        if ($result == false) {
            $this->setLastErrorMessage('Ausgewählte Art der Behandlung steht nicht für die gewählte Versicherung zur Verfügung!');
        }
        return $result;
    }
}