<?php

namespace Arzttermine\Integration\Integration\Samedi;

use Arzttermine\Core\Basic;

class Network extends Basic {

    /**
     * @var string
     */
    protected $tablename = DB_TABLENAME_INTEGRATION_5_NETWORKS;

    /**
     * @var string json
     */
    protected $data = '';

    /**
     * @var array
     */
    protected $all_keys
        = array(
            "id",
            "data"
        );

    /**
     * @var array
     */
    protected $secure_keys
        = array(
            "id",
            "data"
        );

    /**
     * @return string json
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param string $json
     * @return self
     */
    public function setData($json)
    {
        $this->data = $json;
        return $this;
    }
}