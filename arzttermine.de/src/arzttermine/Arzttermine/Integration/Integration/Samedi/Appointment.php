<?php

namespace Arzttermine\Integration\Integration\Samedi;

use Arzttermine\Core\Basic;
use Arzttermine\MedicalSpecialty\TreatmentType as ArzttermineTreatmenttype;

class Appointment extends Basic {

    /**
     * @var string
     */
    protected $tablename = DB_TABLENAME_INTEGRATION_5_APPOINTMENTS;

    /**
     * @var int
     */
    protected $user_id;

    /**
     * @var int
     */
    protected $location_id;

    /**
     * @var string
     */
    protected $start_at;

    /**
     * @var string json
     */
    protected $samedi_metadata = '';

    /**
     * @var string
     */
    protected $samedi_treatmenttype_ids = '';

    /**
     * @var string
     */
    protected $created_at;

    /**
     * @var array
     */
    protected $all_keys
        = array(
            "id",
            "user_id",
            "location_id",
            "start_at",
            "samedi_metadata",
            "samedi_treatmenttype_ids",
            "created_at"
        );

    /**
     * @var array
     */
    protected $secure_keys
        = array(
            "samedi_metadata",
            "samedi_treatmenttype_ids",
        );

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     * @return self
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getLocationId()
    {
        return $this->location_id;
    }

    /**
     * @param int $location_id
     * @return self
     */
    public function setLocationId($location_id)
    {
        $this->location_id = $location_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getStartAt()
    {
        return $this->start_at;
    }

    /**
     * @param string $start_at
     * @return self
     */
    public function setStartAt($start_at)
    {
        $this->start_at = $start_at;
        return $this;
    }

    /**
     * @return string
     */
    public function getSamediMetadata()
    {
        return $this->samedi_metadata;
    }

    /**
     * @param string $samedi_metadata
     * @return self
     */
    public function setSamediMetadata($samedi_metadata)
    {
        $this->samedi_metadata = $samedi_metadata;
        return $this;
    }

    /**
     * @return array
     */
    public function getSamediTreatmenttypeIds()
    {
        return explode(',', $this->samedi_treatmenttype_ids);
    }

    /**
     * Return SamediTreatmenttype Objects
     * @return Treatmenttype[]
     */
    public function getSamediTreatmenttypes()
    {
        $samediTreatmenttypes = array();
        $samediTreatmenttypeIds = $this->getSamediTreatmenttypeIds();
        if (!empty($samediTreatmenttypeIds)) {
            /**
             * @var Treatmenttype $samediTreatmenttype
             */
            foreach ($samediTreatmenttypeIds as $samediTreatmenttypeId) {
                $samediTreatmenttypes[] = new TreatmentType($samediTreatmenttypeId);
            }
        }
        return $samediTreatmenttypes;
    }

    /**
     * Return converted Arzttermine Treatmenttype Objects.
     * The returned array is indexed by the id so there are no redundant tts
     * @return ArzttermineTreatmenttype[]
     */
    public function getArzttermineTreatmenttypes()
    {
        $arzttermineTreatmenttypes = array();
        $samediTreatmenttypes = $this->getSamediTreatmenttypes();
        if (!empty($samediTreatmenttypes)) {
            /**
             * @var Treatmenttype $samediTreatmenttype
             */
            foreach ($samediTreatmenttypes as $samediTreatmenttype) {
                $arzttermineTreatmenttype = $samediTreatmenttype->getParentTreatmentType();
                if ($arzttermineTreatmenttype->isValid()) {
                    $arzttermineTreatmenttypes[$arzttermineTreatmenttype->getId()] = $arzttermineTreatmenttype;
                }
            }
        }
        return $arzttermineTreatmenttypes;
    }

    /**
     * @param string $samedi_treatmenttype_ids
     * @return self
     */
    public function setSamediTreatmenttypeIds($samedi_treatmenttype_ids)
    {
        $this->samedi_treatmenttype_ids = $samedi_treatmenttype_ids;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param string $created_at
     * @return self
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
        return $this;
    }

}