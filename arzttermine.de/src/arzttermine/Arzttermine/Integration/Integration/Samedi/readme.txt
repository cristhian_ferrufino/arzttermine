From: Michael Siebert <siebert@samedi.de>
Date: Thu, Oct 25, 2012 at 1:48 PM
Subject: Fwd: Arzttermine.de
To: Thomas Hillard <tomhillard@gmail.com>

arzttermine-allgemein: this is "your" account which you can use to
fetch available appointments, book these appointments etc.
arzttermin-hno and arzttermin-frauenheilkunde are test accounts which
you can book appointments in. these are your customer's accounts.
Under "Einstellungen/Netzwerk" they have configured some appointment
types which you can then see and book.

I hope it's no problem that everything is in german, but you'll have
to learn german either way ;) The PDFs also include a short usage
guide. If you need a more  extensive manual, you can get one from our
wiki: https://wiki.samedi.de/display/doc/Handbuch

Now to the API calls:

First, some terminology:
* Our customers are "practices"
* every practice has a unique practice id. yours is "ryhv054348v1c2s7"
* most of a practices' HTTP resources are grouped under /practices/<practice id>
* we use JSON
* when you access a different practice, you only see what they
configured for your practice
* for historical reasons, appointments are named events in our app
* there are different types of appointments you can book. these are
called event_types
* event types are grouped under categories (also called event categories)


I think you'll figure out how you can log in to the system by looking
at our login form at /login, so I'll skip that.

To fetch a list of practices to which you can refer appointments, use the URL
https://app.samedi.de/practices/ryhv054348v1c2s7/referrers.json

Then fetch a list of categories available for one of those practices:
https://app.samedi.de/practices/<practice id>/calendar/categories.json

Fetch a list of event types you can book:
https://app.samedi.de/practices/<practice id>/calendar/event_types.json
Although you *could* fetch the event types without a category, always
make the user select a category (member "event_categories") before calculating availabilities!

Now, fetch available dates:
https://app.samedi.de/practices/<practice id>/calendar/event_types/<event type id>/availability/<year>/<month>.json?event_category_id=<event category id>
and times for a specific day:
https://app.samedi.de/practices/<practice id>/calendar/event_types/<event type id>/times/2012/10/25.json?event_category_id=16065


with that, you could essentially book an appointment:

POST to https://app.samedi.de/practices/<practice_id>/calendar/events.json
event[event_category_id]=14
event[event_type_id]=9
event[starts_at]=2011-06-01T12:00+02:00
event[token]=f5f872101270489bd4dada515dc31fad4e36b8f4
event[attendant][xref]=<some patient id you can assign, e.g. the arzttermine.de user id>
event[attendant][data]=<json>


attendant data is a json string with some of these members:

    "title", "gender",
    { name: "first_name", required: true }, { required: true, name: "last_name" },
    "phone", "mobile", "email", "homepage", "street", "zip",
    "city", "born_on", "fax", "country",
    "insurance_company", "insurance_number"

and observe the response.



From: Michael Siebert <siebert@samedi.de>
Date: Wed, Nov 7, 2012 at 6:35 PM
Subject: Re: Arzttermine.de
To: thomas hillard <tomhillard@gmail.com>


Hey Thomas,
thanks for your feedback, I'll try to answer your questions the best way I can

Question 1) For Bookings:
POST to /practices/<practice_id>/calendar/events.json
Is there a call where we can get the valid values for "insurance_company"
and/or "insurance_number"? Are these optional? or mandatory?

Answer 1) GET /insurances.json and use the ids for insurance_company.
insurance number is the contract number of the patient. This field is
not mandatory, you'll likely want to ignore it.

Question 2) For Insurance:
We only ask users if they are private, insurance right now. If you have
insurance we'll need to use them, otherwise we'll have to do data mapping on
our side.

Answer 2) In addition to the ids from /insurances.json, you can specify
"public" or "private". If you don't want to ask for the specific
insurance company, this would be the way to go.

Question 3) For Appointments:
Right now we organize our appointments by Medical Specialty Category. It
appears from the Samedi API we would need to get the data by the "event"
type (what we are understanding as the type of service by a provide of a
specialty). Is there a way that we can get the data by the "Event" category?
Otherwise, we'll need to do a lot of mapping on our end to know which
"event" types relate to which "event" category.

In other words, does it make sense to fetch..
+ all available events (appointments)
+ for each practice
+ with all event_types (or better: event_categories)?

Answer 3) An EventType is the type of service, you're right. Maybe you'll
want to look at the "field" member of a practice (GET
/practices/<id>.json) and group all event types for that practice
under these fields? If that doesn't work, we could maybe introduce
some meta data for that. These could then be configured by the user in
samedi? (we should talk about that at a later stage?)

Question 4) Caching: Last, just to be sure, will we be allowed to cache the
appointment data? Otherwise we're looking at a lot of real time calls, which
might not be so efficient.

Answer 4) You can cache the available times a bit, but before you actually
book the event, you'll have to fetch it because the token is only valid a few minutes.
