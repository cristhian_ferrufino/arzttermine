<?php

namespace Arzttermine\Integration\Integration\Samedi;

use Arzttermine\Integration\Booking as IntegrationBookingCore;

class Booking extends IntegrationBookingCore {

    /**
     * @var string
     */
    protected $tablename = DB_TABLENAME_INTEGRATION_5_BOOKINGS;

    /**
     * @var int
     */
    protected $integration_id = 5;

    /**
     * @var int
     */
    protected $status;

    /**
     * @var string
     */
    protected $practice_id = '';

    /**
     * @var int
     */
    protected $category_id = '';

    /**
     * @var int
     */
    protected $event_type_id = '';

    /**
     * @var string
     */
    protected $result_json = '';

    /**
     * @var string
     */
    protected $cancelable_until = '';

    /**
     * @var string
     */
    protected $created_at;

    /**
     * @var string
     */
    protected $created_by;

    /**
     * @var string
     */
    protected $updated_at;

    /**
     * @var string
     */
    protected $updated_by;

    /**
     * @var array
     */
    protected $all_keys
        = array(
            "id",
            "status",
            "booking_id",
            'practice_id',
            'category_id',
            'event_type_id',
            'result_json',
            'cancelable_until',
            "created_at",
            "created_by",
            "updated_at",
            "updated_by"
        );

    /**
     * @var array
     */
    protected $secure_keys
        = array(
            "status",
            'practice_id',
            'category_id',
            'event_type_id',
            'result_json',
            'cancelable_until',
            "created_at",
            "created_by",
            "updated_at",
            "updated_by"
        );

    /**
     * @param int $status
     *
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getPracticeId()
    {
        return $this->practice_id;
    }

    /**
     * @param string $practice_id
     * @return self
     */
    public function setPracticeId($practice_id)
    {
        $this->practice_id = $practice_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getCategoryId()
    {
        return $this->category_id;
    }

    /**
     * @param int $category_id
     * @return self
     */
    public function setCategoryId($category_id)
    {
        $this->category_id = $category_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getEventTypeId()
    {
        return $this->event_type_id;
    }

    /**
     * @param int $event_type_id
     * @return self
     */
    public function setEventTypeId($event_type_id)
    {
        $this->event_type_id = $event_type_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getResultJson()
    {
        return $this->result_json;
    }

    /**
     * @param string $result_json
     * @return self
     */
    public function setResultJson($result_json)
    {
        $this->result_json = $result_json;
        return $this;
    }

    /**
     * @return string
     */
    public function getCancelableUntil()
    {
        return $this->cancelable_until;
    }

    /**
     * @param string $cancelable_until
     * @return self
     */
    public function setCancelableUntil($cancelable_until)
    {
        $this->cancelable_until = $cancelable_until;
        return $this;
    }

    /**
     * @param int $booking_id
     *
     * @return self
     */
    public function setBookingId($booking_id)
    {
        $this->booking_id = $booking_id;

        return $this;
    }

    /**
     * @return int
     */
    public function getBookingId()
    {
        return $this->booking_id;
    }

    /**
     * @param string $created_at
     *
     * @return self
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param string $created_by
     *
     * @return self
     */
    public function setCreatedBy($created_by)
    {
        $this->created_by = $created_by;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Returns status information in Html
     *
     * @return string
     */
    public function getStatusHtml()
    {
        if ($this->getStatus() == Booking::STATUS_OK) {
            $html = '<span class="green">status=OK</span><br />' . "\n" .
                    'created_at=' . $this->getCreatedAt() . "<br />\n";
        } else {
            $html = '<span class="red">status=ERROR</span><br />' . "\n" .
                    'created_at=' . $this->getCreatedAt() . "<br />\n";
        }

        return $html;
    }
}
