<?php

namespace Arzttermine\Integration\Integration\Samedi\Types;

/**
 * Using in book and get getBookedAppointment functions result.
 */
class SBooking {

    public $bookingCoreObj;

    public function __construct($sbookingObj)
    {
        assert(isset($sbookingObj));
        if (isset($sbookingObj->data)) {
            $this->bookingCoreObj = $sbookingObj->data;
        } else {
            $this->bookingCoreObj = $sbookingObj;
        }
    }

    public function getData()
    {
        return $this->bookingCoreObj;
    }

    public function getPracticeName()
    {
        return $this->bookingCoreObj->event->practice_name;
    }

    public function getEventTypeXref()
    {
        return $this->bookingCoreObj->event->event_type_xref;
    }

    public function getCreatorType()
    {
        return $this->bookingCoreObj->event->creator_type;
    }

    public function getUpdaterName()
    {
        return $this->bookingCoreObj->event->updater_name;
    }

    public function getCreatorId()
    {
        return $this->bookingCoreObj->event->creator_id;
    }

    public function getXref()
    {
        return $this->bookingCoreObj->event->xref;
    }

    public function getDuration()
    {
        return $this->bookingCoreObj->event->duration;
    }

    public function getAttendantXref()
    {
        return $this->bookingCoreObj->event->attendant_xref;
    }

    public function getEventTypeId()
    {
        return $this->bookingCoreObj->event->event_type_id;
    }

    public function setEventTypeId($value)
    {
        $this->bookingCoreObj->event->event_type_id = $value;
    }

    public function getEndsAt()
    {
        return $this->bookingCoreObj->event->ends_at;
    }

    public function getKind()
    {
        return $this->bookingCoreObj->event->kind;
    }

    public function getUpdaterType()
    {
        return $this->bookingCoreObj->event->updater_type;
    }

    public function getEditable()
    {
        return $this->bookingCoreObj->event->editable;
    }

    public function getPracticeId()
    {
        return $this->bookingCoreObj->event->practice_id;
    }

    public function setPracticeId($value)
    {
        $this->bookingCoreObj->event->practice_id = $value;
    }

    public function getCreatorPracticeId()
    {
        return $this->bookingCoreObj->event->creator_practice_id;
    }

    public function getUpdatedAt()
    {
        return $this->bookingCoreObj->event->updated_at;
    }

    public function getUpdaterId()
    {
        return $this->bookingCoreObj->event->updater_id;
    }

    public function getAttendantId()
    {
        return $this->bookingCoreObj->event->attendant_id;
    }

    public function getStartsAt()
    {
        return $this->bookingCoreObj->event->starts_at;
    }

    public function setStartsAt($stringDateTime)
    {
        $this->bookingCoreObj->event->starts_at = $stringDateTime;
    }

    public function getCreatedAt()
    {
        return $this->bookingCoreObj->event->created_at;
    }

    public function getCancelableUntil()
    {
        return $this->bookingCoreObj->event->cancelable_until;
    }

    public function setCancelableUntil($cancelableUntil)
    {
        $this->bookingCoreObj->event->cancelable_until = $cancelableUntil;
    }

    public function getNameExternal()
    {
        return $this->bookingCoreObj->event->name_external;
    }

    public function getExternal()
    {
        return $this->bookingCoreObj->event->external;
    }

    public function getDeletedAt()
    {
        return $this->bookingCoreObj->event->deleted_at;
    }

    public function getAttendantState()
    {
        return $this->bookingCoreObj->event->attendant_state;
    }

    public function getName()
    {
        return $this->bookingCoreObj->event->name;
    }

    public function getEventCategoryId()
    {
        return $this->bookingCoreObj->event->event_category_id;
    }

    public function setEventCategoryId($value)
    {
        $this->bookingCoreObj->event->event_category_id = $value;
    }

    public function getBaseUpdatedAt()
    {
        return $this->bookingCoreObj->event->base_updated_at;
    }

    public function getColor()
    {
        return $this->bookingCoreObj->event->color;
    }

    public function getReferral()
    {
        return $this->bookingCoreObj->event->referral;
    }

    public function getId()
    {
        return $this->bookingCoreObj->event->id;
    }

    public function getCreatorName()
    {
        return $this->bookingCoreObj->event->creator_name;
    }

    public function getTodos()
    {
        return $this->bookingCoreObj->event->todos;
    }

    public function getStructuredComment()
    {
        return $this->bookingCoreObj->event->structured_comment;
    }

    public function getIdentityId()
    {
        return $this->bookingCoreObj->event->identity->id;
    }

    public function getIdentity()
    {
        return $this->bookingCoreObj->event->identity;
    }

    public function getAttendant()
    {
        return $this->bookingCoreObj->attendant;
    }

    public function getBlocks()
    {
        $blocks = $this->bookingCoreObj->event->blocks;
        $ret = array();
        foreach ($blocks as $block) {
            $ret[] = new SBookBlock($block);
        }
    }

    public function getResources()
    {
        return $this->bookingCoreObj->event->resources;
    }
}
