<?php

namespace Arzttermine\Integration\Integration\Samedi\Types;

/**
 * NOTE: This crappy class has error supression on each return statement as we're going to remove it soon
 */
class SBookingShort {

    public $bookingCoreObj;

    public function __construct($bookingObj)
    {
        $this->bookingCoreObj = new \stdClass();

        if (!is_null($bookingObj)) {
            $this->bookingCoreObj = $bookingObj;
        }
    }

    public function getData()
    {
        return $this->bookingCoreObj;
    }

    public function getPracticeName()
    {
        return @$this->bookingCoreObj->practice_name;
    }

    public function getEventTypeXref()
    {
        return @$this->bookingCoreObj->event_type_xref;
    }

    public function getCreatorType()
    {
        return @$this->bookingCoreObj->creator_type;
    }

    public function getUpdaterName()
    {
        return @$this->bookingCoreObj->updater_name;
    }

    public function getCreatorId()
    {
        return @$this->bookingCoreObj->creator_id;
    }

    public function getXref()
    {
        return @$this->bookingCoreObj->xref;
    }

    public function getDuration()
    {
        return @$this->bookingCoreObj->duration;
    }

    public function getAttendantXref()
    {
        return @$this->bookingCoreObj->attendant_xref;
    }

    public function getEventTypeId()
    {
        return @$this->bookingCoreObj->event_type_id;
    }

    public function getEndsAt()
    {
        return @$this->bookingCoreObj->ends_at;
    }

    public function getKind()
    {
        return @$this->bookingCoreObj->kind;
    }

    public function getUpdaterType()
    {
        return @$this->bookingCoreObj->updater_type;
    }

    public function getEditable()
    {
        return @$this->bookingCoreObj->editable;
    }

    public function getPracticeId()
    {
        return @$this->bookingCoreObj->practice_id;
    }

    public function getUpdatedAt()
    {
        return @$this->bookingCoreObj->updated_at;
    }

    public function getUpdaterId()
    {
        return @$this->bookingCoreObj->updater_id;
    }

    public function getAttendantId()
    {
        return @$this->bookingCoreObj->attendant_id;
    }

    public function getStartsAt()
    {
        return @$this->bookingCoreObj->starts_at;
    }

    public function getCancelableUntil()
    {
        return @$this->bookingCoreObj->cancelable_until;
    }

    public function getNameExternal()
    {
        return @$this->bookingCoreObj->name_external;
    }

    public function getExternal()
    {
        return @$this->bookingCoreObj->external;
    }

    public function getAttendantState()
    {
        return @$this->bookingCoreObj->attendant_state;
    }

    public function getName()
    {
        return @$this->bookingCoreObj->name;
    }

    public function getEventCategoryId()
    {
        return @$this->bookingCoreObj->event_category_id;
    }

    public function getBaseUpdatedAt()
    {
        return @$this->bookingCoreObj->base_updated_at;
    }

    public function getColor()
    {
        return @$this->bookingCoreObj->color;
    }

    public function getReferral()
    {
        return @$this->bookingCoreObj->referral;
    }

    public function getId()
    {
        return @$this->bookingCoreObj->id;
    }

    public function getCreatorName()
    {
        return @$this->bookingCoreObj->creator_name;
    }

    public function getStructuredComment()
    {
        return @$this->bookingCoreObj->structured_comment;
    }

    public function getAttendant()
    {
        return @$this->bookingCoreObj->data->attendant;
    }
}