<?php

namespace Arzttermine\Integration\Integration\Samedi\Types;

class SBookBlock {

    private $sbookBlockObj;

    public function __construct($sbookBlockObj)
    {
        assert(isset($sbookBlockObj));
        $this->sbookBlockObj = $sbookBlockObj;
    }

    public function getEndsAt()
    {
        return $this->sbookBlockObj->ends_at;
    }

    public function getResourceXref()
    {
        return $this->sbookBlockObj->resource_xref;
    }

    public function getStartsAt()
    {
        return $this->sbookBlockObj->starts_at;
    }

    /**
     * result example , "room" or "person" , ...
     */
    public function getResourceType()
    {
        return $this->sbookBlockObj->resource_type;
    }

    public function getResourceId()
    {
        return $this->sbookBlockObj->resource_id;
    }

    public function getId()
    {
        return $this->sbookBlockObj->id;
    }
}