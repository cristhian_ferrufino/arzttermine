<?php

namespace Arzttermine\Integration\Integration\Samedi\Types;

class SDay {

	public $sday;

	public function __construct($sday) {
		assert($sday != null);
		$this->sday = $sday;
	}

	public function getData() {
		return $this->sday;
	}

	public function getDate() {
		return $this->sday->date;
	}

	public function isAvailable() {
		return $this->sday->available;
	}
	
}