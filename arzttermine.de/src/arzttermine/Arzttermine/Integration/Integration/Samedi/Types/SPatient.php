<?php

namespace Arzttermine\Integration\Integration\Samedi\Types;

class SPatient {

	public $samediPatientObj;
	// patient's private data
	private $privateData;

	public function __construct($samediPatientObj) {
		assert($samediPatientObj != null);
		$this->samediPatientObj = $samediPatientObj;
		$pdata = $this->samediPatientObj->data;
		$stripData = substr($pdata , strpos($pdata , '{'));
		$this->privateData = json_decode($stripData);
	}
	
	public function getData() {
		return $this->samediPatientObj;
	}
	
	public function getPatientPrivateData() {
		return $this->privateData;
	}
	

	public function isBlocked() {
		return $this->samediPatientObj->blocked;
	}
	
	public function getId() {
		return $this->samediPatientObj->id;
	}

	public function isUser() {
		return $this->samediPatientObj->is_user;
	}

	public function getUpdatedAt() {
		return $this->samediPatientObj->updated_at;
	}

	public function getXref() {
		return $this->samediPatientObj->xref;
	}

	public function getZdaActivationDate() {
		return $this->samediPatientObj->zda_activation_date;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////// patient private data /////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public function getGender() {
		return $this->privateData->gender;

	}

	public function getTitle() {
		return $this->privateData->title;

	}

	public function getCompany() {
		return $this->privateData->company;

	}

	public function getCompanyType() {
		return $this->privateData->company_type;

	}

	public function getLastName() {			
		return $this->privateData->last_name;

	}

	public function getFirstName() {
		return $this->privateData->first_name;

	}

	public function getBornOn() {
		return $this->privateData->born_on;

	}

	public function getInsuranceCompany() {
		return $this->privateData->insurance_company;

	}

	public function getInsuranceNumber() {
		return $this->privateData->insurance_number;

	}

	public function getAskedForPhone() {
		return $this->privateData->asked_for_phone;

	}

	public function getAskedForMobile() {
		return $this->privateData->asked_for_mobile;

	}

	public function getAskedForEmail() {
		return $this->privateData->asked_for_email;

	}

	public function getPhone() {
		return $this->privateData->phone;

	}

	public function getFax() {
		return $this->privateData->fax;

	}

	public function getMobile() {
		return $this->privateData->mobile;

	}

	public function getEmail() {
		return $this->privateData->email;

	}

	public function getHomepage() {
		return $this->privateData->homepage;

	}

	public function getStreet() {
		return $this->privateData->street;

	}

	public function getZip() {
		return $this->privateData->zip;

	}

	public function getCity() {
		return $this->privateData->city;

	}

	public function getCountryCode() {
		return $this->privateData->country;

	}

	public function getComment() {
		return $this->privateData->comment;

	}

}