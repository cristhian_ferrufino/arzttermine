<?php

namespace Arzttermine\Integration\Integration\Samedi\Types;

class SNetworkContact {

	public $networkcontact;
	public $healer;

	public function __construct($networkcontact) {
		$this->networkcontact = $networkcontact;
		$this->healer = $networkcontact->healer_data;

	}

	public function getHealerId() {
		return $this->networkcontact->healer_id;
	}

	public function getTagList() {
		return $this->networkcontact->tag_list;
	}

	public function getCanRefer() {
		return $this->networkcontact->can_refer;
	}

	public function getUserData() {
		return $this->networkcontact->user_data;
	}

	public function getComment() {
		return $this->networkcontact->comment;
	}

	public function getId() {
		return $this->networkcontact->id;
	}

	public function getHealerCity() {
		return $this->healer->city;
	}

	public function getHealerBornOn() {
		return $this->healer->born_on;
	}

	public function getHealerUserId() {
		return $this->healer->user_id;
	}

	public function getHealerLongitude() {
		return $this->healer->longitude;
	}

	public function getHealerEmail() {
		return $this->healer->email;
	}

	public function getHealerPhone() {
		return $this->healer->phone;
	}

	public function getHealerFieldId() {
		return $this->healer->field_id;
	}

	public function getHealerPracticeId() {
		return $this->healer->practice_id;
	}

	public function getHealerPracticeName() {
		return $this->healer->practice_name;
	}

	public function getHealerCountry() {
		return $this->healer->country;
	}

	public function getHealerZip() {
		return $this->healer->zip;
	}

	public function getHealerFax() {
		return $this->healer->fax;
	}

	public function getHealerLastName() {
		return $this->healer->last_name;
	}

	public function getHealerServicePackageName() {
		return $this->healer->service_package_name;
	}

	public function getHealerStreet() {
		return $this->healer->street;
	}

	public function getHealerInstitutionType() {
		return $this->healer->institution_type;
	}

	public function getHealerHomepage() {
		return $this->healer->homepage;
	}

	public function getHealerMobile() {
		return $this->healer->mobile;
	}

	public function getHealerFirstName() {
		return $this->healer->first_name;
	}

	public function getHealerGender() {
		return $this->healer->gender;
	}

	public function getHealerTitle() {
		return $this->healer->title;
	}

	public function getHealerId2() {
		return $this->healer->id;
	}

	public function getHealerIsUser() {
		return $this->healer->is_user;
	}

	public function getHealerLatitude() {
		return $this->healer->latitude;
	}

}