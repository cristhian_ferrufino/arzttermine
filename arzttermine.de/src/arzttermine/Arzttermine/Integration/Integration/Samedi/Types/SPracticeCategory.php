<?php

namespace Arzttermine\Integration\Integration\Samedi\Types;

class SPracticeCategory {

	public $samediCategoryObj;

	public function __construct($samediCategoryObj) {
		assert($samediCategoryObj != null);
		$this->samediCategoryObj = $samediCategoryObj;
	}

	public function getData() {
		return $this->samediCategoryObj;
	}

	public function getId() {
		return $this->samediCategoryObj->id;
	}

	public function getName() {
		return $this->samediCategoryObj->name;
	}

	public function getIsPublic() {
		return $this->samediCategoryObj->isPublic;
	}

	public function getPositionExternal() {
		return $this->samediCategoryObj->positionExternal;
	}

	public function getHasHealer() {
		return $this->samediCategoryObj->hasHealer;
	}

	public function getSubtitle() {
		return $this->samediCategoryObj->subtitle;
	}

}