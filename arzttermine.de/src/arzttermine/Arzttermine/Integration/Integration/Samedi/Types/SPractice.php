<?php

namespace Arzttermine\Integration\Integration\Samedi\Types;

class SPractice {

	private $sReferer;

	public function __construct($sReferer) {
		assert($sReferer != null);
		$this->sReferer = $sReferer;
	}

	public function getData() {
		return $this->sReferer;
	}

	public function getId() {
		return $this->sReferer->id;
	}

	public function getName() {
		return $this->sReferer->name;
	}

}