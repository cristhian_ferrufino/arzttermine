<?php

namespace Arzttermine\Integration\Integration\Samedi\Types;

class STime {

	public $stime;

	public function __construct($stime) {
		assert($stime != null);
		$this->stime = $stime;
	}

	public function getData() {
		return $this->stime;
	}

	public function getTime() {
		return $this->stime->time;
	}

	public function getToken() {
		return $this->stime->token;
	}

}