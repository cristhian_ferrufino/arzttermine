<?php

namespace Arzttermine\Integration\Integration\Samedi\Types;

class CategoryEventType {

	public $samediEventTypeObj;

	public function __construct($samediEventTypeObj) {
		assert($samediEventTypeObj != null);
		$this->samediEventTypeObj = $samediEventTypeObj;
	}

	public function getData()
	{
		return $this->samediEventTypeObj;
	}

	public function getId() {
		return $this->samediEventTypeObj->id;
	}

	public function getName() {
		return $this->samediEventTypeObj->name;
	}
	public function getAttendantRequired()
	{
		return $this->samediEventTypeObj->attendant_required;
	}
	
	public function getCommentForm()
	{
		if ($this->samediEventTypeObj->comment_form){
			return json_decode($this->samediEventTypeObj->comment_form); 
		}
		else{
			return null;
		}
	}
	
	public function getDuration()
	{
		return $this->samediEventTypeObj->duration;
	}
	
	public function getDurationInMinutes()
	{
		$duration = explode(':',$this->getDuration());
		$hours = intval($duration[0]);
		$minutes = intval($duration[1]);
		return 60*$hours + $minutes;
	}
	public function getInsuranceCompanyConstraints()
	{
		return $this->samediEventTypeObj->insurance_company_constraints;
	}
	public function getArzttermineFormattedInsuranceIds() {
		$insuranceCompanyConstraints = $this->getInsuranceCompanyConstraints();
		$insuranceIdsArray = array();
		if (strripos($insuranceCompanyConstraints, 'all') !== false) {
			return '1,2';
		}
		if (strripos($insuranceCompanyConstraints, 'private') !== false) {
			$insuranceIdsArray[] = 1;
		}
		if (strripos($insuranceCompanyConstraints, 'public') !== false) {
			$insuranceIdsArray[] = 2;
		}
		return implode(',', $insuranceIdsArray);
	}
    /*
    ["referrer_booking_algorithm"]=NULL
    ["default_waitlist_id"]=    NULL
    //["attendant_required"]=    bool(true)
    ["insurance_company_constraints"]=    string(3) "all"
    ["insurance_company_type_constraints"]=    string(3) "all"
    ["category"]=    string(16) "Dr.  Beike, Sven"
    //["comment_form"]= string(186) "[{"name":"Alergien","required":false,"type":"combo","config":{"values":"Impfstoffe\r\nBienen\r\nPollen"}},{"name":"Blutgruppe bestimmt?","required":true,"type":"checkbox","config":null}]"
    ["practice"]="h516t4rv29ymp2rp"
    ["ask_for_tk_data"]=bool(false)
    ["available_internally"]=bool(true)
    ["external_booking_algorithm"]=NULL
    ["maximum_bookahead_time"]=int(0)
    ["is_public"]=bool(true)
    ["todo_preset_ids"]=array(0) {}
    ["comment_set"]= object(stdClass){
      ["name"]= string(10) "Akupunktur"
      ["explicit"]=bool(false)
      ["practice_id"]=int(176304)
      ["xref"]="si_000f2b6734"
      ["id"]=int(3884)
      ["fields"]=string(186) "[{"name":"Alergien","required":false,"type":"combo","config":{"values":"Impfstoffe\r\nBienen\r\nPollen"}},{"name":"Blutgruppe bestimmt?","required":true,"type":"checkbox","config":null}]"
    }
    ["opening_hours"]=NULL
    ["attendant_user_required_external"]=bool(true)
    ["minimum_bookahead_time"]=int(-1)
    ["is_available_for_referral"]=bool(true)
    ["duration"]= "00:30"
    ["xref"]="si_f6f866e54a"
    ["internal_booking_algorithm"]=NULL
    ["minimum_cancel_time"]=NULL
    ["description"]= "Wenn Sie einen Termin zur Akupunktur wÃ¼nschen, wÃ¤hlen Sie bitte diese Terminart. <br>"
    ["name"]="Akupunktur"
    ["allowed_external_durations"]=array(1) {[0]=>"00:30"}
    ["id"]="55422"
    ["event_categories"]=object(stdClass){["16069"]="Dr.  Beike, Sven"}
    ["internal_event_booking_hint"]=NULL
*/
}