<?php

namespace Arzttermine\Integration\Integration\Samedi\Types;

use Arzttermine\Booking\Booking;

class SamediPatient {
	public $first_name;
	public $last_name;
	public $mobile;
	public $email;
	public $gender;
	public $insurance_company;

	public function __construct(Booking $booking) {
		$this->first_name = $booking->getFirstName();
		$this->last_name = $booking->getLastName();
		$this->mobile = $booking->getPhone();
		$this->email = $booking->getEmail();
		$gender = $booking->getGender();
		if ($gender == 0) {
			$this->gender = 'male';
		} else {
			$this->gender = 'female';
		}
		$insurance_id = $booking->getInsuranceId();
		if ($insurance_id == INSURANCE_TYPE_PRIVATE) {
			$this->insurance_company = 'private';
		} else {
			$this->insurance_company = 'public';
		}		
	}
}