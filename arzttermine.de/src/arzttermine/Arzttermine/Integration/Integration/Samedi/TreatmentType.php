<?php

namespace Arzttermine\Integration\Integration\Samedi;

use Arzttermine\Core\Basic;
use Arzttermine\MedicalSpecialty\TreatmentType as ArzttermineTreatmentType;

class TreatmentType extends Basic {

    /**
     * @var string
     */
    protected $tablename = DB_TABLENAME_INTEGRATION_5_TREATMENTTYPES;

    /**
     * @var int
     */
    protected $id;

    /**
     * Arzttermine\MedicalSpecialty\TreatmentType->id
     * @var int
     */
    protected $parent_id = '';

    /**
     * @var int
     */
    protected $duration = '';

    /**
     * @var string
     */
    protected $name = '';

    /**
     * @var string
     */
    protected $insurance_ids = '';

    /**
     * @var string
     */
    protected $created_at = '';

    /**
     * @var array
     */
    protected $all_keys
        = array(
            "id",
            "parent_id",
            "duration",
            "name",
            "insurance_ids",
            "created_at"
        );

    /**
     * @var array
     */
    protected $secure_keys
        = array(
            "parent_id",
            "duration",
            "name",
            "insurance_ids",
        );

    /**
     * @return int
     */
    public function getParentId()
    {
        return $this->parent_id;
    }

    /**
     * Returns the parent (ArzttermineTreatmentType) object
     * @return ArzttermineTreatmentType
     */
    public function getParentTreatmentType()
    {
        $arzttermineTreatmenttype = new ArzttermineTreatmentType($this->getParentId());
        return $arzttermineTreatmenttype;
    }

    /**
     * @param int $parent_id
     * @return self
     */
    public function setParentId($parent_id)
    {
        $this->parent_id = $parent_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param int $duration
     * @return self
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int[]
     */
    public function getInsuranceIds()
    {
        return explode(',', $this->insurance_ids);
    }

    /**
     * @param array|int $insurance_ids
     *
     * @return self
     */
    public function setInsuranceIds($insurance_ids)
    {
        if (!is_array($insurance_ids)) {
            $insurance_ids = array(intval($insurance_ids));
        }

        $insurance_ids = array_filter($insurance_ids, 'intval');

        $this->insurance_ids = implode(',', $insurance_ids);

        return $this;
    }

}