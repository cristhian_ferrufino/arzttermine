<?php

namespace Arzttermine\Integration\Integration\Terminblock;

use Arzttermine\Appointment\Appointment;
use Arzttermine\Appointment\Provider;
use Arzttermine\Calendar\Calendar;
use Arzttermine\Integration\Integration as IntegrationCore;
use Arzttermine\Core\Configuration;

/**
 * Integration Class
 *
 * @author Axel Kuzmik <axel@kuzmik.de>
 */
class Integration extends IntegrationCore {

    /**
     * @var int
     */
    protected $id = 9;

    /**
     * @inheritdoc
     */
    public function getAvailableAppointments(Provider $provider, $format = '')
    {
        return;
    }

    /**
     * @inheritdoc
     */
    public function getNextAvailableAppointment(Provider $provider, $format = '')
    {
        return;
    }

    /**
     * @inheritdoc
     */
    public function getAppointmentsBlock(Provider $provider, $widget_slug = '')
    {
        $appointments = array();

        if ($provider->getLocation()->getStatus() != LOCATION_STATUS_VISIBLE_APPOINTMENTS) {
            $appointments['type'] = 'no-available-appointments';

            return $appointments;
        }

        $appointments['type'] = 'appointments'; // This *was* integration_9, but it functions as Terminanfrage, so leave it
        $appointments['appointment_count'] = 0; // For the "none available" message
        $appointments['widgetSlug'] = $widget_slug;

        $days = $provider->getDateRange();
        
        // Hide the Terminblock for the given number of days (from today)
        $days_to_hide = intval(Configuration::get('hide_all_int9_doctors_apts_for_next_days'));

        foreach ($days as $day) {
            $date = $day->getDate();

            // Store the day's date
            $appointments['days'][$date]['date']      = $date;
            $appointments['days'][$date]['day_text']  = Calendar::getWeekdayTextByDateTime($date, true);
            $appointments['days'][$date]['date_text'] = date('d.m.', strtotime($date));
            
            $appointments_for_one_day = array();
            
            // Only calculate appointments if we are NOT hiding today, and if today is NOT a holiday
            if (!($days_to_hide > 0)) {
                if (Calendar::isWorkingDay($date)) {
                    $appointment = new Appointment();
                    $appointment
                        ->setUser($provider->getUser())
                        ->setLocation($provider->getLocation());

                    // NOTE: The additional setDate calls are to fix a quirk in the code
                    
                    $appointment->setStartTime('08:00:00')->setEndTime('10:00:00')->setDate($date);
                    $appointments_for_one_day[] = array(
                        'url' => $appointment->getUrl($provider),
                        'time_text' => '8 - 10'
                    );

                    $appointment->setStartTime('10:00:00')->setEndTime('14:00:00')->setDate($date);;
                    $appointments_for_one_day[] = array(
                        'url' => $appointment->getUrl($provider),
                        'time_text' => '10 - 14'
                    );

                    $appointment->setStartTime('16:00:00')->setEndTime('20:00:00')->setDate($date);;
                    $appointments_for_one_day[] = array(
                        'url' => $appointment->getUrl($provider),
                        'time_text' => '16 - 20'
                    );
                }
            } else {
                $days_to_hide--;
            }

            // Store the appointments
            $appointments['days'][$date]['appointments'] = $appointments_for_one_day;
            
            // Keep count of how many appointments we have so far
            $appointments['appointment_count'] += count($appointments_for_one_day);
        }

        if (!$appointments['appointment_count']) {
            $appointmentsBlock['type'] = 'no-available-appointments';
        }
        
        return $appointments;
    }

    /**
     * @inheritdoc
     */
    public function processApiRequest()
    {
        return;
    }

    /**
     * @inheritdoc
     */
    public function hasAppointmentEnquiry()
    {
        return true;
    }
}
