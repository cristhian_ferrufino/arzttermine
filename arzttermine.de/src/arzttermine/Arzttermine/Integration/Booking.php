<?php

namespace Arzttermine\Integration;

use Arzttermine\Core\Basic;

/**
 * Changes on this object / db-table requires changes on ALL other integration_X_bookings tables!
 */
class Booking extends Basic {

    const STATUS_ERROR  = 0;
    const STATUS_OK     = 1;

    const FUNCTION_BOOK     = 1;
    const FUNCTION_CANCEL   = 2;

    /**
     * string[]
     */
    private static $STATUS_TEXTS
        = array(
            self::STATUS_ERROR  => 'Ungültig',
            self::STATUS_OK     => 'Termin weitergeleitet'
        );

    /**
     * @var string
     */
    protected $tablename = DB_TABLENAME_INTEGRATION_BOOKINGS;

    /**
     * @var int
     */
    protected $status;

    /**
     * @var int
     */
    protected $booking_id;

    /**
     * @var int
     */
    protected $function_id;

    /**
     * @var int
     */
    protected $integration_id;

    /**
     * @var string
     */
    protected $log_text;

    /**
     * @var string
     */
    protected $created_at;

    /**
     * @var string
     */
    protected $created_by;

    /**
     * @var string
     */
    protected $updated_at;

    /**
     * @var string
     */
    protected $updated_by;

    /**
     * @var array
     */
    protected $all_keys
        = array(
            "id",
            "status",
            "booking_id",
            "function_id",
            "integration_id",
            "log_text",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by"
        );

    /**
     * @var array
     */
    protected $secure_keys
        = array(
            "id",
            "status",
            "booking_id",
            "function_id",
            "integration_id",
            "log_text",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by"
        );

    /**
     * Returns the status id
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns the status text
     * Can be used as static method as well as object method
     *
     * @param int $status
     * @return string
     */
    public function getStatusText($status = null)
    {
        if ($status === null) {
            $status = $this->getStatus();
        }
        return self::$STATUS_TEXTS[$status];
    }

    /**
     * @param int $status
     *
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Adds log text
     *
     * @param string $log
     * 
     * @return self
     */
    public function addLogText($log)
    {
        $this->log_text .= $log;
        
        return $this;
    }

    /**
     * @param int $booking_id
     *
     * @return self
     */
    public function setBookingId($booking_id)
    {
        $this->booking_id = $booking_id;

        return $this;
    }

    /**
     * @return int
     */
    public function getBookingId()
    {
        return $this->booking_id;
    }

    /**
     * @param string $created_at
     *
     * @return self
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param string $created_by
     *
     * @return self
     */
    public function setCreatedBy($created_by)
    {
        $this->created_by = $created_by;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * @param int $function_id
     *
     * @return self
     */
    public function setFunctionId($function_id)
    {
        $this->function_id = $function_id;

        return $this;
    }

    /**
     * @return int
     */
    public function getFunctionId()
    {
        return $this->function_id;
    }

    /**
     * @param int $integration_id
     *
     * @return self
     */
    public function setIntegrationId($integration_id)
    {
        $this->integration_id = $integration_id;

        return $this;
    }

    /**
     * @return int
     */
    public function getIntegrationId()
    {
        return $this->integration_id;
    }

    /**
     * @return Integration
     */
    public function getIntegration()
    {
        return Integration::getIntegration($this->getIntegrationId());
    }

    /**
     * @param string $log_text
     *
     * @return self
     */
    public function setLogText($log_text)
    {
        $this->log_text = $log_text;

        return $this;
    }

    /**
     * @return string
     */
    public function getLogText()
    {
        return $this->log_text;
    }

    /**
     * @param string $updated_at
     *
     * @return self
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param string $updated_by
     *
     * @return self
     */
    public function setUpdatedBy($updated_by)
    {
        $this->updated_by = $updated_by;

        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedBy()
    {
        return $this->updated_by;
    }
}