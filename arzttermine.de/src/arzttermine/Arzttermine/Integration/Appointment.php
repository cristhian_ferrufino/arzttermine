<?php

namespace Arzttermine\Integration;

use Arzttermine\Appointment\Provider;
use Arzttermine\Booking\Appointment as CoreAppointment;
use Arzttermine\Calendar\Calendar;
use Arzttermine\Core\BasicNoIndex;

/**
 * @todo Maybe a swapping table or status attr should be added here to avoid syncing problems
 */
class Appointment extends BasicNoIndex {

    /**
     * @public string
     */
    protected $tablename = DB_TABLENAME_INTEGRATION_APPOINTMENTS;

    /**
     * @public int
     */
    protected $user_id;

    /**
     * @public int
     */
    protected $location_id;

    /**
     * @public string
     */
    protected $start_at;

    /**
     * @public string
     */
    protected $insurance_ids = '';

    /**
     * @public string
     */
    protected $medical_specialty_ids = '';

    /**
     * @public string
     */
    protected $treatment_type_ids = '';

    /**
     * @public array
     */
    protected $all_keys
        = array(
            "user_id",
            "location_id",
            "start_at",
            "insurance_ids",
            "medical_specialty_ids",
            "treatment_type_ids"
        );

    /**
     * @public array
     */
    protected $secure_keys
        = array(
            "user_id",
            "location_id",
            "start_at",
            "insurance_ids",
            "medical_specialty_ids",
            "treatment_type_ids"
        );

    /**
     * Returns the user_id
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Sets the user_id
     *
     * @param int $userId
     *
     * @return self
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Returns the location_id
     *
     * @return int
     */
    public function getLocationId()
    {
        return $this->location_id;
    }

    /**
     * Sets the location_id
     *
     * @param int $locationId
     *
     * @return self
     */
    public function setLocationId($locationId)
    {
        $this->location_id = $locationId;

        return $this;
    }

    /**
     * Returns the start_at
     *
     * @return string
     */
    public function getStartAt()
    {
        return $this->start_at;
    }

    /**
     * Sets the start_at
     *
     * @param string $startAt
     *
     * @return self
     */
    public function setStartAt($startAt)
    {
        $this->start_at = $startAt;

        return $this;
    }

    /**
     * Returns the insurance_ids
     *
     * @return array
     */
    public function getInsuranceIds()
    {
        return explode(',', $this->insurance_ids);
    }

    /**
     * Sets the insurance_ids
     *
     * @param array $insuranceIds
     *
     * @return self
     */
    public function setInsuranceIds(array $insuranceIds)
    {
        if (is_array($insuranceIds)) {
            $this->insurance_ids = implode(',', $insuranceIds);
        }

        return $this;
    }

    /**
     * Returns the medical specialty ids
     *
     * @return array
     */
    public function getMedicalSpecialtyIds()
    {
        return explode(',', $this->medical_specialty_ids);
    }

    /**
     * Sets the medical specialty ids
     *
     * @param array $medicalSpecialtyIds
     *
     * @return self
     */
    public function setMedicalSpecialtyIds(array $medicalSpecialtyIds)
    {
        if (is_array($medicalSpecialtyIds)) {
            $this->medical_specialty_ids = implode(',', $medicalSpecialtyIds);
        }

        return $this;
    }

    /**
     * Returns the treatment type ids
     *
     * @return array treatment_type_ids
     */
    public function getTreatmentTypeIds()
    {
        return explode(',', $this->treatment_type_ids);
    }

    /**
     * Sets the treatment type ids
     *
     * @param array $treatmentTypeIds
     *
     * @return self
     */
    public function setTreatmentTypeIds($treatmentTypeIds)
    {
        if (is_array($treatmentTypeIds)) {
            $this->treatment_type_ids = implode(',', $treatmentTypeIds);
        }

        return $this;
    }

    /**
     * Deletes appointments by user and location
     *
     * @param int $userId
     * @param int $locationId
     *
     * @return bool
     */
    public function delete($userId, $locationId)
    {
        if (!is_numeric($userId) || !is_numeric($locationId)) {
            return false;
        }

        return $this->deleteWhere('user_id=' . $userId . ' AND location_id=' . $locationId) > 0;
    }

    /**
     * Return array of CoreAppointment
     *
     * @todo: For now the way how the treatmenttypes and medicalspecialties are stored in the
     * DB are unefficient. But to make this method work with parameter arrays for theses filter as it should
     * we use the unlikely way to build a "OR"ed FIND_IN_SET()
     *
     * @param Provider $provider
     * @param string $format
     *
     * @return CoreAppointment[]|bool
     */
    public function getAvailableAppointments(Provider $provider, $format = '')
    {
        $subQuery = '';
        // insuranceId
        if (!empty($insuranceId)) {
            if (!is_array($insuranceId) && is_numeric($insuranceId)) {
                $insuranceId = array($insuranceId);
            }
            $subQuery .= ' AND FIND_IN_SET(' . implode(',', $insuranceId) . ', insurance_ids)';
        }
        // treatmentTypeIds
        if (!empty($treatmentTypeIds)) {
            if (!is_array($treatmentTypeIds) && is_numeric($treatmentTypeIds)) {
                $treatmentTypeIds = array($treatmentTypeIds);
            }
            $subSubQuery = '';
            foreach ($treatmentTypeIds as $treatmentTypeId) {
                $subSubQuery .= (empty($subSubQuery)) ? '' : ' OR ';
                $subSubQuery .= 'FIND_IN_SET(' . $treatmentTypeId . ', treatment_type_ids)';
            }
            $subQuery .= ' AND (' . $subSubQuery . ')';
        }
        // medicalSpecialtyIds
        if (!empty($medicalSpecialtyIds)) {
            if (!is_array($medicalSpecialtyIds) && is_numeric($medicalSpecialtyIds)) {
                $medicalSpecialtyIds = array($medicalSpecialtyIds);
            }
            $subSubQuery = '';
            foreach ($medicalSpecialtyIds as $medicalSpecialtyId) {
                $subSubQuery .= (empty($subSubQuery)) ? '' : ' OR ';
                $subSubQuery .= 'FIND_IN_SET(' . $medicalSpecialtyId . ', medical_specialty_ids)';
            }
            $subQuery .= ' AND (' . $subSubQuery . ')';
        }

        $sqlDateEnd = Calendar::getDate($datetime_end) . ' 23:59:59';
        /** @var Appointment[] $integrationAppointments */
        $integrationAppointments = $this->findObjects(
                                        '	start_at >= "' . $datetime_start . '"
			AND
				start_at <= "' . $sqlDateEnd . '"
			AND
				user_id=' . $user_id . '
			AND
				location_id=' . $location_id . '
			' . $subQuery . '
			ORDER BY start_at ASC',
                                            /**
                                             * we use DISTINCT to avoid redundant appointments which can ocur
                                             * when more than one appointment with different criterias is available.
                                             * Could be optimized if DISTINCT needs to much db resources. A post processor should
                                             * remove the redundant appoitments then.
                                             */
                                            'DISTINCT start_at'
        );

        if (empty($integrationAppointments) || !is_array($integrationAppointments)) {
            return false;
        }

        $appointments = array();
        foreach ($integrationAppointments as $integrationAppointment) {
            $appointmentStartAt = $integrationAppointment->getStartAt();
            $appointmentDate = Calendar::getDate($appointmentStartAt);

            switch ($format) {
                case 'datetime':
                    /**
                     * API uses this
                     */
                    $appointments[$appointmentDate][] = $appointmentStartAt;
                    break;
                case 'Appointment':
                default:
                    $appointment = new CoreAppointment($appointmentStartAt);
                    if ($appointment->isValid()) {
                        /**
                         * @todo: check if we need to set InsuranceIdSSSSS, MedicalSpecialtyIdSSSSS and TreatmentTypeIdSSSSS
                         *
                         *                $appointment->setInsuranceIds($integrationAppointment->getInsuranceIds());
                         *                $appointment->setMedicalSpecialtyIds($integrationAppointment->getMedicalSpecialtyIds());
                         *                $appointment->setTreatmentTypeIds($integrationAppointment->getTreatmentTypeIds());
                         */
                        $appointments[$appointmentDate][] = $appointment;
                    } else {
                        // if there are no other appointments on that date than remember
                        // that this date has no appointments but is valid
                        if (!isset($appointments[$appointmentDate])) {
                            $appointments[$appointmentDate] = array();
                        }
                    }
                    break;
            }
        }

        return $appointments;
    }

    /**
     * Returns the next available appointment for a date area from the cache
     *
     * @param Provider $provider
     * @param string $format [Appointment, datetime]
     *
     * @return CoreAppointment[]|bool false on error
     */
    public function getNextAvailableAppointment(Provider $provider, $format = '')
    {
        $subQuery = '';
        // insuranceId
        if (!empty($insurance_id)) {
            if (!is_array($insurance_id) && is_numeric($insurance_id)) {
                $insurance_id = array($insurance_id);
            }
            $subQuery .= ' AND FIND_IN_SET(' . implode(',', $insurance_id) . ', insurance_ids)';
        }
        // treatmentTypeIds
        if (!empty($treatmenttype_ids)) {
            if (!is_array($treatmenttype_ids) && is_numeric($treatmenttype_ids)) {
                $treatmenttype_ids = array($treatmenttype_ids);
            }
            $subSubQuery = '';
            foreach ($treatmenttype_ids as $treatmenttype_id) {
                $subSubQuery .= (empty($subSubQuery)) ? '' : ' OR ';
                $subSubQuery .= 'FIND_IN_SET(' . $treatmenttype_id . ', treatment_type_ids)';
            }
            $subQuery .= ' AND (' . $subSubQuery . ')';
        }
        // medicalSpecialtyIds
        if (!empty($medical_specialty_ids)) {
            if (!is_array($medical_specialty_ids) && is_numeric($medical_specialty_ids)) {
                $medical_specialty_ids = array($medical_specialty_ids);
            }
            $subSubQuery = '';
            foreach ($medical_specialty_ids as $medical_specialty_id) {
                $subSubQuery .= (empty($subSubQuery)) ? '' : ' OR ';
                $subSubQuery .= 'FIND_IN_SET(' . $medical_specialty_id . ', medical_specialty_ids)';
            }
            $subQuery .= ' AND (' . $subSubQuery . ')';
        }

        $date_end = Calendar::getDate($date_end) . ' 23:59:59';
        /** @var Appointment[] $integration_appointments */
        $integration_appointments = $this->findObjects(
                                         '	start_at >= "' . $date_start . '"
			AND
				start_at <= "' . $date_end . '"
			AND
				user_id=' . $user_id . '
			AND
				location_id=' . $location_id . '
			' . $subQuery . '
			ORDER BY start_at ASC
			LIMIT 1'
        );

        if (!is_array($integration_appointments) || empty($integration_appointments)) {
            return false;
        }
        foreach ($integration_appointments as $integration_appointment) {
            switch ($format) {
                case 'datetime':
                    return $integration_appointment->getStartAt();

                case 'Appointment':
                default:
                    $appointment = new CoreAppointment($integration_appointment->getStartAt());
                    if ($appointment->isValid()) {
                        return $appointment;
                    }
            }
        }

        return false;
    }
}
