<?php

namespace Arzttermine\Integration;

use Arzttermine\Appointment\Provider;
use Arzttermine\Booking\Booking;

interface IntegrationInterface {

    public function processApiRequest();

    public function bookAppointment(Booking $booking);

    public function cancelBooking(Booking $booking);

    public function hasAppointmentEnquiry();

    public function getAppointmentsBlock(Provider $provider, $widget_slug = '');
}