<?php

namespace Arzttermine\Integration;

use Arzttermine\Appointment\Provider;
use Arzttermine\Booking\Booking as StandardBooking;
use Arzttermine\Calendar\Calendar;
use Arzttermine\Location\Location;
use Arzttermine\Appointment\Appointment as AppointmentAppointment;

abstract class Integration implements IntegrationInterface {

    /**
     * @var int
     */
    protected $id = 0;

    /**
     * @var int
     */
    protected $status;

    /**
     * @var int
     */
    protected $last_error_code;

    /**
     * @var string
     */
    protected $last_error_message;

    /**
     * @var array
     */
    public static $INTEGRATIONS_CLASSES_INSTANCE_CACHES = array();

    /**
     * Returns the id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function processApiRequest()
    {
        return;
    }

    /**
     * {@inheritdoc}
     */
    public function hasAppointmentEnquiry()
    {
        return false;
    }

    /**
     * Returns the integration object
     *
     * @param int $integration_id
     *
     * @return Integration
     */
    public static function getIntegration($integration_id)
    {
        if (!isset($GLOBALS['CONFIG']['INTEGRATIONS'][$integration_id])) {
            // @todo: Throw exception
            return false;
        }

        $integration_class_name = $GLOBALS['CONFIG']['INTEGRATIONS'][$integration_id]['CLASS_NAME'];
        $integration = new $integration_class_name();

        return $integration;
    }

    /**
     * Returns the integration object
     *
     * @param int $location_id
     *
     * @return Integration
     */
    public static function getIntegrationByLocationId($location_id)
    {
        $location = new Location($location_id);

        if (!$location->isValid()) {
            return false;
        }

        return self::getIntegration($location->getIntegrationId());
    }

    /**
     * Returns the status id
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set the last error code
     *
     * @param int $code
     */
    public function setLastErrorCode($code)
    {
        $this->last_error_code = intval($code);
    }

    /**
     * Returns the last error code (if available)
     *
     * @return int
     */
    public function getLastErrorCode()
    {
        return $this->last_error_code;
    }

    /**
     * Sets the last error message
     *
     * @param string $error_message
     *
     * @return self
     */
    public function setLastErrorMessage($error_message)
    {
        $this->last_error_message = trim($error_message);

        return $this;
    }

    /**
     * Returns the last error message (if available)
     *
     * @return string
     */
    public function getLastErrorMessage()
    {
        return $this->last_error_message;
    }

    /**
     * @inheritdoc
     */
    public function isConnected($user_id, $location_id)
    {
        $integrationConnection = new IntegrationConnection();
        return $integrationConnection->isConnected($user_id, $location_id);
    }

    /**
     * Returns the integration name
     *
     * @param int $integration_id
     *
     * @return string
     */
    public static function findName($integration_id)
    {
        $name = 'Invalid Integration';

        if (isset($GLOBALS['CONFIG']['INTEGRATIONS'][$integration_id]['NAME'])) {
            $name = $GLOBALS['CONFIG']['INTEGRATIONS'][$integration_id]['NAME'];
        }

        return $name;
    }

    /**
     * Returns the integration name
     *
     * @param int $integration_id
     *
     * @return string
     */
    public function getName($integration_id = 0)
    {
        if (!$integration_id) {
            $integration_id = $this->getId();
        }

        return self::findName($integration_id);
    }

    /**
     * Returns wheather the integration has manual booking
     *
     * @return string
     */
    public function hasAdminManualBooking()
    {
        return $GLOBALS['CONFIG']['INTEGRATIONS'][$this->getId()]['ADMIN_MANUAL_BOOKING'];
    }

    /**
     * returns wheather the integration has manual booking
     *
     * @return string
     */
    public function hasAdminManualMailing()
    {
        return $GLOBALS['CONFIG']['INTEGRATIONS'][$this->getId()]['ADMIN_MANUAL_MAILING'];
    }

    /**
     * Returns all available appointments for a datetime block
     *
     * @param Provider $provider
     * @param string $format
     *
     * @return array
     */
    public function getAvailableAppointments(Provider $provider, $format = '')
    {
        return array();
    }

    /**
     * Returns the next available time
     *
     * @param Provider $provider
     * @param string $format
     *
     * @return array
     */
    public function getNextAvailableAppointment(Provider $provider, $format = '')
    {
        return array();
    }

    /**
     * @param Provider $provider
     * @param string $widget_slug
     *
     * @return array
     */
    public function getAppointmentsBlock(Provider $provider, $widget_slug = '')
    {
        $appointments['type'] = 'appointments';
        $appointments['appointment_count'] = 0; // For the "none available" message
        
        $found_appointments = array();
        
        // Sort the hits we have and just take the data we need
        foreach ($provider->getAppointments() as $found_appointment) {
            $found_appointments[$found_appointment->getDate()->getDate()][] = array(
                'url' => $found_appointment->getUrl($provider),
                'time_text' => $found_appointment->getTimeText()
            );

            $appointments['appointment_count']++;
        }
        
        // Loop through each available day in the query (usually about 20 days)
        foreach ($provider->getDateRange() as $date) {
            $the_day = $date->getDate();
            // Set the output data for this. We need labels etc
            $appointments['days'][$the_day]['date_text'] = $date->format('d.m.');
            $appointments['days'][$the_day]['day_text']  = Calendar::getWeekdayTextByDateTime($the_day, true);
            
            // If we also found appointments above, add them
            if (isset($found_appointments[$the_day])) {
                $appointments['days'][$the_day]['appointments'] = $found_appointments[$the_day];
            }
        }
        
        if (!$appointments['appointment_count']) {
            $appointments['type'] = 'no-available-appointments';
        }
        
        return $appointments;
    }
    
    /**
     * @param int $user_id
     * @param int $location_id
     *
     * @return bool
     */
    public function checkDoctorHasPrivateAA($user_id, $location_id)
    {
        $dateStart = Calendar::getFirstPossibleDateTimeForSearch();
        $dateEnd = Calendar::addDays($dateStart, $GLOBALS['CONFIG']['CALENDAR_MAX_DAYS_AHEAD'], true);
        $_appointment = $this->getNextAvailableAppointment($dateStart, $dateEnd, $user_id, $location_id, 1);

        return is_object($_appointment);
    }

    /**
     * @param int $user_id
     * @param int $location_id
     *
     * @return bool
     */
    public function checkDoctorHasPublicAA($user_id, $location_id)
    {
        $dateStart = Calendar::getFirstPossibleDateTimeForSearch();
        $dateEnd = Calendar::addDays($dateStart, $GLOBALS['CONFIG']['CALENDAR_MAX_DAYS_AHEAD'], true);
        $_appointment = $this->getNextAvailableAppointment($dateStart, $dateEnd, $user_id, $location_id, 2);

        return is_object($_appointment);
    }

    /**
     * Book an appointment
     *
     * @param StandardBooking $booking
     *
     * @return bool
     */
    public function bookAppointment(StandardBooking $booking)
    {
        StandardBooking::sendBookingEmail($booking, 'booking-new.html');

        return true;
    }

    /**
     * Cancel a booking
     *
     * @param StandardBooking $booking
     *
     * @return bool
     */
    public function cancelBooking(StandardBooking $booking)
    {
        return true;
    }

    /**
     * Returns all integration booking objects in an array for the given booking
     *
     * @param StandardBooking $booking
     *
     * @return array
     */
    public function getIntegrationBookings(StandardBooking $booking)
    {
        $integration_booking = new Booking();

        return $integration_booking->findObjects('booking_id=' . $booking->getId() . ' ORDER BY created_at DESC;');
    }

    /**
     * Check if the appointment is valid for the specific integration.
     * Each integration should have its own checkers depending on the integrations requirements.
     *
     * @param AppointmentAppointment $appointment
     *
     * @return bool
     */
    public function appointmentIsValid(AppointmentAppointment $appointment)
    {
        return true;
    }
}
