<?php

namespace Arzttermine\Search;

use Arzttermine\Core\Basic;
use Arzttermine\Insurance\Insurance;

/**
 * SearchResultText Class
 *
 * @author Axel Kuzmik <axel@kuzmik.de>
 */
class Text extends Basic {

    const STATUS_DRAFT = 0;

    const STATUS_ACTIVE = 1;

    /**
     * @var string
     */
    protected $tablename = DB_TABLENAME_SEARCHRESULTTEXTS;

    /**
     * @var int
     */
    protected $status;

    /**
     * @var string
     */
    protected $permalink;

    /**
     * @var string
     */
    protected $address_search;

    /**
     * @var int
     */
    protected $district_id;

    /**
     * @var int
     */
    protected $medical_specialty_id;

    /**
     * @var int
     */
    protected $insurance_id;

    /**
     * @var string
     */
    protected $content_1;

    /**
     * @var string
     */
    protected $html_title;

    /**
     * @var string
     */
    protected $html_h1;

    /**
     * @var string
     */
    protected $html_meta_description;

    /**
     * @var string
     */
    protected $html_meta_keywords;

    /**
     * @var string
     */
    protected $created_at;

    /**
     * @var string
     */
    protected $created_by;

    /**
     * @var string
     */
    protected $updated_at;

    /**
     * @var string
     */
    protected $updated_by;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var int
     */
    protected $profile_asset;

    /**
     * @var int
     */
    protected $mf_average;

    /**
     * @var int
     */
    protected $mf_total;

    /**
     * @var array
     */
    protected $all_keys
        = array(
            "id",
            "status",
            "permalink",
            "medical_specialty_id",
            "insurance_id",
            "address_search",
            "district_id",
            "content_1",
            "html_title",
            "html_meta_description",
            "html_meta_keywords",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by"
        );

    /**
     * @var array
     */
    protected $secure_keys
        = array(
            "id",
            "status",
            "permalink",
            "medical_specialty_id",
            "insurance_id",
            "address_search",
            "district_id",
            "content_1",
            "html_title",
            "html_meta_description",
            "html_meta_keywords",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by"
        );

    /**
     * Calc some vars
     *
     * @return bool
     */
    public function calcVars()
    {
        $this->setUrl($this->permalink);

        return true;
    }

    /**
     * @param string $updated_by
     *
     * @return self
     */
    public function setUpdatedBy($updated_by)
    {
        $this->updated_by = $updated_by;

        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedBy()
    {
        return $this->updated_by;
    }

    /**
     * @param string $updated_at
     *
     * @return self
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param string $created_by
     *
     * @return self
     */
    public function setCreatedBy($created_by)
    {
        $this->created_by = $created_by;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * @param string $html_h1
     *
     * @return self
     */
    public function setHtmlH1($html_h1)
    {
        $this->html_h1 = $html_h1;

        return $this;
    }

    /**
     * @return string
     */
    public function getHtmlH1()
    {
        return $this->html_h1;
    }

    /**
     * @param string $created_at
     *
     * @return self
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param int $profile_asset
     *
     * @return self
     */
    public function setProfileAsset($profile_asset)
    {
        $this->profile_asset = $profile_asset;

        return $this;
    }

    /**
     * @return int
     */
    public function getProfileAsset()
    {
        return $this->profile_asset;
    }

    /**
     * @param int $district_id
     *
     * @return self
     */
    public function setDistrictId($district_id)
    {
        $this->district_id = $district_id;

        return $this;
    }

    /**
     * @return int
     */
    public function getDistrictId()
    {
        return $this->district_id;
    }

    /**
     * @param int $insurance_id
     *
     * @return self
     */
    public function setInsuranceId($insurance_id)
    {
        $this->insurance_id = $insurance_id;

        return $this;
    }

    /**
     * @return int
     */
    public function getInsuranceId()
    {
        return $this->insurance_id;
    }

    /**
     * @return Insurance
     */
    public function getInsurance()
    {
        return new Insurance($this->getInsuranceId());
    }

    /**
     * @param int $medical_specialty_id
     *
     * @return self
     */
    public function setMedicalSpecialtyId($medical_specialty_id)
    {
        $this->medical_specialty_id = $medical_specialty_id;

        return $this;
    }

    /**
     * @return int
     */
    public function getMedicalSpecialtyId()
    {
        return $this->medical_specialty_id;
    }

    /**
     * @param string $address_search
     *
     * @return self
     */
    public function setAddressSearch($address_search)
    {
        $this->address_search = $address_search;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddressSearch()
    {
        return $this->address_search;
    }

    /**
     * @param string $permalink
     *
     * @return self
     */
    public function setPermalink($permalink)
    {
        $this->permalink = $permalink;

        return $this;
    }

    /**
     * @return string
     */
    public function getPermalink()
    {
        return $this->permalink;
    }

    /**
     * @param string $url
     *
     * @return self
     */
    public function setUrl($url)
    {
        $this->url = trim($url);

        return $this;
    }

    /**
     * @param bool $absolute
     * @param bool $secure
     *
     * @return string
     */
    public function getUrl($absolute = false, $secure = false)
    {
        return ($absolute ? ($secure ? $GLOBALS['CONFIG']['URL_HTTPS_LIVE'] : $GLOBALS['CONFIG']['URL_HTTP_LIVE']) : '') . $this->url;
    }

    /**
     * Returns the status id
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Checks if this object has a specific html title
     *
     * @return bool
     */
    public function hasHtmlTitle()
    {
        return (bool)$this->html_title;
    }

    /**
     * Returns this objects specific html title
     *
     * @return string
     */
    public function getHtmlTitle()
    {
        return $this->html_title;
    }

    /**
     * Checks if this object has a content_1
     *
     * @return bool
     */
    public function hasContent1()
    {
        return (bool)$this->content_1;
    }

    /**
     * Returns this objects content_1
     *
     * @return string
     */
    public function getContent1()
    {
        return do_shortcode(wpautop($this->content_1));
    }

    /**
     * Checks if this object has a specific html meta description
     *
     * @return bool
     */
    public function hasHtmlMetaDescription()
    {
        return (bool)$this->html_meta_description;
    }

    /**
     * Returns this objects specific html meta description
     *
     * @return string
     */
    public function getHtmlMetaDescription()
    {
        return $this->html_meta_description;
    }

    /**
     * Checks if this object has a specific html meta keywords
     *
     * @return bool
     */
    public function hasHtmlMetaKeywords()
    {
        return (bool)$this->html_meta_keywords;
    }

    /**
     * Returns this objects specific html meta keywords
     *
     * @return string
     */
    public function getHtmlMetaKeywords()
    {
        return $this->html_meta_keywords;
    }
}
