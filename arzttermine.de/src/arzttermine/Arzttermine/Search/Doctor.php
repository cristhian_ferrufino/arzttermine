<?php

namespace Arzttermine\Search;

use Arzttermine\Application\Application;
use Arzttermine\Calendar\Calendar;
use Arzttermine\Event\Event;
use Arzttermine\Location\District;
use Arzttermine\Location\Location;
use Arzttermine\Maps\GoogleMap;
use Arzttermine\Media\Asset;
use Arzttermine\MedicalSpecialty\MedicalSpecialty;
use Arzttermine\User\User;
use NGS\Managers\LocationsManager;
use NGS\Managers\SearchResultCacheTmpManager;
use NGS\Managers\TreatmentTypesManager;
use NGS\Util\ProviderRow;

/**
 * Class Doctor
 * @package Arzttermine\Search
 *
 * @deprecated
 */
class Doctor {

    /**
     * Execute the SQL query and search for providers with the given id's
     *
     * @todo DO NOT rely on View::setRef for program flow. Data should be returned.
     *
     * @param $userIds
     *
     * @return ProviderRow[]
     **/
    public static function getDoctorsById($userIds)
    {
        $app = Application::getInstance();
        $db = $app->getSql();
        $view = $app->getView();

        $query
            = "	SELECT
						" . DB_TABLENAME_USERS . ".id as user_id,
						" . DB_TABLENAME_USERS . ".status as user_status,
						" . DB_TABLENAME_USERS . ".slug as user_slug,
						" . DB_TABLENAME_USERS . ".title as user_title,
						" . DB_TABLENAME_USERS . ".first_name as user_first_name,
						" . DB_TABLENAME_USERS . ".last_name as user_last_name,
						" . DB_TABLENAME_USERS . ".profile_asset_id as user_profile_asset_id,
						" . DB_TABLENAME_USERS . ".profile_asset_filename as user_profile_asset_filename,
						" . DB_TABLENAME_LOCATIONS . ".id as location_id,
						" . DB_TABLENAME_LOCATIONS . ".status as location_status,
						" . DB_TABLENAME_LOCATIONS . ".integration_id as location_integration_id,
						" . DB_TABLENAME_LOCATIONS . ".street as location_street,
						" . DB_TABLENAME_LOCATIONS . ".city as location_city,
						" . DB_TABLENAME_LOCATIONS . ".zip as location_zip,
						" . DB_TABLENAME_LOCATIONS . ".lat as location_lat,
						" . DB_TABLENAME_LOCATIONS . ".lng as location_lng
					FROM " . DB_TABLENAME_LOCATIONS . "
					INNER JOIN `location_user_connections` ON `location_user_connections`.`location_id` = " . DB_TABLENAME_LOCATIONS . ".`id`
					INNER JOIN " . DB_TABLENAME_USERS . " ON " . DB_TABLENAME_USERS . ".`id`= `location_user_connections`.`user_id` ";

        $query .= "WHERE ";
        $query .= sprintf(" `user_id` IN (%s) ", join(",", $userIds));

        $db->query($query);

        $providers = array();
        if ($db->nf() > 0) {
            while ($db->next_record()) {
                /*
                 * make sure the Record has only assocciated and not also index values
                * or this could result in much higher memory allocation
                */
                $data = $db->Record;

                $provider = new ProviderRow();
                $provider->setSearchResultData($data);
                // $provider->setMedicalSpecialtyId($medicalSpecialtyId);
                // $provider->setTreatmentTypeId($treatmentTypeId);
                $providers[] = $provider;
            }
        }

        $result = array();

        $total = count($providers);
        if ($total == 0) {
            return array();
        }

        $result['search_result_total'] = $total;
        $view->setRef('search_result_total', $result['search_result_total']);

        $result['providers'] = $providers;
        $view->setRef('providerRows', $providers);

        return $result;
    }

    /**
     * Set the markers for google maps
     * Here we need to rebuild some of the User class methods as we
     * don't want to initiate all the users as objects which cost to much time
     * 
     * @param GoogleMap $map
     * @param array $providers
     * @param array $filter
     */
    public static function setGooglemapMarkers(GoogleMap $map, $providers, $filter)
    {
        $index = 0;
        /** @var ProviderRow $provider */
        foreach ((array)$providers as $provider) {
            $lat = $provider->getSearchResultData('location_lat');
            $lng = $provider->getSearchResultData('location_lng');
            if ($lat == 0 || $lng == 0) {
                continue;
            }

            $name = $provider->getSearchResultData('user_title') . ' ' .
                    $provider->getSearchResultData('user_first_name') . ' ' .
                    $provider->getSearchResultData('user_last_name');

            // the url should match $user->getUrlWithFilter($filter)
            $insurance_slug = '';
            if (isset($filter['insurance_id']) && isset($GLOBALS['CONFIG']['INSURANCE_SLUGS'][$filter['insurance_id']])) {
                $insurance_slug = '/' . $GLOBALS['CONFIG']['INSURANCE_SLUGS'][$filter['insurance_id']];
            }
            $url = '/arzt/' .
                   $provider->getSearchResultData('user_slug') .
                   $insurance_slug;

            // the url_profile_image should match $user->getProfileAssetUrl(ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED)
            $url_profile_image = '';
            $profile_asset_id = $provider->getSearchResultData('user_profile_asset_id');
            $profile_asset_filename = $provider->getSearchResultData('user_profile_asset_filename');

            if (is_numeric($profile_asset_id) && $profile_asset_id > 0 && $profile_asset_filename != '') {
                $url_asset = Asset::getUrlByIdFilenameOwner($profile_asset_id, $profile_asset_filename, ASSET_OWNERTYPE_USER, ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED, false);
                if ($url_asset) {
                    $url_profile_image = $url_asset;
                } else {
                    $url_profile_image = getStaticUrl($GLOBALS['CONFIG']['ASSET_GFX_TYPES'][ASSET_OWNERTYPE_USER]['SIZES'][ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED]['default'], false);
                }
            }

            $map->addMarker(
                $provider->getSearchResultData('user_slug'),
                $lat,
                $lng,
                $url,
                $url_profile_image,
                $name,
                $provider->getSearchResultData('location_street'),
                $provider->getSearchResultData('location_zip') . ' ' . $provider->getSearchResultData('location_city'),
                $provider->getUser()->hasContract()
            );
            $index++;
        }
    }

    /**
     * Returns doctors as HTML blocks and metadata used for displaying the results (e.g. date_start, pagination etc.)
     * Used for the initial page rendering (search.php) or as an API call for pagination (class.api.php).
     *
     * @param array $params form or request data
     *
     * @return array
     */
    public static function doSearch(array $params)
    {
        $app = Application::getInstance();
        $view = $app->getView();

        // Kludge
        if (!isset($params['distance'])) {
            $params['distance'] = 2; // See config.php for distance values
        }
        
        // DATES ================================================================

        /**
         * calc date_start, date_end, date_days, date_calendar_days
         * date_calendar_days: number of days between date_start and date_end (including date_start)
         * date_days: number of days shown in the calendar
         */
        $date_calendar_days = 20;

        $date_start = getParam('date_start');
        if (empty($date_start)) {
            $date_start = Calendar::getFirstPossibleDateTimeForSearch();
        }
        
        // minus 1 to include the date_start
        $date_end = Calendar::addDays($date_start, ($date_calendar_days - 1), true);
        $date_days = count(Calendar::getDatesInAreaWithFilter($date_start, $date_end));
        

        // END - DATES ================================================================

        $page = getParam('p', 1);

        $city = self::getSearchResultsFirstDoctorCity($params);
        $result = self::getSearchResults($params, $page, $GLOBALS['CONFIG']['SEARCH_PAGINATION_NUM'], $city);

        // set the search-location for show at least the "Suchstandort"
        $view->setRef('lat', $params['lat']);
        $view->setRef('lng', $params['lng']);

        if (!isset($result['search_result_total']) || $result['search_result_total'] == 0) {
            $view->addMessage('STATUS', 'ERROR', 'In dieser Region haben wir leider keine Ärzte mit Ihren Suchkriterien gefunden.');
        } else {
            // get the district values for the select box
            $districts = District::getCityDistricts($city);
            if (!empty($districts)) {
                $view->setRef('districts', $districts);
            }

            if (isset($params['treatmenttype_id']) && $params['treatmenttype_id'] > 0) {
                $treatmentTypesManager = TreatmentTypesManager::getInstance();
                $treatmenttypeDto = $treatmentTypesManager->getById(intval($params['treatmenttype_id']));
                $view->setRef('treatmenttypeDto', $treatmenttypeDto);
            }

            $view->setRef('search_result_total', $result['search_result_total']);
            if (isset($result['filter'])) {
                $view->setRef('filter', $result['filter']);
            }
            if (isset($result['pagination'])) {
                $view->setRef('pagination', $result['pagination']);
            }
            if (isset($result['providers'])) {
                $view->setRef('providers', $result['providers']);
            }
            if (isset($result['providers_start_index'])) {
                $view->setRef('providers_start_index', $result['providers_start_index']);
            }
            if (isset($result['providers_end_index'])) {
                $view->setRef('providers_end_index', $result['providers_end_index']);
            }

            // treat treatmenttype as a string to allow non set value
            $mapMarkerApiUrl = sprintf(
                '%s/search-markers.json?medical_specialty_id=%d&treatmenttype_id=%s&insurance_id=%d&lat=%f&lng=%f&location=%s&distance=%d',
                $GLOBALS['CONFIG']['URL_API'],
                $params['medical_specialty_id'],
                $params['treatmenttype_id'] > 0 ? $params['treatmenttype_id'] : '',
                $params['insurance_id'],
                $params['lat'],
                $params['lng'],
                $params['location'],
                $params['distance']
            );
            $view->setRef('mapMarkerApiUrl', $mapMarkerApiUrl);

            /**
             * load the appointment data for each visible user (on the current page)
             **/
            $searchResultCacheTmpManager = SearchResultCacheTmpManager::getInstance();
            // add the appointment data to the providers and filter them as well
            $providerRows = $searchResultCacheTmpManager->getProviderRowsByFilters(
                $result['providers'],
                $date_start,
                $date_end,
                $result['filter']['insurance_id'],
                $result['filter']['treatmenttype_id'],
                1
            );
            
            $view
                ->setRef('date_start', $date_start)
                ->setRef('date_end', $date_end)
                ->setRef('date_calendar_days', $date_calendar_days)
                ->setRef('date_days', $date_days)
                ->setRef('providerRows', $providerRows);

            $result['dates'] = array(
                'date_start'         => $date_start,
                'date_end'           => $date_end,
                'date_calendar_days' => $date_calendar_days,
                'date_days'          => $date_days
            );

            return $result;
        }

        return array();
    }

    /**
     * @param array $form
     *
     * @return array
     */
    public static function getSearchResultsFirstDoctorCity($form)
    {
        $locationsManager = LocationsManager::getInstance();

        $distance = $GLOBALS['CONFIG']['SEARCH_DISTANCES_KM'][2];
        if (isset($form['distance']) && isset($GLOBALS['CONFIG']['SEARCH_DISTANCES_KM'][$form['distance']])) {
            $distance = $GLOBALS['CONFIG']['SEARCH_DISTANCES_KM'][$form['distance']];
        }

        $city = $locationsManager->getCityByGivenFilters(
            Location::STATUS_VISIBLE_APPOINTMENTS,
            User::STATUS_VISIBLE_APPOINTMENTS,
            !empty($form['medical_specialty_id']) ? intval($form['medical_specialty_id']) : 0,
            !empty($form['treatmenttype_id'])     ? intval($form['treatmenttype_id'])     : 0,
            $distance,
            $form['lng'],
            $form['lat']
        );

        return $city;
    }

    /**
     * Return an array with all relevant data for a search
     *
     * @param form
     * @param page
     * @param per_page (optional)
     * @param city (optional)
     * @param per_page =null
     *
     * @return array[
     *        'providers' = array ProviderRow,
     *        'filter' = array['medical_specialty_id','insurance_id','treatmenttype_id'],
     *        'search_result_total',
     *        'pagination',
     *        'providers_start_index',
     *        'providers_end_index',
     *        ]
     */
    public static function getSearchResults($form, $page, $per_page = null, $city = null)
    {
        global $CONFIG;
        
        $app = Application::getInstance();
        $router = $app->container->get('router');
        
        $medicalSpecialtyId = $form['medical_specialty_id'];
        $treatmentTypeId = !empty($form['treatmenttype_id']) ? $form['treatmenttype_id'] : 0;
        $distance = $form['distance'];

        // The search can be an index or a number
        // This is to try to help with backwards compatibility but is NOT good
        // @todo Remove indexed distances
        if (!empty($CONFIG['SEARCH_DISTANCES_KM'][$distance])) {
            $distance = $CONFIG['SEARCH_DISTANCES_KM'][$distance];
        } elseif (empty($distance)) {
            $distance = $CONFIG['SEARCH_DISTANCES_KM_DEFAULT'];
        }
        
        // get all providers that match
        $providers = self::findDoctorsByGivenFilters(
            $medicalSpecialtyId,
            $treatmentTypeId,
            $city,
            $distance,
            $form['lng'],
            $form['lat']
        );

        $result = array();
        // set the filter
        $result['filter'] = array(
            'medical_specialty_id' => $medicalSpecialtyId,
            'insurance_id'         => $form['insurance_id'],
            'treatmenttype_id'     => $treatmentTypeId
        );

        $total = count($providers);
        if ($total == 0) {
            return array();
        }
        $result['search_result_total'] = $total;

        /**
         * build the pagination and set offset *****************************
         */
        if (!is_numeric($per_page) || $per_page < 1 || $per_page > $GLOBALS['CONFIG']['SEARCH_MAX_RESULTS']) {
            $per_page = $GLOBALS['CONFIG']['SEARCH_PAGINATION_NUM'];
        }
        // sanitize page
        if (!is_numeric($page) || $page < 1 || (($page - 1) > ceil($total / $per_page))) {
            $page = 1;
        }
        $_offset = 0;
        // PAGINATION
        if ($total > $per_page) {
            $_offset = (($page - 1) * $per_page);

            $params = array(
                'form' => array(
                    'medical_specialty_id' => $medicalSpecialtyId,
                    'treatmenttype_id'     => $treatmentTypeId,
                    'insurance_id'         => $form['insurance_id'],
                    'lat'                  => $form['lat'],
                    'lng'                  => $form['lng'],
                    'location'             => $form['location']
                )
            );
            
            // Widget slug param
            $url = $router->generate('search');

            if ($app->isWidget()) {
                $url = $router->generate('widget', array('widget_slug' => $app->getWidgetContainer()->getSlug(), 'parameter_string' => $url));
            }
            
            $url .= '?' . http_build_query($params);
            
            $result['pagination'] = paginate_links(
                array(
                    'base'              => $url . '&p=%#%',
                    'format'            => '',
                    'total'             => ceil($total / $per_page),
                    'current'           => $page,
                    'add_args'          => '',
                    'next_text'         => Application::getInstance()->_('mehr Ärzte'),
                    'prev_text'         => Application::getInstance()->_('zurück'),
                    'show_page_numbers' => false
                )
            );
        }

        $providers_start_index = $_offset;
        $providers_end_index = (($page * $per_page) - 1);
        // sanitize the correct end
        if ($providers_end_index >= $total) {
            $providers_end_index = ($total - 1);
        }

        if ($per_page >= $GLOBALS['CONFIG']['SEARCH_MAX_RESULTS']) {
            // we need all results
            $result['providers'] = $providers;
        } else {
            // get the requested providers only
            $requestedProviders = array();
            for ($x = $providers_start_index; $x <= $providers_end_index; $x++) {
                $requestedProviders[$x] = $providers[$x];
            }
            unset($providers);
            $result['providers'] = $requestedProviders;
        }

        $result['providers_start_index'] = $providers_start_index;
        $result['providers_end_index'] = $providers_end_index;

        return $result;
    }

    /**
     * Execute the SQL query and search for matching providers
     * Filters some results also
     *
     * @param medicalSpecialtyId
     * @param treatmentTypeId
     * @param city
     * @param distance
     * @param lng
     * @param lat
     *
     * @return array ProviderRow
     *
     **/
    public static function findDoctorsByGivenFilters(
        $medicalSpecialtyId,
        $treatmentTypeId,
        $city,
        $distance,
        $lng,
        $lat
    )
    {
        $db = Application::getInstance()->getSql();

        // sanitizing
        $medicalSpecialtyId = is_numeric($medicalSpecialtyId) ? $medicalSpecialtyId : 0;
        $treatmentTypeId = is_numeric($treatmentTypeId) ? $treatmentTypeId : 0;
        $city = sqlsave($city);
        if (
            !is_numeric($lng)
            ||
            !is_numeric($lat)
            ||
            !is_numeric($distance)
        ) {
            return array();
        }

        $q
            = "(
			 6368 * SQRT(2*(1-cos(RADIANS(`lat`)) *
			 cos(RADIANS(" . $lat . ")) * (sin(RADIANS(`lng`)) *
			 sin(RADIANS(" . $lng . ")) + cos(RADIANS(`lng`)) *
			 cos(RADIANS(" . $lng . "))) - sin(RADIANS(`lat`)) * sin(RADIANS(" . $lat . "))))) AS distance ";

        $query
            = "SELECT
					" . DB_TABLENAME_USERS . ".id as user_id,
					" . DB_TABLENAME_USERS . ".status as user_status,
					" . DB_TABLENAME_USERS . ".slug as user_slug,
					" . DB_TABLENAME_USERS . ".title as user_title,
					" . DB_TABLENAME_USERS . ".first_name as user_first_name,
					" . DB_TABLENAME_USERS . ".last_name as user_last_name,
					" . DB_TABLENAME_USERS . ".profile_asset_id as user_profile_asset_id,
					" . DB_TABLENAME_USERS . ".profile_asset_filename as user_profile_asset_filename,
					" . DB_TABLENAME_LOCATIONS . ".id as location_id,
					" . DB_TABLENAME_LOCATIONS . ".status as location_status,
					" . DB_TABLENAME_LOCATIONS . ".integration_id as location_integration_id,
					" . DB_TABLENAME_LOCATIONS . ".street as location_street,
					" . DB_TABLENAME_LOCATIONS . ".city as location_city,
					" . DB_TABLENAME_LOCATIONS . ".zip as location_zip,
					" . DB_TABLENAME_LOCATIONS . ".lat as location_lat,
					" . DB_TABLENAME_LOCATIONS . ".lng as location_lng,
					" . $q . "
				FROM " . DB_TABLENAME_LOCATIONS . "
						INNER JOIN `location_user_connections` ON `location_user_connections`.`location_id` = " . DB_TABLENAME_LOCATIONS . ".`id`
						INNER JOIN " . DB_TABLENAME_USERS . " ON " . DB_TABLENAME_USERS . ".`id`= `location_user_connections`.`user_id` ";

        if (!empty($treatmentTypeId) && is_numeric($treatmentTypeId)) {
            $query .= "LEFT JOIN " . DB_TABLENAME_DOCTOR_TREATMENTTYPES . " ON " . DB_TABLENAME_DOCTOR_TREATMENTTYPES . ".`doctor_id`=" . DB_TABLENAME_USERS . ".`id` WHERE " . DB_TABLENAME_DOCTOR_TREATMENTTYPES . ".`treatment_type_id`=" . $treatmentTypeId . " AND";
        } else {
            $query .= "WHERE ";
        }

        $query .= " FIND_IN_SET(" . $medicalSpecialtyId . ", " . DB_TABLENAME_USERS . ".`medical_specialty_ids`)
				AND FIND_IN_SET(" . $medicalSpecialtyId . ", " . DB_TABLENAME_LOCATIONS . ".`medical_specialty_ids`)
				AND
				(
					" . DB_TABLENAME_LOCATIONS . ".`status`=" . LOCATION_STATUS_VISIBLE . "
					OR
					" . DB_TABLENAME_LOCATIONS . ".`status`=" . LOCATION_STATUS_VISIBLE_APPOINTMENTS . "
				)
				AND
				(
					" . DB_TABLENAME_USERS . ".`status`=" . USER_STATUS_VISIBLE . "
					OR
					" . DB_TABLENAME_USERS . ".`status`=" . USER_STATUS_VISIBLE_APPOINTMENTS . "
				) ";
        
        // Fire the event and process any changes to the query here
        $event = Application::getInstance()->container->get('dispatcher')->dispatch('search.query_where', new Event($query));
        $query = $event->data;

        if (!empty($city)) {
            $query .= sprintf(" AND `city`='%s' ", $city);
        }
        $query .= sprintf(" HAVING distance<%d ", sqlsave($distance));
        $query .= " ORDER BY `position_factor` DESC, distance ASC LIMIT " . $GLOBALS['CONFIG']['SEARCH_MAX_RESULTS'];
        $db->query($query);

        $providers = array();
        if ($db->nf() > 0) {
            while ($db->next_record()) {
                $data = $db->Record;

                $provider = new ProviderRow();
                $provider->setSearchResultData($data);
                $provider->setMedicalSpecialtyId($medicalSpecialtyId);
                $provider->setTreatmentTypeId($treatmentTypeId);
                $providers[] = $provider;
            }
        }
        
        return $providers;
    }

    static function searchDoctorsApi($location, $lat, $lng, $medicalSpecialtyId, $insuranceId, $date_start, $date_days, $page, $per_page)
    {
        $view = Application::getInstance()->getView();

        // sanitize data
        $errors = array();
        $form = array();
        $form["order"] = 1; // the api should not use the position_factor / check CONFIG['SEARCH_DISTANCE_ORDER_SQL']
        $form["location"] = sanitize_location($location);
        $form["lat"] = is_numeric($lat) ? $lat : '';
        $form["lng"] = is_numeric($lng) ? $lng : '';

        $form['medical_specialty_id'] = sanitize_option('int', $medicalSpecialtyId);
        if (!isset($form['medical_specialty_id']) || !MedicalSpecialty::idExists($form["medical_specialty_id"])) {
            $errors[] = 'error: medical_specialty_id';
        }
        $form['insurance_id'] = sanitize_option('int', $insuranceId);
        if (!isset($form['insurance_id']) || !isset($GLOBALS['CONFIG']['INSURANCES'][$form['insurance_id']])) {
            $errors[] = 'error: insurance_id';
        }

        $view->setRef('date_days', $date_days);
        if (!$date_start >= date('Y-m-d')) {
            $date_start = Calendar::getFirstPossibleDateTimeForSearch();
        }
        $date_start = Calendar::getDate($date_start);
        $view->setRef('date_start', $date_start);

        if (!empty($errors)) {
            return array('error' => $errors);
        }

        $valid_locations = self::getValidLocations($form);
        if (empty($valid_locations)) {
            return array(
                'status' => '404',
                'error'  => array('error:search location not found')
            );
        } elseif (is_array($valid_locations) && !empty($valid_locations)) {
            $num_valid_locations = sizeof($valid_locations);

            // IMMER NUR DEN ERSTEN NEHMEN!
            // PROBLEM bei "München": es werden 3 gefunden!?
            if ($num_valid_locations > 1) {
                $valid_locations = array($valid_locations[0]);
                $num_valid_locations = 1;
            }

            // if we have more than one result than show all results
            // and ask which is the right one
            if ($num_valid_locations > 1) {
                // set_form_search_locations($form, $valid_locations);
                return array(
                    'status' => '500',
                    'error'  => array('error:found more than one search location')
                );
            } else {
                $form = array_merge($form, $valid_locations[0]);
                // if we have a formatted address than show that one in the form
                if (isset($form['formatted_address_short']) && $form['formatted_address_short'] != '') {
                    $form['location'] = $form['formatted_address_short'];
                } else {
                    // try to get a "near to" address
                    // reverse geocoding / address lookup
                    $locations_approximate = GoogleMap::findLocationsByLatLng($form['lat'], $form['lng']);
                    if (is_array($locations_approximate) && isset($locations_approximate[0]['formatted_address_short_postal_code_locality'])) {
                        $form['location_approximate'] = $locations_approximate[0]['formatted_address_short_postal_code_locality'];
                    }
                }

                $data = array();
                $result = self::getSearchResults($form, $page, $per_page);

                if (!isset($result['search_result_total']) || $result['search_result_total'] == 0) {
                    return array(
                        'status'  => '404',
                        'error'   => array('error:found no results'),
                        'message' => 'Diese Kombination hat keine Ergebnisse. Bitte passen Sie Ihre Suche an.'
                    );
                } else {
                    $view->setRef('search_result_total', $result['search_result_total']);

                    $searchResultCacheTmpManager = SearchResultCacheTmpManager::getInstance();
                    $providers = $searchResultCacheTmpManager->getProviderRowsByFilters(
                        $result['providers'],
                        $date_start,
                        $date_days,
                        $insuranceId,
                        null,
                        1
                    );
                    $view->setRef('providerRows', $providers);

                    if (isset($result['filter'])) {
                        $view->setRef('filter', $result['filter']);
                    }
                    if (isset($result['pagination'])) {
                        $view->setRef('pagination', $result['pagination']);
                    }
                    if (isset($result['providers'])) {
                        $view->setRef('providers', $result['providers']);
                    }
                    if (isset($result['providers_start_index'])) {
                        $view->setRef('providers_start_index', $result['providers_start_index']);
                    }
                    if (isset($result['providers_end_index'])) {
                        $view->setRef('providers_end_index', $result['providers_end_index']);
                    }

                    // reformat the data for json output
                    foreach ($providers as $provider) {
                        // provider data
                        $user = $provider->getUser();
                        $location = $provider->getLocation();
                        $distance = $provider->getDistance();
                        $appointment_text = $provider->getNextAvailableAppointmentText();
                        $datetime = $provider->getNextAvailableAppointment()->getDateTime();
                        if (!$datetime) {
                            $datetime = null;
                        }
                        $provider_data = array(
                            'user_id'                         => $user->getId(),
                            'user_name'                       => $user->getName(),
                            'user_rating_average'             => $user->getRating('rating_average'),
                            'user_profile_image'              => array(
                                'sizes' => array(
                                    'web-list'    => $user->getProfileAssetUrl(ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED, true),
                                    'web-profile' => $user->getProfileAssetUrl(ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED, true),
                                    'ios-list'    => $user->getProfileAssetUrl(ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED, true),
                                    'ios-profile' => $user->getProfileAssetUrl(ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED, true),
                                )
                            ),
                            'location_id'                     => $location->getId(),
                            'location_profile_image'          => array(
                                'sizes' => array(
                                    'web-list'    => $location->getProfileAssetUrl(ASSET_GFX_SIZE_MEDIUM_LANDSCAPE_CROPPED, true),
                                    'web-profile' => $location->getProfileAssetUrl(ASSET_GFX_SIZE_MEDIUM_LANDSCAPE_CROPPED, true),
                                    'ios-list'    => $location->getProfileAssetUrl(ASSET_GFX_SIZE_MEDIUM_LANDSCAPE_CROPPED, true),
                                    'ios-profile' => $location->getProfileAssetUrl(ASSET_GFX_SIZE_MEDIUM_LANDSCAPE_CROPPED, true),
                                )
                            ),
                            'location_street'                 => $location->getStreet(),
                            'location_city'                   => $location->getCity(),
                            'location_zip'                    => $location->getZip(),
                            'location_lat'                    => $location->getLat(),
                            'location_lng'                    => $location->getLng(),
                            'distance'                        => $distance,
                            'next_available_appointment'      => $datetime,
                            'next_available_appointment_text' => $appointment_text,
                        );

                        $data[] = $provider_data;
                    }
                }
                $result_data = array(
                    'status' => '200',
                    'result' => $data
                );
                if (isset($form['location_approximate'])) {
                    $result_data['location_approximate'] = $form['location_approximate'];
                }

                return $result_data;
            }
        }

        return array('status' => '500');
    }

    /* *****************************************************************
     * API
     * ***************************************************************** /
    /**
     * search for doctors
     *
     * @param location
     * @param medicalSpecialtyId
     * @param insuranceId
     * @param date_start
     * @param date_days
     * @param page
     * @param per_page
     * 
     * @return array doctors
    */

    /**
     * ask google for the start location
     *
     * @param array
     *
     * @return array valid locations
     */
    public static function getValidLocations($form)
    {
        // if we have lat and lng allready given than we dont need to ask google
        if (isset($form["lat"]) && is_numeric($form["lat"]) && isset($form["lng"]) && is_numeric($form["lng"])) {
            $valid_locations[0] = $form;
        } else {
            // ask google if he knows this place or if there are more than one results
            // "," instead of " " between the searchparameters influences the result
            $location_string = $form["location"];
            // use the district if it is set
            if (isset($form['district_id']) && intval($form['district_id']) > 0) {
                $district = new District($form['district_id']);
                if ($district->isValid()) {
                    $location_string .= ' ' . $district->getName();
                }
            }

            // get the google-object
            $valid_locations = GoogleMap::findLocationsByAddress($location_string);
            if ($valid_locations == 0 || sizeof($valid_locations) == 0) {
                // nothing to find => save time and sql-queries
                return array();
            }
        }

        return $valid_locations;
    }
}
