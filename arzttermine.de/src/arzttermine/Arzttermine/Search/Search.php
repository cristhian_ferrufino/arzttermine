<?php

namespace Arzttermine\Search;

use Arzttermine\Application\Application;
use Arzttermine\Appointment\Appointment;
use Arzttermine\Appointment\Provider;
use Arzttermine\Calendar\Calendar;
use Arzttermine\Core\DateTime;
use Arzttermine\Event\Event;
use Arzttermine\Insurance\Insurance;
use Arzttermine\Location\Location;
use Arzttermine\MedicalSpecialty\MedicalSpecialty;
use Arzttermine\MedicalSpecialty\TreatmentType;
use Arzttermine\User\User;

/**
 * Find appointments using given filters. Currently, search parameters are member variables.
 * Note: The future plan is to move each type of "filter" into its own class and subsequently lazy load them as required.
 *
 * @package Arzttermine\Search
 */
class Search {

    /**
     * @var DateTime
     */
    protected $start;

    /**
     * @var DateTime
     */
    protected $end;

    /**
     * @var \DatePeriod
     */
    protected $date_range;

    /**
     * 10,6 representation of longitude
     * 
     * @var string
     */
    protected $lat = '0.0';

    /**
     * 10,6 representation of longitude
     * 
     * @var string
     */
    protected $lng = '0.0';

    /**
     * Allows us to search for a user directly
     * 
     * @var int
     */
    protected $user_id;

    /**
     * Allows us to search for a location directly
     *
     * @var int
     */
    protected $location_id;

    /**
     * Medical specialties. Compulsory.
     * 
     * @var int
     */
    protected $medical_specialty_id;

    /**
     * Optional treatment type(s)
     * 
     * @var int
     */
    protected $treatment_type_id;

    /**
     * @var int
     */
    protected $insurance_id;

    /**
     * Search radius in km
     * 
     * @var int
     */
    protected $radius;

    /**
     * For pagination
     * 
     * @var int
     */
    protected $page = 1;

    /**
     * For pagination
     * 
     * @var int
     */
    protected $per_page = 20;

    /**
     * @var int
     */
    protected $result_count = 0;
    
    /**
     * @var array
     */
    protected $last_result = array();

    /**
     * Initialise some compulsory filters
     */
    public function __construct()
    {
        $this->setDefaultParameters();
    }

    /**
     * @return self
     */
    public function setDefaultParameters()
    {
        global $CONFIG;
        
        // Default start and end dates. They're "now" (depending on time) and "now plus config-specified time period"
        $start = Calendar::getNextAvailableSearchDate();
        
        // The end is calculated in the method
        $this->setDateBounds($start);
        
        // Default lat/lng
        $latitude  = !empty($CONFIG['GEO_COORDINATE_DEFAULT']['lat']) ? $CONFIG['GEO_COORDINATE_DEFAULT']['lat'] : '0.0';
        $longitude = !empty($CONFIG['GEO_COORDINATE_DEFAULT']['lng']) ? $CONFIG['GEO_COORDINATE_DEFAULT']['lng'] : '0.0';
        $this
            ->setLat($latitude)
            ->setLng($longitude);

        // Default search radius
        $radius = !empty($CONFIG['SEARCH_DISTANCES_KM_DEFAULT']) ? $CONFIG['SEARCH_DISTANCES_KM_DEFAULT'] : 5; // Default to 5km. Hardcoded here.
        $this->setRadius($radius);

        // Default medical specialty
        $medical_specialty_id = !empty($CONFIG['SEARCH_MEDICAL_SPECIALTY_DEFAULT']) ? $CONFIG['SEARCH_MEDICAL_SPECIALTY_DEFAULT'] : 1; // Defaults to hardcoded Zahnarzt/Dentist = 1
        $this->setMedicalSpecialtyId($medical_specialty_id);

        // Default insurance. Let's default to a single insurance for now, as most of the system only supports searching for one
        // Note: The config default is an int, while the default here is an array of ints. The setter checks for both.
        $insurance = !empty($CONFIG['SEARCH_INSURANCE_DEFAULT']) ? $CONFIG['SEARCH_INSURANCE_DEFAULT'] : 2; // Public insurance hardcoded
        $this->setInsuranceId($insurance);
        
        // Default pagination
        $this->setPerPage($CONFIG['SEARCH_PAGINATION_NUM']);
        
        return $this;
    }

    /**
     * Get member variables for API calls, etc  
     * 
     * @return array
     */
    public function getParameters()
    {
        return array(
            'medical_specialty_id' => $this->getMedicalSpecialtyId(),
            'treatmenttype_id'     => $this->getTreatmentTypeId(),
            'insurance_id'         => $this->getInsuranceId(),
            'lat'                  => $this->getLat(),
            'lng'                  => $this->getLng(),
            'distance'             => $this->getRadius()
        );
    }

    /**
     * Run the search using the set parameters
     * 
     * @return self
     */
    public function findDoctorsAndLocations()
    {
        global $CONFIG;
        $results = array();

        if (!$this->validate()) {
            return $results;
        }

        //check cache
        //$cache = (extension_loaded('Memcached'))?new \Memcached():new \Memcache();
        //$cache->addServer($CONFIG['MEMCACHED_SERVER'], 11211);
        //$cachekey = "/doctors-and-locations/".$this->getLat()."/".$this->getLng()."/".$this->getTreatmentTypeId()."/".$this->getMedicalSpecialtyId()."/".$this->getRadius()."/".$GLOBALS['CONFIG']['SEARCH_MAX_RESULTS'];

        $docAndLocResult = false;//$cache->get($cachekey);
        if($docAndLocResult === false)
        {
            header('X-Search-Cache: miss');

            $db = Application::getInstance()->getSql();

            $distance
                = "(
                 6368 * SQRT(2*(1-cos(RADIANS(`lat`)) *
                 cos(RADIANS(" . $this->getLat() . ")) * (sin(RADIANS(`lng`)) *
                 sin(RADIANS(" . $this->getLng() . ")) + cos(RADIANS(`lng`)) *
                 cos(RADIANS(" . $this->getLng() . "))) - sin(RADIANS(`lat`)) * sin(RADIANS(" . $this->getLat() . "))))) AS distance ";

            $query
                = "SELECT
                        " . DB_TABLENAME_USERS . ".id as user_id,
                        " . DB_TABLENAME_USERS . ".status as user_status,
                        " . DB_TABLENAME_USERS . ".slug as user_slug,
                        " . DB_TABLENAME_USERS . ".title as user_title,
                        " . DB_TABLENAME_USERS . ".first_name as user_first_name,
                        " . DB_TABLENAME_USERS . ".last_name as user_last_name,
                        " . DB_TABLENAME_USERS . ".profile_asset_id as user_profile_asset_id,
                        " . DB_TABLENAME_USERS . ".profile_asset_filename as user_profile_asset_filename,
                        " . DB_TABLENAME_LOCATIONS . ".id as location_id,
                        " . DB_TABLENAME_LOCATIONS . ".status as location_status,
                        " . DB_TABLENAME_LOCATIONS . ".name as location_name,
                        " . DB_TABLENAME_LOCATIONS . ".integration_id as location_integration_id,
                        " . DB_TABLENAME_LOCATIONS . ".street as location_street,
                        " . DB_TABLENAME_LOCATIONS . ".city as location_city,
                        " . DB_TABLENAME_LOCATIONS . ".zip as location_zip,
                        " . DB_TABLENAME_LOCATIONS . ".lat as location_lat,
                        " . DB_TABLENAME_LOCATIONS . ".lng as location_lng,
                        " . $distance . "
                    FROM " . DB_TABLENAME_LOCATIONS . "
                            INNER JOIN `location_user_connections` ON `location_user_connections`.`location_id` = " . DB_TABLENAME_LOCATIONS . ".`id`
                            INNER JOIN " . DB_TABLENAME_USERS . " ON " . DB_TABLENAME_USERS . ".`id`= `location_user_connections`.`user_id` ";

            if ($ttid = $this->getTreatmentTypeId()) {
                $query .= "LEFT JOIN " . DB_TABLENAME_DOCTOR_TREATMENTTYPES . " ON " . DB_TABLENAME_DOCTOR_TREATMENTTYPES . ".`doctor_id`=" . DB_TABLENAME_USERS . ".`id` WHERE " . DB_TABLENAME_DOCTOR_TREATMENTTYPES . ".`treatment_type_id`=" . $ttid . " AND";
            } else {
                $query .= "WHERE ";
            }

            $query .= " FIND_IN_SET(" . $this->getMedicalSpecialtyId() . ", " . DB_TABLENAME_USERS . ".`medical_specialty_ids`)
                    AND FIND_IN_SET(" . $this->getMedicalSpecialtyId() . ", " . DB_TABLENAME_LOCATIONS . ".`medical_specialty_ids`) " .

                    // Visible Locations
                    "AND " . DB_TABLENAME_LOCATIONS . ".`status` IN (" . LOCATION_STATUS_VISIBLE . "," . LOCATION_STATUS_VISIBLE_APPOINTMENTS . ") " .

                    // Visible Users
                    "AND " . DB_TABLENAME_USERS . ".`status` IN (" . USER_STATUS_VISIBLE . "," . USER_STATUS_VISIBLE_APPOINTMENTS . ") ";

            $event = Application::getInstance()->container->get('dispatcher')->dispatch('search.query_where', new Event($query));
            $query = $event->data;

            $query .= sprintf(" HAVING distance<%d ", $this->getRadius());
            $query .= " ORDER BY `position_factor` DESC, distance ASC LIMIT " . $GLOBALS['CONFIG']['SEARCH_MAX_RESULTS'];
            $db->query($query);

            $docAndLocResult = $db->getResult();
/*            if($cache instanceof \Memcached) {
                $cache->set($cachekey, $docAndLocResult, 86400);
            } else {
                $cache->set($cachekey, $docAndLocResult, 0, 86400);
            }
*/
        } else {
            header('X-Search-Cache: hit');
        }


        if(is_array($docAndLocResult)) {
            // This is how many results we have
            $this->setResultCount(count($docAndLocResult));

            // ...BUT only save how many we need(!)
            $this->setLastResult(
                array_slice(
                    $docAndLocResult, // ALL results
                    ($this->getPage() - 1) * $this->getPerPage(), // Offset
                    $this->getPerPage() // How many results to show
                )
            );
        }
        
        return $this;
    }

    /**
     * Given an array of providers, find all appointments in the database that belong to them
     * 
     * @param Provider[] $providers
     *
     * @return array
     */
    public function findAppointments(array $providers = array())
    {
        global $CONFIG;
        $appointments = array();
        $doctors = array();
        $locations = array();
        $tt_condition = '';

        // If the providers are empty, assume we want to use the last result
        if (empty($providers)) {
            $providers = $this->asObjects();
        }
        
        // Compile a list of user and location IDs that we'll use in the query
        foreach ($providers as $provider) {
            $doctors[]   = $provider->getUserId();
            $locations[] = $provider->getLocationId();
        }

        // If there's a TreatmentType, add it to the query
        if ($tt = $this->getTreatmentTypeId()) {
            $tt_condition = sprintf(' AND FIND_IN_SET(%d,`treatmenttype_ids`)', $tt);
        }

        // If we have no doctors or locations, we have no appointments
        if (empty($doctors) || empty($locations)) {
            return $appointments;
        }

        $db = Application::getInstance()->getSql();

        // Manually request this from the table
        // @todo Move to class if possible
        $query = sprintf(
            'SELECT * ' .
            'FROM ' . DB_TABLENAME_APPOINTMENTS . ' ' .
            "WHERE `user_id` IN (%s)
                AND `location_id` IN (%s)
                AND `date`>='%s'
                AND `date`<='%s'
                AND FIND_IN_SET(%d,`insurance_ids`) %s",
            implode(',', array_unique($doctors)),
            implode(',', array_unique($locations)),
            $this->getStart()->getDateTime(),
            $this->getEnd()->getDateTime(),
            $this->getInsuranceId(),
            $tt_condition
        );

        $db->query($query);
        $results = $db->getResult();

        //sort by start_time
        $startTime = array();
        foreach ($results as $key => $row) {
            $startTime[$key]  = $row['start_time'];
        }
        array_multisort($startTime, SORT_ASC, $results);

        // Map the results to a usable array
        foreach ($results as $result) {
            $appointment = new Appointment();
            $appointment->hydrate($result);

            $appointments[$result['location_id']][$result['user_id']][] = $appointment;
        }

        if($appointments === false) {
            $appointments = array();
        }
        return $appointments;
    }

    /**
     * IMPORTANT
     * The following method assumes we have available the keys in the result from self::findDoctorsAndLocations
     * 
     * @todo Check that these data keys exist before using them
     * 
     * @return Provider[]
     */
    public function asObjects()
    {
        $results = array();
        $search_results = $this->getLastResult();
        $provider = new Provider();
        $provider->setDateRange($this->getStart(), $this->getEnd());
        
        foreach($search_results as $search_result) {
            // Don't add this row to results if there's bad data
            if (empty($search_result['user_id']) || empty($search_result['location_id'])) {
                continue;
            }
            
            // Clone the provider as we already have the data ready
            $result = clone $provider;

            $result
                ->setUser(new User($search_result['user_id']))
                ->setLocation(new Location($search_result['location_id']))
                ->setInsuranceId($this->getInsuranceId())
                ->setMedicalSpecialtyId($this->getMedicalSpecialtyId())
                ->setTreatmentTypeId($this->getTreatmentTypeId());
            
            // Distance from origin
            $result->setDistance($search_result['distance']);

            $results[] = $result;
        }
        
        return $results;
    }

    /**
     * Return our last result as an array (default storage)
     * 
     * @return array
     */
    public function asArray()
    {
        $result = $this->getLastResult();
        
        // Default to nothing if the result is corrupted
        if (!is_array($result)) {
            return array();
        }
        
        return $result;
    }

    /**
     * Clear all search parameters and reset to default
     * 
     * @return self
     */
    public function reset()
    {
        return $this->setDefaultParameters();
    }

    /**
     * @return int
     */
    public function getResultCount()
    {
        return intval($this->result_count);
    }

    /**
     * @param int $result_count
     *
     * @return self
     */
    public function setResultCount($result_count)
    {
        $this->result_count = intval($result_count);

        /**
         * recalc the page based on the number of results
         */
        $this->sanitizePage();

        return $this;
    }
    
    /**
     * @return array
     */
    public function getLastResult()
    {
        // Force array
        if (!is_array($this->last_result)) {
            $this->last_result = array($this->last_result);
        }
        
        return $this->last_result;
    }

    /**
     * @param array $last_result
     *
     * @return self
     */
    public function setLastResult(array $last_result = array())
    {
        $this->last_result = $last_result;

        return $this;
    }

    /**
     * @return string
     */
    public function getLat()
    {
        return strval($this->lat);
    }

    /**
     * Force a decimal value
     * 
     * @param string $lat
     *
     * @return self
     */
    public function setLat($lat)
    {
        $this->lat = sprintf('%f', $lat);

        return $this;
    }

    /**
     * @param string $lat
     *
     * @return self
     */
    public function setLatitude($lat)
    {
        return $this->setLat($lat);
    }

    /**
     * @return string
     */
    public function getLng()
    {
        return strval($this->lng);
    }

    /**
     * Force a decimal value
     * 
     * @param string $lng
     *
     * @return self
     */
    public function setLng($lng)
    {
        $this->lng = sprintf('%f', $lng);

        return $this;
    }

    /**
     * Set the longitude and latitude in one call
     * 
     * @param array $coords Should include latitude and longitude in short- or longhand
     * 
     * @return self
     */
    public function setSearchOrigin(array $coords)
    {
        // Try for the shorthand, then fallback to longhand, then just fail
        $lat = !empty($coords['lat']) ? $coords['lat'] : (!empty($coords['latitude'])  ? $coords['latitude']  : '');
        $lng = !empty($coords['lng']) ? $coords['lng'] : (!empty($coords['longitude']) ? $coords['longitude'] : '');
        
        $this
            ->setLat($lat)
            ->setLng($lng);
        
        return $this;
    }

    /**
     * @return array
     */
    public function getSearchOrigin()
    {
        return array('lat' => $this->getLat(), 'lng' => $this->getLng());
    }

    /**
     * @param string $lng
     *
     * @return self
     */
    public function setLongitude($lng)
    {
        return $this->setLng($lng);
    }

    /**
     * Get the number of days between start and end (inclusive)
     *
     * @return int
     */
    public function getDayCount()
    {
        return count($this->getDatesInRange(true));
    }

    /**
     * Returns an array with days that are between two dates/datetimes
     *
     * @param bool $with_filter
     *
     * @return DateTime[]
     */
    public function getDatesInRange($with_filter = false)
    {
        $dates = array();

        $date_range = $this->getDateRange();

        /** @var DateTime $date */
        foreach ($date_range as $date) {
            // "N" gives us an int with Monday = 1 to Sunday = 7
            if ($with_filter && $date->format('N') > 5) {
                continue;
            }
            // @todo Fix casting :/
            $dates[] = new DateTime($date->format('Y-m-d H:i:s'));
        }
        return $dates;
    }

    /**
     * @return \DatePeriod
     */
    public function getDateRange()
    {
        return $this->date_range;
    }

    /**
     * @param \DatePeriod $date_range
     *
     * @return self
     */
    public function setDateRange(\DatePeriod $date_range)
    {
        $this->date_range = $date_range;

        return $this;
    }

    /**
     * NOTE: Not necessarily connected to start and end
     * 
     * @param DateTime|string $start
     * @param DateTime|string $end
     * 
     * @return self
     */
    public function setDateBounds($start, $end = null)
    {
        global $CONFIG;
        
        if (!($start instanceof DateTime)) {
            $start = new DateTime($start);
        }
        
        // Default the range end to the config search period
        if (empty($end)) {
            $end = clone $start;
            $end->addDays($CONFIG['SEARCH_CALENDAR_MAX_DAYS']);
        } elseif (!($end instanceof DateTime)) {
            $end = new DateTime($end);
        }

        /**
         * add the time component to the end so that DatePeriod counts that day too
         */
        $this
            ->setDateRange(new \DatePeriod(
                $start,
                new \DateInterval('P1D'),
                $end->setTime('23','59','59')
            ))
            ->setStart($start)
            ->setEnd($end);

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getStart()
    {
        if (!($this->start instanceof DateTime)) {
            $this->setStart($this->start);
        }

        return $this->start;
    }

    /**
     * @param string|DateTime $start
     *
     * @return self
     */
    public function setStart($start)
    {
        if (!($start instanceof DateTime)) {
            $start = new DateTime($start);
        }

        $this->start = $start;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getEnd()
    {
        if (!($this->end instanceof DateTime)) {
            $this->setEnd($this->end);
        }

        return $this->end;
    }

    /**
     * @param string|DateTime $end
     *
     * @return self
     */
    public function setEnd($end)
    {
        if (!($end instanceof DateTime)) {
            $end = new DateTime($end);
        }

        $this->end = $end;

        return $this;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return intval($this->user_id);
    }

    /**
     * @param int $user_id
     *
     * @return self
     */
    public function setUserId($user_id)
    {
        $this->user_id = intval($user_id);

        return $this;
    }

    /**
     * @param User $user
     *
     * @return self
     */
    public function setUser(User $user)
    {
        $this->setUserId($user->getId());
        
        return $this;
    }

    /**
     * @return Location
     */
    public function getLocation()
    {
        return new Location($this->getLocationId());
    }

    /**
     * @return int
     */
    public function getLocationId()
    {
        return intval($this->location_id);
    }

    /**
     * @param int $location_id
     *
     * @return self
     */
    public function setLocationId($location_id)
    {
        $this->location_id = intval($location_id);

        return $this;
    }

    /**
     * @param Location $location
     *
     * @return self
     */
    public function setLocation(Location $location)
    {
        $this->setLocationId($location->getId());
        
        return $this;
    }

    /**
     * Return radius in whole km
     * 
     * @return int
     */
    public function getRadius()
    {
        return intval($this->radius);
    }

    /**
     * This only takes whole integers (i.e. floats are truncated)
     * 
     * @param int $radius
     *
     * @return self
     */
    public function setRadius($radius)
    {
        $this->radius = intval($radius);

        return $this;
    }

    /**
     * @return int
     */
    public function getMedicalSpecialtyId()
    {
        return intval($this->medical_specialty_id);
    }

    /**
     * @param int $medical_specialty_id
     *
     * @return self
     */
    public function setMedicalSpecialtyId($medical_specialty_id)
    {
        $this->medical_specialty_id = intval($medical_specialty_id);

        return $this;
    }

    /**
     * @return MedicalSpecialty
     */
    public function getMedicalSpecialty()
    {
        return new MedicalSpecialty($this->getMedicalSpecialtyId());
    }

    /**
     * @param MedicalSpecialty|int $medical_specialty
     *
     * @return self
     */
    public function setMedicalSpecialty($medical_specialty)
    {
        if ($medical_specialty instanceof MedicalSpecialty) {
            $medical_specialty = $medical_specialty->getId();
        }
        
        return $this->setMedicalSpecialtyId($medical_specialty);
    }

    /**
     * @return int
     */
    public function getTreatmentTypeId()
    {
        return intval($this->treatment_type_id);
    }

    /**
     * @param int $treatment_type_id
     *
     * @return self
     */
    public function setTreatmentTypeId($treatment_type_id)
    {
        $this->treatment_type_id = intval($treatment_type_id);

        return $this;
    }

    /**
     * @return TreatmentType
     */
    public function getTreatmentType()
    {
        return new TreatmentType($this->getTreatmentTypeId());
    }

    /**
     * @param TreatmentType|int $treatment_type
     *
     * @return self
     */
    public function setTreatmentType($treatment_type)
    {
        if ($treatment_type instanceof TreatmentType) {
            $treatment_type = $treatment_type->getId();
        }

        return $this->setTreatmentTypeId($treatment_type);
    }

    /**
     * @return int
     */
    public function getInsuranceId()
    {
        return intval($this->insurance_id);
    }

    /**
     * @return Insurance
     */
    public function getInsurance()
    {
        return new Insurance($this->getInsuranceId());
    }

    /**
     * @param int $insurance_id
     *
     * @return self
     */
    public function setInsuranceId($insurance_id)
    {
        $this->insurance_id = intval($insurance_id);

        return $this;
    }

    /**
     * @param Insurance|int $insurance
     * 
     * @return self
     */
    public function setInsurance($insurance)
    {
        if ($insurance instanceof Insurance) {
            $insurance = $insurance->getId();
        }
        
        $this->setInsuranceId($insurance);
        
        return $this;
    }

    /**
     * @return int
     */
    public function getPerPage()
    {
        return intval($this->per_page);
    }

    /**
     * @param int $per_page
     *
     * @return self
     */
    public function setPerPage($per_page)
    {
        global $CONFIG;
        
        $per_page = intval($per_page);
        
        if ($per_page < 1) {
            $per_page = 1;
        }
        
        if ($per_page > $CONFIG['SEARCH_MAX_RESULTS']) {
            $per_page = $CONFIG['SEARCH_MAX_RESULTS'];
        }
        
        $this->per_page = $per_page;

        return $this;
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return intval($this->page);
    }

    /**
     * @param int $page
     *
     * @return self
     */
    public function setPage($page)
    {
        $page = intval($page);
        
        if ($page < 1) {
            $page = 1;
        }
        
        $this->page = $page;

        return $this;
    }

    /**
     * Sanitizes the page
     * 
     * @return void
     */
    public function sanitizePage()
    {
        $highest_page = ceil($this->getResultCount() / $this->getPerPage());

        if ($this->getPage() > $highest_page) {
            $this->setPage($highest_page);
        }
    }

    /**
     * @return int
     */
    public function getStartIndex() {
        return ($this->getPage() - 1) * $this->getPerPage();
    }

    /**
     * Validate the current settings for search
     * 
     * @return bool
     */
    public function validate()
    {
        $is_valid = true;
        
        $lat = $this->getLat();
        $lng = $this->getLng();
        
        // The only edge case here is if the lat/lng is 0.0, which is in the middle of the Atlantic anyway...
        if (empty($lat) || empty($lng)) {
            $is_valid = false;
        }
        
        // @todo: Further validations pre-search here
        
        return $is_valid;
    }
}
