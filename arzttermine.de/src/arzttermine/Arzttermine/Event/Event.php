<?php

namespace Arzttermine\Event;

/**
 * Arzttermine Event for the event dispatcher.
 * Ideally, this class would be extended whereever possible to create more concrete implementations of $data
 *
 * @package Arzttermine\Event
 */
class Event extends \Symfony\Component\EventDispatcher\Event {

    /**
     * @var mixed
     */
    public $data;

    /**
     * @param mixed $data
     */
    public function __construct($data = null)
    {
        $this->data = $data;
    }
    
}