<?php

namespace Arzttermine\Referral;

class PatientReferral extends Referral {

    /**
     * @var string
     */
    protected $tablename = 'patient_referrals';

    /**
     * @var string
     */
    protected $first_name;

    /**
     * @var string
     */
    protected $last_name;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var array
     */
    protected $all_keys
        = array(
            'id',
            'first_name',
            'last_name',
            'email',
            'referral_code',
            'booking_id',
            'medical_specialty_id',
            'type',
            'data',
            'restrictions',
            'price_type',
            'status'
        );

    /**
     * @var array
     */
    protected $secure_keys
        = array(
            'id',
            'first_name',
            'last_name',
            'email',
            'referral_code',
            'booking_id',
            'medical_specialty_id',
            'type',
            'data',
            'restrictions',
            'price_type',
            'status'
        );

    /**
     * @param string $first_name
     *
     * @return self
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @param string $last_name
     *
     * @return self
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @param string $email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
}