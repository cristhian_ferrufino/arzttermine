<?php

namespace Arzttermine\Referral;

use Arzttermine\Booking\Booking;
use Arzttermine\Core\Basic;

class ReferralInvitation extends Basic {

    const SOURCE_EMAIL = 0;
    const SOURCE_FACEBOOK = 1;
    const SOURCE_GPLUS = 2;
    const SOURCE_TWITTER = 3;
    const SOURCE_SMS = 4;

    /**
     * @var string
     */
    protected $tablename = 'referral_invitations';
    
    /**
     * @var string
     */
    protected $referral_code = '';

    /**
     * @var int
     */
    protected $booking_id;

    /**
     * @var string
     */
    protected $sent_to = '';

    /**
     * @var string
     */
    protected $sent_at = '';

    /**
     * @var int
     */
    protected $via;

    /**
     * @var string
     */
    protected $code_used = '';

    /**
     * @var int
     */
    protected $shared_count;

    /**
     * @var array
     */
    protected $all_keys
        = array(
            'id',
            'referral_code',
            'sent_to',
            'sent_at',
            'via',
            'code_used',
            'shared_count'
        );

    /**
     * @var array
     */
    protected $secure_keys
        = array(
            'referral_code',
            'sent_to',
            'sent_at',
            'via',
            'code_used'
        );

    /**
     * @param int $booking_id
     *
     * @return self
     */
    public function setBookingId($booking_id)
    {
        $this->booking_id = $booking_id;

        return $this;
    }

    /**
     * @return int
     */
    public function getBookingId()
    {
        return $this->booking_id;
    }

    /**
     * @return Booking
     */
    public function getBooking()
    {
        return new Booking($this->getBookingId());
    }

    /**
     * @param string $code_used
     *
     * @return self
     */
    public function setCodeUsed($code_used)
    {
        $this->code_used = trim($code_used);

        return $this;
    }

    /**
     * @return string
     */
    public function getCodeUsed()
    {
        return $this->code_used;
    }

    /**
     * @param string $referral_code
     *
     * @return self
     */
    public function setReferralCode($referral_code)
    {
        $this->referral_code = trim($referral_code);

        return $this;
    }

    /**
     * @return string
     */
    public function getReferralCode()
    {
        return $this->referral_code;
    }

    /**
     * @param string $sent_at
     *
     * @return self
     */
    public function setSentAt($sent_at)
    {
        $this->sent_at = $sent_at;

        return $this;
    }

    /**
     * @return string
     */
    public function getSentAt()
    {
        return $this->sent_at;
    }

    /**
     * @param string $sent_to
     *
     * @return self
     */
    public function setSentTo($sent_to)
    {
        $this->sent_to = $sent_to;

        return $this;
    }

    /**
     * @return string
     */
    public function getSentTo()
    {
        return $this->sent_to;
    }

    /**
     * @param int $shared_count
     *
     * @return self
     */
    public function setSharedCount($shared_count)
    {
        $this->shared_count = $shared_count;

        return $this;
    }

    /**
     * @return int
     */
    public function getSharedCount()
    {
        return $this->shared_count;
    }

    /**
     * @param int $via
     *
     * @return self
     */
    public function setVia($via)
    {
        $this->via = $via;

        return $this;
    }

    /**
     * @return int
     */
    public function getVia()
    {
        return $this->via;
    }

    /**
     * Only allow platforms we have previously defined.
     * NOTE: Requires all class constant values be unique
     * 
     * @param int $source_platform
     * 
     * @return bool
     */
    public static function isSourceValid($source_platform)
    {
        $_class = new \ReflectionClass(__CLASS__);
        $platforms = $_class->getConstants();
        $platforms = array_flip($platforms);
        
        return in_array($source_platform, $platforms);
    }
}