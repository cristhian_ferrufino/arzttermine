<?php

namespace Arzttermine\Referral;

use Arzttermine\Booking\Booking;
use Arzttermine\Core\Basic;
use Arzttermine\MedicalSpecialty\MedicalSpecialty;

class Referral extends Basic {

    const STATUS_NEW                            = 0;
    const STATUS_IN_PROCESSING                  = 1;
    const STATUS_APPOINTMENT_COMPLETION_FAILED  = 2;
    const STATUS_APPOINTMENT_COMPLETION_UNABLE  = 3;
    const STATUS_APPOINTMENT_COMPLETION_PROBLEM = 4;
    const STATUS_APPOINTMENT_COMPLETED          = 5;
    const STATUS_REWARD_SENT                    = 6;
    const STATUS_REWARD_DENIED                  = 7;
    
    const TYPE_SYSTEM = 0;
    const TYPE_USER = 1;

    /**
     * @var string
     */
    protected $tablename = 'referrals';

    /**
     * @var string
     */
    protected $referral_code;

    /**
     * @var string
     */
    protected $first_name;

    /**
     * @var string
     */
    protected $last_name;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var int
     */
    protected $booking_id;

    /**
     * @var int
     */
    protected $medical_specialty_id;

    /**
     * @var int
     */
    protected $type = self::TYPE_SYSTEM;

    /**
     * @var string
     */
    protected $data;

    /**
     * JSON encoded string
     * 
     * @var string
     */
    protected $restrictions = '[]';

    /**
     * The type of referral object this is. For example, an Amazon voucher, user referral, etc
     * See $CONFIG for REFERRAL_PRICE_TYPES
     * 
     * @var string
     */
    protected $price_type = '';

    /**
     * @var int
     */
    protected $status;

    /**
     * @var array
     */
    protected $all_keys
        = array(
            "id",
            "referral_code",
            "booking_id",
            "medical_specialty_id",
            "type",
            "data",
            "restrictions",
            "price_type",
            "status"
        );

    /**
     * @var array
     */
    protected $secure_keys
        = array(
            "id",
            "referral_code",
            "booking_id",
            "medical_specialty_id",
            "type",
            "data",
            "restrictions",
            "price_type",
            "status"
        );

    /**
     * @var array
     */
    protected $last_error = array();

    /**
     * @param int $type
     *
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $booking_id
     *
     * @return self
     */
    public function setBookingId($booking_id)
    {
        $this->booking_id = $booking_id;

        return $this;
    }

    /**
     * @return int
     */
    public function getBookingId()
    {
        return $this->booking_id;
    }

    /**
     * Return actual booking object linked to this referral
     *
     * @return Booking
     */
    public function getBooking()
    {
        return new Booking($this->getBookingId());
    }

    /**
     * Return actual booking object linked to this referral
     *
     * @return MedicalSpecialty
     */
    public function getMedicalSpecialty()
    {
        return new MedicalSpecialty($this->getMedicalSpecialtyId());
    }

    /**
     * @param string $data
     *
     * @return self
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param int $medical_specialty_id
     *
     * @return self
     */
    public function setMedicalSpecialtyId($medical_specialty_id)
    {
        $this->medical_specialty_id = $medical_specialty_id;

        return $this;
    }

    /**
     * @return int
     */
    public function getMedicalSpecialtyId()
    {
        return $this->medical_specialty_id;
    }

    /**
     * @param string $price_type
     *
     * @return self
     */
    public function setPriceType($price_type)
    {
        $this->price_type = $price_type;

        return $this;
    }

    /**
     * @return string
     */
    public function getPriceType()
    {
        return $this->price_type;
    }

    /**
     * @param string $referral_code
     *
     * @return self
     */
    public function setReferralCode($referral_code)
    {
        $this->referral_code = $referral_code;

        return $this;
    }

    /**
     * @return string
     */
    public function getReferralCode()
    {
        return $this->referral_code;
    }

    /**
     * @param array $restrictions
     *
     * @return self
     */
    public function setRestrictions(array $restrictions)
    {
        $this->restrictions = json_encode($restrictions);

        return $this;
    }

    /**
     * @return string
     */
    public function getRestrictions()
    {
        return json_decode($this->restrictions, true);
    }

    /**
     * @param string $status
     *
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return array
     */
    public function getLastError()
    {
        return $this->last_error;
    }
}