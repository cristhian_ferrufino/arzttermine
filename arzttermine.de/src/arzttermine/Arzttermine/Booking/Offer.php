<?php

namespace Arzttermine\Booking;

use Arzttermine\Application\Application;
use Arzttermine\Calendar\Calendar;
use Arzttermine\Core\Basic;
use Arzttermine\Location\Location;
use Arzttermine\Mail\Mailing;
use Arzttermine\User\User;
use DateTime;

class Offer extends Basic {

    /**
     * @var string
     */
    protected $tablename = DB_TABLENAME_BOOKING_OFFERS;

    /**
     * @var string
     */
    protected $slug;

    /**
     * @var string
     */
    protected $status;

    /**
     * @var int
     */
    protected $booking_id;

    /**
     * @var int
     */
    protected $user_id;

    /**
     * @var int
     */
    protected $location_id;

    /**
     * @var string
     */
    protected $appointment_start_at;

    /**
     * This value comes from Salesforce. Indicates the booking was updated in SF and not from an offer
     * 
     * @var bool
     */
    protected $booked_in_practice;

    /**
     * @var string
     */
    protected $expires_at;

    /**
     * @var string
     */
    protected $mail_send_at;

    /**
     * @var string
     */
    protected $comment_intern;

    /**
     * @var string
     */
    protected $comment_patient;

    /**
     * @var string
     */
    protected $answered_at;

    /**
     * @var string
     */
    protected $created_at;

    /**
     * @var string
     */
    protected $created_by;

    /**
     * @var string
     */
    protected $updated_at;

    /**
     * @var string
     */
    protected $updated_by;

    /**
     * @var string
     */
    protected $url = '';

    /**
     * @var array
     */
    protected $all_keys
        = array(
            "id",
            "slug",
            "status",
            "booking_id",
            "user_id",
            "location_id",
            "appointment_start_at",
            "booked_in_practice",
            "expires_at",
            "mail_send_at",
            "comment_intern",
            "comment_patient",
            "answered_at",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by"
        );

    /**
     * @var array
     */
    protected $secure_keys
        = array(
            "id",
            "slug",
            "status",
            "user_id",
            "location_id",
            "appointment_start_at",
            "booked_in_practice",
            "expires_at",
            "mail_send_at",
            "comment_intern",
            "comment_patient",
            "answered_at",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by"
        );

    /**
     * Offer is new
     */
    const STATUS_NEW = 1;

    /**
     * Offer is awaiting reply
     */
    const STATUS_PENDING = 2;

    /**
     * Offer has passed it's acceptance date
     */
    const STATUS_EXPIRED = 3;

    /**
     * Offer has been accepted
     */
    const STATUS_ACCEPTED = 4;

    /**
     * Patient does not want the offer (or any offer)
     */
    const STATUS_REJECTED = 5;

    /**
     * Another appointment for the same booking was accepted
     */
    const STATUS_ANSWERED = 6;

    /**
     * @var array
     */
    private static $STATUS_TEXTS
        = array(
            self::STATUS_NEW      => 'Neu',
            self::STATUS_PENDING  => 'Auf Antwort warten',
            self::STATUS_ACCEPTED => 'Akzeptiert',
            self::STATUS_REJECTED => 'Abgeleht',
            self::STATUS_EXPIRED  => 'Abgelaufen',
            self::STATUS_ANSWERED => 'Anderen Termin gewählt',
        );

    /**
     * Calc some vars
     *
     * @return bool
     */
    public function calcVars()
    {
        $this->setUrl('/terminantwort/' . $this->slug);

        return true;
    }

    /**
     * @return string
     */
    public function getAdminEditUrl()
    {
        return '/administration/booking-offers?action=edit&id=' . $this->getId();
    }

    /**
     * @param string $url
     *
     * @return self
     */
    public function setUrl($url)
    {
        $this->url = trim($url);

        return $this;
    }

    /**
     * @param bool $absolute
     * @param bool $secure
     *
     * @return string
     */
    public function getUrl($absolute = false, $secure = false)
    {
        return ($absolute ? ($secure ? $GLOBALS['CONFIG']['URL_HTTPS_LIVE'] : $GLOBALS['CONFIG']['URL_HTTP_LIVE']) : '') . $this->url;
    }
    
    /**
     * @param string $answered_at
     *
     * @return self
     */
    public function setAnsweredAt($answered_at)
    {
        $this->answered_at = $answered_at;

        return $this;
    }

    /**
     * @return string
     */
    public function getAnsweredAt()
    {
        return $this->answered_at;
    }

    /**
     * @param string $appointment_start_at
     *
     * @return self
     */
    public function setAppointmentStartAt($appointment_start_at)
    {
        $this->appointment_start_at = $appointment_start_at;

        return $this;
    }

    /**
     * @return string
     */
    public function getAppointmentStartAt()
    {
        return $this->appointment_start_at;
    }

    /**
     * @param string $created_at
     *
     * @return self
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param string $created_by
     *
     * @return self
     */
    public function setCreatedBy($created_by)
    {
        $this->created_by = $created_by;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * @param string $expires_at
     *
     * @return self
     */
    public function setExpiresAt($expires_at)
    {
        $this->expires_at = $expires_at;

        return $this;
    }

    /**
     * @return string
     */
    public function getExpiresAt()
    {
        return $this->expires_at;
    }

    /**
     * @param int $location_id
     *
     * @return self
     */
    public function setLocationId($location_id)
    {
        $this->location_id = $location_id;

        return $this;
    }

    /**
     * @return int
     */
    public function getLocationId()
    {
        return $this->location_id;
    }

    /**
     * @param string $mail_send_at
     *
     * @return self
     */
    public function setMailSendAt($mail_send_at)
    {
        $this->mail_send_at = $mail_send_at;

        return $this;
    }

    /**
     * @return string
     */
    public function getMailSendAt()
    {
        return $this->mail_send_at;
    }

    /**
     * @param string $updated_at
     *
     * @return self
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param string $updated_by
     *
     * @return self
     */
    public function setUpdatedBy($updated_by)
    {
        $this->updated_by = $updated_by;

        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedBy()
    {
        return $this->updated_by;
    }

    /**
     * @param int $user_id
     *
     * @return self
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Returns a new Slug Code
     *
     * @return string
     */
    private static function getNewSlug()
    {
        return substr(md5(uniqid()), 0, 12);
    }

    /**
     * Returns the slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $status
     *
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = (int)$status;

        return $this;
    }

    /**
     * Returns the status id
     * If a status text is given then returns the id.
     * If no param is given it returns the objects status id
     *
     * @param string $status_text
     *
     * @return int
     */
    public function getStatus($status_text = '')
    {
        if (empty($status_text)) {
            return $this->status;
        } else {
            $flipped_statuses = array_flip(self::$STATUS_TEXTS);
            if (isset($flipped_statuses[$status_text])) {
                return $flipped_statuses[$status_text];
            }
        }

        return null;
    }

    /**
     * Returns the status text
     * Can be used as static method as well as object method
     *
     * @param int $status
     *
     * @return string
     */
    public function getStatusText($status = null)
    {
        return !empty($status) ? self::$STATUS_TEXTS[$status] : self::$STATUS_TEXTS[$this->status];
    }

    /**
     * Returns the status text array with all available statuses
     *
     * @return array
     */
    public static function getStatusTextArray()
    {
        return self::$STATUS_TEXTS;
    }

    /**
     * Returns the booking_id
     *
     * @return int
     */
    public function getBookingId()
    {
        return $this->booking_id;
    }

    /**
     * @param int $booking_id
     *
     * @return self
     */
    public function setBookingId($booking_id)
    {
        $this->booking_id = intval($booking_id);

        return $this;
    }

    /**
     * @return Booking
     */
    public function getBooking()
    {
        return new Booking($this->getBookingId());
    }

    /**
     * Returns the user
     *
     * @return User
     */
    public function getUser()
    {
        return new User($this->user_id);
    }

    /**
     * Returns the location
     *
     * @return Location
     */
    public function getLocation()
    {
        return new Location($this->location_id);
    }

    /**
     * Returns the appointment object
     *
     * @return Appointment
     */
    public function getAppointment()
    {
        return new Appointment($this->appointment_start_at);
    }

    /**
     * Checks if the offer was already booked in the practice.
     *
     * @return bool
     */
    public function isBookedInPractice()
    {
        return (bool)$this->booked_in_practice;
    }

    /**
     * Returns the datetime
     *
     * @param string $format
     *
     * @return DateTime string
     */
    public function getCreatedAtFormat($format = 'Y-m-d')
    {
        // dirty hack, but I want the german weekday
        if ($format == 'w d.m.Y, H:i') {
            return Calendar::getWeekdayDateTimeText($this->created_at, true);
        }

        return date($format, strtotime($this->created_at));
    }

    /**
     * Returns the internal comment
     *
     * @return string
     */
    public function getCommentIntern()
    {
        return $this->comment_intern;
    }

    /**
     * Returns the patient comment
     *
     * @return string
     */
    public function getCommentPatient()
    {
        return $this->comment_patient;
    }

    /**
     * Checks if the offer has expired
     *
     * @return bool
     */
    public function hasExpired()
    {
        if ($this->status == self::STATUS_EXPIRED) {
            return true;
        }

        if ($this->expires_at == '0000-00-00 00:00:00' || empty($this->expires_at)) {
            return false;
        }

        $now = new DateTime("now");
        $expiresAt = new DateTime($this->expires_at);
        return ($now > $expiresAt);
    }

    /**
     * Checks if the offer has been answered already
     *
     * @return bool
     */
    public function hasBeenAnswered()
    {
        return (
            $this->status == self::STATUS_ACCEPTED
            ||
            $this->status == self::STATUS_REJECTED
            ||
            $this->status == self::STATUS_ANSWERED
            ||
            $this->status == self::STATUS_EXPIRED
        );
    }

    /**
     * Checks if the offer is pending (open) or not
     */
    public function isOpen($booking_id = null)
    {
        if (!$booking_id) {
            $booking_id = $this->getBookingId();
        }

        $this->load($booking_id, 'booking_id');

        if ($this->getBooking()->isValid()) {
            return (bool)$this->count('booking_id=' . $booking_id . ' AND (status=' . self::STATUS_NEW . ' OR status=' . self::STATUS_PENDING . ');');
        }

        return false;
    }

    /**
     * Stores a new bookingOffer in the DB
     *
     * @param array $data
     *
     * @return Offer|bool
     */
    public static function create($data)
    {
        global $app, $profile;

        if (empty($data['created_at'])) {
            $data['created_at'] = $app->now('datetime');
        }

        if (empty($data['created_by'])) {
            $data['created_by'] = $profile->id;
        }

        if (empty($data['status'])) {
            $data['status'] = self::STATUS_NEW;
        }

        if (empty($data['slug'])) {
            $data['slug'] = Offer::getNewSlug();
        }

        $bookingOffer = new Offer();
        $new_id = $bookingOffer->saveNew($data);
        $bookingOffer->load($new_id);

        if (!$bookingOffer->isValid()) {
            return new self();
        }

        return $bookingOffer;
    }

    /**
     * Updates the status of this object and update all other booking offers
     * for the same booking with the status STATUS_ANSWERED
     *
     * @return bool
     */
    public function updateStatusAccepted()
    {

        $this->answered_at = Application::getInstance()->now('datetime');
        $this->status = Offer::STATUS_ACCEPTED;

        if (!$this->save(array('status', 'answered_at'))) {
            return false;
        }

        $this->updateOffers();

        return true;
    }

    /**
     * Update a group of offers attached to the current booking id
     *
     * @param int $status
     * @param int $match
     */
    public function updateOffers($status = self::STATUS_ANSWERED, $match = self::STATUS_PENDING)
    {
        $bookingOffers = $this->findObjects('booking_id=' . $this->booking_id . ' AND status=' . $match);

        if (!empty($bookingOffers)) {
            /** @var Offer[] $bookingOffers */
            foreach ($bookingOffers as $bookingOffer) {
                $bookingOffer->answered_at = Application::getInstance()->now('datetime');
                $bookingOffer->status = $status;
                $bookingOffer->save(array('status', 'answered_at'));
            }
        }
    }

    /**
     * Sends the booking offer(s) email to the patient.
     * Send only unanswered offers (by its status).
     *
     * @param Booking
     *
     * @return bool
     */
    public static function sendBookingOffersEmail(Booking $booking)
    {
        $app = Application::getInstance();
        $view = $app->getView();

        if (!$booking->isValid()) {
            return false;
        }

        $bookingOffer = new self();
        $bookingOffers = $bookingOffer->findObjects(
                                      'booking_id=' . $booking->getId() . '
				AND
				(status=' . self::STATUS_NEW . ' OR status=' . self::STATUS_PENDING . ')
				ORDER BY appointment_start_at ASC'
        );

        if (empty($bookingOffers)) {
            return false;
        }

        // Combine template content with CMS content
        $view->setRef('booking_offers', $bookingOffers);
        $view->setRef('booking', $booking);
        $bookingOffersHtml = $view->fetch('booking-offers/mail-booking-offers.tpl');

        $user = $booking->getUser();
        if (!is_object($user) || !$user->isValid()) {
            return false;
        }

        $location = $booking->getLocation();
        if (!is_object($location) || !$location->isValid()) {
            return false;
        }

        // Send mail to patient
        $mailing = new Mailing();
        $mailing->type = MAILING_TYPE_HTML;
        $mailing->content_filename = '/mailing/patients/booking-offer-new.html';
        $mailing->from = $GLOBALS['CONFIG']["BOOKINGS_EMAIL_ADDRESS"];
        $mailing->from_name = $GLOBALS['CONFIG']["BOOKINGS_EMAIL_NAME"];
        $mailing->data = array();
        $mailing->data['domain'] = $GLOBALS['CONFIG']['DOMAIN'];
        $mailing->data['booking_salutation_full_name'] = $booking->getSalutationName();
        $mailing->data['booking_appointment_date_text'] = $bookingOffer->getAppointment()->getWeekdayDateText();
        $mailing->data['booking_appointment_time_text'] = $bookingOffer->getAppointment()->getTimeText();
        $mailing->data['user_name'] = $user->getName();
        $mailing->data['location_name'] = $location->getName();
        $mailing->data['location_address'] = $location->getAddress(" - ");
        $mailing->data['booking_offers_html'] = $bookingOffersHtml;
        $mailing->addAddressTo($booking->getEmail());
        $result = $mailing->send();

        if ($result) {
            /** @var Offer[] $bookingOffers */
            foreach ($bookingOffers as $bookingOffer) {
                $bookingOffer->status = self::STATUS_PENDING;
                $bookingOffer->mail_send_at = Application::getInstance()->now();
                $bookingOffer->save(
                             array(
                                 'status',
                                 'mail_send_at'
                             )
                );
            }
        }

        return $result;
    }

    /**
     * Sends the a mail to the patient and tells them that their booking offer has expired
     *
     * @param Booking
     *
     * @return bool
     */
    public static function sendExpiredBookingOffersEmail(Booking $booking)
    {
        if (!$booking->isValid()) {
            return false;
        }

        $user = $booking->getUser();
        if (!is_object($user) || !$user->isValid()) {
            return false;
        }

        $location = $booking->getLocation();
        if (!is_object($location) || !$location->isValid()) {
            return false;
        }

        // send a mail to patient
        $mailing = new Mailing();
        $mailing->type = MAILING_TYPE_HTML;
        $mailing->content_filename = '/mailing/patients/booking-offer-expired.html';
        $mailing->from = $GLOBALS['CONFIG']["BOOKINGS_EMAIL_ADDRESS"];
        $mailing->from_name = $GLOBALS['CONFIG']["BOOKINGS_EMAIL_NAME"];
        $mailing->data = array();
        $mailing->data['domain'] = $GLOBALS['CONFIG']['DOMAIN'];
        $mailing->data['booking_salutation_full_name'] = $booking->getSalutationName();
        $mailing->data['booking_appointment_date_text'] = $booking->getAppointment()->getWeekdayDateText();
        $mailing->data['booking_appointment_time_text'] = $booking->getAppointment()->getTimeText();
        $mailing->data['user_name'] = $user->getName();
        $mailing->data['location_name'] = $location->getName();
        $mailing->data['location_address'] = $location->getAddress(" - ");
        $mailing->addAddressTo($booking->getEmail());
        $result = $mailing->send();

        return $result;
    }

    /**
     * @return Offer[]
     */
    public static function getExpiredOffers()
    {
        // Find all pending booking offers where the expire date is overdue
        $searchSql
            = '(
				expires_at IS NOT NULL
				AND
				expires_at != "0000-00-00 00:00:00"
				AND
				NOW() >= expires_at
			)
			AND
			(status=' . Offer::STATUS_PENDING . ')';

        $bookingOffer = new Offer();
        $bookings = $bookingOffer->findObjects($searchSql);

        return $bookings;
    }
}
