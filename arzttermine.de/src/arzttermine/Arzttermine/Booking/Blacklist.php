<?php

namespace Arzttermine\Booking;

use Arzttermine\Core\Basic;

class Blacklist extends Basic {

    /**
     * @var string
     */
    protected $tablename = 'blacklist';

    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     */
    protected $phone;

    /**
     * @var string
     */
    protected $ip;

    /**
     * @var string
     */
    protected $message;

    /**
     * @var string
     */
    protected $comment;

    /**
     * @var string
     */
    protected $created_at;

    /**
     * @var int
     */
    protected $created_by;

    /**
     * @var string
     */
    protected $updated_at;

    /**
     * @var int
     */
    protected $updated_by;

    /**
     * @var array
     */
    protected $all_keys
        = array(
            'id',
            'email',
            'phone',
            'ip',
            'message',
            'comment',
            'created_at',
            'created_by',
            'updated_at',
            'updated_by'
        );

    /**
     * @var array
     */
    protected $secure_keys
        = array(
            'id',
            'email',
            'phone',
            'ip',
            'message',
            'comment'
        );

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     * @return self
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return self
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return self
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     * @return self
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
        return $this;
    }

    /**
     * @param string $created_at
     *
     * @return self
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param int $created_by
     *
     * @return self
     */
    public function setCreatedBy($created_by)
    {
        $this->created_by = $created_by;

        return $this;
    }

    /**
     * @return int
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * @param string $updated_at
     *
     * @return self
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param int $updated_by
     *
     * @return self
     */
    public function setUpdatedBy($updated_by)
    {
        $this->updated_by = $updated_by;

        return $this;
    }

    /**
     * @return int
     */
    public function getUpdatedBy()
    {
        return $this->updated_by;
    }

    /**
     * @param string $email
     * @param string $phone
     * @param string $ip
     *
     * @return bool
     */
    public function hasUser($email, $phone, $ip)
    {
        return (
            $this->hasEmail($email) ||
            $this->hasPhone($phone) ||
            $this->hasIP($ip)
        );
    }

    /**
     * @param string $email
     * @param string $phone
     * @param string $ip
     *
     * @return Blacklist
     *
     * @todo: Fix redundant object loading.
     */
    public function loadUser($email, $phone, $ip)
    {
        $sql = '';
        if (!empty($email)) {
            $sql = '(email="'.sqlsave($email).'")';
        }
        // normalize phone to only digits
        $phone = preg_replace("/[^0-9]+/", "", $phone);
        if (!empty($phone)) {
            if (!empty($sql)) $sql .= ' OR ';
            $sql .= '(phone="' . sqlsave($phone) . '")';
        }
        if (!empty($ip)) {
            if (!empty($sql)) $sql .= ' OR ';
            $sql .= '(ip="' . sqlsave($ip) . '")';
        }
        $object = $this->findObject($sql);
        if ($object->isValid()) {
            $this->load($object->getId());
        }
        return $this;
    }

    /**
     * @param string $email
     *
     * @return mixed
     */
    public function hasEmail($email)
    {
        if (empty($email)) return false;
        return $this->findByKey($email, 'email');
    }

    /**
     * @param string $phone
     *
     * @return mixed
     */
    public function hasPhone($phone)
    {
        // normalize phone to only digits
        $phone = preg_replace("/[^0-9]+/", "", $phone);
        if (empty($phone)) return false;
        return $this->findByKey($phone, 'phone');
    }

    /**
     * @param string $ip
     *
     * @return mixed
     */
    public function hasIP($ip)
    {
        if (empty($ip)) return false;
        return $this->findByKey($ip, 'ip');
    }

}
