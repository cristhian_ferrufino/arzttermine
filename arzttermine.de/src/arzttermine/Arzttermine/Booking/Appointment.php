<?php

namespace Arzttermine\Booking;

use Arzttermine\Application\Application;
use Arzttermine\Calendar\Calendar;
use Arzttermine\Location\Location;
use Arzttermine\MedicalSpecialty\MedicalSpecialty;
use Arzttermine\MedicalSpecialty\TreatmentType;
use NGS\DAL\DTO\TreatmentTypesDto;

class Appointment {

    /**
     * @var string
     */
    protected $datetime;

    /**
     * For appointment ranges, this will have a value
     *
     * @var string
     */
    protected $datetime_end;

    /**
     * @var string
     */
    protected $metadata;

    /**
     * @var TreatmentTypesDto[]
     */
    protected $treatmenttypes = array();

    /**
     * @var int
     */
    protected $user_id;

    /**
     * @var int
     */
    protected $location_id;

    /**
     * @var int
     */
    protected $insurance_id;

    /**
     * @var int
     */
    protected $medicalSpecialty_id;

    /**
     * @var int
     */
    protected $treatmenttype_id;

    /**
     * This text overwrites the Appointment text that is shown in the appointment block
     *
     * @var string
     */
    protected $custom_text;

    /**
     * This is only used in special cases like in the cron jobs
     *
     * @todo: change that and get rid if that from here
     * (search in the code for the constructor of Appointment() where the insuranceIds are used)
     *
     * @var string
     */
    protected $insurance_ids;

    /**
     * Constructor
     *
     * @param string $datetime
     * @param string $metadata
     * @param TreatmentTypesDto[]|array $treatmenttypeDtos
     */
    public function __construct($datetime = null, $metadata = null, $treatmenttypeDtos = array())
    {
        $this->datetime = $datetime;
        $this->metadata = $metadata;
        $this->treatmenttypeDtos = $treatmenttypeDtos;
    }

    /**
     * Checks if the appointment is valid
     * The Year of the appointment should be > 2010
     *
     * @return bool
     */
    public function isValid()
    {
        return (
            !empty($this->datetime)
            &&
            $this->datetime != '0000-00-00 00:00:00'
            &&
            date('Y', strtotime($this->datetime)) > 2010
        );
    }

    /**
     * Returns the datetime in a given format
     *
     * @param string $format
     *
     * @return string
     */
    public function getFormat($format = 'Y-m-d')
    {
        return date($format, strtotime($this->datetime));
    }

    /**
     * Returns the datetime_end in a given format
     *
     * @param string $format
     *
     * @return string
     */
    public function getEndFormat($format = 'Y-m-d')
    {
        return date($format, strtotime($this->datetime_end));
    }

    /**
     * Returns the date
     *
     * @return string
     */
    public function getDate()
    {
        return date('Y-m-d', strtotime($this->datetime));
    }

    /**
     * Returns the formatted date
     *
     * @param string $format
     * @return string
     */
    public function getDateFormat($format = 'd.m.Y')
    {
        return date($format, strtotime($this->datetime));
    }

    /**
     * Returns the date
     *
     * @return string
     */
    public function getDateText()
    {
        return date('Y.m.d', strtotime($this->datetime));
    }

    /**
     * Returns the weekday and date
     *
     * @param bool $short_weekday
     *
     * @return string
     */
    public function getWeekdayDateText($short_weekday = false)
    {
        return Calendar::getWeekdayDateText($this->datetime, $short_weekday);
    }

    /**
     * Returns the datetime
     *
     * @return string
     */
    public function getDateTime()
    {
        return $this->datetime;
    }

    /**
     * Returns the datetime in url attr-format '20120325T164500'
     *
     * @return string
     */
    public function getDateTimeUrlAttr()
    {
        return date('Ymd\THis', strtotime($this->datetime));
    }

    /**
     * Returns the datetime end in url attr-format '20120325T164500'
     *
     * @return string
     */
    public function getDateTimeEndUrlAttr()
    {
        return date('Ymd\THis', strtotime($this->datetime_end));
    }

    /**
     * Returns the datetime in '16:45'-format
     *
     * @param string $datetime
     * 
     * @return string
     */
    public function getTimeText($datetime = null)
    {
        if (empty($datetime)) {
            $datetime = $this->datetime;
        }
        
        return date('H:i', strtotime($datetime));
    }

    /**
     * Returns the datetime in 'Sonntag 25.03.2012 um 16:45 Uhr'-format
     *
     * @param bool $short
     *
     * @return string
     */
    public function getDateTimeText($short = false)
    {
        return Calendar::getWeekdayDateTimeText($this->datetime, $short);
    }

    /**
     * Sets a datetime end for appointment ranges
     *
     * @param string $datetime
     *
     * @return self
     */
    public function setDateTimeEnd($datetime)
    {
        $this->datetime_end = $datetime;

        return $this;
    }

    /**
     * Returns the datetime end
     *
     * @return string
     */
    public function getDateTimeEnd()
    {
        return $this->datetime_end;
    }

    /**
     * @return null|string
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * Returns the url for an appointment
     *
     * @param int $user_id
     * @param int $location_id
     * @param int $insurance_id
     * @param int $medical_specialty_id
     * @param int $treatmenttype_id
     *
     * @return string
     */
    public function getUrl($user_id = null, $location_id = null, $insurance_id = null, $medical_specialty_id = null, $treatmenttype_id = null)
    {
        $app = Application::getInstance();
        $router = $app->container->get('router');

        if (empty($user_id)) {
            $user_id = $this->getUserId();
        }

        if (empty($location_id)) {
            $location_id = $this->getLocationId();
        }

        if (empty($insurance_id)) {
            $insurance_id = $this->getInsuranceId();
        }

        if (empty($medical_specialty_id)) {
            $medical_specialty_id = $this->getMedicalSpecialtyId();
        }

        if (empty($treatmenttype_id)) {
            $treatmenttype_id = $this->getTreatmenttypeId();
        }

        $parameters = array(
            'u'  => $user_id,
            'l'  => $location_id,
            'm'  => $medical_specialty_id,
            'tt' => !empty($treatmenttype_id) ? $treatmenttype_id : '',
            'i'  => $insurance_id,
            's'  => $this->getDateTimeUrlAttr()
        );

        if (!empty($this->datetime_end)) {
            $parameters['e'] = $this->getDateTimeEndUrlAttr();
        }

        $url = $router->generate('appointment');

        // Take care for widgets
        if ($app->isWidget()) {
            $url = $router->generate('widget', array('widget_slug' => $app->getWidgetContainer()->getSlug(), 'parameter_string' => $url));
        }

        $url .= '?' . http_build_query($parameters);

        return "{$GLOBALS['CONFIG']['URL_HTTPS_LIVE']}{$app->i18nTranslateUrl($url)}";
    }

    /**
     * @param string $custom_text
     *
     * @return self
     */
    public function setCustomText($custom_text)
    {
        $this->custom_text = filter_var($custom_text, FILTER_SANITIZE_STRING);

        return $this;
    }

    /**
     * @return string
     */
    public function getCustomText()
    {
        return $this->custom_text;
    }

    /**
     * @param string $datetime
     *
     * @return self
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * @param int $insurance_id
     *
     * @return self
     */
    public function setInsuranceId($insurance_id)
    {
        $this->insurance_id = $insurance_id;

        return $this;
    }

    /**
     * @return int
     */
    public function getInsuranceId()
    {
        return $this->insurance_id;
    }

    /**
     * @param string $insurance_ids
     *
     * @return self
     */
    public function setInsuranceIds($insurance_ids)
    {
        $this->insurance_ids = $insurance_ids;

        return $this;
    }

    /**
     * @return string
     */
    public function getInsuranceIds()
    {
        return $this->insurance_ids;
    }

    /**
     * @return Location
     */
    public function getLocation()
    {
        return new Location($this->getLocationId());
    }

    /**
     * @param int $location_id
     *
     * @return self
     */
    public function setLocationId($location_id)
    {
        $this->location_id = $location_id;

        return $this;
    }

    /**
     * @return int
     */
    public function getLocationId()
    {
        return $this->location_id;
    }

    /**
     * @return MedicalSpecialty
     */
    public function getMedicalSpecialty()
    {
        return new MedicalSpecialty($this->getMedicalSpecialtyId());
    }

    /**
     * @param int $medicalSpecialty_id
     *
     * @return self
     */
    public function setMedicalSpecialtyId($medicalSpecialty_id)
    {
        $this->medicalSpecialty_id = $medicalSpecialty_id;

        return $this;
    }

    /**
     * @return int
     */
    public function getMedicalSpecialtyId()
    {
        return $this->medicalSpecialty_id;
    }

    /**
     * @param array|\NGS\DAL\DTO\TreatmentTypesDto[] $treatmenttypeDtos
     *
     * @return self
     */
    public function setTreatmenttypeDtos($treatmenttypeDtos)
    {
        $this->treatmenttypeDtos = $treatmenttypeDtos;

        return $this;
    }

    /**
     * @return array|\NGS\DAL\DTO\TreatmentTypesDto[]
     */
    public function getTreatmenttypeDtos()
    {
        return $this->treatmenttypeDtos;
    }

    /**
     * @param int $treatmenttype_id
     *
     * @return self
     */
    public function setTreatmenttypeId($treatmenttype_id)
    {
        $this->treatmenttype_id = $treatmenttype_id;

        return $this;
    }

    /**
     * @return int
     */
    public function getTreatmenttypeId()
    {
        return $this->treatmenttype_id;
    }

    /**
     * @return TreatmentType
     */
    public function getTreatmentType()
    {
        return new TreatmentType($this->getTreatmenttypeId());
    }

    /**
     * @param \NGS\DAL\DTO\TreatmentTypesDto[] $treatmenttypes
     *
     * @return self
     */
    public function setTreatmenttypes($treatmenttypes)
    {
        $this->treatmenttypes = $treatmenttypes;

        return $this;
    }

    /**
     * @return \NGS\DAL\DTO\TreatmentTypesDto[]
     */
    public function getTreatmenttypes()
    {
        return $this->treatmenttypes;
    }

    /**
     * @param int $user_id
     *
     * @return self
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }
}