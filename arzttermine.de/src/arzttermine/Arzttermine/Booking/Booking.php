<?php

namespace Arzttermine\Booking;

use Arzttermine\Application\Application;
use Arzttermine\Calendar\Calendar;
use Arzttermine\Core\Basic;
use Arzttermine\Core\Configuration;
use Arzttermine\Core\DateTime;
use Arzttermine\Coupon\UsedCoupon;
use Arzttermine\Insurance\Provider;
use Arzttermine\Integration\Integration;
use Arzttermine\Location\Location;
use Arzttermine\Mail\Mailing;
use Arzttermine\MedicalSpecialty\MedicalSpecialty;
use Arzttermine\MedicalSpecialty\TreatmentType;
use Arzttermine\User\User;
use Arzttermine\Widget\Container;
use MadMimi\Connection;
use MadMimi\CurlRequest;
use MadMimi\Options\Mail\Transactional;

class Booking extends Basic {

    const STATUS_NEW = 1;
    const STATUS_PENDING = 2;
    const STATUS_RECEIVED = 3;
    const STATUS_ABORTED = 4;
    const STATUS_INVALID = 5;
    const STATUS_CANCELLED_BY_PATIENT_LOGIN = 6;
    const STATUS_CANCELLED_BY_PATIENT = 7;
    const STATUS_RECEIVED_ALTERNATIVE = 8;
    const STATUS_RECEIVED_ALTERNATIVE_PENDING = 9;
    const STATUS_RECEIVED_UNCONFIRMED = 10;
    const STATUS_PENDING_CALLBACK = 11;
    const STATUS_FULFILLED = 12;
    const STATUS_ABORTED_UNREACHABLE = 13;
    const STATUS_SETTLED = 14;
    const STATUS_INVENTORY_PATIENT = 15;
    const STATUS_NOSHOW = 16;
    const STATUS_PENDING_LOCATION_NOT_REACHED = 17;
    const STATUS_PENDING_BOOKING_OFFER_ACCEPTED = 18;
    const STATUS_PENDING_BOOKING_OFFER_DECLINED = 19;
    const STATUS_PENDING_BOOKING_OFFER_EXPIRED = 20;
    const STATUS_INTEGRATION_INVALID = 21;
    const STATUS_PENDING_CANCELLATION_PENDING = 22;

    /**
     * @warning: Theses texts need to be in sync with the Salesforce Patient->Status texts!
     */
    private static $STATUS_TEXTS
        = array(
            self::STATUS_NEW                            => 'Neu',
            self::STATUS_PENDING                        => 'In Bearbeitung',
            self::STATUS_RECEIVED                       => 'Termin bekommen',
            self::STATUS_ABORTED                        => 'Abgebrochen',
            self::STATUS_INVALID                        => 'Ungültig',
            self::STATUS_CANCELLED_BY_PATIENT_LOGIN     => 'Termin storniert - Login',
            self::STATUS_CANCELLED_BY_PATIENT           => 'Termin storniert',
            self::STATUS_RECEIVED_ALTERNATIVE           => 'Ausweichtermin bekommen',
            self::STATUS_RECEIVED_ALTERNATIVE_PENDING   => 'Ausweichtermin bekommen (vorläufig)',
            self::STATUS_RECEIVED_UNCONFIRMED           => 'Termin bekommen (no confirmation)',
            self::STATUS_PENDING_CALLBACK               => 'In Bearbeitung - Rückrufbitte',
            self::STATUS_FULFILLED                      => 'Termin wahrgenommen',
            self::STATUS_ABORTED_UNREACHABLE            => 'Abgebrochen - nicht erreicht',
            self::STATUS_SETTLED                        => 'Abgerechnet',
            self::STATUS_INVENTORY_PATIENT              => 'Bestandspatient (laut Reporting)',
            self::STATUS_NOSHOW                         => 'No-show',
            self::STATUS_PENDING_LOCATION_NOT_REACHED   => 'In Bearbeitung - Praxis nicht erreicht',
            self::STATUS_PENDING_CANCELLATION_PENDING   => 'In Bearbeitung - Stornierung offen',
            self::STATUS_PENDING_BOOKING_OFFER_ACCEPTED => 'In Bearbeitung (Terminvorschlag angenommen)',
            self::STATUS_PENDING_BOOKING_OFFER_DECLINED => 'In Bearbeitung (Terminvorschlag abgelehnt)',
            self::STATUS_PENDING_BOOKING_OFFER_EXPIRED  => 'In Bearbeitung (Terminvorschlag abgelaufen)',
            self::STATUS_INTEGRATION_INVALID            => 'Integration ungültig'
        );

    /**
     * These statuses need to be in sync with the $STATUS_TEXT array
     * That array holds the text conversions for a patient as he
     * should not see internal definitions
     */
    private static $STATUS_PATIENT_TEXTS
        = array(
            self::STATUS_NEW                            => 'In Bearbeitung',
            self::STATUS_PENDING                        => 'In Bearbeitung',
            self::STATUS_RECEIVED                       => 'Termin bestätigt',
            self::STATUS_ABORTED                        => 'Terminanfrage abgebrochen',
            self::STATUS_INVALID                        => 'Ungültig',
            self::STATUS_CANCELLED_BY_PATIENT_LOGIN     => 'Termin storniert',
            self::STATUS_CANCELLED_BY_PATIENT           => 'Termin storniert',
            self::STATUS_RECEIVED_ALTERNATIVE           => 'Termin bestätigt',
            self::STATUS_RECEIVED_ALTERNATIVE_PENDING   => 'Ausweichtermin',
            self::STATUS_RECEIVED_UNCONFIRMED           => 'Termin unbestätigt - Bitte an die Praxis wenden',
            self::STATUS_PENDING_CALLBACK               => 'In Bearbeitung - Rückrufbitte (0800 / 2222133)',
            self::STATUS_FULFILLED                      => 'Termin bestätigt',
            self::STATUS_ABORTED_UNREACHABLE            => 'Terminanfrage abgebrochen',
            self::STATUS_SETTLED                        => 'Termin bestätigt',
            self::STATUS_INVENTORY_PATIENT              => 'Termin bestätigt',
            self::STATUS_NOSHOW                         => 'Termin nicht wahrgenommen - Rückrufbitte (0800 / 2222133)',
            self::STATUS_PENDING_LOCATION_NOT_REACHED   => 'In Bearbeitung',
            self::STATUS_PENDING_CANCELLATION_PENDING   => 'In Bearbeitung',
            self::STATUS_PENDING_BOOKING_OFFER_ACCEPTED => 'In Bearbeitung (Terminvorschlag angenommen)',
            self::STATUS_PENDING_BOOKING_OFFER_DECLINED => 'In Bearbeitung (Terminvorschlag abgelehnt)',
            self::STATUS_PENDING_BOOKING_OFFER_EXPIRED  => 'In Bearbeitung',
            self::STATUS_INTEGRATION_INVALID            => 'Integration ungültig'
        );

    /**
     * @var string
     */
    protected $tablename = DB_TABLENAME_BOOKINGS;

    /**
     * @var string
     */
    protected $slug = '';

    /**
     * @var int
     */
    protected $status;

    /**
     * @var string
     */
    protected $backend_marker = '';

    /**
     * @var string
     */
    protected $source_platform = '';

    /**
     * @var int
     */
    protected $integration_id;

    /**
     * @var string
     */
    protected $integration_status = '0';

    /**
     * @var string
     */
    protected $integration_log = '';

    /**
     * @var string
     */
    protected $salesforce_updated_at = null;

    /**
     * @var string
     */
    protected $form_once = '';

    /**
     * @var int
     */
    protected $user_id;

    /**
     * @var int
     */
    protected $location_id;

    /**
     * @var string
     */
    protected $appointment_start_at = '';

    /**
     * @var int
     */
    protected $gender;

    /**
     * @var string
     */
    protected $first_name = '';

    /**
     * @var string
     */
    protected $last_name = '';

    /**
     * @var string
     */
    protected $email = '';

    /**
     * @var string
     */
    protected $phone = '';

    /**
     * @var int
     */
    protected $insurance_id;

    /**
     * @var int Actually a tinyint/bool
     */
    protected $returning_visitor;

    /**
     * @var int
     */
    protected $medical_specialty_id;

    /**
     * @var int
     */
    protected $treatmenttype_id;

    /**
     * @var string
     */
    protected $comment_intern = ''; // define ("BOOKING_STATUS_NEW", 1); / SF: "Neu"

    /**
     * @var string
     */
    protected $comment_patient = ''; // define ("BOOKING_STATUS_IN_PROCESSING", 2); / SF: "In Bearbeitung"

    /**
     * @var string
     */
    protected $booked_language = ''; // define ("BOOKING_STATUS_PROCESSED", 3); / SF: "Termin bekommen"

    /**
     * @var bool
     */
    protected $flexible_time = '0'; // define ("BOOKING_STATUS_CANCELED", 4); / SF: "Abgebrochen" (no appointment was found)

    /**
     * @var bool
     */
    protected $flexible_location = '0'; // define ("BOOKING_STATUS_UNVALID", 5); / SF: "Ungültig"

    /**
     * @var string
     */
    protected $created_at = ''; // define ("BOOKING_STATUS_INVALID_LOGIN", 6); / SF: "Termin storniert - Login"

    /**
     * @var string
     */
    protected $created_by = ''; // SF: "Termin storniert" (by patient per email or phone)

    /**
     * @var string
     */
    protected $updated_at = ''; // SF: "Ausweichtermin bekommen"

    /**
     * @var string
     */
    protected $updated_by = ''; // SF: "Ausweichtermin bekommen (vorläufig)"

    /**
     * @var bool
     */
    protected $newsletter_subscription; // SF: "Termin bekommen (no confirmation)"

    /**
     * @var string
     */
    protected $salesforce_id = ''; // SF: "In Bearbeitung - Rückrufbitte"

    /**
     * @var string
     */
    protected $ip_address = ''; // SF: "Termin wahrgenommen"

    /**
     * @var int
     */
    protected $insurance_provider_id; // SF: "Abgebrochen - nicht erreicht"

    /**
     * @var string
     */
    protected $url = ''; // SF: "Abgerechnet"

    /**
     * @var string
     */
    protected $last_error_message = ''; // SF: "Bestandspatient (laut Reporting)"

    /**
     * @var string
     */
    protected $booking_offer_slug = ''; // SF: "No-show"

    /**
     * @var string
     */
    protected $updated_confirmed_at = ''; // SF: "In Bearbeitung (Terminvorschlag angenommen)"

    /**
     * @var int
     */
    protected $confirmed = 0; // SF: "In Bearbeitung (Terminvorschlag abgelehnt)"

    /**
     * @var string
     *
     * @deprecated
     */
    protected $integration_booking_data_serialized; // SF: "In Bearbeitung (Terminvorschlag abgelaufen)"

    /**
     * If the "returning_visitor" flag was changed by the doctor, then this booking is "dirty"
     * 
     * @var bool
     */
    protected $dirty;

    /**
     * @var string
     */
    protected $contact_preference = '';

    protected $recall_preference = '';

    protected $recall_phone = '';

    /**
     * @var array
     */
    protected $all_keys
        = array(
            "id",
            "slug",
            "status",
            "backend_marker",
            "source_platform",
            "integration_id",
            "integration_status",
            "integration_log",
            "salesforce_updated_at",
            "form_once",
            "user_id",
            "location_id",
            "appointment_start_at",
            "gender",
            "first_name",
            "last_name",
            "email",
            "phone",
            "insurance_id",
            "returning_visitor",
            "medical_specialty_id",
            "treatmenttype_id",
            "comment_intern",
            "comment_patient",
            "booked_language",
            "flexible_time",
            "flexible_location",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by",
            "updated_confirmed_at",
            "confirmed",
            "newsletter_subscription",
            "salesforce_id",
            "ip_address",
            "insurance_provider_id",
            "dirty",
            "contact_preference",
            "recall_preference",
            "recall_phone"
        );

    /**
     * @var array
     */
    protected $secure_keys
        = array(
            "id",
            "slug",
            "status",
            "backend_marker",
            "source_platform",
            "integration_id",
            "integration_status",
            "integration_log",
            "salesforce_updated_at",
            "form_once",
            "user_id",
            "location_id",
            "appointment_start_at",
            "gender",
            "first_name",
            "last_name",
            "email",
            "phone",
            "insurance_id",
            "returning_visitor",
            "treatmenttype_id",
            "comment_intern",
            "comment_patient",
            "booked_language",
            "flexible_time",
            "flexible_location",
            "created_at",
            "created_by",
            "updated_at",
            "updated_confirmed_at",
            "confirmed",
            "updated_by",
            "newsletter_subscription",
            "salesforce_id",
            "ip_address",
            "insurance_provider_id",
            "contact_preference",
            "recall_preference",
            "recall_phone"
        );

    /**
     * Returns a new Nonce (Form Once) Code
     *
     * @return string
     */
    public static function getNonce()
    {
        return substr(md5(uniqid()), 0, 8);
    }

    /**
     * Checks if this form has already been sent
     *
     * @param string $nonce
     *
     * @return bool
     */
    public static function nonceExists($nonce)
    {
        $_booking = new Booking();

        return $_booking->count('form_once="' . sqlsave($nonce) . '"');
    }

    /**
     * Checks if this form_once is valid
     *
     * @param string $nonce
     *
     * @return bool
     */
    public static function nonceIsValid($nonce)
    {
        return (strlen($nonce) == 8);
    }

    /**
     * Returns the status text array with all available statuses
     *
     * @return array
     */
    public static function getStatusTextArray()
    {
        return self::$STATUS_TEXTS;
    }

    /**
     * Returns the all statuses where the booking is marked as RECEIVED
     * but where the patient has not been at the appointment yet
     *
     * Sometimes it is needed to reduce the statuses by one specific status.
     * In this case use the hide_statuses array.
     *
     * @param array $hide_statuses (optional)
     *
     * @return array
     */
    public static function getReceivedStatuses($hide_statuses = array())
    {
        $statuses = array(
            self::STATUS_RECEIVED,
            self::STATUS_RECEIVED_ALTERNATIVE,
            self::STATUS_RECEIVED_ALTERNATIVE_PENDING,
            self::STATUS_RECEIVED_UNCONFIRMED,
        );

        if (!is_array($hide_statuses)) {
            $hide_statuses = array();
        }

        // return all statuses that are not in hide_statuses
        return array_diff($statuses, $hide_statuses);
    }

    /**
     * Returns the all statuses where the booking can be reviewed by the patient
     *
     * @param array $hide_statuses (optional)
     *
     * @return array
     */
    public static function getReviewableStatuses($hide_statuses = array())
    {
        $statuses = array(
            self::STATUS_RECEIVED,
            self::STATUS_RECEIVED_ALTERNATIVE,
            self::STATUS_RECEIVED_ALTERNATIVE_PENDING,
            self::STATUS_RECEIVED_UNCONFIRMED,
            self::STATUS_FULFILLED,
            self::STATUS_SETTLED,
            self::STATUS_INVENTORY_PATIENT
        );

        if (!is_array($hide_statuses)) {
            $hide_statuses = array();
        }

        // return all statuses that are not in hide_statuses
        return array_diff($statuses, $hide_statuses);
    }

    /**
     * Returns the all statuses where the booking is marked as chargeable
     * which means the patient was in the praxis.
     * This is not getRevenueStatuses() because its not possible to say by its status
     * if the booking made revenues as this is stored by the user/doctor.
     *
     * Sometimes it is needed to reduce the statuses by one specific status.
     * In this case use the hide_statuses array.
     *
     * @param array $hide_statuses (optional)
     *
     * @return array
     */
    public static function getChargeableStatuses($hide_statuses = array())
    {
        $statuses = array(
            self::STATUS_FULFILLED,
            self::STATUS_SETTLED,
            self::STATUS_INVENTORY_PATIENT
        );

        if (!is_array($hide_statuses)) {
            $hide_statuses = array();
        }

        // return all statuses that are not in hide_statuses
        return array_diff($statuses, $hide_statuses);
    }

    /**
     * Returns the all statuses where the booking is assigned /reserved
     * which means nobody else can (currently) use that appointment.
     *
     * Sometimes it is needed to reduce the statuses by one specific status.
     * In this case use the hide_statuses array.
     *
     * @param array $hide_statuses (optional)
     *
     * @return array
     */
    public static function getBookedStatuses($hide_statuses = array())
    {
        $statuses = array(
            self::STATUS_NEW,
            self::STATUS_PENDING,
            self::STATUS_RECEIVED,
            self::STATUS_RECEIVED_ALTERNATIVE,
            self::STATUS_RECEIVED_ALTERNATIVE_PENDING,
            self::STATUS_RECEIVED_UNCONFIRMED,
            self::STATUS_PENDING_CALLBACK,
            self::STATUS_FULFILLED,
            self::STATUS_SETTLED,
            self::STATUS_INVENTORY_PATIENT,
            self::STATUS_NOSHOW,
            self::STATUS_PENDING_LOCATION_NOT_REACHED,
            self::STATUS_PENDING_BOOKING_OFFER_ACCEPTED,
            self::STATUS_PENDING_BOOKING_OFFER_DECLINED,
            self::STATUS_PENDING_BOOKING_OFFER_EXPIRED
        );

        if (!is_array($hide_statuses)) {
            $hide_statuses = array();
        }

        // return all statuses that are not in hide_statuses
        return array_diff($statuses, $hide_statuses);
    }

    /**
     * Checks and sanitizes the values for a booking
     * and returns the appointment_start_at, user and location in the data array
     *
     * @param array $data
     *
     * @return array
     */
    public static function sanitizeDataSetup($data)
    {
        $errors = array();

        if (empty($data['appointment_start_at']) && isset($data['start_at_id'])) {
            // the start_at_id can be unset for appointment enquiry
            $appointment_start_at = Calendar::UTC2DateTime($data['start_at_id']);

            if ($appointment_start_at != false) {
                $data['appointment_start_at'] = $appointment_start_at;
            } else {
                $data['appointment_start_at'] = '0000-00-00 00:00:00';
            }
        }

        // if there is an appointment range (end_at_id) then convert that and store in array
        if (!empty($data['end_at_id']) && empty($data['appointment_end_at'])) {
            $data['appointment_end_at'] = Calendar::UTC2DateTime($data['end_at_id']);
        }

        // load location and user
        $location = new Location($data['location_id']);

        if (!$location->isValid()) {
            $errors[] = 'Error: Wrong location_id';
        }

        $user = new User($data['user_id']);

        if (!$user->isValid() || $user->getGroupId() != GROUP_DOCTOR) {
            $errors[] = 'Error: Wrong user_id';
        }

        // adding available insurance ids for the appointment
        $data['available_insurance_ids'] = array();

        if ($location->isValid() && $user->isValid()) {
            $data['location'] = $location;
            $data['user'] = $user;

            $data['available_insurance_ids'] = $location->getAvailableInsuranceIds($user, $data['appointment_start_at']);
        }

        if (!empty($errors)) {
            error_log($errors);
        }

        return $data;
    }

    /**
     * Checks and sanitizes the values for a booking
     *
     * @param array $data
     *
     * @return array
     */
    public static function sanitizeDataForm($data)
    {
        $app = Application::getInstance();
        $view = $app->container->get('templating');
        $errors = array();

        if (!isset($data['gender']) || !is_numeric($data['gender']) || !isset($GLOBALS['CONFIG']['ARRAY_GENDER'][$data['gender']])) {
            $view->addMessage('STATUS', 'ERROR', $app->_('Bitte wählen Sie Ihre Anrede aus.'));
            $errors[] = 'Error: Need gender';
        }

        $_email = sanitize_email($data['email']);
        if (!is_email($_email)) {
            $view->addMessage('STATUS', 'ERROR', $app->_('Geben Sie bitte Ihre E-Mailadresse ein.'));
            $errors[] = 'Error: Need valid email';
        } else {
            $data['email'] = $_email;
        }

        $data['first_name'] = sanitize_text_field($data['first_name']);
        if (mb_strlen($data['first_name'], $GLOBALS["CONFIG"]["SYSTEM_CHARSET"]) < 1) {
            $view->addMessage('STATUS', 'ERROR', $app->_('Bitte geben Sie Ihren Vornamen ein.'));
            $errors[] = 'Error: Need first name';
        }

        $data['last_name'] = sanitize_text_field($data['last_name']);
        if (mb_strlen($data['last_name'], $GLOBALS["CONFIG"]["SYSTEM_CHARSET"]) < 1) {
            $view->addMessage('STATUS', 'ERROR', $app->_('Bitte geben Sie Ihren Nachnamen ein.'));
            $errors[] = 'Error: Need last name';
        }

        $data['phone'] = sanitize_phonenumber($data['phone']);
        if (strlen($data['phone']) < 6) {
            $view->addMessage('STATUS', 'ERROR', $app->_('Bitte geben Sie eine gültige Telefonnummer ein.'));
            $errors[] = 'Error: Need phone number';
        }

        if (!isset($data['insurance_id']) || !is_numeric($data['insurance_id']) || !isset($GLOBALS['CONFIG']['INSURANCES'][$data['insurance_id']])) {
            $view->addMessage('STATUS', 'ERROR', $app->_('Bitte geben Sie an, wie Sie versichert sind.'));
            $errors[] = 'Error: Need valid insurance_id';
        }

        if (!isset($data['returning_visitor'])) {
            $view->addMessage('STATUS', 'ERROR', $app->_('Bitte geben Sie an, ob Sie schon einmal bei diesem Arzt in Behandlung waren.'));
            $errors[] = 'Error: Need information about returning_visitor';
        }

        if (!empty($errors)) {
            $data['errors'] = $errors;
        }

        return $data;
    }

    /**
     * Sends the booking email to the patient
     *
     * @todo Remove static access
     *
     * @param Booking $booking
     * @param string $template_filename
     *
     * @return bool
     */
    public static function sendBookingEmail($booking, $template_filename = 'booking-new.html')
    {
        $user = $booking->getUser();
        $location = $booking->getLocation();
        if (!$user->isValid() || !$location->isValid()) {
            return false;
        }

        // send a mail to patient
        $mailing = new Mailing();
        $mailing->type = MAILING_TYPE_HTML;

        $data = array(
            'domain'                            => $GLOBALS['CONFIG']['DOMAIN'],
            'booking_phone'                     => $booking->getPhone(),
            'booking_email'                     => $booking->getEmail(),
            'user_name'                         => $user->getName(),
            'location_name'                     => $location->getName(),
            'location_address'                  => $location->getAddress("<br />"),
            'location_phone'                    => $location->getPhone(),
            'appointment_date'                  => $booking->getAppointment()->getWeekdayDateText(),
            'appointment_time'                  => $booking->getAppointment()->getTimeText(),
            'booking_salutation_full_name'      => $booking->getSalutationName(),
            'booking_appointment_start_at'      => $booking->getAppointment()->getDateTime(),
            'booking_appointment_start_at_text' => $booking->getAppointment()->getDateTimeText(),
            'booking_returning_visitor_text'    => $booking->getReturningVisitorText(),
            'booking_insurance_text'            => $booking->getInsuranceText(),
            'booking_treatmenttype_text'        => $booking->getTreatmenttypeText()
        );

        $template_prefix = '/mailing/integrations/' . $booking->getIntegrationId() . '/';

        $mailing->content_filename = $template_prefix . $template_filename;
        $mailing->from = $GLOBALS['CONFIG']["BOOKINGS_EMAIL_ADDRESS"];
        $mailing->from_name = $GLOBALS['CONFIG']["BOOKINGS_EMAIL_NAME"];
        $mailing->data = $data;
        $mailing->addAddressTo($data['booking_email']);

        return $mailing->send();
    }

    /**
     * Returns the appointment object
     *
     * @return Appointment
     */
    public function getAppointment()
    {
        return new Appointment($this->appointment_start_at);
    }

    /**
     * Returns the full name with salutation
     *
     * @return string
     */
    public function getSalutationName()
    {
        if ($this->getGender() === User::GENDER_MALE) {
            $name = 'Sehr geehrter ';
        } else {
            $name = 'Sehr geehrte ';
        }

        $name .= $this->getName(true);

        return $name;
    }

    /**
     * Returns the full name
     *
     * @param bool $add_gender
     *
     * @return string
     */
    public function getName($add_gender = true)
    {
        $_name = '';

        if ($add_gender) {
            $_name .= "{$this->getGenderText()} ";
        }

        $_name .= "{$this->first_name} {$this->last_name}";

        return $_name;
    }

    /**
     * Returns the patients gender text
     *
     * @return string
     */
    public function getGenderText()
    {
        return !empty($GLOBALS['CONFIG']['ARRAY_GENDER'][$this->getGender()]) ? $GLOBALS['CONFIG']['ARRAY_GENDER'][$this->getGender()] : '';
    }

    /**
     * Returns the returning visitor text
     *
     * @return string
     */
    public function getReturningVisitorText()
    {
        if (isset($GLOBALS['CONFIG']["ARRAY_NO_YES"][$this->returning_visitor])) {
            return $GLOBALS['CONFIG']["ARRAY_NO_YES"][$this->returning_visitor];
        } else {
            return 'N/A';
        }
    }

    /**
     * Returns the insurance text
     *
     * @return string
     */
    public function getInsuranceText()
    {
        if (isset($GLOBALS['CONFIG']["INSURANCES"][$this->insurance_id])) {
            return $GLOBALS['CONFIG']["INSURANCES"][$this->insurance_id];
        } else {
            return 'N/A';
        }
    }

    /**
     * Returns the treatmenttype text
     *
     * @return string
     */
    public function getTreatmenttypeText()
    {
        $tt = new TreatmentType($this->treatmenttype_id);

        return $tt->isValid() ? $tt->getName() : '';
    }

    /**
     * Returns the all statuses where the booking is pending
     *
     * Sometimes it is needed to reduce the statuses by one specific status.
     * In this case use the hide_statuses array.
     *
     * @param array $hide_statuses
     *
     * @return array
     */
    public static function getPendingStatuses($hide_statuses = array())
    {
        $statuses = array(
            self::STATUS_PENDING,
            self::STATUS_PENDING_CALLBACK,
            self::STATUS_PENDING_LOCATION_NOT_REACHED,
            self::STATUS_PENDING_CANCELLATION_PENDING,
        );

        if (!is_array($hide_statuses)) {
            $hide_statuses = array();
        }

        // return all statuses that are not in hide_statuses
        return array_diff($statuses, $hide_statuses);
    }

    /**
     * Calc some vars
     *
     * @return bool
     */
    public function calcVars()
    {
        /*
         * hack for widgets:
         * oh boy this is such a hack to get the slug of the widget and calc the
         * booking urls right instead of using relative paths.
         * works also with 'buchung/'+$this->slug but this is even worse
         * 
         * Neill [04.14]: I added a dirty member var in Application to accept the Widget :/
         */
        $urlPrefix = '';

        $app = Application::getInstance();
        $widgetContainer = $app->getWidgetContainer();

        if ($app->isWidget() && !empty($widgetContainer) && $widgetContainer instanceof Container) {
            $urlPrefix = $widgetContainer->getDefaultUrl();
        }

        $this->setUrl($urlPrefix . '/buchung/' . $this->getSlug());

        return true;
    }

    /**
     * @param string $url
     *
     * @return self
     */
    public function setUrl($url)
    {
        $this->url = trim($url);

        return $this;
    }

    /**
     * Returns the slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @return string
     */
    public function getSalesforceUrl()
    {
        return 'https://eu1.salesforce.com/' . $this->getSalesforceId();
    }

    /**
     * @return string
     */
    public function getSalesforceId()
    {
        return $this->salesforce_id;
    }

    /**
     * @return string
     */
    public function getCancelUrl()
    {
        return $this->getUrl(true, true) . '?action=stornieren';
    }

    /**
     * @param bool $absolute
     * @param bool $secure
     *
     * @return string
     */
    public function getUrl($absolute = false, $secure = false)
    {
        return ($absolute ? ($secure ? $GLOBALS['CONFIG']['URL_HTTPS_LIVE'] : $GLOBALS['CONFIG']['URL_HTTP_LIVE']) : '') . $this->url;
    }

    /**
     * @return string
     */
    public function getAdminEditUrl()
    {
        return '/administration/bookings?action=edit&id=' . $this->getId();
    }

    /**
     * @param string $backend_marker
     *
     * @return self
     */
    public function setBackendMarker($backend_marker)
    {
        $this->backend_marker = $backend_marker;

        return $this;
    }

    /**
     * @return string
     */
    public function getBackendMarker()
    {
        return $this->backend_marker;
    }

    /**
     * @param string $booked_language
     *
     * @return self
     */
    public function setBookedLanguage($booked_language)
    {
        $this->booked_language = $booked_language;

        return $this;
    }

    /**
     * @return string
     */
    public function getBookedLanguage()
    {
        return $this->booked_language;
    }

    /**
     * @param string $created_at
     *
     * @return self
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param string $created_by
     *
     * @return self
     */
    public function setCreatedBy($created_by)
    {
        $this->created_by = $created_by;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * @param string $form_once
     *
     * @return self
     */
    public function setFormOnce($form_once)
    {
        $this->form_once = $form_once;

        return $this;
    }

    /**
     * @return string
     */
    public function getFormOnce()
    {
        return $this->form_once;
    }

    /**
     * @param string $integration_log
     *
     * @return self
     */
    public function setIntegrationLog($integration_log)
    {
        $this->integration_log = $integration_log;

        return $this;
    }

    /**
     * @return string
     */
    public function getIntegrationLog()
    {
        return $this->integration_log;
    }

    /**
     * @param string $ip_address
     *
     * @return self
     */
    public function setIpAddress($ip_address)
    {
        $this->ip_address = $ip_address;

        return $this;
    }

    /**
     * @return string
     */
    public function getIpAddress()
    {
        return $this->ip_address;
    }

    /**
     * @todo Return an object instead of an int
     *
     * @param int $insurance_provider_id
     *
     * @return self
     */
    public function setInsuranceProviderId($insurance_provider_id)
    {
        $this->insurance_provider_id = $insurance_provider_id;

        return $this;
    }

    /**
     * @return Provider
     */
    public function getInsuranceProvider()
    {
        return new Provider($this->getInsuranceProviderId());
    }

    /**
     * @return int
     */
    public function getInsuranceProviderId()
    {
        return $this->insurance_provider_id;
    }

    /**
     * @param int $medical_specialty_id
     *
     * @return self
     */
    public function setMedicalSpecialtyId($medical_specialty_id)
    {
        $this->medical_specialty_id = $medical_specialty_id;

        return $this;
    }

    /**
     * @return MedicalSpecialty
     */
    public function getMedicalSpecialty()
    {
        return new MedicalSpecialty($this->getMedicalSpecialtyId());
    }

    /**
     * @return int
     */
    public function getMedicalSpecialtyId()
    {
        return $this->medical_specialty_id;
    }

    /**
     * @param boolean $newsletter_subscription
     *
     * @return self
     */
    public function setNewsletterSubscription($newsletter_subscription)
    {
        $this->newsletter_subscription = (bool)$newsletter_subscription;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getNewsletterSubscription()
    {
        return $this->newsletter_subscription;
    }

    /**
     * @param int $returning_visitor
     *
     * @return self
     */
    public function setReturningVisitor($returning_visitor)
    {
        $this->returning_visitor = $returning_visitor;

        return $this;
    }

    /**
     * @param int $treatmenttype_id
     *
     * @return self
     */
    public function setTreatmenttypeId($treatmenttype_id)
    {
        $this->treatmenttype_id = $treatmenttype_id;

        return $this;
    }

    /**
     * @return TreatmentType
     */
    public function getTreatmentType()
    {
        return new TreatmentType($this->getTreatmenttypeId());
    }

    /**
     * @return int
     */
    public function getTreatmenttypeId()
    {
        return $this->treatmenttype_id;
    }

    /**
     * @param string $updated_at
     *
     * @return self
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param string $updated_by
     *
     * @return self
     */
    public function setUpdatedBy($updated_by)
    {
        $this->updated_by = $updated_by;

        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedBy()
    {
        return $this->updated_by;
    }

    /**
     * @param string $appointment_start_at
     *
     * @return self
     */
    public function setAppointmentStartAt($appointment_start_at)
    {
        // Use a proxy object to format for us
        $datetime = new DateTime($appointment_start_at);
        $this->appointment_start_at = $datetime->getDateTime();

        return $this;
    }

    /**
     * Returns the userId
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Returns the locationId
     *
     * @return int
     */
    public function getLocationId()
    {
        return $this->location_id;
    }

    /**
     * @param int $status
     *
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @param string $booking_offer_slug
     *
     * @return self
     */
    public function setBookingOfferSlug($booking_offer_slug)
    {
        $this->booking_offer_slug = $booking_offer_slug;

        return $this;
    }

    /**
     * @param string $comment_intern
     *
     * @return self
     */
    public function setCommentIntern($comment_intern)
    {
        $this->comment_intern = $comment_intern;

        return $this;
    }

    /**
     * @param string $comment_patient
     *
     * @return self
     */
    public function setCommentPatient($comment_patient)
    {
        $this->comment_patient = $comment_patient;

        return $this;
    }

    /**
     * @param string $email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @param string $first_name
     *
     * @return self
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;

        return $this;
    }

    /**
     * @param boolean $flexible_location
     *
     * @return self
     */
    public function setFlexibleLocation($flexible_location)
    {
        $this->flexible_location = $flexible_location;

        return $this;
    }

    /**
     * @param boolean $flexible_time
     *
     * @return self
     */
    public function setFlexibleTime($flexible_time)
    {
        $this->flexible_time = $flexible_time;

        return $this;
    }

    /**
     * @param int $gender
     *
     * @return self
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @param int $insurance_id
     *
     * @return self
     */
    public function setInsuranceId($insurance_id)
    {
        $this->insurance_id = $insurance_id;

        return $this;
    }

    /**
     * @param int $integration_id
     *
     * @return self
     */
    public function setIntegrationId($integration_id)
    {
        $this->integration_id = $integration_id;

        return $this;
    }

    /**
     * @param string $integration_status
     *
     * @return self
     */
    public function setIntegrationStatus($integration_status)
    {
        $this->integration_status = $integration_status;

        return $this;
    }

    /**
     * @param string $last_error_message
     *
     * @return self
     */
    public function setLastErrorMessage($last_error_message)
    {
        $this->last_error_message = $last_error_message;

        return $this;
    }

    /**
     * @param string $last_name
     *
     * @return self
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;

        return $this;
    }

    /**
     * @param int $location_id
     *
     * @return self
     */
    public function setLocationId($location_id)
    {
        $this->location_id = $location_id;

        return $this;
    }

    /**
     * @param string $phone
     *
     * @return self
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @param string $slug
     *
     * @return self
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @param string $source_platform
     *
     * @return self
     */
    public function setSourcePlatform($source_platform)
    {
        $this->source_platform = $source_platform;

        return $this;
    }

    /**
     * @param int $user_id
     *
     * @return self
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Returns the status text
     * Can be used as static method as well as object method
     *
     * @param int $status
     *
     * @return string
     */
    public function getStatusText($status = null)
    {
        return self::findStatusText(!empty($status) ? $status : $this->status);
    }

    /**
     * Crappy function to get past this weird static/object thing
     *
     * @param null $status
     */
    public static function findStatusText($status = null)
    {
        return self::$STATUS_TEXTS[$status];
    }

    /**
     * Returns the status text for a patient
     * Can be used as static method as well as object method
     *
     * @param int $status
     *
     * @return string
     */
    public function getStatusPatientText($status = null)
    {
        if (!$status) {
            return self::$STATUS_PATIENT_TEXTS[$this->status];
        } else {
            return self::$STATUS_PATIENT_TEXTS[$status];
        }
    }

    /**
     * Checks if the booking have the given status or statuses
     *
     * @param int $status
     *
     * @return bool
     */
    public function hasStatus($status)
    {
        if (!is_array($status)) {
            $status = array($status);
        }

        return in_array($this->status, $status);
    }

    /**
     * Returns the integration status id
     *
     * @return int
     */
    public function getIntegrationStatus()
    {
        return $this->integration_status;
    }

    /**
     * Saves the integration status
     *
     * @param status
     *
     * @return bool
     */
    public function saveIntegrationStatus($status)
    {
        $this->integration_status = $status;

        return $this->save(array('integration_status'));
    }

    /**
     * Returns the datetime
     *
     * @param string $format
     *
     * @return \DateTime string
     */
    public function getCreatedAtFormat($format = 'Y-m-d')
    {
        // Dirty hack, but we want the German weekday
        // @todo Remove
        if ($format == 'w d.m.Y, H:i') {
            return Calendar::getWeekdayDateTimeText($this->created_at, true);
        }

        return date($format, strtotime($this->created_at));
    }

    /**
     * Returns the number of hours from the time the booking was made to the appointment
     *
     * @return int
     */
    public function getHoursCreatedAtToAppointment()
    {
        if (!$this->hasValidAppointmentStartAt()) {
            return 0;
        }

        $datetime1 = new DateTime($this->getCreatedAt());
        $datetime2 = new DateTime($this->getAppointmentStartAt());
        $interval = $datetime1->diff($datetime2);
        
        return ($interval->d * 24) + $interval->h;
    }

    /**
     * Returns the internal comment
     *
     * @return string
     */
    public function getCommentIntern()
    {
        return $this->comment_intern;
    }

    /**
     * Returns if the patient has flexible time
     *
     * @return bool
     */
    public function getFlexibleTime()
    {
        return (bool)$this->flexible_time;
    }

    /**
     * Get the last error message (if available)
     *
     * @return string
     */
    public function getLastErrorMessage()
    {
        return $this->last_error_message;
    }

    /**
     * Checks if the booking should show a "print this appointment" link.
     * Only integrations with an automated booking process should show this link
     *
     * @return bool
     */
    public function showPrintLinkOnBookingConfirmationPage()
    {
        if (!isset($GLOBALS['CONFIG']['INTEGRATIONS'][$this->getIntegrationId()]['AUTOMATIC_BOOKING_PROCESS'])) {
            return false;
        }

        return $GLOBALS['CONFIG']['INTEGRATIONS'][$this->getIntegrationId()]['AUTOMATIC_BOOKING_PROCESS'];
    }

    /**
     * Returns the integrationId
     * Warning: A booking can have an integrationId that does not corespond to the current location->integration_id
     * as for the time of booking the location could have had another integration
     *
     * @return int
     */
    public function getIntegrationId()
    {
        if ($this->integration_id) {
            $integration_id = $this->integration_id;
        } else {
            // use that as a default
            $integration_id = $this->getLocation()->getIntegrationId();
        }

        return $integration_id;
    }

    /**
     * Returns the location
     *
     * @return Location
     */
    public function getLocation()
    {
        return new Location($this->location_id);
    }

    /**
     * Checks if the booking should show a ics link.
     * Only integrations with an automated booking process should show this link
     *
     * @return bool
     */
    public function showICSLinkOnBookingConfirmationPage()
    {
        if (!isset($GLOBALS['CONFIG']['INTEGRATIONS'][$this->getIntegrationId()]['AUTOMATIC_BOOKING_PROCESS'])) {
            return false;
        }

        return $GLOBALS['CONFIG']['INTEGRATIONS'][$this->getIntegrationId()]['AUTOMATIC_BOOKING_PROCESS'];
    }

    /**
     * Checks if the booking has a valid appointment_start_at datetime
     * See more doc at self::isValidAppointmentStartAt()
     *
     * @return bool
     */
    public function hasValidAppointmentStartAt()
    {
        return (self::isValidAppointmentStartAt($this->getAppointmentStartAt()));
    }

    /**
     * Checks if the appointment_start_at is a valid datetime
     * Some integrations don't need/have a valid starting datetime (like docdir, int8)
     * In that case the message in the booking confirmation does not show the appointment datetime
     * but a message that the appointment will be set later (via telefon, etc).
     *
     * The appointment can be '0000-00-00 00:00:00' or '1970-01-01 00:00:00' because Salesforce
     * needs a valid datetime for datetimes. So we have choosen this datetime as an identifier for
     * unvalid datetimes.
     *
     * @param string $appointment_start_at
     *
     * @return bool
     */
    public static function isValidAppointmentStartAt($appointment_start_at)
    {
        return (
            $appointment_start_at !== '0000-00-00 00:00:00'
            &&
            $appointment_start_at !== '1970-01-01 00:00:00'
            &&
            $appointment_start_at !== null
            &&
            strtotime($appointment_start_at)
        );
    }

    /**
     * @return string
     */
    public function getAppointmentStartAt()
    {
        return $this->appointment_start_at;
    }

    /**
     * Call the integration booking
     *
     * @return bool
     */
    public function integrationBook()
    {
        $_integration = $this->getIntegration();
        $result = $_integration->bookAppointment($this);

        if (!$result) {
            $this->last_error_message = $_integration->getLastErrorMessage();
        }

        return $result;
    }

    /**
     * Returns the integration object
     *
     * @return Integration
     */
    public function getIntegration()
    {
        return Integration::getIntegration($this->getLocation()->getIntegrationId());
    }

    /**
     * True if a booking exists that matches the params
     *
     * @param string $email
     * @param string|DateTime $appointment_start_at
     * @param int|MedicalSpecialty $medical_specialty
     *
     * @return bool
     */
    public function isSimilar($email, $appointment_start_at, $medical_specialty)
    {
        $booking_threshold = intval(Configuration::get('similar_booking_within_days_number'));
        $email = filter_var($email, FILTER_SANITIZE_EMAIL);

        // We only need the ID
        if ($medical_specialty instanceof MedicalSpecialty) {
            $medical_specialty = $medical_specialty->getId();
        }
        $medical_specialty = intval($medical_specialty);

        // We need a date here
        if (!($appointment_start_at instanceof DateTime)) {
            $appointment_start_at = new DateTime($appointment_start_at);
        }

        $in_the_past = clone $appointment_start_at;
        $in_the_past->modify("-{$booking_threshold} days");

        $statuses = Booking::getStatusesSql(Booking::getBookedStatuses(), 'OR');

        $where = " email='{$email}' AND " .
            ($statuses ? " {$statuses} AND " : '') .
            " appointment_start_at <= '{$appointment_start_at->getDateTime()}' AND " .
            " appointment_start_at >= '{$in_the_past->getDateTime()}' AND " .
            " medical_specialty_id = {$medical_specialty}";

        $result = $this->count($where);

        return (bool)$result;
    }

    /**
     * Checks if this location has an appointment enquiry integration
     * (where appointments are not specified like int8 or int9)
     * Dont use the Location->hasAppointmentEnquiryIntegration() method here as
     * the booking can have a different integration as the location has
     * because the integration could have had changed.
     *
     * @return bool
     */
    public function hasAppointmentEnquiryIntegration()
    {
        return $GLOBALS['CONFIG']['INTEGRATIONS'][$this->getIntegrationId()]['APPOINTMENT_ENQUIRY'];
    }

    /**
     * Returns the links to the integration bookings
     *
     * @return string
     */
    public function getIntegrationBookingHtml()
    {
        $html = '';
        $integration = $this->getIntegration();

        // @todo A validity check here
        if (!$integration) {
            return false;
        }

        $integration_bookings = $integration->getIntegrationBookings($this);

        if (is_array($integration_bookings) && !empty($integration_bookings)) {
            foreach ($integration_bookings as $integration_booking) {
                if (is_object($integration_booking) && method_exists($integration_booking, 'getStatusHtml')) {
                    $html .= $integration_booking->getStatusHtml();
                }
            }
        }

        return $html;
    }

    /**
     * Returns the number of mailings for this booking
     *
     * @return int
     */
    public function countBookingMailings()
    {
        $mailing = new Mailing();

        return $mailing->count('source_type_id=' . MAILING_SOURCE_TYPE_ID_BOOKING . ' AND source_id=' . $this->getId());
    }

    /**
     * Returns the mailings for this booking
     *
     * @return string
     */
    public function getBookingMailings()
    {
        $mailing = new Mailing();

        return $mailing->findObjects('source_type_id=' . MAILING_SOURCE_TYPE_ID_BOOKING . ' AND source_id=' . $this->getId());
    }

    /**
     * - Stores a new booking in the DB
     *   The data should be sanitized before
     * - Sends out a mail to support team
     * - Sends out a mail to the patient depending on the integration
     * - Pushs the booking to Salesforce
     *
     * @todo Reconsider injection of User and Location
     *
     * @param array $data
     *
     * @return Booking || bool
     */
    public function create($data)
    {
        $app = Application::getInstance();
        $profile = $app->getCurrentUser();

        // Don't recreate the booking
        if ($this->getId()) {
            return $this->load($this->getId());
        }

        /** @var User $user */
        $user = $data['user'];
        if (!is_object($user) || !$user->isValid()) {
            return false;
        }

        /** @var Location $location */
        $location = $data['location'];
        if (!is_object($location) || !$location->isValid()) {
            return false;
        }

        $data['integration_id'] = $location->getIntegrationId();

        if (empty($data['created_at'])) {
            $data['created_at'] = date('Y-m-d H:i:s');
        }

        if (empty($data['created_by'])) {
            $data['created_by'] = $profile->getId();
        }

        if (empty($data['status'])) {
            $data['status'] = self::STATUS_NEW;
        }

        if (empty($data['slug'])) {
            $data['slug'] = $this->getNewSlug();
        }

        $data['ip_address'] = $_SERVER['REMOTE_ADDR'];
        $data['source_platform'] = $app->getSourcePlatform();
        $data['salesforce_updated_at'] = $data['created_at'];
        $data['flexible_location'] = '0';
        $data['updated_by'] = $profile->getId();
        $data['dirty'] = 0;
        $data['updated_confirmed_at'] = $data['created_at'];
        if(!isset($data['insurance_provider_id'])) {
            $data['insurance_provider_id'] = 1;
        }
        $this->setContactPreference($data['contact_preference']);
        $data['contact_preference'] = $this->contact_preference;


        $new_booking_id = $this->saveNew($data);
        $this->load($new_booking_id);

        if (!$this->isValid()) {
            return false;
        }

        if ($GLOBALS['CONFIG']['SALESFORCE_ENABLE']) {
            // push the new booking to salesforce
            $this->createInSalesforce($data['referral_code']);
        }

        return $this;
    }

    /**
     * Returns a new Slug Code
     *
     * @return string
     */
    public function getNewSlug()
    {
        return substr(md5(uniqid()), 0, 12);
    }

    /**
     * Returns a new Slug Code
     *
     * @return string
     */
    public function logSalesforceError($errormessage)
    {
        $this->setCommentIntern($this->getCommentIntern()."\n".$errormessage);
        $this->save(
            array(
                'comment_intern',
            )
        );
    }

    /**
     * Create the booking in salesforce
     *
     * @return bool
     */
    public function createInSalesforce($referral_code = null)
    {
        // if the booking was already created than don't do it again
        if (Calendar::isValidDateTime($this->getSalesforceUpdatedAt()) && $this->getSalesforceId()) {
            return false;
        }

        try {
            $salesforce_interface = new \SalesforceInterface();

            if ($this->getSalesforceId() !== null && $this->getSalesforceId() != '' && strlen($this->getSalesforceId()) > 0) {
                // There is an salesforce_id but no salesforce_updated_at datetime,
                // so get the salesforce created_at date and store that
                $select = "Id, CreatedDate";
                $where = "Id = '" . $this->getSalesforceId() . "'";
                $result = $salesforce_interface->query($select, $where);

                if (!is_object($result)) {
                    $this->logSalesforceError('ERROR: wrong salesforce_id');
                    error_log('wrong salesforce_id '.$this->getSalesforceId().' on createInSalesforce');
                    return false;
                }

                // Update the salesforce timestamp and make clear that this booking was synced
                $this->setSalesforceUpdatedAt(date('Y-m-d H:i:s', strtotime($result->CreatedDate)));
                $this->save('salesforce_updated_at');

                return true;
            }

            if(!filter_var($this->getEmail(), FILTER_VALIDATE_EMAIL)) {
                $this->logSalesforceError('ERROR: missing or wrong required field email');
                error_log('missing or wrong required field email in booking ' . $this->getId() . ' on createInSalesforce');
                return false;
            }

            if(strlen($this->getPhone()) > 40) {
                $this->logSalesforceError('ERROR: field phone is to long');
                error_log('field phone is to long ('.strlen($this->getPhone()).') in booking ' . $this->getId() . ' on createInSalesforce');
                return false;
            }


            // This is a new booking, so sync it to salesforce
            $location = $this->getLocation();
            $user = $this->getUser();

            // Get medical specialty name for the specific doctor
            $medical_specialty = new MedicalSpecialty($this->getMedicalSpecialtyId());
            $medical_specialty_name = $medical_specialty->getName();

            $salesforce_id = $salesforce_interface->upsertBooking(
                $this->getId(),
                $this->getGender(),
                $this->getFirstName(),
                $this->getLastName(),
                $this->getEmail(),
                $this->getPhone(),
                $this->getSourcePlatform(),
                $this->getTreatmenttypeId(),
                $this->getInsuranceId(),
                $this->getAppointmentStartAt(),
                $location->getSalesforceId(),
                $medical_specialty_name,
                $this->getStatus(),
                $this->getReturningVisitor(),
                $user->getName(),
                $user->hasContract(),
                $this->getCommentPatient(),
                Integration::findName($this->getIntegrationId()),
                $this->getFlexibleLocation(),
                $this->getSourcePlatform(),
                $this->getBookingOfferSlug(),
                $referral_code,
                $this->contact_preference
            );

            // fail || update? (should be insert)
            if (is_bool($salesforce_id)) {
                error_log('upsertBooking failed for booking '.$this->getId().' on createInSalesforce');
                return false;
            }

            // Set the new salesforce_id
            $this->setSalesforceId($salesforce_id);

            // Update the salesforce timestamp and make clear that this booking was synced
            $this->setSalesforceUpdatedAt(Application::getInstance()->now());

            $this->save(
                array(
                    'salesforce_id',
                    'salesforce_updated_at'
                )
            );
        } catch (\Exception $e) {
            $this->logSalesforceError('ERROR: '.$e->getMessage());
            error_log("Exception in Salesforce. Details: {$e->getMessage()}");

            return false;
        }

        return true;
    }

    /**
     * @return string
     */
    public function getSalesforceUpdatedAt()
    {
        return $this->salesforce_updated_at;
    }

    /**
     * @param string $salesforce_updated_at
     *
     * @return self
     */
    public function setSalesforceUpdatedAt($salesforce_updated_at)
    {
        $this->salesforce_updated_at = $salesforce_updated_at;

        return $this;
    }

    /**
     * Returns the user
     *
     * @return User
     */
    public function getUser()
    {
        return new User($this->user_id);
    }

    /**
     * Returns the patients gender according to User::GENDER_
     *
     * @return int
     */
    public function getGender()
    {
        return intval($this->gender);
    }

    /**
     * Returns the first name
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Returns the last name
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Returns the email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Returns the phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Returns the source platform string
     *
     * @return string
     */
    public function getSourcePlatform()
    {
        return $this->source_platform;
    }

    /**
     * Returns the insurance_id
     *
     * @return string
     */
    public function getInsuranceId()
    {
        return $this->insurance_id;
    }

    /**
     * Returns the status id
     * If a status text is given then returns the id.
     * If no param is given it returns the objects status id
     *
     * @param string $status_text (optional)
     *
     * @return int
     */
    public function getStatus($status_text = null)
    {
        if (empty($status_text)) {
            return $this->status;
        } else {
            $flipped_statuses = array_flip(self::$STATUS_TEXTS);

            if (isset($flipped_statuses[$status_text])) {
                return $flipped_statuses[$status_text];
            }
        }

        return 0;
    }

    /**
     * @param bool $show_dirty Do magic to show the "actual" value of the value (including changes made by the doctor)
     * 
     * @return bool
     */
    public function getReturningVisitor($show_dirty = false)
    {
        return 
            $show_dirty && $this->getDirty() ?  // Should we show dirty, AND are we dirty?
                !$this->returning_visitor :     // If yes, then invert the actual value (i.e. doctor has changed it)
                (bool)$this->returning_visitor; // Otherwise just return the basic value
    }

    /**
     * @return bool
     */
    public function getDirty()
    {
        return (bool)$this->dirty;
    }

    /**
     * @param bool|int $dirty
     *
     * @return self
     */
    public function setDirty($dirty)
    {
        $this->dirty = intval($dirty);

        return $this;
    }

    /**
     * @return array
     */
    public function getContactPreference()
    {
        return explode(',',$this->contact_preference);
    }

    /**
     * @param string $contact_preference
     */
    public function setContactPreference($contact_preference)
    {
        $this->contact_preference = implode(',',$contact_preference);
    }

    /**
     * @return string
     */
    public function getRecallPhone()
    {
        return $this->recall_phone;
    }

    /**
     * @param string $recall_phone
     */
    public function setRecallPhone($recall_phone)
    {
        $this->recall_phone = $recall_phone;
    }

    /**
     * @return array
     */
    public function getRecallPreference()
    {
        return explode(',',$this->recall_preference);
    }

    /**
     * @param string $recall_preference
     */
    public function setRecallPreference($recall_preference)
    {
        $this->recall_preference = implode(',',$recall_preference);
    }

    /**
     * Returns the patient comment
     *
     * @return string
     */
    public function getCommentPatient()
    {
        return $this->comment_patient;
    }

    /**
     * Returns if the patient has flexible location
     *
     * @return bool
     */
    public function getFlexibleLocation()
    {
        return (bool)$this->flexible_location;
    }

    /**
     * @param string $salesforce_id
     *
     * @return self
     */
    public function setSalesforceId($salesforce_id)
    {
        $this->salesforce_id = $salesforce_id;

        return $this;
    }

    /**
     * Filter display status in reporting to one of these three
     * 
     * @todo Check that this default is sane
     * 
     * @return int
     */
    public function getReportingStatus()
    {
        switch ($this->getStatus()) {
            case Booking::STATUS_RECEIVED:
            case Booking::STATUS_RECEIVED_ALTERNATIVE:
            case Booking::STATUS_RECEIVED_ALTERNATIVE_PENDING:
            case Booking::STATUS_RECEIVED_UNCONFIRMED:
                $reporting_status = 1;
                break;
            case Booking::STATUS_CANCELLED_BY_PATIENT_LOGIN:
            case Booking::STATUS_CANCELLED_BY_PATIENT:
                $reporting_status = 2;
                break;
            case Booking::STATUS_NOSHOW:
                $reporting_status = 4;
                break;
            default:
                $reporting_status = 0;
                break;
        }

        return $reporting_status;
    }

    /**
     * Method mapping the above to actual text
     * 
     * @todo Remove in favour of translations
     * 
     * @return string
     */
    public function getReportingStatusText()
    {
        switch($this->getReportingStatus()) {
            case 1:
                $reporting_status = 'Bestätigt';
                break;
            case 2:
                $reporting_status = 'Storniert';
                break;
            case 4:
                $reporting_status = 'No show';
                break;
            default:
                $reporting_status = '';
        }
        
        return $reporting_status;
    }

    /**
     * Update SF with an arbitrary array
     *
     * @param array $fields
     *
     * @return bool
     */
    public function sendToMadmimi($email, $name)
    {
        global $CONFIG;

        $connection = new Connection($CONFIG['MADMIMI_USER'], $CONFIG['MADMIMI_TOKEN'], new CurlRequest());

        $options = new Transactional();
        $options->setPromotionName($CONFIG['MADMIMI_NEWSLETTER_PROMOTION'])
            ->setTo($email, $name);

        $transactionId = $connection->request($options);
    }

    /**
     * Update SF with an arbitrary array
     *
     * @param array $fields
     *
     * @return bool
     */
    public function updateSalesforce(array $fields = array())
    {
        if (!$GLOBALS['CONFIG']['SALESFORCE_ENABLE']) {
            return true;
        }

        // Make sure the fields array has all our keys
        // @todo NO NO NO NO NO NO NO
        foreach ($fields as $key => $value) {
            $this->$key = $value;
        }

        try {
            $salesforce_interface = new \SalesforceInterface();

            if (!$this->getSalesforceId()) {
                return false;
            }

            // this is a new booking, so sync it to salesforce
            $location = $this->getLocation();
            $user = $this->getUser();

            // Get medical specialty name for the specific doctor
            $medical_specialty = new MedicalSpecialty($this->getMedicalSpecialtyId());
            $medical_specialty_name = $medical_specialty->getName();

            // get the coupon code
            // @todo: Move away from Booking object
            $coupon = new UsedCoupon($this->getId(), 'booking_id');
            $couponCode = null;
            if ($coupon->isValid()) {
                $couponCode = $coupon->getCouponCode();
            }

            $upsert_result = $salesforce_interface->upsertBooking(
                $this->getId(),
                $this->getGender(),
                $this->getFirstName(),
                $this->getLastName(),
                $this->getEmail(),
                $this->getPhone(),
                $this->getSourcePlatform(),
                $this->getTreatmenttypeId(),
                $this->getInsuranceId(),
                $this->getAppointmentStartAt(),
                $location->getSalesforceId(),
                $medical_specialty_name,
                $this->getStatus(),
                $this->getReturningVisitor(),
                $user->getName(),
                $user->hasContract(),
                $this->getCommentPatient(),
                Integration::findName($this->getIntegrationId()),
                $this->getFlexibleLocation(),
                $this->getSourcePlatform(),
                $this->getBookingOfferSlug(),
                $couponCode,
                $this->contact_preference,
                $this->recall_preference,
                $this->getRecallPhone()
            );

            // fail || update? (should be insert)
            if ($upsert_result === false) {
                return false;
            }
            // update the salesforce timestamp and make clear that this booking was synced
            $this->setSalesforceUpdatedAt(Application::getInstance()->now());
            $this->save(array('salesforce_updated_at'));
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @return string
     */
    public function getBookingOfferSlug()
    {
        return $this->booking_offer_slug;
    }

    /**
     * @return Offer[]
     */
    public function getBookingOffers()
    {
        $bookingOffer = new Offer();
        return $bookingOffer->findObjects('booking_id='.$this->id);
    }

    /**
     * Cancel this booking
     *
     * @param int $status
     *
     * @return bool
     */
    public function cancel($status)
    {
        if (!$this->isCancelable()) {
            return false;
        }

        $this->saveStatus($status, true);

        // Also cancel all pending booking offers
        $this->cancelOffers();

        // tell the integration to cancel the booking
        $this->getIntegration()->cancelBooking($this);

        return true;
    }

    /**
     * Checks if the booking can be canceled by its current status
     *
     * @return bool
     */
    public function isCancelable()
    {
        $is_cancelable = false;

        switch ($this->getStatus()) {
            case self::STATUS_NEW:
            case self::STATUS_PENDING:
            case self::STATUS_RECEIVED:
            case self::STATUS_RECEIVED_ALTERNATIVE:
            case self::STATUS_RECEIVED_ALTERNATIVE_PENDING:
            case self::STATUS_RECEIVED_UNCONFIRMED:
            case self::STATUS_PENDING_CALLBACK:
            case self::STATUS_PENDING_LOCATION_NOT_REACHED:
                $is_cancelable = true;
        }

        return $is_cancelable;
    }

    /**
     * Save the status
     *
     * @param int $status
     * @param bool $update_salesforce
     *
     * @return bool
     */
    public function saveStatus($status, $update_salesforce = false)
    {
        $this->status = $status;
        $result = $this->save(array('status'));

        if ($result && $update_salesforce) {

            // save the new status in Salesforce
            if ($GLOBALS['CONFIG']['SALESFORCE_ENABLE']) {
                try {
                    $user = $this->getUser();

                    if (!$user->isValid()) {
                        return false;
                    }

                    $salesforce_interface = new \SalesforceInterface();
                    $result = $salesforce_interface->upsertBooking(
                        $this->getId(), // ID
                        null, // Gender
                        null, // First name
                        null, // Lastname
                        null, // Email
                        null, // Telephone
                        null, // Source (unused)
                        null, // $behandlungsgrund
                        null, // Insurance
                        null, // Appointment time
                        null, // Appointment at
                        null, // Specialty
                        $this->getStatus(), // Status
                        null, // Returning visitor
                        null, // Doctor name
                        $user->hasContract() // Contract
                    );

                    if ($result) {
                        // store the successful upsert
                        $this->setSalesforceUpdatedAt(Application::getInstance()->now());
                        $this->save('salesforce_updated_at');
                    }
                } catch (\Exception $e) {
                    $result = false;
                }
            }
        }

        return $result;
    }

    /**
     * Cancel offers for this booking
     *
     * @todo: Clean this up
     */
    public function cancelOffers()
    {
        $bookingOffer = new Offer();
        $bookingOffers = $bookingOffer->findObjects('booking_id=' . $this->getId() . ' AND (status=' . Offer::STATUS_NEW . ' OR status=' . Offer::STATUS_PENDING . ')');

        if (!empty($bookingOffers)) {
            $bookingOffer->setBookingId($this->getId());
            $bookingOffer->updateOffers(Offer::STATUS_REJECTED);
        }
    }

    /**
     * Checks if there are unanswered booking offers for this booking
     *
     * @return bool
     */
    public function hasUnansweredBookingOffers()
    {
        $bookingOffer = new Offer();

        return $bookingOffer->isOpen($this->getId());
    }

    /**
     * This function gets the bookings which appointment date is after given days
     * and has a decent integration
     *
     * @param int $days
     * @param int $integrationId
     * @param array $bookingStatuses
     *
     * @return Booking[]
     */
    public function getBookingsByDaysAfter($days, $integrationId, $bookingStatuses)
    {
        // build the query for all statuses
        $bookingStatusesSql = Booking::getStatusesSql($bookingStatuses, 'OR', 'b');
        $bookingStatusesSql = ($bookingStatusesSql == '') ? '' : ' AND (' . $bookingStatusesSql . ')';

        $query = sprintf(
            "SELECT b.id FROM %s AS b, %s AS u, %s AS l
              WHERE
                b.`user_id` = u.`id`
              AND
                b.`location_id` = l.`id`
              AND
                DATE(`appointment_start_at`) = DATE_ADD(CURRENT_DATE, INTERVAL %d DAY)
              AND
                l.`integration_id`=%d
              %s",
            DB_TABLENAME_BOOKINGS,
            DB_TABLENAME_USERS,
            DB_TABLENAME_LOCATIONS,
            $days,
            $integrationId,
            $bookingStatusesSql
        );

        return $this->findObjectsSql($query);
    }

    /**
     * Returns the "OR" or "AND" sql string for a given statuses array
     *
     * @param array $statuses
     * @param string $bool_check (OR or AND)
     * @param string $tablename (for sql shortcuts like "bookings AS b")
     *
     * @return string
     */
    public static function getStatusesSql($statuses, $bool_check, $tablename = DB_TABLENAME_BOOKINGS)
    {
        $sql = '';

        if (is_array($statuses) && !empty($statuses)) {
            foreach ($statuses as $status) {
                $sql .= ($sql == '') ? '' : " {$bool_check} ";
                $sql .= $tablename . '.`status`=' . $status;
            }

            $sql = ($sql == '') ? '' : '(' . $sql . ')';
        }

        return $sql;
    }
}
