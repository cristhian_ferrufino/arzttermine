<?php

namespace Arzttermine\Insurance;

use Arzttermine\Core\Basic;

class Provider extends Basic {

    /**
     * @var string
     */
    protected $tablename = DB_TABLENAME_INSURANCE_PROVIDERS;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var int
     */
    protected $order;

    /**
     * @var array
     */
    protected $all_keys
        = array(
            "id",
            "name",
            "order"
        );

    /**
     * @var array
     */
    protected $secure_keys
        = array(
            "id",
            "name",
            "order"
        );

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return $this->order;
    }
}