<?php

namespace Arzttermine\Insurance;

use Arzttermine\Application\Application;
use Arzttermine\Core\Basic;

class Insurance extends Basic {

    const STATUS_DRAFT = 0;
    const STATUS_ACTIVE = 1;

    /**
     * Privat Krankenversicherung (Private Health Insurance)
     */
    const PKV = 1;
    
    /**
     * Gesetzlich Krankenversicherung (Public Health Insurance)
     */
    const GKV = 2;

    /**
     * @var string
     */
    protected $tablename = DB_TABLENAME_INSURANCES;

    /**
     * @var int
     */
    protected $parent_id;

    /**
     * @var int
     */
    protected $status;

    /**
     * @var string
     */
    protected $slug_de;

    /**
     * @var string
     */
    protected $slug_en;

    /**
     * @var string
     */
    protected $slug_es;

    /**
     * @var string
     */
    protected $name_de;

    /**
     * @var string
     */
    protected $name_en;

    /**
     * @var string
     */
    protected $name_es;

    /**
     * @var string
     */
    protected $created_at;

    /**
     * @var string
     */
    protected $created_by;

    /**
     * @var string
     */
    protected $updated_at;

    /**
     * @var string
     */
    protected $updated_by;

    /**
     * @var array
     */
    protected $all_keys
        = array(
            "id",
            "parent_id",
            "status",
            "slug_de",
            "slug_en",
            "name_de",
            "name_en",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by"
        );

    /**
     * @var array
     */
    protected $secure_keys
        = array(
            "id",
            "parent_id",
            "status",
            "slug_de",
            "slug_en",
            "name_de",
            "name_en",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by"
        );

    /**
     * {@inheritdoc}
     */
    public function load($value, $key = 'id')
    {
        // We need to translate the slug key before loading it
        if ($key == 'slug') {
            $key .= "_" . Application::getInstance()->getLanguageCode();
        }

        return parent::load($value, $key);
    }
    
    /**
     * Returns the status id
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns the name in the current language
     *
     * @param string $lang
     *
     * @return string
     */
    public function getName($lang = '')
    {
        $attr = 'name_' . ($lang ? $lang : Application::getInstance()->getLanguageCode());

        if (isset($this->$attr)) {
            return $this->$attr;
        }

        return '';
    }

    /**
     * Returns the name in the current language
     *
     * @param int $id
     * @param string $lang
     *
     * @return string
     */
    public static function getNameById($id, $lang = '')
    {
        $insurance = new Insurance($id);

        if ($insurance->isValid()) {
            return $insurance->getName($lang);
        }

        return '';
    }

    /**
     * Checks if the id exists
     *
     * @param int $id
     *
     * @return bool
     */
    public static function idExists($id)
    {
        $insurance = new Insurance($id, 'id');

        return $insurance->isValid();
    }

    /**
     * Returns the name as singular or plural in the current language
     *
     * @return string
     */
    public function getSlug()
    {
        $attr = 'slug_' . Application::getInstance()->getLanguageCode();

        if (isset($this->$attr)) {
            return $this->$attr;
        }

        return '';
    }

    /**
     * Returns the names
     *
     * @param bool $add_zero
     *
     * @return string
     */
    public static function getHtmlOptions($add_zero = false)
    {
        $options = array();

        if ($add_zero) {
            $options[0] = '...bitte wählen...';
        }

        $insurance = new Insurance();
        /** @var Insurance[] $insurances */
        $insurances = $insurance->findObjects('status=' . Insurance::STATUS_ACTIVE);

        if (!empty($insurances) && is_array($insurances)) {
            foreach ($insurances as $_insurance) {
                $options[$_insurance->getId()] = $_insurance->getName();
            }
        }

        return $options;
    }

    /**
     * @param string $created_at
     *
     * @return self
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param string $created_by
     *
     * @return self
     */
    public function setCreatedBy($created_by)
    {
        $this->created_by = $created_by;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * @param string $name_de
     *
     * @return self
     */
    public function setNameDe($name_de)
    {
        $this->name_de = $name_de;

        return $this;
    }

    /**
     * @return string
     */
    public function getNameDe()
    {
        return $this->name_de;
    }

    /**
     * @param string $name_en
     *
     * @return self
     */
    public function setNameEn($name_en)
    {
        $this->name_en = $name_en;

        return $this;
    }

    /**
     * @return string
     */
    public function getNameEn()
    {
        return $this->name_en;
    }

    /**
     * @param int $parent_id
     *
     * @return self
     */
    public function setParentId($parent_id)
    {
        $this->parent_id = $parent_id;

        return $this;
    }

    /**
     * @return int
     */
    public function getParentId()
    {
        return $this->parent_id;
    }

    /**
     * @param string $slug_de
     *
     * @return self
     */
    public function setSlugDe($slug_de)
    {
        $this->slug_de = $slug_de;

        return $this;
    }

    /**
     * @return string
     */
    public function getSlugDe()
    {
        return $this->slug_de;
    }

    /**
     * @param string $slug_en
     *
     * @return self
     */
    public function setSlugEn($slug_en)
    {
        $this->slug_en = $slug_en;

        return $this;
    }

    /**
     * @return string
     */
    public function getSlugEn()
    {
        return $this->slug_en;
    }

    /**
     * @param string $updated_at
     *
     * @return self
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param string $updated_by
     *
     * @return self
     */
    public function setUpdatedBy($updated_by)
    {
        $this->updated_by = $updated_by;

        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedBy()
    {
        return $this->updated_by;
    }
}
