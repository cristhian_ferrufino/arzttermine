<?php

namespace Arzttermine\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerInterface;

interface ServiceProviderInterface {
    
    /**
     * Registers services in the container
     *
     * This method should only configure/create services and parameters.
     * It should NOT get services from the container
     * 
     * @param ContainerInterface $container
     */
    public function register(ContainerInterface $container);

    /**
     * Bootstrapping of the service. Full configuration goes here, including event registration
     *
     * @param ContainerInterface $container
     */
    public function boot(ContainerInterface $container);
}