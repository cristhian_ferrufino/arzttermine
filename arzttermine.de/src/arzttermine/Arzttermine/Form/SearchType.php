<?php

namespace Arzttermine\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class SearchType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options = array())
    {
        $builder
            // Location fields
            ->add('location', TextType::class, array('constraints' => array(new Length(array('min' => 2)), new NotBlank())))
            ->add('district_id', IntegerType::class)
            ->add('distance', IntegerType::class)
        
            // Medical fields
            ->add('insurance_id', IntegerType::class, array('constraints' => new NotBlank()))
            ->add('medical_specialty_id', IntegerType::class, array('constraints' => new NotBlank()))
            ->add('treatmenttype_id', IntegerType::class);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'search';
    }
}