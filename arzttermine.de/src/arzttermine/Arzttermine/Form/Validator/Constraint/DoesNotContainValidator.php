<?php

namespace Arzttermine\Form\Validator\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class DoesNotContainValidator extends ConstraintValidator {

    /**
     * @var array
     */
    protected $patterns = array(
        DoesNotContain::TYPE_URL           => '/((https?\:\/\/)|(www[ ]*[.]{1})){1}[ -a-z0-9]+[.]{1}[ ]*[a-z]{2,4}/i',
        DoesNotContain::TYPE_EMAIL_ADDRESS => '/([\s]*)([_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*([ ]+|)@([ ]+|)([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,}))([\s]*)/i',
        DoesNotContain::TYPE_PHONE_NUMBER  => '/^([\+][0-9]{1,3}[ \.\-])?([\(]{1}[0-9]{1,6}[\)])?([0-9 \.\-\/]{3,20})((x|ext|extension)[ ]?[0-9]{1,4})?$/'
        
    );
    
    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint)
    {
        $input = $this->normalize($value);
        
        /** @var DoesNotContain $constraint */
        foreach(array(DoesNotContain::TYPE_URL, DoesNotContain::TYPE_EMAIL_ADDRESS, DoesNotContain::TYPE_PHONE_NUMBER) as $type) {
            if ($constraint->flags & $type) {
                if (preg_match($this->patterns[$type], $input) === 1) {
                    $this->context->addViolation($constraint->getMessage($type));
                }
            }
        }
    }

    /**
     * Normalize the string
     * 
     * @param string $input
     *
     * @return string
     */
    protected function normalize($input)
    {
        $numbers = array(
            'en' => array(
                'zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'
            ),
            'de' => array(
                'null', 'eins', 'zwei', 'drei', 'vier', 'fünf', 'sechs', 'sieben', 'acht', 'neun'
            )
        );

        $input = str_replace($numbers['en'], array_keys($numbers['en']), $input);
        $input = str_replace($numbers['de'], array_keys($numbers['de']), $input);
        $input = str_replace(array('[dot]', '[at]'), array('.', '@'), $input);
        
        return $input;
    }
    
}