<?php

namespace Arzttermine\Form\Validator\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class DoesNotContain extends Constraint {

    // Types to check for
    const TYPE_URL           = 1;
    const TYPE_EMAIL_ADDRESS = 2;
    const TYPE_PHONE_NUMBER  = 4;

    /**
     * @var array
     */
    protected $messages = array(
        self::TYPE_URL           => 'Bitte entschuldigen Sie, es ist nicht möglich Ihre Website Öffentlich in Ihrem Profil anzuzeigen. Für Fragen und Hilfe steht Ihnen unser Support unter Tel. 030 609 840 229 jederzeit zur Verfügung.',
        self::TYPE_EMAIL_ADDRESS => 'Bitte entschuldigen Sie, es ist nicht möglich eine Emailadresse Öffentlich in Ihrem Profil anzuzeigen. Für Fragen und Hilfe steht Ihnen unser Support unter Tel. 030 609 840 229 jederzeit zur Verfügung.',
        self::TYPE_PHONE_NUMBER  => 'Bitte entschuldigen Sie, es ist nicht möglich die Telefonnummer in Ihrem Profil zu Ändern. Für Fragen und Hilfe steht Ihnen unser Support unter Tel. 030 609 840 229 jederzeit zur Verfügung.'
    );

    /**
     * @var string
     */
    public $message = 'This field contains invalid data';

    /**
     * @var int
     */
    public $flags = 0;

    /**
     * @param array|int $options
     */
    public function __construct($options = null)
    {
        if (!empty($options) && !is_array($options)) {
            $options = array('flags' => intval($options));
        }
        
        parent::__construct($options);
    }
    
    /**
     * @param int $type
     * 
     * @return string
     */
    public function getMessage($type)
    {
        if (array_key_exists($type, $this->messages)) {
            return $this->messages[$type];
        }
        
        return '';
    }
}