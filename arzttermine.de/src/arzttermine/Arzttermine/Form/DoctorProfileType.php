<?php

namespace Arzttermine\Form;

use Arzttermine\Form\Validator\Constraint\DoesNotContain;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Form used to update the doctor's profile ONLY in the doctor's area
 * @todo Extend to admin
 *
 * @package Arzttermine\Form
 */
class DoctorProfileType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options = array())
    {
        $builder
            // Anything with a content validator (length, email) also needs a NotBlank validator, or it won't validate it :S
            ->add('title',      TextType::class, array('constraints' => array(new Length(array('min' => 2)))))
            ->add('first_name', TextType::class, array('constraints' => array(new NotBlank(), new Length(array('min' => 2)))))
            ->add('last_name',  TextType::class, array('constraints' => array(new NotBlank(), new Length(array('min' => 2)))))

            ->add('uid', TextType::class, array('constraints' => array(new NotBlank())))

            // Standard non-required doctor info
            ->add('docinfo_1', TextType::class, array('constraints' => array(new DoesNotContain(DoesNotContain::TYPE_URL + DoesNotContain::TYPE_EMAIL_ADDRESS + DoesNotContain::TYPE_PHONE_NUMBER))))
            ->add('docinfo_2', TextType::class, array('constraints' => array(new DoesNotContain(DoesNotContain::TYPE_EMAIL_ADDRESS + DoesNotContain::TYPE_PHONE_NUMBER))))
            ->add('docinfo_3', TextType::class, array('constraints' => array(new DoesNotContain(DoesNotContain::TYPE_URL + DoesNotContain::TYPE_EMAIL_ADDRESS + DoesNotContain::TYPE_PHONE_NUMBER))))
            ->add('docinfo_4', TextType::class, array('constraints' => array(new DoesNotContain(DoesNotContain::TYPE_URL + DoesNotContain::TYPE_EMAIL_ADDRESS + DoesNotContain::TYPE_PHONE_NUMBER))))
            ->add('docinfo_5', TextType::class, array('constraints' => array(new DoesNotContain(DoesNotContain::TYPE_URL + DoesNotContain::TYPE_EMAIL_ADDRESS + DoesNotContain::TYPE_PHONE_NUMBER))))
            ->add('docinfo_6', TextType::class, array('constraints' => array(new DoesNotContain(DoesNotContain::TYPE_URL + DoesNotContain::TYPE_EMAIL_ADDRESS + DoesNotContain::TYPE_PHONE_NUMBER))))
            
            // Medical specialties and associated treatment types as a collection of text objects
            ->add('medical_specialties_ids', CollectionType::class, array('entry_type' => TextType::class, 'allow_add' => true, 'allow_delete' => true))
            ->add('treatment_types', CollectionType::class, array('entry_type' => TextType::class, 'allow_add' => true, 'allow_delete' => true))

            ->add('current_password', PasswordType::class)
            ->add('new_password',     PasswordType::class);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'doctor_profile';
    }
}