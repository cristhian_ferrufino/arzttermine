<?php

namespace Arzttermine\Form;

use Arzttermine\User\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Count;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Regex;

class BookingType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options = array())
    {
        $builder
            // Personal details
            ->add('gender', ChoiceType::class, array(
                    'choices' => array(
                        'Male' => User::GENDER_MALE,
                        'Female' => User::GENDER_FEMALE
                    ),
                    'constraints' => array(new NotBlank())
                )
            ) // Labels unused
            
            // Anything with a content validator (length, email) also needs a NotBlank validator, or it won't validate it :S
            ->add('first_name', TextType::class, array('constraints' => array(new NotBlank(), new Length(array('min' => 2)))))
            ->add('last_name',  TextType::class, array('constraints' => array(new NotBlank(), new Length(array('min' => 2)))))
            ->add('email',      TextType::class, array('constraints' => array(new NotBlank(), new Email())))
            ->add('phone',      TextType::class, array('constraints' => array(new Regex(array('pattern' => '/^([0-9\(\)\/\+ \-]*)$/i',)), new NotBlank(), new Length(array('min' => 6)))))
            ->add('contact_preference', CollectionType::class, array('constraints' => new Count(array('min' => 1, 'minMessage' => "min contact preference")), 'entry_type' => TextType::class, 'allow_add' => true, 'allow_delete' => true))


            // Booking details
            // Note: Types (2nd param) are currently only used as a guide/validation only
            ->add('user_id', IntegerType::class, array('constraints' => new NotBlank()))
            ->add('location_id', IntegerType::class, array('constraints' => new NotBlank()))
            ->add('insurance_id', TextType::class, array('constraints' => new NotBlank()))
            ->add('insurance_provider_id', TextType::class)
            ->add('treatmenttype_id', TextType::class)
            ->add('medical_specialty_id', TextType::class)
            ->add('form_once', TextType::class) // @todo Check validity
            ->add('returning_visitor', ChoiceType::class, array(
                    'choices' => array(
                        'No' => 0,
                        'Yes' => 1
                    ),
                    'constraints' => array(new NotBlank())
                )
            ) // Labels unused

            // Booking times. These can currently be two formats :/
            ->add('start_at_id', TextType::class)
            ->add('appointment_start_at', TextType::class)
            ->add('end_at_id', TextType::class)
            ->add('appointment_end_at', TextType::class)

            // Miscellaneous info
            // @todo Remove/refactor some of this
            ->add('agb', null, array('constraints' => array(new IsTrue(), new NotBlank()))) // For some reason, the checkbox type breaks
            ->add('newsletter_subscription', null)
            ->add('flexible_location', null)
            ->add('comment_patient', TextType::class)
            ->add('available_insurance_ids', TextType::class)
            ->add('referral_code', TextType::class)
            ->add('referral_code_confirmation', TextType::class);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'booking';
    }
}