<?php

namespace Arzttermine\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ContactType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options = array())
    {
        $builder
            ->add('name', TextType::class, array('constraints' => array(new NotBlank(), new Length(array('min' => 2)))))
            ->add('email', TextType::class, array('constraints' => array(new NotBlank(), new Email())))
            ->add('subject', TextType::class, array('constraints' => array(new NotBlank(), new Length(array('min' => 2)))))
            ->add('message', TextareaType::class, array('constraints' => array(new NotBlank(), new Length(array('min' => 4)))));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'contact';
    }
}