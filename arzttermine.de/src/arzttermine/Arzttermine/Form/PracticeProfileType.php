<?php

namespace Arzttermine\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Form used to update the doctor's profile ONLY in the doctor's area
 * @todo Extend to admin
 *
 * @package Arzttermine\Form
 */
class PracticeProfileType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options = array())
    {
        $builder
            // Anything with a content validator (length, email) also needs a NotBlank validator, or it won't validate it :S
            ->add('phone', TextType::class, array('required'=>false, 'constraints' => array(new Length(array('min' => 2)))))
            ->add('www',   UrlType::class, array('required'=>false, 'constraints' => array(new Length(array('min' => 2)))))

            ->add('uid', TextType::class, array('constraints' => array(new NotBlank())))

            // Medical specialties and associated treatment types as a collection of text objects
            ->add('medical_specialties_ids', CollectionType::class, array('entry_type' => TextType::class, 'allow_add' => true, 'allow_delete' => true, 'entry_options' => array('constraints' => array(new NotBlank()))));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'doctor_location';
    }
}