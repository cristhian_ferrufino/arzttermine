<?php

namespace Arzttermine\Form;

use Arzttermine\User\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * @package Arzttermine\Form
 */
class DoctorRegistrationType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options = array())
    {
        $builder
            // Anything with a content validator (length, email) also needs a NotBlank validator, or it won't validate it :S
            ->add('gender', ChoiceType::class, array(
                    'choices' => array(
                        'Male' => User::GENDER_MALE,
                        'Female' => User::GENDER_FEMALE
                    ), // This is also apparently optional during registration...
                    'empty_data' => User::GENDER_MALE // ... so we default to male :/
                )
            ) // Labels unused
            ->add('title',      TextType::class, array('required' => false, 'constraints' => array(new Length(array('min' => 2))), 'invalid_message' => 'The title must be at least 2 characters long')) // Title is optional during registration
            ->add('first_name', TextType::class, array('constraints' => array(new NotBlank(), new Length(array('min' => 2))), 'invalid_message' => 'Bitte geben Sie Ihren Vornamen ein'))
            ->add('last_name',  TextType::class, array('constraints' => array(new NotBlank(), new Length(array('min' => 2))), 'invalid_message' => 'Bitte geben Sie Ihren Nachnamen ein'))
            ->add('email',      EmailType::class, array('constraints' => array(new Email(), new Length(array('min' => 6))), 'invalid_message' => 'Bitte geben Sie Ihre Email ein'))

            ->add('practice_name', TextType::class, array('constraints' => array(new NotBlank(), new Length(array('min' => 2))), 'invalid_message' => 'Bitte geben Sie Ihren Praxisnamen an'))
            ->add('city', TextType::class, array('constraints' => array(new NotBlank(), new Length(array('min' => 2))),          'invalid_message' => 'Bitte geben Sie Ihre Stadt an'))
            ->add('zip', TextType::class, array('constraints' => array(new NotBlank(), new Length(array('min' => 2))),          'invalid_message' => 'Bitte geben Sie Ihre PLZ an'))
            ->add('url', UrlType::class, array('constraints' => array(new NotBlank(), new Length(array('min' => 2))),          'invalid_message' => 'Bitte geben Sie Ihre Firmen-Websiteadresse an'))
            ->add('doctors_count', IntegerType::class, array('constraints' => array(new NotBlank()),          'invalid_message' => 'Bitte geben Sie die Anzahl der Ärzte in Ihrer Praxis an'))
            ->add('registration_reasons', TextType::class, array('constraints' => array(new NotBlank())))
            ->add('street', TextType::class, array('constraints' => array(new NotBlank(), new Length(array('min' => 2))),          'invalid_message' => 'Bitte geben Sie Ihre Firmenadresse an'))
            ->add('phone_mobile', TextType::class, array('required' => false, 'constraints' => array(new Length(array('min' => 6))))) // Not required for registration
            ->add('phone', TextType::class, array('required' => false, 'constraints' => array(new Length(array('min' => 6))))) // Not required for registration


            ->add('uid', HiddenType::class) // Used to track user as we move through the process
            
            // But this *is* required at registration
            ->add('medical_specialty_id', TextType::class, array('constraints' => array(new NotBlank()), 'invalid_message' => 'Bitte wählen Sie Ihr Fachgebiet'));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'doctor_registration';
    }
}
