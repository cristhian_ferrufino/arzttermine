<?php

namespace Arzttermine\Form;

use Arzttermine\Insurance\Insurance;
use Arzttermine\User\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * @package Arzttermine\Form
 */
class PatientProfileType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options = array())
    {
        $builder
            ->add('gender', ChoiceType::class, array(
                    'choices' => array(
                        'Male' => User::GENDER_MALE,
                        'Female' => User::GENDER_FEMALE
                    ),
                    'constraints' => array(new NotBlank())
                )
            )
            ->add('first_name', TextType::class, array('constraints' => array(new NotBlank(), new Length(array('min' => 2)))))
            ->add('last_name',  TextType::class, array('constraints' => array(new NotBlank(), new Length(array('min' => 2)))))

            ->add('phone',      TextType::class, array('constraints' => array(new NotBlank(), new Length(array('min' => 6))), 'invalid_message' => 'Bitte geben Sie Ihre Telefonnumer ein'))

            ->add('insurance', ChoiceType::class, array(
                    'choices' => array(
                        'Privat' => Insurance::PKV,
                        'Geseztlich' => Insurance::GKV
                    ),
                    'constraints' => array(new NotBlank())
                )
            )
        
            ->add('uid', TextType::class, array('constraints' => array(new NotBlank())))

            ->add('current_password', PasswordType::class)
            ->add('new_password',     PasswordType::class);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'patient_profile';
    }
}