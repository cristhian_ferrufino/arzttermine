<?php

namespace Arzttermine\Form;

use Arzttermine\Insurance\Insurance;
use Arzttermine\User\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\IsTrue;

/**
 * @package Arzttermine\Form
 */
class PatientRegistrationType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options = array())
    {
        $builder
            // Anything with a content validator (length, email) also needs a NotBlank validator, or it won't validate it :S
            ->add('gender', ChoiceType::class, array(
                    'choices' => array(
                        'Male' => User::GENDER_MALE,
                        'Female' => User::GENDER_FEMALE
                    ), // This is also apparently optional during registration...
                    'empty_data' => User::GENDER_MALE // ... so we default to male :/
                )
            ) // Labels unused
            ->add('first_name', TextType::class, array('constraints' => array(new NotBlank(), new Length(array('min' => 2))), 'invalid_message' => 'Bitte geben Sie Ihren Vornamen ein'))
            ->add('last_name',  TextType::class, array('constraints' => array(new NotBlank(), new Length(array('min' => 2))), 'invalid_message' => 'Bitte geben Sie Ihren Nachnamen ein'))
            ->add('email',      TextType::class, array('constraints' => array(new NotBlank(), new Email()),                   'invalid_message' => 'Bitte geben Sie Ihre E-Mail-Adresse ein'))
            ->add('phone',      TextType::class, array('constraints' => array(new NotBlank(), new Length(array('min' => 6))), 'invalid_message' => 'Bitte geben Sie Ihre Telefonnumer ein'))

            ->add('insurance', ChoiceType::class, array(
                    'choices' => array(
                        'Privat' => Insurance::PKV,
                        'Geseztlich' => Insurance::GKV
                    ),
                    'constraints' => array(new NotBlank()),
                    'empty_data' => Insurance::GKV
                )
            )
                
            ->add('password', TextType::class, array(
                    'constraints' => array(
                        new NotBlank(),
                        new Length(array('min' => 8)), // Minimum 8 characters
                        new Regex(array('pattern' => '/[äÄöÖüÜa-zA-Z]+/')), // with 1 letter
                        new Regex(array('pattern' => '/[0-9]+/')) // and 1 number
                    ),
                    'invalid_message' => 'Das Password sollte mindestes 8 Zeichen haben und mindestens einen Buchstaben sowie eine Zahl enthalten.')
                )
            
            ->add('newsletter_subscription', TextType::class)
            ->add('agb', null, array('constraints' => array(new IsTrue(), new NotBlank()), 'invalid_message' => 'Bitte bestätigen Sie, dass Sie die Datenschutzbedingungen und AGB akzeptieren.'));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'patient_registration';
    }
}