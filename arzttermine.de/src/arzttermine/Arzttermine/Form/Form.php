<?php

namespace Arzttermine\Form;

use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Forms;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\Validator\Validation;

class Form {

    /**
     * @param FormTypeInterface $form
     *
     * @return \Symfony\Component\Form\Form
     */
    public static function getForm(FormTypeInterface $form)
    {
        $builder = self::getFormFactory()->createBuilder();
        $form->buildForm($builder, array());
        
        return $builder->getForm();
    }

    /**
     * @return \Symfony\Component\Form\FormFactoryInterface
     */
    private static function getFormFactory()
    {
        return Forms::createFormFactoryBuilder()
                    ->addExtension(new ValidatorExtension(Validation::createValidator()))
                    ->getFormFactory();
    }

    /**
     * Get errors as key/value pairs in the form element/message
     *
     * @param FormInterface $form
     *
     * @return array
     */
    public static function getFormErrors(FormInterface $form)
    {
        $errors = array();
        
        if ($form->count() > 0) {
            foreach ($form->all() as $child) {
                /**
                 * @var \Symfony\Component\Form\Form $child
                 */
                if (!$child->isValid()) {
                    $errors[$child->getName()] = self::getFormErrors($child);
                }
            }
        } else {
            /**
             * @var \Symfony\Component\Form\FormError $error
             */
            foreach ($form->getErrors() as $error) {
                $errors[] = $error->getMessage();
            }
        }

        return $errors;
    }
}