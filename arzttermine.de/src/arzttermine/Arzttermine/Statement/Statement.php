<?php

namespace Arzttermine\Statement;

use Arzttermine\Booking\Booking;
use Arzttermine\Core\Basic;
use Arzttermine\Core\DateTime;
use Arzttermine\Location\Location;
use Arzttermine\User\User;
use Symfony\Component\Validator\Constraints\Date;

class Statement extends Basic {

    const STATUS_OPEN      = 1;
    const STATUS_SUBMITTED = 2;
    const STATUS_CLOSED    = 4;
    
    /**
     * @var string
     */
    protected $tablename = 'statements';

    /**
     * @var int
     */
    protected $status;

    /**
     * @var Location
     */
    protected $location;
    
    /**
     * @var int
     */
    protected $location_id;

    /**
     * Formatted as YYYY-MM
     * 
     * @var string
     */
    protected $billing_period;

    /**
     * @var DateTime
     */
    protected $created_at;

    /**
     * @var User
     */
    protected $created_by;

    /**
     * @var DateTime
     */
    protected $updated_at;

    /**
     * @var User
     */
    protected $updated_by;

    /**
     * @var Booking[]
     */
    protected $bookings;

    /**
     * @var array
     */
    protected $all_keys = array(
        'id',
        'status',
        'location_id',
        'billing_period',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    );

    /**
     * @var array
     */
    protected $secure_keys = array();

    /**
     * @return Location
     */
    public function getLocation()
    {
        if (!$this->location instanceof Location) {
            $this->location = new Location($this->getLocationId());
        }
        
        return $this->location;
    }

    /**
     * @param Location|int $location
     *
     * @return $this
     */
    public function setLocation($location)
    {
        if ($location instanceof Location) {
            $location = $location->getId();
        }
        
        $this->setLocationId($location);
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getLocationId()
    {
        return intval($this->location_id);
    }

    /**
     * @param int $location_id
     *
     * @return self
     */
    public function setLocationId($location_id)
    {
        $this->location_id = intval($location_id);

        return $this;
    }
    
    /**
     * @return string
     */
    public function getBillingPeriod()
    {
        $date = new DateTime($this->billing_period);
        return $date->format('Y-m');
    }

    /**
     * Set the year and month of the billing period
     * 
     * @param string $period
     *
     * @return $this
     */
    public function setBillingPeriod($period)
    {
        $date = new DateTime($period);
        $this->billing_period = $date->format('Y-m');
        
        return $this;
    }
    
    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return new DateTime($this->created_at);
    }

    /**
     * @param int|User $created_by
     *
     * @return self
     */
    public function setCreatedBy($created_by)
    {
        if ($created_by instanceof User) {
            $created_by = $created_by->getId();
        }
        
        $this->created_by = intval($created_by);

        return $this;
    }

    /**
     * @return User
     */
    public function getCreatedBy()
    {
        return new User(intval($this->created_by));
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return new DateTime($this->updated_at);
    }

    /**
     * @param int|User $updated_by
     *
     * @return self
     */
    public function setUpdatedBy($updated_by)
    {
        if ($updated_by instanceof User) {
            $updated_by = $updated_by->getId();
        }

        $this->updated_by = $updated_by;

        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedBy()
    {
        return new User(intval($this->updated_by));
    }

    /**
     * @return Booking[]
     */
    public function getBookings()
    {
        if (is_null($this->bookings)) {
            $start_date = new DateTime($this->getBillingPeriod());
            $end_date   = new DateTime($this->getBillingPeriod());
            
            // Create date bookends
            $start_date->modify('first day of this month');
            $start_date->setTime(0, 0, 0);
            $end_date->modify('last day of this month');
            $end_date->setTime(23, 59, 59);
            
            $booking = new Booking(); // For querying
            $bookings = $booking->findObjects(
                'location_id=' . $this->getLocation()->getId() . ' AND ' .
                
                // Filter dates
                'appointment_start_at >= "' . $start_date->getDateTime() . '" AND ' .
                'appointment_start_at <= "' . $end_date->getDateTime() .'" AND ' .
                
                'status IN (' . implode(',', array(
                        Booking::STATUS_RECEIVED,
                        Booking::STATUS_RECEIVED_ALTERNATIVE,
                        Booking::STATUS_RECEIVED_ALTERNATIVE_PENDING,
                        Booking::STATUS_RECEIVED_UNCONFIRMED,
                        Booking::STATUS_CANCELLED_BY_PATIENT_LOGIN,
                        Booking::STATUS_CANCELLED_BY_PATIENT,
                        Booking::STATUS_NOSHOW
                    )) . ')'
            );
            
            $this->bookings = $bookings;
        }
        
        return $this->bookings;
    }

    /**
     * Update booking data according to the passed array. The statement will
     * only update bookings that belong to it, (hopefully) preventing hacky data
     * 
     * @param array $booking_data
     *
     * @return $this
     */
    public function updateBookings(array $booking_data)
    {
        /** @var Booking[] $bookings */
        $bookings = array();
        
        // Add index to bookings :/
        foreach ($this->getBookings() as $booking) {
            $bookings[$booking->getId()] = $booking;
        }
        
        foreach ($booking_data as $data) {
            if (
                !empty($data['booking_id']) &&
                !empty($data['status']) &&
                isset($data['appointment_start_at']) &&
                isset($data['returning_visitor']) &&
                in_array($data['booking_id'], array_keys($bookings)))
            {
                $bookings[$data['booking_id']]
                    ->setAppointmentStartAt($data['appointment_start_at'])
                    ->setDirty($data['returning_visitor'] ^ $bookings[$data['booking_id']]->getReturningVisitor()); // If it's different, then it's TRUE

                // !todo: Remove this hardcoded hacky trash
                switch ($data['status']) {
                    case 1:
                    case 4:
                        $bookings[$data['booking_id']]->setStatus(Booking::STATUS_RECEIVED);
                        break;
                    case 2:
                        $bookings[$data['booking_id']]->setStatus(Booking::STATUS_NOSHOW);
                        break;
                    default:
                        break;
                }
                
                $bookings[$data['booking_id']]->save(array('appointment_start_at', 'dirty', 'status'));
            }
        }
        
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return intval($this->status);
    }

    /**
     * @param int $status
     *
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = intval($status);

        return $this;
    }

    /**
     * A function that guarantees a valid Statement object for a given location/billing period
     * 
     * @param string $billing_period
     * @param Location|int $location
     * 
     * @return Statement
     */
    public static function forPeriod($billing_period, $location)
    {
        if (!$location instanceof Location) {
            $location = new Location(intval($location));
        }
        
        // Try find a statement that matches what we're looking for
        $statement = new Statement();
        $statement->setBillingPeriod($billing_period); // To sanitise only :/
        
        $statement = $statement->findObject(
            'location_id = ' . $location->getId() . ' AND ' .
            'billing_period = "' . $statement->getBillingPeriod() . '"'
        );
        
        // If the statement doesn't exist, save a new copy (if the location exists)
        if (!$statement->isValid() && $location->isValid()) {
            $statement
                ->setBillingPeriod($billing_period)
                ->setLocation($location)
                ->setStatus(self::STATUS_OPEN)
                ->saveNewObject();
        }
        
        return $statement;
    }
}