<?php

namespace Arzttermine\Cms;

use Arzttermine\Application\Application;
use Arzttermine\Core\Basic;

/**
 * CmsDir Class
 *
 *
 * @author Axel Kuzmik <axel@kuzmik.de>
 */
class CmsDir extends Basic {

    const ROOT_DIR_ID = 1;

    const ROOT_PREV_DIR_ID = 0;

    var $tablename = DB_TABLENAME_CMS_DIRS;

    var $tablename_metas = DB_TABLENAME_CMS_DIR_METAS;

    var $id;

    var $parent_id;

    var $created_at;

    var $created_by;

    var $updated_at;

    var $updated_by;

    // set in loadMetas()
    // array [lang][db_meta_key]
    var $metas;

    // all content objects (initiated by loadContents())
    var $contents;

    var $all_keys
        = array(
            "id",
            "parent_id",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by",
        );

    var $secure_keys
        = array(
            "id",
            "parent_id",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by",
        );

    var $db_meta_keys
        = array(
            "name",
            "title"
        );

    var $all_meta_keys
        = array(
            "name",
            "title",
            "path",
            "titles"
        );

    /**
     * @todo Remove this in favour of getters/setters
     *
     * @param string $property
     * @param mixed $value
     */
    public function __set($property, $value)
    {
        $this->$property = $value;
    }

    /**
     * @todo See above
     *
     * @param $property
     *
     * @return mixed|null
     */
    public function __get($property)
    {
        return isset($this->$property) ? $this->$property : null;
    }
    
    /**
     * Loads a dir into the object
     *
     * @param value
     * @param key ='id'
     *
     * @return id || false
     */
    public function load($value, $key = 'id')
    {
        if (parent::load($value, $key)) {
            $this->loadMetas($this->id);
            // contents is not loaded at this stage
            // can be filled with this->loadContents()
            $this->contents = false;
            $this->calcVars();

            return $this->id;
        } else {
            return false;
        }
    }

    /**
     * Loads the dirmeta
     *
     * @param dir_id
     *
     * @return true
     */
    public function loadMetas($dir_id)
    {
        $db = Application::getInstance()->getSql();

        $_dir_id = $dir_id;
        $db->query("SELECT parent_id FROM " . $this->tablename . " WHERE id='" . $dir_id . "';");

        while ($db->nf()) {
            $db->next_record();
            $parent_id = $db->f('parent_id');
            // is this root ?
            if ($parent_id == self::ROOT_PREV_DIR_ID) {
                // add the root slash
                foreach ($GLOBALS['CONFIG']['CMS_LOCALE'] as $_lang => $value) {
                    $this->metas[$_lang]['path'] = '/' . (isset($this->metas[$_lang]['path']) ? $this->metas[$_lang]['path'] : '');
                    $this->metas[$_lang]['titles'] = '/' . (isset($this->metas[$_lang]['titles']) ? $this->metas[$_lang]['titles'] : '');

                    if ($dir_id == self::ROOT_DIR_ID) {
                        $this->metas[$_lang]['name'] = '/';
                        $this->metas[$_lang]['title'] = '/';
                    }
                }
                break;
            }
            // all other dirs
            $db->query("SELECT lang,name,title FROM " . $this->tablename_metas . " WHERE dir_id='" . $_dir_id . "';");
            while ($db->next_record()) {
                $_lang = $db->f('lang');
                $this->metas[$_lang]['path'] = $db->f('name') . '/' . (isset($this->metas[$_lang]['path']) ? $this->metas[$_lang]['path'] : '');
                $this->metas[$_lang]['titles'] = $db->f('title') . '/' . (isset($this->metas[$_lang]['titles']) ? $this->metas[$_lang]['titles'] : '');
                // is this THIS dir?
                if ($_dir_id == $dir_id) {
                    // than grab the dirname and title just for this dir
                    $this->metas[$_lang]['name'] = $db->f('name');
                    $this->metas[$_lang]['title'] = $db->f('title');
                }
            }
            $_dir_id = $parent_id;
            $db->query("SELECT parent_id FROM " . $this->tablename . " WHERE id='" . $parent_id . "';");
        }

        return true;
    }

    /**
     * try to find the dir path or at least some (beginning) part of it
     *
     * @param path
     * @param lang
     *
     * @return false = error | array = lang, dir_id, found excerpt of dir path
     *
     * There are more than one way to do this:
     * 1. check each part of the dirpath and search in the db (this solution)
     * 2. search in the sitemap array
     * 3. store each dirpath + filename in the db (problem to keep that in sync)
     */
    public function findPath($path, $lang)
    {
        $db = Application::getInstance()->getSql();

        if ($path == '/') {
            return array('dir_id'  => self::ROOT_DIR_ID,
                         'lang'    => $lang,
                         'dirpath' => '/'
            );
        }

        $_paths = explode('/', $path);
        if (is_array($_paths)) {
            $_parent_id = self::ROOT_DIR_ID;
            $_level = 0;
            // default: at least we have the root dir
            $_dir_id = self::ROOT_DIR_ID;
            $_lang = $lang;
            $_dirname_match = '/';
            foreach ($_paths as $_dirname) {
                if ($_dirname == '') {
                    continue;
                }
                // if the first item was found than follow only this language
                if ($_lang != '') {
                    $_sql_lang = " AND " . $this->tablename_metas . ".lang='" . $_lang . "'";
                } else {
                    $_sql_lang = '';
                }
                $sql
                    = "
				SELECT
					" . $this->tablename . ".id,
					" . $this->tablename . ".parent_id,
					" . $this->tablename_metas . ".dir_id,
					" . $this->tablename_metas . ".name,
					" . $this->tablename_metas . ".lang
					FROM
					" . $this->tablename . "," . $this->tablename_metas . "
				WHERE
					" . $this->tablename . ".parent_id=" . $_parent_id . "
					AND " . $this->tablename . ".id = " . $this->tablename_metas . ".dir_id
					" . $_sql_lang . "
					AND " . $this->tablename_metas . ".name = '" . sqlsave($_dirname) . "';";
                $db->query($sql);
                if ($db->nf() == 1) {
                    if ($db->next_record()) {
                        $_dir_id = $db->f('dir_id');
                        $_parent_id = $_dir_id;
                        $_lang = $db->f('lang');
                        if ($_level > 0) {
                            $_dirname_match .= '/';
                        }
                        $_dirname_match .= $_dirname;
                    }
                }
                $_level++;
            }

            return array('dir_id'  => $_dir_id,
                         'lang'    => $_lang,
                         'dirpath' => $_dirname_match
            );
        }

        return false;
    }

    /**
     * Loads all contents that belongs to this dir
     * and sort the contents if needed
     *
     * @param lang
     * @param order_by
     *
     * @return bool
     */
    public function loadContents($lang = '', $order_by = '')
    {
        $_contents = array();

        // if the array should not be sorted by anything than
        // set the lang to the default
        if ($lang == '') {
            $_langs = $GLOBALS['CONFIG']['CMS_LOCALE'];
            $_lang = array_shift($_langs);
            $lang = $_lang['code'];
        }
        $_order_by_sql = '';
        if ($order_by != '') {
            $_order_by_sql = ' ORDER BY ' . $order_by;
        }
        $_content = new CmsContent();
        $_content_ids = $_content->findContentsWithMeta(
                                 DB_TABLENAME_CMS_CONTENTS . ".dir_id=" . $this->id . "
			AND " . DB_TABLENAME_CMS_CONTENTS . ".id=" . DB_TABLENAME_CMS_CONTENT_METAS . ".content_id
			AND " . DB_TABLENAME_CMS_CONTENT_METAS . ".lang='" . $lang . "'" .
                                 $_order_by_sql,
                                     DB_TABLENAME_CMS_CONTENTS . ".id",
                                     'id'
        );

        if (is_array($_content_ids) && count($_content_ids) > 0) {
            foreach ($_content_ids as $_content_id) {
                $_content = new CmsContent($_content_id);
                if ($_content->isValid()) {
                    $_contents[] = $_content;
                }
            }
        }
        $this->contents = $_contents;

        return true;
    }

    /**
     * Returns all contents that belongs to this dir as an object array
     *
     * @param
     *
     * @return
     */
    public function getContents($lang = '', $order_by = '')
    {
        // check if we allready have the content array and we don't want a sorted array
        if ($this->contents && $lang == '' && $order_by == '') return $this->contents;
        if ($this->loadContents($lang, $order_by)) {
            return $this->contents;
        } else {
            return array();
        }
    }

    /**
     * Sort the content array
     *
     * @param $sort_by
     */
    public function sortContents($contents_array)
    {
        if (!empty($contents_array)) {
            foreach ($contents_array as $content) {
                $_keys = $content->filename;
            }
        }
        return $contents_array;
    }

    /**
     * Calc some vars
     *
     * @param
     *
     * @return true
     */
    public function calcVars()
    {
        $this->url_admin_explorer_content = '/administration/cms?a=explorer_content&select_dir_id=' . $this->id;
    }

    /**
     * Stores a new object by an array
     *
     * @param array $data
     * @param bool $save_only_registered_keys
     *
     * @return bool
     */
    public function saveNew($data, $save_only_registered_keys = true)
    {
        $db = Application::getInstance()->getSql();

        $sql = "INSERT INTO " . $this->tablename . " SET ";
        $i = 0;
        foreach ($data as $key => $value) {
            if (!in_array($key, $this->all_keys)) continue;

            if ($i > 0) $sql .= ', ';
            if (in_array($key, $this->secure_keys)) {
                $value = sqlsave($value);
            }
            $sql .= $key . "='" . $value . "'";
            $i++;
        }
        if ($db->query($sql)) {
            if (isset($data['id'])) {
                $dir_id = $data['id'];
            } else {
                // if db-schema is auto_increment
                $dir_id = $db->insert_id();
            }

            // now add all metas
            foreach ($GLOBALS['CONFIG']['CMS_LOCALE'] as $lang => $values) {
                $sql = "INSERT INTO " . $this->tablename_metas . " SET dir_id=" . $dir_id . ',';
                foreach ($this->db_meta_keys as $key) {
                    $sql .= $key . '="' . sqlsave($data[$key . '_' . $lang]) . '",';
                }
                $sql .= 'lang="' . $lang . '";';
                $db->query($sql);
            }

            return $dir_id;
        } else {
            return false;
        }
    }

    /**
     * Stores an object
     *
     * @param array keys
     * @param bool save_meta_keys=false
     *
     * @return bool
     */
    public function save($keys = "0", $save_meta_keys = false)
    {
        $db = Application::getInstance()->getSql();

        if ($keys == "0") $keys = $this->all_keys;
        elseif (!is_array($keys)) {
            // if its just one key (as a string)
            $keys = array($keys);
        }

        $sql = "UPDATE " . $this->tablename . " SET ";
        $i = 0;
        foreach ($keys as $key) {
            if ($i > 0) $sql .= ', ';
            if (in_array($key, $this->secure_keys)) {
                $value = sqlsave($this->$key);
            } else {
                $value = $this->$key;
            }
            $sql .= $key . "='" . $value . "'";
            $i++;
        }
        $sql .= " WHERE id=" . $this->id;
        $db->query($sql);
        $rows = $db->affected_rows();

        // now the same for the metas
        if ($save_meta_keys) {
            // first delete the old metas
            $sql = "DELETE FROM " . $this->tablename_metas . " WHERE dir_id=" . $this->id . ';';
            $db->query($sql);

            foreach ($GLOBALS['CONFIG']['CMS_LOCALE'] as $lang => $values) {
                // now insert a meta for each lang
                $sql = "INSERT INTO " . $this->tablename_metas . " SET ";
                $sql .= 'dir_id=' . $this->id . ',';
                foreach ($this->db_meta_keys as $key) {
                    $sql .= $key . '="' . sqlsave($this->metas[$lang][$key]) . '",';
                }
                $sql .= 'lang="' . $lang . '";';
                $db->query($sql);
            }
        }
        // entities have been changed. Recalc some vars
        $this->calcVars();

        return $rows;
    }

    /**
     * Load child dirs
     *
     * @return true|false
     */
    public function loadDirs()
    {
        $dirs = $this->findObjects('parent_id=' . $this->id . ' ORDER BY created_at');
        if (!empty($dirs)) {
            $this->dirs = $dirs;
            foreach ($dirs as $dir) {
                $dir->loadDirs();
            }
        }

        return true;
    }

    /**
     * Returns a html ul-list
     * <ul>
     *   <li><a href=""></a>
     *     <ul>
     *       <li><a href=""></a></li>
     *       <li><a href=""></a></li>
     *     </ul>
     *   </li>
     * </ul>
     *
     * @param include_root
     * @param add_first_ul
     * @param first
     *
     * @return html
     */
    public function getHtmlList($include_root = true, $add_first_ul = true, $first = true, $level = 0)
    {
        $app = Application::getInstance();

        $_html = '';
        $level++;
        if (isset($this->dirs) && !empty($this->dirs)) {
            foreach ($this->dirs as $dir) {
                $_html .= $dir->getHtmlList($include_root, $add_first_ul, false, $level);
            }
            if ($add_first_ul && !$first) {
                $_html = '<ul>' . $_html . '</ul>';
            }
        }
        if (!$include_root && $this->id == self::ROOT_DIR_ID) return $_html;

        $_html = '<li class="level' . ($level - 1) . '"><a href="' . $this->getPath($app->getLanguageCode()) . '">' . $this->getTitle($app->getLanguageCode()) . '</a>' . $_html . '</li>';
        if ($first) {
            $_html = '<ul>' . $_html . '</ul>';
        }

        return $_html;
    }

    /**
     * Deletes the dir AND the dir_metas in all languages
     *
     * @param id
     * @param bool delete_contents=true
     *
     * @return affected rows
     */
    public function delete($id = '', $delete_contents = true)
    {
        $db = Application::getInstance()->getSql();

        if (!isset($id) || !is_numeric($id)) $id = $this->id;
        // never delete the root tree!
        if ($id == self::ROOT_DIR_ID) return false;

        // delete the child dirs
        $_dirs = $this->findObjects('parent_id=' . $this->id);
        if (!empty($_dirs)) {
            foreach ($_dirs as $_dir) {
                if ($_dir->isValid()) {
                    $_dir->delete($_dir->id, $delete_contents);
                }
            }
        }

        if ($delete_contents) {
            // delete all contents in this dir
            $_contents = $this->getContents();
            if (count($_contents) > 0) {
                foreach ($_contents as $_content) {
                    $_content->delete();
                }
            }
        }

        $sql = "DELETE FROM " . $this->tablename . " WHERE id=" . $id . ";";
        $db->query($sql);
        $rows = $db->affected_rows();
        $sql = "DELETE FROM " . $this->tablename_metas . " WHERE dir_id=" . $id . ";";
        $db->query($sql);

        return $rows;
    }

    /**
     * Try to find ONE dir meta and return the found array
     *
     * @param where
     * @param select
     *
     * @return key or 0 (error)
     */
    public function findDirMeta($where, $select = "*")
    {
        $db = Application::getInstance()->getSql();

        $sql = "SELECT " . $select . " FROM " . $this->tablename_metas . " WHERE " . $where . ";";
        $db->query($sql);
        if ($db->nf() == 1) {
            if ($db->next_record()) {
                return $db->Record;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    /**
     * Returns a array with all dir_meta keys which are db relevant (not the values!)
     *
     * @param
     *
     * @return string
     */
    public function getDbMetaKeys()
    {
        $_keys = array();
        foreach ($this->db_meta_keys as $meta_key) {
            foreach ($this->$meta_key as $key => $value) {
                $_keys[] = $meta_key . '_' . $key;
            }
        }

        return $_keys;
    }

    /**
     * Returns all dir data for forms
     *
     * @param
     *
     * @return string
     */
    public function getFormData()
    {
        $_data = array();
        foreach ($this->all_keys as $key) {
            $_data[$key] = $this->$key;
        }
        // also the dir_metas
        foreach ($this->metas as $lang => $metas) {
            foreach ($this->metas[$lang] as $meta => $value) {
                $_data[$meta . '_' . $lang] = $value;
            }
        }

        return $_data;
    }

    /**
     * Updates all metas with the data from form
     *
     * @param
     *
     * @return
     */
    public function updateMetasFromForm($form)
    {
        foreach ($this->db_meta_keys as $meta_key) {
            $_tmp = array();
            foreach ($GLOBALS['CONFIG']['CMS_LOCALE'] as $lang => $values) {
                if (isset($form[$meta_key . '_' . $lang])) {
                    // this does not work
                    // $this->$meta_key[$lang]=$form[$meta_key.'_'.$lang];
                    // so we do it this way
                    $this->metas[$lang][$meta_key] = $form[$meta_key . '_' . $lang];
                }
            }
        }

        return true;
    }

    /**
     * Helper *********************************************
     */

    /**
     * Returns the name in the desired language
     *
     * @param lang
     *
     * @return string dirname
     */
    public function getName($lang)
    {
        if (isset($this->metas[$lang]['name'])) {
            return $this->metas[$lang]['name'];
        } else {
            return '?';
        }
    }

    /**
     * Returns the dirpath in the desired language
     *
     * @param lang
     *
     * @return string dirpath
     */
    public function getPath($lang)
    {
        if (isset($this->metas[$lang]['path'])) {
            return $this->metas[$lang]['path'];
        } else {
            return '?';
        }
    }

    /**
     * Returns the title in the desired language
     *
     * @param lang
     *
     * @return string title
     */
    public function getTitle($lang)
    {
        if (isset($this->metas[$lang]['title'])) {
            return $this->metas[$lang]['title'];
        } else {
            return '?';
        }
    }

    /**
     * Check if the dir_id is within this dir
     *
     * @param dir_id
     *
     * @return bool
     */
    public function isInDir($dir_id)
    {
        if ($this->id == $dir_id) {
            return true;
        } else {
            $dir = new CmsDir();
            // find all childs
            $dir_arrays = $dir->finds('parent_id=' . $this->id);
            if (is_array($dir_arrays) && !empty($dir_arrays)) {
                foreach ($dir_arrays as $dir_array) {
                    $dir->load($dir_array['id']);
                    if ($dir->isInDir($dir_id)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
}