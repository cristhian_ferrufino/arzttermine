<?php

namespace Arzttermine\Cms;

use Arzttermine\Application\Application;
use Arzttermine\User\User;

class CmsView {

    // Filthy hack
    public static $root_url = '/administration/cms';
    
    /**
     * @param $dir_id
     * @param $content_id
     * @param $action
     *
     * @return string
     */
    public static function getDirContentExplorer($dir_id, $content_id, $action)
    {
        $view = Application::getInstance()->getView();

        // dir
        $html = self::actionButton('cms_admin_dir_new', 'adm/dir_new.png', self::$root_url . '?a=dir_new&dir_id=' . $dir_id);
        $view->set('dir_buttons', $html);
        $html = '';
        foreach ($GLOBALS['CONFIG']['CMS_LOCALE'] as $lang => $values) {
            $html .= self::getDirExplorer($dir_id, $lang);
        }
        $view->set('dir_container', $html);

        $view->set(
            'content_buttons',
            self::actionButton('cms_admin_content_new', 'adm/content_new.png', self::$root_url . '?a=content_new&dir_id=' . $dir_id)
        );

        // content
        $html = '';
        foreach ($GLOBALS['CONFIG']['CMS_LOCALE'] as $lang => $values) {
            switch ($action) {
                case 'explorer_content':
                    $html .= self::getContentExplorer($dir_id, $lang);
                    break;
                case 'explorer_version':
                    $html .= self::getContentVersionExplorer($content_id, $lang);
                    break;
                case 'explorer_version_diff':
                    $html .= self::getContentVersionExplorer($content_id, $lang, 'explorer_version_diff');
                    break;
            }
        }
        $view->set('content_container', $html);

        return $view->fetch('admin/cms/dir_content_explorer.tpl');
    }

    /**
     * @param $access
     * @param $icon
     * @param $url
     * @param string $target
     *
     * @return string
     */
    private static function actionButton($access, $icon, $url, $target = '')
    {
        $profile = Application::getInstance()->getCurrentUser();

        $html = '';
        $html_target = '';
        if ($target != '') {
            $html_target = ' target="' . $target . '"';
        }
        if ($profile->checkPrivilege($access)) {
            $html .= '<a href="' . $url . '"' . $html_target . '><img src="' . getStaticUrl($icon) . '" /></a>';
        }

        return $html;
    }

    /**
     * @param $select_dir_id
     * @param $lang
     *
     * @return string
     */
    private static function getDirExplorer($select_dir_id, $lang)
    {
        global $DIR_ARRAY;

        $img_expand = getStaticUrl('adm/tree_expand.gif');
        $img_collapse = getStaticUrl('adm/tree_collapse.gif');
        $img_line = getStaticUrl('adm/tree_vertline.gif');
        $img_split = getStaticUrl('adm/tree_split.gif');
        $img_end = getStaticUrl('adm/tree_end.gif');
        $img_leaf = getStaticUrl('adm/tree_leaf.gif');
        $img_spc = getStaticUrl('adm/tree_space.gif');
        $img_dir_open = getStaticUrl('adm/tree_dir_open.gif');
        $img_dir_closed = getStaticUrl('adm/tree_dir_closed.gif');

        $DIR_ARRAY = array();
        self::getDirArray(CmsDir::ROOT_PREV_DIR_ID);
        $dir_array = $DIR_ARRAY;

        /* Get Node numbers to expand */
        $open_dirs = self::getOpenedNodes();

        $counter1 = 0;
        $maxlevel = 0;
        foreach ($dir_array as $dir) {
            $tmp_dir_id = $dir["dir_id"];
            /* check if the curent user has the right to see this dir */
            // if (grantdiraccess($tmp_dir_id)) {
            if (1) {
                $tree[$tmp_dir_id][0] = $dir["spacer"]; // tree level [n]
                $tree[$tmp_dir_id][1] = 0; // last item in subtree [0|1]
                if ($dir["spacer"] > $maxlevel) {
                    $maxlevel = $dir["spacer"];
                }

                /* get dirs to expand */
                $tmp_expand = 0;
                if (is_array($open_dirs)) {
                    if (in_array($tmp_dir_id, $open_dirs)) {
                        $tmp_expand = 1;
                    }
                }
                $expand[$tmp_dir_id] = $tmp_expand;

                /* get dirs to be visible / the root node is always visible */
                if (($dir["parent_id"] != CmsDir::ROOT_PREV_DIR_ID) && ($expand[$dir["parent_id"]] == 1) && ($visible[$dir["parent_id"]] == 1) || ($tmp_dir_id == CmsDir::ROOT_DIR_ID)) {
                    $visible[$tmp_dir_id] = 1;
                } else {
                    $visible[$tmp_dir_id] = 0;
                }

                /* set the last subnode in each node */
                $is_last = 1;
                $counter2 = 0;
                $tmp_parent_id = $dir["parent_id"];
                foreach ($dir_array as $tmp_dir) {
                    if ($tmp_parent_id == $tmp_dir["parent_id"]) {
                        if ($counter2 > $counter1) {
                            $is_last = 0;
                        }
                    }
                    $counter2++;
                }
                if ($is_last) {
                    $tree[$tmp_dir_id][1] = 1;
                }
            }
            $counter1++;
        }

        $maxlevel = $maxlevel + 2;
        $_html
            = '<div class="dir_wrapper">
					<div class="toggle_trigger ' . $lang . '"><a href="#"><img src="' . getStaticUrl($GLOBALS['CONFIG']['CMS_LOCALE'][$lang]['icon']) . '" alt="' . $GLOBALS['CONFIG']['CMS_LOCALE'][$lang]['name'] . '" /></a></div>
					<table class="toggle_container dirs">';

        foreach ($dir_array as $dir) {
            $dir_id = $dir['dir_id'];
            /* the dir_array does not contain all get_dir_array() - elements !!! */
            /* so we have to get all desired things by ourself */
            $_dir = new CmsDir($dir_id);
            $_class = ($dir_id == $select_dir_id) ? ' selected' : "";
            if (isset($visible[$dir_id]) && $visible[$dir_id]) {
                /* start new row  */
                $_html .= '<tr class="dir' . $_class . '">';
                $_first = ' class="first"';
                /* vertical lines from higher levels */
                $i = 0;
                while ($i < $dir["spacer"] - 1) {
                    if ($levels[$i] == 1) {
                        $_html .= '<td><img src="' . $img_line . '"></td>';
                    } else {
                        $_html .= '<td' . $_first . '><a name="' . $dir_id . '"></a><img src="' . $img_spc . '"></td>';
                        $_first = '';
                    }
                    $i++;
                }
                /* corner at end of subtree or t-split */
                if ($tree[$dir_id][1] == 1) {
                    $_html .= '<td style="text-align: right;"><img src="' . $img_end . '"></td>';
                    $levels[$dir["spacer"] - 1] = 0;
                } else {
                    $_html .= '<td><img src="' . $img_split . '"></td>';
                    $levels[$dir["spacer"] - 1] = 1;
                }
                /* Node (with subtree) or Leaf (no subtree) */
                $has_childnodes = 0;
                foreach ($dir_array as $tmp_dir) {
                    if ($tmp_dir["parent_id"] == $dir_id) {
                        $has_childnodes = 1;
                        break;
                    }
                }
                if ($has_childnodes) {
                    if ($expand[$dir_id] == 0) {
                        $_html .= '<td><a href="' . self::$root_url . '?dir_open=' . $dir_id . '&select_dir_id=' . $dir_id . '"><img src="' . $img_expand . '"></a></td>';
                    } else {
                        $_html .= '<td><a href="' . self::$root_url . '?dir_close=' . $dir_id . '&select_dir_id=' . $dir_id . '"><img src="' . $img_collapse . '"></a></td>';
                    }
                } else {
                    /* Tree Leaf */
                    $_html .= '<td><img src="' . $img_leaf . '"></td>';
                }
                /* output item text */
                $img_dir = $dir_id == $select_dir_id ? $img_dir_open : $img_dir_closed;
                //  IE-BUG: style="width:1%;"
                $_html .= '<td style="width:1%;"><a href="' . self::$root_url . '?select_dir_id=' . $dir["dir_id"] . '" title="' . $_dir->getPath($lang) . '"><img src="' . $img_dir . '" border="0" alt=""></a></td>';
                $_html .= '<td colspan="' . ($maxlevel - $dir["spacer"]) . '" class="dirname"><a href="' . self::$root_url . '?select_dir_id=' . $dir["dir_id"] . '" title="' . $_dir->getPath($lang) . '">' . $_dir->getName($lang) . '</a></td>';
                $_html .= '<td class="last"><div class="actionicons">';
                $_html .= self::actionButton('cms_admin_dir_edit', 'adm/document_edit.png', self::$root_url . '?a=dir_edit&dir_id=' . $dir["dir_id"]);
                $_html .= self::actionButton('cms_admin_dir_delete', 'adm/delete.png', self::$root_url . '?a=dir_delete&dir_id=' . $dir["dir_id"]);
                $_html .= '</div></td></tr>' . "\n";
            }
        }
        $_html .= "</table></div>\n";

        return $_html;
    }

    /**
     * @param $dir_id
     *
     * @return int
     */
    private static function getDirArray($dir_id)
    {
        global $DIR_ARRAY, $SPACER;

        $db = Application::getInstance()->getSql();

        $db->query("SELECT * FROM " . DB_TABLENAME_CMS_DIRS . " WHERE parent_id='" . $dir_id . "';");
        if ($db->nf() == 0) {
            return 0;
        }

        $SPACER++;

        while ($db->next_record()) {
            $tmp_dir_id[] = $db->f("id");
            $tmp_parent_id[] = $db->f("parent_id");
        }

        reset($tmp_dir_id);
        foreach ($tmp_dir_id as $key => $id) {
            /* if its the root dir than show the slash */
            if ($id == CmsDir::ROOT_DIR_ID) {
                $DIR_ARRAY[$id]["name"] = "/";
            } else {
                // now get all dir metas (names,etc) for this dir
                $db->query("SELECT * FROM " . DB_TABLENAME_CMS_DIR_METAS . " WHERE dir_id='" . $id . "';");
                if ($db->nf() == 0) {
                    return 0;
                }
                while ($db->next_record()) {
                    $DIR_ARRAY[$id]['name'][$db->f('lang')] = $db->f('name');
                    $DIR_ARRAY[$id]['title'][$db->f('lang')] = $db->f('title');
                }
            }
            $DIR_ARRAY[$id]["dir_id"] = $id;
            $DIR_ARRAY[$id]["parent_id"] = $tmp_parent_id[$key];
            $DIR_ARRAY[$id]["spacer"] = $SPACER;
            // $DIR_ARRAY[$id]["filenr"] = countfiles($id);

            self::getDirArray($id);
        }
        $SPACER--;

        return 0;
    }

    /**
     * @return array|string
     */
    private static function getOpenedNodes()
    {
        $app = Application::getInstance();
        $db = $app->getSql();
        $request = $app->getRequest();
        $profile = $app->getCurrentUser();

        $dir_open = $request->getQuery('dir_open');
        $dir_close = $request->getQuery('dir_close');
        if (is_numeric($dir_open)) {
            /* does this dir_id exist? */
            $db->query("SELECT id FROM " . DB_TABLENAME_CMS_DIRS . " WHERE id='" . $dir_open . "';");
            if ($db->nf()) {
                /* check if it already exists in open_nodes */
                $db->query("SELECT * FROM " . DB_TABLENAME_CMS_DIR_OPEN_NODES . " WHERE dir_id='" . $dir_open . "' AND owner_id='" . $profile->getId() . "';");
                /* if it doesnt exist than insert */
                if (!$db->nf()) {
                    $db->query("INSERT INTO " . DB_TABLENAME_CMS_DIR_OPEN_NODES . " (dir_id,owner_id) VALUES ('" . $dir_open . "','" . $profile->getId() . "');");
                }
            }
        }
        if (is_numeric($dir_close)) {
            /* try to delete */
            $db->query("DELETE FROM " . DB_TABLENAME_CMS_DIR_OPEN_NODES . " WHERE dir_id='" . $dir_close . "' AND owner_id='" . $profile->getId() . "';");
        }
        $db->query("SELECT * FROM " . DB_TABLENAME_CMS_DIR_OPEN_NODES . " WHERE owner_id=" . $profile->getId() . ";");
        $dir_id_array = array();
        while ($db->next_record()) {
            $dir_id_array[] = $db->f("dir_id");
        }

        return (!empty($dir_id_array) ? $dir_id_array : "");
    }

    /**
     * CONTENT ************************************************
     */

    /**
     * Return the HTML-Content-View
     *
     * @param int $dir_id
     * @param string $lang
     *
     * @return string
     */
    public static function getContentExplorer($dir_id, $lang)
    {
        $_html
            = '<div class="content_wrapper">
					<div class="toggle_trigger ' . $lang . '"><a href="#"><img src="' . getStaticUrl($GLOBALS['CONFIG']['CMS_LOCALE'][$lang]['icon']) . '" alt="' . $GLOBALS['CONFIG']['CMS_LOCALE'][$lang]['name'] . '" /></a></div>
					<table class="toggle_container contents">';

        $_dir = new CmsDir($dir_id);
        if (!$_dir->isValid()) {
            return 'non valid dir_id!';
        }

        $_contents = $_dir->getContents($lang, 'filename');
        if (count($_contents) > 0) {
            $_content_nr = 1;
            foreach ($_contents as $_content) {
                /* start new row  */
                $_tree_img = (count($_contents) == $_content_nr) ? getStaticUrl('adm/tree_end.gif') : getStaticUrl('adm/tree_split.gif');
                $_mimetype_img = getStaticUrl('adm/mime_html.gif');

                $_html .= '<tr class="content">';
                $_html .= '<td class="first"><img src="' . $_tree_img . '" alt="" /></td>';
                $_html .= '<td><img src="' . $_mimetype_img . '" alt="" /></td>';
                $_html .= '<td class="filename"><a href="' . $_content->url_admin_explorer_version . '">' . $_content->getVirtualFilename($lang) . '</a></td>';
                $_html .= '<td class="last"><div class="actionicons">';
                $_html .= self::actionButton('cms_admin', 'adm/document_view_online.png', $_content->getUrl($lang, true), '_blank');
                $_html .= self::actionButton('cms_admin_version_edit', 'adm/version_change.png', $_content->url_admin_explorer_version);
                $_html .= self::actionButton('cms_admin_version_diff', 'adm/version_diff.png', $_content->url_admin_content_version_diff);
                $_html .= self::actionButton('cms_admin_content_edit', 'adm/document_edit.png', $_content->url_admin_content_edit);
                $_html .= self::actionButton('cms_admin_content_delete', 'adm/delete.png', $_content->url_admin_content_delete);
                $_html .= '</div></td></tr>' . "\n";
                $_content_nr++;
            }
        } else {
            $_html .= '<tr><td><div class="hint center">no contents</div></td></tr>' . "\n";
        }
        $_html .= "</table></div>\n";

        return $_html;
    }

    /**
     * VERSION ************************************************
     */
    /**
     * Return the HTML-Versions-View
     *
     * @param int $content_id
     * @param string $lang
     * @param string (i.e. explorer_version_diff)
     *
     * @return string
     */
    public static function getContentVersionExplorer($content_id, $lang, $view = '')
    {
        $_html
            = '<div class="content_wrapper">
                    <div class="toggle_trigger ' . $lang . '"><a href="#"><img src="' . getStaticUrl($GLOBALS['CONFIG']['CMS_LOCALE'][$lang]['icon']) . '" alt="' . $GLOBALS['CONFIG']['CMS_LOCALE'][$lang]['name'] . '" /></a></div>
                    <table class="toggle_container contents">';

        $_content = new CmsContent($content_id);
        if (!$_content->isValid()) {
            return 'non valid content_id!';
        }

        // show the content options in the first line
        $_mimetype_img = getStaticUrl('adm/mime_html.gif');
        $_html .= '<tr class="content_header">';
        $_html .= '<td class="first tree"><img src="' . getStaticUrl('adm/tree_end.gif') . '" alt="" /></td>';
        $_html .= '<td><img src="' . $_mimetype_img . '" alt="" /></td>';
        $_html .= '<td class="filename" colspan="3"><a href="' . $_content->url_admin_explorer_content . '">' . $_content->getVirtualFilename($lang) . '</a></td>';
        $_html .= '<td class="last" colspan="2"><div class="actionicons">';
        $_html .= self::actionButton('cms_admin', 'adm/document_view_online.png', $_content->getUrl($lang, true), '_blank');
        $_html .= self::actionButton('cms_admin_version_edit', 'adm/version_change.png', $_content->url_admin_explorer_content);
        $_html .= self::actionButton('cms_admin_version_diff', 'adm/version_diff.png', $_content->url_admin_content_version_diff);
        $_html .= self::actionButton('cms_admin_content_edit', 'adm/document_edit.png', $_content->url_admin_content_edit);
        $_html .= self::actionButton('cms_admin_content_delete', 'adm/delete.png', $_content->url_admin_content_delete);
        $_html .= '</div></td></tr>';
        $_html .= '<tr class="version_header">';
        $_html .= '<td class="first"></td><td class="tree"><img src="' . getStaticUrl('adm/tree_vertline.gif') . '" alt="" /></td>';
        $_html .= '<td></td><th>id</td><th>date</td><th>author</td>';
        $_html .= '<td class="last"><div class="actionicons">';
        $_html .= self::actionButton('cms_admin_content_edit', 'adm/folder_up.png', $_content->url_admin_explorer_content);
        $_html .= self::actionButton('cms_admin_content_delete', 'adm/version_new.png', $_content->url_admin_content_version_new . '&lang=' . $lang);
        $_html .= '</div></td></tr>' . "\n";

        // show all versions
        $_versions = $_content->getVersions($lang);
        $_content_active_version_id = $_content->getActiveVersionId($lang);
        if (count($_versions) > 0) {
            $_version_nr = 1;
            foreach ($_versions as $_version) {
                /* start new row  */
                $_active = ($_version->getId() == $_content_active_version_id) ? ' selected' : '';
                $_tree_img = (count($_versions) == $_version_nr) ? getStaticUrl('adm/tree_end.gif') : getStaticUrl('adm/tree_split.gif');
                $_mimetype_img = getStaticUrl('adm/mime_html.gif');
                $_user = new User($_version->getCreatedBy());
                $_username = $_user->getFullName();

                $_html .= '<tr class="version' . $_active . '">';
                $_html .= '<td class="first"></td><td class="tree"><img src="' . $_tree_img . '" alt="" /></td>';
                $_html .= '<td><img src="' . $_mimetype_img . '" alt="" /></td>';
                $_html .= '<td class="data">' . $_version->version_nr . '</td>';
                $_html .= '<td class="data">' . $_version->created_at . '</td>';
                $_html .= '<td class="data" style="text-align:right;">' . $_username . '</td>';
                $_html .= '<td class="last"><div class="actionicons">';
                switch ($view) {
                    case 'explorer_version_diff':
                        // wrap the radio button with a (div) selector or jQeury will fail on adressing them
                        $_html .= '<div class="value1' . $_version->lang . '" style="display:inline;"><input type="radio" value="' . $_version->id . '" name="version_1_' . $_version->lang . '" style="width:16px;margin:0 2px 0 0;"></div>';
                        $_html .= '<div class="value2' . $_version->lang . '" style="display:inline;"><input type="radio" value="' . $_version->id . '" name="version_2_' . $_version->lang . '" style="width:16px;margin:0 2px 0 0;"></div>';
                        break;
                    default:
                        // standard version action buttons
                        $_html .= self::actionButton('cms_admin_version_activate', 'adm/version_activate.png', $_version->url_admin_version_activate);
                        $_html .= self::actionButton('cms_admin_version_view', 'adm/version_view.png', $_version->url_admin_version_view, '_blank');
                        $_html .= self::actionButton('cms_admin_version_edit', 'adm/version_edit.png', $_version->url_admin_version_edit);
                        $_html .= self::actionButton('cms_admin_version_delete', 'adm/delete.png', $_version->url_admin_version_delete);
                        break;
                }
                $_html .= '</div></td></tr>' . "\n";
                $_version_nr++;
            }
            // show the diff button in diff view
            if ($view == 'explorer_version_diff') {
                $_html .= '<tr class="version_footer">';
                $_html .= '<td colspan="7" class="last"><div class="actionicons">';
                $_html .= '<a id="diff_' . $_version->lang . '" href="#">show diff</a></div></td></tr>' . "\n";
            }
        } else {
            $_html .= '<tr><td colspan="7"><div class="hint center">no versions</div></td></tr>' . "\n";
        }
        $_html .= "</table></div></div>\n";

        // show the diff js in diff view
        if ($view == 'explorer_version_diff') {
            $_html
                .= '
<script type="text/javascript">
	//<![CDATA[
		$("#diff_' . $_version->lang . '").click(function() {
	 		$.fancybox({
				\'href\':\'' . $_content->url_admin_content_version_diff . '&s=ok&lang=' . $_version->lang . '&version_1_' . $_version->lang . '=\' + $(".value1' . $_version->lang . ' input[@name=\'version_1_' . $_version->lang . '\']:checked").val() + \'&version_2_' . $_version->lang . '=\' + $(".value2' . $_version->lang . ' input[@name=\'version_2_' . $_version->lang . '\']:checked").val()
	 		});
		});
	//]]>
</script>
';
        }

        return $_html;
    }
}