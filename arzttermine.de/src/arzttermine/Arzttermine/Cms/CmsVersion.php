<?php

namespace Arzttermine\Cms;

use Arzttermine\Application\Application;
use Arzttermine\Core\Basic;

class CmsVersion extends Basic {

    /**
     * @var string
     */
    protected $tablename = DB_TABLENAME_CMS_VERSIONS;

    /**
     * @var int
     */
    protected $content_id;

    /**
     * @var int
     */
    protected $version_nr;

    /**
     * @var string
     */
    protected $lang = '';

    /**
     * @var string
     */
    protected $content1 = '';

    /**
     * @var string
     */
    protected $content2 = '';

    /**
     * @var string
     */
    protected $created_at = '';

    /**
     * @var string
     */
    protected $created_by = '';

    /**
     * @var array
     */
    protected $all_keys
        = array(
            "id",
            "content_id",
            "version_nr",
            "lang",
            "content1",
            "content2",
            "created_at",
            "created_by"
        );

    /**
     * @var array
     */
    protected $secure_keys
        = array(
            "id",
            "content_id",
            "version_nr",
            "lang",
            "content1",
            "content2",
            "created_at",
            "created_by"
        );

    /**
     * Calc some vars
     *
     * @param
     *
     * @return true
     */
    function calcVars()
    {
        $_content = new CmsContent($this->content_id);
        if ($_content->isValid()) {
            $this->dir_id = $_content->dir_id;
        }
        $this->url_admin_version_delete = '/administration/cms?a=version_delete&select_dir_id=' . $this->dir_id . '&content_id=' . $this->content_id . '&version_id=' . $this->id;
        $this->url_admin_version_edit = '/administration/cms?a=version_edit&select_dir_id=' . $this->dir_id . '&content_id=' . $this->content_id . '&version_id=' . $this->id;
        $this->url_admin_version_view = '/administration/cms?a=version_view&select_dir_id=' . $this->dir_id . '&content_id=' . $this->content_id . '&version_id=' . $this->id;
        $this->url_admin_version_activate = '/administration/cms?a=version_activate&select_dir_id=' . $this->dir_id . '&content_id=' . $this->content_id . '&version_id=' . $this->id;

        return true;
    }

    /**
     * @todo Remove this in favour of getters/setters
     * 
     * @param string $property
     * @param mixed $value
     */
    public function __set($property, $value)
    {
        $this->$property = $value;
    }

    /**
     * @todo See above
     * 
     * @param $property
     *
     * @return mixed|null
     */
    public function __get($property)
    {
        return isset($this->$property) ? $this->$property : null;
    }

    // ********************************************************
    // OUT: 0=Error
    // OUT: >0=new id
    function saveNew($data, $save_only_registered_keys = true)
    {
        $db = Application::getInstance()->getSql();

        // first check, if this is a version that is based on a previous version
        if (isset($data['id']) || !isset($data['version_nr'])) {
            $_version_nr = 1;
            // find the new version_nr
            $sql
                = '
			SELECT
				MAX(version_nr) AS max_version_nr
			FROM
				' . $this->tablename . '
			WHERE
				content_id=' . $data['content_id'] . '
			AND
				lang="' . $data['lang'] . '"';

            $db->query($sql);
            if ($db->next_record()) {
                $_version_nr = $db->f('max_version_nr');
                $_version_nr++;
            }
            $data['version_nr'] = $_version_nr;
            unset($data['id']);
        }

        $sql = "INSERT INTO " . $this->tablename . " SET ";
        $i = 0;
        foreach ($data as $key => $value) {
            // check if the key is valid
            if (!in_array($key, $this->all_keys)) {
                continue;
            }

            if ($i > 0) $sql .= ', ';
            if (in_array($key, $this->secure_keys)) {
                $value = sqlsave($value);
            }
            $sql .= $key . "='" . $value . "'";
            $i++;
        }
        if ($db->query($sql)) {
            if (isset($data['id'])) {
                $version_id = $data['id'];
            } else {
                // if db-schema is auto_increment
                $version_id = $db->insert_id();
            }

            return $version_id;
        } else {
            return false;
        }
    }

    /**
     * Sets all version data from forms
     *
     * @param
     *
     * @return string
     */
    public function setFormData($form)
    {
        foreach ($this->all_keys as $key) {
            if (isset($form[$key])) {
                $this->$key = $form[$key];
            }
        }

        return true;
    }

    /**
     * Sets the active_version_id for a content
     *
     * @return bool
     */
    public function activate()
    {
        $_content = new CmsContent($this->content_id);
        if (!$_content->isValid()) return false;

        return $_content->activateVersion($this->id, $this->lang);
    }

    /**
     * Returns the id
     *
     * @return int
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * @param string $content2
     *
     * @return self
     */
    public function setContent2($content2)
    {
        $this->content2 = trim($content2);

        return $this;
    }

    /**
     * @return string
     */
    public function getContent2()
    {
        return $this->content2;
    }

    /**
     * @param string $content1
     *
     * @return self
     */
    public function setContent1($content1)
    {
        $this->content1 = trim($content1);

        return $this;
    }

    /**
     * @return string
     */
    public function getContent1()
    {
        return $this->content1;
    }
    
}