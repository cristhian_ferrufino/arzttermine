<?php

namespace Arzttermine\Cms;

use Arzttermine\Application\Application;
use Arzttermine\Core\Basic;

class CmsContent extends Basic {

    const STATUS_DRAFT = 0;
    const STATUS_PUBLISHED = 1;
    
    /**
     * @var string
     */
    public $tablename = DB_TABLENAME_CMS_CONTENTS;

    /**
     * @var string
     */
    public $tablename_metas = DB_TABLENAME_CMS_CONTENT_METAS;

    /**
     * @var int
     */
    public $dir_id;

    /**
     * @var int
     */
    public $template_id;

    /**
     * @var int
     */
    public $engine_id;

    /**
     * @var string
     */
    public $mimetype;

    /**
     * @var int
     */
    protected $status = self::STATUS_DRAFT;

    /**
     * @var string
     */
    public $created_at;

    /**
     * @var string
     */
    public $created_by;

    /**
     * @var string
     */
    public $updated_at;

    /**
     * @var string
     */
    public $updated_by;

    // set in loadMetas()
    // array [lang][db_meta_key]
    /**
     * @var array
     */
    public $metas;

    // set in loadByUrl($url) or loadByVersion($version_id, $lang)
    // this is the language of the current content which fits with the url
    /**
     * @var string
     */
    public $current_lang;

    // TODO
    // var $content_type;
    // var $content_template;
    // should this content be cached?
    // var $force_caching;
    // should this content be excluded via meta robots noindex?
    // var $is_meta_robots_noindex;

    // all version objects (initiated by loadVersions())
    /**
     * @var array
     */
    public $versions;

    /**
     * @var array
     */
    public $all_keys
        = array(
            "id",
            "dir_id",
            "template_id",
            "engine_id",
            "mimetype",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by",
        );

    /**
     * @var array
     */
    public $secure_keys
        = array(
            "id",
            "dir_id",
            "template_id",
            "engine_id",
            "mimetype",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by",
        );

    /**
     * @var array
     */
    public $db_meta_keys
        = array(
            "active_version_id",
            "title",
            "use_dirpath",
            "filename",
            "tags",
            "description"
        );

    /**
     * @var array
     */
    public $all_meta_keys
        = array(
            "active_version_id",
            "title",
            "use_dirpath",
            "filename",
            "tags",
            "description",
            "url"
        );

    /**
     * @return int
     */
    public function getStatus()
    {
        return intval($this->status);
    }

    /**
     * @param int $status
     *
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = intval($status);

        return $this;
    }
    
    /**
     * Returns the content object
     *
     * @param int|string $id_or_url
     *
     * @return CmsContent
     */
    public static function getCmsContent($id_or_url)
    {
        $content = new CmsContent($id_or_url);

        // check if its an ID
        if (is_numeric($id_or_url)) {
            $content->load($id_or_url);
        }
        // nope. now check if its a url
        if (!$content->isValid()) {
            // try to find the right content
            $content->loadByUrl($id_or_url);
        }

        return $content;
    }

    /**
     * Loads a content by an url into the object and sets the language
     * 
     * @param string $url
     *
     * @return bool|int
     */
    public function loadByUrl($url)
    {
        $app = Application::getInstance();

        // set default language
        $_lang = $app->getLanguageCode();
        // remove I18N code from url if there is one
        foreach ($GLOBALS['CONFIG']['CMS_LOCALE'] as $_code => $_value) {
            if (preg_match('#^/(' . $_code . ')/#', $url, $_matches)) {
                $_lang = $_matches[1];
                $url = preg_replace('#^/' . $_lang . '/#', '/', $url);
                $app->setLanguageCode($_lang);
                break;
            } elseif ($url == '/' . $_code) {
                // only the language dir is requested (without "/" at the end)
                $url = '/';
                $_lang = $_code;
                $app->setLanguageCode($_lang);
                break;
            }
        }

        $_content_array = $this->findByUrl($url, $_lang);
        if (!is_array($_content_array)) {
            return false;
        }
        $_result = $this->load($_content_array['content_id']);
        if (!$_result) {
            return false;
        }

        // set the language
        $this->setCurrentLang($_content_array['lang']);
        // also set the application language
        $app->setLanguageCode($_content_array['lang']);

        return $_content_array['content_id'];
    }

    /**
     * find the content via a url
     *
     * @param string $url (without protcol and domain)
     * @param string $lang
     *
     * @return false=error | array(content_id,lang)
     */
    public function findByUrl($url, $lang)
    {
        $_result = $this->findByUrlWithinDirStructure($url, $lang);
        if (!is_array($_result)) {
            $_result = $this->findByUrlWithinContentStructure($url, $lang);
            if (!is_array($_result)) {
                // is this a directory without a filename?
                // add a index filename and try again

                // is there a slash at the end?
                if (!preg_match('/\/$/', $url)) {
                    $url .= '/';
                }
                $url .= CMS_DIRECTORY_INDEX_FILENAME;
                $_result = $this->findByUrlWithinDirStructure($url, $lang);
            }
        }
        
        return $_result;
    }

    /**
     * find the content via a url
     * - Checks if there is a content with cms_content_metas.use_dirpath=1
     * which fits to the url
     *
     * @param string $url (without protcol and domain)
     * @param string $lang
     *
     * @return bool|array
     */
    public function findByUrlWithinDirStructure($url, $lang)
    {
        // lets find the content via the url
        $_dir = new CmsDir();
        // array(dir_id,lang,(matched) dirpath)
        $_dir_array = $_dir->findPath($url, $lang);
        if (!is_array($_dir_array)) {
            return false;
        } else {
            // we found the dir, now find the content
            // delete the dirpath
            $_content_filename = preg_replace('/^' . str_replace('/', '\/', $_dir_array['dirpath']) . '/', '', $url);
            // delete the heading slash
            $_content_filename = preg_replace('/^\//', '', $_content_filename);
            // if there is no lang set (root dir or whatever) than set the default
            if (!isset($GLOBALS['CONFIG']['CMS_LOCALE'][$_dir_array['lang']])) {
                $_dir_array['lang'] = Application::getInstance()->getLanguageCode();
            }

            // find the content with an active version
            $_content_array = $this->findContentWithMeta(
                $this->tablename . ".dir_id=" . $_dir_array['dir_id'] . "
				AND " . $this->tablename . ".id=" . $this->tablename_metas . ".content_id
				AND " . $this->tablename_metas . ".use_dirpath=1
				AND " . $this->tablename_metas . ".lang='" . $_dir_array['lang'] . "'
				AND " . $this->tablename_metas . ".active_version_id!=0
				AND " . $this->tablename_metas . ".filename='" . sqlsave($_content_filename) . "'",
                $this->tablename . ".id,
				" . $this->tablename . ".dir_id,
				" . $this->tablename_metas . ".content_id,
				" . $this->tablename_metas . ".filename,
				" . $this->tablename_metas . ".lang"
            );
            if (is_array($_content_array)) {
                return array(
                    'content_id' => $_content_array['id'],
                    'lang'       => $_content_array['lang']
                );
            }
        }

        return false;
    }

    /**
     * Try to find ONE content with the desired metas and return the found array
     *
     * @param where
     * @param select
     *
     * @return key or 0 (error)
     */
    public function findContentWithMeta($where, $select = "*", $key = '')
    {
        $db = Application::getInstance()->getSql();

        $sql
            = "
		SELECT
			" . $select . "
			FROM
			" . $this->tablename . "," . $this->tablename_metas . "
		WHERE
			" . $where . ";";

        $db->query($sql);
        if ($db->nf() == 1) {
            if ($db->next_record()) {
                if ($key != '') {
                    return $db->Record[$key];
                } else {
                    return $db->Record;
                }
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    /**
     * find the content via a url
     * Checks if there is a content with cms_content_metas.use_dirpath=0
     * which fits to the url
     *
     * @param string URL (without protcol and domain)
     *
     * @return false=error | array(content_id,lang)
     */
    public function findByUrlWithinContentStructure($url)
    {
        // delete the heading slash
        $_content_filename = preg_replace('/^\//', '', $url);
        // find the content with an active version
        $_content_array = $this->findContentWithMeta(
            $this->tablename . ".id=" . $this->tablename_metas . ".content_id
			AND " . $this->tablename_metas . ".use_dirpath=0
			AND " . $this->tablename_metas . ".active_version_id!=0
			AND " . $this->tablename_metas . ".filename='" . sqlsave($_content_filename) . "'",
            $this->tablename . ".id,
			" . $this->tablename . ".dir_id,
			" . $this->tablename_metas . ".content_id,
			" . $this->tablename_metas . ".filename,
			" . $this->tablename_metas . ".lang"
        );
        if (is_array($_content_array)) {
            return array(
                'content_id' => $_content_array['id'],
                'lang'       => $_content_array['lang']
            );
        }

        return false;
    }

    /**
     * @todo See above
     *
     * @param $property
     *
     * @return mixed|null
     */
    public function __get($property)
    {
        return isset($this->$property) ? $this->$property : null;
    }

    /**
     * @todo Remove this in favour of getters/setters
     *
     * @param string $property
     * @param mixed $value
     */
    public function __set($property, $value)
    {
        $this->$property = $value;
    }

    /**
     * @param array $data
     * @param bool $save_only_registered_keys
     *
     * @return bool|int
     */
    public function saveNew($data, $save_only_registered_keys = true)
    {
        $db = Application::getInstance()->getSql();

        $sql = "INSERT INTO " . $this->tablename . " SET ";
        $i = 0;
        foreach ($data as $key => $value) {
            // check if the key is valid
            if ($save_only_registered_keys && !in_array($key, $this->all_keys)) {
                continue;
            }

            if ($i > 0) {
                $sql .= ', ';
            }
            if (in_array($key, $this->secure_keys)) {
                $value = sqlsave($value);
            }
            $sql .= $key . "='" . $value . "'";
            $i++;
        }
        if ($db->query($sql)) {
            if (isset($data['id'])) {
                $content_id = $data['id'];
            } else {
                // if db-schema is auto_increment
                $content_id = $db->insert_id();
            }

            // now add all metas
            foreach ($GLOBALS['CONFIG']['CMS_LOCALE'] as $lang => $values) {
                $sql = "INSERT INTO " . $this->tablename_metas . " SET content_id=" . $content_id . ',';
                foreach ($this->db_meta_keys as $key) {
                    if (isset($data[$key . '_' . $lang])) {
                        $sql .= $key . '="' . sqlsave($data[$key . '_' . $lang]) . '",';
                    }
                }
                $sql .= 'lang="' . $lang . '";';
                $db->query($sql);
            }

            return $content_id;
        } else {
            return false;
        }
    }

    /**
     * Try to find ONE OR MORE content with the desired metas and return the found array
     *
     * @param string $where
     * @param string $select
     * @param string $key
     *
     * @return mixed array or key array or 0 (error)
     */
    public function findContentsWithMeta($where, $select = "*", $key = '')
    {
        $db = Application::getInstance()->getSql();

        $sql
            = "
		SELECT
			" . $select . "
			FROM
			" . $this->tablename . "," . $this->tablename_metas . "
		WHERE
			" . $where . ";";
        $db->query($sql);
        if ($db->nf() > 0) {
            $_array = array();
            while ($db->next_record()) {
                if ($key != '') {
                    $_array[] = $db->Record[$key];
                } else {
                    $_array[] = $db->Record;
                }
            }

            return $_array;
        } else {
            return 0;
        }
    }

    /**
     * Delete content with all versions
     *
     * @param int $id
     *
     * @return int
     */
    public function delete($id = null)
    {
        $db = Application::getInstance()->getSql();

        if (empty($id)) {
            $id = $this->getId();
        }

        // first delete all versions
        $_versions = $this->getVersions();
        if (count($_versions) > 0) {
            foreach ($_versions as $_version) {
                $_version->delete();
            }
        }

        $sql = "DELETE FROM " . $this->tablename . " WHERE id=" . $id . ";";
        $db->query($sql);
        $rows = $db->affected_rows();
        $sql = "DELETE FROM " . $this->tablename_metas . " WHERE content_id=" . $id . ";";
        $db->query($sql);
        $rows = ($rows + $db->affected_rows());

        return $rows;
    }

    /**
     * Returns all versions that belongs to this content
     *
     * @param
     *
     * @return
     */
    public function getVersions($lang = '')
    {
        if ($this->versions) {
            return $this->versions;
        }
        if ($this->loadVersions($lang)) {
            return $this->versions;
        } else {
            return array();
        }
    }

    /**
     * Loads all versions that belongs to this content
     *
     * @param lang
     *
     * @return bool
     */
    public function loadVersions($lang = '')
    {
        if ($lang != '' && isset($GLOBALS['CONFIG']['CMS_LOCALE'][$lang])) {
            $_sql_lang = ' AND lang="' . $lang . '"';
        } else {
            $_sql_lang = '';
        }
        $_version = new CmsVersion();
        $_versions = $_version->findObjects('content_id=' . $this->id . $_sql_lang . ' ORDER BY version_nr DESC');
        $this->versions = $_versions;

        return true;
    }

    /**
     * Returns a array with all dir_meta keys which are db relevant (not the values!)
     *
     * @param
     *
     * @return string
     */
    public function getDbMetaKeys()
    {
        $_keys = array();
        foreach ($this->db_meta_keys as $meta_key) {
            foreach ($this->$meta_key as $key => $value) {
                $_keys[] = $meta_key . '_' . $key;
            }
        }

        return $_keys;
    }

    /**
     * Returns all content data for forms
     *
     * @param
     *
     * @return string
     */
    public function getFormData()
    {
        $_data = array();
        foreach ($this->all_keys as $key) {
            $_data[$key] = $this->$key;
        }
        // also the dir_metas
        foreach ($this->metas as $lang => $metas) {
            foreach ($this->metas[$lang] as $meta => $value) {
                $_data[$meta . '_' . $lang] = $value;
            }
        }

        return $_data;
    }

    /**
     * Helper *********************************************
     */

    /**
     * Updates all metas with the data from form
     *
     * @param array $form
     * @param array $default_values
     *
     * @return bool
     */
    public function updateMetasFromForm($form, array $default_values = array())
    {
        foreach ($this->db_meta_keys as $meta_key) {
            foreach ($GLOBALS['CONFIG']['CMS_LOCALE'] as $lang => $values) {
                if (isset($form[$meta_key . '_' . $lang])) {
                    // this does not work
                    #$this->$meta_key[$lang]=$form[$meta_key.'_'.$lang];
                    // so we do it this way
                    $this->metas[$lang][$meta_key] = $form[$meta_key . '_' . $lang];
                } elseif (isset($default_values[$meta_key])) {
                    $this->metas[$lang][$meta_key] = $default_values[$meta_key];
                }
            }
        }

        return true;
    }

    /**
     * Returns the filename in the desired language
     *
     * @param lang
     *
     * @return string dirname
     */
    public function getFilename($lang = null)
    {
        if (!$lang) {
            $lang = $app = Application::getInstance()->getLanguageCode();
        }

        if (isset($this->metas[$lang]['filename'])) {
            return $this->metas[$lang]['filename'];
        } else {
            return '?';
        }
    }

    /**
     * Returns the filename in the desired language
     *
     * @param string $lang
     *
     * @return string
     */
    public function getVirtualFilename($lang = null)
    {
        if (empty($lang)) {
            $lang = Application::getInstance()->getLanguageCode();
        }
        if (isset($this->metas[$lang]['filename'])) {
            if ($this->metas[$lang]['use_dirpath']) {
                $_root = '';
            } else {
                $_root = '/';
            }

            return $_root . $this->metas[$lang]['filename'];
        } else {
            return '?';
        }
    }

    /**
     * Returns the url for a content
     *
     * @param language =null
     * @param absolute url=false
     *
     * @return string a href
     */
    public function getUrl($lang = null, $absolute_url = false)
    {
        if ($lang == null) {
            $lang = Application::getInstance()->getLanguageCode();
        }

        if (isset($this->metas[$lang]['url'])) {
            if ($absolute_url) {
                $_host = $GLOBALS["CONFIG"]["URL_HTTP_LIVE"];
            } else {
                $_host = '';
            }
            $_i18n = '';
            if ($lang != $GLOBALS['CONFIG']['CMS_LOCALE_DEFAULT']) {
                $_i18n = '/' . $lang;
            }

            return $_host . $_i18n . $this->metas[$lang]['url'];
        } else {
            return false;
        }
    }

    /**
     * Sets the active version for this content
     *
     * @param
     *
     * @return version_id
     */
    public function activateVersion($version_id, $lang)
    {
        $this->updated_at = Application::getInstance()->now();
        $this->updated_by = Application::getInstance()->getCurrentUser()->getId();
        $this->metas[$lang]['active_version_id'] = $version_id;

        return $this->save(
            array(
                'updated_at',
                'updated_by'
            ), true
        );
    }

    public function save($keys = "0", $save_meta_keys = false)
    {
        $db = Application::getInstance()->getSql();

        if ($keys == "0") {
            $keys = $this->all_keys;
        } elseif (!is_array($keys)) {
            // if its just one key (as a string)
            $keys = array($keys);
        }

        $sql = "UPDATE " . $this->tablename . " SET ";
        $i = 0;
        foreach ($keys as $key) {
            if ($i > 0) {
                $sql .= ', ';
            }
            if (in_array($key, $this->secure_keys)) {
                $value = sqlsave($this->$key);
            } else {
                $value = sqlunsave($this->$key);
            }
            $sql .= $key . "='" . $value . "'";
            $i++;
        }
        $sql .= " WHERE id=" . $this->id;
        $db->query($sql);
        $rows = $db->affected_rows();

        // now the same for the metas
        if ($save_meta_keys) {
            // first delete the old metas
            $sql = "DELETE FROM " . $this->tablename_metas . " WHERE content_id=" . $this->id . ';';
            $db->query($sql);

            foreach ($GLOBALS['CONFIG']['CMS_LOCALE'] as $lang => $values) {
                // now insert a meta for each lang
                $sql = "INSERT INTO " . $this->tablename_metas . " SET ";
                $sql .= 'content_id=' . $this->id . ',';
                foreach ($this->db_meta_keys as $key) {
                    $sql .= $key . '="' . sqlsave($this->metas[$lang][$key]) . '",';
                }
                $sql .= 'lang="' . $lang . '";';
                $db->query($sql);
            }
        }

        // entities have been changed. Recalc some vars
        $this->calcVars();

        return $rows;
    }

    /**
     * Calc some vars
     *
     * @param
     *
     * @return true
     */
    public function calcVars()
    {
        $_dir = new CmsDir($this->dir_id);
        if (!$_dir->isValid()) {
            return false;
        }

        // calc the url for this content
        foreach ($GLOBALS['CONFIG']['CMS_LOCALE'] as $lang => $values) {
            $this->metas[$lang]['dirpath'] = $_dir->getPath($lang);
            $this->metas[$lang]['filename'] = isset($this->metas[$lang]['filename']) ? $this->metas[$lang]['filename'] : '';
            if (!empty($this->metas[$lang]['use_dirpath'])) {
                $this->metas[$lang]['url'] = $this->metas[$lang]['dirpath'] . $this->metas[$lang]['filename'];
            } else {
                $this->metas[$lang]['url'] = '/' . $this->metas[$lang]['filename'];
            }
            // should the directory index be removed?
            // e.g. /de instead of /de/index
            if (CMS_USE_DIRECTORY_INSTEAD_OF_INDEX_FILENAME) {
                $this->metas[$lang]['url'] = preg_replace('/\/index$/i', '', $this->metas[$lang]['url']);
            }
        }
        $this->url_admin_content_delete = '/administration/cms?a=content_delete&select_dir_id=' . $this->dir_id . '&content_id=' . $this->id;
        $this->url_admin_content_edit = '/administration/cms?a=content_edit&select_dir_id=' . $this->dir_id . '&content_id=' . $this->id;
        $this->url_admin_content_version_new = '/administration/cms?a=version_new&dir_id=' . $this->dir_id . '&content_id=' . $this->id;
        $this->url_admin_content_version_diff = '/administration/cms?a=version_diff&dir_id=' . $this->dir_id . '&content_id=' . $this->id;
        $this->url_admin_explorer_content = '/administration/cms?a=explorer_content&select_dir_id=' . $this->dir_id;
        $this->url_admin_explorer_version = '/administration/cms?a=explorer_version&select_dir_id=' . $this->dir_id . '&content_id=' . $this->id;

        return true;
    }

    /**
     * *********************************************************
     * FRONTEND ************************************************
     * *********************************************************
     */

    /**
     * Returns the url for a content
     *
     * @param language =null
     *
     * @return CmsVersion
     */
    public function getActiveVersion($lang = null)
    {
        return new CmsVersion($this->getActiveVersionId($lang));
    }

    /**
     * Returns the url for a content
     *
     * @param language =null
     *
     * @return active_version_id || bool false
     */
    public function getActiveVersionId($lang = null)
    {
        if (!$lang) {
            $lang = Application::getInstance()->getLanguageCode();
        }

        if (isset($this->metas[$lang]['active_version_id'])) {
            return $this->metas[$lang]['active_version_id'];
        } else {
            return false;
        }
    }

    /**
     * Returns the diff between two versions
     *
     * @param
     *
     * @return string
     */
    public function getVersionsDiff($version1, $version2)
    {
        $_diff_html = wp_text_diff($version1->content1, $version2->content1);

        return $_diff_html;
    }

    /**
     * Loads a content by a version_id into the object
     * and sets the language
     *
     * @param string URL (without protcol and domain)
     *
     * @return 0=error | >0=content_id
     */
    public function loadByVersion($version_id)
    {
        $_version = new CmsVersion($version_id);
        if (!$_version->isValid()) {
            return false;
        }
        $_content_id = $_version->content_id;
        $this->load($_content_id);
        if (!$this->isValid()) {
            return false;
        }
        $_lang = $_version->lang;
        // temporarily overwrite the active_version_id
        $this->metas[$_lang]['active_version_id'] = $version_id;
        // set the language
        $this->setCurrentLang($_lang);
        // also set the application language
        Application::getInstance()->setLanguageCode($_lang);

        return $_content_id;
    }

    /**
     * Loads a content into the object
     *
     * @param value
     * @param key ='id'
     *
     * @return id || false
     */
    public function load($value, $key = 'id')
    {
        if (parent::load($value, $key)) {
            $this->loadMetas();
            $this->calcVars();

            return $this->id;
        } else {
            return false;
        }
    }

    /**
     * Loads the contentmeta
     *
     * @return true
     */
    public function loadMetas()
    {
        $db = Application::getInstance()->getSql();

        // get the content metas
        $db->query("SELECT * FROM " . $this->tablename_metas . " WHERE content_id='" . $this->id . "';");
        while ($db->next_record()) {
            $_lang = $db->f('lang');
            foreach ($this->db_meta_keys as $_key) {
                $this->metas[$_lang][$_key] = $db->f($_key);
            }
        }

        return true;
    }

    /**
     * sets the current language
     *
     * @param string
     *
     * @return 0=error | true=success
     */
    public function setCurrentLang($lang)
    {
        if (isset($GLOBALS['CONFIG']['CMS_LOCALE'][$lang])) {
            $this->current_lang = $lang;
        }

        return true;
    }

    /**
     * Returns the mimetype for a content
     *
     * @return string
     */
    public function getMimetype()
    {
        return $this->mimetype;
    }

    /**
     * Returns the title for a content
     *
     * @param string $lang
     *
     * @return string
     */
    public function getTitle($lang = '')
    {
        if (!$lang && $this->getCurrentLang()) {
            $lang = $this->getCurrentLang();
        }

        if ($lang && !isset($GLOBALS['CONFIG']['CMS_LOCALE'][$lang])) {
            $lang = Application::getInstance()->getLanguageCode();
        }

        return isset($this->metas[$lang]['title']) ? $this->metas[$lang]['title'] : '';
    }

    /**
     * returns the current language
     *
     * @return string
     */
    public function getCurrentLang()
    {
        return $this->current_lang;
    }

    /**
     * Returns the description for a content
     *
     * @param
     *
     * @return string a href
     */
    public function getDescription($lang = '')
    {
        if ($lang == '' && $this->getCurrentLang() != '') {
            $lang = $this->getCurrentLang();
        } elseif ($lang != '' && !isset($GLOBALS['CONFIG']['CMS_LOCALE'][$lang])) {
            // take the default language
            $lang = Application::getInstance()->getLanguageCode();
        }

        if (isset($this->metas[$lang]['description'])) {
            return $this->metas[$lang]['description'];
        } else {
            return '';
        }
    }

    /**
     * Returns the urlencoded tags
     *
     * @param
     *
     * @return string
     */
    public function getTagsUrlencoded()
    {
        $tags = $this->getTags();
        $_array = explode(',', $tags);
        // go through and urlencode
        foreach ($_array as $row => $tag) {
            $_array[$row] = urlencode(trim($tag));
        }
        $tags = implode(',', $_array);

        return $tags;
    }

    /**
     * Returns the tags for a content
     *
     * @param
     *
     * @return string a href
     */
    public function getTags($lang = '')
    {
        if ($lang == '' && $this->getCurrentLang() != '') {
            $lang = $this->getCurrentLang();
        } elseif ($lang != '' && !isset($GLOBALS['CONFIG']['CMS_LOCALE'][$lang])) {
            // take the default language
            $lang = Application::getInstance()->getLanguageCode();
        }

        if (isset($this->metas[$lang]['tags'])) {
            return $this->metas[$lang]['tags'];
        } else {
            return '';
        }
    }

    /**
     * Returns the breadcrumb for this content
     *
     * @param
     *
     * @return string a href
     */
    public function getBreadcrumbHtml()
    {
        $_dir = new CmsDir($this->dir_id);
        if (!$_dir->isValid()) {
            return '';
        }
        $_html = '';
        while ($_dir->id != Dir::ROOT_DIR_ID) {
            $_link = '<a href="' . $_dir->getPath($this->getCurrentLang()) . '">' . $_dir->getTitle($this->getCurrentLang()) . '</a>';
            if ($_html != '') {
                $_link .= ' &gt; ';
            }
            $_html = $_link . $_html;
            $_dir = new CmsDir($_dir->parent_id);
            if (!$_dir->isValid()) {
                return '';
            }
        }

        return $_html;
    }

    /**
     * renders the active version and returns the html-content
     *
     * @param string content_name (=sql_name)
     * @param string lang
     *
     * @return false=error | html
     */
    public function render($content_name, $render_name = 'render_content', $lang = '')
    {
        if ($lang == '' && $this->getCurrentLang() != '') {
            $lang = $this->getCurrentLang();
        } else {
            // take the default language
            $lang = Application::getInstance()->getLanguageCode();
        }
        // load the active version and render
        $_version = new CmsVersion($this->metas[$lang]['active_version_id']);

        if ($_version->isValid() && property_exists($_version, $content_name)) {
            if (!method_exists($this, $render_name)) {
                $render_name = 'render_passthru';
            }

            return trim($this->$render_name($_version->$content_name));
        }

        return '';
    }

    /**
     * *********************************************************
     * RENDER HANDLES ******************************************
     * *********************************************************
     *
     * These rederers are called in the smarty templates:
     * e.g.
     * {render name='content1' method='render_content'};
     */

    /**
     * just pass the content via the engine
     *
     * @param string content
     *
     * @return false=error | html
     */
    public function render_passthru($content)
    {
        if (isset($GLOBALS['CONFIG']['CMS_ENGINES'][$this->engine_id]['function'])) {
            if (method_exists($this, $GLOBALS['CONFIG']['CMS_ENGINES'][$this->engine_id]['function'])) {
                $_engine_name = $GLOBALS['CONFIG']['CMS_ENGINES'][$this->engine_id]['function'];

                return $this->$_engine_name($content);
            }
        }

        return $content;
    }

    /**
     * content
     *
     * @param string content
     *
     * @return false=error | html
     */
    public function render_content($content)
    {
        if (isset($GLOBALS['CONFIG']['CMS_ENGINES'][$this->engine_id]['function'])) {
            if (method_exists($this, $GLOBALS['CONFIG']['CMS_ENGINES'][$this->engine_id]['function'])) {
                $_engine_name = $GLOBALS['CONFIG']['CMS_ENGINES'][$this->engine_id]['function'];

                return $this->$_engine_name($content);
            }
        }

        return $content;
    }

    /**
     * *********************************************************
     * ENGINES *************************************************
     * *********************************************************
     */

    /**
     * render content with shortcode and autop functionality
     *
     * @param content
     *
     * @return string
     */
    public function engine_static_shortcode_autop($content)
    {
        return $this->raw_formatter($content, true);
    }

    /**
     * disables shortcodes
     * credits: http://css-tricks.com/snippets/wordpress/disable-automatic-formatting-using-a-shortcode/
     *
     * @ToDo: check if wptexturize(); is also needed here
     *
     * @param content
     * @param bool use_autop_formatting=false
     *
     * @return content
     */
    public function raw_formatter($content, $use_autop_formatting = false)
    {
        $new_content = '';
        $pattern_full = '{(\[raw\].*?\[/raw\])}is';
        $pattern_contents = '{\[raw\](.*?)\[/raw\]}is';
        $pieces = preg_split($pattern_full, $content, -1, PREG_SPLIT_DELIM_CAPTURE);

        foreach ($pieces as $piece) {
            if (preg_match($pattern_contents, $piece, $matches)) {
                $new_content .= $matches[1];
            } else {
                if ($use_autop_formatting) {
                    $new_content .= do_shortcode(wpautop($piece));
                } else {
                    $new_content .= do_shortcode($piece);
                }
            }
        }

        return $new_content;
    }

    /**
     * render content with shortcode functionality
     *
     * @param content
     *
     * @return string
     */
    public function engine_static_shortcode($content)
    {
        return $this->raw_formatter($content, false);
    }

    /**
     * render content without any modification
     *
     * @param content
     *
     * @return string
     */
    public function engine_static($content)
    {
        return $content;
    }

    /**
     * render/eval php content
     *
     * @param content /code
     *
     * @return string
     */
    public function engine_php($content)
    {
        return eval('?>' . $content);
    }
    
    /**
     * Returns the rendered active version content of the content
     *
     * @return string
     */
    public function getContent()
    {
        $_html = '';
        if ($this->isValid()) {
            $view = Application::getInstance()->getView();
            $view->set('content', $this);
            $_html = $view->fetch($this->getTemplate());
        }

        return $_html;
    }

    /**
     * Returns the template for a content
     *
     * @param
     *
     * @return string a href
     */
    public function getTemplate()
    {
        if (isset($GLOBALS['CONFIG']['CMS_TEMPLATES'][$this->template_id]['template'])) {
            return $GLOBALS['CONFIG']['CMS_TEMPLATES'][$this->template_id]['template'];
        }

        return $GLOBALS['CONFIG']['CMS_TEMPLATES'][1]['template'];
    }
}