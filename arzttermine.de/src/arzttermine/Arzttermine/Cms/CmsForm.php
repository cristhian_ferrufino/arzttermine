<?php

namespace Arzttermine\Cms;

use Arzttermine\Application\Application;

class CmsForm {

    // Filthy hack
    public static $root_url = '/administration/cms';

    /**
     * @param $form
     * @param $dir_id
     * @param string $action
     *
     * @return string
     */
    public static function getDirNew($form, $dir_id)
    {
        $view = Application::getInstance()->getView();

        $view->set('url_form_action', self::$root_url);
        $_dir = new CmsDir($dir_id);
        $view->set('url_cancel', $_dir->url_admin_explorer_content);
        $view->set('form', $form);
        // reform the data for smarty (indexed) arrays
        $_form_lang = array_values($GLOBALS['CONFIG']['CMS_LOCALE']);
        $_index = 0;
        foreach ($GLOBALS['CONFIG']['CMS_LOCALE'] as $lang => $values) {
            $_form_lang[$_index]['name'] = isset($form['name_' . $lang]) ? $form['name_' . $lang] : '';
            $_form_lang[$_index]['title'] = isset($form['title_' . $lang]) ? $form['title_' . $lang] : '';
            $_index++;
        }
        $view->set('form_lang', $_form_lang);
        $view->set('parent_id_select', self::getHtmlParentDirsSelect($dir_id, 'parent_id'));

        return $view->fetch('admin/cms/form_dir_new.tpl');
    }

    /**
     * @param $selected_id
     * @param $name
     *
     * @return string
     */
    public static function getHtmlParentDirsSelect($selected_id, $name)
    {
        global $HTML_DIR_BOX;

        $html = '<select name="form[' . $name . ']" size="10" style="width:300px;">';
        /* build the root dir */
        if ($selected_id == CmsDir::ROOT_DIR_ID) {
            $html .= '<option value="' . CmsDir::ROOT_DIR_ID . '" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;/';
        } else {
            $html .= '<option value="' . CmsDir::ROOT_DIR_ID . '">&nbsp;&nbsp;&nbsp;&nbsp;/';
        }

        $HTML_DIR_BOX = '';
        self::getHtmlParentSubDirsSelect(CmsDir::ROOT_DIR_ID, $selected_id);
        $html .= $HTML_DIR_BOX . '</select>';

        return $html;
    }

    /**
     * @param $dir_id
     * @param $selected_id
     *
     * @return int
     */
    public static function getHtmlParentSubDirsSelect($dir_id, $selected_id)
    {
        global $HTML_DIR_BOX, $SPACER;
        $app = Application::getInstance();
        $db = $app->getSql();

        $db->query("SELECT * FROM " . DB_TABLENAME_CMS_DIRS . " WHERE parent_id=" . $dir_id . ";");
        if ($db->nf() == 0) {
            return 0;
        }

        /* calc the size of the spaces */
        $SPACER++;
        $SPACES = "";
        for ($i = 0; $i < $SPACER; $i++) {
            $SPACES .= "&nbsp;&nbsp;&nbsp;&nbsp;";
        }

        while ($db->next_record()) {
            $_dir_ids[] = $db->f("id");
        }
        foreach ($_dir_ids as $_dir_id) {
            $dir = new CmsDir($_dir_id);
            if ($_dir_id == $selected_id) {
                $HTML_DIR_BOX .= '<option value="' . $_dir_id . '" selected="selected">' . $SPACES . $dir->getPath($app->getLanguageCode());
            } else {
                $HTML_DIR_BOX .= '<option value="' . $_dir_id . '">' . $SPACES . $dir->getPath($app->getLanguageCode());
            }

            self::getHtmlParentSubDirsSelect($_dir_id, $selected_id);
        }
        $SPACER--;

        return 0;
    }

    /**
     * @param $form
     * @param string $action
     *
     * @return string
     */
    public static function getDirEdit($form)
    {
        $view = Application::getInstance()->getView();

        $view->set('url_form_action', self::$root_url);
        $_dir = new CmsDir($form['id']);
        $view->set('url_cancel', $_dir->url_admin_explorer_content);
        $view->set('form', $form);
        // reform the data for smarty (indexed) arrays
        $_form_lang = array_values($GLOBALS['CONFIG']['CMS_LOCALE']);
        $_index = 0;
        foreach ($GLOBALS['CONFIG']['CMS_LOCALE'] as $lang => $values) {
            $_form_lang[$_index]['name'] = $form['name_' . $lang];
            $_form_lang[$_index]['title'] = $form['title_' . $lang];
            $_index++;
        }
        $view->set('form_lang', $_form_lang);
        $view->set('parent_id_select', self::getHtmlParentDirsSelect($form['parent_id'], 'parent_id'));

        return $view->fetch('admin/cms/form_dir_edit.tpl');
    }

    /**
     * @param $form
     *
     * @return mixed
     */
    public static function checkDirEdit($form)
    {
        return self::checkDirNew($form);
    }

    /**
     * @param $form
     *
     * @return mixed
     */
    public static function checkDirNew($form)
    {
        $_index = 0;
        $app = Application::getInstance();
        $view = $app->container->get('templating');

        foreach ($GLOBALS['CONFIG']['CMS_LOCALE'] as $lang => $values) {
            if (mb_strlen($form['name_' . $lang], $GLOBALS["CONFIG"]["SYSTEM_CHARSET"]) < 1) {
                $view->addMessage('FORM_DIR_NEW_NAME_' . $lang, 'ERROR', $app->static_text('need_dirname'));
            }
            if (mb_strlen($form['title_' . $lang], $GLOBALS["CONFIG"]["SYSTEM_CHARSET"]) < 1) {
                $view->addMessage('FORM_DIR_NEW_TITLE_' . $lang, 'ERROR', $app->static_text('need_dirtitle'));
            }
            $_index++;
        }

        return $form;
    }

    /**
     * @param $form
     * @param string $action
     *
     * @return string
     */
    public static function getDirDelete($form)
    {
        $view = Application::getInstance()->getView();

        $view->set('url_form_action', self::$root_url);
        $_dir = new CmsDir($form['id']);
        $view->set('url_cancel', $_dir->url_admin_explorer_content);
        $view->set('form', $form);

        // reform the data for smarty (indexed) arrays
        $_form_lang = array_values($GLOBALS['CONFIG']['CMS_LOCALE']);
        $_index = 0;
        foreach ($GLOBALS['CONFIG']['CMS_LOCALE'] as $lang => $values) {
            $_form_lang[$_index]['dirpath'] = $_dir->getPath($lang);
            $_index++;
        }
        $view->set('form_lang', $_form_lang);

        return $view->fetch('admin/cms/form_dir_delete.tpl');
    }

    /**
     * @param $form
     * @param string $action
     *
     * @return string
     */
    public static function getContentNew($form)
    {
        $view = Application::getInstance()->getView();

        $view->set('url_form_action', self::$root_url);
        $view->set('url_cancel', self::$root_url);

        $form['dir_id__message'] = $view->getHtmlFormMessages('FORM_CONTENT_NEW_DIR_ID', 'field');
        $form['template_id__message'] = $view->getHtmlFormMessages('FORM_CONTENT_NEW_TEMPLATE_ID', 'field');
        $form['engine_id__message'] = $view->getHtmlFormMessages('FORM_CONTENT_NEW_ENGINE_ID', 'field');
        $form['mimetype__message'] = $view->getHtmlFormMessages('FORM_CONTENT_NEW_MIMETYPE', 'field');
        $view->set('form', $form);

        $view->set('template_ids', array_keys($GLOBALS['CONFIG']['CMS_TEMPLATES']));
        $_template_values = array();
        foreach ($GLOBALS['CONFIG']['CMS_TEMPLATES'] as $value) {
            $_template_values[] = $value['name'];
        }
        $view->set('template_values', $_template_values);

        $view->set('engine_ids', array_keys($GLOBALS['CONFIG']['CMS_ENGINES']));
        $_engine_values = array();
        foreach ($GLOBALS['CONFIG']['CMS_ENGINES'] as $value) {
            $_engine_values[] = $value['name'];
        }
        $view->set('engine_values', $_engine_values);

        // reform the data for smarty (indexed) arrays
        $_form_lang = array_values($GLOBALS['CONFIG']['CMS_LOCALE']);
        $_dir = new CmsDir($form['dir_id']);
        $view->set('url_cancel', $_dir->url_admin_explorer_content);
        $_index = 0;
        foreach ($GLOBALS['CONFIG']['CMS_LOCALE'] as $lang => $values) {
            if ($_dir->isValid()) {
                $_form_lang[$_index]['dirpath'] = $_dir->getPath($lang);
            }
            $_form_lang[$_index]['title'] = isset($form['title_' . $lang]) ? $view->form($form['title_' . $lang]) : '';
            $_form_lang[$_index]['title__form_input_class'] = $view->hasMessages('FORM_CONTENT_NEW_TITLE_' . $lang, ' input_error', 1);
            $_form_lang[$_index]['title__message'] = $view->getHtmlFormMessages('FORM_CONTENT_NEW_TITLE_' . $lang, 'field');
            $_form_lang[$_index]['use_dirpath'] = $view->form($form['use_dirpath_' . $lang]);
            $_form_lang[$_index]['use_dirpath__form_input_class'] = $view->hasMessages('FORM_CONTENT_NEW_PATH_' . $lang, ' input_error', 1);
            $_form_lang[$_index]['use_dirpath__message'] = $view->getHtmlFormMessages('FORM_CONTENT_NEW_PATH_' . $lang, 'field');
            $_form_lang[$_index]['filename'] = isset($form['filename_' . $lang]) ? $view->form($form['filename_' . $lang]) : '';
            $_form_lang[$_index]['filename__form_input_class'] = $view->hasMessages('FORM_CONTENT_NEW_FILENAME_' . $lang, ' input_error', 1);
            $_form_lang[$_index]['filename__message'] = $view->getHtmlFormMessages('FORM_CONTENT_NEW_FILENAME_' . $lang, 'field');
            $_form_lang[$_index]['tags'] = isset($form['tags_' . $lang]) ? $view->form($form['tags_' . $lang]) : '';
            $_form_lang[$_index]['tags__form_input_class'] = $view->hasMessages('FORM_CONTENT_NEW_TAGS_' . $lang, ' input_error', 1);
            $_form_lang[$_index]['tags__message'] = $view->getHtmlFormMessages('FORM_CONTENT_NEW_TAGS_' . $lang, 'field');
            $_form_lang[$_index]['description'] = isset($form['description_' . $lang]) ? $view->form($form['description_' . $lang]) : '';
            $_form_lang[$_index]['description__form_input_class'] = $view->hasMessages('FORM_CONTENT_NEW_DESCRIPTION_' . $lang, ' input_error', 1);
            $_form_lang[$_index]['description__message'] = $view->getHtmlFormMessages('FORM_CONTENT_NEW_DESCRIPTION_' . $lang, 'field');
            $_index++;
        }
        $view->set('form_lang', $_form_lang);

        return $view->fetch('admin/cms/form_content_new.tpl');
    }

    /**
     * @param $form
     * @param string $action
     *
     * @return string
     */
    public static function getContentEdit($form)
    {
        $view = Application::getInstance()->getView();

        $view->set('url_form_action', self::$root_url);
        $view->set('url_cancel', self::$root_url);

        $form['dir_id__message'] = $view->getHtmlFormMessages('FORM_CONTENT_NEW_DIR_ID', 'field');
        $form['template_id__message'] = $view->getHtmlFormMessages('FORM_CONTENT_NEW_TEMPLATE_ID', 'field');
        $form['engine_id__message'] = $view->getHtmlFormMessages('FORM_CONTENT_NEW_ENGINE_ID', 'field');
        $form['mimetype__message'] = $view->getHtmlFormMessages('FORM_CONTENT_NEW_MIMETYPE', 'field');
        $view->set('form', $form);
        $view->set('dir_id_select', self::getHtmlParentDirsSelect($form['dir_id'], 'dir_id'));

        $view->set('template_ids', array_keys($GLOBALS['CONFIG']['CMS_TEMPLATES']));
        $_template_values = array();
        foreach ($GLOBALS['CONFIG']['CMS_TEMPLATES'] as $id => $value) {
            $_template_values[] = $value['name'];
        }
        $view->set('template_values', $_template_values);

        $view->set('engine_ids', array_keys($GLOBALS['CONFIG']['CMS_ENGINES']));
        $_engine_values = array();
        foreach ($GLOBALS['CONFIG']['CMS_ENGINES'] as $id => $value) {
            $_engine_values[] = $value['name'];
        }
        $view->set('engine_values', $_engine_values);

        // reform the data for smarty (indexed) arrays
        $_form_lang = array_values($GLOBALS['CONFIG']['CMS_LOCALE']);
        $_dir = new CmsDir($form['dir_id']);
        $view->set('url_cancel', $_dir->url_admin_explorer_content);
        $_index = 0;
        foreach ($GLOBALS['CONFIG']['CMS_LOCALE'] as $lang => $values) {
            if ($_dir->isValid()) {
                $_form_lang[$_index]['dirpath'] = $_dir->getPath($lang);
            }
            $_form_lang[$_index]['title'] = $view->form($form['title_' . $lang]);
            $_form_lang[$_index]['title__form_input_class'] = $view->hasMessages('FORM_CONTENT_NEW_TITLE_' . $lang, ' input_error', 1);
            $_form_lang[$_index]['title__message'] = $view->getHtmlFormMessages('FORM_CONTENT_NEW_TITLE_' . $lang, 'field');
            $_form_lang[$_index]['use_dirpath'] = $view->form($form['use_dirpath_' . $lang]);
            $_form_lang[$_index]['use_dirpath__form_input_class'] = $view->hasMessages('FORM_CONTENT_NEW_PATH_' . $lang, ' input_error', 1);
            $_form_lang[$_index]['use_dirpath__message'] = $view->getHtmlFormMessages('FORM_CONTENT_NEW_PATH_' . $lang, 'field');
            $_form_lang[$_index]['filename'] = $view->form($form['filename_' . $lang]);
            $_form_lang[$_index]['filename__form_input_class'] = $view->hasMessages('FORM_CONTENT_NEW_FILENAME_' . $lang, ' input_error', 1);
            $_form_lang[$_index]['filename__message'] = $view->getHtmlFormMessages('FORM_CONTENT_NEW_FILENAME_' . $lang, 'field');
            $_form_lang[$_index]['tags'] = $view->form($form['tags_' . $lang]);
            $_form_lang[$_index]['tags__form_input_class'] = $view->hasMessages('FORM_CONTENT_NEW_TAGS_' . $lang, ' input_error', 1);
            $_form_lang[$_index]['tags__message'] = $view->getHtmlFormMessages('FORM_CONTENT_NEW_TAGS_' . $lang, 'field');
            $_form_lang[$_index]['description'] = $view->form($form['description_' . $lang]);
            $_form_lang[$_index]['description__form_input_class'] = $view->hasMessages('FORM_CONTENT_NEW_DESCRIPTION_' . $lang, ' input_error', 1);
            $_form_lang[$_index]['description__message'] = $view->getHtmlFormMessages('FORM_CONTENT_NEW_DESCRIPTION_' . $lang, 'field');
            $_index++;
        }
        $view->set('form_lang', $_form_lang);

        return $view->fetch('admin/cms/form_content_edit.tpl');
    }

    /**
     * @param $form
     *
     * @return mixed
     */
    public static function checkContentEdit($form)
    {
        return self::checkContentNew($form);
    }

    /**
     * @param $form
     *
     * @return mixed
     */
    public static function checkContentNew($form)
    {
        $view = Application::getInstance()->getView();

        $_index = 0;
        $app = Application::getInstance();

        foreach ($GLOBALS['CONFIG']['CMS_LOCALE'] as $lang => $values) {
            // sanitize
            $form['tags_' . $lang] = sanitize_tags($form['tags_' . $lang]);

            // check
            if (mb_strlen($form['title_' . $lang], $GLOBALS["CONFIG"]["SYSTEM_CHARSET"]) < 1) {
                $view->addMessage('FORM_CONTENT_NEW_TITLE_' . $lang, 'ERROR', $app->static_text('need_content_title'));
            }
            if (mb_strlen($form['filename_' . $lang], $GLOBALS["CONFIG"]["SYSTEM_CHARSET"]) < 1) {
                $view->addMessage('FORM_CONTENT_NEW_FILENAME_' . $lang, 'ERROR', $app->static_text('need_content_filename'));
            }
            // no root slash at the beginning!
            if (mb_strpos($form['filename_' . $lang], '/', 0, $GLOBALS["CONFIG"]["SYSTEM_CHARSET"]) === 0) {
                $view->addMessage('FORM_CONTENT_NEW_FILENAME_' . $lang, 'ERROR', $app->static_text('root_slash_not_allowed'));
            }
            $_index++;
        }

        // check if the dir_id is valid
        $_dir = new CmsDir($form['dir_id']);
        if (!$_dir->isValid()) {
            $view->addMessage('FORM_CONTENT_NEW_DIR_ID', 'ERROR', $app->static_text('dir_id_is_not_valid'));
        }
        // check if the template_id is valid
        if (!isset($GLOBALS['CONFIG']['CMS_TEMPLATES'][$form['template_id']])) {
            $view->addMessage('FORM_CONTENT_NEW_TEMPLATE_ID', 'ERROR', $app->static_text('template_id_is_not_valid'));
        }
        // check if the engine_id is valid
        if (!isset($GLOBALS['CONFIG']['CMS_ENGINES'][$form['engine_id']])) {
            $view->addMessage('FORM_CONTENT_NEW_ENGINE_ID', 'ERROR', $app->static_text('engine_id_is_not_valid'));
        }

        return $form;
    }

    /**
     * @param $form
     * @param string $action
     *
     * @return string
     */
    public static function getContentDelete($form)
    {
        $view = Application::getInstance()->getView();

        $view->set('url_form_action', self::$root_url);
        $_content = new CmsContent($form['id']);
        $view->set('url_cancel', $_content->url_admin_explorer_content);
        $view->set('form', $form);

        // reform the data for smarty (indexed) arrays
        $_form_lang = array_values($GLOBALS['CONFIG']['CMS_LOCALE']);
        $_index = 0;
        foreach ($GLOBALS['CONFIG']['CMS_LOCALE'] as $lang => $values) {
            $_form_lang[$_index]['title'] = $view->form($form['title_' . $lang]);
            $_form_lang[$_index]['filename'] = $view->form($form['filename_' . $lang]);
            $_form_lang[$_index]['url'] = $view->form($form['url_' . $lang]);
            $_form_lang[$_index]['tags'] = $view->form($form['tags_' . $lang]);
            $_form_lang[$_index]['description'] = $view->form($form['description_' . $lang]);
            $_index++;
        }
        $view->set('form_lang', $_form_lang);

        return $view->fetch('admin/cms/form_content_delete.tpl');
    }

    /**
     * @param $form
     * @param string $action
     *
     * @return string
     */
    public static function getVersionNew($form)
    {
        $view = Application::getInstance()->getView();

        $view->set('url_form_action', self::$root_url);
        $view->set('media_lang', getStaticUrl($GLOBALS['CONFIG']['CMS_LOCALE'][$form['lang']]['icon']));
        $_content = new CmsContent($form['content_id']);
        $view->set('filename', $_content->getUrl($form['lang']));
        $view->set('url_cancel', $_content->url_admin_explorer_version);

        $form['content_id__message'] = $view->getHtmlFormMessages('FORM_VERSION_NEW_CONTENT_ID', 'field');
        $form['lang__message'] = $view->getHtmlFormMessages('FORM_VERSION_LANG', 'field');
        $form['dir_id'] = $_content->dir_id;
        $view->set('form', $form);

        // set the form for each version content
        $_index = 0;
        $_html_editors = '';

        foreach ($GLOBALS['CONFIG']['CMS_TEMPLATES'][$_content->template_id]['version_contents'] as $id => $values) {
            $_form_editor['version_content_id'] = $values['version_content'];
            $_form_editor['name'] = $values['name'];
            foreach ($values['editors'] as $_key => $_template) {
                $_editor_name = $_key;
                $_editor_template = $_template;
                // use the first editor
                // todo: check if the editor is useable, if not change to default
                // break;
            }
            // supress the php notice :)
            if (!isset($form[$values['version_content']])) {
                $form[$values['version_content']] = '';
            }
            // format the initial!!! textarea value
            if ($_editor_name == 'tinymce') {
                $_form_editor['version_content'] = wp_richedit_pre($form[$values['version_content']]);
            } else {
                $_form_editor['version_content'] = wp_htmledit_pre($form[$values['version_content']]);
            }
            // dont change the editor_id as wordpress uses this (#content) everywhere!!!
            $view->set('editor_id', 'content');
            $view->set('form_editor', $_form_editor);
            $_html_editors .= $view->fetch($_editor_template);
            $_index++;
        }
        $view->set('editors', $_html_editors);

        return $view->fetch('admin/cms/form_version_new.tpl');
    }

    /**
     * @param $form
     * @param string $action
     *
     * @return string
     */
    public static function getVersionEdit($form)
    {
        $view = Application::getInstance()->getView();

        $view->set('url_form_action', self::$root_url);
        $view->set('media_lang', getStaticUrl($GLOBALS['CONFIG']['CMS_LOCALE'][$form['lang']]['icon']));
        $_content = new CmsContent($form['content_id']);
        $view->set('filename', $_content->getUrl($form['lang']));
        $view->set('url_cancel', $_content->url_admin_explorer_version);

        $form['content_id__message'] = $view->getHtmlFormMessages('FORM_VERSION_NEW_CONTENT_ID', 'field');
        $form['content__message'] = $view->getHtmlFormMessages('FORM_VERSION_CONTENT', 'field');
        $form['dir_id'] = $_content->dir_id;
        $view->set('form', $form);

        // set the form for each version content
        $_index = 0;
        $_html_editors = '';

        foreach ($GLOBALS['CONFIG']['CMS_TEMPLATES'][$_content->template_id]['version_contents'] as $id => $values) {
            $_form_editor['version_content_id'] = $values['version_content'];
            $_form_editor['name'] = $values['name'];
            // set the form for the editor
            foreach ($values['editors'] as $_key => $_template) {
                $_editor_name = $_key;
                $_editor_template = $_template;
                // use the first editor
                // todo: check if the editor is useable, if not change to default
                // break;
            }
            // format the initial!!! textarea value
            if ($_editor_name == 'tinymce') {
                $_form_editor['version_content'] = wp_richedit_pre($form[$values['version_content']]);
            } else {
                $_form_editor['version_content'] = wp_htmledit_pre($form[$values['version_content']]);
            }
            // dont change the editor_id as wordpress uses this (#content) everywhere!!!
            $view->set('editor_id', 'content');
            $view->set('form_editor', $_form_editor);
            $_html_editors .= $view->fetch($_editor_template);
            $_index++;
        }
        $view->set('editors', $_html_editors);

        return $view->fetch('admin/cms/form_version_edit.tpl');
    }

    /**
     * @param $form
     *
     * @return mixed
     */
    public static function checkVersionEdit($form)
    {
        return self::checkVersionNew($form);
    }

    /**
     * @param $form
     *
     * @return mixed
     */
    public static function checkVersionNew($form)
    {
        $view = Application::getInstance()->getView();

        $app = Application::getInstance();
        // check if the content_id is valid
        $_content = new CmsContent($form['content_id']);
        if (!$_content->isValid()) {
            $view->addMessage('FORM_VERSION_NEW_CONTENT_ID', 'ERROR', $app->static_text('content_id_is_not_valid'));
        }

        return $form;
    }

    /**
     * @param $form
     * @param string $action
     *
     * @return string
     */
    public static function getVersionDelete($form)
    {
        $view = Application::getInstance()->getView();

        $view->set('url_form_action', self::$root_url);
        $view->set('media_lang', getStaticUrl($GLOBALS['CONFIG']['CMS_LOCALE'][$form['lang']]['icon']));
        $_version = new CmsVersion($form['id']);
        $view->set('version_nr', $_version->version_nr);
        $view->set('created_at', $_version->created_at);
        $_content = new CmsContent($form['content_id']);
        $view->set('filename', $_content->getUrl($form['lang']));
        $view->set('url_cancel', $_content->url_admin_explorer_version);
        $view->set('form', $form);

        return $view->fetch('admin/cms/form_version_delete.tpl');
    }
}