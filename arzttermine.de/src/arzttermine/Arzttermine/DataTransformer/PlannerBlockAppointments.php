<?php

namespace Arzttermine\DataTransformer;

use Arzttermine\Appointment\PlannerBlock;
use Arzttermine\Core\DateTime;

class PlannerBlockAppointments {

    /**
     * @param array $calendar_events
     * 
     * @return PlannerBlock[]
     */
    public static function toBlocks(array $calendar_events)
    {
        $blocks = array();
        
        foreach($calendar_events as $event) {
            $block = new PlannerBlock();
            $block
                ->setStartTime(new DateTime($event['start']))
                ->setEndTime(new DateTime($event['end']))
                ->setLength($event['metadata']['blockLength'])
                ->setTreatmentTypeIds($event['metadata']['treatmentTypes'])
                ->setInsuranceIds($event['metadata']['insuranceTypes'])
                ->setRecurrenceInterval($event['metadata']['interval'])
                ->setRecurUntil($event['metadata']['until'])
                ->setExclusions($event['metadata']['exclusions']);
            
            $blocks[] = $block;
        }
        
        return $blocks;
    }

    /**
     * @param PlannerBlock[] $blocks
     * 
     * @return array
     */
    public static function toFullCalendar(array $blocks)
    {
        $calendar_events = array();
        
        foreach($blocks as $block) {
            // For displaying the time in a more easily readable format
            $block_label = $block->getLength() >= 60 ? ($block->getLength() / 60) . 'St' : $block->getLength() . 'Min';
            
            $block_data = array(
                // Custom title format only for the calendar. We don't need to display seconds
                'title' => "{$block->getStartTime()->format('H:i')} - {$block->getEndTime()->format('H:i')} ({$block_label})",
                'start' => $block->getStartTime()->getDateTime(),
                'end'   => $block->getEndTime()->getDateTime(),
                'className' => 'planner-block',
                'metadata' => array(
                    'blockLength'    => $block->getLength(), // length is a reserved keyword in JS
                    'treatmentTypes' => $block->getTreatmentTypeIds(),
                    'insuranceTypes' => $block->getInsuranceIds(),
                    'interval'       => $block->getRecurrenceInterval(),
                    'until'          => $block->getRecurUntil(),
                    'exclusions'     => array_map(function($date) {
                                            /** @var DateTime $date */
                                            return $date->getDate();
                                        }, $block->getExclusions())
                )
            );
            
            // If it's a recurring event, tag it with an ID so we can generate the appropriate blocks
            if ($block->getRecurrenceInterval()) {
                $block_data['id'] = 'recurring_event_' . $block->getId();
            }

            $calendar_events[] = $block_data;
        }
        
        return $calendar_events;
    }

}