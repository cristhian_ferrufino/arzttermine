<?php

namespace Arzttermine\Application;

use Arzttermine\Security\Authentication;
use Arzttermine\Core\ControllerResolver;
use Arzttermine\DependencyInjection\ServiceProviderInterface;
use Arzttermine\Http\Request;
use Arzttermine\Router\Router;
use Arzttermine\View\View;
use Arzttermine\Widget\Container as WidgetContainer;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Class Application
 * 
 * Implements HttpKernelInterface and TerminableInterface, but PHP's policy on
 * Interfaces and Subclasses disallows the directive
 */
class Application {

    /**
     * Singleton instance of the class
     *
     * @todo Remove reliance on the singleton pattern for Application and rely on container injection
     * 
     * @var Application
     * @static
     */
    private static $instance;

    /**
     * Dependency Injection container
     * 
     * @var Container
     */
    public $container;

    /**
     * @var ServiceProviderInterface[]
     */
    private $service_providers = array();

    /**
     * If the application has initialized itself, this is true
     * 
     * @var bool
     */
    private $is_booted = false;

    // DEPRECATED ============================================================================

    /**
     * @var WidgetContainer
     */
    protected $widget_container;

    /**
     * @var bool Flag to tell us if the app should be bootstrapped as a widget or as a full site
     */
    private $is_widget = false;
    
    // =======================================================================================

    /**
     * Singleton accessor
     * 
     * @todo Remove singleton
     *
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
    
        return self::$instance;
    }

    /**
     * @deprecated 
     * 
     * @param Authentication $current_user
     *
     * @return self
     */
    public function setCurrentUser($current_user)
    {
        $this->container->set('current_user', $current_user);

        return $this;
    }

    /**
     * @deprecated
     * 
     * @return Authentication
     */
    public function getCurrentUser()
    {
        return $this->container->get('current_user');
    }

    /**
     * @deprecated DO NOT USE THIS
     * 
     * @param View $view
     *
     * @return self
     */
    public function setView(View $view)
    {
        $this->container->set('templating', $view);

        return $this;
    }

    /**
     * @deprecated
     * 
     * @return View
     */
    public function getView()
    {
        return $this->container->get('templating');
    }

    /**
     * Constructor. Determines the application's purpose
     */
    private function __construct()
    {
        // This will hold our services and configuration
        $this->container = new Container();

        // Environment
        $this->container->setParameter('app.environment', $GLOBALS['CONFIG']['SYSTEM_ENVIRONMENT']);
        
        // Default source platform
        $this->container->setParameter('app.source_platform', 'web');

        // Session handler and the main request object
        $session = new Session();

        Request::setTrustedProxies(['172.31.0.0/16']);
        $request = Request::createFromGlobals();

        // Bind the session to the request
        $request->setSession($session);

        $this->container->set('session', $session);
        $this->container->set('request', $request);
        
        // Set up the kernel
        $controller_resolver = new ControllerResolver($this->container);
        $request_stack       = new RequestStack();
        $dispatcher          = new EventDispatcher();
        
        $this->container->set('resolver', $controller_resolver);
        $this->container->set('request_stack', $request_stack);
        $this->container->set('dispatcher', $dispatcher);
        
        $http_kernel = new HttpKernel($dispatcher, $controller_resolver, $request_stack);
        
        $this->container->set('http_kernel', $http_kernel);

        // Admin area should be secured. This is a shim
        // @todo Remove
        if (preg_match('/^\/admin/', $this->container->get('request')->getRequestUri())) {
            $this->container->setParameter('app.secure', true);

            // Deactivate anayltics and social media, as we're in the admin
            $GLOBALS['CONFIG']['GOOGLE_ANALYTICS_ACTIVATE'] = false;
            $GLOBALS['CONFIG']['SOCIALMEDIA_ACTIVATE'] = false;
        } else {
            $this->container->setParameter('app.secure', false);
        }
    }

    /**
     * Bootstrap required services and set important variables here
     */
    protected function boot()
    {
        if ($this->is_booted) {
            return;
        }
        
        // Bootstrap services in the container
        foreach($this->service_providers as $service_provider) {
            if ($service_provider instanceof ServiceProviderInterface) {
                $service_provider->boot($this->container);
            }
        }
        
        $this->is_booted = true;
    }

    /**
     * Execute the application
     */
    public function run()
    {
        if (!$this->container->has('request')) {
            $this->container->set('request', new Request($_GET, $_POST, array(), $_COOKIE, $_FILES, $_SERVER));
        }
        
        $this->boot();

        /** @var Request $request */
        $request = $this->container->get('request');
        
        $response = $this->handle($request);
        $response->send();
        $this->terminate($request, $response);
    }

    /**
     * Handles a Request to convert it to a Response.
     *
     * When $catch is true, the implementation must catch all exceptions
     * and do its best to convert them to a Response instance.
     *
     * @param Request $request A Request instance
     * @param int     $type    The type of the request
     *                          (one of HttpKernelInterface::MASTER_REQUEST or HttpKernelInterface::SUB_REQUEST)
     * @param bool    $catch Whether to catch exceptions or not
     *
     * @return Response A Response instance
     *
     * @throws \Exception When an Exception occurs during processing
     */
    public function handle(Request $request, $type = HttpKernelInterface::MASTER_REQUEST, $catch = true)
    {
        if (!$this->is_booted) {
            $this->boot();
        }

        $response = $this->container->get('http_kernel')->handle($request, $type, $catch);

        return $response;
    }

    /**
     * Terminates a request/response cycle.
     *
     * Should be called after sending the response and before shutting down the kernel.
     *
     * @param Request  $request  A Request instance
     * @param Response $response A Response instance
     */
    public function terminate(Request $request, Response $response)
    {
        $this->container->get('http_kernel')->terminate($request, $response);
    }

    /**
     * Register a service and store it in the container
     * 
     * @param ServiceProviderInterface $service
     *
     * @return self
     */
    public function registerService(ServiceProviderInterface $service)
    {
        $this->service_providers[] = $service;
        $service->register($this->container);
        
        return $this;
    }

    /**
     * @deprecated Use controller sugar or fetch from container
     * 
     * @return Request
     */
    public function getRequest()
    {
        return $this->container->get('request');
    }

    /**
     * Sets/Adds a config value
     *
     * @param string $key
     * @param string $value
     * @return self
     */
    public function setConfig($key, $value)
    {
        if (!empty($key)) {
            $GLOBALS['CONFIG'][$key] = $value;
        }
        return $this;
    }

    /**
     * Returns the config
     *
     * @param string $key
     * @param mixed $default
     * 
     * @return mixed
     */
    public function getConfig($key = null, $default = null)
    {
        return isset($GLOBALS['CONFIG'][$key]) ? $GLOBALS['CONFIG'][$key] : $default;
    }

    /**
     * Set the widget status
     *
     * @param bool $is_widget
     *
     * @return self
     */
    public function setIsWidget($is_widget = false)
    {
        $this->is_widget = (bool)$is_widget;

        return $this;
    }

    /**
     * @param WidgetContainer $widgetContainer
     *
     * @return self
     */
    public function setWidgetContainer(WidgetContainer $widgetContainer)
    {
        $this->widget_container = $widgetContainer;

        return $this;
    }

    /**
     * @return WidgetContainer
     */
    public function getWidgetContainer()
    {
        return $this->widget_container;
    }

    /**
     * Tells us if the application is in widget mode or not
     * 
     * @deprecated Use application context
     *
     * @return boolean
     */
    public function isWidget()
    {
        return (bool)$this->is_widget;
    }

    /**
     * Set the valid user flag
     * 
     * @deprecated
     *
     * @param bool $need_valid_user
     *
     * @return self
     */
    public function setNeedValidUser($need_valid_user)
    {
        $this->container->setParameter('app.secure', (bool)$need_valid_user);

        return $this;
    }

    /**
     * Check if the app needs a valid user or not
     * 
     * @deprecated
     *
     * @return bool
     */
    public function needsValidUser()
    {
        return (bool)$this->container->getParameter('app.secure');
    }

    /**
     * @return Session
     */
    public function getSession()
    {
        return $this->container->get('session');
    }

    /**
     * @deprecated Use controller sugar instead or fetch from container
     * 
     * @return \Arzttermine\Db\MySQLConnection
     */
    public function getSql()
    {
        return $this->container->get('database');
    }

    /**
     * @param string $source_platform
     */
    public function setSourcePlatform($source_platform)
    {
        $this->container->setParameter('app.source_platform', $source_platform);
    }

    /**
     * @return string
     */
    public function getSourcePlatform()
    {
        return $this->container->getParameter('app.source_platform');
    }

    /**
     * Returns schema, domain and url and parameters
     * example: http://www.example.com/filename?a=1&b=2
     *
     * @param bool $parameters Include query string
     *
     * @return string
     */
    public function getUrl($parameters = true)
    {
        return $this->container->get('request')
                    ->getSchemeAndHttpHost() . ($parameters ? $_SERVER['REQUEST_URI'] : $_SERVER['SCRIPT_NAME']);
    }

    /**
     * Returns system environment string
     *
     * @return string
     */
    public static function getEnvironmentString()
    {
        return $GLOBALS['CONFIG']['SYSTEM_ENVIRONMENT'];
    }

    /**
     * Checks if the connection is secured
     *
     * @return bool
     */
    public function isSecure()
    {
        return $this->container->get('request')->isSecure();
    }

    /**
     * Return if the device is mobile (tablet || phone)
     *
     * @return bool
     */
    public function deviceIsMobile()
    {
        return $this->container->get('context')->isMobile();
    }

    /**
     * Sets the language
     * 
     * @deprecated
     *
     * @param string $lang
     *
     * @return self
     */
    public function setLanguageCode($lang)
    {
        $this->container->get('translator')->setLocale($lang);
        
        return $this;
    }

    /**
     * @deprecated
     * 
     * @return string
     */
    public function getLanguageCode()
    {
        return $this->container->get('translator')->getLocale();
    }

    /**
     * Translate a text
     * 
     * @deprecated
     *
     * @param string $text
     *
     * @return string
     */
    public function _($text)
    {
        return $this->static_text($text);
    }

    /**
     * Returns a static text depending on the language
     * Gets the text from the array static_text
     *
     * @deprecated
     * 
     * @param string $text_id
     *
     * @return string
     */
    public function static_text($text_id)
    {
        $translation = $this->container->get('translator');

        return $translation->trans($text_id, array(), null, 'de_DE');
    }

    /**
     * Returns a static text depending on the language
     * Gets the text from a file
     *
     * @deprecated
     *
     * @param string $id
     *
     * @return string
     */
    public function static_file($id)
    {
        $_filename = $GLOBALS['CONFIG']['SYSTEM_PATH_LOCALE'] . '/' . $this->getLanguageCode() . '/static_file/' . basename($id);
        if (is_file($_filename)) {
            return file_get_contents($_filename);
        }
        return '';
    }

    /**
     * Translates the url depending on the language
     *
     * @deprecated
     * 
     * @param string $url url
     * @param string $lang language code
     *
     * @return string
     */
    public function i18nTranslateUrl($url, $lang = '')
    {
        if (!$lang) {
            $lang = $this->getLanguageCode();
        }

        // remove the lang code if there is one
        $url = self::i18nRemoveLangCodeFromUrl($url);

        $parsed_url  = parse_url($url);
        $scheme = !empty($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : '';
        $host   = !empty($parsed_url['host'])   ? $parsed_url['host'] : '';
        $path   = !empty($parsed_url['path'])   ? $parsed_url['path'] : '';
        $query  = !empty($parsed_url['query']) ? ($parsed_url['query'] != '' ? '?' . $parsed_url['query'] : '') : '';
        
        // set the (new) language code
        if ($path == '/' && $GLOBALS['CONFIG']['CMS_LOCALE'][$lang]['url_prefix'] != '') {
            // the homepage doesn't need the trailing slash
            $path = $GLOBALS['CONFIG']['CMS_LOCALE'][$lang]['url_prefix'];
        } else {
            // everything but the homepage
            $path = $GLOBALS['CONFIG']['CMS_LOCALE'][$lang]['url_prefix'] . $path;
        }

        return $scheme . $host . $path . $query;
    }

    /**
     * Removes the language code (if there is any) from the url
     * 
     * @deprecated
     *
     * @param string $url
     *
     * @return string
     */
    public function i18nRemoveLangCodeFromUrl($url)
    {
        $parsed_url  = parse_url($url);
        $scheme = !empty($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : '';
        $host   = !empty($parsed_url['host'])   ? $parsed_url['host'] : '';
        $path   = !empty($parsed_url['path'])   ? $parsed_url['path'] : '';
        $query  = !empty($parsed_url['query']) ? ($parsed_url['query'] != '' ? '?' . $parsed_url['query'] : '') : '';
        
        $url_parts = explode('/', trim($path, '/'));
        $lang = array_shift($url_parts);
        if (in_array($lang, $GLOBALS['CONFIG']['CMS_LOCALE'])) {
            array_shift($url_parts);
            $path = '/' . implode('/', $url_parts);
        }

        return $scheme . $host . $path . $query;
    }

    /**
     * @deprecated
     * 
     * @param string $format
     *
     * @return mixed
     */
    public function now($format = 'datetime')
    {
        $time = time();
        
        switch ($format) {
            case 'datetime':
                return date('Y-m-d H:i:s', $time);
                break;
            case 'date':
                return date('Y-m-d', $time);
                break;
            case 'time':
                return date('H:i:s', $time);
                break;
            case 'timestamp':
            default:
                return $time;
                break;
        }
    }

    /**
     * @param string|array $message
     * 
     * @return self
     */
    public function addFlashMessage($message)
    {
        if ($message) {
            if (is_string($message)) {
                $this->addSessionMessage($message);
            } elseif (!empty($message['text'])) {
                $this->addSessionMessage($message['text'], !empty($message['status']) ? trim($message['status']) : 'NOTICE');
            }
        }
        
        return $this;
    }

    /**
     * Load a route and parse it as if it was the original request (i.e. redirect without redirecting)
     *
     * @param string $uri
     * @param array|string $message
     * 
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function forward($uri, $message = null)
    {
        $this->addFlashMessage($message);
        
        /** @var \Symfony\Component\Routing\Matcher\UrlMatcherInterface $matcher */
        $matcher = $this->container->get('router')->getMatcher();
        $path = $matcher->match($uri);
        
        // The GET and POST vars remain the same. We pass "path" in to allow the handler to work again
        $sub_request = $this->container->get('request_stack')->getCurrentRequest()->duplicate(null, null, $path);

        return $this->container->get('http_kernel')->handle($sub_request, HttpKernelInterface::SUB_REQUEST);
    }

    /**
     * @param string $url
     * @param array|string $message
     * 
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirect($url, $message = null)
    {
        $this->addFlashMessage($message);

        return $this->container->get('request')->redirect($url);
    }

    /**
     * Gets all flash messages (optionally by category)
     *
     * @param string $key
     *
     * @return mixed
     */
    public function getSessionMessages($key = '')
    {
        if ($key) {
            return $this->container->get('session')->getFlashBag()->get($key);
        }

        return $this->container->get('session')->getFlashBag()->all();
    }

    /**
     * Adds flash message
     *
     * @deprecated Use controller sugar or fetch from container
     * 
     * @param string $message
     * @param string $type
     *
     * @return void
     */
    public function addSessionMessage($message, $type = 'NOTICE')
    {
        $this->container->get('session')->getFlashBag()->add($type, $message);
    }

    /**
     * @param int $length
     *
     * @return string
     */
    public static function createUid($length = 6)
    {
        srand((double)(microtime() * 1000000));
        $valid_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
        $uid = '';

        while ($length > 0) {
            $uid .= $valid_chars[rand(0, strlen($valid_chars) - 1)];
            $length--;
        }

        return $uid;
    }

}
