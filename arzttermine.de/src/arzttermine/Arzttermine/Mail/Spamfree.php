<?php

namespace Arzttermine\Mail;

/**
 * Spamfree class
 *
 * original by http://www.polepositionmarketing.com/library/wp-spamfree/
 *
 * @author Axel Kuzmik <axel@kuzmik.de>
 *
 * **************************************************
 * 1. include the js-file in the template
 * --------------------------------------------------
 * {$this->addJsInclude('<script type="text/javascript" src="/media/js/gros-js.php"></script>')}
 * **************************************************
 * 2. create the file gros-js.php with this content:
 * --------------------------------------------------
 * <?php
 * include_once $_SERVER['DOCUMENT_ROOT']."/../include/include.php";
 * require_once $CONFIG["SYSTEM_CLASS_SPAMFREE"];
 * Spamfree::getJs();
 * ?>
 * **************************************************
 * 3. in the comment/contact form check if the user is a spammer/robot
 * --------------------------------------------------
 * if (Spamfree::isSpam()) {
 * echo 'SPAM!!!';
 * }
 * **************************************************
 */
$GLOBALS['CONFIG']['SYSTEM_SPAMFREE_COOKIE_NAME'] = "sforcookieid";
$GLOBALS['CONFIG']['SYSTEM_SPAMFREE_COOKIE_KEY'] = "dGbuSftwbrto";

class Spamfree {

    /**
     * Use this method to check if the user is a spammer
     *
     * @return bool
     */
    public static function isSpam()
    {
        return !(
            (!empty($_COOKIE[$GLOBALS['CONFIG']['SYSTEM_SPAMFREE_COOKIE_NAME']]) && !empty($GLOBALS['CONFIG']['SYSTEM_SPAMFREE_COOKIE_KEY'])) &&
            ($_COOKIE[$GLOBALS['CONFIG']['SYSTEM_SPAMFREE_COOKIE_NAME']] === $GLOBALS['CONFIG']['SYSTEM_SPAMFREE_COOKIE_KEY'])
        );
    }

    /**
     * Sets the cookie directly,
     * sends the headers
     * and also returns the javascript which also tries to set the cookie via javascript
     */
    function getJs()
    {

        @setcookie($GLOBALS['CONFIG']['SYSTEM_SPAMFREE_COOKIE_NAME'], $GLOBALS['CONFIG']['SYSTEM_SPAMFREE_COOKIE_KEY'], 0, '/');

        header('Cache-Control: no-cache');
        header('Pragma: no-cache');
        header('Content-Type: application/x-javascript');

        echo "
function SetCookie( name, value, expires, path, domain, secure ) {
	var today = new Date();
	today.setTime( today.getTime() );
	if ( expires ) {
		expires = expires * 1000 * 60 * 60 * 24;
	}
	var expires_date = new Date( today.getTime() + (expires) );
	document.cookie = name+'='+escape( value ) +
		( ( expires ) ? ';expires='+expires_date.toGMTString() : '' ) + //expires.toGMTString()
		( ( path ) ? ';path=' + path : '' ) +
		( ( domain ) ? ';domain=' + domain : '' ) +
		( ( secure ) ? ';secure' : '' );
}

function spamfreeValidation() {
	SetCookie('" . $GLOBALS['CONFIG']['SYSTEM_SPAMFREE_COOKIE_NAME'] . "','" . $GLOBALS['CONFIG']['SYSTEM_SPAMFREE_COOKIE_KEY'] . "','','/');
}

spamfreeValidation();

";
    }
}