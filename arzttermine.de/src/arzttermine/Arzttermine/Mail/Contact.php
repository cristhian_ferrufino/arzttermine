<?php

namespace Arzttermine\Mail;

use Arzttermine\Core\Basic;

class Contact extends Basic {

    /**
     * @var string
     */
    protected $tablename = DB_TABLENAME_CONTACTS;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     */
    protected $subject;

    /**
     * @var string
     */
    protected $message;

    /**
     * @var string
     */
    protected $comment;

    /**
     * @var string
     */
    protected $created_at;

    /**
     * @var string
     */
    protected $created_by;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var string
     */
    protected $lang;

    /**
     * @var string
     */
    protected $replied_at;

    /**
     * @var string
     */
    protected $replied_by;

    /**
     * @var array
     */
    protected $all_keys
        = array(
            "id",
            "lang",
            "name",
            "email",
            "subject",
            "message",
            "comment",
            "created_at",
            "created_by",
            "replied_at",
            "replied_by"
        );

    /**
     * @var array
     */
    protected $secure_keys
        = array(
            "id",
            "lang",
            "name",
            "email",
            "subject",
            "message",
            "comment",
            "created_at",
            "created_by",
            "replied_at",
            "replied_by"
        );

    /**
     * @return string
     */
    public function getAdminEditUrl()
    {
        return '/admin/contacts/index.php?action=edit&id=' . $this->getId();
    }

    /**
     * Renders the Contact for html output
     *
     * @deprecated
     *
     * @return string
     */
    public function getContactHtml()
    {
        return;
    }

    /**
     * Returns the subject
     *
     * @param int $excerpt_length
     *
     * @return string
     */
    public function getSubject($excerpt_length = 0)
    {
        if ($excerpt_length > 0) {
            return getExcerpt($this->subject, $excerpt_length);
        }

        return $this->subject;
    }

    /**
     * Returns the message
     *
     * @param int $excerpt_length
     *
     * @return string
     */
    public function getMessage($excerpt_length = 0)
    {
        if ($excerpt_length > 0) {
            return getExcerpt($this->message, $excerpt_length);
        }

        return $this->message;
    }

    /**
     * Returns the created_at date
     *
     * @param string $format
     *
     * @return string a href
     */
    public function getCreatedAt($format = 'D, d M Y H:i:s')
    {
        return mysql2date($format, $this->created_at, false);
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $comment
     *
     * @return self
     */
    public function setComment($comment)
    {
        $this->comment = trim($comment);

        return $this;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }
    
}