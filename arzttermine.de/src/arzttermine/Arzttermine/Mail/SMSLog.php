<?php

namespace Arzttermine\Mail;

use Arzttermine\Application\Application;
use Arzttermine\Core\Basic;

class SMSLog extends Basic {

    /**
     * @var string
     */
    public $tablename = DB_TABLENAME_SMS_LOG;

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $info;

    /**
     * @var string
     */
    public $sms_from;

    /**
     * @var string
     */
    public $sms_to;

    /**
     * @var string
     */
    public $message;

    /**
     * @var string
     */
    public $response;

    /**
     * @var string
     */
    public $created_at;

    /**
     * @var array
     */
    public $all_keys
        = array(
            "id",
            "info",
            "sms_from",
            "sms_to",
            "message",
            "response",
            "created_at",
        );

    /**
     * @var array
     */
    public $secure_keys
        = array(
            "info",
            "sms_from",
            "sms_to",
            "message",
            "response",
            "created_at",
        );

    /**
     * @param mixed $value
     * @param string $key
     */
    public function __construct($value = 0, $key = 'id')
    {
        $this->id = $this->load($value, $key);
    }

    /**
     * @param string $toPhoneNumber
     * @param string $from
     * @param string $message
     * @param string $response
     * @param string $info
     * @return bool|int
     */
    public static function add($toPhoneNumber, $from, $message, $response, $info = "")
    {
        $smsLog = new SMSLog();

        $_data = array();

        $_data['sms_to'] = $toPhoneNumber;
        $_data['sms_from'] = $from;
        $_data['message'] = $message;
        $_data['response'] = $response;
        $_data['info'] = $info;
        $_data['created_at'] = Application::getInstance()->now();

        return $smsLog->saveNew($_data);
    }
}