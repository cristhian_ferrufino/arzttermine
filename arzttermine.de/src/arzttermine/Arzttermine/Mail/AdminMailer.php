<?php

namespace Arzttermine\Mail;

class AdminMailer {

    /**
     * Send a system email to the administrator
     *
     * @param string|array $recipients
     * @param string $message
     * @param string $subject
     * @param string $source
     * @param int $line
     */
    public static function send($recipients, $message, $subject = '', $source = 'General error', $line = 0)
    {
        $mailer = new PHPMailer();
        $mailer->IsHTML(false);

        if (!$subject) {
            $subject = $message;
        }

        // Process recipients
        if (!is_array($recipients)) {
            $recipients = array($recipients);
        }

        foreach ($recipients as $recipient) {
            $mailer->addAddress($recipient);
        }

        $mailer->Body = trim($message) . "\n\n\nSYSTEM_ENVIRONMENT: {$GLOBALS['CONFIG']['SYSTEM_ENVIRONMENT']}\n\n_SERVER:\n\n" . print_r($_SERVER, true);
        $mailer->Subject = "Warning ({$source} [{$line}]): {$subject}";
        $mailer->From = $GLOBALS['CONFIG']["SYSTEM_MAIL_FROM_EMAIL"];
        $mailer->Sender = $GLOBALS['CONFIG']["SYSTEM_MAIL_FROM_EMAIL"];

        $mailer->Send();
    }
}