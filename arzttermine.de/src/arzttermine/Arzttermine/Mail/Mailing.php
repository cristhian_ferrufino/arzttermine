<?php

namespace Arzttermine\Mail;

use Arzttermine\Application\Application;
use Arzttermine\Cms\CmsContent;
use Arzttermine\Core\Basic;

class Mailing extends Basic {

    /**
     * @var string
     */
    public $tablename = DB_TABLENAME_MAILINGS;

    /**
     * @var int
     */
    public $status;

    /**
     * @var int
     */
    public $type = MAILING_TYPE_TEXT;

    /**
     * @var int
     */
    public $source_type_id = null;

    /**
     * @var int
     */
    public $source_id = null;

    /**
     * @var string
     */
    public $subject = '';

    /**
     * @var int
     */
    public $content_id = null; // content_id or content_filename must be filled

    /**
     * @var string
     */
    public $content_filename = ''; // to find the content where the mailtext is

    /**
     * @var string
     */
    public $body_text = '';

    /**
     * @var string
     */
    public $body_html = '';

    /**
     * @var string
     */
    public $charset = 'utf-8';

    /**
     * @var array
     */
    public $data = array();

    /**
     * @var array
     */
    public $mailto = array();

    /**
     * @var array
     */
    public $mailcc = array();

    /**
     * @var array
     */
    public $mailbcc = array();

    /**
     * @var array
     */
    public $replyTo = array();

    /**
     * @var string
     */
    public $from = '';

    /**
     * @var string
     */
    public $from_name = '';

    /**
     * @var bool
     */
    public $use_cc = false;

    /**
     * @var bool
     */
    public $use_bcc = false;

    /**
     * @var string
     */
    public $created_at;

    /**
     * @var string
     */
    public $created_by;

    /**
     * @var array
     */
    public $all_keys
        = array(
            "id",
            "status",
            "type",
            "source_type_id",
            "source_id",
            "mailto",
            "mailcc",
            "mailbcc",
            "subject",
            "content_id",
            "content_filename",
            "body_text",
            "body_html",
            "created_at",
            "created_by"
        );

    /**
     * @var array
     */
    public $secure_keys
        = array(
            "id",
            "status",
            "type",
            "source_type_id",
            "source_id",
            "mailto",
            "mailcc",
            "mailbcc",
            "subject",
            "content_id",
            "content_filename",
            "body_text",
            "body_html",
            "created_at",
            "created_by"
        );

    /**
     * @var PHPMailer
     */
    private $mailer = null;

    /**
     * @param mixed $value
     * @param string $key
     */
    public function __construct($value = 0, $key = 'id')
    {
        $this->id = $this->load($value, $key);

        $this->use_cc = $GLOBALS['CONFIG']['MAILING_SEND_CC'];
        $this->use_bcc = $GLOBALS['CONFIG']['MAILING_SEND_BCC'];

        $this->mailer = new PHPMailer();
        if($GLOBALS['CONFIG']['SYSTEM_MAIL_IS_SMTP'] === true) {
            $this->mailer->Mailer = 'smtp';
            $this->mailer->Host = $GLOBALS['CONFIG']['SYSTEM_MAIL_HOST'];
            $this->mailer->Port = ($GLOBALS['CONFIG']['SYSTEM_MAIL_SMTP_SECURE'] != '')?587:25;
            $this->mailer->SMTPAutoTLS = false;
            $this->mailer->SMTPSecure = $GLOBALS['CONFIG']['SYSTEM_MAIL_SMTP_SECURE'];
            $this->mailer->SMTPAuth = $GLOBALS['CONFIG']['SYSTEM_MAIL_SMTP_AUTH'];
            $this->mailer->Username = $GLOBALS['CONFIG']['SYSTEM_MAIL_SMTP_USER'];
            $this->mailer->Password = $GLOBALS['CONFIG']['SYSTEM_MAIL_SMTP_PASS'];
        }
    }

    /**
     * Adds a mail receiver
     *
     * @param string $mode : (mailto, mailcc, mailbcc)
     * @param string $email
     * @param string $name
     *
     * @return bool
     */
    public function addAddress($mode, $email, $name = '')
    {
        $_email = sanitize_email($email);

        if (!is_email($_email)) {
            return false;
        }

        // we need the double quotes for the serialized email name syntax
        $_name = str_replace('"', "'", sanitize_title($name));
        $this->{$mode}[] = array(
            'email' => $_email,
            'name'  => $_name
        );

        return true;
    }

    /**
     * Add an address to one of the recipient fields
     *
     * @param string $email
     * @param string $name
     *
     * @return bool
     */
    public function addAddressTo($email, $name = '')
    {
        return $this->addAddress('mailto', $email, $name);
    }

    /**
     * Add CC address if enabled
     *
     * @param string $email
     * @param string $name
     *
     * @return bool
     */
    public function addAddressCc($email, $name = '')
    {
        if (!$this->use_cc) {
            return true;
        }

        return $this->addAddress('mailcc', $email, $name);
    }

    /**
     * Add BCC address if enabled
     *
     * @param string $email
     * @param string $name
     *
     * @return bool
     */
    public function addAddressBcc($email, $name = '')
    {
        if (!$this->use_bcc) {
            return true;
        }

        return $this->addAddress('mailbcc', $email, $name);
    }

    /**
     * Add a reply to address
     *
     * @param string $email
     * @param string $name
     *
     * @return void
     */
    public function setReplyTo($email, $name = '')
    {
        $this->replyTo['email'] = $email;
        $this->replyTo['name'] = $name;
    }

    /**
     * Returns the emails in email syntax divided with commas
     *
     * @param $mode : (mailto, mailcc, mailbcc)
     *
     * @return string
     */
    public function getReceiverString($mode)
    {
        $recipients = array();

        foreach ($this->{$mode} as $to) {
            if ($to['name'] != '') {
                $recipients[] = '"' . $to['name'] . '" <' . $to['email'] . '>';
            } else {
                $recipients[] = $to['email'];
            }
        }

        return implode(',', $recipients);
    }

    /**
     * Replace content with the data array
     *
     * @static
     *
     * @param string $content
     * @param array $data
     *
     * @return string
     */
    public static function arrayReplace($content, $data)
    {
        if (is_array($data) && !empty($data)) {
            foreach ($data as $key => $value) {
                if (is_string($value)) {
                    $content = str_replace('{' . $key . '}', $value, $content);
                }
            }
        }

        return $content;
    }

    /**
     * Assign to the mailer all the recipients from the recipient lists
     *
     * @return void
     */
    protected function assignRecipients()
    {
        // Standard addresses
        foreach ((array)$this->mailto as $to) {
            $this->mailer->AddAddress($to['email'], $to['name']);
        }

        // CC addresses
        foreach ((array)$this->mailcc as $to) {
            $this->mailer->AddCC($to['email'], $to['name']);
        }

        // BCC addresses
        foreach ((array)$this->mailbcc as $to) {
            $this->mailer->AddBCC($to['email'], $to['name']);
        }
        if ($this->use_bcc) {
            // send every email also to this address for documentation (unless CONFIG.MAILING_SEND_BCC != true)
            $this->mailer->AddBCC($GLOBALS['CONFIG']['SYSTEM_MAIL_BCC_FROM_EMAIL'], $GLOBALS['CONFIG']['SYSTEM_MAIL_BCC_FROM_NAME']);
        }

        // Reply to address
        if (!empty($this->replyTo)) {
            $this->mailer->AddReplyTo($this->replyTo['email'], $this->replyTo['name']);
        }
    }

    /**
     * Take the mail body from the CMS and send the mail amd optionally store the mail in the mailing db
     *
     * @param bool $store_in_db
     *
     * @return bool
     */
    public function send($store_in_db = true)
    {
        $body = null;

        if (!empty($this->content_id) && is_numeric($this->content_id)) {
            $source = $this->content_id;
        } else {
            $source = $this->content_filename;
        }
        // test if there is a subject set already. If so then dont overwrite that one
        $body = $this->getContent($source, !$this->hasSubject());

        // return if content_id and content_filename are both not valid
        if (empty($body)) {
            return false;
        }

        $this->mailer->CharSet = $this->charset;
        $this->mailer->IsHTML($this->type != MAILING_TYPE_TEXT);

        $this->body_text = $this->arrayReplace($body, $this->data);
        $this->mailer->Body = $this->body_text;

        $this->subject = $this->arrayReplace($this->getSubject(), $this->data);
        $this->mailer->Subject = $this->subject;

        if ($this->from != '') {
            $from_name = ($this->from_name != '' ? $this->from_name : $this->from);
            $this->mailer->SetFrom($this->from, $from_name, false);
        }

        $this->assignRecipients();

        $result = $this->mailer->Send();

        // now store the message in the db *******************************
        if ($store_in_db) {
            $this->saveToDatabase($result ? MAILING_STATUS_SEND : MAILING_STATUS_ERROR);
        }

        return $result;
    }

    /**
     * Send the mail body and optionally store the mail in the mailing db
     *
     * @param bool $store_in_db
     *
     * @return bool
     */
    public function sendPlain($store_in_db = true)
    {
        $this->body_text = $this->arrayReplace($this->body_text, $this->data);
        $this->mailer->Body = $this->body_text;

        $this->subject = $this->arrayReplace($this->subject, $this->data);
        $this->mailer->Subject = $this->subject;

        if ($this->from != '') {
            $from_name = ($this->from_name != '' ? $this->from_name : $this->from);
            $this->mailer->SetFrom($this->from, $from_name, false);
        }

        $this->assignRecipients();

        $result = $this->mailer->send();

        // now store the message in the db *******************************
        if ($store_in_db) {
            $this->saveToDatabase($result ? MAILING_STATUS_SEND : MAILING_STATUS_ERROR);
        }

        return $result;
    }

    /**
     * Log the email to the database
     *
     * @param int $status
     *
     * @return bool
     */
    protected function saveToDatabase($status = MAILING_STATUS_ERROR)
    {
        $profile = Application::getInstance()->getCurrentUser();

        $_data = array();

        $_data['status'] = $status;
        $_data['subject'] = $this->subject;
        $_data['mailto'] = $this->getReceiverString('mailto');
        $_data['mailcc'] = $this->getReceiverString('mailcc');
        $_data['mailbcc'] = $this->getReceiverString('mailbcc');
        $_data['content_id'] = $this->content_id;
        $_data['content_filename'] = $this->content_filename;
        $_data['type'] = $this->type;
        $_data['source_type_id'] = $this->source_type_id;
        $_data['source_id'] = $this->source_id;
        $_data['body_text'] = $this->body_text;
        $_data['body_html'] = $this->body_html;
        $_data['created_at'] = Application::getInstance()->now();
        $_data['created_by'] = $profile->getId();

        return $this->saveNew($_data);
    }

    /**
     * Set the source_type_id
     *
     * @param source_type_id
     *
     * @return bool
     */
    public function setSourceTypeId($source_type_id)
    {
        $this->source_type_id = $source_type_id;

        return true;
    }

    /**
     * Set the source_id
     *
     * @param source_id
     *
     * @return bool
     */
    public function setSourceId($source_id)
    {
        $this->source_id = $source_id;

        return true;
    }

    /**
     * Get the subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set the source_id
     *
     * @param source_id
     *
     * @return self
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * Check if there is set a subject
     *
     * @return bool
     */
    public function hasSubject()
    {
        return empty($this->subject) ? false : true;
    }


    /**
     * Get the mail content either from the templates or from the CMS (in this order)
     *
     * @param string $filename
     * @param bool $setSubject
     *
     * @return string|false
     */
    public function getContent($filename, $setSubject = true)
    {
        $app = Application::getInstance();
        $view = $app->getView();

        $content = false;

        // if it is a widget then add the prefix
        if ($app->isWidget()) {
            $filename = $app->getWidgetContainer()->getWidgetTemplatePattern() . $filename;
        }

        // first try to load it from as a template as this should be faster
        // remove the leading slash for smarty
        $templateFilename = preg_replace('#^/#', '', $filename);
        $templateFilenameTpl = $templateFilename.'.tpl';
        if ($view->templateExists($templateFilenameTpl)) {
            // set the data for smarty
            foreach ($this->data as $key => $value) {
                $view->setRef($key, $value);
            }
            $content = $view->fetch($templateFilenameTpl);

            if ($setSubject) {
                // build the subject filename
                $pathInfo = pathinfo($templateFilename);
                $templateFilenameTpl = $pathInfo['dirname'].'/'.$pathInfo['filename'].'.subject.'.$pathInfo['extension'].'.tpl';
                if ($view->templateExists($templateFilenameTpl)) {
                    $this->setSubject($view->fetch($templateFilenameTpl));
                } else {
                    return false;
                }
            }
        } else {
            // now try to find it in the CMS
            $cmsContent = CmsContent::getCmsContent($filename);
            if ($cmsContent->isValid()) {
                $content = $cmsContent->getContent();
            }
            if ($setSubject) {
                $this->setSubject($cmsContent->getTitle());
            }
        }

        return $content;
    }

}