<?php

namespace Arzttermine\Mail;

use Arzttermine\Booking\Booking;
use Arzttermine\Core\Configuration;
use Twilio\Exceptions\RestException;
use Twilio\Rest\Client;

class SMS {

    /**
     * @param string $toPhoneNumber Germany phone number (ex. 015712345678)
     * @param string $message
     * @param string $info this is informational field only saved into table (not used to send the sms).
     *
     * @return string API result.
     */
    public static function sendSmsToGermany($toPhoneNumber, $message = '', $info = '')
    {
        if (empty($message)) {
            return false;
        }

        $from = Configuration::get('sms_from_title');

        $massenversand_id = Configuration::get('massenversand_id');
        $massenversand_password = Configuration::get('massenversand_password');
        $massenversand_base_url = Configuration::get('massenversand_base_url');

        if (empty($massenversand_id) || empty($massenversand_password) || empty($massenversand_base_url)) {
            $ret = "Error: CMS config missing for API! (" . $massenversand_id . ", " . $massenversand_password . ", " . $massenversand_base_url . ")";
            SMSLog::add($toPhoneNumber, $from, $message, $ret, $info);

            return $ret;
        }

        $message = self::convertGermanSymbolsToEnglish($message);

        // 25.07.2014: DON'T return, but let Massenversand send a MMS instead
        /*
        if (strlen($message) > 160) {
            $ret = "Error: message length exceeds 160 non-unicode chars!";
            SMSLog::add($toPhoneNumber, $from, $message, $ret, $info);

            return $ret;
        }
        */

        $from = self::convertGermanSymbolsToEnglish($from);

        if ($vpn = self::getValidGermanNumber($toPhoneNumber)) {
            if ($GLOBALS['CONFIG']['SMS_ENABLE'] === true) {

                $sid = 'ACe42644041b391839dfb173b498acae09';
                $token = 'd8fd50f1c751b9fdd40dde5f692b7c3b';
                $client = new Client($sid, $token);

                //lookup valid phoneNumber format
                try {
                    $number = $client->lookups
                        ->phoneNumbers($vpn)
                        ->fetch(
                            array("type" => "carrier","CountryCode" => "DE")
                        );
                } catch(RestException $e) {
                    $ret = "Error: wrong number format!";
                    SMSLog::add($toPhoneNumber, $from, $message, $ret, $info);

                    return $ret;
                }

                $vpn = $number->phoneNumber;
                $ret = $client->messages->create(
                // the number you'd like to send the message to
                    $vpn,
                    array(
                        // A Twilio phone number you purchased at twilio.com/console
                        'from' => 'Arzttermine',
                        // the body of the text message you'd like to send
                        'body' => $message
                    )
                );

/*
                $params = array(
                    'receiver' => $vpn,
                    'sender'   => $from,
                    'msg'      => $message,
                    'id'       => $massenversand_id,
                    'pw'       => $massenversand_password,
                    'time'     => 0,
                    'msgtype'  => strlen($message) > 160 ? 'c' : 't',
                    'getID'    => 1,
                    'countMsg' => 1
                );

                $ret = file_get_contents($massenversand_base_url . '?' . http_build_query($params));
*/

                SMSLog::add($toPhoneNumber, $from, $message, (string)$ret, $info);

                return $ret;
            } else {
                $ret = "Warning: SMS is not enabled for this enviorment!";
                SMSLog::add($toPhoneNumber, $from, $message, $ret, $info);

                return $ret;
            }
        } else {
            $ret = "Error: wrong number format!";
            SMSLog::add($toPhoneNumber, $from, $message, $ret, $info);

            return $ret;
        }
    }

    /**
     * @param string $number
     *
     * @return string valid phone number to send sms to. Null otherwise
     */
    public static function getValidGermanNumber($number)
    {
        if (!isset($number)) {
            return null;
        }
        $number = strval($number);
        $number = preg_replace("/[^0-9]/", "", $number);
        $number = ltrim($number, "0");
        if (!self::startsWith($number, strval(GERMANY_COUNTRY_TEL_CODE))) {
            $number = GERMANY_COUNTRY_TEL_CODE . $number;
        }
        if (self::startsWith($number, strval(GERMANY_COUNTRY_TEL_CODE) . '0')) {
            $number = strval(GERMANY_COUNTRY_TEL_CODE) . substr($number, 3);
        }
        if (!self::startsWith($number, strval(GERMANY_COUNTRY_TEL_CODE) . strval(GERMANY_CELL_PHONE_NUMBERS_START_WITH))) {
            return null;
        }

        return '00' . $number;
    }

    public static function convertGermanSymbolsToEnglish($text)
    {
        $text = str_replace("\xc3\xa4", 'ae', $text);
        $text = str_replace("\xc3\x84", 'AE', $text);
        $text = str_replace("\xc3\xb6", 'oe', $text);
        $text = str_replace("\xc3\x96", 'Oe', $text);
        $text = str_replace("\xc3\xbc", 'ue', $text);
        $text = str_replace("\xc3\x9c", 'Ue', $text);
        $text = str_replace("\xc3\x9f", 'ss', $text);

        return $text;
    }

    private static function startsWith($haystack, $needle)
    {
        return strpos($haystack, $needle) === 0;
    }

    /**
     * @param Booking $booking
     * @return bool
     */
    public static function sendBookingConfirmationSms($booking)
    {
        if (!isset($booking) || !$booking->isValid()) {
            return false;
        }
        $date = date('d.m.Y', strtotime($booking->getAppointmentStartAt()));
        $time = date('H:i', strtotime($booking->getAppointmentStartAt()));

        $user = $booking->getUser();
        $location = $booking->getLocation();
        if (!$user->isValid() || !$location->isValid()) {
            return false;
        }
        $title = $user->getTitle();
        $lastName = $user->getLastName();
        $street = $location->getStreet();
        $zip = $location->getZip();
        $city = $location->getCity();
        $message = "Terminbestätigung von Arzttermine.de: " . $title . " " . $lastName . " am " . $date . " um " . $time . " Uhr.\nAdresse: " . $street . ", " . $zip . " " . $city;
        $message = self::convertGermanSymbolsToEnglish($message);
        if (strlen($message) > 160) {
            $message = "Terminbestätigung von Arzttermine.de: " . $title . " " . $lastName . " am " . $date . " um " . $time . " Uhr.\nAdresse: " . $street . ", " . $city;
            $message = self::convertGermanSymbolsToEnglish($message);
        }
        if (strlen($message) > 160) {
            $message = "Terminbestätigung von Arzttermine.de: " . $title . " " . $lastName . " am " . $date . " um " . $time . " Uhr.\nAdresse: " . $street;
            $message = self::convertGermanSymbolsToEnglish($message);
        }
        if (strlen($message) > 160) {
            $message = "Terminbestätigung von Arzttermine.de: " . $lastName . " am " . $date . " um " . $time . " Uhr.\nAdresse: " . $street;
            $message = self::convertGermanSymbolsToEnglish($message);
        }
        if (strlen($message) > 160) {
            $message = substr("Terminbestätigung von Arzttermine.de: " . $lastName . " am " . $date . " um " . $time . " Uhr.\nAdresse: " . $street, 0, 160);
            $message = self::convertGermanSymbolsToEnglish($message);
        }
        self::sendSmsToGermany($booking->getPhone(), $message, "Booking ID: " . $booking->getId());
        
        return true;
    }
}
