<?php

namespace Arzttermine\Location;

use Arzttermine\Application\Application;
use Arzttermine\Core\Basic;
use Arzttermine\Maps\GoogleMap;

class District extends Basic {

    /**
     * @var string
     */
    protected $tablename = DB_TABLENAME_DISTRICTS;

    /**
     * @var int
     */
    protected $id;

    /**
     * Level depends on the city size.
     * In a major city like Berlin level 1 is a Bezirk, level 2 is a Ortsteil
     *
     * @var int
     */
    protected $level;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $city;

    /**
     * @var float
     */
    protected $lat;

    /**
     * @var float
     */
    protected $lng;

    /**
     * @var string
     */
    protected $created_at;

    /**
     * @var string
     */
    protected $created_by;

    /**
     * @var string
     */
    protected $updated_at;

    /**
     * @var string
     */
    protected $updated_by;

    /**
     * @var array
     */
    protected $all_keys
        = array(
            "id",
            "level",
            "name",
            "city",
            "lat",
            "lng",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by"
        );

    /**
     * @var array
     */
    protected $secure_keys
        = array(
            "id",
            "level",
            "name",
            "city",
            "lat",
            "lng",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by"
        );

    /**
     * Returns the level
     *
     * @return int level
     */
    public function getLevel()
    {
        return intval($this->level);
    }

    /**
     * Returns the name
     *
     * @return string
     */
    public function getName()
    {
        return trim($this->name);
    }

    /**
     * Returns the city
     *
     * @return string
     */
    public function getCity()
    {
        return trim($this->city);
    }

    /**
     * Returns the lat
     *
     * @return float lat
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Returns the lng
     *
     * @return float lng
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * @param float $lat
     *
     * @return self
     */
    public function setLat($lat)
    {
        $this->lat = floatval($lat);

        return $this;
    }

    /**
     * @param float $lng
     *
     * @return self
     */
    public function setLng($lng)
    {
        $this->lng = floatval($lng);

        return $this;
    }

    /**
     * Checks if the id exists
     *
     * @param int $id
     *
     * @return bool
     */
    public static function idExists($id)
    {
        $district = new District($id, 'id');

        return $district->isValid();
    }

    /**
     * Updates the lat and lng values
     *
     * @return array|bool
     */
    public function updateGeocoordinates()
    {
        $address = "{$this->getCity()},{$this->getName()}";
        $coords = GoogleMap::getGeoCoords($address);

        if (!empty($coords)) {
            $this->setLat($coords['lat']);
            $this->setLng($coords['lng']);
        } else {
            return false;
        }

        return $this->save(array('lat', 'lng'));
    }

    /**
     * @return array
     */
    public function getGeoCoords()
    {
        return array('lat' => $this->getLat(), 'lng' => $this->getLng());
    }

    /**
     * Returns the name
     *
     * @param int $id
     * @param string $default
     *
     * @return string
     */
    public static function getNameById($id, $default = '')
    {
        $district = new District($id);

        if ($district->isValid()) {
            return $district->getName();
        }

        return $default;
    }

    /**
     * Returns all districts matching a city
     *
     * @param string $city
     *
     * @return array District
     */
    public static function getCityDistricts($city)
    {
        $district = new self();

        return $district->findObjects('city="' . sanitize_location($city) . '" ORDER BY name ASC');
    }

    /**
     * Returns all districts
     *
     * @param bool $addZero adds an empty option
     * @param string $text Default text for the empty option
     *
     * @return array [id, text]
     */
    public static function getHtmlOptionsWithCity($addZero = false, $text = '')
    {
        $options = array();
        if ($addZero) {
            $options[0] = $text ? $text : Application::getInstance()->_('...bitte wählen...');
        }

        $district = new District();
        /** @var District[] $districts */
        $districts = $district->findObjects('1=1 ORDER BY city ASC, name ASC');

        if (is_array($districts) && !empty($districts)) {
            foreach ($districts as $district) {
                $options[$district->getId()] = $district->getCity() . ' / ' . $district->getName();
            }
        }

        return $options;
    }
}
