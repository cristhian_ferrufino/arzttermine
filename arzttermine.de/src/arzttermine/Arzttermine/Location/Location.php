<?php

namespace Arzttermine\Location;

use Arzttermine\Application\Application;
use Arzttermine\Insurance\Insurance;
use Arzttermine\Core\Basic;
use Arzttermine\Integration\Integration\Terminblock\Integration;
use Arzttermine\Media\Asset;
use Arzttermine\MedicalSpecialty\MedicalSpecialty;
use Arzttermine\Review\Review;
use Arzttermine\User\User;
use Arzttermine\Core\Configuration;
use NGS\Managers\UsersAaManager;
use Stash\Driver\FileSystem;
use Stash\Pool;

class Location extends Basic {

    const STATUS_DRAFT = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_VISIBLE = 2;
    const STATUS_VISIBLE_APPOINTMENTS = 3;

    /**
     * @var string
     */
    protected $tablename = DB_TABLENAME_LOCATIONS;

    /**
     * @var string
     */
    protected $slug = '';

    /**
     * @var int
     */
    protected $status;

    /**
     * @var int
     */
    protected $integration_id;

    /**
     * @var Integration
     */
    protected $integration;

    /**
     * @var string
     */
    protected $name = '';

    /**
     * @var string
     */
    protected $preferred_medical_specialty_text = '';

    /**
     * @var string
     */
    protected $street = '';

    /**
     * @var string
     */
    protected $city = '';

    /**
     * @var string
     */
    protected $zip = '';

    /**
     * @var string
     */
    protected $country_code = ''; // ISO 3166

    /**
     * @var float
     */
    protected $lat;

    /**
     * @var float
     */
    protected $lng;

    /**
     * @var string
     */
    protected $email = '';

    /**
     * @var string
     */
    protected $www = '';

    /**
     * @var string
     */
    protected $phone = '';

    /**
     * @var bool
     */
    protected $phone_visible;

    /**
     * @var string
     */
    protected $fax = '';

    /**
     * @var bool
     */
    protected $fax_visible;

    /**
     * @var bool
     */
    protected $newsletter_subscription;

    /**
     * Separated by commas
     *
     * @var string
     */
    protected $member_ids = '';

    /**
     * @var int
     */
    protected $primary_member_id;

    /**
     * ... also separated by commas
     *
     * @var string
     */
    protected $medical_specialty_ids = '';

    /**
     * @var string
     */
    protected $neighborhood_ids = '';

    /**
     * @var int
     */
    protected $position_factor;

    /**
     * @var float
     */
    protected $rating_average;

    /**
     * @var float
     */
    protected $rating_1;

    /**
     * @var float
     */
    protected $rating_2;

    /**
     * @var float
     */
    protected $rating_3;

    /**
     * @var string
     */
    protected $info_1 = '';

    /**
     * @var string
     */
    protected $info_location = '';

    /**
     * @var string
     */
    protected $info_seo = '';

    /**
     * @var string
     */
    protected $html_title = '';

    /**
     * @var string
     */
    protected $html_meta_description = '';

    /**
     * @var string
     */
    protected $html_meta_keywords = '';

    /**
     * @var string
     */
    protected $comment_intern = '';

    /**
     * @var string
     */
    protected $data_source = '';

    /**
     * @var int
     */
    protected $profile_asset_id;

    /**
     * @var string
     */
    protected $salesforce_id = '';

    /**
     * @var string
     */
    protected $google_place_id = '';
    
    /**
     * @var string
     */
    protected $profile_asset_filename = '';

    /**
     * @var string
     */
    protected $created_at = '';

    /**
     * @var string
     */
    protected $created_by = '';

    /**
     * @var string
     */
    protected $updated_at = '';

    /**
     * @var string
     */
    protected $updated_by = '';

    /**
     * @var string
     */
    protected $url = '';

    /**
     * @var Asset
     */
    protected $profile_asset;

    /**
     * @var Asset[]
     */
    protected $assets;

    /**
     * @var bool
     */
    protected $pkv_only;

    /**
     * @var array
     */
    protected $all_keys
        = array(
            "id",
            "slug",
            "status",
            "integration_id",
            "name",
            "preferred_medical_specialty_text",
            "street",
            "city",
            "zip",
            "country_code",
            "neighborhood_ids",
            "lat",
            "lng",
            "email",
            "www",
            "phone",
            "phone_visible",
            "fax",
            "fax_visible",
            "newsletter_subscription",
            "member_ids",
            "medical_specialty_ids",
            "primary_member_id",
            "position_factor",
            "rating_average",
            "rating_1",
            "rating_2",
            "rating_3",
            "info_1",
            "info_location",
            "info_seo",
            "html_title",
            "html_meta_description",
            "html_meta_keywords",
            "comment_intern",
            "data_source",
            "profile_asset_id",
            "salesforce_id",
            "google_place_id",
            "profile_asset_filename",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by",
            "pkv_only"
        );

    /**
     * @var array
     */
    protected $secure_keys
        = array(
            "id",
            "slug",
            "status",
            "integration_id",
            "name",
            "preferred_medical_specialty_text",
            "street",
            "city",
            "zip",
            "country_code",
            "neighborhood_ids",
            "lat",
            "lng",
            "email",
            "www",
            "phone",
            "phone_visible",
            "fax",
            "fax_visible",
            "newsletter_subscription",
            "member_ids",
            "medical_specialty_ids",
            "primary_member_id",
            "position_factor",
            "rating_average",
            "rating_1",
            "rating_2",
            "rating_3",
            "info_1",
            "info_location",
            "info_seo",
            "html_title",
            "html_meta_description",
            "html_meta_keywords",
            "comment_intern",
            "data_source",
            "profile_asset_id",
            "salesforce_id",
            "profile_asset_filename",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by",
            "pkv_only"
        );

    /**
     *
     * @var User[]
     */
    private $members;

    /**
     * Returns the users array
     *
     * @return array user objects
     */
    public function getMembers()
    {
        if (!is_array($this->members)) {
            $this->loadMembers();
        }

        return $this->members;
    }

    /**
     * Returns the users array with all members nevertheless which status
     *
     * @return array user objects
     */
    public function getAllMembers()
    {
        $_users = array();
        $_ids = explode(',', $this->member_ids);
        if (is_array($_ids) && !empty($_ids)) {
            foreach ($_ids as $_id) {
                $_user = new User($_id);
                if ($_user->isValid()) {
                    $_users[] = $_user;
                }
            }
        }
        return $_users;
    }

    /**
     * Returns the users ids array
     *
     * @param
     *
     * @return array user ids
     */
    public function getMemberIds()
    {
        $members = $this->getMembers();
        $ids = array();
        foreach ($members as $member) {
            $ids[] = $member->id;
        }

        return $ids;
    }

    /**
     * @param string $member_ids
     * @return self
     */
    public function setMemberIds($member_ids)
    {
        $this->member_ids = $member_ids;

        return $this;
    }

    /**
     * @param int $primary_member_id
     * @return self
     */
    public function setPrimaryMemberId($primary_member_id)
    {
        $this->primary_member_id = $primary_member_id;

        return $this;
    }

    /**
     * Loads the members (users)
     *
     * @return self
     */
    public function loadMembers()
    {
        $_users = array();
        $_ids = explode(',', $this->member_ids);
        if (is_array($_ids) && !empty($_ids)) {
            foreach ($_ids as $_id) {
                $_user = new User($_id);
                if ($_user->isValid() && $_user->isProfileVisible()) {
                    $_users[] = $_user;
                }
            }
        }
        
        $this->members = $_users;
        
        return $this;
    }

    /**
     * Returns the name. It will of course be empty if the object is invalid, but that's fine
     *
     * @return string
     */
    public function getName()
    {
        return trim($this->name);
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = trim($name);

        return $this;
    }

    /**
     * Returns the zip
     *
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Returns the city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Returns the street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     *
     * @return self
     */
    public function setStreet($street)
    {
        $this->street = trim($street);

        return $this;
    }

    /**
     * @param string $zip
     *
     * @return self
     */
    public function setZip($zip)
    {
        $this->zip = trim($zip);

        return $this;
    }

    /**
     * @param string $city
     *
     * @return self
     */
    public function setCity($city)
    {
        $this->city = trim($city);

        return $this;
    }

    /**
     * Returns the phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $www
     *
     * @return self
     */
    public function setWww($www)
    {
        $this->www = $www;

        return $this;
    }

    /**
     * @param string $phone
     *
     * @return self
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Returns the visible phone number
     *
     * @return string
     */
    public function getPhoneVisible()
    {
        return $this->phone_visible;
    }

    /**
     * Checks if there is a visible phone number
     *
     * @return bool
     */
    public function hasPhoneVisible()
    {
        return ($this->phone_visible == '') ? false : true;
    }

    /**
     * Returns the integration_id
     *
     * @return int
     */
    public function getIntegrationId()
    {
        return $this->integration_id;
    }

    /**
     * @param int $integration_id
     * 
     * @return self
     */
    public function setIntegrationId($integration_id)
    {
        $this->integration_id = intval($integration_id);
        
        return $this;
    }

    /**
     * @return Integration
     */
    public function getIntegration()
    {
        if (!$this->integration) {
            $this->integration = Integration::getIntegration($this->getIntegrationId());
        }

        return $this->integration;
    }

    /**
     * Returns the salesforce_id
     *
     * @return string
     */
    public function getSalesforceId()
    {
        return $this->salesforce_id;
    }

    /**
     * Returns the google_place_id
     *
     * @return string
     */
    public function getGooglePlaceId()
    {
        if($this->google_place_id != '') {
            return $this->google_place_id;
        }

        $client = new \Google_Client();
        $client->setDeveloperKey("AIzaSyA-cGD6Wm_86W_BzBctDnMMD5j7iM0is1I");
        $cache = new Pool(new FileSystem());
        $client->setCache($cache);
        $http = $client->authorize();

        // make the call!
        $url = 'https://maps.googleapis.com/maps/api/place/radarsearch/json?location='.$this->lat.','.$this->lng.'&radius=50&keyword='.$this->name;
        $result = $http->get($url);
        $result = json_decode($result->getBody());
        if($result->status == 'OK') {
            if(count($result->results) > 1) {
                $url = 'https://maps.googleapis.com/maps/api/place/textsearch/json?query='.$this->name;
                $response = $http->get($url);
                $response = json_decode($response->getBody());
                if($response->status == 'OK') {
                    $this->google_place_id = $response->results[0]->place_id;
                }
            } else {
                $this->google_place_id = $result->results[0]->place_id;
            }

            $this->save(array('google_place_id'));
        }

        return $this->google_place_id;
    }

    /**
     * Returns a HTML formatted link to submit a google review
     *
     * @return string
     */
    public function getGooglePlaceReviewLink($linktext='google_review', $targetBlank=true, $urlIdentifier='', $infolink=false)
    {
        $url = $this->getPlainGooglePlaceReviewLink($urlIdentifier, $infolink);

        return '<a href="' . $url . '"' . (($targetBlank===true) ? ' target="_blank"' : '') . '>' . $linktext . '</a>';
    }

    /**
     * Returns the plain url string to google to submit a review
     *
     * @param string $urlIdentifier
     * @param bool $infolink
     *
     * @return string
     */
    public function getPlainGooglePlaceReviewLink($urlIdentifier = '', $infolink = false)
    {
        $client = new \Google_Client();
        $client->setDeveloperKey("AIzaSyA-cGD6Wm_86W_BzBctDnMMD5j7iM0is1I");

        $shortener = new \Google_Service_Urlshortener($client);
        $url = new \Google_Service_Urlshortener_Url();
        $url->longUrl = 'http://search.google.com/local/writereview?placeid=' . $this->getGooglePlaceId();
        if($urlIdentifier !='') {
          $url->longUrl .= '&unique='.$urlIdentifier;
        }
        $short = $shortener->url->insert($url);

        return $short->getId() . (($infolink===true) ? '.info' : '');
    }

    /**
     * @param bool $absolute
     *
     * @return string
     */
    public function getUrl($absolute = false)
    {
        return ($absolute ? $GLOBALS['CONFIG']['URL_HTTP_LIVE'] : '') . $this->url;
    }

    /**
     * Returns the street without number.
     * Gets the street by a regex. Could fail.
     * Use this together with getStreetNumber() and getStreetExtension()
     *
     * Use this only if you really need to (e.g. for a partner like gelbe seiten export)!
     * There are examples where this should fail like:
     * - Straße am 17. Juni
     * - Hauptstr. 13a - 13c
     *
     * @todo Remove/refactor/reconsider
     *
     * @return string|null
     */
    public function getStreetWONumber()
    {
        $street = null;
        if (preg_match('/([\D]*) ([\d \/\-]+)\s?([\D]*)/', $this->getStreet(), $match)) {
            return trim($match[1]);
        }

        return $street;
    }

    /**
     * Returns the street number.
     * Gets the number by a regex. Could fail.
     * Use this together with getStreetWONumber() and getStreetExtension()
     *
     * @todo Remove/refactor/reconsider
     *
     * @return string|null
     */
    public function getStreetNumber()
    {
        $number = null;
        if (preg_match('/([\D]*) ([\d \/\-]+)\s?([\D]*)/', $this->getStreet(), $match)) {
            return trim($match[2]);
        }

        return $number;
    }

    /**
     * Returns the street number extension.
     * Gets the extension by a regex. Could fail.
     * Use this together with getStreetWONumber() and getStreetNumber()
     *
     * @todo Remove/refactor/reconsider
     *
     * @return string|null
     */
    public function getStreetExtension()
    {
        $extension = null;
        if (preg_match('/([\D]*) ([\d \/\-]+)\s?([\D]*)/', $this->getStreet(), $match)) {
            $extension = trim($match[3]);
        }

        return $extension;
    }

    /**
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Calculate and assign variables
     */
    public function calcVars()
    {
        $this->setUrl('/praxis/' . $this->slug);
    }

    /**
     * @param string $url
     *
     * @return self
     */
    public function setUrl($url)
    {
        $this->url = trim($url);

        return $this;
    }

    /**
     * @return string
     */
    public function getAdminEditUrl()
    {
        return '/administration/locations?action=edit&id=' . $this->getId();
    }

    /**
     * @return int
     */
    public function getProfileAssetId()
    {
        return $this->profile_asset_id;
    }

    /**
     * Set the profile_asset_id
     *
     * @param int $id
     * @return self
     */
    public function setProfileAssetId($id)
    {
        $this->profile_asset_id = $id;
        return $this;
    }

    /**
     * Returns the url to the profile asset/gfx for a size
     * if there is no one than return the url to the default asset/gfx
     *
     * @param int $size
     * @param bool $add_host
     *
     * @return string
     */
    public function getProfileAssetUrl($size = ASSET_GFX_SIZE_ORIGINAL, $add_host = false)
    {
        // check if the url can be calculated by the id and filename
        if (is_numeric($this->profile_asset_id) && $this->profile_asset_id > 0 && $this->profile_asset_filename != '') {
            $url = Asset::getUrlByIdFilenameOwner($this->profile_asset_id, $this->profile_asset_filename, ASSET_OWNERTYPE_LOCATION, $size, $add_host);
            if ($url) {
                return $url;
            }
        }
        // calc the asset
        $this->loadProfileAsset();
        if (is_object($this->profile_asset) && $this->profile_asset->isValid()) {
            return $this->profile_asset->getGfxUrl($size, $add_host);
        } else {
            return getStaticUrl($GLOBALS['CONFIG']['ASSET_GFX_TYPES'][ASSET_OWNERTYPE_LOCATION]['SIZES'][$size]['default'], $add_host);
        }
    }

    /**
     * Sets the profile asset and calcs the total of assets
     */
    public function loadProfileAsset()
    {
        // set the profile asset
        if ($this->profile_asset_id != 0) {
            $_profile_asset = new Asset($this->profile_asset_id);
            if ($_profile_asset->isValid()) {
                $this->profile_asset = $_profile_asset;
            }
        } elseif ($GLOBALS['CONFIG']['ASSET_USE_LAST_ASSET_AS_PROFILE_ASSET']) {
            $_asset = new Asset();
            $_profile_asset = $_asset->findObject('parent_id=0 AND owner_id=' . $this->getId() . ' AND owner_type=' . ASSET_OWNERTYPE_LOCATION . ' ORDER BY created_at DESC');
            if ($_profile_asset->isValid()) {
                $this->profile_asset = $_profile_asset;
            }
        }
    }

    /**
     * Updates the profile_asset_filename
     * This is mainly for caching purpose
     *
     * @return bool
     */
    public function updateProfileAssetFilename()
    {
        $result = false;
        // calc the asset
        $this->loadProfileAsset();
        if (is_object($this->profile_asset) && $this->profile_asset->isValid()) {
            $this->profile_asset_id = $this->profile_asset->getId();
            $this->profile_asset_filename = $this->profile_asset->getFilename();
            $result = $this->save(
                array(
                    'profile_asset_id',
                    'profile_asset_filename'
                )
            );
        }

        return $result;
    }

    /**
     * Returns the asset on the nr position (order by created_at)
     *
     * @param
     *
     * @return string
     */
    public function getAssetByNr($nr)
    {
        if ($nr < 1) {
            return false;
        }
        $nr--;
        $_asset = new Asset();
        $_assets = $_asset->findObjects('parent_id=0 AND owner_id=' . $this->id . ' AND owner_type=' . ASSET_OWNERTYPE_LOCATION . ' ORDER BY created_at');
        if (isset($_assets[$nr])) {
            return $_assets[$nr];
        } else {
            return false;
        }
    }

    /**
     * Shows $num thumbs and creates the fancybox items
     *
     * @param int $num
     * @param bool $show_profile_asset
     *
     * @return string
     */
    public function getAssetThumbsHtml($num, $show_profile_asset = false)
    {
        $view = Application::getInstance()->getView();

        $_assets = $this->getAssets();
        $_asset_thumbs = array();
        $_asset_hiddens = array();
        $_x = 1;

        foreach ($_assets as $_asset) {
            if (!$show_profile_asset) {
                if (
                    ($this->profile_asset_id == $_asset->id)
                    ||
                    ($this->profile_asset_id == 0)
                ) {
                    continue;
                }
            }
            if ($_x <= $num) {
                $_asset_thumbs[] = $_asset;
            } else {
                $_asset_hiddens[] = $_asset;
            }
            $_x++;
        }

        $view->setRef('asset_thumbs', $_asset_thumbs);
        $view->setRef('asset_hiddens', $_asset_hiddens);

        return $view->fetch('_gallery_thumbs.tpl');
    }

    /**
     * Returns all assets for this object
     *
     * @param
     *
     * @return array assets
     */
    public function getAssets()
    {
        if (is_array($this->assets)) {
            return $this->assets;
        } else {
            return $this->loadAssets();
        }
    }

    /**
     * Loads and returns all assets for this object
     *
     * @return array assets
     */
    public function loadAssets()
    {
        $_asset = new Asset();
        $_assets = $_asset->findObjects('parent_id=0 AND owner_id=' . $this->getId() . ' AND owner_type=' . ASSET_OWNERTYPE_LOCATION . ' ORDER BY created_at ASC');
        $this->assets = $_assets;

        return $this->assets;
    }

    /**
     * Get available insurances for a given location with user and integration
     *
     * @todo Move to appropriate Integration classes
     *
     * @param User $user
     * @param string $appointment_start_at
     *
     * @return array
     */
    public function getAvailableInsuranceIds(User $user, $appointment_start_at = '')
    {
        $available_insurance_ids = array();

        if ($this->getIntegrationId() == 1) {
            $usersAaManager = UsersAaManager::getInstance();
            $appointmentBlock = $usersAaManager->getByDoctorIdAndAppointmentTime($user->getId(), $appointment_start_at);
            if (isset($appointmentBlock)) {
                $availableInsurancesForApp = explode(',', $appointmentBlock->getInsuranceTypes());
                foreach ($availableInsurancesForApp as $value) {
                    $available_insurance_ids[] = $usersAaManager->getInsuranceIndexByType($value);
                }
            }
        } elseif ($this->getIntegrationId() == 2) {
            // We have two insurances, and this isn't changing soon. Hardcoded to remove NGS
            // @todo: Refactor properly
            $available_insurance_ids = array(1, 2);
        } elseif ($this->getIntegrationId() == 8) {
            // We have two insurances, and this isn't changing soon. Hardcoded to remove NGS
            // @todo: Refactor properly
            $available_insurance_ids = array(1, 2);
        }

        return $available_insurance_ids;
    }

    /**
     * Counts the number of members for this location
     *
     * @return int
     */
    public function countMembers()
    {
        return count($this->getMembers());
    }

    /**
     * @return string
     */
    public function getDataSource()
    {
        return $this->data_source;
    }

    /**
     * @param string $data_source
     * @return self
     */
    public function setDataSource($data_source)
    {
        $this->data_source = $data_source;
        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param string $updated_at
     *
     * @return self
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @param string $updated_by
     *
     * @return self
     */
    public function setUpdatedBy($updated_by)
    {
        $this->updated_by = $updated_by;

        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedBy()
    {
        return $this->updated_by;
    }

    /**
     * @param string $created_by
     *
     * @return self
     */
    public function setCreatedBy($created_by)
    {
        $this->created_by = $created_by;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * @param string $created_at
     *
     * @return self
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Returns the locations slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     * @return self
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Returns the status id
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns the status text
     *
     * @param bool $sanitize_to_text
     *
     * @return string
     */
    public function getStatusText($sanitize_to_text = false)
    {
        $_status_text = $GLOBALS['CONFIG']['LOCATION_STATUS'][$this->status];

        return $sanitize_to_text ? sanitize_title($_status_text) : $_status_text;
    }

    /**
     * @param int $status
     *
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Returns the primary member id
     *
     * @return int
     */
    public function getPrimaryMemberId()
    {
        return $this->primary_member_id;
    }

    /**
     * Checks if this location has an appointment enquiry integration
     * (where appointments are not specified like int8 or int9)
     *
     * @return bool
     */
    public function hasAppointmentEnquiryIntegration()
    {
        if ($integration = $this->getIntegration()) {
            return $integration->hasAppointmentEnquiry();
        }
        
        return false;
    }

    /**
     * Checks if this object has a profile page
     *
     * @return bool
     */
    public function hasProfile()
    {
        return !empty($this->slug);
    }

    /**
     * Returns the www address
     *
     * @return string
     */
    public function getWww()
    {
        return $this->www;
    }

    /**
     * Returns this objects specific html title
     *
     * @return string
     */
    public function getHtmlTitle()
    {
        return $this->hasHtmlTitle() ? $this->html_title : $this->getHtmlDefaultTitle();
    }

    /**
     * Checks if this object has a specific html title
     *
     * @return bool
     */
    public function hasHtmlTitle()
    {
        return !empty($this->html_title);
    }

    /**
     * Returns this objects default html title
     *
     * @return string
     */
    public function getHtmlDefaultTitle()
    {
        return
            $this->getName() .
            ', ' .
            $this->getMedicalSpecialityIdsText(' und ') .
            ' Praxis in ' .
            $this->getCityZip();
    }

    /**
     * Returns the Texts for this objects medical specialties
     *
     * @param string $delimiter
     *
     * @return string
     */
    public function getMedicalSpecialityIdsText($delimiter = ', ')
    {
        $_medical_specialty = new MedicalSpecialty();

        return getCategoriesText($this->medical_specialty_ids, $_medical_specialty->getMedicalSpecialtyValues(), $delimiter);
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;
        
        return $this;
    }

    /**
     * Returns the city and zip
     *
     * @return string
     */
    public function getCityZip()
    {
        return "{$this->zip} {$this->city}";
    }

    /**
     * Returns this objects specific html meta description
     *
     * @return string
     */
    public function getHtmlMetaDescription()
    {
        return $this->hasHtmlMetaDescription() ? $this->html_meta_description : $this->getHtmlDefaultMetaDescription();
    }

    /**
     * Checks if this object has a specific html meta description
     *
     * @return bool
     */
    public function hasHtmlMetaDescription()
    {
        return !empty($this->html_meta_description);
    }

    /**
     * Returns this objects default html meta description
     *
     * @return string
     */
    public function getHtmlDefaultMetaDescription()
    {
        return
            'In der ' .
            $this->getName() .
            ' in ' .
            $this->getCityZip() .
            ' sofort einen Termin bekommen. Buchen Sie jetzt Ihren Termin online auf Arzttermine.de.';
    }

    /**
     * Returns this objects specific html meta keywords
     *
     * @return string
     */
    public function getHtmlMetaKeywords()
    {
        return $this->hasHtmlMetaKeywords() ? $this->html_meta_keywords : $this->getHtmlDefaultMetaKeywords();
    }

    /**
     * Checks if this object has a specific html meta keywords
     *
     * @return bool
     */
    public function hasHtmlMetaKeywords()
    {
        return !empty($this->html_meta_keywords);
    }

    /**
     * returns this objects default html meta keywords
     *
     * @return string
     */
    public function getHtmlDefaultMetaKeywords()
    {
        return
            $this->getName() .
            ', ' .
            $this->getMedicalSpecialityIdsText(', ') .
            ', ' .
            $this->getCityZip() .
            ', Termin buchen, Arzttermin';
    }

    /**
     * Returns the latitude
     *
     * @return float
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Returns the longitude
     *
     * @return float
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * Returns the city and zip in sanitized url form
     *
     * @return string
     */
    public function getUrlCityZip()
    {
        return url($this->getCityZip());
    }

    /**
     * @param string $country_code
     *
     * @return self
     */
    public function setCountryCode($country_code)
    {
        $this->country_code = $country_code;

        return $this;
    }

    /**
     * @return string
     */
    public function getCountryCode()
    {
        return $this->country_code;
    }

    /**
     * Returns the address
     *
     * @param string $delimiter
     *
     * @return string
     */
    public function getAddress($delimiter = '<br />')
    {
        return $this->getStreet() . $delimiter . $this->getCityZip();
    }

    /**
     * @param string $fax
     *
     * @return self
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * @param boolean $fax_visible
     *
     * @return self
     */
    public function setFaxVisible($fax_visible)
    {
        $this->fax_visible = $fax_visible;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getFaxVisible()
    {
        return $this->fax_visible;
    }

    /**
     * @return bool
     */
    public function isPkvOnly()
    {
        return $this->pkv_only;
    }

    /**
     * @param bool $pkv_only
     */
    public function setPkvOnly($pkv_only)
    {
        $this->pkv_only = $pkv_only;
    }

    /**
     * @param float $lat
     *
     * @return self
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * @param float $lng
     *
     * @return self
     */
    public function setLng($lng)
    {
        $this->lng = $lng;

        return $this;
    }

    /**
     * Checks if a preferred MS has been set
     *
     * @return bool
     */
    public function hasPreferredMedicalSpecialtyText()
    {
        return !empty($this->preferred_medical_specialty_text);
    }

    /**
     * Returns the a custom specialty medical specialty text provided by the location
     *
     * @return string
     */
    public function getPreferredMedicalSpecialtyText()
    {
        return $this->preferred_medical_specialty_text;
    }

    /**
     * Returns a formated info text
     *
     * @param int $id
     *
     * @return string
     */
    public function getInfoText($id)
    {
        if (!isset($this->$id)) {
            return '';
        }

        return nl2br(do_shortcode(wpautop($this->$id)));
    }

    /**
     * Returns the title
     *
     * @param int $length
     *
     * @return string
     */
    public function getTitle($length = 0)
    {
        if (isset($this->title)) {
            if ($length > 0) {
                $title = mb_substr($this->title, 0, $length, $GLOBALS['CONFIG']['SYSTEM_CHARSET']);
                if (mb_strlen($this->title, $GLOBALS['CONFIG']['SYSTEM_CHARSET']) > $length) {
                    $title .= '...';
                }

                return $title;
            } else {
                return $this->title;
            }
        } else {
            return '';
        }
    }

    /**
     * Renders this teaser for html output and filters out some bad things
     *
     * @return string
     */
    public function getRssBodyHtml()
    {
        $_content = str_replace('[asset', '[asset case_id=' . $this->id, $this->body);
        $_content = preg_replace('/<!--more(.*?)?-->/', '', $_content);
        $_html = do_shortcode(wpautop($_content));

        return str_replace(']]>', ']]&gt;', $_html);
    }

    /**
     * Renders a excerpt
     *
     * @param int $excerpt_length
     *
     * @return string
     */
    public function getBodyExcerpt($excerpt_length = 55)
    {
        return getExcerpt($this->body, $excerpt_length);
    }

    /**
     * Returns the teaser
     *
     * @return string
     */
    public function getTeaser()
    {
        return $this->getBodyHtml(true, false, false);
    }

    /**
     * Renders this for html output
     *
     * @param bool $only_teaser
     * @param bool $add_teaser_image
     * @param bool $add_teaser_link
     * @param bool $add_more_anchor
     *
     * @return string
     */
    public function getBodyHtml($only_teaser = false, $add_teaser_image = false, $add_teaser_link = false, $add_more_anchor = true)
    {
        $content = $this->get_extended($this->body);

        // add the "more"-link
        if ($only_teaser && $add_teaser_link) {
            $content['main'] .= '<a href="' . $this->getUrl() . '" class="more-link">Weiterlesen</a>';
        }

        if ($add_teaser_image) {
            if ($this->countAssets() > 0) {
                // add the asset only if there is not allready one
                if (false === strpos($content['main'], '[asset')) {
                    $content['main'] = '[asset]' . $content['main'];
                }
            }
        }

        // add the case_id to let the shortcode know where we are
        $content['main'] = str_replace('[asset', '[asset case_id=' . $this->id, $content['main']);
        $content['extended'] = str_replace('[asset', '[asset case_id=' . $this->id, $content['extended']);
        $_html = do_shortcode(wpautop($content['main']));

        if (!$only_teaser && $content['extended'] != '') {
            if ($add_more_anchor) {
                $_html .= '<a name="more"></a>';
            }
            $_html .= do_shortcode(wpautop($content['extended']));
        }

        return $_html;
    }

    /**
     * Get extended entry info (<!--more-->).
     *
     * There should not be any space after the second dash and before the word
     * 'more'. There can be text or space(s) after the word 'more', but won't be
     * referenced.
     *
     * The returned array has 'main' and 'extended' keys. Main has the text before
     * the <code><!--more--></code>. The 'extended' key has the content after the
     * <code><!--more--></code> comment.
     *
     * @since 1.0.0
     *
     * @param string $post Post content.
     *
     * @return array Post before ('main') and after ('extended').
     */
    private function get_extended($post)
    {
        // Match the new style more links
        if (preg_match('/<!--more(.*?)?-->/', $post, $matches)) {
            list($main, $extended) = explode($matches[0], $post, 2);
        } else {
            $main = $post;
            $extended = '';
        }

        // Strip leading and trailing whitespace
        $main = preg_replace('/^[\s]*(.*)[\s]*$/', '\\1', $main);
        $extended = preg_replace('/^[\s]*(.*)[\s]*$/', '\\1', $extended);

        return array(
            'main'     => $main,
            'extended' => $extended
        );
    }

    /**
     * Counts the number of assets that are uploaded to this object
     *
     * @param
     *
     * @return string
     */
    public function countAssets()
    {
        $_asset = new Asset();

        return $_asset->count('parent_id=0 AND owner_id=' . $this->id . ' AND owner_type=' . ASSET_OWNERTYPE_LOCATION);
    }

    /**
     * Returns the MedicalSpecialtiesIds in an array
     * Notice: Ids not checked for validity
     *
     * @return array
     */
    public function getMedicalSpecialtyIds()
    {
        return explode(',', $this->medical_specialty_ids);
    }

    /**
     * @param array $neighborhood_ids
     *
     * @return self
     */
    public function setNeighborhoodIds(array $neighborhood_ids)
    {
        $this->neighborhood_ids = implode(',', $neighborhood_ids);

        return $this;
    }

    /**
     * @return array
     */
    public function getNeighborhoodIds()
    {
        return explode(',', $this->neighborhood_ids);
    }

    /**
     * Returns an array with the breadcrumbs
     *
     * @param array $filter
     *
     * @return array
     */
    public function getBreadcrumbs($filter = null)
    {
        $app = Application::getInstance();
        $breadcrumbs = array();

        // and add one breadcrumb per medical specialty
        $medicalSpecialties = $this->getMedicalSpecialties();

        foreach ($medicalSpecialties as $medicalSpecialty) {
            $insuranceUrl = '';
            // if the insurance is selected
            if (isset($filter['insurance_id'])) {
                $insurance = new Insurance($filter['insurance_id'], 'id');
                if ($insurance->isValid()) {
                    $insuranceUrl = '/' . $insurance->getSlug();
                }
            }

            $breadcrumb = array(
                array(
                    'url'   => $app->i18nTranslateUrl('/'),
                    'title' => 'Startseite',
                    'text'  => 'Arzttermine.de'
                ),
                array(
                    'url'  => $app->i18nTranslateUrl(
                            '/' .
                            $medicalSpecialty->getSlug() .
                            '/' .
                            sanitize_slug($this->getCity()) .
                            $insuranceUrl
                        ),
                    'text' => $medicalSpecialty->getName() . ' in ' . $this->getCity()
                ),
                array(
                    'url'  => $this->getUrlWithFilter($filter),
                    'text' => $this->getName()
                )
            );
            $breadcrumbs[] = $breadcrumb;
        }

        return $breadcrumbs;
    }

    /**
     * Returns the MedicalSpecialties objects for this object
     *
     * @return MedicalSpecialty[]
     */
    public function getMedicalSpecialties()
    {
        $medical_specialties = array();

        foreach ($this->getMedicalSpecialtyIds() as $msid) {
            $ms = new MedicalSpecialty($msid);
            if ($ms->isValid()) {
                $medical_specialties[] = $ms;
            }
        }

        return $medical_specialties;
    }

    /**
     * @param array $medical_specialty_ids
     *
     * @return self
     */
    public function setMedicalSpecialtyIds(array $medical_specialty_ids = array())
    {
        $this->medical_specialty_ids = implode(',', $medical_specialty_ids);

        return $this;
    }

    /**
     * @param MedicalSpecialty[] $medical_specialties
     *
     * @return $this
     */
    public function setMedicalSpecialties(array $medical_specialties)
    {
        $medical_specialty_ids = array();
        
        foreach ($medical_specialties as $ms) {
            $medical_specialty_ids[] = $ms->getId();
        }
        
        array_unique($medical_specialty_ids);
        
        $this->setMedicalSpecialtyIds($medical_specialty_ids);
            
        return $this;
    }

    /**
     * Returns the url with filter attrs
     *
     * @param array $filter_array
     * @param string $name
     * @param bool $absolute_url
     *
     * @return string
     */
    public function getUrlWithFilter($filter_array, $name = 'url', $absolute_url = false)
    {
        $url = $this->getUrl($absolute_url);
        $attrs = '';

        foreach ($filter_array as $filter_id => $filter_value) {
            switch ($filter_id) {
                case 'insurance_id':
                    if (isset($GLOBALS['CONFIG']['INSURANCE_SLUGS'][$filter_value])) {
                        $attrs = '/' . $GLOBALS['CONFIG']['INSURANCE_SLUGS'][$filter_value];
                    }
                    break;
            }
        }

        return Application::getInstance()->i18nTranslateUrl($url . $attrs);
    }

    /**
     * Returns the reviews for a location which are coming from all connected users
     *
     * @return Review[]
     */
    public function getReviews()
    {
        if (!$this->isValid()) {
            return null;
        }

        return Review::getReviewsForLocation($this->getMemberIds());
    }

    /**
     * Returns the number of reviews for a location which are coming from all connected users
     *
     * @return int
     */
    public function countReviews()
    {
        if (!$this->isValid()) {
            return null;
        }

        return Review::countReviewsForLocation($this->getMemberIds());
    }

    /**
     * Updates the ratings for the location and stores them in the db
     *
     * @return bool
     */
    public function updateRatings()
    {
        $review = new Review();
        $ratings = $review->getRatingsAverageArrayForLocation($this->getMemberIds());

        if (is_array($ratings) && !empty($ratings)) {
            $this->rating_1 = $ratings['rating_1'];
            $this->rating_2 = $ratings['rating_2'];
            $this->rating_3 = $ratings['rating_3'];
            $this->rating_average = $ratings['rating_average'];

            return $this->save(
                array(
                    'rating_1',
                    'rating_2',
                    'rating_3',
                    'rating_average'
                )
            );
        }

        return false;
    }

    /**
     * Backwards compatibility
     *
     * @deprecated
     *
     * @return bool
     */
    public function showReviews()
    {
        return $this->showRatings();
    }

    /**
     * - Dont show ratings if the user has no reviews
     *
     * @return bool
     */
    public function showRatings()
    {
        return !empty($this->rating_average);
    }

    /**
     * If the location must have a "double-check" option at booking
     *
     * @return bool
     */
    public function mustValidateTreatmentType()
    {
        $location_ids = explode(',', Configuration::get('treatmenttype_confirm_for_locations'));

        return in_array($this->getId(), $location_ids);
    }

    /**
     * returns the rating html
     *
     * @param int $id
     * @param string $text
     * @param string $user_id
     *
     * @return string
     */
    public function getRatingHtml($id, $text, $user_id = '')
    {
        if (!empty($user_id)) {
            $user_id = '-l' . $user_id;
        } else {
            $user_id = '';
        }

        $html = '<div class="rating"><div class="text">' . Application::getInstance()
                ->_($text) . '</div><div class="stars">';

        for ($x = 0.5; $x <= 5; $x += 0.5) {
            if ($this->$id >= $x) {
                $_checked = ' checked="checked"';
            } else {
                $_checked = '';
            }
            $html .= '<input class="star {split:2}" type="radio" name="rating' . $user_id . '-l' . $this->id . '-' . $id . '" value="' . $x . '" disabled="disabled"' . $_checked . ' />';
        }
        $html .= '</div></div><div class="clear"></div>';

        return $html;
    }

    /**
     * Returns the social bookmarks code
     *
     * @return string
     */
    public function getSocialBookmarksCode()
    {
        return '<div class="social_bookmarks" href="' . $this->getPermalink() . '"></div>';
    }

    /**
     * Returns the permalink
     *
     * @return string
     */
    public function getPermalink()
    {
        return $this->getUrl('url', true);
    }

    /**
     * returns the rating value for a key
     *
     * @param string $key
     *
     * @return float
     */
    public function getRating($key = 'rating_average')
    {
        $value = 0;
        switch ($key) {
            case 'rating_1':
            case 'rating_2':
            case 'rating_3':
            case 'rating_average':
                $value = $this->$key;
                break;
        }

        return $value;
    }

    /**
     * Returns the locations that are connected to a user
     *
     * @param userId
     *
     * @return array Location
     */
    public static function getLocationsByUser($userId)
    {
        $location = new self();
        $sql
            = "
			SELECT location_id AS id
			FROM " . DB_TABLENAME_LOCATION_USER_CONNECTIONS . " luc, " . DB_TABLENAME_USERS . " u
			WHERE luc.user_id=" . $userId . " AND luc.user_id = u.id";

        return $location->findObjectsSql($sql);
    }

    /**
     * @param string $salesforceId
     *
     * @return int|bool
     */
    public function getIdBySalesforceId($salesforceId)
    {
        /*
         * delete the 3 last characters from $salesforceId
         * the Id stored in SF is 3 characters longer than the
         * Id that is exported in CSV export and eventually
         * stored in the database.
         */
        $salesforceId = substr($salesforceId, 0, -3);
        return $this->findKey('salesforce_id LIKE "'.$salesforceId.'" AND status > '.self::STATUS_DRAFT, 'id');
    }

    /**
     * @return bool
     */
    public function isVisible()
    {
        return (
            in_array($this->getStatus(), array(
                self::STATUS_ACTIVE,
                self::STATUS_VISIBLE,
                self::STATUS_VISIBLE_APPOINTMENTS
            ))
        );
    }

    /**
     * Is another user allowed to edit this Location?
     * If the user shares a practice with another, then yes
     *
     * @param int $editor The user id of the editor
     *
     * @return bool
     */
    public function isEditAllowed($editor)
    {
        $permission = false;

        if ($this->isValid()) {
            foreach ($this->getMembers() as $member) {
                if ($member instanceof User && $member->getId() == $editor) {
                    $permission = true;
                    break;
                }
            }
        }

        return $permission;
    }

    /**
     * @param array $origin
     * @param int $radius
     * 
     * @return Location[]
     */
    public function findWithinRadius($origin, $radius)
    {
        // Whole numbers please
        $radius = intval($radius);
        
        // Try for the shorthand, then fallback to longhand, then just fail
        $lat = !empty($origin['lat']) ? $origin['lat'] : (!empty($origin['latitude'])  ? $origin['latitude']  : '');
        $lng = !empty($origin['lng']) ? $origin['lng'] : (!empty($origin['longitude']) ? $origin['longitude'] : '');

        if (!$lat || !$lng || !$radius) {
            return array();
        }

        $locations = array();

        $distance
            = "(
			 6368 * SQRT(2*(1-cos(RADIANS(`lat`)) *
			 cos(RADIANS(" . $lat . ")) * (sin(RADIANS(`lng`)) *
			 sin(RADIANS(" . $lng . ")) + cos(RADIANS(`lng`)) *
			 cos(RADIANS(" . $lng . "))) - sin(RADIANS(`lat`)) * sin(RADIANS(" . $lat . "))))) AS distance ";

        $query
            = " SELECT " .
                implode(',', $this->getQueryColumns()) .
                ", " . $distance . "
                FROM " . DB_TABLENAME_LOCATIONS;
        
        $query .= " WHERE  
            (
                " . DB_TABLENAME_LOCATIONS . ".`status`=" . LOCATION_STATUS_VISIBLE . "
                OR
                " . DB_TABLENAME_LOCATIONS . ".`status`=" . LOCATION_STATUS_VISIBLE_APPOINTMENTS . "
            ) ";

        $query .= sprintf(" HAVING distance<%d ", $radius);
        $query .= " ORDER BY `position_factor` DESC, distance ASC LIMIT " . $GLOBALS['CONFIG']['SEARCH_MAX_RESULTS'];
        
        $db = Application::getInstance()->getSql();
        $db->query($query);
        
        foreach($db->getResult() as $result) {
            $location = new Location();
            unset($result['distance']); // Not needed
            $locations[] = $location->hydrate($result);
        }
        
        return $locations;
    }

    /**
     * Echoes out all data about a location and its users.
     * Uses flush() and ob_flush() to force an immediately response to the browser/client.
     *
     * Alternatively this way could be used:
     *
     * $ids = array();
     * $query = '
     * SELECT
     *   u.id AS user_id, l.id AS location_id
     * FROM '.DB_TABLENAME_USERS.' u, '.DB_TABLENAME_LOCATIONS.' l, '.DB_TABLENAME_LOCATION_USER_CONNECTIONS.' luc
     * WHERE
     *   luc.user_id = u.id
     * AND
     *   luc.location_id = l.id
     * ORDER BY l.id ASC';
     *
     * @todo Remove
     */
    public static function echoExportProviders()
    {
        $location = new Location();
        $location_ids = $location->findKeys('1=1 ORDER BY id ASC');

        if (empty($location_ids)) {
            echo 'ERROR: No available locations';

            return;
        }

        echo '"LocationId","LocationStatusText","LocationName","LocationZIP","LocationCity","LocationStreet","LocationPhone","LocationPhoneVisible","LocationIntegrationId","LocationIntegrationText","LocationUrl","UserId","UserStatusText","UserGender","UserTitle","UserFirstName","UserLastName","UserIsPaying","UserUrl","UserMedicialSpecialties"' . "\n";
        flush();
        ob_flush();
        foreach ($location_ids as $location_id) {
            $_location = new Location($location_id);
            $users = $_location->getMembers();
            /** @var User[] $users */
            if (is_array($users) && !empty($users)) {
                foreach ($users as $user) {
                    $integrationName = '';
                    $integration = $_location->getIntegration();
                    if (is_object($integration)) {
                        $integrationName = $integration->getName();
                    }
                    echo sprintf(
                             '"%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s"',
                             sanitize_csv_field($_location->getId()),
                             sanitize_csv_field($_location->getStatusText(true)),
                             sanitize_csv_field($_location->getName()),
                             sanitize_csv_field($_location->getZip()),
                             sanitize_csv_field($_location->getCity()),
                             sanitize_csv_field($_location->getStreet()),
                             sanitize_csv_field($_location->getPhone()),
                             sanitize_csv_field($_location->getPhoneVisible()),
                             sanitize_csv_field($_location->getIntegrationId()),
                             sanitize_csv_field($integrationName),
                             sanitize_csv_field($_location->getUrl(true)),
                             sanitize_csv_field($user->getId()),
                             sanitize_csv_field($user->getStatusText(true)),
                             sanitize_csv_field($user->getGenderText()),
                             sanitize_csv_field($user->getTitle()),
                             sanitize_csv_field($user->getFirstName()),
                             sanitize_csv_field($user->getLastName()),
                             sanitize_csv_field($user->hasContract() ? 'Ja' : 'Nein'),
                             sanitize_csv_field($user->getUrl(true)),
                             sanitize_csv_field($user->getMedicalSpecialityIdsText())
                         ) . "\n";
                    flush();
                    ob_flush();
                }
            }
        }
    }
}
