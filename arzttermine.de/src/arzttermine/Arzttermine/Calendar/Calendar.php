<?php

namespace Arzttermine\Calendar;

use Arzttermine\Application\Application;
use Arzttermine\Core\DateTime;
use Arzttermine\Core\Configuration;

class Calendar {

    /**
     * returns the first possible datetime where to search for free times
     *
     * @deprecated Use the \DateTime version where possible
     *
     * @return DateTime
     */
    static function getFirstPossibleDateTimeForSearch()
    {
        if (date('H:i') >= SHOW_NEXT_DAY_APPS_AFTER_THIS_TIME) {
            return date('Y-m-d', strtotime('tomorrow'));
        } else {
            return date('Y-m-d');
        }
    }

    /**
     * If the time of day is sufficiently late, we just make the search look for "the next day"
     * 
     * @return DateTime
     */
    public static function getNextAvailableSearchDate()
    {
        $date = new DateTime();

        //from friday 3pm till monday 11am search from monday 11am on
        if($date->format('N') == 5 && $date->format('H:i')>='15:00') {
            $date->modify('next monday');
            $date->setTime(11,0);

            //if if's after SHOW_NEXT_DAY_APPS_AFTER_THIS_TIME search from next day 10am on
        } elseif($date->format('N') > 5) {
            $date->modify('next monday');
            $date->setTime(11,0);

        //if if's after SHOW_NEXT_DAY_APPS_AFTER_THIS_TIME search from next day 10am on
        } elseif ($date->format('H:i') >= SHOW_NEXT_DAY_APPS_AFTER_THIS_TIME) {
            $date->modify('+1 day');
            $date->setTime(10,0);
            // Miss the next day as well

        //otherwise search from tomorrow 00:00
        } else {
            $date = new DateTime('tomorrow');
        }
        
        return $date;
    }

    /**
     * checks if a date time is valid
     *
     * @param string $datetime
     *
     * @return bool
     */
    public static function isValidDateTime($datetime)
    {
        if (preg_match("/^(\d{4})-(\d{2})-(\d{2}) ([01][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/", $datetime, $matches)) {
            return strtotime($datetime) > 0;
        }

        return false;
    }

    /**
     * checks if a date is valid
     *
     * @param string $date
     *
     * @return bool
     */
    public static function isValidDate($date)
    {
        return strtotime($date) > 0;
    }

    /**
     * Single method to consistently return dates in the specified format
     * 
     * @param DateTime|string $datetime
     *
     * @return string
     */
    static function getDate($datetime)
    {
        if ($datetime instanceof DateTime) {
            return $datetime->format('Y-m-d');
        }
        
        return date('Y-m-d', strtotime($datetime));
    }

    /**
     * Single method to consistently return dates in the specified format
     *
     * @param DateTime|string $datetime
     *
     * @return string
     */
    static function getTime($datetime)
    {
        if ($datetime instanceof DateTime) {
            return $datetime->format('H:i:s');
        }
        
        return date('H:i:s', strtotime($datetime));
    }

    /**
     * Returns the last possible datetime where to search for free times
     *
     * @return string
     */
    static function getLastPossibleDateTimeForSearch()
    {
        return date('Y-m-d', strtotime('+' . intval($GLOBALS['CONFIG']['CALENDAR_MAX_DAYS_AHEAD']) . ' day'));
    }

    /**
     * Returns the weekday in ISO8601 format
     * (1 = monday, 7 = sunday)
     *
     * @param DateTime|string $datetime
     *
     * @return string weekday
     */
    public static function getWeekdayISO8601($datetime)
    {
        if ($datetime instanceof DateTime) {
            return $datetime->format('N');
        }
        
        return date('N', strtotime($datetime));
    }

    /**
     * Get the first date in a DateRange.
     * Yes, this is necessary :/
     * 
     * @param \DatePeriod $date_range
     * 
     * @return DateTime
     */
    public static function getRangeStart(\DatePeriod $date_range)
    {
        $days = iterator_to_array($date_range);
        return end($days);
    }

    /**
     * Get the last date in a DateRange.
     * Yes, this is necessary :/
     *
     * @param \DatePeriod $date_range
     *
     * @return DateTime
     */
    public static function getRangeEnd(\DatePeriod $date_range)
    {
        $days = iterator_to_array($date_range);
        return reset($days);
    }

    /**
     * converts a UTC datetime in ISO_8601/RFC3339 ("Y-m-d\TH:i:sP") format
     * to a datetime in ISO 9075 ("Y-m-d H:i:s") format
     * in the given timezone
     *
     * @param datetime
     * @param datetimezone
     *
     * @return datetime or bool
     */
    public static function UTC2SystemDateTime($utc, $datetimezone = null)
    {
        if ($datetimezone == null) {
            $datetimezone = $GLOBALS['CONFIG']['SYSTEM_TIMEZONE'];
        }
        // utc allways has its own timezone per definition, so no need to set the timezone here
        $date = new DateTime($utc);
        // now set the (new) timezone
        $date->setTimezone(new \DateTimeZone($datetimezone));

        // and return the datetime within the new timezone
        return $date->format('Y-m-d H:i:s');
    }

    /**
     * converts the local datetime to a UTC datetime
     *
     * @param datetime
     *
     * @return datetime or bool
     */
    public static function LocalDateTime2UTC($datetime)
    {
        $_dta = Calendar::getDateTimeArray($datetime);
        if (!is_array($_dta)) {
            return false;
        }

        return gmdate("Y-m-d H:i:s", mktime($_dta['hour'], $_dta['minute'], $_dta['second'], $_dta['month'], $_dta['day'], $_dta['year']));
    }

    /**
     * checks if a datetime or date is valid
     *
     * @param string|\Datetime $datetime_or_date
     *
     * @return array|bool
     */
    static function getDateTimeArray($datetime_or_date)
    {
        if (preg_match("/^(\d{4})-(\d{2})-(\d{2}) ([01][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/", $datetime_or_date, $matches)) {
            return array(
                'year'   => $matches[1],
                'month'  => $matches[2],
                'day'    => $matches[3],
                'hour'   => $matches[4],
                'minute' => $matches[5],
                'second' => $matches[6]
            );
        }
        if (preg_match("/^(\d{4})-(\d{2})-(\d{2})$/", $datetime_or_date, $matches)) {
            return array(
                'year'   => $matches[1],
                'month'  => $matches[2],
                'day'    => $matches[3],
                'hour'   => '00',
                'minute' => '00',
                'second' => '00'
            );
        }

        return false;
    }

    /**
     * converts the utc to datetime
     *
     * @param datetime_id
     *
     * @return datetime or bool
     */
    public static function UTC2DateTime($datetime_id)
    {
        $date_create_from_format = date_create_from_format('Ymd\THis', $datetime_id);
        if (!$date_create_from_format) {
            $date_create_from_format = date_create_from_format('Ymd\THis\Z', $datetime_id);
        }
        if (!$date_create_from_format) {
            return false;
        }

        return $date_create_from_format->format('Y-m-d H:i:s');
    }

    /**
     * returns the datetime in yyyymmdd format
     *
     * @param datetime
     *
     * @return datetime or bool
     */
    public static function dateTime2DateId($datetime)
    {
        $_dta = Calendar::getDateTimeArray($datetime);
        if (!is_array($_dta)) {
            return false;
        }

        return $_dta['year'] . $_dta['month'] . $_dta['day'];
    }

    /**
     * @param $date1
     * @param $date2
     *
     * @return string
     */
    public static function dateDiffDaysCount($date1, $date2)
    {
        $date1 = date_create($date1);
        $date2 = date_create($date2);

        return $date2->diff($date1)->format("%a");
    }

    /**
     * Returns the datetime in yyyymmddThhiissZ format
     *
     * @param string $datetime
     *
     * @return string datetime or bool
     */
    public static function dateTime2UTC($datetime)
    {
        $_dta = Calendar::getDateTimeArray($datetime);
        if (!is_array($_dta)) {
            return false;
        }

        return $_dta['year'] . $_dta['month'] . $_dta['day'] . 'T' . $_dta['hour'] . $_dta['minute'] . $_dta['second'] . 'Z';
    }

    /**
     * Returns the time in seconds
     *
     * @param string $time
     *
     * @return string|bool seconds or bool
     */
    public static function time2Seconds($time)
    {
        if (preg_match("/^([01][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/", $time, $matches)) {
            $_seconds = ($matches[1] * (3600)) + ($matches[2] * 60) + $matches[3];

            return $_seconds;
        }

        return false;
    }

    /**
     * @param $datetime
     * @param bool $short_weekday
     * @param string $date_format
     *
     * @return string
     */
    public static function getWeekdayDateText($datetime, $short_weekday = false, $date_format = 'd.m.Y')
    {
        return Calendar::getWeekdayTextByDateTime($datetime, $short_weekday) . ' ' . date($date_format, strtotime($datetime));
    }

    /**
     * returns the weekday text
     *
     * @param string $date
     * @param bool $short weekday (=false)
     *
     * @return string
     */
    public static function getWeekdayTextByDateTime($date, $short = false)
    {
        $_weekday = date('w', strtotime($date));

        return self::getWeekdayText($_weekday, $short);
    }

    /**
     * Returns the weekday text
     *
     * @param int $weekday (0=Sun - 6=Sat)
     * @param bool $short (=false)
     *
     * @return string
     */
    public static function getWeekdayText($weekday, $short = false)
    {
        $app = Application::getInstance();

        switch ($weekday) {
            case 0:
            case 7: // ISO-8601
                if ($short) {
                    return $app->_('So');
                } else {
                    return $app->_('Sonntag');
                }
                break;
            case 1:
                if ($short) {
                    return $app->_('Mo');
                } else {
                    return $app->_('Montag');
                }
                break;
            case 2:
                if ($short) {
                    return $app->_('Di');
                } else {
                    return $app->_('Dienstag');
                }
                break;
            case 3:
                if ($short) {
                    return $app->_('Mi');
                } else {
                    return $app->_('Mittwoch');
                }
                break;
            case 4:
                if ($short) {
                    return $app->_('Do');
                } else {
                    return $app->_('Donnerstag');
                }
                break;
            case 5:
                if ($short) {
                    return $app->_('Fr');
                } else {
                    return $app->_('Freitag');
                }
                break;
            case 6:
                if ($short) {
                    return $app->_('Sa');
                } else {
                    return $app->_('Samstag');
                }
                break;
        }

        return '';
    }

    /**
     * Returns the date_end calculated by the date_start and date_days
     * with taking care of the hidden / filtered days like weekends
     *
     * hack!!! Should be optimized. Right now I don't know how...
     * This relies on the fact that only weekends are filtered out
     * in method getDatesInAreaWithFilter()
     *
     * @param $date_start
     * @param $date_days
     *
     * @return bool
     */
    public static function getDateEndByStartAndDays($date_start, $date_days)
    {
        // date_days * 2 should find all days
        $date_end = self::addDays($date_start, ($date_days * 2));
        $dates = self::getDatesInAreaWithFilter($date_start, $date_end);
        if (empty($dates)) {
            return false;
        }

        return $dates[$date_days - 1];
    }

    /**
     * @param string $datetime
     * @param int $daysCount
     * @param bool $dateOnly
     *
     * @return bool|string
     */
    public static function addDays($datetime, $daysCount, $dateOnly = false)
    {
        return date(
            $dateOnly ? 'Y-m-d' : 'Y-m-d H:i:s',
            strtotime('+' . $daysCount . ' day', strtotime($datetime))
        );
    }

    /**
     * Returns an array with days that are between two dates/datetimes
     * AND filters out the days that should not be shown in the calendar
     * Here it filters out the weekends
     * 
     * @deprecated use getDatesInRange
     *
     * @param datetime
     * @param datetime
     *
     * @return array
     */
    public static function getDatesInAreaWithFilter($date_start, $date_end)
    {
        $dates = self::getDatesInRange($date_start, $date_end, true);
        
        return array_map(function($date) {
            /** @var DateTime $date */
            return $date->getDate();
        }, $dates);
    }

    /**
     * Returns an array with days that are between two dates/datetimes
     * 
     * @param string|DateTime $date_start
     * @param string|DateTime $date_end
     * @param bool $with_filter
     *
     * @return DateTime[]
     */
    public static function getDatesInRange($date_start, $date_end, $with_filter = false)
    {
        $dates = array();
        
        if (!($date_start instanceof DateTime)) {
            $date_start = new DateTime($date_start);
        }

        if (!($date_end instanceof DateTime)) {
            $date_end = new DateTime($date_end);
        }

        $date_range = new \DatePeriod($date_start, new \DateInterval('P1D'), $date_end);

        /** @var DateTime $date */
        foreach ($date_range as $date) {
            // "N" gives us an int with Monday = 1 to Sunday = 7
            if ($with_filter && $date->format('N') > 5) {
                continue;
            }
            // @todo Fix casting :/
            $dates[] = new DateTime($date->format('Y-m-d H:i:s'));
        }

        return $dates;
    }

    /**
     * @param $datetime_start
     * @param $datetime_end
     * @param string $format
     *
     * @return string
     */
    public static function getDiff($datetime_start, $datetime_end, $format = '%a')
    {
        $datetime1 = new DateTime($datetime_start);
        $datetime2 = new DateTime($datetime_end);
        $interval = $datetime1->diff($datetime2);

        return $interval->format($format);
    }

    /**
     * @param null $datetime
     *
     * @return bool
     */
    public static function isInWorkingHours($datetime = null)
    {
        if (!isset($datetime)) {
            $datetime = date("Y-m-d");
        }
        if (!self::isWorkingDay(date('Y-m-d', strtotime($datetime)))) {
            return false;
        }
        $time = date('H:i', strtotime($datetime));

        return $time > WORKING_HOUR_START && $time < WORKING_HOUR_END;
    }

    /**
     * @param $date
     *
     * @return bool
     */
    public static function isWorkingDay($date)
    {
        $holidaysString = Configuration::get('holidays');
        $holidaysArray = explode(',', $holidaysString);
        $weekday = date('N', strtotime($date));

        return $weekday <= 5 && !in_array($date, $holidaysArray);
    }

    /**
     * @param $date
     * @param $city
     * @param $zipCode
     *
     * @return bool
     */
    public static function isRegionalHoliday($date, $city, $zipCode)
    {

        $weekday = date('N', strtotime($date));
        if ($weekday == 6 || $weekday == 7) {
            return true;
        }
        $regionalHolidaysText = Configuration::get('regional-holidays');
        // var_dump($regionalHolidaysText,$datetime, $city, $zipCode);exit;
        $regionalHolidayBlocks = explode(';', $regionalHolidaysText);
        foreach ($regionalHolidayBlocks as $regionalHolidayBlock) {
            $openBracketPos = strpos($regionalHolidayBlock, '(');
            $closeBracketPos = strpos($regionalHolidayBlock, ')');
            $regionalHolidayCity = substr($regionalHolidayBlock, 0, $openBracketPos);
            if (strtolower($city) !== strtolower($regionalHolidayCity)) {
                continue;
            }
            $regionalHolidayZipCodesText = substr($regionalHolidayBlock, $openBracketPos + 1, $closeBracketPos - $openBracketPos - 1);
            //example "10000-12000,13456,23567,22000-23000" 
            $regionalHolidayZipCodesBlockArray = explode(',', $regionalHolidayZipCodesText);
            $zipInvolved = self::isZipBlocksContains($regionalHolidayZipCodesBlockArray, $zipCode);
            if (!$zipInvolved) {
                continue;
            }

            $colonPos = strpos($regionalHolidayBlock, ':');
            $holidaysDatesText = substr($regionalHolidayBlock, $colonPos + 1);
            $holidaysDatesArray = explode(',', $holidaysDatesText);
            if (in_array($date, $holidaysDatesArray)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $zipBlocks
     * @param $zip
     *
     * @return bool
     */
    private static function isZipBlocksContains($zipBlocks, $zip)
    {
        foreach ($zipBlocks as $zipBlock) {
            if (strpos($zipBlock, '-') !== false) {
                list($startZip, $endZip) = explode('-', $zipBlock);
                if (intval($zip) >= intval($startZip) && intval($zip) <= intval($endZip)) {
                    return true;
                }
            } else {
                if ($zipBlock === $zip) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param string $datetime
     * @param int $count
     * @param bool $timeOnly
     *
     * @return bool|string
     */
    public static function addMinutes($datetime, $count, $timeOnly = false)
    {
        return date(
            $timeOnly ? 'H:i:s' : 'Y-m-d H:i:s',
            strtotime('+' . $count . ' minute', strtotime($datetime))
        );
    }

    /**
     * @param $date_start
     * @param $days_number
     *
     * @return bool|string
     */
    public static function getEndtimeExcludingWeekdays($date_start, $days_number)
    {
        $_date_start = '';
        $_visible_days = 0;
        $_offset = 0;

        while ($_visible_days < $days_number) {
            $_date_start = Calendar::addDays($date_start, $_offset, true);
            $_weekday = Calendar::getWeekday($_date_start);
            $_offset++;
            if ($_weekday == 0 || $_weekday == 6) {
                continue;
            }
            $_visible_days++;
        }

        return $_date_start;
    }

    /**
     * @param $datetime
     *
     * @return bool|string
     */
    public static function getWeekday($datetime)
    {
        return date('w', strtotime($datetime));
    }

    /**
     * @param $date_start
     * @param $date_days
     * @param bool $printDates
     *
     * @return string
     */
    public static function getCalendarDaysHtml($date_start, $date_days, $printDates = true)
    {
        $_html = '';
        $_offset = 0;
        $_visible_days = 0;
        while ($_visible_days < $date_days) {
            $_date_start = Calendar::addDays($date_start, $_offset);
            $_offset++;
            $_weekday = Calendar::getWeekday($_date_start);
            if ($_weekday == 0 || $_weekday == 6) {
                continue;
            }

            if ($_offset == 0) {
                $_class = 'first';
            } elseif ($_offset == ($date_days - 1)) {
                $_class = 'last';
            } else {
                $_class = '';
            }
            $_html .= self::getCalendarDayHtml($_date_start, $_class, $printDates);
            $_visible_days++;
        }

        return $_html;
    }

    /**
     * @param $date
     * @param null $class
     * @param bool $printDates
     *
     * @return string
     */
    public static function getCalendarDayHtml($date, $class = null, $printDates = true)
    {
        $app = Application::getInstance();
        $view = $app->getView();

        $_weekday = Calendar::getWeekdayTextByDatetime($date);
        $_date = Calendar::getDateText($date);
        $view->setRef('weekday', $_weekday);
        $view->setRef('date', $printDates ? $_date : '');
        $view->setRef('class', $class);

        return $view->fetch('calendar/_calendar_day.tpl');
    }

    /**
     * Returns the date in text form
     *
     * @param string $datetime
     *
     * @return string
     */
    static function getDateText($datetime)
    {
        return date('d.m.Y', strtotime($datetime));
    }

    /**
     * @param $datetime
     * @param bool $short_weekday
     *
     * @return string
     */
    public function getCalendarWeekdayDateTimeText($datetime, $short_weekday = false)
    {
        return Calendar::getWeekdayDateTimeText($datetime, $short_weekday);
    }

    /**
     * @param $datetime
     * @param bool $short
     *
     * @return string
     */
    public static function getWeekdayDateTimeText($datetime, $short = false)
    {
        if ($short) {
            return Calendar::getWeekdayTextByDateTime($datetime, $short) . ' ' . date('d.m.Y', strtotime($datetime)) . ', ' . date('H:i', strtotime($datetime));
        } else {
            return Calendar::getWeekdayTextByDateTime($datetime, $short) . ' ' . date('d.m.Y', strtotime($datetime)) . ' um ' . date('H:i', strtotime($datetime)) . ' Uhr';
        }
    }

    /**
     * @param $datetime
     * @param string $format
     *
     * @return string
     */
    public function getCalendarDateTimeFormat($datetime, $format = 'Y-m-d H:i:s')
    {
        return Calendar::getDateTimeFormat($datetime, $format);
    }

    /**
     * @param string $datetime
     * @param string $format
     *
     * @return bool|string
     */
    static function getDateTimeFormat($datetime, $format = 'Y-m-d H:i:s')
    {
        return date($format, strtotime($datetime));
    }
}

