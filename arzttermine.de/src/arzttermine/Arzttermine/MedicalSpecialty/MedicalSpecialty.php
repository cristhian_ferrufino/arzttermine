<?php

namespace Arzttermine\MedicalSpecialty;

use Arzttermine\Application\Application;
use Arzttermine\Core\Basic;
use Arzttermine\Event\Event;
use Arzttermine\Location\Location;

class MedicalSpecialty extends Basic {

    const STATUS_DRAFT = 0;
    const STATUS_ACTIVE = 1;

    /**
     * @var string
     */
    protected $tablename = DB_TABLENAME_MEDICAL_SPECIALTIES;

    /**
     * @var int
     */
    protected $status;

    /**
     * @var string
     */
    protected $slug_de = '';

    /**
     * @var string
     */
    protected $slug_en = '';

    /**
     * @var string
     */
    protected $slug_es = '';

    /**
     * @var string
     */
    protected $name_singular_de = '';

    /**
     * @var string
     */
    protected $name_singular_en = '';

    /**
     * @var string
     */
    protected $name_singular_es = '';

    /**
     * @var string
     */
    protected $name_plural_de = '';

    /**
     * @var string
     */
    protected $name_plural_en = '';

    /**
     * @var string
     */
    protected $name_plural_es = '';

    /**
     * @var string
     */
    protected $description_de = '';

    /**
     * @var string
     */
    protected $description_en = '';

    /**
     * @var string
     */
    protected $description_es = '';

    /**
     * @var string
     */
    protected $created_at = '';

    /**
     * @var string
     */
    protected $created_by = '';

    /**
     * @var string
     */
    protected $updated_at = '';

    /**
     * @var string
     */
    protected $updated_by = '';

    /**
     * @var string
     */
    protected $url = '';

    /**
     * @var array
     */
    protected $all_keys
        = array(
            "id",
            "status",
            "slug_de",
            "name_singular_de",
            "name_plural_de",
            "slug_en",
            "name_singular_en",
            "name_plural_en",
            "description_de",
            "description_en",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by"
        );

    /**
     * @var array
     */
    protected $secure_keys
        = array(
            "id",
            "status",
            "slug_de",
            "name_singular_de",
            "name_plural_de",
            "slug_en",
            "name_singular_en",
            "name_plural_en",
            "description_de",
            "description_en"
        );

    /**
     * @return array
     */
    public function toArray()
    {
        return array(
            'id'   => $this->getId(),
            'name' => $this->getName(),
            'slug' => $this->getSlug(),
        );
    }
    
    /**
     * Get the string that determines labels
     *
     * @param bool $singular
     *
     * @return string
     */
    protected static function getLabelAttribute($singular)
    {
        return 'name_' . ($singular ? 'singular' : 'plural') . '_' . Application::getInstance()->getLanguageCode();
    }

    /**
     * {@inheritdoc}
     */
    public function load($value, $key = 'id')
    {
        // We need to translate the slug key before loading it
        if ($key == 'slug') {
            $key .= "_" . Application::getInstance()->getLanguageCode();
        }
        
        return parent::load($value, $key);
    }

    /**
     * Returns the status id
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @return string
     */
    public function getUpdatedBy()
    {
        return $this->updated_by;
    }

    /**
     * Returns the name as singular or plural in the current language
     *
     * @param bool $singular
     *
     * @return string
     */
    public function getName($singular = true)
    {
        $attr = self::getLabelAttribute($singular);
        
        if (isset($this->$attr)) {
            return $this->$attr;
        }
        
        return '';
    }

    /**
     * Returns the name as singular or plural in the current language
     *
     * @param int $id
     * @param bool $singular
     *
     * @return string
     */
    public static function getNameById($id, $singular = true)
    {
        $medical_specialty = new MedicalSpecialty($id);

        if ($medical_specialty->isValid()) {
            return $medical_specialty->getName($singular);
        }

        return '';
    }

    /**
     * Checks if the id exists
     *
     * @param int $id
     *
     * @return bool
     */
    public static function idExists($id)
    {
        if (!is_numeric($id) || $id < 1) {
            return false;
        }

        $medical_specialty = new MedicalSpecialty($id);

        return ($medical_specialty->isValid());
    }

    /**
     * Returns the name as singular or plural in the current language
     *
     * @return string
     */
    public function getSlug()
    {
        $attr = 'slug_' . Application::getInstance()->getLanguageCode();

        if (isset($this->$attr)) {
            return $this->$attr;
        }

        return '';
    }

    /**
     * Returns the description
     *
     * @param string $lang
     *
     * @return string
     */
    public function getDescription($lang = null)
    {
        if ($lang == null) {
            $lang = Application::getInstance()->getLanguageCode();
        }
        $attr = "description_{$lang}";

        return $this->$attr;
    }

    /**
     * Returns the name
     *
     * @param bool $add_zero
     * @param bool $singular
     *
     * @return array
     */
    public static function getHtmlOptions($add_zero = false, $singular = true)
    {
        global $CONFIG;
        $cache = (extension_loaded('Memcached'))?new \Memcached():new \Memcache();
        $cache->addServer($CONFIG['MEMCACHED_SERVER'], 11211);

        $cachekey = "medical-specialty-html-options-sorted-".Application::getInstance()->getSourcePlatform();
        $html_options = $cache->get($cachekey);
        if($html_options === false) {

            $html_options = array();

            if ($add_zero) {
                $_array[0] = Application::getInstance()->_('...bitte wählen...');
            }

            $medical_specialty = new MedicalSpecialty();
            $medical_specialties = $medical_specialty->getMedicalSpecialties();

            if ($medical_specialties) {
                /** @var MedicalSpecialty[] $medical_specialties */
                foreach ($medical_specialties as $_medical_specialty) {
                    $html_options[$_medical_specialty->getId()] = $_medical_specialty->getName($singular);
                }
            }

            if($cache instanceof \Memcached) {
                $cache->set($cachekey, $html_options, 432000);
            } else {
                $cache->set($cachekey, $html_options, 0, 432000);
            }
        }

        return $html_options;
    }

    /**
     * Returns the medical_specialty_values
     *
     * @param bool $singular
     *
     * @return array
     */
    public function getMedicalSpecialtyValues($singular = true)
    {
        $attr = self::getLabelAttribute($singular);

        return $this->findValues('status=' . self::STATUS_ACTIVE . ' ORDER BY ' . $attr, $attr, 'id');
    }

    /**
     * Returns the MedicalSpecialty Array
     *
     * @param bool $singular
     *
     * @return MedicalSpecialty[]
     */
    public function getMedicalSpecialties($singular = true)
    {
        $attr = self::getLabelAttribute($singular);

        // Always put Zahnarzt first
        $where = 'status=' . self::STATUS_ACTIVE;
        $order_by = ' ORDER BY CASE WHEN id = 1 THEN 1 ELSE 2 END, ' . $attr . ' ASC';

        $event = Application::getInstance()->container->get('dispatcher')->dispatch('medical_specialty.query_where', new Event($where));
        $where = $event->data;
        
        return $this->findObjects($where . $order_by);
    }

    /**
     * @param bool $only_active
     * 
     * @return TreatmentType[]
     */
    public function getTreatmentTypes($only_active = true)
    {
        if (!$this->isValid()) {
            return array();
        }

        $tt = new TreatmentType();
        $where = 'medical_specialty_id = ' . $this->getId();
        
        if ($only_active) {
            $where .= ' AND status=' . TreatmentType::STATUS_ACTIVE;
        }
        
        return $tt->findObjects($where);
    }

    /**
     * Returns the javascript array code for the homepage selectbox
     *
     * @param bool $singular
     *
     * @return string
     */
    public static function getSelectLocationMedicalSpecialtyIdsJavascriptOptions($singular = true)
    {
        global $CONFIG;
        $app = Application::getInstance();
        $request = $app->getRequest();
        $cache = (extension_loaded('Memcached'))?new \Memcached():new \Memcache();
        $cache->addServer($CONFIG['MEMCACHED_SERVER'], 11211);

        $cachekey = "medical-specialty-js-options-sorted";
        $cities = $cache->get($cachekey);
        if($cities === false) {
            $db = $app->getSql();

            $cities = array();

            $where = sprintf(
                " WHERE %s.`status` = %d AND %s.`status` = %d  AND city IN (%s) ",
                DB_TABLENAME_LOCATIONS,
                Location::STATUS_VISIBLE_APPOINTMENTS,
                DB_TABLENAME_MEDICAL_SPECIALTIES,
                self::STATUS_ACTIVE,
                "'" . implode("','", $GLOBALS['CONFIG']['LOCATIONS_SELECT']) . "'"
            );

            $event = $app->container->get('dispatcher')->dispatch('medical_specialty.query_where', new Event($where));
            $where = $event->data;

            $sql = sprintf(
                "SELECT city, %s.`id`, %s.`%s` as `med_spec_name` " .
                "FROM %s " .
                "LEFT JOIN %s ON FIND_IN_SET(%s.id, %s.medical_specialty_ids) " .
                $where .
                "GROUP BY `city`, %s.`id` " .
                "ORDER BY CASE WHEN %s.`id` = 1 THEN 1 ELSE 2 END, med_spec_name ASC, city;",
                DB_TABLENAME_MEDICAL_SPECIALTIES,
                DB_TABLENAME_MEDICAL_SPECIALTIES,
                self::getLabelAttribute($singular),
                DB_TABLENAME_LOCATIONS,
                DB_TABLENAME_MEDICAL_SPECIALTIES,
                DB_TABLENAME_MEDICAL_SPECIALTIES,
                DB_TABLENAME_LOCATIONS,
                DB_TABLENAME_MEDICAL_SPECIALTIES,
                DB_TABLENAME_MEDICAL_SPECIALTIES
            );

            $db->query($sql);

            if ($db->nf() > 0) {
                while ($db->next_record()) {
                    // YES these key/value pairs are backwards. Chrome sorts JS objects if the key is numeric. HURRR
                    $cities[$db->f('city')][$db->f('med_spec_name')] = $db->f('id');
                }
            }

            if($cache instanceof \Memcached) {
                $cache->set($cachekey, $cities, 432000);
            } else {
                $cache->set($cachekey, $cities, 0, 432000);
            }
        }


        
        if ($request->isXmlHttpRequest()) {
            $request->json($cities);
        }

        return $cities;
    }
}
