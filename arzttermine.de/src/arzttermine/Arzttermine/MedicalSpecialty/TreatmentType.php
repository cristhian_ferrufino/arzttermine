<?php

namespace Arzttermine\MedicalSpecialty;

use Arzttermine\Application\Application;
use Arzttermine\Core\Basic;
use Arzttermine\User\User;
use NGS\Managers\DoctorsManager;

class TreatmentType extends Basic {

    const STATUS_DRAFT = 0;

    const STATUS_ACTIVE = 1;

    /**
     * @var string
     */
    protected $tablename = DB_TABLENAME_TREATMENTTYPES;

    /**
     * @var string
     */
    protected $slug;

    /**
     * @var int
     */
    protected $status;

    /**
     * @var int
     */
    protected $medical_specialty_id;

    /**
     * @var string
     */
    protected $name_de;

    /**
     * @var string
     */
    protected $name_en;

    /**
     * @var string
     */
    protected $name_es;

    /**
     * @var string
     */
    protected $created_at;

    /**
     * @var string
     */
    protected $created_by;

    /**
     * @var string
     */
    protected $updated_at;

    /**
     * @var string
     */
    protected $updated_by;

    /**
     * @var array
     */
    protected $all_keys
        = array(
            "id",
            "slug",
            "status",
            "medical_specialty_id",
            "name_de",
            "name_en",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by"
        );

    /**
     * @var array
     */
    protected $secure_keys
        = array(
            "id",
            "slug",
            "status",
            "medical_specialty_id",
            "name_de",
            "name_en",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by"
        );

    /**
     * @return array
     */
    public function toArray()
    {
        return array(
            'id'   => $this->getId(),
            'name' => $this->getName(),
            'slug' => $this->getSlug(),
            'medical_specialty_id' => $this->getMedicalSpecialtyId(),
        );
    }
    
    /**
     * @return string
     */
    public function getSlug()
    {
        return trim($this->slug);
    }
    
    /**
     * Returns the status id
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns an array with all treatmenttypes for one medical specialty
     *
     * @param User $user
     * @param int $medical_specialty_id
     * @param bool $add_zero
     *
     * @return array
     */
    public function getHtmlOptionsForMedicalSpecialty(User $user, $medical_specialty_id, $add_zero = false)
    {
        $app = Application::getInstance();

        $name = 'name_' . $app->getLanguageCode();
        $tt = new TreatmentType();

        $result = $tt->findValues(
                     'status=' . self::STATUS_ACTIVE . '
				AND
			medical_specialty_id=' . $medical_specialty_id . '
			AND id IN (' . implode(',', $user->getTreatmentTypeIds()) . ')
			ORDER BY ' . $name . ' ASC', $name, 'id'
        );

        if ($add_zero) {
            $result[0] = $app->_('...bitte wählen...');
        }

        return $result;
    }

    /**
     * Returns an html select options with all treatmenttypes for some medical specialties
     *
     * @param User $user
     * @param MedicalSpecialty[] $medical_specialties
     * @param bool $add_zero
     *
     * @return array array('Zahnarzt'=>array(123=>'x',456=>'y'), 'Allgemeinarzt'=>array(...))
     */
    public function getHtmlOptionsForMedicalSpecialties(User $user, array $medical_specialties, $add_zero = false)
    {
        if (!is_array($medical_specialties)) {
            return array();
        }

        $result = array();
        if ($add_zero) {
            $result[0] = Application::getInstance()->_('...bitte wählen...');
        }

        /** @var MedicalSpecialty[] $medical_specialties */
        foreach ($medical_specialties as $medical_specialty) {
            $result[$medical_specialty->getName()] = $this->getHtmlOptionsForMedicalSpecialty($user, $medical_specialty->id, false);
        }

        return $result;
    }

    /**
     * Returns an html select options with all treatmenttypes for given treatmenttypes
     *
     * @param User $user
     * @param TreatmentType[] $treatmenttypes
     * @param bool $add_zero
     *
     * @return array array('Zahnarzt'=>array(123=>'x',456=>'y'), 'Allgemeinarzt'=>array(...))
     */
    public function getHtmlOptionsByTreatmenttypes(User $user, array $treatmenttypes, $add_zero = false)
    {
        if (!is_array($treatmenttypes)) {
            return array();
        }

        $result = array();
        if ($add_zero) {
            $result[0] = Application::getInstance()->_('...bitte wählen...');
        }

        /** @var Treatmenttype[] $treatmenttypes */
        foreach ($treatmenttypes as $treatmenttype) {
            if (!$treatmenttype->isValid() || !in_array($treatmenttype->id, $user->getTreatmentTypeIds())) continue;
            $medicalspecialtyName = $treatmenttype->getMedicalSpecialty()->getName();
            $result[$medicalspecialtyName][$treatmenttype->getId()] = $treatmenttype->getName();
        }
        return $result;
    }

    /**
     * Returns the name
     *
     * @param int $id treatmenttype_id
     *
     * @return string
     */
    public static function getNameStatic($id)
    {
        $tt = new TreatmentType($id);

        if ($tt->isValid()) {
            return $tt->getName();
        }

        return '';
    }

    /**
     * Checks if a treatmenttype exists
     *
     * @param int $id treatmenttype_id
     *
     * @return bool
     */
    public static function idExists($id)
    {
        if (!is_numeric($id) || $id < 1) {
            return false;
        }

        $tt = new TreatmentType($id);

        return ($tt->isValid());
    }

    /**
     * @param int $user_id
     * @param int $location_id
     * @param string $datetime (YYYY-mm-dd HH:ii:ss)
     *
     * @return array
     */
    public static function getTreatmentTypesForGivenTime($user_id, $location_id, $datetime)
    {
        return DoctorsManager::getInstance()->getTreatmentTypesForGivenTime($user_id, $location_id, $datetime);
    }

    /**
     * Returns the treatmenttype id for a match of the treatmenttype name_de AND the medical specialty sigular name_de
     *
     * @param string $medicalSpecialtyNameSingularDe
     * @param string $treatmentTypeNameDe
     *
     * @return int|null
     */
    public static function getIdByMedicalSpecialtyNameSingularDeAndTreatmenttypeNameDe($medicalSpecialtyNameSingularDe, $treatmentTypeNameDe)
    {
        $medicalSpecialty = new MedicalSpecialty();
        /**
         * @var MedicalSpecialty $medicalSpecialty
         */
        $medicalSpecialty = $medicalSpecialty->findObject('name_singular_de="'.sqlsave($medicalSpecialtyNameSingularDe).'"');
        if (!$medicalSpecialty->isValid()) {
            return null;
        }

        $treatmentType = new TreatmentType();
        /**
         * @var TreatmentType $treatmentType
         */
        $treatmentType = $treatmentType->findObject('medical_specialty_id='.$medicalSpecialty->getId().' AND name_de="'.sqlsave($treatmentTypeNameDe).'"');
        if (!$treatmentType->isValid()) {
            return null;
        }
        return $treatmentType->getId();
    }

    /**
     * @return int
     */
    public function getMedicalSpecialtyId()
    {
        return intval($this->medical_specialty_id);
    }

    /**
     * @param int $medical_specialty_id
     *
     * @return self
     */
    public function setMedicalSpecialtyId($medical_specialty_id)
    {
        $this->medical_specialty_id = intval($medical_specialty_id);

        return $this;
    }

    /**
     * Returns the name as singular or plural in the current language
     *
     * @param bool $singular
     *
     * @return string
     */
    public function getName($singular = true)
    {
        $attr = self::getLabelAttribute($singular);

        if (isset($this->$attr)) {
            return $this->$attr;
        }

        return '';
    }

    /**
     * Get the string that determines labels
     *
     * @return string
     */
    protected static function getLabelAttribute()
    {
        return 'name_' . Application::getInstance()->getLanguageCode();
    }

    /**
     * @return MedicalSpecialty
     */
    public function getMedicalSpecialty()
    {
        $ms = new MedicalSpecialty($this->getMedicalSpecialtyId());
        
        return $ms;
    }

    /**
     * Take the ID from either an object or as-is 
     * 
     * @param int|MedicalSpecialty $medical_specialty
     *
     * @return self
     */
    public function setMedicalSpecialty($medical_specialty)
    {
        if ($medical_specialty instanceof MedicalSpecialty) {
            $medical_specialty = $medical_specialty->getId();
        }
        
        $this->setMedicalSpecialtyId($medical_specialty);
        
        return $this;
    }

}
