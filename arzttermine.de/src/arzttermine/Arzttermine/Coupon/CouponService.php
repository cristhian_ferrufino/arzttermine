<?php

namespace Arzttermine\Coupon;

use Arzttermine\Application\Application;
use Arzttermine\Booking\Booking;
use Arzttermine\Event\Event;
use Arzttermine\Referral\ReferralInvitation;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CouponService {

    /**
     * @var Coupon
     */
    protected $coupon;

    /**
     * @var int
     */
    protected $source_platform = ReferralInvitation::SOURCE_EMAIL;

    /**
     * @var ContainerInterface $container
     */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        
        // Initialize a default (empty) coupon
        $this->coupon = new Coupon();
    }

    /**
     * @param int $source_platform
     *
     * @return self
     */
    public function setSourcePlatform($source_platform)
    {
        if (ReferralInvitation::isSourceValid($source_platform)) {
            $this->source_platform = $source_platform;
        } else {
            $this->source_platform = ReferralInvitation::SOURCE_EMAIL;
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getSourcePlatform()
    {
        return $this->source_platform;
    }

    /**
     * Lets us know if there is a coupon waiting for us
     * 
     * @return bool
     */
    public function hasCoupon()
    {
        return !empty($this->coupon) && $this->coupon->isValid();
    }

    /**
     * Generate a basic coupon code. This cannot be used by itself
     * 
     * @param int $length
     * 
     * @return string
     */
    public function generateCouponCode($length = 4)
    {
        return substr(str_shuffle('0123456789bcdfghjklmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ'), 0, $length);
    }

    /**
     * Forget whatever coupon code we were remembering
     * 
     * @return self
     */
    public function forget()
    {
        $this->container->get('session')->remove('coupon_code');
        
        return $this;
    }

    /**
     * Remember a referral code for later
     * 
     * @param string $referral_code
     * 
     * @return self
     */
    public function remember($referral_code = '')
    {
        $this->container->get('session')->set('coupon_code', $referral_code);
        
        return $this;
    }

    /**
     * Attempt to load the referral from multiple sources
     */
    public function findCoupon()
    {
        // We do these both. The query string (if exists) overrides anything in the session.
        $this->loadFromRequest();
        
        if (!$this->coupon instanceof Coupon || !$this->coupon->isValid()) {
            $this->loadFromSession();
        }
    }

    /**
     * @param string $coupon_code
     *
     * @return self
     */
    public function retrieveCoupon($coupon_code)
    {
        $coupon = new Coupon($coupon_code, 'code');
        if ($coupon->isValid() && $coupon->isActive()) {
            $this->coupon = $coupon;
            $this->remember($coupon_code);
        }
        
        return $this;
    }

    /**
     * POST['referral_code'] or GET['rc'], in that order
     * 
     * @return self
     */
    public function loadFromRequest()
    {
        $request = $this->container->get('request');

        $referral_code = (isset($request->getParam('form')['referral_code']))?$request->getParam('form')['referral_code']:'';
        if($referral_code != '') {
            $coupon_code = $referral_code;
        } else {
            $coupon_code = $request->getQuery('rc');
        }

        if ($coupon_code != '') {
            $this->retrieveCoupon($coupon_code);
        }
        
        return $this;
    }

    /**
     * @return self
     */
    public function loadFromSession()
    {
        $session = $this->container->get('session');
        
        if ($coupon_code = $session->get('coupon_code')) {
            $this->retrieveCoupon($coupon_code);
        }
        
        return $this;
    }

    /**
     * @return Coupon
     */
    public function getCoupon()
    {
        return $this->coupon;
    }

    /**
     * @return bool
     */
    public function hasValidCouponType()
    {
        return array_key_exists($this->getCoupon()->getOfferType(), $GLOBALS['CONFIG']['REFERRAL_PRICE_TYPES']);
    }

    /**
     * Mark the referral as invalid, and optionally tell the user
     */
    public function markAsInvalid($with_message = true)
    {
        $app = Application::getInstance();
        $view = $app->getView();
        
        if ($with_message) {
            $view->setRef('referral_error',
                $app->_(
                    'Der von Ihnen genutzte Code oder Link ist leider nicht mehr aktuell. ' .
                    'Bitte versuchen Sie es erneut oder kontaktieren Sie unseren Kundenservice für weitere Fragen: 0800 / 2222 133'
                )
            );
        }
        
        $this->forget();
    }

    /**
     * @return array
     */
    public function getPricingInfo()
    {
        $pricing_info = array();
        
        if ($this->hasValidCouponType()) {
            $pricing_info = $GLOBALS['CONFIG']['REFERRAL_PRICE_TYPES'][$this->getCoupon()->getOfferType()];
        }
        
        return $pricing_info;
    }

    /**
     * The full process. Find a referral, attach it to the view, and handle everything
     * 
     * @param string $page The "tooltip text" that describes the page we're on
     */
    public function enable($page = '')
    {
        $app = Application::getInstance();
        $request = $this->container->get('request');
        $session = $this->container->get('session');
        $view = $app->getView();

        $this->findCoupon();

        // @todo Remove all references to View here and move to Event Listeners
        if ($this->getCoupon()->isValid()) {
            if ($this->hasValidCouponType()) {
                if ($pricing_info = $this->getPricingInfo()) {
                    $view->setRefs(array(
                            'referral_code'     => $this->getCoupon()->getCode(),
                            'referral_price'    => $pricing_info['price'],
                            'referral_company'  => $pricing_info['company'],
                            'referral_company_image_name' => $pricing_info['logo']
                        ));
                }

                // If either sp or source_platform is set, use it as the referral source platform
                if ($source_platform_referrer = $request->getQuery('sp', $request->getQuery('source_platform'))) {
                    $this->setSourcePlatform($source_platform_referrer);
                    $session->set('referral_source_platform', $this->getSourcePlatform());
                }
            } else {
                $this->markAsInvalid();
            }
            
            $view
                ->setRef('patientRefCode', $this->getCoupon()->getType() === Coupon::TYPE_USER)
                ->setRef('tooltip_page', $page);
        }
        
        $event = $this->container->get('dispatcher')->dispatch('referral_service.enable', new Event($this->getCoupon()));
        $this->coupon = $event->data;
    }

    /**
     * Add the referral to the booking, and forget about it
     * 
     * @param Booking $booking
     */
    public function markAsUsed(Booking $booking)
    {
        if (!$this->getCoupon()->isValid()) {
            return;
        }
        
        $used_coupon = new UsedCoupon();
        $used_coupon
            ->setCouponId($this->getCoupon()->getId())
            ->setBookingId($booking->getId())
            ->setMedicalSpecialtyId($booking->getMedicalSpecialtyId())
            ->setEmail($booking->getEmail());

        $used_coupon->saveNewObject();
        
        $this->forget();
    }
}