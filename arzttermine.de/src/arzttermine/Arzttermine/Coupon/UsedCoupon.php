<?php

namespace Arzttermine\Coupon;

use Arzttermine\Booking\Booking;
use Arzttermine\Core\Basic;
use Arzttermine\MedicalSpecialty\MedicalSpecialty;
use Arzttermine\User\User;

class UsedCoupon extends Basic {

    const STATUS_NEW = 0;
    const STATUS_IN_PROCESSING = 1;
    const STATUS_APPOINTMENT_COMPLETION_FAILED = 2;
    const STATUS_APPOINTMENT_COMPLETION_UNABLE = 3;
    const STATUS_APPOINTMENT_COMPLETION_PROBLEM = 4;
    const STATUS_APPOINTMENT_COMPLETED = 5;
    const STATUS_REWARD_SENT = 6;
    const STATUS_REWARD_DENIED = 7;

    // @todo Remove these from here
    public static $STATUS_TEXTS = array(
        self::STATUS_NEW                            => 'New',
        self::STATUS_IN_PROCESSING                  => 'Processing',
        self::STATUS_APPOINTMENT_COMPLETION_FAILED  => 'Failed',
        self::STATUS_APPOINTMENT_COMPLETION_UNABLE  => 'Unable',
        self::STATUS_APPOINTMENT_COMPLETION_PROBLEM => 'Problem',
        self::STATUS_APPOINTMENT_COMPLETED          => 'Complete',
        self::STATUS_REWARD_SENT                    => 'Reward sent',
        self::STATUS_REWARD_DENIED                  => 'Reward denied'
    );

    /**
     * @var string
     */
    protected $tablename = 'bookings_coupons';

    /**
     * @var string
     */
    protected $email;

    /**
     * @var int
     */
    protected $booking_id;

    /**
     * @var int
     */
    protected $coupon_id;

    /**
     * @var int
     */
    protected $medical_specialty_id;

    /**
     * @var int
     */
    protected $status = self::STATUS_NEW;

    /**
     * @var array
     */
    protected $all_keys
        = array(
            'email',
            'booking_id',
            'coupon_id',
            'medical_specialty_id',
            'status'
        );

    /**
     * @var array
     */
    protected $secure_keys
        = array(
            'email'
        );

    /**
     * @param string $email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return new User($this->getEmail(), 'email');
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param int $booking_id
     *
     * @return self
     */
    public function setBookingId($booking_id)
    {
        $this->booking_id = $booking_id;

        return $this;
    }

    /**
     * @return Booking
     */
    public function getBooking()
    {
        return new Booking($this->getBookingId());
    }

    /**
     * @return int
     */
    public function getBookingId()
    {
        return $this->booking_id;
    }

    /**
     * @param int $coupon_id
     *
     * @return self
     */
    public function setCouponId($coupon_id)
    {
        $this->coupon_id = $coupon_id;

        return $this;
    }

    /**
     * @return int
     */
    public function getCouponId()
    {
        return $this->coupon_id;
    }

    /**
     * @return string
     */
    public function getCouponCode()
    {
        $couponCode = null;
        // the code is stored in the coupon
        $coupon = new Coupon($this->getCouponId(), 'id');
        if ($coupon->isValid()) {
            $couponCode = $coupon->getCode();
        }
        return $couponCode;
    }

    /**
     * @param int $medical_specialty_id
     *
     * @return self
     */
    public function setMedicalSpecialtyId($medical_specialty_id)
    {
        $this->medical_specialty_id = intval($medical_specialty_id);

        return $this;
    }

    /**
     * @return MedicalSpecialty
     */
    public function getMedicalSpecialty()
    {
        return new MedicalSpecialty($this->getMedicalSpecialtyId());
    }

    /**
     * @return int
     */
    public function getMedicalSpecialtyId()
    {
        return $this->medical_specialty_id;
    }

    /**
     * @param int $status
     *
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }
}