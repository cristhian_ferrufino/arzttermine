<?php

namespace Arzttermine\Coupon;

use Arzttermine\DependencyInjection\ServiceProviderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CouponServiceProvider implements ServiceProviderInterface {

    /**
     * {@inheritdoc}
     */
    public function register(ContainerInterface $container)
    {
        $coupon_service = new CouponService($container);
        $container->set('coupon_service', $coupon_service);
    }

    /**
     * {@inheritdoc}
     */
    public function boot(ContainerInterface $container)
    {
    }

}