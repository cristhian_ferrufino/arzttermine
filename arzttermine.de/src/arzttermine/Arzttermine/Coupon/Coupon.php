<?php

namespace Arzttermine\Coupon;

use Arzttermine\Application\Application;
use Arzttermine\Core\Basic;
use Arzttermine\User\User;

class Coupon extends Basic {
    
    const TYPE_SYSTEM = 0;
    const TYPE_USER = 1;

    /**
     * @var string
     */
    protected $tablename = 'coupons';

    /**
     * @var string
     */
    protected $code;

    /**
     * See $CONFIG for REFERRAL_PRICE_TYPES
     * 
     * @var string
     */
    protected $offer_type;

    /**
     * @var string
     */
    protected $note;

    /**
     * JSON encoded string
     *
     * @var string
     */
    protected $restrictions = '[]';

    /**
     * @var int
     */
    protected $type = self::TYPE_SYSTEM;

    /**
     * @var int
     */
    protected $active = 0;

    /**
     * Email of the user that created the referral code
     * 
     * @var string
     */
    protected $created_by;

    /**
     * DateTime when the code was created
     *
     * @var string
     */
    protected $created_at;

    /**
     * Email of the user that updated the referral code
     *
     * @var string
     */
    protected $updated_by;

    /**
     * DateTime when the code was updated
     *
     * @var string
     */
    protected $updated_at;

    /**
     * @var array
     */
    protected $all_keys = array(
        'id',
        'code',
        'active',
        'offer_type',
        'note',
        'restrictions',
        'type',
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    );

    /**
     * @var array
     */
    protected $secure_keys = array(
        'code',
        'active',
        'offer_type',
        'note',
        'restrictions',
        'created_by'
    );

    /**
     * @var array
     */
    protected $last_error = array();

    /**
     * @param int $type
     *
     * @return self
     */
    public function setType($type)
    {
        $this->type = intval($type);

        return $this;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param array $restrictions
     *
     * @return self
     */
    public function setRestrictions(array $restrictions)
    {
        $this->restrictions = json_encode($restrictions);

        return $this;
    }

    /**
     * @return string
     */
    public function getRestrictions()
    {
        return json_decode($this->restrictions, true);
    }

    /**
     * @param string $code
     *
     * @return self
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $offer_type
     *
     * @return self
     */
    public function setOfferType($offer_type)
    {
        $this->offer_type = $offer_type;

        return $this;
    }

    /**
     * @return string
     */
    public function getOfferType()
    {
        return $this->offer_type;
    }

    /**
     * @param string $note
     *
     * @return self
     */
    public function setNote($note)
    {
        $this->note = trim($note);

        return $this;
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param bool $active
     *
     * @return self
     */
    public function setActive($active)
    {
        $this->active = intval($active);

        return $this;
    }

    /**
     * @return int
     */
    public function isActive()
    {
        return (bool)$this->active;
    }

    /**
     * @param string $created_by
     *
     * @return self
     */
    public function setCreatedBy($created_by)
    {
        $this->created_by = $created_by;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * @param string $created_at
     *
     * @return self
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param string $updated_by
     *
     * @return self
     */
    public function setUpdatedBy($updated_by)
    {
        $this->updated_by = $updated_by;

        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedBy()
    {
        return $this->updated_by;
    }

    /**
     * @param string $updated_at
     *
     * @return self
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @return array
     */
    public function getLastError()
    {
        return $this->last_error;
    }

    /**
     * Does the referral apply?
     *
     * @param int $user_id
     * @param int $location_id
     * @param int $medical_specialty_id
     * @param string $email
     *
     * @return array
     */
    public function isApplicable($user_id, $location_id, $medical_specialty_id = 0, $email = '')
    {
        $app = Application::getInstance();
        $restrictions = $this->getRestrictions();

        // Stub to generate consistent return values
        $set_error = function($slug) use ($app, $restrictions) {
            return array(
                'error_field_name' => $slug,
                'data'             => !empty($restrictions[$slug]) ? $restrictions[$slug] : '[None]',
                'message'          => $app->static_text($slug)
            );
        };

        // Checking date to be between restricted dates
        if (!empty($restrictions['accessible_from']) || !empty($restrictions['accessible_until'])) {
            $current_time = date('Y-m-d H:i:s', time());

            if (!empty($restrictions['accessible_from']) && $current_time < $restrictions['accessible_from']) {
                $this->last_error = $set_error('accessible_from');
                return false;
            }
            if (!empty($restrictions['accessible_until']) && $current_time > $restrictions['accessible_until']) {
                $this->last_error = $set_error('accessible_until');
                return false;
            }
        }

        // Checking location restriction 
        if (!empty($restrictions['non_accessible_location_ids'])) {
            if (in_array($location_id, explode(',', $restrictions['non_accessible_location_ids']))) {
                $this->last_error = $set_error('non_accessible_location_ids');
                return false;
            }
        }

        // Checking user restriction
        if (!empty($restrictions['non_accessible_user_ids'])) {
            if (in_array($user_id, explode(',', $restrictions['non_accessible_user_ids']))) {
                $this->last_error = $set_error('non_accessible_user_ids');
                return false;
            }
        }
        
        if (!empty($email) && strtolower(trim($email)) === strtolower($this->getCreatedBy())) {
            $this->last_error = $set_error('referral_owner_creator_error');
            return false;
        }

        // Checking medical specialty restriction
        if (!empty($restrictions['accessible_medical_specialty_ids'])) {
            if ($restrictions['accessible_medical_specialty_ids'] != 'all' && !in_array($medical_specialty_id, explode(',', $restrictions['accessible_medical_specialty_ids']))) {
                $this->last_error = $set_error('accessible_medical_specialty_ids');
                return false;
            }
        }

        if (!empty($email)) {
            if ($this->isUsed($email, $medical_specialty_id)) {
                $this->last_error = $set_error('referral_exists_medical_specialty');
                return false;
            }
        }

        // Checking mobile site restriction 
        if ($app->deviceIsMobile() &&
            isset($restrictions['accessible_mobile']) &&
            !(intval($restrictions['accessible_medical_specialty_ids']) === 1)
        ) {
            $this->last_error = $set_error('accessible_mobile');
            return false;
        }

        // Checking user has contract restriction
        $user = new User($user_id);
        if (isset($restrictions['accessible_has_no_contract']) &&
            !$user->hasContract() &&
            intval($restrictions['accessible_has_no_contract']) !== 1
        ) {
            $this->last_error = $set_error('accessible_has_no_contract');
            return false;
        }
        
        if (
            isset($restrictions['accessible_has_contract']) &&
            $user->hasContract() &&
            intval($restrictions['accessible_has_contract']) !== 1
        ) {
            $this->last_error = $set_error('accessible_has_contract');
            return false;
        }

        return true;
    }

    /**
     * Check if a coupon was used by seeing if there's a used coupon entry
     * 
     * @param string $email
     * @param int $medical_specialty_id
     *
     * @return bool
     */
    public function isUsed($email, $medical_specialty_id = 0)
    {
        $used_coupon = new UsedCoupon();
        $email = filter_var($email, FILTER_SANITIZE_EMAIL); // @todo Escape

        $query = "email = '{$email}' AND coupon_id = {$this->getId()}";

        if (!empty($medical_specialty_id) && is_int($medical_specialty_id)) {
            $query .= " AND medical_specialty_id = {$medical_specialty_id}";
        }
        
        if ($used_coupon->count($query) > 0) {
            return true;
        }

        return false;
    }
    
}