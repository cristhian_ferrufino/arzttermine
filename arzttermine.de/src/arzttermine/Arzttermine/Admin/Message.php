<?php

namespace Arzttermine\Admin;

use Arzttermine\Application\Application;
use Arzttermine\Core\Basic;
use Arzttermine\User\User;

/**
 * AdminMessage Class
 *
 * @author Axel Kuzmik <axel@kuzmik.de>
 */
class Message extends Basic {

    /**
     * @var string
     */
    protected $tablename = DB_TABLENAME_ADMIN_MESSAGES;

    /**
     * @var string
     */
    protected $message;

    /**
     * @var string
     */
    protected $created_at;

    /**
     * @var string
     */
    protected $created_by;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var string
     */
    protected $author = '';

    /**
     * @var array
     */
    protected $all_keys
        = array(
            "id",
            "message",
            "created_at",
            "created_by"
        );

    /**
     * @var array
     */
    protected $secure_keys
        = array(
            "id",
            "message",
            "created_at",
            "created_by"
        );

    /**
     * Calc some vars
     *
     * @return bool
     */
    public function calcVars()
    {
        $this->url = "/adminmessages/{$this->getId()}";

        $user = new User($this->created_by);

        if ($user->isValid()) {
            $this->author = $user->getFullName();
        } else {
            $this->author = '&gt;non valid user&lt;';
        }

        return true;
    }

    /**
     * Returns all Version data for forms
     *
     * @return string
     */
    public function getFormData()
    {
        $data = array();

        foreach ($this->all_keys as $key) {
            $data[$key] = $this->$key;
        }

        return $data;
    }

    /**
     * Renders a excerpt for this blogpost for html output
     *
     * @return string
     */
    public function getMessage()
    {
        return do_shortcode(wpautop($this->message));
    }

    /**
     * Renders a excerpt for this blogpost for html output
     *
     * @param int $excerpt_length
     *
     * @return string
     */
    public function getExcerpt($excerpt_length = 55)
    {
        return getExcerpt($this->message, $excerpt_length);
    }

    /**
     * Returns the url to this blogpost
     *
     * @param bool $absolute_url
     *
     * @return string a href
     */
    public function getUrl($absolute_url = false)
    {
        if ($absolute_url) {
            $_host = $GLOBALS["CONFIG"]["URL_HTTP_LIVE"];
        } else {
            $_host = '';
        }

        if (isset($this->url)) {
            return $_host . $this->url;
        }

        return '';
    }

    /**
     * Returns the author for this blogpost
     *
     * @return string a href
     */
    public function getAuthor()
    {
        if (isset($this->author)) {
            return $this->author;
        }

        return '';
    }

    /**
     * Returns the last change date or the create date as default
     *
     * @return string
     */
    public function getUpdatedAt()
    {
        if (isset($this->updated_at) && $this->updated_at != '0000-00-00 00:00:00') {
            return $this->updated_at;
        } else {
            return $this->created_at;
        }
    }

    /**
     * Returns the human time diff until the blogpost will be published
     * Will return '' if the blogpost is allready published
     *
     * @return string human time diff
     */
    public function getCreatedAtHumanTimeDiff()
    {
        return human_time_diff(mysql2date('U', $this->created_at), Application::getInstance()->now('timestamp'));
    }
}