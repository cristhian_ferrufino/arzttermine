<?php

namespace Arzttermine\Admin;

interface AdminModuleInterface {

    /**
     * Load configuration
     */
    public function boot();

    /**
     * Render a string of HTML for the frontend
     * 
     * @todo Replace with templating
     * 
     * @return \Symfony\Component\HttpFoundation\Response|string
     */
    public function render();

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @param string $role
     * @return bool
     */
    public function checkPrivilege($role = '');

    /**
     * @param Admin $admin
     */
    public function setAdmin(Admin $admin);
    
}