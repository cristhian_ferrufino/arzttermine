<?php

namespace Arzttermine\Admin\Module;

use Arzttermine\Admin\Configuration;
use Arzttermine\Admin\Module;
use Arzttermine\Core\DateTime;
use Arzttermine\User\User;

final class Media extends Module {

    /**
     * @var string
     */
    protected $title = 'Media Admin';

    /**
     * @var string
     */
    protected $role = 'media_admin';

    /**
     * @var string
     */
    protected $slug = 'media';

    /**
     * {@inheritdoc}
     */
    public function boot()
    {
        $MENU_ARRAY = array(
            "id"                           => array("sql" => 1, "name" => "ID", "show" => 0, "show_iframe" => 0, "edit" => 0, "new" => 0, "search" => 0, "sort" => 0, "nowrap" => 0),
            "filename"                     => array("sql" => 1, "name" => "Filename", "show" => 1, "show_iframe" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1),
            "category"                     => array("sql" => 1, "name" => "Verzeichnis", "show" => 1, "show_iframe" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "list_category", "para" => true), "form_input" => array("type" => "select", "values" => $this->form_category())),
            "filesize"                     => array("sql" => 1, "name" => "Dateigröße", "show" => 1, "show_iframe" => 1, "edit" => 0, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "list_size", "para" => true)),
            "width"                        => array("sql" => 1, "name" => "Breite", "show" => 1, "show_iframe" => 1, "edit" => 0, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1),
            "height"                       => array("sql" => 1, "name" => "Höhe", "show" => 1, "show_iframe" => 1, "edit" => 0, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1),
            "label"                        => array("sql" => 1, "name" => "Caption", "show" => 1, "show_iframe" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "description"                  => array("sql" => 1, "name" => "Description", "show" => 1, "show_iframe" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "tags"                         => array("sql" => 1, "name" => "Tags", "show" => 1, "show_iframe" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "mimetype"                     => array("sql" => 1, "name" => "MimeType", "show" => 1, "show_iframe" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "created_at"                   => array("sql" => 1, "name" => "Erstellungsdatum", "show" => 0, "show_iframe" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "created_by"                   => array("sql" => 1, "name" => "Ersteller", "show" => 0, "show_iframe" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "form_created_by", "para" => true)),
            "updated_at"                   => array("sql" => 1, "name" => "Letztes Update", "show" => 1, "show_iframe" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "updated_by"                   => array("sql" => 1, "name" => "Letzter Bearbeiter", "show" => 1, "show_iframe" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "form_updated_by", "para" => true), "list_function" => array("name" => "form_updated_by", "para" => true)),
            "file_upload"                  => array("sql" => 0, "name" => "Dateiupload", "show" => 0, "show_iframe" => 0, "edit" => 1, "new" => 1, "search" => 0, "sort" => 0, "nowrap" => 0, "form_input" => array("type" => "file")),
            "tip_upload_max_filesize_text" => array("sql" => 1, "name" => '<p>Medien können max. ' . ini_get("upload_max_filesize") . ' groß sein.</p>', "show" => 0, "show_iframe" => 0, "edit" => 1, "new" => 1, "search" => 0, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "none")),
        );
        
        $BUTTON = array(
            "new"    => array("func" => "media_admin_new", "gfx" => "document_new.png", "alt_text" => "Neuer Eintrag", "script" => $this->admin->getUrl(), "action" => "new", "th" => 1, "td" => 0),
            "edit"   => array("func" => "media_admin_edit", "gfx" => "document_edit.png", "alt_text" => "Bearbeiten", "script" => $this->admin->getUrl(), "action" => "edit", "th" => 0, "td" => 1),
            "delete" => array("func" => "media_admin_delete", "gfx" => "delete.png", "alt_text" => "Löschen", "script" => $this->admin->getUrl(), "action" => "delete", "th" => 0, "td" => 1),
            "search" => array("func" => "media_admin_search", "gfx" => "search.png", "alt_text" => "Suchen", "script" => $this->admin->getUrl(), "action" => "search", "th" => 1, "td" => 0),
        );
        
        $TABLE_CONFIG = array(
            "db_name"     => "admin",
            "table_name"  => DB_TABLENAME_MEDIAS,
            "alpha_index" => "filename",
        );
        
        $URL_PARA = array(
            "page"         => 1,
            "alpha"        => '',
            "sort"         => 'filename',
            "direction"    => 0,
            "num_per_page" => 25,
            "type"         => 'alpha',
        );

        $configuration = new Configuration();
        $configuration->menu    = $MENU_ARRAY;
        $configuration->buttons = $BUTTON;
        $configuration->table   = $TABLE_CONFIG;
        $configuration->default_parameters = $URL_PARA;

        $this->configuration = $configuration;
    }

    /**
     * {@inheritdoc}
     */
    public function render()
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $html = '';

        $action = getParam("action", 'list');
        $form_data = getParam("form_data");

        switch ($action) {
            case "edit":
                if (!$this->checkPrivilege('media_admin_edit')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                } else {
                    $id = getParam("id");
                    if (is_array($form_data)) {
                        $html = $this->change_one($action, $id, $form_data, $this->parameters);
                    } else {
                        $html = $this->form_change($action, $id, $this->parameters);
                    }
                }
                break;
            
            case "delete":
                if (!$this->checkPrivilege('media_admin_delete')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                } else {
                    if (!isset($_REQUEST["confirm_id"])) {
                        $id = getParam("id");
                        $html = $this->ask4delete($id, $this->parameters);
                    } else {
                        $confirm_id = getParam("confirm_id");
                        if ($this->delete_one($confirm_id)) {
                            $view->addMessage('STATUS', 'NOTICE', $translator->trans('the_entry_was_deleted_successfully'));
                        } else {
                            $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_could_not_be_deleted'));
                        }
                    }
                }
                break;
            
            case "search":
                if (!$this->checkPrivilege('media_admin_search')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                if (isset($_REQUEST["send_button"])) {
                    $this->parameters["page"] = 1;
                    $this->parameters["alpha"] = '';
                }
                $html = $this->show_search_form($action, $form_data, $this->parameters);
                if (is_array($form_data)) {
                    $html .= $this->get_list($this->parameters, $form_data);
                }
                break;
            
            case "new":
                if (!$this->checkPrivilege('media_admin_new')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                if (is_array($form_data)) {
                    $html = $this->new_one($action, $form_data, $this->parameters);
                } else {
                    $html = $this->form_new($action, $this->parameters);
                }
                break;
        }

        if (empty($html)) {
            if ($this->checkPrivilege('media_admin_view')) {
                $html = $this->get_list($this->parameters);
            }
        }

        return $html;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function list_category($data)
    {
        if (
            empty($data["category"]) ||
            empty($GLOBALS['CONFIG']['MEDIA_CATEGORIES'][$data["category"]]["name"])
        ) {
            return '';
        }

        return ($GLOBALS['CONFIG']['MEDIA_CATEGORIES'][$data["category"]]["name"]);
    }

    /**
     * @return array
     */
    public function form_category()
    {
        $media_categories = array();
        
        // @todo: Fix this seemingly meaningless map
        foreach ((array)$GLOBALS['CONFIG']['MEDIA_CATEGORIES'] as $key => $category) {
            $media_categories[$key] = $GLOBALS['CONFIG']['MEDIA_CATEGORIES'][$key]["name"];
        }

        return $media_categories;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function list_size($data)
    {
        $size = !empty($data["filesize"]) ? $data["filesize"] : 0;
        
        return formatBytes($size, 2);
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function form_updated_by($data)
    {
        $data['updated_by'] = !empty($data['updated_by']) ? intval($data['updated_by']) : 0;

        return (new User($data['updated_by']))->getFullName();
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function form_created_by($data)
    {
        $data['created_by'] = !empty($data['created_by']) ? intval($data['created_by']) : 0;

        return (new User($data['created_by']))->getFullName();
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function list_image($data)
    {
        /* get the template to display the image */
        $filename = explode(".", $data["filename"]);
        $ext = array_pop($filename);
        $tmp_webfilename = $GLOBALS['CONFIG']['MEDIA_CATEGORIES'][$data["category"]]["url_path"] . $data["filename"];

        switch ($ext) {
            case "swf":
                $HTML_IMAGE
                    = "
<script>
<!--
  if (document.all) { // internet explorer
    if (navigator.appVersion.indexOf(\"Mac\") == -1 && navigator.appVersion.indexOf(\"3.1\") == -1)
    { // IE auf PC und nicht unter Win3.1
      document.write('<OBJECT CLASSID=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" codebase=\"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=4,0,2,0\" WIDTH=\"" . $data["width"] . "\" HEIGHT=\"" . $data["height"] . "\" NAME=\"sw\" ID=\"sw\">');
      document.write('  <PARAM NAME=\"Movie\" VALUE=\"$tmp_webfilename\">');
      document.write('  <PARAM NAME=\"quality\" VALUE=\"high\">');
      if (navigator.mimeTypes && navigator.mimeTypes[\"application/x-shockwave-flash\"] && navigator.mimeTypes[\"application/x-shockwave-flash\"].enabledPlugin && navigator.plugins && navigator.plugins[\"Shockwave Flash\"])
      {}
      else { // Flash 2 oder kein Flash
        document.write('Sorry, you need the Flash-Plugin to see the Flashmovie!');
      }
      document.write('</OBJECT>');
    } else { // IE auf Mac oder Win3.1
      document.write('You need the Flash-Plugin to see the Flashmovie!');
    }
  } else {
      document.write('You need the Flash-Plugin to see the Flashmovie!');
  }
// -->
</script><noscript>You need the Flash-Plugin to see the Flashmovie!</noscript>
";
                break;
            case "mp3":
                $HTML_IMAGE = '<img src="' . getStaticUrl('adm/media_mimetype_mp3.png') . '" title="' . $data["filename"] . '" alt="' . $data["filename"] . '"></a>';
                break;
            case "mov":
            case "avi":
                $HTML_IMAGE = '<img src="' . getStaticUrl('adm/media_mimetype_avi.png') . '" title="' . $data["filename"] . '" alt="' . $data["filename"] . '"></a>';
                break;
            case "css":
                $HTML_IMAGE = '<img src="' . getStaticUrl('adm/media_mimetype_css.png') . '" title="' . $data["filename"] . '" alt="' . $data["filename"] . '"></a>';
                break;
            case "js":
                $HTML_IMAGE = '<img src="' . getStaticUrl('adm/media_mimetype_js.png') . '" title="' . $data["filename"] . '" alt="' . $data["filename"] . '"></a>';
                break;
            case "pdf":
                $HTML_IMAGE = '<img src="' . getStaticUrl('adm/media_mimetype_pdf.png') . '" title="' . $data["filename"] . '" alt="' . $data["filename"] . '"></a>';
                break;
            case "doc":
            case "docx":
                $HTML_IMAGE = '<img src="' . getStaticUrl('adm/media_mimetype_doc.png') . '" title="' . $data["filename"] . '" alt="' . $data["filename"] . '"></a>';
                break;
            case "ppt":
                $HTML_IMAGE = '<img src="' . getStaticUrl('adm/media_mimetype_ppt.png') . '" title="' . $data["filename"] . '" alt="' . $data["filename"] . '"></a>';
                break;
            case "xls":
            case "xlsx":
                $HTML_IMAGE = '<img src="' . getStaticUrl('adm/media_mimetype_xls.png') . '" title="' . $data["filename"] . '" alt="' . $data["filename"] . '"></a>';
                break;
            case "ico":
            case "gif":
            case "jpg":
            case "jpeg":
            case "png":
            default:
                $HTML_IMAGE = '<img src="' . $tmp_webfilename . '" title="' . $data["filename"] . '" alt="' . $data["filename"] . '"></a>';
                break;
        }

        return $HTML_IMAGE;
    }

    /**
     * @param array $URL_PARA
     * @param array $search
     *
     * @return string
     */
    public function get_list($URL_PARA, $search = array())
    {
        return $this->admin->admin_get_list_gfx(0, $search, $URL_PARA);
    }
    
    /**
     * @param string $action
     * @param array $form_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function new_one($action, $form_data, $URL_PARA)
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $profile = $this->container->get('current_user');
        $db = $this->container->get('database');
        $html = '';

        /* the name of the uploaded file */
        $form_data["filename"] = $_FILES["form_data"]["name"]["file_upload"];
        if ($form_data["filename"] == "") {
            $view->addMessage('STATUS', 'WARNING', $translator->trans('need_uploadfile'));
            return $this->form_new($action, $URL_PARA, $form_data);
        }
        /* check if the image has the correct fileextension and characters */
        if (!preg_match('/^[a-zA-Z\d_-]+\.(ico|gif|jpg|jpeg|png|swf|mp3|mov|avi|mpg|css|js|pdf|xls|xlsx|doc|docx|ppt)$/i', $form_data["filename"])) {
            $view->addMessage('STATUS', 'WARNING', $translator->trans('wrong_extension'));
            return $this->form_new($action, $URL_PARA, $form_data);
        }

        /* check if there is a file with that category and the same name */
        $db->query('SELECT * FROM ' . $this->configuration->table["table_name"] . ' WHERE category=' . $form_data["category"] . ' AND filename="' . $form_data["filename"] . '";');
        if ($db->num_rows() > 0) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('there_is_allready_a_file_in_this_category'));
            return $this->form_new($action, $URL_PARA, $form_data);
        }
        if (!$this->admin_save_file_fs($_FILES["form_data"]["tmp_name"]["file_upload"], $GLOBALS['CONFIG']['MEDIA_CATEGORIES'][$form_data["category"]]["file_path"] . $form_data["filename"])) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('could_not_save_the_media'));
            return $this->form_new($action, $URL_PARA, $form_data);
        }
        $form_data["tags"] = sanitize_tags($form_data["tags"]);
        $form_data["created_by"] = $profile->getId();
        $form_data["created_at"] = (new DateTime())->getDateTime();
        $form_data["updated_by"] = $profile->getId();
        $form_data["updated_at"] = (new DateTime())->getDateTime();
        $form_data["filesize"] = filesize($GLOBALS['CONFIG']['MEDIA_CATEGORIES'][$form_data["category"]]["file_path"] . $form_data["filename"]);

        /* try to get the size (supported by getimagesize()) */
        $img_array = @getimagesize($GLOBALS['CONFIG']['MEDIA_CATEGORIES'][$form_data["category"]]["file_path"] . $form_data["filename"]);
        if (is_array($img_array)) {
            $form_data["width"] = $img_array[0];
            $form_data["height"] = $img_array[1];
            $form_data["mimetype"] = $img_array['mime'];
        } else {
            $form_data["width"] = 0;
            $form_data["height"] = 0;
            $form_data["mimetype"] = '';
        }

        $_id = $this->admin->admin_set_new($form_data);
        if ($_id > 0) {
            $this->calc_url_para($URL_PARA, $form_data);
            $view->addMessage('STATUS', 'NOTICE', $translator->trans('action_was_successfully'));
        } else {
            if ($_id == -1) {
                $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_allready_exists'));
            } elseif ($_id == -2) {
                /* delete the file from the fs */
                $this->admin_delete_file_fs($GLOBALS['CONFIG']['MEDIA_CATEGORIES'][$form_data["category"]]["file_path"] . $form_data["filename"]);
                $view->addMessage('STATUS', 'ERROR', $translator->trans('name_of_primary_key_is_empty'));
            } else {
                /* delete the file from the fs */
                $this->admin_delete_file_fs($GLOBALS['CONFIG']['MEDIA_CATEGORIES'][$form_data["category"]]["file_path"] . $form_data["filename"]);
                $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_could_not_be_created'));
            }

            $html = $this->form_new($action, $URL_PARA, $form_data);
        }

        return $html;
    }

    /**
     * @param string $action
     * @param int $id
     * @param array $form_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function change_one($action, $id, $form_data, $URL_PARA)
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $profile = $this->container->get('current_user');
        $db = $this->container->get('database');
        $html = '';

        /* the name of the uploaded file */
        $form_data["upload_filename"] = $_FILES["form_data"]["name"]["file_upload"];

        /* check if there is a file with that category and the same name */
        /* if there is a new file than take that filename */
        if ($form_data["upload_filename"] != "") {
            $tmp_filename_check = $form_data["upload_filename"];
        } else {
            $tmp_filename_check = $form_data["filename"];
        }

        /* check if the image has the correct fileextension and characters */
        if (!preg_match('/^[a-zA-Z\d_-]+\.(ico|gif|jpg|jpeg|png|swf|mp3|mov|avi|mpg|css|js|pdf|xls|xlsx|doc|docx|ppt)$/i', $tmp_filename_check)) {
            $view->addMessage('STATUS', 'WARNING', $translator->trans('wrong_extension'));
            return $this->form_change($action, $id, $URL_PARA, $form_data);
        }

        $db->query('SELECT * FROM ' . $this->configuration->table["table_name"] . ' WHERE category=' . $form_data["category"] . ' AND filename="' . $tmp_filename_check . '" AND id!=' . $id . ';');
        if ($db->num_rows() > 0) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('there_is_allready_a_file_in_this_category'));
            return $this->form_change($action, $id, $URL_PARA, $form_data);
        }
        
        /* check if category, filename have changed or if there was an upload */
        $db->query('SELECT * FROM ' . $this->configuration->table["table_name"] . ' WHERE id=' . $id . ';');
        if ($db->num_rows() == 0) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('could_not_find_media'));
            return $this->form_change($action, $id, $URL_PARA, $form_data);
        }
        
        $db->next_record();
        $old_category = $db->f("category");
        $old_filename = $db->f("filename");
        /* if there is an upload than store the file ************** */
        if ($form_data["upload_filename"] != "") {
            /* this is the new filename */
            $form_data["filename"] = $form_data["upload_filename"];
            if (!$this->admin_save_file_fs($_FILES["form_data"]["tmp_name"]["file_upload"], $GLOBALS['CONFIG']['MEDIA_CATEGORIES'][$form_data["category"]]["file_path"] . $form_data["filename"])) {
                $view->addMessage('STATUS', 'ERROR', $translator->trans('could_not_save_the_media'));
                return $this->form_change($action, $id, $URL_PARA, $form_data);
            }
            /* if the old file has an another filename or resists in another category than delete it. */
            /* otherwise it was overwritten and we are not alowed to delete the file at this point */
            if (($old_category != $form_data["category"]) || ($old_filename != $form_data["filename"])) {
                /* delete the file from the fs */
                $this->admin_delete_file_fs($GLOBALS['CONFIG']['MEDIA_CATEGORIES'][$old_category]["file_path"] . $old_filename);
            }
            /* the category or the filename have changed ************** */
        } elseif (($old_category != $form_data["category"]) || ($old_filename != $form_data["filename"])) {
            /* save the old file under the new address and name */
            if (!$this->admin_save_file_fs($GLOBALS['CONFIG']['MEDIA_CATEGORIES'][$old_category]["file_path"] . $old_filename, $GLOBALS['CONFIG']['MEDIA_CATEGORIES'][$form_data["category"]]["file_path"] . $form_data["filename"])) {
                $view->addMessage('STATUS', 'ERROR', $translator->trans('could_not_save_the_media'));
            } else {
                /* delete the old file from the fs */
                $this->admin_delete_file_fs($GLOBALS['CONFIG']['MEDIA_CATEGORIES'][$old_category]["file_path"] . $old_filename);
            }
        }

        $form_data["tags"] = sanitize_tags($form_data["tags"]);
        $form_data["updated_by"] = $profile->getId();
        $form_data["updated_at"] = (new DateTime())->getDateTime();
        
        // Note, this will fail if the image is missing. Not a huge problem, so suppress warnings
        $form_data["filesize"] = @filesize($GLOBALS['CONFIG']['MEDIA_CATEGORIES'][$form_data["category"]]["file_path"] . $form_data["filename"]);

        /* try to get the size (supported by getimagesize()) */
        $img_array = @getimagesize($GLOBALS['CONFIG']['MEDIA_CATEGORIES'][$form_data["category"]]["file_path"] . $form_data["filename"]);
        if (is_array($img_array)) {
            $form_data["width"] = $img_array[0];
            $form_data["height"] = $img_array[1];
            $form_data["mimetype"] = $img_array['mime'];
        } else {
            $form_data["width"] = 0;
            $form_data["height"] = 0;
            $form_data["mimetype"] = '';
        }

        $_result = $this->admin->admin_set_update($id, $form_data);
        if ($_result == true) {
            $this->calc_url_para($URL_PARA, $form_data);
            $view->addMessage('STATUS', 'NOTICE', $translator->trans('action_was_successfully'));
        } else {
            $view->addMessage('STATUS', 'WARNING', $translator->trans('no_data_has_been_updated'));

            $html = $this->form_change($action, $id, $URL_PARA, $form_data);
        }

        return $html;
    }

    /**
     * @param int $id
     *
     * @return bool
     */
    public function delete_one($id)
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $db = $this->container->get('database');

        /* get the db-date before we delete */
        $db->query('SELECT category,filename FROM ' . $this->configuration->table["table_name"] . ' WHERE id=' . $id . ';');
        if ($db->num_rows() == 0) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('could_not_find_media'));
            return false;
        }
        $db->next_record();
        $tmp_category = $db->f("category");
        $tmp_filename = $db->f("filename");

        /* delete the file in the db */
        $_result = $this->admin->admin_set_delete($id);
        if ($_result) {
            /* delete the file from the fs */
            $this->admin_delete_file_fs($GLOBALS['CONFIG']['MEDIA_CATEGORIES'][$tmp_category]["file_path"] . $tmp_filename);
        }

        return true;
    }

    /**
     * @param int $id
     * @param array $URL_PARA
     *
     * @return string
     */
    public function ask4delete($id, $URL_PARA)
    {
        return $this->admin->admin_get_confirm_table($id, $URL_PARA, 'delete');
    }

    /**
     * @param string $action
     * @param array $URL_PARA
     * @param array $error_data
     *
     * @return string
     */
    public function form_new($action, $URL_PARA, $error_data = array())
    {
        return $this->admin->admin_get_form($action, 0, $error_data, $URL_PARA);
    }

    /**
     * @param string $action
     * @param int $id
     * @param array $URL_PARA
     * @param array $error_data
     *
     * @return string
     */
    public function form_change($action, $id, $URL_PARA, $error_data = array())
    {
        return $this->admin->admin_get_form($action, $id, $error_data, $URL_PARA);
    }

    /**
     * @param string $action
     * @param array $search_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function show_search_form($action, $search_data, $URL_PARA)
    {
        return $this->admin->admin_get_form($action, 0, $search_data, $URL_PARA);
    }

    /**
     * OUT: ok: TRUE/error: FALSE
     * 
     * @param string $source
     * @param string $destination
     *
     * @return bool
     */
    public function admin_save_file_fs($source, $destination)
    {
        $tmp_result = copy($source, $destination);
        @chmod($destination, 0644);

        return $tmp_result;
    }

    /**
     * @param string $filename
     *
     * @return bool
     */
    public function admin_delete_file_fs($filename)
    {
        return unlink($filename);
    }

    /**
     * calc the parameters to show the last edited or new entry
     * 
     * @param array $_url_para
     * @param array $form_data
     *
     * @return bool
     */
    public function calc_url_para($_url_para, $form_data)
    {
        $db = $this->container->get('database');

        if (!is_array($_url_para)) {
            return false;
        }

        /* try to get the id of the file */
        $db->query('SELECT id FROM ' . $this->configuration->table["table_name"] . ' WHERE category=' . $form_data["category"] . ' AND filename="' . $form_data["filename"] . '";');
        if ($db->num_rows() == 0) {
            return false;
        }
        $db->next_record();
        $id = $db->f("id");

        /* alphanumeric navigation */
        if ($_url_para["type"] == "alpha") {
            if (isset($form_data["filename"])) {
                $_url_para["alpha"] = substr($form_data["filename"], 0, 1);

                $sql_txt = 'SELECT id FROM ' . $this->configuration->table["table_name"] . ' WHERE ' . $this->configuration->table["alpha_index"] . ' LIKE "' . $_url_para["alpha"] . '%" ORDER BY ' . $_url_para["sort"];
                $db->query($sql_txt);
                $tmp_counter = 0;
                while ($db->next_record()) {
                    if (isset($db->Record[0])) {
                        $tmp_id = $db->Record[0];
                        $tmp_counter++;
                        if ($tmp_id == $id) {
                            break;
                        }
                    }
                }
                $_url_para["page"] = ceil($tmp_counter / $_url_para["num_per_page"]);
                $GLOBALS['URL_PARA']["page"] = $_url_para["page"];
                $GLOBALS['URL_PARA']["alpha"] = $_url_para["alpha"];
            }
        } else {
            /* numeric navigation */
            $sql_txt = 'SELECT id FROM ' . $this->configuration->table["table_name"] . ' ORDER BY ' . $_url_para["sort"];
            $db->query($sql_txt);
            $tmp_counter = 0;
            while ($db->next_record()) {
                if (isset($db->Record[0])) {
                    $tmp_id = $db->Record[0];
                    $tmp_counter++;
                    if ($tmp_id == $id) {
                        break;
                    }
                }
            }
            $_url_para["page"] = ceil($tmp_counter / $_url_para["num_per_page"]);
            $GLOBALS['URL_PARA']["page"] = $_url_para["page"];
        }

        return true;
    }
}