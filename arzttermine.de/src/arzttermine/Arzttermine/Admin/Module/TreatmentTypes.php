<?php

namespace Arzttermine\Admin\Module;

use Arzttermine\Admin\Configuration;
use Arzttermine\Admin\Module;
use Arzttermine\Core\DateTime;
use Arzttermine\MedicalSpecialty\MedicalSpecialty;
use Arzttermine\User\User;

final class TreatmentTypes extends Module {

    /**
     * @var string
     */
    protected $title = 'Behandlungsarten Admin';

    /**
     * @var string
     */
    protected $role = 'treatmenttype_admin';

    /**
     * @var string
     */
    protected $slug = 'treatment_types';

    /**
     * {@inheritdoc}
     */
    public function boot()
    {
        $translator = $this->container->get('translator');

        $MENU_ARRAY = array(
            "id"                   => array("sql" => 1, "name" => "ID", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "status"               => array("sql" => 1, "name" => "Status", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "select", "values" => $GLOBALS['CONFIG']['TREATMENTTYPE_STATUS']), "list_function" => array("name" => "get_data_status", "para" => true)),
            "medical_specialty_id" => array("sql" => 1, "name" => "Fachrichtung", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_medical_specialty_id", "para" => true), "form_input" => array("type" => "select", "values" => $this->get_values_medical_specialty_id())),
            "name_de"              => array("sql" => 1, "name" => "Name de", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "name_en"              => array("sql" => 1, "name" => "Name en", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "slug"                 => array("sql" => 1, "name" => "Permalink", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 0, "nowrap" => 1),
            "created_at"           => array("sql" => 1, "name" => "Erstellungsdatum", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "none")),
            "created_by"           => array("sql" => 1, "name" => "Erstellt von", "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_created_by", "para" => true), "list_function" => array("name" => "get_data_created_by", "para" => true)),
            "updated_at"           => array("sql" => 1, "name" => "Letztes Update", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "updated_by"           => array("sql" => 1, "name" => "Letzter Bearbeiter", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_updated_by", "para" => true), "list_function" => array("name" => "get_data_updated_by", "para" => true)),
        );

        $BUTTON = array(
            "new"    => array("func" => "treatmenttype_admin_new", "alt_text" => $translator->trans('new_item'), "script" => $this->admin->getUrl(), "gfx" => "document_new.png", "action" => "new", "th" => 1, "td" => 0),
            "edit"   => array("func" => "treatmenttype_admin_edit", "alt_text" => $translator->trans('edit_item'), "script" => $this->admin->getUrl(), "gfx" => "document_edit.png", "action" => "edit", "th" => 0, "td" => 1),
            "delete" => array("func" => "treatmenttype_admin_delete", "alt_text" => $translator->trans('delete_item'), "script" => $this->admin->getUrl(), "gfx" => "delete.png", "action" => "delete", "th" => 0, "td" => 1),
            "search" => array("func" => "treatmenttype_admin_search", "alt_text" => $translator->trans('search'), "script" => $this->admin->getUrl(), "gfx" => "search.png", "action" => "search", "th" => 1, "td" => 0)
        );
        $TABLE_CONFIG = array(
            "table_name"  => DB_TABLENAME_TREATMENTTYPES,
            "alpha_index" => "name",
        );
        $URL_PARA = array(
            'page'         => 1,
            'alpha'        => '',
            "sort"         => 0,
            "direction"    => 'DESC',
            "num_per_page" => 25,
            "type"         => 'num'
        );

        $configuration = new Configuration();
        $configuration->menu    = $MENU_ARRAY;
        $configuration->buttons = $BUTTON;
        $configuration->table   = $TABLE_CONFIG;
        $configuration->default_parameters = $URL_PARA;

        $this->configuration = $configuration;
    }

    /**
     * {@inheritdoc}
     */
    public function render()
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $html = '';

        $action = getParam("action", 'list');
        $form_data = getParam("form_data");

        switch ($action) {
            case "edit":
                if (!$this->checkPrivilege('treatmenttype_admin_edit')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                } else {
                    $id = getParam("id");
                    if (is_array($form_data)) {
                        $html = $this->change_one($action, $id, $form_data, $this->parameters);
                    } else {
                        $html = $this->form_change($action, $id, $this->parameters);
                    }
                }
                break;
            
            case "delete":
                if (!$this->checkPrivilege('treatmenttype_admin_delete')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                } else {
                    if (!isset($_REQUEST["confirm_id"])) {
                        $id = getParam("id");
                        $html = $this->ask4delete($id, $this->parameters);
                    } else {
                        $confirm_id = getParam("confirm_id");
                        if ($this->delete_one($confirm_id)) {
                            $view->addMessage('STATUS', 'NOTICE', $translator->trans('the_entry_was_deleted_successfully'));
                        } else {
                            $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_could_not_be_deleted'));
                        }
                    }
                }
                break;
            
            case "search":
                if (!$this->checkPrivilege('treatmenttype_admin_search')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                
                if (isset($_REQUEST["send_button"])) {
                    $this->parameters["page"] = 1;
                    $this->parameters["alpha"] = '';
                }
                
                $html = $this->show_search_form($action, $form_data, $this->parameters);
                if (is_array($form_data)) {
                    $html .= $this->get_list($this->parameters, $form_data);
                }
                break;
            
            case "new":
                if (!$this->checkPrivilege('treatmenttype_admin_new')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                if (is_array($form_data)) {
                    $html = $this->new_one($action, $form_data, $this->parameters);
                } else {
                    $html = $this->form_new($action, $this->parameters);
                }
                break;
        }

        if (empty($html)) {
            if ($this->checkPrivilege('treatmenttype_admin_view')) {
                $html = $this->get_list($this->parameters);
            }
        }

        return $html;
    }

    /**
     * @param array $URL_PARA
     * @param array $search
     *
     * @return string
     */
    public function get_list($URL_PARA, $search = array())
    {
        return $this->admin->admin_get_list(0, $search, $URL_PARA);
    }
    
    /**
     * @param string $action
     * @param array $form_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function new_one($action, $form_data, $URL_PARA)
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $profile = $this->container->get('current_user');
        $html = '';

        // 1. set defaults
        $form_data["created_by"] = $profile->getId();
        $form_data["created_at"] = (new DateTime())->getDateTime();

        // 2. check
        if (1 > mb_strlen(sanitize_slug($form_data["slug"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
            $view->addMessage('admin_message_slug', 'ERROR', $translator->trans('Der Permalink ist nicht gültig!'));
        }
        if (1 > mb_strlen(sanitize_title($form_data["name_en"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
            $view->addMessage('admin_message_name', 'ERROR', $translator->trans('unvalid_name'));
        }
        if (1 > mb_strlen(sanitize_title($form_data["name_de"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
            $view->addMessage('admin_message_name', 'ERROR', $translator->trans('unvalid_name'));
        }

        // 3. exit on errors
        if ($view->countMessages()) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('Bitte prüfen Sie Ihre Eingaben.'));
            return $this->form_change($action, 0, $URL_PARA, $form_data);
        }
        
        // 4. sanitize	
        $form_data["slug"] = sanitize_slug($form_data["slug"]);
        $form_data["name_en"] = sanitize_title($form_data["name_en"]);
        $form_data["name_de"] = sanitize_title($form_data["name_de"]);

        $_id = $this->admin->admin_set_new($form_data);
        if ($_id > 0) {
            $view->addMessage('STATUS', 'NOTICE', $translator->trans('action_was_successfully'));
        } elseif ($_id == -1) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_allready_exists'));
            $html = $this->form_new($action, $URL_PARA, $form_data);
        } elseif ($_id == -2) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('name_of_primary_key_is_empty'));
            $html = $this->form_new($action, $URL_PARA, $form_data);
        } else {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_could_not_be_created'));
        }

        return $html;
    }
    
    /**
     * @param string $action
     * @param int $id
     * @param array $form_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function change_one($action, $id, $form_data, $URL_PARA)
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $profile = $this->container->get('current_user');
        $html = '';

        // 1. set defaults
        $form_data["updated_by"] = $profile->getId();
        $form_data["updated_at"] = (new DateTime())->getDateTime();

        // 2. check
        if (1 > mb_strlen(sanitize_slug($form_data["slug"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
            $view->addMessage('admin_message_slug', 'ERROR', $translator->trans('Der Permalink ist nicht gültig!'));
        }

        // 3. exit on errors
        if ($view->countMessages()) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('Bitte prüfen Sie Ihre Eingaben.'));
            return $this->form_change($action, $id, $URL_PARA, $form_data);
        }
        
        // 4. sanitize
        $form_data["slug"] = sanitize_slug($form_data["slug"]);

        $_result = $this->admin->admin_set_update($id, $form_data);
        if ($_result == true) {
            $view->addMessage('STATUS', 'NOTICE', $translator->trans('action_was_successfully'));
            return '';
        }

        if ($_result == false) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('no_data_has_been_updated'));
            $html = $this->form_change($action, $id, $URL_PARA, $form_data);
        }

        return $html;
    }

    /**
     * @param int $id
     * @param array $URL_PARA
     *
     * @return string
     */
    public function ask4delete($id, $URL_PARA)
    {
        return $this->admin->admin_get_confirm_table($id, $URL_PARA, 'delete');
    }

    /**
     * @param int $id
     *
     * @return int
     */
    public function delete_one($id)
    {
        return $this->admin->admin_set_delete($id);
    }

    /**
     * @param string $action
     * @param array $URL_PARA
     * @param array $error_data
     *
     * @return string
     */
    public function form_new($action, $URL_PARA, $error_data = array())
    {
        $_html = $this->admin->admin_get_form($action, 0, $error_data, $URL_PARA);
        $_html .= '
<script type="text/javascript">
$(document).ready(function() {
	$("#form_data_name").slugIt({
		events: \'blur\',
		output: \'#form_data_slug\'
	});
});
</script>';

        $this->getView()
            ->addJsInclude(getStaticUrl('jquery/plugins/jquery.slugit.js'))
            ->setFocus('#form_data_name');

        return $_html;
    }

    /**
     * @param string $action
     * @param int $id
     * @param array $URL_PARA
     * @param array $error_data
     *
     * @return string
     */
    public function form_change($action, $id, $URL_PARA, $error_data = array())
    {
        $_html = $this->admin->admin_get_form($action, $id, $error_data, $URL_PARA);
        $_html .= '
<script type="text/javascript">
$(document).ready(function() {
	if ($("#form_data_slug").val().length < 1) {
		$("#form_data_name").slugIt({
		events: \'blur\',
		output: \'#form_data_slug\'
		});
	}
});
</script>';
        
        $this->getView()
            ->addJsInclude(getStaticUrl('jquery/plugins/jquery.slugit.js'))
            ->setFocus('#form_data_name');

        return $_html;
    }

    /**
     * @param string $action
     * @param array $search_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function show_search_form($action, $search_data, $URL_PARA)
    {
        return $this->admin->admin_get_form($action, 0, $search_data, $URL_PARA);
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_created_by($data = array())
    {
        $data['created_by'] = !empty($data['created_by']) ? intval($data['created_by']) : 0;

        return (new User($data['created_by']))->getFullName();
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_created_at($data = array())
    {
        return mysql2date('d.m.Y H:i:s', $data['created_at'], false);
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_status($data = array())
    {
        if (isset($GLOBALS['CONFIG']['TREATMENTTYPE_STATUS'][$data['status']])) {
            return $GLOBALS['CONFIG']['TREATMENTTYPE_STATUS'][$data['status']];
        } else {
            return 'N/A';
        }
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_medical_specialty_id($data = array())
    {
        return MedicalSpecialty::getNameById($data['medical_specialty_id']);
    }

    /**
     * @return array
     */
    public function get_values_medical_specialty_id()
    {
        return MedicalSpecialty::getHtmlOptions(true);
    }
    
}