<?php

namespace Arzttermine\Admin\Module;

use Arzttermine\Admin\Configuration as AdminConfiguration;
use Arzttermine\Admin\Module;
use Arzttermine\Core\ConfigurationData;

final class Configuration extends Module {

    /**
     * @var string
     */
    protected $title = 'Configuration Admin';

    /**
     * @var string
     */
    protected $role = 'configuration_admin';

    /**
     * @var string
     */
    protected $slug = 'configuration';

    /**
     * {@inheritdoc}
     */
    public function boot()
    {
        $translator = $this->container->get('translator');

        $MENU_ARRAY = array(
            "id"           => array("sql" => 1, "name" => "ID", "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "option_name"  => array("sql" => 1, "name" => "OptionName", "show" => 1, "edit" => 1, "new" => 1, "search" => 0, "sort" => 1, "nowrap" => 1),
            "option_value" => array("sql" => 1, "name" => "OptionValue", "show" => 1, "edit" => 1, "new" => 1, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "textarea", "optional" => ' cols="60" rows="10" style="width:300px;"'), "list_function" => array("name" => "get_data_option_value", "para" => true)),
            "autoload"     => array("sql" => 1, "name" => "Autoload", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "select", "values" => $GLOBALS['CONFIG']['ARRAY_NO_YES']), "list_function" => array("name" => "get_data_autoload", "para" => true)),
        );

        $BUTTON = array(
            "new"    => array("func" => "configuration_admin_new", "alt_text" => $translator->trans('new_item'), "script" => $this->admin->getUrl(), "gfx" => "document_new.png", "action" => "new", "th" => 1, "td" => 0),
            "edit"   => array("func" => "configuration_admin_edit", "alt_text" => $translator->trans('edit_item'), "script" => $this->admin->getUrl(), "gfx" => "document_edit.png", "action" => "edit", "th" => 0, "td" => 1),
            "delete" => array("func" => "configuration_admin_delete", "alt_text" => $translator->trans('delete_item'), "script" => $this->admin->getUrl(), "gfx" => "delete.png", "action" => "delete", "th" => 0, "td" => 1)
        );

        $TABLE_CONFIG = array(
            "table_name"  => DB_TABLENAME_CONFIGURATION,
            "alpha_index" => "option_name",
        );

        $URL_PARA = array(
            'page'         => 1,
            'alpha'        => '',
            "sort"         => 0,
            "direction"    => 'DESC',
            "num_per_page" => 50,
            "type"         => 'num'
        );

        $configuration = new AdminConfiguration();
        $configuration->menu    = $MENU_ARRAY;
        $configuration->buttons = $BUTTON;
        $configuration->table   = $TABLE_CONFIG;
        $configuration->default_parameters = $URL_PARA;

        $this->configuration = $configuration;
    }

    /**
     * {@inheritdoc}
     */
    public function render()
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $html = '';

        $action = getParam("action", 'list');
        $form_data = getParam("form_data");

        switch ($action) {
            case "edit":
                if (!$this->checkPrivilege('configuration_admin_edit')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                } else {
                    $id = getParam("id");
                    if (is_array($form_data)) {
                        $html = $this->change_one($action, $id, $form_data, $this->parameters);
                    } else {
                        $html = $this->form_change($action, $id, $this->parameters);
                    }
                }
                break;
            
            case "delete":
                if (!$this->checkPrivilege('configuration_admin_delete')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                } else {
                    if (!isset($_REQUEST["confirm_id"])) {
                        $id = getParam("id");
                        $html = $this->ask4delete($id, $this->parameters);
                    } else {
                        $confirm_id = getParam("confirm_id");
                        if ($this->delete_one($confirm_id)) {
                            $view->addMessage('STATUS', 'NOTICE', $translator->trans('the_entry_was_deleted_successfully'));
                        } else {
                            $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_could_not_be_deleted'));
                        }
                    }
                }
                break;
            
            case "new":
                if (!$this->checkPrivilege('configuration_admin_new')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                if (is_array($form_data)) {
                    $html = $this->new_one($action, $form_data, $this->parameters);
                } else {
                    $html = $this->form_new($action, $this->parameters);
                }
                break;
        }

        if (empty($html)) {
            if ($this->checkPrivilege('configuration_admin_view')) {
                $html = $this->get_list($this->parameters);
            }
        }

        return $html;
    }

    /**
     * @param array $data
     *
     * @return mixed
     */
    public function get_data_autoload($data = array())
    {
        $data['autoload'] = !empty($data['autoload']) ? $data['autoload'] : 0;
        
        return $GLOBALS["CONFIG"]["ARRAY_NO_YES"][$data['autoload']];
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_option_value($data = array())
    {
        $option_value = empty($data['option_value']) ? $data['option_value'] : '';
        
        if (!empty($option_value)) {
            $option_value = mb_substr($option_value, 0, 20, $GLOBALS['CONFIG']['SYSTEM_CHARSET']);

            if (mb_strlen($data['option_value'], $GLOBALS['CONFIG']['SYSTEM_CHARSET']) > 20) {
                $option_value .= '...';
            }
        }

        return $option_value;
    }

    /**
     * @param array $URL_PARA
     * @param array $search
     *
     * @return string
     */
    public function get_list($URL_PARA, $search = array())
    {
        return $this->admin->admin_get_list(0, $search, $URL_PARA);
    }

    /**
     * @param string $action
     * @param array $form_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function new_one($action, $form_data, $URL_PARA)
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');

        if ($view->countMessages()) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('Bitte prüfen Sie Ihre Eingaben.'));
            return $this->form_change($action, 0, $URL_PARA, $form_data);
        }

        $configurationData = new ConfigurationData();
        $configurationData
            ->setOptionName($form_data['option_name'])
            ->setOptionValue($form_data['option_value'])
            ->setAutoload((bool)$form_data['autoload'])
            ->saveNewObject();

        if ($configurationData->isValid()) {
            $view->addMessage('STATUS', 'NOTICE', $translator->trans('action_was_successfully'));
        } else {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_could_not_be_created'));
        }

        return '';
    }

    /**
     * @param string $action
     * @param int $id
     * @param array $form_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function change_one($action, $id, $form_data, $URL_PARA)
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $html = '';

        $configurationData = new ConfigurationData($id);
        if (!$configurationData->isValid()) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('Bitte prüfen Sie Ihre Eingaben.'));
        }

        // 3. exit on errors
        if ($view->countMessages()) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('Bitte prüfen Sie Ihre Eingaben.'));
            return $this->form_change($action, $id, $URL_PARA, $form_data);
        }

        $_result = $configurationData
            ->setOptionName($form_data['option_name'])
            ->setOptionValue($form_data['option_value'])
            ->setAutoload((bool)$form_data['autoload'])
            ->save();

        if ($_result == true) {
            $view->addMessage('STATUS', 'NOTICE', $translator->trans('action_was_successfully'));
        } else {
            $view->addMessage('STATUS', 'WARNING', $translator->trans('no_data_has_been_updated'));

            $html = $this->form_change($action, $id, $URL_PARA, $form_data);
        }

        return $html;
    }

    /**
     * @param int $id
     * @param array $URL_PARA
     *
     * @return string
     */
    public function ask4delete($id, $URL_PARA)
    {
        return $this->admin->admin_get_confirm_table($id, $URL_PARA, 'delete');
    }

    /**
     * @param int $id
     *
     * @return int
     */
    public function delete_one($id)
    {
        $configuration = new ConfigurationData($id);
        $result = 0;
        
        if ($configuration->isValid()) {
            $result = $configuration->delete();
        }
        
        return $result;
    }

    /**
     * @param string $action
     * @param array $URL_PARA
     * @param array $error_data
     *
     * @return string
     */
    public function form_new($action, $URL_PARA, $error_data = array())
    {
        return $this->admin->admin_get_form($action, 0, $error_data, $URL_PARA);
    }

    /**
     * @param string $action
     * @param int $id
     * @param array $URL_PARA
     * @param array $error_data
     *
     * @return string
     */
    public function form_change($action, $id, $URL_PARA, $error_data = array())
    {
        return $this->admin->admin_get_form($action, $id, $error_data, $URL_PARA);
    }

    /**
     * @param string $action
     * @param array $search_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function show_search_form($action, $search_data, $URL_PARA)
    {
        return $this->admin->admin_get_form($action, 0, $search_data, $URL_PARA);
    }

}