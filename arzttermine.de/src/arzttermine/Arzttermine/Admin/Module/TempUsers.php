<?php

namespace Arzttermine\Admin\Module;

use Arzttermine\Admin\Configuration;
use Arzttermine\Admin\Module;
use Arzttermine\Mail\Mailing;
use Arzttermine\MedicalSpecialty\MedicalSpecialty;
use Arzttermine\User\User;
use NGS\Managers\MedicalSpecialtiesManager;
use NGS\Managers\TempUsersDataManager;
use NGS\Managers\TreatmentTypesManager;
use NGS\Managers\UsersManager;

final class TempUsers extends Module {

    /**
     * @var string
     */
    protected $title = 'Temp Users Admin';

    /**
     * @var string
     */
    protected $role = 'user_admin';

    /**
     * @var string
     */
    protected $slug = 'temp_users';

    /**
     * {@inheritdoc}
     */
    public function boot()
    {
        $translator = $this->container->get('translator');

        $MENU_ARRAY = array(
            "id"                    => array("sql" => 1, "name" => "ID", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "title"                 => array("sql" => 1, "name" => 'Titel', "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "first_name"            => array("sql" => 1, "name" => $translator->trans('first_name'), "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "last_name"             => array("sql" => 1, "name" => $translator->trans('last_name'), "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_last_name", "para" => true)),
            "gender"                => array("sql" => 1, "name" => "Geschlecht", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_gender", "para" => true), "form_input" => array("type" => "select", "values" => $GLOBALS['CONFIG']['ARRAY_GENDER'])),
            "email"                 => array("sql" => 1, "name" => $translator->trans('email'), "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "created_at"            => array("sql" => 1, "name" => $translator->trans('created_at'), "show" => 1, "edit" => 0, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "medical_specialty_ids" => array("sql" => 1, "name" => "Fachrichtungen", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_medical_specialty_ids", "para" => true), "form_input" => array("type" => "function", "name" => "get_form_medical_specialty_ids", "para" => true)),
            "treatment_types"       => array("sql" => 1, "name" => "Treatment Types", "show" => 0, "edit" => 1, "new" => 1, "search" => 0, "sort" => 0, "nowrap" => 1, "list_function" => array("name" => "", "para" => true), "form_input" => array("type" => "function", "name" => "get_form_treatment_types", "para" => true)),
            "location_www"          => array("sql" => 1, "name" => 'Url', "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "location_name"         => array("sql" => 1, "name" => 'Praxis', "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "location_doctors_count"=> array("sql" => 1, "name" => 'Anzahl Ärzte', "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "location_street"       => array("sql" => 1, "name" => 'Street', "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "location_zip"          => array("sql" => 1, "name" => 'Post Code', "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "location_city"         => array("sql" => 1, "name" => 'City', "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "registration_reasons"  => array("sql" => 1, "name" => 'Registrierungsgrund', "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 0, "nowrap" => 1, "form_input" => array("type" => "textarea", "optional" => ' cols="100" rows="10"')),
            "registration_funnel"   => array("sql" => 1, "name" => 'Registrierungsverweis', "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 0, "nowrap" => 1),
        );
        $BUTTON = array(
            "edit"     => array("func" => "user_admin_edit", "alt_text" => $translator->trans('Bearbeiten'), "script" => $this->admin->getUrl(), "gfx" => "document_edit.png", "action" => "edit", "th" => 0, "td" => 1),
            "activate" => array("func" => "user_admin_activate", "alt_text" => $translator->trans('Activate'), "script" => $this->admin->getUrl(), "gfx" => "version_activate.png", "action" => "activate", "th" => 0, "td" => 1),
            "delete"   => array("func" => "user_admin_delete", "alt_text" => $translator->trans('Löschen'), "script" => $this->admin->getUrl(), "gfx" => "delete.png", "action" => "delete", "th" => 0, "td" => 1),
            "search"   => array("func" => "user_admin_search", "alt_text" => $translator->trans('Suchen'), "script" => $this->admin->getUrl(), "gfx" => "search.png", "action" => "search", "th" => 1, "td" => 0),
        );
        $TABLE_CONFIG = array(
            "table_name" => DB_TABLENAME_TEMP_USERS_DATA,
        );
        $URL_PARA = array(
            "page"         => 1,
            "alpha"        => '',
            "sort"         => 'id',
            "direction"    => 'DESC',
            "num_per_page" => 50,
            "type"         => "num"
        );

        $configuration = new Configuration();
        $configuration->menu    = $MENU_ARRAY;
        $configuration->buttons = $BUTTON;
        $configuration->table   = $TABLE_CONFIG;
        $configuration->default_parameters = $URL_PARA;

        $this->configuration = $configuration;
    }

    /**
     * {@inheritdoc}
     */
    public function render()
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $tempUsersDataManager = TempUsersDataManager::getInstance();
        $html = '';

        $action = getParam("action", 'list');
        $form_data = getParam("form_data");

        switch ($action) {
            case "edit":
                if (!$this->checkPrivilege('user_admin_edit')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                } else {
                    $id = getParam("id");
                    if (is_array($form_data)) {
                        $html = $this->change_one($action, $id, $form_data, $this->parameters);
                    } else {
                        $html = $this->form_change($action, $id, $this->parameters);
                    }
                }
                break;
            
            case "delete":
                if (!$this->checkPrivilege('user_admin_delete')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                } else {
                    if (!isset($_REQUEST["confirm_id"])) {
                        $id = getParam("id");
                        $html = $this->ask4delete($id, $this->parameters);
                    } else {
                        $idToBeDeleted = getParam("confirm_id");
                        if (isset($idToBeDeleted)) {
                            $result = $tempUsersDataManager->deleteById($idToBeDeleted);
                            if ($result == -1) {
                                $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_could_not_be_deleted'));
                            } else {
                                $view->addMessage('STATUS', 'NOTICE', $translator->trans('the_entry_was_deleted_successfully'));
                            }
                        }
                    }
                }
                break;
            
            case "search":
                if (!$this->checkPrivilege('user_admin_search')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                if (isset($_REQUEST["send_button"])) {
                    $this->parameters["page"] = 1;
                    $this->parameters["alpha"] = '';
                }
                $html = $this->show_search_form($action, $form_data, $this->parameters);
                if (is_array($form_data)) {
                    $html .= $this->get_list($this->parameters, $form_data);
                }
                break;
            
            case 'activate':
                if (!$this->checkPrivilege('user_admin_activate')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                } else {
                    if (!isset($_REQUEST["confirm_id"])) {
                        $id = getParam("id");
                        $html = $this->ask4activate($id, $this->parameters);
                    } else {
                        $idToBeActivated = getParam("confirm_id");
                        $newUser = $tempUsersDataManager->activateById($idToBeActivated);
                        $this->sendActivationEmail($newUser);
                        $view->addMessage('STATUS', 'NOTICE', $translator->trans('the_entry_was_activated_successfully'));
                    }
                }
                break;
        }

        if (empty($html)) {
            if ($this->checkPrivilege('user_admin_view')) {
                $html = $this->get_list($this->parameters);
            }
        }

        return $html;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_first_name($data = array())
    {
        $_user = new User($data['id']);

        return '<a href="' . $_user->getUrl() . '" target="_blank">' . $_user->getFirstName() . '</a>';
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_gender($data = array())
    {
        if (isset($GLOBALS['CONFIG']['ARRAY_GENDER'][$data['gender']])) {
            return $GLOBALS['CONFIG']['ARRAY_GENDER'][$data['gender']];
        };

        return 'N/A';
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_form_medical_specialty_ids($data = array())
    {
        $_html = '';
        $_ids = $data['medical_specialty_ids'];
        $_ids = explode(',', $_ids);
        $_medical_specialty = new MedicalSpecialty();
        $_medical_specialties = $_medical_specialty->getMedicalSpecialties();
        $_html_rows = '';
        /** @var MedicalSpecialty[] $_medical_specialties */
        foreach ($_medical_specialties as $_medical_specialty) {
            $_html_rows .= '<tr><td width="20"><input type="checkbox" onchange="addOrRemoveTreatmentTypes(' . $_medical_specialty->getId() . ')" id="form_data_medical_specialty_ids_' . $_medical_specialty->getId() . '" value="1" name="form_data[medical_specialty_ids][' . $_medical_specialty->getId() . ']" ' . (in_array($_medical_specialty->getId(), $_ids) ? " checked" : "") . '></td><td><label for="form_data_medical_specialty_ids_' . $_medical_specialty->getId() . '">' . $_medical_specialty->getName() . '</label></td></tr>';
        }
        $_html .= '<table class="table table-striped">';
        $_html .= '<tr class="headers"><th colspan="2">Fachrichtungen</th></tr>';
        $_html .= $_html_rows;
        $_html .= "</table>\n";

        return $_html;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_medical_specialty_ids($data = array())
    {
        $_medical_specialty = new MedicalSpecialty();

        return getCategoriesText($data['medical_specialty_ids'], $_medical_specialty->getMedicalSpecialtyValues(), '<br />');
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_form_treatment_types($data = array())
    {
        $medicalSpecialtiesManager = MedicalSpecialtiesManager::getInstance();
        $tempUsersDataManager = TempUsersDataManager::getInstance();
        $treatmentTypesManager = TreatmentTypesManager::getInstance();
        $medicalSpecialtiesJson = '';
        $_html = '';
        
        $temp_user_id = $data['id'];
        $tempUser = $tempUsersDataManager->getById($temp_user_id);
        $existingTreatmentTypes = $treatmentTypesManager->getByIds(explode(',', $tempUser->getTreatmentTypeIds()));
        $medSpecIdsArrayWithExistingTreatmentTypes = array();
        /** @var \NGS\DAL\DTO\TreatmentTypesDto[] $existingTreatmentTypes */
        foreach ($existingTreatmentTypes as $treatmentType) {
            $medSpecIdsArrayWithExistingTreatmentTypes[$treatmentType->getMedicalSpecialtyId()][] = $treatmentType->getId();
        }
        $_ids = $data['medical_specialty_ids'];
        $_ids = explode(',', $_ids);

        $_medical_specialty = new MedicalSpecialty();
        $allMedicalSpecialties = $_medical_specialty->getMedicalSpecialties();
        $allMedicalSpecialtiesWithTreatmentTypes = array();
        $medicalSpecialties = array();
        foreach ($allMedicalSpecialties as $medicalSpecialty) {
            $medicalSpecialtiesJson[$medicalSpecialty->getId()] = json_encode($medicalSpecialty);

            if(in_array($medicalSpecialty->getId(), $_ids)) {
                $medicalSpecialties[] = $medicalSpecialty;
            }

            $ttypes = $medicalSpecialty->getTreatmentTypes();
            foreach ($ttypes as $tt) {
                $allMedicalSpecialtiesWithTreatmentTypes[$tt->getMedicalSpecialtyId()][$tt->getId()] = $tt->getName();
            }
        }

        $_html_rows = '';
        /** @var MedicalSpecialty $medicalSpecialty */
        foreach ($medicalSpecialties as $medicalSpecialty) {
            $_html_rows .= '<tr id="treatment_types_for_medical_specialty_' . $medicalSpecialty->getId() . '"><td width="20"><div>';
            $_html_rows .= '<h3>' . $medicalSpecialty->getName() . '</h3>';
            $treatmentTypes = $medicalSpecialty->getTreatmentTypes();
            $_html_rows .= '<ul>';
            foreach ($treatmentTypes as $treatmentType) {
                $checked = (
                isset($medSpecIdsArrayWithExistingTreatmentTypes[$medicalSpecialty->getId()])
                &&
                in_array($treatmentType->getId(), $medSpecIdsArrayWithExistingTreatmentTypes[$medicalSpecialty->getId()])
                    ?
                    " checked"
                    :
                    ""
                );
                $_html_rows .= '<li style="list-style: none;"><input type="checkbox" id="form_data_treatment_type_ids_' . $treatmentType->getId() . '" value="' . $medicalSpecialty->getId() . '" name="form_data[treatment_type_ids][' . $treatmentType->getId() . ']" ' . $checked . ' ><span>' . $treatmentType->getName() . '</span></li>';
            }
            $_html_rows .= '</ul></div></td></tr>';
        }
        $_html .= '<table class="table table-striped" id="treatment_types">';
        $_html .= '<tr class="headers"><th colspan="2">Treatment Types</th></tr>';
        $_html .= $_html_rows;
        $_html .= "</table>\n";
        $_html
            .= '<script>                         
                var allMedicalSpecialtiesWithTreatmentTypes=' . json_encode($allMedicalSpecialtiesWithTreatmentTypes) . ';
                var medicalSpecialtiesJson=' . json_encode($medicalSpecialtiesJson) . ';
                var medSpecIdsArrayWithExistingTreatmentTypes=' . json_encode($medSpecIdsArrayWithExistingTreatmentTypes) . ';
                function addOrRemoveTreatmentTypes(medicalSpecId){
                    var element=document.getElementById("form_data_medical_specialty_ids_"+medicalSpecId);
                   
                    if (typeof(element) != "undefined" && element != null){
                            if(element.checked){
                                var tr=createTreatmentTypesSectionByMedicalSpecialtyId(medicalSpecId);
                                var treatmentTypes=document.getElementById("treatment_types");
                                var newRow=treatmentTypes.insertRow(-1);
                                newRow.setAttribute("id","treatment_types_for_medical_specialty_"+medicalSpecId);
                                newRow.innerHTML=tr;
                                
                            }else{
                                var row=document.getElementById("treatment_types_for_medical_specialty_"+medicalSpecId);
                                row.parentNode.removeChild(row);
                            }
                        }
                    }   
                
                
                function createTreatmentTypesSectionByMedicalSpecialtyId(id){
                    var string= "<td width=\"20\"><div><h3>"+medicalSpecialtiesJson[id]["nameSingularDe"]+"</h3><ul>";
                    for (var i=0;i<allMedicalSpecialtiesWithTreatmentTypes[id].length;i++){
                        string+="<li style=\"list-style: none;\"><input type=\"checkbox\" name=\"form_data[treatment_type_ids][";
                        string+=allMedicalSpecialtiesWithTreatmentTypes[id][i]["id"];
                        string+="]\" value="+id+" id=\"form_data_treatment_type_ids_";
                        string+=allMedicalSpecialtiesWithTreatmentTypes[id][i]["id"];
                        string+="\"";
                        
                        if(typeof(medSpecIdsArrayWithExistingTreatmentTypes[id])!="undefined" && medSpecIdsArrayWithExistingTreatmentTypes[id].indexOf(allMedicalSpecialtiesWithTreatmentTypes[id][i]["id"])!=-1){
                            string+="checked=\"checked\"";
                        }
                        string+="><span>";
                        string+=allMedicalSpecialtiesWithTreatmentTypes[id][i]["nameDe"];
                        string+="</span></li>";
                    }
                    string+="</ul></div></td>";
                    return string;
                }
               
              </script>';

        return $_html;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_last_name($data = array())
    {
        if (isset($data['slug']) && $data['slug'] != '') {
            $_link = $GLOBALS['CONFIG']['URL_HTTP_LIVE'] . '/' . $data['slug'];
            $_name = $data['last_name'];

            return '<a href="' . $_link . '" target="_blank">' . $_name . '</a>';
        }

        return $data['last_name'];
    }

    /**
     * @param array $URL_PARA
     * @param array $search
     *
     * @return string
     */
    public function get_list($URL_PARA, $search = array())
    {
        return $this->admin->admin_get_list(0, $search, $URL_PARA);
    }

    /**
     * @param string $action
     * @param int $id
     * @param array $form_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function change_one($action, $id, $form_data, $URL_PARA)
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $html = '';

        if (1 > mb_strlen(sanitize_title($form_data["last_name"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
            $view->addMessage('admin_message_last_name', 'ERROR', $translator->trans('Bitte geben Sie einen Nachnamen an'));
        }

        // if there are checkboxes than set the keys to make sure that deselected checkboxes are beeing set
        $form_data = $this->admin->set_unselected_checkbox_values($form_data);

        $_medical_specialty_ids = '';
        if (isset($form_data['medical_specialty_ids']) && is_array($form_data['medical_specialty_ids'])) {
            $_medical_specialty_ids = implode(',', array_keys($form_data['medical_specialty_ids']));
        }

        $form_data['medical_specialty_ids'] = $_medical_specialty_ids;
        $locationInfo = array(
            'location_name' => $form_data['location_name'], 'location_street' => $form_data['location_street'],
            'location_zip'  => $form_data['location_zip'], 'location_city' => $form_data['location_city']
        );

        //shows error if one or more location fields are empty
        //doesn't show error if all location fields are empty
        if (count(array_filter($locationInfo)) > 0) {
            foreach ($locationInfo as $fieldName => $input) {
                if (empty($input)) {
                    $view->addMessage('admin_message_' . $fieldName, 'ERROR', $translator->trans('All location fields are mandatory'));
                }
            }
        }
        if (!isset($form_data['medical_specialty_ids']) || !isset($form_data['treatment_type_ids'])) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('Please input at least 1 medical specialty and 1 treatment type'));
            return $this->form_change($action, $id, $URL_PARA, $form_data);
        } elseif ($view->countMessages()) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('Bitte prüfen Sie Ihre Eingaben.'));
            return $this->form_change($action, $id, $URL_PARA, $form_data);
        }
        // 4. sanitize
        $form_data["title"] = sanitize_title($form_data["title"]);
        $form_data["first_name"] = sanitize_title($form_data["first_name"]);
        $form_data["last_name"] = sanitize_title($form_data["last_name"]);
        $form_data["location_name"] = sanitize_title($form_data["location_name"]);
        $form_data["location_street"] = sanitize_title($form_data["location_street"]);
        $form_data["location_zip"] = sanitize_title($form_data["location_zip"]);
        $form_data["location_city"] = sanitize_title($form_data["location_city"]);

        $_result = $this->admin->admin_set_update($id, $form_data);
        if ($_result == true) {
            $view->addMessage('STATUS', 'NOTICE', $translator->trans('action_was_successfully'));
            return '';
        }

        if ($_result == false) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('no_data_has_been_updated'));
            $html = $this->form_change($action, $id, $URL_PARA, $form_data);
        }

        return $html;
    }

    /**
     * @param $id
     * @param $URL_PARA
     *
     * @return string
     */
    public function ask4delete($id, $URL_PARA)
    {
        return $this->admin->admin_get_confirm_table($id, $URL_PARA, 'delete');
    }

    /**
     * @param int $id
     * @param array $URL_PARA
     *
     * @return string
     */
    public function ask4activate($id, $URL_PARA)
    {
        return $this->admin->admin_get_confirm_table($id, $URL_PARA, 'activate');
    }

    /**
     * @param string $action
     * @param int $id
     * @param array $URL_PARA
     * @param array $error_data
     *
     * @return string
     */
    public function form_change($action, $id, $URL_PARA, $error_data = array())
    {
        $this->getView()->setFocus('#form_data_email');

        return $this->admin->admin_get_form($action, $id, $error_data, $URL_PARA);
    }

    /**
     * @param string $action
     * @param array $search_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function show_search_form($action, $search_data, $URL_PARA)
    {
        return $this->admin->admin_get_form($action, 0, $search_data, $URL_PARA);
    }

    /**
     * @param \NGS\DAL\DTO\UsersDto $userDto
     */
    public function sendActivationEmail($userDto)
    {
        $templateFilename = '/mailing/doctors/doctor-activation.txt';
        $_mailing = new Mailing();
        $_mailing->type = MAILING_TYPE_TEXT;
        $_mailing->content_filename = $templateFilename;
        $_mailing->data['domain'] = $GLOBALS['CONFIG']['DOMAIN'];
        
        // $_mailing->data['url'] = $GLOBALS['protocol'].$_SERVER['HTTP_HOST'].'/'.$GLOBALS['CONFIG']['DOCTORS_LOGIN_URL'].'/do_aktivieren?code='.$activation_code;

        // NM 26.02.2014.
        // we send reset_password because we don't require the doc to provide password on account registration
        // the account will be activated when the doc resets the password for the first time (actually, it's not resetting, but setting the password)
        $resetPasswordCode = substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 10);
        $usersManager = UsersManager::getInstance();
        $usersManager->setResetPasswordCode($userDto->getId(), $resetPasswordCode);
        $_mailing->data['url'] = $GLOBALS['protocol'] . $_SERVER['HTTP_HOST'] . '/' . $GLOBALS['CONFIG']['DOCTORS_LOGIN_URL'] . '/passwort_zuruecksetzen/' . $resetPasswordCode;

        $_mailing->data['doctor_name'] = $userDto->getTitle() . " " . $userDto->getFirstName() . " " . $userDto->getLastName();
        $_mailing->addAddressTo($userDto->getEmail());
        $_mailing->send();
    }

}