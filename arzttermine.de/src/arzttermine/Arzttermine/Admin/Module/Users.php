<?php

namespace Arzttermine\Admin\Module;

use Arzttermine\Admin\Configuration;
use Arzttermine\Admin\Module;
use Arzttermine\Booking\Booking;
use Arzttermine\Core\DateTime;
use Arzttermine\Media\Asset;
use Arzttermine\MedicalSpecialty\MedicalSpecialty;
use Arzttermine\MedicalSpecialty\TreatmentType;
use Arzttermine\Security\Group;
use Arzttermine\User\User;

final class Users extends Module {

    /**
     * @var string
     */
    protected $title = 'Users Admin';

    /**
     * @var string
     */
    protected $role = 'user_admin';

    /**
     * @var string
     */
    protected $slug = 'users';

    /**
     * {@inheritdoc}
     */
    public function boot()
    {
        $translator = $this->container->get('translator');

        $MENU_ARRAY = array(
            "id"                      => array("sql" => 1, "name" => "ID", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "html_title"              => array("sql" => 1, "name" => 'HTML-Title', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("template" => "admin/_form_text_counter.tpl")),
            "html_meta_description"   => array("sql" => 1, "name" => 'HTML-Meta-Description', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("template" => "admin/_form_text_counter.tpl")),
            "html_meta_keywords"      => array("sql" => 1, "name" => 'HTML-Meta-Keywords', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("template" => "admin/_form_text_counter.tpl")),
            "googlepreview"           => array("sql" => 0, "name" => 'GooglePreview', "show" => 0, "edit" => 1, "new" => 1, "search" => 0, "sort" => 0, "nowrap" => 0, "form_input" => array("type" => "function", "name" => "get_form_googlepreview", "para" => true)),
            "status"                  => array("sql" => 1, "name" => "Status", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "select", "values" => $GLOBALS['CONFIG']['USER_STATUS']), "list_function" => array("name" => "get_data_status", "para" => true)),
            "gender"                  => array("sql" => 1, "name" => "Geschlecht", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_gender", "para" => true), "form_input" => array("type" => "select", "values" => $GLOBALS['CONFIG']['ARRAY_GENDER'])),
            "title"                   => array("sql" => 1, "name" => 'Titel', "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "first_name"              => array("sql" => 1, "name" => $translator->trans('first_name'), "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "last_name"               => array("sql" => 1, "name" => $translator->trans('last_name'), "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_last_name", "para" => true)),
            "phone"                   => array("sql" => 1, "name" => "Telefon (privat)", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "slug"                    => array("sql" => 1, "name" => "Permalink", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "group_id"                => array("sql" => 1, "name" => $translator->trans('group'), "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "select", "values" => $this->get_group_box()), "list_function" => array("name" => "get_data_group", "para" => true)),
            "email"                   => array("sql" => 1, "name" => $translator->trans('email'), "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "function", "name" => "get_email_form", "para" => true)),
            "password"                => array("sql" => 0, "name" => $translator->trans('password'), "show" => 0, "edit" => 1, "new" => 1, "search" => 0, "sort" => 0, "nowrap" => 1, "form_input" => array("type" => "password")),
            "pw2"                     => array("sql" => 0, "name" => $translator->trans('repeat_password'), "show" => 0, "edit" => 1, "new" => 1, "search" => 0, "sort" => 0, "nowrap" => 0, "form_input" => array("type" => "password")),
            "fixed_week"              => array("sql" => 1, "name" => "Fixed Week", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_fixed_week", "para" => true), "form_input" => array("type" => "select", "values" => $GLOBALS['CONFIG']['FIXED_WEEK'])),
            "photos"                  => array("sql" => 0, "name" => "Fotos", "show" => 1, "edit" => 0, "new" => 0, "search" => 0, "sort" => 0, "nowrap" => 1, "list_function" => array("name" => "get_data_assets", "para" => true)),
            "newsletter_subscription" => array("sql" => 1, "name" => "Newsletter", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "checkbox", "values" => array(1 => '&nbsp;'))),
            "rating_average"          => array("sql" => 1, "name" => "Rating &#216;", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "function", "name" => "get_rating_average", "para" => true)),
            "rating_1"                => array("sql" => 1, "name" => "Rating 1", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "function", "name" => "get_rating_1", "para" => true)),
            "rating_2"                => array("sql" => 1, "name" => "Rating 2", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "function", "name" => "get_rating_2", "para" => true)),
            "rating_3"                => array("sql" => 1, "name" => "Rating 3", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "function", "name" => "get_rating_3", "para" => true)),
            "medical_specialty_ids"   => array("sql" => 1, "name" => "Fachrichtungen", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_medical_specialty_ids", "para" => true), "form_input" => array("type" => "function", "name" => "get_form_medical_specialty_ids", "para" => true)),
            "treatment_types"         => array("sql" => 1, "name" => "Treatment Types", "show" => 0, "edit" => 1, "new" => 1, "search" => 0, "sort" => 0, "nowrap" => 1, "list_function" => array("name" => "", "para" => true), "form_input" => array("type" => "function", "name" => "get_form_treatment_types", "para" => true)),
            "docinfo_1"               => array("sql" => 1, "name" => 'Info Philosophy', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => ' style="width:300px;height:60px;"')),
            "docinfo_5"               => array("sql" => 1, "name" => 'Info Spezialisierung', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => ' style="width:300px;height:60px;"')),
            "docinfo_6"               => array("sql" => 1, "name" => 'Info Leistungsspektrum', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => ' style="width:300px;height:60px;"')),
            "docinfo_2"               => array("sql" => 1, "name" => 'Info Ausbildung', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => ' style="width:300px;height:60px;"')),
            "docinfo_3"               => array("sql" => 1, "name" => 'Info Sprachen', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => ' style="width:300px;height:60px;"')),
            "docinfo_4"               => array("sql" => 1, "name" => 'Info Auszeichnungen', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => ' style="width:300px;height:60px;"')),
            "info_seo"                => array("sql" => 1, "name" => 'SEO-TEXT', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => ' style="width:300px;height:60px;"')),
            "comment_intern"          => array("sql" => 1, "name" => 'Kommentar <span style="color:#ff0000;">(intern)', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => ' style="width:300px;height:60px;"')),
            "data_source"             => array("sql" => 1, "name" => "Data source", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "has_contract"            => array("sql" => 1, "name" => "Mit Vertrag", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "select", "values" => $GLOBALS['CONFIG']['ARRAY_NO_YES']), "list_function" => array("name" => "get_data_has_contract", "para" => true)),
            "paid_reviews"            => array("sql" => 1, "name" => "Review Service", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "select", "values" => $GLOBALS['CONFIG']['ARRAY_NO_YES']), "list_function" => array("name" => "get_data_paid_reviews", "para" => true)),
            "activated_at"            => array("sql" => 1, "name" => "Aktiv", "show" => 1, "edit" => 1, "new" => 1, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "datetime"), "list_function" => array("name" => "get_activated_at", "para" => true)),
            "created_at"              => array("sql" => 1, "name" => $translator->trans('created_at'), "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "created_by"              => array("sql" => 1, "name" => $translator->trans('created_by'), "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "updated_at"              => array("sql" => 1, "name" => $translator->trans('updated_at'), "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "updated_by"              => array("sql" => 1, "name" => $translator->trans('updated_by'), "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "last_login_at"           => array("sql" => 1, "name" => $translator->trans('last_login_at'), "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
        );

        $BUTTON = array(
            "new"    => array("func" => "user_admin_new", "alt_text" => $translator->trans('Neu'), "script" => $this->admin->getUrl(), "gfx" => "document_new.png", "action" => "new", "th" => 1, "td" => 0),
            "edit"   => array("func" => "user_admin_edit", "alt_text" => $translator->trans('Bearbeiten'), "script" => $this->admin->getUrl(), "gfx" => "document_edit.png", "action" => "edit", "th" => 0, "td" => 1),
            "delete" => array("func" => "user_admin_delete", "alt_text" => $translator->trans('Löschen'), "script" => $this->admin->getUrl(), "gfx" => "delete.png", "action" => "delete", "th" => 0, "td" => 1),
            "search" => array("func" => "user_admin_search", "alt_text" => $translator->trans('Suchen'), "script" => $this->admin->getUrl(), "gfx" => "search.png", "action" => "search", "th" => 1, "td" => 0),
        );

        $TABLE_CONFIG = array(
            "table_name"  => DB_TABLENAME_USERS,
            "alpha_index" => "last_name",
        );

        $URL_PARA = array(
            "page"         => 1,
            "alpha"        => '',
            "sort"         => 'id',
            "direction"    => 'DESC',
            "num_per_page" => 50,
            "type"         => "num"
        );

        $configuration = new Configuration();
        $configuration->menu    = $MENU_ARRAY;
        $configuration->buttons = $BUTTON;
        $configuration->table   = $TABLE_CONFIG;
        $configuration->default_parameters = $URL_PARA;

        $this->configuration = $configuration;
    }

    /**
     * {@inheritdoc}
     */
    public function render()
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $html = '';

        $action = getParam("action", 'list');
        $form_data = getParam("form_data");

        switch ($action) {
            case "edit":
                if (!$this->checkPrivilege('user_admin_edit')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                } else {
                    $id = getParam("id");
                    if (is_array($form_data)) {
                        $html = $this->change_one($action, $id, $form_data, $this->parameters);
                    } else {
                        $html = $this->form_change($action, $id, $this->parameters);
                    }
                }
                break;
            
            case "delete":
                if (!$this->checkPrivilege('user_admin_delete')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                } else {
                    if (!isset($_REQUEST["confirm_id"])) {
                        $id = getParam("id");
                        $html = $this->ask4delete($id, $this->parameters);
                    } else {
                        $confirm_id = getParam("confirm_id");
                        if ($this->delete_one($confirm_id)) {
                            $view->addMessage('STATUS', 'NOTICE', $translator->trans('the_entry_was_deleted_successfully'));
                        } else {
                            $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_could_not_be_deleted'));
                        }
                    }
                }
                break;
            
            case "search":
                if (!$this->checkPrivilege('user_admin_search')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                if (isset($_REQUEST["send_button"])) {
                    $this->parameters["page"] = 1;
                    $this->parameters["alpha"] = '';
                }
                $html = $this->show_search_form($action, $form_data, $this->parameters);
                if (is_array($form_data)) {
                    $html .= $this->get_list($this->parameters, $form_data);
                }
                break;
            
            case "new":
                if (!$this->checkPrivilege('user_admin_new')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                if (is_array($form_data)) {
                    $html = $this->new_one($action, $form_data, $this->parameters);
                } else {
                    $html = $this->form_new($action, $this->parameters);
                }
                break;
        }

        if (empty($html)) {
            if ($this->checkPrivilege('user_admin_view')) {
                $html = $this->get_list($this->parameters);
            }
        }

        return $html;
    }

    /**
     * @return array
     */
    public function get_group_box()
    {
        $db = $this->container->get('database');
        $results = array();

        $db->query('SELECT id, name FROM ' . DB_TABLENAME_GROUPS);
        $num = $db->num_rows();
        for ($i = 1; $i <= $num; $i++) {
            $db->next_record();
            $results[$db->f("id")] = $db->f("name");
        }

        return $results;
    }

    /**
     * @param array $data
     *
     * @return int
     */
    public function get_data_group($data = array())
    {
        return (new Group())->findKey('id="' . $data["group_id"] . '"', 'name');
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_first_name($data = array())
    {
        $_user = new User($data['id']);

        return '<a href="' . $_user->getUrl() . '" target="_blank">' . $_user->getFullName() . '</a>';
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_activated_at($data = array())
    {
        return ($data['activated_at'] == '0000-00-00 00:00:00' ?
            '<span class="red">Nein</span>' :
            '<span class="green">Ja (' . $data['activated_at'] . ')</span>');
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_status($data = array())
    {
        if (isset($GLOBALS['CONFIG']['USER_STATUS'][$data['status']])) {
            return $GLOBALS['CONFIG']['USER_STATUS'][$data['status']];
        } else {
            return 'N/A';
        }
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_gender($data = array())
    {
        return !empty($GLOBALS['CONFIG']['ARRAY_GENDER'][$data['gender']]) ? $GLOBALS['CONFIG']['ARRAY_GENDER'][$data['gender']] : '';
    }

    /**
     * @param array $data
     *
     * @return mixed
     */
    public function get_fixed_week($data = array())
    {
        return $GLOBALS['CONFIG']['FIXED_WEEK'][$data['fixed_week']];
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_assets($data = array())
    {
        $_asset = new Asset();
        $html_assets = '';
        
        $total = $_asset->count('parent_id=0 AND owner_id=' . $data['id'] . ' AND owner_type=' . ASSET_OWNERTYPE_USER);
        
        if ($total == 0) {
            return '<span class="red">' . $total . '</span>';
        } else {
            $assets = $_asset->findObjects('owner_id=' . $data['id'] . ' AND size="' . ASSET_GFX_SIZE_THUMBNAIL . '" AND owner_type=' . ASSET_OWNERTYPE_USER);
            if (!empty($assets)) {
                $html_assets = '<ul class="asset-thumb">';
                /** @var Asset $_asset */
                foreach ($assets as $_asset) {
                    $html_assets .= '<li><img src="' . $_asset->getUrl() . '" title="" /></li>';
                }
                $html_assets .= '</ul>';
            }
            
            return '<span class="green">' . $total . ' <img src="' . getStaticUrl('adm/icon-images.png') . '" style="vertical-align:text-bottom;" class="tooltip" title="' . htmlspecialchars($html_assets) . '" /></span>';
        }
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_form_medical_specialty_ids($data = array())
    {
        $html = '';
        $ids = !empty($data['medical_specialty_ids']) ? $data['medical_specialty_ids'] : '';
        $ids = explode(',', $ids);
        $medical_specialty = new MedicalSpecialty();
        $medical_specialties = $medical_specialty->getMedicalSpecialties();
        $html_rows = '';
        foreach ($medical_specialties as $medical_specialty) {
            $html_rows .= '<tr><td width="20"><input type="checkbox" id="form_data_medical_specialty_ids_' . $medical_specialty->getId() . '" value="' . $medical_specialty->getId() . '" name="form_data[medical_specialty_ids][]" ' . (in_array($medical_specialty->getId(), $ids) ? " checked" : "") . '></td><td><label for="form_data_medical_specialty_ids_' . $medical_specialty->getId() . '">' . $medical_specialty->getName() . '</label></td></tr>';
        }
        $html .= '<table id="medical_specialties" class="table table-striped">';
        $html .= '<tr class="headers"><th colspan="2">Fachrichtungen</th></tr>';
        $html .= $html_rows;
        $html .= "</table>\n";

        return $html;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_medical_specialty_ids($data = array())
    {
        $medical_specialty = new MedicalSpecialty();

        return getCategoriesText($data['medical_specialty_ids'], $medical_specialty->getMedicalSpecialtyValues(), '<br />');
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_form_treatment_types($data = array())
    {
        // Something went wrong. Don't proceeed
        if (!isset($data['id'])) {
            return '';
        }

        // Container for queries
        $ms   = new MedicalSpecialty();
        $tt   = new TreatmentType();
        $user = new User($data['id']);

        $all_medical_specialties = $ms->getMedicalSpecialties();
        $all_treatment_types = array();

        // Get the TT, but sort them by parent ID.
        // We do it this way because we want the TT ID in the key
        /** @var TreatmentType $treatment_type */
        foreach ($tt->findObjects('status=' . TreatmentType::STATUS_ACTIVE) as $treatment_type) {
            $all_treatment_types[$treatment_type->getMedicalSpecialtyId()][$treatment_type->getId()] = $treatment_type;
        }

        $user_ms = $user->getMedicalSpecialties();
        $user_tt = $user->getTreatmentTypes();

        $html      = '';
        $html_rows = '';

        foreach ($user_ms as $ms) {
            $treatment_types = $ms->getTreatmentTypes();

            $html_rows .= '<tr data-msid="' . $ms->getId() . '"><td width="20"><div>';
            $html_rows .= '<h3>' . $ms->getName() . '</h3>';

            $html_rows .= '<ul>';

            foreach ($treatment_types as $tt) {
                // The user has the TT if the array key is in the user's TT array
                $checked = array_key_exists($tt->getId(), $user_tt) ? ' checked' : '';

                $html_rows .= '<li style="list-style: none;"><input type="checkbox" id="form_data_treatment_type_ids_' . $tt->getId() . '" value="' . $tt->getId() . '" name="form_data[treatment_type_ids][' . $ms->getId() . '][]" ' . $checked . '><span>' . $tt->getName() . '</span></li>';
            }
            $html_rows .= '</ul></div></td></tr>';
        }
        $html .= '<table class="table table-striped" id="treatment_types">';
        $html .= '<thead><tr class="headers"><th colspan="2">Treatment Types</th></tr></thead>';
        $html .= '<tbody>';
        $html .= $html_rows;
        $html .= "</tbody></table>\n";

        // Convert the objects into arrays for the JS
        $ms_json = array();
        /** @var MedicalSpecialty $ms */
        foreach ($all_medical_specialties as $ms) {
            $ms_json[$ms->getId()] = $ms->toArray();
        }
        $ms_json = json_encode($ms_json);

        $tt_json = array();
        foreach ($all_treatment_types as $msid => $tt_group) {
            /** @var TreatmentType $tt */
            foreach ($tt_group as $ttid => $tt) {
                $tt_json[$msid][$ttid] = $tt->toArray();
            }
        }
        $tt_json = json_encode($tt_json);

        $selected_tt_json = array();
        foreach($selected_tt_json as $tt) {
            $selected_tt_json[$tt->getId()] = $tt->toArray();
        }
        $selected_tt_json = json_encode($selected_tt_json);

        $html
            .= <<<SCRIPT

        <script>
        AT._defer(function(){

            var all_ms = {$ms_json},
                all_tt = {$tt_json},
                selected_tt = {$selected_tt_json};
                
            // Tables
            var \$ms_container = \$('#medical_specialties'),
                \$tt_container = \$('#treatment_types');
            
            var addTreatmentTypes = function(msid) {
                // If there's no TT for this MS, just return
                if (all_tt[msid] === undefined) {
                    return;
                }
                
                var html = '<tr data-msid="' + msid + '"><td width="20"><div><h3>' + all_ms[msid]['name'] + '</h3><ul>';
                
                for (tt in all_tt[msid]) {
                    if (all_tt[msid].hasOwnProperty(tt)) {
                        html += '<li style="list-style: none;"><input type="checkbox" name="form_data[treatment_type_ids][' + msid + '][]" value="' + tt + '" id="form_data_treatment_type_ids_' + tt + '"';
                        
                        // Returns index of hit, so -1 means "not found"
                        if ($.inArray(tt, selected_tt) > -1) {
                            html += ' checked="checked"';
                        }
                        
                        html += '><span>' + all_tt[msid][tt]['name'] + '</span></li>';
                    }
                }
                
                html += '</ul></div></td></tr>';
                
                \$tt_container.find('tbody').append(html);
            };
            
            var removeTreatmentTypes = function(msid) {
                \$tt_container.find('tr').filter('[data-msid="' + msid + '"]').remove();
            }
        
            \$('#form_row_medical_specialty_ids').find('input').change(function() {
                var \$this = \$(this);
                // The checked value is the *new* value
                if (\$this.is(':checked')) {
                    addTreatmentTypes(\$this.val());
                } else {
                    removeTreatmentTypes(\$this.val());
                }
            });
        });
        </script>
SCRIPT;

        return $html;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_last_name($data = array())
    {
        if (!empty($data['slug']) && !empty($data['last_name'])) {
            return '<a href="' . $this->admin->generateUrl('doctor_profile_no_insurance', array('slug' => $data['slug'])) . '" target="_blank">' . $data['last_name'] . '</a>';
        }

        return $data['last_name'];
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_form_googlepreview($data = array())
    {
        $_user = new User();
        
        if (!empty($data['id'])) {
            $_user->load($data['id']);
        }
        
        $title = '';
        $description = '';
        $url_base = '';
        if ($_user->isValid()) {
            $title = $_user->getHtmlDefaultTitle();
            $description = $_user->getHtmlDefaultMetaDescription();
            $url_base = $_user->getPermalink();
            $url_base = str_replace($_user->getSlug(), '', $url_base);
            $url_base = str_replace('http://', '', $url_base);
        }

        $html
            = <<<EOF
<div id="googlepreview" class="google-result">
	<div class="title"></div>
	<div class="url"></div>
	<div class="description"></div>
</div>
<script type="text/javascript">
var AT = AT || {};
AT._defer(function(){

var input_html_title_default = '$title';
var input_html_meta_description_default = '$description';

function update_googlepreview() {
	var input_html_title = $('#form_data_html_title').val();
	if (input_html_title.length < 1) { input_html_title = input_html_title_default; }
	$('#googlepreview .title').html(input_html_title);

	var input_html_meta_description = $('#form_data_html_meta_description').val();
	if (input_html_meta_description.length < 1) { input_html_meta_description = input_html_meta_description_default; }
	$('#googlepreview .description').html(input_html_meta_description);

	var input_slug = $('#form_data_slug').val();
	if (input_slug.length < 1) { input_slug = ''; }
	$('#googlepreview .url').html('$url_base' + input_slug);
};

$(document).ready(function() {
	update_googlepreview();

	$('#form_data_html_title').keyup(function() {
		update_googlepreview();
		return false;
	});
	$('#form_data_html_meta_description').keyup(function() {
		update_googlepreview();
		return false;
	});
	$('#form_data_slug').keyup(function() {
		update_googlepreview();
		return false;
	});
});

});
</script>
EOF;

        return $html;
    }

    /**
     * @param array $data
     *
     * @return mixed
     */
    public function get_data_has_contract($data = array())
    {
        return $GLOBALS["CONFIG"]["ARRAY_NO_YES"][$data['has_contract']];
    }

    /**
     * @param array $data
     *
     * @return mixed
     */
    public function get_data_paid_reviews($data = array())
    {
        return $GLOBALS["CONFIG"]["ARRAY_NO_YES"][$data['paid_reviews']];
    }



    /**
     * @param array $data
     *
     * @return string
     */
    public function get_email_form($data = array())
    {
        $email = empty($data['email']) ? '' : $data['email'];
        return '<input type="text" id="form_data_email" class="form_text" name="form_data[email]" value="' . $email .'" autocomplete="off">';
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_rating_average($data = array())
    {
        $rating = isset($data['rating_average']) ? $data['rating_average'] : 0;
        return '<input name="form_data[rating_average]" type="number" max="5.0" min="0.0" step="0.1" value="' . $rating . '"/>';
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_rating_1($data = array())
    {
        $rating = isset($data['rating_1']) ? $data['rating_1'] : 0;
        return '<input name="form_data[rating_1]" type="number" max="5.0" min="0.0" step="0.1" value="' . $rating . '"/>';
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_rating_2($data = array())
    {
        $rating = isset($data['rating_2']) ? $data['rating_2'] : 0;
        return '<input name="form_data[rating_2]" type="number" max="5.0" min="0.0" step="0.1" value="' . $rating . '"/>';
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_rating_3($data = array())
    {
        $rating = isset($data['rating_3']) ? $data['rating_3'] : 0;
        return '<input name="form_data[rating_3]" type="number" max="5.0" min="0.0" step="0.1" value="' . $rating . '"/>';
    }

    /**
     * @param array $URL_PARA
     * @param array $search
     *
     * @return string
     */
    public function get_list($URL_PARA, $search = array())
    {
        return $this->admin->admin_get_list(0, $search, $URL_PARA);
    }

    /**
     * @param string $action
     * @param array $form_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function new_one($action, $form_data, $URL_PARA)
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $profile = $this->container->get('current_user');
        $html = '';

        // 1. set defaults
        $form_data["created_by"] = $profile->getId();
        $form_data["created_at"] = (new DateTime())->getDateTime();

        // 2. check
        if (1 > mb_strlen(sanitize_title($form_data["last_name"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
            $view->addMessage('admin_message_last_name', 'ERROR', $translator->trans('Bitte geben Sie einen Nachnamen an'));
        }
        if ($form_data["password"] != $form_data["pw2"]) {
            $view->addMessage('admin_message_password', 'ERROR', $translator->trans('passwords_are_not_equal'));
            // clear passwords
            $form_data["password"] = $form_data["pw2"] = "";
        }
        if (1 > mb_strlen(sanitize_slug($form_data["slug"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
            $view->addMessage('admin_message_slug', 'ERROR', $translator->trans('Der Permalink ist nicht gültig!'));
        } else {
            $_slug = sanitize_slug($form_data["slug"]);
            $_user = new User();
            $_total = $_user->count('slug="' . sqlsave($_slug) . '"');
            if ($_total > 0) {
                $view->addMessage('admin_message_slug', 'ERROR', 'Der Permalink ist bereits vergeben!');
            }
        }

        // if there are checkboxes than set the keys to make sure that deselected checkboxes are beeing set
        $form_data = $this->admin->set_unselected_checkbox_values($form_data);

        $_medical_specialty_ids = '';
        if (isset($form_data['medical_specialty_ids']) && is_array($form_data['medical_specialty_ids'])) {
            $_medical_specialty_ids = implode(',', $form_data['medical_specialty_ids']);
        }
        $form_data['medical_specialty_ids'] = $_medical_specialty_ids;

        // 3. exit on errors
        if ($view->countMessages()) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('Bitte prüfen Sie Ihre Eingaben.'));
            return $this->form_change($action, 0, $URL_PARA, $form_data);
        }
        // 4. sanitize
        $form_data["title"] = sanitize_title($form_data["title"]);
        $form_data["first_name"] = sanitize_title($form_data["first_name"]);
        $form_data["last_name"] = sanitize_title($form_data["last_name"]);
        $form_data["slug"] = sanitize_slug($form_data["slug"]);

        $_id = $this->admin->admin_set_new($form_data);
        //inserting doctor's treatment types to doctors_treatment_types table.

        // The user
        $user = new User($_id);

        //updating doctor's treatment types.
        if ($user->isValid() && isset($form_data["treatment_type_ids"])) {
            $user->saveTreatmentTypes(array_flatten($form_data["treatment_type_ids"], []));
        }

        if ($_id > 0) {
            if ($form_data['password'] != '') {
                $_user = new User($_id);
                if ($_user->isValid() && $_user->changePassword($form_data['password'])) {
                    $view->addMessage('STATUS', 'NOTICE', $translator->trans('action_was_successfully'));
                } else {
                    $view->addMessage('STATUS', 'ERROR', 'Das Passwort konnte nicht gesetzt werden.');
                }
            } else {
                $view->addMessage('STATUS', 'NOTICE', $translator->trans('action_was_successfully'));
            }
        } elseif ($_id == -1) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_allready_exists'));
            $html = $this->form_new($action, $URL_PARA, $form_data);
        } elseif ($_id == -2) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('name_of_primary_key_is_empty'));
            $html = $this->form_new($action, $URL_PARA, $form_data);
        } else {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_could_not_be_created'));
        }

        return $html;
    }

    /**
     * @param string $action
     * @param int $id
     * @param array $form_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function change_one($action, $id, $form_data, $URL_PARA)
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $profile = $this->container->get('current_user');
        $html = '';

        // 1. set defaults
        $form_data["updated_by"] = $profile->getId();
        $form_data["updated_at"] = (new DateTime())->getDateTime();
        if($form_data["activated_at"] == '0000-00-00 00:00:00') {
            unset($form_data["activated_at"]);
        }

        // 2. check
        if (1 > mb_strlen(sanitize_title($form_data["last_name"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
            $view->addMessage('admin_message_last_name', 'ERROR', $translator->trans('Bitte geben Sie einen Nachnamen an'));
        }
        if ($form_data["password"] != $form_data["pw2"]) {
            $view->addMessage('admin_message_password', 'ERROR', $translator->trans('passwords_are_not_equal'));
            // clear passwords
            $form_data["password"] = $form_data["pw2"] = "";
        }
        if (1 > mb_strlen(sanitize_slug($form_data["slug"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
            $view->addMessage('admin_message_slug', 'ERROR', $translator->trans('Der Permalink ist nicht gültig!'));
        } else {
            $_slug = sanitize_slug($form_data["slug"]);
            $_user = new User();
            $_total = $_user->count('slug="' . sqlsave($_slug) . '" AND id!=' . $id);
            if ($_total > 0) {
                $view->addMessage('admin_message_slug', 'ERROR', 'Der Permalink ist bereits vergeben!');
            }
        }

        // if there are checkboxes than set the keys to make sure that deselected checkboxes are beeing set
        $form_data = $this->admin->set_unselected_checkbox_values($form_data);

        $_medical_specialty_ids = '';
        if (isset($form_data['medical_specialty_ids']) && is_array($form_data['medical_specialty_ids'])) {
            $_medical_specialty_ids = implode(',', $form_data['medical_specialty_ids']);
        }
        $form_data['medical_specialty_ids'] = $_medical_specialty_ids;

        // 3. exit on errors
        if ($view->countMessages()) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('Bitte prüfen Sie Ihre Eingaben.'));
            return $this->form_change($action, $id, $URL_PARA, $form_data);
        }
        
        // 4. sanitize
        $form_data["title"] = sanitize_title($form_data["title"]);
        $form_data["first_name"] = sanitize_title($form_data["first_name"]);
        $form_data["last_name"] = sanitize_title($form_data["last_name"]);
        $form_data["slug"] = sanitize_slug($form_data["slug"]);

        $_result = $this->admin->admin_set_update($id, $form_data);

        //updating doctor's treatment types.

        // The user
        $user = new User($id);

        //updating doctor's treatment types.
        if ($user->isValid() && isset($form_data["treatment_type_ids"])) {
            $user->saveTreatmentTypes(array_flatten($form_data["treatment_type_ids"], []));
        }

        if ($_result == true) {
            if ($user->isValid()) {
                // update the profile_asset_filename
                $user->updateProfileAssetFilename();

                // check if the password should change
                if ($form_data['password'] != '') {
                    if ($user->changePassword($form_data['password'])) {
                        $view->addMessage('STATUS', 'NOTICE', $translator->trans('action_was_successfully'));
                    } else {
                        $view->addMessage('STATUS', 'ERROR', 'Das Passwort konnte nicht geändert werden.');
                    }
                } else {
                    $view->addMessage('STATUS', 'NOTICE', $translator->trans('action_was_successfully'));
                }
            }
        } else {
            $view->addMessage('STATUS', 'WARNING', $translator->trans('no_data_has_been_updated'));

            $html = $this->form_change($action, $id, $URL_PARA, $form_data);
        }

        return $html;
    }

    /**
     * @param int $id
     * @param array $URL_PARA
     *
     * @return string
     */
    public function ask4delete($id, $URL_PARA)
    {
        return $this->admin->admin_get_confirm_table($id, $URL_PARA, 'delete');
    }

    /**
     * @param int $id
     *
     * @return bool|int
     */
    public function delete_one($id)
    {
        $user = new User($id);
        if (!$user->isValid()) {
            return false;
        }

        return $user->delete();
    }

    /**
     * @param string $action
     * @param array $URL_PARA
     * @param array $error_data
     *
     * @return string
     */
    public function form_new($action, $URL_PARA, $error_data = array())
    {
        return $this->admin->admin_get_form($action, 0, $error_data, $URL_PARA);
    }

    /**
     * @param string $action
     * @param int $id
     * @param array $URL_PARA
     * @param array $error_data
     *
     * @return string
     */
    public function form_change($action, $id, $URL_PARA, $error_data = array())
    {
        $html = $this->admin->admin_get_form($action, $id, $error_data, $URL_PARA);

        $user = new User($id);
        if ($user->isValid()) {
            $html .= get_gallery($user, 'user', ASSET_OWNERTYPE_USER);
        }

        $this->getView()->setFocus('#form_data_email');

        return $html;
    }

    /**
     * @param string $action
     * @param array $search_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function show_search_form($action, $search_data, $URL_PARA)
    {
        return $this->admin->admin_get_form($action, 0, $search_data, $URL_PARA);
    }
}