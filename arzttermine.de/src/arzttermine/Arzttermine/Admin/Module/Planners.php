<?php

namespace Arzttermine\Admin\Module;

use Arzttermine\Admin\Configuration;
use Arzttermine\Admin\Module;
use Arzttermine\Appointment\AppointmentGeneratorService;
use Arzttermine\Appointment\Planner;
use Arzttermine\DataTransformer\PlannerBlockAppointments;
use Arzttermine\Location\Location;
use Arzttermine\User\User;

final class Planners extends Module {

    /**
     * @var string
     */
    protected $title = 'Planner Admin';

    /**
     * @var string
     */
    protected $role = 'integration_admin';

    /**
     * @var string
     */
    protected $slug = 'planners';

    /**
     * {@inheritdoc}
     */
    public function boot()
    {
        $this->configuration = new Configuration();
    }

    /**
     * {@inheritdoc}
     */
    public function render()
    {
        $request = $this->container->get('request');
        $view = $this->getView();

        // Bootstrap planner
        $planner = new Planner();
        $location = new Location($request->getQuery('location_id', 0));
        $user = new User($request->getQuery('user_id', 0));

        $planner = $planner->getForUserLocation($user, $location);
        $locations = $location->findObjects('integration_id = 1 ORDER BY name'); // @todo: Fix the hardcoded-ness
        $users = $location->getMembers();

        $view->setRefs(array(
            'doctor' => $user,
            'practice' => $location,
            'users' => $users,
            'locations' => $locations,
            'planner' => $planner
        ));

        // Save the planner if we need to
        if ($request->isPost()) {
            $payload = $request->getRequest('payload');
            $blocks = json_decode($payload, true); // Everything is wrapped in here to avoid input var count errors

            $planner_blocks = PlannerBlockAppointments::toBlocks($blocks);

            $planner
                ->clearBlocks()
                ->setBlocks($planner_blocks)
                ->saveBlocks();

            // Set and save appointment rules
            $planner
                ->setLeadIn($request->getRequest('lead_in'))
                ->setSearchPeriod($request->getRequest('search_period'))
                ->save(array('lead_in', 'search_period'));

            // Generate if needed
            if ($request->getRequest('generate')) {
                AppointmentGeneratorService::getInstance()->clear($planner)->generate($planner);
            }
        }

        $view->setRef('blocks', PlannerBlockAppointments::toFullCalendar($planner->getBlocks())); // Set it here to prevent double-load

        return $view->fetch('admin/planner/planner.tpl');
    }

    

}