<?php

namespace Arzttermine\Admin\Module;

use Arzttermine\Admin\Configuration;
use Arzttermine\Admin\Module;
use Arzttermine\Core\DateTime;
use Arzttermine\Mail\Contact;
use Arzttermine\User\User;

final class Mailings extends Module {

    /**
     * @var string
     */
    protected $title = 'Mailings Admin';

    /**
     * @var string
     */
    protected $role = 'mailing_admin';

    /**
     * @var string
     */
    protected $slug = 'mailings';

    /**
     * {@inheritdoc}
     */
    public function boot()
    {
        $translator = $this->container->get('translator');

        $MENU_ARRAY = array(
            "id"               => array("sql" => 1, "name" => "ID", "show" => 1, "edit" => 0, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "status"           => array("sql" => 1, "name" => "Status", "show" => 1, "edit" => 1, "new" => 1, "search" => 0, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_status_name", "para" => true), "list_function" => array("name" => "get_status_name", "para" => true)),
            "mailto"           => array("sql" => 1, "name" => 'An', "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "none_htmlentities")),
            "mailcc"           => array("sql" => 1, "name" => 'CC', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "none_htmlentities")),
            "mailbcc"          => array("sql" => 1, "name" => 'BCC', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "none_htmlentities")),
            "subject"          => array("sql" => 1, "name" => 'Betreff', "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "none")),
            "content_id"       => array("sql" => 1, "name" => 'Content_id', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "none")),
            "content_filename" => array("sql" => 1, "name" => 'Content_filename', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "none")),
            "body_text"        => array("sql" => 1, "name" => 'Mailtext', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_body_text_form", "para" => true)),
            "created_at"       => array("sql" => 1, "name" => $translator->trans('created_at'), "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "none")),
            "created_by"       => array("sql" => 1, "name" => $translator->trans('created_by'), "show" => 0, "edit" => 0, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_created_by_name", "para" => true), "list_function" => array("name" => "get_created_by_name", "para" => true)),
        );
        $BUTTON = array(
            "edit"   => array("func" => "mailing_admin_edit", "alt_text" => $translator->trans('edit_item'), "script" => $this->admin->getUrl(), "gfx" => "document_edit.png", "action" => "edit", "th" => 0, "td" => 1),
            "search" => array("func" => "mailing_admin_search", "alt_text" => $translator->trans('search'), "script" => $this->admin->getUrl(), "gfx" => "search.png", "action" => "search", "th" => 1, "td" => 0),
        );
        $TABLE_CONFIG = array(
            "table_name"  => DB_TABLENAME_MAILINGS,
            "alpha_index" => "mailto",
        );
        $URL_PARA = array(
            'page'         => 1,
            'alpha'        => '',
            "sort"         => 0,
            "direction"    => 'DESC',
            "num_per_page" => 25,
            "type"         => 'num'
        );

        $configuration = new Configuration();
        $configuration->menu    = $MENU_ARRAY;
        $configuration->buttons = $BUTTON;
        $configuration->table   = $TABLE_CONFIG;
        $configuration->default_parameters = $URL_PARA;

        $this->configuration = $configuration;
    }

    /**
     * {@inheritdoc}
     */
    public function render()
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $html = '';

        $action = getParam("action", 'list');
        $form_data = getParam("form_data");

        switch ($action) {
            case "edit":
                if (!$this->checkPrivilege('mailing_admin_edit')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                } else {
                    $id = getParam("id");
                    if (is_array($form_data)) {
                        $html = $this->change_one($action, $id, $form_data, $this->parameters);
                    } else {
                        $html = $this->form_change($action, $id, $this->parameters);
                    }
                }
                break;
            
            case "delete":
                if (!$this->checkPrivilege('mailing_admin_delete')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                } else {
                    if (!isset($_REQUEST["confirm_id"])) {
                        $id = getParam("id");
                        $html = $this->ask4delete($id, $this->parameters);
                    } else {
                        $confirm_id = getParam("confirm_id");
                        if ($this->delete_one($confirm_id)) {
                            $view->addMessage('STATUS', 'NOTICE', $translator->trans('the_entry_was_deleted_successfully'));
                        } else {
                            $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_could_not_be_deleted'));
                        }
                    }
                }
                break;
            
            case "search":
                if (!$this->checkPrivilege('mailing_admin_search')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                if (isset($_REQUEST["send_button"])) {
                    $this->parameters["page"] = 1;
                    $this->parameters["alpha"] = '';
                }
                $html = $this->show_search_form($action, $form_data, $this->parameters);
                if (is_array($form_data)) {
                    $html .= $this->get_list($this->parameters, $form_data);
                }
                break;
            
            case "new":
                if (!$this->checkPrivilege('mailing_admin_new')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                if (is_array($form_data)) {
                    $html = $this->new_one($action, $form_data, $this->parameters);
                } else {
                    $html = $this->form_new($action, $this->parameters);
                }
                break;
        }

        if (empty($html)) {
            if ($this->checkPrivilege('mailing_admin_view')) {
                $html = $this->get_list($this->parameters);
            }
        }

        return $html;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_created_by_name($data = array())
    {
        $user = new User();
        
        if (!empty($data['created_by'])) {
            $user->load($data['created_by']);
        }

        return $user->getFullName();
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_body_text_form($data)
    {
        if (empty($data['body_text'])) {
            return '';
        }
        
        return str_replace("\n", "<br>", $data["body_text"]);
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_status_name($data = array())
    {
        if (isset($GLOBALS['CONFIG']['MAILING_STATUS'][$data['status']])) {
            return $GLOBALS['CONFIG']['MAILING_STATUS'][$data['status']];
        } else {
            return 'N/A';
        }
    }

    /**
     * @param array $URL_PARA
     * @param array $search
     *
     * @return string
     */
    public function get_list($URL_PARA, $search = array())
    {
        return $this->admin->admin_get_list(0, $search, $URL_PARA);
    }

    /**
     * @param string $action
     * @param array $form_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function new_one($action, $form_data, $URL_PARA)
    {
        $profile = $this->container->get('current_user');
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $html = '';

        // 1. set defaults
        $form_data["updated_by"] = $profile->getId();
        $form_data["updated_at"] = (new DateTime())->getDateTime();

        $_id = $this->admin->admin_set_new($form_data);
        if ($_id > 0) {
            $view->addMessage('STATUS', 'NOTICE', $translator->trans('action_was_successfully'));
        } elseif ($_id == -1) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_allready_exists'));
            $html = $this->form_new($action, $URL_PARA, $form_data);
        } elseif ($_id == -2) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('name_of_primary_key_is_empty'));
            $html = $this->form_new($action, $URL_PARA, $form_data);
        } else {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_could_not_be_created'));
        }

        return $html;
    }

    /**
     * Currently read only
     * 
     * @return string
     */
    public function change_one()
    {
        return '';
    }

    /**
     * @param int $id
     * @param array $URL_PARA
     *
     * @return string
     */
    public function ask4delete($id, $URL_PARA)
    {
        return $this->admin->admin_get_confirm_table($id, $URL_PARA, 'delete');
    }

    /**
     * @param int $id
     *
     * @return int
     */
    public function delete_one($id)
    {
        return $this->admin->admin_set_delete($id);
    }

    /**
     * @param int $action
     * @param array $URL_PARA
     * @param array $error_data
     *
     * @return string
     */
    public function form_new($action, $URL_PARA, $error_data = array())
    {
        return $this->admin->admin_get_form($action, 0, $error_data, $URL_PARA);
    }

    /**
     * @param string $action
     * @param int $id
     * @param array $URL_PARA
     * @param array $error_data
     *
     * @return string
     */
    public function form_change($action, $id, $URL_PARA, $error_data = array())
    {
        return $this->admin->admin_get_form($action, $id, $error_data, $URL_PARA);
    }

    /**
     * @param string $action
     * @param int $id
     * @param array $URL_PARA
     * @param array $error_data
     *
     * @return string
     */
    public function form_mail($action, $id, $URL_PARA, $error_data = array())
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        
        if (empty($error_data)) {
            $error_data = array();

            $_contact = new Contact($id);
            if (!$_contact->isValid()) {
                $view->addMessage('STATUS', 'ERROR', $translator->trans('Falsche ID!'));

                return '';
            }
            $error_data["mailtext"] .= "\n\n-------------------------------------------------------------------\nAm " . $_contact->getCreatedAt('d.m.Y / H:i:s') . " schrieb " . $_contact->getName() . " (" . $_contact->getEmail() . "):\n" . $error_data["mailtext"];
            $error_data["mailtext"] .= "\n" . $_contact->getMessage();
        }

        return $this->admin->admin_get_form($action, $id, $error_data, $URL_PARA);
    }

    /**
     * @param string $action
     * @param array $search_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function show_search_form($action, $search_data, $URL_PARA)
    {
        return $this->admin->admin_get_form($action, 0, $search_data, $URL_PARA);
    }
}