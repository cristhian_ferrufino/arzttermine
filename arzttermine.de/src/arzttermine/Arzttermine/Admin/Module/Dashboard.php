<?php

namespace Arzttermine\Admin\Module;

use Arzttermine\Admin\Configuration;
use Arzttermine\Admin\Message;
use Arzttermine\Admin\Module;
use Arzttermine\Blog\Blog;
use Arzttermine\Booking\Booking;
use Arzttermine\Mail\Contact;
use Arzttermine\User\User;

final class Dashboard extends Module {

    /**
     * @var string
     */
    protected $title = 'Dashboard';

    /**
     * @var string
     */
    protected $role = 'system_internal_group';

    /**
     * @var string
     */
    protected $slug = 'dashboard';

    /**
     * {@inheritdoc}
     */
    public function boot()
    {
        $this->configuration = new Configuration();
    }

    /**
     * {@inheritdoc}
     */
    public function render()
    {
        $profile = $this->container->get('current_user');
        $view = $this->getView();

        /**
         * ********************************************************
         * Bookings
         */
        if ($profile->checkPrivilege('booking_admin')) {
            $_booking = new Booking();
            $bookingsCount = $_booking->count('1=1');
            $bookingsCountNew = $_booking->count('status=' . Booking::STATUS_NEW);
            $bookingsCountPending = $_booking->count('status=' . Booking::STATUS_PENDING);

            $view
                ->setRef('showBookings', true)
                ->setRef('bookingsCount', $bookingsCount)
                ->setRef('bookingsCountNew', $bookingsCountNew)
                ->setRef('bookingsCountPending', $bookingsCountPending);
        }

        /**
         * ********************************************************
         * Online Users
         */
        if ($profile->checkPrivilege('user_admin')) {
            $user = new User();
            $usersCount = $user->count('1=1');
            $usersHaveContract = $user->count('has_contract=1');
            $usersOnlineCount = $user->count('last_login_at >= (NOW() - INTERVAL 1 DAY) AND GROUP_ID='.GROUP_DOCTOR);
            $users = $user->findObjects('GROUP_ID='.GROUP_DOCTOR.' ORDER BY last_login_at DESC LIMIT 10');

            $view
                ->setRef('showUsers', true)
                ->setRef('usersCount', $usersCount)
                ->setRef('usersHaveContract', $usersHaveContract)
                ->setRef('usersOnlineCount', $usersOnlineCount)
                ->setRef('users', $users);
        }

        /**
         * ********************************************************
         * CONTACT
         */
        if ($profile->checkPrivilege('contact_admin')) {
            $_contact = new Contact();
            $contactsCount = $_contact->count('1=1');

            $_contact = $_contact->findObject('1=1 ORDER BY created_at DESC');
            if ($_contact->isValid()) {
                $url = $_contact->getAdminEditUrl();
                $subject = $_contact->getSubject(20);
                $createdAtDiff = human_time_diff(strtotime($_contact->getCreatedAt()));
                $createdAtDate = $_contact->getCreatedAt();
                $message = $_contact->getMessage(20);
                $view
                    ->setRef('showContacts', true)
                    ->setRef('url', $url)
                    ->setRef('subject', $subject)
                    ->setRef('createdAtDiff', $createdAtDiff)
                    ->setRef('createdAtDate', $createdAtDate)
                    ->setRef('message', $message)
                    ->setRef('subject', $subject);
            }
        }
        return $view->fetch('admin/dashboard/dashboard.tpl');
    }
}