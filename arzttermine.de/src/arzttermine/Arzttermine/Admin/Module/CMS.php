<?php

namespace Arzttermine\Admin\Module;

use Arzttermine\Admin\Configuration;
use Arzttermine\Admin\Module;
use Arzttermine\Cms\CmsContent;
use Arzttermine\Cms\CmsDir;
use Arzttermine\Cms\CmsForm;
use Arzttermine\Cms\CmsVersion;
use Arzttermine\Cms\CmsView;
use Arzttermine\Core\DateTime;

final class CMS extends Module {

    /**
     * @var string
     */
    protected $title = 'CMS';

    /**
     * @var string
     */
    protected $role = 'cms_admin';

    /**
     * @var string
     */
    protected $slug = 'cms';

    /**
     * {@inheritdoc}
     */
    public function boot()
    {
        $this->configuration = new Configuration();
    }

    /**
     * {@inheritdoc}
     */
    public function render()
    {
        $profile = $this->container->get('current_user');
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $now = (new DateTime())->getDateTime();
        $html = '';

        $select_dir_id = getParam('select_dir_id');
        if (!is_numeric($select_dir_id)) {
            $select_dir_id = getParam('dir_id', CmsDir::ROOT_DIR_ID);
        }
        if (!is_numeric($select_dir_id)) {
            $select_dir_id = CmsDir::ROOT_DIR_ID;
        }

        $action = getParam('a', 'explorer_content');
        $status = getParam('s');
        $form = getParam('form');

        switch ($action) {
            /* DIR ****************************************************** */
            case "dir_new":
                if (!$this->checkPrivilege('cms_admin_dir_new')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    $action = 'explorer_content';
                    break;
                }
                if ($status == 'ok') {
                    // save NEW dir ****************************************************
                    $form = CmsForm::checkDirNew($form);
                    
                    if ($view->countMessages()) {
                        $view->addMessage('STATUS', 'ERROR', $translator->trans('Bitte prüfen Sie Ihre Eingaben.'));
                        $html = CmsForm::getDirNew($form, CmsDir::ROOT_DIR_ID);
                    } else {
                        $form['created_at'] = $now;
                        $form['created_by'] = $profile->getId();
                        $form['updated_at'] = $now;
                        $form['updated_by'] = $profile->getId();
                        $dir = new CmsDir();
                        $_new_dir_id = $dir->saveNew($form);

                        if (!$_new_dir_id) {
                            $view->addMessage('STATUS', 'ERROR', $translator->trans('could_not_create_new_dir'));
                        } else {
                            $view->addMessage('STATUS', 'NOTICE', $translator->trans('dir_created_successfully'));
                        }
                        $select_dir_id = $_new_dir_id;
                        $action = 'explorer_content';
                    }
                } else {
                    // set defaults
                    $html = CmsForm::getDirNew($form, $select_dir_id);
                }
                break;

            case "dir_edit":
                if (!$this->checkPrivilege('cms_admin_dir_edit')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    $action = 'explorer_content';
                    break;
                }
                if ($status == 'ok') {
                    // save ****************************************************
                    $form = CmsForm::checkDirEdit($form);
                    if ($view->countMessages()) {
                        $view->addMessage('STATUS', 'ERROR', $translator->trans('Bitte prüfen Sie Ihre Eingaben.'));
                        $html = CmsForm::getDirEdit($form);
                    } else {
                        $_dir_id = $form['id'];
                        $_dir = new CmsDir($_dir_id);
                        if ($_dir->isInDir($form['parent_id'])) {
                            // check if the user wanted to hang this dir to itself or a subtree of itself
                            $view->addMessage('STATUS', 'ERROR', $translator->trans('can_not_move_the_dir_to_this_position'));
                            $select_dir_id = $_dir_id;
                            $action = 'explorer_content';
                            break;
                        }
                        $_dir->updated_at = $now;
                        $_dir->updated_by = $profile->getId();
                        $_dir->parent_id = $form['parent_id'];
                        $_save_data = array('updated_at', 'updated_by', 'parent_id');
                        $_dir->updateMetasFromForm($form);
                        $_result = $_dir->save($_save_data, true);

                        if (!$_result) {
                            $view->addMessage('STATUS', 'ERROR', $translator->trans('could_not_edit_dir'));
                        } else {
                            $view->addMessage('STATUS', 'NOTICE', $translator->trans('dir_edited_successfully'));
                        }
                        $select_dir_id = $_dir_id;
                        $action = 'explorer_content';
                    }
                } else {
                    // set defaults
                    $dir = new CmsDir($select_dir_id);
                    $form = $dir->getFormData();
                    $html = CmsForm::getDirEdit($form);
                }
                break;

            case "dir_delete":
                if (!$this->checkPrivilege('cms_admin_dir_delete')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    $action = 'explorer_content';
                    break;
                }
                if ($status == 'ok') {
                    // delete ****************************************************
                    $_dir = new CmsDir($form['id']);
                    if (!$_dir->isValid()) {
                        $view->addMessage('STATUS', 'ERROR', $translator->trans('could_not_delete_dir'));
                    } else {
                        $_parent_dir_id = $_dir->parent_id;
                        if (!$_dir->delete()) {
                            $view->addMessage('STATUS', 'ERROR', $translator->trans('could_not_delete_dir'));
                        } else {
                            $view->addMessage('STATUS', 'NOTICE', $translator->trans('dir_deleted_successfully'));
                            $_select_dir_id = $_parent_dir_id;
                        }
                    }
                    $action = 'explorer_content';
                } else {
                    // set defaults
                    $_dir = new CmsDir(getParam('dir_id'));
                    if (!$_dir->isValid()) {
                        $view->addMessage('STATUS', 'ERROR', $translator->trans('there_is_no_dir_with_this_dir_id'));
                        $action = 'explorer_content';
                        break;
                    }
                    $form = $_dir->getFormData();
                    $html = CmsForm::getDirDelete($form);
                }
                break;

            /* CONTENT ************************************************** */
            case "content_new":
                if (!$this->checkPrivilege('cms_admin_content_new')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    $action = 'explorer_content';
                    break;
                }
                if ($status == 'ok') {
                    // save NEW content ****************************************************
                    $form = CmsForm::checkContentNew($form);
                    if ($view->countMessages()) {
                        $view->addMessage('STATUS', 'ERROR', $translator->trans('Bitte prüfen Sie Ihre Eingaben.'));
                        $html = CmsForm::getContentNew($form);
                    } else {
                        $form['created_at'] = $now;
                        $form['created_by'] = $profile->getId();
                        $form['updated_at'] = $now;
                        $form['updated_by'] = $profile->getId();
                        if(!isset($form['active_version_id_de'])) {
                            $form['active_version_id_de'] = 0;
                        }
                        $content = new CmsContent();
                        $_new_content_id = $content->saveNew($form);

                        if (!$_new_content_id) {
                            $view->addMessage('STATUS', 'ERROR', $translator->trans('could_not_create_new_content'));
                        } else {
                            $view->addMessage('STATUS', 'NOTICE', $translator->trans('content_created_successfully'));
                        }
                        $select_dir_id = $form['dir_id'];
                        $action = 'explorer_content';
                    }
                } else {
                    // set defaults
                    $form = array('dir_id' => getParam('dir_id'));
                    foreach ($GLOBALS['CONFIG']['CMS_LOCALE'] as $code => $values) {
                        $form['use_dirpath_' . $code] = 1;
                    }
                    $html = CmsForm::getContentNew($form);
                }
                break;

            case "content_edit":
                if (!$this->checkPrivilege('cms_admin_content_edit')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    $action = 'explorer_content';
                    break;
                }
                if ($status == 'ok') {
                    // save ****************************************************
                    $form = CmsForm::checkContentEdit($form);
                    if ($view->countMessages()) {
                        $view->addMessage('STATUS', 'ERROR', $translator->trans('Bitte prüfen Sie Ihre Eingaben.'));
                        $html = CmsForm::getContentEdit($form);
                    } else {
                        $_content_id = $form['id'];
                        $_content = new CmsContent($_content_id);
                        if (!$_content->isValid()) {
                            $view->addMessage('STATUS', 'ERROR', $translator->trans('could_not_edit_content'));
                            $action = 'explorer_content';
                            break;
                        }
                        $_content->updated_at = $now;
                        $_content->updated_by = $profile->getId();
                        $_content->template_id = $form['template_id'];
                        $_content->engine_id = $form['engine_id'];
                        $_content->mimetype = $form['mimetype'];
                        $_content->dir_id = $form['dir_id'];
                        $_save_data = array('dir_id', 'template_id', 'engine_id', 'mimetype', 'updated_at', 'updated_by');
                        $_content->updateMetasFromForm($form, array('use_dirpath' => '0'));
                        $_result = $_content->save($_save_data, true);

                        if (!$_result) {
                            $view->addMessage('STATUS', 'ERROR', $translator->trans('could_not_edit_content'));
                        } else {
                            $view->addMessage('STATUS', 'NOTICE', $translator->trans('content_edited_successfully'));
                        }
                        $select_dir_id = $form['dir_id'];
                        $action = 'explorer_content';
                    }
                } else {
                    // set defaults
                    $_content = new CmsContent(getParam('content_id'));
                    if (!$_content->isValid()) {
                        $view->addMessage('STATUS', 'ERROR', $translator->trans('there_is_no_content_with_this_content_id'));
                        $action = 'explorer_content';
                        break;
                    }
                    $form = $_content->getFormData();
                    $html = CmsForm::getContentEdit($form);
                }
                break;

            case "content_change_dir":
                break;
            case "content_delete":
                if (!$this->checkPrivilege('cms_admin_content_delete')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    $action = 'explorer_version';
                    break;
                }
                if ($status == 'ok') {
                    // delete ****************************************************
                    $_content = new CmsContent($form['id']);
                    if (!$_content->isValid()) {
                        $view->addMessage('STATUS', 'ERROR', $translator->trans('could_not_delete_content'));
                    } else {
                        if (!$_content->delete()) {
                            $view->addMessage('STATUS', 'ERROR', $translator->trans('could_not_delete_content'));
                        } else {
                            $view->addMessage('STATUS', 'NOTICE', $translator->trans('content_deleted_successfully'));
                        }
                    }
                    $select_dir_id = $form['dir_id'];
                    $action = 'explorer_content';
                } else {
                    // set defaults
                    $_content = new CmsContent(getParam('content_id'));
                    if (!$_content->isValid()) {
                        $view->addMessage('STATUS', 'ERROR', $translator->trans('there_is_no_content_with_this_content_id'));
                        $action = 'explorer_content';
                        break;
                    }
                    $form = $_content->getFormData();
                    $html = CmsForm::getContentDelete($form);
                }
                break;

            /* VERSION ********************************************* */
            case "version_activate":
                if (!$this->checkPrivilege('cms_admin_version_activate')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    $action = 'explorer_version';
                    break;
                }
                // activate ****************************************************
                $_version = new CmsVersion(getParam('version_id'));
                if (!$_version->isValid()) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('there_is_no_version_with_this_version_id'));
                    $action = 'explorer_version';
                    break;
                }
                if ($_version->activate()) {
                    $view->addMessage('STATUS', 'NOTICE', $translator->trans('version_activated_successfully'));
                } else {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('version_could_not_be_activated'));
                }
                $action = 'explorer_version';
                break;

            case "version_new":
                if (!$this->checkPrivilege('cms_admin_version_new')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    $action = 'explorer_version';
                    break;
                }
                if ($status == 'ok') {
                    // save ****************************************************
                    $form = CmsForm::checkVersionNew($form);
                    if ($view->countMessages()) {
                        $view->addMessage('STATUS', 'ERROR', $translator->trans('Bitte prüfen Sie Ihre Eingaben.'));
                        $html = CmsForm::getVersionNew($form);
                    } else {
                        // create the NEW version
                        $form['created_at'] = $now;
                        $form['created_by'] = $profile->getId();
                        if(!isset($form['content2'])) {
                            $form['content2'] = '';
                        }
                        $_version = new CmsVersion();
                        $_new_version_id = $_version->saveNew($form);

                        if (!$_new_version_id) {
                            $view->addMessage('STATUS', 'ERROR', $translator->trans('could_not_create_version'));
                        } else {
                            $view->addMessage('STATUS', 'NOTICE', $translator->trans('version_new_successfully'));
                        }
                        $select_dir_id = $form['dir_id'];
                        $content_id = $form['content_id'];
                        $action = 'explorer_version';
                    }
                } else {
                    // set defaults
                    $_content = new CmsContent(getParam('content_id'));
                    if (!$_content->isValid()) {
                        $view->addMessage('STATUS', 'ERROR', $translator->trans('there_is_no_content_with_this_content_id'));
                        $action = 'explorer_version';
                        break;
                    }
                    $form = array('content_id' => $_content->getId(), 'lang' => getParam('lang'));
                    $html = CmsForm::getVersionNew($form);
                }
                break;

            case "version_view":
                if (!$this->checkPrivilege('cms_admin_version_view')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    $action = 'explorer_version';
                    break;
                }
                $_version_id = getParam('version_id');
                // use the global "content" as some parts expect this to be there
                $content = new CmsContent();
                $content->loadByVersion($_version_id);
                if (!$content->isValid()) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('version_id_is_not_valid'));
                    $action = 'explorer_version';
                    break;
                }

                // load the version into the content
                $view->set('content', $content);
                $view->setHeadTitle($content->getTitle());
                $view->setMetaTag('keywords', $content->getTags());
                $view->setMetaTag('description', $content->getDescription());
                
                // render the page
                $html = $view->fetch($content->getTemplate());
                break;

            case "version_edit":
                if (!$this->checkPrivilege('cms_admin_version_edit')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    $action = 'explorer_version';
                    break;
                }
                
                if ($status == 'ok') {
                    // save ****************************************************
                    $form = CmsForm::checkVersionEdit($form);
                    if ($view->countMessages()) {
                        $view->addMessage('STATUS', 'ERROR', $translator->trans('Bitte prüfen Sie Ihre Eingaben.'));
                        $html = CmsForm::getVersionEdit($form);
                    } else {
                        $form['created_at'] = $now;
                        $form['created_by'] = $profile->getId();
                        if(!isset($form['content2'])) {
                            $form['content2'] = '';
                        }

                        $_version = new CmsVersion($form['id']);
                        // make sure that this version is valid,
                        // belongs to this content and
                        // is owned by the same editor
                        if (!$_version->isValid() ||
                            $_version->content_id != $form['content_id'] ||
                            $_version->created_by != $profile->getId()
                        ) {
                            $form['minor_change'] = 0;
                        }

                        if ($form['minor_change'] == 1) {
                            // minor changes: overwrite the old version
                            $_version->setFormData($form);
                            $_result = $_version->save();

                            if (!$_result) {
                                $view->addMessage('STATUS', 'ERROR', $translator->trans('could_not_edit_version'));
                            } else {
                                $view->addMessage('STATUS', 'NOTICE', $translator->trans('version_edited_successfully'));
                            }
                        } else {
                            // create a NEW version
                            $_version = new CmsVersion();
                            $_new_version_id = $_version->saveNew($form);
                            if (!$_new_version_id) {
                                $view->addMessage('STATUS', 'ERROR', $translator->trans('could_not_edit_version'));
                            } else {
                                $view->addMessage('STATUS', 'NOTICE', $translator->trans('version_edited_successfully'));
                            }
                        }
                        $content_id = $form['content_id'];
                        $select_dir_id = $form['dir_id'];
                        $action = 'explorer_version';
                    }
                } else {
                    // set defaults
                    $_version = new CmsVersion(getParam('version_id'));
                    if (!$_version->isValid()) {
                        $view->addMessage('STATUS', 'ERROR', $translator->trans('there_is_no_version_with_this_version_id'));
                        $action = 'explorer_version';
                        break;
                    }
                    $form = $_version->getDatas();
                    $html = CmsForm::getVersionEdit($form);
                }
                break;

            case "version_delete":
                if (!$this->checkPrivilege('cms_admin_version_delete')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    $action = 'explorer_version';
                    break;
                }
                if ($status == 'ok') {
                    // delete ****************************************************
                    $_version = new CmsVersion($form['id']);
                    if (!$_version->isValid()) {
                        $view->addMessage('STATUS', 'ERROR', $translator->trans('could_not_delete_version'));
                    } else {
                        if (!$_version->delete()) {
                            $view->addMessage('STATUS', 'ERROR', $translator->trans('could_not_delete_version'));
                        } else {
                            $view->addMessage('STATUS', 'NOTICE', $translator->trans('version_deleted_successfully'));
                        }
                    }
                    $select_dir_id = $form['dir_id'];
                    $content_id = $form['content_id'];
                    $action = 'explorer_version';
                } else {
                    // set defaults
                    $_version = new CmsVersion(getParam('version_id'));
                    if (!$_version->isValid()) {
                        $view->addMessage('STATUS', 'ERROR', $translator->trans('there_is_no_version_with_this_version_id'));
                        $action = 'explorer_version';
                        break;
                    }
                    $form = $_version->getDatas();
                    $form['lang'] = $_version->lang;
                    $html = CmsForm::getVersionDelete($form);
                }
                break;

            case "version_diff":
                if (!$this->checkPrivilege('cms_admin_version_diff')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    $action = 'explorer_version';
                    break;
                }
                
                if ($status == 'ok') {
                    // show the diff ****************************************************
                    $_content_id = getParam('content_id');
                    $_content = new CmsContent($_content_id);
                    $_lang = getParam('lang');
                    $_version_1_id = getParam('version_1_' . $_lang);
                    $_version_2_id = getParam('version_2_' . $_lang);
                    $_version_1 = new CmsVersion($_version_1_id);
                    $_version_2 = new CmsVersion($_version_2_id);
                    if (!$_version_1->isValid() || !$_version_2->isValid()) {
                        $view->set('status', $translator->trans('one_or_more_non_valid_versions'));
                    } elseif (($_version_1->content_id != $_content_id) || ($_version_2->content_id != $_content_id)) {
                        $view->set('status', $translator->trans('versions_are_not_from_the_same_content'));
                    } else {
                        $view->set('content', $_content);
                        $view->set('lang', $_lang);
                        $view->set('version_1', $_version_1);
                        $view->set('version_2', $_version_2);
                        $view->set('title_left', 'version_nr: ' . $_version_1->version_nr . '<br />date: ' . mysql2date('D, d M Y H:i:s +0000', $_version_1->created_at));
                        $view->set('title_right', 'version_nr: ' . $_version_2->version_nr . '<br />date: ' . mysql2date('D, d M Y H:i:s +0000', $_version_2->created_at));
                        $_diff = $_content->getVersionsDiff($_version_1, $_version_2);
                        if ($_diff == '') {
                            $view->set('status', $translator->trans('there_are_no_differences_between_the_two_versions'));
                        }
                        $view->set('diff', $_diff);
                    }
                    $html = $view->fetch('admin/cms/_version_diff.tpl');
                } else {
                    // set defaults
                    $action = 'explorer_version_diff';
                }
                break;

            /* ************************************************** */
            case "view":
            default:
                break;
        }
        
        if ($action == 'explorer_content' || $action == 'explorer_version' || $action == 'explorer_version_diff') {
            if ($this->checkPrivilege('cms_admin')) {
                if (!isset($content_id) || !is_numeric($content_id)) {
                    $content_id = getParam("content_id");
                }
                $html = CmsView::getDirContentExplorer($select_dir_id, $content_id, $action);
            }
        }

        // NOTE: The HTML could be empty here if there are no actions or permissions available!
        return $html;
    }

}