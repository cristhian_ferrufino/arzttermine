<?php

namespace Arzttermine\Admin\Module;

use Arzttermine\Admin\Configuration;
use Arzttermine\Admin\Module;
use Arzttermine\Booking\Booking;
use Arzttermine\Booking\Offer;
use Arzttermine\Calendar\Calendar;
use Arzttermine\Cms\CmsContent;
use Arzttermine\Core\DateTime;
use Arzttermine\Insurance\Insurance;
use Arzttermine\Integration\Integration;
use Arzttermine\Location\Location;
use Arzttermine\Mail\Mailing;
use Arzttermine\MedicalSpecialty\TreatmentType;
use Arzttermine\Review\Review;
use Arzttermine\User\User;
use NGS\Managers\DoctorsManager;

final class Bookings extends Module {

    /**
     * @var string
     */
    protected $title = 'Buchungen Admin';

    /**
     * @var string
     */
    protected $role = 'booking_admin';

    /**
     * @var string
     */
    protected $slug = 'bookings';

    /**
     * {@inheritdoc}
     */
    public function boot()
    {
        $translator = $this->container->get('translator');

        $MENU_ARRAY = array(
            "id"                       => array("sql" => 1, "name" => "ID", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "slug"                     => array("sql" => 1, "name" => "Slug", "show" => 1, "edit" => 0, "new" => 0, "search" => 1, "sort" => 0, "nowrap" => 1, "form_input" => array("type" => "none")),
            "status"                   => array("sql" => 1, "name" => "Status", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "select", "values" => Booking::getStatusTextArray()), "list_function" => array("name" => "get_data_status", "para" => true)),
            "appointment_start_at"     => array("sql" => 1, "name" => "Termin", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "list_function" => array("name" => "get_data_appointment_start_at", "para" => true), "form_function" => array("name" => "get_data_appointment_start_at", "para" => true)),
            "salesforce_id"            => array("sql" => 1, "name" => "SalesforceID", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_salesforce_id", "para" => true), "list_function" => array("name" => "get_data_salesforce_id", "para" => true)),
            "salesforce_updated_at"    => array("sql" => 1, "name" => "SalesforceUpdatedAt", "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 0, "nowrap" => 1, "form_input" => array("type" => "none"), "list_function" => array("name" => "get_salesforce_updated_at", "para" => true)),
            "insurance_id"             => array("sql" => 1, "name" => "Versicherung", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_insurance_id", "para" => true), "form_input" => array("type" => "select", "values" => $GLOBALS['CONFIG']['INSURANCES'])),
            "gender"                   => array("sql" => 1, "name" => "F/M", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_gender", "para" => true), "form_input" => array("type" => "select", "values" => $GLOBALS['CONFIG']['ARRAY_GENDER'])),
            "first_name"               => array("sql" => 1, "name" => "Vorname", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "last_name"                => array("sql" => 1, "name" => "Nachname", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "phone"                    => array("sql" => 1, "name" => "Telefon", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "email"                    => array("sql" => 1, "name" => "E-Mail", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "has_contract"             => array("sql" => 1, "name" => "Mit Vertrag", "show" => 1, "edit" => 1, "new" => 1, "search" => 0, "sort" => 0, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_user_has_contract", "para" => true), "list_function" => array("name" => "get_user_has_contract", "para" => true)),
            "location_id"              => array("sql" => 1, "name" => "Praxis", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_form_location_id", "para" => true), "list_function" => array("name" => "get_data_location_id", "para" => true)),
            "treatmenttype_id"         => array("sql" => 1, "name" => "Behandlungsgrund", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_treatmenttype", "para" => true), "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_treatmenttype", "para" => true)),
            "returning_visitor"        => array("sql" => 1, "name" => "Dem Arzt bekannt", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_returning_visitor", "para" => true), "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_returning_visitor", "para" => true)),
            "integration_action"       => array("sql" => 0, "name" => "Integration", "show" => 1, "edit" => 0, "new" => 0, "search" => 0, "sort" => 0, "nowrap" => 1, "list_function" => array("name" => "get_data_integration_action", "para" => true)),
            "integration_status"       => array("sql" => 1, "name" => 'Integration Status', "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_integration_status", "para" => true), "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_integration_status", "para" => true)),
            "user_id"                  => array("sql" => 1, "name" => "Arzt", "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_form_user_id", "para" => true), "list_function" => array("name" => "get_data_user_id", "para" => true)),
            "flexible_time"            => array("sql" => 1, "name" => "Flexible Zeit", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_flexible_time", "para" => true), "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_flexible_time", "para" => true)),
            "flexible_location"        => array("sql" => 1, "name" => "Flexibler Ort", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_flexible_location", "para" => true), "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_flexible_location", "para" => true)),
            "booked_language"          => array("sql" => 1, "name" => "Sprache", "show" => 0, "edit" => 1, "new" => 0, "search" => 1, "sort" => 0, "nowrap" => 1, "form_input" => array("type" => "none")),
            "integration_booking_data" => array("sql" => 0, "name" => 'Integration Booking Data', "show" => 0, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_function" => array("name" => "get_data_integration_data", "para" => true), "form_input" => array("type" => "none")),
            "integration_log"          => array("sql" => 1, "name" => 'Integration-Log', "show" => 0, "edit" => 1, "new" => 0, "search" => 1, "sort" => 0, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => ' style="width:300px;height:60px;"')),
            "backend_marker"           => array("sql" => 1, "name" => "BackendMarker", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "select", "values" => $GLOBALS['CONFIG']['BACKEND_MARKER_CLASSES']), "list_function" => array("name" => "get_data_backend_marker", "para" => true)),
            "source_platform"          => array("sql" => 1, "name" => "Plattform", "show" => 0, "edit" => 1, "new" => 0, "search" => 1, "sort" => 0, "nowrap" => 1, "form_input" => array("type" => "none")),
            "comment_intern"           => array("sql" => 1, "name" => 'Kommentar <span style="color:#ff0000;">(intern)', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => ' style="width:300px;height:60px;"')),
            "comment_patient"          => array("sql" => 1, "name" => 'Kommentar Patient', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => 'readonly style="width:300px;height:60px;"')),
            "booking_offer"            => array("sql" => 0, "name" => "Create BookingOffer", "show" => 1, "edit" => 0, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_booking_offer", "para" => true)),
            "booking_offers"           => array("sql" => 0, "name" => "&nbsp;", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 0, "nowrap" => 1, "form_function" => array("name" => "form_booking_offers", "para" => true), "form_input" => array("type" => "none")),
            "created_at"               => array("sql" => 1, "name" => "Erstellungsdatum", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "created_by"               => array("sql" => 1, "name" => "Erstellt von", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_created_by", "para" => true), "list_function" => array("name" => "get_data_created_by", "para" => true)),
            "updated_at"               => array("sql" => 1, "name" => "Letztes Update", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "updated_by"               => array("sql" => 1, "name" => "Letzter Bearbeiter", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_updated_by", "para" => true), "list_function" => array("name" => "get_data_updated_by", "para" => true)),
            "contact_preference"       => array("sql" => 1, "name" => "Kontaktpräferenz", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 0, "nowrap" => 1, "form_input" => array("type" => "none")),
        );

        $BUTTON = array(
            "new"                  => array("func" => "booking_admin_new", "alt_text" => $translator->trans('new_item'), "script" => $this->admin->getUrl(), "gfx" => "document_new.png", "action" => "new", "th" => 1, "td" => 0),
            "edit"                 => array("func" => "booking_admin_edit", "alt_text" => $translator->trans('edit_item'), "script" => $this->admin->getUrl(), "gfx" => "document_edit.png", "action" => "edit", "th" => 0, "td" => 1),
            "delete"               => array("func" => "booking_admin_delete", "alt_text" => $translator->trans('delete_item'), "script" => $this->admin->getUrl(), "gfx" => "delete.png", "action" => "delete", "th" => 0, "td" => 1),
            "search"               => array("func" => "booking_admin_search", "alt_text" => $translator->trans('search'), "script" => $this->admin->getUrl(), "gfx" => "search.png", "action" => "search", "th" => 1, "td" => 0),
            "csv"                  => array("func" => "booking_admin_csv", "alt_text" => 'Liste als CSV herunterladen', "script" => $this->admin->getUrl(), "gfx" => "download.gif", "action" => "csv", "th" => 1, "td" => 0),
            "backend_marker"       => array("tr" => 1, "th" => 0, "td" => 0, "template_key" => "tr_class"),
        );

        $TABLE_CONFIG = array(
            "table_name"  => DB_TABLENAME_BOOKINGS,
            "alpha_index" => "last_name",
        );

        $URL_PARA = array(
            'page'         => 1,
            'alpha'        => '',
            "sort"         => 0,
            "direction"    => 'DESC',
            "num_per_page" => 50,
            "type"         => 'num'
        );

        $configuration = new Configuration();
        $configuration->menu    = $MENU_ARRAY;
        $configuration->buttons = $BUTTON;
        $configuration->table   = $TABLE_CONFIG;
        $configuration->default_parameters = $URL_PARA;

        $this->configuration = $configuration;
    }

    /**
     * {@inheritdoc}
     */
    public function render()
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $html = '';

        $action = getParam("action", 'list');
        $form_data = getParam("form_data");

        switch ($action) {
            case "edit":
                if (!$this->checkPrivilege('booking_admin_edit')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                
                $id = getParam("id");
                
                if (!empty($form_data) && is_array($form_data)) {
                    if ($GLOBALS['CONFIG']['SALESFORCE_ENABLE']) {
                        /*
                         * after editting the booking, the relative Salesforce Patienten record should also be editted
                         * create a new instance of salesforce_interface
                         * login to salesforce account
                         * call upsertBooking with required parameters
                         */
                        try {
                            $salesforce_interface = new \SalesforceInterface();

                            $doctor = new User($form_data['user_id']);
                            $doctor_name = '';

                            if (!empty($form_data['user_id']) && $doctor->isValid()) {
                                $doctor_name = "{$doctor->getTitle()} {$doctor->getFirstName()} {$doctor->getLastName()}";
                            }

                            $salesforce_interface->upsertBooking(
                                $id,
                                $form_data['gender'],
                                $form_data['first_name'],
                                $form_data['last_name'],
                                $form_data['email'],
                                $form_data['phone'],
                                null,
                                null,
                                $form_data['insurance_id'],
                                null,
                                null,
                                null,
                                $form_data['status'],
                                intval($form_data['returning_visitor']),
                                $doctor_name
                            );
                            // save the status that this booking was pushed to Salesforce
                            $booking = new Booking($id);
                            if ($booking->isValid()) {
                                $booking->setSalesforceUpdatedAt(date('Y-m-d H:i:s'));
                                $booking->save('salesforce_updated_at');
                            }
                        } catch (\Exception $e) {
                            /*
                             * the user is redirected back to the edit form and a
                             * notice message appears. No edits are applied in the AT database
                            */
                            $view->addMessage('STATUS', 'NOTICE', $translator->trans('Something went wrong, please try again.'));
                        }
                    }
                    $html = $this->change_one($action, $id, $form_data, $this->parameters);
                } else {
                    $html = $this->form_change($action, $id, $this->parameters);
                }
                
                break;
            case "delete":
                if (!$this->checkPrivilege('booking_admin_delete')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                
                if (!isset($_REQUEST["confirm_id"])) {
                    $html = $this->ask4delete(getParam("id"), $this->parameters);
                    break;
                }
                
                try {
                    $confirm_id = getParam("confirm_id");
                        
                    if ($GLOBALS['CONFIG']['SALESFORCE_ENABLE']) {
                        /*before deleting the booking, the relative Salesforce Patienten record should also be deleted*/
                        $salesforce_interface = new \SalesforceInterface();
                        $salesforce_interface->deletePatientenById($confirm_id);
                    }

                    //delete booking_offers for this booking
                    $booking = new Booking($confirm_id);
                    $booking_offers = $booking->getBookingOffers();
                    foreach($booking_offers as $booking_offer) {
                        $booking_offer->delete();
                    }
                    
                    /* if all went weel in SF delete the record in AT */
                    if ($this->delete_one($confirm_id)) {
                        $view->addMessage('STATUS', 'NOTICE', $translator->trans('the_entry_was_deleted_successfully'));
                    } else {
                        $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_could_not_be_deleted'));
                    }
                } catch (\Exception $e) {
                    /*
                     * the user is redirected to the booking list and a
                     * notice message appears. The delete action is not applied in the AT database
                    */
                    $view->addMessage('STATUS', 'NOTICE', $translator->trans('Something went wrong, please try again.'));
                }
                
                break;
            case "cancel":
                if (!$this->checkPrivilege('booking_admin_cancel')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                
                if (!isset($_REQUEST["confirm_id"])) {
                    $this->ask4cancel(getParam("id"), $this->parameters);
                } else {
                    $confirm_id = getParam("confirm_id");
                    $res = $this->cancel_one($confirm_id);
                    if ($res === true) {
                        $view->addMessage('STATUS', 'NOTICE', $translator->trans('the_entry_was_canceled_successfully'));
                    } else {
                        $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_could_not_be_canceled' . ' (' . $res . ')'));
                    }
                }
                
                break;

            case "search":
                if (!$this->checkPrivilege('booking_admin_search')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                
                if (isset($_REQUEST["send_button"])) {
                    $this->parameters["page"] = 1;
                    $this->parameters["alpha"] = '';
                }
                
                $html = $this->show_search_form($action, $form_data, $this->parameters);
                
                if (!empty($form_data) && is_array($form_data)) {
                    $html .= $this->get_list($this->parameters, $form_data); 
                }
                
                break;
            
            case "new":
                if (!$this->checkPrivilege('booking_admin_new')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                
                if (is_array($form_data)) {
                    $html = $this->new_one($action, $form_data, $this->parameters);
                } else {
                    $html = $this->form_new($action, $this->parameters);
                }
                break;
            
            case "integration_book":
                if (!$this->checkPrivilege('booking_admin_integration_book')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                $html = $this->integration_book();
                break;
            
            case "salesforce_push":
                if (!$this->checkPrivilege('booking_admin_edit')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                } else {
                    if ($GLOBALS['CONFIG']['SALESFORCE_ENABLE']) {
                        $booking = new Booking(getParam("id"));
                        if ($booking->isValid()) {
                            if ($booking->createInSalesforce()) {
                                $view->addMessage('STATUS', 'NOTICE', 'The booking was successfully created in salesforce');
                            } else {
                                $view->addMessage('STATUS', 'ERROR', 'The booking could not be created in salesforce');
                            }
                        }
                    } else {
                        $view->addMessage('STATUS', 'ERROR', 'The environment is configured to not use salesforce so the booking was not created in salesforce');
                    }
                }
                break;
            
            case "csv":
                if (!$this->checkPrivilege('booking_admin_csv')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                $this->show_csv(); // NOTE: THIS AVOIDS THE RESPONSE MODEL AND KILLS THE APP
                break;

            case "review-mail":
                $booking = new Booking(getParam("id"));
                $mailing_template = '/mailing/patients/review-doctor.html';
                if($booking->getUser()->hasPaidReview()) {
                    $mailing_template = '/mailing/patients/paid-review-doctor.html';
                }

                if (Review::sendPatientEmail($mailing_template, $booking)) {
                    $view->addMessage('STATUS', 'NOTICE', 'The booking review mail was successfully send to the patient');
                }
                break;
        }

        // Default the screen to the List view
        if (empty($html)) {
            if ($this->checkPrivilege('booking_admin_view')) {
                $html = $this->get_list($this->parameters);
            }
        }
        
        return $html;
    }

    // CONFIG FUNCTIONS
    // =================================================================================================================

    /**
     * @param array $data
     * 
     * @return string
     */
    public function get_data_created_by($data = array())
    {
        $user = new User();
        
        if (!empty($data['created_by'])) {
            $user->load($data['created_by']);
            $name = $user->getFullName();
        } else {
            $name = 'Patient';
        }
        return $name;
    }

    /**
     * @param array $data
     * 
     * @return string
     */
    public function get_data_created_at($data = array())
    {
        return !empty($data['created_at']) ? mysql2date('d.m.Y H:i:s', $data['created_at'], false) : '';
    }

    /**
     * @param array $data
     * 
     * @return string
     */
    public function get_data_updated_by($data = array())
    {
        $_user = new User();
        
        if (!empty($data['updated_by'])) {
            $_user->load($data['updated_by']);
        }

        return $_user->getFullName();
    }

    /**
     * @param array $data
     * 
     * @return string
     */
    public function get_data_status($data = array())
    {
        if (!empty($data['status'])) {
            $status_texts = Booking::getStatusTextArray();
            if (isset($status_texts[$data['status']])) {
                return $status_texts[$data['status']];
            }   
        }

        return '';
    }

    /**
     * @param array $data
     * 
     * @return string
     */
    public function get_data_flexible_time($data = array())
    {
        if (!empty($data) &&
            isset($data['flexible_time']) &&
            isset($GLOBALS['CONFIG']['ARRAY_NO_YES'][$data['flexible_time']])
        ) {
            return $GLOBALS['CONFIG']['ARRAY_NO_YES'][$data['flexible_time']];
        }

        return '';
    }

    /**
     * @param array $data
     * 
     * @return string
     */
    public function get_data_flexible_location($data = array())
    {
        if (!empty($data) &&
            isset($data['flexible_location']) &&
            isset($GLOBALS['CONFIG']['ARRAY_NO_YES'][$data['flexible_location']])
        ) {
            return $GLOBALS['CONFIG']['ARRAY_NO_YES'][$data['flexible_location']];
        }

        return '';
    }

    /**
     * @param array $data
     * 
     * @return string
     */
    public function get_data_backend_marker($data = array())
    {
        if (!empty($data) &&
            isset($data['backend_marker']) &&
            isset($GLOBALS['CONFIG']['BACKEND_MARKER_CLASSES'][$data['backend_marker']])
        ) {
            return $GLOBALS['CONFIG']['BACKEND_MARKER_CLASSES'][$data['backend_marker']];
        }

        return '';
    }

    /**
     * @param array $data
     * 
     * @return string
     */
    public function get_data_user_id(array $data = array())
    {
        $_user = new User();
        
        if (!empty($data['user_id'])) {
            $_user->load($data['user_id']);
        }
        
        if ($_user->isValid() && $_user->hasProfile()) {
            return '[' . $_user->getId() . '] <a href="' . $_user->getUrl() . '" target="_blank">' . $_user->getName() . '</a>';
        }

        return $_user->getName();
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_form_user_id(array $data = array())
    {
        $html = '';
        if (!empty($data) && isset($data['user_id'])) {
            $userId = $data['user_id'];
            $user = new User($userId);
            if ($user->isValid()) {
                $html = $user->getName().'<br />' .
                    $user->getMedicalSpecialityIdsText().'<br /><br />' .
                    'id: ' . $user->getId() . ' | <a href="' . $user->getUrl() . '" target="_blank">View</a> | '.
                    '<a href="' . $user->getAdminEditUrl() . '" target="_blank">Edit</a>';
            }
        }
        return $html;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_location_id($data = array())
    {
        if (!empty($data) && isset($data['location_id'])) {
            $locationId = $data['location_id'];
            $location = new Location($locationId);
            if (!$location->isValid()) {
                return '';
            }

            $length = 30;
            $name = mb_substr($location->getName(), 0, $length, $GLOBALS['CONFIG']['SYSTEM_CHARSET']);
            if (mb_strlen($location->getName(), $GLOBALS['CONFIG']['SYSTEM_CHARSET']) > $length) {
                $name .= '&hellip;';
            }

            return '[' . $location->getId() . '] <a href="' . $location->getUrl() . '" target="_blank">' . $name . '</a>';
        }

        return '';
    }

    /**
     * @param array $data
     * 
     * @return string
     */
    public function get_form_location_id($data = array())
    {
        $html = '';
        if (!empty($data) && isset($data['location_id'])) {
            $locationId = $data['location_id'];
            $location = new Location($locationId);
            if ($location->isValid()) {
                $html = $location->getName().'<br />' .
                $location->getAddress().'<br />' .
                '<i class="fa fa-phone"></i> '. $location->getPhone().'<br /><br />' .
                'id: ' . $location->getId() . ' | <a href="' . $location->getUrl() . '" target="_blank">View</a> | '.
                '<a href="' . $location->getAdminEditUrl() . '" target="_blank">Edit</a>';
            }
        }
        return $html;
    }

    /**
     * @param array $data
     * 
     * @return string
     */
    public function get_data_gender($data = array())
    {
        if (isset($data['gender']) && isset($GLOBALS['CONFIG']['ARRAY_GENDER'][$data['gender']])) {
            return $GLOBALS['CONFIG']['ARRAY_GENDER'][$data['gender']];
        }

        return '';
    }

    /**
     * @param array $data
     * 
     * @return string
     */
    public function get_data_appointment_start_at($data = array())
    {
        if (empty($data['appointment_start_at'])) {
            return '';
        }
        
        $text = $data['appointment_start_at'];
        if ($data['appointment_start_at'] == '1970-01-01 00:00:00') {
            $text = 'Terminanfrage';
        }

        return $text;
    }

    /**
     * @param array $data
     * 
     * @return int
     */
    public function get_data_insurance_id($data = array())
    {
        if (empty($data['insurance_id'])) {
            return Insurance::GKV;
        }
        
        return $GLOBALS['CONFIG']['INSURANCES'][$data['insurance_id']];
    }

    /**
     * @param array $data
     * 
     * @return string
     */
    public function get_data_returning_visitor($data = array())
    {
        if (
            isset($data['returning_visitor']) &&
            isset($GLOBALS['CONFIG']['ARRAY_NULL_NO_YES'][$data['returning_visitor']])
        ) {
            return $GLOBALS['CONFIG']['ARRAY_NULL_NO_YES'][$data['returning_visitor']];
        }

        return '';
    }

    /**
     * @param array $data
     * 
     * @return string
     */
    public function get_data_integration_action($data = array())
    {
        if (empty($data['location_id'])) {
            return '';
        }

        $_integration = Integration::getIntegrationByLocationId($data['location_id']);

        if ($_integration === false || !class_implements($_integration, 'IntegrationInterface')) {
            return 'Error';
        }
        
        $html = $_integration->getName();
        if ($_integration->hasAdminManualMailing()) {
            $_booking = new Booking($data['id']);
            $_total_mails = 0;
            if ($_booking->isValid()) {
                $_total_mails = $_booking->countBookingMailings();
            }
            $_icon = 'ico/email.png';
            if ($_total_mails > 0) {
                $_icon = 'ico/email_add.png';
            }
            $html .= '<img style="cursor:pointer" onclick="$(this).siblings().not(\'.cancel_booking\').toggle()" src="' . getStaticUrl($_icon) . '" alt="" title="Verschickte Mails: ' . $_total_mails . '" class="button-action" />';
            $html .= '<div style="display:none" class="mail_links">
		                <div onclick="$(this).parent().hide();" class="close">x</div>
		                <a href="' . $this->admin->getUrl() . '?action=integration_mail&booking_id=' . $data['id'] . '&template=admin-booking-new.txt&option=new" class="integration_mail fancybox.iframe">new booking</a>
                        <a href="' . $this->admin->getUrl() . '?action=integration_mail&booking_id=' . $data['id'] . '&template=admin-booking-generic.txt&option=cannot-reach" class="integration_mail fancybox.iframe">Patient Cannot be reached by phone</a>
                        <a href="' . $this->admin->getUrl() . '?action=integration_mail&booking_id=' . $data['id'] . '&template=admin-booking-generic.txt&option=cannot-place" class="integration_mail fancybox.iframe">Appointment cannot be placed</a>
                        <a href="' . $this->admin->getUrl() . '?action=integration_mail&booking_id=' . $data['id'] . '&template=admin-booking-generic.txt&option=cancel" class="integration_mail fancybox.iframe">Cancel Appointment</a>
                  </div>';
        }
        
        if ($_integration->hasAdminManualBooking()) {
            $html .= '<a class="cancel_booking" href="' . $this->admin->getUrl() . '?action=integration_book&booking_id=' . $data['id'] . '" target="_blank" onClick="return confirm(\'Soll der Termin in das AIS gebucht werden?\');"><img src="' . getStaticUrl('adm/integration_book.png') . '" alt="" /></a>';
        }

        return $html;
    }

    /**
     * @param array $data
     * 
     * @return string
     */
    public function get_data_integration_status($data = array())
    {
        if (isset($data['integration_status']) &&
            isset($GLOBALS['CONFIG']['BOOKING_INTEGRATION_STATUS'][$data['integration_status']])
        ) {
            return $GLOBALS['CONFIG']['BOOKING_INTEGRATION_STATUS'][$data['integration_status']];
        }

        return '';
    }

    /**
     * @param array $data
     * 
     * @return string
     */
    public function get_data_integration_data($data = array())
    {
        if (isset($data['id'])) {
            $_booking = new Booking($data['id']);
            if ($_booking->isValid()) {
                return $_booking->getIntegrationBookingHtml();
            }
        }

        return '';
    }

    /**
     * @param array $data
     * 
     * @return string
     */
    public function get_data_treatmenttype($data = array())
    {
        $html = '';

        if (isset($data['treatmenttype_id'])) {
            $tt = new TreatmentType($data['treatmenttype_id']);
            $html = $tt->getName();
        }

        return $html;
    }

    /**
     * @param array $data
     * 
     * @return string
     */
    public function get_user_has_contract($data = array())
    {
        $user = new User();

        if (!empty($data['user_id'])) {
            $user->load($data["user_id"]);
        }
        
        return $GLOBALS["CONFIG"]["ARRAY_NO_YES"][$user->getHasContract()];
    }

    /**
     * @param array $data
     * 
     * @return string
     */
    public function get_salesforce_updated_at($data = array())
    {
        $html = '';

        if (!empty($data['salesforce_updated_at']) && !empty($data['id'])) {
            if (Calendar::isValidDateTime($data['salesforce_updated_at'])) {
                $html = '<span class="green">' . $data['salesforce_updated_at'] . '</span>';
            } else {
                $html = '<a href="' . $this->admin->generateUrl('admin_bookings') . '?action=salesforce_push&id=' . $data['id'] . '&' . $this->admin->admin_get_url_para($this->parameters) . '"><span class="red">' . $this->container->get('translator')->trans('Push booking') . '</span></a>';
            }   
        }

        return $html;
    }

    /**
     * @param array $data
     * 
     * @return string
     */
    public function get_data_booking_offer($data = array())
    {
        $html = '';
        
        if (!empty($data['id'])) {
            $booking = new Booking($data['id']);
            if ($booking->isValid()) {
                $url = sprintf('/administration/booking-offers?action=new&form_data[booking_id]=%d&form_data[user_id]=%d&form_data[location_id]=%d', $booking->getId(), $booking->getUserId(), $booking->getLocationId());
                $html = '<a href="' . $url . '">' . $this->container->get('translator')->trans('Create Booking Offer') . '</a>';
            }
        }

        return $html;
    }

    /**
     * @param array $data
     * @return string
     */
    public function form_booking_offers($data = array())
    {
        $view = $this->getView();

        $booking = new Booking($data['id']);
        $bookingOffers = $booking->getBookingOffers();

        return $view
            ->setRef('booking_offers', $bookingOffers)
            ->fetch('admin/booking/_form_booking_offers.tpl');
    }

    /**
     * @param array $data
     * 
     * @return string
     */
    public function get_data_salesforce_id($data = array())
    {
        if (empty($data['salesforce_id'])) {
            return '<a href=""></a>';
        }
        
        return '<a href="https://eu1.salesforce.com/' . $data['salesforce_id'] . '" target="_blank">' . $data['salesforce_id'] . '</a>';
    }

    /**
     * @param array $params
     * @param array $search
     * 
     * @return string
     */
    public function get_list($params = array(), $search = array())
    {
        if (empty($params)) {
            $params = $this->parameters;
        }
        
        $html = $this->admin->admin_get_list(0, $search, $params);
        $html .= '<script type="text/javascript">
AT._defer(function() {
    $(document).ready(function() {
        $(".integration_mail").fancybox({
            maxWidth	: 800,
            maxHeight	: 600,
            fitToView	: false,
            width		: \'70%\',
            height		: \'70%\',
            autoSize	: false,
            closeClick	: false,
            openEffect	: \'none\',
            closeEffect	: \'none\'
        });
    });
});
</script>';

        return $html;
    }


    /**
     * @param string $action
     * @param array $form_data
     * @param array $URL_PARA
     * 
     * @return string
     */
    public function new_one($action, $form_data, $URL_PARA)
    {
        $profile = $this->container->get('current_user');
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $html = '';

        // 1. set defaults
        $form_data["updated_by"] = $profile->getId();
        $form_data["updated_at"] = (new DateTime())->getDateTime();

        // 2. check
        if (1 > mb_strlen(sanitize_title($form_data["first_name"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
            $view->addMessage('admin_message_first_name', 'ERROR', $translator->trans('Es muss ein Vorname angegeben werden'));
        }
        if (1 > mb_strlen(sanitize_title($form_data["last_name"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
            $view->addMessage('admin_message_last_name', 'ERROR', $translator->trans('Es muss ein Nachname angegeben werden'));
        }

        // if there are checkboxes than set the keys to make sure that deselected checkboxes are beeing set
        $form_data = $this->admin->set_unselected_checkbox_values($form_data);

        // 3. exit on errors
        if ($view->countMessages()) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('Bitte prüfen Sie Ihre Eingaben.'));
            return $this->form_change($action, '', $URL_PARA, $form_data);
        }
        // 4. sanitize
        $form_data["first_name"] = sanitize_title($form_data["first_name"]);
        $form_data["last_name"] = sanitize_title($form_data["last_name"]);

        $_id = $this->admin->admin_set_new($form_data);
        if ($_id > 0) {
            $view->addMessage('STATUS', 'NOTICE', $translator->trans('Die Buchung wurde erfolgreich mit der ID ' . $_id . ' angelegt.<br /><a href="/administration/bookings?action=edit&id=' . $_id . '">Buchung bearbeiten</a>'));
        } elseif ($_id == -1) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_allready_exists'));
            $html = $this->form_new($action, $URL_PARA, $form_data);
        } elseif ($_id == -2) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('name_of_primary_key_is_empty'));
            $html = $this->form_new($action, $URL_PARA, $form_data);
        } else {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_could_not_be_created'));
        }

        return $html;
    }

    /**
     * @param string $action
     * @param int $id
     * @param array $form_data
     * @param array $URL_PARA
     * 
     * @return string
     */
    public function change_one($action, $id, $form_data, $URL_PARA)
    {
        $profile = $this->container->get('current_user');
        $view = $this->getView();
        $translator = $this->container->get('translator');

        // 1. set defaults
        $form_data["updated_by"] = $profile->getId();
        $form_data["updated_at"] = date('Y-m-d H:i:s');

        // 2. check
        if (1 > mb_strlen(sanitize_title($form_data["first_name"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
            $view->addMessage('admin_message_first_name', 'ERROR', $translator->trans('Es muss ein Vorname angegeben werden'));
        }
        if (1 > mb_strlen(sanitize_title($form_data["last_name"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
            $view->addMessage('admin_message_last_name', 'ERROR', $translator->trans('Es muss ein Nachname angegeben werden'));
        }

        // if there are checkboxes than set the keys to make sure that deselected checkboxes are beeing set
        $form_data = $this->admin->set_unselected_checkbox_values($form_data);

        // 3. exit on errors
        if ($view->countMessages()) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('Bitte prüfen Sie Ihre Eingaben.'));
            return $this->form_change($action, $id, $URL_PARA, $form_data);
        }
        
        // 4. sanitize
        $form_data["first_name"] = sanitize_title($form_data["first_name"]);
        $form_data["last_name"] = sanitize_title($form_data["last_name"]);

        // if there are checkboxes than set the keys to make sure that deselected checkboxes are beeing set
        $form_data = $this->admin->set_unselected_checkbox_values($form_data);

        $_result = $this->admin->admin_set_update($id, $form_data);

        if ($_result == true) {
            $view->addMessage('STATUS', 'NOTICE', $translator->trans('action_was_successfully'));
            return '';
        }
        
        if ($_result == false) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('no_data_has_been_updated'));
        }

        return $this->form_change($action, $id, $URL_PARA, $form_data);
    }

    /**
     * @param int $id
     * @param array $URL_PARA
     * 
     * @return string
     */
    public function ask4delete($id, $URL_PARA)
    {
        return $this->admin->admin_get_confirm_table($id, $URL_PARA, 'delete');
    }
    
    /**
     * @param int $id
     * @param array $URL_PARA
     * 
     * @return string
     */
    public function ask4cancel($id, $URL_PARA)
    {
        return $this->admin->admin_get_confirm_table($id, $URL_PARA, 'cancel');
    }

    /**
     * @param int $id
     * 
     * @return int
     */
    public function delete_one($id)
    {
        return $this->admin->admin_set_delete($id);
    }

    /**
     * @param int $id
     * 
     * @return bool
     */
    public function cancel_one($id)
    {
        $result = false;
        $booking = new Booking($id);
        if ($booking->isValid()) {
            $result = $booking->cancel(Booking::STATUS_CANCELLED_BY_PATIENT);
        }
        return $result;
    }

    /**
     * @param string $action
     * @param array $URL_PARA
     * @param array $error_data
     * 
     * @return string
     */
    public function form_new($action, $URL_PARA, $error_data = array())
    {
        $view = $this->getView();
        
        $_html = $this->admin->admin_get_form($action, 0, $error_data, $URL_PARA);
        $view->setFocus('#form_data_first_name');

        return $_html;
    }

    /**
     * @param string $action
     * @param int $id
     * @param array $URL_PARA
     * @param array $error_data
     * 
     * @return string
     */
    public function form_change($action, $id, $URL_PARA, $error_data = array())
    {
        $view = $this->getView();
        $view->setRef('url_doctor_review', $this->admin->generateUrl('admin_bookings') . '?action=review-mail&id=' . $id . '&' . $this->admin->admin_get_url_para($this->parameters));
	    $_booking = new Booking($id);
        $view->setRef('url_doctor_review_google_tracker', $_booking->getLocation()->getGooglePlaceReviewLink('check google review tracker', true, $_booking->getSlug(), true));

        $_html = $this->admin->admin_get_form($action, $id, $error_data, $URL_PARA, 'admin/booking/_form_data_table.tpl');
        $view->setFocus('#form_data_first_name');

        return $_html;
    }


    /**
     * @param string $action
     * @param array $search_data
     * @param array $URL_PARA
     * 
     * @return string
     */
    public function show_search_form($action, $search_data, $URL_PARA)
    {
        return $this->admin->admin_get_form($action, 0, $search_data, $URL_PARA);
    }


    /**
     * @param int $booking_id
     * @param array $url_para
     * 
     * @return string
     */
    public function form_integration_mail($booking_id, $url_para)
    {
        $view = $this->getView();
        
        $_booking = new Booking($booking_id);
        if (!$_booking->isValid()) {
            $_html = 'Ungültige BuchungsID';
        } else {
            $main_body = '';
            $subject = '';

            if (getParam('option') == "new") {
                $main_body = CmsContent::getCmsContent('/mailing/integrations/' . $_booking->getIntegration()->getId() . '/admin-booking-new-content.txt')->getContent();
                $subject = CmsContent::getCmsContent('/mailing/integrations/' . $_booking->getIntegration()->getId() . '/admin-booking-new-content.txt')->getTitle();
            } else if (getParam('option') == "cannot-reach") {
                $main_body = CmsContent::getCmsContent('/mailing/integrations/' . $_booking->getIntegration()->getId() . '/booking-cannot-reach.html')->getContent();
                $subject = CmsContent::getCmsContent('/mailing/integrations/' . $_booking->getIntegration()->getId() . '/booking-cannot-reach.html')->getTitle();
            } else if (getParam('option') == "cannot-place") {
                $main_body = CmsContent::getCmsContent('/mailing/integrations/' . $_booking->getIntegration()->getId() . '/admin-booking-cannot-place-content.txt')->getContent();
                $subject = CmsContent::getCmsContent('/mailing/integrations/' . $_booking->getIntegration()->getId() . '/admin-booking-cannot-place-content.txt')->getTitle();
            } else if (getParam('option') == "cancel") {
                $main_body = CmsContent::getCmsContent('/mailing/integrations/' . $_booking->getIntegration()->getId() . '/admin-booking-cancel.txt')->getContent();
                $subject = CmsContent::getCmsContent('/mailing/integrations/' . $_booking->getIntegration()->getId() . '/admin-booking-cancel.txt')->getTitle();
            }
            $view->setRef('booking', $_booking);
            $view->setRef('admin_hidden', $this->admin->admin_get_hidden($url_para));
            $_data = array();
            $_data['user_name'] = $_booking->getUser()->getName();
            $_data['location_name'] = $_booking->getLocation()->getName();
            $_body = $main_body;
            $view->setRef('user_name', $_booking->getUser()->getName());
            $view->setRef('booking_salutation_full_name', $_booking->getSalutationName());
            $view->setRef('location_name', $_booking->getLocation()->getName());
            $view->setRef('location_address', $_booking->getLocation()->getAddress(', '));
            $view->setRef('appointment_date', $_booking->getAppointment()->getWeekdayDateText());
            $view->setRef('appointment_time', $_booking->getAppointment()->getTimeText());
            $view->setRef('template_filename', '/mailing/integrations/' . $_booking->getIntegration()->getId() . '/' . getParam('template'));
            $view->setRef('option', getParam('option'));
            $view->setRef('body', $_body);
            $_subject = Mailing::arrayReplace($subject, $_data);
            $view->setRef('title', $_subject);
            $view->setRef('subject', $_subject);
            $_html = $view->fetch('admin/booking/_integration_mail_iframe_p1.tpl');
        }

        return $_html;
    }


    /**
     * Dump a CSV to the client and terminate (without hooks) the request
     */
    public function show_csv()
    {
        $_booking = new Booking();
        $_bookings = $_booking->findObjects('1=1 ORDER BY created_at ASC');
        if (!empty($_bookings)) {
            $_html = 'id,created_at,appointment_start_at,status,first_name,last_name,email,phone,insurance_text,returning_visitor_text,user_id,user_name,location_id,location_name,medical_specialty_text,treatmenttype_text,PLZ,Stadt,comment_intern' . "\n";

            /** @var Booking $_booking */
            foreach ($_bookings as $_booking) {
                $_location = $_booking->getLocation();
                $_user = $_booking->getUser();
                $ttName = TreatmentType::getNameStatic($_booking->getTreatmenttypeId());
                $_html .= '"' . $_booking->getId() . '","' .
                    sanitize_csv_field($_booking->getCreatedAt()) . '","' .
                    sanitize_csv_field($_booking->getAppointment()->getDateTime()) . '","' .
                    sanitize_csv_field($_booking->getStatusText(true)) . '","' .
                    sanitize_csv_field($_booking->getFirstName()) . '","' .
                    sanitize_csv_field($_booking->getLastName()) . '","' .
                    sanitize_csv_field($_booking->getEmail()) . '","' .
                    sanitize_csv_field($_booking->getPhone()) . '","' .
                    sanitize_csv_field($_booking->getInsuranceText()) . '","' .
                    sanitize_csv_field($_booking->getReturningVisitorText()) . '","' .
                    sanitize_csv_field($_user->getId()) . '","' .
                    sanitize_csv_field($_user->getName()) . '","' .
                    sanitize_csv_field($_location->getId()) . '","' .
                    sanitize_csv_field($_location->getName()) . '","' .
                    sanitize_csv_field($_user->getMedicalSpecialityIdsText('|')) . '","' .

                    sanitize_csv_field($ttName) . '","' .
                    sanitize_csv_field($_location->getZip()) . '","' .
                    sanitize_csv_field($_location->getCity()) . '","' .
                    sanitize_csv_field(preg_replace("/(\r\n|\n\r|\r|\n)/", " ", $_booking->getCommentIntern())) . '"' . "\n";
            }
            // use iso-8859-1 instead of utf-8 ONLY because ms excel on mac os
            // messes up the encoding for csv import
            // import should be then with "Windows (ANSI)" or "Windows Latin 1"
            header("Content-Type: text/csv; charset=iso-8859-1");
            header("Content-Disposition: attachment; filename=bookings-" . date('Ymd_His') . '-iso-8859-1.csv');
            header("Pragma: no-cache");
            header("Expires: 0");
            echo utf8_decode($_html);
            exit;
        }
        echo 'ERROR: No available cases';
        exit;
    }


    /**
     * @return string
     */
    public function integration_book()
    {
        $booking = new Booking(getParam('booking_id'));
        
        if (!$booking->isValid()) {
            $html = 'Invalid booking_id';
        } else {
            if ($booking->integrationBook() !== true) {
                $html = 'Booking failed';
            } else {
                $html = 'Booking OK';
            }
        }

        return $html;
    }

    /**
     * @param int $id
     * @param array $form_data
     * 
     * @return bool
     */
    public function integration_mail($id, $form_data)
    {
        $content = CmsContent::getCmsContent($form_data['template_filename']);
        $data = array();

        $data['user_name'] = $form_data['user_name'];
        $data['location_name'] = $form_data['location_name'];
        $data['booking_salutation_full_name'] = $form_data['booking_salutation_full_name'];
        $data['location_address'] = $form_data['location_address'];
        $data['appointment_date'] = $form_data['appointment_date'];
        $data['appointment_time'] = $form_data['appointment_time'];
        $data['domain'] = $GLOBALS['CONFIG']['DOMAIN'];
        $data['body'] = nl2br($form_data['body']);
        
        $body = $content->getContent();
        $body = Mailing::arrayReplace($body, $data);
        
        $mailing = new Mailing();
        $mailing->type = MAILING_TYPE_TEXT;
        $mailing->source_id = $id;
        $mailing->source_type_id = MAILING_SOURCE_TYPE_ID_BOOKING;
        $mailing->from = $GLOBALS['CONFIG']["BOOKINGS_EMAIL_ADDRESS"];
        $mailing->from_name = $GLOBALS['CONFIG']["BOOKINGS_EMAIL_NAME"];
        $mailing->body_text = $body;
        $mailing->subject = $form_data['subject'];
        $mailing->addAddressTo($form_data['email'], $form_data['name']);

        return $mailing->sendPlain();
    }
}
