<?php

namespace Arzttermine\Admin\Module;

use Arzttermine\Admin\Configuration;
use Arzttermine\Admin\Module;
use Arzttermine\Core\DateTime;
use Arzttermine\User\User;

final class Groups extends Module {

    /**
     * @var string
     */
    protected $title = 'Groups Admin';

    /**
     * @var string
     */
    protected $role = 'group_admin';

    /**
     * @var string
     */
    protected $slug = 'groups';

    /**
     * {@inheritdoc}
     */
    public function boot()
    {
        $translator = $this->container->get('translator');

        $MENU_ARRAY = array(
            "id"         => array("sql" => 1, "name" => "ID", "show" => 1, "edit" => 0, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1),
            "name"       => array("sql" => 1, "name" => $translator->trans('name'), "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "updated_at" => array("sql" => 1, "name" => $translator->trans('updated_at'), "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "updated_by" => array("sql" => 1, "name" => $translator->trans('updated_by'), "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_updated_by_name", "para" => true), "list_function" => array("name" => "get_updated_by_name", "para" => true)),
            "access"     => array("sql" => 1, "name" => $translator->trans('access'), "show" => 0, "edit" => 1, "new" => 1, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "function", "name" => "funcsecbox", "para" => true)),
        );

        $BUTTON = array(
            "new"    => array("func" => "group_admin_new", "alt_text" => $translator->trans('new_item'), "script" => $this->admin->getUrl(), "gfx" => "document_new.png", "action" => "new", "th" => 1, "td" => 0),
            "edit"   => array("func" => "group_admin_edit", "alt_text" => $translator->trans('edit_item'), "script" => $this->admin->getUrl(), "gfx" => "document_edit.png", "action" => "edit", "th" => 0, "td" => 1),
            "delete" => array("func" => "group_admin_delete", "alt_text" => $translator->trans('delete_item'), "script" => $this->admin->getUrl(), "gfx" => "delete.png", "action" => "delete", "th" => 0, "td" => 1),
            "search" => array("func" => "group_admin_search", "alt_text" => $translator->trans('search'), "script" => $this->admin->getUrl(), "gfx" => "search.png", "action" => "search", "th" => 1, "td" => 0),
        );

        $TABLE_CONFIG = array(
            "db_name"     => "admin",
            "table_name"  => DB_TABLENAME_GROUPS,
            "alpha_index" => "name",
        );
        $URL_PARA = array(
            "page"         => 1,
            "alpha"        => '',
            "sort"         => 0,
            "direction"    => 'ASC',
            "num_per_page" => 25,
            "type"         => 'num'
        );

        $configuration = new Configuration();
        $configuration->menu    = $MENU_ARRAY;
        $configuration->buttons = $BUTTON;
        $configuration->table   = $TABLE_CONFIG;
        $configuration->default_parameters = $URL_PARA;

        $this->configuration = $configuration;
    }

    /**
     * {@inheritdoc}
     */
    public function render()
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $html = '';

        $action = getParam("action", 'list');
        $form_data = getParam("form_data");

        switch ($action) {
            case "edit":
                if (!$this->checkPrivilege('group_admin_edit')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                } else {
                    $id = getParam("id");
                    if (is_array($form_data)) {
                        $html = $this->change_one($action, $id, $form_data, $this->parameters);
                    } else {
                        $html = $this->form_change($action, $id, $this->parameters);
                    }
                }
                break;
            
            case "delete":
                if (!$this->checkPrivilege('group_admin_delete')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                } else {
                    if (!isset($_REQUEST["confirm_id"])) {
                        $id = getParam("id");
                        $html = $this->ask4delete($id, $this->parameters);
                    } else {
                        $confirm_id = getParam("confirm_id");
                        if ($this->delete_one($confirm_id)) {
                            $view->addMessage('STATUS', 'NOTICE', $translator->trans('the_entry_was_deleted_successfully'));
                        } else {
                            $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_could_not_be_deleted'));
                        }
                    }
                }
                break;
            
            case "search":
                if (!$this->checkPrivilege('group_admin_search')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                
                if (isset($_REQUEST["send_button"])) {
                    $this->parameters["page"] = 1;
                    $this->parameters["alpha"] = '';
                }
                
                $html = $this->show_search_form($action, $form_data, $this->parameters);
                
                if (is_array($form_data)) {
                    $html .= $this->get_list($this->parameters, $form_data);
                }
                break;
            
            case "new":
                if (!$this->checkPrivilege('group_admin_new')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                if (is_array($form_data)) {
                    $html = $this->new_one($action, $form_data, $this->parameters);
                } else {
                    $html = $this->form_new($action, $this->parameters);
                }
                break;
        }

        if (empty($html)) {
            if ($this->checkPrivilege('group_admin_view')) {
                $html = $this->get_list($this->parameters);
            }
        }

        return $html;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function funcsecbox($data = array())
    {
        $db = $this->container->get('database');
        $html = '';

        if (isset($data['id'])) {
            $db->query('SELECT access_id FROM ' . DB_TABLENAME_GROUP_ACCESSES . ' WHERE group_id=' . $data["id"] . ';');
            
            while ($db->next_record()) {
                $tmp_access[$db->f("access_id")] = 1;
            }
        }
        
        foreach ($GLOBALS['CONFIG']['GROUP_ACCESS'] as $access_id => $accesses) {
            $html .= '<table class="table table-striped" align="center" width="100%">';
            $html .= '<tr class="headers"><th colspan="2">' . $access_id . '</th></tr>';
            $class = '';
            foreach ($accesses as $sub_access_id) {
                $html .= '<tr><td>' . $sub_access_id . '</td><td width="20"><input type="checkbox" value="1" name="form_data[access_id][' . $access_id . '_' . $sub_access_id . ']" ' . (isset($tmp_access[$access_id . '_' . $sub_access_id]) ? " checked" : "") . '></td></tr>';
            }
            $html .= "</table>\n";
        }

        return $html;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_updated_by_name($data = array())
    {
        $data['updated_by'] = !empty($data['updated_by']) ? intval($data['updated_by']) : 0;

        return (new User($data['updated_by']))->getFullName();
    }

    /**
     * @param int $id
     * @param array $URL_PARA
     *
     * @return string
     */
    public function ask4delete($id, $URL_PARA)
    {
        return $this->admin->admin_get_confirm_table($id, $URL_PARA, 'delete');
    }

    /**
     * @param int $id
     *
     * @return int
     */
    public function delete_one($id)
    {
        $database = $this->container->get('database');
        $result = $this->admin->admin_set_delete($id);
        
        if ($result) {
            $database->query('DELETE FROM ' . DB_TABLENAME_GROUP_ACCESSES . ' WHERE group_id = "' . $id . '";');
            $database->query('DELETE FROM ' . DB_TABLENAME_USERS . ' WHERE group_id = "' . $id . '";');
        }

        return $result;
    }

    /**
     * @param string $action
     * @param array $URL_PARA
     * @param array $error_data
     *
     * @return string
     */
    public function form_new($action, $URL_PARA, $error_data = array())
    {
        $this->getView()->setFocus('#form_data_name');

        return $this->admin->admin_get_form($action, 0, $error_data, $URL_PARA);
    }

    /**
     * @param string $action
     * @param int $id
     * @param array $URL_PARA
     * @param array $error_data
     *
     * @return string
     */
    public function form_change($action, $id, $URL_PARA, $error_data = array())
    {
        $this->getView()->setFocus('#form_data_name');

        return $this->admin->admin_get_form($action, $id, $error_data, $URL_PARA);
    }

    /**
     * @param string $action
     * @param array $search_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function show_search_form($action, $search_data, $URL_PARA)
    {
        return $this->admin->admin_get_form($action, 0, $search_data, $URL_PARA);
    }

    /**
     * @param array $URL_PARA
     * @param array $search
     *
     * @return string
     */
    public function get_list($URL_PARA, $search = array())
    {
        return $this->admin->admin_get_list(0, $search, $URL_PARA);
    }

    /**
     * @param string $action
     * @param array $form_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function new_one($action, $form_data, $URL_PARA)
    {
        $translator = $this->container->get('translator');
        $database = $this->container->get('database');
        $profile = $this->container->get('current_user');
        $view = $this->getView();
        $html = '';
        
        $form_data["updated_by"] = $profile->getId();
        $form_data["updated_at"] = (new DateTime())->getDateTime();

        foreach ($GLOBALS['CONFIG']['GROUP_ACCESS'] as $access_id => $accesses) {
            if (isset($form_data["access_id"][$access_id])) {
                $tmp_access_id[] = $access_id;
            }
            foreach ($accesses as $sub_access_id) {
                if (isset($form_data["access_id"][$access_id . '_' . $sub_access_id])) {
                    $tmp_access_id[] = $access_id . '_' . $sub_access_id;
                }
            }
        }

        if (!isset($form_data["name"]) || $form_data["name"] == '') {
            $view->addMessage('STATUS', 'WARNING', $translator->trans('need_name'));
            return $this->form_change($action, 0, $URL_PARA, $form_data);
        }

        unset($form_data["access_id"]);

        $id = $this->admin->admin_set_new($form_data);
        if ($id > 0) {
            if (!empty($tmp_access_id) && is_array($tmp_access_id)) {
                foreach ($tmp_access_id as $access_id) {
                    $database->query('INSERT INTO ' . DB_TABLENAME_GROUP_ACCESSES . ' (group_id, access_id) VALUES (' . $id . ',"' . $access_id . '");');
                }
            }

            $view->addMessage('STATUS', 'NOTICE', $translator->trans('action_was_successfully'));
        } else {
            if ($id == -1) {
                $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_allready_exists'));
            } elseif ($id == -2) {
                $view->addMessage('STATUS', 'ERROR', $translator->trans('name_of_primary_key_is_empty'));
            } else {
                $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_could_not_be_created'));
            }

            $html = $this->form_new($action, $URL_PARA, $form_data);
        }

        return $html;
    }

    /**
     * @param string $action
     * @param int $id
     * @param array $form_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function change_one($action, $id, $form_data, $URL_PARA)
    {
        $translator = $this->container->get('translator');
        $database = $this->container->get('database');
        $profile = $this->container->get('current_user');
        $view = $this->getView();
        $html = '';
        
        $form_data["updated_by"] = $profile->getId();
        $form_data["updated_at"] = (new DateTime())->getDateTime();

        foreach ($GLOBALS['CONFIG']['GROUP_ACCESS'] as $access_id => $accesses) {
            if (isset($form_data["function_id"][$access_id])) {
                $tmp_access_id[] = $access_id;
            }
            
            foreach ($accesses as $sub_access_id) {
                if (isset($form_data["access_id"][$access_id . '_' . $sub_access_id])) {
                    $tmp_access_id[] = $access_id . '_' . $sub_access_id;
                }
            }
        }
        
        unset($form_data["access_id"]);
        
        $result = $this->admin->admin_set_update($id, $form_data);
        
        if ($result == true) {
            /* first delete all old entries */
            $database->query('DELETE FROM ' . DB_TABLENAME_GROUP_ACCESSES . ' WHERE group_id = ' . $form_data["id"] . ';');
            if (!empty($tmp_access_id) && is_array($tmp_access_id)) {
                foreach ($tmp_access_id as $access_id) {
                    $database->query('INSERT INTO ' . DB_TABLENAME_GROUP_ACCESSES . ' (group_id, access_id) VALUES (' . $form_data["id"] . ',"' . $access_id . '");');
                }
            }

            $view->addMessage('STATUS', 'NOTICE', $translator->trans('action_was_successfully'));
        } else {
            $view->addMessage('STATUS', 'WARNING', $translator->trans('no_data_has_been_updated'));

            $html = $this->form_change($action, $id, $URL_PARA, $form_data);
        }

        return $html;
    }
}