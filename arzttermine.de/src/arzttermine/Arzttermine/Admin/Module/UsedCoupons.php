<?php

namespace Arzttermine\Admin\Module;

use Arzttermine\Admin\Configuration;
use Arzttermine\Admin\Module;
use Arzttermine\Booking\Booking;
use Arzttermine\Coupon\Coupon;
use Arzttermine\Coupon\UsedCoupon;
use Arzttermine\MedicalSpecialty\MedicalSpecialty;

final class UsedCoupons extends Module {

    /**
     * @var string
     */
    protected $title = 'Gutscheine Admin';

    /**
     * @var string
     */
    protected $role = 'coupon_admin';

    /**
     * @var string
     */
    protected $slug = 'used_coupons';

    /**
     * {@inheritdoc}
     */
    public function boot()
    {
        $translator = $this->container->get('translator');

        $MENU_ARRAY = array(
            "email"                => array("sql" => 1, "name" => "Email", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "booking_id"           => array("sql" => 1, "name" => "Booking with", "show" => 1, "edit" => 0, "new" => 1, "search" => 1, "sort" => 0, "nowrap" => 1, "list_function" => array("name" => "get_data_booking", "para" => true)),
            "coupon_id"            => array("sql" => 1, "name" => 'Coupon code', "show" => 1, "edit" => 0, "new" => 1, "search" => 1, "sort" => 0, "nowrap" => 1, "list_function" => array("name" => "get_data_coupon", "para" => true)),
            "medical_specialty_id" => array("sql" => 1, "name" => "Medical specialty", "show" => 1, "edit" => 0, "new" => 1, "search" => 1, "sort" => 0, "nowrap" => 0, "list_function" => array("name" => "get_data_medical_specialty", "para" => true)),
            "status"               => array("sql" => 1, "name" => "Status", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_status", "para" => true), "form_input" => array("type" => "function", "name" => "get_form_status", "para" => true))
        );

        $BUTTON = array(
            "edit"   => array("func" => "coupon_admin_edit", "alt_text" => $translator->trans('edit_item'), "script" => $this->admin->getUrl(), "gfx" => "document_edit.png", "action" => "edit", "th" => 0, "td" => 1),
            "delete" => array("func" => "coupon_admin_delete", "alt_text" => $translator->trans('delete_item'), "script" => $this->admin->getUrl(), "gfx" => "delete.png", "action" => "delete", "th" => 0, "td" => 1),
            "search" => array("func" => "coupon_admin_search", "alt_text" => $translator->trans('search'), "script" => $this->admin->getUrl(), "gfx" => "search.png", "action" => "search", "th" => 1, "td" => 0)
        );

        $TABLE_CONFIG = array(
            "table_name"  => DB_TABLENAME_USED_COUPONS,
            "alpha_index" => "email",
        );

        $URL_PARA = array(
            'page'         => 1,
            'alpha'        => '',
            "sort"         => 0,
            "direction"    => 'DESC',
            "num_per_page" => 50,
            "type"         => 'num'
        );

        $configuration = new Configuration();
        $configuration->menu    = $MENU_ARRAY;
        $configuration->buttons = $BUTTON;
        $configuration->table   = $TABLE_CONFIG;
        $configuration->default_parameters = $URL_PARA;

        $this->configuration = $configuration;
    }

    /**
     * {@inheritdoc}
     */
    public function render()
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $html = '';

        $action = getParam("action", 'list');
        $form_data = getParam("form_data");

        switch ($action) {
            case "edit":
                if (!$this->checkPrivilege('coupon_admin_edit')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                } else {
                    $id = getParam("id");
                    if (is_array($form_data)) {
                        $html = $this->change_one($action, $id, $form_data, $this->parameters);
                    } else {
                        $html = $this->form_change($action, $id, $this->parameters);
                    }
                }
                break;
            
            case "delete":
                if (!$this->checkPrivilege('coupon_admin_delete')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                } else {
                    if (!isset($_REQUEST["confirm_id"])) {
                        $id = getParam("id");
                        $html = $this->ask4delete($id, $this->parameters);
                    } else {
                        $confirm_id = getParam("confirm_id");
                        if ($this->delete_one($confirm_id)) {
                            $view->addMessage('STATUS', 'NOTICE', $translator->trans('the_entry_was_deleted_successfully'));
                        } else {
                            $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_could_not_be_deleted'));
                        }
                    }
                }
                break;
            
            case "search":
                if (!$this->checkPrivilege('coupon_admin_search')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                
                if (isset($_REQUEST["send_button"])) {
                    $this->parameters["page"] = 1;
                    $this->parameters["alpha"] = '';
                }
                
                $html = $this->show_search_form($action, $form_data, $this->parameters);
                
                if (is_array($form_data)) {
                    $html .= $this->get_list($this->parameters, $form_data);
                }
                
                break;
        }

        if (empty($html)) {
            if ($this->checkPrivilege('coupon_admin_view')) {
                $html = $this->get_list($this->parameters);
            }
        }

        return $html;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_booking($data = array())
    {
        if (isset($data['booking_id'])) {
            $booking = new Booking(intval($data['booking_id']));

            return $booking->getLocation()->getName();
        }

        return 'N/A';
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_coupon($data = array())
    {
        if (isset($data['coupon_id'])) {
            $coupon = new Coupon(intval($data['coupon_id']));
            $types = $GLOBALS['CONFIG']['REFERRAL_PRICE_TYPES'];

            if (isset($types[$coupon->getOfferType()])) {
                $type = $types[$coupon->getOfferType()];

                return "{$type['company']} ({$type['price']})/[{$coupon->getCode()}]";
            }
        }

        return 'N/A';
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_medical_specialty($data = array())
    {
        if (isset($data['medical_specialty_id'])) {
            $ms = new MedicalSpecialty(intval($data['medical_specialty_id']));

            return $ms->getName();
        }

        return 'N/A';
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_status($data = array())
    {
        if (isset($data['status']) && UsedCoupon::$STATUS_TEXTS[$data['status']]) {
            return UsedCoupon::$STATUS_TEXTS[$data['status']];
        }

        return 'N/A';
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_form_status($data = array())
    {
        $html = '<select name="form_data[status]">';

        $selected = $data['status'];

        foreach (UsedCoupon::$STATUS_TEXTS as $code => $label) {
            $html .= '<option value="' . $code . '"' . ($selected == $code ? ' selected' : '') . '>' . $label . '</option>';
        }

        $html .= '</select>';

        return $html;
    }

    /**
     * @param array $URL_PARA
     * @param array $search
     *
     * @return string
     */
    public function get_list($URL_PARA, $search = array())
    {
        return $this->admin->admin_get_list(0, $search, $URL_PARA);
    }

    /**
     * @param string $action
     * @param array $form_data
     * @param array $URL_PARA
     *
     * @return mixed
     */
    public function new_one($action, $form_data, $URL_PARA)
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');

        if ($view->countMessages()) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('Bitte prüfen Sie Ihre Eingaben.'));
            return $this->form_change($action, 0, $URL_PARA, $form_data);
        }

        $email = filter_var($form_data['email'], FILTER_SANITIZE_EMAIL);

        $coupon = new UsedCoupon();
        $coupon
            ->setEmail($email)
            ->setStatus(intval($form_data['status']))
            ->saveNewObject();

        if ($coupon->isValid()) {
            $view->addMessage('STATUS', 'NOTICE', $translator->trans('action_was_successfully'));
        } else {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_could_not_be_created'));
        }

        return $action;
    }

    /**
     * @param string $action
     * @param int $id
     * @param $form_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function change_one($action, $id, $form_data, $URL_PARA)
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $html = '';

        $coupon = new UsedCoupon($id);
        // @todo: More specific error message
        if (!$coupon->isValid()) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('Bitte prüfen Sie Ihre Eingaben.'));
        }

        // 3. exit on errors
        if ($view->countMessages()) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('Bitte prüfen Sie Ihre Eingaben.'));
            return $this->form_change($action, $id, $URL_PARA, $form_data);
        }

        $email = filter_var($form_data['email'], FILTER_SANITIZE_EMAIL);

        $_result = $coupon
            ->setEmail($email)
            ->setStatus(intval($form_data['status']))
            ->save();

        if ($_result == true) {
            $view->addMessage('STATUS', 'NOTICE', $translator->trans('action_was_successfully'));
        } else {
            $view->addMessage('STATUS', 'WARNING', $translator->trans('no_data_has_been_updated'));

            $html = $this->form_change($action, $id, $URL_PARA, $form_data);
        }

        return $html;
    }

    /**
     * @param int $id
     * @param array $URL_PARA
     *
     * @return string
     */
    public function ask4delete($id, $URL_PARA)
    {
        return $this->admin->admin_get_confirm_table($id, $URL_PARA, 'delete');
    }

    /**
     * @param int $id
     *
     * @return int
     */
    public function delete_one($id)
    {
        return $this->admin->admin_set_delete($id);
    }

    /**
     * @param string $action
     * @param array $URL_PARA
     * @param array $error_data
     *
     * @return string
     */
    public function form_new($action, $URL_PARA, $error_data = array())
    {
        return $this->admin->admin_get_form($action, 0, $error_data, $URL_PARA);
    }

    /**
     * @param string $action
     * @param int $id
     * @param array $URL_PARA
     * @param array $error_data
     *
     * @return string
     */
    public function form_change($action, $id, $URL_PARA, $error_data = array())
    {
        return $this->admin->admin_get_form($action, $id, $error_data, $URL_PARA);
    }

    /**
     * @param string $action
     * @param array $search_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function show_search_form($action, $search_data, $URL_PARA)
    {
        return $this->admin->admin_get_form($action, 0, $search_data, $URL_PARA);
    }
}