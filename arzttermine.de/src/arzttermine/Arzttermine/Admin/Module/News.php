<?php

namespace Arzttermine\Admin\Module;

use Arzttermine\Admin\Configuration;
use Arzttermine\Admin\Module;
use Arzttermine\Core\DateTime;
use Arzttermine\User\User;

final class News extends Module {

    /**
     * @var string
     */
    protected $title = 'Blog Admin';

    /**
     * @var string
     */
    protected $role = 'blog_admin';

    /**
     * @var string
     */
    protected $slug = 'news';

    /**
     * {@inheritdoc}
     */
    public function boot()
    {
        $translator = $this->container->get('translator');

        $MENU_ARRAY = array(
            "id"           => array("sql" => 1, "name" => "ID", "show" => 1, "edit" => 0, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "status"       => array("sql" => 1, "name" => "Status", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "select", "values" => $GLOBALS['CONFIG']['BLOGPOST_STATUS']), "list_function" => array("name" => "get_status_name", "para" => true)),
            "topic"        => array("sql" => 1, "name" => "Topic", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0),
            "title"        => array("sql" => 1, "name" => "Titel", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "list_function" => array("name" => "get_title_name", "para" => true)),
            "slug"         => array("sql" => 1, "name" => "Permalink", "show" => 0, "edit" => 1, "new" => 1, "search" => 0, "sort" => 0, "nowrap" => 1),
            "published_at" => array("sql" => 1, "name" => "Datum", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "datetime"), "init_value" => (new DateTime())->getDateTime()),
            "body"         => array("sql" => 1, "name" => "Teaser/Text", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "editor_tinymce")),
            "created_at"   => array("sql" => 1, "name" => "Erstellungsdatum", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "none")),
            "created_by"   => array("sql" => 1, "name" => "Author", "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_created_by_name", "para" => true), "list_function" => array("name" => "get_created_by_name", "para" => true)),
            "updated_at"   => array("sql" => 1, "name" => "Letztes Update", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "updated_by"   => array("sql" => 1, "name" => "Letzter Bearbeiter", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_updated_by_name", "para" => true), "list_function" => array("name" => "get_updated_by_name", "para" => true)),
        );
        $BUTTON = array(
            "new"    => array("func" => "blog_admin_new", "alt_text" => $translator->trans('new_item'), "script" => $this->admin->getUrl(), "gfx" => "document_new.png", "action" => "new", "th" => 1, "td" => 0),
            "edit"   => array("func" => "blog_admin_edit", "alt_text" => $translator->trans('edit_item'), "script" => $this->admin->getUrl(), "gfx" => "document_edit.png", "action" => "edit", "th" => 0, "td" => 1),
            "delete" => array("func" => "blog_admin_delete", "alt_text" => $translator->trans('delete_item'), "script" => $this->admin->getUrl(), "gfx" => "delete.png", "action" => "delete", "th" => 0, "td" => 1),
            "search" => array("func" => "blog_admin_search", "alt_text" => $translator->trans('search'), "script" => $this->admin->getUrl(), "gfx" => "search.png", "action" => "search", "th" => 1, "td" => 0),
        );
        $TABLE_CONFIG = array(
            "table_name"  => DB_TABLENAME_BLOGPOSTS,
            "alpha_index" => "title",
        );
        $URL_PARA = array(
            'page'         => 1,
            'alpha'        => '',
            'sort'         => 0,
            'direction'    => 'DESC',
            'num_per_page' => 25,
            'type'         => 'num'
        );

        $configuration = new Configuration();
        $configuration->menu    = $MENU_ARRAY;
        $configuration->buttons = $BUTTON;
        $configuration->table   = $TABLE_CONFIG;
        $configuration->default_parameters = $URL_PARA;

        $this->configuration = $configuration;
    }

    /**
     * {@inheritdoc}
     */
    public function render()
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $html = '';

        $action = getParam("action", 'list');
        $form_data = getParam("form_data");

        switch ($action) {
            case "edit":
                if (!$this->checkPrivilege('blog_admin_edit')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                } else {
                    $id = getParam("id");
                    if (is_array($form_data)) {
                        $html = $this->change_one($action, $id, $form_data, $this->parameters);
                    } else {
                        $html = $this->form_change($action, $id, $this->parameters);
                    }
                }
                break;
            
            case "delete":
                if (!$this->checkPrivilege('blog_admin_delete')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                } else {
                    if (!isset($_REQUEST["confirm_id"])) {
                        $id = getParam("id");
                        $html = $this->ask4delete($id, $this->parameters);
                    } else {
                        $confirm_id = getParam("confirm_id");
                        if ($this->delete_one($confirm_id)) {
                            $view->addMessage('STATUS', 'NOTICE', $translator->trans('the_entry_was_deleted_successfully'));
                        } else {
                            $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_could_not_be_deleted'));
                        }
                    }
                }
                break;
            
            case "search":
                if (!$this->checkPrivilege('blog_admin_search')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                
                if (isset($_REQUEST["send_button"])) {
                    $this->parameters["page"] = 1;
                    $this->parameters["alpha"] = '';
                }

                $html = $this->show_search_form($action, $form_data, $this->parameters);
                if (is_array($form_data)) {
                    $html .= $this->get_list($this->parameters, $form_data);
                }
                break;
            
            case "new":
                if (!$this->checkPrivilege('blog_admin_new')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                if (is_array($form_data)) {
                    $html = $this->new_one($action, $form_data, $this->parameters);
                } else {
                    $html = $this->form_new($action, $this->parameters);
                }
                break;
        }

        if (empty($html)) {
            if ($this->checkPrivilege('blog_admin_view')) {
                $html = $this->get_list($this->parameters);
            }
        }

        return $html;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_status_name($data = array())
    {
        if (isset($GLOBALS['CONFIG']['BLOGPOST_STATUS'][$data['status']])) {
            return $GLOBALS['CONFIG']['BLOGPOST_STATUS'][$data['status']];
        }
        
        return 'N/A';
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_title_name($data = array())
    {
        $link = $GLOBALS['CONFIG']['URL_HTTP_LIVE'] . '/news/' . $data['slug'];
        $title = $data['title'];

        return '<a href="' . $link . '" target="_blank">' . $title . '</a>';
    }
    
    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_created_by($data = array())
    {
        $data['created_by'] = !empty($data['created_by']) ? intval($data['created_by']) : 0;

        return (new User($data['created_by']))->getFullName();
    }

    /**
     * @return array
     */
    public function get_select_created_by()
    {
        $_user = new User();
        $users = array();
        
        /** @var User $_user */
        foreach ($_user->findObjects('1=1 ORDER BY last_name') as $_user) {
            $users[$_user->getId()] = $_user->getFullName();
        }

        return $users;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_updated_by_name($data = array())
    {
        $data['updated_by'] = !empty($data['updated_by']) ? intval($data['updated_by']) : 0;

        return (new User($data['updated_by']))->getFullName();
    }

    /**
     * @param array $URL_PARA
     * @param array $search
     *
     * @return string
     */
    public function get_list($URL_PARA, $search = array())
    {
        return $this->admin->admin_get_list(0, $search, $URL_PARA);
    }

    /**
     * @param string $action
     * @param array $form_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function new_one($action, $form_data, $URL_PARA)
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $profile = $this->container->get('current_user');
        $html = '';

        // 1. set defaults
        $form_data["created_by"] = $profile->getId();
        $form_data["created_at"] = (new DateTime())->getDateTime();

        // 2. check
        if (1 > mb_strlen(sanitize_slug($form_data["slug"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
            $view->addMessage('admin_message_slug', 'ERROR', $translator->trans('Der Permalink ist nicht gültig!'));
        }
        if (1 > mb_strlen(sanitize_title($form_data["topic"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
            $view->addMessage('admin_message_topic', 'ERROR', $translator->trans('Geben Sie bitte ein Topic ein!'));
        }
        if (1 > mb_strlen(sanitize_title($form_data["title"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
            $view->addMessage('admin_message_title', 'ERROR', $translator->trans('unvalid_title'));
        }
        if (1 > mb_strlen($form_data["body"], $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
            $view->addMessage('admin_message_body', 'ERROR', $translator->trans('Geben Sie bitte einen Text ein!'));
        }

        // 3. exit on errors
        if ($view->countMessages()) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('Bitte prüfen Sie Ihre Eingaben.'));
            return $this->form_change($action, 0, $URL_PARA, $form_data);
        }
        // 4. sanitize
        $form_data["slug"] = sanitize_slug($form_data["slug"]);
        $form_data["topic"] = sanitize_title($form_data["topic"]);
        $form_data["title"] = sanitize_title($form_data["title"]);
        // $form_data["body"] = sanitize_textarea_field($form_data["body"]);

        $_id = $this->admin->admin_set_new($form_data);
        if ($_id > 0) {
            $view->addMessage('STATUS', 'NOTICE', $translator->trans('action_was_successfully'));
        } elseif ($_id == -1) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_allready_exists'));
            $html = $this->form_new($action, $URL_PARA, $form_data);
        } elseif ($_id == -2) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('name_of_primary_key_is_empty'));
            $html = $this->form_new($action, $URL_PARA, $form_data);
        } else {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_could_not_be_created'));
        }

        return $html;
    }

    /**
     * @param string $action
     * @param int $id
     * @param array $form_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function change_one($action, $id, $form_data, $URL_PARA)
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $profile = $this->container->get('current_user');
        $html = '';

        // 1. set defaults
        $form_data["updated_by"] = $profile->getId();
        $form_data["updated_at"] = (new DateTime())->getDateTime();

        // 2. check
        if (1 > mb_strlen(sanitize_slug($form_data["slug"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
            $view->addMessage('admin_message_slug', 'ERROR', $translator->trans('Der Permalink ist nicht gültig!'));
        }
        if (1 > mb_strlen(sanitize_title($form_data["topic"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
            $view->addMessage('admin_message_topic', 'ERROR', $translator->trans('Geben Sie bitte ein Topic ein!'));
        }
        if (1 > mb_strlen(sanitize_title($form_data["title"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
            $view->addMessage('admin_message_title', 'ERROR', $translator->trans('unvalid_title'));
        }
        if (1 > mb_strlen($form_data["body"], $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
            $view->addMessage('admin_message_body', 'ERROR', $translator->trans('Geben Sie bitte einen Text ein!'));
        }

        // 3. exit on errors
        if ($view->countMessages()) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('Bitte prüfen Sie Ihre Eingaben.'));
            return $this->form_change($action, $id, $URL_PARA, $form_data);
        }
        
        // 4. sanitize
        $form_data["slug"] = sanitize_slug($form_data["slug"]);
        $form_data["topic"] = sanitize_title($form_data["topic"]);
        $form_data["title"] = sanitize_title($form_data["title"]);

        $_result = $this->admin->admin_set_update($id, $form_data);
        if ($_result == true) {
            $view->addMessage('STATUS', 'NOTICE', $translator->trans('action_was_successfully'));
            return '';
        }

        if ($_result == false) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('no_data_has_been_updated'));
            $html = $this->form_change($action, $id, $URL_PARA, $form_data);
        }

        return $html;
    }

    /**
     * @param int $id
     * @param array $URL_PARA
     *
     * @return string
     */
    public function ask4delete($id, $URL_PARA)
    {
        return $this->admin->admin_get_confirm_table($id, $URL_PARA, 'delete');
    }

    /**
     * @param int $id
     *
     * @return int
     */
    public function delete_one($id)
    {
        return $this->admin->admin_set_delete($id);
    }

    /**
     * @param string $action
     * @param array $URL_PARA
     * @param array $error_data
     *
     * @return string
     */
    public function form_new($action, $URL_PARA, $error_data = array())
    {
        $html = $this->admin->admin_get_form($action, 0, $error_data, $URL_PARA);
        
        $html .= '
<script type="text/javascript">
    $(document).ready(function() {
        $("#form_data_title").slugIt({
            events: \'blur\',
            output: \'#form_data_slug\'
        });
    });
</script>';

        $this->getView()
            ->setFocus('#form_data_title')
            ->addJsInclude(getStaticUrl('jquery/plugins/jquery.slugit.js'));

        return $html;
    }

    /**
     * @param string $action
     * @param int $id
     * @param array $URL_PARA
     * @param array $error_data
     *
     * @return string
     */
    public function form_change($action, $id, $URL_PARA, $error_data = array())
    {
        $html = $this->admin->admin_get_form($action, $id, $error_data, $URL_PARA);
        
        $html .= '
<script type="text/javascript">
    $(document).ready(function() {
        if ($("#form_data_slug").val().length < 1) {
            $("#form_data_title").slugIt({
            events: \'blur\',
            output: \'#form_data_slug\'
            });
        }
    });
</script>';

        $this->getView()
            ->setFocus('#form_data_title')
            ->addJsInclude(getStaticUrl('jquery/plugins/jquery.slugit.js'));

        return $html;
    }

    /**
     * @param string $action
     * @param array $search_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function show_search_form($action, $search_data, $URL_PARA)
    {
        return $this->admin->admin_get_form($action, 0, $search_data, $URL_PARA);
    }
}