<?php

namespace Arzttermine\Admin\Module;

use Arzttermine\Admin\Configuration;
use Arzttermine\Admin\Module;
use Arzttermine\Core\DateTime;
use Arzttermine\User\User;
use Arzttermine\Widget\Container;

final class Widgets extends Module {

    /**
     * @var string
     */
    protected $title = 'Widgets Admin';

    /**
     * @var string
     */
    protected $role = 'widget_admin';

    /**
     * @var string
     */
    protected $slug = 'widgets';

    /**
     * {@inheritdoc}
     */
    public function boot()
    {
        $translator = $this->container->get('translator');

        $MENU_ARRAY = array(
            "id"             => array("sql" => 1, "name" => 'ID', "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "status"         => array("sql" => 1, "name" => 'Status', "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "select", "values" => $GLOBALS['CONFIG']['WIDGET_STATUS']), "list_function" => array("name" => "get_data_status", "para" => true)),
            "name"           => array("sql" => 1, "name" => 'Name', "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "slug"           => array("sql" => 1, "name" => 'Slug', "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_slug", "para" => true)),
            "pattern"        => array("sql" => 1, "name" => 'Pattern (ID)', "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "html_head"      => array("sql" => 1, "name" => 'html_head', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => ' style="width:300px;height:60px;"')),
            "html_body"      => array("sql" => 1, "name" => 'html_body', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => ' style="width:300px;height:60px;"')),
            "comment_intern" => array("sql" => 1, "name" => 'Kommentar <span style="color:#ff0000;">(intern)', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => ' style="width:300px;height:60px;"')),
            "views"          => array("sql" => 1, "name" => 'Views', "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "api_user_ids"   => array("sql" => 1, "name" => 'API UserIds', "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_api_user_ids", "para" => true)),
            "api"            => array("sql" => 0, "name" => 'API', "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_api", "para" => 1), "form_function" => array("name" => "get_data_api", "para" => 1), "form_input" => array("type" => "none")),
            "created_at"     => array("sql" => 1, "name" => "Erstellungsdatum", "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "created_by"     => array("sql" => 1, "name" => "Erstellt von", "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_created_by", "para" => true), "list_function" => array("name" => "get_data_created_by", "para" => true)),
            "updated_at"     => array("sql" => 1, "name" => "Letztes Update", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "updated_by"     => array("sql" => 1, "name" => "Letzter Bearbeiter", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_updated_by", "para" => true), "list_function" => array("name" => "get_data_updated_by", "para" => true)),
        );

        $BUTTON = array(
            "new"    => array("func" => "widget_admin_new", "alt_text" => $translator->trans('new_item'), "script" => $this->admin->getUrl(), "gfx" => "document_new.png", "action" => "new", "th" => 1, "td" => 0),
            "edit"   => array("func" => "widget_admin_edit", "alt_text" => $translator->trans('edit_item'), "script" => $this->admin->getUrl(), "gfx" => "document_edit.png", "action" => "edit", "th" => 0, "td" => 1),
            "delete" => array("func" => "widget_admin_delete", "alt_text" => $translator->trans('delete_item'), "script" => $this->admin->getUrl(), "gfx" => "delete.png", "action" => "delete", "th" => 0, "td" => 1),
            "search" => array("func" => "widget_admin_search", "alt_text" => $translator->trans('search'), "script" => $this->admin->getUrl(), "gfx" => "search.png", "action" => "search", "th" => 1, "td" => 0),
        );
        $TABLE_CONFIG = array(
            "table_name"  => DB_TABLENAME_WIDGETS,
            "alpha_index" => "slug",
        );
        $URL_PARA = array(
            'page'         => 1,
            'alpha'        => '',
            "sort"         => 0,
            "direction"    => 'DESC',
            "num_per_page" => 25,
            "type"         => 'num'
        );

        $configuration = new Configuration();
        $configuration->menu    = $MENU_ARRAY;
        $configuration->buttons = $BUTTON;
        $configuration->table   = $TABLE_CONFIG;
        $configuration->default_parameters = $URL_PARA;

        $this->configuration = $configuration;
    }

    /**
     * {@inheritdoc}
     */
    public function render()
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $html = '';

        $action = getParam("action", 'list');
        $form_data = getParam("form_data");

        switch ($action) {
            case "edit":
                if (!$this->checkPrivilege('widget_admin_edit')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                } else {
                    $id = getParam("id");
                    if (is_array($form_data)) {
                        $html = $this->change_one($action, $id, $form_data, $this->parameters);
                    } else {
                        $html = $this->form_change($action, $id, $this->parameters);
                    }
                }
                break;
            
            case "delete":
                if (!$this->checkPrivilege('widget_admin_delete')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                } else {
                    if (!isset($_REQUEST["confirm_id"])) {
                        $id = getParam("id");
                        $html = $this->ask4delete($id, $this->parameters);
                    } else {
                        $confirm_id = getParam("confirm_id");
                        if ($this->delete_one($confirm_id)) {
                            $view->addMessage('STATUS', 'NOTICE', $translator->trans('the_entry_was_deleted_successfully'));
                        } else {
                            $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_could_not_be_deleted'));
                        }
                    }
                }
                break;

            case "search":
                if (!$this->checkPrivilege('widget_admin_search')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                
                if (isset($_REQUEST["send_button"])) {
                    $this->parameters["page"] = 1;
                    $this->parameters["alpha"] = '';
                }
                
                $html = $this->show_search_form($action, $form_data, $this->parameters);
                if (is_array($form_data)) {
                    $html .= $this->get_list($this->parameters, $form_data);
                }
                break;
            
            case "new":
                if (!$this->checkPrivilege('widget_admin_new')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                if (is_array($form_data)) {
                    $html = $this->new_one($action, $form_data, $this->parameters);
                } else {
                    $html = $this->form_new($action, $this->parameters);
                }
                break;
        }

        if (empty($html)) {
            if ($this->checkPrivilege('widget_admin_view')) {
                $html = $this->get_list($this->parameters);
            }
        }

        return $html;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_created_by($data = array())
    {
        $data['created_by'] = !empty($data['created_by']) ? intval($data['created_by']) : 0;

        return (new User($data['created_by']))->getFullName();
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_updated_by($data = array())
    {
        $data['updated_by'] = !empty($data['updated_by']) ? intval($data['updated_by']) : 0;

        return (new User($data['updated_by']))->getFullName();
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_status($data = array())
    {
        if (isset($GLOBALS['CONFIG']['WIDGET_STATUS'][$data['status']])) {
            return $GLOBALS['CONFIG']['WIDGET_STATUS'][$data['status']];
        }

        return 'N/A';
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_slug($data = array())
    {
        $widget = new Container($data['id']);
        if ($widget->isValid()) {
            $link = '<a href="' . $widget->getDefaultUrl() . '">' . $widget->getSlug() . '</a>';
        } else {
            $link = $data['slug'];
        }

        return $link;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_api_user_ids($data = array())
    {
        $links = '';
        $user_ids = explode(',', $data['api_user_ids']);
        if (!empty($user_ids)) {
            foreach ($user_ids as $user_id) {
                $user = new User($user_id);
                if ($user->isValid()) {
                    $links .= (empty($links) ? '' : '<br />') . '<a href="'.$user->getAdminEditUrl().'">' . $user->getName() . '</a>';
                }
            }
        }

        return $links;
    }

    function get_data_api($data = 0)
    {
        return '<a href="/api/v1/widget-data-export.csv?widget='.$data['slug'].'" target="_blank">API-Endpunkt: widget-data-export.csv</a>';
    }

    /**
     * @param array $URL_PARA
     * @param array $search
     *
     * @return string
     */
    public function get_list($URL_PARA, $search = array())
    {
        return $this->admin->admin_get_list(0, $search, $URL_PARA);
    }
    
    /**
     * @param string $action
     * @param array $form_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function new_one($action, $form_data, $URL_PARA)
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $profile = $this->container->get('current_user');
        $html = '';

        // 1. set defaults
        $form_data["created_by"] = $profile->getId();
        $form_data["created_at"] = (new DateTime())->getDateTime();

        // 2. check
        if (1 > mb_strlen(sanitize_slug($form_data["slug"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
            $view->addMessage('admin_message_slug', 'ERROR', $translator->trans('Es muss ein Slug angegeben werden'));
        }
        if (1 > mb_strlen(sanitize_key($form_data["pattern"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
            $view->addMessage('admin_message_pattern', 'ERROR', $translator->trans('Es muss ein Pattern angegeben werden'));
        }
        if (1 > mb_strlen(sanitize_title($form_data["name"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
            $view->addMessage('admin_message_name', 'ERROR', $translator->trans('Es muss ein Name angegeben werden'));
        }

        // if there are checkboxes than set the keys to make sure that deselected checkboxes are beeing set
        $form_data = $this->admin->set_unselected_checkbox_values($form_data);

        // 3. exit on errors
        if ($view->countMessages()) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('Bitte prüfen Sie Ihre Eingaben.'));
            return $this->form_change($action, 0, $URL_PARA, $form_data);
        }
        // 4. sanitize
        $form_data["slug"] = sanitize_slug($form_data["slug"]);
        $form_data["name"] = sanitize_title($form_data["name"]);

        $_id = $this->admin->admin_set_new($form_data);
        if ($_id > 0) {
            $view->addMessage('STATUS', 'NOTICE', $translator->trans('Das Widget wurde erfolgreich mit der ID ' . $_id . ' angelegt.<br /><a href="/administration/widgets?edit&id=' . $_id . '">Widget bearbeiten</a>'));
        } elseif ($_id == -1) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_allready_exists'));
            $html = $this->form_new($action, $URL_PARA, $form_data);
        } elseif ($_id == -2) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('name_of_primary_key_is_empty'));
            $html = $this->form_new($action, $URL_PARA, $form_data);
        } else {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_could_not_be_created'));
        }

        return $html;
    }

    /**
     * @param string $action
     * @param int $id
     * @param array $form_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function change_one($action, $id, $form_data, $URL_PARA)
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $profile = $this->container->get('current_user');
        $html = '';

        // 1. set defaults
        $form_data["updated_by"] = $profile->getId();
        $form_data["updated_at"] = (new DateTime)->getDateTime();

        // 2. check
        if (1 > mb_strlen(sanitize_slug($form_data["slug"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
            $view->addMessage('admin_message_slug', 'ERROR', $translator->trans('Es muss ein Slug angegeben werden'));
        }
        if (1 > mb_strlen(sanitize_key($form_data["pattern"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
            $view->addMessage('admin_message_pattern', 'ERROR', $translator->trans('Es muss ein Pattern angegeben werden'));
        }
        if (1 > mb_strlen(sanitize_title($form_data["name"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
            $view->addMessage('admin_message_name', 'ERROR', $translator->trans('Es muss ein Name angegeben werden'));
        }

        // if there are checkboxes than set the keys to make sure that deselected checkboxes are beeing set
        $form_data = $this->admin->set_unselected_checkbox_values($form_data);

        // 3. exit on errors
        if ($view->countMessages()) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('Bitte prüfen Sie Ihre Eingaben.'));
            return $this->form_change($action, $id, $URL_PARA, $form_data);
        }
        
        // 4. sanitize
        $form_data["slug"] = sanitize_slug($form_data["slug"]);
        $form_data["name"] = sanitize_title($form_data["name"]);

        // if there are checkboxes than set the keys to make sure that deselected checkboxes are beeing set
        $form_data = $this->admin->set_unselected_checkbox_values($form_data);

        $_result = $this->admin->admin_set_update($id, $form_data);
        if ($_result == true) {
            $view->addMessage('STATUS', 'NOTICE', $translator->trans('action_was_successfully'));
            return '';
        }

        if ($_result == false) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('no_data_has_been_updated'));
            $html = $this->form_change($action, $id, $URL_PARA, $form_data);
        }

        return $html;
    }

    /**
     * @param int $id
     * @param array $URL_PARA
     *
     * @return string
     */
    public function ask4delete($id, $URL_PARA)
    {
        return $this->admin->admin_get_confirm_table($id, $URL_PARA, 'delete');
    }

    /**
     * @param int $id
     *
     * @return int
     */
    public function delete_one($id)
    {
        return $this->admin->admin_set_delete($id);
    }

    /**
     * @param string $action
     * @param array $URL_PARA
     * @param array $error_data
     *
     * @return string
     */
    public function form_new($action, $URL_PARA, $error_data = array())
    {
        $this->getView()->setFocus('#form_data_name');

        return $this->admin->admin_get_form($action, 0, $error_data, $URL_PARA);
    }

    /**
     * @param string $action
     * @param int $id
     * @param array $URL_PARA
     * @param array $error_data
     *
     * @return string
     */
    public function form_change($action, $id, $URL_PARA, $error_data = array())
    {
        $this->getView()->setFocus('#form_data_name');

        return $this->admin->admin_get_form($action, $id, $error_data, $URL_PARA);
    }

    /**
     * @param string $action
     * @param array $search_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function show_search_form($action, $search_data, $URL_PARA)
    {
        return $this->admin->admin_get_form($action, 0, $search_data, $URL_PARA);
    }
}