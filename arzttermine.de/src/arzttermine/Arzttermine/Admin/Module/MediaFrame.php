<?php

namespace Arzttermine\Admin\Module;

use Arzttermine\Admin\Configuration;
use Arzttermine\Admin\Module;
use Arzttermine\Media\Media;

final class MediaFrame extends Module {

    /**
     * @var string
     */
    protected $title = 'Media Admin';

    /**
     * @var string
     */
    protected $role = 'media_admin';

    /**
     * @var string
     */
    protected $slug = 'media';

    /**
     * {@inheritdoc}
     */
    public function boot()
    {
        $MENU_ARRAY = array(
            "id"                           => array("sql" => 1, "name" => "ID", "show" => 0, "show_iframe" => 0, "edit" => 0, "new" => 0, "search" => 0, "sort" => 0, "nowrap" => 0),
            "filename"                     => array("sql" => 1, "name" => "Filename", "show" => 1, "show_iframe" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1),
            "category"                     => array("sql" => 1, "name" => "Verzeichnis", "show" => 1, "show_iframe" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "list_category", "para" => true), "form_input" => array("type" => "select", "values" => array())),
            "filesize"                     => array("sql" => 1, "name" => "Dateigröße", "show" => 1, "show_iframe" => 1, "edit" => 0, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "list_size", "para" => true)),
            "width"                        => array("sql" => 1, "name" => "Breite", "show" => 1, "show_iframe" => 1, "edit" => 0, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1),
            "height"                       => array("sql" => 1, "name" => "Höhe", "show" => 1, "show_iframe" => 1, "edit" => 0, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1),
            "label"                        => array("sql" => 1, "name" => "Caption", "show" => 1, "show_iframe" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "description"                  => array("sql" => 1, "name" => "Description", "show" => 1, "show_iframe" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "tags"                         => array("sql" => 1, "name" => "Tags", "show" => 1, "show_iframe" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "mimetype"                     => array("sql" => 1, "name" => "MimeType", "show" => 1, "show_iframe" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "created_at"                   => array("sql" => 1, "name" => "Erstellungsdatum", "show" => 0, "show_iframe" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "created_by"                   => array("sql" => 1, "name" => "Ersteller", "show" => 0, "show_iframe" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "form_created_by", "para" => true)),
            "updated_at"                   => array("sql" => 1, "name" => "Letztes Update", "show" => 1, "show_iframe" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "updated_by"                   => array("sql" => 1, "name" => "Letzter Bearbeiter", "show" => 1, "show_iframe" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "form_updated_by", "para" => true), "list_function" => array("name" => "form_updated_by", "para" => true)),
            "file_upload"                  => array("sql" => 0, "name" => "Dateiupload", "show" => 0, "show_iframe" => 0, "edit" => 1, "new" => 1, "search" => 0, "sort" => 0, "nowrap" => 0, "form_input" => array("type" => "file")),
            "tip_upload_max_filesize_text" => array("sql" => 1, "name" => '<p>Medien können max. ' . ini_get("upload_max_filesize") . ' groß sein.</p>', "show" => 0, "show_iframe" => 0, "edit" => 1, "new" => 1, "search" => 0, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "none")),
        );

        $BUTTON = array(
            "new"    => array("func" => "media_admin_new", "gfx" => "document_new.png", "alt_text" => "Neuer Eintrag", "script" => $this->admin->getUrl(), "action" => "new", "th" => 1, "td" => 0),
            "edit"   => array("func" => "media_admin_edit", "gfx" => "document_edit.png", "alt_text" => "Bearbeiten", "script" => $this->admin->getUrl(), "action" => "edit", "th" => 0, "td" => 1),
            "delete" => array("func" => "media_admin_delete", "gfx" => "delete.png", "alt_text" => "Löschen", "script" => $this->admin->getUrl(), "action" => "delete", "th" => 0, "td" => 1),
            "search" => array("func" => "media_admin_search", "gfx" => "search.png", "alt_text" => "Suchen", "script" => $this->admin->getUrl(), "action" => "search", "th" => 1, "td" => 0),
        );

        $TABLE_CONFIG = array(
            "db_name"     => "admin",
            "table_name"  => DB_TABLENAME_MEDIAS,
            "alpha_index" => "filename",
        );

        $URL_PARA = array(
            "page"         => 1,
            "alpha"        => '',
            "sort"         => 'filename',
            "direction"    => 0,
            "num_per_page" => 25,
            "type"         => 'alpha',
        );

        $configuration = new Configuration();
        $configuration->menu    = $MENU_ARRAY;
        $configuration->buttons = $BUTTON;
        $configuration->table   = $TABLE_CONFIG;
        $configuration->default_parameters = $URL_PARA;

        $this->configuration = $configuration;
    }

    /**
     * {@inheritdoc}
     */
    public function render()
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $html = '';

        $action = getParam("action", 'list');
        $form_data = getParam("form_data");

        switch ($action) {
            case 'image_media_send_to_editor':
                $_html = image_media_send_to_editor(getParam('id'), getParam('form'));
                // send the html via javascript and exit();
                $this->media_send_to_editor($_html);
                break;

            case "search":
                if (!$this->checkPrivilege('media_admin_search')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                
                if (isset($_REQUEST["send_button"])) {
                    $URL_PARA["page"] = 1;
                }

                $html = $this->show_search_form($action, $form_data, $this->parameters);
                
                if (is_array($form_data)) {
                    $html .= $this->get_list($this->parameters, $form_data);
                }
                
                break;
        }

        if (empty($html)) {
            if ($this->checkPrivilege('media_admin_view')) {
                $html = $this->get_list($this->parameters);
            }
        }

        return $html;
    }

    /**
     * @param $URL_PARA
     * @param int $search
     *
     * @return \Arzttermine\View\Html
     */
    public function get_list($URL_PARA, $search = array())
    {
        return $this->admin->admin_get_list_gfx_iframe(0, $search, $URL_PARA);
    }

    /**
     * @param $action
     * @param $search_data
     * @param $URL_PARA
     *
     * @return string
     */
    public function show_search_form($action, $search_data, $URL_PARA)
    {
        return $this->admin->admin_get_form($action, 0, $search_data, $URL_PARA);
    }

    /**
     * @param $html
     */
    public function media_send_to_editor($html)
    {
        ?>
        <script type="text/javascript">
            /* <![CDATA[ */
            var win = window.dialogArguments || opener || parent || top;
            win.send_to_editor('<?php echo addslashes($html); ?>');
            /* ]]> */
        </script>
        <?php
        exit;
    }

    /**
     * @param $html
     * @param $id
     * @param $caption
     * @param $title
     * @param $align
     * @param $url
     * @param $size
     * @param string $alt
     *
     * @return string
     */
    public function image_add_caption($html, $id, $caption, $title, $align, $url, $size, $alt = '')
    {
        $id = (0 < (int)$id) ? 'attachment_' . $id : '';

        if (!preg_match('/width="([0-9]+)/', $html, $matches)) {
            return $html;
        }

        $width = $matches[1];

        $html = preg_replace('/(class=["\'][^\'"]*)align(none|left|right|center)\s?/', '$1', $html);
        if (empty($align)) {
            $align = 'none';
        }

        $shcode = '[caption id="' . $id . '" align="align' . $align
                  . '" width="' . $width . '" caption="' . addslashes($caption) . '"]' . $html . '[/caption]';

        return $shcode;
    }

    /**
     * @param $id
     * @param $form
     *
     * @return string
     */
    public function image_media_send_to_editor($id, $form)
    {
        $_media = new Media($id);
        if (!$_media->valid()) {
            return '';
        }

        $url = isset($form['url']) ? $form['url'] : '';
        $align = !empty($form['align']) ? $form['align'] : 'none';
        $size = !empty($form['image-size']) ? $form['image-size'] : 'full';
        $alt = !empty($form['image_alt']) ? $form['image_alt'] : '';
        $title = $alt;

        $hwstring = '';
        if (isset($form['width'])) {
            $hwstring .= 'width="' . intval($form['width']) . '" ';
        }
        if (isset($form['height'])) {
            $hwstring .= 'height="' . intval($form['height']) . '" ';
        }

        $class = 'align' . esc_attr($align) . ' size-' . esc_attr($size) . ' wp-image-' . $id;
        $html = '<img src="' . esc_attr($_media->getUrl()) . '" alt="' . esc_attr($alt) . '" title="' . esc_attr($title) . '" ' . $hwstring . 'class="' . $class . '" />';

        if ($url) {
            $html = '<a href="' . esc_attr($url) . "\"{$form['rel']}>{$html}</a>";
        }

        return $html;
    }
}