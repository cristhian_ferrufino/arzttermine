<?php

namespace Arzttermine\Admin\Module;

use Arzttermine\Admin\Configuration;
use Arzttermine\Admin\Module;
use Arzttermine\Booking\Booking;
use Arzttermine\Core\DateTime;
use Arzttermine\Integration\Integration;
use Arzttermine\Integration\IntegrationConnection;
use Arzttermine\Location\Location;
use Arzttermine\Maps\GoogleMap;
use Arzttermine\Media\Asset;
use Arzttermine\MedicalSpecialty\MedicalSpecialty;
use Arzttermine\User\User;
use NGS\Managers\BookingsManager;
use Stash\Driver\FileSystem;
use Stash\Pool;

final class Locations extends Module {

    /**
     * @var string
     */
    protected $title = 'Praxen Admin';

    /**
     * @var string
     */
    protected $role = 'location_admin';

    /**
     * @var string
     */
    protected $slug = 'locations';

    /**
     * {@inheritdoc}
     */
    public function boot()
    {
        $translator = $this->container->get('translator');

        $MENU_ARRAY = array(
            "id"                      => array("sql" => 1, "name" => "ID", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "open_bookings"           => array("sql" => 0, "name" => "Open Bookings", "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_open_bookings_count", "para" => true), "list_function" => array("name" => "get_open_bookings_count", "para" => true)),
            "html_title"              => array("sql" => 1, "name" => 'HTML-Title', "show" => 0, "edit" => 1, "new" => 1, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("template" => "admin/_form_text_counter.tpl")),
            "html_meta_description"   => array("sql" => 1, "name" => 'HTML-Meta-Description', "show" => 0, "edit" => 1, "new" => 1, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("template" => "admin/_form_text_counter.tpl")),
            "html_meta_keywords"      => array("sql" => 1, "name" => 'HTML-Meta-Keywords', "show" => 0, "edit" => 1, "new" => 1, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("template" => "admin/_form_text_counter.tpl")),
            "googlepreview"           => array("sql" => 0, "name" => 'GooglePreview', "show" => 0, "edit" => 1, "new" => 1, "search" => 0, "sort" => 0, "nowrap" => 0, "form_input" => array("type" => "function", "name" => "get_form_googlepreview", "para" => true)),
            "status"                  => array("sql" => 1, "name" => "Status", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "select", "values" => $GLOBALS['CONFIG']['LOCATION_STATUS']), "list_function" => array("name" => "get_data_status", "para" => true)),
            "integration_id"          => array("sql" => 1, "name" => "Integration", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "function", "name" => "get_form_integration_id", "para" => true), "list_function" => array("name" => "get_data_integration_id", "para" => true)),
            "salesforce_id"           => array("sql" => 1, "name" => "Salesforce ID", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_salesforce_id", "para" => true)),
            "google_place_id"         => array("sql" => 1, "name" => "google_place_id", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "function", "name" => "get_form_google_place_id", "para" => true)),
            "position_factor"         => array("sql" => 1, "name" => "PFaktor", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "name"                    => array("sql" => 1, "name" => "Name", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_name", "para" => true)),
            "slug"                    => array("sql" => 1, "name" => "Permalink", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "street"                  => array("sql" => 1, "name" => "Straße", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "city"                    => array("sql" => 1, "name" => "Ort", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "zip"                     => array("sql" => 1, "name" => "PLZ", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "country_code"            => array("sql" => 1, "name" => "Land", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "form_input" => array("type" => "select", "values" => $GLOBALS['CONFIG']['LOCATION_COUNTRY_CODES']), "list_function" => array("name" => "get_data_country_code", "para" => true)),
            "lat"                     => array("sql" => 1, "name" => "lat", "show" => 0, "edit" => 1, "new" => 0, "search" => 1, "sort" => 0, "nowrap" => 1, "form_input" => array("type" => "none")),
            "lng"                     => array("sql" => 1, "name" => "lng", "show" => 0, "edit" => 1, "new" => 0, "search" => 1, "sort" => 0, "nowrap" => 1, "form_input" => array("type" => "none")),
            "email"                   => array("sql" => 1, "name" => "E-Mail", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "www"                     => array("sql" => 1, "name" => "www", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "phone"                   => array("sql" => 1, "name" => "Telefon [vorwahl / nummer]", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "fax"                     => array("sql" => 1, "name" => "Fax", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "phone_visible"           => array("sql" => 1, "name" => "Telefon (sichtbar) [vorwahl / nummer]", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "fax_visible"             => array("sql" => 1, "name" => "Fax (sichtbar)", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "member_ids"              => array("sql" => 1, "name" => "Ärzte", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 0, "nowrap" => 1, "list_function" => array("name" => "get_data_member_ids", "para" => true)),
            "primary_member_id"       => array("sql" => 1, "name" => "erste Arzt", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 0, "nowrap" => 1),
            "photos"                  => array("sql" => 0, "name" => "Fotos", "show" => 1, "edit" => 0, "new" => 0, "search" => 0, "sort" => 0, "nowrap" => 1, "list_function" => array("name" => "get_data_assets", "para" => true)),
            "newsletter_subscription" => array("sql" => 1, "name" => "Newsletter", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "checkbox", "values" => array(1 => '&nbsp;'))),
            "rating_average"          => array("sql" => 1, "name" => "Rating &#216;", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "function", "name" => "get_rating_average", "para" => true)),
            "rating_1"                => array("sql" => 1, "name" => "Rating 1", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "function", "name" => "get_rating_1", "para" => true)),
            "rating_2"                => array("sql" => 1, "name" => "Rating 2", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "function", "name" => "get_rating_2", "para" => true)),
            "rating_3"                => array("sql" => 1, "name" => "Rating 3", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "function", "name" => "get_rating_3", "para" => true)),
            "medical_specialty_ids"   => array("sql" => 1, "name" => "Fachrichtungen", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_medical_specialty_ids", "para" => true), "form_input" => array("type" => "function", "name" => "get_form_medical_specialty_ids", "para" => true)),
            "info_1"                  => array("sql" => 1, "name" => "Beschreibung", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => ' style="width:300px;height:60px;"')),
            "info_location"           => array("sql" => 1, "name" => "Wegbeschreibung", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => ' style="width:300px;height:60px;"')),
            "info_seo"                => array("sql" => 1, "name" => "SEO-TEXT", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => ' style="width:300px;height:60px;"')),
            "comment_intern"          => array("sql" => 1, "name" => 'Kommentar <span style="color:#ff0000;">(intern)', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => ' style="width:300px;height:60px;"')),
            "created_at"              => array("sql" => 1, "name" => "Erstellungsdatum", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "none")),
            "created_by"              => array("sql" => 1, "name" => "Erstellt von", "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_created_by", "para" => true), "list_function" => array("name" => "get_data_created_by", "para" => true)),
            "updated_at"              => array("sql" => 1, "name" => "Letztes Update", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "updated_by"              => array("sql" => 1, "name" => "Letzter Bearbeiter", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_updated_by", "para" => true), "list_function" => array("name" => "get_data_updated_by", "para" => true)),
            "pkv_only"                => array("sql" => 1, "name" => "PKV only", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "checkbox", "values" => array(1 => '&nbsp;'))),
        );

        $BUTTON = array(
            "new"    => array("func" => "location_admin_new", "alt_text" => $translator->trans('Neu'), "script" => $this->admin->getUrl(), "gfx" => "document_new.png", "action" => "new", "th" => 1, "td" => 0),
            "edit"   => array("func" => "location_admin_edit", "alt_text" => $translator->trans('Bearbeiten'), "script" => $this->admin->getUrl(), "gfx" => "document_edit.png", "action" => "edit", "th" => 0, "td" => 1),
            "delete" => array("func" => "location_admin_delete", "alt_text" => $translator->trans('Löschen'), "script" => $this->admin->getUrl(), "gfx" => "delete.png", "action" => "delete", "th" => 0, "td" => 1),
            "search" => array("func" => "location_admin_search", "alt_text" => $translator->trans('Suchen'), "script" => $this->admin->getUrl(), "gfx" => "search.png", "action" => "search", "th" => 1, "td" => 0),
            "csv"    => array("func" => "location_admin_csv", "alt_text" => 'Liste als CSV herunterladen', "script" => $this->admin->getUrl(), "gfx" => "download.gif", "action" => "csv", "th" => 1, "td" => 0),
        );
        $TABLE_CONFIG = array(
            "table_name"  => DB_TABLENAME_LOCATIONS,
            "alpha_index" => "name",
        );
        $URL_PARA = array(
            'page'         => 1,
            'alpha'        => '',
            "sort"         => 0,
            "direction"    => 'DESC',
            "num_per_page" => 50,
            "type"         => 'num'
        );

        $configuration = new Configuration();
        $configuration->menu    = $MENU_ARRAY;
        $configuration->buttons = $BUTTON;
        $configuration->table   = $TABLE_CONFIG;
        $configuration->default_parameters = $URL_PARA;

        $this->configuration = $configuration;
    }

    /**
     * {@inheritdoc}
     */
    public function render()
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $html = '';

        $action = getParam("action", 'list');
        $form_data = getParam("form_data");

        switch ($action) {
            case "edit":
                if (!$this->checkPrivilege('location_admin_edit')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                } else {
                    $id = getParam("id");
                    if (is_array($form_data)) {
                        $html = $this->change_one($action, $id, $form_data, $this->parameters);
                    } else {
                        $html = $this->form_change($action, $id, $this->parameters);
                    }
                }
                break;
            
            case "delete":
                if (!$this->checkPrivilege('location_admin_delete')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                } else {
                    if (!isset($_REQUEST["confirm_id"])) {
                        $id = getParam("id");
                        $html = $this->ask4delete($id, $this->parameters);
                    } else {
                        $confirm_id = getParam("confirm_id");
                        if ($this->delete_one($confirm_id)) {
                            $view->addMessage('STATUS', 'NOTICE', $translator->trans('the_entry_was_deleted_successfully'));
                        } else {
                            $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_could_not_be_deleted'));
                        }
                    }
                }
                break;
            
            case "search":
                if (!$this->checkPrivilege('location_admin_search')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                
                if (isset($_REQUEST["send_button"])) {
                    $this->parameters["page"] = 1;
                    $this->parameters["alpha"] = '';
                }
                
                $html = $this->show_search_form($action, $form_data, $this->parameters);
                
                if (is_array($form_data)) {
                    $html .= $this->get_list($this->parameters, $form_data);
                }
                break;
            
            case "new":
                if (!$this->checkPrivilege('location_admin_new')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                
                if (is_array($form_data)) {
                    $html = $this->new_one($action, $form_data, $this->parameters);
                } else {
                    $html = $this->form_new($action, $this->parameters);
                }
                break;
            
            case "csv":
                if (!$this->checkPrivilege('location_admin_csv')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                $this->show_csv();
                break;
        }

        if (empty($html)) {
            if ($this->checkPrivilege('location_admin_view')) {
                $html = $this->get_list($this->parameters);
            }
        }

        return $html;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_created_by($data = array())
    {
        $data['created_by'] = !empty($data['created_by']) ? intval($data['created_by']) : 0;

        return (new User($data['created_by']))->getFullName();
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_created_at($data = array())
    {
        return mysql2date('d.m.Y H:i:s', $data['created_at'], false);
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_updated_by($data = array())
    {
        $data['updated_by'] = !empty($data['updated_by']) ? intval($data['updated_by']) : 0;

        return (new User($data['updated_by']))->getFullName();
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_status($data = array())
    {
        if (isset($GLOBALS['CONFIG']['LOCATION_STATUS'][$data['status']])) {
            return $GLOBALS['CONFIG']['LOCATION_STATUS'][$data['status']];
        } else {
            return 'N/A';
        }
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_country_code($data = array())
    {
        if (isset($GLOBALS['CONFIG']['LOCATION_COUNTRY_CODES'][$data['country_code']])) {
            return $GLOBALS['CONFIG']['LOCATION_COUNTRY_CODES'][$data['country_code']];
        } else {
            return 'N/A';
        }
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_name($data = array())
    {
        if (!empty($data['slug'])) {
            $link = $GLOBALS['CONFIG']['URL_HTTP_LIVE'] . '/praxis/' . $data['slug'];
            $name = $data['name'];

            return '<a href="' . $link . '" target="_blank">' . $name . '</a>';
        }

        return !empty($data['name']) ? $data['name'] : '';
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_assets($data = array())
    {
        $asset = new Asset();
        $total = $asset->count('parent_id=0 AND owner_id=' . $data['id'] . ' AND owner_type=' . ASSET_OWNERTYPE_LOCATION);
        
        if (!$total) {
            return '<span class="red">' . $total . '</span>';
        }

        $assets = $asset->findObjects('owner_id=' . $data['id'] . ' AND size="' . ASSET_GFX_SIZE_THUMBNAIL . '" AND owner_type=' . ASSET_OWNERTYPE_LOCATION);
        $html_assets = '';
        
        if (!empty($assets)) {
            $html_assets .= '<ul class="asset-thumb">';
            /** @var Asset[] $assets */
            foreach ($assets as $_asset) {
                $html_assets .= '<li><img src="' . $_asset->getUrl() . '" title="" /></li>';
            }
            $html_assets .= '</ul>';
        }

        return '<span class="green">' . $total . ' <img src="' . getStaticUrl('adm/icon-images.png') . '" data-toggle="tooltip" data-placement="top" title="' . htmlspecialchars($html_assets) . '" /></span>';
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_member_ids($data = array())
    {
        $html = '';
        
        if (!empty($data['member_ids'])) {
            $ids = explode(',', $data['member_ids']);
            foreach ($ids as $id) {
                $user = new User($id);
                $html .= empty($html) ? '' : '<br />';
                if ($user->isValid()) {
                    $html .= $user->getName() . ' (<a href="' . $user->getAdminEditUrl() . '" target="_blank">edit</a>';
                    if ($user->hasProfile()) {
                        $html .= ' <a href="' . $user->getUrl('url') . '" target="_blank"> / view</a>';
                    }
                    $html .= ' <a href="/' . $data['integration_id'] . '" target="_blank"> / termine</a>)';
                } else {
                    $html .= '<span class="red">Wrong ID [' . $id . ']</span>';
                }
            }
        }

        return $html;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_form_medical_specialty_ids($data = array())
    {
        $html = '';
        $ids = !empty($data['medical_specialty_ids']) ? $data['medical_specialty_ids'] : '';
        $ids = explode(',', $ids);
        $_medical_specialty = new MedicalSpecialty();
        $_medical_specialties = $_medical_specialty->getMedicalSpecialties();
        $_html_rows = '';
        
        /** @var MedicalSpecialty[] $_medical_specialties */
        foreach ($_medical_specialties as $_medical_specialty) {
            $_html_rows .= '<tr><td width="20"><input type="checkbox" id="form_data_medical_specialty_ids_' . $_medical_specialty->getId() . '" value="1" name="form_data[medical_specialty_ids][' . $_medical_specialty->getId() . ']" ' . (in_array($_medical_specialty->getId(), $ids) ? " checked" : "") . '></td><td><label for="form_data_medical_specialty_ids_' . $_medical_specialty->getId() . '">' . $_medical_specialty->getName() . '</label></td></tr>';
        }
        
        $html .= '<table class="table table-striped">';
        $html .= '<tr class="headers"><th colspan="2">Fachrichtungen</th></tr>';
        $html .= $_html_rows;
        $html .= "</table>\n";

        return $html;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_medical_specialty_ids($data = array())
    {
        $_medical_specialty = new MedicalSpecialty();

        return getCategoriesText($data['medical_specialty_ids'], $_medical_specialty->getMedicalSpecialtyValues(), '<br />');
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_form_integration_id($data = array())
    {
        $_integration_id = !empty($data['integration_id']) ? $data['integration_id'] : 0;
        $_html = '<select id="form_data_integration_id" name="form_data[integration_id]">';
        
        foreach ($GLOBALS['CONFIG']['INTEGRATIONS'] as $_id => $_integrations_data) {
            $_html .= '<option value="' . $_id . '"' . ($_id == $_integration_id ? ' selected="selected"' : '') . '>' . $_integrations_data['NAME'] . ' [' . $_id . ']</option>';
        }
        
        $_html .= '</select>';

        return $_html;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_integration_id($data = array())
    {
        if (!empty($data['integration_id'])) {
            return Integration::findName($data['integration_id']);
        }

        return '<span class="red">N/A</span>';
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_form_html_title($data = array())
    {
        $_html = '';
        $_location = new Location($data['id']);
        
        if ($_location->isValid()) {
            $_html = '<span>' . $_location->getHtmlDefaultTitle() . '</span><br />';
        }
        
        $_html .= $this->admin->admin_template('admin/_form_text_counter.tpl', 'html_title', $data['html_title'], null, null, null, null, null);

        return $_html;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_form_googlepreview($data = array())
    {
        $id = !empty($data['id']) ? $data['id'] : 0;
        
        $location = new Location($id);
        $title = '';
        $description = '';
        $url_base = '';

        if ($location->isValid()) {
            $title = $location->getHtmlDefaultTitle();
            $description = $location->getHtmlDefaultMetaDescription();
            $url_base = $location->getPermalink();
            $url_base = str_replace($location->getSlug(), '', $url_base);
            $url_base = str_replace('http://', '', $url_base);
        }

        return <<<HTML
<div id="googlepreview" class="google-result">
	<div class="title"></div>
	<div class="url"></div>
	<div class="description"></div>
</div>
<script type="text/javascript">
AT._defer(function(){

var input_html_title_default = '$title';
var input_html_meta_description_default = '$description';

var update_googlepreview = function() {
	var input_html_title = $('#form_data_html_title').val();
	var googlePreview = $('#googlepreview');
	
	if (input_html_title.length < 1) { input_html_title = input_html_title_default; }
	googlePreview.find('.title').html(input_html_title);

	var input_html_meta_description = $('#form_data_html_meta_description').val();
	if (input_html_meta_description.length < 1) { input_html_meta_description = input_html_meta_description_default; }
	googlePreview.find('.description').html(input_html_meta_description);

	var input_slug = $('#form_data_slug').val();
	if (input_slug.length < 1) { input_slug = ''; }
	googlePreview.find('.url').html('$url_base' + input_slug);
};

$(function() {
	update_googlepreview();

	$('#form_data_html_title').keyup(function() {
		update_googlepreview();
		return false;
	});
	$('#form_data_html_meta_description').keyup(function() {
		update_googlepreview();
		return false;
	});
	$('#form_data_slug').keyup(function() {
		update_googlepreview();
		return false;
	});
});
});
</script>
HTML;
    }

    /**
     * @param array $data
     *
     * @return int|string
     */
    public function get_open_bookings_count($data = array())
    {
        $bookingsManager = BookingsManager::getInstance();
        $locationId = $data["id"];
        $newBookingsForLocation = $bookingsManager->getBookingsByLocationIdAndStatus($locationId, Booking::STATUS_NEW);
        if (count($newBookingsForLocation) == 0) {
            return count($newBookingsForLocation);
        } else {
            $href = $GLOBALS['CONFIG']['URL_HTTP_LIVE'] . '/administration/bookings?action=search&open_bookings=1&form_data[location_id_SEARCH_STYLE]=0&form_data[location_id]=' . $locationId;

            return '<a href="' . $href . '" target="_blank">' . count($newBookingsForLocation) . '</a>';
        }
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_salesforce_id($data = array())
    {
        return '<a href="https://eu1.salesforce.com/'.$data['salesforce_id'].'" target="_blank">'.$data['salesforce_id'].'</a>';
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_form_google_place_id($data = array())
    {
        $place_id = '';

        if($data['google_place_id'] != '') {
            $place_id = $data['google_place_id'];
        } else {
            $location = new Location($data['id']);
            if ($location->isValid()) {
                $place_id = $location->getGooglePlaceId();
            }
        }

        $html = '<input type="text" id="form_data_google_place_id" class="form_text" name="form_data[google_place_id]" value="'.$place_id.'" size="50">';
        if($place_id!='') {
            $html .= ' <a href="http://search.google.com/local/writereview?placeid=' . $place_id . '" target="_blank">google_review<a/>';
        }

        return $html;
    }




    /**
     * @param array $data
     *
     * @return string
     */
    public function get_rating_average($data = array())
    {
        $rating = isset($data['rating_average']) ? $data['rating_average'] : 0;
        return '<input name="form_data[rating_average]" type="number" max="5.0" min="0.0" step="0.1" value="' . $rating . '"/>';
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_rating_1($data = array())
    {
        $rating = isset($data['rating_1']) ? $data['rating_1'] : 0;
        return '<input name="form_data[rating_1]" type="number" max="5.0" min="0.0" step="0.1" value="' . $rating . '"/>';
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_rating_2($data = array())
    {
        $rating = isset($data['rating_2']) ? $data['rating_2'] : 0;
        return '<input name="form_data[rating_2]" type="number" max="5.0" min="0.0" step="0.1" value="' . $rating . '"/>';
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_rating_3($data = array())
    {
        $rating = isset($data['rating_3']) ? $data['rating_3'] : 0;
        return '<input name="form_data[rating_3]" type="number" max="5.0" min="0.0" step="0.1" value="' . $rating . '"/>';
    }

    /**
     * @param array $URL_PARA
     * @param array $search
     *
     * @return string
     */
    public function get_list($URL_PARA,$search = array())
    {
        return $this->admin->admin_get_list(0, $search, $URL_PARA);
    }

    /**
     * @param string $action
     * @param array $form_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function new_one($action, $form_data, $URL_PARA)
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $profile = $this->container->get('current_user');
        $html = '';
        
        // 1. set defaults
        $form_data["created_by"] = $profile->getId();
        $form_data["created_at"] = (new DateTime())->getDateTime();

        // 2. check
        if (1 > mb_strlen(sanitize_title($form_data["name"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
            $view->addMessage('admin_message_name', 'ERROR', $translator->trans('Es muss ein Name angegeben werden'));
        }
        if (1 > mb_strlen(sanitize_slug($form_data["slug"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
            $view->addMessage('admin_message_slug', 'ERROR', $translator->trans('Der Permalink ist nicht gültig!'));
        } else {
            $_slug = sanitize_slug($form_data["slug"]);
            $_location = new Location();
            $_total = $_location->count('slug="' . sqlsave($_slug) . '"');
            if ($_total > 0) {
                $view->addMessage('admin_message_slug', 'ERROR', 'Der Permalink ist bereits vergeben!');
            }
        }

        // if there are checkboxes than set the keys to make sure that deselected checkboxes are beeing set
        $form_data = $this->admin->set_unselected_checkbox_values($form_data);

        $_medical_specialty_ids = '';
        if (isset($form_data['medical_specialty_ids']) && is_array($form_data['medical_specialty_ids'])) {
            $_medical_specialty_ids = implode(',', array_keys($form_data['medical_specialty_ids']));
        }
        $form_data['medical_specialty_ids'] = $_medical_specialty_ids;

        // 3. exit on errors
        if ($view->countMessages()) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('Bitte prüfen Sie Ihre Eingaben.'));
            return $this->form_change($action, 0, $URL_PARA, $form_data);
        }
        // 4. sanitize
        $form_data["slug"] = sanitize_slug($form_data["slug"]);
        $form_data["name"] = sanitize_text_field($form_data["name"]);
        $form_data["street"] = sanitize_text_field($form_data["street"]);
        $form_data["city"] = sanitize_text_field($form_data["city"]);
        $form_data["zip"] = sanitize_text_field($form_data["zip"]);
        $form_data["email"] = sanitize_email($form_data["email"]);
        $form_data["www"] = sanitize_url($form_data["www"]);
        $form_data["phone"] = sanitize_text_field($form_data["phone"]);
        $form_data["fax"] = sanitize_text_field($form_data["fax"]);
        $form_data["info_1"] = sanitize_textarea_field($form_data["info_1"]);
        $form_data["comment_intern"] = sanitize_textarea_field($form_data["comment_intern"]);
        $form_data['member_ids'] = preg_replace('/[^\d\,]/', '', $form_data['member_ids']);

        // get the geocode
        $_address = $form_data['street'] . ',' . $form_data['zip'] . ' ' . $form_data['city'];
        $_coords = GoogleMap::getGeoCoords($_address);
        if (!empty($_coords)) {
            $form_data['lat'] = $_coords['lat'];
            $form_data['lng'] = $_coords['lng'];
        } else {
            $form_data['lat'] = '';
            $form_data['lng'] = '';
        }

        $id = $this->admin->admin_set_new($form_data);
        if ($id > 0) {
            /* first delete all old entries */
            $wrong_ids = array();
            $integrationConnection = new IntegrationConnection();
            $integrationConnection->deleteLocationConnections($id);
            if ($form_data['member_ids'] != '') {
                $user_ids = explode(',', $form_data['member_ids']);
                if (is_array($user_ids) && !empty($user_ids)) {
                    foreach ($user_ids as $user_id) {
                        $user = new User($user_id);
                        if (!$user->isValid() || !$integrationConnection->connect($user_id, $id, false)) {
                            $wrong_ids[] = $user_id;
                        }
                    }
                }
            }
            if (!empty($wrong_ids)) {
                $view->addMessage('STATUS', 'WARNING', 'Die Ärzte mit den IDs ' . implode(',', $wrong_ids) . ' existieren nicht.');
            }
            $view->addMessage('STATUS', 'NOTICE', $translator->trans('Die Location wurde erfolgreich mit der ID ' . $id . ' angelegt.<br /><a href="/administration/locations?action=edit&id=' . $id . '">Location bearbeiten</a>'));
        } elseif ($id == -1) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_allready_exists'));
            $html = $this->form_new($action, $URL_PARA, $form_data);
        } elseif ($id == -2) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('name_of_primary_key_is_empty'));
            $html = $this->form_new($action, $URL_PARA, $form_data);
        } else {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_could_not_be_created'));
        }

        return $html;
    }

    /**
     * @param string $action
     * @param int $id
     * @param array $form_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function change_one($action, $id, $form_data, $URL_PARA)
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $profile = $this->container->get('current_user');
        $html = '';
        
        // 1. set defaults
        $form_data["updated_by"] = $profile->getId();
        $form_data["updated_at"] = (new DateTime)->getDateTime();

        // 2. check
        if (1 > mb_strlen(sanitize_title($form_data["name"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
            $view->addMessage('admin_message_name', 'ERROR', $translator->trans('Der Name ist nicht gültig'));
        }
        if (1 > mb_strlen(sanitize_slug($form_data["slug"]), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])) {
            $view->addMessage('admin_message_slug', 'ERROR', $translator->trans('Der Permalink ist nicht gültig!'));
        } else {
            $_slug = sanitize_slug($form_data["slug"]);
            $_location = new Location();
            $_total = $_location->count('slug="' . sqlsave($_slug) . '" AND id!=' . $id);
            if ($_total > 0) {
                $view->addMessage('admin_message_slug', 'ERROR', 'Der Permalink ist bereits vergeben!');
            }
        }

        // if there are checkboxes than set the keys to make sure that deselected checkboxes are beeing set
        $form_data = $this->admin->set_unselected_checkbox_values($form_data);

        $_medical_specialty_ids = '';
        if (isset($form_data['medical_specialty_ids']) && is_array($form_data['medical_specialty_ids'])) {
            $_medical_specialty_ids = implode(',', array_keys($form_data['medical_specialty_ids']));
        }
        $form_data['medical_specialty_ids'] = $_medical_specialty_ids;

        // 3. exit on errors
        if ($view->countMessages()) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('Bitte prüfen Sie Ihre Eingaben.'));
            return $this->form_change($action, $id, $URL_PARA, $form_data);
        }
        // 4. sanitize
        //$form_data["slug"] = sanitize_slug($form_data["slug"]);
        $form_data["name"] = sanitize_text_field($form_data["name"]);
        $form_data["street"] = sanitize_text_field($form_data["street"]);
        $form_data["city"] = sanitize_text_field($form_data["city"]);
        $form_data["zip"] = sanitize_text_field($form_data["zip"]);
        $form_data["email"] = sanitize_email($form_data["email"]);
        $form_data["www"] = sanitize_url($form_data["www"]);
        $form_data["phone"] = sanitize_text_field($form_data["phone"]);
        $form_data["fax"] = sanitize_text_field($form_data["fax"]);
        $form_data["info_1"] = sanitize_textarea_field($form_data["info_1"]);
        $form_data["comment_intern"] = sanitize_textarea_field($form_data["comment_intern"]);
        $form_data['member_ids'] = preg_replace('/[^\d\,]/', '', $form_data['member_ids']);

        $_coords = array();
        $location = new Location($id);
        if ($location->isValid() && $location->getLat()!='' && $location->getLng()!='' && $form_data['city']==$location->getCity()) {
            $_coords['lat'] = $location->getLat();
            $_coords['lng'] = $location->getLng();
        } else {
            // get the geocode
            $_address = $form_data['street'] . ',' . $form_data['zip'] . ' ' . $form_data['city'];
            $_coords = GoogleMap::getGeoCoords($_address);
        }


        if (!empty($_coords)) {
            $form_data['lat'] = $_coords['lat'];
            $form_data['lng'] = $_coords['lng'];
        } else {
            $form_data['lat'] = '';
            $form_data['lng'] = '';
        }

        // if there are checkboxes than set the keys to make sure that deselected checkboxes are beeing set
        $form_data = $this->admin->set_unselected_checkbox_values($form_data);

        $_result = $this->admin->admin_set_update($id, $form_data);
        if ($_result == true) {
            // update the profile_asset_filename
            $location = new Location($id);
            if ($location->isValid()) {
                $location->updateProfileAssetFilename();
            }

            /* first delete all old entries */
            $wrong_ids = array();
            $integrationConnection = new IntegrationConnection();
            $integrationConnection->deleteLocationConnections($id);
            if ($form_data['member_ids'] != '') {
                $user_ids = explode(',', $form_data['member_ids']);
                if (is_array($user_ids) && !empty($user_ids)) {
                    foreach ($user_ids as $user_id) {
                        $user = new User($user_id);
                        if (!$user->isValid() || !$integrationConnection->connect($user_id, $id, false)) {
                            $wrong_ids[] = $user_id;
                        }
                    }
                }
            }
            
            if (!empty($wrong_ids)) {
                $view->addMessage('STATUS', 'WARNING', 'Die Ärzte mit den IDs ' . implode(',', $wrong_ids) . ' existieren nicht.');
            }
            
            $view->addMessage('STATUS', 'NOTICE', $translator->trans('action_was_successfully'));
        } else {
            $view->addMessage('STATUS', 'WARNING', $translator->trans('no_data_has_been_updated'));

            $html = $this->form_change($action, $id, $URL_PARA, $form_data);
        }

        return $html;
    }

    /**
     * @param int $id
     * @param array $URL_PARA
     *
     * @return string
     */
    public function ask4delete($id, $URL_PARA)
    {
        return $this->admin->admin_get_confirm_table($id, $URL_PARA, 'delete');
    }

    /**
     * @param int $id
     *
     * @return int
     */
    public function delete_one($id)
    {
        return $this->admin->admin_set_delete($id);
    }

    /**
     * @param string $action
     * @param array $URL_PARA
     * @param array $error_data
     *
     * @return string
     */
    public function form_new($action, $URL_PARA, $error_data = array())
    {
        $this->getView()->setFocus('#form_data_name');

        return $this->admin->admin_get_form($action, 0, $error_data, $URL_PARA);
    }

    /**
     * @param string $action
     * @param int $id
     * @param array $URL_PARA
     * @param array $error_data
     *
     * @return string
     */
    public function form_change($action, $id, $URL_PARA, $error_data = array())
    {
        $this->getView()->setFocus('#form_data_name');
        $html = $this->admin->admin_get_form($action, $id, $error_data, $URL_PARA);

        $location = new Location($id);
        if ($location->isValid()) {
            $html .= get_gallery($location, 'location', ASSET_OWNERTYPE_LOCATION);
        }

        return $html;
    }

    /**
     * @param string $action
     * @param array $search_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function show_search_form($action, $search_data, $URL_PARA)
    {
        return $this->admin->admin_get_form($action, 0, $search_data, $URL_PARA);
    }

    /**
     * Note: TERMINATES THE APP
     */
    public function show_csv()
    {
        header("Content-Type: text/csv; charset=utf-8");
        header("Content-Disposition: attachment; filename=locations-users-" . date('Ymd_His') . '.csv');
        header("Pragma: no-cache");
        header("Expires: 0");
        
        // its utf8 content. sizeof doesnt work here! So let the browser do the calculation :)
        // header("Content-Length: ".sizeof($_html));
        Location::echoExportProviders();
        
        exit;
    }
}