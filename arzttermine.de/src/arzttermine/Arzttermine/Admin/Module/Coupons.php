<?php

namespace Arzttermine\Admin\Module;

use Arzttermine\Admin\Configuration;
use Arzttermine\Admin\Module;
use Arzttermine\Core\DateTime;
use Arzttermine\Coupon\Coupon;
use Arzttermine\MedicalSpecialty\MedicalSpecialty;

final class Coupons extends Module {

    /**
     * @var string
     */
    protected $title = 'Gutscheine Admin';

    /**
     * @var string
     */
    protected $role = 'coupon_admin';

    /**
     * @var string
     */
    protected $slug = 'coupons';

    /**
     * {@inheritdoc}
     */
    public function boot()
    {
        $translator = $this->container->get('translator');

        $MENU_ARRAY = array(
            "id"           => array("sql" => 1, "name" => "ID", "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "code"         => array("sql" => 1, "name" => "Code", "show" => 1, "edit" => 1, "new" => 1, "search" => 0, "sort" => 1, "nowrap" => 1),
            "active"       => array("sql" => 1, "name" => "Aktiv", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "select", "values" => $GLOBALS['CONFIG']['ARRAY_NO_YES']), "list_function" => array("name" => "get_data_active", "para" => true)),
            "offer_type"   => array("sql" => 1, "name" => "Offer Type", "show" => 1, "edit" => 1, "new" => 1, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "function", "name" => "get_form_offer_type", "para" => true), "list_function" => array("name" => "get_data_offer_type", "para" => true)),
            "note"         => array("sql" => 1, "name" => 'Kommentar <span style="color:#ff0000;">(intern)</span>', "show" => 1, "edit" => 1, "new" => 1, "search" => 0, "sort" => 0, "nowrap" => 1),
            "restrictions" => array("sql" => 1, "name" => "Restrictions", "show" => 0, "edit" => 1, "new" => 1, "search" => 0, "sort" => 0, "nowrap" => 0, "form_input" => array("type" => "function", "name" => "get_form_restrictions", "para" => true)),
            "type"         => array("sql" => 1, "name" => "Type", "show" => 1, "edit" => 1, "new" => 1, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "function", "name" => "get_form_type", "para" => true), "list_function" => array("name" => "get_data_type", "para" => true)),
            "created_by"   => array("sql" => 1, "name" => "Erstellt von", "show" => 1, "edit" => 0, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "created_at"   => array("sql" => 1, "name" => "Erstellt am", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"))
        );

        $BUTTON = array(
            "new"    => array("func" => "coupon_admin_new", "alt_text" => $translator->trans('new_item'), "script" => $this->admin->getUrl(), "gfx" => "document_new.png", "action" => "new", "th" => 1, "td" => 0),
            "edit"   => array("func" => "coupon_admin_edit", "alt_text" => $translator->trans('edit_item'), "script" => $this->admin->getUrl(), "gfx" => "document_edit.png", "action" => "edit", "th" => 0, "td" => 1),
            "delete" => array("func" => "coupon_admin_delete", "alt_text" => $translator->trans('delete_item'), "script" => $this->admin->getUrl(), "gfx" => "delete.png", "action" => "delete", "th" => 0, "td" => 1)
        );

        $TABLE_CONFIG = array(
            "table_name"  => DB_TABLENAME_COUPONS,
            "alpha_index" => "code",
        );

        $URL_PARA = array(
            'page'         => 1,
            'alpha'        => '',
            "sort"         => 0,
            "direction"    => 'DESC',
            "num_per_page" => 50,
            "type"         => 'num'
        );

        $configuration = new Configuration();
        $configuration->menu    = $MENU_ARRAY;
        $configuration->buttons = $BUTTON;
        $configuration->table   = $TABLE_CONFIG;
        $configuration->default_parameters = $URL_PARA;

        $this->configuration = $configuration;
    }

    /**
     * {@inheritdoc}
     */
    public function render()
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $html = '';

        $action = getParam("action", 'list');
        $form_data = getParam("form_data");

        switch ($action) {
            case "edit":
                if (!$this->checkPrivilege('coupon_admin_edit')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                } else {
                    $id = getParam("id");
                    if (is_array($form_data)) {
                        $html = $this->change_one($action, $id, $form_data, $this->parameters);
                    } else {
                        $html = $this->form_change($action, $id, $this->parameters);
                    }
                }
                break;
            
            case "delete":
                if (!$this->checkPrivilege('coupon_admin_delete')) {
                    error_log(print_r(__LINE__, true));
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                } else {
                    if (!isset($_REQUEST["confirm_id"])) {
                        $id = getParam("id");
                        $html = $this->ask4delete($id, $this->parameters);
                    } else {
                        $confirm_id = getParam("confirm_id");
                        if ($this->delete_one($confirm_id)) {
                            $view->addMessage('STATUS', 'NOTICE', $translator->trans('the_entry_was_deleted_successfully'));
                        } else {
                            $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_could_not_be_deleted'));
                        }
                    }
                }
                break;
            
            case "new":
                if (!$this->checkPrivilege('coupon_admin_new')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                if (is_array($form_data)) {
                    $html = $this->new_one($action, $form_data, $this->parameters);
                } else {
                    $html = $this->form_new($action, $this->parameters);
                }
                break;
        }

        if (empty($html)) {
            if ($this->checkPrivilege('coupon_admin_view')) {
                $html = $this->get_list($this->parameters);
            }
        }

        return $html;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_offer_type($data = array())
    {
        $types = $GLOBALS['CONFIG']['REFERRAL_PRICE_TYPES'];

        if (isset($types[$data['offer_type']])) {
            return $types[$data['offer_type']]['company'] . " (" . $types[$data['offer_type']]['price'] . ") ";
        } else {
            return 'N/A';
        }
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_type($data = array())
    {
        if (isset($data['type'])) {
            if (intval($data['type']) === Coupon::TYPE_SYSTEM) {
                return 'Gutschein';
            } elseif (intval($data['type']) === Coupon::TYPE_USER) {
                return 'Referral';
            }
        }

        return 'N/A';
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_form_offer_type($data = array())
    {
        $types = $GLOBALS['CONFIG']['REFERRAL_PRICE_TYPES'];
        $html = '<select name="form_data[offer_type]">';

        $selected = $data['offer_type'];

        foreach ($types as $code => $type) {
            $html .= '<option value="' . $code . '"' . ($selected == $code ? ' selected' : '') . '>' . $type['company'] . ($type['price'] ? " ({$type['price']}) " : '') . '</option>';
        }

        $html .= '</select>';

        return $html;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_form_type($data = array())
    {
        $html = '';

        if (is_array($data) && !isset($data['type'])) {
            $data['type'] = Coupon::TYPE_SYSTEM;
        }

        $html .= '<label><input type="radio" name="form_data[type]" value="' . Coupon::TYPE_SYSTEM . '" ' .
                 (intval($data['type']) == Coupon::TYPE_SYSTEM ? ' checked' : '') . '>&nbsp;System (default)</label>';

        $html .= '<label><input type="radio" name="form_data[type]" value="' . Coupon::TYPE_USER . '" ' .
                 (intval($data['type']) == Coupon::TYPE_USER ? ' checked' : '') . '>&nbsp;Referral</label>';

        return $html;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_form_restrictions($data = array())
    {
        $restrictions = array();
        $html = '';

        if (!empty($data['restrictions']) && !is_array($data['restrictions'])) {
            $restrictions = json_decode($data['restrictions'], true);
            // The decode failed, so revert the restrictions
            if (!is_array($restrictions)) {
                $restrictions = array();
            }
        }

        // accessible_has_contract
        $accessible_has_contract = !isset($restrictions['accessible_has_contract']) ? 1 : intval($restrictions['accessible_has_contract']);
        $html .= '<div>Accessible for has contract&nbsp;';
        $html .= '<select name="form_data[restrictions][accessible_has_contract]">';
        $html .= '<option value="0"' . ($accessible_has_contract === 0 ? ' selected' : '') . '>No</option>';
        $html .= '<option value="1"' . ($accessible_has_contract === 1 ? ' selected' : '') . '>Yes</option>';
        $html .= '</select>';
        $html .= '</div>';

        // accessible_has_no_contract
        $accessible_has_no_contract = !isset($restrictions['accessible_has_no_contract']) ? 1 : intval($restrictions['accessible_has_no_contract']);
        $html .= '<div>Accessible for has no contract&nbsp;';
        $html .= '<select name="form_data[restrictions][accessible_has_no_contract]">';
        $html .= '<option value="0"' . ($accessible_has_no_contract === 0 ? ' selected' : '') . '>No</option>';
        $html .= '<option value="1"' . ($accessible_has_no_contract === 1 ? ' selected' : '') . '>Yes</option>';
        $html .= '</select>';
        $html .= '</div>';

        // accessible_mobile
        $accessible_mobile = !isset($restrictions['accessible_mobile']) ? 1 : intval($restrictions['accessible_mobile']);

        $html .= '<div>Accessible on mobile site&nbsp;';
        $html .= '<select name="form_data[restrictions][accessible_mobile]">';
        $html .= '<option value="0"' . ($accessible_mobile === 0 ? ' selected' : '') . '>No</option>';
        $html .= '<option value="1"' . ($accessible_mobile === 1 ? ' selected' : '') . '>Yes</option>';
        $html .= '</select>';
        $html .= '</div>';

        // non_accessible_user_ids
        $non_accessible_user_ids = !isset($restrictions['non_accessible_user_ids']) ? '' : $restrictions['non_accessible_user_ids'];
        $html .= '<div>Non accessible with doctors&nbsp;';
        $html .= '<input type="text" name="form_data[restrictions][non_accessible_user_ids]" value="' . $non_accessible_user_ids . '"/>';
        $html .= '</div>';

        // search_locations
        $search_locations = !isset($restrictions['search_locations']) ? '' : $restrictions['search_locations'];
        $html .= '<div>Non accessible with locations&nbsp;';
        $html .= '<input type="text" name="form_data[restrictions][search_locations]" value="' . $search_locations . '"/>';
        $html .= '</div>';

        // accessible_from
        $accessible_from = !isset($restrictions['accessible_from']) ? '' : $restrictions['accessible_from'];
        $html .= '<div>Accessible from&nbsp;';
        $html .= '<input type="date" name="form_data[restrictions][accessible_from]" value="' . $accessible_from . '" class="datetimepicker" />';
        $html .= '</div>';

        // accessible_until
        $accessible_until = !isset($restrictions['accessible_until']) ? '' : $restrictions['accessible_until'];
        $html .= '<div>Accessible until&nbsp;';
        $html .= '<input type="date" name="form_data[restrictions][accessible_until]" value="' . $accessible_until . '" class="datetimepicker" />';
        $html .= '</div>';

        // accessible_medical_specialty_ids
        $medical_specialties = MedicalSpecialty::getHtmlOptions(true);
        $accessible_medical_specialty_ids = empty($restrictions['accessible_medical_specialty_ids']) ? array() : explode(',', $restrictions['accessible_medical_specialty_ids']);

        $html .= '<div>Accessible with medical specialties&nbsp;<br/>';
        $html .= '<input type="checkbox" id="check_uncheck_all"/> check/uncheck all<br/>';
        $html .= '<div class="specialties">';
        foreach ($medical_specialties as $msid => $specialty) {
            $html .= '<label><input type="checkbox" name="form_data[specialties][]" value="' . intval($msid) . '" ' .
                     (in_array($msid, $accessible_medical_specialty_ids) ? ' checked' : '') . '/>&nbsp;' . $specialty . '</label>';
        }
        $html .= '</div>';
        $html .= '</div>';

        // Hacky styling
        $html .= '<style type="text/css">.widefat div{margin:12px auto}.specialties label{float:none;text-align:left;margin:6px auto}</style>';

        // Check/uncheck button
        $html .= '<script type="text/javascript">$(function(){$("#check_uncheck_all").click(function(){$(".specialties input").prop("checked", this.checked)})});</script>';

        return $html;
    }

    /**
     * @param array $data
     *
     * @return mixed
     */
    public function get_data_active($data = array())
    {
        return $GLOBALS["CONFIG"]["ARRAY_NO_YES"][$data['active']];
    }

    /**
     * @param array $form_data
     *
     * @return mixed
     */
    public function prepare($form_data)
    {
        $form_data["created_by"] = $this->container->get('current_user')->getEmail();
        $form_data['restrictions']['accessible_medical_specialty_ids'] = implode(',', $form_data['specialties']);

        return $form_data;
    }

    /**
     * @param array $URL_PARA
     * @param array $search
     *
     * @return string
     */
    public function get_list($URL_PARA, $search = array())
    {
        return $this->admin->admin_get_list(0, $search, $URL_PARA);
    }

    /**
     * @param string $action
     * @param array $form_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function new_one($action, $form_data, $URL_PARA)
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');

        $form_data = $this->prepare($form_data);

        if ($view->countMessages()) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('Bitte prüfen Sie Ihre Eingaben.'));
            return $this->form_change($action, 0, $URL_PARA, $form_data);
        }

        $coupon = new Coupon();
        $coupon
            ->setCode($form_data['code'])
            ->setActive((bool)$form_data['active'])
            ->setOfferType($form_data['offer_type'])
            ->setNote($form_data['note'])
            ->setRestrictions($form_data['restrictions'])
            ->setType($form_data['type'])
            ->setCreatedBy($form_data['created_by'])
            ->setCreatedAt((new DateTime())->getDateTime())
            ->saveNewObject();

        if ($coupon->isValid()) {
            $view->addMessage('STATUS', 'NOTICE', $translator->trans('action_was_successfully'));
        } else {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_could_not_be_created'));
        }

        return '';
    }

    /**
     * @param string $action
     * @param int $id
     * @param array $form_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function change_one($action, $id, $form_data, $URL_PARA)
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $html = '';

        // 1. set defaults
        $form_data = $this->prepare($form_data);

        $coupon = new Coupon($id);
        if (!$coupon->isValid()) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('Bitte prüfen Sie Ihre Eingaben.'));
        }

        // 3. exit on errors
        if ($view->countMessages()) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('Bitte prüfen Sie Ihre Eingaben.'));
            return $this->form_change($action, $id, $URL_PARA, $form_data);
        }

        $_result = $coupon
            ->setCode($form_data['code'])
            ->setActive((bool)$form_data['active'])
            ->setOfferType($form_data['offer_type'])
            ->setNote($form_data['note'])
            ->setRestrictions($form_data['restrictions'])
            ->setType($form_data['type'])
            ->setUpdatedBy($form_data['created_by'])
            ->setUpdatedAt((new DateTime())->getDateTime())
            ->save();

        if ($_result == true) {
            $view->addMessage('STATUS', 'NOTICE', $translator->trans('action_was_successfully'));
        } else {
            $view->addMessage('STATUS', 'WARNING', $translator->trans('no_data_has_been_updated'));

            $html = $this->form_change($action, $id, $URL_PARA, $form_data);
        }

        return $html;
    }

    /**
     * @param int $id
     * @param array $URL_PARA
     *
     * @return string
     */
    public function ask4delete($id, $URL_PARA)
    {
        return $this->admin->admin_get_confirm_table($id, $URL_PARA, 'delete');
    }

    /**
     * @param int $id
     *
     * @return int
     */
    public function delete_one($id)
    {
        return $this->admin->admin_set_delete($id);
    }

    /**
     * @param string $action
     * @param array $URL_PARA
     * @param int $error_data
     *
     * @return string
     */
    public function form_new($action, $URL_PARA, $error_data = 0)
    {
        return $this->admin->admin_get_form($action, 0, $error_data, $URL_PARA);
    }
    
    /**
     * @param string $action
     * @param int $id
     * @param array $URL_PARA
     * @param array $error_data
     *
     * @return string
     */
    public function form_change($action, $id, $URL_PARA, $error_data = array())
    {
        return $this->admin->admin_get_form($action, $id, $error_data, $URL_PARA);
    }

    /**
     * @param string $action
     * @param array $search_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function show_search_form($action, $search_data, $URL_PARA)
    {
        return $this->admin->admin_get_form($action, 0, $search_data, $URL_PARA);
    }
}