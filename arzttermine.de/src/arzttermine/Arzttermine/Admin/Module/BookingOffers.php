<?php

namespace Arzttermine\Admin\Module;

use Arzttermine\Admin\Configuration;
use Arzttermine\Admin\Module;
use Arzttermine\Booking\Booking;
use Arzttermine\Booking\Offer;
use Arzttermine\Calendar\Calendar;
use Arzttermine\Core\DateTime;
use Arzttermine\Location\Location;
use Arzttermine\User\User;

final class BookingOffers extends Module {

    /**
     * @var string
     */
    protected $title = 'BookingOffer Admin';

    /**
     * @var string
     */
    protected $role = 'bookingoffer_admin';

    /**
     * @var string
     */
    protected $slug = 'bookingoffers';

    /**
     * {@inheritdoc}
     */
    public function boot()
    {
        $translator = $this->container->get('translator');

        $MENU_ARRAY = array(
            "id"                   => array("sql" => 1, "name" => "ID", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "slug"                 => array("sql" => 1, "name" => "Slug", "show" => 1, "edit" => 0, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1),
            "status"               => array("sql" => 1, "name" => "Status", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "select", "values" => Offer::getStatusTextArray()), "list_function" => array("name" => "get_data_status", "para" => true)),
            "action"               => array("sql" => 0, "name" => "Action", "show" => 1, "edit" => 0, "new" => 0, "search" => 0, "sort" => 0, "nowrap" => 1, "list_function" => array("name" => "get_data_action", "para" => true)),
            "booking"              => array("sql" => 1, "name" => "Booking", "show" => 1, "edit" => 0, "new" => 0, "search" => 0, "sort" => 0, "nowrap" => 1, "list_function" => array("name" => "get_data_booking", "para" => true)),
            "booking_id"           => array("sql" => 1, "name" => "BookingID", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "appointment_start_at" => array("sql" => 1, "name" => "Terminvorschlag", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "datetime"), "list_function" => array("name" => "get_data_appointment_start_at", "para" => true)),
            "expires_at"           => array("sql" => 1, "name" => "Ablaufdatum", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "datetime"), "list_function" => array("name" => "get_data_expires_at", "para" => true)),
            "booked_in_practice"   => array("sql" => 1, "name" => "BookedInPractice", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "select", "values" => $GLOBALS['CONFIG']['ARRAY_NO_YES']), "list_function" => array("name" => "get_data_booked_in_practice", "para" => true)),
            "user_id"              => array("sql" => 1, "name" => "Arzt", "show" => 1, "edit" => 1, "new" => 1, "search" => 0, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_user_id", "para" => true)),
            "location_id"          => array("sql" => 1, "name" => "Praxis", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_location_id", "para" => true)),
            "mail_send_at"         => array("sql" => 1, "name" => "Mail send", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "comment_intern"       => array("sql" => 1, "name" => 'Kommentar <span style="color:#ff0000;">(intern)', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => ' style="width:300px;height:60px;"')),
            "comment_patient"      => array("sql" => 1, "name" => 'Kommentar Patient', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "textarea", "values" => "", "optional" => 'readonly style="width:300px;height:60px;"')),
            "answered_at"          => array("sql" => 1, "name" => "Geantwortet", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "list_function" => array("name" => "get_data_answered_at", "para" => true)),
            "created_at"           => array("sql" => 1, "name" => "Erstellungsdatum", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "created_by"           => array("sql" => 1, "name" => "Erstellt von", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_created_by", "para" => true), "list_function" => array("name" => "get_data_created_by", "para" => true)),
            "updated_at"           => array("sql" => 1, "name" => "Letztes Update", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "updated_by"           => array("sql" => 1, "name" => "Letzter Bearbeiter", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_updated_by", "para" => true), "list_function" => array("name" => "get_data_updated_by", "para" => true)),
        );

        $BUTTON = array(
            "new"    => array("func" => "bookingoffer_admin_new", "alt_text" => $translator->trans('new_item'), "script" => $this->admin->getUrl(), "gfx" => "document_new.png", "action" => "new", "th" => 1, "td" => 0),
            "edit"   => array("func" => "bookingoffer_admin_edit", "alt_text" => $translator->trans('edit_item'), "script" => $this->admin->getUrl(), "gfx" => "document_edit.png", "action" => "edit", "th" => 0, "td" => 1),
            "delete" => array("func" => "bookingoffer_admin_delete", "alt_text" => $translator->trans('delete_item'), "script" => $this->admin->getUrl(), "gfx" => "delete.png", "action" => "delete", "th" => 0, "td" => 1),
            "cancel" => array("func" => "bookingoffer_admin_cancel", "alt_text" => $translator->trans('cancel_item'), "script" => $this->admin->getUrl(), "gfx" => "cancel.png", "action" => "cancel", "th" => 0, "td" => 1),
            "search" => array("func" => "bookingoffer_admin_search", "alt_text" => $translator->trans('search'), "script" => $this->admin->getUrl(), "gfx" => "search.png", "action" => "search", "th" => 1, "td" => 0),
        );
        
        $TABLE_CONFIG = array(
            "table_name"  => DB_TABLENAME_BOOKING_OFFERS,
            "alpha_index" => "last_name",
        );
        
        $URL_PARA = array(
            'page'         => 1,
            'alpha'        => '',
            "sort"         => 0,
            "direction"    => 'DESC',
            "num_per_page" => 50,
            "type"         => 'num'
        );
        
        $configuration = new Configuration();
        $configuration->menu    = $MENU_ARRAY;
        $configuration->buttons = $BUTTON;
        $configuration->table   = $TABLE_CONFIG;
        $configuration->default_parameters = $URL_PARA;

        $this->configuration = $configuration;
    }

    /**
     * {@inheritdoc}
     */
    public function render()
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $html = '';

        $action = getParam('action', 'list');
        $form_data = getParam('form_data');
        $id = getParam('id');

        switch ($action) {
            case "edit":
                if (!$this->checkPrivilege('bookingoffer_admin_edit')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                
                if (is_array($form_data)) {
                    $html = $this->change_one($action, $id, $form_data, $this->parameters);
                } else {
                    $html = $this->form_change($action, $id, $this->parameters);
                }
                
                break;
            
            case "delete":
                if (!$this->checkPrivilege('bookingoffer_admin_delete')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                
                if (!isset($_REQUEST["confirm_id"])) {
                    $html = $this->ask4delete($id, $this->parameters);
                } else {
                    $confirm_id = getParam('confirm_id');
                    if ($this->delete_one($confirm_id)) {
                        $view->addMessage('STATUS', 'NOTICE', $translator->trans('the_entry_was_deleted_successfully'));
                    } else {
                        $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_could_not_be_deleted'));
                    }
                }
                
                break;
            
            case "search":
                if (!$this->checkPrivilege('bookingoffer_admin_search')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                
                if (isset($_REQUEST["send_button"])) {
                    $URL_PARA["page"] = 1;
                    $URL_PARA["alpha"] = '';
                }
                
                $html = $this->show_search_form($action, $form_data, $this->parameters);
                
                if (is_array($form_data)) {
                    $html .= $this->get_list($this->parameters, $form_data);
                }
                
                break;
            
            case "new":
                if (!$this->checkPrivilege('bookingoffer_admin_new')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                
                if (is_array($form_data)) {
                    $html = $this->new_one($action, $form_data, $this->parameters);
                } else {
                    $html = $this->form_new($action, $this->parameters);
                }
                
                break;
            
            case "send_booking_offers_email":
                if (!$this->checkPrivilege('bookingoffer_admin_send_email')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                
                $bookingOffer = new Offer(getParam('id'));
                $booking = new Booking($bookingOffer->getBookingId());
                
                if (!$booking->isValid()) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('Invalid BookingId'));
                    break;
                }
                
                if (Offer::sendBookingOffersEmail($booking)) {
                    $view->addMessage('STATUS', 'NOTICE', $translator->trans('Booking offers email has been send.'));
                } else {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('Sending the booking offers failed!'));
                }
                
                break;
        }
        // Default the screen to the List view
        if (empty($html)) {
            if ($this->checkPrivilege('bookingoffer_admin_view')) {
                $html = $this->get_list($this->parameters);
            }
        }
        
        return $html;
    }

    /**
     * @param array $params
     * @param array $search
     *
     * @return string
     */
    public function get_list($params, $search = array())
    {
        return $this->admin->admin_get_list(0, $search, $params);
    }

    /**
     * @param string $action
     * @param array $form_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function new_one($action, $form_data, $URL_PARA)
    {
        $profile = $this->container->get('current_user');
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $html = '';

        // 1. set defaults
        $form_data["created_by"] = $profile->getId();
        $form_data["created_at"] = date('Y-m-d H:i:s');

        // 2. check
        if (!is_numeric(sanitize_option('int', $form_data["user_id"])) || sanitize_option('int', $form_data["user_id"]) == 0) {
            $view->addMessage('admin_message_user_id', 'ERROR', $translator->trans('Es muss eine UserID angegeben werden'));
        }
        if (!is_numeric(sanitize_option('int', $form_data["location_id"])) || sanitize_option('int', $form_data["location_id"]) == 0) {
            $view->addMessage('admin_message_location_id', 'ERROR', $translator->trans('Es muss eine LocationID angegeben werden'));
        }
        if (!is_numeric(sanitize_option('int', $form_data["booking_id"])) || sanitize_option('int', $form_data["booking_id"]) == 0) {
            $view->addMessage('admin_message_booking_id', 'ERROR', $translator->trans('Es muss eine BookingID angegeben werden'));
        }
        if (!Calendar::isValidDateTime($form_data["appointment_start_at"])) {
            $view->addMessage('admin_message_appointment_start_at', 'ERROR', $translator->trans('Es muss ein gültiger Ausweichtermin angegeben werden'));
        }

        // if there are checkboxes than set the keys to make sure that deselected checkboxes are beeing set
        $form_data = $this->admin->set_unselected_checkbox_values($form_data);

        // 3. exit on errors
        if ($view->countMessages()) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('Bitte prüfen Sie Ihre Eingaben.'));
            $html = $this->form_change($action, 0, $URL_PARA, $form_data);

            return $html;
        }
        // 4. sanitize

        $bookingOffer = Offer::create($form_data);
        if ($bookingOffer->isValid()) {
            $view->addMessage('STATUS', 'NOTICE', $translator->trans('action_was_successfully'));
        } else {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_could_not_be_created'));
        }

        return $html;
    }

    /**
     * @param string $action
     * @param int $id
     * @param array $form_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function change_one($action, $id, $form_data, $URL_PARA)
    {
        $profile = $this->container->get('current_user');
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $html = '';

        // 1. set defaults
        $form_data["updated_by"] = $profile->getId();
        $form_data["updated_at"] = date('Y-m-d H:i:s');

        // 2. check
        if (!is_numeric(sanitize_option('int', $form_data["user_id"])) || sanitize_option('int', $form_data["user_id"]) == 0) {
            $view->addMessage('admin_message_user_id', 'ERROR', $translator->trans('Es muss eine UserID angegeben werden'));
        }
        if (!is_numeric(sanitize_option('int', $form_data["location_id"])) || sanitize_option('int', $form_data["location_id"]) == 0) {
            $view->addMessage('admin_message_location_id', 'ERROR', $translator->trans('Es muss eine LocationID angegeben werden'));
        }
        if (!is_numeric(sanitize_option('int', $form_data["booking_id"])) || sanitize_option('int', $form_data["booking_id"]) == 0) {
            $view->addMessage('admin_message_booking_id', 'ERROR', $translator->trans('Es muss eine BookingID angegeben werden'));
        }

        // if there are checkboxes than set the keys to make sure that deselected checkboxes are beeing set
        $form_data = $this->admin->set_unselected_checkbox_values($form_data);

        // 3. exit on errors
        if ($view->countMessages()) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('Bitte prüfen Sie Ihre Eingaben.'));
            $html = $this->form_change($action, $id, $URL_PARA, $form_data);

            return $html;
        }
        // 4. sanitize

        // if there are checkboxes than set the keys to make sure that deselected checkboxes are beeing set
        $form_data = $this->admin->set_unselected_checkbox_values($form_data);

        $_result = $this->admin->admin_set_update($id, $form_data);

        if ($_result == true) {
            $view->addMessage('STATUS', 'NOTICE', $translator->trans('action_was_successfully'));
        } else {
            $view->addMessage('STATUS', 'WARNING', $translator->trans('no_data_has_been_updated'));

            $html = $this->form_change($action, $id, $URL_PARA, $form_data);
        }

        return $html;
    }

    /**
     * @param int $id
     * @param array $URL_PARA
     *
     * @return string
     */
    public function ask4delete($id, $URL_PARA)
    {
        return $this->admin->admin_get_confirm_table($id, $URL_PARA, 'delete');
    }

    /**
     * @param int $id
     *
     * @return int
     */
    public function delete_one($id)
    {
        return $this->admin->admin_set_delete($id);
    }

    /**
     * @param string $action
     * @param array $URL_PARA
     * @param int $error_data
     *
     * @return string
     */
    public function form_new($action, $URL_PARA, $error_data = 0)
    {
        $this->getView()->setFocus('#form_data_first_name');

        return $this->admin->admin_get_form($action, 0, $error_data, $URL_PARA);
    }

    /**
     * @param string $action
     * @param int $id
     * @param array $URL_PARA
     * @param int $error_data
     *
     * @return string
     */
    public function form_change($action, $id, $URL_PARA, $error_data = 0)
    {
        $this->getView()->setFocus('#form_data_first_name');

        return $this->admin->admin_get_form($action, $id, $error_data, $URL_PARA);
    }

    /**
     * @param string $action
     * @param array $search_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function show_search_form($action, $search_data, $URL_PARA)
    {
        return $this->admin->admin_get_form($action, 0, $search_data, $URL_PARA);
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_created_by($data = array())
    {
        $userId = !empty($data['created_by']) ? intval($data['created_by']) : 0;
        $user = new User($userId);

        return $user->getFullName();
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_created_at($data = array())
    {
        $created_at = !empty($data['created_at']) ? $data['created_at'] : '';
        
        return mysql2date('d.m.Y H:i:s', $created_at, false);
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_updated_by($data = array())
    {
        $userId = !empty($data['updated_by']) ? intval($data['updated_by']) : 0;
        $user = new User($userId);

        return $user->getFullName();
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_status($data = array())
    {
        $status_texts = Offer::getStatusTextArray();
        
        if (isset($status_texts[$data['status']])) {
            return $status_texts[$data['status']];
        }
        
        return 'N/A';
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_user_id($data = array())
    {
        $userId = !empty($data['user_id']) ? intval($data['user_id']) : 0;
        $_user = new User($userId);
        
        if (!$_user->isValid()) {
            return 'N/A';
        }
        
        if ($_user->hasProfile()) {
            return '[' . $_user->getId() . '] <a href="' . $_user->getUrl() . '" target="_blank">' . $_user->getName() . '</a>';
        }
        
        return '[' . $_user->getId() . '] ' . $_user->getName();
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_booking($data = array())
    {
        $bookingId = !empty($data['booking_id']) ? intval($data['booking_id']) : 0;
        $booking = new Booking($bookingId);
        
        if (!$booking->isValid()) {
            return 'Invalid BookingID';
        }

        return '<a href="/terminanfrage/' . $booking->getSlug() . '" target="_blank">Offers</a> | <a href="' . $booking->getAdminEditUrl() . '" target="_blank">ID: ' . $booking->getId() . '</a> ' . $booking->getName() . ' / ' . $booking->getEmail();
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_location_id($data = array())
    {
        $locationId = !empty($data['location_id']) ? intval($data['location_id']) : 0;
        $location = new Location($locationId);
        
        if (!$location->isValid()) {
            return '';
        }
        
        $length = 30;
        $name = mb_substr($location->getName(), 0, $length, $GLOBALS['CONFIG']['SYSTEM_CHARSET']);
        if (mb_strlen($location->getName(), $GLOBALS['CONFIG']['SYSTEM_CHARSET']) > $length) {
            $name .= '&hellip;';
        }

        return '[' . $location->getId() . '] <a href="' . $location->getUrl() . '" target="_blank">' . $name . '</a>';
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_appointment_start_at($data = array())
    {
        if (empty($data['appointment_start_at'])) {
            return '';
        }
        
        $text = $data['appointment_start_at'];
        if ($data['appointment_start_at'] == '1970-01-01 00:00:00') {
            $text = 'Terminanfrage';
        }

        return $text;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_expires_at($data = array())
    {
        // Force an expire value. Now. Yes, immediately
        if (empty($data['expires_at'])) {
            $data['expires_at'] = 'now';
        }
        
        $now = new DateTime();
        $expiresAt = new DateTime($data['expires_at']);
        
        if ($now > $expiresAt) {
            $color = '#ff0000';
        } else {
            $color = '#5AA419';
        }

        return '<span style="color:' . $color . '">' . $data['expires_at'] . '</span>';
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_action($data = array())
    {
        $mailIcon = getStaticUrl('ico/email.png');
        if ($data['mail_send_at']) {
            $mailIcon = getStaticUrl('ico/email_open.png');
        }

        return '<a href="' . $this->admin->getUrl() . '?action=send_booking_offers_email&id=' . $data['id'] . '"><img src="' . $mailIcon . '" alt="" title="Send booking offers email" class="button-action" /></a>';
    }

    /**
     * @param array $data
     *
     * @return mixed
     */
    public function get_data_booked_in_practice($data = array())
    {
        if (empty($data['booked_in_practice'])) {
            $data['booked_in_practice'] = 0;
        }

        return $GLOBALS['CONFIG']['ARRAY_NO_YES'][$data['booked_in_practice']];
    }
}