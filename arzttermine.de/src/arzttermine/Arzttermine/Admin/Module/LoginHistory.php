<?php

namespace Arzttermine\Admin\Module;

use Arzttermine\Admin\Configuration;
use Arzttermine\Admin\Module;
use Arzttermine\User\User;

final class LoginHistory extends Module {

    /**
     * @var string
     */
    protected $title = 'Login History Admin';

    /**
     * @var string
     */
    protected $role = 'login_history_admin';

    /**
     * @var string
     */
    protected $slug = 'login_history';

    /**
     * {@inheritdoc}
     */
    public function boot()
    {
        $translator = $this->container->get('translator');

        $MENU_ARRAY = array(
            "id"               => array("sql" => 1, "name" => "ID", "show" => 1, "edit" => 0, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1),
            "user_id"          => array("sql" => 1, "name" => 'User', "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_user_id", "para" => true)),
            "user_type"        => array("sql" => 1, "name" => 'User Type', "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_user_type", "para" => true)),
            "ip"               => array("sql" => 1, "name" => 'IP', "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_user_ip", "para" => true)),
            "operating_system" => array("sql" => 1, "name" => 'Operating System', "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_user_operating_system", "para" => true)),
            "browser"          => array("sql" => 1, "name" => 'Browser', "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_user_browser", "para" => true)),
            "browser_version"  => array("sql" => 1, "name" => 'Browser Version', "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_user_browser_version", "para" => true)),
            "date"             => array("sql" => 1, "name" => 'Login Date', "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1)
        );

        $BUTTON = array(
            "delete" => array("func" => "location_admin_delete", "alt_text" => $translator->trans('Löschen'), "script" => $this->admin->getUrl(), "gfx" => "delete.png", "action" => "delete", "th" => 0, "td" => 1),
            "search" => array("func" => "location_admin_search", "alt_text" => $translator->trans('Suchen'), "script" =>  $this->admin->getUrl(), "gfx" => "search.png", "action" => "search", "th" => 1, "td" => 0)
        );

        $TABLE_CONFIG = array(
            "table_name"  => DB_TABLENAME_LOGIN_HISTORY,
            "alpha_index" => "name",
        );
        $URL_PARA = array(
            "page"         => 1,
            "alpha"        => '',
            "sort"         => 0,
            "direction"    => 'DESC',
            "num_per_page" => 25,
            "type"         => 'num'
        );

        $configuration = new Configuration();
        $configuration->menu    = $MENU_ARRAY;
        $configuration->buttons = $BUTTON;
        $configuration->table   = $TABLE_CONFIG;
        $configuration->default_parameters = $URL_PARA;

        $this->configuration = $configuration;
    }

    /**
     * {@inheritdoc}
     */
    public function render()
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $html = '';

        $action = getParam("action", 'list');
        $form_data = getParam("form_data");

        switch ($action) {
            case "delete":
                if (!$this->checkPrivilege('login_history_admin')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                } else {
                    if (!isset($_REQUEST["confirm_id"])) {
                        $id = getParam("id");
                        $html = $this->ask4delete($id, $this->parameters);
                    } else {
                        $confirm_id = getParam("confirm_id");
                        if ($this->delete_one($confirm_id)) {
                            $view->addMessage('STATUS', 'NOTICE', $translator->trans('the_entry_was_deleted_successfully'));
                        } else {
                            $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_could_not_be_deleted'));
                        }
                    }
                }
                break;
            
            case "search":
                if (!$this->checkPrivilege('login_history_admin')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                if (isset($_REQUEST["send_button"])) {
                    $this->parameters["page"] = 1;
                    $this->parameters["alpha"] = '';
                }
                
                $html = $this->show_search_form($action, $form_data, $this->parameters);
                if (is_array($form_data)) {
                    $html .= $this->get_list($this->parameters, $form_data);
                }
                break;
        }

        if (empty($html)) {
            if ($this->checkPrivilege()) {
                $html = $this->get_list($this->parameters);
            }
        }

        return $html;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_user_id($data = array())
    {
        $id = $data['user_id'];
        $user_type_id = $data['user_type'];
        $_html = '';
        if ($id) {
            switch ($user_type_id) {
                case LOGIN_DOCTOR_TYPE:
                    $_user = new User($id);
                    if ($_user->isValid()) {
                        $_link = $GLOBALS['CONFIG']['URL_HTTP_LIVE'] . '/' . $_user->getSlug();
                        $_html .= '<a href="' . $_link . '" target="_blank">' . $_user->getName() . '</a>';
                    } else {
                        $_html .= '<span class="red">Wrong ID [' . $id . ']</span><br />';
                    }
                    break;
                /*
                 * Broken, as the PatientsManager no longer exists
                case LOGIN_PATIENT_TYPE:
                    $patientsManager = PatientsManager::getInstance();
                    $patient = $patientsManager->getPatientById($id);
                    if ($patient) {
                        $_html .= $patient->getFirstName() . ' ' . $patient->getLastName();
                    } else {
                        $_html .= '<span class="red">wrong id: ' . $id . '</span><br />';
                    }
                    break;
                */
                default:
                    break;
            }
        }

        return $_html;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_user_type($data = array())
    {
        return (
            isset($data['user_type']) &&
            isset($GLOBALS['CONFIG']['USER_LOGIN_TYPES'][$data['user_type']])
        ) ? $GLOBALS['CONFIG']['USER_LOGIN_TYPES'][$data['user_type']] : 'N/A';
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_user_ip($data = array())
    {
        return !empty($data['ip']) ? $data['ip'] : 'N/A';
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_user_operating_system($data = array())
    {
        return !empty($data['operating_system']) ? $data['operating_system'] : 'N/A';
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_user_browser($data = array())
    {
        return !empty($data['browser']) ? $data['browser'] : 'N/A';
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_user_browser_version($data = array())
    {
        return !empty($data['browser_version']) ? $data['browser_version'] : 'N/A';
    }

    /**
     * @param array $URL_PARA
     * @param int $search
     *
     * @return string
     */
    public function get_list($URL_PARA, $search = 0)
    {
        return $this->admin->admin_get_list(0, $search, $URL_PARA);
    }

    /**
     * @param int $id
     * @param array $URL_PARA
     *
     * @return string
     */
    public function ask4delete($id, $URL_PARA)
    {
        return $this->admin->admin_get_confirm_table($id, $URL_PARA);
    }

    /**
     * @param int $id
     *
     * @return int
     */
    public function delete_one($id)
    {
        return $this->admin->admin_set_delete($id);
    }

    /**
     * @param string $action
     * @param array $search_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function show_search_form($action, $search_data, $URL_PARA)
    {
        return $this->admin->admin_get_form($action, 0, $search_data, $URL_PARA);
    }
}