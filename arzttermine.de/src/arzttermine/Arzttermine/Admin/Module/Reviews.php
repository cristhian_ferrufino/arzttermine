<?php

namespace Arzttermine\Admin\Module;

use Arzttermine\Admin\Configuration;
use Arzttermine\Admin\Module;
use Arzttermine\Booking\Booking;
use Arzttermine\Core\DateTime;
use Arzttermine\User\User;

final class Reviews extends Module {

    /**
     * @var string
     */
    protected $title = 'Reviews Admin';

    /**
     * @var string
     */
    protected $role = 'review_admin';

    /**
     * @var string
     */
    protected $slug = 'reviews';

    /**
     * {@inheritdoc}
     */
    public function boot()
    {
        $translator = $this->container->get('translator');

        $MENU_ARRAY = array(
            "id"            => array("sql" => 1, "name" => "ID", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "user_id"       => array("sql" => 1, "name" => "Arzt", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_user_id", "para" => true), "form_input" => array("type" => "none")),
            "booking_id"    => array("sql" => 1, "name" => "BookingID", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "list_function" => array("name" => "get_data_booking_id", "para" => true)),
            "patient_email" => array("sql" => 1, "name" => "Patient eMail", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "patient_name"  => array("sql" => 1, "name" => "Patient Name", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "rating_1"      => array("sql" => 1, "name" => "Rating1", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "rating_2"      => array("sql" => 1, "name" => "Rating2", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "rating_3"      => array("sql" => 1, "name" => "Rating3", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "rate_text"     => array("sql" => 1, "name" => "RatingText", "show" => 0, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "rating_4"      => array("sql" => 1, "name" => "AT Rating", "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "rate_4_text"     => array("sql" => 1, "name" => "AT Rating Text", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 0, "nowrap" => 1, "form_input" => array("type" => "none")),
            "rated_at"      => array("sql" => 1, "name" => "Rated at", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "approved_at"   => array("sql" => 1, "name" => "Approved at", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "datetime")),
        );

        $BUTTON = array(
            "new"    => array("func" => "review_admin_new", "alt_text" => $translator->trans('new_item'), "script" => $this->admin->getUrl(), "gfx" => "document_new.png", "action" => "new", "th" => 1, "td" => 0),
            "edit"   => array("func" => "review_admin_edit", "alt_text" => $translator->trans('edit_item'), "script" => $this->admin->getUrl(), "gfx" => "document_edit.png", "action" => "edit", "th" => 0, "td" => 1),
            "delete" => array("func" => "review_admin_delete", "alt_text" => $translator->trans('delete_item'), "script" => $this->admin->getUrl(), "gfx" => "delete.png", "action" => "delete", "th" => 0, "td" => 1),
            "cancel" => array("func" => "review_admin_cancel", "alt_text" => $translator->trans('cancel_item'), "script" => $this->admin->getUrl(), "gfx" => "cancel.png", "action" => "cancel", "th" => 0, "td" => 1),
            "search" => array("func" => "review_admin_search", "alt_text" => $translator->trans('search'), "script" => $this->admin->getUrl(), "gfx" => "search.png", "action" => "search", "th" => 1, "td" => 0)
        );
        $TABLE_CONFIG = array(
            "table_name"  => DB_TABLENAME_REVIEWS,
            "alpha_index" => "patient_email",
        );
        $URL_PARA = array(
            'page'         => 1,
            'alpha'        => '',
            "sort"         => 0,
            "direction"    => 'DESC',
            "num_per_page" => 50,
            "type"         => 'num'
        );

        $configuration = new Configuration();
        $configuration->menu    = $MENU_ARRAY;
        $configuration->buttons = $BUTTON;
        $configuration->table   = $TABLE_CONFIG;
        $configuration->default_parameters = $URL_PARA;

        $this->configuration = $configuration;
    }

    /**
     * {@inheritdoc}
     */
    public function render()
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $html = '';

        $action = getParam("action", 'list');
        $form_data = getParam("form_data");

        switch ($action) {
            case "edit":
                if (!$this->checkPrivilege('review_admin_edit')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                } else {
                    $id = getParam("id");
                    if (is_array($form_data)) {
                        $html = $this->change_one($action, $id, $form_data, $this->parameters);
                    } else {
                        $html = $this->form_change($action, $id, $this->parameters);
                    }
                }
                break;
            
            case "delete":
                if (!$this->checkPrivilege('review_admin_delete')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                } else {
                    if (!isset($_REQUEST["confirm_id"])) {
                        $id = getParam("id");
                        $html = $this->ask4delete($id, $this->parameters);
                    } else {
                        $confirm_id = getParam("confirm_id");
                        if ($this->delete_one($confirm_id)) {
                            $view->addMessage('STATUS', 'NOTICE', $translator->trans('the_entry_was_deleted_successfully'));
                        } else {
                            $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_could_not_be_deleted'));
                        }
                    }
                }
                break;
            
            case "search":
                if (!$this->checkPrivilege('review_admin_search')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                if (isset($_REQUEST["send_button"])) {
                    $this->parameters["page"] = 1;
                    $this->parameters["alpha"] = '';
                }
                $html = $this->show_search_form($action, $form_data, $this->parameters);
                if (is_array($form_data)) {
                    $html .= $this->get_list($this->parameters, $form_data);
                }
                break;
            
            case "new":
                if (!$this->checkPrivilege('review_admin_new')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                if (is_array($form_data)) {
                    $html = $this->new_one($action, $form_data, $this->parameters);
                } else {
                    $html = $this->form_new($action, $this->parameters);
                }
                break;
        }

        if (empty($html)) {
            if ($this->checkPrivilege('review_admin_view')) {
                $html = $this->get_list($this->parameters);
            }
        }

        return $html;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_reviewed_at($data = array())
    {
        return mysql2date('d.m.Y H:i:s', $data['reviewed_at'], false);
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_user_id($data = array())
    {
        $userId = $data['user_id'];
        $_user = new User($userId);
        if (!$_user->isValid()) {
            return 'N/A';
        }
        
        if ($_user->hasProfile()) {
            return '<a href="' . $_user->getUrl() . '" target="_blank">' . $_user->getName() . '</a>';
        } else {
            return $_user->getName();
        }
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_booking_id($data = array())
    {
        $bookingId = $data['booking_id'];
        $booking = new Booking($bookingId);
        if (!$booking->isValid()) {
            return 'N/A';
        }

        return '<a href="' . $booking->getAdminEditUrl() . '" target="_blank">' . $booking->getId() . '</a>';
    }

    /**
     * @param array $URL_PARA
     * @param array $search
     *
     * @return string
     */
    public function get_list($URL_PARA, $search = array())
    {
        return $this->admin->admin_get_list(0, $search, $URL_PARA);
    }

    /**
     * @param string $action
     * @param array $form_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function new_one($action, $form_data, $URL_PARA)
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $profile = $this->container->get('current_user');
        $html = '';

        // 1. set defaults
        $form_data["created_by"] = $profile->getId();
        $form_data["created_at"] = (new DateTime())->getDateTime();

        // 2. check

        // if there are checkboxes than set the keys to make sure that deselected checkboxes are beeing set
        $form_data = $this->admin->set_unselected_checkbox_values($form_data);

        // 3. exit on errors
        if ($view->countMessages()) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('Bitte prüfen Sie Ihre Eingaben.'));
            return $this->form_change($action, 0, $URL_PARA, $form_data);
        }
        
        // 4. sanitize
        $_id = $this->admin->admin_set_new($form_data);
        if ($_id > 0) {
            $view->addMessage('STATUS', 'NOTICE', $translator->trans('action_was_successfully'));
        } elseif ($_id == -1) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_allready_exists'));
            $html = $this->form_new($action, $URL_PARA, $form_data);
        } elseif ($_id == -2) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('name_of_primary_key_is_empty'));
            $html = $this->form_new($action, $URL_PARA, $form_data);
        } else {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_could_not_be_created'));
        }

        return $html;
    }

    /**
     * @param string $action
     * @param int $id
     * @param array $form_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function change_one($action, $id, $form_data, $URL_PARA)
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $profile = $this->container->get('current_user');
        $html = '';

        // 1. set defaults
        $form_data["updated_by"] = $profile->getId();
        $form_data["updated_at"] = (new DateTime())->getDateTime();

        // 2. check

        // 3. exit on errors
        if ($view->countMessages()) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('Bitte prüfen Sie Ihre Eingaben.'));
            return $this->form_change($action, $id, $URL_PARA, $form_data);
        }
        
        // 4. sanitize
        $_result = $this->admin->admin_set_update($id, $form_data);
        if ($_result == true) {
            $view->addMessage('STATUS', 'NOTICE', $translator->trans('action_was_successfully'));
            return '';
        }

        if ($_result == false) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('no_data_has_been_updated'));
            $html = $this->form_change($action, $id, $URL_PARA, $form_data);
        }

        return $html;
    }

    /**
     * @param int $id
     * @param array $URL_PARA
     *
     * @return string
     */
    public function ask4delete($id, $URL_PARA)
    {
        return $this->admin->admin_get_confirm_table($id, $URL_PARA, 'delete');
    }

    /**
     * @param int $id
     *
     * @return int
     */
    public function delete_one($id)
    {
        return $this->admin->admin_set_delete($id);
    }

    /**
     * @param string $action
     * @param array $URL_PARA
     * @param array $error_data
     *
     * @return string
     */
    public function form_new($action, $URL_PARA, $error_data = array())
    {
        return $this->admin->admin_get_form($action, 0, $error_data, $URL_PARA);
    }

    /**
     * @param string $action
     * @param int $id
     * @param array $URL_PARA
     * @param array $error_data
     *
     * @return string
     */
    public function form_change($action, $id, $URL_PARA, $error_data = array())
    {
        return $this->admin->admin_get_form($action, $id, $error_data, $URL_PARA);
    }

    /**
     * @param string $action
     * @param array $search_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function show_search_form($action, $search_data, $URL_PARA)
    {
        return $this->admin->admin_get_form($action, 0, $search_data, $URL_PARA);
    }
}