<?php

namespace Arzttermine\Admin\Module;

use Arzttermine\Admin\Configuration;
use Arzttermine\Admin\Module;
use Arzttermine\Core\DateTime;
use Arzttermine\Insurance\Insurance;
use Arzttermine\Location\District;
use Arzttermine\MedicalSpecialty\MedicalSpecialty;
use Arzttermine\Search\Text;
use Arzttermine\User\User;

final class SEO extends Module {

    /**
     * @var string
     */
    protected $title = 'SEO Admin';

    /**
     * @var string
     */
    protected $role = 'searchresulttext_admin';

    /**
     * @var string
     */
    protected $slug = 'seo';

    /**
     * {@inheritdoc}
     */
    public function boot()
    {
        $translator = $this->container->get('translator');

        $MENU_ARRAY = array(
            "id"                    => array("sql" => 1, "name" => "ID", "show" => 1, "edit" => 1, "new" => 0, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "status"                => array("sql" => 1, "name" => "Status", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "select", "values" => $GLOBALS['CONFIG']['SEARCH_RESULT_TEXT_STATUS']), "list_function" => array("name" => "get_data_status", "para" => true)),
            "permalink"             => array("sql" => 1, "name" => "Permalink", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_permalink", "para" => true)),
            "medical_specialty_id"  => array("sql" => 1, "name" => "Fachrichtung", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_medical_specialty_id", "para" => true), "form_input" => array("type" => "select", "values" => $this->get_values_medical_specialty_id())),
            "insurance_id"          => array("sql" => 1, "name" => "Versicherung", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_insurance_id", "para" => true), "form_input" => array("type" => "select", "values" => $this->get_values_insurance_id())),
            "address_search"        => array("sql" => 1, "name" => "Adresssuchtext", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "district_id"           => array("sql" => 1, "name" => "Ortsteil", "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "list_function" => array("name" => "get_data_district_id", "para" => true), "form_input" => array("type" => "select", "values" => District::getHtmlOptionsWithCity(true, $translator->trans('...optional...')))),
            "html_title"            => array("sql" => 1, "name" => 'HTML-Title', "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("template" => "admin/_form_text_counter.tpl")),
            "html_meta_description" => array("sql" => 1, "name" => 'HTML-Meta-Description', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("template" => "admin/_form_text_counter.tpl")),
            "html_meta_keywords"    => array("sql" => 1, "name" => 'HTML-Meta-Keywords', "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1, "form_input" => array("template" => "admin/_form_text_counter.tpl")),
            "content_1"             => array("sql" => 1, "name" => "Content", "show" => 0, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "editor_tinymce", "values" => "")),
            "mf_average"            => array("sql" => 1, "name" => 'Bewertung', "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "mf_total"              => array("sql" => 1, "name" => 'Teilnehmer', "show" => 1, "edit" => 1, "new" => 1, "search" => 1, "sort" => 1, "nowrap" => 1),
            "created_at"            => array("sql" => 1, "name" => "Erstellungsdatum", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 0, "form_input" => array("type" => "none")),
            "created_by"            => array("sql" => 1, "name" => "Erstellt von", "show" => 1, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_created_by", "para" => true), "list_function" => array("name" => "get_data_created_by", "para" => true)),
            "updated_at"            => array("sql" => 1, "name" => "Letztes Update", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none")),
            "updated_by"            => array("sql" => 1, "name" => "Letzter Bearbeiter", "show" => 0, "edit" => 1, "new" => 0, "search" => 0, "sort" => 1, "nowrap" => 1, "form_input" => array("type" => "none"), "form_function" => array("name" => "get_data_updated_by", "para" => true), "list_function" => array("name" => "get_data_updated_by", "para" => true)),
        );

        $BUTTON = array(
            "new"    => array("func" => "searchresulttext_admin_new", "alt_text" => $translator->trans('new_item'), "script" => $this->admin->getUrl(), "gfx" => "document_new.png", "action" => "new", "th" => 1, "td" => 0),
            "edit"   => array("func" => "searchresulttext_admin_edit", "alt_text" => $translator->trans('edit_item'), "script" => $this->admin->getUrl(), "gfx" => "document_edit.png", "action" => "edit", "th" => 0, "td" => 1),
            "delete" => array("func" => "searchresulttext_admin_delete", "alt_text" => $translator->trans('delete_item'), "script" => $this->admin->getUrl(), "gfx" => "delete.png", "action" => "delete", "th" => 0, "td" => 1),
            "search" => array("func" => "searchresulttext_admin_search", "alt_text" => $translator->trans('search'), "script" => $this->admin->getUrl(), "gfx" => "search.png", "action" => "search", "th" => 1, "td" => 0)
        );
        $TABLE_CONFIG = array(
            "table_name"  => DB_TABLENAME_SEARCHRESULTTEXTS,
            "alpha_index" => "name",
        );
        $URL_PARA = array(
            'page'         => 1,
            'alpha'        => '',
            "sort"         => 0,
            "direction"    => 'DESC',
            "num_per_page" => 25,
            "type"         => 'num'
        );

        $configuration = new Configuration();
        $configuration->menu    = $MENU_ARRAY;
        $configuration->buttons = $BUTTON;
        $configuration->table   = $TABLE_CONFIG;
        $configuration->default_parameters = $URL_PARA;

        $this->configuration = $configuration;
    }

    /**
     * {@inheritdoc}
     */
    public function render()
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $html = '';

        $action = getParam("action", 'list');
        $form_data = getParam("form_data");

        switch ($action) {
            case "edit":
                if (!$this->checkPrivilege('searchresulttext_admin_edit')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                } else {
                    $id = getParam("id");
                    if (is_array($form_data)) {
                        $html = $this->change_one($action, $id, $form_data, $this->parameters);
                    } else {
                        $html = $this->form_change($action, $id, $this->parameters);
                    }
                }
                break;
            
            case "delete":
                if (!$this->checkPrivilege('searchresulttext_admin_delete')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                } else {
                    if (!isset($_REQUEST["confirm_id"])) {
                        $id = getParam("id");
                        $html = $this->ask4delete($id, $this->parameters);
                    } else {
                        $confirm_id = getParam("confirm_id");
                        if ($this->delete_one($confirm_id)) {
                            $view->addMessage('STATUS', 'NOTICE', $translator->trans('the_entry_was_deleted_successfully'));
                        } else {
                            $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_could_not_be_deleted'));
                        }
                    }
                }
                break;
            
            case "search":
                if (!$this->checkPrivilege('searchresulttext_admin_search')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                
                if (isset($_REQUEST["send_button"])) {
                    $this->parameters["page"] = 1;
                    $this->parameters["alpha"] = '';
                }
                
                $html = $this->show_search_form($action, $form_data, $this->parameters);
                if (is_array($form_data)) {
                    $html .= $this->get_list($this->parameters, $form_data);
                }
                break;
            
            case "new":
                if (!$this->checkPrivilege('searchresulttext_admin_new')) {
                    $view->addMessage('STATUS', 'ERROR', $translator->trans('access_denied'));
                    break;
                }
                
                if (is_array($form_data)) {
                    $html = $this->new_one($action, $form_data, $this->parameters);
                } else {
                    $html = $this->form_new($action, $this->parameters);
                }
                break;
        }

        if (empty($html)) {
            if ($this->checkPrivilege('searchresulttext_admin_view')) {
                $html = $this->get_list($this->parameters);
            }
        }

        return $html;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_created_by($data = array())
    {
        $data['created_by'] = !empty($data['created_by']) ? intval($data['created_by']) : 0;

        return (new User($data['created_by']))->getFullName();
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_created_at($data = array())
    {
        return mysql2date('d.m.Y H:i:s', $data['created_at'], false);
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_updated_by($data = array())
    {
        $data['updated_by'] = !empty($data['updated_by']) ? intval($data['updated_by']) : 0;

        return (new User($data['updated_by']))->getFullName();
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_status($data = array())
    {
        if (isset($GLOBALS['CONFIG']['SEARCH_RESULT_TEXT_STATUS'][$data['status']])) {
            return $GLOBALS['CONFIG']['SEARCH_RESULT_TEXT_STATUS'][$data['status']];
        }
        
        return 'N/A';
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_permalink($data = array())
    {
        if (isset($data['permalink'])) {
            return '<a href="' . $GLOBALS['CONFIG']['URL_HTTP_LIVE'] . $data['permalink'] . '">' . $data['permalink'] . '</a>';
        }

        return $data['permalink'];
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_medical_specialty_id($data = array())
    {
        return MedicalSpecialty::getNameById($data['medical_specialty_id']);
    }

    /**
     * @return array
     */
    public function get_values_medical_specialty_id()
    {
        return MedicalSpecialty::getHtmlOptions(true);
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_district_id($data = array())
    {
        return District::getNameById($data['district_id']);
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function get_data_insurance_id($data = array())
    {
        return Insurance::getNameById($data['insurance_id']);
    }

    /**
     * @return string
     */
    public function get_values_insurance_id()
    {
        return Insurance::getHtmlOptions(true);
    }

    /**
     * @param array $URL_PARA
     * @param array $search
     *
     * @return string
     */
    public function get_list($URL_PARA, $search = array())
    {
        return $this->admin->admin_get_list(0, $search, $URL_PARA);
    }

    /**
     * @param string $action
     * @param array $form_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function new_one($action, $form_data, $URL_PARA)
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $profile = $this->container->get('current_user');
        $html = '';

        // 1. set defaults
        $form_data["created_by"] = $profile->getId();
        $form_data["created_at"] = (new DateTime())->getDateTime();

        // 2. check
        $_permalink = sanitize_permalink($form_data["permalink"]);
        $_search_result_text = new Text();
        $_total = $_search_result_text->count('permalink="' . sqlsave($_permalink) . '"');
        if ($_total > 0) {
            $view->addMessage('admin_message_permalink', 'ERROR', 'Der Permalink ist bereits vergeben!');
        }

        // if there are checkboxes than set the keys to make sure that deselected checkboxes are beeing set
        $form_data = $this->admin->set_unselected_checkbox_values($form_data);

        // 3. exit on errors
        if ($view->countMessages()) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('Bitte prüfen Sie Ihre Eingaben.'));
            return $this->form_change($action, 0, $URL_PARA, $form_data);
        }
        
        // 4. sanitize
        $form_data["permalink"] = sanitize_permalink($form_data["permalink"]);
        $form_data["medical_specialty_id"] = sanitize_option('int', $form_data["medical_specialty_id"]);
        $form_data["address_search"] = sanitize_text_field($form_data["address_search"]);

        $_id = $this->admin->admin_set_new($form_data);
        if ($_id > 0) {
            $view->addMessage('STATUS', 'NOTICE', $translator->trans('Der Suchergebnistext wurde erfolgreich mit der ID ' . $_id . ' angelegt.<br /><a href="/administration/seo?edit&id=' . $_id . '">Eintrag bearbeiten</a>'));
        } elseif ($_id == -1) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_allready_exists'));
            $html = $this->form_new($action, $URL_PARA, $form_data);
        } elseif ($_id == -2) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('name_of_primary_key_is_empty'));
            $html = $this->form_new($action, $URL_PARA, $form_data);
        } else {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('the_entry_could_not_be_created'));
        }

        return $html;
    }
    
    /**
     * @param string $action
     * @param int $id
     * @param array $form_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function change_one($action, $id, $form_data, $URL_PARA)
    {
        $view = $this->getView();
        $translator = $this->container->get('translator');
        $profile = $this->container->get('current_user');
        $html = '';

        // 1. set defaults
        $form_data["updated_by"] = $profile->getId();
        $form_data["updated_at"] = (new DateTime)->getDateTime();

        // 2. check
        $_permalink = sanitize_permalink($form_data["permalink"]);
        $_search_result_text = new Text();
        $_total = $_search_result_text->count('permalink="' . sqlsave($_permalink) . '" AND id!=' . $id);
        if ($_total > 0) {
            $view->addMessage('admin_message_permalink', 'ERROR', 'Der Permalink ist bereits vergeben!');
        }

        // if there are checkboxes than set the keys to make sure that deselected checkboxes are beeing set
        $form_data = $this->admin->set_unselected_checkbox_values($form_data);

        // 3. exit on errors
        if ($view->countMessages()) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('Bitte prüfen Sie Ihre Eingaben.'));
            return $this->form_change($action, $id, $URL_PARA, $form_data);
        }
        // 4. sanitize
        $form_data["permalink"] = sanitize_permalink($form_data["permalink"]);
        $form_data["medical_specialty_id"] = sanitize_option('int', $form_data["medical_specialty_id"]);
        $form_data["address_search"] = sanitize_text_field($form_data["address_search"]);

        // if there are checkboxes than set the keys to make sure that deselected checkboxes are beeing set
        $form_data = $this->admin->set_unselected_checkbox_values($form_data);

        $_result = $this->admin->admin_set_update($id, $form_data);
        if ($_result == true) {
            $view->addMessage('STATUS', 'NOTICE', $translator->trans('action_was_successfully'));
            return '';
        }

        if ($_result == false) {
            $view->addMessage('STATUS', 'ERROR', $translator->trans('no_data_has_been_updated'));
            $html = $this->form_change($action, $id, $URL_PARA, $form_data);
        }

        return $html;
    }

    /**
     * @param int $id
     * @param array $URL_PARA
     *
     * @return string
     */
    public function ask4delete($id, $URL_PARA)
    {
        return $this->admin->admin_get_confirm_table($id, $URL_PARA, 'delete');
    }

    /**
     * @param int $id
     *
     * @return int
     */
    public function delete_one($id)
    {
        return $this->admin->admin_set_delete($id);
    }

    /**
     * @param string $action
     * @param array $URL_PARA
     * @param array $error_data
     *
     * @return string
     */
    public function form_new($action, $URL_PARA, $error_data = array())
    {
        $this->getView()->setFocus('#form_data_address_locality');

        return $this->admin->admin_get_form($action, 0, $error_data, $URL_PARA);
    }

    /**
     * @param string $action
     * @param int $id
     * @param array $URL_PARA
     * @param array $error_data
     *
     * @return string
     */
    public function form_change($action, $id, $URL_PARA, $error_data = array())
    {
        $this->getView()->setFocus('#form_data_address_locality');

        return $this->admin->admin_get_form($action, $id, $error_data, $URL_PARA);
    }

    /**
     * @param string $action
     * @param array $search_data
     * @param array $URL_PARA
     *
     * @return string
     */
    public function show_search_form($action, $search_data, $URL_PARA)
    {
        return $this->admin->admin_get_form($action, 0, $search_data, $URL_PARA);
    }
    
    
}