<?php

namespace Arzttermine\Admin;

use Arzttermine\Application\Application;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class Module implements AdminModuleInterface {

    /**
     * @var \Arzttermine\Admin\Configuration
     */
    public $configuration;

    /**
     * @var array
     */
    public $parameters = array();

    /**
     * @var string
     */
    protected $title = 'Administration';

    /**
     * Slug used to reference routing etc
     * 
     * @var string
     */
    protected $slug = '';

    /**
     * Role required to see the module index page (not specific actions)
     * 
     * @var string
     */
    protected $role = '';

    /**
     * Reference to parent Admin object
     * @todo Better than this
     *
     * @var Admin
     */
    protected $admin;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = trim($title);

        return $this;
    }

    /**
     * @return \Arzttermine\View\View;
     */
    public function getView()
    {
        $view = $this->container->get('templating');
        $view->disableCaching();
        return $view;
    }

    /**
     * {@inheritdoc}
     */
    public function checkPrivilege($role = '')
    {
        if (empty($role)) {
            $role = $this->role;
        }
        
        $profile = $this->container->get('current_user');
        return $profile->checkPrivilege($role);
    }

    /**
     * @param Admin $admin
     */
    public function setAdmin(Admin $admin)
    {
        $this->admin = $admin;
        $this->container = $this->admin->getContainer();
    }

    /**
     * Parse parameters sent to the module
     */
    public function parseParameters()
    {
        $request = $this->container->get('request');
        $get  = $request->getQuery();
        $post = $request->getRequest();
        
        // Merge an array of default params, filtered GET, and filtered POST
        $this->parameters = array_merge(
            $this->configuration->default_parameters,
            array_intersect_key($get, $this->configuration->default_parameters),
            array_intersect_key($post, $this->configuration->default_parameters)
        );
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
}