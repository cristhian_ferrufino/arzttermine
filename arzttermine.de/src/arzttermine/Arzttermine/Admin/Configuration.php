<?php

namespace Arzttermine\Admin;

class Configuration {

    /**
     * @var array
     */
    public $default_parameters = array();

    /**
     * @var array
     */
    public $table = array();

    /**
     * @var array
     */
    public $buttons = array();

    /**
     * @var array
     */
    public $menu = array();

}