<?php

namespace Arzttermine\Admin;

use Arzttermine\Application\Application;
use Arzttermine\Media\Media;
use Arzttermine\View\Html;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class Admin implements ContainerAwareInterface {

    use ContainerAwareTrait;

    /**
     * =
     */
    const SEARCH_STYLE_EQ = 0;

    /**
     * >
     */
    const SEARCH_STYLE_GT = 1;

    /**
     * >=
     */
    const SEARCH_STYLE_GTOEQ = 2;

    /**
     * <
     */
    const SEARCH_STYLE_LT = 3;

    /**
     * <=
     */
    const SEARCH_STYLE_LTOEQ = 4;

    /**
     * NOT
     */
    const SEARCH_STYLE_NOT = 5;

    /**
     * =''
     */
    const SEARCH_STYLE_EMPTY = 6;

    /**
     * != ''
     */
    const SEARCH_STYLE_NOTEMPTY = 7;

    /**
     * LIKE
     */
    const SEARCH_STYLE_LIKE = 8;

    /**
     * BETWEEN
     */
    const SEARCH_STYLE_BETWEEN = 9;

    /**
     * @var array
     */
    public $searched_keys;

    /**
     * @var AdminModuleInterface
     */
    protected $module;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @return ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @return Module
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @param AdminModuleInterface $module
     *
     * @return self
     */
    public function setModule(AdminModuleInterface $module)
    {
        if ($module instanceof ContainerAware) {
            $module->setContainer($this->container);
        }

        $module->setAdmin($this);
        $this->module = $module;

        return $this;
    }
    
    /**
     * Load miscellaneous config here
     */
    public function initialise()
    {
        $this->module->boot();
    }

    /**
     * @return string
     */
    public function generateUrl($route, $parameters = array(), $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH)
    {
        return $this->container->get('router')->generate($route, $parameters, $referenceType);
    }

    /**
     * Get current admin URL (with module)
     * 
     * @return string
     */
    public function getUrl()
    {
        // NOTE: This may break things
        if (!$this->getModule()->getSlug()) {
            return '';
        }
        
        return $this->generateUrl('admin_' . $this->getModule()->getSlug());
    }

    /**
     * Shortcut to return the request service.
     *
     * @throws \Exception
     * @return \Arzttermine\Http\Request
     */
    public function getRequest()
    {
        if (!$this->container->has('request')) {
            throw new \Exception('Request not found');
        }

        return $this->container->get('request');
    }

    /**
     * Shortcut to return the Doctrine Registry service.
     *
     * @return \Arzttermine\Db\MySQLConnection
     * @throws \Exception
     */
    public function getDatabase()
    {
        if (!$this->container->has('database')) {
            throw new \Exception('Database not found');
        }

        return $this->container->get('database');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Session\Session
     * @throws \Exception
     */
    public function getSession()
    {
        if (!$this->container->has('session')) {
            throw new \Exception('Session not found');
        }

        return $this->container->get('session');
    }

    /**
     * @return \Symfony\Component\Translation\Translator
     * @throws \Exception
     */
    public function getTranslator()
    {
        if (!$this->container->has('translator')) {
            throw new \Exception('Translator not found');
        }

        return $this->container->get('translator');
    }

    /**
     * @return \Arzttermine\View\View
     * @throws \Exception
     */
    public function getView()
    {
        if (!$this->container->has('templating')) {
            throw new \Exception('View not found');
        }

        return $this->container->get('templating');
    }

    /**
     * Get the currently active user
     *
     * @return \Arzttermine\Security\Authentication
     * @throws \Exception
     */
    public function getUser()
    {
        if (!$this->container->has('current_user')) {
            throw new \Exception('User Profile not found');
        }

        return $this->container->get('current_user');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function display()
    {
        $view = $this->container->get('templating');

        // @todo Move somewhere more logical
        $this->getModule()->parseParameters();
        
        $content = $this->module->render();
        
        // HACKY SHIM
        if ($content instanceof Response) {
            return $content;
        }
        
        return $view
            ->setRef('ContentRight', $content)
            ->addHeadTitle($this->module->getTitle())
            ->display('admin/content_left_right.tpl');
    }

    /**
     * get the backend navigation
     *
     * @return Html
     */
    public function get_navigation()
    {
        return $this->getView()->fetch('admin/_navi.tpl');
    }

    /**
     * Shortcut method
     *
     * @return array
     */
    protected function getMenu()
    {
        return $this->getModule()->configuration->menu;
    }

    /**
     * Shortcut method
     *
     * @return array
     */
    protected function getTableConfig()
    {
        return $this->getModule()->configuration->table;
    }

    /**
     * Shortcut method
     *
     * @return array
     */
    protected function getButton()
    {
        return $this->getModule()->configuration->buttons;
    }
    
    /**
     * If there are checkboxes in the MENU_ARRAY
     * than set the keys=0 to make sure that deselected checkboxes are beeing set/saved
     *
     * @param array $form_data
     *
     * @return array form_data
     */
    public function set_unselected_checkbox_values($form_data)
    {
        $MENU_ARRAY = $this->getMenu();

        foreach ($MENU_ARRAY as $key => $val) {
            if ($MENU_ARRAY[$key]["sql"] == 1 &&
                isset($MENU_ARRAY[$key]["form_input"]["type"]) &&
                $MENU_ARRAY[$key]["form_input"]["type"] == 'checkbox' &&
                !isset($form_data[$key])
            ) {
                $form_data[$key] = 0;
            }
        }

        return $form_data;
    }

    /** ***************************************************
     * Returns the list
     *
     * @param int $id
     * @param array $search
     * @param array $url_para
     *
     * @return string
     */
    public function admin_get_list($id, $search = array(), $url_para = array())
    {
        $MENU_ARRAY = $this->getMenu();
        $TABLE_CONFIG = $this->getTableConfig();
        $BUTTON = $this->getButton();

        $db = $this->getDatabase();
        $view = $this->getView();
        $key_array = array();
        
        // get the primary key
        $tmp = $this->admin_get_primary_key($TABLE_CONFIG["table_name"]);
        $id_field = $tmp["ID_FIELD"];

        $total = $this->admin_get_record_number($search);
        $url_para = $this->admin_check_url_para($url_para, $id_field, $total);

        /* create table with navigation */
        $url_para = $this->admin_set_navigation($url_para, $total, $search);
        $search_para = '';
        $where = ' WHERE 1=1';

        /* possible sql querys */
        if (!empty($search)) {
            /* read content */
            $search_para = $this->admin_get_search_para($search);
            
            if ($search_query = $this->admin_get_search_query($search)) {
                $where .= ' AND ' . $search_query;
            }
        }

        $_start = (($url_para['page'] - 1) * $url_para['num_per_page']);
        
        if ($url_para["type"] == "alpha") {
            $where .= ' AND ' . $TABLE_CONFIG["alpha_index"] . ' LIKE "' . ($url_para["alpha"] == "_" ? '\\_' : sqlsave($url_para["alpha"])) . '%"';
        }
        
        $sql = 'SELECT * FROM ' . $TABLE_CONFIG["table_name"] . $where . ' ORDER BY ' . $url_para["sort"] . ' ' . $url_para["direction"] . ' LIMIT ' . $_start . ',' . $url_para["num_per_page"] . ';';
        
        $db->query($sql);
        
        // set action buttons
        $action_buttons = '';
        foreach ($BUTTON as $key => $val) {
            if ($val["th"] == 1) {
                if (isset($BUTTON[$key]["extended_parameters"]) && $BUTTON[$key]["extended_parameters"] == 1) {
                    $extended_parameters = $this->admin_get_search_para($search);
                } else {
                    $extended_parameters = '';
                }
                $action_buttons .= $this->button($BUTTON[$key]["func"], $BUTTON[$key]["alt_text"], $BUTTON[$key]["script"], $this->admin_get_url_para(array_merge($url_para, array('action' => $BUTTON[$key]["action"]))) . $extended_parameters, $BUTTON[$key]["gfx"]);
            }
        }

        // save origins
        $tmp_orig_sort = $url_para["sort"];
        $tmp_orig_direction = $url_para["direction"];

        $i = 0;
        foreach ($MENU_ARRAY as $key => $val) {
            /* show only if wanted in $MENU_ARRAY (show=1) */
            if ($MENU_ARRAY[$key]["show"] == 1) {
                $url_para["sort"] = $key;
                $key_array[$i]['name'] = $MENU_ARRAY[$key]["name"];
                // is this item sortable?
                if ($MENU_ARRAY[$key]["sort"] == 1) {
                    // is that the curent order-property?
                    if ($key == $tmp_orig_sort) {
                        $url_para["direction"] = ($tmp_orig_direction == "ASC") ? "DESC" : "ASC";
                        $key_array[$i]['ordered_by_this'] = true;
                    }
                    $key_array[$i]['url_order'] = $this->getUrl() . '?' . $this->admin_get_url_para($url_para) . $search_para;
                    $url_para["direction"] = $tmp_orig_direction;
                }
                $i++;
            }
        }
        /* reset backup sort and direction */
        $url_para["sort"] = $tmp_orig_sort;
        $url_para["direction"] = $tmp_orig_direction;

        $i = 0;
        $data = array();
        /* read data */
        while ($db->next_record()) {
            $data[$i] = $db->Record;
            $i++;
        }
        
        $data_array = array();
        /* create the table data */
        for ($i = 0, $j = count($data); $i < $j; $i++) {
            /* print buttons */
            foreach ($BUTTON as $key => $val) {
                if ($val["td"] == 1) {
                    // get rid of the php notices
                    if (!isset($data_array[$i]['item_buttons'])) {
                        $data_array[$i]['item_buttons'] = '';
                    }
                    $data_array[$i]['item_buttons'] .= $this->button($BUTTON[$key]["func"], $BUTTON[$key]["alt_text"], $BUTTON[$key]["script"], $this->admin_get_url_para(array_merge($url_para, array('action' => $BUTTON[$key]["action"], 'id' => $data[$i][$id_field]))), $BUTTON[$key]["gfx"]);
                }
                if (isset($val["tr"]) && $val["tr"] == 1 && isset($val['template_key'])) {
                    $data_array[$i][$val['template_key']] = (isset($BUTTON[$key]["function"]["name"]) && method_exists($this->getModule(), $BUTTON[$key]["function"]["name"])
                        ? call_user_func_array(array($this->getModule(), $BUTTON[$key]["function"]["name"]), !empty($BUTTON[$key]["function"]["para"]) ? array($data[$i]) : array())
                        : htmlentities($data[$i][$key], ENT_COMPAT, $GLOBALS['CONFIG']['SYSTEM_CHARSET']));
                }
            }
            $k = 0;
            foreach ($MENU_ARRAY as $key => $val) {
                if ($MENU_ARRAY[$key]["show"] == 1) {
                    if (isset($MENU_ARRAY[$key]["nowrap"]) && $MENU_ARRAY[$key]["nowrap"] == 1) {
                        $data_array[$i]['keys'][$k]['nowrap'] = true;
                    }
                    /* if there is a function to visualise the data than use that one */
                    $data_array[$i]['keys'][$k]['content'] =
                        (isset($MENU_ARRAY[$key]["list_function"]["name"]) && method_exists($this->getModule(), $MENU_ARRAY[$key]["list_function"]["name"])
                            ? call_user_func_array(array($this->getModule(), $MENU_ARRAY[$key]["list_function"]["name"]), !empty($MENU_ARRAY[$key]["list_function"]["para"]) ? array($data[$i]) : array())
                            : htmlentities($data[$i][$key], ENT_COMPAT, $GLOBALS['CONFIG']['SYSTEM_CHARSET']));
                    $k++;
                }
            }
        }
        
        $view->setRef('key_array', $key_array);
        $view->setRef('data_array', $data_array);
        $view->setRef('img_order_direction', ($url_para["direction"] == "ASC") ? getStaticUrl('adm/asc.gif') : getStaticUrl('adm/desc.gif'));
        $view->setRef('action_buttons', $action_buttons);

        return $view->fetch('admin/_list.tpl');
    }

    /** ***************************************************
     * sets the pagination links
     *
     * @param total
     * @param search
     * @param url_para
     * @return self
     */
    function setPagination($total, $search, $url_para) {
        $view = $this->getView();

        if (is_array($search) && !empty($search)) {
            $search_para = $this->admin_get_search_para($search);
        } else {
            $search_para = "";
        }

        $pagination = '';
        if ($total <= $url_para["num_per_page"]) {
            // no pagination needed
        } elseif ($url_para["type"]=="alpha") {
            if ($url_para["page"] <= "0" || $url_para["page"] > ceil($total / $url_para['num_per_page']) || $url_para["page"] == "") {
                $url_para["page"] = 1;
            }
            /* alphanumeric navigation ****************************/
            $view->setRef('pagination_alpha_total', $total);
            $view->setRef('pagination_alpha_current', strtoupper($url_para["alpha"]));

            $_page = $url_para['page'];
            unset($url_para['page']);
            $_base = $this->getUrl().'?'.$this->admin_get_url_para($url_para) . '&' . $search_para;
            $url_para['page'] = $_page;
            $pagination = paginate_links( array(
                'base' => add_query_arg( 'page', '%#%', $_base ),
                'format' => '',
                'total' => ceil($total / $url_para["num_per_page"]),
                'current' => $url_para["page"],
                'type' => 'bootstrap'
            ));
        } else {
            /* numeric navigation *************************/
            $_page = $url_para['page'];
            unset($url_para['page']);
            $_base = $this->getUrl().'?'.$this->admin_get_url_para($url_para) . '&' . $search_para;
            $url_para['page'] = $_page;
            $pagination = paginate_links( array(
                'base' => add_query_arg( 'page', '%#%', $_base ),
                'format' => '',
                'total' => ceil($total / $url_para["num_per_page"]),
                'current' => $url_para["page"],
                'type' => 'bootstrap'
            ));
        }

        $view->setRef('pagination', $pagination);

        return $this;
    }

    /** ***************************************************
     * sets the pagination as a selectbox
     *
     * @param total
     * @param search
     * @param url_para
     * @return self
     */
    function setPaginationSelectbox($total, $search, $url_para) {
        $view = $this->getView();

        $pages = 0;
        if ($total > $url_para["num_per_page"]) {
            $pages = intval($total / $url_para["num_per_page"]);
            if ($total % $url_para["num_per_page"]) {
                $pages++;
            }
        }
        if ($pages < 1) {
            return $this;
        }

        $_html = '<div style="float:right;"><form method="post" action="'.$this->getUrl().'">';
        $_page = $url_para['page'];
        unset($url_para['page']);
        $_html .= $this->admin_get_hidden($url_para);
        $_html .= (!empty($search))?$this->admin_hidden("action","search"):'';
        $_html .= (!empty($search))?$this->admin_get_search_hidden($search):'';

        $_html .= '<select name="page" size="1">';
        $delta = 0;
        for ($page = 1; $page <= $pages; $page++) {
            $_html .= '<option value="'.$page.'"'.($_page == $page?' selected="selected"':'').'>'.($delta+1).' - '.($delta+$url_para["num_per_page"]).'</option>';
            $delta = $delta + $url_para["num_per_page"];
        }
        $_html .= '</select><input type="submit" value="OK" class="btn btn-default">';
        $_html .= '</form></div>';

        $view->setRef('pagination_select', $_html);
        return $this;
    }

    /**
     * Returns the metadata for a table and the primary key name
     * z.B.:
     * [Field] => id
     * [Type] => int(11)
     * [Null] => NO
     * [Key] => PRI
     * [Default] =>
     * [Extra] => auto_increment
     *
     * @param string $table
     *
     * @return array ['ID_FIELD' = sql row that holds the primary key, 'METADATA']
     */
    private function admin_get_primary_key($table = "")
    {
        $db = $this->container->get('database');
        $id_field = '';

        // check if the database name is in the table
        if (strstr($table, '.')) {
            // if so than delete the database name
            list($_db, $_table) = explode('.', $table);
            $table = $_table;
        }
        $metadata = $db->metadata($table, true);

        for ($i = 0; $i < $metadata['num_fields']; $i++) {
            // This key is the unique Identifier to indenticate the item
            if (isset($metadata[$i]["Key"])) {
                if (strstr($metadata[$i]["Key"], "PRI")) {
                    $id_field = $metadata[$i]["Field"];
                }
            }
        }

        return array(
            "ID_FIELD" => $id_field,
            "METADATA" => $metadata
        );
    }

    /** ***************************************************
     * returns the number of found elements
     *
     * @param array $search => array with possible search attrs
     * @param string $alpha => limit to alpha index
     *
     * @return int results
     */
    private function admin_get_record_number($search, $alpha = '')
    {
        $TABLE_CONFIG = $this->getTableConfig();

        $db = $this->getDatabase();

        $tmp = $this->admin_get_primary_key($TABLE_CONFIG["table_name"]);
        $id_field = $tmp["ID_FIELD"];
        $where = ' WHERE 1=1';
        
        if (!empty($search)) {
            if ($search_query = $this->admin_get_search_query($search)) {
                $where .= " AND " . $search_query;
            }
        }

        /* special case: alphanumeric navigation */
        if (!empty($alpha)) {
            $where .= " AND " . $TABLE_CONFIG["alpha_index"] . " LIKE '" . ($alpha == "_" ? '\\' . $alpha : $alpha) . "%'";
        }

        $db->query("SELECT " . $id_field . " FROM " . $TABLE_CONFIG["table_name"] . " $where;");

        return $db->num_rows();
    }

    /** ***************************************************
     * returns the sql part for the search
     * AND sets $this->searched_keys with all keys that are in the search
     *
     * @param array $search
     *
     * @return string sql
     */
    public function admin_get_search_query($search)
    {
        $MENU_ARRAY = $this->getMenu();

        if (!is_array($search) || empty($search)) {
            return '';
        }

        // these keys are in the search
        $_searched_keys = array();

        while (list($key, $value) = each($search)) {
            $form_edit_type = isset($MENU_ARRAY[$key]["form_input"]["type"]) ? $MENU_ARRAY[$key]["form_input"]["type"] : "text";

            switch ($form_edit_type) {
                case "text":
                case "textarea":
                case "editor_tinymce":
                case "date":
                case "datetime":
                case "none":
                    if ($value != "" && !preg_match("/_SEARCH_/", $key)) {
                        if (isset($search[$key . '_SEARCH_STYLE'])) {
                            $key_search_style = $search[$key . '_SEARCH_STYLE'];
                        }
                    } else if (preg_match("/_SEARCH_STYLE/", $key) && ($value == self::SEARCH_STYLE_NOTEMPTY || $value == self::SEARCH_STYLE_EMPTY)) {
                        $key_search_style = $value;
                        $key = str_replace("_SEARCH_STYLE", "", $key);
                    } else {
                        unset($key_search_style);
                    }
                    if (isset($key_search_style)) {
                        $_searched_keys[$key] = 1;
                        switch ($key_search_style) {
                            case self::SEARCH_STYLE_GT:
                                $where[] = "$key > '$value'";
                                break;
                            case self::SEARCH_STYLE_GTOEQ:
                                $where[] = "$key >= '$value'";
                                break;
                            case self::SEARCH_STYLE_LT:
                                $where[] = "$key < '$value'";
                                break;
                            case self::SEARCH_STYLE_LTOEQ:
                                $where[] = "$key <= '$value'";
                                break;
                            case self::SEARCH_STYLE_NOT:
                                $where[] = "$key != '$value'";
                                break;
                            case self::SEARCH_STYLE_EMPTY:
                                $where[] = "$key = ''";
                                break;
                            case self::SEARCH_STYLE_NOTEMPTY:
                                $where[] = "$key != ''";
                                break;
                            case self::SEARCH_STYLE_LIKE:
                                $value = str_replace("%", "\%", $value);
                                $where[] = "$key LIKE '%$value%'";
                                break;
                            case self::SEARCH_STYLE_BETWEEN:
                                if (isset($search[$key . '_SEARCH_SECOND'])) {
                                    $where[] = "($key >= '$value' AND $key <= '" . $search[$key . '_SEARCH_SECOND'] . "')";
                                }
                                break;
                            case self::SEARCH_STYLE_EQ:
                            default:
                                $where[] = "$key = '$value'";
                                break;
                        }
                    }
                    break;
                case "select":
                case "radio":
                case "checkbox":
                case "password":
                    /* search only if we want so (and the checkbox is set) */
                    if (isset($search[$key . '_SEARCH_STYLE'])) {
                        $_searched_keys[$key] = 1;
                        $where[] = "$key = '$value'";
                    }
                    break;
                default:
                    if ($value != "" && !preg_match("/_SEARCH_/", $key)) {
                        $_searched_keys[$key] = 1;
                        $value = str_replace("%", "\%", $value);
                        $where[] = "$key LIKE '%$value%'";
                    }
                    break;
            }
        }
        $this->searched_keys = $_searched_keys;

        return !empty($where) ?  implode(" AND ", $where) : '';
    }

    /********************************************************
     * INPUT:
     * url_para    => array mit den url_parametern
     ********************************************************/
    private function admin_check_url_para($url_para, $id_field = "", $total = -1)
    {
        $MENU_ARRAY = $this->getMenu();
        $TABLE_CONFIG = $this->getTableConfig();

        if ($id_field == "") {
// TOCHECK:
// muss hier nicht stehen???
//		$tmp = $this->admin_get_primary_key($TABLE_CONFIG["table_name"]);
//		$id_field = $tmp["ID_FIELD"];
//		$metadata = $tmp["METADATA"];
            $id_field = $this->admin_get_primary_key($TABLE_CONFIG["table_name"]);
        }
        // alphanumeric is valid if the alpha_index is set
        if (!isset($TABLE_CONFIG["alpha_index"]) && $url_para["type"] == "alpha") {
            $url_para["type"] = "num";
        }

        if (!isset($url_para['type']) || $url_para["type"] == '' || ($url_para["type"] != "alpha" && $url_para["type"] != "num")) {
            $url_para["type"] = "num";
        }
        if (!isset($url_para['page']) || !is_numeric($url_para['page']) || $url_para['page'] < 1) {
            $url_para['page'] = 1;
        }
        if (!isset($url_para["alpha"])) {
            $url_para["alpha"] = '';
        }
        if (!isset($url_para["num_per_page"]) || !is_numeric($url_para["num_per_page"]) || $url_para['num_per_page'] < 1) {
            $url_para["num_per_page"] = 20;
        }
        if (!isset($url_para["sort"])) {
            $url_para["sort"] = 0;
        }
        if (!isset($url_para["direction"]) || $url_para["direction"] == '' || ($url_para["direction"] != "ASC" && $url_para["direction"] != "DESC")) {
            $url_para["direction"] = "ASC";
        }
        if (false == array_key_exists($url_para["sort"], $MENU_ARRAY)) {
            $url_para["sort"] = $id_field;
        }
        // check if number of pages fits with total
        if ($total != -1) {
            if ($url_para['page'] > ceil($total / $url_para['num_per_page'])) {
                $url_para['page'] = 1;
            }
        }

        return $url_para;
    }

    /** ***************************************************
     * sets the navigation in the view
     * AND returns the validated url_para
     *
     * @param string $url_para
     * @param int $total
     * @param array $search
     *
     * @return string
     */
    public function admin_set_navigation($url_para, $total, $search = array())
    {
        $TABLE_CONFIG = $this->getTableConfig();

        $db = $this->getDatabase();
        $view = $this->getView();

        $view->setRef('total', $total);
        $view->setRef('url_form_action', $this->getUrl());
        $view->setRef('url_para', $url_para);
        $view->setRef('navigation_type', $url_para["type"]);
        if (!empty($search)) {
            $view->setRef('is_search', true);
            $view->setRef('search_values', $this->admin_get_search_hidden($search));
        }
        if (!isset($TABLE_CONFIG["alpha_index"])) {
            $view->setRef('allow_alpha', false);
        } else {
            $view->setRef('allow_alpha', true);
            // prepare the search string
            if (!empty($search)) {
                $search_para = $this->admin_get_search_para($search);
                $search_sql_query = ' WHERE ' . $this->admin_get_search_query($search);
            } else {
                $search_para = "";
                $search_sql_query = '';
            }
            if ($url_para['type'] == 'num') {
                /* numeric navigation ****************/
                $_url_para = $url_para;
                $_url_para['type'] = 'alpha';
                $_url_para['page'] = '1';
                $view->setRef('url_type_alpha', $this->getUrl() . '?' . $this->admin_get_url_para($_url_para) . $search_para);
            } else {
                /* alphanumeric navigation ****************/
                $_url_para = $url_para;
                $_url_para['type'] = 'num';
                $view->setRef('url_type_num', $this->getUrl() . '?' . $this->admin_get_url_para($_url_para) . $search_para);
                if ($url_para["type"] == "alpha") {
                    /* get all chars which occur on the begining of the alpha_index string */
                    $db->query('SELECT DISTINCT UPPER(SUBSTRING(' . $TABLE_CONFIG["alpha_index"] . ',1,1)) ' . $TABLE_CONFIG["alpha_index"] . ' FROM ' . $TABLE_CONFIG["table_name"] . $search_sql_query . ' ORDER BY ' . $TABLE_CONFIG["alpha_index"] . ';');
                    $_url_para = $url_para;
                    $_url_para["page"] = 0;
                    $_pagination_alpha = '';

                    foreach ($db->getResult() as $index => $result) {
                        /* set the startvalue */
                        if ($url_para['alpha'] == "") {
                            $url_para['alpha'] = $result[$TABLE_CONFIG["alpha_index"]];
                        }
                        if (strtoupper($url_para['alpha']) == $result[$TABLE_CONFIG["alpha_index"]]) {
                            $_pagination_alpha .= '<li class="active"><span>'.$result[$TABLE_CONFIG["alpha_index"]].'<span class="sr-only">(current)</span></span></li>';
                        } else {
                            $_url_para["alpha"] = $result[$TABLE_CONFIG["alpha_index"]];
                            $_pagination_alpha .= '<li><a href="'.$this->getUrl().'?'.$this->admin_get_url_para($_url_para).$search_para.'">'.$result[$TABLE_CONFIG["alpha_index"]]."</a></li>";
                        }
                    }

                    /* get the number of rows for this startvalue */
                    $total = $this->admin_get_record_number($search, $url_para["alpha"]);
                    $view->setRef('pagination_alpha_index', $TABLE_CONFIG["alpha_index"]);
                    $view->setRef('pagination_alpha', $_pagination_alpha);

                }
            }
        }
        $this->setPagination($total, $search, $url_para);
        $this->setPaginationSelectbox($total, $search, $url_para);

        // set the link for the num_per_pages
        $url_para_num_per_page = $url_para;
        unset($url_para_num_per_page['num_per_page']);
        $url_para_num_per_page['page'] = 1;
        $url_para_num_per_page = $this->getUrl().'?'.$this->admin_get_url_para($url_para_num_per_page);
        $view->setRef('url_para_num_per_page', $url_para_num_per_page);

        $view->setRef('_list_navigation', $view->fetch('admin/_list_navigation.tpl'));

        return $url_para;
    }

    /**
     * Returns a string with hidden fields
     *
     * @param array $url_para
     *
     * @return string
     */
    private function admin_get_search_hidden($url_para = array())
    {
        $html = '';
        
        if (!empty($url_para)) {
            foreach ($url_para as $key => $value) {
                $html .= $this->admin_hidden("form_data[" . $key . "]", $value);
            }   
        }

        return $html;
    }

    /**
     * @param string $name
     * @param string $value
     *
     * @return string
     */
    private function admin_hidden($name, $value)
    {
        return '<input type="hidden" name="' . $name . '" value="' . esc_attr($value) . '">';
    }

    /**
     * Return a url parameter string for the search
     *
     * @param array $search
     * @param string $action
     *
     * @return string
     */
    public function admin_get_search_para($search, $action = '')
    {
        $parameters = array();
        
        if (!empty($search)) {
            foreach ($search as $key => $value) {
                $parameters["form_data[{$key}]"] = $value;
            }
        }

        if (empty($action)) {
            $parameters['action'] = 'search';
        }
        
        return http_build_query($parameters);
    }

    /**
     * Returns a url formed string from an array
     *
     * @deprecated
     * 
     * @param array $parameters
     *
     * @return string
     */
    public function admin_get_url_para($parameters = array())
    {
        return http_build_query($parameters);
    }

    /********************************************************
     * INPUT:
     * url_para    => array mit den url_parametern
     * OUTPUT:
     * input hidden tags
     ********************************************************/
    public function admin_get_hidden($url_para)
    {
        $hidden = '';
        
        foreach ($url_para as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $k => $v) {
                    $hidden .= $this->admin_hidden("{$key}[{$k}]", $v);
                }
            } else {
                $hidden .= $this->admin_hidden($key, $value);
            }
        }

        return $hidden;
    }

    /**
     * Returns an image button.
     * Checks if the current user has the right to see and use the button.
     *
     * @param string $access (the function from main/config/funcdef.inc)
     * @param string $label (label for the alt text)
     * @param string $URL
     * @param string $GET
     * @param string $img (will be extended with getStaticUrl('adm/')
     * @param int $active/inactive button
     * @param int $new_window
     *
     * @return string
     */
    private function button($access, $label, $URL, $GET, $img = "", $active = 1, $new_window = 0)
    {
        $html = '';
        if ($this->container->get('current_user')->checkPrivilege($access)) {
            if ($active < 1) {
                $img_name = explode('.', $img);
                $ext = array_pop($img_name);
                $img = implode($img_name, '.') . '_d.' . $ext;
            } else {
                $html .= '<a href="' . $URL . (($GET != "") ? ('?' . $GET) : ('')) . '"' . ($new_window == 1 ? ' target="_blank"' : '') . '>';
            }
            $html .= '<img src="' . getStaticUrl('adm/' . $img) . '" title="' . $label . '" class="button-action">';
            $html .= ($active != "" ? "</a>" : "");
        }

        return $html;
    }

    /** ***************************************************
     * Returns the media list
     *
     * @param int $id
     * @param array $search
     * @param array $url_para
     *
     * @return string
     */
    public function admin_get_list_gfx($id, $search, $url_para)
    {
        $TABLE_CONFIG = $this->getTableConfig();
        $MENU_ARRAY = $this->getMenu();
        $BUTTON = $this->getButton();

        $db = $this->getDatabase();
        $view = $this->getView();
        $search_sql_query = '';

        // get the primary key
        $tmp = $this->admin_get_primary_key($TABLE_CONFIG["table_name"]);
        $id_field = $tmp["ID_FIELD"];

        $total = $this->admin_get_record_number($search);
        $url_para = $this->admin_check_url_para($url_para, $id_field, $total);

        /* create table with navigation */
        $url_para = $this->admin_set_navigation($url_para, $total, $search);

        /* possible sql querys */
        if (is_array($search)) {
            /* read content */
            $search_para = $this->admin_get_search_para($search);
        } else {
            $search_para = "";
            $search_sql_query = "";
        }
        $_start = (($url_para['page'] - 1) * $url_para['num_per_page']);
        if (is_array($search)) {
            $search_sql_query = $this->admin_get_search_query($search);
        }
        
        if ($url_para["type"] == "alpha") {
            $where = $TABLE_CONFIG["alpha_index"] . ' LIKE "' . ($url_para["alpha"] == "_" ? '\\_' : sqlsave($url_para['alpha'])) . '%"';
        } else {
            $where = '1=1';
        }

        $db->query('SELECT * FROM ' . $TABLE_CONFIG["table_name"] .
                   ' WHERE ' . $where .
                   (!empty($search_sql_query) ? ' AND ' . $search_sql_query : '') .
                   ' ORDER BY ' . $url_para["sort"] . ' ' . $url_para["direction"] .
                   ' LIMIT ' . $_start . ',' . $url_para["num_per_page"] . ';');

        // set action buttons
        $action_buttons = '';
        foreach ($BUTTON as $key => $val) {
            if ($val["th"] == 1) {
                if (isset($BUTTON[$key]["extended_parameters"]) && $BUTTON[$key]["extended_parameters"] == 1) {
                    $extended_parameters = $this->admin_get_search_para($search, 0);
                } else {
                    $extended_parameters = "";
                }
                $action_buttons .= $this->button($BUTTON[$key]["func"], $BUTTON[$key]["alt_text"], $BUTTON[$key]["script"], $this->admin_get_url_para(array_merge($url_para, array('action' => $BUTTON[$key]["action"]))) . $extended_parameters, $BUTTON[$key]["gfx"]);
            }
        }

        $i = 0;
        /* read records */
        while ($db->next_record()) {
            $data[$i] = $db->Record;
            $i++;
        }

        $data_array = array();
        if (isset($data)) {
            // save origins
            $tmp_orig_sort = $url_para["sort"];
            $tmp_orig_direction = $url_para["direction"];

            // create tablecontent
            for ($i = 0; $i < sizeof($data); $i++) {
                // if there is a edit option than create a link for each image
                if (isset($BUTTON["edit"])) {
                    $data_array[$i]['url_edit'] = $BUTTON["edit"]["script"] . '?' . $this->admin_get_url_para(array_merge($url_para, array('id' => $data[$i][$id_field], 'action' => $BUTTON["edit"]["action"])));
                }
                // NOTE: THIS IS BAD, as it exists in one module. It's a HACK
                $data_array[$i]['media'] = $this->module->list_image($data[$i]);

                // get each data row
                $j = 0;
                foreach ($MENU_ARRAY as $key => $val) {
                    /* show only if wanted in $MENU_ARRAY (show=1) */
                    if ($MENU_ARRAY[$key]["show"] == 1) {
                        /* if there is a function for the data than execute and get the data from there */
                        $data_array[$i]['keys'][$j]['content'] = 
                            (isset($MENU_ARRAY[$key]["list_function"]["name"]) && method_exists($this->getModule(), $MENU_ARRAY[$key]["list_function"]["name"])
                                ? call_user_func_array(array($this->getModule(), $MENU_ARRAY[$key]["list_function"]["name"]), !empty($MENU_ARRAY[$key]["list_function"]["para"]) ? array($data[$i]) : array())
                                : htmlentities($data[$i][$key], ENT_COMPAT, $GLOBALS['CONFIG']['SYSTEM_CHARSET']));

                        $url_para["sort"] = $key;
                        $data_array[$i]['keys'][$j]['name'] = $MENU_ARRAY[$key]["name"];
                        // is this item sortable?
                        if ($MENU_ARRAY[$key]["sort"] == 1) {
                            // is that the curent order-property?
                            if ($key == $tmp_orig_sort) {
                                $url_para["direction"] = ($tmp_orig_direction == "ASC") ? "DESC" : "ASC";
                                $data_array[$i]['keys'][$j]['ordered_by_this'] = true;
                            }
                            $data_array[$i]['keys'][$j]['url_order'] = $this->getUrl() . '?' . $this->admin_get_url_para($url_para) . $search_para;
                            $url_para["direction"] = $tmp_orig_direction;
                        }
                        $j++;
                    }
                }
                /* reset backup sort and direction */
                $url_para["sort"] = $tmp_orig_sort;
                $url_para["direction"] = $tmp_orig_direction;

                foreach ($BUTTON as $key => $val) {
                    if ($val["td"] == 1) {
                        // get rid of php notice
                        if (!isset($data_array[$i]['item_buttons'])) {
                            $data_array[$i]['item_buttons'] = '';
                        }
                        $data_array[$i]['item_buttons'] .= $this->button($BUTTON[$key]["func"], $BUTTON[$key]["alt_text"], $BUTTON[$key]["script"], $this->admin_get_url_para(array_merge($url_para, array('id' => $data[$i][$id_field], 'action' => $BUTTON[$key]["action"]))), $BUTTON[$key]["gfx"]);
                    }
                }
            }
        }
        $view->setRef('style_id', 'admin_media');
        $view->setRef('data_array', $data_array);
        $view->setRef('img_order_direction', ($url_para["direction"] == "ASC") ? getStaticUrl('adm/asc.gif') : getStaticUrl('adm/desc.gif'));
        $view->setRef('action_buttons', $action_buttons);

        return $view->fetch('admin/_list_gfx.tpl');
    }

    /** ***************************************************
     * Returns the media list in the iframe version
     *
     * @param id
     * @param search
     * @param url_para
     *
     * @return html
     */
    public function admin_get_list_gfx_iframe($id, $search, $url_para)
    {
        $TABLE_CONFIG = $this->getTableConfig();
        $MENU_ARRAY = $this->getMenu();
        
        $db = $this->getDatabase();
        $view = $this->getView();
        $data_array = array();

        // get the primary key
        $tmp = $this->admin_get_primary_key($TABLE_CONFIG["table_name"]);
        $id_field = $tmp["ID_FIELD"];

        $total = $this->admin_get_record_number($search);
        $url_para = $this->admin_check_url_para($url_para, $id_field, $total);

        /* create table with navigation */
        $url_para = $this->admin_set_navigation($url_para, $total, $search);

        /* possible sql querys */
        if (is_array($search)) {
            /* read content */
            $search_para = $this->admin_get_search_para($search);
        } else {
            $search_para = "";
            $search_sql_query = "";
        }

        $_start = (($url_para['page'] - 1) * $url_para['num_per_page']);
        if ($url_para["type"] == "alpha") {
            if (is_array($search)) {
                $search_sql_query = ' AND ' . $this->admin_get_search_query($search);
            }
            $db->query('SELECT * FROM ' . $TABLE_CONFIG["table_name"] . ' WHERE ' . $TABLE_CONFIG["alpha_index"] . ' like "' . ($url_para["alpha"] == "_" ? '\\_' : sqlsave($url_para["alpha"])) . '%"' . $search_sql_query . ' ORDER BY ' . $url_para["sort"] . ' ' . $url_para["direction"] . ' LIMIT ' . $_start . ',' . $url_para["num_per_page"] . ';');
        } else {
            // numeric navigation
            if (is_array($search)) {
                $search_sql_query = ' WHERE ' . $this->admin_get_search_query($search);
            }
            $db->query('SELECT * FROM ' . $TABLE_CONFIG["table_name"] . $search_sql_query . ' ORDER BY ' . $url_para["sort"] . ' ' . $url_para["direction"] . ' LIMIT ' . $_start . ',' . $url_para["num_per_page"] . ';');
        }

        $i = 0;
        /* read records */
        while ($db->next_record()) {
            $data[$i] = $db->Record;
            $i++;
        }
        if (isset($data)) {
            // save origins
            $tmp_orig_sort = $url_para["sort"];
            $tmp_orig_direction = $url_para["direction"];

            // create tablecontent
            for ($i = 0; $i < sizeof($data); $i++) {
                // insert the media image
                $data_array[$i]['media'] = list_image($data[$i]);
                // insert the id
                $_id = $data[$i]['id'];
                $data_array[$i]['id'] = $_id;
                $_media = new Media($_id);
                $data_array[$i]['url'] = $_media->getUrl();

                // get each data row
                $j = 0;
                foreach ($MENU_ARRAY as $key => $val) {
                    /* show only if wanted in $MENU_ARRAY (show=1) */
                    if ($MENU_ARRAY[$key]["show_iframe"] == 1) {
                        /* if there is a function for the data than execute and get the data from there */
                        $data_array[$i]['keys'][$j]['content'] = 
                            (isset($MENU_ARRAY[$key]["list_function"]["name"]) && method_exists($this->getModule(), $MENU_ARRAY[$key]["list_function"]["name"])
                                ? call_user_func_array(array($this->getModule(), $MENU_ARRAY[$key]["list_function"]["name"]), !empty($MENU_ARRAY[$key]["list_function"]["para"]) ? array($data[$i]) : array())
                                : htmlentities($data[$i][$key], ENT_COMPAT, $GLOBALS['CONFIG']['SYSTEM_CHARSET']));

                        $url_para["sort"] = $key;
                        $data_array[$i]['keys'][$j]['name'] = $MENU_ARRAY[$key]["name"];
                        // is this item sortable?
                        if ($MENU_ARRAY[$key]["sort"] == 1) {
                            // is that the curent order-property?
                            if ($key == $tmp_orig_sort) {
                                $url_para["direction"] = ($tmp_orig_direction == "ASC") ? "DESC" : "ASC";
                                $data_array[$i]['keys'][$j]['ordered_by_this'] = true;
                            }
                            $data_array[$i]['keys'][$j]['url_order'] = $this->getUrl() . '?' . $this->admin_get_url_para($url_para) . $search_para;
                            $url_para["direction"] = $tmp_orig_direction;
                        }
                        $j++;
                    }
                }
                /* reset backup sort and direction */
                $url_para["sort"] = $tmp_orig_sort;
                $url_para["direction"] = $tmp_orig_direction;
            }
        }
        $view->setRef('style_id', 'admin_media_iframe');
        $view->setRef('data_array', $data_array);
        $view->setRef('img_order_direction', ($url_para["direction"] == "ASC") ? getStaticUrl('adm/asc.gif') : getStaticUrl('adm/desc.gif'));

        // $view->setRef('action_buttons',$action_buttons);
        return $view->fetch('admin/_list_gfx_iframe.tpl');
    }

    /********************************************************
     * INPUT:
     * action        => aktion nach der die form erzeugt wird
     * id                => spaltenwert der sql-spalte mit dem primaerschluessel, muss ein eindeutiger wert sein
     * values        => array, mit den werten einer vorherigen form, die wegen fehlern oder einer suche noch einmal in die neue form eingetragen werden
     * url_para    => array mit allen parameter die mit der url uebergeben werden -> call_by_reference
     * OUTPUT:
     * HTML            => html formular
     * COMMENT:
     * je nach action(new,edit,search) wird ein forumlar erzeugt. bei fehlern oder bei der suche, wird das formular nocheinmal, mit den bisherigen werten dargestellt
     ********************************************************/
    public function admin_get_form($action, $id, $form_data, $url_para, $customDataTemplate = '')
    {
        $TABLE_CONFIG = $this->getTableConfig();
        $BUTTON = $this->getButton();
        
        $db = $this->getDatabase();
        $view = $this->getView();
        $translator = $this->getTranslator();

        $tmp = $this->admin_get_primary_key($TABLE_CONFIG["table_name"]);
        $id_field = $tmp["ID_FIELD"];
        $metadata = $tmp["METADATA"];
        $url_para = $this->admin_check_url_para($url_para, $id_field);

        /**
         * Set action depending header and button texts
         */
        if ($action == "edit" && $id != "" && $id != "0" && $id != null) {
            $db->query("SELECT * FROM " . $TABLE_CONFIG["table_name"] . " WHERE " . $id_field . "='" . sqlsave($id) . "';");
            if ($db->next_record()) {
                $_values = $db->Record;
            }
            // merge with the values from the form
            $form_data = array_merge($_values, (array)$form_data);
            $view->setRef('hidden_id', $form_data[$id_field]);
            $view->setRef('header', $translator->trans('edit_item'));
            $view->setRef('text_submit', $translator->trans('save'));
            $form_data["id"] = $id;
            $form_data["id_field"] = $id_field;
        } elseif ($action == "new") {
            $view->setRef('header', $translator->trans('new_item'));
            $view->setRef('text_submit', $translator->trans('save'));
        } elseif ($action == "search") {
            $view->setRef('header', $translator->trans('search'));
            $view->setRef('text_submit', $translator->trans('start_search'));
            // populate $this->searched_keys
            $this->admin_get_search_query($form_data);
        } else {
            // all other "actions" defined in $BUTTON:
            $db->query("SELECT * FROM " . $TABLE_CONFIG["table_name"] . " WHERE " . $id_field . "='" . sqlsave($id) . "';");
            if ($db->next_record()) {
                $_values = $db->Record;
            }
            // merge with the values from the form
            $form_data = array_merge((array)$_values, (array)$form_data);
            $view->setRef('hidden_id', $form_data[$id_field]);
            if (isset($BUTTON[$action]['text_header'])) {
                $view->setRef('header', $BUTTON[$action]['text_header']);
            } else {
                $view->setRef('header', $translator->trans('edit_item'));
            }
            if (isset($BUTTON[$action]['text_submit'])) {
                $view->setRef('text_submit', $BUTTON[$action]['text_submit']);
            } else {
                $view->setRef('text_submit', $translator->trans('save'));
            }
            $form_data["id"] = $id;
            $form_data["id_field"] = $id_field;
        }
        $view->setRef('text_cancel', $translator->trans('cancel'));
        $view->setRef('hidden_values', $this->admin_get_hidden($url_para));

        /**
         * Set data array depending on action key
         */
        $data_array = $this->admin_get_form_data_array($action, $id, $form_data);
        $view->setRef('data_array',$data_array);

        $view->setRef('action',$action);
        $view->setRef('url_form_action', $this->getUrl());
        $view->setRef('url_cancel', $this->getUrl() . '?' . $this->admin_get_url_para(array_merge($url_para, array('actionlist' => 'list'))));
        if (!empty($customDataTemplate)) {
            $view->setRef('form_data_table', $view->fetch($customDataTemplate));
        }
        return $view->fetch('admin/_form.tpl');
    }

    /**
     * @param $action
     * @param $id
     * @param $form_data
     * @return array
     */
    function admin_get_form_data_array($action, $id, $form_data) {
        $app = Application::getInstance();
        $view = $app->getView();

        $MENU_ARRAY = $this->getMenu();
        $TABLE_CONFIG = $this->getTableConfig();

        $tmp = $this->admin_get_primary_key($TABLE_CONFIG["table_name"]);
        $id_field = $tmp["ID_FIELD"];
        $metadata = $tmp["METADATA"];

        /**
         * Set data array depending on action key
         */
        $data_array = array();
        $i = 0;
        foreach ($MENU_ARRAY as $key=>$val) {
            if ($MENU_ARRAY[$key][$action] == 1) {
                $data_array[$i]['key'] = $key;
                $data_array[$i]['name'] = $MENU_ARRAY[$key]["name"];
                if (isset($MENU_ARRAY[$key][$action."_show_th"]) && $MENU_ARRAY[$key][$action . "_show_th"] == 0) {
                    $data_array[$i]['show_th'] = false;
                } else {
                    $data_array[$i]['show_th'] = true;
                }

                if ($key == $id_field && $action == "edit")
                    $view->setRef('id_field_old',$id_field.'_old');

                // hide non searched keys in search mode
                if ($action == 'search' && is_array($this->searched_keys) && !empty($this->searched_keys)) {
                    if (!isset($this->searched_keys[$key])) {
                        $data_array[$i]['hide'] = true;
                        $view->setRef('hide_non_searched_keys',true);
                    }
                }
                /*
                 * Set the value for the (input) field
                 */
                $init_value = "";
                if (isset($MENU_ARRAY[$key]["init_value"]) && $action == "new") {
                    $init_value = $MENU_ARRAY[$key]["init_value"];
                }
                if (isset($MENU_ARRAY[$key]["form_value"])) {
                    $tmp_value = $MENU_ARRAY[$key]["form_value"];
                } elseif (isset($MENU_ARRAY[$key]["form_function"]["name"]) && method_exists($this->getModule(), $MENU_ARRAY[$key]["form_function"]["name"])) {
                    /* insert id and id_field if wanted */
                    $tmp_value = call_user_func_array(array($this->getModule(), $MENU_ARRAY[$key]["form_function"]["name"]), !empty($MENU_ARRAY[$key]["form_function"]["para"]) ? array($form_data) : array());
                } else {
                    $tmp_value = isset($form_data[$key]) ? $form_data[$key] : $init_value;
                }
                // get the right form input type
                if (isset($MENU_ARRAY[$key]["form_input"])) {
                    $tmp_form_input_optional = (isset($MENU_ARRAY[$key]["form_input"]["optional"]) ? $MENU_ARRAY[$key]["form_input"]["optional"] : "");
                    $tmp_form_input_values = (isset($MENU_ARRAY[$key]["form_input"]["values"]) ? $MENU_ARRAY[$key]["form_input"]["values"] : "");
                    if (isset($MENU_ARRAY[$key]["form_input"]["template"])) {
                        // if the template is set than use that
                        $data_array[$i]['content'] = $this->admin_template($MENU_ARRAY[$key]["form_input"]["template"], $key, $tmp_value, $tmp_form_input_values, $tmp_form_input_optional, $form_data, $action, $metadata);
                    } else {
                        // otherwise take the type (which uses the default templates)
                        $data_array[$i]['content'] = $this->admin_input($MENU_ARRAY[$key]["form_input"]["type"], $key, $tmp_value, $tmp_form_input_values, $tmp_form_input_optional, $form_data, $action, $metadata);
                    }
                } else {
                    // default (textfield)
                    $data_array[$i]['content'] = $this->admin_input("text", $key, $tmp_value, "", ' size="50"', $form_data, $action, $metadata);
                }
                $i++;
            } elseif ($key == $id_field && $MENU_ARRAY[$key]["edit"] == 0 && $action == "edit") {
                // add the hidden field in edit mode
                $data_array[$i]['hidden'] = $this->admin_hidden("form_data[".$id_field."]",$id);
                $i++;
            }
        }
        return $data_array;
    }

    /**
     *
     * @param template
     * @param name
     * @param value
     * @param selectable_values => array oder einzelwert mit den einzuziehenden namen und werten die gesendet werden
     * @param optional - optional attrs (size,maxlength,cols...)
     * @param row - form_data
     * @param actionaction
     * @param metadata
     *
     * @return html
     */
    public function admin_template($template, $name, $value, $selectable_values, $optional)
    {
        $view = $this->getView();

        $form_data['name'] = $name;
        $form_data['value_raw'] = $value;
        $form_data['value'] = esc_html($value);
        $form_data['selectable_values'] = $selectable_values;
        $form_data['optional'] = $optional;
        $form_data['message'] = $view->getHtmlFormMessages('admin_message_' . $name, 'field');
        $view->setRef('form_data', $form_data);

        return $view->fetch($template);
    }

    /********************************************************
     * INPUT:
     * type        => name des gewuenschten input-tags
     * name        => name der spaeteren variable
     * value       => startwert der eine vorselektierung ermoeglicht
     * opt         => optionale parameter z.B. size,maxlength,cols....
     * selectable_values => array oder einzelwert mit den einzuziehenden namen und werten die gesendet werden
     * row         => form_data
     * action      => action
     * metadata    => array der metadata der aktuellen tabelle
     * OUTPUT:
     * input       => input tag
     * COMMENT:
     * ausgabe eines input-tags je nach vorgegebenen type
     ********************************************************/
    private function admin_input($type, $name, $value, $selectable_values, $optional, $row, $action, $metadata)
    {
        $MENU_ARRAY = $this->getMenu();

        $_html = "";
        switch ($type) {
            case "text":
                if ($action == "search") {
                    $_html = $this->admin_input_search_select($name, $row);
                    $_html .= '</td><td>';
                    $_html .= $this->admin_template_search('admin/_form_text.tpl', $name, $value, $selectable_values, $optional, $row, $action, $metadata);
                } else {
                    $_html = $this->admin_template('admin/_form_text.tpl', $name, $value, $selectable_values, $optional, $row, $action, $metadata);
                }
                break;
            case "datetime":
                if ($action == "search") {
                    $_html = $this->admin_input_search_select($name, $row);
                    $_html .= '</td><td>';
                    $_html .= $this->admin_template_search('admin/_form_datetime.tpl', $name, $value, $selectable_values, $optional, $row, $action, $metadata);
                } else {
                    $_html .= $this->admin_template('admin/_form_datetime.tpl', $name, $value, $selectable_values, $optional, $row, $action, $metadata);
                }
                break;
            case "date":
                if ($action == "search") {
                    $_html = $this->admin_input_search_select($name, $row);
                    $_html .= '</td><td>';
                    $_html .= $this->admin_template_search('admin/_form_date.tpl', $name, $value, $selectable_values, $optional, $row, $action, $metadata);
                } else {
                    $_html .= $this->admin_template('admin/_form_date.tpl', $name, $value, $selectable_values, $optional, $row, $action, $metadata);
                }
                break;
            case "textarea":
                if ($action == "search") {
                    $_html = $this->admin_input_search_select($name, $row);
                    $_html .= '</td><td>';
                    $_html .= $this->admin_template_search('admin/_form_text.tpl', $name, $value, $selectable_values, $optional, $row, $action, $metadata);
                } else {
                    $_html .= $this->admin_template('admin/_form_textarea.tpl', $name, $value, $selectable_values, $optional, $row, $action, $metadata);
                }
                break;
            case "editor_tinymce":
                if ($action == "search") {
                    $_html = '</td><td>';
                    $_html .= $this->admin_template('admin/_form_text.tpl', $name, $value, $selectable_values, $optional, $row, $action, $metadata);
                } else {
                    $value = wp_richedit_pre($value);
                    $this->getView()->set('editor_id', 'content');
                    $_html .= $this->admin_template('admin/_form_editor_tinymce.tpl', $name, $value, $selectable_values, $optional, $row, $action, $metadata);
                }
                break;
            case "radio":
                foreach ($selectable_values as $key => $val) {
                    $_html .= '<input type="radio" name="form_data[' . $name . ']" value="' . $key . '"' . $optional . ($value == $key ? " checked" : "") . '> ' . $val . '<br>';
                }
                if ($action == "search") {
                    $_html = '<div style="text-align: right;"><input type="checkbox" name="form_data[' . $name . '_SEARCH_STYLE]" value="1"' . $optional .
                             (isset($row[$name . "_SEARCH_STYLE"]) && $row[$name . "_SEARCH_STYLE"] == "1" ? " checked" : "") . ' OnClick="
						if (this.checked){
							$(\'#' . $name . 'panel1\').show();
						}else{
							$(\'#' . $name . 'panel1\').hide();
						}"></div></td><td>
						<div id="' . $name . 'panel1" style="display: ' . (isset($row[$name . "_SEARCH_STYLE"]) && $row[$name . "_SEARCH_STYLE"] == "1" ? "block" : "none") . '">' . $_html . '</div>';
                }
                break;
            case "checkbox":
                foreach ($selectable_values as $key => $val) {
                    $_html .= '<input type="checkbox" name="form_data[' . $name . ']" value="' . $key . '"' . $optional . ($value == $key ? " checked" : "") . '> ' . $val . '<br>';
                }
                if ($action == "search") {
                    $_html = '<div style="text-align: right;"><input type="checkbox" name="form_data[' . $name . '_SEARCH_STYLE]" value="1"' . $optional .
                             (isset($row[$name . "_SEARCH_STYLE"]) && $row[$name . "_SEARCH_STYLE"] == "1" ? " checked" : "") . ' OnClick="
						if (this.checked){
							$(\'#' . $name . 'panel1\').show();
						}else{
							$(\'#' . $name . 'panel1\').hide();
						}"></div></td><td>
						<div id="' . $name . 'panel1" style="display: ' . (isset($row[$name . "_SEARCH_STYLE"]) && $row[$name . "_SEARCH_STYLE"] == "1" ? "block" : "none") . '">' . $_html . '</div>';
                }
                break;
            case "select":
                $_html = '<select id="form_data_' . $name . '" name="form_data[' . $name . ']"' . $optional . '>';
                foreach ($selectable_values as $key => $val) {
                    $_html .= '<option value="' . $key . '"' . ($value == $key ? ' selected="selected"' : "") . '>' . $val . '</option>';
                }
                $_html .= '</select>';
                if ($action == "search") {
                    $_html = '<div style="text-align: right;"><input type="checkbox" name="form_data[' . $name . '_SEARCH_STYLE]" value="1"' . $optional .
                             (isset($row[$name . "_SEARCH_STYLE"]) && $row[$name . "_SEARCH_STYLE"] == "1" ? ' checked="checked"' : "") . ' OnClick="
						if (this.checked){
							$(\'#' . $name . 'panel1\').show();
						}else{
							$(\'#' . $name . 'panel1\').hide();
						}"></div></td><td>
						<div id="' . $name . 'panel1" style="display: ' . (isset($row[$name . "_SEARCH_STYLE"]) && $row[$name . "_SEARCH_STYLE"] == "1" ? "block" : "none") . '">' . $_html . '</div>';
                }
                break;
            case "button":
                if ($action == "search") {
                    $_html = '</td><td>';
                }
                $_html .= '<input type="button" name="form_data[' . $name . ']" value="' . $value . '"' . $optional . '>';
                break;
            case "file":
                if ($action == "search") {
                    $_html = '</td><td>';
                }
                $_html .= '<input type="file" name="form_data[' . $name . ']"' . $optional . '>';
                break;
            case "password":
                if ($action == "search") {
                    $_html = $this->admin_input_search_select($name, $row);
                    $_html .= '</td><td>';
                    $_html .= $this->admin_template_search('admin/_form_password.tpl', $name, $value, $selectable_values, $optional, $row, $action, $metadata);
                } else {
                    $_html = $this->admin_template('admin/_form_password.tpl', $name, $value, $selectable_values, $optional, $row, $action, $metadata);
                }
                break;
            case "function":
                if ($action == "search") {
                    $_html = $this->admin_input_search_select($name, $row);
                    $_html .= '</td><td>';
                    $_html .= $this->admin_template_search('admin/_form_text.tpl', $name, $value, $selectable_values, $optional, $row, $action, $metadata);
                } elseif (method_exists($this->getModule(), $MENU_ARRAY[$name]["form_input"]["name"])) {
                    $_html .= call_user_func_array(array($this->getModule(), $MENU_ARRAY[$name]["form_input"]["name"]), !empty($MENU_ARRAY[$name]["form_input"]["para"]) ? array($row) : array());
                }
                break;
            case "none":
            case "none_htmlentities":
                if ($action == "search") {
                    $_html = $this->admin_input_search_select($name, $row);
                    $_html .= '</td><td>';
                    $_html .= $this->admin_template_search('admin/_form_text.tpl', $name, $value, $selectable_values, $optional, $row, $action, $metadata);
                } else {
                    if ($type == 'none_htmlentities') {
                        $_html .= htmlentities($value);
                    } else {
                        $_html .= $value;
                    }
                }
                break;
            case "optional":
                if ($action == "search") {
                    $_html = $this->admin_input_search_select($name, $row);
                    $_html .= '</td><td>';
                    $_html .= $this->admin_template_search('admin/_form_text.tpl', $name, $value, $selectable_values, $optional, $row, $action, $metadata);
                } else {
                    $_html .= $optional;
                }
                break;
        }

        return $_html;
    }

    /**
     * Returns the search select box
     *
     * @param name
     * @param row
     *
     * @return html
     */
    private function admin_input_search_select($name, $row)
    {
        return '<select name="form_data[' . $name . '_SEARCH_STYLE]" size="1" OnChange="if (this.value==' . self::SEARCH_STYLE_BETWEEN . ') {
				$(\'#' . $name . 'panel2\').show();
			} else {
				$(\'#' . $name . 'panel2\').hide();
			}
			if (this.value==' . self::SEARCH_STYLE_EMPTY . ' || this.value==' . self::SEARCH_STYLE_NOTEMPTY . ') {
				$(\'#' . $name . 'panel1\').hide();
			} else {
				$(\'#' . $name . 'panel1\').show();
			}
			">
		<option value="' . self::SEARCH_STYLE_EQ . '"' . (isset($row[$name . "_SEARCH_STYLE"]) && $row[$name . "_SEARCH_STYLE"] == self::SEARCH_STYLE_EQ ? ' selected="selected"' : "") . '>=</option>
		<option value="' . self::SEARCH_STYLE_GT . '"' . (isset($row[$name . "_SEARCH_STYLE"]) && $row[$name . "_SEARCH_STYLE"] == self::SEARCH_STYLE_GT ? ' selected="selected"' : "") . '>&gt;</option>
		<option value="' . self::SEARCH_STYLE_GTOEQ . '"' . (isset($row[$name . "_SEARCH_STYLE"]) && $row[$name . "_SEARCH_STYLE"] == self::SEARCH_STYLE_GTOEQ ? ' selected="selected"' : "") . '>&gt;=</option>
		<option value="' . self::SEARCH_STYLE_LT . '"' . (isset($row[$name . "_SEARCH_STYLE"]) && $row[$name . "_SEARCH_STYLE"] == self::SEARCH_STYLE_LT ? ' selected="selected"' : "") . '>&lt;</option>
		<option value="' . self::SEARCH_STYLE_LTOEQ . '"' . (isset($row[$name . "_SEARCH_STYLE"]) && $row[$name . "_SEARCH_STYLE"] == self::SEARCH_STYLE_LTOEQ ? ' selected="selected"' : "") . '>&lt;=</option>
		<option value="' . self::SEARCH_STYLE_NOT . '"' . (isset($row[$name . "_SEARCH_STYLE"]) && $row[$name . "_SEARCH_STYLE"] == self::SEARCH_STYLE_NOT ? ' selected="selected"' : "") . '>!=</option>
		<option value="' . self::SEARCH_STYLE_EMPTY . '"' . (isset($row[$name . "_SEARCH_STYLE"]) && $row[$name . "_SEARCH_STYLE"] == self::SEARCH_STYLE_EMPTY ? ' selected="selected"' : "") . '>EMPTY</option>
		<option value="' . self::SEARCH_STYLE_NOTEMPTY . '"' . (isset($row[$name . "_SEARCH_STYLE"]) && $row[$name . "_SEARCH_STYLE"] == self::SEARCH_STYLE_NOTEMPTY ? ' selected="selected"' : "") . '>NOT EMPTY</option>
		<option value="' . self::SEARCH_STYLE_LIKE . '"' . (isset($row[$name . "_SEARCH_STYLE"]) && $row[$name . "_SEARCH_STYLE"] == self::SEARCH_STYLE_LIKE ? ' selected="selected"' : "") . '>LIKE</option>
		<option value="' . self::SEARCH_STYLE_BETWEEN . '"' . (isset($row[$name . "_SEARCH_STYLE"]) && $row[$name . "_SEARCH_STYLE"] == self::SEARCH_STYLE_BETWEEN ? ' selected="selected"' : "") . '>BETWEEN</option></select>';
    }

    /**
     * admin_template wrapper for search type select
     *
     * @param <see admin_template>
     *
     * @return html
     */
    private function admin_template_search($template, $name, $value, $selectable_values, $optional, $row, $action, $metadata)
    {
        return '
		<div id="' . $name . 'panel1" style="display: ' . (isset($row[$name . "_SEARCH_STYLE"]) && ($row[$name . "_SEARCH_STYLE"] == self::SEARCH_STYLE_EMPTY || $row[$name . "_SEARCH_STYLE"] == self::SEARCH_STYLE_NOTEMPTY) ? "none" : "block") . '">' .
               $this->admin_template($template, $name, $value, $selectable_values, $optional, $row, $action, $metadata) .
               '</div>
               <div id="' . $name . 'panel2" style="display: ' . (isset($row[$name . "_SEARCH_STYLE"]) && $row[$name . "_SEARCH_STYLE"] == self::SEARCH_STYLE_BETWEEN ? "block" : "none") . '">' .
               $this->admin_template($template, $name . '_SEARCH_SECOND', $value, $selectable_values, $optional, $row, $action, $metadata) .
               '</div>';
    }

    /********************************************************
     * INPUT:
     * id                => spaltenwert der sql-spalte mit dem primaerschluessel, muss ein eindeutiger wert sein
     * url_para    => array mit allen parameter die mit der url uebergeben werden -> call_by_reference
     * OUTPUT:
     * HTML            => html-tabelle mit dem zu loeschenden datensatz
     * COMMENT:
     * anzeige des zu loeschenden datensatz, mit abfrage auf loeschen
     ********************************************************/
    public function admin_get_confirm_table($id, $url_para, $action = 'delete', $template = 'admin/_confirm.tpl')
    {
        $TABLE_CONFIG = $this->getTableConfig();
        $MENU_ARRAY = $this->getMenu();
        
        $db = $this->getDatabase();
        $view = $this->getView();
        $data_array = array();

        $tmp = $this->admin_get_primary_key($TABLE_CONFIG["table_name"]);
        $id_field = $tmp["ID_FIELD"];
        $metadata = $tmp["METADATA"];

        $url_para = $this->admin_check_url_para($url_para, $id_field);

        $db->query("SELECT * FROM {$TABLE_CONFIG['table_name']} WHERE {$id_field}='{$id}';");
        /* create tablecontent */
        if ($db->next_record()) {
            $i = 0;
            foreach ($metadata as $key => $val) {
                if (
                    isset($val["Field"])
                    && isset($MENU_ARRAY[$val["Field"]]["show"]) && ($MENU_ARRAY[$val["Field"]]["show"] == 1)
                    && isset($MENU_ARRAY[$val["Field"]]["sql"]) && ($MENU_ARRAY[$val["Field"]]["sql"] == 1)
                ) {
                    $data_array[$i]['key'] = $MENU_ARRAY[$val["Field"]]["name"];
                    $data_array[$i]['value'] = $db->Record[$metadata[$key]["Field"]];
                    $i++;
                }
            }

            $view->setRef('id_field', $db->Record[$id_field]);
            $view->setRef('hidden_values', $this->admin_get_hidden($url_para));
            $view->setRef('action', $action);
            $view->setRef('data_array', $data_array);
            $view->setRef('url_form_action', $this->getUrl());
            $view->setRef('url_cancel', $this->getUrl() . '?' . $this->admin_get_url_para(array_merge($url_para, array('action' => 'list'))));

            return $view->fetch($template);
        }

        return '';
    }

    /**
     * Inserts a new item
     *
     * @param array $insert_data
     *
     * @return int
     */
    public function admin_set_new($insert_data)
    {
        $TABLE_CONFIG = $this->getTableConfig();
        $MENU_ARRAY = $this->getMenu();

        $db = $this->getDatabase();

        //primaerschluessel herausfinden
        $tmp = $this->admin_get_primary_key($TABLE_CONFIG["table_name"]);
        $id_field = $tmp["ID_FIELD"];
        $metadata = $tmp["METADATA"];

        for ($i = 0; $i < $metadata["num_fields"]; $i++) {
            if ($metadata[$i]["Field"] == $id_field) {
                break;
            }
        }
        //primaerschluessel ist keine unique ID sondern, muss auf vorhanden sein geprueft werden
        if (!strstr($metadata[$i]["Extra"], "auto_increment")) {
            //wenn dieses feld nicht ausgefuellt wurde,abbrechen und fehlermeldung ausgeben
            if ($insert_data[$id_field] == "") {
                return -2;
            }
            $db->query("SELECT " . $id_field . " FROM " . $TABLE_CONFIG["table_name"] . " WHERE " . $id_field . "='" . $insert_data[$id_field] . "';");
            if ($db->num_rows() > 0) { //primaerschluessel bereits vorhanden,abbruch und fehlermeldung
                return -1;
            }
        }

        $cols = array();
        $values = array();
        foreach ($insert_data as $col => $value) {
            if (!empty($MENU_ARRAY[$col]["sql"])) {
                $cols[] = $col;
                $values[] = sqlsave($value);
            }
        }
        
        $db->query("INSERT INTO {$TABLE_CONFIG['table_name']} (" . implode(',', $cols) . ") VALUES ('" . implode("','", $values) . "');");
        $id = $db->insert_id();

        return $id;
    }

    /**
     * Updates an item
     *
     * @param int $id
     * @param array $update_data
     *
     * @return int affected rows
     */
    public function admin_set_update($id, $update_data)
    {
        $TABLE_CONFIG = $this->getTableConfig();
        $MENU_ARRAY = $this->getMenu();

        $db = $this->getDatabase();

        //primaerschluessel herausfinden
        $tmp = $this->admin_get_primary_key($TABLE_CONFIG["table_name"]);
        $id_field = $tmp["ID_FIELD"];
        $metadata = $tmp["METADATA"];

        for ($i = 0; $i < $metadata["num_fields"]; $i++) {
            if ($metadata[$i]["Field"] == $id_field) {
                break;
            }
        }

        $values = array();
        foreach ($update_data as $key => $value) {
            if (!empty($MENU_ARRAY[$key]["sql"])) {
                $values[] = "{$key}='" . sqlsave($value) . "'";
            }
        }

        $db->query("UPDATE {$TABLE_CONFIG['table_name']} SET " . implode(',', $values) . " WHERE {$id_field}='{$id}';");

        return ($db->query_id() !== false);
    }

    /**
     * Deletes an item from the db
     *
     * @param int $id
     *
     * @return int
     */
    public function admin_set_delete($id)
    {
        $db = $this->getDatabase();
        $table_config = $this->getTableConfig();

        $meta = $this->admin_get_primary_key($table_config['table_name']);
        $id_column = $meta['ID_FIELD'];

        $db->query("DELETE FROM {$table_config['table_name']} WHERE {$id_column} ='{$id}';");

        return $db->affected_rows();
    }
}