<?php

namespace Arzttermine\Media;

use Arzttermine\Application\Application;

class Media {

    var $id;

    var $owner_id;

// owner_type: 0:User|1:Institution
    var $owner_type;

// parent_id: id to media with size=0=orig
    var $parent_id;

// size: 0:max size (400x300)|1:thumbnail
    var $size;

    var $filesize;

    var $width;

    var $height;

    var $description;

    var $filename;

    var $content_type;

    var $created_at;

    var $created_by;

    var $updated_at;

    var $updated_by;

// just the dir on the fs
    var $dirpath;

// dir and filename on the fs
    var $filepath;

// relative path to the media
    var $url;

    var $all_keys
        = array(
            "owner_id",
            "owner_type",
            "parent_id",
            "size",
            "filesize",
            "width",
            "height",
            "description",
            "filename",
            "content_type",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by"
        );

    var $secure_keys
        = array(
            "description",
            "filename",
            "content_type",
        );

// ********************************************************
    function __construct($id = 0)
    {
        $this->id = $this->load($id);
    }

// ********************************************************
    function debug()
    {
        echo '*******************************<br>';
        echo 'id:' . $this->id . "<br>";
        foreach ($this->all_keys as $value) {
            echo $value . ':' . $this->$value . "<br>";
        }
        echo 'dirpath:' . $this->dirpath . "<br>";
        echo 'filepath:' . $this->filepath . "<br>";
        echo 'url:' . $this->url . "<br>";
        echo '*******************************<br>';
    }

// ********************************************************
// OUT: true | false
    function valid()
    {
        if ($this->id > 0) return true;
        else {
            return false;
        }
    }
// ********************************************************
// OUT: 0=Error => no login
// OUT: >0=id
    function load($id)
    {
        $db = Application::getInstance()->getSql();

        if (!is_numeric($id) || $id == 0) return 0;
        $sql = "SELECT * FROM " . DB_TABLENAME_MEDIAS . " WHERE id=" . $id;
        $db->query($sql);
        if ($db->nf() == 1) {
            $db->next_record();
            foreach ($db->Record as $key => $value) {
                $this->$key = $value;
            }
            $this->calcVars();

            return $id;
        } else {
            return 0;
        }
    }
    
    public function getUrl()
    {
        if (isset($GLOBALS['CONFIG']['MEDIA_CATEGORIES'][$this->category]['url_path'])) {
            $this->url = $GLOBALS['CONFIG']['MEDIA_CATEGORIES'][$this->category]['url_path'];
        }
        
        return $this->url . $this->filename;
    }

// ********************************************************
// copy a file from fs to a new destination
// OUT: 0=Error
// OUT: >0=new id
    function copyNewFsMedia($data, $from_filepath, $to_filename)
    {
        $db = Application::getInstance()->getSql();

        $sql = "INSERT INTO " . DB_TABLENAME_MEDIAS . " SET ";
        $i = 0;
        foreach ($data as $key => $value) {
            if ($i > 0) $sql .= ', ';
            if (in_array($key, $this->secure_keys)) {
                $value = sqlsave($value);
            }
            $sql .= $key . "='" . $value . "'";
            $i++;
            $this->$key = $value;
        }
        if ($db->query($sql)) {
            $this->id = $db->insert_id();
            // set the new filename
            $this->filename = $to_filename;
            $sql = "UPDATE " . DB_TABLENAME_MEDIAS . " SET filename='" . $this->filename . "' WHERE id=" . $this->id . ";";
            $db->query($sql);
            // set url and dirpath
            $this->loadPath();
            if (!copy($from_filepath, $this->filepath)) {
                $this->delete();

                return 0;
            }
            $this->load($this->id);

            return $this->id;
        } else {
            return 0;
        }
    }

// ********************************************************
// save an uploaded file
// OUT: 0=Error
// OUT: >0=new id
    function saveNewUpload($data, $from_filepath, $to_filename)
    {
        $db = Application::getInstance()->getSql();

        $sql = "INSERT INTO " . DB_TABLENAME_MEDIAS . " SET ";
        $i = 0;
        foreach ($data as $key => $value) {
            if ($i > 0) $sql .= ', ';
            if (in_array($key, $this->secure_keys)) {
                $value = sqlsave($value);
            }
            $sql .= $key . "='" . $value . "'";
            $i++;
            $this->$key = $value;
        }
        if ($db->query($sql)) {
            $this->id = $db->insert_id();
            // set the filename: "id.uid.extension"
            // get the extension
            $_file_ext = strtolower(strrchr($to_filename, '.'));
            // get the uid
            $_uid = Application::getInstance()->createUid($GLOBALS['CONFIG']['MEDIA_CHARS_UID_POSTFIX']);
            $this->filename = $this->id . '_' . $_uid . $_file_ext;
            $sql = "UPDATE " . DB_TABLENAME_MEDIAS . " SET filename='" . $this->filename . "' WHERE id=" . $this->id . ";";
            $db->query($sql);
            // set url and dirpath
            $this->loadPath();
            if (!move_uploaded_file($from_filepath, $this->filepath)) {
                $this->delete();

                return 0;
            }
            $this->load($this->id);

            return $this->id;
        } else {
            return 0;
        }
    }

// ********************************************************
// OUT: 0=Error
// OUT: >0=OK
    function save($keys = 0)
    {
        $db = Application::getInstance()->getSql();

        if ($keys == 0) $keys = $this->all_keys;
        elseif (!is_array($keys)) {
            // if its just one key (as a string)
            $keys = array($keys);
        }

        $sql = "UPDATE " . DB_TABLENAME_MEDIAS . " SET ";
        $i = 0;
        foreach ($keys as $key) {
            if ($i > 0) $sql .= ', ';
            if (in_array($key, $this->secure_keys)) {
                $value = sqlsave($this->$key);
            } else {
                $value = $this->$key;
            }
            $sql .= $key . "='" . $value . "'";
            $i++;
        }
        $sql .= "WHERE id=" . $this->id;

        return $db->query($sql);
    }

// ********************************************************
// delete the media 
// OUT: false=ERROR
// OUT: true=OK
    function delete($id = 0, $rm_dir = 0)
    {
        $db = Application::getInstance()->getSql();

        if ($id == 0) {
            $id = $this->id;
            // unvalid this media
            $this->id = 0;
        }
        $media = new Media($id);
        $result_file = true;
        if (is_file($media->filepath)) {
            $result_file = @unlink($media->filepath);
        }
        $sql = "DELETE FROM " . DB_TABLENAME_MEDIAS . " WHERE id=" . $id . ";";
        $db->query($sql);
        $result_sql = $db->affected_rows();
        $result_dir = true;
        if ($rm_dir && is_dir($media->dirpath)) {
            $result_dir = @rmdir($media->dirpath);
        }
        if (!$result_sql || !$result_file || !$result_dir) return false;
        else return true;
    }
// ********************************************************
// Delete a media. If it has childs than delete them too
// OUT: 0=Error
// OUT: >0=OK
    function deletes($id = 0)
    {
        if ($id == 0) {
            $id = $this->id;
        }
        $result_childs = true;
        // find child and delete them
        $_ids = $this->finds('parent_id=' . $id);
        if (is_array($_ids)) {
            foreach ($_ids as $_id) {
                $_result = $this->delete($_id);
                if (!$_result) $result_childs = false;
            }
        }
        // now delete itself
        $result_this = $this->delete($id, 1);
        if (!$result_this || !$result_childs) return false;
        else return true;
    }

// ********************************************************
// OUT: 0=Error
// OUT: >0=id
    function find($where)
    {
        $db = Application::getInstance()->getSql();

        $sql = "SELECT * FROM " . DB_TABLENAME_MEDIAS . " WHERE " . $where . ";";
        $db->query($sql);
        if ($db->nf() == 1) {
            $db->next_record();
            $id = $db->f("id");

            return $id;
        } else {
            return 0;
        }
    }
// ********************************************************
// OUT: 0=Error
// OUT: ARRAY=ids
    function finds($where)
    {
        $db = Application::getInstance()->getSql();

        $sql = "SELECT * FROM " . DB_TABLENAME_MEDIAS . " WHERE " . $where . ";";
        $db->query($sql);
        if ($db->nf() > 0) {
            $ids = array();
            while ($db->next_record()) {
                $ids[] = $db->f("id");
            }

            return $ids;
        } else {
            return 0;
        }
    }

// ********************************************************
// OUT: number of found entries
    function count($where)
    {
        $db = Application::getInstance()->getSql();

        $sql = "SELECT COUNT(*) AS num FROM " . DB_TABLENAME_MEDIAS . " WHERE " . $where . ";";
        $db->query($sql);
        if ($db->nf() > 0) {
            $db->next_record();

            return $db->f("num");
        } else {
            return 0;
        }
    }

// ********************************************************
// convert the fileextension (if necessary)
// IN: filepath
// OUT: new filepath
    function convertFilename($filename)
    {
        $_pathinfo = pathinfo($filename);
        switch ($_pathinfo["extension"]) {
            case 'gif':
            case 'png':
                $new_extension = 'png';
                break;
            default:
            case 'jpeg':
            case 'jpg':
                $new_extension = 'jpg';
                break;
        }

        return $_pathinfo["dirname"] . '/' . $_pathinfo["filename"] . '.' . $new_extension;
    }

// ********************************************************
// IN: GFX_SIZE_* constant from config.php
    function scale($new_size)
    {
        // scale the original ***
        $image = new Image();
        $_num_source = $image->loadImage($this->filepath);
        $_num_dest = $image->resizeImage($_num_source, $GLOBALS["CONFIG"]["MEDIA_SIZES"][$new_size]["x"], $GLOBALS["CONFIG"]["MEDIA_SIZES"][$new_size]["y"], $GLOBALS["CONFIG"]["MEDIA_SIZES"][$new_size]["dont_stretch_small"], $GLOBALS["CONFIG"]["MEDIA_SIZES"][$new_size]["scale_natural"]);
        if (!$_num_dest) {
            return false;
        }
        // change the filename/mimetype if necessary
        $_new_filepath = $this->convertFilename($this->filepath);
        $image->saveImage($_new_filepath, $_num_dest);
        // if the filename has changed
        if ($this->filepath != $_new_filepath) {
            unlink($this->filepath);
            // update the changed properties
            $this->filepath = $_new_filepath;
            $this->filename = basename($_new_filepath);
            $this->url = $GLOBALS["CONFIG"]["MEDIA_URL"] . $path . '/' . $this->filename;
        }
        // save the changed properties
        $this->width = $image->arrLoadedImages[$_num_dest]['iWidth'];
        $this->height = $image->arrLoadedImages[$_num_dest]['iHeight'];
        $this->filesize = filesize($this->filepath);
        $this->save(
             array(
                 "filename",
                 "width",
                 "height",
                 "filesize"
             )
        );

        return true;
    }
}