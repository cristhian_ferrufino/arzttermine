<?php

namespace Arzttermine\Media;

class Image {

    // holds a multidimensional array of images (name, size, type and resources)
    var $arrLoadedImages;

    // error messages
    var $sError;

    // constructor
    function __construct()
    {
        // setup default values
        $this->arrLoadedImages = array();
    }

    function createImage($iWidth, $iHeight)
    {
        // find the next available position
        $iThisIndex = count($this->arrLoadedImages);

        $this->arrLoadedImages[$iThisIndex]['sFileName'] = '';
        $this->arrLoadedImages[$iThisIndex]['iWidth'] = $iWidth;
        $this->arrLoadedImages[$iThisIndex]['iHeight'] = $iHeight;
        $this->arrLoadedImages[$iThisIndex]['iType'] = '';
        $this->arrLoadedImages[$iThisIndex]['rImage'] = imagecreatetruecolor($iWidth, $iHeight);

        return $iThisIndex;
    }

    function loadImage($sSourceFileName)
    {
        // find the dimensions of the source image
        list($iSourceWidth, $iSourceHeight, $iSourceType) = getimagesize($sSourceFileName);
        // save them in the array
        $image = array(
            'sFileName' => $sSourceFileName,
            'iWidth'    => $iSourceWidth,
            'iHeight'   => $iSourceHeight,
            'iType'     => $iSourceType
        );

        switch ($iSourceType) {
            case 1:
                $image['rImage'] = imagecreatefromgif($sSourceFileName);
                break;
            case 2:
                $image['rImage'] = imagecreatefromjpeg($sSourceFileName);
                break;
            case 3:
                $image['rImage'] = imagecreatefrompng($sSourceFileName);
                break;
            default:
                return false;
        }

        $this->arrLoadedImages[] = $image;

        return end(array_values(array_keys($this->arrLoadedImages)));
    }

    function overlayImage($iTargetIndex, $iSourceIndex)
    {
        return imagecopy($this->arrLoadedImages[$iTargetIndex]['rImage'], $this->arrLoadedImages[$iSourceIndex]['rImage'], 0, 0, 0, 0, $this->arrLoadedImages[$iSourceIndex]['iWidth'], $this->arrLoadedImages[$iSourceIndex]['iHeight']);
    }

    function outputImage($sType, $iIndex)
    {
        switch (strtolower($sType)) {
            case '.jpg':
            case '.jpeg':
                if (function_exists("imagejpeg")) {
                    header("Content-type: image/jpeg");
                    imagejpeg($this->arrLoadedImages[$iIndex]['rImage'], '', $GLOBALS["CONFIG"]["IMAGE_RESIZE_JPEG_QUALITY_PERCENT"]);
                } else {
                    $this->sError = 'No JPG support on the server';

                    return false;
                }
                break;
            case '.gif':
                if (function_exists("imagegif")) {
                    header("Content-type: image/gif");
                    imagegif($this->arrLoadedImages[$iIndex]['rImage']);
                } else {
                    $this->sError = 'No GIF support on the server';

                    return false;
                }
                break;
            case '.png':
                if (function_exists("imagepng")) {
                    header("Content-type: image/png");
                    imagepng($this->arrLoadedImages[$iIndex]['rImage']);
                } else {
                    $this->sError = 'No PNG support on the server';

                    return false;
                }
                break;
            default:
                $this->sError = 'Image type not recognised';

                return false;
        }

        // If we get here, we haven't failed
        return true;
    }

    function saveImage($sOutputFileName, $iIndex)
    {
        $this->sOutputFileName = $sOutputFileName;
        $pathinfo = pathinfo($sOutputFileName);
        switch (strtolower($pathinfo["extension"])) {
            case 'jpg':
            case 'jpeg':
                if (function_exists("imagejpeg")) {
                    return imagejpeg($this->arrLoadedImages[$iIndex]['rImage'], $sOutputFileName, $GLOBALS["CONFIG"]["IMAGE_RESIZE_JPEG_QUALITY_PERCENT"]);
                } else {
                    $this->sError = 'No JPG support on the server';

                    return false;
                }
                break;
            case 'gif':
                if (function_exists("imagegif")) {
                    return imagegif($this->arrLoadedImages[$iIndex]['rImage'], $sOutputFileName);
                } else {
                    $this->sError = 'No GIF support on the server';

                    return false;
                }
                break;
            case 'png':
                if (function_exists("imagepng")) {
                    return imagepng($this->arrLoadedImages[$iIndex]['rImage'], $sOutputFileName);
                } else {
                    $this->sError = 'No PNG support on the server';

                    return false;
                }
                break;
            default:
                $this->sError = 'Image type not recognised';

                return false;
        }
    }

    // IN: dont_stretch_small: If the gfx is smaller than max_width & max_height than don't scale that gfx
    // IN: scale_natural: true=leave the aspect-ratio | false=scale to max_width & max_height (cut if the ratio doesnt fit)
    function resizeImage($iSourceIndex, $max_width, $max_height, $dont_stretch_small = true, $scale_natural = true)
    {
        $src_width = $this->arrLoadedImages[$iSourceIndex]['iWidth'];
        $src_height = $this->arrLoadedImages[$iSourceIndex]['iHeight'];

        // check if only shrinking is wanted
        if ($dont_stretch_small) {
            if (($src_width <= $max_width) && ($src_height <= $max_height)) {
                $iFinalIndex = $this->createImage($src_width, $src_height);
                $success = imagecopyresampled($this->arrLoadedImages[$iFinalIndex]['rImage'], $this->arrLoadedImages[$iSourceIndex]['rImage'], 0, 0, 0, 0, $src_width, $src_height, $src_width, $src_height);
                if ($success) {
                    return $iFinalIndex;
                } else {
                    $this->sError = 'Couldnt resize image';

                    return false;
                }
            }
        }

        # image resizes to natural height and width
        if ($scale_natural == true) {
            if ($src_width > $src_height) {
                $thumb_width = $max_width;
                $thumb_height = floor($src_height * ($max_width / $src_width));
            } else if ($src_width < $src_height) {
                $thumb_height = $max_height;
                $thumb_width = floor($src_width * ($max_height / $src_height));
            } else {
                $thumb_width = $max_height;
                $thumb_height = $max_height;
            }
            $iFinalIndex = $this->createImage($thumb_width, $thumb_height);
            $success = imagecopyresampled($this->arrLoadedImages[$iFinalIndex]['rImage'], $this->arrLoadedImages[$iSourceIndex]['rImage'], 0, 0, 0, 0, $thumb_width, $thumb_height, $src_width, $src_height);
            if ($success) {
                return $iFinalIndex;
            } else {
                $this->sError = 'Couldnt resize image';

                return false;
            }
            # image is fixed to supplied width and height and cropped
        } elseif ($scale_natural == false) {
            $max_ratio = $max_width / $max_height;
            $off_w = 0;
            $off_h = 0;
            # thumbnail is landscape
            if ($max_ratio > 1) {
                # uploaded pic is landscape
                if ($src_width > $src_height) {
                    $src_ratio = $src_width / $src_height;
                    // avoid black lines over/under or left/right of the gfx
                    // and check how to scale
                    if ($src_ratio < $max_ratio) {
                        $thumb_width = $max_width;
                        $thumb_height = ceil($max_width * ($src_height / $src_width));
                        $off_h = ((($thumb_height - $max_height) / 2) * $src_height / $thumb_height);
                        $off_w = 0;
                    } else {
                        $thumb_height = $max_height;
                        $thumb_width = ceil($max_height * ($src_width / $src_height));
                        $off_w = ((($thumb_width - $max_width) / 2) * $src_width / $thumb_width);
                        $off_h = 0;
                    }
                    # uploaded pic is portrait
                } else {
                    $thumb_height = $max_width;
                    $thumb_width = ceil($max_width * ($src_height / $src_width));
                    if ($thumb_width > $max_width) {
                        $thumb_width = $max_width;
                        $thumb_height = ceil($max_width * ($src_height / $src_width));
                    }
                    $off_h = ($src_height - $src_width) / 2;
                }

                $iFinalIndex = $this->createImage($max_width, $max_height);
                $success = imagecopyresampled($this->arrLoadedImages[$iFinalIndex]['rImage'], $this->arrLoadedImages[$iSourceIndex]['rImage'], 0, 0, $off_w, $off_h, $thumb_width, $thumb_height, $src_width, $src_height);
                if ($success) {
                    return $iFinalIndex;
                } else {
                    $this->sError = 'Couldnt resize image';

                    return false;
                }
            } else {
                # thumbnail is portait ###########################
                if ($src_height > $src_width) {
                    # uploaded pic is portrait *******************
                    $src_ratio = $src_width / $src_height;
                    // avoid black lines over/under or left/right of the gfx
                    // and check how to scale
                    if ($src_ratio < $max_ratio) {
                        // scale the height
                        $src_height_orig = $src_height;
                        $src_height = ceil($src_width * ($max_height / $max_width));
                        $off_h = ($src_height_orig - $src_height) / 2;
                    } else {
                        // scale the width
                        $src_width_orig = $src_width;
                        $src_width = ceil($src_height * ($max_width / $max_height));
                        $off_w = ($src_width_orig - $src_width) / 2;
                    }
                } else {
                    # uploaded pic is landscape OR **************
                    # uploaded pic is square ********************
                    $src_width_orig = $src_width;
                    $src_width = ceil($src_height * ($max_width / $max_height));
                    $off_w = ($src_width_orig - $src_width) / 2;
                }

                $iFinalIndex = $this->createImage($max_width, $max_height);
                $success = imagecopyresampled($this->arrLoadedImages[$iFinalIndex]['rImage'], $this->arrLoadedImages[$iSourceIndex]['rImage'], 0, 0, $off_w, $off_h, $max_width, $max_height, $src_width, $src_height);
                if ($success) {
                    return $iFinalIndex;
                } else {
                    $this->sError = 'Couldnt resize image';

                    return false;
                }
            }
        }
        
        return false;
    }

    function flipImage($iSourceIndex, $mode)
    {
        $width = $this->arrLoadedImages[$iSourceIndex]['iWidth'];
        $height = $this->arrLoadedImages[$iSourceIndex]['iHeight'];
        $src_x = 0;
        $src_y = 0;
        $src_width = $width;
        $src_height = $height;

        switch ($mode) {
            case '1': //vertical
                $src_y = $height - 1;
                $src_height = -$height;
                break;
            case '2': //horizontal
                $src_x = $width - 1;
                $src_width = -$width;
                break;
            case '3': //both
                $src_x = $width - 1;
                $src_y = $height - 1;
                $src_width = -$width;
                $src_height = -$height;
                break;
            default:
                return $iSourceIndex;
        }
        $iFinalIndex = $this->createImage($width, $height);
        if (imagecopyresampled($this->arrLoadedImages[$iFinalIndex]['rImage'], $this->arrLoadedImages[$iSourceIndex]['rImage'], 0, 0, $src_x, $src_y, $width, $height, $src_width, $src_height)) {
            return $iFinalIndex;
        }

        return $iSourceIndex;
    }

    function rotateImage($iSourceIndex, $degrees)
    {
        $_resource = imagerotate($this->arrLoadedImages[$iSourceIndex]['rImage'], $degrees, 0);
        $this->arrLoadedImages[$iSourceIndex]['rImage'] = $_resource;
        // update the sizes
        $this->arrLoadedImages[$iSourceIndex]['iWidth'] = imagesx($this->arrLoadedImages[$iSourceIndex]['rImage']);
        $this->arrLoadedImages[$iSourceIndex]['iHeight'] = imagesy($this->arrLoadedImages[$iSourceIndex]['rImage']);

        if (is_resource($_resource)) {
            return true;
        } else {
            $this->sError = 'Couldnt rotate image';

            return false;
        }
    }

    function rotateImagePerPixel($iSourceIndex, $rotation)
    {
        $width = imagesx($this->arrLoadedImages[$iSourceIndex]['rImage']);
        $height = imagesy($this->arrLoadedImages[$iSourceIndex]['rImage']);
        switch ($rotation) {
            case 90:
                $iFinalIndex = $this->createImage($height, $width);
                break;
            case 180:
                $iFinalIndex = $this->createImage($width, $height);
                break;
            case 270:
                $iFinalIndex = $this->createImage($height, $width);
                break;
            case 0:
                return $iFinalIndex;
                break;
            case 360:
                return $iFinalIndex;
                break;
        }
        if ($iFinalIndex) {
            for ($i = 0; $i < $width; $i++) {
                for ($j = 0; $j < $height; $j++) {
                    $reference = imagecolorat($this->arrLoadedImages[$iSourceIndex]['rImage'], $i, $j);
                    switch ($rotation) {
                        case 90:
                            if (!@imagesetpixel($this->arrLoadedImages[$iFinalIndex]['rImage'], ($height - 1) - $j, $i, $reference)) {
                                return false;
                            }
                            break;
                        case 180:
                            if (!@imagesetpixel($this->arrLoadedImages[$iFinalIndex]['rImage'], $width - $i, ($height - 1) - $j, $reference)) {
                                return false;
                            }
                            break;
                        case 270:
                            if (!@imagesetpixel($this->arrLoadedImages[$iFinalIndex]['rImage'], $j, $width - $i, $reference)) {
                                return false;
                            }
                            break;
                    }
                }
            }

            return $iFinalIndex;
        }

        return false;
    }
}