<?php

namespace Arzttermine\Media;

use Arzttermine\Application\Application;
use Arzttermine\Core\Basic;

class Asset extends Basic {

    /**
     * @var string
     */
    protected $tablename = DB_TABLENAME_ASSETS;

    /**
     * @var int
     */
    protected $owner_id;

    /**
     * @var int
     */
    protected $owner_type;

    /**
     * @var int
     */
    protected $gallery_id;

    /**
     * @var int
     */
    protected $parent_id;

    /**
     * @var int
     */
    protected $filesize;

    /**
     * @var int
     */
    protected $size;

    /**
     * @var int
     */
    protected $width;

    /**
     * @var int
     */
    protected $height;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var string
     */
    protected $filename;

    /**
     * @var string
     */
    protected $mime_type;

    /**
     * @var string
     */
    protected $created_at;

    /**
     * @var int
     */
    protected $created_by;

    /**
     * @var string
     */
    protected $updated_at;

    /**
     * @var int
     */
    protected $updated_by;

    /**
     * The dir on the FS
     *
     * @var string
     */
    protected $dirpath;

    /**
     * Dir and filename on the FS
     *
     * @var string
     */
    protected $filepath;

    /**
     * Relative path to the gfx
     *
     * @var string
     */
    protected $url;

    /**
     * Absollute path to the gfx
     *
     * @var string
     */
    protected $http_url;

    /**
     * array with assets / initiated by loadChildrens()
     *
     * @var array
     */
    protected $children;

    /**
     * @var array
     */
    protected $all_keys
        = array(
            "id",
            "owner_id",
            "owner_type",
            "gallery_id",
            "parent_id",
            "size",
            "filesize",
            "width",
            "height",
            "description",
            "filename",
            "mime_type",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by"
        );

    /**
     * @var array
     */
    protected $secure_keys
        = array(
            "description",
            "filename",
            "mime_type",
        );

    /**
     * Calcs some attrs
     *
     * @return bool
     */
    public function calcVars()
    {
        // If this is a child than calc the properties (path) from parent
        // Assume that the parent_id exists and dont check here for performance
        $parent_id = $this->isParent() ? $this->id : $this->parent_id;

        $path = self::calcPath($parent_id);

        $this->url = "{$GLOBALS["CONFIG"]["ASSET_URL"]}{$path}/{$this->filename}";
        $this->http_url = "{$GLOBALS['CONFIG']['URL_HTTP_LIVE']}{$this->url}";
        $this->dirpath = "{$GLOBALS["CONFIG"]["ASSET_PATH"]}{$path}";
        $this->filepath = "{$this->dirpath}/{$this->filename}";

        return true;
    }

    /**
     * "Encodes" the path
     *
     * @param int $id
     *
     * @return bool
     */
    public static function calcPath($id)
    {
        $id = sprintf("%09d", $id);

        return substr($id, 0, 3) . '/' . substr($id, 3, 3) . '/' . substr($id, 6, 3);
    }

    /**
     * Checks if this is a parent asset
     *
     * @return bool
     */
    public function isParent()
    {
        return !($this->parent_id > 0);
    }

    /**
     * @param int $parent_id
     *
     * @return self
     */
    public function setParentId($parent_id)
    {
        $this->parent_id = $parent_id;

        return $this;
    }

    /**
     * @return int
     */
    public function getParentId()
    {
        return $this->parent_id;
    }
    
    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return int
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * @param string $description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = trim($description);

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $updated_at
     *
     * @return self
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param string $updated_by
     *
     * @return self
     */
    public function setUpdatedBy($updated_by)
    {
        $this->updated_by = $updated_by;

        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedBy()
    {
        return $this->updated_by;
    }

    /**
     * Return the filename
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param int $filesize
     *
     * @return self
     */
    public function setFilesize($filesize)
    {
        $this->filesize = $filesize;

        return $this;
    }

    /**
     * @return int
     */
    public function getFilesize()
    {
        return $this->filesize;
    }

    /**
     * @param int $gallery_id
     *
     * @return self
     */
    public function setGalleryId($gallery_id)
    {
        $this->gallery_id = $gallery_id;

        return $this;
    }

    /**
     * @return int
     */
    public function getGalleryId()
    {
        return $this->gallery_id;
    }

    /**
     * @param int $width
     *
     * @return self
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param int $height
     *
     * @return self
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param int $owner_id
     *
     * @return self
     */
    public function setOwnerId($owner_id)
    {
        $this->owner_id = $owner_id;

        return $this;
    }

    /**
     * @return int
     */
    public function getOwnerId()
    {
        return $this->owner_id;
    }

    /**
     * @param int $owner_type
     *
     * @return self
     */
    public function setOwnerType($owner_type)
    {
        $this->owner_type = $owner_type;

        return $this;
    }

    /**
     * @return int
     */
    public function getOwnerType()
    {
        return $this->owner_type;
    }

    /**
     * @param int $size
     *
     * @return self
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param string $mimeType
     *
     * @return self
     */
    public function setMimeType($mimeType)
    {
        $this->mime_type = $mimeType;

        return $this;
    }

    /**
     * @return string
     */
    public function getMimeType()
    {
        return $this->mime_type;
    }

    /**
     * @return string
     */
    public function getDirpath()
    {
        return $this->dirpath;
    }

    /**
     * @return string
     */
    public function getFilepath()
    {
        return $this->filepath;
    }

    /**
     * Load one or all childrens (if there are any) and stores them in $this->childrens[index:size]
     *
     * @param int $size
     *
     * @return bool
     */
    private function loadChild($size = null)
    {
        $where = '';

        if ($size) {
            $where = " AND size={$size}";
        }

        $ids = $this->findKeys("parent_id={$this->getId()}{$where}", 'id');

        if (is_array($ids) && !empty($ids)) {
            // we need to reindex them by the size
            foreach ($ids as $id) {
                $asset = new Asset($id);
                if ($asset->isValid()) {
                    $this->children[$asset->size] = $asset;
                }
            }
        }

        return true;
    }

    /**
     * Returns a child object
     *
     * @param int $size
     *
     * @return object|bool
     */
    public function getChild($size)
    {
        if (!is_array($this->children) || !isset($this->children[$size])) {
            $this->loadChild($size);
        }

        if (isset($this->children[$size])) {
            return $this->children[$size];
        }

        return false;
    }

    /**
     * Check if this is a gfx
     *
     * @return bool
     */
    public function isGfx()
    {
        return in_array($this->mime_type, $GLOBALS['CONFIG']['ASSET_GFX_POSSIBLE_MIMETYPES']);
    }

    /**
     * Return the url to the asset
     *
     * @param int|string $size
     * @param bool $add_host
     *
     * @return string
     */
    public function getUrl($size = ASSET_GFX_SIZE_ORIGINAL, $add_host = false)
    {
        $host = '';

        if ($add_host) {
            $host = $GLOBALS['CONFIG']['URL_HTTP_LIVE'];
        }

        // Equals "if ($size != ASSET_GFX_SIZE_ORIGINAL)"
        if ($this->isParent()) {
            $asset = $this->getChild($size);

            if (is_object($asset) && $asset->isValid()) {
                return $asset->getUrl($size, $add_host);
            }
        }

        return "{$host}{$this->url}";
    }

    /**
     * Return the url to the asset if id and filename are given
     * this method is mainly for avoiding database performance costs
     *
     * @static
     *
     * @param int $id
     * @param string $filename
     * @param int $ownertype
     * @param int $size (can be a child)
     * @param bool $add_host
     *
     * @return string
     */
    public static function getUrlByIdFilenameOwner($id, $filename, $ownertype, $size = ASSET_GFX_SIZE_ORIGINAL, $add_host = false)
    {
        if (!is_numeric($id) || $id == 0 || $filename == '') {
            return '';
        }

        $host = '';

        if ($add_host) {
            $host = $GLOBALS['CONFIG']['URL_HTTP_LIVE'];
        }

        $pathinfo = pathinfo($filename);
        $filename = "{$pathinfo['filename']}{$GLOBALS['CONFIG']['ASSET_GFX_TYPES'][$ownertype]['SIZES'][$size]['postfix']}.{$pathinfo['extension']}";
        $path = self::calcPath($id);
        $url = self::convertFilepath("{$GLOBALS['CONFIG']['ASSET_URL']}{$path}/{$filename}");

        return "{$host}{$url}";
    }

    /**
     * Return the url to the asset and replace assets with
     * their icon if they can not be displayed in the browser
     *
     * @param int $size (can be a child)
     * @param bool $absolute_path
     *
     * @return string
     */
    public function getGfxUrl($size = ASSET_GFX_SIZE_ORIGINAL, $absolute_path = false)
    {
        $pathinfo = pathinfo($this->getFilename());

        switch ($pathinfo["extension"]) {
            case "swf":
                $url = getStaticUrl('adm/media_mimetype_swf.png', $absolute_path);
                break;
            case "mp3":
                $url = getStaticUrl('adm/media_mimetype_mp3.png', $absolute_path);
                break;
            case "mov":
            case "avi":
                $url = getStaticUrl('adm/media_mimetype_avi.png', $absolute_path);
                break;
            case "css":
                $url = getStaticUrl('adm/media_mimetype_css.png', $absolute_path);
                break;
            case "js":
                $url = getStaticUrl('adm/media_mimetype_js.png', $absolute_path);
                break;
            case "pdf":
                $url = getStaticUrl('adm/media_mimetype_pdf.png', $absolute_path);
                break;
            case "doc":
            case "docx":
                $url = getStaticUrl('adm/media_mimetype_doc.png', $absolute_path);
                break;
            case "ppt":
                $url = getStaticUrl('adm/media_mimetype_ppt.png', $absolute_path);
                break;
            case "xls":
            case "xlsx":
                $url = getStaticUrl('adm/media_mimetype_xls.png', $absolute_path);
                break;
            case "ico":
            case "gif":
            case "jpg":
            case "jpeg":
            case "png":
            default:
                $url = $this->getUrl($size, $absolute_path);
                break;
        }

        return $url;
    }

    /**
     * Save the upload file in FS in a new dir
     *
     * @param string $filepath_src
     * @param string $name
     * @param bool $save_from_local_file
     *
     * @return bool
     */
    public function saveUpload($filepath_src, $name, $save_from_local_file = false)
    {
        // should we keep the filename or generate a new one?
        if ($this->isGfx()
            && isset($GLOBALS['CONFIG']['ASSET_GFX_TYPES'][$this->owner_type]['USE_ORIGINAL_FILENAME'])
            && $GLOBALS['CONFIG']['ASSET_GFX_TYPES'][$this->owner_type]['USE_ORIGINAL_FILENAME']
        ) {
            $this->filename = $name;
        } elseif (!$this->isGfx() && $GLOBALS['CONFIG']['ASSET_USE_ORIGINAL_FILENAME']) {
            $this->filename = $name;
        } else {
            // Set the filename: "id_uid.extension"
            // Get the extension
            $file_ext = strtolower(strrchr($name, '.'));

            // Get the uid
            $uid = strtolower(Application::createUid($GLOBALS['CONFIG']['ASSET_GFX_CHARS_UID_POSTFIX']));
            $this->filename = "{$this->id}_{$uid}{$file_ext}";
        }

        if (!$this->save(array('filename'))) {
            return false;
        }

        // Set url and dirpath and create the dir
        $this->calcVars();

        // Create the dir
        if (!is_dir($this->dirpath)) {
            if (!@mkdir($this->dirpath, 0775, true)) {
                error_log('could not mkdir: '.__FILE__.' / '.__LINE__);
                return false;
            }
        }

        // Get the file
        if ($save_from_local_file) {
            if (!copy($filepath_src, $this->filepath)) {
                return false;
            }
        } else {
            if (!move_uploaded_file($filepath_src, $this->filepath)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Delete the asset
     *
     * @param int $id
     * @param bool $remove_directory
     *
     * @return bool
     */
    public function delete($id = null, $remove_directory = false)
    {
        if (!$id) {
            $id = $this->id;
        }

        $asset = new Asset($id);
        $result_file = true;

        if (is_file($asset->filepath)) {
            $result_file = @unlink($asset->filepath);
        }

        $result_sql = parent::delete($id);

        $result_dir = true;

        if ($remove_directory && is_dir($asset->dirpath)) {
            $result_dir = @rmdir($asset->dirpath);
        }

        if (!$result_sql || !$result_file || !$result_dir) {
            return false;
        }

        // Invalidate this object
        $this->id = 0;

        return true;
    }

    /**
     * Delete the asset, if there are children, than delete them too
     *
     * @param int $id
     *
     * @return bool
     */
    public function deleteWithChildren($id = null)
    {
        if (!$id) {
            $id = $this->id;
        }

        $result_children = true;

        // Find children and delete them
        $ids = $this->findKeys("parent_id={$id}", 'id');

        if (is_array($ids)) {
            foreach ($ids as $_id) {
                $result = $this->delete($_id);
                if (!$result) {
                    $result_children = false;
                }
            }
        }

        // Now delete myself
        $result_this = $this->delete($id, true);

        if (!$result_this || !$result_children) {
            return false;
        }

        return true;
    }

    /**
     * 1. Store the file data in db
     * 2. Store the upload file in fs
     * 3. Scale if its a gfx
     *
     * @param int $ownertype
     * @param int $owner_id
     * @param array $data
     * @param array $files_upload
     * @param bool $save_from_local_file
     *
     * @return bool|int ID or false on error
     */
    public function saveNewParent($ownertype, $owner_id, $data, $files_upload, $save_from_local_file = false)
    {
        $app = Application::getInstance();
        $profile = $app->getCurrentUser();

        $data['created_at'] = $app->now('datetime');
        $data['created_by'] = $profile->getId();
        $data['owner_id'] = $owner_id;
        $data['owner_type'] = $ownertype;
        $data['mime_type'] = self::getFileMimeType($_FILES['upload']['tmp_name']);
        $data['filesize'] = $files_upload['size'];
        if(!isset($data['parent_id'])) {
            $data['parent_id'] = 0;
        }
        if(!isset($data['size'])) {
            $data['size'] = 0;
        }
        if(!isset($data['width'])) {
            $data['width'] = 0;
        }
        if(!isset($data['height'])) {
            $data['height'] = 0;
        }
        if(!isset($data['updated_by'])) {
            $data['updated_by'] = $profile->getId();
        }

        $id = $this->saveNew($data, true);

        if (!$id) {
            return false;
        }

        $this->load($id);

        // Store the original upload file in the fs
        if (!$this->saveUpload($_FILES['upload']['tmp_name'], $_FILES['upload']['name'], $save_from_local_file)) {
            $this->deleteWithChildren();

            return false;
        }

        // If this is a gfx than scale
        if ($this->isGfx()) {
            $data['parent_id'] = $id;

            // Rename the original filename if needed and get the dimensions
            $this->scaleGfx();

            // Check if there are exif-informations in the gfx
            // If so, than check if we also have to rotate the gfx
            list($rotate, $flip) = $this->getRotateFlip($this->filepath);

            // We dont need this information in each gfx
            $data['description'] = '';
            $data['filesize'] = 0;

            // Now resize each gfx
            foreach ($GLOBALS['CONFIG']['ASSET_GFX_TYPES'][$ownertype]['SIZES'] as $size_id => $size_data) {
                // Take the properties from the parent file
                $_data = $data;
                $_data['size'] = $size_id;
                $asset = new Asset();

                if (!$_id = $asset->saveNew($_data, true)) {
                    $this->deleteWithChildren();

                    return false;
                }

                $asset->load($_id);

                // Set the new filename
                $pathinfo = pathinfo($this->filepath);

                $asset->filename = "{$pathinfo['filename']}{$size_data['postfix']}.{$pathinfo['extension']}";

                if (!$asset->save(array('filename'))) {
                    $this->deleteWithChildren();

                    return false;
                }

                // Set url and dirpath
                $asset->calcVars();

                if (!copy($this->filepath, $asset->filepath)) {
                    $this->deleteWithChildren();

                    return false;
                }

                $asset->scaleGfx($size_data, $rotate, $flip);
            }

            // Delete the orignial gfx if not needed
            if (!$GLOBALS['CONFIG']['ASSET_GFX_TYPES'][$ownertype]['KEEP_ORIGINAL_FILE']) {
                if (is_file($this->filepath)) {
                    @unlink($this->filepath);
                }
            }
        }

        return $this->id;
    }

    /**
     * Convert the fileextension (if necessary)
     *
     * @param string $filepath
     *
     * @return int 0=Error | >0=new id
     */
    public static function convertFilepath($filepath)
    {
        $pathinfo = pathinfo($filepath);
        $extension = $pathinfo['extension'];

        // Change the extension only for gif, as this will be mapped to png
        if (preg_match('/gif/i', $extension)) {
            $extension = 'png';
        }

        return "{$pathinfo['dirname']}/{$pathinfo['filename']}.{$extension}";
    }

    /**
     * Scale the gfx
     *
     * @param string $size_data
     * @param string $rotate
     * @param string $flip
     *
     * @return array
     */
    public function scaleGfx($size_data = '', $rotate = '', $flip = '')
    {
        $image = new Image();
        $num_dest = $image->loadImage($this->filepath);
        $save = array(
            'filename',
            'width',
            'height',
            'filesize'
        );

        // First rotate and flip. Than scale to make sure, that the destination dimensions are as wanted
        if ($size_data) {
            if ($flip) {
                $num_flip = $image->flipImage($num_dest, $flip);

                if ($num_dest == $num_flip) {
                    return false;
                } else {
                    $num_dest = $num_flip;
                }
            }

            if ($rotate) {
                if (!$image->rotateImage($num_dest, $rotate)) {
                    return false;
                }
            }

            // Scale the original
            $num_dest = $image->resizeImage(
                              $num_dest,
                                  $size_data['x'],
                                  $size_data['y'],
                                  $size_data['dont_stretch_small'],
                                  $size_data['scale_natural']
            );

            if (!$num_dest) {
                return false;
            }

            // Change the filename/mimetype if necessary
            $new_filepath = $this->convertFilepath($this->filepath);
            $image->saveImage($new_filepath, $num_dest);

            // If the filename has changed
            if ($this->filepath != $new_filepath) {
                unlink($this->filepath);
                // Update the changed properties
                $this->filepath = $new_filepath;
                $this->filename = basename($new_filepath);
            }

            // The mimetype could have changed
            $this->mime_type = self::getFileMimeType($this->filepath);
            $save[] = 'mime_type';

            // Save the changed properties
            $this->width = $image->arrLoadedImages[$num_dest]['iWidth'];
            $this->height = $image->arrLoadedImages[$num_dest]['iHeight'];
            $this->filesize = filesize($this->filepath);

            $this->save($save);
        }

        return true;
    }

    /**
     * Check if there are exif-informations in the gfx
     * If so, than check if we also have to rotate the gfx
     *
     * @param string $filename
     *
     * @return array
     */
    public function getRotateFlip($filename)
    {
        $rotate = '';
        $flip = '';
        $exif = @exif_read_data($filename, 0, true);

        if (is_array($exif)) {
            $ort = !empty($exif['IFD0']['Orientation']) ? $exif['IFD0']['Orientation'] : null;
            switch ($ort) {
                case 1: // nothing
                    break;
                case 2: // horizontal flip
                    $flip = 1;
                    break;
                case 3: // 180 rotate left
                    $rotate = 180;
                    break;
                case 4: // vertical flip
                    $flip = 2;
                    break;
                case 5: // vertical flip + 90 rotate right
                    $flip = 2;
                    $rotate = -90;
                    break;
                case 6: // 90 rotate right
                    $rotate = -90;
                    break;
                case 7: // horizontal flip + 90 rotate right
                    $flip = 1;
                    $rotate = -90;
                    break;
                case 8: // 90 rotate left
                    $rotate = 90;
                    break;
            }
        }

        return array(
            $rotate,
            $flip
        );
    }

    /**
     * Get the mime-type for a file in fs
     *
     * @param string $filename in fs
     *
     * @return string
     */
    public static function getFileMimeType($filename)
    {
        $fi = new \finfo(FILEINFO_MIME_TYPE);

        return $fi->buffer(file_get_contents($filename));
    }
}
