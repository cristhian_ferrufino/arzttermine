<?php

namespace Arzttermine\Controller;

use Arzttermine\Core\Api;

class APIController extends Controller {

    public function indexAction($method, $type)
    {
        if (!empty($method) && !empty($type)) {
            $api = new Api();
            return $api->processRequest($method, $type);
        }
        
        $this->notFound();
    }
}