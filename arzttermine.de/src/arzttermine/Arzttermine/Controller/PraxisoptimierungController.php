<?php

namespace Arzttermine\Controller;

use Arzttermine\Application\Application;
use Arzttermine\MedicalSpecialty\MedicalSpecialty;
use Arzttermine\Mail\Mailing;

class PraxisoptimierungController extends Controller {

    /**
     * @param string $slug
     * 
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function viewAction($slug = 'index')
    {

        $view = $this->getView();
        $view->setRef('select_location_medical_specialty_id_options', MedicalSpecialty::getSelectLocationMedicalSpecialtyIdsJavascriptOptions());
        $view->setRef('MedicalSpecialtyOptions', MedicalSpecialty::getHtmlOptions());

        $template = '';
        switch ($slug) {
            case 'atsearch':
            case 'atweb':
            case 'atrep':
            case 'atpatient':
            case 'atcalendar':
            case 'atcall':
            case 'atwidget':
            case 'index':
                $template = "{$slug}.tpl";
                break;
            default:
                $this->notFound();
                break;
        }

        $view->setRef('google_share_url', Application::getInstance()->getUrl()); // @todo Remove
        return $view->display("praxisoptimierung/{$template}");
    }

    /**
     * Mailer action
     */
    public function contactAction()
    {
        $request = $this->getRequest();
        $result = array('data' => 'fail'); // Assume fail

        if ($request->isPost()) {
            $post = $request->request->all();
            $post = filter_var_array($post, FILTER_SANITIZE_STRING);

            $keys = array(
                'name',
                'phone',
                'praxis',
                'website',
                'medical_specialty',
                'zip',
                'email',
                'date',
                'time',
                'atsearch',
                'atwidget',
                'atweb',
                'atrep',
                'atpatient',
                'atcalender',
                'atcall'
            );
            
            $message = 'Neue Anfrage:<br/><br/>';
            foreach ($keys as $key) {
                $message .= "{$key}: " . (isset($post[$key]) ? $post[$key] : '') . '<br>';
            }

            $_mailing = new Mailing();
            $_mailing->type = MAILING_TYPE_TEXT;
            $_mailing->from = $GLOBALS["CONFIG"]["CONTACTS_EMAIL_ADDRESS"];
            $_mailing->subject = "Praxisoptimierung Formular";
            $_mailing->body_text = $message;
            $_mailing->addAddressTo('sales@arzttermine.de');
            $_mailing->from = $GLOBALS['CONFIG']["BOOKINGS_EMAIL_ADDRESS"];
            $_mailing->from_name = $GLOBALS['CONFIG']["BOOKINGS_EMAIL_NAME"];

            if ($_mailing->sendPlain(true)) {
                $result['data'] = 'ok';
            }
        }
        
        return $request->json($result);
    }

}