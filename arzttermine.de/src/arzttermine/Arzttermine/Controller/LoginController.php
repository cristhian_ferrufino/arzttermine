<?php

namespace Arzttermine\Controller;

class LoginController extends Controller {

    /**
     * Log the user in
     */
    public function loginAction()
    {
        $view = $this->getView();
        $request = $this->getRequest();
        $profile = $this->getUser();

        if ($request->isPost()) {
            if ($profile->login($request->getRequest('email'), $request->getRequest('password'))) {
                if($profile->isMemberOf(1)) {
                    return $this->redirect($this->generateUrl('admin_dashboard'));
                }
                return $this->redirect($request->getQuery('referrer', $this->generateUrl('home')));
            } else {
                $this->addSessionMessage($this->trans('Bitte überprüfen Sie Ihre E-Mail-Adresse/Passwort'), 'ERROR');
            }
        }

        return $view
            ->setRef('email', $request->getRequest('email'))
            ->addHeadTitle('Login')
            ->display('login/login.tpl');
    }

    /**
     * Log the user out and redirect to the homepage
     */
    public function logoutAction()
    {
        $this->getUser()->logout();
        $this->getView()->addMessage('STATUS', 'NOTICE', $this->trans('Sie haben sich erfolgreich ausgeloggt!'));

        $GLOBALS['CONFIG']['GOOGLE_ANALYTICS_ACTIVATE'] = false;

        return $this->redirectTo('home');
    }
}