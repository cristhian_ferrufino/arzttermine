<?php

namespace Arzttermine\Controller;

use Arzttermine\Application\Application;
use Arzttermine\Form\ContactType;
use Arzttermine\Form\Form;
use Arzttermine\Mail\Contact;
use Arzttermine\Mail\Mailing;
use Symfony\Component\Form\FormInterface;

class ContactController extends Controller {

    /**
     * Contact page
     */
    public function indexAction()
    {
        $app = Application::getInstance();
        $request = $this->getRequest();
        $view = $this->getView();

        // Contact form
        $form = Form::getForm(new ContactType());
        $errors = array();

        if ($request->isPost()) {
            $form->submit($request->getRequest('form'));

            // SpamFree currently doesn't work
            if (false) { // if (Spamfree::isSpam()) {
                $app->addSessionMessage($app->_('Wir konnten Ihre Nachricht leider nicht annehmen, weil unser System Sie als Spammer eingestuft hat!'), 'ERROR');
            } else {
                if (!$form->isValid()) {
                    $app->addSessionMessage($app->_('Bitte prüfen Sie Ihre Eingaben.'), 'ERROR');
                    $errors = Form::getFormErrors($form);
                } else {
                    $form_data = $form->getData();

                    // Kludge some additional fields
                    $form_data['created_at'] = $app->now('datetime');
                    $form_data['created_by'] = $app->getCurrentUser()->getId();
                    $form_data['lang'] = $app->getLanguageCode();

                    $contact = new Contact();
                    $_new_contact_id = $contact->saveNew($form_data);

                    if (!$_new_contact_id) {
                        $app->addSessionMessage($app->static_text('could_not_create_new_contact'), 'ERROR');
                    } else {
                        $_mailing = new Mailing();
                        $_mailing->type = MAILING_TYPE_TEXT;
                        $_mailing->content_filename = '/mailing/contact.txt';
                        $_mailing->from = $GLOBALS["CONFIG"]["CONTACTS_EMAIL_ADDRESS"];
                        $_mailing->from_name = 'Kontaktformular - Arzttermine.de';
                        $_mailing->data = $form_data;
                        $_mailing->data['domain'] = $GLOBALS['CONFIG']['DOMAIN'];
                        $_mailing->addAddressTo($GLOBALS["CONFIG"]["CONTACTS_EMAIL_ADDRESS"], $GLOBALS["CONFIG"]["CONTACTS_EMAIL_NAME"]);
                        $_mailing->send();

                        $app->addSessionMessage($app->static_text('contact_created_successfully'));
                    }

                    $view->setRef('mail_sent', true);
                }
            }
        }

        return $view
            ->setRefs(
            array(
                'url_form_action' => $request->getRequestUri(),
                'form'            => $form->getData(),
                'errors'          => $errors,
                'active_page'     => 101
            )
            ) // Kludge for active page (sidebar)
            ->addHeadTitle('Kontakt')
            ->display('contact/contact.tpl');
    }

    /**
     * @param FormInterface $form
     *
     * @return string
     */
    protected function getFormSummary(FormInterface $form)
    {
        return $this->getView()
            ->setRef('form', $form->getData())
            ->fetch('contact/_contact_summary.tpl');
    }
}
