<?php

namespace Arzttermine\Controller;

use Arzttermine\Application\Application;
use Arzttermine\Event\Event;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class Controller implements ContainerAwareInterface {

    use ContainerAwareTrait;

    /**
     * Generates a URL from the given parameters.
     *
     * @param string         $route         The name of the route
     * @param mixed          $parameters    An array of parameters
     * @param bool|string    $referenceType The type of reference (one of the constants in UrlGeneratorInterface)
     *
     * @return string The generated URL
     *
     * @see UrlGeneratorInterface
     */
    public function generateUrl($route, $parameters = array(), $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH)
    {
        return $this->getRouter()->generate($route, $parameters, $referenceType);
    }

    /**
     * @param string $url
     * @param string|array $message
     * 
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirect($url, $message = null)
    {
        return Application::getInstance()->redirect($url, $message);
    }

    /**
     * As above, but takes a route (as opposed to qualified URL)
     * 
     * @param string $route The route as found in the routing config
     * @param array $parameters
     * @param string|array $message
     * 
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirectTo($route, $parameters = array(), $message = null)
    {
        return $this->redirect($this->generateUrl($route, $parameters), $message);
    }

    /**
     * @param string $url
     * @param string|array $message
     * 
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function forward($url, $message = null)
    {
        return Application::getInstance()->forward($url, $message);
    }

    /**
     * As above, but takes a route (as opposed to qualified URL)
     * 
     * @param string $route The route as found in the routing config
     * @param array $parameters
     * @param string|array $message
     * 
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function forwardTo($route, $parameters = array(), $message = null)
    {
        return $this->forward($this->generateUrl($route, $parameters), $message);
    }

    /**
     * Show the 404 page
     */
    public function notFound()
    {
        throw new NotFoundHttpException();
    }

    /**
     * @param string $message
     * @param array $parameters
     * @param string $domain
     * @param string $locale
     *
     * @return string
     */
    public function trans($message, $parameters = array(), $domain = null, $locale = null)
    {
        try {
            $translator = $this->getTranslator();
        } catch (\Exception $e) {
            return $message;
        }
        
        return $translator->trans($message, $parameters, $domain, $locale);
    }

    /**
     * Sugar method to wrap data in a compatible event object and dispatch
     * 
     * @param string $event_name
     * @param Event|mixed $data
     * 
     * @return Event
     */
    public function dispatchEvent($event_name, $data = null)
    {
        if (!$data instanceof Event) {
            $data = new Event($data);
        }
        
        return $this->getDispatcher()->dispatch($event_name, $data);
    }

    /**
     * Shortcut to return the request service.
     *
     * @throws \Exception
     * @return \Symfony\Component\Routing\Router
     */
    public function getRouter()
    {
        if (!$this->container->has('router')) {
            throw new \Exception('Router not found');
        }

        return $this->container->get('router');
    }
    
    /**
     * Shortcut to return the request service.
     *
     * @throws \Exception
     * @return \Arzttermine\Http\Request
     */
    public function getRequest()
    {
        if (!$this->container->has('request')) {
            throw new \Exception('Request not found');
        }
        
        return $this->container->get('request');
    }

    /**
     * Shortcut to return the Doctrine Registry service.
     *
     * @return \Arzttermine\Db\MySQLConnection
     * @throws \Exception
     */
    public function getDatabase()
    {
        if (!$this->container->has('database')) {
            throw new \Exception('Database not found');
        }

        return $this->container->get('database');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Session\Session
     * @throws \Exception
     */
    public function getSession()
    {
        if (!$this->container->has('session')) {
            throw new \Exception('Session not found');
        }

        return $this->container->get('session');
    }

    /**
     * @return \Symfony\Component\Translation\Translator
     * @throws \Exception
     */
    public function getTranslator()
    {
        if (!$this->container->has('translator')) {
            throw new \Exception('Translator not found');
        }

        return $this->container->get('translator');
    }

    /**
     * @return \Arzttermine\View\View
     * @throws \Exception
     */
    public function getView()
    {
        if (!$this->container->has('templating')) {
            throw new \Exception('View not found');
        }

        return $this->container->get('templating');
    }

    /**
     * Get the currently active user
     * 
     * @return \Arzttermine\Security\Authentication
     * @throws \Exception
     */
    public function getUser()
    {
        if (!$this->container->has('current_user')) {
            throw new \Exception('User Profile not found');
        }

        return $this->container->get('current_user');
    }

    /**
     * @return \Symfony\Component\EventDispatcher\EventDispatcher
     * @throws \Exception
     */
    public function getDispatcher()
    {
        if (!$this->container->has('dispatcher')) {
            throw new \Exception('Event Dispatcher not found');
        }

        return $this->container->get('dispatcher');
    }

    /**
     * Returns true if the service id is defined.
     *
     * @param string $id The service id
     *
     * @return bool true if the service id is defined, false otherwise
     */
    public function has($id)
    {
        return $this->container->has($id);
    }

    /**
     * @param string $id The service id
     *
     * @return object
     * @throws \Exception
     */
    public function get($id)
    {
        if (!$this->container->has($id)) {
            throw new \Exception("Container item with name '{$id}' not found");
        }

        return $this->container->get($id);
    }

    /**
     * Adds flash message
     *
     * @param string $message
     * @param string $type
     *
     * @return void
     */
    public function addSessionMessage($message, $type = 'NOTICE')
    {
        $this->container->get('session')->getFlashBag()->add($type, $message);
    }
    
}