<?php

namespace Arzttermine\Controller;

use Arzttermine\Application\Application;
use Arzttermine\Appointment\Appointment;
use Arzttermine\Booking\Blacklist;
use Arzttermine\Booking\Booking;
use Arzttermine\Booking\Exception\NonceExistsException;
use Arzttermine\Calendar\Calendar;
use Arzttermine\Core\Configuration;
use Arzttermine\Core\DateTime;
use Arzttermine\Form\BookingType;
use Arzttermine\Form\Form;
use Arzttermine\Insurance\Provider;
use Arzttermine\Integration\Integration;
use Arzttermine\Location\Location;
use Arzttermine\Mail\AdminMailer;
use Arzttermine\MedicalSpecialty\MedicalSpecialty;
use Arzttermine\MedicalSpecialty\TreatmentType;
use Arzttermine\Coupon\CouponService;
use Arzttermine\User\User;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Security\Core\Exception\NonceExpiredException;

class BookingController extends Controller {

    /**
     * Process form data and place the booking
     * 
     * @param FormInterface $form
     * @param User $user
     * @param Location $location
     *
     * @return Booking
     */
    protected function placeBooking(FormInterface $form, User $user, Location $location)
    {
        global $CONFIG;

        $view = $this->getView();
        $session = $this->getSession();
        $is_mobile = $this->get('context')->isMobile();

        // Our booking (blank)
        $booking = new Booking();

        if($session->has('widget_source_platform')) {
            $app = Application::getInstance();
            $app->setSourcePlatform($session->get('widget_source_platform'));
        }


        // Lock the form's data
        $form->submit($form->getData());
        
        if (!$form->isValid()) {
            $errorkeys = array_keys(Form::getFormErrors($form));
            if($form->getErrors()->count() > 0) {
                $contact_pref_error = $form->getErrors()->current()->getOrigin()->getConfig()->getName();
                if ($contact_pref_error == 'contact_preference') {
                    $errorkeys[] = 'contact_preference';
                }
            }
            $view->setRef('error_messages', $errorkeys); // @todo Don't rely on this
            $view->addMessage('STATUS', 'ERROR', $this->trans('Bitte prüfen Sie Ihre Eingaben'));
            return $booking;
        }

        // the form uses "form[referral_code]" but the CouponService listens to param "rc"
        // so inject the referral_code if there is one
        if ($form->get('referral_code')->getData()) {
            $this->get('coupon_service')->retrieveCoupon($form->get('referral_code')->getData());
            if($this->get('coupon_service')->getCoupon()->isValid() === false) {
                $view->setRef('error_messages', array('referral_code'));
                $view->addMessage('STATUS', 'ERROR', $this->trans('Bitte prüfen Sie Ihre Eingaben'));
                return $booking;
            }
        }
        
        // Don't proceed if patient is blacklisted
        if ($this->patientIsBlacklisted($form, $is_mobile)) {
            return $booking;
        }

        // Check if there is already a booking with this form_once
        if (Booking::nonceExists($form->get('form_once')->getData())) {
            $view->addMessage('STATUS', 'NOTICE', $this->trans('Diese Buchung wurde bereits erstellt.'));
            return $booking;
        }

        $msid = $form->get('medical_specialty_id')->getData();
        $ttid = $form->get('treatmenttype_id')->getData();
        $insurance_id = $form->get('insurance_id')->getData();
        $startDateTime = $form->get('start_at_id')->getData();

        $appointment = new Appointment();
        $appointment
            ->setLocation($location)
            ->setUser($user)
            ->setInsuranceIds($insurance_id)
            ->setTreatmentTypeIds($ttid)
            ->setStartTime($startDateTime);

        // Check if the TreatmentType is valid for this appointment
        if ($location->getIntegration()->appointmentIsValid($appointment) !== true) {
            $errorMessage = $location->getIntegration()->getLastErrorMessage();
            $view->addMessage('STATUS', 'ERROR', $this->trans($errorMessage));
            return $booking;
        }

        // Force the medical specialty to match the treatment type
        if ($ttid) {
            $treatmentType = new TreatmentType($ttid);
            if ($treatmentType->isValid()) {
                $msid = $treatmentType->getMedicalSpecialtyId();
            }
        }
        
        // If the MS is *still* empty, force a default
        if (empty($msid)) {
            $msid = $user->getDefaultMedicalSpecialty()->getId();
        }

        // Don't book if we need to validate the treatment type first
        if ($location->mustValidateTreatmentType()) {
            if (!$session->has('Location_TreatmentType_Validated')) {
                $session->set('Location_TreatmentType_Validated', true);
                return $booking;
            } else {
                $session->remove('Location_TreatmentType_Validated');
            }
        }

        // Don't book if a similar booking exists
        if ($this->hasSimilarBookings($form, $msid, $is_mobile)) {
            return $booking;
        }

        // Process the given referral code
        // @todo Move to another point in the process
        if (!$this->checkCouponCode($form, $user, $location, $msid)) {
            return $booking;
        }

        // Hack
        $booking_data = array_merge(
            $form->getData(), array(
                'user'                  => $user,
                'location'              => $location,
                'medical_specialty_id'  => $msid // NOT NICE. @todo: Fix
            )
        );
        
        $booking->create($booking_data);
        
        if ($booking->isValid()) {
            if ($booking->integrationBook()) {
                
                // todo: remove the appointment (or flag it as taken?) from the appoitments table !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                if ($this->get('coupon_service')->hasCoupon()) {
                    Booking::sendBookingEmail($booking, 'booking-new-coupon.html');
                    $this->get('coupon_service')->markAsUsed($booking);
                    
                    // Push the coupon code to salesforce
                    // @todo: Trigger an event instead
                    $booking->updateSalesforce();
                }

                // set a flag that the booking should show the google data layer code for "page" only once
                $session->set('Booking_First_Page_View', true);

                if($CONFIG['MADMIMI_ENABLE'] && $booking->getNewsletterSubscription()) {
                    $booking->sendToMadmimi($booking->getEmail(), $booking->getName());
                }

                return $booking;
            }

            $view->addMessage('STATUS', 'ERROR', $booking->getLastErrorMessage());
        } else {
            $view->addMessage('STATUS', 'ERROR', $this->trans('Die Buchung konnte nicht plaziert werden.'));
        }

        return $booking;
    }

    /**
     * @param FormInterface $form
     * @param bool $is_mobile
     *
     * @return bool
     */
    protected function patientIsBlacklisted(FormInterface $form, $is_mobile = false)
    {
        $view = $this->getView();
        $blacklist = new Blacklist();

        if ($blacklist->hasUser(
            $form->get('email')->getData(),
            $form->get('phone')->getData(),
            $this->getRequest()->getClientIp()
        )) {
            $blacklist->loadUser(
                $form->get('email')->getData(),
                $form->get('phone')->getData(),
                $this->getRequest()->getClientIp()
            );
            $message = $blacklist->getMessage();
            if (empty($message)) {
                $message = 'notice.error.booking_patient_is_blacklisted_default';
            }
            $message = $this->trans($message);
            if ($is_mobile) {
                $view->addMessage('STATUS', 'ERROR', $message);
            } else {
                $view
                    ->setRef('blackListPatient', true)
                    ->setRef('blackListPatientMessage', $message);
            }

            return true;
        }

        return false;
    }

    /**
     * @param FormInterface $form
     * @param int $medical_specialty_id
     * @param bool $is_mobile
     *
     * @return bool
     */
    protected function hasSimilarBookings(FormInterface $form, $medical_specialty_id, $is_mobile = false)
    {
        $view = $this->getView();

        if (
        (new Booking())->isSimilar(
            $form->get('email')->getData(),
            $form->get('appointment_start_at')->getData(),
            $medical_specialty_id
        )
        ) {
            $similar_bookings_message = $this->trans('Laut unserem System haben Sie bereits eine ähnliche Terminbuchung durchgeführt. Wenn Sie Ihren bestehenden Termin verändern wollen, loggen Sie sich bitte ein und stornieren Sie den Termin, bevor Sie fortfahren. Oder wenden Sie sich für weitere Hilfe an unsere kostenlose Kunden-Hotline 0800 2222 133.');

            if ($is_mobile) {
                $view->addMessage('STATUS', 'ERROR', $similar_bookings_message);
            } else {
                $view
                    ->setRef('patientHasSimilarBooking', true)
                    ->setRef('patientSimilarBookingMessage', $similar_bookings_message);
            }

            return true;
        }

        return false;
    }

    /**
     * Handles the common requests
     *
     * @todo: POST to a separate action
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $view = $this->getView();
        $request = $this->getRequest();
        $session = $this->getSession();

        // Clear current session messages
        $session->getFlashBag()->clear();

        // Enable coupon codes
        $this->get('coupon_service')->enable('booking');

        // Deactivate all social media on this side to prevent making the url or referrer public
        $GLOBALS['CONFIG']['SOCIALMEDIA_ACTIVATE'] = false;

        $form_data = array();

        // GET variables
        if ($request->isGet()) {
            $form_data = array(
                'location_id'          => $request->getQuery('l'),
                'user_id'              => $request->getQuery('u'),
                'insurance_id'         => $request->getQuery('i'),
                'medical_specialty_id' => $request->getQuery('m'),
                'treatmenttype_id'     => $request->getQuery('tt'),
                'start_at_id'          => $request->getQuery('s'),
                'end_at_id'            => $request->getQuery('e'),
                'appointment_start_at' => Calendar::UTC2DateTime($request->getQuery('s')),
                'appointment_end_at'   => Calendar::UTC2DateTime($request->getQuery('e')),
                'form_once'            => Booking::getNonce(),
                'referral_code'        => $this->get('coupon_service')->getCoupon()->getCode()
            );
            
            $view->setRef('error_messages', []);
        } elseif ($request->isPost()) {
            $form_data = $request->getRequest('form');
            // Hack
            // @todo Migrate to one calendar format EVERYWHERE
            $form_data['appointment_start_at'] = Calendar::UTC2DateTime($form_data['start_at_id']);
            $form_data['appointment_end_at'] = Calendar::UTC2DateTime($form_data['end_at_id']);
        } else {
            // It's not a GET or POST ...? We don't know what's going on
            $this->notFound();
        }

        //preselect contact_preference
        if(!isset($form_data['contact_preference'])) {
            $form_data['contact_preference'] = array('email');
        }

        // The booking form
        $form = Form::getForm(new BookingType());
        $form->setData($form_data);

        // Bind the User and Location objects
        $user = new User($form->get('user_id')->getData());
        $location = new Location($form->get('location_id')->getData());

        if (!$user->isValid() || !$location->isValid() || !$user->isMemberOf(GROUP_DOCTOR)) {
            return $this->redirectTo('home', array(), array(
                'status' => 'NOTICE',
                'text'   => $this->trans('Ups, etwas ist schiefgegangen.')
            ));
        }

        // Set available insurers for the User and Location
        $view->setRef(
            'available_insurance_ids', $location->getAvailableInsuranceIds($user, $form->get('appointment_start_at')->getData())
        );

        // Check that the integration is available
        $integration = Integration::getIntegration($location->getIntegrationId());

        if (!$integration->isConnected($user->getId(), $location->getId())) {
            return $this->redirectTo('home', array(), array(
                'status' => 'NOTICE',
                'text'   => $this->trans('Ups, etwas ist schiefgegangen.')
            ));
        }

        $view->setRef('integration_id', $integration->getId());

        // If it's a POST, make the booking
        if ($request->isPost()) {
            $booking = $this->placeBooking($form, $user, $location);

            if ($booking->isValid()) {
                return $this->redirect($booking->getUrl());
            }
        }

        // Prepare and show the view
        return $view
            ->setRef('booking_agb_confirmation_text', $this->trans('Ich stimme der Verwendung meiner Daten zur Vermittlung meines Termins unter den in der <a target="_blank" href="/datenschutz">Datenschutzerklärung</a> genannten Bedingungen zu. Arzttermine.de darf insbesondere meine Daten an den Arzt weiterleiten und dieser rückübermitteln, ob ich den Termin wahrgenommen habe.'))
            ->setRef('booking_newsletter_text', $this->trans('Newsletter abonnieren'))
            ->setRef('contentMain', $this->getFormContent($form, $user, $location, $request->isPost()))
            ->setRef('form', $form->getData())
            ->setRef('user', $user)
            ->setRef('location', $location)
            ->setRef('medical_specialty_id', (empty($form_data['medical_specialty_id']) ? 0 : $form_data['medical_specialty_id']))
            ->setRef('medical_specialty_name', MedicalSpecialty::getNameById($form_data['medical_specialty_id']))
            ->setRef('treatment_type_id', $form_data['treatmenttype_id'])
            ->setRef('treatment_type_name', TreatmentType::getNameStatic($form_data['treatmenttype_id']))
            ->setRef('googleTagManagerDataLayerJs', $view->fetch('js/tagmanager/datalayer/booking.tpl'))
            ->setRef('facebook_tracking_js', "fbq('track', 'InitiateCheckout');\n")

            ->addHeadTitle('Termin buchen bei ' . title($user->getName()))
            ->setMetaTag('description', 'Nutzer') // @todo Better description/keywords
            ->setMetaTag('keywords', 'Nutzer')
            ->disableCaching()
            ->display('booking/booking.tpl');
    }

    /**
     * Manage the referral codes passed into the form
     *
     * @param FormInterface $form
     * @param User $user
     * @param Location $location
     * @param int $medical_specialty_id
     * 
     * @return bool
     */
    protected function checkCouponCode(FormInterface $form, User $user, Location $location, $medical_specialty_id)
    {
        /** @var CouponService $coupon_service */
        $coupon_service = $this->get('coupon_service');
        $coupon = $coupon_service->getCoupon();
        
        // If there are any restrictions, don't process the code
        if ($coupon_service->hasCoupon() && !$coupon->isApplicable($user->getId(), $location->getId(), $medical_specialty_id, $form->get('email')->getData())) {
            $last_error = $coupon->getLastError();
            $this->getView()->addMessage('STATUS', 'ERROR', $last_error['message']);
            return false;
        }
        
        return true;
    }

    /**
     * Stub
     *
     * @todo Transfer post-only actions here
     */
    public function placeAction()
    {
        $this->indexAction();
    }

    /**
     * Generates the HTML content for the form
     *
     * @todo Refactor out. Total mess
     *
     * @param FormInterface $form
     * @param User $user
     * @param Location $location
     * @param bool $check_for_errors Just get the form, or check it as well?
     *
     * @return string
     */
    public function getFormContent(FormInterface $form, User $user, Location $location, $check_for_errors = true)
    {
        $app = Application::getInstance();
        $view = $this->getView();

        $errors = array();

        // @todo: check if there is a global way to have a url prefix for widgets
        $urlPrefix = '';
        if ($app->isWidget()) {
            $urlPrefix = '/w/' . $app->getWidgetContainer()->getSlug();
        }
        $view->setRef('url_form_action', $urlPrefix . '/termin');
        $view->setRef('url_cancel', $this->getRequest()->getRequestUri());

        if ($check_for_errors) {
            if (!$form->get('gender')->isValid()) {
                $errors[] = 'gender';
            }

            if (!$form->get('first_name')->isValid()) {
                $errors[] = 'first_name';
            }

            if (!$form->get('last_name')->isValid()) {
                $errors[] = 'last_name';
            }

            if (!$form->get('email')->isValid()) {
                $errors[] = 'email';
            }

            if (!$form->get('phone')->isValid()) {
                $errors[] = 'phone';
            }

            if (!$form->get('insurance_id')->isValid()) {
                $errors[] = 'insurance_id';
            }

            if (!$form->get('returning_visitor')->isValid()) {
                $errors[] = 'returning_visitor';
            }

            if (!$form->get('agb')->isValid()) {
                $errors[] = 'agb';
            }

            if (!$form->isSubmitted()) {
                $form->get('comment_patient')->setData(sanitize_textarea_field($form->get('comment_patient')->getData()));
            }

            if ($form->get('referral_code')->getData() && !$form->get('referral_code_confirmation')->getData()) {
                $errors[] = 'referral_code_confirmation';
            }
        }

        $treatmentType = new TreatmentType();
        if ($location->getIntegrationId() == 5) { // Magic Samedi number
            // @todo: we assume that the insurance_id is already set and valid at this stage.
            $appointment = new Appointment();
            $appointment
                ->setUserId($user->getId())
                ->setLocationId($location->getId())
                ->setInsuranceIds(array($form->get('insurance_id')->getData()))
                ->setStartTime($form->get('appointment_start_at')->getData());
            $integration = new Integration\Samedi\Integration();
            $treatmentTypes = $integration->getTreatmentTypesForAppointment($appointment);
            $treatmentTypesOptgroupArray = $treatmentType->getHtmlOptionsByTreatmenttypes($user, $treatmentTypes);
        } else {
            $treatmentTypesOptgroupArray = $treatmentType->getHtmlOptionsForMedicalSpecialties($user, $user->getMedicalSpecialties(), true);
        }

        $view->setRef('treatmenttypes', $treatmentTypesOptgroupArray);
        $view->setRef('insuranceProviders', $this->getInsuranceProviders());

        $view->setRefs(
            array(
                'form'        => $form->getData(),
                'user'        => $user,
                'location'    => $location,
                'form_errors' => $errors
            )
        );

        return $view->fetch('booking/_booking.tpl');
    }

    /**
     * Helper function to return a set of options for the provider box
     *
     * @return array
     */
    private function getInsuranceProviders()
    {
        $data = array();

        $provider = new Provider();
        /** @var Provider[] $providers */
        $providers = $provider->findObjects();

        foreach ($providers as $_provider) {
            $data[$_provider->getId()] = $_provider->getName();
        }

        // Now just tack on the selection
        array_unshift($data, $this->trans('...bitte wählen...'));

        return $data;
    }

    /**
     * View a canceled booking
     *
     *
     * @param string $slug
     */
    public function canceledAction($slug)
    {
        $view = $this->getView();
        $booking = new Booking($slug, 'slug');

        if ($booking->isValid()) {
            $user = new User();
            $user->load($booking->getUserId());
            if ($user->isValid()) {
                $location = new Location();
                $location->load($booking->getLocationId());
                if ($location->isValid()) {

                    if ($this->getRequest()->request->has('phone')) {
                        $booking->setRecallPhone($this->getRequest()->request->get('phone'));
                        $form = $this->getRequest()->request->get('form');
                        $booking->setRecallPreference($form['contact_availability']);
                        $booking->save(array('recall_preference','recall_phone'));

                        //update salesforce
                        $booking->updateSalesforce();

                        return $view
                            ->addHeadTitle('Termin bei ' . title($user->getName()))
                            ->setMetaTag('description', 'Nutzer')
                            ->setMetaTag('keywords', 'Nutzer')
                            ->setRef('user', $user)
                            ->setRef('booking', $booking)
                            ->setRef('location', $location)
                            ->setRef('weekDate', $booking->getAppointmentStartAt())
                            ->setRef('weekDay', date("d", (strtotime($booking->getAppointmentStartAt()))))
                            ->disableCaching()
                            ->display('booking_profile/booking_recontact.tpl');
                    } else {
                        return $view
                            ->addHeadTitle('Termin bei ' . title($user->getName()))
                            ->setMetaTag('description', 'Nutzer')
                            ->setMetaTag('keywords', 'Nutzer')
                            ->setRef('user', $user)
                            ->setRef('booking', $booking)
                            ->setRef('location', $location)
                            ->setRef('weekDate', $booking->getAppointmentStartAt())
                            ->setRef('weekDay', date("d", (strtotime($booking->getAppointmentStartAt()))))
                            ->disableCaching()
                            ->display('booking_profile/booking_canceled.tpl');
                    }
                }
            }
        }
        $this->notFound();
    }

    /**
     * View a previously made booking (booking profile)
     *
     * @todo Refactor
     *
     * @param string $slug
     */
    public function viewAction($slug)
    {
        #$app = Application::getInstance();
        $view = $this->getView();
        $request = $this->getRequest();
        $session = $this->getSession();

        // These are optional and are just added with a query string
        $extension = $request->getQuery('type', '');
        $action = $request->getQuery('action', 'view');

        $GLOBALS['CONFIG']['SOCIALMEDIA_ACTIVATE'] = false;
        $template = 'error';

        $user = new User();
        $location = new Location();
        $booking = new Booking($slug, 'slug');

        if ($booking->isValid()) {
            // load location and user
            $location->load($booking->getLocationId());
            if ($location->isValid()) {
                $user->load($booking->getUserId());
                if ($user->isValid()) {
                    switch ($extension) {
                        case 'ics':
                            $template = 'ics';
                            break;
                        default:
                            $view->setRef('location', $location);
                            $view->setRef('user', $user);
                            $view->setRef('booking', $booking);

                            if ($session->has('Booking_First_Page_View')) {
                                $session->remove('Booking_First_Page_View');
                                $page = '/buchung';
                            } else {
                                $page = '/buchung-profil';
                            }
                            $view->setRef('dataLayerPage', $page);
                            $view->setRef('googleTagManagerDataLayerJs', $view->fetch('js/tagmanager/datalayer/booking-profile.tpl'));
                            $view->setRef('facebook_tracking_js', "fbq('track', 'Purchase', {value: '0.00', currency: 'EUR'});");


                            // @todo Remove all mailings and FIX
                            // Check for cancellation action
                            switch ($action) {
                                case 'stornieren':
                                    if ($booking->getStatus() == Booking::STATUS_PENDING_BOOKING_OFFER_EXPIRED) {
                                        // patient cant cancel automatically after the booking offer has expired
                                        // patient support has not finished/touched that booking again

                                        // but in any case: inform patient support that patient hit that button and MAYBE wants to
                                        // cancel the appointment anyway
                                        AdminMailer::send(
                                            $GLOBALS['CONFIG']['BOOKINGS_EMAIL_ADDRESS'],
                                            "Buchung stornieren: Der Patient hat einen Terminvorschlag für die BookingId {$booking->getId()} akzeptiert obwohl sie bereits abgelaufen war. Unbedingt prüfen und Patient kontaktieren.\n{$booking->getAdminEditUrl()}\n{$booking->getSalesforceUrl()}",
                                            'Terminvorschlag: Patient hat auf Terminvorschlag stornieren geklickt, obwohl die Buchung im Status Terminvorschlag abgelaufen war',
                                            __FILE__,
                                            __LINE__
                                        );
                                        $this->redirectTo('home', array(), array(
                                            'status' => 'ERROR',
                                            'text'   => 'Ihre Buchung konnte nicht storniert werden, weil der Terminvorschlag bereits abgelaufen war.<br />Bitte rufen Sie unseren kostenlosen Support an: ' . $GLOBALS['CONFIG']['PHONE_SUPPORT']
                                        ));
                                    }

                                    if ($booking->getStatus() == Booking::STATUS_PENDING_BOOKING_OFFER_ACCEPTED) {
                                        // patient cant cancel automatically after he already accepted the booking offer and
                                        // patient support has not finished/touched that booking again

                                        // but in any case: inform patient support that patient hit that button and MAYBE wants to
                                        // cancel the appointment anyway
                                        AdminMailer::send(
                                            $GLOBALS['CONFIG']['BOOKINGS_EMAIL_ADDRESS'],
                                            "Buchung stornieren: Der Patient hat einen Terminvorschlag für die BookingId {$booking->getId()} akzeptiert und danach auch auf den Link 'Termin löschen' geklickt. Unbedingt prüfen und Patient kontaktieren.\n{$booking->getAdminEditUrl()}\n{$booking->getSalesforceUrl()}",
                                            'Terminvorschlag: Patient hat Antwort gewechselt',
                                            __FILE__,
                                            __LINE__
                                        );
                                        $this->redirectTo('home', array(), array(
                                            'status' => 'ERROR',
                                            'text'   => 'Ihre Buchung konnte nicht storniert werden, weil Sie einem Terminvorschlag bereits zugesagt hatten.<br />Bitte rufen Sie unseren kostenlosen Support an: ' . $GLOBALS['CONFIG']['PHONE_SUPPORT']
                                        ));
                                    }

                                    // as we use the same link to cancel but have more than one booking status for that we
                                    // need this switch here
                                    if ($booking->hasUnansweredBookingOffers()) {
                                        // if there are unanswered booking offers then tell patient support to check that first
                                        $status = Booking::STATUS_PENDING_BOOKING_OFFER_DECLINED;
                                    } else {
                                        $status = Booking::STATUS_CANCELLED_BY_PATIENT;
                                    }


                                    if ($booking->cancel($status)) {
                                        // inform patient support that patient cancelled the appointment
                                        AdminMailer::send(
                                            $GLOBALS['CONFIG']['BOOKINGS_EMAIL_ADDRESS'],
                                            "Buchung stornieren: Der Patient hat die Buchung mit der BookingId {$booking->getId()} storniert.\n{$booking->getAdminEditUrl()}\n{$booking->getSalesforceUrl()}",
                                            "Buchung stornieren: Der Patient hat die Buchung mit der BookingId {$booking->getId()} storniert",
                                            __FILE__,
                                            __LINE__
                                        );
                                        return $this->redirectTo('booking_canceled', array('slug'=>$slug));
                                    } else {
                                        $this->redirectTo('home', array(), array(
                                            'status' => 'ERROR',
                                            'text'   => 'Ihre Buchung konnte nicht storniert werden. Bitte rufen Sie unseren kostenlosen Support an: ' . $GLOBALS['CONFIG']['PHONE_SUPPORT']
                                        ));
                                    }

                                    break;
                            }

                            $template = 'booking';
                            break;
                    }
                }
            }
        }

        // ********************************************************************
        switch ($template) {
            case 'ics':
                $userName = $user->getName();
                $locationAddress = $location->getAddress(', ');
                $appointment = $booking->getAppointment();
                // add 30 min just to have an end
                $datetimeEnd = new DateTime($appointment->getDateTime());
                $datetimeEnd->add(new \DateInterval('PT30M'));
                $appointment->setDateTimeEnd($datetimeEnd->format('Y-m-d H:i:s'));

                // credits to: http://www.lemnatec.com/sites/all/libraries/iCalcreator/summary.html
                $config = array("unique_id" => "Arzttermine.de"); // set Your unique id
                $v = new \vcalendar($config); // create a new calendar instance
                $v->setProperty("method", "PUBLISH"); // required of some calendar software
                $v->setProperty("x-wr-calname", "Arzttermin"); // required of some calendar software
                $v->setProperty("X-WR-CALDESC", "Ihr Arzttermin"); // required of some calendar software
                $tz = "Europe/Berlin";
                $v->setProperty("X-WR-TIMEZONE", $tz); // required of some calendar software

                $vevent = & $v->newComponent("vevent"); // create an event calendar component
                $vevent->setProperty(
                    "dtstart", array(
                        "year"  => $appointment->getFormat('Y'),
                        "month" => $appointment->getFormat('m'),
                        "day"   => $appointment->getFormat('d'),
                        "hour"  => $appointment->getFormat('H'),
                        "min"   => $appointment->getFormat('i'),
                        "sec"   => $appointment->getFormat('s')
                    )
                );
                $vevent->setProperty(
                    "dtend", array(
                        "year"  => $appointment->getEndFormat('Y'),
                        "month" => $appointment->getEndFormat('m'),
                        "day"   => $appointment->getEndFormat('d'),
                        "hour"  => $appointment->getEndFormat('H'),
                        "min"   => $appointment->getEndFormat('i'),
                        "sec"   => $appointment->getEndFormat('s')
                    )
                );
                $vevent->setProperty("LOCATION", $locationAddress); // property name - case independent
                $vevent->setProperty("URL", $location->getUrl(true));
                $vevent->setProperty("summary", "Arzttermin bei " . $userName);
                $vevent->setProperty("description", "Arzttermine.de: " . $GLOBALS['CONFIG']['PHONE_SUPPORT'] . " Bitte rufen Sie uns kostenfrei an, falls Sie den Termin nicht wahrnehmen können.");
                $vevent->setProperty("comment", "Arzttermine.de - schnell. einfach. online.");

                $v->returnCalendar();

                exit;

            case 'booking':
                return $view
                    ->addHeadTitle('Termin bei ' . title($user->getName()))
                    ->setMetaTag('description', 'Nutzer')
                    ->setMetaTag('keywords', 'Nutzer')
                    ->setRef('weekDate', $booking->getAppointmentStartAt())
                    ->setRef('weekDay', date("d", (strtotime($booking->getAppointmentStartAt()))))
                    ->setRef('facebook_tracking_js', "fbq('track', 'Lead');\n")
                    ->disableCaching()
                    ->display('booking_profile/booking_profile.tpl');
                break;

            default:
            case 'error':
                $this->notFound();
                break;
        }
    }
}
