<?php

namespace Arzttermine\Controller;

use Arzttermine\Admin\Admin;
use Arzttermine\Admin\AdminModuleInterface;
use Arzttermine\Admin\Module\Blacklists;
use Arzttermine\Admin\Module\BookingOffers;
use Arzttermine\Admin\Module\Bookings;
use Arzttermine\Admin\Module\CMS;
use Arzttermine\Admin\Module\Configuration;
use Arzttermine\Admin\Module\Contacts;
use Arzttermine\Admin\Module\Coupons;
use Arzttermine\Admin\Module\Dashboard;
use Arzttermine\Admin\Module\Districts;
use Arzttermine\Admin\Module\Groups;
use Arzttermine\Admin\Module\Insurances;
use Arzttermine\Admin\Module\Locations;
use Arzttermine\Admin\Module\LoginHistory;
use Arzttermine\Admin\Module\Mailings;
use Arzttermine\Admin\Module\Media;
use Arzttermine\Admin\Module\MediaFrame;
use Arzttermine\Admin\Module\MedicalSpecialties;
use Arzttermine\Admin\Module\Messages;
use Arzttermine\Admin\Module\News;
use Arzttermine\Admin\Module\Planners;
use Arzttermine\Admin\Module\Reviews;
use Arzttermine\Admin\Module\SEO;
use Arzttermine\Admin\Module\TempUsers;
use Arzttermine\Admin\Module\TreatmentTypes;
use Arzttermine\Admin\Module\UsedCoupons;
use Arzttermine\Admin\Module\Users;
use Arzttermine\Admin\Module\Widgets;

/**
 * This class wraps procedural admin modules
 *
 * @package Arzttermine\Controller\Admin
 */
class AdminController extends Controller {

    /**
     * @var Admin
     */
    protected $admin;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->admin = new Admin();
    }

    /**
     * @param AdminModuleInterface $module
     * 
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function showModule(AdminModuleInterface $module)
    {
        // Container 
        $this->admin->setContainer($this->container);
        
        // Load the module into the Admin wrapper
        $this->admin->setModule($module);
        
        // Manually boot config here as we can modify the class further
        $this->admin->initialise();

        // Check we're allowed to be here or not
        $this->checkPrivilege();
        
        return $this->admin->display();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dashboardAction()
    {
        return $this->showModule(new Dashboard());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function blacklistsAction()
    {
        return $this->showModule(new Blacklists());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function bookingsAction()
    {
        return $this->showModule(new Bookings());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function bookingOffersAction()
    {
        return $this->showModule(new BookingOffers());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function cmsAction()
    {
        return $this->showModule(new CMS());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function contactsAction()
    {
        return $this->showModule(new Contacts());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function configurationAction()
    {
        return $this->showModule(new Configuration());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function couponsAction()
    {
        return $this->showModule(new Coupons());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function usedCouponsAction()
    {
        return $this->showModule(new UsedCoupons());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function districtsAction()
    {
        return $this->showModule(new Districts());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function groupsAction()
    {
        return $this->showModule(new Groups());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function insurancesAction()
    {
        return $this->showModule(new Insurances());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function locationsAction()
    {
        return $this->showModule(new Locations());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginHistoryAction()
    {
        return $this->showModule(new LoginHistory());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function mailingsAction()
    {
        return $this->showModule(new Mailings());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function medicalSpecialtiesAction()
    {
        return $this->showModule(new MedicalSpecialties());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newsAction()
    {
        return $this->showModule(new News());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function reviewsAction()
    {
        return $this->showModule(new Reviews());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function seoAction()
    {
        return $this->showModule(new SEO());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function tempUsersAction()
    {
        return $this->showModule(new TempUsers());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function treatmentTypesAction()
    {
        return $this->showModule(new TreatmentTypes());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function usersAction()
    {
        return $this->showModule(new Users());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function plannersAction()
    {
        return $this->showModule(new Planners());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function widgetsAction()
    {
        return $this->showModule(new Widgets());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function mediaAction()
    {
        return $this->showModule(new Media());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function mediaFrameAction()
    {
        return $this->showModule(new MediaFrame());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function messagesAction()
    {
        return $this->showModule(new Messages());
    }
    
    /**
     * The user has been disallowed access, so push them here
     */
    public function deniedAction()
    {
        $view = $this->getView();
        $view
            ->addHeadTitle('Denied')
            ->setRef('ContentRight', $view->fetch('denied.tpl'));

        return $view->display('admin/content_left_right.tpl');
    }

    /**
     * @return bool
     */
    protected function checkPrivilege()
    {
        $module = $this->admin->getModule();
        
        if (
            $module instanceof AdminModuleInterface &&
            !$module->checkPrivilege()
        ) {
            $this->redirectTo('admin_denied');
        }
    }
}