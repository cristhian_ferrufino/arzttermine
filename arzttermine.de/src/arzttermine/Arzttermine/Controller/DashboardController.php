<?php

namespace Arzttermine\Controller;

use Arzttermine\Application\Application;
use Arzttermine\Appointment\AppointmentGeneratorService;
use Arzttermine\Appointment\Planner;
use Arzttermine\Booking\Booking;
use Arzttermine\Core\DateTime;
use Arzttermine\DataTransformer\PlannerBlockAppointments;
use Arzttermine\Form\DoctorProfileType;
use Arzttermine\Form\DoctorRegistrationType;
use Arzttermine\Form\Form;
use Arzttermine\Form\PracticeProfileType;
use Arzttermine\Location\Location;
use Arzttermine\Mail\Mailing;
use Arzttermine\Media\Asset;
use Arzttermine\MedicalSpecialty\MedicalSpecialty;
use Arzttermine\Statement\Statement;
use Arzttermine\User\PendingUser;
use Arzttermine\User\User;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DashboardController extends Controller {

    /**
     * Check a user is logged in/allowed to be here and redirect if not
     * @todo Move to a authentication handler
     * 
     * @param User $doctor
     * 
     * @return bool
     */
    private function checkIdentity(User $doctor)
    {
        $app = Application::getInstance();
        
        if (!$this->checkUser($doctor)) {
            $app->getCurrentUser()->logout();
            $message = array('status' => 'ERROR', 'text' => 'Sie müssen angemeldet sein um diese Seite sehen zu können.');
            
            if ($doctor->getStatus() == User::STATUS_DRAFT) {
                $message = array('status' => 'ERROR', 'text' => 'Ihr Konto wurde deaktiviert. Bitte kontaktieren Sie unseren Support.');
            }

            $app->addFlashMessage($message);
            
            return false;
        }
        
        return true;
    }

    /**
     * Check the current user is valid
     *
     * @param User $user
     *
     * @return bool
     */
    private function checkUser(User $user)
    {
        return
            (
             $user->isValid() &&
             $user->hasProfile()) &&
             in_array(
                 $user->getStatus(), array(
                     User::STATUS_ACTIVE,
                     User::STATUS_VISIBLE,
                     User::STATUS_VISIBLE_APPOINTMENTS)
             ) &&
            $user->isMemberOf(GROUP_DOCTOR);
    }

    /**
     * Log the doctor into the Doctor's area
     * @todo Check doctors that haven't been activated
     * @todo Add login history row
     * @todo Refactor/abstract with LoginController:loginAction
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAction()
    {
        $view = $this->getView();
        $request = $this->getRequest();
        $profile = $this->getUser();

        if ($request->isPost()) {
            // Log out any existing profile
            $profile->logout();
            
            if ($profile->login($request->getRequest('email'), $request->getRequest('password'))) {
                return $this->redirect($request->getQuery('referrer', $this->generateUrl('doctor_account')));
            } else {
                $this->addSessionMessage('Bitte überprüfen Sie Ihre E-Mail-Adresse/Passwort', 'ERROR');
            }
        } elseif ($this->checkUser($profile)) {
            // redirect to profile if doctor is logged in
            $this->redirectTo('doctor_account');
        }

        return $view
            ->setRef('email', $request->getRequest('email'))
            ->addHeadTitle('Login')
            ->disableCaching()
            ->display('doctor/dashboard/login.tpl');
    }

    /**
     * Doctor's area main page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function accountAction()
    {
        $app = Application::getInstance();
        $request = $app->getRequest();
        $router = $app->container->get('router');
        $view = $app->getView();

        $errors = array();
        $ms = new MedicalSpecialty();

        $slug = $request->getQuery('arzt');
        $doctor = new User($slug, 'slug');

        if (!$doctor->isValid()) {
            $doctor = $app->getCurrentUser();
        }
        
        if (!$this->checkIdentity($app->getCurrentUser())) {
            return $this->redirectTo('doctor_login');
        }

        // The editor (logged-in user) must be a member of this practice doctor to edit it
        if (!$doctor->isEditAllowed($app->getCurrentUser()->getId())) {
            return $app->redirect($router->generate('doctor_login'), array('status' => 'ERROR', 'text' => 'Sie haben keinen Zugriff auf dieses Profil.'));
        }

        $practices = $doctor->getLocations();

        if ($request->isPost()) {
            $errors = $this->saveProfile($doctor);
        }

        $view->setRefs(array(
            'doctor' => $doctor,
            'doctors' => $this->getAllDoctors($practices),
            'locations' => $practices,
            'slug' => $doctor->getSlug(),
            'specialties' => $doctor->getMedicalSpecialtyIds(),
            'all_specialties' => $ms->findObjects(),
            'error_messages' => $errors,
            'errors' => array_keys($errors)
        ));
        
        return $view
            ->disableCaching()
            ->display('doctor/dashboard/account.tpl');
    }

    /**
     * @param Location[] $locations
     *
     * @return array
     */
    private function getAllDoctors($locations = array())
    {
        $doctors = array();

        foreach ($locations as $location) {
            $_doctors = $location->getMembers();
            /** @var User[] $_doctors */
            foreach ($_doctors as $_doctor) {
                $doctors[$_doctor->getId()] = $_doctor;
            }
        }

        return $doctors;
    }

    /**
     * @param User $user
     *
     * @return array
     */
    private function saveProfile(User $user)
    {
        $app = Application::getInstance();
        $request = $app->getRequest();
        $errors = array();

        // Defaults for data we're expecting
        // @todo: Handle *more* in the form object
        $defaults = array(
            'uid'        => 0, // User being edited. Editor comes from session
            'title'      => '',
            'first_name' => '',
            'last_name'  => '',
            'docinfo_1'  => '',
            'docinfo_2'  => '',
            'docinfo_3'  => '',
            'docinfo_4'  => '',
            'docinfo_5'  => '',
            'docinfo_6'  => '',
            'current_password'   => '',
            'new_password'       => '',
            'treatment_types'    => array(),
            'medical_specialties_ids' => array()
        );

        // Combine parameters from the query (POST) that match the keys in $defaults
        $parameters = array_replace_recursive($defaults, $request->getRequest());

        $form = Form::getForm(new DoctorProfileType());
        $form->submit($parameters);

        // The user is trying to change their password, so do the following little dance
        // --------------------------------------------------------------------------------
        $old_password = $form->get('current_password')->getData();
        $new_password = $form->get('new_password')->getData();

        // We have an old...
        if ($old_password) {
            // ... but no new
            if (!$new_password) {
                $form->get('new_password')->addError(new FormError('Bitte geben Sie ein neues Passwort ein'));
            }

            // ... but it's wrong
            if (!$user->checkCredentials($user->getEmail(), $old_password)) {
                $form->get('current_password')->addError(new FormError('Das aktuelle Passwort ist nicht korrekt'));
            }
        }

        // We have a new, but no old
        if ($new_password && !$old_password) {
            $form->get('current_password')->addError(new FormError('Bitte geben Sie Ihr aktuelles Passwort ein'));
        }
        // --------------------------------------------------------------------------------

        if ($form->isValid()) {
            // Grab the form data once it's submitted and validated
            $data = $form->getData();

            // Set above. Indicates we should use the new password
            if ($old_password && $new_password) {
                $user->changePassword($new_password);
            }

            // @todo Check location with GMaps here
            $user
                ->setTitle($data['title'])
                ->setFirstName($data['first_name'])
                ->setLastName($data['last_name'])
                ->setDocinfo1($data['docinfo_1'])
                ->setDocinfo2($data['docinfo_2'])
                ->setDocinfo3($data['docinfo_3'])
                ->setDocinfo4($data['docinfo_4'])
                ->setDocinfo5($data['docinfo_5'])
                ->setDocinfo6($data['docinfo_6'])
                ->setMedicalSpecialtyIds($data['medical_specialties_ids'])
                ->save();

            $user->saveTreatmentTypes($data['treatment_types']);

            $app->addSessionMessage('Ihre Daten wurden aktualisiert');

        } else {
            $errors = Form::getFormErrors($form);
        }

        return $errors;
    }

    /**
     * Doctors' practices
     */
    public function practiceAction()
    {
        $app = Application::getInstance();
        $request = $app->getRequest();
        $view = $app->getView();

        $slug = $request->getQuery('praxis');
        $practice = new Location($slug, 'slug');

        if (!$practice->isValid()) {
            $app->redirect('doctor_account');
        }

        // We will show the locations only for the logged-in user
        $doctor = $app->getCurrentUser();
        $ms = new MedicalSpecialty();

        if (!$this->checkIdentity($doctor)) {
            return $this->redirectTo('doctor_login');
        }

        if ($request->isPost()) {
            // The editor (logged-in user) must be a member of this practice to edit it
            if ($practice->isEditAllowed($app->getCurrentUser()->getId())) {
                $form = Form::getForm(new PracticeProfileType());
                $form->setData($request->getRequest('form'));
                $this->saveLocationProfile($practice, $form);
            } else {
                $app->addSessionMessage('Sie haben keinen Zugriff auf dieses Praxisprofil.', 'ERROR');
            }
        }

        $view->setRefs(array(
                'location' => $practice,
                'doctors' => $practice->getMembers(),
                'locations' => $doctor->getLocations(),
                'slug' => $practice->getSlug(),
                'specialties' => $practice->getMedicalSpecialtyIds(),
                'all_specialties' => $ms->findObjects()
            ));
        
        return $view
            ->disableCaching()
            ->display('doctor/dashboard/practice.tpl');
    }

    /**
     * @param Location $practice
     * @param \Symfony\Component\Form\Form $form
     */
    protected function saveLocationProfile(Location $practice, \Symfony\Component\Form\Form $form)
    {
        $app = Application::getInstance();
        $request = $app->getRequest();

        // Defaults for data we're expecting
        // @todo: Handle *more* in the form object
        $defaults = array(
            'uid'   => 0, // Location being edited. Editor comes from session
            'phone' => '',
            'www'   => '',
            'medical_specialties_ids' => array()
        );

        // Combine parameters from the query (POST) that match the keys in $defaults
        $parameters = array_replace_recursive($defaults, $request->getRequest());

        $form->submit($parameters);

        if ($form->isValid()) {
            // Grab the form data once it's submitted and validated
            $data = $form->getData();

            $practice
                ->setPhone(sqlsave($data['phone']))
                ->setWww(sqlsave($data['www']))
                ->setMedicalSpecialtyIds($data['medical_specialties_ids'])
                ->save();

            $app->addFlashMessage('Die Daten wurden aktualisiert.');

        } else {
            // @todo Do something with these errors
            // $errors = Form::getFormErrors($form);
        }
    }

    /**
     * Display a doctor's planners and appointments
     */
    public function plannerAction()
    {
        $app = Application::getInstance();
        $request = $app->getRequest();
        $view = $app->getView();

        // Try slug-based lookup (normally used on the dashboard)
        $slug = $request->getQuery('arzt');
        $doctor = new User($slug, 'slug');

        // If it failed, try the user ID lookup (based on form data)
        if (!$doctor->isValid()) {
            $doctor->load(intval($request->getParam('user_id')));
        }

        // If THAT failed, just use the logged-in user
        if (!$doctor->isValid()) {
            $doctor = $app->getCurrentUser();
        }

        if (!$this->checkIdentity($app->getCurrentUser())) {
            return $this->redirectTo('doctor_login');
        }

        // We need to display a dropdown of all locations
        $practices = $doctor->getLocations();

        $location_slug = $request->getQuery('praxis');
        $practice = new Location($location_slug, 'slug');

        // If it failed, try the user ID lookup (based on form data)
        if (!$practice->isValid()) {
            $practice->load(intval($request->getParam('location_id')));
        }

        $planner = new Planner();
        $planner = $planner->getForUserLocation($doctor, $practice);

        // We want to save new Planner Blocks...
        if ($request->isPost()) {
            $payload = $request->getRequest('payload');
            $blocks = json_decode($payload, true); // Everything is wrapped in here to avoid input var count errors
            
            $planner_blocks = PlannerBlockAppointments::toBlocks($blocks);
            $planner
                ->clearBlocks()
                ->setBlocks($planner_blocks)
                ->saveBlocks();
            
            // Set and save appointment rules
            $planner
                ->setLeadIn($request->getRequest('lead_in'))
                ->setSearchPeriod($request->getRequest('search_period'))
                ->save(array('lead_in', 'search_period'));

            // Generate appointments immediately
            AppointmentGeneratorService::getInstance()->clear($planner)->generate($planner);
        }

        $view->setRefs(array(
                'doctor' => $doctor,
                'practice' => $practice,
                'doctors' => $this->getAllDoctors($practices),
                'locations' => $practices,
                'slug' => $doctor->getSlug(),
                'planner' => $planner,
                'blocks' => PlannerBlockAppointments::toFullCalendar($planner->getBlocks())
            ));
        
        return $view
            ->disableCaching()
            ->display('doctor/dashboard/planner/planner.tpl');
    }

    /**
     * Show a list of patients/booking for this doctor (backend)
     */
    public function patientsAction()
    {
        $app = Application::getInstance();
        $request = $app->getRequest();
        $view = $app->getView();

        // Try slug-based lookup (normally used on the dashboard)
        $slug = $request->getQuery('arzt');
        $doctor = new User($slug, 'slug');

        // If it failed, try the user ID lookup (based on form data)
        if (!$doctor->isValid()) {
            $doctor->load(intval($request->getParam('user_id')));
        }

        // If THAT failed, just use the logged-in user
        if (!$doctor->isValid()) {
            $doctor = $app->getCurrentUser();
        }

        if (!$this->checkIdentity($app->getCurrentUser())) {
            return $this->redirectTo('doctor_login');
        }

        // We need to display a dropdown of all locations
        $practices = $doctor->getLocations();

        $location_slug = $request->getQuery('praxis');
        $practice = new Location($location_slug, 'slug');

        // If it failed, try the user ID lookup (based on form data)
        if (!$practice->isValid()) {
            $practice->load(intval($request->getParam('location_id')));
        }

        // Display patient/booking data
        // Hacky month filter :/
        $start_date = new DateTime();
        $start_date->setTime(0, 0, 0); // Absolute start
        
        $month = trim(strtolower($request->getQuery('month', 'current')));
        
        if ($month === 'last') {
            $start_date->modify('-1 month');
        }
        
        // Set the end date to match the start (instead of declaring a new var) so we don't have
        // any weird February-style date crossover mistakes (i.e. Feb 30 = Mar 2)
        $end_date = clone $start_date;
        $end_date->setTime('23', '59', '59'); // Absolute end

        // Get month bounds
        $start_date->modify('first day of this month');
        $end_date->modify('last day of this month');
        
        // This query is really bad.
        // @todo Fix please :(
        $booking = new Booking();
        $bookings = $booking->findObjects(
            'user_id=' . $doctor->getId() . ' AND ' .
            // Filter dates
            'appointment_start_at >= "' . $start_date->getDateTime() . '" AND ' .
            'appointment_start_at <= "' . $end_date->getDateTime() .'" AND ' .
            // Only allow these statuses for some reason
            'status IN (' . implode(',', array(
                    Booking::STATUS_RECEIVED,
                    Booking::STATUS_RECEIVED_ALTERNATIVE,
                    Booking::STATUS_RECEIVED_ALTERNATIVE_PENDING,
                    Booking::STATUS_RECEIVED_UNCONFIRMED,
                    Booking::STATUS_CANCELLED_BY_PATIENT_LOGIN,
                    Booking::STATUS_CANCELLED_BY_PATIENT,
                    Booking::STATUS_NOSHOW
            )) . ')'
        );
        
        $view->setRefs(array(
            'doctor' => $doctor,
            'practice' => $practice,
            'doctors' => $this->getAllDoctors($practices),
            'locations' => $practices,
            'slug' => $doctor->getSlug(),
            'bookings' => $bookings,
            'month' => $month
        ));
        
        return $view
            ->disableCaching()
            ->display('doctor/dashboard/patients.tpl');
    }

    /**
     * Show the reporting page for this doctor (backend)
     */
    public function reportingAction()
    {
        $request = $this->getRequest();
        $view = $this->getView();
        $current_user = $this->container->get('current_user');

        // Try slug-based lookup (normally used on the dashboard)
        $slug = $request->getQuery('arzt');
        $doctor = new User($slug, 'slug');

        // If it failed, try the user ID lookup (based on form data)
        if (!$doctor->isValid()) {
            $doctor->load(intval($request->getParam('user_id')));
        }

        // If THAT failed, just use the logged-in user
        if (!$doctor->isValid()) {
            $doctor = $current_user;
        }

        if (!$this->checkIdentity($current_user)) {
            return $this->redirectTo('doctor_login');
        }

        // We need to display a dropdown of all locations
        $practices = $doctor->getLocations();

        $location_slug = $request->getQuery('praxis');
        $practice = new Location($location_slug, 'slug');

        // If it failed, try the user ID lookup (based on form data)
        if (!$practice->isValid()) {
            $practice->load(intval($request->getParam('location_id')));
        }
        
        // The default date is the *most current report allowed*
        $default_date = new DateTime();
        $default_date->modify('-1 month');
        $default_date->modify('last day of this month');
        $billing_period = new DateTime($request->getParam('period', $default_date->format('Y-m')));

        // If the billing period is newer than the most recent report, change it
        if ($billing_period > $default_date) {
            $billing_period = $default_date;
        }
        
        // Get the statement for the given billing period and practice. Take note of above
        $statement = Statement::forPeriod($billing_period, $practice);

        // Display monthly statement data
        // @todo Optimise relationships and saving process. Too much here is manual
        if ($request->isPost()) {
            $payload = json_decode($request->getRequest('payload'), true);
            
            $statement
                ->updateBookings($payload)
                ->save(); // @todo: This becomes more useful with statuses...
        }
        
        $view->setRefs(array(
            'doctor' => $doctor,
            'practice' => $practice,
            'doctors' => $this->getAllDoctors($practices),
            'locations' => $practices,
            'slug' => $doctor->getSlug(),
            'statement' => $statement,
            'billing_period' => $billing_period->format('Y-m')
        ));
        
        return $view
            ->disableCaching()
            ->display('doctor/dashboard/reporting/reporting.tpl');
    }

    /**
     * The register action is a series of pages with the same route. The step parameter dictates workflow
     */
    public function registerAction()
    {
        $app = Application::getInstance();
        $request = $app->getRequest();
        $view = $app->getView();

        $form = Form::getForm(new DoctorRegistrationType());
        $step = $request->getParam('step', 1);
        $errors = array();

        // This is relevant for *every* request
        switch ($step) {
            case 1:
                if(!$request->request->has('practice_name') ||
                    $request->get('practice_name') == '') {
                    $form->remove('practice_name');
                }
                // Remove form elements that aren't submitted at this point
                $form
                    ->remove('city')
                    ->remove('phone_mobile')
                    ->remove('medical_specialty_id')
                    ->remove('zip')
                    ->remove('doctors_count')
                    ->remove('registration_reasons')
                    ->remove('street')
                    ->remove('url');

                // Log the user out if they're logged in
                if ($app->getCurrentUser()->isValid()) {
                    $app->getCurrentUser()->logout();
                }
                break;
            case 2:
                // Remove form elements we don't need
                $form
                    ->remove('first_name')
                    ->remove('street')
                    ->remove('email')
                    ->remove('phone')
                    ->remove('zip')
                    ->remove('doctors_count')
                    ->remove('registration_reasons')
                    ->remove('url');
                break;
            case 3:
                // Remove form elements we don't need
                $form
                    ->remove('first_name')
                    ->remove('last_name')
                    ->remove('gender')
                    ->remove('email')
                    ->remove('phone')
                    ->remove('practice_name')
                    ->remove('city')
                    ->remove('phone_mobile')
                    ->remove('medical_specialty_id');
                break;
            default:
                // Step is invalid
                // @todo Redirect?
                break;
        }

        // This is a post request. Make the magic happen
        if ($request->isPost()) {
            $errors = $this->register($form, $step);
            if($errors instanceof RedirectResponse) {
                return $errors;
            }

            if (empty($errors)) {
                $step++;
            }

            if($request->hasParam('practice_name') && isset($practice_name_formchild)) {
                $form->add($practice_name_formchild);
            }
        }

        return $view
            ->setRefs(array(
                    'medicalSpecialtiesOptions' => MedicalSpecialty::getHtmlOptions(), // Not efficient, but easiest way to ensure we always have these
                    'data' => $form->getData(),
                    'errors' => $errors,
                    'error_messages' => array_keys($errors),
                    'step' => $step
                ))
            ->disableCaching()
            ->display("doctor/dashboard/register/register-{$step}.tpl"); // Dynamic registration pages
    }

    /**
     * Actually registers the user. i.e. saving forms etc.
     * Note: This is a goddamn mess. Combining everything into one route killed the process
     *
     * @param \Symfony\Component\Form\Form $form
     * @param int $step
     *
     * @return array
     */
    protected function register(\Symfony\Component\Form\Form $form, $step = 1)
    {
        $app = Application::getInstance();
        $request = $app->getRequest();
        $router = $app->container->get('router');

        $pending_user = new PendingUser();
        $errors = array();

        $form->submit($request->getRequest());
        $data = $form->getData();

        switch ($step) {
            // The initial user creation
            case 1:
                if ($form->isValid()) {
                    // If the email is in use, don't process the application
                    if (
                        !User::isEmailAvailable($data['email']) ||
                        !PendingUser::isEmailAvailable($data['email'])
                    ) {
                        $errors['email'] = array('Ein Account mit dieser E-Mail-Adresse besteht bereits');
                        return $errors;
                    }

                    // @todo Move this?
                    $activation_code = substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 6);

                    $pending_user
                        ->setFirstName(sqlsave($data['first_name']))
                        ->setLastName(sqlsave($data['last_name']))
                        ->setTitle(sqlsave($data['title']))
                        ->setGender(sqlsave($data['gender']))
                        ->setEmail(sqlsave($data['email']))
                        ->setPhone(sqlsave($data['phone']))
                        ->setActivationCode($activation_code)
                        ->saveNewObject();

                    // Sends relevant email notifications to AT and the user
                    $pending_user->sendNotifications();

                    $app->getView()->setRef('uid', $pending_user->getId());
                } else {
                    $errors = Form::getFormErrors($form);
                }
                break;

            // Practice information
            case 2:
                $user_id = intval($request->getRequest('uid'));
                if ($form->isValid()) {
                    $pending_user->load($user_id);

                    if ($pending_user->isValid()) {
                        $pending_user
                            ->setLocationCity(sqlsave($data['city']))
                            ->setLocationName(sqlsave($data['practice_name']))
                            ->setPhoneMobile(sqlsave($data['phone_mobile']))
                            ->setMedicalSpecialtyIds(array($data['medical_specialty_id']))
                            ->save();

                        // Sends relevant email notifications to AT
                        $pending_user->sendNotifications(true);

                        $app->getView()->setRef('uid', $user_id);
                    } else {
                        $errors['uid'] = array('User ID is invalid');
                        $app->addSessionMessage('User is invalid', 'ERROR');
                    }
                } else {
                    $app->getView()->setRef('uid', $user_id);
                    $errors = Form::getFormErrors($form);
                }
                break;

            case 3:
                $user_id = intval($request->getRequest('uid'));
                if ($form->isValid()) {
                    $pending_user->load($user_id);

                    if ($pending_user->isValid()) {
                        $pending_user
                            ->setLocationStreet(sqlsave($data['street']))
                            ->setLocationZip(sqlsave($data['zip']))
                            ->setLocationDoctorsCount(sqlsave($data['doctors_count']))
                            ->setRegistrationReasons(sqlsave(implode(', ',$data['registration_reasons'])))
                            ->setLocationWww(sqlsave($data['url']))
                            ->save();

                        // Sends relevant email notifications to AT
                        $pending_user->sendNotifications(true);

                        $app->getView()->setRef('uid', $user_id);
                    } else {
                        $errors['uid'] = array('User ID is invalid');
                        $app->addSessionMessage('User is invalid', 'ERROR');
                    }
                } else {
                    $app->getView()->setRef('uid', $user_id);
                    $errors = Form::getFormErrors($form);
                }
                break;

            // Survey information
            case 4:
                $user_id = intval($request->getRequest('uid'));
                $pending_user->load($user_id);

                // Note: This can fail if the pending user has already been accepted
                if ($pending_user->isValid()) {
                    $pending_user
                        ->setRegistrationReasons($pending_user->getRegistrationReasons()."\n\n".sqlsave($request->getRequest('registration_reasons')))
                        ->setRegistrationFunnel(sqlsave($request->getRequest('registration_funnel')))
                        ->setRegistrationFunnelOther(sqlsave($request->getRequest('registration_funnel_other')))
                        ->save();

                    // Sends relevant email notifications to AT
                    $pending_user->sendNotifications(true);

                    return $app->redirect($router->generate('doctor_login'), 'Vielen Dank für Ihre Registrierung bei Arzttermine.de!');
                } else {
                    $errors['uid'] = array('User ID is invalid');
                    $app->addSessionMessage('User is invalid', 'ERROR');
                }
                break;

            // Invalid step. Just fail and return
            default:
                // @todo Invalidate registration process
                break;
        }

        return $errors;
    }

    /**
     * Forgot password action. Receives POST
     * 
     * @return Response
     */
    public function forgotPasswordAction()
    {
        $app = Application::getInstance();
        $request = $app->getRequest();

        if (!$request->isPost()) {
            $this->redirectTo('doctor_login');
        }

        // Quick and nasty validation for email
        $email = strtolower($request->getRequest('email'));
        $email_is_valid = filter_var($email, FILTER_VALIDATE_EMAIL);

        if (!$email_is_valid) {
            $this->redirectTo('doctor_login', array(), array('status' => 'ERROR', 'text' => 'Diese E-Mail-Adresse ist ungültig'));
        }

        // Only send the password reset request if we've got a user in the DB
        $doctor = new User($email, 'email');
        if ($doctor->isValid() && $doctor->isActive()) {
            $reset_password_code = $doctor->generateResetPasswordCode();
            $doctor->setResetPasswordCode($reset_password_code);
            $doctor->save('reset_password_code');
            $this->sendPasswordEmail($doctor, $reset_password_code);
        }

        // Tell the user the password reset was succesful even if they don't exist. Designed to stop bots figuring out email addresses
        return $this->redirectTo('doctor_login', array(), 'Anfrage zum Passwort-Reset wurde gesendet. Bitte überprüfen Sie Ihr E-Mail-Postfach.');
    }

    /**
     * @todo Check that the user isn't resetting their password to the same thing. Possibly redundant
     *
     * @param string $reset_password_code
     * 
     * @return Response
     */
    public function resetPasswordAction($reset_password_code = '')
    {
        $app = Application::getInstance();
        $request = $app->getRequest();
        $view = $app->getView();
        $router = $app->container->get('router');

        // No code means no dice
        if (!$reset_password_code) {
            return $this->redirect($router->generate('doctor_login'));
        }

        // We need to update the password at this stage
        // @todo Make this a standalone FormType with validators etc
        if ($request->isPost()) {
            $doctor = new User($reset_password_code, 'reset_password_code');

            // Doctor doesn't exist. Usually because the code is wrong
            if (!$doctor->isValid()) {
                return $app->redirect($router->generate('doctor_login'), array('type' => 'ERROR', 'text' => 'Falscher Passwortreset-Code'));
            }

            // Passwords need to match and not be empty
            $password = $request->getRequest('password');
            $confirm  = $request->getRequest('repeat_password');

            if (empty($password)) {
                return $app->redirect($router->generate('doctor_reset_password', array('reset_password_code' => $reset_password_code)), array('type' => 'ERROR', 'text' => 'Bitte geben Sie ein Passwort an.'));
            }

            if ($password !== $confirm) {
                return $app->redirect($router->generate('doctor_reset_password', array('reset_password_code' => $reset_password_code)), array('type' => 'ERROR', 'text' => 'Die Passwörter stimmen nicht überein.'));
            }

            $doctor->changePassword($request->getRequest('password'));

            /*
             * The process of registering an account doesn't require explicit activation by the doctor any more.
             * Instead, a password reset link is emailed to him. The first time he gets here, the account will be activated.
             * I don't think there's a security concern here because this point can't be reached without prior backend approval
             * from our side which leads to generating the reset_password_code in the initial email.
             */
            if (!$doctor->isActive() && $doctor->getActivatedAt() === '0000-00-00 00:00:00') {
                $doctor->activate();
            }

            return $this->redirect($router->generate('doctor_login'), 'Passwort wurde erfolgreich geändert.');
        }

        return $view
            ->disableCaching()
            ->display('doctor/dashboard/reset-password.tpl');
    }

    /**
     * @param User $user
     * @param string $reset_password_code
     */
    protected function sendPasswordEmail(User $user, $reset_password_code)
    {
        $router = Application::getInstance()->container->get('router');

        $_mailing = new Mailing();
        $_mailing->type = MAILING_TYPE_TEXT;
        $_mailing->from = $GLOBALS['CONFIG']["REVIEWS_EMAIL_ADDRESS"];
        $_mailing->from_name = $GLOBALS['CONFIG']["REVIEWS_EMAIL_NAME"];
        $_mailing->content_filename = '/mailing/doctors/forgot-password.txt';
        $_mailing->data['domain'] = $GLOBALS['CONFIG']['DOMAIN'];
        $_mailing->data['url'] =  'https://'.$GLOBALS['CONFIG']['DOMAIN'].$router->generate('doctor_reset_password', array('reset_password_code' => $reset_password_code));
        $_mailing->data['doctor_name'] = $user->getFullName();
        $_mailing->addAddressTo($user->getEmail());
        $_mailing->send();
    }

    /**
     * Change the user's profile pic (account dashboard)
     * 
     * @return Response
     */
    public function profilePicAction()
    {
        $app = Application::getInstance();
        $request = $app->getRequest();
        $view = $app->getView();

        $slug = $request->getQuery('arzt');
        $doctor = new User($slug, 'slug');

        if (!$doctor->isValid()) {
            $doctor = $app->getCurrentUser();
        }

        if (!$this->checkIdentity($doctor)) {
            return $this->redirectTo('doctor_login');
        }

        if ($request->isPost()) {

            if ($doctor->isEditAllowed($app->getCurrentUser()->getId())) {
                $action = $request->getParam('action');

                // @todo Fix the hacky switch block
                switch ($action) {
                    case 'select':
                        $asset_id = intval($request->getRequest('aid'));
                        $asset = new Asset($asset_id);

                        // If the asset isn't a parent, we've accidentally sent a child. Load the parent instead
                        if (!$asset->isParent()) {
                            $asset->load($asset->getParentId());
                        }

                        // Update the doctor's profile with the new asset info
                        $doctor
                            ->setProfileAssetId($asset_id)
                            ->setProfileAssetFilename($asset->getFilename())
                            ->save();

                        $app->addSessionMessage('Das Profilbild wurde festgelegt');

                        break;

                    case 'delete':
                        $asset_id = intval($request->getRequest('aid'));
                        $asset = new Asset($asset_id);

                        // If the asset isn't a parent, we've accidentally sent a child. Load the parent instead
                        if (!$asset->isParent()) {
                            $asset->load($asset->getParentId());
                        }

                        // Delete the parent asset and all generated children. The above snippet *should* ensure we're always
                        // dealing with a parent asset
                        $asset->deleteWithChildren();

                        $app->addSessionMessage('Das Profilbild wurde gelöscht');

                        break;

                    case 'new':
                        $asset = new Asset();
                        $extension = end(explode('.', $_FILES['upload']['name']));

                        // @todo More verbose error handling.
                        // You can check the $_FILES error array against PHP constants to see why something fails
                        if (
                            in_array($extension, $GLOBALS["CONFIG"]["ASSET_POSSIBLE_FILEEXTENSIONS"]) &&
                            $_FILES['upload']['error'] == UPLOAD_ERR_OK
                        ) {
                            $asset->saveNewParent(ASSET_OWNERTYPE_USER, $doctor->getId(), array('gallery_id' => 0), $_FILES['upload']);

                            if ($asset->isValid()) {
                                $doctor
                                    ->setProfileAssetId($asset->getId())
                                    ->setProfileAssetFilename($asset->getFilename())
                                    ->save();

                                $app->addSessionMessage('Das Profilbild wurde gespeichert');
                            } else {
                                $app->addSessionMessage('Das Profilbild konnte nicht gespeichert werden', 'ERROR');
                            }
                        } else {
                            $app->addSessionMessage('Das Profilbild konnte nicht gespeichert werden', 'ERROR');
                        }

                        break;

                    default:
                        $app->addSessionMessage('Invalid action', 'ERROR');
                }

            } else {
                $app->addSessionMessage('You do not have permission to edit this practice profile.', 'ERROR');
            }

            // @todo Better handling :(
            if ($request->isXmlHttpRequest()) {
                exit;
            }
        }

        return $view
            ->setRefs(array(
                    'doctor' => $doctor,
                    'locations' => $doctor->getLocations(),
                    'slug' => $doctor->getSlug(),
                    'assets' => $doctor->getAssets()
                ))
            ->disableCaching()
            ->display('doctor/dashboard/profile-pic.tpl');
    }

    /**
     * Kludgey little mini-API endpoint for the doctor's area
     *
     * @return Response
     */
    public function dataAction()
    {
        $app = Application::getInstance();
        $view = $app->getView();
        $request = $app->getRequest();
        $response = new Response();

        switch ($request->getQuery('type')) {
            // Request all treatment types (for specialty selection)
            case 'treatment_types':
                $slug = $request->getQuery('arzt');
                $doctor = new User($slug, 'slug');

                if (!$doctor->isValid()) {
                    $doctor = $app->getCurrentUser();
                }

                $medical_specialty = new MedicalSpecialty($request->getQuery('msid'));

                $view->setRefs(array(
                        'medical_specialty' => $medical_specialty,
                        'treatment_types'   => $medical_specialty->getTreatmentTypes(),
                        'active_treatment_types' => array_keys($doctor->getTreatmentTypes())
                    ));
                
                $response->setContent($view->fetch('doctor/dashboard/data/treatment-types.tpl'));
                break;
            default:
                break;
        }

        return $response;
    }
    
}
