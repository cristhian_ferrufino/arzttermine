<?php

namespace Arzttermine\Controller;

use Arzttermine\Appointment\Provider;
use Arzttermine\Coupon\CouponService;
use Arzttermine\Insurance\Insurance;
use Arzttermine\Location\Location;
use Arzttermine\Maps\GoogleMap;
use Arzttermine\Search\Search;
use Arzttermine\User\User;

class PracticeController extends Controller {

    /**
     * Profile view
     *
     * @param string $slug
     * @param string $insurance
     * 
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction($slug = '', $insurance = '')
    {
        $request = $this->getRequest();
        $view = $this->getView();
        $insurance_slug = $insurance; // kludge

        $location = new Location($slug, 'slug');

        if (
            !$location->isValid() ||
            !$location->hasProfile() ||
            !$location->isVisible()
        ) {
            $this->notFound();
        }

        // Set up the provider query using our normal search engine object
        // --------------------------------------------------------------------
        $search = new Search();

        // These are for the widget
        if ($date_start = $request->getQuery('date_start')) {
            $search->setDateBounds($date_start);
        }

        // Replace the slug with a real insurance object
        $insurance = new Insurance();
        if (!empty($insurance_slug)) {
            $insurance->load($insurance_slug, 'slug');
        }
        if (!$insurance->isValid()) {
            $insurance->load(Insurance::GKV);
        }
        // tell the search which insurance we have selected
        $search->setInsurance($insurance);

        // Appointment provider objects
        $providers = array();
        
        // if the widget provides a user that limit the result to that
        $users = $location->getMembers();
        // If the widget provides a location then limit the result to that
        if ($user_id = intval($request->getQuery('u'))) {
            // Then filter all locations based on the passed location ID
            array_filter(
                $users, function ($u) use ($user_id) {
                    /** @var User $u */
                    return $u->getId() == $user_id;
                }
            );
        }

        if($location->getLat() == 0 && $location->getLng() == 0) {
            if ($coords = GoogleMap::getGeoCoords($location->getAddress(', '))) {
                $location->setLat($coords['lat']);
                $location->setLng($coords['lng']);
                $location->save(array('lat','lng'));
            }
        }
        
        // Get some basic provider objects so we can fetch appointments with them
        /** @var User[] $users */
        foreach ($users as $user) {
            $provider = new Provider();
            $providers[] = $provider
                ->setDateRange($search->getStart(), $search->getEnd())
                ->setUser($user)
                ->setLocation($location)
                // @todo: Allow easy toggle for each of these
                ->setInsurance($insurance)
                ->setMedicalSpecialty($user->getDefaultMedicalSpecialty());
        }

        $this->dispatchEvent('search.before_search', $search);
        
        $appointments = $search->findAppointments($providers);
        /** @var Provider $provider */
        foreach ($providers as $provider) {
            $provider->findAppointments($appointments);
        }

        $this->get('coupon_service')->enable('search');
        
        return $view
            ->addHeadTitle(title($location->getHtmlTitle() . ', ' . $this->trans('Termin buchen')))
            ->setMetaTag('description', meta($location->getHtmlMetaDescription()))
            ->setMetaTag('keywords', meta($location->getHtmlMetaKeywords()))
            ->addHeadHtml('<link rel="canonical" href="' . $location->getUrl(true) . '" />') // as we use different urls to point to this profil page we should tell google where the origin is

            ->setRefs(array(
                    'providers'    => $providers,
                    'location'     => $location,
                    'breadcrumbs'  => $location->getBreadcrumbs(array('insurance_id' => $insurance->getId())),
                    'search'       => $search,
                    'insurance'    => $insurance
                ))

            ->display('location/profile.tpl');
    }
}