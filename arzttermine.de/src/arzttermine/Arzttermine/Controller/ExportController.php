<?php

namespace Arzttermine\Controller;

use Arzttermine\Application\Application;
use Arzttermine\Location\Location;
use Arzttermine\User\User;
use Symfony\Component\HttpFoundation\Response;

class ExportController extends Controller {

    /**
     * Leverate
     */
    public function leverateAction()
    {
        $app = Application::getInstance();
        $request = $this->getRequest();
        $view = $this->getView();

        $db = Application::getInstance()->getSql();

        $sql
            = '
			SELECT
				u.id AS user_id, l.id AS location_id
			FROM ' . DB_TABLENAME_USERS . ' u, ' . DB_TABLENAME_LOCATIONS . ' l, ' . DB_TABLENAME_LOCATION_USER_CONNECTIONS . ' luc
		 	WHERE
				luc.user_id = u.id
			AND
				luc.location_id = l.id
			AND
				u.status = ' . USER_STATUS_VISIBLE_APPOINTMENTS . '
			AND
				l.status = ' . LOCATION_STATUS_VISIBLE_APPOINTMENTS . '
			AND
				u.has_contract=1
			AND
				(
				l.city = "Berlin"
				)
			AND
                l.country_code = "DE"
            AND
                u.comment_intern NOT LIKE "%REG%"
			ORDER BY l.city ASC, l.id ASC';

        $db->query($sql);
        $records = array();
        if ($db->nf() > 0) {
            while ($db->next_record()) {
                $records[] = $db->Record;
            }
        }

        if (empty($records)) {
            $data = 'ERROR: No available records';
        } else {
            $data = '"ID";"city_name";"zip_code1";"zip_code2";"category";"category_name";"category_article";"ihr";"post_text";"post_title";"post_description";"image_url";"destination_url";"domain_caption";"price";"currency"' . "\n";
            foreach ($records as $record) {
                $userId = $record['user_id'];
                $locationId = $record['location_id'];

                $user = new User($userId);
                $location = new Location($locationId);
                $providerId = $location->getId() . '-' . $user->getId();

                if ($user->isValid() && $location->isValid()) {
                    if (!in_array(1, $user->getMedicalSpecialtyIds())) {
                        continue;
                    }
                    $data .= sprintf(
                            '"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s"',
                            sanitize_csv_field($user->getId()),
                            sanitize_csv_field($location->getCity()),
                            sanitize_csv_field($location->getZip()),
                            sanitize_csv_field('DE:'.$location->getZip()),
                            sanitize_csv_field('zahnarzt'),
                            sanitize_csv_field('Zahnarzt'),
                            sanitize_csv_field(($user->getGenderText() == 'Herr') ? 'der' : 'die'),
                            sanitize_csv_field(($user->getGenderText() == 'Herr') ? 'Ihren' : 'Ihre'),
                            sanitize_csv_field('Finden Sie Ihren Zahnarzt in Berlin. Mit Arzttermine.de'),
                            sanitize_csv_field($user->getName().' - Zahnarzt'),
                            sanitize_csv_field($location->getStreet().','.$location->getZip().' '.$location->getCity()),
                            sanitize_csv_field('https://www.arzttermine.de'.$user->getProfileAssetUrl(ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED)),
                            sanitize_csv_field($user->getUrl(true, true).'/gkv'),
                            sanitize_csv_field('www.arzttermine.de'),
                            sanitize_csv_field('0.00'),
                            sanitize_csv_field('EUR')
                        ) . "\n";
                }
            }
        }

        $headers = array(
            'Content-Type' => 'text/csv'
        );
        $response = new Response($data, Response::HTTP_OK, $headers);

        return $response;
    }
}