<?php

namespace Arzttermine\Controller;

use NGS\Framework\Dispatcher;

/**
 * Wrapper for the NGS system
 */
class NGSController extends Controller {

    /**
     * Take a generic URL or request, and forward it to the right handler
     */
    public function proxyAction()
    {
        $_url = explode('/', $this->getRequest()->getPathInfo());

        if (strpos($_SERVER['REQUEST_URI'], "?")) {
            $_SERVER['REQUEST_URI'] = substr($_SERVER['REQUEST_URI'], 0, strpos($_SERVER['REQUEST_URI'], "?"));
        }
        if (!empty($_url[2]) && strpos($_url[2], "do_") !== false) {
            $actionMappingArr = array(
                $GLOBALS['CONFIG']['DOCTORS_LOGIN_URL'] => array(
                    'do_registrieren_aerzte'     => $GLOBALS['CONFIG']['DOCTORS_LOGIN_URL'] . '/do_register_doctor',
                    'do_aktivieren'              => $GLOBALS['CONFIG']['DOCTORS_LOGIN_URL'] . '/do_activate',
                    'do_speichern_aerzte_profil' => $GLOBALS['CONFIG']['DOCTORS_LOGIN_URL'] . '/do_save_doctor_profile',
                    'do_anmelden'                => $GLOBALS['CONFIG']['DOCTORS_LOGIN_URL'] . '/do_login',
                    'do_passwort_vergessen'      => $GLOBALS['CONFIG']['DOCTORS_LOGIN_URL'] . '/do_doctor_forgot_password',
                    'do_anmelden_vergessen'      => $GLOBALS['CONFIG']['DOCTORS_LOGIN_URL'] . '/do_doctor_forgot_login',
                    'do_passwort_zuruecksetzen'  => $GLOBALS['CONFIG']['DOCTORS_LOGIN_URL'] . '/do_reset_password',
                    'do_fachrichtung_verwalten'  => $GLOBALS['CONFIG']['DOCTORS_LOGIN_URL'] . '/do_save_medical_specialties',
                    'do_laden_pic'               => $GLOBALS['CONFIG']['DOCTORS_LOGIN_URL'] . '/do_upload_profile_pic',
                    'do_update_confirmed'        => $GLOBALS['CONFIG']['DOCTORS_LOGIN_URL'] . '/do_update_booking_confirmed',
                    'do_update_statement_status' => $GLOBALS['CONFIG']['DOCTORS_LOGIN_URL'] . '/do_update_statement_status',
                    'do_ertellen_termin'         => $GLOBALS['CONFIG']['DOCTORS_LOGIN_URL'] . '/do_create_appointment',
                    'do_fachrichtung_loschen'    => $GLOBALS['CONFIG']['DOCTORS_LOGIN_URL'] . '/do_delete_medical_specialty',
                    'do_speichern_praxis'        => $GLOBALS['CONFIG']['DOCTORS_LOGIN_URL'] . '/do_save_location',
                    'do_praxis_fachrichtung'     => $GLOBALS['CONFIG']['DOCTORS_LOGIN_URL'] . '/do_save_location_specialties',
                    'do_approve_review'          => $GLOBALS['CONFIG']['DOCTORS_LOGIN_URL'] . '/do_approve_review'
                ),
                $GLOBALS['CONFIG']['PATIENTS_URL']      => array(
                    'do_patienten_registrieren'     => $GLOBALS['CONFIG']['PATIENTS_URL'] . '/do_register_patient',
                    'do_aktivieren'                 => $GLOBALS['CONFIG']['PATIENTS_URL'] . '/do_activate',
                    'do_passwort_zuruecksetzen'     => $GLOBALS['CONFIG']['PATIENTS_URL'] . '/do_reset_password',
                    'do_speichern_patienten_profil' => $GLOBALS['CONFIG']['PATIENTS_URL'] . '/do_save_patient_profile',
                    'do_anmelden'                   => $GLOBALS['CONFIG']['PATIENTS_URL'] . '/do_login',
                    'do_passwort_vergessen'         => $GLOBALS['CONFIG']['PATIENTS_URL'] . '/do_forgot_password',
                    'do_arzt_bewerten'              => $GLOBALS['CONFIG']['PATIENTS_URL'] . '/do_review_doctor',
                    'do_bewertung_abbrechen'        => $GLOBALS['CONFIG']['PATIENTS_URL'] . '/do_cancel_review'
                ),
                $GLOBALS['CONFIG']['AFFILIATES_URL']    => array(
                    'do_passwort_zuruecksetzen' => $GLOBALS['CONFIG']['AFFILIATES_URL'] . '/do_reset_password',
                    'do_anmelden'               => $GLOBALS['CONFIG']['AFFILIATES_URL'] . '/do_login',
                    'do_passwort_vergessen'     => $GLOBALS['CONFIG']['AFFILIATES_URL'] . '/do_forgot_password',
                ),
            );

            // Append sub page parameter to the end of the request. It's getting lost
            $_SERVER['REQUEST_URI'] = '/dyn/' . $actionMappingArr[$_url[1]][$_url[2]] . (!empty($_url[3]) ? '/' . $_url[3] : '');
        }

        $url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $_url = explode('/', $url);

        if (
            $_url[1] == "ngsadmin" ||
            $_url[1] == $GLOBALS['CONFIG']['DOCTORS_LOGIN_URL'] ||
            $_url[1] == $GLOBALS['CONFIG']['PATIENTS_URL'] ||
            $_url[1] == $GLOBALS['CONFIG']['PATIENTS_LOGIN_URL'] ||
            $_url[1] == $GLOBALS['CONFIG']['AFFILIATES_URL'] ||
            $_url[1] == $GLOBALS['CONFIG']['AFFILIATES_LOGIN_URL'] ||
            $_url[1] == $GLOBALS['CONFIG']['PATIENTS_REGISTER_URL']
        ) {
            //in case of 'ngsadmin' we have a just a 'admin' local package
            $custRootDir = str_replace("ngsadmin", "admins", $_url[1]);
            $custRootDir = str_replace($GLOBALS['CONFIG']['DOCTORS_LOGIN_URL'], "doctors", $custRootDir);
            $custRootDir = str_replace($GLOBALS['CONFIG']['PATIENTS_LOGIN_URL'], "patients", $custRootDir);
            $custRootDir = str_replace($GLOBALS['CONFIG']['AFFILIATES_LOGIN_URL'], "affiliates", $custRootDir);
            $custRootDir = str_replace($GLOBALS['CONFIG']['PATIENTS_REGISTER_URL'], "patients", $custRootDir);
            $custRootDir = str_replace($GLOBALS['CONFIG']['PATIENTS_URL'], "patients", $custRootDir);
            $custRootDir = str_replace($GLOBALS['CONFIG']['AFFILIATES_URL'], "affiliates", $custRootDir);

            if ($_url[1] == $GLOBALS['CONFIG']['PATIENTS_LOGIN_URL'] || $_url[1] == $GLOBALS['CONFIG']['AFFILIATES_LOGIN_URL']) {
                $_REQUEST["p"] = "login";
            }

            if ($_url[1] == $GLOBALS['CONFIG']['PATIENTS_REGISTER_URL']) {
                $_REQUEST["p"] = "register";
            }
            
            unset($_url[0]);
            unset($_url[1]);

            if (isset($_url[2])) {
                $_REQUEST["p"] = $_url[2];
                unset($_url[2]);
            }
            
            if (isset($_url[3])) {
                $_REQUEST["sub_pages"] = array_slice($_url, 0);
            }
            
            $_SERVER['REQUEST_URI'] = "/dyn/" . $custRootDir . "/main";

            if (isset($_url[0])) {
                $_SERVER['REQUEST_URI'] .= '/' . implode('/', $_url);
            }

            $_url[1] = "dyn";
        }
        
        if ($_url[1] == "dyn") {
            if (!isset($_url[2])) {
                $_url[2] = null; // Not having it causes E_NOTICE meltdown.
            }

            $_SERVER['REQUEST_URI'] = substr($_SERVER['REQUEST_URI'], 4);
            $_REQUEST["_url"] = $_SERVER['REQUEST_URI'];

            $_REQUEST["_url"] = substr($_REQUEST["_url"], strlen($_url[2]) + 1);

            $custRootDir = str_replace("ngsadmin", "admins", $_url[2]);
            $custRootDir = str_replace($GLOBALS['CONFIG']['DOCTORS_LOGIN_URL'], "doctors", $custRootDir);
            $custRootDir = str_replace($GLOBALS['CONFIG']['PATIENTS_URL'], "patients", $custRootDir);
            $custRootDir = str_replace($GLOBALS['CONFIG']['AFFILIATES_URL'], "affiliates", $custRootDir);

            $_REQUEST["_url"] = '/' . $custRootDir . $_REQUEST["_url"];
        }

        // Auto dispatches
        new Dispatcher();
    }
}