<?php

namespace Arzttermine\Controller;

use Arzttermine\Booking\Booking;
use Arzttermine\Booking\Offer;
use Arzttermine\Mail\AdminMailer;
use Arzttermine\Mail\SMS;

class BookingOfferController extends Controller {

    /**
     * @param array $parameters
     */
    public function showAllAction($parameters = array())
    {
        $view = $this->getView();

        $GLOBALS['CONFIG']['SOCIALMEDIA_ACTIVATE'] = false;

        if (!empty($parameters['slug'])) {
            $booking = new Booking($parameters['slug'], 'slug');

            if ($booking->isValid()) {
                $user = $booking->getUser();
                $location = $booking->getLocation();
                $bookingOffer = new Offer();
                $bookingOffers = $bookingOffer->findObjects(
                                              '(status=' . Offer::STATUS_PENDING . ' OR status=' . Offer::STATUS_NEW . ')' .
                                              ' AND ' .
                                              'booking_id=' . $booking->getId() .
                                              ' ORDER BY appointment_start_at ASC'
                );

                return $view
                    ->setRefs(array(
                        'booking_offers' => $bookingOffers,
                        'location'       => $location,
                        'user'           => $user,
                        'booking'        => $booking
                    ))
                    ->addHeadTitle('Terminanfrage für ' . title($user->getName()))
                    ->display('booking-offers/booking-offers.tpl');
            }
        }

        $this->notFound();
    }

    /**
     * @param string $slug
     * @param string $method
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showOneAction($method = '', $slug = '')
    {
        $view = $this->getView();

        if (empty($slug)) {
            $this->notFound();
        }

        $bookingOffer = new Offer($slug, 'slug');
        if (!$bookingOffer->isValid()) {
            $this->notFound();
        }

        $booking = new Booking($bookingOffer->getBookingId());
        if (!$booking->isValid()) {
            return $this->redirectTo('home', array(), array(
                'status' => 'ERROR',
                'text'   => 'Etwas ist schief gegangen. Kontaktieren Sie uns bitte telefonisch über unsere kostenfreie Hotline 0800 22 22 133 oder schreiben Sie uns eine E-Mail an support@arzttermine.de'
            ));
        }

        // Check for expiration
        if ($bookingOffer->hasExpired()) {
            AdminMailer::send(
               $GLOBALS['CONFIG']['BOOKINGS_EMAIL_ADDRESS'],
               "Terminvorschlag: Der Patient hat die Buchung mit der BookingId {$booking->getId()} beantwortet nachdem sie abgelaufen war.\n{$booking->getAdminEditUrl()}\n{$booking->getSalesforceUrl()}",
               "Terminvorschlag (BookingId {$booking->getId()})"
            );
            return $this->redirect(
                $booking->getUrl(), array(
                    'status' => 'WARNING',
                    'text'   => "Dieser Terminvorschlag ist bereits abgelaufen und damit nicht mehr gültig, weil er nicht rechtzeitig beantwortet wurde.<br />Bitte kontaktieren Sie uns per E-Mail oder telefonisch kostenfrei unter {$GLOBALS['CONFIG']['PHONE_SUPPORT']} falls Sie den Termin nocheinmal ändern möchten."
                ));
        }

        // Check if this offer was already answered
        if ($bookingOffer->hasBeenAnswered()) {
            AdminMailer::send(
               $GLOBALS['CONFIG']['BOOKINGS_EMAIL_ADDRESS'],
               "Terminvorschlag: Der Patient hat die Buchung mit der BookingId {$booking->getId()} erneut beantwortet.\n{$booking->getAdminEditUrl()}\n{$booking->getSalesforceUrl()}",
               "Terminvorschlag (BookingId {$booking->getId()})"
            );
            return $this->redirect(
                $booking->getUrl(), array(
                    'status' => 'WARNING',
                    'text'   => 'Sie haben diesen Terminvorschlag bereits beantwortet.<br />Bitte kontaktieren Sie uns falls Sie den Termin nocheinmal ändern möchten.'
                ));
        }

        if (empty($method) || $method !== '1') {
            return $this->redirectTo('home', array(), array(
                    'status' => 'ERROR',
                    'text'   => 'Etwas ist schief gegangen. Kontaktieren Sie uns bitte telefonisch über unsere kostenfreie Hotline 0800 22 22 133 oder schreiben Sie uns eine E-Mail an support@arzttermine.de'
                )
            );
        }

        $user = $booking->getUser();
        $location = $booking->getLocation();
        $view->setRefs(array(
            'location' => $location,
            'user' => $user,
            'booking' => $booking
        ));

        // Update the current offer and cancel other offers for the same booking
        if ($bookingOffer->updateStatusAccepted()) {
            if ($bookingOffer->isBookedInPractice()) {
                // The booking was already booked in the practice
                $status = Booking::STATUS_RECEIVED_ALTERNATIVE;
            } else {
                $status = Booking::STATUS_PENDING_BOOKING_OFFER_ACCEPTED;
            }

            $booking
                ->setStatus($status)
                ->setUserId($bookingOffer->getUserId())
                ->setLocationId($bookingOffer->getLocationId())
                ->setAppointmentStartAt($bookingOffer->getAppointmentStartAt());

            $resultSave = $booking->save(array('status', 'appointment_start_at'));
            $resultSalesforce = $booking->setBookingOfferSlug($bookingOffer->getSlug())->updateSalesforce();

            if (!$resultSave || !$resultSalesforce) {
                AdminMailer::send(
                    $GLOBALS['CONFIG']["SALESFORCE_ALERT_MESSAGES_EMAILS"],
                    "Updating status for booking (ID {$booking->getId()}) failed.",
                    'Booking::saveStatus() failed',
                    basename(__FILE__),
                    __LINE__
                );
            } else {
                // send an email
                if ($bookingOffer->isBookedInPractice()) {
                    Booking::sendBookingEmail(
                        $booking,
                        'booking-new-confirmed.html'
                    );
                    SMS::sendBookingConfirmationSms($booking);
                }

                return $this->redirectTo('booking_profile', array('slug' => $booking->getSlug()));
            }
        }

        return $this->redirectTo('home', array(), array(
            'status' => 'ERROR',
            'text'   => 'Etwas ist schief gegangen. Kontaktieren Sie uns bitte telefonisch über unsere kostenfreie Hotline 0800 22 22 133 oder schreiben Sie uns eine E-Mail an support@arzttermine.de'
       ));
    }
}