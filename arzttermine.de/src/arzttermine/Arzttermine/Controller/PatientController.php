<?php

namespace Arzttermine\Controller;

use Arzttermine\Booking\Booking;
use Arzttermine\Form\Form;
use Arzttermine\Form\PatientProfileType;
use Arzttermine\Form\PatientRegistrationType;
use Arzttermine\Insurance\Insurance;
use Arzttermine\Mail\Mailing;
use Arzttermine\User\Patient;
use Symfony\Component\Form\FormError;

class PatientController extends Controller {

    /**
     * Patient's dashboard
     */
    public function accountAction()
    {
        $request = $this->getRequest();
        $view = $this->getView();

        $patient = new Patient();
        $patient = $patient->getCurrentUser();

        // @todo Check group here maybe
        if (!$patient->isValid()) {
            $this->redirectTo('patient_login', array(), array('status' => 'ERROR', 'text' => 'You must be logged in to view this page.'));
        }
        
        $errors = array();
        
        if ($request->isPost()) {
            $errors = $this->saveProfile($patient);
        }
        
        $view
            ->setRefs(array(
                'patient' => $patient,
                'error_messages' => $errors,
                'errors' => array_keys($errors))
            );

        $GLOBALS['CONFIG']['SOCIALMEDIA_ACTIVATE'] = false; // @todo Remove
        return $view->display('patient/account.tpl');
    }

    /**
     * Register a new patient
     */
    public function registerAction()
    {
        $request = $this->getRequest();
        $view = $this->getView();

        $patient = new Patient();
        $patient = $patient->getCurrentUser();

        // @todo If the user is already logged in, just forward them to the account page
        if (!$request->isPost() && $patient->isValid()) {
            $this->redirectTo('patient_account');
        }

        $errors = array();
        $form = Form::getForm(new PatientRegistrationType());
        
        if ($request->isPost()) {
            $errors = $this->register($form);
            
            if (empty($errors)) {
                $this->redirectTo('patient_login', array(), 'Sie haben sich erfolgreich registriert! Um Ihr Konto zu aktivieren, klicken Sie bitte auf den Link in der Bestätigungs-E-Mail');
            }
        }

        $view
            ->setRefs(array(
                'patient' => $patient,
                'error_messages' => $errors,
                'errors' => array_keys($errors),
                'data' => $form->getData()
            ));

        $GLOBALS['CONFIG']['SOCIALMEDIA_ACTIVATE'] = false; // @todo Remove
        return $view->display('patient/register.tpl');
    }

    /**
     * @param \Symfony\Component\Form\Form $form
     *
     * @return array
     */
    protected function register(\Symfony\Component\Form\Form $form)
    {
        $request = $this->getRequest();

        $patient = new Patient();
        $errors = array();

        $form->submit($request->getRequest());
        $data = $form->getData();

        if ($form->isValid()) {
            // If the email is in use, don't process the application
            if (!Patient::isEmailAvailable($data['email'])) {
                $errors['email'] = array('Diese Mailadresse wird bereits verwendet.');
                return $errors;
            }

            $patient
                ->setFirstName(sqlsave($data['first_name']))
                ->setLastName(sqlsave($data['last_name']))
                ->setGender(sqlsave($data['gender']))
                ->setEmail(sqlsave($data['email']))
                ->setPhone(sqlsave($data['phone']))
                ->setInsurance(new Insurance($data['insurance']))
                ->saveNewObject();
            
            // Change the password to the one supplied
            $patient->changePassword($data['password']);
            
            // Everything's done, send an activation email
            $patient->sendActivationEmail();
        } else {
            $errors = Form::getFormErrors($form);
        }
        
        return $errors;
    }

    /**
     * Activate a user using the given code
     */
    public function activateAction()
    {
        $request = $this->getRequest();

        $patient = new Patient();
        $code = $request->getParam('code');
        $patient->load($code, 'password_reset_code');
        
        if (!$code || !$patient->isValid()) {
            $this->redirectTo('patient_login', array(), array('status' => 'ERROR', 'text' => 'Ungültiger Aktivierungs-Code.'));
        }
        
        $patient->activate();

        $this->redirectTo('patient_login', array(), 'Ihr Konto wurde erfolgreich aktiviert.');
    }

    /**
     * @param Patient $patient
     * 
     * @return array
     */
    protected function saveProfile(Patient $patient)
    {
        $request = $this->getRequest();
        $errors = array();

        // Defaults for data we're expecting
        $defaults = array(
            'gender'     => '',
            'first_name' => '',
            'last_name'  => '',
            'phone'      => '',
            'insurance'  => '',
            'current_password'   => '',
            'new_password'       => ''
        );

        // Combine parameters from the query (POST) that match the keys in $defaults
        $parameters = array_replace_recursive($defaults, $request->getRequest());

        $form = Form::getForm(new PatientProfileType());
        $form->submit($parameters);

        // The user is trying to change their password, so do the following little dance
        // --------------------------------------------------------------------------------
        $old_password = $form->get('current_password')->getData();
        $new_password = $form->get('new_password')->getData();
        
        // We have an old...
        if ($old_password) {
            // ... but no new
            if (!$new_password) {
                $form->get('new_password')->addError(new FormError('Please enter a new password'));
            }
            
            // ... but it's wrong
            if (!$patient->checkCredentials($patient->getEmail(), $old_password)) {
                $form->get('current_password')->addError(new FormError('Current password not correct'));
            }
        }
        
        // We have a new, but no old
        if ($new_password && !$old_password) {
            $form->get('current_password')->addError(new FormError('Please enter your current password'));
        }
        // --------------------------------------------------------------------------------
        
        if ($form->isValid()) {
            // Grab the form data once it's submitted and validated
            $data = $form->getData();
            $insurance = new Insurance($data['insurance']);
            
            // Set above. Indicates we should use the new password
            if ($old_password && $new_password) {
                $patient->changePassword($new_password);
            }
            
            // @todo Check location with GMaps here
            $patient
                ->setGender($data['gender'])
                ->setFirstName($data['first_name'])
                ->setLastName($data['last_name'])
                ->setPhone($data['phone'])
                ->setInsurance($insurance)
                ->save();

            $this->addSessionMessage('Profile successfully updated');

        } else {
            $errors = Form::getFormErrors($form);
        }
        
        return $errors;
    }

    /**
     * Crappy hacky login function that wraps some shims for patient authentication
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAction()
    {
        $view = $this->getView();
        $request = $this->getRequest();
        
        $patient = new Patient();
        $profile = $patient->getCurrentUser();

        if ($request->isPost()) {
            if ($profile->login($request->getRequest('email'), $request->getRequest('password'))) {
                return $this->redirect($request->getQuery('referrer', $this->generateUrl('patient_account')));
            } else {
                $this->addSessionMessage('Bitte überprüfen Sie Ihre E-Mail-Adresse/Passwort', 'ERROR');
            }
        }
        
        // Crappy little hack to add logout to patient area
        if ($request->getQuery('action') === 'logout') {
            $patient->logout();
            $this->redirectTo('patient_login', array(), 'You have been logged out');
        }

        return $view
            ->setRef('email', $request->getRequest('email'))
            ->addHeadTitle('Patienten Login')
            ->display('patient/login.tpl');
    }

    /**
     * Show all appointments for this user
     */
    public function appointmentsAction()
    {
        $view = $this->getView();

        $patient = new Patient();
        $patient = $patient->getCurrentUser();

        // @todo Check group here maybe
        if (!$patient->isValid()) {
            $this->redirectTo('patient_login', array(), array('status' => 'ERROR', 'text' => 'You must be logged in to view this page.'));
        }
        
        $upcoming_bookings = $patient->getBookings('upcoming');
        $pending_bookings = $patient->getBookings('requests', 'created_at', 8); // "Terminanfragen"
        $past_bookings = $patient->getBookings('past');

        $view->setRefs(array(
            'patient' => $patient,
            'upcoming_bookings' => $upcoming_bookings,
            'pending_bookings' => $pending_bookings,
            'past_bookings' => $past_bookings
        ));

        $GLOBALS['CONFIG']['SOCIALMEDIA_ACTIVATE'] = false; // @todo Remove
        return $view->display('patient/appointments.tpl');
    }

    /**
     * Endpoint for miscellaneous data requests
     */
    public function dataAction()
    {
        $request = $this->getRequest();
        $response = array();
        
        if (!$request->isXmlHttpRequest()) {
            $this->redirectTo('home');
        }
        
        switch ($request->getParam('type')) {
            case 'cancel_appointment':
                $patient = new Patient();
                $patient = $patient->getCurrentUser();
                
                if ($patient->isActive()) {
                    // The booking needs to be valid and owned by the requesting user
                    // We do this by checking against email addresses, as that's the only key we have :/
                    $booking = new Booking(intval($request->getRequest('booking_id')));
                    if (
                        $booking->isValid() &&
                        mb_strtolower($patient->getEmail(), $GLOBALS['CONFIG']['SYSTEM_CHARSET']) === mb_strtolower($booking->getEmail(), $GLOBALS['CONFIG']['SYSTEM_CHARSET'])
                    ) {
                        $integration = $booking->getIntegration();
                        $integrationId = $integration->getId();

                        $result = $integration->cancelBooking($booking);
                        if ($result === true) {
                            // Save the status and store in Salesforce
                            $booking->saveStatus(Booking::STATUS_CANCELLED_BY_PATIENT_LOGIN, true);
                            
                            // Update the integration status as these integrations dont do it by themselves after calling cancelBooking() (@todo: but should)
                            if ($integrationId == 1 || $integrationId == 8) {
                                $booking->saveIntegrationStatus(BOOKING_INTEGRATION_STATUS_CANCELED);
                            }

                            $patient->sendNotificationEmails($booking, $booking->getUser(), $booking->getLocation());

                            $response = array('status' => 'success', 'booking_status' => $booking->getStatusPatientText(Booking::STATUS_CANCELLED_BY_PATIENT_LOGIN));
                        } else {
                            $response = array('status' => 'error', 'message' => $result);
                        }
                    } else {
                        $response = array('status' => 'error', 'message' => 'No access to this action');
                    } 
                } else {
                    $response = array('status' => 'error', 'message' => 'Invalid patient');
                }
                
                break;
            default:
                break;
        }

        $request->json($response);
    }

    /**
     * Forgot password action. Receives POST
     */
    public function forgotPasswordAction()
    {
        $request = $this->getRequest();

        if (!$request->isPost()) {
            $this->redirectTo('patient_account');
        }

        // Quick and nasty validation for email
        $email = strtolower($request->getRequest('email'));
        $email_is_valid = filter_var($email, FILTER_VALIDATE_EMAIL);

        if (!$email_is_valid) {
            $this->redirectTo('patient_login', array(), array('status' => 'ERROR', 'text' => 'Diese E-Mail-Adresse ist ungültig'));
        }

        // Only send the password reset request if we've got a user in the DB
        $patient = new Patient($email, 'email');
        if ($patient->isActive()) {
            $reset_password_code = $patient->generateResetPasswordCode();
            $patient->setResetPasswordCode($reset_password_code);
            $patient->save('password_reset_code');
            $this->sendPasswordEmail($patient, $reset_password_code);
        }

        // Tell the user the password reset was succesful even if they don't exist. Designed to stop bots figuring out email addresses
        $this->redirectTo('patient_login', array(), 'Anfrage zum Passwort-Reset wurde gesendet. Bitte überprüfen Sie Ihr E-Mail-Postfach.');
    }

    /**
     * @param Patient $patient
     * @param string $reset_password_code
     */
    protected function sendPasswordEmail(Patient $patient, $reset_password_code)
    {
        $mailing = new Mailing();
        $mailing->type = MAILING_TYPE_TEXT;
        $mailing->content_filename = '/mailing/patients/forgot-password.txt';
        $mailing->data['domain'] = $GLOBALS['CONFIG']['DOMAIN'];
        $mailing->data['url'] = $this->generateUrl('patient_reset_password', array('code' => $reset_password_code), true);
        $mailing->data['patient_name'] = $patient->getName();
        $mailing->addAddressTo($patient->getEmail());
        $mailing->send();
    }

    /**
     * @todo Check that the user isn't resetting their password to the same thing. Possibly redundant
     *
     * @param string $reset_password_code
     * 
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function resetPasswordAction($reset_password_code = '')
    {
        $request = $this->getRequest();
        $view = $this->getView();
        
        // No code means no dice
        if (!$reset_password_code) {
            $this->redirectTo('patient_login');
        }
        
        // We need to update the password at this stage
        // @todo Make this a standalone FormType with validators etc
        if ($request->isPost()) {
            $patient = new Patient($reset_password_code, 'password_reset_code');
            
            // Patient doesn't exist. Usually because the code is wrong
            if (!$patient->isValid()) {
                $this->redirectTo('patient_login', array(), array('status' => 'ERROR', 'text' => 'Falscher Passwortreset-Code'));
            }

            // Passwords need to match and not be empty
            $password = $request->getRequest('password');
            $confirm  = $request->getRequest('repeat_password');

            if (empty($password)) {
                $this->redirectTo('patient_reset_password', array('code' => $reset_password_code), array('status' => 'ERROR', 'text' => 'Bitte geben Sie ein Passwort an.'));
            }

            if ($password !== $confirm) {
                $this->redirectTo('patient_reset_password', array('code' => $reset_password_code), array('status' => 'ERROR', 'text' => 'Die Passwörter stimmen nicht überein.'));
            }

            $patient->changePassword($request->getRequest('password'));

            $this->redirectTo('patient_login', array(), 'Passwort wurde erfolgreich geändert.');
        }

        $GLOBALS['CONFIG']['SOCIALMEDIA_ACTIVATE'] = false; // @todo Remove
        return $view->display('patient/reset-password.tpl');
    }
}