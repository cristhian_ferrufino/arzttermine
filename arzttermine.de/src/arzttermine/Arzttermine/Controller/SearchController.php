<?php

namespace Arzttermine\Controller;

use Arzttermine\Insurance\Insurance;
use Arzttermine\Calendar\Calendar;
use Arzttermine\Location\District;
use Arzttermine\Maps\GoogleMap;
use Arzttermine\MedicalSpecialty\MedicalSpecialty;
use Arzttermine\Search\Doctor;
use Arzttermine\Search\Search;
use Arzttermine\Search\Text;
use NGS\DAL\DTO\MedicalSpecialtiesDto;
use NGS\Managers\MedicalSpecialtiesManager;

class SearchController extends Controller {

    /**
     * Take a permalink (url) and try parse meaningful values from them
     * 
     * @param string $permalink
     *
     * @return array
     */
    protected function getPermalinkSearchTerms($permalink = '')
    {
        $parameters = array();
        $search_result_text = new Text(sqlsave('/' . trim($permalink, '/')), 'permalink');

        if ($search_result_text->isValid() && $search_result_text->getStatus() == SEARCH_RESULT_TEXT_ACTIVE) {
            $parameters['medical_specialty'] = new MedicalSpecialty($search_result_text->getMedicalSpecialtyId());
            $parameters['insurance'] = new Insurance($search_result_text->getInsuranceId());
            $parameters['location'] = sanitize_location($search_result_text->getAddressSearch());
            $parameters['district'] = new District($search_result_text->getDistrictId());
            $parameters['search_result_text'] = $search_result_text;
        }
        
        return $parameters;
    }

    /**
     * Stub to add meta tags when none have been specified
     * 
     * @param MedicalSpecialty $medical_specialty
     * @param string $location
     */
    protected function setDefaultMetaTags(MedicalSpecialty $medical_specialty = null, $location = '')
    {
        $view = $this->getView();
        
        if (!$view->getMetaTag('description')) {
            if (!empty($medical_specialty) && $medical_specialty->isValid()) {
                $location_title = '';
                if (!empty($location)) {
                    $location_title = ' in ' . $location;
                }
                $view->setMetaTag('description', $medical_specialty->getName() . ' - Termin online buchen' . $location_title . ' - Finden Sie den passenden Arzt und buchen Sie Ihren Wunschtermin schnell und einfach online.');
            } else {
                $view->setMetaTag('description', 'Arzttermin online buchen - Finden Sie den passenden Arzt und buchen Sie Ihren Wunschtermin schnell und einfach online.');
            }
        }

        if (!$view->getMetaTag('keywords')) {
            $view->setMetaTag('keywords', 'Arzttermin, Arzttermine, online, buchen, Wunschtermin');
        }

        if ($view->getHeadTitleCount() === 1 && !empty($location)) {
            $pagetitle = 'Arzttermine in ' . ucfirst(title($location));
            if (!empty($medical_specialty) && $medical_specialty->isValid()) {
                $pagetitle.=' beim '.$medical_specialty->getName();
            }
            $view->addHeadTitle($pagetitle);
        }
    }
    
    /**
     * A catch-all SEO-text search parser
     * 
     * @param string $permalink
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function searchAction($permalink = '')
    {
        $permalink = str_replace('.', '', $permalink);
        $parameters = $this->getPermalinkSearchTerms($permalink);

        if (!empty($parameters)) {
            // Assign view variables. We assume all data in the database is valid.
            // @todo: Don't assume this
            if (!empty($parameters['search_result_text'])) {
                $view = $this->getView();
                
                /** @var Text $text */
                $text = $parameters['search_result_text'];
                
                // Title
                if ($text->hasHtmlTitle()) {
                    $view->addHeadTitle(title($text->getHtmlTitle()));
                }

                // Description
                if ($text->hasHtmlMetaDescription()) {
                    $view->setMetaTag('description', meta($text->getHtmlMetaDescription()));
                }

                // Keywords
                if ($text->hasHtmlMetaKeywords()) {
                    $view->setMetaTag('keywords', meta($text->getHtmlMetaKeywords()));
                }

                // Generic content(?)
                if ($text->hasContent1()) {
                    $view->setRef('search_result_text_content_1', $text->getContent1());
                }
            }
            return $this->indexAction(
                !empty($parameters['medical_specialty']) ? $parameters['medical_specialty']->getSlug() : '',
                !empty($parameters['location'])          ? $parameters['location'] : '',
                !empty($parameters['insurance'])         ? $parameters['insurance']->getSlug() : ''
            );
        } else {
            return $this->indexAction('', $permalink, '');
        }
    }

    /**
     * @param string $medical_specialty_slug
     * @param string $location_slug
     * @param string $insurance
     * 
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction($medical_specialty_slug = '', $location_slug = '', $insurance = '')
    {
        global $CONFIG;
        
        $request = $this->getRequest();
        $view = $this->getView();

        /** @var \Arzttermine\Coupon\CouponService */
        $this->get('coupon_service')->enable('search');

        // An object representing this search request
        $search = new Search();

        $form = $request->getQuery('form', array());

        // Default form fields
        $form_fields = array(
            'medical_specialty_id' => 0,
            'location'             => '',
            'insurance_id'         => 0,
        );
        
        // Merge some defaults
        $form = array_merge($form_fields, $form);

        // Medical Specialty (from parameter)
        // ------------------------------------------------------------------------------------------------------
        if (!empty($medical_specialty_slug)) {
            $medical_specialty = new MedicalSpecialty($medical_specialty_slug, 'slug');
        } else {
            $medical_specialty = new MedicalSpecialty(intval($form['medical_specialty_id']));
        }

        if ($medical_specialty->isValid()) {
            $search->setMedicalSpecialty($medical_specialty);
        }

        if (!empty($form['treatmenttype_id'])) {
            $search->setTreatmentTypeId($form['treatmenttype_id']);
        }

        // Insurance (from parameter)
        // ------------------------------------------------------------------------------------------------------
        if (!empty($insurance)) {
            $insurance_type = new Insurance($insurance, 'slug');
        } else {
            $insurance_type = new Insurance(intval($form['insurance_id']));
        }

        if ($insurance_type->isValid()) {
            $search->setInsurance($insurance_type);
        }

        // Location (from parameter)
        // ------------------------------------------------------------------------------------------------------
        if ($location_slug && empty($form['location']))
            $form['location'] = $location_slug;

        $form['location'] = sanitize_location($form['location']);

        // Prepend exact location to city location if user has selected a city along with an exact location
        // NOTE: This is only for mobile(!), as we're using GPS data
        if ($exact_location = $request->getQuery('exact-location')) {
            $form['location'] = $exact_location . ($form['location'] ? ', ' . $form['location'] : '');
        }

        // Merge with values from url, but values from the form are higher priority
        $form = array_merge($form_fields, $form);

        // Try get the geo coordinates
        // @todo Detect failures
        if ($coords = GoogleMap::getGeoCoords($form['location'])) {
            $search->setSearchOrigin($coords);
        }

        // if there is a district selected than overwrite the lat & lng
        if (!empty($form['district_id'])) {
            $district = new District(intval($form['district_id']));
            if ($district->isValid()) {
                $search->setSearchOrigin($district->getGeoCoords());
                $view->setRef('district', $district);
            }
        }
        
        // Set the radius of the search (in km)
        if (!empty($form['distance'])) {
            $search->setRadius($form['distance']);
        }
        
        // Set the insurance. This is only set when parameters are not (or at least, that's the idea)
        if (!empty($form['insurance_id'])) {
            $search->setInsuranceId($form['insurance_id']);
        }

        // Set the start date of the search
        if ($date_start = $request->getQuery('date_start')) {
            $search->setDateBounds($date_start);
        }

        // For pagination
        $page = $request->getQuery('p', 1);
        $search->setPage($page);

        // Final chance to modify the search
        $this->dispatchEvent('search.before_search', $search);
        
        // Do the search(es)
        $doctors_and_locations = $search->findDoctorsAndLocations();
        $results = $doctors_and_locations->asObjects();
        $appointments = $search->findAppointments($results);

        // Zip the doctors/locations results with appointments
        foreach ($results as $result) {
            $result->findAppointments($appointments);
        }

        // Set default tags if possible
        $this->setDefaultMetaTags($search->getMedicalSpecialty(), $form['location']);
        
        // The following block is a complete piece fucking garbage. Refactor and move
        // ==========================================================================================
        // Get medical specialty values used for form elements
        $putMedicalSpecialityAndTreatmentTypesMap = $this->getMedicalSpecialityAndTreatmentTypesMap();
        $view->setRef('MedicalSpecialityAndTreatmentTypesMap', json_encode($putMedicalSpecialityAndTreatmentTypesMap));
        $view->setRef('MedicalSpecialityAndTreatmentTypesMapArray', $putMedicalSpecialityAndTreatmentTypesMap);

        $view->setRef('MedicalSpecialtyOptions', MedicalSpecialty::getHtmlOptions());
        $date_calendar_days = 20;
        $date_start = $request->getQuery('date_start');
        if (empty($date_start)) {
            $date_start = Calendar::getFirstPossibleDateTimeForSearch();
        }
        // minus 1 to include the date_start
        $date_end = Calendar::addDays($date_start, ($date_calendar_days - 1), true);
        $date_days = count(Calendar::getDatesInAreaWithFilter($date_start, $date_end));
        $view->setRef('date_start', $date_start);
        $view->setRef('date_end', $date_end);
        $view->setRef('date_calendar_days', $date_calendar_days);
        $view->setRef('date_days', $date_days);
        // Merge the location into the search parameters, and build the API URL
        $map_marker_api_url = $CONFIG['URL_API'] . '/search-markers.json?' . http_build_query(array_merge(array('location' => $form['location']), $search->getParameters()));
        // ==========================================================================================

        return $view
            ->setRef('form', $form)
            ->setRef('search', $search)
            ->setRef('results', $results)
            ->setRef('marker_url', $map_marker_api_url)
            ->setRef('mapCoordinates', $form['location'])
            ->setRef('googleTagManagerDataLayerJs', $view->fetch('js/tagmanager/datalayer/search.tpl'))
            ->display('search/search.tpl');
    }

    /**
     * @return array
     */
    protected function getMedicalSpecialityAndTreatmentTypesMap()
    {
        $select_location_medical_specialty_treatmenttypes_options = array();

        /** @var MedicalSpecialtiesDto[] $allMedicalSpecialtiesWithTreatmentTypes */
        $_medical_specialty = new MedicalSpecialty();
        $_medical_specialties = $_medical_specialty->getMedicalSpecialties();

        foreach ($_medical_specialties as $mstt) {
            $ttypes = $mstt->getTreatmentTypes();
            foreach ($ttypes as $tt) {
                $select_location_medical_specialty_treatmenttypes_options[$tt->getMedicalSpecialtyId()][$tt->getId()] = $tt->getName();
            }
        }
        
        return $select_location_medical_specialty_treatmenttypes_options;
    }

    /**
     * checks for valid attrs and add error messages on wrong values
     *
     * @param array
     *
     * @return array
     */
    protected function checkParameters($form)
    {
        $form['location'] = sanitize_location($form['location']);

        if (empty($form['location']) || mb_strlen($form['location'], $GLOBALS['CONFIG']['SYSTEM_CHARSET']) < 3) {
            $form['location'] = 'Berlin';
        }

        if (!isset($form['medical_specialty_id']) || !MedicalSpecialty::idExists($form['medical_specialty_id'])) {
            $form['medical_specialty_id'] = 1;
        }

        if (!isset($form['insurance_id']) || !isset($GLOBALS['CONFIG']['INSURANCES'][$form['insurance_id']])) {
            $form['insurance_id'] = 1;
        }

        return $form;
    }

    /**
     * We have hit a custom search page (e.g. LASIK)
     *
     * @param string $type What type of custom page we want. Comes from route
     * 
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function customAction($type = '')
    {
        $view = $this->getView();

        $json = '';
        $ids = array();
        $source = SYSTEM_PATH . '/include/data/search/' . $type . '.json';

        // @todo Location ok for storage?
        try {
            if (!file_exists($source)) {
                throw new \Exception();
            }

            $data = file_get_contents($source);
            $json = json_decode($data, true);
        } catch (\Exception $e) {
            $this->notFound();
        }

        foreach ($json['doctors'] as $value) {
            $ids[] = $value['id'];
        }

        // This function sets view vars :/
        Doctor::getDoctorsById($ids);

        return $view->setRefs(
            array(
                'doctors'                 => $json['doctors'],
                'lat'                     => $json['origin']['lat'],
                'lng'                     => $json['origin']['lng'],
                'hideFilters'             => (bool)$json['hideFilters'],
                'hideHeadline'            => (bool)$json['hideHeadline'],
                'pageType'                => 'custom-search',
                'additional_body_classes' => $type
            )
        )->display('search/search.tpl');
    }
}
