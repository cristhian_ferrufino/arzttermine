<?php

namespace Arzttermine\Controller;

use Arzttermine\User\User;
use Arzttermine\Location\Location;
use Arzttermine\MedicalSpecialty\MedicalSpecialty;
use Doctrine\Common\Cache\FilesystemCache;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use Kevinrob\GuzzleCache\CacheMiddleware;
use Kevinrob\GuzzleCache\Storage\DoctrineCacheStorage;
use Kevinrob\GuzzleCache\Strategy\GreedyCacheStrategy;
use Symfony\Component\HttpFoundation\Response;

class HomepageController extends Controller {

    /**
     * Homepage
     */
    public function indexAction()
    {
        $request = $this->getRequest();
        $view = $this->getView();

        $this->getMagazinQuote();
        $this->getFeaturedDoctors();
        $this->getHomepageQuote();

        /** @var \Arzttermine\Coupon\CouponService */
        $this->get('coupon_service')->enable('homepage');

        $form = array(
            'medical_specialty_id' => intval($request->query->get('medical_specialty_id')),
            'insurance_id'         => intval($request->query->get('insurance_id')),
            'location'             => $request->query->get('location')
        );
        $cookie = array();
        if($form['location'] == null) {
            $form['location'] = $view->getUserCity();

            if($form['location'] !== null && $request->cookies->has('at_user_city') === false) {
                $cookie = array('name' => 'at_user_city',
                    'value' => $form['location'],
                    'path' => '/',
                    'time' => time() + 3600 * 24 * 7
                );
            }
        }

        $view->setRef('form', $form);

        // Randomizing the index for the css class
        $view->setRef('cinema_index', rand(1, 7));

        // Set possible medical_specialty_ids for location select box
        $view->setRef('select_location_medical_specialty_id_options', MedicalSpecialty::getSelectLocationMedicalSpecialtyIdsJavascriptOptions());
        $view->setRef('MedicalSpecialtyOptions', MedicalSpecialty::getHtmlOptions());
        $view->setRef('googleTagManagerDataLayerJs', $view->fetch('js/tagmanager/datalayer/homepage.tpl'));

        // Meta
        return $view
            ->addHeadTitle($this->trans('content.homepage.title'))
            ->display('homepage.tpl', Response::HTTP_OK, $cookie);
    }

    /**
     * Grab the doctors that will be featured on the homepage
     */
    protected function getFeaturedDoctors()
    {
        global $CONFIG;
        $cache = (extension_loaded('Memcached'))?new \Memcached():new \Memcache();
        $cache->addServer($CONFIG['MEMCACHED_SERVER'], 11211);

        $cachekey = "featured-doctors";
        $featured_doctors = $cache->get($cachekey);
        if($featured_doctors === false) {
            $_user = new User();
            /** @var User[] $users */
            $users = $_user->getUsersByIds(array(9500, 641, 1562, 5919, 427));

            $featured_doctors = array();

            foreach ($users as $user) {
                // Doctor's locations
                $default_location = $user->getDefaultLocation();
                if (!$default_location instanceof Location) {
                    continue;
                }

                // Doctor's specialties
                $default_specialty = $user->getDefaultMedicalSpecialty();
                if (!$default_specialty instanceof MedicalSpecialty) {
                    continue;
                }

                $featured_doctors[$user->getId()] = array(
                    'name' => $user->getFullName(),
                    'url' => $user->getUrl(),
                    'profile' => $user->getProfileAssetUrl(4),
                    'city' => $default_location->getCity(),
                    'specialty' => $default_specialty->getName(),
                    'specialty_id' => $default_specialty->getId(),
                );
            }
            if($cache instanceof \Memcached) {
                $cache->set($cachekey, $featured_doctors, 432000);
            } else {
                $cache->set($cachekey, $featured_doctors, 0, 432000);
            }
        }

        $this->getView()->setRef('featured_doctors', $featured_doctors);
    }

    /**
     * Set the homepage quotes. Looks kludgey
     */
    protected function getHomepageQuote()
    {
        $available_quotes = array(
            array(
                'user'    => 'Serap V.',
                'profile' => '/static/images/content/doc-5.jpg',
                'content' => 'Ich wurde sehr nett aufgenommen. Die Behandlung war sehr angenehm und die Ärztin sehr kompetent und hat sich Zeit für mich genommen. Lange mußte ich nicht warten. Sehr gut ist die Parkplatzsituation - gar kein Problem.'
            ),
            array(
                'user'    => 'Juliane S.',
                'profile' => '/static/images/content/doc-5.jpg',
                'content' => 'Ich hatte einen sehr guten Eindruck. Die Wartezeit war kurz, die MitarbeiterInnen waren sehr nett und freundlich. Auch Frau Dr. Labin war sehr nett, sie hat sich Zeit genommen und ich konnte Fragen stellen. Das Polikum ist gut erreichbar durch die U1 und diverse Busse.'
            ),
            array(
                'user'    => 'Paulina G.',
                'profile' => '/static/images/content/doc-5.jpg',
                'content' => 'Kompetenz und Freundlichkeit in perfekter Kombination. Ich fühlte mich gleich gut aufgehoben. Diese Praxis möchte ich gerne weiter empfehlen.'
            ),
            array(
                'user'    => 'Karl K.',
                'profile' => '/static/images/content/doc-5.jpg',
                'content' => 'Wie immer sehr freundlich und professionell ohne Wartezeit. Vielen herzlichen Dank!'
            ),
        );

        shuffle($available_quotes);

        $this->getView()->setRef('quote', $available_quotes[0]);
    }

    protected function getMagazinQuote()
    {
        //get magazin.arzttermine.de latest 3 blog posts
        $stack = HandlerStack::create();
        $stack->push(new CacheMiddleware(new GreedyCacheStrategy(
            new DoctrineCacheStorage(
                new FilesystemCache('/tmp/apicache')
            ),
            3600
        )), 'cache');

        $client = new Client(['handler' => $stack]);
        try {
            $body = $client->get('https://www.arzttermine.de/magazin/wp-json/wp/v2/posts', ['query' => ['orderby' => 'date', 'per_page' => '3']])->getBody();
            $obj = json_decode($body);

            $news = array();
            foreach($obj as $item)
            {
                $text = substr($item->excerpt->rendered, 0, 100);
                $text = preg_replace('#[^\s]*$#s', '', $text) . '...';

                $newsitem = array('id'=>$item->id, 'excerpt'=>$text, 'link'=>$item->link, 'title'=>$item->title->rendered);

                $mediaurl = $item->_links->{'wp:featuredmedia'};
                if($mediaurl !== null) {
                    $mediabody = $client->get($mediaurl[0]->href)->getBody();
                    $mediaobj = json_decode($mediabody);
                    $newsitem['img'] = $mediaobj->media_details->sizes->medium->source_url;
                }
                $news[] = $newsitem;
            }
            sort($news);
            $this->getView()->setRef('news', $news);
        }  catch (\Exception $e) {
        }
    }

    protected function getUserCity($clientIp)
    {
        $adapter = new \Http\Adapter\Guzzle6\Client();
        $provider = new \Geocoder\Provider\FreeGeoIp\FreeGeoIp($adapter);
        $geocoder = new \Geocoder\StatefulGeocoder($provider, 'de');
        try {
            $geocode = $geocoder->geocode($clientIp);
            return $geocode->first()->getLocality();
        } catch (\Exception $e) {
            return null;
        }
    }
}
