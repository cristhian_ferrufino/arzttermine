<?php

namespace Arzttermine\Controller;

use Symfony\Component\HttpFoundation\Response;

class PageController extends Controller {

    /**
     * We can't find anything (404 page)
     * 
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function fourohfourAction()
    {
        return $this->getView()
            ->addHeadTitle('404')
            ->display('404.tpl', Response::HTTP_NOT_FOUND);

    }
}