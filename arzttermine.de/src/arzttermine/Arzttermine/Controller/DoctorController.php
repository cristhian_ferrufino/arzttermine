<?php

namespace Arzttermine\Controller;

use Arzttermine\Application\Application;
use Arzttermine\Appointment\Provider;
use Arzttermine\Booking\Booking;
use Arzttermine\Insurance\Insurance;
use Arzttermine\Location\Location;
use Arzttermine\Mail\Mailing;
use Arzttermine\Maps\GoogleMap;
use Arzttermine\Review\Review;
use Arzttermine\Search\Search;
use Arzttermine\User\User;

class DoctorController extends Controller {

    /**
     * @param string $slug
     * @param string $insurance
     * 
     * @return \Symfony\Component\HttpFoundation\Response;
     */
    public function profileAction($slug = '', $insurance = '')
    {
        $request = $this->getRequest();
        $view = $this->getView();
        $insurance_slug = $insurance; // kludge

        // We need a user, and we need locations. Without these, we need to 404
        // --------------------------------------------------------------------
        $user = new User($slug, 'slug');

        if (!$user->isValid() || !$user->isProfileVisible()) {
            $this->notFound();
        }

        // Get the providers for this user, filtering the invisible ones
        $locations = $user->getLocations();

        // If the widget provides a location then limit the result to that
        if ($location_id = intval($request->getQuery('l'))) {
            // Then filter all locations based on the passed location ID
            $filteredLocations = array_filter(
                $locations, function ($l) use ($location_id) {
                    /** @var Location $l */
                    return $l->getId() == $location_id;
                }
            );
            $locations = $filteredLocations;
        }

        if (!count($locations)) {
            $this->notFound();
        }

        foreach($locations as $location) {
            if ($location->getLat() == 0 && $location->getLng() == 0) {
                if ($coords = GoogleMap::getGeoCoords($location->getAddress(', '))) {
                    $location->setLat($coords['lat']);
                    $location->setLng($coords['lng']);
                    $location->save(array('lat', 'lng'));
                }
            }
        }

        // Set up the provider query using our normal search engine object
        // --------------------------------------------------------------------
        $search = new Search();
        
        // These are for the widget
        if ($date_start = $request->getQuery('date_start')) {
            $search->setDateBounds($date_start);
        }

        // Replace the slug with a real insurance object
        $insurance = new Insurance();
        if (!empty($insurance_slug)) {
            $insurance->load($insurance_slug, 'slug');
        }
        if (!$insurance->isValid()) {
            $insurance->load(Insurance::GKV);
        }
        // tell the search which insurance we have selected
        $search->setInsurance($insurance);

        // Appointment provider objects
        $providers = array();

        // Get some basic provider objects so we can fetch appointments with them
        /** @var Location[] $locations */
        foreach ($locations as $location) {
            $provider = new Provider();
            $providers[] = $provider
                ->setDateRange($search->getStart(), $search->getEnd())
                ->setUser($user)
                ->setLocation($location)
                // @todo: Allow easy toggle for each of these
                ->setInsurance($insurance)
                ->setMedicalSpecialty($user->getDefaultMedicalSpecialty());
        }
        
        $this->dispatchEvent('search.before_search', $search);

        $appointments = $search->findAppointments($providers);
        /** @var Provider $provider */
        foreach ($providers as $provider) {
            $provider->findAppointments($appointments);
        }

        $this->get('coupon_service')->enable('search');

        return $view
            ->addHeadTitle(title($user->getHtmlTitle() . ', ' . $this->trans('Termin buchen')))
            ->setMetaTag('description', meta($user->getHtmlMetaDescription()))
            ->setMetaTag('keywords', meta($user->getHtmlMetaKeywords()))
            ->addHeadHtml('<link rel="canonical" href="' . $user->getUrl(true) . '" />') // as we use different urls to point to this profil page we should tell google where the origin is
            
            ->setRefs(array(
                'providers'    => $providers,
                'user'         => $user,
                'breadcrumbs'  => $user->getBreadcrumbs($locations, array('insurance_id' => $insurance->getId())),
                'search'       => $search,
                'insurance'    => $insurance
            ))
            
            // Hack for mobile :/
            ->setRef('marker_url', $GLOBALS['CONFIG']['URL_API'] . '/search-markers.json?' . http_build_query($search->getParameters()))
            ->setRef('googleTagManagerDataLayerJs', $view->fetch('js/tagmanager/datalayer/doctor_profile.tpl'))

            ->display('doctor/profile.tpl');
    }    

    /**
     * @param string $slug
     */
    public function reviewAction($slug = '')
    {
        $app = Application::getInstance();
        $view = $this->getView();
        $request = $this->getRequest();

        $GLOBALS['CONFIG']['SOCIALMEDIA_ACTIVATE'] = false;

        $booking = new Booking($slug, 'slug');
        $user = new User();
        $location = new Location();

        if ($booking->isValid()) {
            // load location and user
            $location->load($booking->getLocationId());
            if ($location->isValid()) {
                $user->load($booking->getUserId());
                if ($user->isValid()) {
                    // check if this review was already made
                    $review = new Review($booking->getId(), 'booking_id');
                    if ($review->isValid()) {
                        return $app->redirect(
                            $user->getUrl(), array(
                                    'status' => 'NOTICE',
                                    'text'   => 'Sie haben diesen Arzt für Ihren Termin bereits bewertet.'
                                )
                        );
                    }

                    $view->setRef('location', $location);
                    $view->setRef('user', $user);
                    $view->setRef('booking', $booking);

                    if ($request->isPost()) {
                        $form = $request->getRequest('form');
                        if (!$this->checkReviewForm($form)) {
                            $view->addMessage('STATUS', 'ERROR', $app->_('Bitte prüfen Sie Ihre Eingaben.'));
                            $view->setRef('contentMain', $this->getReviewForm($form));
                        } else {
                            $form['rated_at'] = $app->now('datetime');
                            $form['booking_id'] = $booking->getId();
                            $form['patient_email'] = $booking->getEmail();
                            $form['user_id'] = $user->getId();
                            $form['allow_sharing'] = $form['allow_sharing'] == '1' ? '1' : '0';
                            // set patient_name to NULL if there is none given
                            if ($form['patient_name'] == '') {
                                unset($form['patient_name']);
                            }

                            $review = new Review();
                            $_new_id = $review->saveNew($form);

                            if (!$_new_id) {
                                $view->addMessage('STATUS', 'ERROR', $app->_('Die Bewertung konnte nicht gespeichert werden. Bitte versuchen sie es später nocheinmal.'));
                            } else {

                                // @todo Use event listeners?
                                $user->updateRatings();
                                $user->updateRatingsForLocations();
                                $reviewAverage = ($review->getRating('rating_1') + $review->getRating('rating_2') + $review->getRating('rating_3'))/3;

                                if($reviewAverage <= 3) {
                                    $this->notifyAboutBadReview($user, $booking, 'arzttermine.de', $review);
                                } else {
                                    $app = Application::getInstance();
                                    $db = $app->container->get('database');
                                    $sql = "UPDATE reviews SET approved_at='".$app->now('datetime')."'";
                                    $db->query($sql);
                                }

                                if ($reviewAverage >= 4 && $user->hasPaidReview() && $location->getGooglePlaceId()!='') {
                                    $this->notifyAboutReview($user, $booking, 'arzttermine.de');

                                    return $app->redirect(
                                        $this->generateUrl('doctor_review_on_google',
                                            array('slug' => $booking->getSlug())),
                                        array(
                                            'status' => 'NOTICE',
                                            'text' => 'Vielen Dank für Ihre Bewertung.'
                                        )
                                    );
                                }

                                return $app->redirect(
                                    $user->getUrl(),
                                    array(
                                        'status' => 'NOTICE',
                                        'text' => 'Vielen Dank für Ihre Bewertung.'
                                    )
                                );
                            }
                        }
                    } else {
                        // set initial values
                        $form = array('allow_sharing' => '1', 'patient_name' => $booking->getName(false));
                        $view->setRef('contentMain', $this->getReviewForm($form));
                    }
                } else {
                    $this->notFound();
                }
            } else {
                $this->notFound();
            }
        } else {
            $this->notFound();
        }

        $view->addHeadTitle('Bewerten Sie ' . title($user->getName()));
        return $view->display('review-doctor/review-doctor.tpl');
    }

    /**
     * @param array $form
     *
     * @return string
     */
    protected function getReviewForm($form = array())
    {
        $view = $this->getView();

        $view->setRef('url_form_action', Application::getInstance()->getRequest()->getRequestUri());
        
        $form['patient_name'] = isset($form['patient_name']) ? $view->form($form['patient_name']) : '';
        $form['patient_name__form_input_class'] = $view->hasMessages('FORM_PATIENT_NAME', ' input_error', 1);
        $form['patient_name__message'] = $view->getHtmlFormMessages('FORM_PATIENT_NAME', 'text');

        $form['rate_text'] = isset($form['rate_text']) ? $view->form($form['rate_text']) : '';
        $form['rate_text__form_input_class'] = $view->hasMessages('FORM_RATE_TEXT', ' input_error', 1);
        $form['rate_text__message'] = $view->getHtmlFormMessages('FORM_RATE_TEXT', 'text');

        $form['rating_1'] = isset($form['rating_1']) ? $view->form($form['rating_1']) : 0;
        $form['rating_1__form_input_class'] = $view->hasMessages('FORM_RATING_1', ' input_error', 1);
        $form['rating_1__message'] = $view->getHtmlFormMessages('FORM_RATING_1', 'text');

        $form['rating_2'] = isset($form['rating_2']) ? $view->form($form['rating_2']) : 0;
        $form['rating_2__form_input_class'] = $view->hasMessages('FORM_RATING_2', ' input_error', 1);
        $form['rating_2__message'] = $view->getHtmlFormMessages('FORM_RATING_2', 'text');

        $form['rating_3'] = isset($form['rating_3']) ? $view->form($form['rating_3']) : 0;
        $form['rating_3__form_input_class'] = $view->hasMessages('FORM_RATING_3', ' input_error', 1);
        $form['rating_3__message'] = $view->getHtmlFormMessages('FORM_RATING_3', 'text');

        $form['rating_4'] = isset($form['rating_4']) ? $view->form($form['rating_4']) : 0;
        $form['rating_4__form_input_class'] = $view->hasMessages('FORM_RATING_4', ' input_error', 1);
        $form['rating_4__message'] = $view->getHtmlFormMessages('FORM_RATING_4', 'text');

        $view->setRef('form', $form);

        return $view->fetch('review-doctor/_p1.tpl');
    }

    /**
     * @param $form
     *
     * @return bool
     */
    protected function checkReviewForm($form)
    {
        $app = Application::getInstance();
        $view = $this->getView();

        $valid = true;

        $form['patient_name'] = sanitize_text_field($form['patient_name']);
        if (
            mb_strlen($form['patient_name'], $GLOBALS["CONFIG"]["SYSTEM_CHARSET"]) > 0
            &&
            mb_strlen($form['patient_name'], $GLOBALS["CONFIG"]["SYSTEM_CHARSET"]) < 3
        ) {
            $valid = false;
            $view->addMessage('FORM_PATIENT_NAME', 'ERROR', $app->_('Bitte geben Sie einen Namen ein, der mind. 3 Buchstaben lang ist'));
        }

        $form['rate_text'] = sanitize_textarea_field($form['rate_text']);
        if (mb_strlen($form['rate_text'], $GLOBALS["CONFIG"]["SYSTEM_CHARSET"]) <= 3) {
            $valid = false;
            $view->addMessage('FORM_RATE_TEXT', 'ERROR', $app->_('Bitte beschreiben Sie Ihre Erfahrungen bei diesem Arzt.'));
        }

        $form['rating_1'] = sanitize_option('int', $form['rating_1']);
        if ($form['rating_1'] < 1 || $form['rating_1'] > 5) {
            $valid = false;
            $view->addMessage('FORM_RATING_1', 'ERROR', $app->_('Bitte bewerten Sie diese Eigenschaft.'));
        }

        $form['rating_2'] = sanitize_option('int', $form['rating_2']);
        if ($form['rating_2'] < 1 || $form['rating_2'] > 5) {
            $valid = false;
            $view->addMessage('FORM_RATING_2', 'ERROR', $app->_('Bitte bewerten Sie diese Eigenschaft.'));
        }

        $form['rating_3'] = sanitize_option('int', $form['rating_3']);
        if ($form['rating_3'] < 1 || $form['rating_3'] > 5) {
            $valid = false;
            $view->addMessage('FORM_RATING_3', 'ERROR', $app->_('Bitte bewerten Sie diese Eigenschaft.'));
        }

        return $valid;
    }


    /**
     * @param string $slug
     */
    public function reviewOnGoogleAction($slug = '')
    {
        $view = $this->getView();
        $GLOBALS['CONFIG']['SOCIALMEDIA_ACTIVATE'] = false;

        $booking = new Booking($slug, 'slug');
        $user = new User();
        $location = new Location();

        if ($booking->isValid()) {
            // load location and user
            $location->load($booking->getLocationId());
            if ($location->isValid()) {
                $user->load($booking->getUserId());
                if ($user->isValid()) {

                    $view->setRef('location', $location);
                    $view->setRef('user', $user);
                    $view->setRef('booking', $booking);
                } else {
                    $this->notFound();
                }
            } else {
                $this->notFound();
            }
        } else {
            $this->notFound();
        }

        $view->addHeadTitle('Bewerten Sie ' . title($user->getName()));
        $googleReview = $this->generateUrl('submit_doctor_review_on_google', ['slug' => $slug]);
        $view->setRef('review', $googleReview);

        return $view->display('review-doctor/review-doctor-on-google.tpl');
    }

    /**
     * @param string $slug
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirectToGoogleReview($slug = '')
    {
        $GLOBALS['CONFIG']['SOCIALMEDIA_ACTIVATE'] = false;
        $booking = new Booking($slug, 'slug');

        $location = new Location();
        $location->load($booking->getLocationId());

        $user = new User();
        $user->load($booking->getUserId());

        if (!$booking->isValid() || !$location->isValid() || !$user->isValid()) {
            return $this->redirectTo('doctor_review_on_google', ['slug' => $slug]);
        }

        $target = $location->getPlainGooglePlaceReviewLink($slug);
        $this->notifyAboutReview($user, $booking, 'google');

        return $this->redirect($target);
    }

    /**
     * @param User $user
     * @param Booking $booking
     * @param string $platform
     */
    private function notifyAboutReview(User $user, Booking $booking, $platform)
    {
        $bodyText = '<p>New review submitted on {platform}:</p>Booking-ID: {bookingId}<br>Created at: {createdAt}<br>Doctor: {doctorName} ({doctorId})<br>Patient: {patientName} ({patientEmail})';

        $mailing = new Mailing();
        $mailing->type = MAILING_TYPE_TEXT;
        $mailing->content_id = 1;
        $mailing->subject = 'New review was submitted on ' . $platform;
        $mailing->addAddressTo('googlereview@arzttermine.de', 'arzttermine.de Reviews');
        $mailing->from = $GLOBALS['CONFIG']["BOOKINGS_EMAIL_ADDRESS"];
        $mailing->from_name = $GLOBALS['CONFIG']["BOOKINGS_EMAIL_NAME"];
        $mailing->body_text = $bodyText;
        $mailing->data = [
            'platform' => $platform,
            'doctorId' => $user->getId(),
            'doctorName' => $user->getFullName(),
            'patientName' => sprintf('%s %s', $booking->getFirstName(), $booking->getLastName()),
            'patientEmail' => $booking->getEmail(),
            'bookingId' => $booking->getId(),
            'createdAt' => date('Y-m-d H:i:s')
        ];

        $mailing->sendPlain(false);
    }

    /**
     * @param User $user
     * @param Booking $booking
     * @param string $platform
     */
    private function notifyAboutBadReview(User $user, Booking $booking, $platform, Review $review)
    {
        $bodyText = 'Bad review submitted on {platform}:
Booking-ID: {bookingId}
Created at: {createdAt}
Doctor: {doctorName} ({doctorId})
Patient: {patientName} ({patientEmail})
Verhalten des Arztes: {rating1}
Wartezeit: {rating2}
Gesamtbewertung: {rating3}
Meinung zu Arzt: {ratingtext}
Buchungsablauf: {rating4}
Meinung zu Service: {rating4text}';

        $mailing = new Mailing();
        $mailing->type = MAILING_TYPE_TEXT;
        $mailing->content_id = 1;
        $mailing->subject = 'Alert : Bad review from a patient on ' . $platform;
        $mailing->addAddressTo('bad-review@arzttermine.de', 'arzttermine.de BadReviews');
        $mailing->from = $GLOBALS['CONFIG']["BOOKINGS_EMAIL_ADDRESS"];
        $mailing->from_name = $GLOBALS['CONFIG']["BOOKINGS_EMAIL_NAME"];
        $mailing->body_text = $bodyText;
        $mailing->data = [
            'rating1' => $review->getRating('rating_1'),
            'rating2' => $review->getRating('rating_2'),
            'rating3' => $review->getRating('rating_3'),
            'rating4' => $review->getRating('rating_4'),
            'rating4text' => $review->getServiceRateText(),
            'ratingtext' => $review->getRateText(),
            'platform' => $platform,
            'doctorId' => $user->getId(),
            'doctorName' => $user->getFullName(),
            'patientName' => sprintf('%s %s', $booking->getFirstName(), $booking->getLastName()),
            'patientEmail' => $booking->getEmail(),
            'bookingId' => $booking->getId(),
            'createdAt' => date('Y-m-d H:i:s')
        ];

        $mailing->sendPlain(false);
    }
}
