<?php

namespace Arzttermine\Controller;

use Arzttermine\Statistic\Statistic;
use Symfony\Component\HttpFoundation\Response;

class StatisticController extends Controller {

    /**
     * @param string $action
     * @param string $type
     * @return mixed
     */
    public function indexAction($action, $type)
    {
        if (!empty($action) && !empty($type)) {
            $statistic = new Statistic();

            $headers = array('Content-Type' => 'text/javascript');
            return new Response($statistic->getJSON($action), Response::HTTP_OK, $headers);
        }

        $this->notFound();
    }
}