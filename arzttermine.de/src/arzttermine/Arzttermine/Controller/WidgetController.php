<?php

namespace Arzttermine\Controller;

use Arzttermine\Application\Application;
use Arzttermine\Widget\WidgetFactory;

class WidgetController extends Controller {

    /**
     * @param string $widget_slug
     * @param string $parameter_string This will be a path e.g. /arzt/gkv
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction($widget_slug = '', $parameter_string = '')
    {
        $app = Application::getInstance();
        
        // Tell the app we're in widget mode
        try {
            $widgetContainer = WidgetFactory::getWidgetContainer($widget_slug);
            $app->setWidgetContainer($widgetContainer)->setIsWidget(true);
            $app->setSourcePlatform($widgetContainer->getSourcePlatform());
            $app->getSession()->set('widget_source_platform', $widgetContainer->getSourcePlatform());

            $parameters = $this->parseParameters($parameter_string);

            return $widgetContainer
                ->setAction($parameters['action'])
                ->setArgs($parameters['args'])
                ->run();
        } catch (\Exception $e) {
            $this->notFound();
        }
    }

    /**
     * Take the path parameters string and turn it into some kind of useful array
     *
     * @param string $parameter_string
     *
     * @return array
     */
    protected function parseParameters($parameter_string = '')
    {
        $parameters = array(
            'action' => 'index',
            'args'   => array()
        );

        $params = explode('/', trim($parameter_string, '/'));
        array_filter($params); // @todo Check that this doesn't break anything

        if (!empty($params[0])) {
            $parameters['action'] = strtolower($params[0]);
            unset($params[0]);
        }

        foreach ((array)$params as $param) {
            $parameters['args'][] = $param;
        }

        return $parameters;
    }
}