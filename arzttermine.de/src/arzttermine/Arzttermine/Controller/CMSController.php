<?php

namespace Arzttermine\Controller;

use Arzttermine\Cms\CmsContent;

class CMSController extends Controller {

    /**
     * Default catch-all for the CMS
     */
    public function indexAction()
    {
        $view = $this->getView();
        $content = new CmsContent();

        $url = $this->getRequest()->getPathInfo();
        $url = rtrim($url, '/');
        
        if (defined('SYSTEM_THEME')) {
            // extend the url with the theme
            $url = '/' . SYSTEM_THEME . $url;
        }

        $content->loadByUrl($url);
        
        // Tag the homepage
        if ($content->isValid()) {
            if ($content->getUrl() == '') {
                $view->setMetaTag('og:type', 'website');
            }

            $view->setRef('content', $content);
            $view->setRef('active_page', $content->getId());
            $view->addHeadTitle($content->getTitle());
            $view->setMetaTag('keywords', $content->getTags());
            $view->setMetaTag('description', $content->getDescription());

            // render the page
            if ($content->getMimetype()) {
                header('Content-Type: ' . $content->getMimetype());
            }

            return $view->display($content->getTemplate());
        }

        return $this->notFound();
    }
}