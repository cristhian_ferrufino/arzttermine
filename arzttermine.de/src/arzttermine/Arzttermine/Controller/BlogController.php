<?php

namespace Arzttermine\Controller;

use Arzttermine\Blog\Blog;
use Arzttermine\Blog\Post;

class BlogController extends Controller {

    public function viewAction($slug = '')
    {
        $request = $this->getRequest();
        $view = $this->getView();
        $profile = $this->getUser();

        // @todo: Refactor with routes instead of checks
        $action = 'show_blogpost';

        if ($page = $request->getQuery('p')) {
            $action = 'show_blog';
        }

        $blogpost = new Post($slug, 'slug');

        if (($slug != '') &&
            (!$blogpost->isValid() ||
             (!$blogpost->isPublished() && !$profile->checkPrivilege('blogcomment_admin_edit'))
            )
        ) {
            $this->notFound();
        } else {
            if (!$blogpost->isValid()) {
                $action = 'show_blog';
            }

            // *****************************************************
            if ($action == 'show_blog') {
                $blog = new Blog();

                // get the acount of all blogposts
                $_blogs_num = $blog->countPosts($profile->checkPrivilege('blog_admin_edit'));

                // sanitize page
                if ($page < 1 || (($page - 1) > ($_blogs_num / $GLOBALS['CONFIG']['BLOG_BLOGPOSTS_PER_PAGE']))) {
                    $page = 1;
                }
                // show older posts link?
                if (($page * $GLOBALS['CONFIG']['BLOG_BLOGPOSTS_PER_PAGE']) < $_blogs_num) {
                    $view->setRef('show_older_posts_page_num', ($page + 1));
                }
                // show newer posts link?
                if ($page > 1) {
                    $view->setRef('show_newer_posts_page_num', ($page - 1));
                }
                $offset = (($page - 1) * $GLOBALS['CONFIG']['BLOG_BLOGPOSTS_PER_PAGE']);

                $blog->loadPosts($offset, $GLOBALS['CONFIG']['BLOG_BLOGPOSTS_PER_PAGE'], $profile->checkPrivilege('blog_admin_edit'));
                $view->setRef('blog', $blog);

                $view->addHeadTitle('News');
                $view->setRef('page_id', 'newsblog');

                $view->setRef('active_page', 'news');
                return $view->display('blog/blog.tpl');
            } elseif ($action == 'show_blogpost') {

                // load the previous and next links
                $blogpost->loadPrevNextBlogPostLinks();
                $view->setRef('blogpost', $blogpost);
                $view->setRef('page_id', 'newsblogpost');
                $view->setRef('url_form_action', $request->getRequestUri());

                $blog = new Blog();
                $view->setRef('blog', $blog);

                $view->addHeadTitle($blogpost->getTitle());
                $view->setMetaTag('keywords', $blogpost->getTags());

                $view->setRef('active_page', 'news');
                return $view->display('blog/blogpost.tpl');
            }
        }
    }
}