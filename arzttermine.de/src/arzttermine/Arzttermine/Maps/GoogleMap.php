<?php

namespace Arzttermine\Maps;

use Arzttermine\Application\Application;
use Http\Discovery\StreamFactoryDiscovery;

class GoogleMap {

    /**
     * @var array
     */
    protected $markers;

    /**
     * @var float
     */
    protected $centerLat;

    /**
     * @var float
     */
    protected $centerLng;

    /**
     * Searches for an address by lat & lng
     *
     * @param lat
     * @param lng
     *
     * @return array
     */
    public static function findLocationsByLatLng($lat, $lng)
    {
        if (!is_numeric($lat) || !is_numeric($lng)) {
            return false;
        }


        global $CONFIG;
        $memcached = (extension_loaded('Memcached'))?new \Memcached():new \Memcache();
        $memcached->addServer($CONFIG['MEMCACHED_SERVER'], 11211);
        $pool = (extension_loaded('Memcached'))?new \Cache\Adapter\Memcached\MemcachedCachePool($memcached):new \Cache\Adapter\Memcache\MemcacheCachePool($memcached);
        $cachePlugin = new \Http\Client\Common\Plugin\CachePlugin($pool, StreamFactoryDiscovery::find(), [
            'respect_cache_headers' => false,
            'default_ttl' => null,
            'cache_lifetime' => 86400*365
        ]);
        $adapter = new \Http\Adapter\Guzzle6\Client();
        $pluginClient = new \Http\Client\Common\PluginClient($adapter, [$cachePlugin]);
        $provider = new \Geocoder\Provider\GoogleMaps\GoogleMaps($pluginClient, 'de', 'AIzaSyD491jqt_2KHhjz2CHMoWXfkX3TKbhSVdw');
        $geocoder = new \Geocoder\StatefulGeocoder($provider, 'de');
        try {
            $geocode = $geocoder->geocode($address);
            return array(array('formatted_address_short_postal_code_locality'=>$geocode->first()->getPostalCode().''.$geocode->first()->getLocality()));
        } catch (\Exception $e) {
            error_log($e->getMessage());
        }
        return array();

        /*
        $url = sprintf('http://maps.googleapis.com/maps/api/geocode/json?latlng=%d,%d&language=de&sensor=false', rawurlencode($lat), rawurlencode($lng));
        $json_string = self::fetchURL($url);

        return self::parseLocations($json_string);
        */
    }

    /**
     * @param $url
     *
     * @return string
     */
    private static function fetchURL($url)
    {
        return @file_get_contents($url);
    }

    /**
     * Parse results
     *
     * @param string $json_string
     *
     * @return array
     */
    static function parseLocations($json_string)
    {
        if ($json_string) {
            // parse the possible destinations into an array
            $_locations = json_decode($json_string, true);
        }
        if (!isset($_locations['status']) || $_locations['status'] != 'OK') {
            return array();
        }
        $_valid_locations = array();
        // check if we have valid locations in germany
        if (isset($_locations['results']) && count($_locations['results']) > 0) {
            $i = 0;
            foreach ($_locations['results'] as $_location) {
                $_valid_locations[$i]['formatted_address'] = $_location['formatted_address'];
                $_valid_locations[$i]['formatted_address_short'] = str_replace(', Deutschland', '', $_location['formatted_address']);

                if (!empty($_location['formatted_address_short'])) {
                    $_valid_locations[$i]['formatted_address_short'] = str_replace(', Österreich', '', $_location['formatted_address_short']);
                }

                $_valid_locations[$i]['lng'] = $_location['geometry']['location']['lng'];
                $_valid_locations[$i]['lat'] = $_location['geometry']['location']['lat'];
                // get the more specific data
                if (isset($_location['address_components']) && count($_location['address_components']) > 0) {
                    foreach ($_location['address_components'] as $_address) {
                        if (!isset($_address['types'][0])) {
                            continue;
                        }
                        $_type = $_address['types'][0];
                        switch ($_type) {
                            case 'country':
                                $_valid_locations[$i]['country'] = $_address['long_name'];
                                break;
                            case 'administrative_area_level_1':
                                $_valid_locations[$i]['administrative_area_level_1'] = $_address['long_name'];
                                break;
                            case 'administrative_area_level_2':
                                $_valid_locations[$i]['administrative_area_level_2'] = $_address['long_name'];
                                break;
                            case 'locality':
                                $_valid_locations[$i]['locality'] = $_address['long_name'];
                                break;
                            case 'route':
                                $_valid_locations[$i]['route'] = $_address['long_name'];
                                break;
                            case 'street_number':
                                $_valid_locations[$i]['street_number'] = $_address['long_name'];
                                break;
                            case 'postal_code':
                                $_valid_locations[$i]['postal_code'] = $_address['long_name'];
                                break;
                        }
                    }
                    if (isset($_valid_locations[$i]['locality']) || isset($_valid_locations[$i]['postal_code'])) {
                        $_valid_locations[$i]['formatted_address_short_postal_code_locality'] = isset($_valid_locations[$i]['postal_code']) ? $_valid_locations[$i]['postal_code'] . ' ' : '';
                        $_valid_locations[$i]['formatted_address_short_postal_code_locality'] .= isset($_valid_locations[$i]['locality']) ? $_valid_locations[$i]['locality'] : '';
                    }
                }
                $i++;
            }
        }

        return $_valid_locations;
    }

    /**
     * Store one maker in the array
     *
     * @param string $slug
     * @param float $lat
     * @param float $lng
     * @param string $url
     * @param string $imgsrc
     * @param string $name
     * @param string $street
     * @param string $cityzip
     * @param bool $available Does this pin show up? Used to filter results
     *
     * @return self
     */
    public function addMarker($slug, $lat, $lng, $url, $imgsrc, $name, $street, $cityzip, $available = true)
    {
        $this->markers[$slug] = array(
            'lat'     => $lat,
            'lng'     => $lng,
            'url'     => $url,
            'imgsrc'  => $imgsrc,
            'name'    => $name,
            'street'  => $street,
            'cityzip' => $cityzip,
            'available' => intval($available)
        );

        return $this;
    }

    /* ************************************************************ */
    /* LOOKUP SERVICE ********************************************* */
    /* ************************************************************ */

    /**
     * @return int
     */
    public function countMarkers()
    {
        return count($this->markers);
    }

    /**
     * return the marker array in a decent format
     *
     * @param format (json,array)
     *
     * @return mixed
     */
    public function getMarkers($format)
    {
        switch ($format) {
            case 'json':
                return json_encode($this->markers);
                break;
            case 'array':
            default:
                return $this->markers;
                break;
        }
    }

    /**
     * Set the center point of the map
     *
     * @param float $lat
     * @param float $lng
     *
     * @return self
     */
    public function setCenterLatLng($lat, $lng)
    {
        $this->centerLat = $lat;
        $this->centerLng = $lng;

        return $this;
    }

    /**
     * Converts address to geo coordinates / lat, lng
     *
     * @param address
     *
     * @return array
     */
    public static function getGeoCoords($address)
    {
        $_valid_locations = self::findLocationsByAddress($address);
        if ($_valid_locations == 0) {
            return array();
        }
        if (!isset($_valid_locations[0]['lat']) || !isset($_valid_locations[0]['lng'])) {
            return array();
        }

        return array(
            'lat' => $_valid_locations[0]['lat'],
            'lng' => $_valid_locations[0]['lng']
        );
    }

    /**
     * Searches for locations by an address
     *
     * @param string $address
     *
     * @return array
     */
    public static function findLocationsByAddress($address)
    {
        $address = mb_strtolower($address, $GLOBALS['CONFIG']['SYSTEM_CHARSET']);
        $address = sanitize_location($address);
        $address = preg_replace('@\s?,\s?@', ',', $address);
        
        // TODO: Should be a much better Country-Sensor...
        // check if there is just a zip code
        if (preg_match('/^[0-9]{5}$/', $address)) {
            $address .= ',deutschland';
        } elseif (preg_match('/^[0-9]{4}$/', $address)) {
            $address .= ',österreich';
        }

        global $CONFIG;
        $memcached = (extension_loaded('Memcached'))?new \Memcached():new \Memcache();
        $memcached->addServer($CONFIG['MEMCACHED_SERVER'], 11211);
        $pool = (extension_loaded('Memcached'))?new \Cache\Adapter\Memcached\MemcachedCachePool($memcached):new \Cache\Adapter\Memcache\MemcacheCachePool($memcached);
        $cachePlugin = new \Http\Client\Common\Plugin\CachePlugin($pool, StreamFactoryDiscovery::find(), [
            'respect_cache_headers' => false,
            'default_ttl' => null,
            'cache_lifetime' => 86400*30
        ]);
        $adapter = new \Http\Adapter\Guzzle6\Client();
        $pluginClient = new \Http\Client\Common\PluginClient($adapter, [$cachePlugin]);
        $provider = new \Geocoder\Provider\GoogleMaps\GoogleMaps($pluginClient, 'de', 'AIzaSyD491jqt_2KHhjz2CHMoWXfkX3TKbhSVdw');
        $geocoder = new \Geocoder\StatefulGeocoder($provider, 'de');

        try {
            $geocode = $geocoder->geocode($address);
            return array(array('lat'=>$geocode->first()->getCoordinates()->getLatitude(),'lng'=>$geocode->first()->getCoordinates()->getLongitude()));
        } catch (\Exception $e) {
            error_log($e->getMessage());
        }
        return array();

/*
        $json_string = self::findCachedLocations($address);
        
        if (!$json_string) {
            $url = sprintf('http://maps.googleapis.com/maps/api/geocode/json?address=%s&language=de&sensor=false', rawurlencode($address));
            $json_string = self::fetchURL($url);
            $json_object = json_decode($json_string);
            // store only valid results
            // bad status could be "OVER_QUERY_LIMIT". Dont save that one!
            if (
                is_object($json_object)
                &&
                $json_object->status == 'OK'
            ) {
                self::saveCachedLocations($address, $json_string);
            }
        }

        return self::parseLocations($json_string);
*/
    }

    /**
     * Try to find a cached address.
     *
     * @param address
     *
     * @return string || false
     */
    private static function findCachedLocations($address)
    {
        $db = Application::getInstance()->getSql();

        // All addresses are stored lower case in the db
        $address = mb_strtolower($address, $GLOBALS['CONFIG']['SYSTEM_CHARSET']);

        $_sql = "SELECT * FROM " . DB_TABLENAME_ADDRESS_GEO_DATAS . " WHERE address='" . sqlsave($address) . "' LIMIT 1;";
        $db->query($_sql);

        if ($db->nf() > 0) {
            $db->next_record();

            return $db->f('result_json');
        }

        return false;
    }

    /**
     * Save an address
     * Stores the query string lowercase in the DB
     *
     * @param string $address
     * @param string $result
     *
     * @return bool
     */
    private static function saveCachedLocations($address, $result)
    {
        $db = Application::getInstance()->getSql();

        $address = mb_strtolower($address, $GLOBALS['CONFIG']['SYSTEM_CHARSET']);

        if ($address == '' || $result == '') {
            return false;
        }

        // double check: dont overwrite data
        $_sql = "SELECT address FROM " . DB_TABLENAME_ADDRESS_GEO_DATAS . " WHERE address='" . sqlsave($address) . "'";
        $db->query($_sql);
        if ($db->nf() < 1) {
            $_sql = "INSERT INTO " . DB_TABLENAME_ADDRESS_GEO_DATAS . " SET address='" . sqlsave($address) . "',result_json='" . sqlsave($result) . "', updated_at=NOW()";

            return $db->query($_sql);
        }

        return true;
    }

    /**
     * @param $lat1
     * @param $lon1
     * @param $lat2
     * @param $lon2
     * @param string $unit
     *
     * @return float
     */
    function geoGetDistance($lat1, $lon1, $lat2, $lon2, $unit = 'K')
    {
        // calculate miles
        $M = 69.09 * rad2deg(acos(sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($lon1 - $lon2))));
        switch (strtoupper($unit)) {
            case 'K':
                // kilometers
                return $M * 1.609344;
                break;
            case 'N':
                // nautical miles
                return $M * 0.868976242;
                break;
            case 'F':
                // feet
                return $M * 5280;
                break;
            case 'I':
                // inches
                return $M * 63360;
                break;
            case 'M':
            default:
                // miles
                return $M;
                break;
        }
    }
}
