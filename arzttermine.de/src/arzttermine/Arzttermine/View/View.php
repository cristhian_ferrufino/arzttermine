<?php

namespace Arzttermine\View;

use Arzttermine\Application\Application;
use Arzttermine\Blog\Blog;
use SmartyBC;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;

class View extends Html {

    /**
     * The Backwards Compatible (BC) wrapper still contains the
     * {php} tag that we're unfortunately still using.
     *
     * @var SmartyBC
     */
    protected $smarty;

    /**
     * CSS includes (links)
     *
     * @var array
     */
    protected $cssInclude = array();

    /**
     * JS includes (scripts)
     *
     * @var array
     */
    protected $jsInclude = array();

    /**
     * Raw JS (JS block)
     *
     * @var array
     */
    protected $js = array();

    /**
     * @var array
     */
    protected $headTitle = array();

    /**
     * No longer used
     * 
     * @var string
     */
    protected $headHtml = '';

    /**
     * The id of an element that should get the focus
     *
     * @var string
     */
    protected $focus;

    /**
     * Browser caching enabled/disabled. Useful for administration or time-sensitive pages
     * 
     * @var bool
     */
    protected $caching = true;

    /**
     * @param array $configuration
     */
    public function __construct(array $configuration = array())
    {
        $defaults = array(
            "TEMPLATE_DIR"  => '',
            "COMPILE_DIR"   => '',
            "PLUGIN_DIR"    => '',
            "DEBUG"         => false,
            "FORCE_COMPILE" => false,
            "COMPILE_CHECK" => false
        );

        $configuration = array_merge($defaults, $configuration);

        $this->smarty = new SmartyBC();
        $this->smarty->compile_check = $configuration['COMPILE_CHECK'];
        $this->smarty->force_compile = $configuration['FORCE_COMPILE'];
        $this->smarty->debugging = $configuration['DEBUG'];
        $this->smarty->error_reporting = E_ALL & ~E_NOTICE;
        // allow user deploy to remove the templates via capistrano
        $this->smarty->_file_perms = 0664;
        $this->smarty
            ->setTemplateDir($configuration['TEMPLATE_DIR'])
            ->setCompileDir($configuration['COMPILE_DIR'])
            ->setPluginsDir($configuration['PLUGIN_DIR'])
            ->addPluginsDir(SYSTEM_PATH . '/include/lib/smarty/plugins/')
            ->assign("CONFIG", $GLOBALS['CONFIG'])  // this is a copy of ($CONFIG) so better use $app->getConfig() in the templates to get realtime data
            ->assign("app", Application::getInstance())
            ->assign("this", $this);

        if (!empty($GLOBALS['helper'])) {
            $this->smarty->assign("helper", $GLOBALS['helper']);
        }
    }

    /**
     * Prepend a directory to our current directory array
     *
     * @param string $directory
     *
     * @return self
     */
    public function prependTemplateDirectory($directory)
    {
        $current_templates = $this->smarty->getTemplateDir();

        if (!is_array($current_templates)) {
            $current_templates = array($current_templates);
        }

        array_unshift($current_templates, $directory);

        $this->smarty->setTemplateDir($current_templates);

        return $this;
    }

    /**
     * Add a template directory to the Smarty instance
     *
     * @param $directory
     *
     * @return self
     */
    public function addTemplateDirectory($directory)
    {
        $this->smarty->addTemplateDir($directory);

        return $this;
    }

    /**
     * Set a bunch of references at once
     *
     * @param array $variables
     *
     * @return self
     */
    public function setRefs(array $variables = array())
    {
        foreach ($variables as $key => $value) {
            $this->setRef($key, $value);
        }

        return $this;
    }

    /**
     * Set an attribute for template processing
     * Can be used with $key in templates
     *
     * @param string $key
     * @param mixed $value
     *
     * @return self
     */
    public function setRef($key, $value)
    {
        $this->smarty->assign($key, $value);
        
        // Backwards compatibility
        // Note: To be removed with the removal of View::set()
        $this->$key = $value;
        
        return $this;
    }

    /**
     * Set an attribute for template processing
     * Can be used with $key in templates
     *
     * @param string $key
     *
     * @return object  the object of the variable
     */
    public function getRef($key)
    {
        return $this->smarty->getVariable($key);
    }

    /**
     * Set an attribute for template processing
     * Can be used with $this->key in templates
     *
     * @deprecated Use setRef instead
     * 
     * @param string $key
     * @param mixed $value
     *
     * @return self
     */
    public function set($key, $value)
    {
        $this->setRef($key, $value);

        return $this;
    }

    /**
     * append a value to a template variable
     *
     * @param string $key
     * @param mixed $value
     *
     * @return self
     */
    public function append($key, $value)
    {
        $this->setRef($key, $this->getRef($key).$value);

        return $this;
    }

    /**
     * prepends a value to a template variable
     *
     * @param string $key
     * @param mixed $value
     *
     * @return self
     */
    public function prepend($key, $value)
    {
        $this->setRef($key, $value.$this->getRef($key));

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function templateExists($resource_name)
    {
        return $this->smarty->templateExists($resource_name);
    }

    /**
     * @param $template
     *
     * @return string
     */
    public function fetch($template)
    {
        // if a theme specific template exists, fetch that one, otherwise fetch the default one
        if (defined('SYSTEM_THEME')) {
            $_template = SYSTEM_THEME . '/' . $template;
            foreach ($this->smarty->getTemplateDir() as $directory) {
                if (file_exists($directory . '/' . $_template)) {
                    $template = $_template;
                }
            }
        }
        
        return $this->smarty->fetch($template);
    }

    /**
     * @param string $template
     * @param int $status
     * 
     * @return Response
     */
    public function display($template, $status = Response::HTTP_OK, $cookie = array())
    {
        $headers = array();

        if (!$this->isCachingEnabled()) {
            $headers = array(
                'Cache-Control' => 'no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0',
                'Pragma' => 'no-cache',
                'Expires' => date('Y-m-d H:i:s')
            );
        }

        $this->smarty->assign('this', $this);

        $response = new Response($this->fetch($template), $status, $headers);
        if ($this->isCachingEnabled()) {
            $response->setSharedMaxAge(3600);
            $date = new \DateTime();
            $date->modify('+3600 seconds');
            $response->setExpires($date);
            $response->setETag(md5($response->getContent()));
            $response->setPublic(); // make sure the response is public/cacheable
        }

        if(is_array($cookie) && !empty($cookie)) {
            $cookieObj = new Cookie($cookie['name'], $cookie['value'], $cookie['time'], $cookie['path']);
            $response->headers->setCookie($cookieObj);
        }

        return $response;
    }

    /**
     * returns the city of current user
     *
     * @return string
     **/
    function getUserCity()
    {
        $request = Application::getInstance()->getRequest();

        //get city name via optional cookie set on homepage
        $city = $request->cookies->get('at_user_city');

        if ($city === null) {
            //get city name via freegeoip
            $clientIp = $request->getClientIp();
            $adapter = new \Http\Adapter\Guzzle6\Client();
            $provider = new \Geocoder\Provider\FreeGeoIp\FreeGeoIp($adapter);
            $geocoder = new \Geocoder\StatefulGeocoder($provider, 'de');
            try {
                $geocode = $geocoder->geocode($clientIp);
                $city = $geocode->first()->getLocality();
            } catch (\Exception $e) {
            }
        }

        if($city === null) {
            $city = 'Berlin';
        }

        return $city;
    }

    /**
     * @return self
     */
    public function enableCaching()
    {
        $this->caching = true;

        return $this;
    }

    /**
     * @return self
     */
    public function disableCaching()
    {
        $this->caching = false;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isCachingEnabled()
    {
        return (bool)$this->caching;
    }

    /**
     * Translate a text
     * (Wrapper for templates)
     *
     * @param string $text
     *
     * @return string
     */
    public function _($text)
    {
        return Application::getInstance()->_($text);
    }

    /**
     * Add a message
     *
     * @param string $service
     * @param string $type (ERROR | WARNING | NOTICE)
     * @param string $message
     *
     * @return self
     */
    public function addMessage($service, $type, $message)
    {
        Application::getInstance()->getSession()->getFlashBag()->add($type, $message);

        return $this;
    }

    // *****************************************************
    /**
     * Messaging System
     *
     * All system or local messages (in forms, etc) are stored in an
     * array where the first index addresses the service [ DEBUG | STATUS | FORM-specific | ...] and
     * the second the messagetype [ERROR | WARNING | NOTICE | ...]
     *
     * services:
     * DEBUG
     * STATUS is the main message queue which is visible at the top of the website
     * FORM-specific: Each form can have its own message, e.g. if the value is not correct
     */

    /**
     * Returns $string if the message is set
     * Mainly used to check for form errors
     *
     * @param string $service
     * @param string $text
     *
     * @return string
     */
    public function hasMessages($service, $text)
    {
        return $this->countMessages() ? $text : '';
    }

    /**
     * Returns the number of messages of a given type.
     *
     * @param string $type
     *
     * @return int
     */
    public function countMessages($type = 'ERROR')
    {
        return $type ? count(Application::getInstance()->getSession()->getFlashBag()->peek($type)) : 0;
    }

    /**
     * Returns some html for the messages.
     * Gets all messages from the messages array
     *
     * @deprecated
     *
     * @return string
     */
    public function getHtmlFormMessages()
    {
        return '';
    }

    /**
     * Returns an array with StdObjects for the status message template
     * Gets all messages from the messages array
     * class usually can be 'error', 'warning', 'notice'
     *
     * @return array StdClass() with indexes 'class', 'html'
     */
    public function getStatusMessages()
    {
        return Application::getInstance()->getSession()->getFlashBag()->all();
    }

    /**
     * Checks if there a StatusMessages available
     *
     * @return bool
     */
    public function hasStatusMessages()
    {
        return (bool)$this->countMessages();
    }

    /**
     * Wrapper method to allow getMediaUrl()-use in templates
     *
     * @param string $name
     * @param bool $absolute_url
     * @param bool $add_timestamp
     *
     * @return string
     */
    public function getMediaUrl($name, $absolute_url = false, $add_timestamp = false)
    {
        return getMediaUrl($name, $absolute_url, $add_timestamp);
    }

    /**
     * Wrapper method to allow getStaticUrl()-use in templates
     *
     * @param string $name
     * @param bool $absolute_url
     * @param bool $add_timestamp
     *
     * @return string
     */
    public function getStaticUrl($name, $absolute_url = false, $add_timestamp = false)
    {
        return getStaticUrl($name, $absolute_url, $add_timestamp);
    }

    /**
     * Sets the focus on a selector
     *
     * @param string $name
     *
     * @return self
     */
    public function setFocus($name)
    {
        $this->focus = $name;

        return $this;
    }

    /**
     * Enqueue a CSS file
     *
     * @deprecated
     *
     * @param $url
     *
     * @return self;
     */
    public function addCssInclude($url)
    {
        $this->cssInclude[] = trim($url);

        return $this;
    }

    /**
     * Output the CSS link elements
     *
     * @deprecated
     *
     * @todo: Remove HTML from class
     */
    public function getCssInclude()
    {
        if (is_array($this->cssInclude) && !empty($this->cssInclude)) {
            $_html = '';

            $this->cssInclude = array_unique($this->cssInclude);

            foreach ($this->cssInclude as $_url) {
                if (!empty($_url)) {
                    $_html .= '<link rel="stylesheet" href="' . $_url . '" type="text/css" media="all" />' . "\n";
                }
            }

            echo $_html;
        }
    }

    /**
     * Add a Javascript include
     *
     * @deprecated
     *
     * @param string
     *
     * @return self
     */
    public function addJsInclude($url)
    {
        $this->jsInclude[] = trim($url);

        return $this;
    }

    /**
     * Output script tags
     *
     * @deprecated
     *
     * @todo: Remove HTML from class
     */
    public function getJsInclude()
    {
        if (is_array($this->jsInclude) && !empty($this->jsInclude)) {
            $_html = '';

            $this->jsInclude = array_unique($this->jsInclude);

            foreach ($this->jsInclude as $_url) {
                if (!empty($_url)) {
                    $_html .= '<script type="text/javascript" src="' . $_url . '"></script>' . "\n";
                }
            }

            echo $_html;
        }
    }

    /**
     * Add arbitrary content to the HTML head.
     *
     * @todo: Remove entirely
     *
     * @deprecated
     *
     * @param string $text
     *
     * @return self
     */
    public function addHeadHtml($text)
    {
        $this->headHtml .= $text;

        return $this;
    }

    /**
     * Get the head HTML
     *
     * @deprecated
     *
     * @return mixed
     */
    public function getHeadHtml()
    {
        return $this->headHtml;
    }

    /**
     * Clear the head title and set it
     *
     * @param string $text
     *
     * @return self
     */
    public function setHeadTitle($text)
    {
        $this->headTitle = array();
        $this->addHeadTitle($text);

        return $this;
    }

    /**
     * Append a string to the title
     *
     * @param string $text
     *
     * @return self
     */
    public function addHeadTitle($text)
    {
        $this->headTitle[] = $text;

        return $this;
    }

    /**
     * @return string
     */
    public function getHeadTitle()
    {
        return implode(' | ', array_reverse(array_filter($this->headTitle)));
    }

    /**
     * Hacky stub to check if the head title has been modified or not. Probably don't use this
     * 
     * @deprecated
     * 
     * @return int
     */
    public function getHeadTitleCount()
    {
        return count($this->headTitle);
    }

    /**
     * @param string $key
     * @param string $text
     *
     * @return self
     */
    public function setMetaTag($key, $text)
    {
        $this->metaTag[$key] = $text;

        return $this;
    }

    /**
     * @param string $key
     * @param string $text
     *
     * @return self
     */
    public function addMetaTag($key, $text)
    {
        $this->metaTag[$key] .= $text;

        return $this;
    }

    /**
     * @param string $key
     *
     * @return string
     */
    public function getMetaTag($key)
    {
        return isset($this->metaTag[$key]) ? $this->metaTag[$key] : null;
    }

    /**
     * @param array $content_ids
     * @param string $dir_id
     * @param string $url
     * @param string $class
     *
     * @return string
     */
    public function getClassActive($content_ids, $dir_id = '', $url = '', $class = '')
    {
        global $content;

        $class_active = ' class="active"';
        if (!empty($class)) {
            $class_active = ' class="active ' . $class . '"';
            $class = ' class="' . $class . '"';
        }

        if (!empty($content_ids)) {
            if (!is_array($content_ids)) {
                $content_ids = array($content_ids);
            }

            foreach ($content_ids as $_content_id) {
                if ($_content_id == $content->getId()) {
                    if (!empty($url)) {
                        if (strpos($_SERVER['PHP_SELF'], $url) === false) {
                            return $class;
                        } else {
                            return $class_active;
                        }
                    } else {
                        return $class_active;
                    }
                }
            }
        }

        if (!empty($dir_id)) {
            // check only the top dir_id
            if ($dir_id == $content->dir_id) {
                return $class_active;
            } else {
                return $class;
            }
        }

        return $class;
    }

    /**
     * Return the language links
     *
     * @todo: Remove HTML from class
     *
     * @return html
     */
    public function getI18NSelectHtml()
    {
        global $content, $search_result_text;
        $app = Application::getInstance();

        $_html = '';
        $_current_lang = $app->getLanguageCode();

        foreach ($GLOBALS['CONFIG']['CMS_LOCALE'] as $lang => $value) {

            if ($_current_lang == $lang) {
                $_html .= '<span class="b l-flag ' . strtolower($value["name"]) . '"></span>';
                continue;
            }

            // get the correct url for each source
            if (isset($content) && $content->isValid()) {
                // CMS
                $_url = $content->getUrl($lang);
            } elseif (isset($search_result_text) && $search_result_text->isValid()) {
                // SearchResultText
                $_url = $app->i18nTranslateUrl($search_result_text->getUrl(), $lang);
            } else {
                $_url = $app->i18nTranslateUrl(Application::getInstance()->getUrl(true), $lang);
            }

            // default is the homepage
            if ($_url == '') {
                $_url = '/';
            }

            $_html .= '<a class="l-flag ' . strtolower($value["name"]) . '" href="' . $_url . '"></a>';
        }

        return $_html;
    }

    /**
     * Show the last x news in a box
     *
     * @param int $total recent count
     *
     * @return string
     */
    public function getLatestBlogPosts($total = 5)
    {
        $blogposts_last = new Blog();
        $blogposts_last->loadPosts(0, $total);
        $this->setRef('blogposts_last', $blogposts_last->getPosts());

        return $this->fetch('blog/_blogposts_last.tpl');
    }
}

