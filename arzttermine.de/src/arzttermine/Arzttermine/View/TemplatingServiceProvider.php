<?php

namespace Arzttermine\View;

use Arzttermine\DependencyInjection\ServiceProviderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TemplatingServiceProvider implements ServiceProviderInterface {

    /**
     * {@inheritdoc}
     */
    public function register(ContainerInterface $container)
    {
        // @todo All view config here (no globals)
        $view = new View($GLOBALS['CONFIG']['SMARTY']);
        $view->setHeadTitle($GLOBALS['CONFIG']['SYSTEM_PRODUCT_TITLE']);
        $container->set('templating', $view);
    }

    /**
     * {@inheritdoc}
     */
    public function boot(ContainerInterface $container)
    {
        // Add current Auth profile to the View
        if ($container->has('current_user')) {
            $container->get('templating')->setRef('profile', $container->get('current_user'));
        }
    }
    
}