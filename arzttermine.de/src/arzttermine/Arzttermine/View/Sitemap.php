<?php

namespace Arzttermine\View;

use Arzttermine\Blog\Post;
use Arzttermine\Cms\CmsContent;
use Arzttermine\Core\DateTime;
use Arzttermine\Location\Location;
use Arzttermine\Search\Text;
use Arzttermine\User\User;

/**
 * Sitemap Class
 *
 * @author Axel Kuzmik <axel@kuzmik.de>
 */
class Sitemap {

    /**
     * @var Sitemap
     */
    protected static $instance;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var \SimpleXmlElement
     */
    protected $xml;

    /**
     * @var array
     */
    protected $high_prio_medical_specialties
        = array(
            1,
            2,
            3,
            4,
            5,
            7,
            10
        );

    // ********************************************************

    /**
     *
     */
    private function __construct()
    {
        $this->xml = new \SimpleXmlElement('<urlset/>');
    }

    /**
     * Factory method to get an instance of sitemap
     *
     * @return Sitemap
     */
    public static function factory()
    {
        if (!self::$instance instanceof Sitemap) {
            self::$instance = new Sitemap();
        }

        return self::$instance;
    }

    /**
     * @param string $type
     *
     * @return Sitemap
     */
    public function setSitemapType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getSitemapType()
    {
        return $this->type;
    }

    /**
     * Render xml string
     *
     * @param bool $prettyPrint
     *
     * @return string
     */
    public function getXml($prettyPrint = true)
    {
        if ($prettyPrint) {
            $dom = dom_import_simplexml($this->xml)->ownerDocument;
            $dom->formatOutput = true;
            $_xml = $dom->saveXML();
        } else {
            $_xml = $this->xml->saveXml();
        }

        // insert the pretty attrs in the urlset-Element
        $_xml = str_replace('<urlset>', '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">', $_xml);

        return $_xml;
    }

    /**
     * Set sitemap index
     */
    public function setIndex()
    {
        $this->xml = new \SimpleXmlElement('<sitemapindex/>');
        $this->xml->addAttribute(
                  'xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9'
        );
    }

    /**
     * Multi array search
     *
     * @param array $array1
     * @param array $array2
     *
     * @return bool
     */
    protected static function in_array_multi($array1, $array2)
    {
        foreach ($array1 as $value) {
            if (in_array($value, $array2)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Set sites for doctors sitemap
     *
     * @param User $user
     */
    public function setDoctors(User $user)
    {
        $where = sprintf('group_id=3 and status >= %d', USER_STATUS_VISIBLE);

        $defaultPriority = $this->getDefault('doctors', 'priority');
        $defaultFrequency = $this->getDefault('doctors', 'frequency');

        $userIds = $user->findKeys($where);
        if (!empty($userIds)) {
            foreach ($userIds as $userId) {
                $oneUser = new User($userId);
                if (!$oneUser->isValid()) {
                    continue;
                }

                $lastmod = new DateTime($oneUser->getUpdatedAt());

                $urlEl = $this->xml->addChild('url');
                $urlEl->addChild('loc', $oneUser->getPermalink());

                if (self::in_array_multi($oneUser->getMedicalSpecialtyIds(), $this->high_prio_medical_specialties)) {
                    $priority = 0.9;
                } else {
                    $priority = $defaultPriority;
                }
                $urlEl->addChild('priority', $priority);
                $urlEl->addChild('changefreq', $defaultFrequency);
                if ($oneUser->getUpdatedAt() !== '0000-00-00 00:00:00') {
                    $urlEl->addChild('lastmod', $lastmod->format('Y-m-d'));
                }
            }
        }
    }

    /**
     * Set sites for locations sitemap
     *
     * @param Location $location
     */
    public function setLocations(Location $location)
    {
        $where = sprintf('status >= %d', LOCATION_STATUS_VISIBLE);

        $defaultPriority = $this->getDefault('locations', 'priority');
        $defaultFrequency = $this->getDefault('locations', 'frequency');

        $locationIds = $location->findKeys($where);
        if (!empty($locationIds)) {
            foreach ($locationIds as $locationId) {
                $oneLocation = new Location($locationId);
                if (!$oneLocation->isValid()) {
                    continue;
                }

                $lastmod = new DateTime($oneLocation->getUpdatedAt());

                $urlEl = $this->xml->addChild('url');
                $urlEl->addChild('loc', $oneLocation->getPermalink());

                if (self::in_array_multi($oneLocation->getMedicalSpecialtyIds(), $this->high_prio_medical_specialties)) {
                    $priority = 0.9;
                } else {
                    $priority = $defaultPriority;
                }
                $urlEl->addChild('priority', $priority);
                $urlEl->addChild('changefreq', $defaultFrequency);
                if ($oneLocation->getUpdatedAt() !== '0000-00-00 00:00:00') {
                    $urlEl->addChild('lastmod', $lastmod->format('Y-m-d'));
                }
            }
        }
    }

    /**
     * Set sites for news sitemap
     *
     * @param Post $blogpost
     */
    public function setBlogs(Post $blogpost)
    {
        $where = sprintf('status = %d', BLOGPOST_STATUS_PUBLISHED);

        $defaultPriority = $this->getDefault('blog', 'priority');
        $defaultFrequency = $this->getDefault('blog', 'frequency');

        /** @var Post $blog */
        foreach ($blogpost->findObjects($where) as $blog) {
            $lastmod = new DateTime($blog->getUpdatedAt());

            $urlEl = $this->xml->addChild('url');
            $urlEl->addChild('loc', $blog->getPermalink());
            $urlEl->addChild('priority', $defaultPriority);
            $urlEl->addChild('changefreq', $defaultFrequency);
            if ($blog->getUpdatedAt() !== '0000-00-00 00:00:00') {
                $urlEl->addChild('lastmod', $lastmod->format('Y-m-d'));
            }
        }
    }

    /**
     * Set sites for landingpages sitemap
     *
     * @param Text $pages
     */
    public function setLandingpages(Text $pages)
    {
        $where = sprintf('status = %d', SEARCH_RESULT_TEXT_ACTIVE);

        $defaultPriority = $this->getDefault('pages', 'priority');
        $defaultFrequency = $this->getDefault('pages', 'frequency');

        /** @var Text $page */
        foreach ($pages->findObjects($where) as $page) {
            $lastmod = new DateTime($page->getUpdatedAt());

            $urlEl = $this->xml->addChild('url');
            $urlEl->addChild('loc', $page->getUrl(true));
            $urlEl->addChild('priority', $defaultPriority);
            $urlEl->addChild('changefreq', $defaultFrequency);
            if ($page->getUpdatedAt() !== '0000-00-00 00:00:00') {
                $urlEl->addChild('lastmod', $lastmod->format('Y-m-d'));
            }
        }
    }

    /**
     * Set sites for core sitemap
     */
    public function setCore()
    {
        return;
    }

    /**
     * Add a sitemapindex entry
     *
     * @param string $url
     *
     * @return void
     */
    public function setIndexItem($url)
    {
        $url = $GLOBALS['CONFIG']['URL_HTTP_LIVE'] . $url;
        $this->xml->addChild('sitemap')->addChild('loc', $url);
    }

    /**
     * Add a sitemap entry
     *
     * - CMS Check, try to find the url in cms
     *        - found, use created_at for $lastmod
     *        - found, but not valid version, dont add xml
     *        - not found
     * - check for media files
     *        - found, use filemtime
     *        - not found, dont add xml
     *
     * ADD xml
     *
     * @param string $url
     * @param float $priority
     * @param string $frequency
     * @param string $lastmod
     *
     * @return void
     */
    public function setSitemapItem($url, $priority, $frequency, $lastmod = null)
    {
        // search for url in cms
        $_content = new CmsContent();

        if ($_content->isValid()) {
            $_version = $_content->getActiveVersion();
            $date = new DateTime($_version->created_at);
            $lastmod = $date->format('Y-m-d');
        } elseif (preg_match('/^\/media\//', $url)) {
            // media file get filemtime
            if (is_file($_SERVER['DOCUMENT_ROOT'] . $url)) {
                $date = new DateTime($_version->created_at);
                $lastmod = $date->format('Y-m-d');
            }
        }

        // if not found in CMS, use lastmod parameter

        $urlEl = $this->xml->addChild('url');
        $urlEl->addChild('loc', $GLOBALS['CONFIG']['URL_HTTP_LIVE'] . $url);

        if (!$priority) {
            $priority = $this->getDefault($this->getSitemapType(), 'priority');
        }
        if (!$frequency) {
            $frequency = $this->getDefault($this->getSitemapType(), 'frequency');
        }

        $urlEl->addChild('priority', $priority);
        $urlEl->addChild('changefreq', $frequency);

        if ($lastmod) {
            $urlEl->addChild('lastmod', $lastmod);
        }
    }

    /**
     * Get default priority or frequency from config for one sitemap type
     *
     * @param string $sitemapName
     * @param string $type
     *
     * @return string
     */
    protected function getDefault($sitemapName, $type)
    {
        return $GLOBALS['CONFIG']['SITEMAP']['TYPES'][$sitemapName][$type];
    }
}