<?php

namespace Arzttermine\View;

class Html {

    /**
     * This is to prevent Smarty breaking in some instances. Feel free to overload
     *
     * @return string
     */
    public function __toString()
    {
        return '';
    }

    // *****************************************************
    // convert data from database for output in html forms
    // to prevent HTML-DOS
    function html($html)
    {
        return html($html);
    }

    function form($html)
    {
        return form($html);
    }

    function alt($html)
    {
        return alt($html);
    }

    function sanitize_attribute($string)
    {
        return sanitize_attribute($string);
    }

    function url($string)
    {
        return urlencode($string);
    }

    function message($text)
    {
        $html = str_replace("\n", '<br />', html($text));

        return $html;
    }

    function message_tags($text)
    {
        return ereg_replace("([^>])\n", "\\1<br />\n", $text);
    }

    function www($link)
    {
        return htmlentities($link, ENT_NOQUOTES, $GLOBALS['CONFIG']['SYSTEM_CHARSET']);
    }

    function meta($text)
    {
        return htmlentities($text, ENT_COMPAT, $GLOBALS['CONFIG']['SYSTEM_CHARSET']);
    }

    function title($text)
    {
        return htmlentities($text, ENT_COMPAT, $GLOBALS['CONFIG']['SYSTEM_CHARSET']);
    }

    function blog($text)
    {
        $html = str_replace("\n", '<br />', html($text));

        return $html;
    }

    function event_description($text)
    {
        $html = str_replace("\n", '<br />', html($text));

        return $html;
    }

    function escape_double($text)
    {
        $html = str_replace('"', '\"', $text);

        return $html;
    }

    function remove_cr($text)
    {
        $html = str_replace("\n", '', $text);

        return $html;
    }

    // OUT: return a string (for titles, descriptions, names, etc)
    function valid_string($text)
    {
        // delete all not wanted chars
        $text = ereg_replace("[^A-Za-z0-9:.,;/%()[]_ '*#?@\"-]", "", $text);
        // delete all multi-whitechars
        $text = preg_replace("/[ ]+/", " ", $text);
        $text = trim($text);
        // if it ends with a single whitechar than delete it and
        // unvalid the string
        if ($text == ' ') {
            $text = '';
        }

        return $text;
    }

    // to prevent SQL-DOS
    function sqlsave($sql_string)
    {
        return mysql_escape_string($sql_string);
    }

    // IN: kilometer
    // OUT: text
    function km2text($km)
    {
        if (abs($km) >= 1) {
            return round(abs($km), 2) . ' Kilometer';
        } else {
            return (round(abs($km), 2) * 1000) . ' Meter';
        }
    }

    /* ***************************************************************** */
    function htmlDateTime($datetime, $style = 'dd.mm.yyyy hh:mm')
    {
        $_html = '';
        list($year, $month, $day, $hour, $minute) = split("[ :-]", $datetime);
        switch ($style) {
            case 'dd.mm.yyyy hh:mm':
                $_html = $day . '.' . $month . '.' . $year . ' - ' . sprintf('%02d', $hour) . ':' . sprintf('%02d', $minute);
                break;
            case 'dd.mm.yyyy um hh:mm Uhr':
                $_html = $day . '.' . $month . '.' . $year . ' um ' . sprintf('%02d', $hour) . ':' . sprintf('%02d', $minute) . ' Uhr';
                break;
            case 'dd.mm.yy / hh:mm':
                $_html = $day . '.' . $month . '.' . substr($year, -2) . ' / ' . sprintf('%02d', $hour) . ':' . sprintf('%02d', $minute);
                break;
            case 'dd.mm.yyyy':
                $_html = $day . '.' . $month . '.' . $year;
                break;
        }

        return $_html;
    }

    /* ***************************************************************** */
    function htmlTimestamp($timestamp, $style = 'dd.mm.yyyy hh:mm')
    {
        return htmlDateTime(date('Y-m-d H:i:s', $timestamp), $style);
    }

    /* ***************************************************************** */
    function datetime2year($datetime)
    {
        list($year, $month, $day, $hour, $minute, $second) = split("[ :-]", $datetime);

        return ($year);
    }

    function datetime2month($datetime)
    {
        list($year, $month, $day, $hour, $minute, $second) = split("[ :-]", $datetime);

        return ($month);
    }

    function datetime2day($datetime)
    {
        list($year, $month, $day, $hour, $minute, $second) = split("[ :-]", $datetime);

        return ($day);
    }

    function datetime2hour($datetime)
    {
        list($year, $month, $day, $hour, $minute, $second) = split("[ :-]", $datetime);

        return ($hour);
    }

    function datetime2minute($datetime)
    {
        list($year, $month, $day, $hour, $minute, $second) = split("[ :-]", $datetime);

        return ($minute);
    }

    /* ***************************************************************** */
    function input_radio($name, $value, $checked = 0, $javascript = "")
    {
        $HTML = '<input type="radio" name="' . $name . '" value="' . $value . '"' . ($checked == $value ? ' checked="checked"' : '') . '' . $javascript . '>';

        return $HTML;
    }

    /* ***************************************************************** */
    function input_text($name, $value, $class = "")
    {
        $HTML = '<input type="text" name="' . $name . '" value="' . $value . '"' . ($class == '' ? '' : ' class="' . $class . '"') . '>';

        return $HTML;
    }

    /* ***************************************************************** */
    function input_select_date($name, $form, $first_empty = false, $fill_key_null = 0, $fill_value_null = 0)
    {
        $d = $form[$name . '_d'];
        $m = $form[$name . '_m'];
        $y = $form[$name . '_y'];
        $HTML = input_select('form[' . $name . '_d]', $d, 1, 31, $first_empty, '', $fill_key_null, $fill_value_null) . '&nbsp;.&nbsp;' . input_select('form[' . $name . '_m]', $m, 1, 12, $first_empty, '', $fill_key_null, $fill_value_null) . '&nbsp;.&nbsp;' . input_select('form[' . $name . '_y]', $y, date("Y"), (date("Y") + 12), $first_empty, '', $fill_key_null, $fill_value_null);

        return $HTML;
    }

    function input_select_birthday($name, $form, $first_empty = false)
    {
        $d = $form[$name . '_d'];
        $m = $form[$name . '_m'];
        $y = $form[$name . '_y'];
        $HTML = input_select('form[' . $name . '_d]', $d, 1, 31, $first_empty) . '&nbsp;.&nbsp;' . input_select('form[' . $name . '_m]', $m, 1, 12, $first_empty) . '&nbsp;.&nbsp;' . input_select('form[' . $name . '_y]', $y, date("Y"), 1900, $first_empty);

        return $HTML;
    }

    function input_select_time($name, $form, $first_empty = false, $fill_key_null = 0, $fill_value_null = 0)
    {
        $h = $form[$name . '_h'];
        $m = $form[$name . '_i'];
        $HTML = input_select('form[' . $name . '_h]', $h, 0, 23, $first_empty, '', $fill_key_null, $fill_value_null) . '&nbsp;:&nbsp;' . input_select('form[' . $name . '_i]', $m, 0, 59, $first_empty, '', $fill_key_null, $fill_value_null);

        return $HTML;
    }

    function input_select_age_years_months($name, $class, $form, $age_max, $first_empty = false)
    {
        $m = $form[$name . '_m'];
        $y = $form[$name . '_y'];
        $HTML = input_select('form[' . $name . '_y]', $y, 1, $age_max, $first_empty, $class) . '&nbsp;Jahre&nbsp;' . input_select('form[' . $name . '_m]', $m, 1, 12, $first_empty, $class) . '&nbsp;Monate';

        return $HTML;
    }

    /* ***************************************************************** */
    // fill_with_null: fill the start-end with x nulls
    function input_select($name, $selected, $start, $end, $first_empty = 0, $class = "", $fill_key_with_null = 0, $fill_value_with_null = 0)
    {
        if ($class == '') {
            $style = ' style="width:60px;"';
        } else {
            $style = ' class="' . $class . '"';
        }
        $HTML = '<select name="' . $name . '" size="1"' . $style . '>';
        if ($first_empty) {
            $HTML .= '<option value=""' . (($selected == '') ? ' selected="selected"' : '') . '></option>';
        }
        $_fill_key_filter = '%0' . $fill_key_with_null . 'd';
        $_fill_value_filter = '%0' . $fill_value_with_null . 'd';
        if ($start < $end) {
            for ($i = $start; $i <= $end; $i++) {
                $_ik = sprintf($_fill_key_filter, $i);
                $_iv = sprintf($_fill_value_filter, $i);
                $HTML .= '<option value="' . $_ik . '"' . (($selected == $_ik) ? ' selected="selected"' : '') . '>' . $_iv . '</option>';
            }
        } else {
            for ($i = $start; $i >= $end; $i--) {
                $_ik = sprintf($_fill_key_filter, $i);
                $_iv = sprintf($_fill_value_filter, $i);
                $HTML .= '<option value="' . $_ik . '"' . (($selected == $_ik) ? ' selected="selected"' : '') . '>' . $_iv . '</option>';
            }
        }
        $HTML .= '</select>';

        return $HTML;
    }

    /* ***************************************************************** */
    function input_select_array($array, $id, $name, $selected, $first_empty = false, $class = "", $javascript = "")
    {
        if ($class == '') {
            $style = ' style="width:60px;"';
        } else {
            $style = ' class="' . $class . '"';
        }
        $HTML = '<select id="' . $id . '" name="' . $name . '"' . $style . $javascript . '>';
        if ($first_empty == true) {
            $HTML .= '<option value=""' . (($selected == '') ? ' selected="selected"' : '') . '></option>';
        }
        foreach ($array as $id => $value) {
            $HTML .= '<option value="' . $id . '"' . (($selected == $id) ? ' selected="selected"' : '') . '>' . $value . '</option>';
        }
        $HTML .= '</select>';

        return $HTML;
    }
}