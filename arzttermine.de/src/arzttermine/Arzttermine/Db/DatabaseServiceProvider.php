<?php

namespace Arzttermine\Db;

use Arzttermine\DependencyInjection\ServiceProviderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DatabaseServiceProvider extends MySQLConnection implements ServiceProviderInterface {

    /**
     * {@inheritdoc}
     */
    public function register(ContainerInterface $container)
    {
        $config = $container->getParameter('database.config.type'); // @todo Fix database config
        
        $db = new MySQLConnection();
        $db->initdb($config);
        $db->connect();
        $container->set('database', $db);
    }

    /**
     * {@inheritdoc}
     */
    public function boot(ContainerInterface $container)
    {
    }
    
}