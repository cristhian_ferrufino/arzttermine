<?php

namespace Arzttermine\Db;

use Doctrine\DBAL\Driver\PDOConnection;
use Doctrine\DBAL\Driver\PDOStatement;
use Doctrine\DBAL\Driver\PDOMySql\Driver;

class Connection {

    /**
     * @var bool
     */
    protected $debug = false;
    
    /**
     * Last database error code
     * 
     * @var int
     */
    protected $last_error = 0;

    /**
     * Last database error message
     * 
     * @var string
     */
    protected $error = '';
    
    /**
     * Row resource
     *
     * @var mixed
     */
    private $row;

    /**
     * @var PDOStatement
     */
    private $statement;

    /**
     * @var PDOConnection
     */
    private $handle;

    /**
     * @param array $params
     * @param string $username
     * @param string $password
     * @param array $driverOptions
     */
    public function __construct(array $params, $username = null, $password = null, array $driverOptions = array())
    {
        // If it's empty, it's false. If it's not, it's true
        if (!empty($params['debug'])) {
            $this->setDebug(true);
        }
        
        $this->connect($params, $username, $password, $driverOptions);
    }

    /**
     * This connects the database. Subsequent calls after the first (i.e. the constructor) destroys
     * the currently open handle and creates a new one.
     * 
     * @param array $params
     * @param string $username
     * @param string $password
     * @param array $driverOptions
     * 
     * @return self
     */
    public function connect(array $params, $username = null, $password = null, array $driverOptions = array())
    {
        // Doctrine's DBAL driver does what we need
        $driver = new Driver();
        
        $this->handle = $driver->connect(
            $params,
            $username,
            $password,
            $driverOptions
        );
        
        return $this;
    }

    /**
     * @return boolean
     */
    public function isDebug()
    {
        return (bool)$this->debug;
    }

    /**
     * @param boolean $debug
     *
     * @return self
     */
    public function setDebug($debug)
    {
        $this->debug = (bool)$debug;

        return $this;
    }

    /**
     * This function PREPARES a statement before executing. Slightly different
     * to the default behaviour of PDO::query
     * 
     * @param string $query
     * @param array $parameters
     *
     * @return PDOStatement
     */
    public function query($query = '', $parameters = array())
    {
        return $this->handle->query($this->handle->prepare($query, $parameters));
    }

    /* public: walk result set */
    function next_record()
    {
        if (!$this->Query_ID) {
            $this->halt("next_record called with no query pending.");

            return 0;
        }

        // usually we dont need the indexed but only the assocciated array, so save memory
        $this->Record = @mysql_fetch_array($this->Query_ID, MYSQL_ASSOC);
        $this->Row += 1;
        $this->Errno = mysql_errno();
        $this->Error = mysql_error();

        $stat = is_array($this->Record);
        if (!$stat && $this->Auto_Free) {
            $this->free();
        }

        return $stat;
    }

    /**
     * Return the last resultset as an array
     *
     * @return array
     */
    public function getResult()
    {
        $results = array();

        if ($this->nf() > 0) {
            while ($this->next_record()) {
                $results[] = $this->Record;
            }
        }

        return $results;
    }

    /**
     * @return int the number of affected rows on success, and -1 if the last query
     * failed.
     */
    public function affected_rows()
    {
        return @mysql_affected_rows($this->Link_ID);
    }

    public function num_rows()
    {
        return @mysql_num_rows($this->Query_ID);
    }

    public function insert_id()
    {
        return @mysql_insert_id($this->Link_ID);
    }

    public function num_fields()
    {
        return @mysql_num_fields($this->Query_ID);
    }

    /* public: shorthand notation */
    public function nf()
    {
        return $this->num_rows();
    }

    public function f($Name)
    {
        return $this->Record[$Name];
    }

    /* public: return table metadata */
    public function metadata($table = '', $full = false)
    {
        $res = array();

        // if no $table specified, assume that we are working with a query
        // result
        if ($table) {
            $this->query('SHOW COLUMNS FROM ' . $table);
            $id = $this->Query_ID;
            if (!$id)
                $this->halt("Metadata query failed.");
        } else {
            $id = $this->Query_ID;
            if (!$id)
                $this->halt("No query specified.");
        }

        $count = @mysql_num_rows($id);

        // made this IF due to performance (one if is faster than $count if's)
        if (!$full) {
            $i = 0;
            while ($row = mysql_fetch_assoc($id)) {
                $res[$i] = $row;
                $i++;
            }
        } else { // full
            $res["num_fields"] = $count;

            $i = 0;
            while ($row = mysql_fetch_assoc($id)) {
                $res[$i] = $row;
                $res["meta"][$res[$i]["Field"]] = $i;
                $i++;
            }
        }

        // free the result only if we were called on a table
        if ($table) @mysql_free_result($id);

        return $res;
    }

    /* private: error handling */
    public function halt($msg)
    {
        $this->Error = @mysql_error($this->Link_ID);
        $this->Errno = @mysql_errno($this->Link_ID);
        if ($this->Halt_On_Error == "no")
            return;

        $this->close();
        $this->haltmsg($msg);

        if ($this->Halt_On_Error != "report")
            die("Session halted.");
    }

    protected function haltmsg($msg)
    {
        $this->debug(sprintf("<b>Database error:</b> %s<br>\n", $msg));
        $this->debug(
            sprintf(
                "<b>MySQL Error</b>: %s (%s)<br>Link_ID:%d,Query_ID:%d\n",
                $this->Errno,
                $this->Error,
                $this->Link_ID,
                $this->Query_ID
            )
        );
    }
}