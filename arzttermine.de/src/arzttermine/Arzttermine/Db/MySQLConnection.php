<?php

namespace Arzttermine\Db;

class MySQLConnection {

    /* public: connection parameters */
    public $Host = "localhost";

    public $Database = "";

    public $User = "root";

    public $Password = "";

    /* public: configuration parameters */
    public $Auto_Free = 1; ## Set to 1 for automatic mysql_free_result()
    public $Debug = 0; ## Set to 1 for debugging messages.
    public $Halt_On_Error = 'no'; ## "yes" (halt with message), "no" (ignore errors quietly), "report" (ignore errror, but spit a warning)
    public $Seq_Table = "db_sequence";

    /* public: result array and current row number */
    public $Record = array();

    public $Row;

    /* public: current error number and error text */
    public $Errno = 0;

    public $Error = "";

    /* public: this is an api revision, not a CVS revision. */
    public $type = "mysql";

    public $revision = "1.2";

    /* private: link and query handles */
    public $Link_ID;

    public $Query_ID;

    /**
     * Internal connection handle (mysqli reference)
     * 
     * @var \mysqli
     */
    private $link;

    private $affected_rows;

    /* public: constructor */
    public function __construct($db = "")
    {
        if ($GLOBALS['CONFIG']['MYSQL_CLASS_DEBUG']) {
            $this->Debug = 1;
        }
        if ($db != "") {
            $this->link = $db;
        }
        $this->Halt_On_Error = $GLOBALS['CONFIG']['MYSQL_CLASS_HALT_ON_ERROR'];
    }

    /* public: set specific data */
    public function initdb($id = "default")
    {
        switch ($id) {
            case "admin":
                $this->Host = $GLOBALS['CONFIG']['MYSQL_ADMIN_HOST'];
                $this->Database = $GLOBALS['CONFIG']['MYSQL_ADMIN_DATABASE'];
                $this->User = $GLOBALS['CONFIG']['MYSQL_ADMIN_USER'];
                $this->Password = $GLOBALS['CONFIG']['MYSQL_ADMIN_PASSWORD'];
                break;
        }
        return true;
    }

    /* public: some trivial reporting */

    public function query_id()
    {
        return $this->Query_ID;
    }

    /**
     * @return \mysqli
     */
    public function getLink()
    {
        return $this->link;
    }

    /* public: connection management */
    public function connect($Database = "", $Host = "", $User = "", $Password = "")
    {
        if ($this->Debug) {
            $this->debug(sprintf("CONNECT(%d)<br>", $this->link));
        }
        /* Handle defaults */
        if ("" == $Database)
            $Database = $this->Database;
        if ("" == $Host)
            $Host = $this->Host;
        if ("" == $User)
            $User = $this->User;
        if ("" == $Password)
            $Password = $this->Password;

        /* establish connection, select database */
        if ($this->link instanceof \mysqli === false) {
            if ($this->Debug)
                $this->debug(sprintf("CONNECTING...<br>"));

            $this->link = mysqli_connect($Host, $User, $Password);
            if (!$this->link) {
                $this->halt("connect($Host, $User, \$Password) failed.");

                return 0;
            }
            if ($this->Debug)
                $this->debug(sprintf("Link_ID:%d<br>", mysqli_get_host_info($this->link)));

            if (!mysqli_select_db($this->link, $Database)) {
                $this->halt("cannot use database " . $this->Database);

                return 0;
            }

            mysqli_query($this->link, 'SET SESSION character_set_client = "' . $GLOBALS['CONFIG']['SYSTEM_DB_CHARSET'] . '"');
            mysqli_query($this->link, 'SET SESSION character_set_connection = "' . $GLOBALS['CONFIG']['SYSTEM_DB_CHARSET'] . '"');
            mysqli_query($this->link, 'SET SESSION character_set_results = "' . $GLOBALS['CONFIG']['SYSTEM_DB_CHARSET'] . '"');
        }

        return $this->link;
    }

    /* public: discard the query result */
    public function free()
    {
        // Uh, just check this because there's weirdness resulting in bad null detection
        // @todo: What the fuck is going on with this. Remove if possible the suppression operator
        if ($this->Query_ID instanceof \mysqli_result) {
            @mysqli_free_result($this->Query_ID);
        }
        $this->Query_ID = null;
    }

    public function close()
    {
        if ($this->Debug) {
            $this->debug(sprintf("CLOSING: %d<br>", $this->link));
        }
        mysqli_close($this->link);
        $this->link = null;
    }

    public function debug($text)
    {
        echo '<div style="clear:both;padding:6px;margin:2px;background-color:#EE0000; color:#FFFFFF;font-size:small;">';
        echo htmlentities($text) . '</div>';
    }

    /* public: perform a query */
    public function query($Query_String)
    {
        /* No empty queries, please, since PHP4 chokes on them. */
        if ($this->Debug) {
            $this->debug(sprintf("MySQL: %s<br>%d, %d<br>\n", $Query_String, $this->link, $this->Query_ID));
        }
        if ($Query_String == "")
            /* The empty query string is passed on from the constructor,
            * when calling the class without a query, e.g. in situations
            * like these: '$db = new DB_Sql_Subclass;'
            */
            return 0;

        if (!$this->connect()) {
            return 0; /* we already complained in connect() about that. */
        };

        # New query, discard previous result.
        if (!is_null($this->Query_ID)) {
            $this->free();
        }

        $Query_String = str_replace(array("\n", "\r", "\t"), array(' ',''), $Query_String);
        if(strpos(strtolower($Query_String), 'select') !== false) {
            $Query_String = "/*qc=on*/" .$Query_String;
        }
        $this->Query_ID = mysqli_query($this->link, $Query_String);
        $this->affected_rows = mysqli_affected_rows($this->link);
        $this->Row = 0;
        $this->Errno = mysqli_errno($this->link);
        $this->Error = mysqli_error($this->link);
        if ($this->Query_ID === false) {
            $this->halt("Invalid SQL: " . $Query_String);
        }
        if ($this->Debug)
            $this->debug("finished<br>\n");

        # Will return nada if it fails. That's fine.
        return $this->Query_ID;
    }

    /* public: walk result set */
    public function next_record()
    {
        if (!($this->Query_ID instanceof \mysqli_result)) {
            $this->halt("next_record called with no query pending.");

            return 0;
        }

        // usually we dont need the indexed but only the assocciated array, so save memory
        $this->Record = mysqli_fetch_array($this->Query_ID, MYSQLI_ASSOC);
        $this->Row += 1;
        $this->Errno = mysqli_errno($this->link);
        $this->Error = mysqli_error($this->link);

        $stat = is_array($this->Record);
        if (!$stat && $this->Auto_Free) {
            $this->free();
        }

        return $stat;
    }

    /**
     * Return the last resultset as an array
     * 
     * @return array
     */
    public function getResult()
    {
        $results = array();

        if ($this->nf() > 0) {
            while ($this->next_record()) {
                $results[] = $this->Record;
            }
        }
        
        return $results;
    }

    /**
     * @return int the number of affected rows on success, and -1 if the last query
     * failed.
     */
    function affected_rows()
    {
        return $this->affected_rows;
    }

    function num_rows()
    {
        return ($this->Query_ID instanceof \mysqli_result)?mysqli_num_rows($this->Query_ID):null;
    }

    function insert_id()
    {
        return mysqli_insert_id($this->link);
    }

    function num_fields()
    {
        return ($this->Query_ID instanceof \mysqli_result)?mysqli_num_fields($this->Query_ID):null;
    }

    /* public: shorthand notation */
    function nf()
    {
        return $this->num_rows();
    }

    function f($Name)
    {
        return $this->Record[$Name];
    }

    /* public: return table metadata */
    function metadata($table = '', $full = false)
    {
        $res = array();

        // if no $table specified, assume that we are working with a query
        // result
        if ($table) {
            $this->connect();
            $this->query('SHOW COLUMNS FROM ' . $table);
            $id = $this->Query_ID;
            if (!$id)
                $this->halt("Metadata query failed.");
        } else {
            $id = $this->Query_ID;
            if (!$id)
                $this->halt("No query specified.");
        }

        $count = mysqli_num_rows($id);

        // made this IF due to performance (one if is faster than $count if's)
        if (!$full) {
            $i = 0;
            while ($row = mysqli_fetch_assoc($id)) {
                $res[$i] = $row;
                $i++;
            }
        } else { // full
            $res["num_fields"] = $count;

            $i = 0;
            while ($row = mysqli_fetch_assoc($id)) {
                $res[$i] = $row;
                $res["meta"][$res[$i]["Field"]] = $i;
                $i++;
            }
        }

        // free the result only if we were called on a table
        if ($table) {
            mysqli_free_result($id);
        }

        return $res;
    }

    /* private: error handling */
    function halt($msg)
    {
        $this->Error = mysqli_error($this->link);
        $this->Errno = mysqli_errno($this->link);
        if ($this->Halt_On_Error == "no")
            return;

        $this->close();
        $this->haltmsg($msg);

        if ($this->Halt_On_Error != "report")
            die("Session halted.");
    }

    function haltmsg($msg)
    {
        $this->debug(sprintf("Database error: %s\n", $msg));
        $this->debug(
             sprintf(
                 "MySQL Error: %s (%s) [Link_ID:%d,Query_ID:%d]\n",
                 $this->Errno,
                 $this->Error,
                 $this->link,
                 $this->Query_ID
             )
        );
    }
}
