<?php

namespace Arzttermine\Security;

use Arzttermine\DependencyInjection\ServiceProviderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class AuthenticationServiceProvider implements ServiceProviderInterface {

    /**
     * @var ContainerInterface
     */
    private $container;
    
    /**
     * {@inheritdoc}
     */
    public function register(ContainerInterface $container)
    {
        $profile = new Authentication();
        $container->set('current_user', $profile);
        
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function boot(ContainerInterface $container)
    {
        $current_user = $container->get('current_user');
        $current_user->loadFromSession();
        
        $container->set('current_user', $current_user);

        /** @var \Symfony\Component\EventDispatcher\EventDispatcher $dispatcher */
        $dispatcher = $container->get('dispatcher');
        
        $dispatcher->addListener(KernelEvents::REQUEST, array($this, 'onKernelRequest'));
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        // Quick kludge to check if we need the user logged in
        // @todo This will be covered in the Security service
        
        // If there's no user and there should be, redirect to a login page
        if (
            !$this->container->get('current_user')->isValid() &&
            $this->container->getParameter('app.secure')
        ) {
            $event->setResponse(new RedirectResponse(
                $this->container->get('router')->generate('login', array('referrer' => $event->getRequest()->getRequestUri()), true)
            ));
        }
    }
}