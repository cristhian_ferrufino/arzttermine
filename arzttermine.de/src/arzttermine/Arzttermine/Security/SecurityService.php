<?php

namespace Arzttermine\Security;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Security service. Manages Authentication and Authorization
 *
 * @package Arzttermine\Security
 */
class SecurityService {

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }
    
    // @todo: Event hooks

    public function authenticate()
    {
        
    }

    /**
     * If the user has the correct context according to the decision maker provided, then this is true
     * 
     * @return bool
     */
    public function isAuthorized()
    {
        
    }
    
    /**
     * Load user settings and rights from the session
     */
    public function obtainContext()
    {
        
    }

    /**
     * Clear user settings and rights from the session (i.e. logout)
     */
    public function clearContext()
    {
        
    }

}