<?php

namespace Arzttermine\Security;

use Arzttermine\DependencyInjection\ServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SecurityServiceProvider implements ServiceInterface {

    /**
     * {@inheritdoc}
     */
    public function initialize(ContainerInterface $container)
    {
        $security = new SecurityService($container);
        
        // @todo: Event listeners
        
        $container->set('security', $security);
    }

}