<?php

namespace Arzttermine\Security;

use Arzttermine\Application\Application;
use Arzttermine\User\User;

class Authentication extends User {

    /**
     * @return int
     */
    public function loadFromSession()
    {
        $session = Application::getInstance()->container->get('session'); 
        
        if ($session->has('USERID')) {
            parent::load($session->get('USERID'));

            return $this->getId();
        } else {
            return 0;
        }
    }

    /**
     * Login a user and return the user_id
     *
     * @param string $email
     * @param string $password
     *
     * @return int
     */
    public function login($email, $password)
    {
        $app = Application::getInstance();
        $session = $app->container->get('session');

        // Always use the logedin User first
        if ($session->has('USERID')) {
            parent::load($session->get('USERID'));

            return true;
        } elseif (User::checkCredentials($email, $password)) {
            parent::load($email, 'email');
            // If the user is valid and has been activated
            if ($this->isValid() && $this->isActive()) {
                $session->set('USERID', $this->getId());

                return true;
            }
        }
        
        return false;
    }

    /**
     * Log the user out
     */
    public function logout()
    {
        if ($this->id) {
            $session = Application::getInstance()->container->get('session');
            $session->remove('USERID');
            $session->invalidate();
            $this->id = 0;
        }
    }
}