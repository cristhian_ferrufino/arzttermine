<?php

namespace Arzttermine\Core;

use Arzttermine\DependencyInjection\ServiceProviderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ApplicationContextServiceProvider implements ServiceProviderInterface {

    /**
     * @var ContainerInterface
     */
    private $container;
    
    /**
     * {@inheritdoc}
     */
    public function register(ContainerInterface $container)
    {
        $context = new ApplicationContextService($container);
        $container->set('context', $context);

        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function boot(ContainerInterface $container)
    {
        if ($container->has('dispatcher')) {
            /** @var \Symfony\Component\EventDispatcher\EventDispatcher $dispatcher */
            $dispatcher = $container->get('dispatcher');
            $dispatcher->addListener(KernelEvents::REQUEST, array($this, 'onKernelRequest'), 128);
        }
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        /** @var ApplicationContextService $context */
        $context = $this->container->get('context');
        $context->determineContext();
        
        // This next part is temporary and should be in its own extension
        if ($this->container->has('request')) {
            /** @var \Arzttermine\Http\Request $request */
            $request = $this->container->get('request');
            
            // @todo Fix the below URLs to remove global config flag
            $url_is_mobile = substr($request->getHost(), 0, 2) === 'm.';
            
            // If we're trying to view mobile on desktop, redirect
            if (!$context->isMobile() && $url_is_mobile) {
                $event->setResponse($request->redirect($GLOBALS['CONFIG']['URL_HTTP_LIVE'] . $request->getRequestUri()));
            }
            
            if ($context->isMobile()) {
                // Also a hack below until the View object is a subclassed Response object
                define('SYSTEM_THEME', 'mobile');
            }
        }

        // @todo Move this
        if ($context->isMobile()) {
            $this->container->setParameter('app.source_platform', 'mobile-site');
        }
    }

}
