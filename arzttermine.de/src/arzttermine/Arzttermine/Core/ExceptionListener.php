<?php

namespace Arzttermine\Core;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ExceptionListener
{
    /**
     * @var ContainerInterface
     */
    private $container;
    
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }
    
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();
        $response = new Response();

        // Not found. Only applies to stuff that's actually missing
        if ($exception instanceof NotFoundHttpException) {
            // If we don't have a template then don't render anything
            if ($this->container->has('templating')) {
                $view = $this->container->get('templating');
                $response->setContent(
                    $view->addHeadTitle('404')->fetch('404.tpl')
                );
            }
            
            $response->setStatusCode(Response::HTTP_NOT_FOUND);
        } else {
            // Everything else will just dump an exception to the client
            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $event->setResponse($response);
    }
}