<?php

namespace Arzttermine\Core;

final class ContextType {

    /**
     * Context flags
     * Any request can be one or more of these
     */
    const MOBILE   = 1;  // Device is mobile
    const STANDARD = 2;  // Application is running in standard mode (i.e. not reduced or mobile)
                         // Note: Flag 4 was here as it was tablet. Can be re-added or re-used if properly refactored
    const CLI      = 8;  // Command Line Interface (i.e. terminal)
    const WIDGET   = 16; // Request is a widget
    const API      = 32; // Request is an API call
    
}