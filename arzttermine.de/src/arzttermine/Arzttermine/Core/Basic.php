<?php

namespace Arzttermine\Core;

use Arzttermine\Application\Application;

/**
 * Basic model
 */
abstract class Basic {

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $tablename = '';

    /**
     * @var array
     */
    protected $all_keys = array();

    /**
     * @var array
     */
    protected $secure_keys = array();

    /**
     * Constructor
     *
     * @param mixed $value
     * @param string $key
     */
    public function __construct($value = 0, $key = 'id')
    {
        $this->id = $this->load($value, $key);
    }

    /**
     * Prevent implicit declaration of variables
     *
     * @param string $property
     * @param mixed $value
     *
     * @throws \Exception
     */
    public function __set($property, $value)
    {
        throw new \Exception("Undefined object property {$property}");
    }

    /**
     * @return array
     */
    public function getAllKeys()
    {
        return $this->all_keys;
    }

    /**
     * @return array
     */
    public function getSecureKeys()
    {
        return $this->secure_keys;
    }

    /**
     * @param int $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Dump the contents of the class
     *
     * @param bool $output_immediately
     *
     * @return string
     */
    public function debug($output_immediately = true)
    {
        $debug = '';

        foreach ($this->all_keys as $value) {
            $debug .= "{$value}: {$value}<br/>";
        }

        if ($output_immediately) {
            echo $debug;
        }

        return $debug;
    }

    /**
     * Checks if this is a valid object
     *
     * @return boolean
     */
    public function isValid()
    {
        return (intval($this->id) > 0);
    }

    /**
     * Loads the values into the object
     *
     * @param mixed $value
     * @param string $key
     *
     * @return int
     */
    public function load($value, $key = 'id')
    {
        if ($key === 'id' && (!ctype_digit((string)$value)  || $value === 0)) {
            return 0;
        }

        if (in_array($key, $this->secure_keys)) {
            $value = sqlsave($value);
        }

        $db = Application::getInstance()->getSql();
        $sql = "SELECT * FROM {$this->tablename} WHERE {$key}=";
        if(ctype_digit((string)$value) === true) {
            $sql .= "{$value};";
        } else {
            $sql .= "'{$value}';";
        }

        $db->query($sql);

        if ($db->nf() == 1) {
            $db->next_record();

            $this->hydrate($db->Record);

            if (method_exists($this, 'calcVars')) {
                $this->calcVars();
            }

            return $this->id;
        }

        return 0;
    }

    /**
     * Populate this object's properties based on the data provided
     *
     * @param array $data
     * 
     * @return self
     */
    public function hydrate(array $data)
    {
        foreach ($data as $key => $value) {
            if (property_exists($this, $key)) {
                $this->$key = $value;
            } else {
                error_log("Warning: Property '{$key}' of class '" . get_class($this) . "' not found. Skipping.");
            }
        }
        
        return $this;
    }
    
    /**
     * Start preparing this object's data for the database
     *
     * @param array|string $filter_keys
     *
     * @return array
     */
    protected function prepare($filter_keys = array())
    {
        // The filter can be a single key or an array of keys
        if (!empty($filter_keys) && !is_array($filter_keys)) {
            $filter_keys = array($filter_keys);
        }

        // If keys are provided, only update what we've been asked to
        if (!empty($filter_keys)) {
            // filter out all non existing keys
            $keys = array_flip(array_intersect_key(array_flip($this->all_keys), array_flip($filter_keys)));
        } else {
            $keys = $this->all_keys;
        }

        return $this->getProperties($keys);
    }

    /**
     * Return columns for this object (for use with a query, e.g.)
     * 
     * @return string[]
     */
    protected function getQueryColumns()
    {
        $sql = array();
        
        foreach ($this->all_keys as $column) {
            $sql[] = " {$this->tablename}.{$column} ";
        }
            
        return $sql;
    }

    /**
     * Get all class properties
     * 
     * @param array $keys
     *
     * @return array
     */
    protected function getProperties(array $keys)
    {
        $data = array();
        
        foreach ($keys as $key) {
            $data[$key] = in_array($key, $this->secure_keys) ? sqlsave($this->$key) : $this->$key;
        }
        
        return $data;
    }

    /**
     * Stores a new object
     *
     * @return bool
     */
    public function saveNewObject()
    {
        $data = $this->getDatas();

        return $this->saveNew($data);
    }

    /**
     * Stores a new object by an array
     *
     * @param array $data
     * @param bool $save_only_registered_keys
     *
     * @return int|bool
     */
    public function saveNew($data, $save_only_registered_keys = true)
    {
        $app = Application::getInstance();
        $db = $app->container->get('database');
        $sql = "INSERT INTO {$this->tablename}";
        $set = array();

        // Tag the "created/updated at" fields if they exist
        if (property_exists($this, 'created_at')) {
            $data['created_at'] = date('Y-m-d H:i:s');
        }

        if (property_exists($this, 'updated_at')) {
            $data['updated_at'] = date('Y-m-d H:i:s');
        }
        
        // Unset data we're not going to use
        if ($save_only_registered_keys) {
            $data = array_intersect_key($data, array_flip($this->all_keys));
        }
        
        // Populate this object AS an object...
        $this->hydrate($data);

        // ... so we can fetch it using internal filters where available
        foreach ($this->prepare() as $key => $value) {
            // we never ever want to store NEW data with an specific id, do we?
            if ($key == 'id') {
                continue;
            }

            $set[] = "{$key}='{$value}'";
        }

        if ($set) {
            $sql .= ' SET ' . implode(',', $set);
        }

        if ($db->query($sql)) {
            if (isset($data['id']) && $data['id'] !== 0) {
                // DB-schema is NOT auto_increment
                $id = $data['id'];
                $this->id = $id;
            } elseif ($id = $db->insert_id()) {
                // DB-schema is auto_increment;
                // If we have a new ID then store the slug
                $this->id = $id;

                // If we need a slug then take it from the ID but only if there is none given
                if (in_array('slug', $this->all_keys) && !isset($data['slug'])) {
                    $this->slug = $id;
                    $this->save('slug');
                }
            }

            return $id;
        }

        return false;
    }

    /**
     * Stores an object
     *
     * @param mixed $keys
     *
     * @return bool
     *
     * Returns int but should be bool.
     * @todo: check if that can be changed without trouble
     */
    public function save($keys = array())
    {
        $app = Application::getInstance();
        $db = $app->container->get('database');
        $set = array();

        // Tag the update with a timestamp
        if (property_exists($this, 'updated_at')) {
            $this->updated_at = date('Y-m-d H:i:s');
        }
        
        $sql = "UPDATE {$this->tablename}";
            
        foreach ($this->prepare($keys) as $key => $value) {
            if(is_numeric($value) === true) {
                $set[] = "{$key}={$value}";
            } else {
                $set[] = "{$key}='{$value}'";
            }

        }

        $sql .= ' SET ' . implode(',', $set);
        $sql .= " WHERE id={$this->id}";

        $db->query($sql);

        if (method_exists($this, 'calcVars')) {
            $this->calcVars();
        }

        return ($db->query_id() !== false);
    }

    /**
     * Try to find ONE object by a given key
     *
     * @param string $where
     * @param string $key
     * @param string $select
     *
     * @return int key or 0 (error)
     */
    public function findKey($where = '1=1', $key = 'id', $select = '*')
    {
        $db = Application::getInstance()->getSql();

        $sql = "SELECT {$select} FROM {$this->tablename} WHERE {$where};";
        $db->query($sql);

        if ($db->nf() == 1) {
            $db->next_record();

            return $db->f($key);
        }

        return 0;
    }

    /**
     * Try to find ONE OR MORE objects by a given key
     *
     * @param string $where
     * @param string $key
     * @param string $select
     *
     * @return array
     */
    public function findKeys($where = '1=1', $key = 'id', $select = '*')
    {
        $db = Application::getInstance()->getSql();
        $keys = array();

        $sql = "SELECT {$select} FROM {$this->tablename} WHERE {$where};";
        $db->query($sql);

        if ($db->nf() > 0) {
            while ($db->next_record()) {
                $keys[] = $db->f($key);
            }
        }

        return $keys;
    }

    /**
     * Try to find ONE OR MORE values and index them by a key
     *
     * @param string $where
     * @param string $select
     * @param string $key
     *
     * @return array
     */
    public function findValues($where = '1=1', $select = '*', $key = 'id')
    {
        $db = Application::getInstance()->getSql();
        $records = array();

        $sql = "SELECT {$select}, {$key} FROM {$this->tablename} WHERE {$where};";
        $db->query($sql);

        if ($db->nf() > 0) {
            while ($db->next_record()) {
                if ($select == '*') {
                    $records[$db->f($key)] = $db->Record;
                } else {
                    $records[$db->f($key)] = $db->f($select);
                }
            }
        }

        return $records;
    }

    /**
     * Try to find ONE object and return the found record
     *
     * @param string $where
     * @param string $select
     *
     * @return mixed record or false on error
     */
    public function find($where = '1=1', $select = '*')
    {
        $db = Application::getInstance()->getSql();

        $sql = "SELECT {$select} FROM {$this->tablename} WHERE {$where} LIMIT 1;";
        $db->query($sql);

        if ($db->nf() == 1) {
            if ($db->next_record()) {
                return $db->Record;
            }
        }

        return false;
    }

    /**
     * This works like ::findKey(), but more logically
     *
     * @param mixed $value
     * @param string $key
     *
     * @return mixed
     */
    public function findByKey($value, $key = 'id')
    {
        $value = sqlsave( (string)$value );
        $where = "{$key}='{$value}'";

        return $this->find($where);
    }

    /**
     * Try to find ONE OR MORE records
     *
     * @param string $where
     * @param string $select
     *
     * @return array
     */
    public function finds($where = '1=1', $select = '*')
    {
        $db = Application::getInstance()->getSql();
        $records = array();

        $sql = "SELECT {$select} FROM {$this->tablename} WHERE {$where};";
        $db->query($sql);

        if ($db->nf() > 0) {
            while ($db->next_record()) {
                $records[] = $db->Record;
            }
        }

        return $records;
    }

    /**
     * Try to find ONE object and return the found object
     *
     * @param string $where
     * @param string $select
     *
     * @return $this
     */
    public function findObject($where = '1=1', $select = '*')
    {
        $db = Application::getInstance()->getSql();

        if ($select != 'id' && $select != '*') {
            $select = 'id,{$select}';
        }

        $sql = "SELECT {$select} FROM {$this->tablename} WHERE {$where} LIMIT 1;";
        $db->query($sql);

        // Load an instance of the model
        $self = get_class($this);
        /** @var self $model */
        $model = new $self();

        if ($db->nf() > 0) {
            while ($db->next_record()) {
                $model->load($db->f('id'));
            }
        }

        return $model;
    }

    /**
     * Try to find ONE OR MORE objects and return the found objects array
     * add_filed_array: attributes which will be available in the objects
     *
     * @param string $where
     * @param string $select
     * @param array $add_fields
     *
     * @return array
     */
    public function findObjects($where = '1=1', $select = '*', $add_fields = array())
    {
        $db = clone Application::getInstance()->getSql();
        $objects = array();

        $sql = "SELECT id FROM {$this->tablename} WHERE {$where};";
        $db->query($sql);

        if ($db->nf() > 0) {
            while ($db->next_record()) {
                $self = get_class($this);
                /** @var self $model */
                $model = new $self($db->f('id'));
                if ($model->isValid()) {
                    // set the additional data in the object
                    $objects[] = $model;
                }
            }
        }
        unset($db);

        return $objects;
    }

    /**
     * Try to find ONE OR MORE objects and return the found objects array
     * index must be id!
     *
     * @param string $sql query
     *
     * @return array
     */
    public function findObjectsSql($sql)
    {
        $db = Application::getInstance()->getSql();
        $objects = array();

        $db->query($sql);

        if ($db->nf() > 0) {
            $ids = array();

            while ($db->next_record()) {
                $ids[] = $db->f('id');
            }

            foreach ($ids as $id) {
                $self = get_class($this);
                /** @var self $model */
                $model = new $self($id);

                if ($model->isValid()) {
                    $objects[] = $model;
                }
            }
        }

        return $objects;
    }

    /**
     * Count the number of records
     *
     * @param string $where
     * @param string $select
     *
     * @return int
     */
    public function count($where = '1=1', $select = '')
    {
        $db = Application::getInstance()->getSql();
        $_select = 'COUNT(*) AS num';

        if ($select) {
            $_select = "{$select}, COUNT(*) AS num";
        }

        $sql = "SELECT {$_select} FROM {$this->tablename} WHERE {$where};";
        $db->query($sql);

        if ($db->nf() > 0) {
            $db->next_record();

            return (int)$db->f('num');
        }

        return 0;
    }

    /**
     * Delete record(s)
     *
     * @param mixed $where (or id)
     *
     * @return int affected rows
     */
    public function delete($where = null)
    {
        $db = Application::getInstance()->getSql();

        if (is_numeric($where)) {
            $where = "id={$where}";
        } elseif (!$where) {
            $where = "id={$this->id}";
        }

        $sql = "DELETE FROM {$this->tablename} WHERE {$where};";
        $db->query($sql);

        return $db->affected_rows();
    }

    /**
     * Delete ONE OR MORE records
     *
     * @param string $where
     *
     * @return int affected rows
     */
    public function deleteWhere($where)
    {
        $db = Application::getInstance()->getSql();

        $sql = "DELETE FROM {$this->tablename} WHERE {$where};";
        $db->query($sql);

        return $db->affected_rows();
    }

    /**
     * Truncates the table
     *
     * @return bool
     */
    public function truncate()
    {
        $db = Application::getInstance()->getSql();

        $sql = "TRUNCATE {$this->tablename};";
        $db->query($sql);
        return $db->affected_rows();
    }

    /**
     * Returns a date
     *
     * @param string $name
     * @param string $format
     *
     * @return string a href
     */
    public function getPrettyDate($name, $format = 'd.m.Y')
    {
        if (isset($this->$name)) {
            return mysql2date($format, $this->$name);
        }

        return '';
    }

    /**
     * Returns a date time
     *
     * @param string $name ,
     * @param string $format
     *
     * @return string a href
     */
    public function getPrettyDateTime($name, $format = 'd.m.Y / H:i:s')
    {
        if (isset($this->$name)) {
            return mysql2date($format, $this->$name);
        }

        return '';
    }

    /**
     * Returns the human time diff from now to creating time
     *
     * @return string human time diff
     */
    public function getCreatedAtHumanTimeDiff()
    {
        if (method_exists($this, 'getCreatedAt')) {
            return human_time_diff(mysql2date('U', $this->getCreatedAt()), Application::getInstance()->now('timestamp'));
        }
        
        return 0;
    }

    /**
     * Return data from object as an array
     *
     * @param mixed $keys
     *
     * @return array
     */
    public function getDatas($keys = '')
    {
        $data = array();

        if ($keys) {
            if (is_array($keys)) {
                // return all given key values
                foreach ($keys as $key) {
                    if (isset($this->$key)) {
                        $data[$key] = $this->$key;
                    }
                }
            } elseif (isset($this->$keys)) {
                // return just one value
                $data[$keys] = $this->$keys;
            }
        } else {
            // return all data
            foreach ($this->all_keys as $key) {
                $data[$key] = $this->$key;
            }
        }
        
        return $data;
    }

    /**
     * Set data for object
     *
     * @param string $key
     * @param mixed $value
     *
     * @return bool
     */
    public function setData($key, $value)
    {
        return $this->$key = $value;
    }

    /**
     * Get data from object
     *
     * @param string $key
     *
     * @return mixed
     */
    public function getData($key = '')
    {
        if (isset($this->$key)) {
            return $this->$key;
        }

        return null;
    }

    /**
     * Returns values from loaded objects
     *
     * @param array $keys
     * @param string $where
     *
     * @return array
     */
    public function getAllData($keys = array(), $where = '1=1')
    {
        $data = array();

        if (!$keys) {
            $keys = $this->all_keys;
        }

        $objects = $this->findObjects($where);

        if (is_array($objects) && !empty($objects)) {
            foreach ($objects as $object) {
                $value = array();

                foreach ($keys as $key) {
                    if (in_array($key, $object->all_keys)) {
                        $value[$key] = $object->$key;
                    }
                }

                $data[] = $value;
            }
        }

        return $data;
    }
}