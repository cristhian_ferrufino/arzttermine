<?php

namespace Arzttermine\Core;

use Arzttermine\DependencyInjection\ServiceProviderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ExceptionListenerServiceProvider implements ServiceProviderInterface {

    /**
     * {@inheritdoc}
     */
    public function register(ContainerInterface $container)
    {
        $exception_listener = new ExceptionListener($container);
        $container->set('exception_listener', $exception_listener);
    }

    /**
     * {@inheritdoc}
     */
    public function boot(ContainerInterface $container)
    {
        if ($container->has('dispatcher')) {
            /** @var \Symfony\Component\EventDispatcher\EventDispatcher $dispatcher */
            $dispatcher = $container->get('dispatcher');
            $dispatcher->addListener(
                KernelEvents::EXCEPTION,
                array($container->get('exception_listener'), 'onKernelException'));
        }
    }

}