<?php

namespace Arzttermine\Core;

use Arzttermine\Mail\AdminMailer;

class Console {

    // BORDER STYLES
    const CONSOLE_HEADER = '=';
    const CONSOLE_SUBHEADER = '-';
    const CONSOLE_NOTICE = '*';
    
    // COLORS
    const COLOR_YELLOW = "\033[1;33m";
    const COLOR_RED    = "\033[1;31m";
    const COLOR_WHITE  = "\033[1;37m";
    const COLOR_BLUE   = "\033[1;34m";
    const COLOR_NONE   = "";

    /**
     * @var self
     */
    private static $instance;
    
    /**
     * Raw options
     * 
     * @var array
     */
    protected $options = array();
    
    /**
     * @var string
     */
    protected $environment = 'development';

    /**
     * @var string
     */
    protected $action = '';

    /**
     * @var bool
     */
    protected $debug = false;

    /**
     * @var bool
     */
    protected $silent = false;

    /**
     * Whether or not to keep track of execution time
     * 
     * @var bool
     */
    protected $profiler = true;

    /**
     * @var string
     */
    private $start_time;

    /**
     * @var string
     */
    private $end_time;

    /**
     * @var bool
     */
    protected $use_color = true;

    /**
     * Current console color
     * 
     * @var string
     */
    private $color;

    /**
     * This will store configuration options for all environments in the format:
     * array( 'environment' => options[] )
     * 
     * @var array
     */
    protected $configured_environments = array();

    /**
     * Set up the console
     */
    protected function __construct()
    {
        $this->loadOptions();
        $this->parseOptions();
    }

    /**
     * Clean up anything we've started
     */
    public function __destruct()
    {
        if ($this->silent) {
            if ($this->debug) {
                error_log(print_r(ob_get_contents(), true));
            }

            @ob_end_clean();
        }
    }

    /**
     * Singleton accessor
     *
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param string $color_code
     * 
     * @return self
     */
    protected function color($color_code = null)
    {
        $this->color = $color_code;
        
        return $this;
    }

    /**
     * Push a single character to the console, completely unchanged.
     * 
     * @param string $character
     *
     * @return self
     */
    public function ch($character = '')
    {
        echo $character;
        
        return $this;
    }

    /**
     * Start the console session
     * 
     * @return self
     */
    public function start()
    {
        if ($this->silent) {
            ob_start();
        }

        $this
            ->writeLine('')
            ->writeHeader('BEGIN');
        
        if ($this->profiler) {
            $this->start_time = microtime(true);
            $this
                ->writeLine('Time: ' . date('Y-m-d H:i:s', $this->start_time))
                ->writeHeader('', 2);
        }

        #if (!$environment_forced) {
        #    show_notice("WARNING! No environment was detected. Using 'development'", 2, CONSOLE_NOTICE);
        #}

        if ($this->debug) {
            $this->writeSubheader('DEBUG MODE ACTIVE', 2);
        }

        $this
            ->writeLine("ENVIRONMENT: {$_ENV['SYSTEM_ENVIRONMENT']}")
            ->writeLine('SCRIPT: ' . $this->action)
            ->writeLine('FLAGS: ' . print_r($this->options, true), 1);
        
        return $this;
    }

    /**
     * End the console session and display any additional information
     * 
     * @return self
     */
    public function stop()
    {
        if ($this->profiler) {
            $this->end_time = microtime(true);
            
            $this
                ->writeHeader('')
                ->writeLine('Time: ' . date('Y-m-d H:i:s', $this->end_time) . ' (' . $this->profile($this->start_time, $this->end_time) . ')');
        }

        $this->writeHeader('END', 2);
        
        return $this;
    }

    /**
     * Calculate difference for two given microtimes
     * 
     * @param string $start_time
     * @param string $current_time
     * @param int $significant_digits
     *
     * @return string
     */
    public function profile($start_time = null, $current_time = null, $significant_digits = 9)
    {
        if (!$start_time && $start_time !== 0) {
            $start_time = microtime(true);
        }

        if (!$current_time && $current_time !== 0) {
            $current_time = microtime(true);
        }

        return number_format(floatval($current_time) - floatval($start_time), intval($significant_digits)) . 's';
    }

    /**
     * @return self
     */
    protected function loadOptions()
    {
        $this->options = getopt('sca:e:d', array('silent', 'no-color', 'action:', 'environment:', 'debug'));
        
        return $this;
    }

    /**
     * @return self
     */
    protected function parseOptions()
    {
        foreach ($this->options as $option => $value) {
            switch ($option) {
                case 'e':
                case 'environment':
                    // Rely on the ENV variable for the option for now
                    // $this->environment = $options[$option];
                    $this->environment = $_ENV['SYSTEM_ENVIRONMENT'];
                    break;

                case 'a':
                case 'action':
                    // Currently unused
                    $this->action = $this->options[$option];
                    break;

                case 'd':
                case 'debug':
                    $this->debug = true;
                    break;

                case 's':
                case 'silent':
                    $this->silent = true;
                    break;

                case 'c':
                case 'no-color':
                    $this->use_color = false;
                    break;
            }
        }

        return $this;
    }

    /**
     * @param string $name
     * @param array $environment
     *
     * @return self
     */
    public function addEnvironment($name, array $environment)
    {
        $this->configured_environments[$name] = $environment;
        
        return $this;
    }

    /**
     * @param array $environments
     *
     * @return self
     */
    public function addEnvironments(array $environments)
    {
        if (!empty($environments)) {
            foreach($environments as $name => $environment) {
                $this->addEnvironment($name, $environment);
            }
        }
        
        return $this;
    }

    /**
     * @param string $string
     * @param int $line_height
     *
     * @return self
     */
    public function writeLine($string, $line_height = 1)
    {
        if ($this->use_color) {
            echo "{$this->color}";
        }
        echo $string;
        if ($this->use_color) {
            echo "\033[0m";
        }
        echo str_repeat("\n", intval($line_height));
        return $this;
    }

    /**
     * @param string $title
     * @param int $line_height
     *
     * @return self
     */
    public function writeHeader($title, $line_height = 1)
    {
        return $this
            ->color(self::COLOR_BLUE)
            ->writeNotice($title, $line_height, self::CONSOLE_HEADER)
            ->color(self::COLOR_NONE);
    }

    /**
     * @param string $title
     * @param int $line_height
     *
     * @return self
     */
    public function writeSubheader($title, $line_height = 1)
    {
        return $this
            ->color(self::COLOR_WHITE)
            ->writeNotice($title, $line_height, self::CONSOLE_SUBHEADER)
            ->color(self::COLOR_NONE);
    }

    /**
     * @param string $title
     * @param int $line_height
     * @param string $character
     *
     * @return self
     */
    public function writeNotice($title, $line_height = 1, $character = self::CONSOLE_NOTICE)
    {
        $title = trim($title);
        $this->writeLine(str_pad($title ? " {$title} " : '', 80, $character, STR_PAD_BOTH), $line_height);
        
        return $this;
    }

    /**
     * @param string $message
     * @param int $line_height
     * 
     * @return self;
     */
    public function writeWarning($message, $line_height = 1)
    {
        $message = trim($message);

        if ($message) {
            $this
                ->color(self::COLOR_YELLOW)
                ->writeNotice("WARNING", 1, self::CONSOLE_NOTICE)
                ->color(self::COLOR_NONE)
                ->writeLine($message, $line_height);
        }
        
        return $this;
    }

    /**
     * @param string $message
     * @param string $subject
     * @param string $source
     * @param int $line
     */
    public function sendWarningEmail($message, $subject, $source = 'CLI', $line = 0)
    {
        AdminMailer::send(
            $GLOBALS['CONFIG']["SALESFORCE_ALERT_MESSAGES_EMAILS"],
            $message,
            $subject,
            $source,
            $line
        );
    }
}