<?php

namespace Arzttermine\Core;

use Arzttermine\Application\Application;

class BasicNoIndex {

    /**
     * @var string
     */
    protected $tablename;

    /**
     * @var array
     */
    protected $all_keys = array();

    /**
     * @var array
     */
    protected $secure_keys = array();

    /**
     * Constructor
     *
     * @param mixed $value
     * @param string $key
     */
    public function __construct($value = 0, $key = 'id')
    {
        if (method_exists($this, 'load')) {
            $this->id = $this->load($value, $key);
        }
    }

    /**
     * Prevent implicit declaration of variables
     *
     * @param string $property
     * @param mixed $value
     *
     * @throws \Exception
     */
    public function __set($property, $value)
    {
        throw new \Exception("Undefined property {$property}");
    }

    /**
     * @return array
     */
    public function getAllKeys()
    {
        return $this->all_keys;
    }

    /**
     * @return array
     */
    public function getSecureKeys()
    {
        return $this->secure_keys;
    }

    /**
     * Save a new object
     *
     * @param array $data
     * @param bool $save_only_registered_keys
     *
     * @return bool
     */
    public function saveNew(array $data = null, $save_only_registered_keys = true)
    {
        $db = Application::getInstance()->getSql();
        $set = array();

        $sql = "INSERT INTO {$this->tablename}";

        if (empty($data)) {
            $data = $this->getDatas();
        }

        foreach ($data as $key => $value) {
            if ($save_only_registered_keys && !in_array($key, $this->all_keys)) {
                continue;
            }

            if (in_array($key, $this->secure_keys)) {
                $value = sqlsave($value);
            }

            $set[] = "{$key}='{$value}'";
        }

        if ($set) {
            $sql .= ' SET ' . implode(',', $set);
        }

        return (bool)$db->query($sql);
    }

    /**
     * Stores a new object
     *
     * @return bool
     */
    public function saveNewObject()
    {
        $data = $this->getDatas();

        return $this->saveNew($data);
    }

    /**
     * Returns all the data for this object as key => value pairs
     *
     * @todo Check against all uses of this class
     *
     * @param array|string $keys
     *
     * @return array
     */
    public function getDatas($keys = null)
    {
        $data = array();

        if (empty($keys)) {
            $keys = $this->all_keys;
        } elseif (!is_array($keys)) {
            $keys = array($keys);
        }

        // return all given key values
        foreach ($keys as $key) {
            if (isset($this->$key)) {
                $data[$key] = $this->$key;
            }
        }

        return $data;
    }

    /**
     * Try to find ONE OR MORE objects and return the found objects array
     * add_filed_array: attributes which will be available in the objects
     *
     * @param string $where
     * @param string $select
     * @param array $add_fields
     *
     * @return array
     */
    public function findObjects($where, $select = '*', array $add_fields = array())
    {
        $db = Application::getInstance()->getSql();
        $objects = array();

        $sql = "SELECT {$select} FROM {$this->tablename} WHERE {$where};";
        $db->query($sql);

        if ($db->nf() > 0) {
            while ($db->next_record()) {
                $self = get_class($this);
                /** @var self $model */
                $model = new $self();

                $keys = array_merge($this->all_keys, $add_fields);

                foreach ($keys as $key) {
                    if (isset($db->Record[$key])) {
                        $model->setData($key, $db->f($key));
                    }
                }

                $objects[] = $model;
            }
        }

        return $objects;
    }

    /**
     * Set data for object
     *
     * @deprecated
     *
     * @param string $key
     * @param mixed $value
     *
     * @return bool
     */
    public function setData($key, $value)
    {
        return $this->$key = $value;
    }

    /**
     * Get data from object
     *
     * @deprecated
     *
     * @param string $key
     *
     * @return mixed
     */
    public function getData($key = '')
    {
        if (isset($this->$key)) {
            return $this->$key;
        }

        return null;
    }

    /**
     * Returns values from loaded objects
     *
     * @param array $keys
     * @param string $where
     *
     * @return array
     */
    public function getAllData($keys = array(), $where = '1=1')
    {
        $data = array();

        if (!$keys) {
            $keys = $this->all_keys;
        }

        $objects = $this->findObjects($where);

        if (is_array($objects) && !empty($objects)) {
            foreach ($objects as $object) {
                $value = array();

                foreach ($keys as $key) {
                    if (in_array($key, $object->all_keys)) {
                        $value[$key] = $object->$_key;
                    }
                }

                $data[] = $value;
            }
        }

        return $data;
    }

    /**
     * Delete ONE OR MORE records
     *
     * @param string $where
     *
     * @return int affected rows
     */
    public function deleteWhere($where)
    {
        $db = Application::getInstance()->getSql();

        $sql = "DELETE FROM {$this->tablename} WHERE {$where};";
        $db->query($sql);

        return $db->affected_rows();
    }

    /**
     * Count the number of records
     *
     * @param string $where
     * @param string $select
     *
     * @return int
     */
    public function count($where = '1=1', $select = '')
    {
        $db = Application::getInstance()->getSql();
        $_select = 'COUNT(*) AS num';

        if ($select) {
            $_select = "{$select}, COUNT(*) AS num";
        }

        $sql = "SELECT {$_select} FROM {$this->tablename} WHERE {$where};";
        $db->query($sql);

        if ($db->nf() > 0) {
            $db->next_record();

            return (int)$db->f('num');
        }

        return 0;
    }

    /**
     * Truncates the table
     *
     * @return bool
     *
     */
    public function truncate()
    {
        $sql = "TRUNCATE TABLE {$this->tablename}";

        return Application::getInstance()->getSql()->query($sql);
    }
}