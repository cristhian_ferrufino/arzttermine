<?php

namespace Arzttermine\Core;

use Arzttermine\Application\Application;
use Arzttermine\Appointment\Provider;
use Arzttermine\Booking\Booking;
use Arzttermine\Event\Event;
use Arzttermine\Insurance\Insurance;
use Arzttermine\Calendar\Calendar;
use Arzttermine\Location\Location;
use Arzttermine\Mail\Mailing;
use Arzttermine\Maps\GoogleMap;
use Arzttermine\MedicalSpecialty\MedicalSpecialty;
use Arzttermine\MedicalSpecialty\TreatmentType;
use Arzttermine\Search\Search;
use Arzttermine\Search\Doctor;
use Arzttermine\User\User;
use Arzttermine\Widget\WidgetFactory;

use NGS\Managers\SearchResultCacheTmpManager;
use NGS\Util\ProviderRow;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Api Class
 *
 * @author Axel Kuzmik <axel@kuzmik.de>
 */
class Api {

    /**
     * Flip a switch if we're a widget
     */
    public function __construct()
    {
        $app = Application::getInstance();
        $view = $app->getView();
        $request = $app->getRequest();
        
        if ($widget_slug = $request->getParam('widget')) {
            try {
                $widget_container = WidgetFactory::getWidgetContainer($widget_slug);
                $app->setWidgetContainer($widget_container)->setIsWidget(true);
                
                $view
                    ->prependTemplateDirectory($widget_container->getDefaultTemplateDirectory())
                    ->prependTemplateDirectory($widget_container->getWidgetTemplateDirectory());
                
            } catch (\Exception $e) {
                // Ignore
            }
        }
    }
    
    /**
     * Process request
     *
     * @param string $action
     * @param string $format
     * @param string $filename
     * 
     * @return Response
     */
    public function processRequest($action, $format = 'json', $filename = '')
    {
        $result = array(self::generateError());
        
        switch ($action) {

            case 'available-appointments':
                /**
                 * get the html results embedded in json
                 * @used in frontend
                 * @todo: refactor to use json data in frontend only
                 */
                $result = $this->getAllAvailableAppointments();
                break;

            case 'appointments':
                /**
                 * get the json results
                 * @used for api customers like TechnikerKrankenkasse (TK)
                 * providers parameter format should match api endpoint 'widget-data-export' <lid>-<uid>
                 */
                $result = $this->getAppointments();
                break;

            case 'next-available-appointment':
                $result = $this->getNextAvailableAppointment();
                break;

            case 'get-available-appointments':
                $result = $this->getAvailableAppointments();
                break;

            case 'get-available-appointments-widgets':
                $result = $this->getAvailableAppointmentsForWidgets();
                break;

            case 'get-provider':
                $result = $this->getProvider();
                break;

            case 'get-medical-specialties':
                $result = $this->getMedicalSpecialties();
                break;

            case 'get-insurances':
                $result = $this->getInsurances();
                break;

            case 'get-treatmenttypes':
                $result = $this->getTreatmentTypes();
                break;

            case 'get-doctors':
                $result = $this->getDoctors();
                break;

            case 'get-locations':
                $result = $this->getLocations();
                break;

            case 'search-markers':
                $result = $this->getSearchMarkers();
                break;

            case 'search-doctors':
                $result = $this->searchDoctors();
                break;

            case 'search-doctors-ios-app':
                $result = $this->searchDoctors(true);
                break;

            case 'get-book-appointment-token':
                $result = $this->getBookingToken();
                break;

            case 'book-appointment':
                $result = $this->bookAppointment();
                break;

            case 'search-page':
                $result = $this->searchPage();
                break;

            case 'widget-data-export':
                $result = $this->widgetDataExport($format);
                $filename = 'widget-data-export-'.date('YmdHis').'.'.$format;
                break;

            // @deprecated
            case 'lasik-book-step':
                $result = $this->lasikBooking();
                break;

            case 'missing-action':
            default:
                $result = self::generateError('missing/wrong action');
                break;
        }

        return $this->output($result, $format, $filename);
    }

    /**
     * Export the provider data for the widget partners
     *
     * @param string $format
     *
     * @return array
     */
    public function widgetDataExport($format)
    {
        $app = Application::getInstance();

        if (!$app->isWidget()) {
            return self::generateError('Invalid widget');
        }

        $widgetContainer = $app->getWidgetContainer();

        // get id/pw from BasicAuth
        $credentials = self::getUserPassword();
        if (!$widgetContainer->checkCredentials($credentials['user'], $credentials['password'])) {
            self::sendHeader();
            exit;
        }

        $result = $widgetContainer->getWidget()->getDataExport($widgetContainer->getSlug(), $format);
        if (!empty($result)) {
            return $result;
        }

        return self::generateError('No valid export handler available for this widget.');
    }

    /**
     * Was in class HttpBasicAuth
     * @deprecated
     * 
     * @return array
     */
    private static function getUserPassword()
    {
        if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
            return array('user'     => $_SERVER['PHP_AUTH_USER'],
                         'password' => $_SERVER['PHP_AUTH_PW']
            );
        } else {
            self::sendHeader();
            exit;
        }
    }

    /**
     * Was in class HttpBasicAuth
     * @deprecated
     */
    private static function sendHeader()
    {
        header('WWW-Authenticate: Basic realm="arzttermine.de"');
        header('HTTP/1.0 401 Unauthorized');
        echo "Please enter a valid user name and a password\n";
        exit;
    }

    /**
     * Temporary booking step for the Lasik landing page
     *
     * @deprecated
     *
     * @return array
     */
    public function lasikBooking()
    {
        $form = filter_var_array($_GET['form'], FILTER_SANITIZE_STRING);

        $message = "Good news, we might have a new Lasik client. <br/><br/>" .
                   "First name: {$form['first_name']} <br/>" .
                   "Last name: {$form['last_name']} <br/>" .
                   "Comment: {$form['comment']} <br/>" .
                   "Phone: {$form['phone']} <br/>" .
                   "Email: {$form['email']} <br/>" .
                   "Dioptrin: {$form['dioptrin']} <br/>" .
                   "Date of birth: {$form['date_of_birth']} <br/>";

        $_mailing = new Mailing();
        $_mailing->type = MAILING_TYPE_TEXT;
        $_mailing->subject = "Someone's interested in Lasik treatment";
        $_mailing->body_text = $message;
        $_mailing->addAddressTo('nm@arzttermine.de'); // @todo Change to a support email?
        $_mailing->sendPlain(false);

        return array('success' => true);
    }

    /**
     * Retrieve search results. Also returns HTML
     *
     * @todo Remove and reuse API call where applicable
     *
     * @return array
     */
    protected function searchPage()
    {
        $app = Application::getInstance();
        $request = $app->getRequest();
        $view = $app->getView();

        // @todo Remove this and reuse the SearchController's implementation
        $params = $request->getQuery();

        // Dirty hack to handle form[] in request vars
        // @todo: Refactor/remove
        if (isset($params['form'])) {
            $params = array_merge($params, $params['form']);
        }
        
        $result = Doctor::doSearch($params);
        
        $data = array(
            'html'               => $view->fetch('search/_providers-block.tpl'),
            'date_start'         => $result['dates']['date_start'],
            'date_end'           => $result['dates']['date_end'],
            'date_calendar_days' => $result['dates']['date_calendar_days'],
            'date_days'          => $result['dates']['date_days'],
            'pagination'         => $result['pagination'],
        );

        return $data;
    }

    /**
     * Place a booking with the system
     * Problem: This call is used by the iOS API with GET instead of POST method.
     *
     * @return array
     */
    protected function bookAppointment()
    {
        $app = Application::getInstance();
        $request = $app->getRequest();

        $source_platform = 'api-' . $request->getParam('platform', 'ios');
        $app->setSourcePlatform($source_platform);
        $newsletter = intval($request->getParam('newsletter', 0));

        $_data = array(
            'location_id'             => intval($request->getParam('location_id')),
            'user_id'                 => intval($request->getParam('user_id')),
            'insurance_id'            => intval($request->getParam('insurance_id')),
            'medical_specialty_id'    => intval($request->getParam('medical_specialty_id')),
            'treatmenttype_id'        => intval($request->getParam('treatmenttype_id')),
            'start_at'                => $request->getParam('start_at'),
            'email'                   => filter_var($request->getParam('email'), FILTER_SANITIZE_EMAIL),
            'gender'                  => $request->getParam('gender'),
            'first_name'              => $request->getParam('first_name'),
            'last_name'               => $request->getParam('last_name'),
            'phone'                   => $request->getParam('phone'),
            'booked_language'         => $request->getParam('booked_language'),
            'form_once'               => $request->getParam('book-appointment-token', ''),
            'returning_visitor'       => intval($request->getParam('returning_visitor', 0)),
            'newsletter_subscription' => $newsletter
        );

        if (!$_data['form_once']) {
            return self::generateError('Need book-appointment-token');
        }

        // Check if there is allready a booking with this form_once
        if (Booking::nonceExists($_data['form_once']) || !Booking::nonceIsValid($_data['form_once'])) {
            return self::generateError('Invalid book-appointment-token');
        }

        // In the api we use start_at
        // @todo: need to switch from start_at_id to start_at everywhere in the booking process
        $_data['start_at_id'] = $_data['start_at'] !== 'docdir' ? Calendar::dateTime2UTC($_data['start_at']) : $_data['start_at'];

        // Sanitize and get user and location
        $_data = Booking::sanitizeDataSetup($_data);

        if (empty($_data['errors'])) {
            $_data = Booking::sanitizeDataForm($_data);

            if (empty($_data['errors'])) {
                $_booking = new Booking();
                $_booking = $_booking->create($_data);

                if ($_booking->isValid()) {
                    // Process all integration needed tasks for this booking
                    if ($_booking->integrationBook()) {
                        Booking::sendBookingEmail($_booking);

                        return array(
                            'status'       => '200',
                            'booking_slug' => $_booking->getSlug(),
                            'booking_url'  => $_booking->getUrl(true)
                        );
                    } else {
                        return self::generateError($_booking->getLastErrorMessage());
                    }
                } else {
                    return self::generateError('Booking not created');
                }
            }
        }

        // Something has failed somewhere, so now we're here
        return array('errors' => $_data['errors']);
    }

    /**
     * Get a nonce for the booking
     *
     * @return array
     */
    protected function getBookingToken()
    {
        return array(
            'status'                 => '200',
            'book-appointment-token' => Booking::getNonce()
        );
    }

    /**
     * Search doctors
     *
     * - location
     * - medical_specialty_id
     * - insurance_id
     * - page
     * - per_page
     * - date_start
     *
     * @param bool $is_mobile
     *
     * @return array
     */
    protected function searchDoctors($is_mobile = false)
    {
        $request = Application::getInstance()->getRequest();

        $location = $request->getQuery('location');
        $lat = $request->getQuery('lat');
        $lng = $request->getQuery('lng');
        $insuranceId = $request->getQuery('insurance_id');
        $date_start = $request->getQuery('date_start');
        $date_days = 7;
        $page = $request->getQuery('page', 1);
        $per_page = $request->getQuery('per_page', $GLOBALS['CONFIG']['SEARCH_PAGINATION_NUM']);
        $medicalSpecialtyId = $request->getQuery('medical_specialty_id');

        if ($is_mobile && !$location) {
            $lat = '';
            $lng = '';
        }

        return Doctor::searchDoctorsApi($location, $lat, $lng, $medicalSpecialtyId, $insuranceId, $date_start, $date_days, $page, $per_page);
    }

    /**
     * Search markers
     * This call should return EXACTLY the same results as in the search to
     * make sure that the pin markers and the doctor list are in sync
     * This will return a max results as of CONFIG.SEARCH_PAGINATION_NUM
     *
     * @environment web (redesign)
     * 
     * @deprecated
     *
     * - location
     * - district_id
     * - medical_specialty_id
     * - insurance_id
     *
     * @return array
     */
    protected function getSearchMarkers()
    {
        global $CONFIG;
        
        $request = Application::getInstance()->getRequest();

        // Grab all the query parameters
        $lat = $request->getQuery('lat');
        $lng = $request->getQuery('lng');
        $location = sanitize_location($request->getQuery('location'));
        $distance = intval($request->getQuery('distance'));
        $districtId = intval($request->getQuery('district_id'));
        $insuranceId = intval($request->getQuery('insurance_id'));
        $treatmentTypeId = intval($request->getQuery('treatmenttype_id'));
        $medicalSpecialtyId = intval($request->getQuery('medical_specialty_id'));

        if (!$location && (!is_numeric($lat) || !is_numeric($lng))) {
            return array('error' => 'Missing location, lat and lng');
        }

        if ($districtId && !is_numeric($districtId)) {
            return array('error' => 'Invalid district ID');
        }

        if (!MedicalSpecialty::idExists($medicalSpecialtyId)) {
            return array('error' => 'Invalid medical specialty');
        }

        if ($treatmentTypeId && !TreatmentType::idExists($treatmentTypeId)) {
            return array('error' => 'Invalid treatment type');
        }

        if (!Insurance::idExists($insuranceId)) {
            return array('error' => 'Invalid insurance');
        }
                
        // The search can be an index or a number
        // This is to try to help with backwards compatibility but is NOT good
        // @todo Remove indexed distances
        if (!empty($CONFIG['SEARCH_DISTANCES_KM'][$distance])) {
            $distance = $CONFIG['SEARCH_DISTANCES_KM'][$distance];
        } elseif (empty($distance)) {
            $distance = $CONFIG['SEARCH_DISTANCES_KM_DEFAULT'];
        }
        
        $form = array(
            'lat'                  => $lat,
            'lng'                  => $lng,
            'location'             => $location,
            'distance'             => $distance,
            'district_id'          => $districtId,
            'insurance_id'         => $insuranceId,
            'treatmenttype_id'     => $treatmentTypeId,
            'medical_specialty_id' => $medicalSpecialtyId
        );
        
        // Strip keys
        $valid_locations = array_values(Doctor::getValidLocations($form));
        if (!$valid_locations) {
            return array('error' => 'Wir konnten den von Ihnen gesuchten Ort nicht finden. Bitte überprüfen Sie Ihre Eingabe.');
        }

        // Only accept the first match
        $form = array_merge($form, $valid_locations[0]);
        
        // if we have a formatted address than show that one in the form
        if (!empty($form['formatted_address_short'])) {
            $form['location'] = $form['formatted_address_short'];
        }
        
        //$city = Doctor::getSearchResultsFirstDoctorCity($form);
        $result = Doctor::getSearchResults(
            $form,
            1,
            $CONFIG['SEARCH_MAX_RESULTS'], // We want all pins
            $location
        );

        if(!isset($result['providers'])) {
            return array('markers'=>array());
        }

        $map = new GoogleMap();
        Doctor::setGooglemapMarkers(
              $map,
              $result['providers'],
              $result['filter'],
              $result['providers_start_index'],
              $result['providers_end_index']
        );

        return array('markers' => $map->getMarkers('array'));
    }

    /**
     * Basic location fetch
     * 
     * @deprecated
     *
     * @return array
     */
    protected function getLocations()
    {
        echo "Can't export all locations. Ask Sysadmin.";
        exit;

        $practices = array();
        $location = new Location();
        $locations = $location->findObjects('status !=' . Location::STATUS_DRAFT);

        if ($locations) {
            /** @var Location[] $locations */
            foreach ($locations as $_location) {
                if ($location instanceof Location && $_location->isValid()) {
                    $data = $_location->getDatas(
                                      array(
                                          'id',
                                          'name',
                                          'street',
                                          'city',
                                          'zip',
                                          'lat',
                                          'lng',
                                          'phone_visible',
                                          'fax_visible',
                                          'member_ids',
                                          'medical_specialty_ids',
                                          'info_1'
                                      )
                    );
                    $data['url'] = $_location->getUrl('url', true);

                    // Add prefix 'location_' to all keys
                    $data = array_combine(
                        array_map(
                            function ($k) {
                                return 'location_' . $k;
                            }, array_keys($data)
                        ),
                        $data
                    );

                    $practices[] = $data;
                }
            }
        }

        return $practices;
    }

    /**
     * Basic doctors fetch
     * @deprecated
     *
     * @return array
     */
    protected function getDoctors()
    {
        echo "Can't export all doctors. Ask Sysadmin.";
        exit;

        $doctors = array();
        $user = new User();
        $users = $user->findObjects('status !=' . User::STATUS_DRAFT . ' AND group_id=' . GROUP_DOCTOR);

        if ($users) {
            /** @var User[] $users */
            foreach ($users as $_user) {
                if ($_user instanceof User && $_user->isValid()) {
                    $data = $_user->getDatas(
                                  array(
                                      'id',
                                      'gender',
                                      'title',
                                      'first_name',
                                      'last_name',
                                      'medical_specialty_ids',
                                      'docinfo_1',
                                      'docinfo_2',
                                      'docinfo_3',
                                      'docinfo_4',
                                      'docinfo_5',
                                      'docinfo_6'
                                  )
                    );
                    $data['url'] = $_user->getUrl('url', true);

                    // Add prefix 'user_' to all keys
                    $data = array_combine(
                        array_map(
                            function ($k) {
                                return 'user_' . $k;
                            }, array_keys($data)
                        ),
                        $data
                    );

                    $doctors[] = $data;
                }
            }
        }

        return $doctors;
    }

    /**
     * Get treatment types. Also supports Samedi types if applicable
     *
     * @return array
     */
    protected function getTreatmentTypes()
    {
        $request = Application::getInstance()->getRequest();

        $user_id = intval($request->getQuery('user_id'));
        $location_id = intval($request->getQuery('location_id'));
        $specialty_id = intval($request->getQuery('specialty_id'));
        $starts_at = str_replace(',', ' ', $request->getQuery('datetime_start'));
        $location = new Location($location_id, 'id');
        $integration_id = intval($location->getIntegrationId());

        // @todo: Refactor this into the integration
        if ($integration_id == $GLOBALS['CONFIG']['INTEGRATIONS'][5]['ID']) {
            $samediTTNamesAndInsurances = TreatmentType::getTreatmentTypesForGivenTime($user_id, $location_id, $starts_at);
            $samediTT = $samediTTNamesAndInsurances['tt_names'];
            $i = -1;
            foreach ($samediTT as $val) {
                $i++;
                $output[$i] = array(
                    'name_de'              => $val,
                    'medical_specialty_id' => $specialty_id,
                    'id'                   => key($samediTT)
                );
                next($samediTT);
            }
            $output[++$i] = array(
                'name_de'              => 'samedi',
                'medical_specialty_id' => 'samedi',
                'id'                   => 'samedi'
            );

            return $output;
        } else {
            $_treatment = new TreatmentType();

            return $_treatment->getAllData(array('id', 'name_de', 'medical_specialty_id'), 'status=' . TreatmentType::STATUS_ACTIVE . ' ORDER BY name_de ASC');
        }
    }

    /**
     * Get all insurances
     *
     * @return array
     */
    protected function getInsurances()
    {
        $insurance = new Insurance();
        $insurances = array();

        // We need to reindex the names as the index changed from name to name_de after i18n translations
        foreach (
            $insurance->getAllData(
                      array(
                          'id',
                          'name_de'
                      ), 'status = ' . Insurance::STATUS_ACTIVE . ' ORDER BY name_de ASC'
            ) as $_insurance
        ) {
            $insurances[] = array(
                'id'   => $_insurance['id'],
                'name' => $_insurance['name_de']
            );
        }

        return $insurances;
    }

    /**
     * Get all medical specialties
     *
     * @return array
     */
    protected function getMedicalSpecialties()
    {
        $medical_specialty = new MedicalSpecialty();
        $medical_specialties = array();

        // We need to reindex the names as the index changed from name to name_singular_de after i18n translations
        foreach (
            $medical_specialty->getAllData(
                              array(
                                  'id',
                                  'name_singular_de'
                              ), 'status = ' . MedicalSpecialty::STATUS_ACTIVE . ' ORDER BY name_singular_de ASC'
            ) as $_medical_specialty
        ) {
            $medical_specialties[] = array(
                'id'   => $_medical_specialty['id'],
                'name' => $_medical_specialty['name_singular_de']
            );
        }

        return $medical_specialties;
    }

    /**
     * Return available appointments for a number of doctors/locations
     * If there is any problem with the given data then this method will return
     * with an error message to avoid unvalid results
     *
     * @environment web (redesign)
     *
     * - date_start
     * - date_end
     * - array providers (userId|locationId,userId|locationId,...)
     * - insurance_id
     * - first_week
     *
     * @return array
     */
    protected function getAllAvailableAppointments()
    {
        $app = Application::getInstance();
        $request = $app->getRequest();
        $view = $app->getView();

        $payload = array();

        $date_start = new DateTime($request->getQuery('date_start'));
        $date_end   = new DateTime($request->getQuery('date_end'));
        if (!Calendar::isValidDate($date_start)) {
            return self::generateError('Invalid date_start');
        }

        // @todo: We are for some reason comparing strings here. Needs fixing
        if (!($date_start >= Calendar::getFirstPossibleDateTimeForSearch() && $date_start <= Calendar::getLastPossibleDateTimeForSearch())) {
            return self::generateError('date_start is not in valid range');
        }
        
        if (!($date_end >= $date_start && $date_end <= Calendar::getLastPossibleDateTimeForSearch())) {
            return self::generateError('date_end is not in valid range');
        }

        // A dirty array
        $provider_list = $request->getQuery('providers', array()); // In the form of <uid>|<lid> ROLLEYES
        
        // Master search
        $search = new Search();
        
        // Search settings
        $insurance         = new Insurance(intval($request->getQuery('insurance_id')));
        $medical_specialty = new MedicalSpecialty(intval($request->getQuery('medical_specialty_id')));
        $treatment_type    = new TreatmentType(intval($request->getQuery('treatmenttype_id')));

        $search
            ->setDateBounds($date_start, $date_end)
            ->setInsurance($insurance)
            ->setMedicalSpecialty($medical_specialty)
            ->setTreatmentType($treatment_type);
        
        $providers = array();
        
        foreach (explode(',', $provider_list) as $provider_ids) {
            list($user_id, $location_id) = explode('|', $provider_ids);

            $user = new User(intval($user_id));
            $location = new Location(intval($location_id));

            if (!$user->isValid() || !$location->isValid()) {
                return self::generateError('Invalid provider');
            }

            if (!$location->getIntegration()->isConnected($user->getId(), $location->getId())) {
                return self::generateError('User or location is invalid');
            }

            $provider = new Provider();
            $provider
                ->setDateRange($date_start, $date_end)
                ->setInsurance($insurance)
                ->setMedicalSpecialty($medical_specialty)
                ->setTreatmentType($treatment_type)
                ->setUser($user)
                ->setLocation($location);
            
            $providers[] = $provider;
            unset($provider);
        }

        $appointments = $search->findAppointments($providers);

        /** @var Provider $provider */
        foreach ($providers as $provider) {
            // find/set appointments from a nested search
            $provider->findAppointments($appointments);
            $view->setRef('appointmentsBlock', $provider->getAppointmentsBlock());
            $html = $view->fetch('appointment/_appointments-block.tpl');
            // set the index in the frontend format <uid>|<lid>
            $payload['providers'][$provider->getUserId() . '|' . $provider->getLocationId()] = array('html' => $html);
        }

        /**
         * The number of returned days
         * Can be less than calendar days as some days could have been
         * removed like the weekends or holidays.
         * Should always match the number of days returned by Provider::getAppointmentsBlock()
         */
        $payload['date_days'] = count(Calendar::getDatesInAreaWithFilter($date_start, $date_end));

        /*
         * The real number of days in the calendar including the start day
         */
        $date_calendar_days = Calendar::getDiff($date_start, $date_end, '%a');

        // We need the days involved, so add one
        $date_calendar_days++;

        $payload['date_calendar_days'] = intval($date_calendar_days);
        $payload['date_start'] = $date_start->getDate();
        $payload['date_end'] = $date_end->getDate();

        return $payload;
    }

    /**
     * Basically the same code as getAllAvailableAppointments() but with
     * different parameter provider format (here <lid>-<uid>. Should not be mixed or
     * recycled because it's already a mess...
     * @todo: need to refactor the whole frontend / api appointment usage
     * and also the provider format
     *
     * Return available appointments for a number of doctors/locations
     * If there is any problem with the given data then this method will return
     * with an error message to avoid unvalid results
     *
     * @environment api
     *
     * - date_start
     * - date_end
     * - insurance_id
     * - string provider (locationId-userId)
     *
     * @return array
     */
    protected function getAppointments()
    {
        $app = Application::getInstance();
        $request = $app->getRequest();
        $view = $app->getView();

        $payload = array();

        $date_start = new DateTime($request->getQuery('date_start'));
        $date_end   = new DateTime($request->getQuery('date_end'));
        if (!Calendar::isValidDate($date_start)) {
            return self::generateError('Invalid date_start');
        }

        // @todo: We are for some reason comparing strings here. Needs fixing
        if (!($date_start >= Calendar::getFirstPossibleDateTimeForSearch() && $date_start <= Calendar::getLastPossibleDateTimeForSearch())) {
            return self::generateError('date_start is not in valid range');
        }

        if (!($date_end >= $date_start && $date_end <= Calendar::getLastPossibleDateTimeForSearch())) {
            return self::generateError('date_end is not in valid range');
        }

        // A dirty array
        $provider_list = $request->getQuery('providers', array()); // In the form of <uid>|<lid> ROLLEYES

        // Master search
        $search = new Search();

        // Search settings
        $insurance         = new Insurance(intval($request->getQuery('insurance_id')));
        $medical_specialty = new MedicalSpecialty(intval($request->getQuery('medical_specialty_id')));
        $treatment_type    = new TreatmentType(intval($request->getQuery('treatmenttype_id')));

        $search
            ->setDateBounds($date_start, $date_end)
            ->setInsurance($insurance)
            ->setMedicalSpecialty($medical_specialty)
            ->setTreatmentType($treatment_type);

        $providers = array();

        foreach (explode(',', $provider_list) as $provider_ids) {
            // provider come from api 'widget-data-export' in the format <lid>-<uid>
            list($location_id, $user_id) = explode('-', $provider_ids);

            $user = new User(intval($user_id));
            $location = new Location(intval($location_id));

            if (!$user->isValid() || !$location->isValid()) {
                return self::generateError('Invalid provider');
            }

            if (!$location->getIntegration()->isConnected($user->getId(), $location->getId())) {
                return self::generateError('User or location is invalid');
            }

            $provider = new Provider();
            $provider
                ->setDateRange($date_start, $date_end)
                ->setInsurance($insurance)
                ->setMedicalSpecialty($medical_specialty)
                ->setTreatmentType($treatment_type)
                ->setUser($user)
                ->setLocation($location);

            $providers[] = $provider;
            unset($provider);
        }

        $appointmentsFromSearch = $search->findAppointments($providers);

        $appointments = array();
        /** @var Provider $provider */
        foreach ($providers as $provider) {
            // find/set appointments from a nested search
            $provider->findAppointments($appointmentsFromSearch);

            // Sort the hits we have and just take the data we need
            foreach ($provider->getAppointments() as $appointment) {
                $date = $appointment->getDate()->getDate();
                // Filter out non working days
                // @todo: move this filter to the search so that we handle with valid appointments here only
                if (Calendar::isWorkingDay($date)) {
                    $appointments[] = array(
                        'provider' => $provider->getId(),
                        'url' => $appointment->getUrl($provider),
                        'iso8601' => $appointment->getFormat('c'),
                        'insurance_id' => $provider->getInsuranceId()
                    );
                }
            }
        }

        return $appointments;
    }

    /**
     * Return available appointments for a number of doctors/locations
     * If there is any problem with the given data then this method will return
     * with an error message to avoid unvalid results
     *
     * @environment web (redesign)
     *
     * - date_start
     * - date_end
     * - user_id
     * - location_id
     * - insurance_id
     *
     * @return array
     */
    protected function getNextAvailableAppointment()
    {
        $app = Application::getInstance();
        $request = $app->getRequest();

        $date_start = $request->getQuery('date_start');
        if (!Calendar::isValidDate($date_start)) {
            return self::generateError('Invalid date_start');
        }

        // @todo: We are for some reason comparing strings here. Needs fixing
        if (!($date_start >= Calendar::getFirstPossibleDateTimeForSearch() && $date_start <= Calendar::getLastPossibleDateTimeForSearch())) {
            return self::generateError('date_start is not in valid range');
        }

        $date_end = $request->getQuery('date_end');
        if (!Calendar::isValidDate($date_end)) {
            return self::generateError('Invalid date_end');
        }

        // @todo: We are for some reason comparing strings here. Needs fixing
        if (!($date_end >= $date_start && $date_end <= Calendar::getLastPossibleDateTimeForSearch())) {
            return self::generateError('date_end is not in valid range');
        }

        $firstWeek  = 0;

        $insuranceId = intval($request->getQuery('insurance_id'));
        $medicalSpecialtyId = intval($request->getQuery('medical_specialty_id'));
        $treatmentTypeId = intval($request->getQuery('treatmenttype_id'));

        if (!MedicalSpecialty::idExists($medicalSpecialtyId)) {
            $medicalSpecialtyId = null;
        }

        if (!TreatmentType::idExists($treatmentTypeId)) {
            $treatmentTypeId = null;
        }

        /**
         * Generate an error and stop processing for any non valid situation here!
         * The order of results should match the order of given providers
         */
        $searchResultCacheTmpManager = SearchResultCacheTmpManager::getInstance(null, null);

        $userId = intval($request->getQuery('user_id'));
        $locationId = intval($request->getQuery('location_id'));

        if (!is_numeric($userId) || !is_numeric($locationId)) {
            return self::generateError('Invalid provider');
        }

        $location = new Location($locationId);
        if (!$location->isValid()) {
            return self::generateError('Location is invalid');
        }

        $user = new User($userId);
        if (!$user->isValid()) {
            return self::generateError('User is invalid');
        }

        $integration = $location->getIntegration();
        if (!$integration || !$integration->isConnected($userId, $locationId)) {
            return self::generateError('User or location is invalid');
        }

        $provider = new ProviderRow();
        $provider
            ->setUser($user)
            ->setLocation($location)
            ->setMedicalSpecialtyId($medicalSpecialtyId)
            ->setTreatmentTypeId($treatmentTypeId);

        // oh boy..
        // @todo: refactor this shitty getProviderRowsByFilters()
        // right now it needs to pass an array, so here we go
        $providers = array($provider);
        // fill the providers with appointments...
        // EVEN THAT THE IDE TELLS YOU THAT $providers IS NOT USED ANYMORE DONT! REMOVE THIS BLOCK!
        // ITS NGS MAGIC...
        $providers = $searchResultCacheTmpManager->getProviderRowsByFilters(
            $providers,
            $date_start,
            $date_end,
            $insuranceId,
            $treatmentTypeId,
            $firstWeek
        );
        $appointment = $provider->getNextAvailableAppointment();
        return $appointment->getDateTime();
    }

    /**
     * Return available appointments for A SINGLE doctor/location
     *
     * @environment mobile, ios app
     *
     * - date_start
     * - datetime_start
     * - date_end
     * - datetime_end
     * - user_id
     * - location_id
     * - medical_specialty_id
     * - insurance_id
     * - first_week
     *
     * @return array
     */
    protected function getAvailableAppointments()
    {
        $app = Application::getInstance();
        $request = $app->getRequest();

        $date_start = $request->getQuery('date_start');
        $datetime_start = $request->getQuery('datetime_start');
        if (!date_create_from_format('Y-m-d H:i:s', $datetime_start)) {
            if (date_create_from_format('Y-m-d', $date_start)) {
                $datetime_start = $date_start;
            } else {
                return self::generateError('Invalid datetime_start');
            }
        }

        // if the datetime_start is today then treat it like its valid but overwrite with tomorrow
        if (date('Y-m-d', strtotime($datetime_start)) == date('Y-m-d')) {
            $datetime_start = Calendar::getFirstPossibleDateTimeForSearch();
        }

        if (!($datetime_start >= Calendar::getFirstPossibleDateTimeForSearch() && $datetime_start <= Calendar::getLastPossibleDateTimeForSearch())) {
            return self::generateError('datetime_start is not in valid range');
        }

        $date_end = $request->getQuery('date_end');
        $datetime_end = $request->getQuery('datetime_end');
        if (!date_create_from_format('Y-m-d H:i:s', $datetime_end)) {
            if (date_create_from_format('Y-m-d', $date_end)) {
                $datetime_end = $date_end;
            } else {
                return self::generateError('Invalid datetime_end');
            }
        }

        if (!($datetime_end >= $datetime_start && $datetime_end <= Calendar::getLastPossibleDateTimeForSearch())) {
            return self::generateError('datetime_end is not in valid range');
        }

        $firstWeek = $request->getQuery('first_week', 0);

        if ($firstWeek != 1) {
            $firstWeek = 0;
        }

        $user_id = intval($request->getQuery('user_id'));
        $location_id = intval($request->getQuery('location_id'));
        $insurance_id = intval($request->getQuery('insurance_id'));

        $location = new Location($location_id);
        $integration = $location->getIntegration();

        if ($user_id <= 0 || $location_id <= 0 || !$integration || !$integration->isConnected($user_id, $location_id)) {
            return self::generateError('user_id or location_id is invalid');
        }

        $user = new User($user_id);

        if ($location->hasAppointmentEnquiryIntegration()) {
            if ($user->getStatus() == User::STATUS_VISIBLE) {
                $_data = $app->_("Keine freien Termine verfügbar");
            } else {
                /*
                 * Don't change that string as the ios app fails to load the doctors page
                 * as this api call is used there
                 */
                $_data = 'docdir';
            }

            return $_data;
        }

        $customMessage = ProviderRow::mobileDoctorIsCustomMessage($user_id, $insurance_id);

        if ($customMessage !== false) {
            return array('message' => '<div class="normal-text" style="padding:6px">' . $app->_($customMessage) . '</div>');
        }

        $searchResultCacheTmpManager = SearchResultCacheTmpManager::getInstance();
        $appointments = $searchResultCacheTmpManager->getAvailableAppointments($user, $location, $datetime_start, $datetime_end, $insurance_id, null, $firstWeek, true, 'datetime');

        return $appointments;
    }

    /**
     * Return available appointments for A SINGLE doctor/location
     *
     * @environment widget
     *
     * - date_start
     * - date_end
     * - user_ids
     * - location_ids
     * - medical_specialty_id
     * - insurance_id
     *
     * @return array
     */
    protected function getAvailableAppointmentsForWidgets()
    {
        $app = Application::getInstance();
        $request = $app->getRequest();
        $view = $app->getView();

        if ($widget_slug = $request->getParam('widget')) {
            try {
                $widget = WidgetFactory::getWidgetContainer($widget_slug);
                $app->setWidgetContainer($widget)->setIsWidget(true);
            } catch (\Exception $e) {
                return self::generateError('Wrong widget slug');
            }
        }
        $view->setRef('widget_slug', $widget_slug);

        $start = new DateTime($request->getParam('date_start'));
        $date_days =  intval($request->getParam('date_days'));
        $index = intval($request->getParam('index', 0));

        if ($index > 0 && (($index * $date_days) < $GLOBALS['CONFIG']['CALENDAR_MAX_DAYS_AHEAD'])) {
            $_add_days = $index * $date_days;
            /**
             * correct usage:
             * $start->addWeekDays($_add_days - 1 + 1)
             */
            $start->addWeekDays($_add_days);
            $end = clone $start;
            $end->addWeekDays($date_days - 1);
        } else {
            $end = clone $start;
            $end->addWeekDays($date_days - 1);
        }

        $search = new Search;
        $search->setStart($start);
        $search->setEnd($end);
        $search->setDateBounds($start, $end);

        $ret = array();
        /**
         * 1. Add the Calendar HTML
         */
        $view->setRef('search', $search);
        $view->setRef('date_days', $date_days);
        $ret[] = Calendar::getCalendarDaysHtml($start, $date_days);

        /**
         * 2. Add the Appointments HTML
         */
        // Replace the slug with a real insurance object
        $insurance = new Insurance();
        if (!empty($insurance_slug)) {
            $insurance->load($insurance_slug, 'slug');
        }
        if (!$insurance->isValid()) {
            $insurance->load(Insurance::GKV);
        }
        // tell the search which insurance we have selected
        $search->setInsurance($insurance);

        // Appointment provider objects
        $providers = array();

        $userIds = $request->getParam('user_ids');
        $locationIds = $request->getParam('location_ids');
        // Get some basic provider objects so we can fetch appointments with them
        /** @var Location[] $locations */
        foreach ($userIds as $parameterIndex => $userId) {
            $user = new User($userId);
            if (!$user->isValid()) continue;
            $location = new Location($locationIds[$parameterIndex]);
            if (!$location->isValid()) continue;

            $provider = new Provider();
            $providers[] = $provider
                ->setDateRange($search->getStart(), $search->getEnd())
                ->setUser($user)
                ->setLocation($location)
                ->setInsurance($insurance)
                ->setMedicalSpecialty($user->getDefaultMedicalSpecialty());
        }

        $event = $app->container->get('dispatcher')->dispatch('search.before_search', new Event($search));
        /** @var Search $search */
        $search = $event->data;

        $appointments = $search->findAppointments($providers);
        
        /** @var Provider $provider */
        foreach ($providers as $provider) {
            $provider->findAppointments($appointments);
            $view->setRef('appointmentsBlock', $provider->getAppointmentsBlock($widget_slug));

            $ret[] = $view->fetch('appointment/_appointments-block.tpl');
        }

        /**
         * 3. Add the prev button status
         */
        if ($index > 0){
            //for prev button enable
            $ret[] = '1';
        } else {
            //for prev button disable
            $ret[] = '0';
        }
        /**
         * 4. Add the prev button status
         */
        if (($index+1) * $date_days < intval($GLOBALS['CONFIG']['CALENDAR_MAX_DAYS_AHEAD'])) {
            //for next button enable
            $ret[] = '1';
        } else {
            //for next button disable
            $ret[] = '0';
        }
        /**
         * 5. Add the index
         */
        $ret[] = $index;

        return $ret;
    }

    /**
     * Helper function to generate consistent error messages
     *
     * @param $message
     *
     * @return array
     */
    public static function generateError($message = 'An unknown error has occurred')
    {
        return array('error' => 'Error: ' . $message);
    }

    /**
     * Send data to the client
     *
     * @param mixed $data
     * @param string $format
     * @param string $filename
     * 
     * @return Response
     */
    public function output($data, $format, $filename = '')
    {
        switch ($format) {
            case 'print_r':
                $html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head><body><pre>';
                $html .= print_r($data, true);
                $html .= '</pre></body></html>';
                return new Response($html);
                break;
            case 'json_debug':
                $html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head><body>';
                $html .= str_replace('{"id"', '<br />{"id"', json_encode($data));
                $html .= '</body></html>';
                return new Response($html);
                break;
            case 'xml':
                $headers = array('Content-Type' => 'application/xml');
                return new Response($data, Response::HTTP_OK, $headers);
                break;
            case 'error':
                throw new NotFoundHttpException;
                break;
            case 'html':
                return new Response($data);
                break;
            case 'csv':
                $headers = array('Content-Type' => 'text/csv');
                if (!empty($filename)) {
                    $headers['Content-disposition'] = 'attachment;filename=' . $filename;
                }
                return new Response($data, Response::HTTP_OK, $headers);
                break;
            case 'json':
            default:
                return new JsonResponse($data);
        }
    }

    /**
     * Return the doctor data in combination with their locations
     *
     * @return array
     */
    public static function getProvider()
    {
        $request = Application::getInstance()->getRequest();

        $user_id = intval($request->getQuery('user_id'));
        $location_id = intval($request->getQuery('location_id'));

        $user = new User($user_id, 'id');
        $location = new Location($location_id, 'id');
        $integration = $location->getIntegration();

        if (!$user->isValid() || !$user->isMemberOf(GROUP_DOCTOR) || !$location->isValid() || !$integration) {
            return self::generateError('Invalid user_id or location_id');
        } elseif (!$integration->isConnected($user_id, $location_id)) {
            return self::generateError('Integration not valid');
        }

        // Provider data
        $data = array(
            'user_id'                           => $user->getId(),
            'location_id'                       => $location->getId(),
            'user_name'                         => $user->getName(),
            'user_profile_image'                => array(
                'sizes' => array(
                    'web-list'    => $user->getProfileAssetUrl(ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED, true),
                    'web-profile' => $user->getProfileAssetUrl(ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED, true),
                    'ios-list'    => $user->getProfileAssetUrl(ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED, true),
                    'ios-profile' => $user->getProfileAssetUrl(ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED, true),
                )
            ),
            'user_medical_specialties_text'     => $user->getMedicalSpecialityIdsText(', '),
            'location_name'                     => $location->getName(),
            'location_street'                   => $location->getStreet(),
            'location_city'                     => $location->getCity(),
            'location_zip'                      => $location->getZip(),
            'location_phone'                    => $location->getPhoneVisible(),
            'location_info_1'                   => $location->getInfoText('info_1'),
            'location_profile_image'            => array(
                'sizes' => array(
                    'web-list'    => $location->getProfileAssetUrl(ASSET_GFX_SIZE_MEDIUM_LANDSCAPE_CROPPED, true),
                    'web-profile' => $location->getProfileAssetUrl(ASSET_GFX_SIZE_MEDIUM_LANDSCAPE_CROPPED, true),
                    'ios-list'    => $location->getProfileAssetUrl(ASSET_GFX_SIZE_MEDIUM_LANDSCAPE_CROPPED, true),
                    'ios-profile' => $location->getProfileAssetUrl(ASSET_GFX_SIZE_MEDIUM_LANDSCAPE_CROPPED, true),
                )
            ),
            'location_medical_specialties_text' => $location->getMedicalSpecialityIdsText(', '),
            'user_info_1'                       => $user->getInfoText('docinfo_1'),
            'user_info_2'                       => $user->getInfoText('docinfo_2'),
            'user_info_3'                       => $user->getInfoText('docinfo_3'),
            'user_info_4'                       => $user->getInfoText('docinfo_4'),
            'user_info_5'                       => $user->getInfoText('docinfo_5'),
            'user_info_6'                       => $user->getInfoText('docinfo_6'),
            'user_rating_average'               => $user->getRating('rating_average'),
        );

        return (array(
            'status' => '200',
            'result' => $data
        ));
    }
}
