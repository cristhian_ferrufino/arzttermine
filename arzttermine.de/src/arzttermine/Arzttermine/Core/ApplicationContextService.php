<?php

namespace Arzttermine\Core;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Detects and manages device context (type, orientation, mode, etc)
 *
 * @package Arzttermine\Core
 */
class ApplicationContextService {

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * Contains context flags
     * 
     * @var int
     */
    private $context = 0;

    /**
     * @var bool
     */
    protected $context_established = false;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Get as much information about the context as we can and load it
     * This only happens once, as a change in context requires re-initialization
     * 
     * @return self
     */
    public function determineContext()
    {
        // Only check once
        if ($this->context_established) {
            return;
        }

        $this->context += ContextType::STANDARD;
        
        if (php_sapi_name() === 'cli') {
            $this->context += ContextType::CLI;
        }

        if ($this->container->has('request')) {
            /** @var \Arzttermine\Http\Request $request */
            $request = $this->container->get('request');
            
            if (strpos($request->getRequestUri(), '/api') !== 0) {
                $this->context += ContextType::API;
            }

            // @todo: Change this to search for a widget container
            if (strpos($request->getRequestUri(), '/w/') !== 0) {
                $this->context += ContextType::WIDGET;
            }
        }

        $this->context_established = true;
    }

    /**
     * Check if the ApplicationContext is <something>
     * 
     * @param int $context_flag
     *
     * @return bool
     */
    public function is($context_flag)
    {
        return (bool)($this->context & $context_flag);
    }

    /**
     * Helper methods to determine context
     */
    
    /**
     * @return bool
     */
    public function isMobile()
    {
        return $this->is(ContextType::MOBILE);
    }

    /**
     * @return bool
     */
    public function isDesktop()
    {
        return $this->is(ContextType::STANDARD);
    }

    /**
     * @return bool
     */
    public function isCLI()
    {
        return $this->is(ContextType::CLI);
    }

    /**
     * @return bool
     */
    public function isWidget()
    {
        return $this->is(ContextType::WIDGET);
    }

    /**
     * @return bool
     */
    public function isAPI()
    {
        return $this->is(ContextType::API);
    }
}
