<?php

namespace Arzttermine\Widget\Widget;

use Arzttermine\Application\Application;
use Arzttermine\Event\Event;
use Arzttermine\Location\Location;
use Arzttermine\Search\Search;
use Arzttermine\User\User;
use Arzttermine\Widget\Exception;
use Arzttermine\Widget\WidgetInterface;

class UniversalWidget implements WidgetInterface {

    public $display_dates = 5;

    /**
     * Widget display for locations/practices. Requires location ID
     *
     * @param array $parameters
     * @return \Symfony\Component\HttpFoundation\Response | \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function practiceAction($parameters = array())
    {
        $app = Application::getInstance();
        $router = $app->container->get('router');

        // Default to public
        // @todo: Something more generic
        $insurance_slug = isset($parameters[0]) ? $parameters[0] : 'gkv';
        if ($insurance_slug != 'gkv' && $insurance_slug != 'pkv') {
            $insurance_slug = 'gkv';
        }

        $location = new Location($app->getRequest()->getQuery('l'));

        if ($location->isValid() && $location->hasProfile() && $location->getStatus() != Location::STATUS_DRAFT) {
            // Load location profile
            return $app->forward($router->generate('practice', array('slug' => $location->getSlug(), 'insurance' => $insurance_slug)));
        } else {
            return $app->redirect($router->generate('not_found'));
        }
    }

    /**
     * Widget display for bookings and booking profiles. Profile requires slug
     *
     * @param array $parameters
     * @param string $action
     * @return \Symfony\Component\HttpFoundation\Response | \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function bookingAction($parameters = array(), $action = 'termin')
    {
        $app = Application::getInstance();
        $router = $app->container->get('router');

        $action = strtolower($action);

        // @todo: Less kludgey/hacky way
        if ($action == 'termin' || $action == 'appointment') {
            return $app->forward($router->generate('appointment'));
        } elseif ($action == 'buchung' || $action == 'booking') {
            return $app->forward($router->generate('booking_profile', array('slug' => $parameters[0])));
        } else {
            return $app->redirect($router->generate('not_found'));
        }
    }

    /**
     * Widget display for doctors
     *
     * @param array $parameters
     * @return \Symfony\Component\HttpFoundation\Response | \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Arzttermine\Widget\Exception
     */
    public function doctorAction($parameters = array())
    {
        $app = Application::getInstance();
        $request = $app->getRequest();
        $router = $app->container->get('router');

        $insurance_slug = isset($parameters[0]) ? $parameters[0] : 'gkv';
        if ($insurance_slug != 'gkv' && $insurance_slug != 'pkv') {
            $insurance_slug = 'gkv';
        }

        $user_id = $request->getQuery('u');
        $user = new User($user_id);

        if ($user->isValid() && $user->hasProfile()) {
            if ($user->getStatus() == User::STATUS_VISIBLE || $user->getStatus() == User::STATUS_VISIBLE_APPOINTMENTS) {
                if ($user->isMemberOf(GROUP_DOCTOR)) {
                    return $app->forward($router->generate('doctor_profile', array('slug' => $user->getSlug(), 'insurance' => $insurance_slug)));
                } else {
                    throw new Exception('Wrong group_id [' . $user->getGroupId() . ']');
                }
            } else {
                throw new Exception('Wrong status [' . $user->getStatus() . ']');
            }
        } else {
            throw new Exception('Invalid User [' . $user_id . ']');
        }
    }

    /**
     * Default widget display
     */
    public function indexAction()
    {
        return Application::getInstance()->container->get('templating')->display('index.tpl');
    }

    /**
     * @inheritdoc
     */
    public function run($action = 'index', array $parameters = array())
    {
        // Limit the number of days in the query
        Application::getInstance()->container->get('dispatcher')->addListener('search.before_search', function(Event $event) {
            /** @var Search $search */
            $search = $event->data;
            
            $start_date = $search->getStart();
            $end_date = clone $start_date;

            // Only display display_dates days at a time.
            // here we subtract 1 day as the end date is inclusive.
            $end_date->modify('+' . intval($this->display_dates - 1) . ' weekdays');
            $search->setDateBounds($start_date, $end_date);
                
            $event->data = $search;
            return $event;
        });

        switch ($action) {
            case 'arzt':
            case 'doctor':
                return $this->doctorAction($parameters);
                break;
            case 'praxis':
            case 'practice':
                return $this->practiceAction($parameters);
                break;
            case 'termin':
            case 'appointment':
            case 'buchung':
            case 'booking':
                return $this->bookingAction($parameters, $action);
                break;
            default:
                return $this->indexAction();
        }
    }

    /**
     * @inheritdoc
     */
    public function getDataExport($widgetSlug, $format)
    {
        $db = Application::getInstance()->getSql();

        $sql
            = '
			SELECT
				u.id AS user_id, l.id AS location_id
			FROM ' . DB_TABLENAME_USERS . ' u, ' . DB_TABLENAME_LOCATIONS . ' l, ' . DB_TABLENAME_LOCATION_USER_CONNECTIONS . ' luc
		 	WHERE
				luc.user_id = u.id
			AND
				luc.location_id = l.id
			AND
				u.status = ' . USER_STATUS_VISIBLE_APPOINTMENTS . '
			AND
				l.status = ' . LOCATION_STATUS_VISIBLE_APPOINTMENTS . '
			AND
				u.has_contract=1
			AND
                l.country_code = "DE"
			ORDER BY l.id ASC';

        $db->query($sql);
        $records = array();
        if ($db->nf() > 0) {
            while ($db->next_record()) {
                $records[] = $db->Record;
            }
        }

        if (empty($records)) {
            $data = 'ERROR: No available records';
        } else {
            $data = '"ProviderId";"WidgetUrl";"LocationName";"LocationStreet";"LocationStreetWONumber";"LocationStreetNumber";"LocationStreetExtension";"LocationZIP";"LocationCity";"LocationPhoneAreaCode";"LocationPhone";"Email";"LocationFaxAreaCode";"LocationFaxNumber";"UserUrl";"UserLink";"UserTitle";"UserFirstName";"UserLastName"' . "\n";
            foreach ($records as $record) {
                $userId = $record['user_id'];
                $locationId = $record['location_id'];

                $user = new User($userId);
                $location = new Location($locationId);
                $providerId = $location->getId() . '-' . $user->getId();

                if ($user->isValid() && $location->isValid()) {
                    $data .= sprintf(
                            '"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s"',
                            sanitize_csv_field($providerId),
                            sanitize_csv_field('https://'.$GLOBALS['CONFIG']['DOMAIN'].'/w/'.$widgetSlug.'/arzt/?u='.$user->getId().'&l='.$location->getId()),
                            sanitize_csv_field($location->getName()),
                            sanitize_csv_field($location->getStreet()),
                            sanitize_csv_field($location->getStreetWONumber()),
                            sanitize_csv_field($location->getStreetNumber()),
                            sanitize_csv_field($location->getStreetExtension()),
                            sanitize_csv_field($location->getZip()),
                            sanitize_csv_field($location->getCity()),
                            '',
                            sanitize_csv_field($location->getPhone()),
                            '',
                            '',
                            sanitize_csv_field($location->getFax()),
                            sanitize_csv_field($user->getUrl(true, true)),
                            '',
                            sanitize_csv_field($user->getTitle()),
                            sanitize_csv_field($user->getFirstName()),
                            sanitize_csv_field($user->getLastName())
                        ) . "\n";
                }
            }
        }
        return $data;
    }
}