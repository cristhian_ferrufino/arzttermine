<?php

namespace Arzttermine\Widget\Widget;

use Arzttermine\Application\Application;
use Arzttermine\Event\Event;
use Arzttermine\Insurance\Insurance;
use Arzttermine\Location\Location;
use Arzttermine\Widget\WidgetInterface;

class DAKWidget implements WidgetInterface {

    /**
     * Widget display for bookings and booking profiles. Profile requires slug
     *
     * @param string $action
     * @param array $parameters
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function bookingAction($action = 'termin', $parameters = array())
    {
        $app = Application::getInstance();
        $router = $app->container->get('router');
        $request = $app->getRequest();

        //check if location is flagged as PKV only --> shouldn't get booked on DAK
        if($request->hasParam('l')) {
            $location = new Location($request->getParam('l'));
            if($location->isPkvOnly()) {
                return $app->redirect($router->generate('not_found'));
            }
        }

        // Override any pre-existing insurance id
        $request->injectParameter('GET',  'insurance_id', Insurance::GKV);
        $request->injectParameter('POST', 'insurance_id', Insurance::GKV);
        
        // Likewise, force it for the $form[] var
        $form = $request->getRequest('form');
        $form['insurance_id'] = Insurance::GKV;
        $request->injectParameter('GET',  'form', $form);
        $request->injectParameter('POST', 'form', $form);
        
        $action = strtolower($action);

        // @todo: Less kludgey/hacky way
        if ($action == 'termin' || $action == 'appointment') {
            return $app->forward($router->generate('appointment'));
        } elseif (($action == 'buchung' || $action == 'booking') && !empty($parameters[0])) {
            return $app->forward($router->generate('booking_profile', array('slug' => $parameters[0])));
        } else {
            return $app->redirect($router->generate('not_found'));
        }
    }

    /**
     * @param array $parameters
     * @param bool $use_seo
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function searchAction($parameters = array(), $use_seo = false)
    {
        $app = Application::getInstance();
        $router = $app->container->get('router');
        $request = $app->getRequest();
        $dispatcher = $app->container->get('dispatcher');

        $dispatcher->addListener('search.query_where', function(Event $event) {
            $event->data .= ' AND ' . DB_TABLENAME_USERS . '.has_contract = 1 ';
            return $event;
        });

        // exclude some unwanted locations/doctors that are not allowed to show up
        // for DAK patients
        $dispatcher->addListener('search.query_where', function(Event $event) {
            $event->data .= ' AND ' . DB_TABLENAME_LOCATIONS . '.id NOT IN (1947,1948,42,39657) AND ' . DB_TABLENAME_LOCATIONS . '.pkv_only = 0';
            return $event;
        });

        $dispatcher->addListener('medical_specialty.query_where', function(Event $event) {
            $event->data .= ' AND ' . DB_TABLENAME_MEDICAL_SPECIALTIES . '.id IN (1, 2, 3, 4, 5, 6, 7, 9, 11, 12, 13, 14, 15, 17, 18, 20, 23, 30, 34, 35, 36, 37, 38, 43, 46, 47, 48, 60, 63, 64, 69, 70, 71, 73, 75, 76, 80, 85, 86) ';
            return $event;
        });

        // Override any pre-existing insurance id
        $request->injectParameter('GET', 'insurance_id', Insurance::GKV);

        // Override the $form[] var
        $form = $request->getQuery('form');
        
        // Force insurance as above
        $form['insurance_id'] = Insurance::GKV;

        // The DAK widget should show a default if the url/search is empty.
        // Here: /zahnarzt/berlin
        if (empty($form['medical_specialty_id'])) {
            $form['medical_specialty_id'] = 1;
        }
        
        if (empty($form['location'])) {
            $form['location'] = 'Berlin';
        }

        $request->injectParameter('GET', 'form', $form);

        if (
            $use_seo  &&
            !empty($parameters['medical_specialty_slug'])  &&
            !empty($parameters['location_slug'])
        ) {
            return $app->forward($router->generate('search_no_trail', $parameters));
        } else {
            return $app->forward($router->generate('search'));
        }
    }

    /**
     * @inheritdoc
     */
    public function run($action = 'index', array $parameters = array())
    {
        switch ($action) {
            case 'termin':
            case 'appointment':
            case 'buchung':
            case 'booking':
                return $this->bookingAction($action, $parameters);
                break;
            case 'search':
            case 'suche':
                return $this->searchAction($parameters);
                break;
            default:
                // Let's assume we're visiting /location/specialty
                $search_parameters = array(
                    'medical_specialty_slug' => $action,
                    'location_slug' => array_shift($parameters)
                );

                return $this->searchAction($search_parameters, true);
        }
    }

    /**
     * @inheritdoc
     */
    public function getDataExport($widgetSlug, $format)
    {
        return array();
    }
}