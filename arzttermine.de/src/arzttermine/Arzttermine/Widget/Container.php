<?php

namespace Arzttermine\Widget;

use Arzttermine\Application\Application;
use Arzttermine\Core\Basic;
use Arzttermine\User\User;

/**
 * Widget Class
 *
 * @author Axel Kuzmik <axel@kuzmik.de>
 */
class Container extends Basic {

    const STATUS_DRAFT = 0;

    const STATUS_ACTIVE = 1;

    /**
     * @var string
     */
    protected $tablename = DB_TABLENAME_WIDGETS;

    /**
     * @var int
     */
    protected $status = self::STATUS_DRAFT;

    /**
     * @var string
     */
    protected $name = '';

    /**
     * @var string
     */
    protected $slug = '';

    /**
     * The widget type. e.g. universal-widget.
     * OR
     * e.g. Arzttermine\Widget\Widget\DAKWidget
     * Will hold fully resolved classname
     *
     * @var string
     */
    protected $pattern = '';

    /**
     * @var string
     */
    protected $html_head;

    /**
     * @var string
     */
    protected $html_body;

    /**
     * @var int
     */
    protected $views = 0;

    /**
     * @var string
     */
    protected $comment_intern = '';

    /**
     * @var string
     */
    protected $api_user_ids = '';

    /**
     * @var string
     */
    protected $created_at;

    /**
     * @var string
     */
    protected $created_by;

    /**
     * @var string
     */
    protected $updated_at;

    /**
     * @var string
     */
    protected $updated_by;

    /**
     * @var array
     */
    protected $all_keys
        = array(
            "id",
            "status",
            "name",
            "slug",
            "pattern",
            "html_head",
            "html_body",
            "views",
            "comment_intern",
            "api_user_ids",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by"
        );

    /**
     * @var array
     */
    protected $secure_keys
        = array(
            "id",
            "status",
            "name",
            "slug",
            "pattern",
            "html_head",
            "html_body",
            "views",
            "comment_intern",
            "api_user_ids",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by"
        );

    /**
     * The actual functioning widget
     *
     * @var WidgetInterface
     */
    protected $widget;

    /**
     * @var string
     */
    protected $action = 'index';

    /**
     * @var array
     */
    protected $args = array();

    /**
     * @param $action
     *
     * @return $this
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param array $args
     *
     * @return $this
     */
    public function setArgs(array $args = array())
    {
        $this->args = $args;

        return $this;
    }

    /**
     * @return array
     */
    public function getArgs()
    {
        return $this->args;
    }

    /**
     * @param WidgetInterface $widget
     *
     * @return $this
     */
    public function setWidget(WidgetInterface $widget)
    {
        $this->widget = $widget;

        return $this;
    }

    /**
     * Return the actual Widget
     *
     * @return WidgetInterface $widget
     */
    public function getWidget()
    {
        return $this->widget;
    }

    /**
     * Returns the default url
     *
     * @param bool $addHost
     *
     * @return string
     */
    public function getDefaultUrl($addHost = false)
    {
        $host = '';

        if ($addHost) {
            $host = Application::getInstance()->getRequest()->getBaseUrl();
        }

        return $host . '/w/' . $this->slug;
    }

    /**
     * Returns the status id
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getPattern()
    {
        return $this->pattern;
    }

    /**
     * Returns the slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Returns the name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the html_head content from the widget
     *
     * @return mixed
     */
    public function getHtmlHead()
    {
        return $this->html_head;
    }

    /**
     * Returns the html_body content from the widget
     *
     * @return mixed
     */
    public function getHtmlBody()
    {
        return $this->html_body;
    }

    /**
     * Increases the views counter
     *
     * @return self
     */
    protected function increaseViewCounter()
    {
        if (!is_numeric($this->views)) {
            $this->views = 0;
        }

        $this->views++;
        $this->save(array('views'));

        return $this;
    }

    /**
     * Dispatches the widget controller and displays the widget
     * (/w/slug/widget_parameters?page_parameters)
     * where page_parameters can be accessed as usual (getParam())
     *
     * e.g. /w/arztatlas-widget/arzt/gkv?u=8&l=2?date_days=5#arzt-atlas.de
     *
     * @throws Exception
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function run()
    {
        $view = Application::getInstance()->getView();

        $this
            ->increaseViewCounter()
            ->setReferences();

        $view
            ->prependTemplateDirectory($this->getDefaultTemplateDirectory())
            ->prependTemplateDirectory($this->getWidgetTemplateDirectory())
            ->setRef('widget', $this);

        try {
            return $this->getWidget()->run($this->getAction(), $this->getArgs());
        } catch (Exception $e) {
            error_log('Error in Widget [' . $this->getId() . ']. Description: ' . $e->getDescription());
            throw $e;
        }
    }

    /**
     * Returns the root template directory
     * 
     * @return string
     */
    public static function getTemplateDirectory()
    {
        return $GLOBALS['CONFIG']['SMARTY']['TEMPLATE_DIR'];
    }

    /**
     * Returns the source_platform name for this widget
     *
     * @param string
     *
     * @return string
     */
    public function getSourcePlatform($slug = null)
    {
        if (empty($slug)) {
            $slug = $this->getSlug();
        }
        return "widget-{$slug}";
    }

    /**
     * Returns the directory path of a widget's templates
     * 
     * @param string $slug
     *
     * @return string
     */
    public function getWidgetTemplatePattern($slug = null)
    {
        if (empty($slug)) {
            $slug = $this->getSlug();
        }
        return "widget/{$slug}";
    }

    /**
     * Returns a default template path
     * 
     * @return string
     */
    public function getDefaultTemplateDirectory()
    {
        return self::getTemplateDirectory() . '/' . $this->getWidgetTemplatePattern('universal');
    }

    /**
     * Return the template directory for the widget
     *
     * @retun string $path
     */
    public function getWidgetTemplateDirectory()
    {
        return self::getTemplateDirectory() . '/' . $this->getWidgetTemplatePattern();
    }

    /**
     * Set template references
     *
     * @return self
     */
    protected function setReferences()
    {
        $view = Application::getInstance()->getView();

        $view
            ->setRef('widget_title', $this->getName())
            ->setRef('widget_slug', $this->getSlug());

        $widget_css = '/widget/css/' . $this->comment_intern . '.css';
        $widget_css_min = '/widget/css/' . $this->comment_intern . '.min.css';

        if (file_exists(ASSET_PATH . $widget_css)) {
            $view->setRef('widget_css', $widget_css);
        }

        if (file_exists(ASSET_PATH . $widget_css_min)) {
            $view->setRef('widget_css', $widget_css_min);
        }

        return $this;
    }

    /**
     * Sets the API user ids
     *
     * @param array $userIds
     * @return self
     */
    public function setApiUserIds($userIds)
    {
        $this->api_user_ids = implode(',', $userIds);

        return $this;
    }

    /**
     * Returns the API user ids
     *
     * @return array
     */
    public function getApiUserIds()
    {
        return explode(',', $this->api_user_ids);
    }

    /**
     * Checks the credentials for a user and if the user is connected to this widget container
     *
     * @param string $email
     * @param string $password
     *
     * @return bool
     */
    public function checkCredentials($email, $password)
    {
        if (!User::checkCredentials($email, $password, GROUP_API)) {
            return false;
        }
        
        $user = new User($email, 'email');
        if (!$user->isValid()) {
            return false;
        }
        
        return in_array($user->getId(), $this->getApiUserIds());
    }
}
