<?php

namespace Arzttermine\Widget;

class Exception extends \Exception {

    /**
     * @var string
     */
    protected $description = '';

    public function __construct($description = '')
    {
        $this->setDescription($description);
    }

    /**
     * @param string $description
     *
     * @return self
     */
    public function setDescription($description = '')
    {
        $this->description = trim($description);

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
}