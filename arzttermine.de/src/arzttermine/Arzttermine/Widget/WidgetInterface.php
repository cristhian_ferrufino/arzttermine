<?php

namespace Arzttermine\Widget;

interface WidgetInterface {

    /**
     * Run the widget. Includes display
     *
     * @param string $action
     * @param array $parameters
     * @return \Symfony\Component\HttpFoundation\Response | \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function run($action = 'index', array $parameters = array());

    /**
     * Collects all data for a widget partner and returns it
     *
     * @param string $widgetSlug
     * @param string $format
     */
    public function getDataExport($widgetSlug, $format);
}