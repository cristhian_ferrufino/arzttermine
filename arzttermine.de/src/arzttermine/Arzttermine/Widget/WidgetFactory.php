<?php

namespace Arzttermine\Widget;

use Arzttermine\Application\Application;
use Arzttermine\Widget\Container as WidgetContainer;

class WidgetFactory {

    protected function __construct()
    {
    }

    /**
     * @param string $widget_slug
     *
     * @return Container
     * @throws \Exception
     */
    public static function getWidgetContainer($widget_slug = '')
    {
        $app = Application::getInstance();

        $widgetContainer = new WidgetContainer($widget_slug, 'slug');

        if (!$widgetContainer->isValid()) {
            $app->redirect($app->container->get('router')->generate('not_found'));
        }

        $widget_type = self::getWidgetClass($widgetContainer->getPattern());

        try {
            // @todo: Check interface properly
            if (class_exists($widget_type)) {
                $widgetContainer->setWidget(new $widget_type());
            } else {
                throw new \Exception;
            }
        } catch (\Exception $e) {
            error_log('Error in Widget [' . $widgetContainer->getId() . ']. Description: Class "' . $widget_type . '" not found.');
            throw new \Exception();
        }

        return $widgetContainer;
    }

    /**
     * Transforms the pattern into a usable class
     *
     * @param string $pattern
     *
     * @return string
     */
    protected static function getWidgetClass($pattern)
    {
        // Use the pattern if the classname is fully resolved
        if (strpos($pattern, '\\') !== false) {
            $class = $pattern;
        } else {
            // Backwards compatibility
            // @todo: Phase out
            $class = 'Arzttermine\\Widget\\Widget\\' .  self::camelize(str_replace('-', '_', $pattern));
        }

        return $class;
    }

    /**
     * Convert a delimited_string to camelCase
     *
     * @param string $string
     *
     * @return string
     */
    private static function camelize($string)
    {
        $string = $str = str_replace(' ', '', ucwords(str_replace('_', ' ', strtolower($string))));

        return $string;
    }
}