<?php

namespace Arzttermine\Review;

use Arzttermine\Application\Application;
use Arzttermine\Booking\Booking;
use Arzttermine\Calendar\Calendar;
use Arzttermine\Core\Basic;
use Arzttermine\Mail\Mailing;

class Review extends Basic {

    /**
     * @var string
     */
    protected $tablename = DB_TABLENAME_REVIEWS;

    /**
     * @var int
     */
    protected $booking_id;

    /**
     * @var int
     */
    protected $user_id;

    /**
     * @var string
     */
    protected $patient_email = '';

    /**
     * @var string
     */
    protected $patient_name = '';

    /**
     * @var int (bool)
     */
    protected $allow_sharing = 0;

    /**
     * @var float
     */
    protected $rating_1;

    /**
     * @var float
     */
    protected $rating_2;

    /**
     * @var float
     */
    protected $rating_3;

    /**
     * @var float
     */
    protected $rating_4;

    /**
     * @var string
     */
    protected $rate_4_text = '';

    /**
     * @var string
     */
    protected $rate_text = '';

    /**
     * @var string
     */
    protected $rated_at = '';

    /**
     * @var string
     */
    protected $approved_at = '';

    /**
     * @var string
     */
    protected $approved = '';

    /**
     * @var string
     */
    protected $url = '';

    /**
     * @var int
     */
    protected $status=1;

    /**
     * @var array
     */
    protected $all_keys
        = array(
            "id",
            "booking_id",
            "user_id",
            "patient_email",
            "patient_name",
            "allow_sharing",
            "rating_1",
            "rating_2",
            "rating_3",
            "rating_4",
            "rate_4_text",
            "rate_text",
            "rated_at",
            "status"
        );

    /**
     * @var array
     */
    protected $secure_keys
        = array(
            "id",
            "booking_id",
            "user_id",
            "patient_email",
            "patient_name",
            "allow_sharing",
            "rating_1",
            "rating_2",
            "rating_3",
            "rating_4",
            "rate_4_text",
            "rate_text",
            "rated_at",
            "status"
        );

    /**
     * @param string $url
     *
     * @return self
     */
    public function setUrl($url)
    {
        $this->url = trim($url);

        return $this;
    }

    /**
     * @param bool $absolute
     * @param bool $secure
     *
     * @return string
     */
    public function getUrl($absolute = false, $secure = false)
    {
        return ($absolute ? ($secure ? $GLOBALS['CONFIG']['URL_HTTPS_LIVE'] : $GLOBALS['CONFIG']['URL_HTTP_LIVE']) : '') . $this->url;
    }

    /**
     * Return the url
     * As the url depends on the booking slug this is sourced out into this static method
     *
     * @param string $key (url, https_url)
     * @param string $bookingSlug
     *
     * @return string
     */
    public static function getUrlByBookingSlug($key, $bookingSlug)
    {
        $url = '';
        switch ($key) {
            case 'https_url_new':
                $url = $GLOBALS['CONFIG']['URL_HTTPS_LIVE'] . '/bewertung/' . $bookingSlug;
                break;
            case 'url_new':
                $url = '/bewertung/' . $bookingSlug;
                break;
        }

        return $url;
    }

    /**
     * @return int
     */
    public function getBookingId()
    {
        return $this->booking_id;
    }

    /**
     * @param int $booking_id
     *
     * @return self
     */
    public function setBookingId($booking_id)
    {
        $this->$booking_id = $booking_id;

        return $this;
    }

    /**
     * @param int $user_id
     *
     * @return self
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Returns the email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->patient_email;
    }

    /**
     * Returns the booking object
     *
     * @return Booking
     */
    public function getBooking()
    {
        $booking = new Booking($this->booking_id);

        return $booking;
    }

    /**
     * Returns if the review was made anonymous
     *
     * @return string
     */
    public function isAnonymous()
    {
        return ($this->patient_name == '');
    }

    /**
     * Returns the rate_text
     *
     * @return string
     */
    public function getRateText()
    {
        return $this->rate_text;
    }

    /**
     * Returns the rate_4_text
     *
     * @return string
     */
    public function getServiceRateText()
    {
        return $this->rate_4_text;
    }

    /**
     * Returns the rate value
     *
     * @param string $key
     *
     * @return float
     */
    public function getRating($key)
    {
        $value = 0;
        switch ($key) {
            case 'rating_1':
            case 'rating_2':
            case 'rating_3':
            case 'rating_4':
            case 'rating_average':
                $value = $this->$key;
        }

        return $value;
    }

    /**
     * @param int $status
     *
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = intval($status);

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns the patients name
     *
     * @return string
     */
    public function getPatientName()
    {
        return $this->patient_name;
    }

    /**
     * Returns the review date
     *
     * @return string
     */
    public function getRatedAtDateText()
    {
        return Calendar::getWeekdayDateText($this->rated_at, true);
    }

    /**
     * Returns the reviews for a user
     *
     * @param int $user_id
     * @param int $rowCount (optional)
     * @param int $offset (optional)
     *
     * @return array Review
     */
    public static function getReviewsForUser($user_id, $rowCount = null, $offset = null)
    {
        $review = new self();
        $limit = '';

        // check if LIMIT should be used
        if ($rowCount != null && is_numeric($rowCount)) {
            $limit = ' LIMIT ';
            if ($offset != null && is_numeric($offset)) {
                $limit .= $offset . ',';
            }
            $limit .= $rowCount;
        }

        return $review->findObjects('user_id=' . $user_id . ' AND approved_at IS NOT NULL AND approved_at!="0000-00-00 00:00:00" ORDER BY rated_at DESC' . $limit);
    }

    /**
     * Returns the number of reviews for a user
     *
     * @param int $user_id
     *
     * @return int
     */
    public static function countReviewsForUser($user_id)
    {
        $review = new self();

        return $review->count('user_id=' . $user_id . ' AND approved_at IS NOT NULL AND approved_at!="0000-00-00 00:00:00"');
    }

    /**
     * Returns the ratings array for a user
     *
     * @param int $userId
     *
     * @return array [float rating_1, float rating_2, float rating_3, float rating_average]
     */
    public function getRatingsAverageArrayForUser($userId)
    {
        return $this->getRatingsAverageArray('user_id=' . $userId);
    }

    /**
     * Returns the ratings array for a user or location depending on the where clause
     *
     * @param string $where
     *
     * @return array [float rating_1, float rating_2, float rating_3, float rating_average]
     */
    public function getRatingsAverageArray($where)
    {
        $db = Application::getInstance()->getSql();

        $ratingsAverage1 = 0;
        $ratingsAverage2 = 0;
        $ratingsAverage3 = 0;

        $sql
            = 'SELECT
					AVG(rating_1) AS ratings_average_1,
					AVG(rating_2) AS ratings_average_2,
					AVG(rating_3) AS ratings_average_3
				FROM ' . $this->tablename . '
				WHERE
					' . $where . '
					AND
					approved_at IS NOT NULL
					AND approved_at!="0000-00-00 00:00:00"';

        $ratingsAverage = 0;
        $db->query($sql);

        if ($db->nf() > 0) {
            while ($db->next_record()) {
                $ratingsAverage1 = $db->f('ratings_average_1');
                $ratingsAverage2 = $db->f('ratings_average_2');
                $ratingsAverage3 = $db->f('ratings_average_3');
                $ratingsAverage = ($ratingsAverage1 + $ratingsAverage2 + $ratingsAverage3) / 3;
                break;
            }
        }

        return array(
            'rating_1'       => $ratingsAverage1,
            'rating_2'       => $ratingsAverage2,
            'rating_3'       => $ratingsAverage3,
            'rating_average' => $ratingsAverage
        );
    }

    /**
     * Returns the reviews for a location
     *
     * @param array $userIds
     * @param int $rowCount (optional)
     * @param int $offset (optional)
     *
     * @return array Review
     */
    public static function getReviewsForLocation($userIds, $rowCount = null, $offset = null)
    {
        $review = new self();
        $limit = '';

        // check if LIMIT should be used
        if (!empty($rowCount) && is_numeric($rowCount)) {
            $limit = ' LIMIT ';
            if (!empty($offset) && is_numeric($offset)) {
                $limit .= $offset . ',';
            }
            $limit .= $rowCount;
        }

        // get all users ratings
        $usersString = '';
        foreach ($userIds as $userId) {
            if ($usersString != '') {
                $usersString .= ' OR ';
            }
            $usersString .= $userId;
        }

        return $review->findObjects('(' . $usersString . ') AND approved_at IS NOT NULL AND approved_at!="0000-00-00 00:00:00" ORDER BY rated_at DESC' . $limit);
    }

    /**
     * Returns the number of reviews for a location
     *
     * @param array $userIds
     *
     * @return int
     */
    public static function countReviewsForLocation($userIds)
    {
        $review = new self();

        // get all users ratings
        $usersString = '';
        foreach ($userIds as $userId) {
            if (!empty($usersString)) {
                $usersString .= ' OR ';
            }
            $usersString .= $userId;
        }

        return $review->count('user_id IN (' . $usersString . ') AND approved_at IS NOT NULL AND approved_at!="0000-00-00 00:00:00"');
    }

    /**
     * Returns the ratings array for a location
     *
     * @param array $userIds
     *
     * @return array [float rating_1, float rating_2, float rating_3, float rating_average]
     */
    public function getRatingsAverageArrayForLocation($userIds)
    {
        if (empty($userIds)) {
            return 0;
        }

        // get all users ratings
        $usersString = '';
        foreach ($userIds as $userId) {
            if (!empty($usersString)) {
                $usersString .= ' OR ';
            }
            $usersString .= 'user_id=' . $userId;
        }

        return $this->getRatingsAverageArray('(' . $usersString . ')');
    }

    /**
     * Sends the review notification and reminder mails to an email
     *
     * @param string $cmsFilename
     * @param Booking $booking
     *
     * @return bool
     */
    public static function sendPatientEmail($cmsFilename, Booking $booking)
    {
        $user = $booking->getUser();
        $appointment = $booking->getAppointment();

        $_mailing = new Mailing();
        $_mailing->type = MAILING_TYPE_HTML;
        $_mailing->from = $GLOBALS['CONFIG']["REVIEWS_EMAIL_ADDRESS"];
        $_mailing->from_name = $GLOBALS['CONFIG']["REVIEWS_EMAIL_NAME"];
        $_mailing->content_filename = $cmsFilename;
        $_mailing->data['domain'] = $GLOBALS['CONFIG']['DOMAIN'];
        $_mailing->data['booking_salutation_full_name'] = $booking->getSalutationName();
        $_mailing->data['user_name'] = $user->getName();
        $_mailing->data['appointment_date'] = $appointment->getWeekdayDateText();
        $_mailing->data['appointment_time'] = $appointment->getTimeText();
        $_mailing->data['location_name'] = $booking->getLocation()->getName();
        $_mailing->data['location_address'] = $booking->getLocation()->getAddress();
        $_mailing->data['doctor_id'] = $user->getId();
        $_mailing->data['booking_id'] = $booking->getId();
        $_mailing->data['patient_email'] = $booking->getEmail();
        $_mailing->data['review_new_https_url'] = self::getUrlByBookingSlug('https_url_new', $booking->getSlug());
        $_mailing->addAddressTo($booking->getEmail());

        return $_mailing->send();
    }

    /**
     * Returns all bookings where the appointment took place between now and X hours ago
     *
     * Returns all bookings matching:
     * - a certain integration
     * - having a certain status
     * - where the appointment is between NOW() and NOW()-$hours (=> not older than $hours from script execution)
     * - not allready reviewed
     *
     * @param array $days
     * @param array $integrationIds
     * @param array $bookingStatuses
     *
     * @return Booking[]
     */
    public static function getBookingsForReviewReminders($days, $integrationIds, $bookingStatuses)
    {
        $bookings = array();

        if (!is_array($days) || empty($days)) {
            return $bookings;
        }

        // build the query for the statuses
        $bookingStatusesSql = Booking::getStatusesSql($bookingStatuses, 'OR');
        $bookingStatusesSql = ($bookingStatusesSql == '') ? '' : ' AND (' . $bookingStatusesSql . ')';

        // build the query for the integrations
        $integrationSql = '';
        if (!empty($integrationIds)) {
            foreach ($integrationIds as $integrationId) {
                $integrationSql .= ($integrationSql == '' ? '' : ' OR ');
                $integrationSql .= DB_TABLENAME_LOCATIONS . ".`integration_id`=" . $integrationId;
            }
            $integrationSql = ($integrationSql == '' ? '' : ' AND (' . $integrationSql . ')');
        }
        // build the query for the days
        $dateSql = '';
        if (!empty($days)) {
            foreach ($days as $day) {
                $dateSql .= ($dateSql == '' ? '' : ' OR ');
                $dateSql .= "DATE(`appointment_start_at`) = DATE_SUB(CURRENT_DATE, INTERVAL " . $day . " DAY)";
            }
            $dateSql = ($dateSql == '' ? '' : ' AND (' . $dateSql . ')');
        }

        $sql
            = "SELECT
					" . DB_TABLENAME_BOOKINGS . ".id
				FROM
					" . DB_TABLENAME_BOOKINGS . "
				LEFT JOIN
					" . DB_TABLENAME_USERS . " ON " . DB_TABLENAME_BOOKINGS . ".`user_id`=" . DB_TABLENAME_USERS . ".`id`
				LEFT JOIN
					" . DB_TABLENAME_LOCATIONS . " ON " . DB_TABLENAME_BOOKINGS . ".`location_id`=" . DB_TABLENAME_LOCATIONS . ".`id`
				WHERE
				" . DB_TABLENAME_BOOKINGS . ".`email` NOT LIKE '%%buchung%%@arzttermine.de'
				" . $dateSql . "
				" . $integrationSql . "
				" . $bookingStatusesSql . "
				AND
				" . DB_TABLENAME_BOOKINGS . ".id NOT IN (
					SELECT booking_id FROM reviews
				)
				AND
                source_platform != 'widget-dak'
                ";

        $booking = new Booking();

        return $booking->findObjectsSql($sql);
    }

    /**
     * Returns all bookings where the appointment took place between now and X hours ago
     *
     * Returns all bookings matching:
     * - a certain integration
     * - having a certain status
     * - where the appointment is between NOW() and NOW()-$hours (=> not older than $hours from script execution)
     * - not allready reviewed
     *
     * @param string $hours
     * @param array $integrationIds
     * @param array $bookingStatuses
     *
     * @return Booking[]
     */
    public static function getBookingsForReviewNotifications($hours, $integrationIds, $bookingStatuses)
    {
        $bookings = array();

        if (!is_numeric($hours)) {
            return $bookings;
        }

        // build the query for the statuses
        $bookingStatusesSql = Booking::getStatusesSql($bookingStatuses, 'OR');
        $bookingStatusesSql = ($bookingStatusesSql == '') ? '' : ' AND (' . $bookingStatusesSql . ')';

        // build the query for the integrations
        $integrationSql = '';
        if (!empty($integrationIds)) {
            foreach ($integrationIds as $integrationId) {
                $integrationSql .= ($integrationSql == '' ? '' : ' OR ');
                $integrationSql .= DB_TABLENAME_LOCATIONS . ".`integration_id`=" . $integrationId;
            }
            $integrationSql = ($integrationSql == '' ? '' : ' AND (' . $integrationSql . ')');
        }

        $sql
            = "SELECT
					" . DB_TABLENAME_BOOKINGS . ".id
				FROM
					" . DB_TABLENAME_BOOKINGS . "
				LEFT JOIN
					" . DB_TABLENAME_USERS . " ON " . DB_TABLENAME_BOOKINGS . ".`user_id`=" . DB_TABLENAME_USERS . ".`id`
				LEFT JOIN
					" . DB_TABLENAME_LOCATIONS . " ON " . DB_TABLENAME_BOOKINGS . ".`location_id`=" . DB_TABLENAME_LOCATIONS . ".`id`
				WHERE
				" . DB_TABLENAME_BOOKINGS . ".`email` NOT LIKE '%%buchung%%@arzttermine.de'
				AND
				`appointment_start_at` >= DATE_SUB(NOW(), INTERVAL " . $hours . " HOUR)
				AND
				`appointment_start_at` <= NOW()
				" . $integrationSql . "
				" . $bookingStatusesSql . "
				AND
				" . DB_TABLENAME_BOOKINGS . ".id NOT IN (
					SELECT booking_id FROM reviews
				)
				AND
				source_platform != 'widget-dak'
				";

        $booking = new Booking();

        return $booking->findObjectsSql($sql);
    }
}
