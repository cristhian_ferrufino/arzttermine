<?php

namespace Arzttermine\Appointment;

use Arzttermine\Core\Basic;
use Arzttermine\Location\Location;
use Arzttermine\User\User;

class Planner extends Basic {

    /**
     * @var string
     */
    protected $tablename = 'planners';
    
    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $user_id;
    
    /**
     * Owning doctor
     *
     * @var User
     */
    protected $user;

    /**
     * @var int
     */
    protected $location_id;
    
    /**
     * Owning practice
     * 
     * @var Location
     */
    protected $location;

    /**
     * These will be lazy-loaded
     * 
     * @var PlannerBlock[]
     */
    protected $blocks = array();

    /**
     * Minimum time period from now until the first appointment will display
     * i.e. "this appointment needs 3 days' notice"
     * 
     * @var int
     */
    protected $lead_in = 0;

    /**
     * Maximum searchable time period from today in days 
     * 
     * @var int
     */
    protected $search_period = 90;

    /**
     * @var array
     */
    protected $all_keys = array(
        'id',
        'user_id',
        'location_id',
        'lead_in',
        'search_period'
    );

    /**
     * @var array
     */
    protected $secure_keys = array();
    
    /**
     * @return int
     */
    public function getId()
    {
        return intval($this->id);
    }

    /**
     * @return User
     */
    public function getUser()
    {
        if (!($this->user instanceof User)){
            $this->setUser(new User(intval($this->user_id)));
        }
        
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return self
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        $this->setUserId($user->getId());

        return $this;
    }

    /**
     * @param $user_id
     *
     * @return self
     */
    public function setUserId($user_id)
    {
        $this->user_id = intval($user_id);

        return $this;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return intval($this->getUser()->getId());
    }
    
    /**
     * @return Location
     */
    public function getLocation()
    {
        if (!($this->location instanceof Location)){
            $this->setLocation(new Location(intval($this->location_id)));
        }
        
        return $this->location;
    }

    /**
     * @param Location $location
     *
     * @return self
     */
    public function setLocation(Location $location)
    {
        $this->location = $location;
        $this->setLocationId($location->getId());

        return $this;
    }

    /**
     * @return int
     */
    public function getLocationId()
    {
        return intval($this->getLocation()->getId());
    }

    /**
     * @param $location_id
     *
     * @return self
     */
    public function setLocationId($location_id)
    {
        $this->location_id = intval($location_id);

        return $this;
    }

    /**
     * @return int
     */
    public function getSearchPeriod()
    {
        return intval($this->search_period);
    }

    /**
     * @param int $search_period
     *
     * @return self
     */
    public function setSearchPeriod($search_period)
    {
        $this->search_period = intval($search_period);

        return $this;
    }

    /**
     * @return int
     */
    public function getLeadIn()
    {
        return intval($this->lead_in);
    }

    /**
     * @param int $lead_in
     *
     * @return self
     */
    public function setLeadIn($lead_in)
    {
        if (intval($lead_in) < 0) {
            $lead_in = 0;
        }

        $this->lead_in = intval($lead_in);

        return $this;
    }


    /**
     * @param PlannerBlock $block
     *
     * @return self
     */
    public function addBlock(PlannerBlock $block)
    {
        $block->setPlanner($this);
        $this->blocks[] = $block;
        
        return $this;
    }

    /**
     * @return PlannerBlock[]
     */
    public function getBlocks()
    {
        // Lazy-load events
        if (empty($this->blocks)) {
            $planner_block = new PlannerBlock();
            $this->blocks = $planner_block->findObjects('planner_id = ' . $this->getId());
        }
        
        return is_array($this->blocks) ? $this->blocks : array($this->blocks);
    }

    /**
     * @param PlannerBlock[] $blocks
     *
     * @return self
     */
    public function setBlocks(array $blocks)
    {
        foreach($blocks as &$block) {
            if (!($block instanceof PlannerBlock)) {
                unset($block);
                continue;
            }
            // Set ourselves as the parent
            $block->setPlanner($this);
        }
        
        $this->blocks = $blocks;

        return $this;
    }
    
    /**
     * Save the events attached to this planner.
     * It might be useful to NOT update events when saving a planner, so this is separate
     * 
     * @return self
     */
    public function saveBlocks()
    {
        foreach ($this->getBlocks() as $block) {
            if ($block instanceof PlannerBlock) {
                $block->saveNewObject();
            }
        }

        return $this;
    }

    /**
     * Delete the currently active blocks for this planner
     *
     * @return self
     */
    public function clearBlocks()
    {
        foreach ($this->getBlocks() as $block) {
            if ($block instanceof PlannerBlock) {
                $block->delete();
            }
        }

        return $this;
    }

    /**
     * @param int|User $user
     *
     * @return Planner
     */
    public function getForUser($user = null)
    {
        if (empty($user)) {
            $user = $this->getUser();
        }
        
        if (!($user instanceof User)) {
            $user = new User(intval($user));
        }

        return $this->findObject('user_id = ' . $user->getId());
    }

    /**
     * @param int|Location $location
     *
     * @return Planner
     */
    public function getForLocation($location = null)
    {
        if (empty($location)) {
            $location = $this->getLocation();
        }

        if (!($location instanceof Location)) {
            $location = new Location(intval($location));
        }

        return $this->findObject('location_id = ' . $location->getId());
    }

    /**
     * Combination of the above two...
     * 
     * @param int|User $user
     * @param int|Location $location
     *
     * @return Planner
     */
    public function getForUserLocation($user = null, $location = null)
    {
        if (empty($user)) {
            $user = $this->getUser();
        }

        if (empty($location)) {
            $location = $this->getLocation();
        }

        if (!($user instanceof User)) {
            $user = new User(intval($user));
        }
        
        if (!($location instanceof Location)) {
            $location = new Location(intval($location));
        }

        /** @var Planner $planner */
        $planner = $this->findObject('user_id = ' . $user->getId() . ' AND location_id = ' . $location->getId());
        
        // If the user/location is fine, but they don't have a planner yet, save a new one and use that
        if (!$planner->isValid() && $user->isValid() && $location->isValid()) {
            $planner
                ->setUser($user)
                ->setLocation($location)
                ->saveNewObject();
        }
        
        return $planner;
    }
}