<?php

namespace Arzttermine\Appointment;

use Arzttermine\Core\DateTime;
use Arzttermine\Location\Location;
use Arzttermine\User\User;

/**
 * This service is designed to handle appointments for doctors and practices
 */
class AppointmentService {

    /**
     * @var self
     */
    protected static $instance;

    /**
     * Prevent direct instantiation
     */
    protected function __construct()
    {
    }

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param User $user
     * @param Location $location
     * @param DateTime $dateStart
     * @param DateTime $dateEnd
     *
     * @return self
     */
    public function flush($user, $location, $dateStart = null, $dateEnd = null)
    {
        $sql = 'user_id='.$user->getId().' AND location_id='.$location->getId();
        $dateStartSql = '';
        if ($dateStart != null) {
            $dateStartSql = ' AND appointment_date >= "'.$dateStart->__toString().'"';
        }
        $dateEndSql = '';
        if ($dateEnd != null) {
            $dateEndSql = ' AND appointment_date <= "'.$dateEnd->__toString().'"';
        }
        $appointment = new Appointment();
        $appointment->deleteWhere($sql . $dateStartSql . $dateEndSql);

        return $this;
    }

    /**
     * @param Appointment[] $appointments
     *
     * @return self
     */
    public function save(array $appointments)
    {
        foreach ($appointments as $appointment) {
            if ($appointment instanceof Appointment) {
                $appointment->saveNewObject();
            }
        }

        return $this;
    }
}