<?php

namespace Arzttermine\Appointment;

use Arzttermine\Core\Basic;
use Arzttermine\Core\DateTime;
use Arzttermine\MedicalSpecialty\TreatmentType;

class PlannerBlock extends Basic {

    /**
     * @var string
     */
    protected $tablename = 'planner_blocks';
    
    /**
     * @var int
     */
    protected $id;

    /**
     * @var Planner
     */
    protected $planner;

    /**
     * @var int
     */
    protected $planner_id;

    /**
     * @var DateTime
     */
    protected $start_time;

    /**
     * @var DateTime
     */
    protected $end_time;

    /**
     * Block in minutes
     * 
     * @var int
     */
    protected $length;

    /**
     * This is a comma-separated string in the database
     *
     * @var array
     */
    protected $insurance_ids;

    /**
     * This is a comma-separated string in the database
     * Note: We find the Medical Specialty through the doctor. Samedi uses TT filtering
     *
     * @var array
     */
    protected $treatment_type_ids;

    /**
     * @var TreatmentType[]
     */
    protected $treatment_types;

    /**
     * Interval in days to repeat this event (0 = no recurrence)
     * 
     * @var int
     */
    protected $recurrence_interval = 0;

    /**
     * This represents a Date and can be null
     * Using DateTime results in a default value, which we don't want. GG PHP
     * 
     * @var string
     */
    protected $recur_until = '';

    /**
     * An array of dates to ignore when generating dates, assuming this event is recurring. Only uses date component (YYYY-MM-DD)
     * 
     * @var DateTime[]
     */
    protected $exclusions = array();

    /**
     * @var array
     */
    protected $all_keys = array(
        'id',
        'planner_id',
        'start_time',
        'end_time',
        'length',
        'insurance_ids',
        'treatment_type_ids',
        'recurrence_interval',
        'recur_until',
        'exclusions'
    );

    /**
     * @var array
     */
    protected $secure_keys = array(
        'id',
        'planner_id',
        'start_time',
        'end_time',
        'length',
        'insurance_ids',
        'treatment_type_ids',
        'recurrence_interval',
        'recur_until',
        'exclusions'
    );

    /**
     * @return int
     */
    public function getId()
    {
        return intval($this->id);
    }
    
    /**
     * Returns block in minutes.
     * Note: This obviously means that to get the block in hours, you need floatval($this->getBlock() / 60)
     * 
     * @return int
     */
    public function getLength()
    {
        // Enforce a minimum of 1 to avoid divide by zero and sanity errors
        return intval(max($this->length, 1));
    }

    /**
     * @param int $length
     *
     * @return self
     */
    public function setLength($length)
    {
        // See getBlock
        $this->length = intval(max($length, 1));

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getEndTime()
    {
        if (!($this->end_time instanceof DateTime)) {
            $this->setEndTime($this->end_time);
        }

        return $this->end_time;
    }

    /**
     * @param string|DateTime $end
     *
     * @return self
     */
    public function setEndTime($end)
    {
        if (!($end instanceof DateTime)) {
            $end = new DateTime($end);
        }

        $this->end_time = $end;

        return $this;
    }

    /**
     * @return int
     */
    public function getPlannerId()
    {
        return intval($this->getPlanner()->getId());
    }

    /**
     * @return Planner
     */
    public function getPlanner()
    {
        if (empty($this->planner) || !($this->planner instanceof Planner)) {
            $this->setPlanner(new Planner(intval($this->planner_id)));
        }
        
        return $this->planner;
    }

    /**
     * Set the parent Planner, including Planner ID
     * 
     * @param Planner $planner
     *
     * @return self
     */
    public function setPlanner(Planner $planner)
    {
        $this->planner = $planner;
        $this->planner_id = $planner->getId();

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getStartTime()
    {
        if (!($this->start_time instanceof DateTime)) {
            $this->setStartTime($this->start_time);
        }

        return $this->start_time;
    }

    /**
     * @param string|DateTime $start
     *
     * @return self
     */
    public function setStartTime($start)
    {
        if (!($start instanceof DateTime)) {
            $start = new DateTime($start);
        }
        
        $this->start_time = $start;

        return $this;
    }

    /**
     * @return array
     */
    public function getInsuranceIds()
    {
        return explode(',', $this->insurance_ids);
    }

    /**
     * @param array|int $insurance_ids
     *
     * @return self
     */
    public function setInsuranceIds($insurance_ids)
    {
        if (!is_array($insurance_ids)) {
            $insurance_ids = array(intval($insurance_ids));
        }

        $insurance_ids = array_filter(array_unique($insurance_ids), 'intval');

        $this->insurance_ids = implode(',', $insurance_ids);

        return $this;
    }

    /**
     * @return array
     */
    public function getTreatmentTypeIds()
    {
        return explode(',', $this->treatment_type_ids);
    }

    /**
     * @param array|int $treatment_type_ids
     *
     * @return self
     */
    public function setTreatmentTypeIds($treatment_type_ids)
    {
        if (!is_array($treatment_type_ids)) {
            $treatment_type_ids = array(intval($treatment_type_ids));
        }

        $treatment_type_ids = array_filter($treatment_type_ids, 'intval');

        $this->treatment_type_ids = implode(',', $treatment_type_ids);

        return $this;
    }

    /**
     * @return TreatmentType[]
     */
    public function getTreatmentTypes()
    {
        $treatment_types = array();

        foreach ($this->getTreatmentTypeIds() as $ttid) {
            $tt = new TreatmentType($ttid);
            if ($tt->isValid()) {
                $treatment_types[] = $tt;
            }
        }

        return $treatment_types;
    }

    /**
     * Set the treatment types using objects
     *
     * @param TreatmentType[] $treatment_types
     *
     * @return self
     */
    public function setTreatmentTypes(array $treatment_types)
    {
        $ttid = array();

        foreach ($treatment_types as $tt) {
            if ($tt->isValid()) {
                $ttid[] = $tt;
            }
        }

        return $this->setTreatmentTypeIds($ttid);
    }

    /**
     * @return int
     */
    public function getRecurrenceInterval()
    {
        return intval($this->recurrence_interval);
    }

    /**
     * @param int $recurrence_interval
     *
     * @return self
     */
    public function setRecurrenceInterval($recurrence_interval)
    {
        $this->recurrence_interval = intval($recurrence_interval);

        return $this;
    }

    /**
     * @return bool
     */
    public function isRecurring()
    {
        return $this->getRecurrenceInterval() > 0;
    }

    /**
     * This method is weird because it's a DateTime that can be null
     * 
     * @return string
     */
    public function getRecurUntil()
    {
        // if we have a value, use a DateTime object to format it for us
        if (!empty($this->recur_until)) {
            $date = new DateTime($this->recur_until);
            return $date->getDate();
        }
        
        return $this->recur_until;
    }

    /**
     * This method is weird because it's a DateTime that can be null
     *
     * @param \DateTime|string $recur_until
     *
     * @return self
     */
    public function setRecurUntil($recur_until)
    {
        if (!empty($recur_until)) {
            if (!$recur_until instanceof DateTime) {
                $recur_until = new DateTime($recur_until);
            }
            
            $this->recur_until = $recur_until->getDate();
        } else {
            $this->recur_until = $recur_until;
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getExclusions()
    {
        return (array)$this->exclusions;
    }

    /**
     * @param array $exclusions
     *
     * @return self
     */
    public function setExclusions($exclusions)
    {
        $this->exclusions = array_map(function($date) {
            return new DateTime($date);
        }, $exclusions);

        return $this;
    }

    /**
     * @param string|DateTime $date
     * 
     * @return bool
     */
    public function isExcludedDate($date)
    {
        if (!($date instanceof DateTime)) {
            $date = new DateTime($date);
        }
        
        $excluded_dates = array_map(function($exclusion) {
            /** @var DateTime $exclusion */
            return $exclusion->getDate();
        }, $this->getExclusions());
        
        return in_array($date->getDate(), $excluded_dates);
    }

    /**
     * {@inheritdoc}
     */
    public function hydrate(array $data)
    {
        foreach ($data as $key => $value) {
            if (property_exists($this, $key)) {
                switch ($key) {
                    case 'start_time':
                    case 'end_time':
                        if ($this->$key instanceof DateTime) {
                            $this->$key = $value;
                        } else {
                            $this->$key = new DateTime($value);
                        }
                        break;
                    case 'exclusions':
                        if (!is_array($value)) {
                            $value = (array)json_decode((string)$value);
                        }
                        $this->setExclusions($value);
                        break;
                    default:
                       $this->$key = $value;
                }
            } else {
                error_log("Warning: Property '{$key}' of class '" . get_class($this) . "' not found. Skipping.");
            }
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    protected function getProperties(array $keys)
    {
        $data = array();

        foreach ($keys as $key) {
            if ($this->$key instanceof DateTime) {
                $value = $this->$key->getDateTime();
            } else {
                $value = $this->$key;
            }
            
            // Cast datetime array to JSON
            if ($key === 'exclusions') {
                $dates = array_map(function($date) {
                    /** @var DateTime $date */
                    return $date->getDate();
                }, (array)$this->$key);
                
                $value = json_encode($dates);
            }
            
            $data[$key] = in_array($key, $this->secure_keys) ? sqlsave($value) : $value;
        }

        return $data;
    }
    
}