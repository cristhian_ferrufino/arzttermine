<?php

namespace Arzttermine\Appointment;

use Arzttermine\Application\Application;
use Arzttermine\Calendar\Calendar;
use Arzttermine\Core\DateTime;
use Arzttermine\Core\Configuration;
use Arzttermine\Insurance\Insurance;
use Arzttermine\Integration\Integration;
use Arzttermine\Location\Location;
use Arzttermine\MedicalSpecialty\MedicalSpecialty;
use Arzttermine\MedicalSpecialty\TreatmentType;
use Arzttermine\User\User;

/**
 * An Appointment Provider is a class that represents a User, Location, Insurance, DateTime, etc that a user can book
 * It's effectively a bag of related items
 *
 * @package Arzttermine\Appointment
 */
class Provider {

    /**
     * We have an array of DateTimes, as DatePeriod doesn't allow day filtering
     * 
     * @var DateTime[]
     */
    protected $date_range;

    /**
     * Approximate distance from a search location (if any) in km
     * 
     * @var float
     */
    protected $distance = 0;
    
    /**
     * @var int
     */
    protected $user_id;

    /**
     * Owning doctor
     *
     * @var User
     */
    protected $user;

    /**
     * @var int
     */
    protected $location_id;

    /**
     * Owning practice
     *
     * @var Location
     */
    protected $location;

    /**
     * The appointments that this provider has available
     * 
     * @var Appointment[]
     */
    protected $appointments;

    /**
     * The ID helps us lazy-load
     * 
     * @var int
     */
    protected $medical_specialty_id;

    /**
     * The medical specialty that this provider row is displaying results for. Compulsory.
     * 
     * @var MedicalSpecialty
     */
    protected $medical_specialty;

    /**
     * If set, this filters the types of appointments by insurance type
     *
     * @var int
     */
    protected $insurance_id;

    /**
     * Insurance object
     *
     * @var int
     */
    protected $insurance;

    /**
     * @var int
     */
    protected $treatment_type_id;

    /**
     * @var TreatmentType
     */
    protected $treatment_type;
    
    /**
     * @param DateTime $start_date
     * @param DateTime $end_date
     * @param User $user
     * @param Location $location
     * @param MedicalSpecialty $medical_specialty
     */
    public function __construct(DateTime $start_date = null, DateTime $end_date = null, User $user = null, Location $location = null, MedicalSpecialty $medical_specialty = null)
    {
        if (!empty($start_date) && !empty($end_date)) {
            $this->setDateRange($start_date, $end_date);
        }
        
        if (!empty($user)) {
            $this->setUser($user);
        }
        
        if (!empty($location)) {
            $this->setLocation($location);
        }
        
        if (!empty($medical_specialty)) {
            $this->setMedicalSpecialty($medical_specialty);
        }
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->location_id.'-'.$this->user_id;
    }

    /**
     * @return DateTime[]
     */
    public function getDateRange()
    {
        return $this->date_range;
    }

    /**
     * Set the date range according to start/end. This ensures we have new objects each time
     * 
     * @param DateTime $start_date
     * @param DateTime $end_date
     * 
     * @return self
     */
    public function setDateRange(DateTime $start_date, DateTime $end_date)
    {
        $this->date_range = Calendar::getDatesInRange($start_date, $end_date, true);
        
        return $this;
    }

    /**
     * @return float
     */
    public function getDistance()
    {
        return floatval($this->distance);
    }

    /**
     * @param float $distance
     *
     * @return self
     */
    public function setDistance($distance)
    {
        $this->distance = floatval($distance);

        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        if (!($this->user instanceof User)){
            $this->setUser(new User(intval($this->user_id)));
        }

        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return self
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        $this->user_id = $this->user_id = intval($user->getId());

        return $this;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return intval($this->getUser()->getId());
    }

    /**
     * @return Location
     */
    public function getLocation()
    {
        if (!($this->location instanceof Location)){
            $this->setLocation(new Location(intval($this->location_id)));
        }

        return $this->location;
    }

    /**
     * @param Location $location
     *
     * @return self
     */
    public function setLocation(Location $location)
    {
        $this->location = $location;
        $this->location_id = $location->getId();

        return $this;
    }

    /**
     * Return just the IDs of available insurance types
     *
     * @return int
     */
    public function getLocationId()
    {
        return intval($this->location_id);
    }

    /**
     * @return Appointment[]
     */
    public function getAppointments()
    {
        // Make sure appointments is always going to return a valid array of Appointment
        if (!is_array($this->appointments)) {
            $this->setAppointments(array($this->appointments));
        }
        
        return $this->appointments;
    }

    /**
     * @param Appointment[] $appointments
     *
     * @return self
     */
    public function setAppointments(array $appointments)
    {
        // Filter out non-Appointment objects
        $appointments = array_filter($appointments, function($appointment) {
            return $appointment instanceof Appointment;
        });
        
        $this->appointments = $appointments;
        
        return $this;
    }

    /**
     * @return MedicalSpecialty
     */
    public function getMedicalSpecialty()
    {
        if (!($this->medical_specialty instanceof MedicalSpecialty)) {
            $this->medical_specialty = new MedicalSpecialty($this->getMedicalSpecialtyId());
        }
        
        return $this->medical_specialty;
    }

    /**
     * @param int|MedicalSpecialty $medical_specialty
     *
     * @return self
     */
    public function setMedicalSpecialty($medical_specialty)
    {
        if (!($medical_specialty instanceof MedicalSpecialty)) {
            $medical_specialty = new MedicalSpecialty(intval($medical_specialty));
        }
        
        $this->medical_specialty = $medical_specialty;
        $this->medical_specialty_id = $medical_specialty->getId();

        return $this;
    }

    /**
     * @return int
     */
    public function getMedicalSpecialtyId()
    {
        return intval($this->medical_specialty_id);
    }

    /**
     * @param $medical_specialty_id
     *
     * @return self
     */
    public function setMedicalSpecialtyId($medical_specialty_id)
    {
        $this->medical_specialty_id = intval($medical_specialty_id);

        return $this;
    }

    /**
     * @return int
     */
    public function getInsuranceId()
    {
        return intval($this->insurance_id);
    }

    /**
     * @param int $insurance_id
     *
     * @return self
     */
    public function setInsuranceId($insurance_id)
    {
        $this->insurance_id = intval($insurance_id);

        return $this;
    }

    /**
     * @return Insurance
     */
    public function getInsurance()
    {
        if (!($this->insurance instanceof Insurance)) {
            $this->insurance = new Insurance($this->getInsuranceId());
        }

        return $this->insurance;
    }

    /**
     * @param Insurance|int $insurance
     *
     * @return self
     */
    public function setInsurance($insurance)
    {
        if (!($insurance instanceof Insurance)) {
            $insurance = new Insurance(intval($insurance));
        }

        $this->insurance = $insurance;
        $this->insurance_id = $insurance->getId();
        
        return $this;
    }

    /**
     * @return int
     */
    public function getTreatmentTypeId()
    {
        return intval($this->treatment_type_id);
    }

    /**
     * @param int $treatment_type_id
     *
     * @return self
     */
    public function setTreatmentTypeId($treatment_type_id)
    {
        $this->treatment_type_id = intval($treatment_type_id);

        return $this;
    }

    /**
     * @return TreatmentType
     */
    public function getTreatmentType()
    {
        if (!($this->treatment_type instanceof TreatmentType)) {
            $this->treatment_type = new TreatmentType($this->treatment_type_id);
        }

        return $this->treatment_type;
    }

    /**
     * @param TreatmentType $treatment_type
     *
     * @return self
     */
    public function setTreatmentType($treatment_type)
    {
        if (!($treatment_type instanceof TreatmentType)) {
            $treatment_type = new TreatmentType(intval($treatment_type));
        }

        $this->treatment_type = $treatment_type;
        $this->treatment_type_id = $treatment_type->getId();

        return $this;
    }

    /**
     * Find appointments that belong to this provider from a nested search
     * See: Search::findAppointments
     * 
     * @param array $appointments
     * 
     * @return self
     */
    public function findAppointments(array $appointments = array())
    {
        if (
            isset($appointments[$this->getLocationId()]) &&
            isset($appointments[$this->getLocationId()][$this->getUserId()])
        ) {
            $this->setAppointments($appointments[$this->getLocationId()][$this->getUserId()]);
        }
    }

    /**
     * @param string $widget_slug
     *
     * @return array
     */
    public function getAppointmentsBlock($widget_slug = '')
    {
        $appointments = array();

        // If we don't have insurance, use this insurance selector block 
        if (!$this->getInsurance()->isValid()) {
            $appointments['type'] = 'no-selected-insurance';

            return $appointments;
        }

        // Check if there's a custom message stored against this doctor
        // @todo: Replace this hacky rubbish
        if ($customMessage = $this->getCustomMessageDoctor(Configuration::get('doctors_calendar_custom_message'))) {
            $appointmentsBlock['type'] = 'custom-message';
            $appointmentsBlock['customMessage'] = $customMessage;

            return $appointmentsBlock;
        }

        $integration = Integration::getIntegration($this->getLocation()->getIntegrationId());
        $appointments = $integration->getAppointmentsBlock($this, $widget_slug);

        /*
         * Filter out non working days.
         */
        foreach ((array)$appointments['days'] as $date => $data) {
            if (!Calendar::isWorkingDay($date)) {
                $appointments['days'][$date]['appointments'] = array();
            }
        }

        return $appointments;
    }

    /**
     * @todo DUMP THIS RUBBISH
     *
     * @param $doctors_calendar_custom_message
     *
     * @return string
     */
    protected function getCustomMessageDoctor($doctors_calendar_custom_message)
    {
        if (!empty($doctors_calendar_custom_message)) {
            $doctors_calendar_custom_message_array = explode('^', $doctors_calendar_custom_message);
            foreach ($doctors_calendar_custom_message_array as $value) {
                $seperatorChar = '->';
                $firstSeperatorPos = strpos($value, $seperatorChar);
                $secondSeperatorPos = strpos($value, $seperatorChar, $firstSeperatorPos + strlen($seperatorChar));
                $customMessageUserId = substr($value, 0, $firstSeperatorPos);
                $customMessageInsuranceId = substr($value, $firstSeperatorPos + strlen($seperatorChar), $secondSeperatorPos - ($firstSeperatorPos + strlen($seperatorChar)));
                if ($this->getUserId() == $customMessageUserId && $this->getInsuranceId() == $customMessageInsuranceId) {
                    $customCaseMessage = trim(trim(substr($value, $secondSeperatorPos + strlen($seperatorChar))), '"');

                    return Application::getInstance()->_($customCaseMessage);
                }
            }
        }

        return '';
    }
}