<?php

namespace Arzttermine\Appointment;

use Arzttermine\Application\Application;
use Arzttermine\Calendar\Calendar;
use Arzttermine\Core\Basic;
use Arzttermine\Core\DateTime;
use Arzttermine\Insurance\Insurance;
use Arzttermine\Location\Location;
use Arzttermine\MedicalSpecialty\TreatmentType;
use Arzttermine\User\User;

/**
 * This class describes an appointment timeslot that is available for patients
 *
 * @package Arzttermine\Appointment
 */
class Appointment extends Basic {
    
    /**
     * @var string
     */
    protected $tablename = 'appointments';

    /**
     * @var int
     */
    protected $id;

    /**
     * Only the date component
     *
     * @var DateTime
     */
    protected $date;

    /**
     * Only the date component
     *
     * @var DateTime
     */
    protected $end_time;

    /**
     * Only the time component
     *
     * @var DateTime
     */
    protected $start_time;

    /**
     * @var int
     */
    protected $user_id;

    /**
     * Owning doctor
     *
     * @var User
     */
    protected $user;

    /**
     * @var int
     */
    protected $location_id;

    /**
     * Owning practice
     *
     * @var Location
     */
    protected $location;

    /**
     * This is a comma-separated string in the database
     * 
     * @var array
     */
    protected $insurance_ids;

    /**
     * This is a comma-separated string in the database
     * Note: We find the Medical Specialty through the doctor. Samedi uses TT filtering
     * 
     * @var array
     */
    protected $treatment_type_ids;

    /**
     * @var array
     */
    protected $all_keys = array(
        'id',
        'date',
        'start_time',
        'end_time',
        'user_id',
        'location_id',
        'insurance_ids',
        'treatment_type_ids',
    );

    /**
     * @var array
     */
    protected $secure_keys = array(
        'id',
        'date',
        'start_time',
        'end_time',
        'user_id',
        'location_id',
        'insurance_ids',
        'treatment_type_ids',
    );


    /**
     * {@inheritdoc}
     */
    public function __construct($value = 0, $key = 'id')
    {
        // Set defaults for all DateTime objects
        $this->date       = new DateTime();
        $this->start_time = new DateTime();
        $this->end_time   = new DateTime();
        
        parent::__construct($value, $key);
    }
    
    /**
     * @return int
     */
    public function getId()
    {
        return intval($this->id);
    }

    /**
     * {@inheritdoc}
     */
    public function hydrate(array $data)
    {
        foreach ($data as $key => $value) {
            if (property_exists($this, $key)) {
                switch ($key) {
                    case 'date':
                    case 'start_time':
                    case 'end_time':
                        if ($this->$key instanceof DateTime) {
                            $this->$key = $value;
                        } else {
                            $this->$key = new DateTime($value);
                        }
                        break;
                    default:
                        $this->$key = $value;
                }
            } else {
                error_log("Warning: Property '{$key}' of class '" . get_class($this) . "' not found. Skipping.");
            }
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    protected function getProperties(array $keys)
    {
        $data = array();

        foreach ($keys as $key) {
            if ($this->$key instanceof DateTime) {
                $value = $this->$key->format('Y-m-d H:i:s');
            } else {
                $value = $this->$key;
            }

            $data[$key] = in_array($key, $this->secure_keys) ? sqlsave($value) : $value;
        }

        return $data;
    }

    /**
     * Return just the Date component as YYYY-MM-DD
     *
     * @return DateTime
     */
    public function getDate()
    {
        if (!($this->date instanceof DateTime)) {
            $this->setDate($this->date);
        }

        return $this->date;
    }

    /**
     * @param DateTime|string $date
     *
     * @return self
     */
    public function setDate($date)
    {
        if (!($date instanceof DateTime)) {
            $date = new DateTime($date);
        }
        
        if ($this->start_time instanceof DateTime) {
            $this->start_time->setDate($date);
        }

        if ($this->end_time instanceof DateTime) {
            $this->end_time->setDate($date);
        }
        
        $this->date = $date;

        return $this;
    }

    /**
     * Return just the Date component as YYYY-MM-DD
     *
     * @return DateTime
     */
    public function getEndTime()
    {
        if (!($this->end_time instanceof DateTime)) {
            $this->setEndTime($this->end_time);
        }

        return $this->end_time;
    }

    /**
     * @param DateTime|string $date
     *
     * @return self
     */
    public function setEndTime($date)
    {
        if (!($date instanceof DateTime)) {
            $date = new DateTime($date);
        }

        $this->end_time = $date;

        return $this;
    }

    /**
     * Return just the Time component as HH:MM:SS
     *
     * @return DateTime
     */
    public function getStartTime()
    {
        if (!($this->start_time instanceof DateTime)) {
            $this->setStartTime($this->start_time);
        }

        return $this->start_time;
    }

    /**
     * @param DateTime|string $time
     *
     * @return self
     */
    public function setStartTime($time)
    {
        if (!($time instanceof DateTime)) {
            $time = new DateTime($time);
        }
        
        $this->start_time = $time;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        if (!($this->user instanceof User)){
            $this->setUser(new User(intval($this->user_id)));
        }

        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return self
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        $this->setUserId($user->getId());

        return $this;
    }

    /**
     * @param $user_id
     * 
     * @return self
     */
    public function setUserId($user_id)
    {
        $this->user_id = intval($user_id);
        
        return $this;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return intval($this->getUser()->getId());
    }

    /**
     * @return Location
     */
    public function getLocation()
    {
        if (!($this->location instanceof Location)){
            $this->setLocation(new Location(intval($this->location_id)));
        }

        return $this->location;
    }

    /**
     * @param Location $location
     *
     * @return self
     */
    public function setLocation(Location $location)
    {
        $this->location = $location;
        $this->setLocationId($location->getId());

        return $this;
    }

    /**
     * Return just the IDs of available insurance types
     * 
     * @return int
     */
    public function getLocationId()
    {
        return intval($this->getLocation()->getId());
    }

    /**
     * @param $location_id
     *
     * @return self
     */
    public function setLocationId($location_id)
    {
        $this->location_id = intval($location_id);

        return $this;
    }

    /**
     * Return insurances as objects
     * 
     * @return Insurance[]
     */
    public function getInsuranceTypes()
    {
        $insurances = array();
        
        foreach (explode(',', $this->insurance_ids) as $insurance_id) {
            $insurance = new Insurance($insurance_id);
            if ($insurance->isValid()) {
                $insurances[] = $insurance;
            }
        }
        
        return $insurances;
    }

    /**
     * @return array
     */
    public function getInsuranceIds()
    {
        return explode(',', $this->insurance_ids);
    }

    /**
     * @param array|int $insurance_ids
     *
     * @return self
     */
    public function setInsuranceIds($insurance_ids)
    {
        if (!is_array($insurance_ids)) {
            $insurance_ids = array(intval($insurance_ids));
        }

        $insurance_ids = array_filter($insurance_ids, 'intval');
        
        $this->insurance_ids = implode(',', $insurance_ids);

        return $this;
    }

    /**
     * @return array
     */
    public function getTreatmentTypeIds()
    {
        return explode(',', $this->treatment_type_ids);
    }

    /**
     * @param array|int $treatment_type_ids
     *
     * @return self
     */
    public function setTreatmentTypeIds($treatment_type_ids)
    {
        if (!is_array($treatment_type_ids)) {
            $treatment_type_ids = array(intval($treatment_type_ids));
        }

        $treatment_type_ids = array_filter($treatment_type_ids, 'intval');

        $this->treatment_type_ids = implode(',', $treatment_type_ids);

        return $this;
    }

    /**
     * @return TreatmentType[]
     */
    public function getTreatmentTypes()
    {
        $treatment_types = array();
        
        foreach ($this->getTreatmentTypeIds() as $ttid) {
            $tt = new TreatmentType($ttid);
            if ($tt->isValid()) {
                $treatment_types[] = $tt;
            }
        }
        
        return $treatment_types;
    }

    /**
     * Set the treatment types using objects
     * 
     * @param TreatmentType[] $treatment_types
     * 
     * @return self
     */
    public function setTreatmentTypes(array $treatment_types)
    {
        $ttid = array();
        
        foreach ($treatment_types as $tt) {
            if ($tt->isValid()) {
                $ttid[] = $tt;
            }
        }
        
        return $this->setTreatmentTypeIds($ttid);
    }

    /**
     * Returns the url for an appointment. Requires an owning Provider row
     * 
     * @param Provider $provider
     *
     * @return string
     */
    public function getUrl(Provider $provider)
    {
        $app = Application::getInstance();
        $router = $app->container->get('router');
        
        // NOTE: We take the first IDs only at this point because multiple aren't supported
        $ttid = $this->getTreatmentTypeIds();
        $insurance_id = $this->getInsuranceIds();
        
        $parameters = array(
            'u'  => $this->getUserId(),
            'l'  => $this->getLocationId(),
            'm'  => $medical_specialty_id = $provider->getMedicalSpecialtyId(),
            'tt' => $treatmenttype_id = array_shift($ttid),
            'i'  => $insurance_id = array_shift($insurance_id),
            's'  => $this->getDateTimeUrlAttr() // @todo Wrap this in a testable function
        );

        if ($end = $this->getDateTimeEndUrlAttr()) {
            $parameters['e'] = $end;
        }

        $url = $router->generate('appointment');

        // Take care for widgets
        if ($app->isWidget() && $app->getWidgetContainer()->getSlug() != 'berlinde') {
            $url = $router->generate('widget', array('widget_slug' => $app->getWidgetContainer()->getSlug(), 'parameter_string' => $url));
        }

        $url .= '?' . http_build_query($parameters);
        $prefix = '';//$app->deviceIsMobile() ? $GLOBALS['CONFIG']['MOBILE_URL_HTTP_LIVE'] : $GLOBALS['CONFIG']['URL_HTTPS_LIVE'];

        return "{$prefix}{$app->i18nTranslateUrl($url)}";
    }
    
    /**
     * Returns the datetime in a given format
     *
     * @deprecated Use getDateFormat instead
     * 
     * @param string $format
     *
     * @return string
     */
    public function getFormat($format = 'Y-m-d')
    {
        return $this->getDateFormat($format);
    }

    /**
     * Returns the datetime_end in a given format
     *
     * @param string $format
     *
     * @return string
     */
    public function getEndFormat($format = 'd.m.Y')
    {
        return $this->getEndTime()->format($format);
    }

    /**
     * Returns the formatted date
     *
     * @param string $format
     * 
     * @return string
     */
    public function getDateFormat($format = 'd.m.Y')
    {
        return $this->getStartTime()->format($format);
    }

    /**
     * Returns the date
     *
     * @return string
     */
    public function getDateText()
    {
        return $this->getDateFormat('Y.m.d');
    }

    /**
     * Returns the datetime
     *
     * @return string
     */
    public function getDateTime()
    {
        return $this->getDateFormat('Y-m-d');
    }

    /**
     * Returns the datetime in url attr-format '20120325T164500'
     *
     * @return string
     */
    public function getDateTimeUrlAttr()
    {
        return $this->getDateFormat('Ymd\THis');
    }

    /**
     * Returns the datetime end in url attr-format '20120325T164500'
     *
     * @return string
     */
    public function getDateTimeEndUrlAttr()
    {
        return $this->getEndFormat('Ymd\THis');
    }

    /**
     * Returns the datetime in '16:45'-format
     *
     * @return string
     */
    public function getTimeText()
    {
        return $this->getDateFormat('H:i');
    }

    /**
     * Returns the weekday and date
     * 
     * @todo Replace the date string as parameter with a DateTime object
     *
     * @param bool $short_weekday
     *
     * @return string
     */
    public function getWeekdayDateText($short_weekday = false)
    {
        return Calendar::getWeekdayDateText($this->getDateFormat(), $short_weekday);
    }

    /**
     * Returns the datetime in 'Sonntag 25.03.2012 um 16:45 Uhr'-format
     *
     * @todo Replace the date string as parameter with a DateTime object
     * 
     * @param bool $short
     *
     * @return string
     */
    public function getDateTimeText($short = false)
    {
        return Calendar::getWeekdayDateTimeText($this->getDateFormat('Y-m-d H:i:s'), $short);
    }
}