<?php

namespace Arzttermine\Appointment;

use Arzttermine\Core\DateTime;
use When\When;

/**
 * This service is designed to generate appointments for doctors and practices based on their planners
 */
class AppointmentGeneratorService {

    /**
     * @var self
     */
    protected static $instance;

    /**
     * Prevent direct instantiation
     */
    protected function __construct()
    {
    }

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }
    
    /**
     * Generate an array of appointments based on a template calendar
     * Note: You can also generate appointments using a temporary Planner as a proxy
     * without committing it to the database
     * 
     * @param Planner $planner
     * @param PlannerBlock[] $blocks If this is null, Planner::getBlocks will be used
     *
     * @return array Nested array of [<Date> => Appointment[]]
     */
    public function generate(Planner $planner, $blocks = null)
    {
        $recurring_blocks = array(); // Move recurring events here for processing later
        
        $doctor = $planner->getUser();
        $practice = $planner->getLocation();
        
        // If we aren't using the second parameter, assume we want to use all blocks from a planner
        if (is_null($blocks)) {
            $blocks = $planner->getBlocks();
        }
        
        // Figure out what blocks are supposed to be recurring
        // @todo Refactor to avoid double looping
        foreach ($blocks as $index => $block) {
            // If there's an interval, get intermediary blocks
            if ($block->getRecurrenceInterval()) {
                $recurring_blocks = array_merge($recurring_blocks, $this->generateRecurring($block));
                
                // Remove the original block because we have it inside the array above
                unset($blocks[$index]);
            }
        }
        
        // Merge current blocks with replicated ones
        $blocks = array_merge($blocks, $recurring_blocks);
        
        // Generate appointments for each block we now have
        foreach ($blocks as $block) {
            // *sad trombone*
            if (!($block instanceof PlannerBlock)) {
                continue;
            }

            $start_time = clone $block->getStartTime();
            $end_time   = clone $block->getEndTime();

            // Get the entire block (in minutes) of our start/end so we can compare it to the event block
            // Note: This is completely awful, thanks to PHP's terrible DateTime handling
            $timeslot = abs($end_time->getTimestamp() - $start_time->getTimestamp()) / 60;
            
            // Take chunks of time off and generate appointments for our event
            while ($timeslot) {
                // If we don't have AT LEAST the number of minutes left in the timeslot for another appointment, stop adding them
                if ($timeslot < $block->getLength()) {
                    break;
                }
                
                $appointment = new Appointment();
                $appointment
                    ->setUser($doctor)
                    ->setLocation($practice)
                    ->setDate($start_time->getDate())
                    ->setInsuranceIds($block->getInsuranceIds())
                    ->setTreatmentTypeIds($block->getTreatmentTypeIds())
                    ->setStartTime($start_time->getDateTime());

                // Add the block to the start time to force the loop and get the end of the block
                $start_time->modify("+{$block->getLength()} minutes");

                // Update the appointment with the end date
                $appointment->setEndTime($start_time->getDateTime()); // Start is now (start + block length)

                $appointment->saveNewObject();
                unset($appointment); // Memory management

                // Reduce the available timeslot by the block length. If they're the same size, this loop is one iteration only
                $timeslot -= $block->getLength();
            }
        }
        
        return $this;
    }

    /**
     * @param PlannerBlock $template
     *
     * @return PlannerBlock[]
     */
    protected function generateRecurring(PlannerBlock $template)
    {
        $weeks_to_repeat = 12; // @todo: Make this configurable
        $recurring_events = array();

        // Get base times
        $start_date = $template->getStartTime();
        $end_date   = $template->getEndTime();
        
        // Repeat until...
        // If we have a date set, use it, otherwise default to the given offset
        if ($recur_until = $template->getRecurUntil()) {
            $final_date = new DateTime($recur_until);
        } else {
            $final_date = new DateTime();
            $final_date->modify("+{$weeks_to_repeat} weeks"); // Add the number of weeks to repeat
        }

        // If for some reason the repeatUntil date is BEFORE the start date, just make them the same (i.e. failure today)
        if ($final_date < $start_date) {
            $final_date = $start_date;
        }
        
        // Our recurrence rule decides the dates
        $recurrence_rule = new When();
        $recurrence_rule
            ->startDate($start_date)
            ->freq($template->getRecurrenceInterval() === 7 ? 'weekly' : 'daily') // Kludge the frequency as we have to use constants
            ->until($final_date)
            ->generateOccurrences();
        
        /** @var \DateTime $datetime */
        foreach ($recurrence_rule->occurrences as $datetime) {
            // Clone times to prevent reference problems
            $start = clone $start_date;
            $end   = clone $end_date;
            
            $start->setDate($datetime);
            $end->setDate($datetime);
            
            $planner_block = clone $template;
            $planner_block
                ->setStartTime($start)
                ->setEndTime($end);
            
            if (!$planner_block->isExcludedDate($start)) {
                $recurring_events[] = $planner_block;
            }
        }
        
        return $recurring_events;
    }

    /**
     * Clear all existing appointments for this user
     * 
     * @param Planner $planner
     * 
     * @return self
     */
    public function clear(Planner $planner)
    {
        $_appointment = new Appointment();
        $_appointment->delete(
            'user_id = ' . $planner->getUserId() . ' AND ' . 
            'location_id = ' . $planner->getLocationId()
        );
        
        return $this;
    }
}