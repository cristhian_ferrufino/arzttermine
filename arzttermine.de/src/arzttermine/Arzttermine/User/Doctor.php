<?php

namespace Arzttermine\User;

use Arzttermine\Application\Application;
use Arzttermine\Insurance\Insurance;
use Arzttermine\Location\Location;
use Arzttermine\Media\Asset;
use Arzttermine\MedicalSpecialty\MedicalSpecialty;
use Arzttermine\MedicalSpecialty\TreatmentType;
use Arzttermine\Review\Review;

class Doctor extends User {

    const GENDER_OTHER = -1;
    const GENDER_MALE = 0;
    const GENDER_FEMALE = 1;

    /**
     * Unpublished
     */
    const STATUS_DRAFT = 0;

    /**
     * Show the profile, allow user to login
     */
    const STATUS_ACTIVE = 1;

    /**
     * Show the profile, allow user to login, show profile in search, show no appointments
     */
    const STATUS_VISIBLE = 2;

    /**
     * Show the profile, allow user to login, show profile in search, show appointments
     */
    const STATUS_VISIBLE_APPOINTMENTS = 3;

    /**
     * Contract constants
     */
    const WITHOUT_CONTRACT = 0;
    const WITH_CONTRACT  = 1;

    /**
     * @var string
     */
    protected $tablename = DB_TABLENAME_USERS;

    /**
     * @var string
     */
    protected $slug = '';

    /**
     * @var int
     */
    protected $group_id;

    /**
     * @var int
     */
    protected $admin_group;

    /**
     * @var string
     */
    protected $status = '';

    /**
     * @var string
     */
    protected $title = '';

    /**
     * @var string
     */
    protected $preferred_medical_specialty_text = '';

    /**
     * @var string
     */
    protected $medical_specialty_ids = '';

    /**
     * @var int Actually a bool
     */
    protected $fixed_week;

    /**
     * @var array
     */
    protected $assets = array();

    /**
     * @var Asset
     */
    protected $profile_asset;

    /**
     * @var int
     */
    protected $profile_asset_id;

    /**
     * @var string
     */
    protected $profile_asset_filename = '';

    /**
     * @var int
     */
    protected $has_contract;


    /**
     * @var string
     */
    protected $phone = '';

    /**
     * @var float
     */
    protected $rating_average;

    /**
     * @var float
     */
    protected $rating_1;

    /**
     * @var float
     */
    protected $rating_2;

    /**
     * @var float
     */
    protected $rating_3;

    /**
     * @var int
     */
    protected $newsletter_subscription;

    /**
     * @var string
     */
    protected $docinfo_1 = '';

    /**
     * @var string
     */
    protected $docinfo_2 = '';

    /**
     * @var string
     */
    protected $docinfo_3 = '';

    /**
     * @var string
     */
    protected $docinfo_4 = '';

    /**
     * @var string
     */
    protected $docinfo_5 = '';

    /**
     * @var string
     */
    protected $docinfo_6 = '';

    /**
     * @var string
     */
    protected $info_seo = '';

    /**
     * @var string
     */
    protected $html_title = '';

    /**
     * @var string
     */
    protected $html_meta_description = '';

    /**
     * @var string
     */
    protected $html_meta_keywords = '';

    /**
     * @var string
     */
    protected $comment_intern = '';

    /**
     * @var string
     */
    protected $created_at = '';

    /**
     * @var string
     */
    protected $created_by = '';

    /**
     * @var string
     */
    protected $activation_code = '';

    /**
     * @var string
     */
    protected $activated_at = '';

    /**
     * @var string
     */
    protected $updated_at = '';

    /**
     * @var string
     */
    protected $updated_by = '';

    /**
     * @var string
     */
    protected $last_login_at = '';

    /**
     * Array with all access rights for this users group as it is in the table 'group_accesses'
     *
     * @var array
     */
    protected $group_accesses = array();

    /**
     * @var string
     */
    protected $fullname = '';

    /**
     * @var string
     */
    protected $shortname = '';

    /**
     * @var string
     */
    protected $casualname = '';

    /**
     * @var string
     */
    protected $url = '';

    /**
     * @var array
     */
    protected $all_keys
        = array(
            "slug",
            "status",
            "group_id",
            "email",
            "gender",
            "title",
            "preferred_medical_specialty_text",
            "first_name",
            "last_name",
            "password_hash",
            "password_hash_salt",
            "fixed_week",
            "medical_specialty_ids",
            "profile_asset_id",
            "profile_asset_filename",
            "reset_password_code",
            "has_contract",
            "rating_average",
            "rating_1",
            "rating_2",
            "rating_3",
            "newsletter_subscription",
            "docinfo_1",
            "docinfo_2",
            "docinfo_3",
            "docinfo_4",
            "docinfo_5",
            "docinfo_6",
            "info_seo",
            "html_title",
            "html_meta_description",
            "html_meta_keywords",
            "comment_intern",
            "created_at",
            "created_by",
            "activation_code",
            "activated_at",
            "updated_at",
            "updated_by",
            "last_login_at"
        );

    /**
     * @var array
     */
    protected $secure_keys
        = array(
            "slug",
            "status",
            "group_id",
            "email",
            "gender",
            "title",
            "preferred_medical_specialty_text",
            "first_name",
            "last_name",
            "password_hash",
            "password_hash_salt",
            "fixed_week",
            "medical_specialty_ids",
            "profile_asset_id",
            "profile_asset_filename",
            "reset_password_code",
            "has_contract",
            "rating_average",
            "rating_1",
            "rating_2",
            "rating_3",
            "newsletter_subscription",
            "docinfo_1",
            "docinfo_2",
            "docinfo_3",
            "docinfo_4",
            "docinfo_5",
            "docinfo_6",
            "info_seo",
            "html_title",
            "html_meta_description",
            "html_meta_keywords",
            "comment_intern",
            "activation_code"
        );

    /**
     * Checks the credentials for a user
     *
     * @param email
     * @param password
     * @param groupId
     *
     * @return bool
     */
    public static function checkCredentials($email, $password, $groupId = null)
    {
        $_email = sanitize_email($email);
        if (!is_email($_email)) {
            return false;
        }

        $groupCheck = '';
        if (!empty($groupId) && is_numeric($groupId)) {
            $groupCheck = ' AND group_id=' . $groupId;
        }

        $user = new User();
        $user = $user->findObject('email="' . sqlsave($_email) . '"' . $groupCheck);

        if (!$user->isValid()) {
            return false;
        }

        if ($user->password_hash_salt == '') {
            return false;
        }

        $password_hash = hash('sha256', PASSWORD_FIX_SALT . $password . $user->password_hash_salt);

        return ($password_hash == $user->password_hash);
    }

    /**
     * @return string
     */
    public function getLastLoginAt()
    {
        return $this->last_login_at;
    }

    /**
     * @return int
     */
    public function getHasContract()
    {
        return intval($this->has_contract);
    }

    /**
     * @return bool
     */
    public function hasContract()
    {
        return intval($this->getHasContract()) === 1;
    }

    /**
     * @return int
     */
    public function getPaidReviews()
    {
        return intval($this->paid_reviews);
    }

    /**
     * @return bool
     */
    public function hasPaidReview()
    {
        return intval($this->getPaidReviews()) === 1;
    }

    /**
     * Set internal vars
     */
    public function calcVars()
    {
        $profile = Application::getInstance()->getCurrentUser();

        // set the names
        $this->fullname = $this->title;
        $this->fullname .= (empty($this->fullname) ? '' : ' ') . $this->first_name;
        $this->fullname .= (empty($this->fullname) ? '' : ' ') . $this->last_name;

        $this->shortname = $this->first_name;
        $this->shortname .= (empty($this->shortname) ? '' : ' ');
        $this->shortname .= (empty($this->last_name) ? '' : mb_substr($this->last_name, 0, 1, $GLOBALS['CONFIG']['SYSTEM_CHARSET']) . '.');

        if ($slug = $this->getSlug()) {
            if ($this->isMemberOf(GROUP_DOCTOR)) {
                $this->setUrl('/arzt/' . $slug);
            } else {
                $this->setUrl('/' . $slug);
            }
        }

        // if the profile user is logged in than show the fullname
        if (isset($profile) && $profile->isValid()) {
            $this->casualname = $this->fullname;
        } else {
            $this->casualname = $this->shortname;
        }
    }

    /**
     * Check the user's group
     *
     * @param $group
     *
     * @return bool
     */
    public function isMemberOf($group)
    {
        return $this->group_id == $group;
    }

    /**
     * Returns the users slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     *
     * @return self
     */
    public function setSlug($slug = '')
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @param string $url
     *
     * @return self
     */
    public function setUrl($url)
    {
        $this->url = trim($url);

        return $this;
    }

    /**
     * @return string
     */
    public function getAdminEditUrl()
    {
        return '/administration/users?action=edit&id=' . $this->getId();
    }

    /**
     * @return int
     */
    public function getProfileAssetId()
    {
        return $this->profile_asset_id;
    }

    /**
     * Set the profile_asset_id
     *
     * @param int $id
     *
     * @return self
     */
    public function setProfileAssetId($id)
    {
        $this->profile_asset_id = intval($id);

        return $this;
    }

    /**
     * @return string
     */
    public function getProfileAssetFilename()
    {
        return $this->profile_asset_filename;
    }

    /**
     * Set the profile_asset_filename
     *
     * @param string $filename
     *
     * @return self
     */
    public function setProfileAssetFilename($filename)
    {
        $this->profile_asset_filename = trim($filename);

        return $this;
    }

    /**
     * Returns the url to the profile asset/gfx for a size
     * if there is no one than return the url to the default asset/gfx
     *
     * @param int $size
     * @param bool $add_host
     *
     * @return string
     */
    public function getProfileAssetUrl($size = ASSET_GFX_SIZE_ORIGINAL, $add_host = false)
    {
        // check if the url can be calculated by the id and filename
        if (is_numeric($this->profile_asset_id) && $this->profile_asset_id > 0 && $this->profile_asset_filename != '') {
            $url = Asset::getUrlByIdFilenameOwner($this->profile_asset_id, $this->profile_asset_filename, ASSET_OWNERTYPE_USER, $size, $add_host);
            if ($url) {
                return $url;
            }
        }
        // calc the asset
        $this->loadProfileAsset();
        if (is_object($this->profile_asset) && $this->profile_asset->isValid()) {
            return $this->profile_asset->getGfxUrl($size, $add_host);
        } else {
            // get the index for the asset by the last name
            // would be so much easier to use random assets but in this case
            // we want fixed images for each doctor
            if ($this->getGroupId() == GROUP_DOCTOR) {
                $lastName = $this->getLastName();
                $firstChar = mb_strtolower($lastName[0]);
                $abc = 'abcdefghijklmnopqrstuvwxyz';
                $position = strpos($abc, $firstChar);
                // calc the index
                $index = floor($position / strlen($abc) * sizeof($GLOBALS['CONFIG']['ASSET_GFX_TYPES'][ASSET_OWNERTYPE_USER]['SIZES'][$size]['default_group_doctors'][$this->getGender()]));
                $filename = $GLOBALS['CONFIG']['ASSET_GFX_TYPES'][ASSET_OWNERTYPE_USER]['SIZES'][$size]['default_group_doctors'][$this->getGender()][$index];
            } else {
                $filename = $GLOBALS['CONFIG']['ASSET_GFX_TYPES'][ASSET_OWNERTYPE_USER]['SIZES'][$size]['default'];
            }
            return getStaticUrl($filename, $add_host);
        }
    }

    /**
     * Sets the profile asset and calcs the total of assets
     */
    public function loadProfileAsset()
    {
        // set the profile asset
        if ($this->profile_asset_id != 0) {
            $_profile_asset = new Asset($this->profile_asset_id);
            if ($_profile_asset->isValid()) {
                $this->profile_asset = $_profile_asset;
            }
        } elseif ($GLOBALS['CONFIG']['ASSET_USE_LAST_ASSET_AS_PROFILE_ASSET']) {
            $_asset = new Asset();
            $_profile_asset = $_asset->findObject('parent_id=0 AND owner_id=' . $this->getId() . ' AND owner_type=' . ASSET_OWNERTYPE_USER . ' ORDER BY created_at DESC');
            if ($_profile_asset->isValid()) {
                $this->profile_asset = $_profile_asset;
            }
        }
    }

    /**
     * Updates the profile_asset_filename
     * This is mainly for caching purpose
     *
     * @return bool
     */
    public function updateProfileAssetFilename()
    {
        $result = false;
        $this->loadProfileAsset();

        if (is_object($this->profile_asset) && $this->profile_asset->isValid()) {
            $this->profile_asset_id = $this->profile_asset->getId();
            $this->profile_asset_filename = $this->profile_asset->getFilename();
            $result = $this->save(
                array(
                    'profile_asset_id',
                    'profile_asset_filename'
                )
            );
        }

        return $result;
    }

    /**
     * Counts the number of assets that are uploaded to this object
     *
     * @return int
     */
    public function countAssets()
    {
        $_asset = new Asset();

        return $_asset->count('parent_id=0 AND owner_id=' . $this->id . ' AND owner_type=' . ASSET_OWNERTYPE_USER);
    }

    /**
     * @param int $newsletter_subscription
     *
     * @return self
     */
    public function setNewsletterSubscription($newsletter_subscription)
    {
        $this->newsletter_subscription = (int)(bool)$newsletter_subscription;

        return $this;
    }

    /**
     * @return int
     */
    public function getNewsletterSubscription()
    {
        return (int)(bool)$this->newsletter_subscription;
    }

    /**
     * Returns the asset on the nr position (order by created_at)
     *
     * @param int $nr
     *
     * @return string
     */
    public function getAssetByNr($nr)
    {
        if ($nr < 1) {
            return false;
        }

        $nr--;
        $_asset = new Asset();
        $_assets = $_asset->findObjects('parent_id=0 AND owner_id=' . $this->getId() . ' AND owner_type=' . ASSET_OWNERTYPE_USER . ' ORDER BY created_at');

        return isset($_assets[$nr]) ? $_assets[$nr] : false;
    }

    /**
     * Shows $num thumbs and creates the fancybox items
     *
     * @param int $num
     * @param bool $show_profile_asset
     *
     * @return array assets
     */
    public function getAssetThumbsHtml($num, $show_profile_asset = false)
    {
        $view = Application::getInstance()->getView();

        $assets = $this->getAssets();
        $asset_thumbs = array();
        $asset_hiddens = array();
        $x = 1;

        /** @var Asset[] $assets */
        foreach ($assets as $asset) {
            if (!$show_profile_asset) {
                if (
                    ($this->profile_asset_id == $asset->getId()) ||
                    ($this->profile_asset_id == 0)
                ) {
                    continue;
                }
            }
            if ($x <= $num) {
                $asset_thumbs[] = $asset;
            } else {
                $asset_hiddens[] = $asset;
            }
            $x++;
        }

        $view->setRef('asset_thumbs', $asset_thumbs);
        $view->setRef('asset_hiddens', $asset_hiddens);

        return $view->fetch('_gallery_thumbs.tpl');
    }

    /**
     * Returns all assets for this object
     *
     * @return Asset[]
     */
    public function getAssets()
    {
        if (!empty($this->assets) && is_array($this->assets)) {
            return $this->assets;
        } else {
            return $this->loadAssets();
        }
    }

    /**
     * Loads and returns all assets for this object
     *
     * @return array assets
     */
    public function loadAssets()
    {
        $_asset = new Asset();
        $_assets = $_asset->findObjects('parent_id=0 AND owner_id=' . $this->id . ' AND owner_type=' . ASSET_OWNERTYPE_USER . ' ORDER BY created_at ASC');
        $this->assets = $_assets;

        return $this->assets;
    }

    /**
     * Change the password for a user
     *
     * @param password
     *
     * @return bool
     */
    public function changePassword($password)
    {
        $this->password_hash_salt = substr(
            str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 6
        );
        $this->password_hash = hash('sha256', PASSWORD_FIX_SALT . $password . $this->password_hash_salt);
        $this->reset_password_code = null;

        return $this->save(
            array(
                'password_hash_salt',
                'password_hash',
                'reset_password_code'
            )
        );
    }

    /**
     * Returns a new Password Code
     *
     * @return string
     */
    public function generateResetPasswordCode()
    {
        return substr(md5(uniqid()), 0, 6);
    }

    /**
     * @param string $reset_password_code
     *
     * @return self
     */
    public function setResetPasswordCode($reset_password_code)
    {
        $this->reset_password_code = strtolower(trim($reset_password_code));

        return $this;
    }

    /**
     * @return string
     */
    public function getResetPasswordCode()
    {
        return $this->reset_password_code;
    }

    /**
     * {@inheritdoc}
     */
    public function delete($where = null)
    {
        $sql = "DELETE FROM " . $this->tablename . " WHERE id=" . $this->id;
        Application::getInstance()->getSql()->query($sql);

        return true;
    }

    /**
     * sets the last login datetime
     *
     * @param string $datetime
     *
     * @return bool
     */
    public function setLast_login_at($datetime)
    {
        $this->last_login_at = $datetime;

        return $this->save("last_login_at");
    }

    /**
     * check if the given User is allowed to do the action
     *
     * @param string $action
     * @param array $data array with more details that are needed for this action
     *
     * @return bool
     */
    public function checkPrivilege($action, $data = array())
    {
        $privilege = false;

        if (!$this->isValid()) {
            return $privilege;
        }

        switch ($action) {

            case 'user_view':
            case 'user_gallery_admin':
                if ($this->group_id == ADMIN_GROUP_ADMIN) {
                    $privilege = true;
                    break;
                }
                break;
            case 'user_gfx_new':
            case 'user_gallery_new':
            case 'user_profile_edit':
                if ($this->admin_group == ADMIN_GROUP_ADMIN || (isset($data['user_id']) && $this->id == $data['user_id'])) {
                    $privilege = true;
                }
                break;

            default:
                return $this->hasGroupAccess($action);
                break;
        }

        return $privilege;
    }

    /**
     * checks if this user has access to a group function
     *
     * @param int $access_id
     *
     * @return bool
     */
    private function hasGroupAccess($access_id)
    {
        $db = Application::getInstance()->getSql();

        if (empty($this->group_accesses)) {
            // get the group access rights for this users group
            $sql = "SELECT access_id FROM " . DB_TABLENAME_GROUP_ACCESSES . " WHERE group_id=" . $this->group_id . ";";
            $db->query($sql);
            if ($db->nf() > 0) {
                while ($db->next_record()) {
                    $this->group_accesses[] = $db->f('access_id');
                }
            }
        }
        if (!is_array($this->group_accesses)) {
            return false;
        }

        return (bool)in_array($access_id, $this->group_accesses);
    }

    /**
     * Checks if an email is available for registration
     *
     * @param string $email
     *
     * @return bool
     */
    public static function isEmailAvailable($email)
    {
        $user = new self($email, 'email');
        return !$user->isValid();
    }

    /**
     * Activate a doctor (only triggers the date field)
     */
    public function activate()
    {
        return $this->setActivatedAt(date('Y-m-d H:i:s'))->save('activated_at');
    }

    /**
     * @param string $activated_at
     *
     * @return self
     */
    public function setActivatedAt($activated_at)
    {
        $this->activated_at = $activated_at;

        return $this;
    }

    /**
     * @return string
     */
    public function getActivatedAt()
    {
        return $this->activated_at;
    }

    /**
     * @param string $activation_code
     *
     * @return self
     */
    public function setActivationCode($activation_code)
    {
        $this->activation_code = $activation_code;

        return $this;
    }

    /**
     * @return string
     */
    public function getActivationCode()
    {
        return $this->activation_code;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param string $updated_at
     *
     * @return self
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @param string $updated_by
     *
     * @return self
     */
    public function setUpdatedBy($updated_by)
    {
        $this->updated_by = $updated_by;

        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedBy()
    {
        return $this->updated_by;
    }

    /**
     * returns the users title
     *
     * @param
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = trim($title);

        return $this;
    }

    /**
     * returns the users first name
     *
     * @param
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * returns the users last name
     *
     * @param
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @param string $first_name
     *
     * @return self
     */
    public function setFirstName($first_name)
    {
        $this->first_name = trim($first_name);

        return $this;
    }

    /**
     * @param string $last_name
     *
     * @return self
     */
    public function setLastName($last_name)
    {
        $this->last_name = trim($last_name);

        return $this;
    }

    /**
     * checks if this object has a profile page
     *
     * @return bool
     */
    public function hasProfile()
    {
        return !empty($this->slug);
    }

    /**
     * @param string $email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Returns the permalink
     *
     * @return string
     */
    public function getPermalink()
    {
        return Application::getInstance()->i18nTranslateUrl($this->getUrl(true));
    }

    /**
     * @param bool $absolute
     * @param bool $secure
     *
     * @return string
     */
    public function getUrl($absolute = false, $secure = false)
    {
        return ($absolute ? ($secure ? $GLOBALS['CONFIG']['URL_HTTPS_LIVE'] : $GLOBALS['CONFIG']['URL_HTTP_LIVE']) : '') . $this->url;
    }

    /**
     * Returns this objects specific html title
     *
     * @return string
     */
    public function getHtmlTitle()
    {
        return $this->hasHtmlTitle() ? $this->html_title : $this->getHtmlDefaultTitle();
    }

    /**
     * Checks if this object has a specific html title
     *
     * @return bool
     */
    public function hasHtmlTitle()
    {
        return !empty($this->html_title);
    }

    /**
     * Returns this objects default html title
     *
     * @return string
     */
    public function getHtmlDefaultTitle()
    {
        $locations = $this->getLocations();
        $cities = array();
        if (!empty($locations)) {
            foreach ($locations as $location) {
                $cities[] = $location->getCity();
            }
        }
        array_unique($cities);
        $citiesString = implode(', ', $cities);

        return
            $this->getName() .
            ', ' .
            $this->getMedicalSpecialityIdsText(' und ') .
            (empty($citiesString) ? '' : ' in ') .
            $citiesString;
    }

    /**
     * returns the users name
     *
     * @return string
     */
    public function getName()
    {
        if ($this->isValid()) {
            return $this->getFullName();
        } else {
            return 'ungültiger User';
        }
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return trim("{$this->title} {$this->first_name} {$this->last_name}");
    }

    /**
     * Returns the Texts for this objects medical specialties
     *
     * @param string $delimiter
     * @param string $delimiter_close
     *
     * @return string
     */
    public function getMedicalSpecialityIdsText($delimiter = ', ', $delimiter_close = '')
    {
        $_medical_specialty = new MedicalSpecialty();

        return getCategoriesText(
            $this->medical_specialty_ids,
            $_medical_specialty->getMedicalSpecialtyValues(),
            $delimiter,
            $delimiter_close
        );
    }

    /**
     * Returns this objects specific html meta description
     *
     * @return string
     */
    public function getHtmlMetaDescription()
    {
        return $this->hasHtmlMetaDescription() ? $this->html_meta_description : $this->getHtmlDefaultMetaDescription();
    }

    /**
     * Checks if this object has a specific html meta description
     *
     * @return bool
     */
    public function hasHtmlMetaDescription()
    {
        return !empty($this->html_meta_description);
    }

    /**
     * Returns this objects default html meta description
     *
     * @return string
     */
    public function getHtmlDefaultMetaDescription()
    {
        return
            'Bei ' .
            $this->getMedicalSpecialityIdsText(' und ') .
            ' ' .
            $this->getName() .
            ' sofort einen Termin bekommen. Buchen Sie jetzt Ihren Termin online auf Arzttermine.de.';
    }

    /**
     * Returns this objects specific html meta keywords
     *
     * @return string
     */
    public function getHtmlMetaKeywords()
    {
        return $this->hasHtmlMetaKeywords() ? $this->html_meta_keywords : $this->getHtmlDefaultMetaKeywords();
    }

    /**
     * Checks if this object has a specific html meta keywords
     *
     * @return bool
     */
    public function hasHtmlMetaKeywords()
    {
        return !empty($this->html_meta_keywords);
    }

    /**
     * Returns this objects default html meta keywords
     *
     * @return string
     */
    public function getHtmlDefaultMetaKeywords()
    {
        return
            $this->getName() .
            ', ' .
            $this->getMedicalSpecialityIdsText(', ') .
            ', Termin buchen, Arzttermin';
    }

    /**
     * Returns the status id
     *
     * @return string
     */
    public function getStatus()
    {
        return intval($this->status);
    }

    /**
     * @param string $status
     *
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = intval($status);

        return $this;
    }

    /**
     * Returns the status text
     *
     * @param bool $sanitize_to_text
     *
     * @return string
     */
    public function getStatusText($sanitize_to_text = false)
    {
        $_status_text = $GLOBALS['CONFIG']['USER_STATUS'][$this->status];

        if ($sanitize_to_text) {
            $_status_text = sanitize_title($_status_text);
        }

        return $_status_text;
    }

    /**
     * Returns the phone number
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     *
     * @return self
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Checks if a custom specialty medical specialty text provided by the doctor exists
     *
     * @return bool
     */
    public function hasPreferredMedicalSpecialtyText()
    {
        return !empty($this->preferred_medical_specialty_text);
    }

    /**
     * Returns the a custom specialty medical specialty text provided by the doctor
     *
     * @return string
     */
    public function getPreferredMedicalSpecialtyText()
    {
        return $this->preferred_medical_specialty_text;
    }

    /**
     * @param bool $fixed_week
     *
     * @return self
     */
    public function setFixedWeek($fixed_week = true)
    {
        $this->fixed_week = (int)$fixed_week;

        return $this;
    }

    /**
     * @return bool
     */
    public function getFixedWeek()
    {
        return (bool)$this->fixed_week;
    }

    /**
     * Returns a formated info text
     *
     * @param int $id
     *
     * @return string
     */
    public function getInfoText($id)
    {
        if (!isset($this->$id)) {
            return '';
        }

        return nl2br(do_shortcode(wpautop($this->$id)));
    }

    /**
     * @param string $docinfo_1
     *
     * @return self
     */
    public function setDocinfo1($docinfo_1)
    {
        $this->docinfo_1 = filter_var(trim($docinfo_1), FILTER_SANITIZE_STRING);

        return $this;
    }

    /**
     * @return string
     */
    public function getDocinfo1()
    {
        return $this->docinfo_1;
    }

    /**
     * @param string $docinfo_2
     *
     * @return self
     */
    public function setDocinfo2($docinfo_2)
    {
        $this->docinfo_2 = filter_var(trim($docinfo_2), FILTER_SANITIZE_STRING);

        return $this;
    }

    /**
     * @return string
     */
    public function getDocinfo2()
    {
        return $this->docinfo_2;
    }

    /**
     * @param string $docinfo_3
     *
     * @return self
     */
    public function setDocinfo3($docinfo_3)
    {
        $this->docinfo_3 = filter_var(trim($docinfo_3), FILTER_SANITIZE_STRING);

        return $this;
    }

    /**
     * @return string
     */
    public function getDocinfo3()
    {
        return $this->docinfo_3;
    }

    /**
     * @param string $docinfo_4
     *
     * @return self
     */
    public function setDocinfo4($docinfo_4)
    {
        $this->docinfo_4 = filter_var(trim($docinfo_4), FILTER_SANITIZE_STRING);

        return $this;
    }

    /**
     * @return string
     */
    public function getDocinfo4()
    {
        return $this->docinfo_4;
    }

    /**
     * @param string $docinfo_5
     *
     * @return self
     */
    public function setDocinfo5($docinfo_5)
    {
        $this->docinfo_5 = filter_var(trim($docinfo_5), FILTER_SANITIZE_STRING);

        return $this;
    }

    /**
     * @return string
     */
    public function getDocinfo5()
    {
        return $this->docinfo_5;
    }

    /**
     * @param string $docinfo_6
     *
     * @return self
     */
    public function setDocinfo6($docinfo_6)
    {
        $this->docinfo_6 = filter_var(trim($docinfo_6), FILTER_SANITIZE_STRING);

        return $this;
    }

    /**
     * @return string
     */
    public function getDocinfo6()
    {
        return $this->docinfo_6;
    }

    /**
     * Returns the MedicalSpecialtiesIds in an array
     * Notice: Ids not checked for validity
     *
     * @return array
     */
    public function getMedicalSpecialtyIds()
    {
        return explode(',', $this->medical_specialty_ids);
    }

    /**
     * @param int $group_id
     *
     * @return self
     */
    public function setGroupId($group_id)
    {
        $this->group_id = $group_id;

        return $this;
    }

    /**
     * @return int
     */
    public function getGroupId()
    {
        return $this->group_id;
    }

    /**
     * @param int $gender
     *
     * @return self
     */
    public function setGender($gender)
    {
        if (in_array(intval($gender), array_keys($GLOBALS['CONFIG']['ARRAY_GENDER']))) {
            $this->gender = intval($gender);
        }

        return $this;
    }

    /**
     * returns the users gender
     *
     * @return int gender ($CONFIG["ARRAY_GENDER"] => 0=male/1=female)
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * returns the users gender text
     *
     * @return string gender ($CONFIG["ARRAY_GENDER"] => 0=male/1=female)
     */
    public function getGenderText()
    {
        return $GLOBALS['CONFIG']['ARRAY_GENDER'][$this->gender];
    }

    /**
     * Returns an array with the breadcrumbs
     *
     * @param array $locations
     * @param array $filter
     *
     * @return array
     */
    public function getBreadcrumbs($locations, $filter = array())
    {
        $app = Application::getInstance();

        $breadcrumbs = array();
        $breadcrumbLocations = array(); // show all locations only once

        /** @var Location[] $locations */
        foreach ($locations as $location) {
            // add the breadcrumbs
            if (empty($breadcrumbLocations[$location->getCity()])) {

                /** @var MedicalSpecialty[] $medicalSpecialties */
                $medicalSpecialties = $this->getMedicalSpecialties();
                foreach ($medicalSpecialties as $medicalSpecialty) {
                    $insuranceUrl = '';
                    // if the insurance is selected
                    if (isset($filter['insurance_id'])) {
                        $insurance = new Insurance($filter['insurance_id'], 'id');
                        if ($insurance->isValid()) {
                            $insuranceUrl = '/' . $insurance->getSlug();
                        }
                    }
                    $breadcrumb = array(
                        array(
                            'url'   => $app->i18nTranslateUrl('/'),
                            'title' => 'Startseite',
                            'text'  => 'Arzttermine.de'
                        ),
                        array(
                            'url'  => $app->i18nTranslateUrl(
                                '/' .
                                $medicalSpecialty->getSlug() .
                                '/' .
                                sanitize_slug($location->getCity()) .
                                $insuranceUrl
                            ),
                            'text' => $medicalSpecialty->getName() . ' in ' . $location->getCity()
                        ),
                        array(
                            'url'  => $this->getUrlWithFilter($filter),
                            'text' => $this->getName()
                        )
                    );
                    $breadcrumbs[] = $breadcrumb;
                }
                // remember that we allready used that city
                $breadcrumbLocations[$location->getCity()] = true;
            }
        }

        return $breadcrumbs;
    }

    /**
     * Returns the MedicalSpecialties objects for this object
     *
     * @return MedicalSpecialty[]
     */
    public function getMedicalSpecialties()
    {
        $specialties = array();

        foreach ($this->getMedicalSpecialtyIds() as $msid) {
            $ms = new MedicalSpecialty($msid);
            if ($ms->isValid()) {
                $specialties[$msid] = $ms;
            }
        }

        return $specialties;
    }

    /**
     * @param array $medical_specialty_ids
     *
     * @return self
     */
    public function setMedicalSpecialtyIds(array $medical_specialty_ids = array())
    {
        $this->medical_specialty_ids = implode(',', $medical_specialty_ids);

        return $this;
    }

    /**
     * @param MedicalSpecialty[] $medical_specialties
     *
     * @return $this
     */
    public function setMedicalSpecialties(array $medical_specialties)
    {
        $medical_specialty_ids = array();

        foreach ($medical_specialties as $ms) {
            $medical_specialty_ids[] = $ms->getId();
        }

        array_unique($medical_specialty_ids);

        $this->setMedicalSpecialtyIds($medical_specialty_ids);

        return $this;
    }

    /**
     * Return the first medical specialty the doctor has (default)
     *
     * @return MedicalSpecialty
     */
    public function getDefaultMedicalSpecialty()
    {
        $specialties = $this->getMedicalSpecialties();
        return array_shift($specialties);
    }

    /**
     * @return TreatmentType[]
     */
    public function getTreatmentTypes()
    {
        $db = Application::getInstance()->getSql();
        $treatment_types = array();

        $sql = "SELECT * FROM " . DB_TABLENAME_DOCTOR_TREATMENTTYPES . " WHERE medical_specialty_id IN (" . implode(',', $this->getMedicalSpecialtyIds()) . ") AND doctor_id = {$this->getId()}";
        $db->query($sql);

        if ($db->nf() > 0) {
            $datasets = array();

            while ($db->next_record()) {
                $datasets[] = array(
                    'doctor_id'            => $db->f('doctor_id'),
                    'medical_specialty_id' => $db->f('medical_specialty_id'),
                    'treatment_type_id'    => $db->f('treatment_type_id')
                );
            }

            foreach ($datasets as $dataset) {
                $tt = new TreatmentType($dataset['treatment_type_id']);

                if ($tt->isValid()) {
                    $treatment_types[$tt->getId()] = $tt;
                }
            }
        }

        return $treatment_types;
    }



    /**
     * Returns the url with filter attrs
     *
     * NOTE: A broken call using a blank $filter_array was crashing this
     *
     * @param array $filter_array
     * @param string $name
     * @param bool $absolute_url
     *
     * @return string
     */
    public function getUrlWithFilter($filter_array, $name = 'url', $absolute_url = false)
    {
        $_url = $this->getUrl($absolute_url);
        $_attrs = '';

        foreach ((array)$filter_array as $filter_id => $filter_value) {
            switch ($filter_id) {
                case 'insurance_id':
                    if (isset($GLOBALS['CONFIG']['INSURANCE_SLUGS'][$filter_value])) {
                        $_attrs = '/' . $GLOBALS['CONFIG']['INSURANCE_SLUGS'][$filter_value];
                    }
                    break;
            }
        }

        return Application::getInstance()->i18nTranslateUrl($_url . $_attrs);
    }

    /**
     * Returns the reviews for a user
     *
     * @return array Review
     */
    public function getReviews()
    {
        if (!$this->isValid()) {
            return null;
        }

        return Review::getReviewsForUser($this->id);
    }

    /**
     * Returns the number of reviews for a user
     *
     * @return int
     */
    public function countReviews()
    {
        if (!$this->isValid()) {
            return null;
        }

        return Review::countReviewsForUser($this->id);
    }

    /**
     * Updates the ratings for the user and stores them in the db
     *
     * @return bool
     */
    public function updateRatings()
    {
        $review = new Review();
        $ratings = $review->getRatingsAverageArrayForUser($this->getId());

        if (is_array($ratings) && !empty($ratings)) {
            $this->rating_1 = $ratings['rating_1'];
            $this->rating_2 = $ratings['rating_2'];
            $this->rating_3 = $ratings['rating_3'];
            $this->rating_average = $ratings['rating_average'];

            return $this->save(
                array(
                    'rating_1',
                    'rating_2',
                    'rating_3',
                    'rating_average'
                )
            );
        }

        return false;
    }

    /**
     * Updates the ratings for all locations where this user is connected to and stores them in the db
     *
     * @return bool
     */
    public function updateRatingsForLocations()
    {
        $locations = $this->getLocations();

        if (empty($locations)) {
            return false;
        }

        foreach ($locations as $location) {
            $location->updateRatings();
        }

        return true;
    }

    /**
     * Returns the users locations, filtering out ones that are not available
     *
     * @return Location[]
     */
    public function getLocations()
    {
        return array_filter(Location::getLocationsByUser($this->getId()), function($_location) {
                /** @var Location $_location */
                return $_location->isVisible();
            });
    }

    /**
     * Returns the rating value for a key
     *
     * @param string $key
     *
     * @return float
     */
    public function getRating($key = 'rating_average')
    {
        $value = 0;

        switch ($key) {
            case 'rating_1':
            case 'rating_2':
            case 'rating_3':
            case 'rating_average':
                $value = $this->$key;
                break;
        }

        return $value;
    }

    /**
     * - Dont show ratings if the user has no reviews
     *
     * @return bool
     */
    public function showReviews()
    {
        return $this->rating_average > 0;
    }

    /**
     * returns the rating html
     *
     * @param string $key
     * @param string $text
     * @param int $location_id
     *
     * @return string
     */
    public function getRatingHtml($key, $text, $location_id = null)
    {
        if ($location_id) {
            $location_id = '-l' . $location_id;
        } else {
            $location_id = '';
        }

        $_html = '<div class="rating"><div class="text">' . Application::getInstance()
                ->_($text) . '</div><div class="stars">';

        for ($x = 0.5; $x <= 5; $x += 0.5) {
            if ($this->$key >= $x) {
                $_checked = ' checked="checked"';
            } else {
                $_checked = '';
            }
            $_html .= '<input class="star {split:2}" type="radio" name="rating-u' . $this->id . $location_id . '-' . $key . '" value="' . $x . '" disabled="disabled"' . $_checked . ' />';
        }

        $_html .= '</div></div><div class="clear"></div>';

        return $_html;
    }

    /**
     * Get only a given set of users according to IDs
     *
     * @param array $userIds
     * @param bool $force_order
     *
     * @return array User
     */
    public function getUsersByIds(array $userIds, $force_order = false)
    {
        $orderBy = '';

        // Force the DB to return the IDs as we supplied them
        if ($force_order) {
            $orderBy = 'ORDER BY FIELD(`id`, ' . implode(',', $userIds) . ')';
        }

        $where = sprintf("`id` IN (%s) %s", implode(',', $userIds), $orderBy);

        return $this->findObjects($where);
    }

    /**
     * @return bool
     */
    public function isProfileVisible()
    {
        return (
        in_array($this->getStatus(), array(
                self::STATUS_VISIBLE,
                self::STATUS_VISIBLE_APPOINTMENTS
            ))
        );
    }

    /**
     * @todo Better than this
     *
     * @return bool
     */
    public function isActive()
    {
        return ($this->activated_at != null && $this->activated_at != '0000-00-00 00:00:00');
    }

    /**
     * Is another user allowed to edit this User?
     * If the user shares a practice with another, then yes
     *
     * @param int $editor The user id of the editor
     *
     * @return bool
     */
    public function isEditAllowed($editor)
    {
        $permission = false;

        if ($this->isValid()) {
            if (intval($editor) === intval($this->getId())) {
                $permission = true;
            } else {
                $editor_locations = Location::getLocationsByUser($editor);
                $members = array();

                /** @var Location[] $editor_locations */
                foreach ($editor_locations as $editor_location) {
                    $members = array_merge($members, $editor_location->getMemberIds());
                }

                array_unique($members);

                if (in_array($this->getId(), $members)) {
                    $permission = true;
                }
            }
        }

        return $permission;
    }
}
