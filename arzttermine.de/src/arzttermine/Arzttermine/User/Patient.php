<?php

namespace Arzttermine\User;

use Arzttermine\Application\Application;
use Arzttermine\Booking\Booking;
use Arzttermine\Core\Basic;
use Arzttermine\Insurance\Insurance;
use Arzttermine\Location\Location;
use Arzttermine\Mail\Mailing;

class Patient extends Basic {

    // @todo Flip this the right way around
    const STATUS_ACTIVE = 'ACTIVE';
    const STATUS_DISABLED = 'DISABLED';
    
    /**
     * @var string
     */
    protected $tablename = DB_TABLENAME_PATIENTS;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     */
    protected $password_hash;

    /**
     * @var string
     */
    protected $password_hash_salt;
    
    /**
     * Apparently the same as reset_password_code
     * 
     * @var string
     */
    protected $password_reset_code;

    /**
     * Apparently the same as password_reset_code
     * 
     * @var string
     */
    protected $reset_password_code;

    /**
     * @var int
     */
    protected $gender;

    /**
     * @var string
     */
    protected $first_name;

    /**
     * @var string
     */
    protected $last_name;

    /**
     * @var string
     */
    protected $phone;

    /**
     * @var int
     */
    protected $insurance;

    /**
     * @var string
     */
    protected $birthday;

    /**
     * @var int
     */
    protected $active; // enum('ACTIVE', 'DISABLED')

    /**
     * @var int
     */
    protected $newsletter_subscription;

    /**
     * @var string
     */
    protected $slug;

    /**
     * @var int
     */
    protected $status;

    /**
     * @var string
     */
    protected $created_at;

    /**
     * @var string
     */
    protected $created_by;

    /**
     * @var string
     */
    protected $activation_code;

    /**
     * @var string
     */
    protected $activated_at;

    /**
     * @var string
     */
    protected $updated_at;

    /**
     * @var string
     */
    protected $updated_by;

    /**
     * @var string
     */
    protected $last_login_at;

    /**
     * @var array
     */
    protected $all_keys
        = array(
            "id",
            "email",
            "password_hash",
            "password_hash_salt",
            "password_reset_code",
            "gender",
            "first_name",
            "last_name",
            "phone",
            "insurance",
            "birthday",
            "active",
            "newsletter_subscription"
        );

    /**
     * @var array
     */
    protected $secure_keys
        = array(
            "id",
            "email",
            "password_hash",
            "password_hash_salt",
            "password_reset_code",
            "gender",
            "first_name",
            "last_name",
            "phone",
            "insurance",
            "birthday",
            "active",
            "newsletter_subscription"
        );

    /**
     * Change the password for a user
     *
     * @param string $password
     *
     * @return bool
     */
    public function changePassword($password)
    {
        return $this
            ->setPasswordHashSalt(substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 6))
            ->setPasswordHash(hash('sha256', PASSWORD_FIX_SALT . $password . $this->getPasswordHashSalt()))
            ->setResetPasswordCode('')
            ->save(array('password_hash_salt', 'password_hash', 'password_reset_code'));
    }

    /**
     * Checks the credentials for a user
     *
     * @param string $email
     * @param string $password
     *
     * @return bool
     */
    public static function checkCredentials($email, $password)
    {
        $_email = sanitize_email($email);
        if (!is_email($_email)) {
            return false;
        }

        $patient = new Patient();
        $patient->load(sqlsave($_email), 'email');
        
        if (!$patient->isValid()) {
            return false;
        }

        if ($patient->getPasswordHashSalt() == '') {
            return false;
        }

        $_password_hash = hash('sha256', PASSWORD_FIX_SALT . $password . $patient->getPasswordHashSalt());
        
        return ($_password_hash === $patient->getPasswordHash());
    }

    /**
     * @param string $password_hash_salt
     *
     * @return self
     */
    public function setPasswordHashSalt($password_hash_salt)
    {
        $this->password_hash_salt = $password_hash_salt;

        return $this;
    }

    /**
     * @return string
     */
    public function getPasswordHashSalt()
    {
        return $this->password_hash_salt;
    }

    /**
     * @param string $password_hash
     *
     * @return self
     */
    public function setPasswordHash($password_hash)
    {
        $this->password_hash = $password_hash;

        return $this;
    }

    /**
     * @return string
     */
    public function getPasswordHash()
    {
        return $this->password_hash;
    }
    
    /**
     * Returns a new Password Code
     *
     * @return string
     */
    public function generateResetPasswordCode()
    {
        return substr(md5(uniqid()), 0, 6);
    }

    /**
     * @param string $reset_password_code
     *
     * @return self
     */
    public function setResetPasswordCode($reset_password_code)
    {
        $this->password_reset_code = $reset_password_code;

        return $this;
    }

    /**
     * @return string
     */
    public function getResetPasswordCode()
    {
        return $this->password_reset_code;
    }

    /**
     * @param int $gender
     *
     * @return self
     */
    public function setGender($gender)
    {
        if (in_array(intval($gender), array_keys($GLOBALS['CONFIG']['ARRAY_GENDER']))) {
            $this->gender = intval($gender);
        }

        return $this;
    }

    /**
     * returns the users gender
     *
     * @return int gender ($CONFIG["ARRAY_GENDER"] => 0=male/1=female)
     */
    public function getGender()
    {
        return intval($this->gender);
    }

    /**
     * returns the users gender text
     *
     * @return string gender ($CONFIG["ARRAY_GENDER"] => 0=male/1=female)
     */
    public function getGenderText()
    {
        return $GLOBALS['CONFIG']['ARRAY_GENDER'][$this->gender];
    }

    /**
     * Returns the users name
     *
     * @return string
     */
    public function getName()
    {
        if ($this->isValid()) {
            return "{$this->getFirstName()} {$this->getLastName()}";
        } else {
            return 'ungültiger User';
        }
    }

    /**
     * @param string $email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $phone
     *
     * @return self
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $slug
     *
     * @return self
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $first_name
     *
     * @return self
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @param string $last_name
     *
     * @return self
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @return self
     */
    public function activate()
    {
        $this->active = self::STATUS_ACTIVE;
        $this->save('active');

        return $this;
    }

    /**
     * @return self
     */
    public function deactivate()
    {
        $this->active = self::STATUS_DISABLED;
        $this->save('active');

        return $this;
    }

    /**
     * @todo Better than this
     *
     * @return bool
     */
    public function isActive()
    {
        return ($this->isValid() && $this->active === self::STATUS_ACTIVE);
    }

    /**
     * @param Insurance $insurance
     *
     * @return self
     */
    public function setInsurance(Insurance $insurance)
    {
        $this->insurance = $insurance->getId(); // @todo Rely on a value that isn't the ID

        return $this;
    }

    /**
     * @return Insurance
     */
    public function getInsurance()
    {
        return new Insurance($this->insurance);
    }

    /**
     * @return bool
     */
    public function isPublicallyInsured()
    {
        return intval($this->getInsurance()->getId()) === Insurance::GKV;
    }

    /**
     * @return bool
     */
    public function isPrivatelyInsured()
    {
        return intval($this->getInsurance()->getId()) === Insurance::PKV;
    }

    /**
     * @return int
     */
    private function loadFromSession()
    {
        if (Application::getInstance()->getSession()->has("PATIENTID")) {
            $this->load(Application::getInstance()->getSession()->get("PATIENTID"));

            return $this->getId();
        } else {
            return 0;
        }
    }

    /**
     * Shim login function
     * 
     * @param string $email
     * @param string $password
     *
     * @return bool
     */
    public function login($email, $password)
    {
        $app = Application::getInstance();
        $session = $app->getSession();

        // Always use the logedin User first
        if ($this->loadFromSession()) {
          return true;
        } elseif (self::checkCredentials($email, $password)) {
            $this->load($email, 'email');
            // If the user is valid and has been activated
            if ($this->isValid() && $this->isActive()) {
                $session->set('PATIENTID', $this->getId());
                return true;
            }
        }

        return false;
    }

    /**
     * Shim logout function
     */
    public function logout()
    {
        $session = Application::getInstance()->getSession();
        $session->remove("PATIENTID");
        $session->invalidate();
        $this->id = 0;
    }

    /**
     * @return Patient
     */
    public function getCurrentUser()
    {
        return new Patient($this->loadFromSession());
    }
    
    /**
     * Get bookings for a patient
     *
     * @param string $bookingAppointmentTime
     *    false: all bookings
     *  'upcoming': return all upcoming bookings
     *  'past': return past bookings
     * @param string $orderBy (=appointment_start_at)
     * @param int $integrationId filteres bookings by integration
     *
     * @return array Booking
     */
    public function getBookings($bookingAppointmentTime = '', $orderBy = 'appointment_start_at', $integrationId = null)
    {
        $booking = new Booking();

        $subQuery = '';
        switch ($bookingAppointmentTime) {
            case 'upcoming':
                $subQuery .= sprintf(" AND %s.`appointment_start_at` > NOW() ", DB_TABLENAME_BOOKINGS);
                break;
            case 'past':
                $subQuery .= sprintf(" AND %s.`appointment_start_at` < NOW() ", DB_TABLENAME_BOOKINGS);
                // dont show apointments with unvalid dates (0000-00-00 or similar)
                $subQuery .= sprintf(" AND %s.`appointment_start_at` > 2010-01-01 ", DB_TABLENAME_BOOKINGS);
                // dont show apointments older than x days for data security
                $subQuery .= sprintf(" AND %s.`appointment_start_at` > DATE_SUB(CURDATE(), INTERVAL ".PATIENT_SHOW_OLD_BOOKINGS_IN_DAYS." DAY) ", DB_TABLENAME_BOOKINGS);
                break;
            case 'requests':
                // show only appointments with no valid appointment date
                // NOTE: This apparently only works when start_at is 0. Using 1-1-1970 puts it in the previous bookings group
                $subQuery .= sprintf(" AND (%s.`appointment_start_at` = 0000-00-00 OR %s.`appointment_start_at`=1970-01-01) ", DB_TABLENAME_BOOKINGS, DB_TABLENAME_BOOKINGS);
                break;
        }

        $sql = sprintf(
            "SELECT %s.`id` FROM %s LEFT JOIN %s on %s.`email`=%s.`email` %s
                                     LEFT JOIN %s on %s.`id`=%s.`user_id`
                                     LEFT JOIN %s on %s.`id`=%s.`location_id` ",
            DB_TABLENAME_BOOKINGS, DB_TABLENAME_PATIENTS, DB_TABLENAME_BOOKINGS, DB_TABLENAME_PATIENTS, DB_TABLENAME_BOOKINGS,
            $subQuery,
            DB_TABLENAME_USERS, DB_TABLENAME_USERS, DB_TABLENAME_BOOKINGS,
            DB_TABLENAME_LOCATIONS, DB_TABLENAME_LOCATIONS, DB_TABLENAME_BOOKINGS
        );
        $sql .= " WHERE " . DB_TABLENAME_PATIENTS . ".`id`=" . $this->id;

        if (!empty($integrationId)) {
            $sql .= sprintf(" AND %s.`integration_id`=%d ", DB_TABLENAME_LOCATIONS, $integrationId);
        }
        $sql .= sprintf(" ORDER BY %s.`%s` DESC ", DB_TABLENAME_BOOKINGS, $orderBy);

        return $booking->findObjectsSql($sql);
    }

    /**
     * Checks if an email is available for registration
     *
     * @param string $email
     *
     * @return bool
     */
    public static function isEmailAvailable($email)
    {
        $user = new self($email, 'email');
        return !$user->isValid();
    }

    /**
     * @param Booking $booking
     * @param User $user
     * @param Location $location
     */
    public function sendNotificationEmails(Booking $booking, User $user, Location $location)
    {
        $this->sendCancellationEmail($booking, $user, $location);
        $this->sendCancellationConfirmationEmail($booking, $user, $location);
    }

    /**
     * Send the activation code to the user. It's just a password reset link with extra sugar
     */
    public function sendActivationEmail()
    {
        $reset_password_code = $this->generateResetPasswordCode();
        $this->setResetPasswordCode($reset_password_code)->save();
        
        $_mailing = new Mailing();
        $_mailing->type = MAILING_TYPE_TEXT;
        $_mailing->content_filename = '/mailing/patients/patient-activation.txt';
        $_mailing->data['domain'] = $GLOBALS['CONFIG']['DOMAIN'];
        $_mailing->data['url'] = Application::getInstance()->container->get('router')->generate('patient_activate', array(), true) . "?code={$reset_password_code}";
        $_mailing->data['patient_name'] = $this->getName();
        $_mailing->addAddressTo($this->getEmail());
        $_mailing->send();
    }

    /**
     * Send a cancellation email to customer support
     * 
     * @param Booking $booking
     * @param User $user
     * @param Location $location
     */
    public function sendCancellationEmail(Booking $booking, User $user, Location $location)
    {
        $appointment = $booking->getAppointment();
        $appointmentDate = $appointment->getDateTimeText();
        $integrationId = $location->getIntegrationId();

        $_mailing = new Mailing();
        $_mailing->type = MAILING_TYPE_HTML;
        $_mailing->content_filename = '/mailing/integrations/'.$integrationId.'/admin-booking-cancellation.html';
        $_mailing->data['domain'] = $GLOBALS['CONFIG']['DOMAIN'];
        $_mailing->data['gender'] = $booking->getGenderText();
        $_mailing->data['last_name'] = $booking->getLastName();
        $_mailing->data['doctor_name'] = $user->getName();
        $_mailing->data['practice_name'] = $location->getName();
        $_mailing->data['appointment_date'] = $appointmentDate;
        $_mailing->data['booking_id'] = $booking->getId();
        $_mailing->addAddressTo($GLOBALS['CONFIG']["BOOKINGS_EMAIL_ADDRESS"], $GLOBALS['CONFIG']["BOOKINGS_EMAIL_NAME"]);
        $_mailing->send();
    }

    /**
     * Send an email to the patient to confirm the cancellation
     * 
     * @param Booking $booking
     * @param User $user
     * @param Location $location
     */
    public function sendCancellationConfirmationEmail(Booking $booking, User $user, Location $location)
    {
        $appointment = $booking->getAppointment();
        $appointmentDate = $appointment->getDateTimeText();
        $integrationId = $location->getIntegrationId();

        $_mailing = new Mailing();
        $_mailing->type = MAILING_TYPE_HTML;
        $_mailing->content_filename = '/mailing/integrations/'.$integrationId.'/booking-cancellation-confirmation.html';
        $_mailing->data['domain'] = $GLOBALS['CONFIG']['DOMAIN'];
        $_mailing->data['gender'] = $booking->getGenderText();
        $_mailing->data['last_name'] = $booking->getLastName();
        $_mailing->data['doctor_name'] = $user->getName();
        $_mailing->data['practice_name'] = $location->getName();
        $_mailing->data['appointment_date'] = $appointmentDate;
        $_mailing->data['booking_id'] = $booking->getId();
        $_mailing->addAddressTo($booking->getEmail());
        $_mailing->send();
    }
}
