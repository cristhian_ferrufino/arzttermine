<?php

namespace Arzttermine\User;

use Arzttermine\Booking\Booking;
use Arzttermine\Core\Basic;
use Arzttermine\Widget\Container;
use Arzttermine\Widget\Widget;

class Affiliate extends Basic {

    /**
     * @var string
     */
    protected $tablename = DB_TABLENAME_AFFILIATES;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     */
    protected $password_hash;

    /**
     * @var string
     */
    protected $password_hash_salt;

    /**
     * @var string
     */
    protected $password_reset_code;

    /**
     * @var string
     */
    protected $company_name;

    /**
     * @var string
     */
    protected $phone;

    /**
     * @var bool
     */
    protected $active;

    /**
     * @var int
     */
    protected $widget_id;

    /**
     * @var array
     */
    protected $all_keys
        = array(
            "id",
            "email",
            "password_hash",
            "password_hash_salt",
            "password_reset_code",
            "company_name",
            "phone",
            "active",
            "widget_id"
        );

    /**
     * @var array
     */
    protected $secure_keys
        = array(
            "id",
            "email",
            "password_hash",
            "password_hash_salt",
            "password_reset_code",
            "company_name",
            "phone",
            "active",
            "widget_id"
        );

    /**
     * Returns the source_plaform field which is used in bookings.source_platform
     * to identify on which platform or widget the booking took place
     *
     * @return string
     */
    public function getSourcePlatform()
    {
        $sourcePlatform = 'invalid';

        $widget = new Container($this->widget_id, 'id');

        if ($widget->isValid()) {
            $sourcePlatform = $widget->getSlug();
        }

        return $sourcePlatform;
    }

    /**
     * Returns the company name
     *
     * @return string
     */
    public function getCompanyName()
    {
        if ($this->isValid()) {
            return $this->company_name;
        } else {
            return 'ungültiger Affiliate';
        }
    }

    /**
     * Returns the email address
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Returns the phone number
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Get bookings for a patient
     *
     * @param bool $bookingAppointmentTime
     *     false: all bookings
     *     'upcoming': return all upcoming bookings
     *     'past': return past bookings
     * @param string $orderBy (=appointment_start_at)
     * @param int $integrationId filteres bookings by integration
     *
     * @return array Booking
     */
    public function getBookings($bookingAppointmentTime = false, $orderBy = 'appointment_start_at', $integrationId = null)
    {
        $booking = new Booking();

        $subQuery = '';

        switch ($bookingAppointmentTime) {
            case 'upcoming':
                $subQuery .= sprintf(" AND %s.`appointment_start_at` > NOW() ", DB_TABLENAME_BOOKINGS);
                break;
            case 'past':
                $subQuery .= sprintf(" AND %s.`appointment_start_at` < NOW() ", DB_TABLENAME_BOOKINGS);
                // dont show apointments with unvalid dates (0000-00-00 or similar)
                $subQuery .= sprintf(" AND %s.`appointment_start_at` > 2010-01-01 ", DB_TABLENAME_BOOKINGS);
                break;
            case 'requests':
                // show only appointments with no valid appointment date
                $subQuery .= sprintf(" AND (%s.`appointment_start_at` = 0000-00-00 OR %s.`appointment_start_at`=1970-01-01) ", DB_TABLENAME_BOOKINGS, DB_TABLENAME_BOOKINGS);
                break;
        }

        $sql = sprintf(
            "SELECT %s.`id` FROM %s LEFT JOIN %s on %s.`email`=%s.`email` %s" .
            "LEFT JOIN %s on %s.`id`=%s.`user_id` " .
            "LEFT JOIN %s on %s.`id`=%s.`location_id` ",
            DB_TABLENAME_BOOKINGS,
            DB_TABLENAME_PATIENTS,
            DB_TABLENAME_BOOKINGS,
            DB_TABLENAME_PATIENTS,
            DB_TABLENAME_BOOKINGS,
            $subQuery,
            DB_TABLENAME_USERS,
            DB_TABLENAME_USERS,
            DB_TABLENAME_BOOKINGS,
            DB_TABLENAME_LOCATIONS,
            DB_TABLENAME_LOCATIONS,
            DB_TABLENAME_BOOKINGS
        );

        $sql .= " WHERE " . DB_TABLENAME_PATIENTS . ".`id`=" . $this->id;

        if ($integrationId != null) {
            $sql .= sprintf(" AND %s.`integration_id`=%d ", DB_TABLENAME_LOCATIONS, $integrationId);
        }

        $sql .= sprintf(" ORDER BY %s.`%s` DESC ", DB_TABLENAME_BOOKINGS, $orderBy);

        return $booking->findObjectsSql($sql);
    }
}
