<?php

namespace Arzttermine\User;

use Arzttermine\Mail\Mailing;

/**
 * Class PendingUser
 * 
 * @todo Refactor this out and use flags in the database to indicate a user's status properly
 *
 * @package Arzttermine\User
 */
class PendingUser extends User {

    /**
     * @var string
     */
    protected $tablename = DB_TABLENAME_TEMP_USERS_DATA;

    /**
     * @var string
     */
    protected $phone_mobile = '';

    /**
     * @var string
     */
    protected $location_doctors_count = 0;

    /**
     * @var string
     */
    protected $location_name = '';

    /**
     * @var string
     */
    protected $location_street = '';

    /**
     * @var string
     */
    protected $location_zip = '';

    /**
     * @var string
     */
    protected $location_city = '';


    /**
     * @var string
     */
    protected $location_www = '';

    /**
     * Not sure if completely unused
     * 
     * @var string
     */
    protected $treatment_type_ids = '';

    /**
     * @var string
     */
    protected $registration_reasons = '';

    /**
     * @var string
     */
    protected $registration_funnel = '';

    /**
     * @var string
     */
    protected $registration_funnel_other = '';

    /**
     * @var array
     */
    protected $all_keys
        = array(
            "email",
            "gender",
            "title",
            "first_name",
            "last_name",
            "phone",
            "password_hash",
            "password_hash_salt",
            "medical_specialty_ids",
            "treatment_type_ids",
            "activation_code",
            "phone_mobile",
            "location_doctors_count",
            "location_name",
            "location_street",
            "location_zip",
            "location_city",
            "location_www",
            "registration_reasons",
            "registration_funnel",
            "registration_funnel_other"
        );

    /**
     * @var array
     */
    protected $secure_keys
        = array(
            "email",
            "gender",
            "title",
            "first_name",
            "last_name",
            "phone",
            "password_hash",
            "password_hash_salt",
            "medical_specialty_ids",
            "treatment_type_ids",
            "activation_code",
            "phone_mobile",
            "location_doctors_count",
            "location_name",
            "location_street",
            "location_zip",
            "location_city",
            "location_www",
            "registration_reasons",
            "registration_funnel",
            "registration_funnel_other"
        );

    /**
     * @return string
     */
    public function getLocationDoctorsCount(): string
    {
        return $this->location_doctors_count;
    }

    /**
     * @param string $location_doctors_count
     */
    public function setLocationDoctorsCount(string $location_doctors_count)
    {
        $this->location_doctors_count = $location_doctors_count;

        return $this;
    }


    /**
     * @param string $location_name
     *
     * @return self
     */
    public function setLocationName($location_name)
    {
        $this->location_name = $location_name;

        return $this;
    }

    /**
     * @return string
     */
    public function getLocationName()
    {
        return $this->location_name;
    }

    /**
     * @param string $location_street
     *
     * @return self
     */
    public function setLocationStreet($location_street)
    {
        $this->location_street = $location_street;

        return $this;
    }

    /**
     * @return string
     */
    public function getLocationStreet()
    {
        return $this->location_street;
    }

    /**
     * @param string $location_zip
     *
     * @return self
     */
    public function setLocationZip($location_zip)
    {
        $this->location_zip = $location_zip;

        return $this;
    }

    /**
     * @return string
     */
    public function getLocationZip()
    {
        return $this->location_zip;
    }

    /**
     * @param string $location_city
     *
     * @return self
     */
    public function setLocationCity($location_city)
    {
        $this->location_city = $location_city;

        return $this;
    }

    /**
     * @return string
     */
    public function getLocationCity()
    {
        return $this->location_city;
    }

    /**
     * @return string
     */
    public function getLocationWww(): string
    {
        return $this->location_www;
    }

    /**
     * @param string $location_www
     */
    public function setLocationWww(string $location_www)
    {
        $this->location_www = $location_www;

        return $this;
    }


    /**
     * @param string $phone_mobile
     *
     * @return self
     */
    public function setPhoneMobile($phone_mobile)
    {
        $this->phone_mobile = $phone_mobile;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhoneMobile()
    {
        return $this->phone_mobile;
    }

    /**
     * @param string $registration_funnel
     *
     * @return self
     */
    public function setRegistrationFunnel($registration_funnel)
    {
        $this->registration_funnel = $registration_funnel;

        return $this;
    }

    /**
     * @return string
     */
    public function getRegistrationFunnel()
    {
        return $this->registration_funnel;
    }

    /**
     * @param string $registration_funnel_other
     *
     * @return self
     */
    public function setRegistrationFunnelOther($registration_funnel_other)
    {
        $this->registration_funnel_other = $registration_funnel_other;

        return $this;
    }

    /**
     * @return string
     */
    public function getRegistrationFunnelOther()
    {
        return $this->registration_funnel_other;
    }

    /**
     * @param string $registration_reasons
     *
     * @return self
     */
    public function setRegistrationReasons($registration_reasons)
    {
        $this->registration_reasons = $registration_reasons;

        return $this;
    }

    /**
     * @return string
     */
    public function getRegistrationReasons()
    {
        return $this->registration_reasons;
    }

    /**
     * Send notifications related to this pending user
     */
    public function sendNotifications($skipUser = false)
    {
        $this->sendSystemNotification();

        if($skipUser === false) {
            $this->sendUserNotification();
        }
    }

    /**
     * Sends an employee a notification that we have a new pending user
     */
    protected function sendSystemNotification()
    {
        $mailing = new Mailing();
        $mailing->type = MAILING_TYPE_TEXT;
        $mailing->content_filename = '/mailing/doctors/doctor-register.txt';
        $mailing->from = $GLOBALS['CONFIG']["REVIEWS_EMAIL_ADDRESS"];
        $mailing->from_name = $GLOBALS['CONFIG']["REVIEWS_EMAIL_NAME"];

        $mailing->data['domain'] = $GLOBALS['CONFIG']['DOMAIN'];
        $mailing->data['email'] = $this->getEmail();
        $mailing->data['doctor_name'] = "{$GLOBALS['CONFIG']['ARRAY_GENDER'][$this->getGender()]} {$this->getFullName()}";
        $mailing->data['medical_specialty'] = "(Please visit for all details: http://www.arzttermine.de/administration/temp_users?action=edit&id={$this->getId()}&page=1&alpha=&sort=id&direction=DESC&num_per_page=50&type=num)";
        $mailing->data['phone'] = $this->getPhone();
        $mailing->data['location_city'] = $this->getLocationCity();
        $mailing->data['location_name'] = $this->getLocationName();
        $mailing->data['phone_mobile'] = $this->getPhoneMobile();
        $mailing->data['registration_reasons'] = $this->getRegistrationReasons();
        $mailing->data['registration_funnel'] = $this->getRegistrationFunnel();
        $mailing->data['doctors_count'] = $this->getLocationDoctorsCount();
        $mailing->data['created_at'] = date('Y-m-d H:i:s');

        $mailing->addAddressTo($GLOBALS['CONFIG']['SALES_EMAIL_ADDRESS']);
        $mailing->send();
        
        return $this;
    }

    /**
     * Sends the potential client a confirmation email
     */
    protected function sendUserNotification()
    {
        $mailing = new Mailing();
        $mailing->type = MAILING_TYPE_TEXT;
        $mailing->content_filename = '/mailing/doctors/doctor-request-confirmation.txt';
        $mailing->from = $GLOBALS['CONFIG']["REVIEWS_EMAIL_ADDRESS"];
        $mailing->from_name = $GLOBALS['CONFIG']["REVIEWS_EMAIL_NAME"];
        $mailing->data['domain'] = $GLOBALS['CONFIG']['DOMAIN'];
        $mailing->data['doctor_salutation_full_name'] = "{$GLOBALS['CONFIG']['ARRAY_GENDER'][$this->getGender()]} {$this->getFirstName()} {$this->getLastName()}";
        $mailing->addAddressTo($this->getEmail());
        $mailing->send();
        
        return $this;
    }
}