<?php
/**
 * Statistic Class
 *
 * @author Axel Kuzmik <axel@kuzmik.de>
 */

namespace Arzttermine\Statistic;

use Arzttermine\Application\Application;
use Arzttermine\Booking\Booking;
use Arzttermine\Core\DateTime;
use Arzttermine\Location\Location;
use Arzttermine\Review\Review;
use Arzttermine\User\User;

class Statistic
{

    /**
     * @var \Arzttermine\Booking\Booking
     */
    protected $booking;

    /**
     * @param int $value
     * @param string $key
     */
    public function __construct($value = 0, $key = 'id')
    {
        $this->booking = new Booking();
        $this->user = new User();
        $this->location = new Location();
        $this->review = new Review();
    }

    /**
     * Returns data for bookings
     *
     * @param string $key
     *
     * @return mixed
     */
    public function count($key = '')
    {
        $data = null;
        switch ($key) {
            case 'bookings_today':
                $data = $this->booking->count('DATE(created_at) = DATE(NOW())');
                break;
            case 'bookings_today_invalid':
                $data = $this->booking->count('DATE(created_at) = DATE(NOW()) AND status=' . Booking::STATUS_INVALID);
                break;
            case 'bookings_today_received':
                $data = $this->booking->count('DATE(created_at) = DATE(NOW()) AND ' . Booking::getStatusesSql(Booking::getReceivedStatuses(), 'OR'));
                break;
            case 'bookings_today_insurance_private':
                $data = $this->booking->count('DATE(created_at) = DATE(NOW()) AND insurance_id=1');
                break;
            case 'bookings_today_insurance_public':
                $data = $this->booking->count('DATE(created_at) = DATE(NOW()) AND insurance_id=2');
                break;
            case 'bookings_today_insurance_private_percent':
                $data1 = $this->booking->count('DATE(created_at) = DATE(NOW()) AND insurance_id=1');
                $data2 = $this->booking->count('DATE(created_at) = DATE(NOW())');
                if ($data2 > 0) {
                    $data = $data1 * 100 / $data2;
                } else {
                    $data = 0;
                }
                break;
            case 'bookings_today_integration_integrated':
                $data = $this->booking->count('DATE(created_at) = DATE(NOW()) AND (integration_id=2 OR integration_id=5)');
                break;
            case 'bookings_today_integration_not_integrated':
                $data = $this->booking->count('DATE(created_at) = DATE(NOW()) AND (integration_id=1)');
                break;
            case 'bookings_today_integration_inquery':
                $data = $this->booking->count('DATE(created_at) = DATE(NOW()) AND (integration_id=8 OR integration_id=9)');
                break;
            case 'bookings_today_integration_telephone':
                $data = $this->booking->count('DATE(created_at) = DATE(NOW()) AND integration_id IS NULL');
                break;
            case 'doctors_all':
                $data = $this->user->count('group_id=' . GROUP_DOCTOR);
                break;
            case 'doctors_signed':
                $data = $this->user->count('group_id=' . GROUP_DOCTOR . ' AND has_contract=1');
                break;
            case 'doctors_signed_draft':
                $data = $this->user->count('group_id=' . GROUP_DOCTOR . ' AND has_contract=1 AND status=' . USER_STATUS_DRAFT);
                break;
            case 'doctors_signed_active':
                $data = $this->user->count('group_id=' . GROUP_DOCTOR . ' AND has_contract=1 AND status=' . USER_STATUS_ACTIVE);
                break;
            case 'doctors_signed_visible':
                $data = $this->user->count('group_id=' . GROUP_DOCTOR . ' AND has_contract=1 AND status=' . USER_STATUS_VISIBLE);
                break;
            case 'doctors_signed_visible_appointments':
                $data = $this->user->count('group_id=' . GROUP_DOCTOR . ' AND has_contract=1 AND status=' . USER_STATUS_VISIBLE_APPOINTMENTS);
                break;
            case 'locations_all':
                $data = $this->location->count('1=1');
                break;
            case 'reviews_today':
                $data = $this->review->count('DATE(rated_at) = DATE(NOW())');
                break;
            case 'reviews_all':
                $data = $this->review->count();
                break;
            case 'reviews_all_approved':
                $data = $this->review->count('approved_at IS NOT NULL AND approved_at != "0000-00-00 00:00:00"');
                break;
            case 'reviews_average_all':
                $data_array = $this->review->getRatingsAverageArray('1=1');
                $data = round($data_array['rating_average'], 4);
                break;
        }

        return $data;
    }

    /**
     * Returns js formated data for bookings
     *
     * @param string $key
     *
     * @return mixed
     */
    public function getJSArray($key = '')
    {
        $db = Application::getInstance()->getSql();
        $data = null;
        switch ($key) {
            case 'bookings_day_today':
            case 'bookings_day_yesterday':
            case 'bookings_day_lastweek':
                if ($key == 'bookings_day_lastweek') {
                    $sql = "SELECT HOUR(created_at) thehour, count(*) bookings FROM bookings WHERE DATE(created_at) = DATE(SUBDATE(CURDATE(),7)) GROUP BY HOUR(created_at)";
                } elseif ($key == 'bookings_day_yesterday') {
                    $sql = "SELECT HOUR(created_at) thehour, count(*) bookings FROM bookings WHERE DATE(created_at) = DATE(SUBDATE(CURDATE(),1)) GROUP BY HOUR(created_at)";
                } else {
                    $sql = "SELECT HOUR(created_at) thehour, count(*) bookings FROM bookings WHERE DATE(created_at) = DATE(NOW()) GROUP BY HOUR(created_at)";
                }
                $db->query($sql);
                $records = array();
                if ($db->nf() > 0) {
                    while ($db->next_record()) {
                        $records[$db->f('thehour')] = $db->f('bookings');
                    }
                }
                $data = '[0, 0],';
                $sum = 0;
                for ($x = 0; $x <= 23; $x++) {
                    if (isset($records[$x])) {
                        $value = $records[$x];
                    } else {
                        $value = 0;
                    }
                    $sum = $value + $sum;
                    $data .= '[' . ($x + 1) . ', ' . $sum . ']';
                    if ($x < 23) {
                        $data .= ',';
                    }
                }
                break;
            case 'bookings_days_year':
                $weeks = 12;
                $sql = "SELECT DAY( created_at ) theday, MONTH( created_at ) themonth, YEAR( created_at ) theyear, count( * ) bookings FROM bookings WHERE created_at > ( CURRENT_DATE - INTERVAL " . $weeks . " WEEK ) GROUP BY DATE( created_at ) ORDER BY theyear, themonth, theday";
                $db->query($sql);
                $records = array();
                if ($db->nf() > 0) {
                    while ($db->next_record()) {
                        $year = $db->f('theyear');
                        $month = sprintf("%02d", $db->f('themonth'));
                        $day = sprintf("%02d", $db->f('theday'));
                        $records["$year-$month-$day"] = $db->f('bookings');
                    }
                }
                // there could be empty days so get an array with all days
                $start = date('Y-m-d', strtotime('-' . $weeks . ' weeks', strtotime('now')));
                $end = date('Y-m-d', strtotime('tomorrow'));
                $period = new \DatePeriod(
                    new DateTime($start),
                    new \DateInterval('P1D'),
                    new DateTime($end)
                );
                $data = '';
                foreach ($period as $date) {
                    if ($data != '') {
                        $data .= ',';
                    }
                    $day = $date->format("Y-m-d");
                    if (isset($records[$day])) {
                        $value = $records[$day];
                    } else {
                        $value = 0;
                    }
                    // javascript is using ms not sec so * 1000
                    $data .= '[' . (strtotime($day) * 1000) . ', ' . $value . ']';
                }
                break;
            case 'bookings_weeks':
            case 'bookings_weeks_xaxis':
                $weeks = 54;
                $sql = "SELECT YEARWEEK( `created_at`, 1 ) AS theyearweek, COUNT( * ) AS bookings FROM `bookings` WHERE YEARWEEK( created_at, 1 ) > YEARWEEK( CURRENT_DATE - INTERVAL " . $weeks . " WEEK, 1 ) GROUP BY YEARWEEK( `created_at`, 1 ) ORDER BY theyearweek ASC";
                $db->query($sql);
                $records = array();
                if ($db->nf() > 0) {
                    while ($db->next_record()) {
                        $records[$db->f('theyearweek')] = $db->f('bookings');
                    }
                }
                $data = '';
                $x = 0;
                foreach ($records as $yearweek => $bookings) {
                    if ($data != '') {
                        $data .= ',';
                    }
                    if ($key == 'bookings_weeks') {
                        $data .= '[' . $x . ', ' . $bookings . ']';
                    } elseif ($key == 'bookings_weeks_xaxis') {
                        // mysql YEARWEEK format is 201406, here we want the week only
                        $data .= '[' . $x . ', ' . substr($yearweek, 4, 2) . ']';
                    }
                    $x++;
                }
                break;
            case 'bookings_months':
            case 'bookings_months_xaxis':
                // start with 2013-04-01
                $sql = "SELECT MONTH( created_at ) themonth, YEAR( created_at ) theyear, COUNT( * ) bookings FROM bookings WHERE created_at >= '2013-04-01 00:00:00' GROUP BY YEAR( `created_at` ) , MONTH( `created_at` ) ORDER BY theyear, themonth ASC";
                $db->query($sql);
                $records = array();
                if ($db->nf() > 0) {
                    while ($db->next_record()) {
                        $records[] = array(
                            'month'    => $db->f('themonth'),
                            'year'     => $db->f('theyear'),
                            'bookings' => $db->f('bookings')
                        );
                    }
                }
                $data = '';
                $x = 0;
                foreach ($records as $values) {
                    if ($data != '') {
                        $data .= ',';
                    }
                    if ($key == 'bookings_months') {
                        $data .= '[' . $x . ', ' . $values['bookings'] . ']';
                    } elseif ($key == 'bookings_months_xaxis') {
                        $data .= '[' . $x . ', "' . sprintf("%02d", $values['month']) . '/' . $values['year'] . '"]';
                    }
                    $x++;
                }
                break;
            case 'bookings_week_current':
            case 'bookings_week_last':
                if ($key == 'bookings_week_last') {
                    $sql = "SELECT DAY(created_at) theday, MONTH(created_at) themonth, YEAR(created_at) theyear, count(*) bookings FROM bookings WHERE YEARWEEK(created_at, 1) = YEARWEEK(CURRENT_DATE - INTERVAL 7 DAY, 1) GROUP BY DAY(created_at) ORDER BY theyear, themonth, theday";
                } elseif ($key == 'bookings_week_current') {
                    $sql = "SELECT DAY(created_at) theday, MONTH(created_at) themonth, YEAR(created_at) theyear, count(*) bookings FROM bookings WHERE YEARWEEK(created_at, 1) = YEARWEEK(CURRENT_DATE, 1) GROUP BY DAY(created_at) ORDER BY theyear, themonth, theday";
                }
                $db->query($sql);
                $records = array();
                if ($db->nf() > 0) {
                    while ($db->next_record()) {
                        $records[] = $db->f('bookings');
                    }
                }
                $data = '';
                for ($x = 0; $x <= 6; $x++) {
                    if ($data != '') {
                        $data .= ',';
                    }
                    if (isset($records[$x])) {
                        $value = $records[$x];
                    } else {
                        $value = 0;
                    }
                    $data .= '[' . $x . ', ' . $value . ']';
                }
                break;
            case 'bookings_month_current':
            case 'bookings_month_last':
                if ($key == 'bookings_month_last') {
                    $sql = "SELECT DAY(created_at) theday, MONTH(created_at) themonth, YEAR(created_at) theyear, count(*) bookings FROM bookings WHERE YEAR(created_at) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(created_at) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) GROUP BY DAY(created_at) ORDER BY theyear, themonth, theday";
                } elseif ($key == 'bookings_month_current') {
                    $sql = "SELECT DAY(created_at) theday, MONTH(created_at) themonth, YEAR(created_at) theyear, count(*) bookings FROM bookings WHERE YEAR(created_at) = YEAR(CURRENT_DATE) AND MONTH(created_at) = MONTH(CURRENT_DATE) GROUP BY DAY(created_at) ORDER BY theyear, themonth, theday";
                }
                $db->query($sql);
                $records = array();
                if ($db->nf() > 0) {
                    while ($db->next_record()) {
                        $records[$db->f('theday')] = $db->f('bookings');
                    }
                }
                $data = '';
                for ($x = 1; $x <= sizeof($records); $x++) {
                    if ($data != '') {
                        $data .= ',';
                    }
                    if (isset($records[$x])) {
                        $value = $records[$x];
                    } else {
                        $value = 0;
                    }
                    $data .= '[' . $x . ', ' . $value . ']';
                }
                break;
            case 'bookings_year_current':
            case 'bookings_year_last':
                // SELECT MONTH(created_at) themonth, YEAR(created_at) theyear, COUNT(*) bookings FROM bookings WHERE SUBSTRING(created_at FROM 1 FOR 7) = SUBSTRING(CURRENT_DATE - INTERVAL 1 MONTH FROM 1 FOR 7);
                break;
            case 'reviews_days_year':
                $weeks = 12;
                $sql = "SELECT DAY( rated_at ) theday, MONTH( rated_at ) themonth, YEAR( rated_at ) theyear, count( * ) reviews FROM reviews WHERE rated_at > ( CURRENT_DATE - INTERVAL " . $weeks . " WEEK ) GROUP BY DATE( rated_at ) ORDER BY theyear, themonth, theday";
                $db->query($sql);
                $records = array();
                if ($db->nf() > 0) {
                    while ($db->next_record()) {
                        $year = $db->f('theyear');
                        $month = sprintf("%02d", $db->f('themonth'));
                        $day = sprintf("%02d", $db->f('theday'));
                        $records["$year-$month-$day"] = $db->f('reviews');
                    }
                }
                // there could be empty days so get an array with all days
                $start = date('Y-m-d', strtotime('-' . $weeks . ' weeks', strtotime('now')));
                $end = date('Y-m-d', strtotime('tomorrow'));
                $period = new \DatePeriod(
                    new DateTime($start),
                    new \DateInterval('P1D'),
                    new DateTime($end)
                );
                $data = '';
                foreach ($period as $date) {
                    if ($data != '') {
                        $data .= ',';
                    }
                    $day = $date->format("Y-m-d");
                    if (isset($records[$day])) {
                        $value = $records[$day];
                    } else {
                        $value = 0;
                    }
                    // javascript is using ms not sec so * 1000
                    $data .= '[' . (strtotime($day) * 1000) . ', ' . $value . ']';
                }
                break;
            case 'reviews_weeks':
            case 'reviews_weeks_xaxis':
                $weeks = 54;
                $sql = "SELECT YEARWEEK( `reated_at`, 1 ) AS theyearweek, COUNT( * ) AS reviews FROM `reviews` WHERE YEARWEEK( rated_at, 1 ) > YEARWEEK( CURRENT_DATE - INTERVAL " . $weeks . " WEEK, 1 ) GROUP BY YEARWEEK( `rated_at`, 1 ) ORDER BY theyearweek ASC";
                $db->query($sql);
                $records = array();
                if ($db->nf() > 0) {
                    while ($db->next_record()) {
                        $records[$db->f('theyearweek')] = $db->f('reviews');
                    }
                }
                $data = '';
                $x = 0;
                foreach ($records as $yearweek => $reviews) {
                    if ($data != '') {
                        $data .= ',';
                    }
                    if ($key == 'reviews_weeks') {
                        $data .= '[' . $x . ', ' . $reviews . ']';
                    } elseif ($key == 'reviews_weeks_xaxis') {
                        // mysql YEARWEEK format is 201406, here we want the week only
                        $data .= '[' . $x . ', ' . substr($yearweek, 4, 2) . ']';
                    }
                    $x++;
                }
                break;
        }

        return $data;
    }

    /**
     * Returns js formated data for bookings
     *
     * @param string $key
     *
     * @return mixed
     */
    public function getData($key = '')
    {
        $db = Application::getInstance()->getSql();
        $data = null;
        switch ($key) {
            case 'bookings_day_today':
            case 'bookings_day_yesterday':
            case 'bookings_day_lastweek':
                if ($key == 'bookings_day_lastweek') {
                    $sql = "SELECT HOUR(created_at) thehour, count(*) bookings FROM bookings WHERE DATE(created_at) = DATE(SUBDATE(CURDATE(),7)) GROUP BY HOUR(created_at)";
                } elseif ($key == 'bookings_day_yesterday') {
                    $sql = "SELECT HOUR(created_at) thehour, count(*) bookings FROM bookings WHERE DATE(created_at) = DATE(SUBDATE(CURDATE(),1)) GROUP BY HOUR(created_at)";
                } else {
                    $sql = "SELECT HOUR(created_at) thehour, count(*) bookings FROM bookings WHERE DATE(created_at) = DATE(NOW()) GROUP BY HOUR(created_at)";
                }
                $db->query($sql);
                $records = array();
                if ($db->nf() > 0) {
                    while ($db->next_record()) {
                        $records[$db->f('thehour')] = $db->f('bookings');
                    }
                }
                $sum = 0;
                for ($x = 0; $x <= 23; $x++) {
                    if (isset($records[$x])) {
                        $value = $records[$x];
                    } else {
                        $value = 0;
                    }
                    $sum = $value + $sum;
                    $data[] = $sum;
                }
                break;
        }
        return $data;
    }

    /**
     * Builds the data array
     *
     * @param array $data_group
     *
     * @return mixed
     */
    public function getDataGroup($data_group)
    {
        $result = array();
        foreach ($data_group as $data) {
            // enrich the array with the data
            $data['data'] = $this->getData($data['id']);
            $result[] = $data;
        }
        return $result;
    }

    /**
     * Builds the data array
     *
     * @param array $data_group
     *
     * @return string
     */
    public function getJSON($data_group)
    {
        $json = '';
        switch ($data_group) {
            case 'bookings_today_yesterday_lastweek':
                $json = $this->getDataGroup(
                    [
                        ['id' => 'bookings_day_today',      'name' => 'Heute'],
                        ['id' => 'bookings_day_yesterday',  'name' => 'Gestern'],
                        ['id' => 'bookings_day_lastweek',   'name' => 'Letzte Woche']
                    ]
                );
                break;
        }
        return json_encode($json);
    }
}