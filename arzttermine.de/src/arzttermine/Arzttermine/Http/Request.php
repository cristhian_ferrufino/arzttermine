<?php

namespace Arzttermine\Http;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Request class. Manages everything to do with the IO between server and client
 */
class Request extends \Symfony\Component\HttpFoundation\Request {

    /**
     * Check if our request is a GET request
     *
     * @return string
     */
    public function isGet()
    {
        return $this->isMethod('get');
    }

    /**
     * Check if our request is a POST request
     *
     * @return string
     */
    public function isPost()
    {
        return $this->isMethod('post');
    }

    /**
     * Check if a given parameter exists in the request body
     *
     * @deprecated Use Request::request::has()
     * 
     * @param string $key
     *
     * @return bool
     */
    public function hasRequest($key)
    {
        return $this->request->has($key);
    }

    /**
     * Check if a given parameter exists in the query string
     *
     * @deprecated Use Request::query::has()
     * 
     * @param string $key
     *
     * @return bool
     */
    public function hasQuery($key)
    {
        return $this->query->has($key);
    }

    /**
     * Check if a given parameter exists anywhere
     *
     * @deprecated Know where your data is coming from!
     *
     * @param string $key
     *
     * @return bool
     */
    public function hasParam($key)
    {
        return $this->hasQuery($key) || $this->hasRequest($key);
    }

    /**
     * Retrieve a member of the $_POST superglobal (REQUEST body)
     *
     * If no $key is passed, returns the entire $_POST array.
     * 
     * @deprecated Use Request::request::get()
     *
     * @param string $key
     * @param mixed $default Default value to use if key not found
     *
     * @return mixed Returns null if key does not exist
     */
    public function getRequest($key = null, $default = null)
    {
        return empty($key) ? $this->request->all() : $this->request->get($key, $default);
    }

    /**
     * Retrieves a member of the $_GET superglobal (QUERY string)
     *
     * If no $key is passed, returns the entire $_GET array.
     * 
     * @deprecated Use Request::query::get()
     *
     * @param string $key
     * @param mixed $default Default value to use if key not found
     *
     * @return mixed Returns null if key does not exist
     */
    public function getQuery($key = null, $default = null)
    {
        return empty($key) ? $this->query->all() : $this->query->get($key, $default);
    }

    /**
     * Retrieve a parameter
     *
     * Retrieves a parameter from the instance. If a
     * parameter matching the $key is not found, default is returned.
     *
     * @deprecated Use Request::get()
     * 
     * @param string $key
     * @param mixed $default Default value to use if key not found
     *
     * @return mixed
     */
    public function getParam($key, $default = null)
    {
        return $this->get($key, $default);
    }

    /**
     * Inject a value into the GET/POST values stored in the request. This is a shim
     * 
     * @deprecated
     * 
     * @param string $type GET|POST
     * @param string $key
     * @param mixed $value
     * 
     * @return self
     */
    public function injectParameter($type = 'GET', $key, $value)
    {
        $type = strtoupper(trim($type));
        
        if ($type === 'GET') {
            $this->query->set($key, $value);
        } elseif ($type === 'POST') {
            $this->request->set($key, $value);
        }
        
        return $this;
    }

    /**
     * Send JSON data back to the client
     *
     * @param array $data
     *
     * @return JsonResponse
     */
    public function json($data = array())
    {
        return new JsonResponse($data);
    }

    /**
     * @param string $url
     * @param int $status
     * 
     * @return RedirectResponse
     */
    public function redirect($url, $status = 302)
    {
        return new RedirectResponse($url, $status);
    }
}