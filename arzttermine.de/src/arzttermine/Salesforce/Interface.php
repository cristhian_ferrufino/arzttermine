<?php

define('SOAP_CLIENT_BASEDIR', SYSTEM_PATH . '/src/arzttermine/Salesforce/SoapClient');

use Arzttermine\Booking\Booking;
use Arzttermine\Insurance\Insurance;
use Arzttermine\MedicalSpecialty\TreatmentType;
use Arzttermine\Mail\PHPMailer;
use \Arzttermine\User\User;
use NGS\Managers\SamediTreatmentTypesManager;

class SalesforceInterface {

    /**
     * @var SforceEnterpriseClient
     */
    private $connection = null;

    /**
     * @return self
     */
    public function __construct()
    {
        try {
            $this->login();
        } catch (Exception $e) {
            $this->connection = null;
        }

        return $this;
    }

    /**
     * The login function is used to make the connection to Salesforce account and handle the session
     * 
     * @return self
     * @throws Exception
     */
    public function login()
    {
        try {
            $mySforceConnection = new SforceEnterpriseClient();
            $mySforceConnection->createConnection(SOAP_CLIENT_BASEDIR . '/enterprise.wsdl.xml', null, array('ssl_method' => SOAP_SSL_METHOD_TLS, 'stream_context' => stream_context_create(array(
                'ssl' => array(
                    'crypto_method' =>  STREAM_CRYPTO_METHOD_TLSv1_2_CLIENT,
                )
            ))));

            $mySforceConnection->login(
                $GLOBALS['CONFIG']['SALESFORCE_USERNAME'],
                $GLOBALS['CONFIG']['SALESFORCE_PASSWORD'] . $GLOBALS['CONFIG']['SALESFORCE_SECURITY_TOKEN']
            );

            $this->connection = $mySforceConnection;
        } catch (Exception $e) {
            // alert the task force
            $mail = new PHPMailer();
            foreach ($GLOBALS['CONFIG']["SALESFORCE_ALERT_MESSAGES_EMAILS"] as $to) {
                $mail->AddAddress($to);
            }
            $mail->Body = 'Exception: ' . $e->faultstring . "\n\n\nSYSTEM_ENVIRONMENT: " . $GLOBALS['CONFIG']['SYSTEM_ENVIRONMENT'] . "\n\n_SERVER:\n\n" . print_r($_SERVER, true);
            $mail->Subject = 'ALERT: Salesforce: login() failed';
            $mail->From = $GLOBALS['CONFIG']["SYSTEM_MAIL_FROM_EMAIL"];
            $mail->Sender = $GLOBALS['CONFIG']["SYSTEM_MAIL_FROM_EMAIL"];
            $mail->IsHTML(false);
            $mail->Send();

            $this->connection = null;

            throw $e;
        }

        return $this;
    }

    /**
     * Returns the connection
     *
     * @return SforceEnterpriseClient
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * The upsertBooking() function stores into Salesforce a new Patienten record upon the creation of a new booking
     * or updates an existing one upon the update of a booking.
     *
     * @todo: Better parameter list!!! Beware of all calling references! Change them too!
     *
     * @return bool 
     * 
     * @param int $backend_id
     * @param int $gender
     * @param string $vorname
     * @param string $nachname
     * @param string $email
     * @param string $telefon
     * @param string $quelle This gets overwritten in the method. @todo: Refactor its usage RIGHT out
     * @param string $behandlungsgrund
     * @param int $insurance_id
     * @param string $appointment_start_at in DATETIME format [YYYY-MM-DD HH:ii:ss]
     * @param string $termin_bei
     * @param string $fachgebiet
     * @param string $status
     * @param string $returning_visitor
     * @param string $doctor_name
     * @param bool $hasContract
     * @param string $comment_patient
     * @param string $integration_name
     * @param string $flexible_location
     * @param string $source_platform
     * @param string $booking_offer_slug
     * @param string $couponCode
     * @param string $contactPreference
     * @param string $recallPreference
     * @param string $recallPhone
     *
     * @return bool|string (false:error / true:update ok), salesforce_id (created)
     */
    public function upsertBooking(
        $backend_id,
        $gender,
        $vorname,
        $nachname,
        $email,
        $telefon,
        $quelle,
        $behandlungsgrund,
        $insurance_id,
        $appointment_start_at,
        $termin_bei,
        $fachgebiet,
        $status,
        $returning_visitor = null,
        $doctor_name = null,
        $hasContract = null,
        $comment_patient = null,
        $integration_name = null,
        $flexible_location = null,
        $source_platform = null,
        $booking_offer_slug = '',
        $couponCode = null,
        $contactPreference = '',
        $recallPreference = '',
        $recallPhone = ''
    )
    {
        // Check if there is a SF record with the same $backend_id and CreatedDate after "2013-08-09"
        if (!$this->isSFcreatedDateAfterThreshold($backend_id)) {
            return false;
        }

        // Sanititze $appointment_start_at
        if (!empty($appointment_start_at)) {
            if (preg_match('#^0000-00-00#', $appointment_start_at)) {
                // this can happen if date is not set like in "docdir"
                $appointment_start_at = '1970-01-01 00:00:00';
            }
            // convert to GMT timezone. Salesforce wants it in this timezone.
            $termin_am = gmdate('Y-m-d\TH:i:s', strtotime($appointment_start_at));
        }

        // Get treatment type name from id
        $tt_name = TreatmentType::getNameStatic($behandlungsgrund);

        if ($tt_name == '') {
            // Try checking Samedi treatment types
            $samediTreatmentTypesManager = SamediTreatmentTypesManager::getInstance();
            $samedi_tt = $samediTreatmentTypesManager->getById($behandlungsgrund);
            if ($samedi_tt) {
                $tt_name = $samedi_tt->name;
            }
        }

        // If still the $tt_name is empty then set it to "not selected"
        if ($tt_name == '') {
            $tt_name = "not selected";
        }

        // Convert gender id to anrede
        $anrede_text = null;
        if ($gender !== null) {
            $anrede_text = intval($gender) === 0 ? 'Herr' : 'Frau';
        }

        // Check quelle if is 1 and set it to Gutschein if so
        // Note: Taken out as Gutschein use has been suspended. Need this here for logic reference
        // $quelle = $quelle == 1 ? 'Gutschein' : ($hasContract == 1 ? 'Online mit Vertrag' : 'Online ohne Vertrag');
        // Non-gutschein
        // $quelle = ($hasContract == 1) ? 'Online mit Vertrag' : 'Online ohne Vertrag';
        if ($hasContract !== null) {
            $quelle = $hasContract ? 'Online mit Vertrag' : 'Online ohne Vertrag';
        }

        /**
         * Get the insurance text
         * Dont use other methods to get the insurance text as Salesforce needs exactly these strings
         */
        if ($insurance_id == Insurance::PKV) {
            $insurance_text = "Privat versichert";
        } elseif ($insurance_id == Insurance::GKV) {
            $insurance_text = "Gesetzlich versichert";
        } else {
            $insurance_text = null;
        }

        $sObject = new \StdClass();
        $sObject->Backend_ID__c = $backend_id;
        $sObject->Anrede__c = $anrede_text;
        $sObject->Vorname__c = $vorname;
        $sObject->Name__c = $nachname;
        $sObject->Email__c = $email;
        $sObject->Telefon__c = $telefon;
        $sObject->Fachgebiet__c = $fachgebiet;
        $sObject->Status__c = Booking::findStatusText($status);
        $sObject->Versicherungsart__c = $insurance_text;
        $sObject->Quelle__c = $quelle;

        if ($returning_visitor !== null) {
            $sObject->Bestandspatient__c = $returning_visitor ? 'Ja' : 'Nein';
        }

        if ($behandlungsgrund !== null) {
            $sObject->Behandlungsgrund__c = $tt_name;
        }

        if (isset($termin_am) && !empty($termin_am)) {
            $sObject->Termin_am__c = $termin_am;
        }

        // Arzt__c == Account SalesforceID
        if ($termin_bei !== null) {
            $sObject->Arzt__c = $termin_bei;
        }

        // Doctor__c == String
        if (!empty($doctor_name)) {
            $sObject->Doktor__c = trim(strip_tags($doctor_name));
        }
        // Kommentar_Patient__c == String
        if (!empty($comment_patient)) {
            $sObject->Kommentar_Patient__c = trim(strip_tags($comment_patient));
        }
        // Integration__c == String
        if (!empty($integration_name)) {
            $sObject->Integration__c = trim(strip_tags($integration_name));
        }
        // Flexibel_Ort__c == Bool
        if (!empty($flexible_location)) {
            $sObject->Flexibel_Ort__c = intval($flexible_location);
        }
        // SourcePlatform__c == String
        if (!empty($source_platform)) {
            $sObject->SourcePlatform__c = trim(strip_tags($source_platform));
        }

        // booking_offer_slug__c = String
        if (!empty($booking_offer_slug)) {
            $sObject->booking_offer_slug__c = trim(strip_tags($booking_offer_slug));
        }

        // CouponCode__c = String
        if (!empty($couponCode)) {
            $sObject->CouponCode__c = trim(strip_tags($couponCode));
        }

        if(!empty($contactPreference)) {
            $sObject->ContactPreference__c = trim(strip_tags($contactPreference));
        }

        if(!empty($recallPreference)) {
            $sObject->TV_zeitfenster_erreichbarkeit__c = trim(strip_tags($recallPreference));
        }

        if(!empty($recallPhone)) {
            $sObject->TV_aktuallisierte_telefonummer__c = trim(strip_tags($recallPhone));
        }

        // ONLY update records (don't create). Yes, I know the name is still "upsert"
        // http://www.salesforce.com/us/developer/docs/api/Content/sforce_api_calls_upsert.htm
        $upsertResponse = $this->connection->upsert('Backend_ID__c', array($sObject), 'Patienten__c');

        $result = false;
        // Check if the transaction was successful
        if ($upsertResponse[0]->success) {
            $result = $upsertResponse[0]->id;
        } else {
            throw new \Exception('ERROR ON BOOKING '.$backend_id.': '.$upsertResponse[0]->errors[0]->message);
        }

        return $result;
    }

    /**
     * The isSFcreatedDateAfterThreshold function checks the creation date of
     * a SF Patienten record with Backend_id = $backendId. If the creation date is
     * after or equal 2013-08-09 00:00:00 or the record does not exist in SF at all it returns
     * true, otherwise it returns false.
     *
     * @param int $backend_id
     *
     * @return bool
     */
    private function isSFcreatedDateAfterThreshold($backend_id)
    {
        $record = $this->query('CreatedDate', "Backend_ID__c = '" . $backend_id . "'");
        if (!is_object($record)) {
            // if there is no record with that id than also return true as nothing can be damaged
            return true;
        }
        $createdDate = strtotime($record->CreatedDate . " + 2 hours");
        $threshold = strtotime("2013-08-09");

        return ($createdDate > $threshold);
    }

    /**
     * Get data from Salesforce
     *
     * @param string $select
     * @param string $where (WARNING: Always use ' instead of " wrapping the value)
     * @param string $table (optional)
     *
     * @return object|bool
     */
    public function query($select, $where, $table = 'Patienten__c')
    {
        $query = "SELECT " . $select . " FROM " . $table . " WHERE " . $where;
        $response = $this->connection->query($query);

        return isset($response->records[0]) ? $response->records[0] : null;
    }

    /**
     * The upsertLocation() function stores into Salesforce a new Account record
     * upon the creation of a new booking with a Location that does
     * not exist in the Account SF object
     *
     * @param int $backend_id
     * @param string $name
     * @param string $street
     * @param string $city
     * @param string $plz
     * @param string $phone
     * @param string $phone_visible
     * @param string $fax
     * @param string $website
     * @param string $fachgebiet
     * @param int $integration_id
     * @param string $vertragsart
     * @param string $email
     * @param string $quelle
     *
     * @return mixed
     */
    public function upsertLocation(
        $backend_id,
        $name,
        $street,
        $city,
        $plz,
        $phone,
        $phone_visible,
        $fax,
        $website,
        $fachgebiet,
        $integration_id,
        $vertragsart,
        $email,
        $quelle
    )
    {
        $sObject = new stdClass();
        $sObject->Name = $name;
        $sObject->BillingStreet = $street;
        $sObject->BillingCity = $city;
        $sObject->BillingPostalCode = $plz;
        $sObject->Phone = $phone;
        $sObject->Arzttermine_de_Telefon_Nummer__c = $phone_visible;
        $sObject->Fax = $fax;
        $sObject->Website = $website;
        $sObject->Fachgebiet__c = $fachgebiet;
        $sObject->Integration__c = $GLOBALS['CONFIG']['INTEGRATIONS'][$integration_id]['NAME'];
        $sObject->Vertragsart__c = $vertragsart;
        $sObject->email__c = $email;
        $sObject->Backend_ID__c = $backend_id;
        $sObject->Arzttermine_de_Telefon_Nummer__c = $phone;

        $upsertResponse = $this->connection->upsert('Backend_ID__c', array($sObject), 'Account');

        $result = false;
        // Check if the transaction was successful
        if ($upsertResponse[0]->success) {
            $result = true;
            // Then check if there was an insert instead of an update
            if ($upsertResponse[0]->created) {
                $result = $upsertResponse[0]->id;
            }
        }

        return $result;
    }

    /**
     * The deleteBookingById() function deletes the SF "Patienten" record with Backend_ID $id
     *
     * @param int $backend_id
     *
     * @return SforceEnterpriseClient|bool
     */
    public function deletePatientenById($backend_id)
    {
        if ($this->isSFcreatedDateAfterThreshold($backend_id)) {
            $record = $this->query('Id', "Backend_ID__c = '" . $backend_id . "'");
            if (!is_object($record)) {
                return false;
            }

            return $this->connection->delete($record->Id);
        }

        return false;
    }
}
