<?php
define('GENDER_MALE', 0);
define('GENDER_FEMALE', 1);

define('GERMANY_COUNTRY_TEL_CODE', 49);
define('GERMANY_CELL_PHONE_NUMBERS_START_WITH', 1);

define('PASSWORD_FIX_SALT','Dw5dGm%dm3!tdSJF3)t4');

define ("CURRENT_DAY_APP_SHOW_AFTER_MINUTES", 60);
define ("SHOW_NEXT_DAY_APPS_AFTER_THIS_TIME", "17:30");

define ("PATIENT_SHOW_OLD_BOOKINGS_IN_DAYS", 30);

/**
 * This set of constants have to be in sync with the
 * table "groups"
 */
/**
 * Admin Group
 */
define ("GROUP_ADMIN",1);
/**
 * Group for users with no function
 */
define ("GROUP_USER",2);
/**
 * Doctor Group
 */
define ("GROUP_DOCTOR",3);
/**
 * Patientsupport User Group
 */
define ("GROUP_PATIENTSUPPORT_USER",4);
/**
 * Patientsupport Admin Group
 */
define ("GROUP_PATIENTSUPPORT_ADMIN",5);
/**
 * API Group
 */
define ("GROUP_API",6);

define ("ASSET_OWNERTYPE_USER", 0);
define ("ASSET_OWNERTYPE_LOCATION", 1);

define ("ASSET_GFX_SIZE_ORIGINAL", 0);
define ("ASSET_GFX_SIZE_STANDARD", 1);
define ("ASSET_GFX_SIZE_THUMBNAIL", 2);
define ("ASSET_GFX_SIZE_MEDIUM_PORTRAIT", 3);
define ("ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED", 4);
define ("ASSET_GFX_SIZE_MEDIUM_LANDSCAPE", 5);
define ("ASSET_GFX_SIZE_MEDIUM_LANDSCAPE_CROPPED", 6);

define ("MEDICAL_SPECIALTY_STATUS_DRAFT", 0);
define ("MEDICAL_SPECIALTY_STATUS_ACTIVE", 1);

define ("WORKING_HOUR_START", '08:00');
define ("WORKING_HOUR_END", '15:00');
define ("NEXT_WORKING_APTS_FOR_NOT_WORKING_HOURS_AFTER", '10:00');

define ("TREATMENTTYPE_STATUS_DRAFT", 0);
define ("TREATMENTTYPE_STATUS_ACTIVE", 1);

define ("INSURANCE_STATUS_DRAFT", 0);
define ("INSURANCE_STATUS_ACTIVE", 1);

define ("LOCATION_STATUS_DRAFT", 0);
define ("LOCATION_STATUS_ACTIVE",1);
define ("LOCATION_STATUS_VISIBLE", 2);
define ("LOCATION_STATUS_VISIBLE_APPOINTMENTS", 3);

define ("USER_STATUS_DRAFT", 0);
// show the profile, allow user to login
define ("USER_STATUS_ACTIVE", 1);
// show the profile, allow user to login, show profile in search, show no appointments
define ("USER_STATUS_VISIBLE", 2);
// show the profile, allow user to login, show profile in search, show appointments
define ("USER_STATUS_VISIBLE_APPOINTMENTS", 3);

define ("BOOKING_INTEGRATION_STATUS_OPEN", 0);
define ("BOOKING_INTEGRATION_STATUS_BOOKED", 1);
define ("BOOKING_INTEGRATION_STATUS_ERROR", 2);
define ("BOOKING_INTEGRATION_STATUS_CANCELED", 3);

define ("BLOGPOST_STATUS_DRAFT", 0);
define ("BLOGPOST_STATUS_PUBLISHED", 1);

define ("SEARCH_RESULT_TEXT_DRAFT", 0);
define ("SEARCH_RESULT_TEXT_ACTIVE", 1);

define ("WIDGET_STATUS_DRAFT", 0);
define ("WIDGET_STATUS_ACTIVE", 1);

// class user: checkPrivilege
define ("ADMIN_GROUP_ADMIN", GROUP_ADMIN);

define ("CMS_DIRECTORY_INDEX_FILENAME", 'index');
define ("CMS_USE_DIRECTORY_INSTEAD_OF_INDEX_FILENAME", true);

define ("MAILING_TYPE_TEXT", 1);
define ("MAILING_TYPE_HTML", 2);
define ("MAILING_TYPE_HTML_TEXT", 3);
define ("MAILING_TYPE_HTML_AUTOTEXT", 4);

define ("MAILING_SOURCE_TYPE_ID_BOOKING", 1);

define ("MAILING_STATUS_ERROR", 0);
define ("MAILING_STATUS_SEND", 1);

define("REFERRAL_STATUS_NEW", 0);
define("REFERRAL_STATUS_IN_PROCESSING", 1);
define("REFERRAL_STATUS_APPOINTMENT_COMPLETION_FAILED", 2);
define("REFERRAL_STATUS_APPOINTMENT_COMPLETION_UNABLE", 3);
define("REFERRAL_STATUS_APPOINTMENT_COMPLETION_PROBLEM", 4);
define("REFERRAL_STATUS_APPOINTMENT_COMPLETED", 5);
define("REFERRAL_STATUS_REWARD_SENT", 6);
define("REFERRAL_STATUS_REWARD_DENIED", 7);

define("REFERRAL_INVITATION_VIA_EMAIL", 0);
define("REFERRAL_INVITATION_VIA_FACEBOOK", 1);
define("REFERRAL_INVITATION_VIA_GPLUS", 2);
define("REFERRAL_INVITATION_VIA_TWITTER", 3);
define("REFERRAL_INVITATION_VIA_SMS", 4);

define('LOGIN_DOCTOR_TYPE', 1);
define('LOGIN_PATIENT_TYPE', 2);
define('LOGIN_AFFILIATE_TYPE', 3);

define('INSURANCE_TYPE_PRIVATE', 1);
define('INSURANCE_TYPE_PUBLIC', 2);

// Days of the week according to date('w')
define('SUNDAY',    0);
define('MONDAY',    1);
define('TUESDAY',   2);
define('WEDNESDAY', 3);
define('THURSDAY',  4);
define('FRIDAY',    5);
define('SATURDAY',  6);

// ***************************************

if (!isset($ROOT)) { $ROOT = realpath($_SERVER["DOCUMENT_ROOT"].'/..'); }
if (!isset($SHARED)) { $SHARED = realpath($_SERVER["DOCUMENT_ROOT"].'/../../../shared'); }


$CONFIG = array(
    "MEMCACHED_SERVER" => "memcached",
    "MADMIMI_ENABLE" => false,
    "MADMIMI_USER" => "madmimi@arzttermine.de",
    "MADMIMI_TOKEN" => "2c983932e452a4300fcfacf8920e3e85",
    "MADMIMI_NEWSLETTER_PROMOTION" => "",
    "DOCTORS_LOGIN_URL" => "aerzte",
    "PATIENTS_URL" => "patienten",
    "PATIENTS_LOGIN_URL" => "patienten/login",
    "PATIENTS_REGISTER_URL" => "patienten/registrierung",
    "AFFILIATES_URL" => "affiliates",
    "AFFILIATES_LOGIN_URL" => "affiliates-login",
    "SYSTEM_DEBUG"								=>false,
    "SYSTEM_PRODUCT_TITLE"						=>"Arzttermine.de",
    "SYSTEM_CMS_NAME"							=>"kuzmik cms v2.0",
    "SYSTEM_SLOGAN"								=>"",
    "SYSTEM_CHARSET"							=>'UTF-8',
    "SYSTEM_DB_CHARSET"							=>'UTF8',
    "SYSTEM_GMT_OFFSET"							=>1,
    "SYSTEM_TIMEZONE"							=>'Europe/Berlin',
    "SYSTEM_SESSION_NAME"						=>'PHPSESSID',
    "SYSTEM_SESSION_MAXLIFETIME"				=>3600,
    "SYSTEM_SESSION_SAVE_PATH"					=>DISTRIBUTED_PATH."/sessions",
    "SYSTEM_ALLOW_DEBUG_IP"						=>"127.0.0.1",
    "SYSTEM_EMAIL_ADMIN"						=>"axel@kuzmik.de",
    "SYSTEM_PASSWORD_CHARS_MIN"					=>6,
    "SYSTEM_NGS_CONSTANTS"						=>$ROOT."/ngs/conf/constants.php",
    "SYSTEM_PATH_LOCALE"						=>$ROOT."/include/locale",

    "SYSTEM_FILE_INTEGRATIONS_CONFIG"			=>$ROOT."/src/arzttermine/Arzttermine/Integration/Integration/config.php",

    "SYSTEM_KEY_CRYPT"							=>'nreutfn8658zfdfslsdnf43z5qbxydsk',
    "SYSTEM_MAIL_HOST"							=>'maildev',
    "SYSTEM_MAIL_IS_SMTP"						=>true,
    "SYSTEM_MAIL_SMTP_AUTH"						=>false,
    "SYSTEM_MAIL_SMTP_USER"                     => '',
    "SYSTEM_MAIL_SMTP_PASS"                     => '',
    "SYSTEM_MAIL_SMTP_DEBUG"					=>false,
    "SYSTEM_MAIL_SMTP_SECURE"					=>'', //TLS
    "SYSTEM_MAIL_USERID"						=>'',
    "SYSTEM_MAIL_PASSWORD"						=>'',
    "SYSTEM_MAIL_CHARSET"						=>'UTF-8',
    "SYSTEM_MAIL_FROM_EMAIL"					=>'info@arzttermine.de',	// Default "From:" address for all outgoing emails
    "SYSTEM_MAIL_FROM_NAME"						=>'Arzttermine.de',			// Default "From:" name for all outgoing emails
    "SYSTEM_MAIL_BCC_FROM_EMAIL"				=>'app.bcc@arzttermine.de',	// send every email also to this address unless CONFIG.MAILING_SEND_BCC
    "SYSTEM_MAIL_BCC_FROM_NAME"					=>'app.bcc@arzttermine.de',

    "CONTACTS_EMAIL_ADDRESS"					=>'info@arzttermine.de',
    "CONTACTS_EMAIL_NAME"						=>'Arzttermine.de',
    "BOOKINGS_EMAIL_ADDRESS"					=>'buchung@arzttermine.de',
    "BOOKINGS_EMAIL_NAME"						=>'Arzttermine.de',
    "DOCTORS_EMAIL_ADDRESS"						=>'kunden@arzttermine.de',
    "DOCTORS_EMAIL_NAME"						=>'Arzttermine.de',
    "REVIEWS_EMAIL_ADDRESS"						=>'support@arzttermine.de',
    "REVIEWS_EMAIL_NAME"						=>'Arzttermine.de',
    "SALES_EMAIL_ADDRESS"						=>'registrierung@arzttermine.de',
    "EKOMI_EMAIL_ADDRESS"						=>'39777-arztterminede@connect.ekomi.de',

    "WIDGET_CONTROLLER_PATH"					=>$ROOT."/include/lib/widget",
    "PROFILE_EDIT_EMAIL_ADDRESS"				=>'praxen-management@arzttermine.de',

    "MEDIA_PATH"								=>DISTRIBUTED_PATH."/media/",
    "MEDIA_URL"									=>"/media/",	// mediathek uploads
    "ASSET_PATH"								=>DISTRIBUTED_PATH.'/asset/',
    "ASSET_URL"									=>"/asset/",	// user generated content
    "STATIC_PATH"								=>$ROOT."/htdocs/static/",
    "STATIC_URL"								=>"/static/",	// css, js, ...

    "DOMAIN"									=>"www.arzttermine.de",
    "URL_HTTP_LIVE"								=>"http://www.arzttermine.de",
    "URL_HTTPS_LIVE"							=>"https://www.arzttermine.de",
    "URL_SET_THEME_DEFAULT"						=>"http://www.arzttermine.de/theme-default",
    "URL_SET_THEME_MOBILE"						=>"http://www.arzttermine.de/theme-mobile",
    "MOBILE_DOMAIN"								=>"m.test.arzttermine.de",
    "MOBILE_URL_HTTP_LIVE"						=>"https://m.arzttermine.de",
    "URL_PROFILE"								=>"/profil",
    "URL_LOGIN"									=>"/login.php",
    "URL_LOGOUT"								=>"/?logout",
    "URL_SIGNUP"								=>"/anmelden",

    "WWW_FILE_DEBUG"							=>$ROOT."/include/debug.php",

    "CALENDAR_VISIBLE_TIMES_FROM"               =>"06:00",
    "CALENDAR_VISIBLE_TIMES_TO"                 =>"20:00",
    "APPOINTMENT_DEFAULT_INCREMENT"             =>"15",
    "APPOINTMENT_DURATION_OPTIONS"              =>array(5,10,15,20,25,30,45,60,75,90,105,120),

    "SMARTY"                                    => array(
        "TEMPLATE_DIR"      => $ROOT."/include/templates",
        "COMPILE_DIR"       => SHARED_PATH . "/templates_c",
        "PLUGIN_DIR"        => SYSTEM_PATH . "/vendor/4nd/smarty/distribution/libs/plugins",
        "DEBUG"             => false,
        "FORCE_COMPILE"     => false,
        "COMPILE_CHECK"     => true
    ),

    
    "CMS_LOCALE_DEFAULT"						=>'de',
    "CMS_LOCALE"								=>array(
        'de' =>array(
            'code'=>'de',
            'code_locale'=>'de_DE',
            'name'=>'Deutsch',
            'icon'=>'adm/de.gif',
            'url_prefix'=>'',
        ),
        'en' =>array(
            'code'=>'en',
            'code_locale'=>'en_EN',
            'name'=>'English',
            'icon'=>'adm/en.gif',
            'url_prefix'=>'/en',
        )
    ),

    'CMS_ENGINES' =>array(
        1=>array(
            'name'=>'HTML / shortcode + auto P',
            'function'=>'engine_static_shortcode_autop',
        ),
        3=>array(
            'name'=>'HTML / shortcode',
            'function'=>'engine_static_shortcode',
        ),
        0=>array(
            'name'=>'HTML',
            'function'=>'engine_static',
        ),
        2=>array(
            'name'=>'PHP',
            'function'=>'engine_php',
        ),
    ),

    'CMS_TEMPLATES' =>array(
        1=>array(
            'name'=>'Content: Full (WYSIWYG)',
            'template'=>'content_full.tpl',
            'version_contents'=>array(
                array(
                    'name'=>'Content',
                    'version_content'=>'content1',
                    'editors'=>array(
                        'default'=>'admin/cms/_form_version_edit_editor_textarea.tpl',
                        'tinymce'=>'admin/cms/_form_version_edit_editor_tinymce.tpl',
                    )
                )
            )
        ),
        2=>array(
            'name'=>'Content: Full (Editor)',
            'template'=>'content_full.tpl',
            'version_contents'=>array(
                array(
                    'name'=>'Content',
                    'version_content'=>'content1',
                    'editors'=>array(
                        'default'=>'admin/cms/_form_version_edit_editor_textarea.tpl',
                    )
                )
            )
        ),
        3=>array(
            'name'=>'Content: Content Left/ Sidebar Right (WYSIWYG)',
            'template'=>'content_left_sidebar_right.tpl',
            'version_contents'=>array(
                array(
                    'name'=>'Content',
                    'version_content'=>'content1',
                    'editors'=>array(
                        'default'=>'admin/cms/_form_version_edit_editor_textarea.tpl',
                        'tinymce'=>'admin/cms/_form_version_edit_editor_tinymce.tpl',
                    ),
                ),
                array(
                    'name'=>'Navigation',
                    'version_content'=>'content2',
                    'editors'=>array(
                        'default'=>'admin/cms/_form_version_edit_editor_textarea.tpl',
                    )
                )
            )
        ),
        4=>array(
            'name'=>'Content: Content Left/ Sidebar Right (Editor)',
            'template'=>'content_left_sidebar_right.tpl',
            'version_contents'=>array(
                array(
                    'name'=>'Content',
                    'version_content'=>'content1',
                    'editors'=>array(
                        'default'=>'admin/cms/_form_version_edit_editor_textarea.tpl',
                    ),
                ),
                array(
                    'name'=>'Navigation',
                    'version_content'=>'content2',
                    'editors'=>array(
                        'default'=>'admin/cms/_form_version_edit_editor_textarea.tpl',
                    )
                )
            )
        ),
        11=>array(
            'name'=>'Content: Sidebar Left/ Content Right (WYSIWYG)',
            'template'=>'content_right_sidebar_left.tpl',
            'version_contents'=>array(
                array(
                    'name'=>'Content',
                    'version_content'=>'content1',
                    'editors'=>array(
                        'default'=>'admin/cms/_form_version_edit_editor_textarea.tpl',
                        'tinymce'=>'admin/cms/_form_version_edit_editor_tinymce.tpl',
                    ),
                ),
                array(
                    'name'=>'Navigation',
                    'version_content'=>'content2',
                    'editors'=>array(
                        'default'=>'admin/cms/_form_version_edit_editor_textarea.tpl',
                    )
                )
            )
        ),
        12=>array(
            'name'=>'Content: Sidebar Left/ Content Right (Editor)',
            'template'=>'content_right_sidebar_left.tpl',
            'version_contents'=>array(
                array(
                    'name'=>'Content',
                    'version_content'=>'content1',
                    'editors'=>array(
                        'default'=>'admin/cms/_form_version_edit_editor_textarea.tpl',
                    ),
                ),
                array(
                    'name'=>'Navigation',
                    'version_content'=>'content2',
                    'editors'=>array(
                        'default'=>'admin/cms/_form_version_edit_editor_textarea.tpl',
                    )
                )
            )
        ),
        5=>array(
            'name'=>'Homepage',
            'template'=>'homepage.tpl',
            'version_contents'=>array(
                array(
                    'name'=>'Content',
                    'version_content'=>'content1',
                    'editors'=>array(
                        'default'=>'admin/cms/_form_version_edit_editor_textarea.tpl',
                    )
                )
            )
        ),
        6=>array(
            'name'=>'Formular: Kontakt',
            'template'=>'contact/contact.tpl',
            'includes'=>$ROOT."/htdocs/content/contact.php",
            'version_contents'=>array(
                array(
                    'name'=>'Content',
                    'version_content'=>'content1',
                    'editors'=>array(
                        'default'=>'admin/cms/_form_version_edit_editor_textarea.tpl',
                    )
                ),
                array(
                    'name'=>'Navigation',
                    'version_content'=>'content2',
                    'editors'=>array(
                        'default'=>'admin/cms/_form_version_edit_editor_textarea.tpl',
                    )
                )
            )
        ),
        7=>array(
            'name'=>'Mail: Text mit Footer',
            'template'=>'mail_text_footer.tpl',
            'version_contents'=>array(
                array(
                    'name'=>'Content',
                    'version_content'=>'content1',
                    'editors'=>array(
                        'default'=>'admin/cms/_form_version_edit_editor_textarea.tpl',
                    )
                )
            )
        ),
        16=>array(
            'name'=>'Mail: HTML',
            'template'=>'mail/mail-generic.tpl',
            'version_contents'=>array(
                array(
                    'name'=>'Content',
                    'version_content'=>'content1',
                    'editors'=>array(
                        'default'=>'admin/cms/_form_version_edit_editor_textarea.tpl',
                    )
                )
            )
        ),
        8=>array(
            'name'=>'Formular Vertikal',
            'template'=>'content_full_form_vertical.tpl',
            'version_contents'=>array(
                array(
                    'name'=>'Content',
                    'version_content'=>'content1',
                    'editors'=>array(
                        'default'=>'admin/cms/_form_version_edit_editor_textarea.tpl',
                    )
                )
            )
        ),
        9=>array(
            'name'=>'Formular Horizontal',
            'template'=>'content_full_form_horizontal.tpl',
            'version_contents'=>array(
                array(
                    'name'=>'Content',
                    'version_content'=>'content1',
                    'editors'=>array(
                        'default'=>'admin/cms/_form_version_edit_editor_textarea.tpl',
                    )
                )
            )
        ),
        10=>array(
            'name'=>'Suche',
            'template'=>'search/search.tpl',
            'version_contents'=>array(
                array(
                    'name'=>'Content',
                    'version_content'=>'content1',
                    'editors'=>array(
                        'default'=>'admin/cms/_form_version_edit_editor_textarea.tpl',
                    )
                ),
                array(
                    'name'=>'Navigation',
                    'version_content'=>'content2',
                    'editors'=>array(
                        'default'=>'admin/cms/_form_version_edit_editor_textarea.tpl',
                    )
                )
            )
        ),
        13=>array(
            'name'=>'mobile: Content: Full (WYSIWYG)',
            'template'=>'mobile/content_full.tpl',
            'version_contents'=>array(
                array(
                    'name'=>'Content',
                    'version_content'=>'content1',
                    'editors'=>array(
                        'default'=>'admin/cms/_form_version_edit_editor_textarea.tpl',
                        'tinymce'=>'admin/cms/_form_version_edit_editor_tinymce.tpl',
                    )
                )
            )
        ),
        14=>array(
            'name'=>'mobile: Content: Full (Editor)',
            'template'=>'mobile/content_full.tpl',
            'version_contents'=>array(
                array(
                    'name'=>'Content',
                    'version_content'=>'content1',
                    'editors'=>array(
                        'default'=>'admin/cms/_form_version_edit_editor_textarea.tpl',
                    )
                )
            )
        ),
        15=>array(
            'name'=>'mobile: Homepage',
            'template'=>'mobile/homepage.tpl',
            'includes'=>$ROOT."/htdocs/content/homepage.php",
            'version_contents'=>array(
                array(
                    'name'=>'Content',
                    'version_content'=>'content1',
                    'editors'=>array(
                        'default'=>'admin/cms/_form_version_edit_editor_textarea.tpl',
                    )
                )
            )
        ),
        99=>array(
            'name'=>'Dynamic Content',
            'template'=>'content_dynamic.tpl',
            'version_contents'=>array(
                array(
                    'name'=>'Dynamic Content',
                    'version_content'=>'content1',
                    'editors'=>array(
                        'default'=>'admin/cms/_form_version_edit_editor_textarea.tpl',
                    )
                ),
                array(
                    'name'=>'Sidebar',
                    'version_content'=>'content2',
                    'editors'=>array(
                        'default'=>'admin/cms/_form_version_edit_editor_textarea.tpl',
                    )
                )
            )
        ),
        100=>array(
            'name'=>'No Template',
            'template'=>'no_template.tpl',
            'version_contents'=>array(
                array(
                    'name'=>'Content',
                    'version_content'=>'content1',
                    'editors'=>array(
                        'default'=>'admin/cms/_form_version_edit_editor_textarea.tpl',
                    )
                ),
            )
        ),
    ),
    "ASSET_ALLOW_MULTIUPLOAD"					=>false,
    "ASSET_USE_LAST_ASSET_AS_PROFILE_ASSET"		=>false,	// set this to false to speed up the profile asset calculation a bit
    "ASSET_USE_ORIGINAL_FILENAME"				=>true,
    "ASSET_POSSIBLE_FILEEXTENSIONS"				=>array("gif","png","jpg","jpeg","pdf","doc","docx","xls","xlsx"),
    "ASSET_GFX_CHARS_UID_POSTFIX"				=>6,
    "ASSET_GFX_POSSIBLE_MIMETYPES"				=>array('image/jpeg','image/pjpeg','image/gif','image/png'),
    "ASSET_GFX_TYPES" => array(
        ASSET_OWNERTYPE_USER => array(
            "OWNERTYPE" => ASSET_OWNERTYPE_USER,
            "KEEP_ORIGINAL_FILE" => false,
            "USE_ORIGINAL_FILENAME" => true,
            "SIZES"	=> array(
                ASSET_GFX_SIZE_STANDARD					=>array("x"=>800,"y"=>800,"postfix"=>"_".ASSET_GFX_SIZE_STANDARD,"dont_stretch_small"=>true,"scale_natural"=>true,"default"=>'asset/user_150x200.jpg',"default_group_doctors"=>array(0=>array('asset/doctor/m1_1.jpg','asset/doctor/m2_1.jpg','asset/doctor/m3_1.jpg','asset/doctor/m4_1.jpg'),1=>array('asset/doctor/f1_1.jpg','asset/doctor/f2_1.jpg','asset/doctor/f3_1.jpg','asset/doctor/f4_1.jpg'))),
                ASSET_GFX_SIZE_THUMBNAIL				=>array("x"=>50, "y"=>50, "postfix"=>"_".ASSET_GFX_SIZE_THUMBNAIL,"dont_stretch_small"=>false,"scale_natural"=>false,"default"=>'asset/user_50x50.jpg',"default_group_doctors"=>array(0=>array('asset/doctor/m1_2.jpg','asset/doctor/m2_2.jpg','asset/doctor/m3_2.jpg','asset/doctor/m4_2.jpg'),1=>array('asset/doctor/f1_2.jpg','asset/doctor/f2_2.jpg','asset/doctor/f3_2.jpg','asset/doctor/f4_2.jpg'))),
                ASSET_GFX_SIZE_MEDIUM_PORTRAIT			=>array("x"=>150,"y"=>200,"postfix"=>"_".ASSET_GFX_SIZE_MEDIUM_PORTRAIT,"dont_stretch_small"=>true,"scale_natural"=>true,"default"=>'asset/user_150x200.jpg',"default_group_doctors"=>array(0=>array('asset/doctor/m1_3.jpg','asset/doctor/m2_3.jpg','asset/doctor/m3_3.jpg','asset/doctor/m4_3.jpg'),1=>array('asset/doctor/f1_3.jpg','asset/doctor/f2_3.jpg','asset/doctor/f3_3.jpg','asset/doctor/f4_3.jpg'))),
                ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED	=>array("x"=>150,"y"=>200,"postfix"=>"_".ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED,"dont_stretch_small"=>false,"scale_natural"=>false,"default"=>'asset/user_150x200.jpg',"default_group_doctors"=>array(0=>array('asset/doctor/m1_4.jpg','asset/doctor/m2_4.jpg','asset/doctor/m3_4.jpg','asset/doctor/m4_4.jpg'),1=>array('asset/doctor/f1_4.jpg','asset/doctor/f2_4.jpg','asset/doctor/f3_4.jpg','asset/doctor/f4_4.jpg'))),
                ASSET_GFX_SIZE_MEDIUM_LANDSCAPE			=>array("x"=>200,"y"=>150,"postfix"=>"_".ASSET_GFX_SIZE_MEDIUM_LANDSCAPE,"dont_stretch_small"=>true,"scale_natural"=>true,"default"=>'asset/user_200x150.jpg',"default_group_doctors"=>array(0=>array('asset/doctor/m1_5.jpg','asset/doctor/m2_5.jpg','asset/doctor/m3_5.jpg','asset/doctor/m4_5.jpg'),1=>array('asset/doctor/f1_5.jpg','asset/doctor/f2_5.jpg','asset/doctor/f3_5.jpg','asset/doctor/f4_5.jpg'))),
                ASSET_GFX_SIZE_MEDIUM_LANDSCAPE_CROPPED	=>array("x"=>200,"y"=>150,"postfix"=>"_".ASSET_GFX_SIZE_MEDIUM_LANDSCAPE_CROPPED,"dont_stretch_small"=>false,"scale_natural"=>false,"default"=>'asset/user_200x150.jpg',"default_group_doctors"=>array(0=>array('asset/doctor/m1_6.jpg','asset/doctor/m2_6.jpg','asset/doctor/m3_6.jpg','asset/doctor/m4_6.jpg'),1=>array('asset/doctor/f1_6.jpg','asset/doctor/f2_6.jpg','asset/doctor/f3_6.jpg','asset/doctor/f4_6.jpg'))),
            )
        ),
        ASSET_OWNERTYPE_LOCATION => array(
            "OWNERTYPE" => ASSET_OWNERTYPE_LOCATION,
            "KEEP_ORIGINAL_FILE" => false,
            "USE_ORIGINAL_FILENAME" => true,
            "SIZES"	=> array(
                ASSET_GFX_SIZE_STANDARD					=>array("x"=>800,"y"=>800,"postfix"=>"_".ASSET_GFX_SIZE_STANDARD,"dont_stretch_small"=>true,"scale_natural"=>true,"default"=>'asset/location_150x200.jpg'),
                ASSET_GFX_SIZE_THUMBNAIL				=>array("x"=>50, "y"=>50, "postfix"=>"_".ASSET_GFX_SIZE_THUMBNAIL,"dont_stretch_small"=>false,"scale_natural"=>false,"default"=>'asset/location_50x50.jpg'),
                ASSET_GFX_SIZE_MEDIUM_PORTRAIT			=>array("x"=>150,"y"=>200,"postfix"=>"_".ASSET_GFX_SIZE_MEDIUM_PORTRAIT,"dont_stretch_small"=>true,"scale_natural"=>true,"default"=>'asset/location_150x200.jpg'),
                ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED	=>array("x"=>150,"y"=>200,"postfix"=>"_".ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED,"dont_stretch_small"=>false,"scale_natural"=>false,"default"=>'asset/location_150x200.jpg'),
                ASSET_GFX_SIZE_MEDIUM_LANDSCAPE			=>array("x"=>200,"y"=>150,"postfix"=>"_".ASSET_GFX_SIZE_MEDIUM_LANDSCAPE,"dont_stretch_small"=>true,"scale_natural"=>true,"default"=>'asset/location_200x150.jpg'),
                ASSET_GFX_SIZE_MEDIUM_LANDSCAPE_CROPPED	=>array("x"=>200,"y"=>150,"postfix"=>"_".ASSET_GFX_SIZE_MEDIUM_LANDSCAPE_CROPPED,"dont_stretch_small"=>false,"scale_natural"=>false,"default"=>'asset/location_200x150.jpg'),
            )
        ),
    ),
    "IMAGE_RESIZE_JPEG_QUALITY_PERCENT"				=>90,
    "FIXED_WEEK"                                                                    =>array(0=>"Dynamic",1=>"Fixed"),
    "ARRAY_GENDER"									=>array(GENDER_MALE=>"Herr",GENDER_FEMALE=>"Frau"),
    "ARRAY_GENDER_SHORT"							=>array(GENDER_MALE=>"M",GENDER_FEMALE=>"W"),
    "ARRAY_NO_YES"									=>array(0=>"Nein",1=>"Ja"),
    "ARRAY_NULL_NO_YES"								=>array(null=>"%",0=>"Nein",1=>"Ja"),

    "USER_FIRST_NAME_MIN_CHARACTERS"				=> 3,
    "CITY_SLASH_REPLACEMENT"						=> '---',
    "USERS_PAGINATION_NUM"							=>20,

    "MEDICAL_SPECIALTY_STATUS" => array(
        MEDICAL_SPECIALTY_STATUS_DRAFT => '<span class="red">Draft</span>',
        MEDICAL_SPECIALTY_STATUS_ACTIVE => '<span class="green">Aktiv</span>',
    ),
    "TREATMENTTYPE_STATUS" => array(
        TREATMENTTYPE_STATUS_DRAFT => '<span class="red">Draft</span>',
        TREATMENTTYPE_STATUS_ACTIVE => '<span class="green">Aktiv</span>',
    ),
    "INSURANCE_STATUS" => array(
        INSURANCE_STATUS_DRAFT => '<span class="red">Draft</span>',
        INSURANCE_STATUS_ACTIVE => '<span class="green">Aktiv</span>',
    ),

    "MAILING_STATUS" =>array(
        MAILING_STATUS_ERROR =>'Fehler beim Senden',
        MAILING_STATUS_SEND =>'Versendet',
    ),
    "USER_STATUS" =>array(
        USER_STATUS_DRAFT =>'<span class="red">Draft</span>',
        USER_STATUS_ACTIVE =>'<span class="green">Aktiv</span>',
        USER_STATUS_VISIBLE =>'<span class="green">Sichtbar</span>',
        USER_STATUS_VISIBLE_APPOINTMENTS =>'<span class="green">Sichtbar + Termine</span>',
    ),
    "LOCATION_STATUS" =>array(
        LOCATION_STATUS_DRAFT =>'<span class="red">Draft</span>',
        LOCATION_STATUS_ACTIVE =>'<span class="green">Aktiv</span>',
        LOCATION_STATUS_VISIBLE =>'<span class="orange">Sichtbar</span>',
        LOCATION_STATUS_VISIBLE_APPOINTMENTS =>'<span class="green">Sichtbar + Termine</span>',
    ),
    "BOOKING_INTEGRATION_STATUS" =>array(
        BOOKING_INTEGRATION_STATUS_OPEN =>'Offen',
        BOOKING_INTEGRATION_STATUS_BOOKED =>'<span class="green">Gebucht</span>',
        BOOKING_INTEGRATION_STATUS_ERROR =>'<span class="red">Fehler</span>',
        BOOKING_INTEGRATION_STATUS_CANCELED =>'<span class="orange">Storniert</span>',
    ),
    "REFERRAL_STATUS" =>array(
        REFERRAL_STATUS_NEW =>'Neu',
        REFERRAL_STATUS_IN_PROCESSING =>'In Bearbeitung',
        REFERRAL_STATUS_APPOINTMENT_COMPLETION_FAILED =>'Terminvereinbarung fehlgeschlagen',
        REFERRAL_STATUS_APPOINTMENT_COMPLETION_UNABLE =>'Terminvereinbarung nicht möglich',
        REFERRAL_STATUS_APPOINTMENT_COMPLETION_PROBLEM =>'Problem bei Terminvereinbarung',
        REFERRAL_STATUS_APPOINTMENT_COMPLETED =>'Terminvereinbarung erfolgreich',
        REFERRAL_STATUS_REWARD_SENT =>'Gutschein gesendet',
        REFERRAL_STATUS_REWARD_DENIED =>'Gutschein verweigert',

    ),
    "REFERRAL_INVITATION_VIA" =>array(
        REFERRAL_INVITATION_VIA_EMAIL =>'Email',
        REFERRAL_INVITATION_VIA_FACEBOOK =>'Facebook',
        REFERRAL_INVITATION_VIA_GPLUS =>'Google+',
        REFERRAL_INVITATION_VIA_TWITTER =>'Twitter',
        REFERRAL_INVITATION_VIA_SMS=>'Sms',
    ),

    "BACKEND_MARKER_CLASSES" =>array(
        ''			=> '',
        'green'		=> '<span class="green">Grün</span>',
        'blue'		=> '<span class="blue">Blau</span>',
        'orange'	=> '<span class="orange">Orange</span>',
        'red'		=> '<span class="red">Rot</span>',
    ),
    "SEARCH_RESULT_TEXT_STATUS" =>array(
        SEARCH_RESULT_TEXT_DRAFT =>'<span class="red">Draft</span>',
        SEARCH_RESULT_TEXT_ACTIVE =>'<span class="green">Aktiv</span>'
    ),

    "INSURANCES" => array(
        INSURANCE_TYPE_PRIVATE => 'Privat versichert',
        INSURANCE_TYPE_PUBLIC => 'Gesetzlich versichert',
    ),
    "INSURANCE_SLUGS" => array(
        INSURANCE_TYPE_PRIVATE => 'pkv',
        INSURANCE_TYPE_PUBLIC => 'gkv',
    ),
    "RATING_VALUES"			=> array("0"=>"0","0.5"=>"0.5","1"=>"1","1.5"=>"1.5","2"=>"2","2.5"=>"2.5","3"=>"3","3.5"=>"3.5","4"=>"4","4.5"=>"4.5","5"=>"5"),
    "PHONE_SUPPORT"			=> '0800 2222 133',
    "PHONE_SUPPORT_PLAIN"   => '08002222133',
    "CALENDAR_TIMES_SHOW_MORE_LINK_AFTER_HOW_MANY_TIMES" => 3,
    "CALENDAR_MAX_DAYS_AHEAD" => 7*10,	// => 20weeks
    "CALENDAR_MAX_DAYS_ON_ONE_PAGE" => 7,	// for sanitizing
    "SEARCH_CALENDAR_MAX_SLIDES" => 5,
    "SEARCH_CALENDAR_MAX_DAYS" => 90,

    "LOCATION_COUNTRY_CODES" => array(
        'DE'    => 'Deutschland',
        'AT'    => 'Österreich',
        'CH'    => 'Schweiz',
    ),

    "LOCATIONS_SELECT" => array(
        'Berlin'			=> 'Berlin',
        'Bremen'			=> 'Bremen',
        'Dresden'			=> 'Dresden',
        'Dortmund'			=> 'Dortmund',
        'Düsseldorf'		=> 'Düsseldorf',
        'Essen'				=> 'Essen',
        'Frankfurt'			=> 'Frankfurt',
        'Hamburg' 			=> 'Hamburg',
        'Hannover'			=> 'Hannover',
        'Köln'				=> 'Köln',
        'Leipzig'			=> 'Leipzig',
        'München' 			=> 'München',
        'Nürnberg' 			=> 'Nürnberg',
        'Stuttgart' 		=> 'Stuttgart',
        'Koblenz'			=> 'Koblenz'
    ),

    "DATE_MONTHS" => array('1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6','7'=>'7','8'=>'8','9'=>'9','10'=>'10','11'=>'11','12'=>'12'),
    "DATE_MONTHS_0" => array(''=>'Monat','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6','7'=>'7','8'=>'8','9'=>'9','10'=>'10','11'=>'11','12'=>'12'),
    "DATE_DAYS" => array('1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6','7'=>'7','8'=>'8','9'=>'9','10'=>'10','11'=>'11','12'=>'12','13'=>'13','14'=>'14','15'=>'15','16'=>'16','17'=>'17','18'=>'18','19'=>'19','20'=>'20','21'=>'21','22'=>'22','23'=>'23','24'=>'24','25'=>'25','26'=>'26','27'=>'27','28'=>'28','29'=>'29','30'=>'30','31'=>'31'),
    "DATE_DAYS_0" => array(''=>'Tag','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6','7'=>'7','8'=>'8','9'=>'9','10'=>'10','11'=>'11','12'=>'12','13'=>'13','14'=>'14','15'=>'15','16'=>'16','17'=>'17','18'=>'18','19'=>'19','20'=>'20','21'=>'21','22'=>'22','23'=>'23','24'=>'24','25'=>'25','26'=>'26','27'=>'27','28'=>'28','29'=>'29','30'=>'30','31'=>'31'),
    "CALENDAR_WEEKDAY_TEXTS" => array('0'=>'Sonntag','1'=>'Montag','2'=>'Dienstag','3'=>'Mittwoch','4'=>'Donnerstag','5'=>'Freitag','6'=>'Samstag','7'=>'Sonntag'),
    "CALENDAR_SHORT_WEEKDAY_TEXTS" => array('0'=>'So.','1'=>'Mo.','2'=>'Di.','3'=>'Mi.','4'=>'Do.','5'=>'Fr.','6'=>'Sa.'),

    "SEARCH_DISTANCES_TEXTS"				=> array(
        0=>'5km',
        1=>'10km',
        2=>'50km',
    ),
    "SEARCH_DISTANCES_KM"					=> array(
        0=>'5',
        1=>'10',
        2=>'50'
    ),
    "SEARCH_DISTANCE_ORDER_SQL"				=> array(
        0=>' ORDER BY position_factor DESC, distance ASC',
        1=>' ORDER BY distance ASC',
        2=>' ORDER BY name ASC',
        3=>' ORDER BY zip ASC',
        4=>' ORDER BY city ASC',
    ),
    "SEARCH_MAX_RESULTS"						=> 2500,
    "SEARCH_PAGINATION_NUM"						=> 20,
    "SEARCH_ADD_SEARCH_LOCATION_MARKER"			=> true,
    
    // Search defaults
    "SEARCH_DISTANCES_KM_DEFAULT"               => '50',
    "GEO_COORDINATE_DEFAULT"                    => array('lat' => '52.52000659999999', 'lng' => '13.404954'), // Berlin
    "SEARCH_MEDICAL_SPECIALTY_DEFAULT"          => 1, // Zahnarzt/Dentist should be forced into the DB as ID=1
    "SEARCH_INSURANCE_DEFAULT"                  => 2, // Hardcoded at the moment. @todo: Less hardcoded default 

    "BLOGPOST_STATUS" => array(
        BLOGPOST_STATUS_DRAFT =>'<span class="orange">Draft</span>',
        BLOGPOST_STATUS_PUBLISHED =>'<span class="green">Published</span>',
    ),
    "BLOG_BLOGPOSTS_PER_PAGE" => 10,

    "WIDGET_STATUS" => array(
        WIDGET_STATUS_DRAFT  =>'<span class="orange">Draft</span>',
        WIDGET_STATUS_ACTIVE =>'<span class="green">Active</span>',
    ),

    "FACEBOOK_URL_FANPAGE"	=>	'www.facebook.com/Arzttermine',

    "SITEMAP" => array(
        "TYPES"	=> array(
            'core' => array(
                "priority" => 0.2,
                "frequency"	=> 'weekly'
            ),
            'doctors' => array(
                "priority" => 0.8,
                "frequency"	=> 'weekly'
            ),
            'locations' => array(
                "priority" => 0.8,
                "frequency"	=> 'weekly'
            ),
            'blog' => array(
                "priority" => 0.5,
                "frequency"	=> 'weekly'
            ),
            'pages' => array(
                "priority" => 0.5,
                "frequency"	=> 'weekly'
            ),
        )
    ),
    "REFERRAL_PRICE_TYPES" => array(
        "Oralbat2013" => array(
            "company" => "Oral B",
            "price" => "",
            "image" => "",
            "logo" => "oral_b_logo.png"
        ),
        "RXQTy9x0" => array(
            "company" => "Amazon",
            "price" => "15 €",
            "image" => "amazon_coupon_15.png",
            "logo" => "amazon_logo.png"
        ),
        "d8rGcgBa" => array(
            "company" => "Amazon",
            "price" => "20 €",
            "image" => "amazon_coupon_20.png",
            "logo" => "amazon_logo.png"
        ),
        "RVB3Pb4L" => array(
            "company" => "Amazon",
            "price" => "10 €",
            "image" => "amazon_coupon_10.png",
            "logo" => "amazon_logo.png"
        ),
        "7pU92UPc" => array(
            "company" => "Amazon",
            "price" => "25 €",
            "image" => "amazon_coupon_20.png",
            "logo" => "amazon_logo.png"
        ),
        "0o1YHZ3L" => array(
            "company" => "d8rGcgBa",
            "price" => "15 €",
            "image" => "itunes_coupon_15.png",
            "logo" => "itunes_logo.png"
        ),
        "rL0pudHz" => array(
            "company" => "iTunes",
            "price" => "25 €",
            "image" => "itunes_coupon_25.png",
            "logo" => "itunes_logo.png"
        ),
        "NjGXD7f9" => array(
            "company" => "Spotify",
            "price" => "1 Monat",
            "image" => "spotify_coupon_1.png",
            "logo" => "spotify_logo.png"
        ),
        "PZ5eaRSB" => array(
            "company" => "Spotify",
            "price" => "2 Monat",
            "image" => "spotify_coupon_2.png",
            "logo" => "spotify_logo.png"
        ),
        //patient default price type
        "ppt12f3a" => array(
            "company" => "Patient",
            "price" => "10 €",
            "image" => "patient_coupon_10.png",
            "logo" => "patient_logo.png"
        ),
        //workhub price type
        "workhub" => array(
            "company" => "Workhub",
            "price" => "",
            "image" => "workhub-logo.png",
            "logo" => "workhub-logo.png"
        ),
        "W3mF8uQ6" => array(
            "company" => "Bleaching Rabatt",
            "price" => "10%",
            "image" => "",
            "logo" => ""
        )
    ),

    "USER_LOGIN_TYPES" => array(
        LOGIN_DOCTOR_TYPE => "Ärzte",
        LOGIN_PATIENT_TYPE => "Patienten"
    ),

    "SALESFORCE_ALERT_MESSAGES_EMAILS"	=> array('salesforce@arzttermine.de'),

    "INTEGRATION_SAMEDI_PRACTICE_LOGIN"     => 'stored-in-local-config',
    "INTEGRATION_SAMEDI_PRACTICE_PASSWORD"  => 'stored-in-local-config',
    "INTEGRATION_SAMEDI_PRACTICE_ID"        => 'stored-in-local-config',

    "MYSQL_ADMIN_HOST"			=> 'db',
    "MYSQL_ADMIN_DATABASE"		=> 'www_arzttermine_de',
    "MYSQL_ADMIN_USER"			=> 'arzttermine',
    "MYSQL_ADMIN_PASSWORD"		=> 'arzttermine',
    "MYSQL_ADMIN_SOCKET"		=> '/tmp/mysql.sock',
    "MYSQL_CLASS_HALT_ON_ERROR"	=> 'no',
    "MYSQL_CLASS_DEBUG"			=> false,

);

$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $protocol = (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] !== 'http' || $_SERVER['HTTP_X_FORWARDED_PORT'] == 443) ? "https://" : "http://";
}

$server_host = $_SERVER['HTTP_HOST'];

$CONFIG["URL_API"] = $protocol . $server_host . '/api/v1';
// returned from the live version, maybe it should be used elsewhere, too
$CONFIG["URL_API_WIDGETS"]=$protocol .$server_host.'/api.php';

if ((isset($_ENV['SYSTEM_ENVIRONMENT']) && $_ENV['SYSTEM_ENVIRONMENT'] == 'development') ||	preg_match('/dev/i',$_SERVER['HTTP_HOST'])) {
    ini_set('display_errors',true);
    ini_set('error_reporting', E_ALL);
    $CONFIG["SYSTEM_ENVIRONMENT"] = 'development';
    $CONFIG["DOMAIN"] = "www.arzttermine.dev";
    $CONFIG["URL_HTTP_LIVE"] = $protocol.$CONFIG['DOMAIN'];
    $CONFIG["URL_HTTPS_LIVE"] = 'http://'.$CONFIG['DOMAIN'];	// don't use https on dev
    $CONFIG["MOBILE_DOMAIN"] = "m.arzttermine.dev";
    $CONFIG["MOBILE_URL_HTTP_LIVE"] = "http://".$CONFIG['MOBILE_DOMAIN'];
    $CONFIG["URL_SET_THEME_DEFAULT"] = $CONFIG["URL_HTTP_LIVE"]."/theme-default";
    $CONFIG["URL_SET_THEME_MOBILE"] = $CONFIG["URL_HTTP_LIVE"]."/theme-mobile";

    $CONFIG["SALESFORCE_ENABLE"] = false;
    $CONFIG["SMS_ENABLE"] = true;
    $CONFIG["I18N_SHOW_SWITCH"] = true;

    $CONFIG["GOOGLE_MAPS_ACTIVATE"] = true;
    $CONFIG["GOOGLE_ANALYTICS_ACTIVATE"] = false;

    $CONFIG["SOCIALMEDIA_ACTIVATE"] = true;

    $CONFIG["MAILING_SEND_CC"] = false;
    $CONFIG["MAILING_SEND_BCC"] = false;

    $CONFIG["MYSQL_CLASS_HALT_ON_ERROR"] = 'report';
    define ("DB_NAME","www_arzttermine_de");

} elseif ((isset($_ENV['SYSTEM_ENVIRONMENT']) && $_ENV['SYSTEM_ENVIRONMENT'] == 'i6_staging') ||	preg_match('/dens\.test/i',$_SERVER['HTTP_HOST'])) {
    $CONFIG["SYSTEM_ENVIRONMENT"] = 'i6_staging';
    $CONFIG["DOMAIN"] = "www.dens.test.arzttermine.de";
    $CONFIG["URL_HTTP_LIVE"] = $protocol.$CONFIG['DOMAIN'];
    $CONFIG["URL_HTTPS_LIVE"] = 'https://'.$CONFIG['DOMAIN'];
    $CONFIG["MOBILE_DOMAIN"] = "m.dens.test.arzttermine.de";
    $CONFIG["MOBILE_URL_HTTP_LIVE"] = $protocol.$CONFIG['MOBILE_DOMAIN'];
    $CONFIG["URL_SET_THEME_DEFAULT"] = $CONFIG["URL_HTTP_LIVE"]."/theme-default";
    $CONFIG["URL_SET_THEME_MOBILE"] = $CONFIG["URL_HTTP_LIVE"]."/theme-mobile";

    $CONFIG["SALESFORCE_ENABLE"] = false;
    $CONFIG["SMS_ENABLE"] = false;
    $CONFIG["I18N_SHOW_SWITCH"] = true;

    $CONFIG["GOOGLE_ANALYTICS_ACTIVATE"] = false;
    $CONFIG["GOOGLE_MAPS_ACTIVATE"] = true;

    $CONFIG["SOCIALMEDIA_ACTIVATE"] = false;

    $CONFIG["URL_API"]=$protocol .'www.dens.test.arzttermine.de/api/v1';

    $CONFIG["MAILING_SEND_CC"] = false;
    $CONFIG["MAILING_SEND_BCC"] = false;

    $CONFIG["MYSQL_CLASS_HALT_ON_ERROR"]	= 'no';
    define ("DB_NAME","dens_arzttermine_de");
    
} elseif ((isset($_ENV['SYSTEM_ENVIRONMENT']) && $_ENV['SYSTEM_ENVIRONMENT'] == 'qatest') || preg_match('/qatest/i',$_SERVER['HTTP_HOST'])) {
    $CONFIG["SYSTEM_ENVIRONMENT"] = 'qatest';
    $CONFIG["DOMAIN"] = "www.qatest.arzttermine.de";
    $CONFIG["URL_HTTP_LIVE"] = $protocol.$CONFIG['DOMAIN'];
    $CONFIG["URL_HTTPS_LIVE"] = 'https://'.$CONFIG['DOMAIN'];
    $CONFIG["MOBILE_DOMAIN"] = "m.qatest.arzttermine.de";
    $CONFIG["MOBILE_URL_HTTP_LIVE"] = $protocol.$CONFIG['MOBILE_DOMAIN'];
    $CONFIG["URL_SET_THEME_DEFAULT"] = $CONFIG["URL_HTTP_LIVE"]."/theme-default";
    $CONFIG["URL_SET_THEME_MOBILE"] = $CONFIG["URL_HTTP_LIVE"]."/theme-mobile";

    $CONFIG["SALESFORCE_ENABLE"] = false;
    $CONFIG["SMS_ENABLE"] = false;
    $CONFIG["I18N_SHOW_SWITCH"] = true;

    $CONFIG["GOOGLE_ANALYTICS_ACTIVATE"] = false;
    $CONFIG["GOOGLE_MAPS_ACTIVATE"] = true;

    $CONFIG["SOCIALMEDIA_ACTIVATE"] = false;

    $CONFIG["URL_API"]=$protocol .'www.qatest.arzttermine.de/api/v1';

    $CONFIG["MAILING_SEND_CC"] = false;
    $CONFIG["MAILING_SEND_BCC"] = false;
    
    $CONFIG["CONTACTS_EMAIL_ADDRESS"]					= 'qatest+info@arzttermine.de';
    $CONFIG["CONTACTS_EMAIL_NAME"]						= 'Arzttermine.de';
    $CONFIG["BOOKINGS_EMAIL_ADDRESS"]					= 'qatest+buchung@arzttermine.de';
    $CONFIG["BOOKINGS_EMAIL_NAME"]						= 'Arzttermine.de';
    $CONFIG["DOCTORS_EMAIL_ADDRESS"]					= 'qatest+kunden@arzttermine.de';
    $CONFIG["DOCTORS_EMAIL_NAME"]						= 'Arzttermine.de';
    $CONFIG["REVIEWS_EMAIL_ADDRESS"]					= 'qatest+support@arzttermine.de';
    $CONFIG["REVIEWS_EMAIL_NAME"]						= 'Arzttermine.de';
    $CONFIG["SALES_EMAIL_ADDRESS"]						= 'qatest+sales@arzttermine.de';

    $CONFIG["MYSQL_CLASS_HALT_ON_ERROR"]	= 'no';
    define ("DB_NAME","qatest_arzttermine_de");

} elseif ((isset($_ENV['SYSTEM_ENVIRONMENT']) && $_ENV['SYSTEM_ENVIRONMENT'] == 'staging') ||	preg_match('/test/i',$_SERVER['HTTP_HOST'])) {
    $CONFIG["SYSTEM_ENVIRONMENT"]='staging';
    $CONFIG["DOMAIN"] = "www.test.arzttermine.de";
    $CONFIG["URL_HTTP_LIVE"] = $protocol.$CONFIG['DOMAIN'];
    $CONFIG["URL_HTTPS_LIVE"] = 'https://'.$CONFIG['DOMAIN'];
    $CONFIG["MOBILE_DOMAIN"] = "m.test.arzttermine.de";
    $CONFIG["MOBILE_URL_HTTP_LIVE"] = $protocol.$CONFIG['MOBILE_DOMAIN'];
    $CONFIG["URL_SET_THEME_DEFAULT"] = $CONFIG["URL_HTTP_LIVE"]."/theme-default";
    $CONFIG["URL_SET_THEME_MOBILE"] = $CONFIG["URL_HTTP_LIVE"]."/theme-mobile";

    $CONFIG["SALESFORCE_ENABLE"] = false;
    $CONFIG["SMS_ENABLE"] = true;
    $CONFIG["I18N_SHOW_SWITCH"] = true;

    $CONFIG["GOOGLE_ANALYTICS_ACTIVATE"] = false;
    $CONFIG["GOOGLE_MAPS_ACTIVATE"] = true;

    $CONFIG["SOCIALMEDIA_ACTIVATE"] = true;

    $CONFIG["MAILING_SEND_CC"] = false;
    $CONFIG["MAILING_SEND_BCC"] = false;

    $CONFIG["MYSQL_CLASS_HALT_ON_ERROR"]	= 'no';
    define ("DB_NAME","test_arzttermine_de");

} elseif ((isset($_ENV['SYSTEM_ENVIRONMENT']) && $_ENV['SYSTEM_ENVIRONMENT'] == 'simplimed') ||	preg_match('/simplimed/i',$_SERVER['HTTP_HOST'])) {
    $CONFIG["SYSTEM_ENVIRONMENT"]='simplimed';
    $CONFIG["DOMAIN"]="simplimed.arzttermine.de";
    $CONFIG["URL_HTTP_LIVE"]=$protocol.$CONFIG['DOMAIN'];
    $CONFIG["URL_HTTPS_LIVE"] = 'https://'.$CONFIG['DOMAIN'];
    $CONFIG["MOBILE_DOMAIN"]="m.arzttermine.de";
    $CONFIG["MOBILE_URL_HTTP_LIVE"]=$protocol.$CONFIG['MOBILE_DOMAIN'];
    $CONFIG["URL_SET_THEME_DEFAULT"]=$CONFIG["URL_HTTP_LIVE"]."/theme-default";
    $CONFIG["URL_SET_THEME_MOBILE"]=$CONFIG["URL_HTTP_LIVE"]."/theme-mobile";

    $CONFIG["SALESFORCE_ENABLE"]=false;
    $CONFIG["SMS_ENABLE"]=true;
    $CONFIG["I18N_SHOW_SWITCH"]=true;

    $CONFIG["GOOGLE_ANALYTICS_ACTIVATE"]=true;
    $CONFIG["GOOGLE_MAPS_ACTIVATE"]=true;

    $CONFIG["SOCIALMEDIA_ACTIVATE"]=true;

    $CONFIG["MAILING_SEND_CC"] = true;
    $CONFIG["MAILING_SEND_BCC"] = true;

    $CONFIG["MYSQL_CLASS_HALT_ON_ERROR"]	= 'no';
    define ("DB_NAME","simplimed_arzttermine_de");
    $CONFIG["MEMCACHED_SERVER"] = "localhost";

} else {
    ini_set('display_errors',false);
    $CONFIG["SYSTEM_ENVIRONMENT"] = 'production';
    $CONFIG["I18N_SHOW_SWITCH"] = true;
    $CONFIG["SYSTEM_SESSION_SAVE_PATH"] = "tcp://localhost:11211?persistent=1&amp;weight=1&amp;timeout=1&amp;retry_interval=15";

    $CONFIG["SALESFORCE_ENABLE"] = false;
    $CONFIG["SMS_ENABLE"] = true;
    $CONFIG["GOOGLE_TAGMANAGER_ID"] = 'GTM-NCPB4BW';
    $CONFIG["GOOGLE_ANALYTICS_ID"] = 'UA-27059894-1';

    $CONFIG["GOOGLE_ANALYTICS_ACTIVATE"] = true;
    $CONFIG["GOOGLE_MAPS_ACTIVATE"] = true;

    $CONFIG["SOCIALMEDIA_ACTIVATE"] = true;
    $CONFIG["FACEBOOK_APP_ID"] = '358430137537019';	// www.arzttermine.de

    $CONFIG["MAILING_SEND_CC"] = true;
    $CONFIG["MAILING_SEND_BCC"] = true;

    $CONFIG["MYSQL_CLASS_HALT_ON_ERROR"] = 'no';
    define ("DB_NAME","www_arzttermine_de");
}

// depreacted? can't find it in the code!?
$CONFIG["GOOGLE_ANALYTICS_ID_WIDGET"] = 'UA-27059894-1';

if (file_exists($SHARED . '/config/config.php')) {
    include_once $SHARED . '/config/config.php';
}

$CONFIG['MEDIA_CATEGORIES'] = array(
    1=>array(
        "name"=>"Images (/)",
        "url_path"=>$GLOBALS['CONFIG']['MEDIA_URL'],
        "file_path"=>$GLOBALS['CONFIG']['MEDIA_PATH'],
    ),
    2=>array(
        "name"=>"Videos (/video)",
        "url_path"=>$GLOBALS['CONFIG']['MEDIA_URL'].'video/',
        "file_path"=>$GLOBALS['CONFIG']['MEDIA_PATH'].'video/',
    )
);

$CONFIG['GROUP_ACCESS'] = array(
    "blacklist" => array(
        "admin",
        "admin_view",
        "admin_new",
        "admin_edit",
        "admin_delete",
        "admin_search",
    ),
    "blog" => array(
        "admin",
        "admin_view",
        "admin_new",
        "admin_edit",
        "admin_delete",
        "admin_search",
    ),
    "booking" => array(
        "admin",
        "admin_view",
        "admin_new",
        "admin_edit",
        "admin_delete",
        "admin_search",
        "admin_integration_book",
        "admin_integration_cancel",
        "admin_csv",
    ),
    "bookingoffer" => array(
        "admin",
        "admin_view",
        "admin_new",
        "admin_edit",
        "admin_delete",
        "admin_search",
        "admin_send_email",
    ),
    "cms" => array(
        "admin",
        "admin_dir_new",
        "admin_dir_edit",
        "admin_dir_delete",
        "admin_content_new",
        "admin_content_edit",
        "admin_content_changeversion",
        "admin_content_view",
        "admin_content_delete",
        "admin_content_changedir",
        "admin_version_new",
        "admin_version_view",
        "admin_version_activate",
        "admin_version_edit",
        "admin_version_delete",
        "admin_version_diff",
        "admin_search",
        "admin_allow_scripting",
        "admin_map",
    ),
    "configuration" => array(
        "admin",
        "admin_view",
        "admin_new",
        "admin_edit",
        "admin_delete",
        "admin_search"
    ),
    "contact" => array(
        "admin",
        "admin_view",
        "admin_new",
        "admin_edit",
        "admin_delete",
        "admin_search",
        "admin_mail",
    ),
    "coupon" => array(
        "admin",
        "admin_view",
        "admin_new",
        "admin_edit",
        "admin_delete",
        "admin_search",
    ),
    "district" => array(
        "admin",
        "admin_view",
        "admin_new",
        "admin_edit",
        "admin_delete",
        "admin_search",
    ),
    "group" => array(
        "admin",
        "admin_view",
        "admin_new",
        "admin_edit",
        "admin_delete",
        "admin_search",
    ),
    "import" => array(
        "admin_doctors"
    ),
    "insurance" => array(
        "admin",
        "admin_view",
        "admin_new",
        "admin_edit",
        "admin_delete",
        "admin_search",
    ),
    "integration" => array(
        "admin",
        "admin_view",
        "admin_new",
        "admin_edit",
        "admin_delete",
        "admin_search",
    ),
    "location" => array(
        "admin",
        "admin_view",
        "admin_new",
        "admin_edit",
        "admin_delete",
        "admin_search",
        "admin_media",
        "admin_csv",
    ),
    "media" => array(
        "admin",
        "admin_view",
        "admin_new",
        "admin_edit",
        "admin_delete",
        "admin_search",
    ),
    "mailing" => array(
        "admin",
        "admin_view",
        "admin_new",
        "admin_edit",
        "admin_delete",
        "admin_search"
    ),
    "medicalspecialty" => array(
        "admin",
        "admin_view",
        "admin_new",
        "admin_edit",
        "admin_delete",
        "admin_search",
        "admin_media",
    ),
    "treatmenttype" => array(
        "admin",
        "admin_view",
        "admin_new",
        "admin_edit",
        "admin_delete",
        "admin_search",
        "admin_media",
    ),
    "review" => array(
        "admin",
        "admin_view",
        "admin_new",
        "admin_edit",
        "admin_delete",
        "admin_search",
        "admin_csv",
    ),
    "searchresulttext" => array(
        "admin",
        "admin_view",
        "admin_new",
        "admin_edit",
        "admin_delete",
        "admin_search",
        "admin_media",
    ),
    "system" => array(
        "internal_group",
        "globalsettings",
        "systeminfo",
        "serverinfo",
        "user_prefs",
        "phpinfo",
    ),
    "user" => array(
        "admin",
        "admin_view",
        "admin_new",
        "admin_edit",
        "admin_delete",
        "admin_search",
        "admin_media",
        "admin_employees",
        "admin_activate"
    ),
    "widget" => array(
        "admin",
        "admin_view",
        "admin_new",
        "admin_edit",
        "admin_delete",
        "admin_search"
    ),
);

define ("DB_TABLENAME_CMS_DIRS",					DB_NAME.".cms_dirs");
define ("DB_TABLENAME_CMS_DIR_METAS",				DB_NAME.".cms_dir_metas");
define ("DB_TABLENAME_CMS_DIR_OPEN_NODES",			DB_NAME.".cms_dir_open_nodes");
define ("DB_TABLENAME_CMS_CONTENTS",				DB_NAME.".cms_contents");
define ("DB_TABLENAME_CMS_CONTENT_METAS",			DB_NAME.".cms_content_metas");
define ("DB_TABLENAME_CMS_VERSIONS",				DB_NAME.".cms_versions");

define ("DB_TABLENAME_APPOINTMENTS",				DB_NAME.".appointments");
define ("DB_TABLENAME_GROUPS",						DB_NAME.".groups");
define ("DB_TABLENAME_GROUP_ACCESSES",				DB_NAME.".group_accesses");
define ("DB_TABLENAME_USERS",						DB_NAME.".users");
define ("DB_TABLENAME_TEMP_USERS_DATA",				DB_NAME.".temp_users_data");
define ("DB_TABLENAME_PATIENTS",					DB_NAME.".patients");
define ("DB_TABLENAME_REVIEWS",            			DB_NAME.".reviews");
define ("DB_TABLENAME_AFFILIATES",					DB_NAME.".affiliates");
define ("DB_TABLENAME_COUNTRIES",					DB_NAME.".countries");
define ("DB_TABLENAME_CONTACTS",					DB_NAME.".contacts");
define ("DB_TABLENAME_MEDIAS",						DB_NAME.".medias");
define ("DB_TABLENAME_ASSETS",						DB_NAME.".assets");
define ("DB_TABLENAME_ADMIN_MESSAGES",				DB_NAME.".admin_messages");
define ("DB_TABLENAME_MEDICAL_SPECIALTIES",			DB_NAME.".medical_specialties");
define ("DB_TABLENAME_INSURANCES",					DB_NAME.".insurances");
define ("DB_TABLENAME_TREATMENTTYPES",				DB_NAME.".treatmenttypes");
define ("DB_TABLENAME_INSURANCE_PROVIDERS",			DB_NAME.".insurance_providers");
define ("DB_TABLENAME_LOCATIONS",					DB_NAME.".locations");
define ("DB_TABLENAME_LOCATION_USER_CONNECTIONS",	DB_NAME.".location_user_connections");
define ("DB_TABLENAME_DISTRICTS",					DB_NAME.".districts");
define ("DB_TABLENAME_BOOKINGS",					DB_NAME.".bookings");
define ("DB_TABLENAME_BOOKING_OFFERS",				DB_NAME.".booking_offers");
define ("DB_TABLENAME_ADDRESS_GEO_DATAS",			DB_NAME.".address_geo_datas");
define ("DB_TABLENAME_MAILINGS",					DB_NAME.".mailings");
define ("DB_TABLENAME_WIDGETS",						DB_NAME.".widgets");
define ("DB_TABLENAME_BLOGPOSTS",					DB_NAME.".blogposts");
define ("DB_TABLENAME_SEARCHRESULTTEXTS",			DB_NAME.".search_result_texts");
define ("DB_TABLENAME_LOGIN_HISTORY",			    DB_NAME.".login_history");
define ("DB_TABLENAME_INTEGRATION_APPOINTMENTS",    DB_NAME.".integration_appointments");
define ("DB_TABLENAME_COUPONS",                     DB_NAME.".coupons");
define ("DB_TABLENAME_USED_COUPONS",                DB_NAME.".bookings_coupons");
define ("DB_TABLENAME_CONFIGURATION",               DB_NAME.".configuration");
define ("DB_TABLENAME_SMS_LOG",                     DB_NAME.".sms_log");

// Integration tables
// @todo Move to config
define ("DB_TABLENAME_INTEGRATION_CACHES",          DB_NAME.".integration_caches");
define ("DB_TABLENAME_INTEGRATION_BOOKINGS",        DB_NAME.".integration_bookings");
define ("DB_TABLENAME_INTEGRATION_2_BOOKINGS",      DB_NAME.".integration_2_bookings");
define ("DB_TABLENAME_INTEGRATION_2_DATAS",         DB_NAME.".integration_2_datas");
define ("DB_TABLENAME_INTEGRATION_5_BOOKINGS",      DB_NAME.".integration_5_bookings");
define ('DB_TABLENAME_INTEGRATION_5_DATAS',         DB_NAME.".integration_5_datas");
define ('DB_TABLENAME_INTEGRATION_5_TREATMENTTYPES',DB_NAME.".integration_5_treatmenttypes");
define ('DB_TABLENAME_INTEGRATION_5_EVENTTYPES',    DB_NAME.".integration_5_eventtypes");
define ('DB_TABLENAME_INTEGRATION_5_NETWORKS',      DB_NAME.".integration_5_networks");
define ('DB_TABLENAME_INTEGRATION_5_APPOINTMENTS',  DB_NAME.".integration_5_appointments");
define ("DB_TABLENAME_INTEGRATION_10_DATAS",        DB_NAME.".integration_10_datas");
define ("DB_TABLENAME_INTEGRATION_10_WEBDAVACCOUNTS", DB_NAME.".integration_10_webdavaccounts");
define ("USERS_AA",                                 DB_NAME.".users_aa");

/**
 * needed for the search results:
 * [10.01.14 17:45:04] Vahagn Sookiasyan: for the first time we run the script to fill the values for int1 doctors
 * [10.01.14 17:45:14] Vahagn Sookiasyan: then manually we set the tt for others (int2)
 * [10.01.14 17:45:27] Vahagn Sookiasyan: and for samedi it added automatically when you import the doctor from Admin panel
 * [10.01.14 17:45:47] Vahagn Sookiasyan: this table is used in search to filter the doctors by TT
 * ...
 * [10.01.14 17:48:40] Vahagn Sookiasyan: in search result and in doctor calendar
 * [10.01.14 17:48:54] Vahagn Sookiasyan: when you Create the Appointment from Admin panel
 * [10.01.14 17:49:10] Vahagn Sookiasyan: in doctor calendar you can set Duration for each TT of doctor
 */
define ("DB_TABLENAME_DOCTOR_TREATMENTTYPES",		DB_NAME.".doctors_treatment_types");

require_once $CONFIG['SYSTEM_FILE_INTEGRATIONS_CONFIG'];
