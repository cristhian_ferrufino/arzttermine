role :web,
  "web01.arzttermine.de"

role :app,
  "web01.arzttermine.de"

# The :primary => true option on the :db role tells capistrano this server is the server from which to run migrations.
# multiple role-calls with the same role name don't overwrite, they add servers.
role :db, "web01.arzttermine.de", :primary => true

set :deploy_to, "/www/dens.test.arzttermine.de"

# =============================================================================
# HOOKS
# =============================================================================

#after "deploy:update_code", "deploy:web:disable"
#after "deploy:symlink", "deploy:web:enable"

# remove all but the last 5 releases
after "deploy", "deploy:cleanup"
