role :web,
  "web04.arzttermine.de"

role :app,
  "web04.arzttermine.de"

# The :primary => true option on the :db role tells capistrano this server is the server from which to run migrations.
# multiple role-calls with the same role name don't overwrite, they add servers.
role :db, "web04.arzttermine.de", :primary => true

set :deploy_to, "/www/www.arzttermine.de"

# =============================================================================
# HOOKS
# =============================================================================

#after "deploy:update_code", "deploy:web:disable"
#after "deploy:symlink", "deploy:web:enable"
