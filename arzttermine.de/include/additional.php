<?php

if (!defined('SYSTEM_PATH')) {
    define('SYSTEM_PATH', dirname(dirname(__FILE__)));
}

// Libraries
require_once SYSTEM_PATH . "/include/lib/wp-includes/formatting.php";
require_once SYSTEM_PATH . "/include/lib/wp-includes/kses.php";
require_once SYSTEM_PATH . "/include/lib/wp-includes/shortcodes.php";
require_once SYSTEM_PATH . "/include/lib/wp-includes/wp-diff.php";
require_once SYSTEM_PATH . "/include/lib/functions.admin.php";
require_once SYSTEM_PATH . "/include/lib/functions.share.php";
require_once SYSTEM_PATH . "/include/lib/functions.unicode.php";
require_once SYSTEM_PATH . "/include/lib/functions.formatting.php";
require_once SYSTEM_PATH . "/include/lib/functions.shortcodes.php";

// Badly formatted classes/files
require_once SYSTEM_PATH . '/src/arzttermine/MobileDetect/MobileDetect.php';
require_once SYSTEM_PATH . "/src/arzttermine/iCalCreator/iCalCreator.php";

// Salesforce
// @todo: Use the phpforce/salesforce bundle from Composer
require_once SYSTEM_PATH . "/src/arzttermine/Salesforce/Interface.php";
