<?php
$t = array();

$t['OK']='OK';
$t['Ja']='Yes';
$t['Nein']='No';
$t['Abbrechen']='Cancel';
$t['Sichern']='Save';
$t['Zurück']='Back';
$t['Login']='Login';
$t['Logout']='Logout';
$t['Optional'] = 'Optional';
$t['freiwillig'] = 'optional';

// contact
$t['contact_header']='Schreiben Sie uns!';
$t['contact_header_p']='Haben Sie Fragen oder wollen Sie uns unterstüzen? Schreiben Sie uns eine Nachricht!';
$t['contact_header_thank_you']='Vielen Dank für Ihr Feedback!';
$t['contact_name']='Name';
$t['contact_email']='E-Mail';
$t['contact_subject']='Betreff';
$t['contact_message']='Ihre Nachricht';
$t['could_not_create_new_contact']='Ihre Nachricht konnte nicht gespeichert werden! Bitte versuchen Sie es später nocheinmal!';
$t['contact_created_successfully']='Vielen Dank für Ihre Nachricht! Wir kümmern uns darum um melden uns bei Ihnen so schnell wie möglich.';
$t['language']='Sprache';
$t['message']='Nachricht';

$t['Bitte prüfen Sie Ihre Eingaben.']='Please check the booking form for errors.';

// login
$t['wrong_email_or_password']='Username und/oder Passwort stimmen nicht überein!';
$t['Sie haben sich erfolgreich ausgeloggt!']='You have been logged out!';

// blog
$t['magazin']='magazine';
$t['blog']='Blog';
$t['url_blog']='/news';
$t['blog_title']='News';
$t['author']='Autor';
$t['leave_a_reply']='Hinterlassen Sie einen Kommentar';
$t['blog_comment_name']='Name (benötigt)';
$t['blog_comment_email']='E-Mail (wird nicht angezeigt) (benötigt)';
$t['blog_comment_submit']='Kommentar abschicken';
$t['could_not_save_comment']='Der Kommentar konnte nicht gespeichert werden.';
$t['comment_created_successfully']='Ihr Kommentar wurde gespeichert und liegt der Redaktion zur Prüfung vor.';
$t['show_older_blogposts']='Ältere News';
$t['show_newer_blogposts']='Neuere News';
$t['unvalid_title']='Der Titel ist nicht gültig!';
$t['unvalid_permalink']='Der Permalink ist nicht gültig!';
$t['comment']='Kommentar';
$t['comments']='Kommentare';
$t['unvalidated_comment']='Unvalidierter Kommentar';
$t['unvalidated_comments']='Unvalidierte Kommentare';
$t['should_this_comment_be_deleted_really']='Soll dieser Kommentar wirklich gelöscht werden?';
$t['comment_validated_successfully']='Der Kommentar wurde aktivert und wird nun angezeigt.';
$t['could_not_validate_comment']='Der Kommentar konnte nicht aktivert werden.';
$t['comment_unvalidated_successfully']='Der Kommentar wurde wieder deaktiviert. Er wird nicht mehr angezeigt.';
$t['could_not_unvalidate_comment']='Der Kommentar konnte nicht deaktiviert werden.';
$t['comment_deleted_successfully']='Der Kommentar wurde gelöscht.';
$t['could_not_delete_comment']='Der Kommentar konnte nicht gelöscht werden.';
$t['blog_guestblogger']='Ihr Thema? Hier vorschlagen und Gastblogger werden!';
$t['url_blog_guestblogger']='/de/kontakt&subject=guestblogger';

/* *************************************************** */

//html5 homepage

$t['Arzttermin online buchen'] = 'Doctor Appointment Online Booking';
$t['Finden Sie Zahnarzt und Arzttermine auf Deutschlands größter Online-Terminplattform.'] = 'Find a dentist and doctor appointments at Germany\'s largest online scheduling platform.';
$t['Einfach und bequem einen freien Wunschtermin aus verschiedenen Fachrichtungen in Berlin, München, Hamburg und anderen deutschen Städten buchen.'] = 'Easy and convenient to book a free appointment request from different disciplines in Berlin, Munich, Hamburg and other German cities.';

$t['Home Arzttermine.de Arzt &amp; Zahnarzt Termine Berlin Frankfurt Hamburg Köln München Leipzig Düsseldorf'] = 'Home Arzttermine.de Doctor &amp; Dentist Appointments in Berlin Frankfurt Hamburg Cologne Munich Leipzig and Dusseldorf';
$t['Home Arzttermine.de Arzt &amp; Zahnarzt Termine'] = 'Home Arzttermine.de Doctor & Dentist Appointments';
$t['Ich bin'] = 'I have';
$t['Ich suche einen Arzt in'] = 'I\'m looking for a doctor in';
$t['...bitte wählen...'] = 'Please choose';
$t['...bitte Ort wählen...'] = 'Please choose a city';

//html5 homepage searchbox
$t['...bitte Fachrichtung wählen...'] = 'Please choose a specialty';
$t['Bitte Stadt wählen...'] = 'Please select a city';
$t['... Arzt | Zahnarzt | Frauenarzt ...'] = 'Doctor | Dentist | Gynecologist...';
$t['Bitte wählen Sie eine Stadt aus.'] = 'Please select a city';
$t['Bitte wählen Sie eine Fachrichtung aus.'] = 'Please select a medical specialty';

$t['Gesetzlich versichert'] = 'Public Insurance';
$t['gesetzlich versichert'] = 'Public Insurance';
$t['Privat versichert'] = 'Private Insurance';
$t['privat versichert'] = 'Private Insurance';
$t['Privat'] = 'Private';
$t['Gesetzlich'] = 'Public';
$t['Termin finden'] = 'Find Appointments';
$t['Erneut suchen'] = 'Search Again';
$t['Ort und/oder Postleitzahl'] = 'City and/or ZipCode';

//html5 homepage feedback area
$t['Patientenmeinung'] = 'Patient Feedback';
$t['Wir melden uns bei Ihnen'] = 'We will contact you';
$t['<b>Wünschen Sie einen Rückruf?</b> Dann füllen Sie einfach die folgenden Felder aus und wir werden uns umgehend bei Ihnen melden.'] = '<b>Would you like a call back?</b> Then please fill in the fields below and we will contact you immediately.';
$t['Ärztemeinung'] = 'Doctors Feedback';

//html5 homepage content area
$t['Arzttermine schnell und bequem online buchen.'] = 'Book medical appointments online quickly and conveniently';
$t['Sie brauchen dringend einen Termin für einen Facharzt in Ihrer Nähe und Sie haben keine Zeit, um das halbe Branchenbuch durchzutelefonieren, bis Sie einen freien Termin bei einem passenden Arzt finden? Arzttermine.de findet für Sie den gesuchten Facharzt, der für Sie auch kurzfristig einen Termin vergeben kann. Sie sparen somit wertvolle Zeit, denn es kann manchmal Tage oder gar Wochen dauern, um bei einem Facharzt einen freien Termin zu finden. Dank der riesigen Ärztedatenbank von Arztermine.de müssen Sie in keinen Telefonbüchern oder dem Internet mehr suchen!<br/>Insbesondere bei akuten Schmerzen ist jede Minute kostbar und ungern durch die Suche nach einem Termin am Telefon verschwendet. Der kostenfreie Service von Arzttermine.de schafft dort ersehnte Abhilfe und findet für Sie in Sekundenschnelle und bequem den passenden Facharzt in Ihrer Nähe. Mit nur einem Mausklick bucht Arzttermine.de für Sie Ihren Wunschtermin.'] = 'They desperately need a date for a specialist in your area and you have no time to durchzutelefonieren half the industry book until you find a free appointment with a suitable doctor? Arzttermine.de is the place for you this specialist who can forgive you for too short a date. So you save valuable time, as it can sometimes take days or even weeks to find a specialist in a free date. Thanks to the huge database of physicians Arztermine.de you need to look in a phone book or the Internet more!
Especially in acute pain is precious and every minute wasted by the reluctant search for a date on the phone. The free service provides Arzttermine.de awaited remedy and there is for you in seconds and easily the appropriate specialist in your area. With a single click Arzttermine.de booked for your preferred date.';
$t['Arzttermine.de erreichen Sie von überall bequem:'] = 'Arzttermine.de easily accessible from anywhere:';
$t['+ über unsere kostenfreie Hotline'] = '+ Via our toll-free hotline';
$t['+ im Internet'] = '+ On the Internet';
$t['+ Finden Sie über Arzttermine.de schnell den passenden Facharzt ohne langes, lästiges Telefonieren, um einen freien Termin zu finden.'] = '+ Quickly find over Arzttermine.de the appropriate medical specialist without long, annoying phone calls to find a free date.';
$t['+ Arzttermine findet für Sie einen Facharzt in Ihrer Nähe, mit Termingarantie!'] = '+ Medical appointments is for a specialist doctor in your area, with Time Guarantee!';
$t['Arzttermine.de: <b>Schnell. Einfach. Online.</b>'] = 'Arzttermine.de: <b>Fast. East. Online.</b>';
$t['Jetzt einen Arzttermin online finden:'] = 'Find an doctor appointment online now!';
$t['» Wählen Sie als erstes Ihre Stadt und die gesuchte Fachrichtung aus.'] = '» First, select the city and the discipline you need.';
$t['» Geben Sie bitte Ihre Versicherungsart an und starten die Terminsuche.'] = '» Please enter your insurance type and start your search.';
$t['» Sie erhalten auf der folgenden Seite eine Auflistung der zur Verfügung stehenden Termine.'] = '» Please visit the following page with the list of possible dates.';
$t['» Um die Auswahl der Termine zu präzisieren, können Sie in der Suchmaske Ihren Stadtteil oder Ihren genauen Standort eingeben.'] = '» To refine the selection of dates and doctors, you can type in your area or your exact location in the search field.';
$t['(Straße Hausnr./Stadtteil , Stadt)'] = '(Street Number. / District, city)';
$t['» Weitere Informationen zu Ihrem Arzt oder der Praxis bieten wir Ihnen auf den Profilseiten.'] = '» Learn more about your doctor or practice we offer you on the profile pages.';
$t['» Durch die Auswahl einer Terminzeit gelangen Sie automatisch auf die Buchungsseite und erhalten nach erfolgreicher Buchung des Termins eine Bestätigung.'] = '» Select a day and time, then request the appointment on the booking page, after a successful request submission you will receive an appointment confirmation.';





//Search Results
$t['Aktuell sind in dieser Region keine Ärzte bei uns gemeldet.'] = 'We don\'t have doctors in this area right now.';
$t['Wir arbeiten daran unser Angebot in Kürze deutschlandweit anzubieten.'] = 'We are working hard to increase our number of doctors.';
$t['Vorherige Woche'] = 'Previous week';
$t['Nächste Woche'] = 'Next week';
$t['Sie sind nur noch einen Klick von Ihrem Wunschtermin entfernt.'] = 'One more click to get this appointment';
$t['Nur noch einen Klick bis zum Wunschtermin'] = 'Just one click to get this appointment';
$t['Sprache'] = 'Language';
$t['Als Arzt registrieren'] = 'Register as a doctor';
$t['Ich bin Patient'] = 'I am a patient';
$t['Ihre Angaben'] = 'Your details';
$t['Patient'] = 'Patient';
$t['Anrede'] = 'Salutation';
$t['Frau'] = 'Female';
$t['Herr'] = 'Male';
$t['Vorname'] = 'First name';
$t['Nachname'] = 'Last name';
$t['E-Mail'] = 'Email';
$t['Telefon'] = 'Phone';
$t['Mobil'] = 'Mobile';
$t['Versicherungsart'] = 'Insurance type';
$t['Waren Sie bereits Patient/in bei'] = 'Have you already been at';
$t['Waren Sie bereits Patient bei diesem Arzt?'] = 'Have you already been to this doctors office?';
$t['Empfehlungscode'] = 'Referral Code';
$t['Behandlungsgrund'] = 'Reason of treatment';
$t['Newsletter abonnieren'] = 'Subscribe to newsletter';
$t['Termin buchen'] = 'Book Appointment';
$t['Online buchen'] = 'Book Online';
$t['online buchen'] = 'Book Online';
$t['Fachgebiete'] = 'Specialty';
$t['Bewertung'] = 'Ratings';
$t['Praxis'] = 'Practice';
$t['Fachkompetenz'] = 'Expertise';
$t['Termin eingehalten'] = 'Wait Time';
$t['Vertrauensverhältnis'] = 'Trust';
$t['Philosophie'] = 'Philosophy';
$t['Behandlungsmöglichkeiten'] = 'Treatment Types';
$t['Spezialisierung'] = 'Specialties';
$t['Sprachen'] = 'Languages';
$t['Ausbildung'] = 'Education and Training';
$t['Auszeichnungen'] = 'Awards';
$t['Mitgliedschaften'] = 'Memberships';
$t['Beschreibung'] = 'Description';
$t['Zum Termin buchen bitte Versicherungsart auswählen'] = 'To see appointments please select your insurance type';
$t['Ärzte in dieser Praxis'] = 'Doctors at this Practice';

$t['Haben Sie noch Fragen? Über das'] = 'Do you have questions?';
$t['Kontaktformular'] = 'Contact us';
$t['helfen wir Ihnen gern.'] = ', we\'re here to help you.';
$t['Durch das Absenden des Buchungsformulars wir der Termin im System hinterlegt. Sie erhalten von uns eine E-Mail zur Bestätigung der Terminvergabe. Dieser wird  durch die Buchungsbestätigung verbindlich.'] = 'Once you submit the form, we will confirm your appointment. You will receive an e-mail to confirm the appointment.';
$t['Zurück zur'] = 'Return to';
$t['Startseite'] = 'homepage';


$t['Ich stimme der Verwendung meiner Daten zur Vermittlung meines Termins unter den in der <a target="_blank" href= %s >Datenschutzerklärung</a> genannten Bedingungen zu. Arzttermine.de darf insbesondere meine Daten an den Arzt weiterleiten und dieser rückübermitteln, ob ich den Termin wahrgenommen habe.'] = 'I agree to the use of my personal data as specified in the <a target="_blank" href= %s >Privacy Policy</a>. Arzttermine.de may forward my data to the doctor and it may be communicated back if I complete the appointment.';
$t['Bitte bestätigen Sie die Einwilligung.'] = 'Bitte bestätigen Sie die Einwilligung.';
$t['Nur noch ein Klick bis zu Ihrem Termin.'] = 'Only one more click to get your appointment.';
$t['Wir nutzen Ihre Daten ausschließlich wie in der <a href="/datenschutz">Datenschutzerklärung</a> festgelegt.'] = 'We use your data only as described in the <a href="/datenschutz">Privacy Statement</a>.';
$t['Möchten Sie telefonisch buchen?'] = 'Do you want to book by phone?';
$t['Kostenfrei anrufen:'] = 'Call free';
$t['Ihre Terminwahl'] = 'Your appointment';
$t['Ihre Buchung war erfolgreich'] = 'Your booking was successful';
$t['Wir werden in Kürze Ihren Wunschtermin per E-Mail bestätigen.<br />Die Terminbuchung ist erst durch diese Bestätigung vollendet.'] = 'We will confirm your appointment shortly. <br />The booking is confirmed once you\'ve received our notification.';

$t['Das hat geklappt!'] = 'That worked!';
$t['Empfehlen Sie uns weiter'] = 'Tell a friend';
$t['Ihre Buchungsdaten werden natürlich nicht weitergegeben oder angezeigt'] = 'Your booking data will be not be shared or displayed';

$t['Arzttermine.de | Schnell, einfach und online zu Ihrem Wunschtermin.'] = 'Arzttermine.de | Quick, easy and online to your preferred appointment';
$t['Telefonisch buchen unter:'] = 'Book by phone:';
$t['Kostenfrei telefonisch buchen:'] = 'Book by phone:';
$t['24 Stunden für Sie erreichbar'] = '24 hours a day for you';
$t['Wir helfen Ihnen gerne:'] = 'We help you:';

$t['Wir haben Ihren Termin in den Praxiscomputer Ihres Arztes eingetragen.'] = 'We have booked your appointment into the doctors computer.';
$t['Sie erhalten in Kürze eine Bestätigung per E-Mail'] = 'You will receive a confirmation about your appointment soon.';
$t['Ihre Buchung war erfolgreich.'] = 'Your booking was successful.';
$t['Noch Fragen?'] = 'Any more questions?';
$t['Haben Sie noch Fragen?'] = 'Do you have any questions?';
$t['Wir sind 24 Stunden für Sie erreichbar:'] = 'We are available 24 hours a day:';
$t['Arzttermine.de - Patienten-Support'] = 'Arzttermine.de - Patient Support';
$t['Was möchten Sie als nächstes tun?'] = 'What would you like to do next?';
$t['Teilen Sie Ihren Freunden mit, wie schnell und einfach es geht.'] = 'Share with your friends, how fast and easy it is.';
$t['Folgen Sie uns auf Twitter und erhalten aktuelle Gesundheit-Tipps.'] = 'Follow us on Twitter and receive the latest Health Tips';
$t['folge uns auf google+'] = 'Follow us on Google+';
$t['abonnieren Sie Arzttermine.de auf Facebook'] = 'subscribe to Arzttermine.de on Facebook';
$t['folgen Sie Arzttermine.de auf Twitter'] = 'subscribe to Arzttermine.de on Twitter';
$t['abonnieren Sie Arzttermine.de auf Xing'] = 'subscribe to Arzttermine.de on Xing';

$t['Startseite'] = 'Home';
$t['Magazin'] = 'Magazine';

$t['Keine freien Termine verfügbar'] = 'No free appointments available';
$t['Nächster freier Termin:'] = 'Next free appointment:';
$t['Diese Buchung wurde bereits erstellt.'] = 'This booking has already been made.';
$t['Ups, etwas ist schiefgegangen.'] = 'Oops, something went wrong.';

$t['Bitte wählen Sie Ihre Anrede aus.'] = 'Please select your gender.';
$t['Geben Sie bitte Ihre E-Mailadresse ein.'] = 'Please enter your Email address.';
$t['Bitte geben Sie Ihren Vornamen ein.'] = 'Please enter your first name.';
$t['Bitte geben Sie Ihren Nachnamen ein.'] = 'Please enter your last name.';
$t['Bitte geben Sie eine gültige Telefonnummer ein.'] = 'Please enter a valid phone number.';
$t['Bitte geben Sie an, wie Sie versichert sind.'] = 'Please select your insurance type.';
$t['Bitte geben Sie an, ob Sie schon einmal bei diesem Arzt in Behandlung waren.'] = 'Please confirm if you already have been to this doctor\'s office.';
$t['Profilseite von'] = 'Profile page of';


# search
$t['in'] = 'in';
$t['haben Termine für Sie.'] = 'have appointments for you.';

# search mobile
$t['Klicken sie hier um die Suche zu ändern.'] = 'Tab to change your search';

# Footer
$t['News'] = 'News';
$t['Presse'] = 'Press';
$t['Kontakt'] = 'Contact';
$t['Aktuelle Stellenausschreibungen'] = 'Jobs';
$t['Impressum'] = 'Imprint';
$t['Arzttermine Imprint'] = 'Arzttermine Imprint';
$t['Datenschutz'] = 'Privacy Policy';
$t['Partner'] = 'Partner';
$t['AGB'] = 'Terms and Conditions';
$t['agb'] = 'Terms and Conditions';
$t['Hotline: 0800 / 2222133 (24h / kostenfrei) - Kontaktanfrage:'] = 'Hotline: 0800 / 2222133 (24h / free):';
$t['Blog'] = 'Blog';
$t['Magazin'] = 'Magazine';
$t['Pressebereich'] = 'Press';
$t['Praxenverzeichnis'] = 'Doctor Directory';
$t['Berliner Zahnärzte<br/>(Stadtteile)'] = 'Berlin Dentists<br/>(neighborhoods)';
$t['Zahnärzte in weiteren<br/>Metropolen'] = 'Dentists in<br/>other cities';
$t['Kinderzahnärzte in <br/>Deutschland'] = 'Pediatric Dentist<br/> in Germany';
$t['Weitere <br/>Fachrichtungen'] = 'Other medical <br/>specialties ';
$t['Weitere Informationen']	= 'Additional information';

/* SPECIALITY */
$t['Frauenarzt'] 		= 'Gynecologist';
$t['Allgemeinarzt'] 	= 'Allgemeinarzt';
$t['MRT-Diagnostik'] 	= 'MRI diagnostics';
$t['Kieferorthopäde'] 	= 'Orthodontist';
$t['Kinderzahnarzt'] 	= 'Pediatric Dentist';
$t['Hautarzt'] 			= 'Dermatologist';
$t['HNO-Arzt'] 			= 'ENT';
$t['Urologe'] 			= 'Urologist';
$t['Orthopäde'] 		= 'Orthopedist';
$t['Homöopathie'] 		= 'Homeopathy';

/* Job-Title singular */
$t['Zahnarzt'] = 'Dentist';
$t['Kinderzahnarzt'] = 'Pediatric Dentist';

/* Job-Title plural */
$t['Zahnärzte'] = 'Dentists';
$t['Kinderzahnärzte'] = 'Pediatric Dentists';




$t['* Wir sind kostenfrei 24h/ Tag für Sie erreichbar.'] = '* We are accessible free of charge 24 hours / day for you.';
$t['Newsbereich'] = 'News area';
$t['Kooperationspartner'] = 'Partners';


$t['/news'] = '/news';
$t['/presse'] = '/presse';
$t['/jobs/'] = '/jobs/';
$t['/praxenverzeichnis'] = '/praxenverzeichnis';
$t['/kontakt'] = '/kontakt';
$t['/impressum'] = '/impressum';
$t['/datenschutz'] = '/datenschutz';
$t['/partner'] = '/partner';
$t['/agb'] = '/agb';
$t['/jobs'] = '/jobs';

# dates / times
$t['Sonntag'] = 'Sunday';
$t['Montag'] = 'Monday';
$t['Dienstag'] = 'Tuesday';
$t['Mittwoch'] = 'Wednesday';
$t['Donnerstag'] = 'Thursday';
$t['Freitag'] = 'Friday';
$t['Samstag'] = 'Saturday';
$t['So'] = 'Sun';
$t['Mo'] = 'Mon';
$t['Di'] = 'Tue';
$t['Mi'] = 'Wed';
$t['Do'] = 'Thu';
$t['Fr'] = 'Fri';
$t['Sa'] = 'Sat';

# cities
$t['München'] 		= 'Munich';
$t['Köln'] 			= 'Cologne';
$t['Düsseldorf'] 	= 'Dusseldorf';
$t['Wien']			= 'Vienna';
$t['Nürnberg']		= 'Nuremberg';



/* *************************************************** */

$t['ok']='OK';
$t['cancel']='Cancel';
$t['save']='Save';
$t['back']='Back';
$t['login']='Login';
$t['logout']='Logout';

$t['language']='Language';
$t['message']='Message';
$t['subject']='Subject';
$t['email']='Email';
$t['name']='Name';
$t['access']='Access';
$t['group']='Group';
$t['first_name']='First name';
$t['last_name']='Last name';
$t['password']='Password';
$t['repeat_password']='repeat password';
$t['last_login_at']='Last login';

// contact
$t['contact_header']='Contact us!';
$t['contact_header_p']='Do you have questions or do you want to support us? Leave a message!';
$t['contact_header_thank_you']='Thank you for your feedback!';
$t['contact_name']='Name';
$t['contact_email']='E-Mail';
$t['contact_subject']='Subject';
$t['contact_message']='Your Message';

$t['spamfree_you_are_identified_as_a_spammer']='Sorry, we could NOT receive your message as our system identified you as a spammer!';

// login
$t['wrong_email_or_password']='Username und/oder Passwort stimmen nicht überein!';
$t['logout_ok']='Du bist nun ausgelogged!';

$t['TXT_MONTH_01']='January';
$t['TXT_MONTH_02']='February';
$t['TXT_MONTH_03']='March';
$t['TXT_MONTH_04']='April';
$t['TXT_MONTH_05']='May';
$t['TXT_MONTH_06']='June';
$t['TXT_MONTH_07']='July';
$t['TXT_MONTH_08']='August';
$t['TXT_MONTH_09']='September';
$t['TXT_MONTH_10']='October';
$t['TXT_MONTH_11']='November';
$t['TXT_MONTH_12']='December';

$t['TXT_DAY_0']='Sunday';
$t['TXT_DAY_1']='Monday';
$t['TXT_DAY_2']='Tuesday';
$t['TXT_DAY_3']='Wednesday';
$t['TXT_DAY_4']='Thursday';
$t['TXT_DAY_5']='Friday';
$t['TXT_DAY_6']='Saturday';

// blog
$t['blog']='Blog';
$t['url_blog']='/news';
$t['blog_title']='News';
$t['author']='Autor';
$t['leave_a_reply']='Hinterlassen Sie einen Kommentar';
$t['blog_comment_name']='Name (benötigt)';
$t['blog_comment_email']='E-Mail (wird nicht angezeigt) (benötigt)';
$t['blog_comment_submit']='Kommentar abschicken';
$t['could_not_save_comment']='Der Kommentar konnte nicht gespeichert werden.';
$t['comment_created_successfully']='Ihr Kommentar wurde gespeichert und liegt der Redaktion zur Prüfung vor.';
$t['show_older_blogposts']='Ältere News';
$t['show_newer_blogposts']='Neuere News';
$t['unvalid_title']='Der Titel ist nicht gültig!';
$t['unvalid_permalink']='Der Permalink ist nicht gültig!';
$t['comment']='Kommentar';
$t['comments']='Kommentare';
$t['unvalidated_comment']='Unvalidierter Kommentar';
$t['unvalidated_comments']='Unvalidierte Kommentare';
$t['should_this_comment_be_deleted_really']='Soll dieser Kommentar wirklich gelöscht werden?';
$t['comment_validated_successfully']='Der Kommentar wurde aktivert und wird nun angezeigt.';
$t['could_not_validate_comment']='Der Kommentar konnte nicht aktivert werden.';
$t['comment_unvalidated_successfully']='Der Kommentar wurde wieder deaktiviert. Er wird nicht mehr angezeigt.';
$t['could_not_unvalidate_comment']='Der Kommentar konnte nicht deaktiviert werden.';
$t['comment_deleted_successfully']='Der Kommentar wurde gelöscht.';
$t['could_not_delete_comment']='Der Kommentar konnte nicht gelöscht werden.';
$t['blog_guestblogger']='Ihr Thema? Hier vorschlagen und Gastblogger werden!';
$t['url_blog_guestblogger']='/de/kontakt&subject=guestblogger';

$t["non_accessible_user_ids"] = "We're sorry, this code cannot be used with this doctor";
$t["non_accessible_location_ids"] = "We're sorry, this code cannot be used with this practice";
$t["accessible_medical_specialty_ids"] = "We're sorry, this code cannot be used for an appointment with this medical specialty";
$t["accessible_has_no_contract"] = "We're sorry, this code cannot be used with this doctor";
$t["accessible_has_contract"] = "We're sorry, this code cannot be used with this doctor";
$t["accessible_mobile"] = "We're sorry, this code cannot be used on our mobile website, please try from a desktop browser";
$t["accessible_from"] = "We're sorry, this code cannot be used yet";
$t["accessible_until"] = "We're sorry, this code has expired";
$t["referral_owner_creator_error"] = "You can't use your own referral code.";
$t["referral_exists_doctor"] = "Es tut uns Leid, mit Ihrem Aktionscode wurde bereits ein Termin bei diesem Arzt gebucht. Bitte kontaktieren Sie unseren Support kostenfrei unter 0800 2222 133";
$t["referral_exists_medical_specialty"] = "Es tut uns Leid, mit Ihrem Aktionscode wurde bereits ein Termin bei einem Arzt dieser Fachrichtung gebucht. Bitte kontaktieren Sie unseren Support kostenfrei unter 0800 2222 133";

$t['access_denied']='Access denied! You don\'t have the rights to access this function';
$t['are_you_sure']='Are you sure?';
$t['the_entry_was_deleted_successfully']='The entry was deleted!';
$t['the_entry_was_activated_successfully']='The entry was successfully activated!';
$t['the_entry_was_canceled_successfully']='The entry was successfully canceled!';
$t['the_entry_could_not_be_deleted']='The entry could not be deleted!';
$t['the_entry_could_not_be_canceled']='The entry could not be canceled!';
$t['action_was_successfully']='The action was successful.';
$t['the_entry_allready_exists']='The entry exists already.';
$t['name_of_primary_key_is_empty']='The name of primary key is empty!';
$t['no_data_has_been_updated']='Could not save the entry or the data has not been changed.';
$t['the_entry_could_not_be_created']='The entry could not be created!';
$t['passwords_are_not_equal']='Both passwords are not equal!';

$t['save']='Save';
$t['new_item']='New Entry';
$t['edit_item']='Edit Entry';
$t['delete_item']='Delete Entry';
$t['cancel_item']='Cancel Entry';
$t['search']='Search';
$t['start_search']='Search';
$t['show_all_search_keys']='Show all search items';
$t['hide_all_search_keys']='Hide all search items';

$t['created_at']='Created at';
$t['created_by']='Created by';
$t['updated_at']='Updated at';
$t['updated_by']='Updated by';

// cms
$t['directory']='Verzeichnis';
$t['content']='Content';
$t['navigation']='Navigation';
$t['could_not_create_new_dir']='Das neue Verzeichnis konnte nicht erstellt werden!';
$t['dir_created_successfully']='Das neue Verzeichnis wurde erstellt.';
$t['could_not_edit_dir']='Das neue Verzeichnis konnte nicht geändert werden!';
$t['dir_edited_successfully']='Das Verzeichnis wurde geändert.';
$t['could_not_create_new_content']='Der neue Content konnte nicht erstellt werden!';
$t['content_created_successfully']='Der neue Content wurde erstellt.';
$t['could_not_edit_content']='Der Content konnte nicht geändert werden!';
$t['content_edited_successfully']='Der Content wurde geändert.';
$t['version_created_successfully']='Die neue Version wurde erstellt.';
$t['could_not_edit_version']='Die Version konnte nicht geändert werden!';
$t['version_edited_successfully']='Die Version wurde geändert.';

$t['dir_new']='Neues Verzeichnis';
$t['dir_edit']='Verzeichnis bearbeiten';
$t['dirname']='Verzeichnisname';
$t['dirtitle']='Verzeichnistitel';
$t['subdir_of']='Übergeordnetes Verzeichnis';
$t['need_dirname']='Bitte geben Sie einen Verzeichnisnamen ein.';
$t['need_dirtitle']='Bitte geben Sie einen Verzeichnistitel ein.';
$t['tip_dir_new_name']='Das ist der Verzeichnisname wie er später auch in der URL erscheint.';
$t['tip_dir_new_title']='Das ist der Verzeichnistitel wie er später auch in der Navigation erscheint.';
$t['tip_dir_new_subdir_of']='In diesem Verzeichnis wird das neue Verzeichnis liegen.';
$t['dir_id_is_not_valid']='Die dir_id ist nicht gültig!';
$t['could_not_delete_dir']='Das Verzeichnis konnte nicht gelöscht werden!';
$t['dir_deleted_successfully']='Das Verzeichnis wurde gelöscht!';
$t['there_is_no_dir_with_this_dir_id']='Es gibt kein Verzeichnis mit dieser id!';
$t['do_you_want_to_delete_this_dir']='Wollen Sie dieses Verzeichnis wirklich löschen?';
$t['you_can_not_restore_this_dir']='Das Verzeichnis und die darin enthaltenen Dateien können nicht wiederhergestellt werden!';
$t['content_id_is_not_valid']='Die content_id ist nicht gültig!';
$t['version_id_is_not_valid']='Die version_id ist nicht gültig!';
$t['can_not_move_the_dir_to_this_position']='Das Verzeichnis kann nicht an diese Stelle verschoben werden!';

$t['content_new']='Neuer Content';
$t['content_edit']='Content bearbeiten';
$t['filename']='Dateiname';
$t['title']='Titel';
$t['use_dirpath']='Verzeichnispfad nutzen';
$t['tags']='Tags';
$t['description']='Description';
$t['template']='Template';
$t['engine']='Engine';
$t['content_of_dir']='Verzeichnis';
$t['tip_content_new_title']='Geben Sie den Titel des Dokuments an.';
$t['tip_content_new_filename']='Geben Sie bitte den Dateinamen an, wie er in der URL erscheinen soll.';
$t['tip_content_new_use_dirpath']='Aktivieren Sie diese Option, wenn die Datei unter diesem Verzeichnis erreichbar sein soll.';
$t['tip_content_new_tags']='Geben Sie bitte die Tags getrennt mit Kommatas ein. Z.B. "spenden,effektiv,marktanalyse"';
$t['tip_content_new_description']='Geben Sie dem Content eine Kurzbeschreibung.';
$t['tip_content_new_template_id']='Welches Template soll für die Anzeige des Contents benutzt werden?';
$t['tip_content_new_engine_id']='Welche Render-Engine soll für die Anzeige des Content benutzt werden?';
$t['need_content_title']='Bitte geben Sie einen Titel für den Content ein.';
$t['need_content_filename']='Bitte geben Sie einen Dateinamen ein.';
$t['there_is_no_content_with_this_content_id']='Es gibt keinen Content mit dieser ID!';
$t['root_slash_not_allowed']='An erster Stelle darf kein Slash stehen!';
$t['do_you_want_to_delete_this_content']='Wollen Sie diesen Content wirklich löschen?';
$t['you_can_not_restore_this_content']='Der Content und all seine Versionen können dann nicht wiederhergestellt werden!';
$t['content_deleted_successfully']='Der Content wurde gelöscht!';
$t['could_not_delete_content']='Der Content konnte nicht gelöscht werden!';
$t['template_id_is_not_valid']='Die Template ID ist nicht gültig!';
$t['engine_id_is_not_valid']='Die Engine ID ist nicht gültig!';

$t['version_new']='Neue Version erstellen';
$t['version_edit']='Version bearbeiten und neue Version erstellen';
$t['do_you_want_to_delete_this_version']='Wollen Sie diese Version wirklich löschen?';
$t['you_can_not_restore_this_version']='Die Version kann dann nicht wiederhergestellt werden!';
$t['version_new_successfully']='Die neue Version wurde erfolgreich erstellt.';
$t['could_not_create_version']='Die neue Version konnte nicht erstellt werden!';
$t['version_deleted_successfully']='Die Version wurde gelöscht!';
$t['could_not_delete_version']='Die Version konnte nicht gelöscht werden!';
$t['version_activated_successfully']='Die Version wurde aktiviert';
$t['version_could_not_be_activated']='Die Version konnte nicht aktiviert werden!';
$t['tip_version_content']='Bitte geben Sie hier den Content ein';
$t['version_minor_change']='Kleine Änderungen: Alte Version überschreiben';

$t['could_not_find_media']='Das Mediafile konnte nicht gefunden werden!';
$t['wrong_extension']='Die hochgeladene Datei hat eine nicht unterstützte Dateiendung!';
$t['there_is_allready_a_file_in_this_category']='Es gibt bereits eine Datei mit diesem Dateinamen und dieser Kategorie!';
$t['could_not_save_the_media']='Die Datei konnte nicht gespeichert werden!';
$t['need_uploadfile']='Bitte geben Sie eine Datei zum Upload an!';

$t['one_or_more_non_valid_versions']='Die angegebenen Versionen sind nicht gültig!';
$t['versions_are_not_from_the_same_content']='Die angegebenen Versionen sind nicht vom gleichen Content!';
$t['there_are_no_differences_between_the_two_versions']='Beide Versionen sind gleich!';

$t['create_new_navigation_link']='Neuen Link erstellen';
$t['delete_navigation_link']='Link löschen';
$t['rename_navigation_link']='Link umbenennen';
$t['navigation_open_all_nodes']='Öffne alles';
$t['navigation_close_all_nodes']='Schließe alles';

return $t;