<?php
$t = array();

$t['ok']='OK';
$t['cancel']='Abbrechen';
$t['save']='Sichern';
$t['back']='Zurück';
$t['login']='Login';
$t['logout']='Logout';

$t['language']='Sprache';
$t['message']='Nachricht';
$t['subject']='Betreff';
$t['email']='E-Mail';
$t['created_at']='Erstelldatum';
$t['name']='Name';
$t['access']='Zugriff';
$t['first_name']='Vorname';
$t['last_name']='Nachname';
$t['password']='Passwort';
$t['group']='Gruppe';
$t['repeat_password']='Passwort wiederholen';
$t['last_login_at']='Letzter Login';

// contact
$t['contact_header']='Schreiben Sie uns!';
$t['contact_header_p']='Haben Sie Fragen oder wollen Sie uns unterstüzen? Schreiben Sie uns eine Nachricht!';
$t['contact_header_thank_you']='Vielen Dank für Ihr Feedback!';
$t['contact_name']='Name';
$t['contact_email']='E-Mail';
$t['contact_subject']='Betreff';
$t['contact_message']='Ihre Nachricht';
$t['could_not_create_new_contact']='Ihre Nachricht konnte nicht gespeichert werden! Bitte versuchen Sie es später nocheinmal!';
$t['contact_created_successfully']='Vielen Dank für Ihre Nachricht! Wir kümmern uns darum um melden uns bei Ihnen so schnell wie möglich.';
$t['language']='Sprache';
$t['message']='Nachricht';

$t['spamfree_you_are_identified_as_a_spammer']='Wir konnten Ihre Nachricht leider nicht annehmen, weil unser System Sie als Spammer eingestuft hat!';

// login
$t['wrong_email_or_password']='Username und/oder Passwort stimmen nicht überein!';
$t['logout_ok']='Sie haben sich erfolgreich ausgeloggt!';

$t['TXT_MONTH_01']='Januar';
$t['TXT_MONTH_02']='Februar';
$t['TXT_MONTH_03']='März';
$t['TXT_MONTH_04']='April';
$t['TXT_MONTH_05']='Mai';
$t['TXT_MONTH_06']='Juni';
$t['TXT_MONTH_07']='Juli';
$t['TXT_MONTH_08']='August';
$t['TXT_MONTH_09']='September';
$t['TXT_MONTH_10']='Oktober';
$t['TXT_MONTH_11']='November';
$t['TXT_MONTH_12']='Dezember';

$t['TXT_DAY_0']='Sonntag';
$t['TXT_DAY_1']='Montag';
$t['TXT_DAY_2']='Dienstag';
$t['TXT_DAY_3']='Mitwoch';
$t['TXT_DAY_4']='Donnerstag';
$t['TXT_DAY_5']='Freitag';
$t['TXT_DAY_6']='Samstag';

// blog
$t['blog']='Blog';
$t['url_blog']='/news';
$t['blog_title']='News';
$t['author']='Autor';
$t['leave_a_reply']='Hinterlassen Sie einen Kommentar';
$t['blog_comment_name']='Name (benötigt)';
$t['blog_comment_email']='E-Mail (wird nicht angezeigt) (benötigt)';
$t['blog_comment_submit']='Kommentar abschicken';
$t['could_not_save_comment']='Der Kommentar konnte nicht gespeichert werden.';
$t['comment_created_successfully']='Ihr Kommentar wurde gespeichert und liegt der Redaktion zur Prüfung vor.';
$t['show_older_blogposts']='Ältere News';
$t['show_newer_blogposts']='Neuere News';
$t['unvalid_title']='Der Titel ist nicht gültig!';
$t['unvalid_permalink']='Der Permalink ist nicht gültig!';
$t['comment']='Kommentar';
$t['comments']='Kommentare';
$t['unvalidated_comment']='Unvalidierter Kommentar';
$t['unvalidated_comments']='Unvalidierte Kommentare';
$t['should_this_comment_be_deleted_really']='Soll dieser Kommentar wirklich gelöscht werden?';
$t['comment_validated_successfully']='Der Kommentar wurde aktivert und wird nun angezeigt.';
$t['could_not_validate_comment']='Der Kommentar konnte nicht aktivert werden.';
$t['comment_unvalidated_successfully']='Der Kommentar wurde wieder deaktiviert. Er wird nicht mehr angezeigt.';
$t['could_not_unvalidate_comment']='Der Kommentar konnte nicht deaktiviert werden.';
$t['comment_deleted_successfully']='Der Kommentar wurde gelöscht.';
$t['could_not_delete_comment']='Der Kommentar konnte nicht gelöscht werden.';
$t['blog_guestblogger']='Ihr Thema? Hier vorschlagen und Gastblogger werden!';
$t['url_blog_guestblogger']='/de/kontakt&subject=guestblogger';

$t["non_accessible_user_ids"] = "Leider gilt dieser Code nicht für den von Ihnen gewählten Arzt";
$t["non_accessible_location_ids"] = "Leider gilt dieser Code nicht für den von Ihnen gewählten Praxen";
$t["accessible_medical_specialty_ids"] = "Leider gilt dieser Code nicht für die von Ihnen gewählte Fachrichtung";
$t["accessible_has_no_contract"] = "Leider gilt dieser Code nicht für den von Ihnen gewählten Arzt";
$t["accessible_has_contract"] = "Leider gilt dieser Code nicht für den von Ihnen gewählten Arzt";
$t["accessible_mobile"] = "Leider gilt dieser Code nicht für unsere mobile Website. Bitte versuchen Sie es erneut über einen Desktop Browser";
$t["accessible_from"] = "Leider kann dieser Code noch nicht genutzt werden";
$t["accessible_until"] = "Leider ist dieser Code nicht mehr gültig";
$t["referral_owner_creator_error"] = "Sie können Ihren eigenen Empfehlungscode nicht verwenden.";
$t["referral_exists_doctor"] = "Es tut uns Leid, mit Ihrem Aktionscode wurde bereits ein Termin bei diesem Arzt gebucht. Bitte kontaktieren Sie unseren Support kostenfrei unter 0800 2222 133";
$t["referral_exists_medical_specialty"] = "Es tut uns Leid, mit Ihrem Aktionscode wurde bereits ein Termin bei einem Arzt dieser Fachrichtung gebucht. Bitte kontaktieren Sie unseren Support kostenfrei unter 0800 2222 133";

// admin
$t['access_denied']='Sie haben nicht die erforderlichen Rechte, um diese Aktion auszuführen!';
$t['are_you_sure']='Sind sie sicher?';
$t['the_entry_was_deleted_successfully']='Der Eintrag wurde erfolgreich gelöscht.';
$t['the_entry_was_activated_successfully']='The entry was successfully activated!';
$t['the_entry_was_canceled_successfully']='Der Eintrag wurde erfolgreich storniert.';
$t['the_entry_could_not_be_deleted']='Der Eintrag konnte nicht gelöscht werden.';
$t['the_entry_could_not_be_canceled']='Der Eintrag konnte nicht storniert werden.';
$t['action_was_successfully']='Die Aktion wurde erfolgreich ausgeführt.';
$t['the_entry_allready_exists']='Der Eintrag existiert schon.';
$t['name_of_primary_key_is_empty']='Feldname des Primärschlüssel ist leer!';
$t['no_data_has_been_updated']='Speichern ist fehlgeschlagen oder es haben sich keine Daten geändert.';
$t['the_entry_could_not_be_created']='Der Eintrag konnte nicht erstellt werden!';
$t['passwords_are_not_equal']='Die eingegebenen Passwörter stimmen nicht überein!';

$t['save']='Speichern';
$t['new_item']='Neuer Eintrag';
$t['edit_item']='Eintrag bearbeiten';
$t['delete_item']='Eintrag löschen';
$t['cancel_item']='kündigen bearbeiten';
$t['search']='Suchen';
$t['start_search']='Suchen';
$t['show_all_search_keys']='Zeige alle Suchparameter';
$t['hide_all_search_keys']='Verstecke alle Suchparameter';

$t['created_at']='Erstellt am';
$t['created_by']='Erstellt von';
$t['updated_at']='Geändert am';
$t['updated_by']='Geändert von';

// cms
$t['directory']='Verzeichnis';
$t['content']='Content';
$t['navigation']='Navigation';
$t['form_error_general']='Bitte prüfen Sie Ihre Eingaben';
$t['could_not_create_new_dir']='Das neue Verzeichnis konnte nicht erstellt werden!';
$t['dir_created_successfully']='Das neue Verzeichnis wurde erstellt.';
$t['could_not_edit_dir']='Das neue Verzeichnis konnte nicht geändert werden!';
$t['dir_edited_successfully']='Das Verzeichnis wurde geändert.';
$t['could_not_create_new_content']='Der neue Content konnte nicht erstellt werden!';
$t['content_created_successfully']='Der neue Content wurde erstellt.';
$t['could_not_edit_content']='Der Content konnte nicht geändert werden!';
$t['content_edited_successfully']='Der Content wurde geändert.';
$t['version_created_successfully']='Die neue Version wurde erstellt.';
$t['could_not_edit_version']='Die Version konnte nicht geändert werden!';
$t['version_edited_successfully']='Die Version wurde geändert.';

$t['dir_new']='Neues Verzeichnis';
$t['dir_edit']='Verzeichnis bearbeiten';
$t['dirname']='Verzeichnisname';
$t['dirtitle']='Verzeichnistitel';
$t['subdir_of']='Übergeordnetes Verzeichnis';
$t['need_dirname']='Bitte geben Sie einen Verzeichnisnamen ein.';
$t['need_dirtitle']='Bitte geben Sie einen Verzeichnistitel ein.';
$t['tip_dir_new_name']='Das ist der Verzeichnisname wie er später auch in der URL erscheint.';
$t['tip_dir_new_title']='Das ist der Verzeichnistitel wie er später auch in der Navigation erscheint.';
$t['tip_dir_new_subdir_of']='In diesem Verzeichnis wird das neue Verzeichnis liegen.';
$t['dir_id_is_not_valid']='Die dir_id ist nicht gültig!';
$t['could_not_delete_dir']='Das Verzeichnis konnte nicht gelöscht werden!';
$t['dir_deleted_successfully']='Das Verzeichnis wurde gelöscht!';
$t['there_is_no_dir_with_this_dir_id']='Es gibt kein Verzeichnis mit dieser id!';
$t['do_you_want_to_delete_this_dir']='Wollen Sie dieses Verzeichnis wirklich löschen?';
$t['you_can_not_restore_this_dir']='Das Verzeichnis und die darin enthaltenen Dateien können nicht wiederhergestellt werden!';
$t['content_id_is_not_valid']='Die content_id ist nicht gültig!';
$t['version_id_is_not_valid']='Die version_id ist nicht gültig!';
$t['can_not_move_the_dir_to_this_position']='Das Verzeichnis kann nicht an diese Stelle verschoben werden!';

$t['content_new']='Neuer Content';
$t['content_edit']='Content bearbeiten';
$t['filename']='Dateiname';
$t['title']='Titel';
$t['use_dirpath']='Verzeichnispfad nutzen';
$t['tags']='Tags';
$t['description']='Beschreibung';
$t['template']='Template';
$t['engine']='Engine';
$t['mimetype']='MimeType';
$t['content_of_dir']='Verzeichnis';
$t['tip_content_new_title']='Geben Sie den Titel des Dokuments an.';
$t['tip_content_new_filename']='Geben Sie bitte den Dateinamen an, wie er in der URL erscheinen soll.';
$t['tip_content_new_use_dirpath']='Aktivieren Sie diese Option, wenn die Datei unter diesem Verzeichnis erreichbar sein soll.';
$t['tip_content_new_tags']='Geben Sie bitte die Tags getrennt mit Kommatas ein. Z.B. "spenden,effektiv,marktanalyse"';
$t['tip_content_new_description']='Geben Sie dem Content eine Kurzbeschreibung.';
$t['tip_content_new_template_id']='Welches Template soll für die Anzeige des Contents benutzt werden?';
$t['tip_content_new_engine_id']='Welche Render-Engine soll für die Anzeige des Content benutzt werden?';
$t['tip_content_new_mimetype']='Welcher MimeType soll für die Anzeige des Content benutzt werden? (Default: text/html)';
$t['need_content_title']='Bitte geben Sie einen Titel für den Content ein.';
$t['need_content_filename']='Bitte geben Sie einen Dateinamen ein.';
$t['there_is_no_content_with_this_content_id']='Es gibt keinen Content mit dieser ID!';
$t['root_slash_not_allowed']='An erster Stelle darf kein Slash stehen!';
$t['do_you_want_to_delete_this_content']='Wollen Sie diesen Content wirklich löschen?';
$t['you_can_not_restore_this_content']='Der Content und all seine Versionen können dann nicht wiederhergestellt werden!';
$t['content_deleted_successfully']='Der Content wurde gelöscht!';
$t['could_not_delete_content']='Der Content konnte nicht gelöscht werden!';
$t['template_id_is_not_valid']='Die Template ID ist nicht gültig!';
$t['engine_id_is_not_valid']='Die Engine ID ist nicht gültig!';

$t['version_new']='Neue Version erstellen';
$t['version_edit']='Version bearbeiten und neue Version erstellen';
$t['do_you_want_to_delete_this_version']='Wollen Sie diese Version wirklich löschen?';
$t['you_can_not_restore_this_version']='Die Version kann dann nicht wiederhergestellt werden!';
$t['version_new_successfully']='Die neue Version wurde erfolgreich erstellt.';
$t['could_not_create_version']='Die neue Version konnte nicht erstellt werden!';
$t['version_deleted_successfully']='Die Version wurde gelöscht!';
$t['could_not_delete_version']='Die Version konnte nicht gelöscht werden!';
$t['version_activated_successfully']='Die Version wurde aktiviert';
$t['version_could_not_be_activated']='Die Version konnte nicht aktiviert werden!';
$t['tip_version_content']='Bitte geben Sie hier den Content ein';
$t['version_minor_change']='Kleine Änderungen: Alte Version überschreiben';

$t['could_not_find_media']='Das Mediafile konnte nicht gefunden werden!';
$t['wrong_extension']='Die hochgeladene Datei hat eine nicht unterstützte Dateiendung!';
$t['there_is_allready_a_file_in_this_category']='Es gibt bereits eine Datei mit diesem Dateinamen und dieser Kategorie!';
$t['could_not_save_the_media']='Die Datei konnte nicht gespeichert werden!';
$t['need_uploadfile']='Bitte geben Sie eine Datei zum Upload an!';

$t['one_or_more_non_valid_versions']='Die angegebenen Versionen sind nicht gültig!';
$t['versions_are_not_from_the_same_content']='Die angegebenen Versionen sind nicht vom gleichen Content!';
$t['there_are_no_differences_between_the_two_versions']='Beide Versionen sind gleich!';

$t['create_new_navigation_link']='Create a new link';
$t['delete_navigation_link']='Delete link';
$t['rename_navigation_link']='Rename link';
$t['navigation_open_all_nodes']='Open all nodes';
$t['navigation_close_all_nodes']='Close all nodes';

return $t;