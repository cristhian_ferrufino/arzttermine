<?php

// Definitions
// =====================================================================================================================

define('DS', DIRECTORY_SEPARATOR);
define('PS', PATH_SEPARATOR);

define('SYSTEM_PATH',      dirname(dirname(__FILE__)));
define('ASSET_PATH',       SYSTEM_PATH . '/htdocs/static');
define('SHARED_PATH',      SYSTEM_PATH . '/../../shared');
define('DISTRIBUTED_PATH', SHARED_PATH . '/distributed');
define('CACHE_PATH',       SHARED_PATH . '/cache');  // needs write permissions as symfony will put cache files here automatically

// Dependencies
// =====================================================================================================================

require_once SYSTEM_PATH . '/config/config.php';
require_once SYSTEM_PATH . '/include/additional.php'; // Kludgey extra includes
require_once SYSTEM_PATH . '/ngs/conf/constants.php'; // Additional (and somewhat useless) other constants

require_once SYSTEM_PATH . '/vendor/autoload.php';
require_once SYSTEM_PATH . '/vendor/symfony/symfony/src/Symfony/Component/ClassLoader/ClassLoader.php';

// System configuration
// =====================================================================================================================

ini_set('gd.jpeg_ignore_warning', 1);
setlocale(LC_MESSAGES, 'de_DE'); // DON'T USE LC_ALL

date_default_timezone_set($GLOBALS['CONFIG']["SYSTEM_TIMEZONE"]);

// Components
// =====================================================================================================================

// Autoloader
/* **************************************************************************** */

use \Symfony\Component\ClassLoader\ClassLoader;

$autoloader = new ClassLoader();
$autoloader->addPrefixes(
    array(
        'Symfony'      => SYSTEM_PATH . '/vendor/symfony/symfony/src/',
        'Arzttermine'  => SYSTEM_PATH . '/src/arzttermine/',
        'NGS'          => SYSTEM_PATH . '/ngs/classes/'
    ));

$autoloader->register();

// Application
/* **************************************************************************** */

use \Arzttermine\Application\Application;

$app = Application::getInstance();

/**
 * Temporary config hacks
 */
$app->container->setParameter('app.default_locale', $GLOBALS['CONFIG']['CMS_LOCALE_DEFAULT']);
$app->container->setParameter('router.resource', SYSTEM_PATH . '/config/routing.yml');
$app->container->setParameter('router.cache_dir', CACHE_PATH);
$app->container->setParameter('database.config.type', 'admin');
$app->container->setParameter('translator.default_locale', $GLOBALS['CONFIG']['CMS_LOCALE_DEFAULT']);
$app->container->setParameter('translator.sources', array(
        'yaml' => array(
            'de' => $GLOBALS['CONFIG']['SYSTEM_PATH_LOCALE'] . '/messages.de.yml',
            'en' => $GLOBALS['CONFIG']['SYSTEM_PATH_LOCALE'] . '/messages.en.yml',
            'es' => $GLOBALS['CONFIG']['SYSTEM_PATH_LOCALE'] . '/messages.es.yml',
        ),
        'array' => array(
            'de' => $GLOBALS['CONFIG']['SYSTEM_PATH_LOCALE'] . '/translations.de.php',
            'en' => $GLOBALS['CONFIG']['SYSTEM_PATH_LOCALE'] . '/translations.en.php',
            'es' => $GLOBALS['CONFIG']['SYSTEM_PATH_LOCALE'] . '/translations.es.php',
        )
    ));

// Container services
/* **************************************************************************** */
$app
    ->registerService(new \Arzttermine\Core\ExceptionListenerServiceProvider())
    ->registerService(new \Arzttermine\Db\DatabaseServiceProvider())
    ->registerService(new \Arzttermine\Router\RoutingServiceProvider())
    ->registerService(new \Arzttermine\View\TemplatingServiceProvider())
    ->registerService(new \Arzttermine\Security\AuthenticationServiceProvider())
    ->registerService(new \Arzttermine\Core\ApplicationContextServiceProvider())
    ->registerService(new \Arzttermine\Translation\TranslationServiceProvider())
    ->registerService(new \Arzttermine\Coupon\CouponServiceProvider());

return $app;
