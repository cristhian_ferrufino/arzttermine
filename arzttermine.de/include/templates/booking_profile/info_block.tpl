<div id="info-block" style="width: 100%">
  <h3>Angefragter Termin</h3>
  <div class="orange-bar">
      {if
      false == $booking->hasAppointmentEnquiryIntegration()
      &&
      $booking->hasValidAppointmentStartAt()
      }
          {calendar action="getWeekdayDateTimeText" datetime=$booking->getAppointmentStartAt() short=false}
      {else}
        <p>
            {$app->_('Vielen Dank für Ihre Anfrage, wir werden uns innerhalb der nächsten Stunden mit freien Terminen bei Ihnen melden.')}
        </p>
      {/if}
  </div>
  <div class="doctor">
    <div class="photo">
      <img src="{$user->getProfileAssetUrl($smarty.const.ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED)}" alt="{alt($user->getName())}">
    </div>
    <div class="info">
      <div>
        <div class="conjunction">bei</div>
        <div class="name">
            {$user->getName()}
        </div>
      </div>
      <div class="address">
        <h4>Adresse</h4>
        <div class="practice-name">{$location->getName()}</div>
        <div class="street">{$location->getAddress()}</div>
        <div class="city"></div>
      </div>
    </div>
  </div>
</div>