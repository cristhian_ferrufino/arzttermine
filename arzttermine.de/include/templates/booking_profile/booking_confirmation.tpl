<section id="booking-confirmation" style="width: 100%">
    {strip}
      <h3>{if $booking->hasAppointmentEnquiryIntegration()}
              {$app->_('Ihre Anfrage war erfolgreich')}
          {else}
              {$app->_('Das sind die nächsten Schritte')}
          {/if}
      </h3>
    {/strip}
  <div>
      {if $booking->hasAppointmentEnquiryIntegration()}
        <h4>Sie brauchen nichts weiter zu tun.</h4>
        <span>Wir werden in Kürze Ihren Wunschtermin per E-Mail bestätigen. Die Terminbuchung ist erst durch diese Bestätigung vollendet.</span>
          {if $booking->showICSLinkOnBookingConfirmationPage()}
            <a href="{$booking->getUrl(true, true)}?ics" class="icon-pre ico-calendar">Termin in meinen Kalender importieren</a>
          {/if}
      {else}
        <ol class="simple-list">
          <li>Ihre Terminanfrage wird nun von uns überprüft.</li>
          <li>Bei einer Rückfrage oder einer möglichen Terminverschiebung wird Sie unser Patienten-Support per Mail oder Telefon kontaktieren (Montag bis Freitag zwischen 07:00 und 18:00 Uhr).</li>
          <li>Ihr Termin wird in einer separaten Mail und per SMS bestätigt.</li>
          <li>Die Terminbuchung ist vollendet und Sie gehen wie gewünscht zu Ihrem bestätigten Arzttermin.</li>
        </ol>
        Bitte beachten Sie, dass Ihr Termin aktuell noch nicht bestätigt ist.
        Der Termin wird in einer gesonderten Email bestätigt, sobald dieser von uns überprüft wurde.
        Bis dahin besteht noch kein gültiger Termin in der angefragten Praxis.
      {/if}
      {* Don't show the print link unless we have a nice print.css}
              {if $booking->showPrintLinkOnBookingConfirmationPage()}
              <a href="javascript:window.print();" class="icon-pre ico-print">Terminerinnerung ausdrucken</a>
              {/if}
      {/*}
  </div>

  <div>
    <h4>Haben Sie noch Fragen?</h4>
    <p>Wir sind 24 Stunden kostenfrei für Sie erreichbar:</p>
    <a href="tel:08002222133">0800 2222 133</a>
  </div>
</section>