{strip}
{assign var="template_body_class" value="two-cols-with-form"}
{assign var="additional_body_classes" value="booking-cancel"}
{include file="header.tpl"}
{/strip}

<div class="container">
  <main>
      <h3>Erfolgreich!</h3>
    <section id="info-block">
      <div class="doctor">
        <div class="photo">
          <img src="{$user->getProfileAssetUrl($smarty.const.ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED)}" alt="{alt($user->getName())}">
        </div>
        <div class="info">
          <div>
            <div class="conjunction">bei</div>
            <div class="name">
              {$user->getName()}
            </div>
          </div>
          <div class="address">
            <h4>Adresse</h4>
            <div class="practice-name">{$location->getName()}</div>
            <div class="street">{$location->getAddress()}</div>
            <div class="city"></div>
          </div>
        </div>
      </div>
    </section>

    <section id="booking-confirmation">
      <section>
         Wir werden Sie unter den von Ihnen angegebenen Informationen kontaktieren.
         <br /><br />
         Falls Sie weitere Fragen zum Stand Ihrer Terminanfrage haben, freuen wir uns über Ihre Email an <a href="mailto:buchung@arzttermine.de" style="display:inline;">buchung@arzttermine.de</a>.
      </section>
    </section>
  </main>
</div>

{include file="footer.tpl"}
