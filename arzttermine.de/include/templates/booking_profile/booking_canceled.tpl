{strip}
{assign var="template_body_class" value="two-cols-with-form"}
{assign var="additional_body_classes" value="booking-cancel"}
{include file="header.tpl"}
{/strip}

<div class="container">
  <main>
      <h3>Sie haben den Terminvorschlag abgelehnt</h3>
    <section id="info-block">
      <div class="doctor">
        <div class="photo">
          <img src="{$user->getProfileAssetUrl($smarty.const.ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED)}" alt="{alt($user->getName())}">
        </div>
        <div class="info">
          <div>
            <div class="conjunction">bei</div>
            <div class="name">
              {$user->getName()}
            </div>
          </div>
          <div class="address">
            <h4>Adresse</h4>
            <div class="practice-name">{$location->getName()}</div>
            <div class="street">{$location->getAddress()}</div>
            <div class="city"></div>
          </div>
        </div>
      </div>
    </section>

    <section id="booking-confirmation">
      <section>
        <span style="font-size: 24px;margin-bottom:30px;color: grey;display:block;">Dies sind die n&auml;chsten Schritte...</span>
        <div id="firststep">
          Es tut uns leid, dass Ihnen der Terminvorschlag nicht zusagt. Wir möchten Ihnen trotzdem helfen einen Termin in Ihrer Wunschpraxis zu bekommen.
          <br /><br />
          Wir können Sie anrufen ...
          <form>
            <button type="button">Ich wünsche eine telefonische Kontaktaufnahme</button>
          </form>
            Falls Sie nicht so lange warten m&ouml;chten, oder Ihre Terminanfrage komplett absagen möchten, k&ouml;nnen Sie uns gerne eine Email an <a href="mailto:buchung@arzttermine.de" style="display:inline;">buchung@arzttermine.de</a> senden.
          <br /><br />
          Bitte teilen Sie uns dann folgende Informationen mit:
          <ul style="list-style-type: disc;margin-left:50px;">
            <li>Postleitzahl</li>
            <li>zeitliche Verf&uuml;gbarkeit</li>
            <li>Arzt/Klinik (ob eine Alternativpraxis in Ordnung ist)</li>
          </ul>
        </div>
        <div id="secondstep">
          Können wir Sie über die unten angezeigte Telefonnummer erreichen ?
          <br /><br />
          <form method="post">
            <label>Telefonnummer</label>
            <input type="text" value="{$booking->getPhone()}" name="phone" />

            <h4>Bitte wählen Sie Ihre zeitliche Verfügbarkeit</h4>
            <div class="form-check form-check-inline">
              <label class="form-check-label">
                <input class="form-check-input" type="checkbox" name="form[contact_availability][]" value="Morgens (07:00 - 09:00)"> Morgens (07:00 - 09:00)
              </label>
            </div>
            <div class="form-check form-check-inline">
              <label class="form-check-label">
                <input class="form-check-input" type="checkbox" name="form[contact_availability][]" value="Vormittags (09:00 - 12:00)"> Vormittags (09:00 - 12:00)
              </label>
            </div>
            <div class="form-check form-check-inline">
              <label class="form-check-label">
                <input class="form-check-input" type="checkbox" name="form[contact_availability][]" value="Mittags (12:00 - 15:00)"> Mittags (12:00 - 15:00)
              </label>
            </div>
            <div class="form-check form-check-inline">
              <label class="form-check-label">
                <input class="form-check-input" type="checkbox" name="form[contact_availability][]" value="Nachmittags (15:00 - 18:00)"> Nachmittags (15:00 - 18:00)
              </label>
            </div>

            <button type="submit">Anfrage senden</button>
          </form>
        </div>
      </section>
    </section>
  </main>
</div>

{include file="footer.tpl"}
