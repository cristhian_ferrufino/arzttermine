<?xml version="1.0" encoding="UTF-8" ?>

<rss version="2.0"
	xmlns:content="http://purl.org/rss/1.0/modules/content/"
	xmlns:wfw="http://wellformedweb.org/CommentAPI/"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:atom="http://www.w3.org/2005/Atom"
	xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
	xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
>
<channel>
	<title>{$this->title}</title>
	<atom:link href="{$this->atom_link}" rel="self" type="application/rss+xml" />
	<link>{$this->link}</link>
	<description>{$this->description}</description>
	<lastBuildDate>{$this->lastbuilddate}</lastBuildDate>
	<language>{$this->lang}</language>
	<sy:updatePeriod>hourly</sy:updatePeriod>
	<sy:updateFrequency>1</sy:updateFrequency>
{foreach from=$blog->getPosts() item='blogpost'}
	<item>
		<title>{$blogpost->getTitle()}</title>
		<link>{$blogpost->getUrl()}</link>
		<comments>{$blogpost->url}#comments</comments>
		<pubDate>{$blogpost->getPublishedAt('D, d M Y H:i:s +0000')}</pubDate>
		<dc:creator>{$blogpost->getAuthor()}</dc:creator>
		<guid isPermaLink="true">{$blogpost->getUrl()}</guid>
		<description><![CDATA[{$blogpost->getExcerpt()}]]></description>
		<content:encoded><![CDATA[{$blogpost->getBlogPostHtml()}]]></content:encoded>
		<slash:comments>{$blogpost->countValidatedComments()}</slash:comments>
	</item>
{/foreach}
</channel>
</rss>
