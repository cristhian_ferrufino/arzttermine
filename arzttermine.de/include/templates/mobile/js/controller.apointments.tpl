{literal}
<script type='text/javascript'>

//global variables
var items = [];
i=2;
$(function() {
	//get the call attributes from the more-appointmens-btn
    var $moreAppointments = $("#more-appointments-btn"); 
	var location_id = $moreAppointments.data('location_id');
	var user_id = $moreAppointments.data('user_id');
	var insurance_id = $moreAppointments.data('insurance_id');

	//get current date and format it appropriately for the json call
	var d = new Date();
	var month = d.getMonth()+1;
	var day = d.getDate();
	var date_start = d.getFullYear() + '-' +
		((''+month).length<2 ? '0' : '') + month + '-' +
		((''+day).length<2 ? '0' : '') + day;

	//create date_end, 60 days after today and format it appropriately for the json call
	var d2 = new Date();
	d2.setDate(d.getDate()+60);
	var month2 = d2.getMonth()+1;
	var day2 = d2.getDate();
	var date_end = d2.getFullYear() + '-' +
		((''+month2).length<2 ? '0' : '') + month2 + '-' +
		((''+day2).length<2 ? '0' : '') + day2;

	//make the JSON call
	$.getJSON('{/literal}{$CONFIG.URL_API}{literal}/get-available-appointments.json?' +
	'location_id='+location_id+
	'&user_id='+user_id+
	'&insurance_id='+insurance_id+
	'&date_start='+date_start+
	'&date_end='+date_end+
	'&first_week=1'

	, function(data) {
		if (typeof data === 'string') {
			{/literal}{*}dont change this string "docdir" as it is needed in the ios app which uses the same api call in the same version{/*}{literal}
			if (data === 'docdir') {
				$('<div class="day"><div class="row free-schedule"><ul><a href="/termin?u='+user_id+'&l='+location_id+'&m=&i='+insurance_id+'"><li style="width:auto;padding:5px 10px">Jetzt freie Termine bei diesem Arzt anfragen</li></a></ul></div></div><div class="clearfix"></div>').appendTo('.doctor-appointments');
				$('.row.more.last').remove();
			} else {
				$(data).appendTo('.doctor-appointments');
				$('<div class="day" style="padding:10px">'+data+'</div>').appendTo('.doctor-appointments');
				$('.row.more.last').remove();
			}
			return;
		}

		$.each(data, function(key, val) {
			//create the div element to append
			if (val.length!==0) {
				//format the date

				$.datepicker.regional['de'] = {clearText: 'löschen', clearStatus: 'aktuelles Datum löschen',
                        closeText: 'schließen', closeStatus: 'ohne Änderungen schließen',
                        prevText: '<zurück', prevStatus: 'letzten Monat zeigen',
                        nextText: 'Vor>', nextStatus: 'nächsten Monat zeigen',
                        currentText: 'heute', currentStatus: '',
                        monthNames: ['Januar','Februar','März','April','Mai','Juni',
                            'Juli','August','September','Oktober','November','Dezember'],
                        monthNamesShort: ['Jan','Feb','Mär','Apr','Mai','Jun',
                            'Jul','Aug','Sep','Okt','Nov','Dez'],
                        monthStatus: 'anderen Monat anzeigen', yearStatus: 'anderes Jahr anzeigen',
                        weekHeader: 'Wo', weekStatus: 'Woche des Monats',
                        dayNames: ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'],
                        dayNamesShort: ['So','Mo','Di','Mi','Do','Fr','Sa'],
                        dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa'],
                        dayStatus: 'Setze DD als ersten Wochentag', dateStatus: 'Wähle D, M d',
                        dateFormat: 'dd.mm.yy', firstDay: 1,
                        initStatus: 'Wähle ein Datum', isRTL: false};
				$.datepicker.setDefaults($.datepicker.regional['de']);

				parsedDate=$.datepicker.parseDate('yy-mm-dd', key);
				formatedDate=$.datepicker.formatDate('DD, d. MM, yy', parsedDate );

				var div_element='<li class="table-view-divider">'+formatedDate+'</li>'
						+'<li class="table-view-cell">';

				$.each(val, function(index, value) {
					//format the value in hh:mm to display in the list
					hour = value.substring(0, value.length - 3);
					hour = hour.substring(11);

					//format the value in YYMMDDThhmmZ format to append it in the booking link
					urlValue = value.replace(/-/g,'');
					urlValue = urlValue.replace(/ /g,'');
					urlValue = urlValue.replace(/:/g,'');
					urlValue = urlValue.slice(0, 8) + "T" + urlValue.slice(8);
					urlValue += "Z";

					//for each free slot create a link to the booking page
					div_element+='<a href="/termin?u='+user_id+'&l='+location_id+'&m=&i='+insurance_id+'&s='+urlValue+'" class="btn btn-primary btn-small" data-datetime="'+value+'">'+hour+'</a>';
                    });
					items.push(div_element);
				}
				div_element+='</li>';
		});
		// when document loads append the first two items
		var firstTwoItems = [items[0],items[1]];
		$('<ul/>', {
			'class': 'my-new-list table-view',
			html: firstTwoItems.join('')
		}).appendTo('.doctor-appointments');
	});

	//clicking on more-appointments-btn results in fetching the next two days
    $moreAppointments.click(function() {
		nextItems=[items[i],items[i+1]];
		$('.doctor-appointments .my-new-list').append(nextItems.join(''));
		i=i+2;
	});
});
</script>
{/literal}
