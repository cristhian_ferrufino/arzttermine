<script type="text/javascript">

//create an array of objects of cities
var AllCities = [
    {
        id: 1,
        name: "Berlin",
        specialties: [1, 5, 10, 12, 13, 17, 18, 19, 26, 27, 28, 29, 30, 31, 32],
        lat: 52.519171,
        lng: 13.4060912
    },
    {
        id: 2,
        name: "Frankfurt",
        specialties: [1, 2, 3, 4, 5, 6, 7, 9, 10, 12, 13, 30],
        lat: 50.1109221,
        lng: 8.6821267
    },
    {
        id: 3,
        name: "Hamburg",
        specialties: [1, 2, 3, 5, 9, 12, 13, 15, 17, 30],
        lat: 53.5510846,
        lng: 9.9936818
    },
    {
        id: 4,
        name: "Köln",
        specialties: [1, 2, 3, 4, 7, 10, 11, 12, 13, 17, 19, 30],
        lat: 50.937531,
        lng: 6.9602786
    },
    {
        id: 5,
        name: "München",
        specialties: [1, 3, 4, 5, 6, 10, 12, 13],
        lat: 48.1366069,
        lng: 11.5770851
    },
    {
        id: 6,
        name: "Bremen",
        specialties: [1, 12, 13, 17, 30],
        lat: 53.078965,
        lng: 8.801765
    },
    {
        id: 7,
        name: "Dresden",
        specialties: [1, 12, 13, 15],
        lat: 51.050365,
        lng: 13.737309
    },
    {
        id: 8,
        name: "Düsseldorf",
        specialties: [1, 2, 5, 13, 15, 20],
        lat: 51.227733,
        lng: 6.773455
    },
    {
        id: 9,
        name: "Essen",
        specialties: [1, 5, 13, 22, 23],
        lat: 51.455628,
        lng: 7.011568
    },
    {
        id: 10,
        name: "Hannover",
        specialties: [1, 3, 6, 10, 12, 13, 30],
        lat: 52.37554,
        lng: 9.731719
    },
    {
        id: 11,
        name: "Leipzig",
        specialties: [1, 13, 15],
        lat: 51.340639,
        lng: 12.377472
    },
    {
        id: 12,
        name: "Nürnberg",
        specialties: [1, 13, 30],
        lat: 49.452022,
        lng: 11.076745
    },
    {
        id: 13,
        name: "Stuttgart",
        specialties: [14, 1, 4, 17],
        lat: 48.775339,
        lng: 9.181738
    },
    {
        id: 14,
        name: "Dortmund",
        specialties: [1],
        lat: 51.513376,
        lng: 7.465333
    }
];

function getDistance( lat1, lon1, lat2, lon2, unit ) {
    var radlat1  = Math.PI * lat1/180,
        radlat2  = Math.PI * lat2/180,
        theta    = lon1-lon2,
        radtheta = Math.PI * theta/180,
        dist;

    dist = Math.acos(Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta)) * 180/Math.PI * 60 * 1.1515;

    if (unit == "K") {
        dist = dist * 1.609344;
    }

    if (unit == "N") {
        dist = dist * 0.8684;
    }

    return dist;
}

function update_form() {
    var select_form_location = $('#form_location').val(),
        select_options = {json_encode($select_location_medical_specialty_id_options)} || {},
        $medical_specialty_id = $('#form_medical_specialty_id');

    $medical_specialty_id.empty().append($('<option class="prompt" value="">...bitte Fachrichtung wählen...</option>'));

    for (var options in select_options[select_form_location]) {
        if (select_options[select_form_location].hasOwnProperty(options)) {
            $medical_specialty_id.append($('<option value="' + select_options[select_form_location][options] + '">' + options + '</option>'));
        }
    }

    $('#form_medical_specialty_id').val('');
}

$(function() {
    $('#form_location').change(function() {
        //clear exact location when changing city
        $('#form_exact_location').val('');

        //check if user selected to use current location
        if ($('#form_location').val() == "currentLocation") {
            locate_user();
        } else {
            update_form();
        }

        return false;
    });
});

//$(document).ready(function() {
function locate_user() {
    //get users location if geolocation exists
    if (navigator.geolocation) {
        //show a masked spinner until getting the user's location
        navigator.geolocation.getCurrentPosition(function(position) {
                    $('#spinner').remove();
                    Location = position;
                    //get latitude longitude of user
                    var currentLat = Location.coords.latitude,
                            currentLng = Location.coords.longitude,
                            unit = "K",
                            distances = [];


                    //get formatted address of user's current location
                    var geocoder = new google.maps.Geocoder();
                    var latLng = new google.maps.LatLng(currentLat, currentLng);
                    geocoder.geocode( { 'latLng': latLng}, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[1]) {
                                //assign the location field to the formatted address
                                $('#form_exact_location').val(results[0].formatted_address)
                            }
                        }
                    });

                    //for each city calculate the distance of the city to the user
                    AllCities.forEach(function( city ) {
                        var distance = getDistance(
                                currentLat, currentLng,
                                city["lat"], city["lng"],
                                unit
                        );
                        //push the name and the distance of each city to a new array of objects
                        distances.push({
                            name: city["name"],
                            distance: distance
                        });

                    }, this);

                    //sort the new array of cities by distances
                    var sortedDistances = distances.sort(function(a,b) { return (a.distance > b.distance) ? 1 : ((b.distance > a.distance) ? -1 : 0); } );

                    //set the value of the dropdown to the nearest city of the user's position
                    $('#form_location').val(sortedDistances[0].name).change();
                    //update medical specialties select
                    update_form();
                },
                function () {
                    //if geolocation failed remove the spinner
                    $('#spinner').remove();
                    alert("Geolocation unavailable. Please check your system settings");
                    $("#form_location").val(0);
                    $("#form_medical_specialty_id").attr('disabled', 'disabled').fadeTo('fast', 0.5);
                });
    }
}

</script>
