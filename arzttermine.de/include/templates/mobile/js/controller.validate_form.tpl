<script>
    function doBounce(element, times, distance, speed) {
        times    = times || 4;
        distance = distance || '4px';
        speed    = speed || 30;

        for (var i = 0; i < times; i++) {
            element.animate({ marginLeft: '-=' + distance }, speed)
                   .animate({ marginLeft: '+=' + distance }, speed);
        }
    }

    function toggleEnabled($elm, is_enabled) {
        is_enabled = !!is_enabled;

        if (is_enabled) {
            $elm.attr('disabled', 'disabled');
            $elm.fadeTo('fast', 0.5);
        } else if(!is_enabled) {
            $elm.removeAttr('disabled');
            $elm.fadeTo('fast', 1);
        }
    }
    
    function enable($elm) {
        toggleEnabled($elm, false);
    }
    
    function disable($elm) {
        toggleEnabled($elm, true);
    }

$(function() {
    var $locations_select = $('#form_location'),
        $insurances_select = $('#form_insurance_id'),
        $specialties_select = $('#form_medical_specialty_id');
    
    $('#form_submit').click(function(e) {
        if (!$locations_select.val()) {
            doBounce($locations_select);
            $locations_select.parent().addClass('has-error');
            e.preventDefault();
        } else {
            $locations_select.parent().removeClass('has-error');

            if (!$specialties_select.val()) {
                doBounce($specialties_select);
                $specialties_select.parent().addClass('has-error');
                e.preventDefault();
            } else {
                $specialties_select.parent().removeClass('has-error');
            }

            if (!$insurances_select.val()) {
                doBounce($insurances_select);
                $insurances_select.parent().addClass('has-error');
                e.preventDefault();
            } else {
                $insurances_select.parent().removeClass('has-error');
            }
        }
    });
});
    
</script>