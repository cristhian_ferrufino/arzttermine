<script>
    $(function() {

        var showMap = function() {
            var $searchMap = $('#search-map'),
                    $mapButton = $("header a.map"),
                    $locateButton = $("header a.map-location-arrow");

            //hide all elements except header and footer
            $mapButton.css('visibility', 'hidden');

            $mapButton.hide();
            $locateButton.show();
            var userMarkerIcon = new google.maps.MarkerImage(
                    "/static/img/googlemaps-mobile-site-pin-user.png",
                    null,
                    null,
                    null,
                    new google.maps.Size(34, 50));
            //create a userMarker
            var userMarker2 = new google.maps.Marker({
                map: googleMap,
                icon: userMarkerIcon
            });

            //create an infobbuble with user's current address
            userInfoBubbleUser2 = new InfoBubble({
                content: '<div class="infoBubbleContent">Aktueller Standort</div>',
                disableAutoPan: false,
                hideCloseButton: true,
                borderRadius: 12,
                borderWidth: 1,
                backgroundColor: '#dbdbdb',
                borderColor: "#cfcfcf",
                padding:0
            });

            //bind click event on marker
            google.maps.event.addListener(userMarker2, 'mousedown', function () {
                userInfoBubbleUser2.open(googleMap, userMarker2);
            });

            $locateButton.click(function() {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var googleMapCenter = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                    googleMap.setCenter(googleMapCenter );
                    userMarker2.setPosition(googleMapCenter);
                });
            });

            $("#doctor-profile-container").hide();

            //fit google map to window size
            $searchMap.height($(window).height()-$('header').height()-$('footer').height()-110);

            //trigger google map resize event
            google.maps.event.trigger(googleMap, "resize");

            //position footer to the bottom
            $("#footer").css({
                position: 'absolute',
                bottom: '0px',
                width: '100%',
                left: '0px'
            });

            //show map
            $searchMap.addClass('visible');
        };

        var hideMap = function() {
            var $searchMap = $('#search-map'),
                    $mapButton = $("header a.map");

            userInfoBubbleUser2.close();

            if ($searchMap.css('visibility') == 'visible') {
                $searchMap.css({
                    height: '0'
                });
                $mapButton.css('visibility', 'visible');
                $("#doctor-profile-container").show();
                $("#footer").css({
                    position: 'relative',
                    bottom: '0px',
                    width: '100%',
                    left: '0px'
                });

                $searchMap.removeClass('visible');
            } else {
                history.back()
            }
        };

        $('header a.map, .doctor-address').click(showMap);
        $("header a.back").click(hideMap);
    });
</script>