<script>
    // on load of the web page
    $(function() {

        var $search_map = $('#search-map');
        
        $(".map-btn").click(function() {

            // Hide all elements except header and footer
            $(".btn.map").hide();

            $(".pagination ,.collection, .search-summary").hide();
            // Fit google map to window size and push it off screen to hide popups
            $search_map.height($(window).height()-$('header').height()-$('footer').height()-110);

            // Position footer to the bottom
            $("#footer").css({
                "position":"absolute",
                "bottom": "0px",
                "width": "100%",
                "left": "0px"
            });
            
            // Show map
            $search_map.addClass('visible');

        });

        $(".back").click(function(e) {
            e.preventDefault();

            if ($search_map.hasClass('visible')) {
                $(".btn.map").show();

                $(".pagination ,.collection, .search-summary").show();
                
                $("#footer").css({
                    "position": "relative",
                    "bottom": "0px",
                    "width": "100%",
                    "left": "0px"
                });

                $search_map.removeClass('visible');

            } else {
                history.back()
            }
        });

    });
</script>
