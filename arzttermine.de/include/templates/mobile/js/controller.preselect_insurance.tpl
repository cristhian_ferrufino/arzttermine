<script type='text/javascript'>
    //on load of the web page
    $(function() {
        var $insurances_select = $('.insurances-select');
        
        //check if user has not selected an insurance
        if ($insurances_select.val() != 1 &&
            $insurances_select.val() != 2
        ) {
            //if true preselect the gesetzlich versichert
            $insurances_select.val(2);
        }
    });
</script>
