{if !isset($hideGlobalPhonenumberInFooter)}
<div class="call-now">
	<a class="btn btn-outline btn-secondary" href="tel:+498002222133">
		<span class="fa fa-phone"></span>
		0800 - 2222133
	</a>
</div>
{/if}

<footer>
	<table>
		<tr>
			<td><a class="" href="/impressum">{$app->_('Impressum')}</a></td>
			<td><a class="" href="/datenschutz">{$app->_('Datenschutz')}</a></td>
			<td><a class="" href="/agb">{$app->_('AGB')}</a></td>
		</tr>
	</table>
    
	<a class="primary-links" href="mailto:support@arzttermine.de">
		<span class="fa fa-envelope"></span>
		support@arzttermine.de
	</a>
</footer>

</div><!-- .content -->

<script src="/js/mobile.js"></script>

<script>
	$(document).ready(function(){
		$.each(AT.q,function(index,f){
			$(f);
		});
	})
</script>

{* Map refreshing/buttons code. Will just force into every template because balls to it for now *}
<script type='text/javascript'>
    AT._defer(function() {
        $(function () {
            var $search_map = $('#search-map');

            var refreshMap = function () {
                if (AT !== undefined) {
                    var map = AT.Maps.getMap(),
                            center = map.getCenter(),
                            zoom = map.getZoom();

                    google.maps.event.trigger(map, 'resize');
                    map.setCenter(center);
                    map.setZoom(zoom);
                }
            };

            if ($search_map.length) {
                $("header a.map").click(function (e) {
                    e.preventDefault();
                    var $footer = $('footer');

                    //hide all elements except header and footer
                    $("header a.map").hide();
                    $(".search-result, .call-now").hide();
                    //position footer to the bottom
                    $footer.css({
                        position: "absolute",
                        bottom: "0",
                        width: "100%",
                        left: "0"
                    });

                    //fit google map to window size
                    $search_map.height($(window).height() - $('header').height() - $footer.height()).addClass('visible').show();

                    refreshMap();
                });
            } else {
                $("header a.map").hide();
            }

            $("header a.back").click(function (e) {
                e.preventDefault();

                if ($search_map.is(':visible')) {
                    $("header a.map").show();
                    $("header a.map-location-arrow").hide();
                    $(".search-result, .call-now").show();
                    $("footer").css({
                        position: "relative",
                        bottom: "0",
                        width: "100%",
                        left: "0"
                    });
                    $search_map.hide();
                } else {
                    history.back();
                }
            });

            // Show more appointments button
            $('.more-appointments-btn').on('click', function(e) {
                e.preventDefault();
                $(this).hide().parents('.doctor-appointments').find('.hide').removeClass('hide');
            });

            // Override event listeners for the map that don't apply on mobile.
            // For some reason, simply using off() doesnt work, so let's just block events
            $('.result').on('mouseenter mouseleave', function(e) { e.stopImmediatePropagation(); });
        });
    });
</script>

{if $CONFIG.GOOGLE_ANALYTICS_ACTIVATE}
    {include file="js/tracking/_google-remarketing-tag.tpl"}
{/if}
</body>
</html>
