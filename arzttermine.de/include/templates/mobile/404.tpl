<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="de">
<head>
<title>404 - Termin oder Arzt nicht gefunden</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="description" content="Leider konnte wir zu Ihre Anfrage keinen passenden Termin oder Arzt finden." />
<link href='{$this->getStaticUrl('img/favicon.png')}' rel='shortcut icon' type='image/x-icon' />
<link rel="stylesheet" type="text/css" media="all" href="/static/old-css/reset-min.css" />
<link rel="stylesheet" type="text/css" media="all" href="/static/old-css/base-min.css" />
<link rel="stylesheet" type="text/css" media="all" href="/static/old-css/fonts-min.css" />
<link rel="stylesheet" type="text/css" media="all" href="/static/old-css/main.css" />
<link rel="stylesheet" type="text/css" media="all" href="/static/old-css/404-site.css" />
<link href='//fonts.googleapis.com/css?family=Asap' rel='stylesheet' type='text/css'>

    <style>
        header a.btn {
            display:none !important;
        }
    </style>

</head>

<body>
{include file="js/tagmanager/tagmanager.tpl"}
	<div id="page-main">
		<div class="page-container">
			<div class="page-wrapper">
				<a id="logo" href="/"><img src="/static/img/logo.png" alt="Home" title="Home" height="60" width="216" /></a>
				<div id="clip-featured-in-white">Vorgestellt in</div>
	
			<div id="meta-page">
				<h1>404 - Notaufnahme</h1>
				<h2>Leider konnte wir keinen passenden Termin oder Arzt für Ihre Anfrage finden..</h2>
				<p>Bitte versuchen Sie es erneut oder wenden Sie sich an unser Service-Team:</p>
				<p><span>+</span> <a href="http://www.arzttermine.de/" title="zurück zur Startseite">Anfrage auf Arzttermine.de erneut starten</a></p>
				<p><span>+</span> <a href="http://www.arzttermine.de/kontakt">Kontakt zum Team</a></p>
				<p><span>+</span> <a href="tel:08002222133">Kostenfreie Kundenhotline 0800 / 2222133</a></p>
				<img id="error_ico" src="/static/img/error_ico.png" alt="Error-Image" width="130">
			</div>
		</div>
	</div>		
	<ul id="meta-nav">
		<li><a href="/datenschutz">Datenschutz</a></li> | <li><a href="/agb">AGB</a></li> | <li><a href="/impressum">Impressum</a></li>
	</ul>
</div>

</body>
</html>
