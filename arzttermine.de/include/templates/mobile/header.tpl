<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>{$this->getHeadTitle()}</title>
    <meta name="viewport" content="initial-scale=1,maximum-scale=1,width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="content-language" content="{$app->getLanguageCode()}" />
    <meta name="description" content="{$this->getMetaTag('description')}" />
    <meta name="keywords" content="{$this->getMetaTag('keywords')}" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="apple-touch-icon" href="{$this->getStaticUrl('mobile/img/logo_icon_57x57.png')}" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{$this->getStaticUrl('mobile/img/logo_icon_114x114.png')}" />
    <link rel="apple-mobile-web-app-title" content="Arzttermine.de" />

    <link href="/css/mobile.css" rel="stylesheet">
    <script src="/js/jquery.min.js"></script>

    <style type="text/css">
        .map-popup {
            background: #fff;
        }
    </style>
    {literal}
        <script type='text/javascript'>
            var AT = AT || {};

            AT.q=[];
            AT._defer=function(f){
                AT.q.push(f);
            };
        </script>
    {/literal}

    {$this->getHeadHtml()}

    <meta property="og:title" content="{$this->getHeadTitle()}" />
    <meta property="og:description" content="{$this->getMetaTag('description')}" />
    <meta property="og:site_name" content="{$CONFIG.SYSTEM_PRODUCT_TITLE}" />
    {if $this->getMetaTag('og:type')}
        <meta property="og:type" content="{$this->getMetaTag('og:type')}" />
    {else}
        <meta property="og:type" content="website" />
    {/if}
    <meta property="og:image" content="http://www.arzttermine.de/static/img/logo.png" />
    <meta property="og:url" content="{$app->getUrl()}" />
</head>
<body class="{$template_body_class}">
{include file="js/tagmanager/tagmanager.tpl"}

<header class="bar bar-nav">
    <a href="/" class="logo"></a>
    <a class="btn btn-link btn-nav pull-left back" href="#">
        <i class="fa fa-chevron-left"></i>
        Zurück
    </a>
    <a class="btn btn-link btn-nav pull-right map" href="#">
        <i class="fa fa-location-arrow"></i>
        Karte
    </a>
</header>

<div class="content">
