{strip}
{assign var="template_body_class" value=" homepage"}
{include file="mobile/header.tpl"}
{/strip}

<style>
    header a.btn {
        display:none !important;
    }
</style>

<form class="search-form" action="{$app->i18nTranslateUrl('/suche')}" name="edit" method="get" enctype="application/x-www-form-urlencoded">

	<p class="introduction">Wählen Sie Ihre Stadt und die Fachrichtung. Wir finden Ihren Arzt. Sie können direkt online einen Termin buchen.</p>

	<fieldset>
		<div class="search-form-row location">
			<span class="fa fa-home input-label"></span>
            <input type="text" id="form_location" placeholder="{'ui.search.place.label'|trans}" name="form[location]"{if $form.location != null} value="{$form.location}"{/if}>
		</div>

		<div class="search-form-row">
			<span class="fa fa-stethoscope input-label"></span>
            <select id="form_medical_specialty_id" name="form[medical_specialty_id]">
                <option value="" class="select-placeholder">Bitte Fachrichtung wählen</option>
                {html_options options=$MedicalSpecialtyOptions}
            </select>
		</div>

		<div class="search-form-row">
			<span class="fa fa-credit-card input-label"></span>
			<select id="form_insurance_id" name="form[insurance_id]">
				<option value="2" {if $form.insurance_id=='2'} selected="selected" {/if}>{$this->_('gesetzlich versichert')}</option>
				<option value="1" {if $form.insurance_id=='1'} selected="selected" {/if}>{$this->_('privat versichert')}</option>
			</select>
		</div>

		<button id="form_submit" type="submit" class="btn btn-primary btn-block">
			<span class="fa fa-search"></span>
			Termin finden
		</button>
	</fieldset>

</form>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyA-cGD6Wm_86W_BzBctDnMMD5j7iM0is1I"></script>
{include file="mobile/js/controller.validate_form.tpl"}
{include file="mobile/footer.tpl"}
