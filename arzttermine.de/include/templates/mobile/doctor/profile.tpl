{assign var="template_body_class" value=" profile-doctor"}
{assign var="hideGlobalPhonenumberInFooter" value=true}
{include file="mobile/header.tpl"}

<div id="search-map" class="search" style="display:none">
    <div id="googlemap"></div>
</div>

<div id="search-location" data-lat="{$search->getLat()}" data-lng="{$search->getLng()}" data-name="Suchstandort"></div>

<div class="doctor" id="doctor-profile-container">

	<ul class="table-view doctor-details card">
		<li class="table-view-cell media">
			<img class="media-object pull-left" src="{$user->getProfileAssetUrl($smarty.const.ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED)}" width="60">
			<div class="media-body">
				<h1>{$user->getName()}</h1>
				{$user->getMedicalSpecialityIdsText('<span class="badge badge-primary">','</span>')}
			</div>
		</li>
	</ul>
    
    {foreach from=$providers item="provider"}
		{assign var="location" value=$provider->getLocation()}
		<div class="doctor-address">
			<h3>{$location->getName()}</h3>
			<p>
				{$location->getStreet()}<br />
				{$location->getZip()} {$location->getCity()}
			</p>
            {if $location->hasPhoneVisible()}
			<p class="phone">
				<a href="tel:{$location->getPhoneVisible()}" class="btn btn-outlined btn-small">
					<span class="fa fa-phone"></span>
					{$location->getPhoneVisible()}
				</a>
			</p>
            {/if}
		</div>

        {assign var="appointmentsBlock" value=$provider->getAppointmentsBlock()}

        {if $location->getIntegrationId() != 8 && $location->getIntegrationId() != 9 && $appointmentsBlock.type!='no-available-appointments'}
        <div class="content-padded">
            <h3>Klicken Sie auf Ihren Wunschtermin</h3>
        </div>
        {/if}
        <div class="doctor-appointments">
            {if $location->getIntegrationId() != 8 && $location->getIntegrationId() != 9 && $appointmentsBlock.type!='no-available-appointments'}
            <ul class="my-new-list table-view">
                {if $appointmentsBlock.type == 'appointments'}
                    {* Only show the first 2 appointment days. *}
                    {assign var="dayCounter" value=0}
                    {assign var="appointmentDays" value=$appointmentsBlock.days}
                    {foreach from=$appointmentDays item="day" key="date"}
                        {if $day.appointments|count}
                            {assign var="dayCounter" value=$dayCounter+1}
                            <li class="table-view-divider{if $dayCounter > 2} hide{/if}">{$day.date_text}</li>
                            <li class="table-view-cell{if $dayCounter > 2} hide{/if}">
                                {assign var="appointments" value=$day.appointments}
                                {foreach from=$appointments item=appointment}
                                    <a class="appointment btn btn-primary btn-small" href="{$appointment.url}" rel="nofollow">{$appointment.time_text}</a>
                                {/foreach}
                            </li>
                        {/if}
                    {/foreach}

                    {* NEXT AVAILABLE APPOINTMENT ======================================================================================= *}
                {elseif $appointmentsBlock.type == 'next-available-appointment'}
                    {assign var="appointment" value=$appointmentsBlock.appointment}
                    {*
                      @todo: 14 is set because it's value for $date_days in other places like search.php or doctor_profile.php
                      consider using it as a config constant!
                    *}
                    {for $counter=0; $counter < 14; $counter++}
                        <div class="col">
                            <div class="col-header">
                                <span class="day"></span>
                                <span class="date"></span>
                            </div>
                        </div>
                    {/for}

                    {* CUSTOM MESSAGE =================================================================================================== *}
                {elseif $appointmentsBlock.type == 'custom-message'}
                    {for $counter=0; $counter < 14; $counter++}
                        <div class="col">
                            <div class="col-header">
                                <span class="day"></span>
                                <span class="date"></span>
                            </div>
                        </div>
                    {/for}

                    {* NO AVAILABLE APPOINTMENTS ======================================================================================== *}
                {elseif $appointmentsBlock.type == 'no-available-appointments'}
                    {for $counter=0; $counter < 14; $counter++}
                        <div class="col">
                            <div class="col-header">
                                <span class="day"></span>
                                <span class="date"></span>
                            </div>
                        </div>
                    {/for}

                    {* NO SELECTED INSURANCE ============================================================================================ *}
                {elseif $appointmentsBlock.type == 'no-selected-insurance'}
                    {for $counter=0; $counter < 14; $counter++}
                        <div class="col">
                            <div class="col-header">
                                <span class="day"></span>
                                <span class="date"></span>
                            </div>
                        </div>
                    {/for}

                    {* INTEGRATION 8 ==================================================================================================== *}
                {elseif $appointmentsBlock.type == 'integration_8'}
                    {for $counter=0; $counter < 14; $counter++}
                        <div class="col">
                            <div class="col-header">
                                <span class="day"></span>
                                <span class="date"></span>
                            </div>
                        </div>
                    {/for}

                {/if}
            </ul>
            {/if}
            <div class="content-padded doctor-more-appointments">
                {if $appointmentsBlock.type == 'no-available-appointments'}
                keine freien Termine verfügbar
                {else}
                <a
                    data-location_id="{$location->getId()}"
                    data-user_id="{$user->getId()}"
                    data-insurance_id="{$filter.insurance_id}"
                    class="btn btn-primary btn-outlined{if $location->getIntegrationId() == 8 || $location->getIntegrationId() == 9}" href="{$appointmentsBlock.url}"{else} more-appointments-btn"{/if}>{if $location->getIntegrationId() == 8 || $location->getIntegrationId() == 9}Jetzt Termin anfragen&nbsp;<span class="fa fa-chevron-right"></span>{else}Alle Termine anzeigen&nbsp;<span class="fa fa-chevron-down"></span>{/if}</a>
                {/if}
            </div>
        </div>
	{/foreach}

    <div class="content-padded">
        {$pay_message}
    </div>
    
	{if $user->getInfoText("docinfo_1")}
		<div class="content-padded">
			<h2>{$this->_('Philosophie')}</h2>
			{$user->getInfoText('docinfo_1')}
		</div>
	{/if}
	{if $user->getInfoText('docinfo_5')}
		<div class="content-padded">
			<h2>{$this->_('Spezialisierung')}</h2>
			{$user->getInfoText('docinfo_5')}
		</div>
	{/if}
	{if $user->getInfoText('docinfo_6')}
		<div class="content-padded">
			<h2>{$this->_('Behandlungsmöglichkeiten')}</h2>
			{$user->getInfoText('docinfo_6')}
		</div>
	{/if}
	{if $user->getInfoText('docinfo_2')}
		<div class="content-padded">
			<h2>{$this->_('Ausbildung')}</h2>
			{$user->getInfoText('docinfo_2')}
		</div>
	{/if}
	{if $user->getInfoText('docinfo_3')}
		<div class="content-padded">
			<h2>{$this->_('Sprachen')}</h2>
			{$user->getInfoText('docinfo_3')}
		</div>
	{/if}
	{if $user->getInfoText('docinfo_4')}
		<div class="content-padded">
			<h2>{$this->_('Auszeichnungen')}</h2>
			{$user->getInfoText('docinfo_4')}
		</div>
	{/if}

    {if $location->hasPhoneVisible()}
	<div class="content-padded content-centered">
		<p>Telefonisch buchen:</p>
		<a href="tel:{$location->getPhoneVisible()}" class="btn btn-outline">
			<span class="fa fa-phone"></span>
			{$location->getPhoneVisible()}
		</a>
	</div>
    {/if}

</div>

{include file="js/maps.js.tpl"}
{include file="mobile/footer.tpl"}