{strip}
    {assign var="template_body_class" value=" booking"}
    {include file="mobile/header.tpl"}
{/strip}

<style>
    header a.btn {
        display:none !important;
    }
</style>

<h1 style="font-size:25px;font-weight:300;font-family:Lato, Tahoma, sans-serif;color:#0090D6;border-bottom:1px solid #ccc;margin-bottom:1rem;">Sie haben den Terminvorschlag abgelehnt</h1>

<div class="orange-bar" style="background:#f5780a;padding:1rem;color:#fff;font-size:1.125rem;font-weight:500;margin-bottom:1rem;">
    Vorgeschlagener Termin: {calendar action="getWeekdayDateTimeText" datetime=$booking->getAppointmentStartAt() short=true}
</div>


<ul class="table-view card">
	<li class="table-view-cell media">
		<img class="media-object pull-left" src="{$user->getProfileAssetUrl($smarty.const.ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED)}" width="60">
		<div class="media-body">
			<h1>{$user->getName()}</h1>
			<p>{$location->getName()}<br />
				{$location->getAddress()}
			</p>
		</div>
	</li>
</ul>

<section id="booking-confirmation" style="background:#f5f5f5;padding:2rem;margin-bottom:1rem;">
	<section>
		<span style="font-size: 24px;margin-bottom:30px;color: grey;display:block;">Dies sind die n&auml;chsten Schritte...</span>
		<span>Wir werden Sie zeitnah kontaktieren um Ihnen einen weiteren Terminvorschlag zu unterbreiten.</span>
		<br /><br />
		Falls Sie nicht so lange warten m&ouml;chten, k&ouml;nnen Sie uns gerne eine Email an <a href="mailto:buchung@arzttermine.de">buchung@arzttermine.de</a> senden.
		<br /><br />
		Bitte teilen Sie uns dann folgende Informationen mit:
		<ul>
			<li>Postleitzahl</li>
			<li>zeitliche Verf&uuml;gbarkeit</li>
			<li>Arzt/Klinik (ob eine Alternativpraxis in Ordnung ist)</li>
		</ul>
	</section>
</section>


{include file="mobile/footer.tpl"}