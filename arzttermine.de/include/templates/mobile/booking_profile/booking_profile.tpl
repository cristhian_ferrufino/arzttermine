{strip}
    {assign var="template_body_class" value=" booking"}
    {include file="mobile/header.tpl"}
{/strip}

<style>
    header a.btn {
        display:none !important;
    }
</style>

<div class="content-padded">
	<h1>
		{if $booking->hasAppointmentEnquiryIntegration()}
			{$app->_('Ihre Anfrage war erfolgreich')}
		{else}
			{$app->_('Ihre Buchung war erfolgreich')}
		{/if}
	</h1>
	{if false == $booking->hasAppointmentEnquiryIntegration()}
	<p>
		{$app->_('Wir bestätigen in Kürze Ihren Wunschtermin.<br />Die Terminbuchung ist erst durch diese Bestätigung verbindlich.')}
	</p>
	{/if}
</div>

<ul class="table-view card">
	<li class="table-view-cell media">
		<img class="media-object pull-left" src="{$user->getProfileAssetUrl($smarty.const.ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED)}" width="60">
		<div class="media-body">
			<h1>{$user->getName()}</h1>
			<p>{$location->getName()}<br />
				{$location->getAddress()}
			</p>
			<p class="appointment">
				{if
					false == $booking->hasAppointmentEnquiryIntegration()
					&&
					$booking->hasValidAppointmentStartAt()
				}
				<span class="datetime">{calendar action="getWeekdayDateTimeText" datetime=$booking->getAppointmentStartAt() short=false}</span>
				{else}
					{if $user->hasContract()}
						{$cmsConfigManager->getVariableValue('mobile_docdir_pay_termin_message')}
					{else}
						{$cmsConfigManager->getVariableValue('mobile_docdir_nonpay_termin_message')}
					{/if}
				{/if}
			</p>
		</div>
	</li>
</ul>

{include file="booking_profile/_booking-conversion-tracking.tpl"}

{include file="mobile/footer.tpl"}