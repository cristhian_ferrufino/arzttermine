{strip}
{assign var="template_body_class" value=" search-results"}
{include file="mobile/header.tpl"}
{/strip}

{if !$search->getResultCount()|count}
<style>
    header a.map {
        display:none !important;
     }
</style>
{/if}

<div id="search-map" class="search">
    <div id="googlemap"></div>
</div>

<div class="search-result">
{if $search->getResultCount()|count}
    <div class="list">
        <div class="content-padded">
            <h1>{$search->getResultCount()} {$search->getMedicalSpecialty()->getName(false)} {$this->_('in')} {$form.location}{if isset($district)} ({$district->getName()}){/if} {$this->_('haben Termine für Sie.')}</h1>
        </div>
        <ul class="table-view">
        {foreach item=providerRow from=$results name=myforeach}
            {assign var="index" value=$smarty.foreach.myforeach.index}

            {assign var="user" value=$providerRow->getUser()}
            {assign var="location" value=$providerRow->getLocation()}
            {assign var="distance" value=floatval($providerRow->getDistance())}
            {assign var="appointmentsBlock" value=$providerRow->getAppointmentsBlock($date_start, $date_end, $filter.insurance_id, $filter.medical_specialty_id, $filter.treatmenttype_id)}


            {* map.js listens to the class .result so keep that class here! *}
            <li class="table-view-cell media result {if $smarty.foreach.first} first{/if}{if $smarty.foreach.last} last{/if}" data-user-id="{$user->getId()}" data-location-id="{$location->getId()}" data-index="{$index + $search->getStartIndex() + 1}">
                <a class="navigate-right" href="{$user->getUrl()}">
                    <div class="media-object pull-left">
                        <img src="{$user->getProfileAssetUrl($smarty.const.ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED)}" width="60">
                        <p class="distance">
                            <span class="badge">{$distance|number_format:2} km</span>
                        </p>
                    </div>
                    <div class="media-body">
                        <h2>{$user->getName()}</h2>
                        <p class="address">
                            {$location->getStreet()}<br />
                            {$location->getZip()} {$location->getCity()}
                        </p>
                        <span property="latitude" content="{$location->getLat()}"></span>
                        <span property="longitude" content="{$location->getLng()}"></span>
                        {if $location->getIntegrationId() == 8 || $location->getIntegrationId() == 9}
                            <div class="appointment">
                                <h3>{$this->_("Jetzt freie Termine bei diesem Arzt anfragen")}</h3>
                            </div>
                        {elseif $providerRow->getAppointments()|count}
                            {assign var='appointments' value=$providerRow->getAppointments()}
                            <div class="appointment">
                                <h3>{$this->_("Nächster Termin:")}</h3>
                                <p>{$appointments[0]->getStartTime()->format('d.m.')} um {$appointments[0]->getStartTime()->format('G:i')}</p>
                            </div>
                        {else}
                            <div class="appointment">
                                <h3>{$this->_("Keine Termine verfügbar")}</h3>
                            </div>
                        {/if}
                    </div>
                </a>
            </li>
        {/foreach}
        </ul>
        <div id="search-location" data-lat="{$search->getLat()}" data-lng="{$search->getLng()}" data-name="Suchstandort"></div>

        {* Pagination *}
        <div class="pagination">
            {paginate
            count=$search->getResultCount()
            ms=$search->getMedicalSpecialtyId()
            tt=$search->getTreatmentTypeId()
            insurance=$search->getInsuranceId()
            page=$search->getPage()
            per_page=$search->getPerPage()
            lat=$search->getLat()
            lng=$search->getLng()
            location=$form.location
            distance=$search->getRadius()
            district=$form.district_id
            }
        </div>
    </div>
{else}
    <div class="content-padded">
        <a href="/">
            {$this->_('Aktuell sind in dieser Region keine Ärzte bei uns angemeldet.')}
            {$this->_('Wir arbeiten daran unser Angebot in Kürze auch in Ihrer Stadt anzubieten.')}
            {$this->_('Klicken sie hier um die Suche zu ändern.')}
        </a>
    </div>
{/if}
</div>

<div class="map-popup-wrapper" id="map-popup" style="display:none">
    <div class="map-popup">
        <div class="photo">
            <img src="" alt="">
        </div>
        <h2 class="name"><a href=""></a></h2>
        <div class="street"></div>
        <div class="cityzip"></div>
        <div class="specialties"></div>
        <a role="button" href="">Termine anzeigen</a>
    </div>
</div>

{include file="js/maps.js.tpl"}
{include file="mobile/footer.tpl"}
