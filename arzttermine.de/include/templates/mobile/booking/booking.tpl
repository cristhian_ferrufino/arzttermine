{strip}
{assign var="template_body_class" value=" booking"}
{include file="mobile/header.tpl"}
{/strip}

<div class="booking">
    <h1 class="content-padded">{if $location->hasAppointmentEnquiryIntegration()}{$app->_('Terminanfrage')}{else}{$app->_('Termin')}{/if}</h1>

    <ul class="table-view card">
        <li class="table-view-cell media">
            <img class="media-object pull-left" src="{$user->getProfileAssetUrl($smarty.const.ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED)}" width="60">
            <div class="media-body">
                <h1>{$user->getName()}</h1>
                <p>{$location->getName()}<br />
                   {$location->getAddress()}
                </p>
                <p class="appointment">
                    {if $location->hasAppointmentEnquiryIntegration()}
                        {*}
                        {if $user->hasContract()}
                            {$cmsConfigManager->getVariableValue('mobile_docdir_pay_termin_message')}
                        {else}
                            {$cmsConfigManager->getVariableValue('mobile_docdir_nonpay_termin_message')}
                        {/if}
                        {/*}
                    {else}
                        <span class="datetime">{calendar action="getWeekdayDateTimeText" datetime=$form.appointment_start_at short=false}</span>
                    {/if}
                </p>
            </div>
        </li>
    </ul>

    {include file="mobile/booking/_form.tpl"}

</div>

{if $CONFIG.GOOGLE_ANALYTICS_ACTIVATE}
    {include file="js/tracking/_google-remarketing-tag.tpl"}
{/if}

{include file="mobile/footer.tpl"}
