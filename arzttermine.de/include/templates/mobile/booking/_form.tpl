<form id="booking-form" action="{$this->url_form_action}" name="edit" method="post" enctype="application/x-www-form-urlencoded" style="display:inline;">
	<fieldset>
		<div class="form-group radio {if in_array('gender', $error_messages)} has-error{/if}">
			<label for="form_gender_1"><input id="form_gender_1" type="radio" name="form[gender]" value="1" class="validate[required]"{if $form.gender=='1'} checked="checked"{/if}>Frau</label>
			<label for="form_gender_0"><input id="form_gender_0" type="radio" name="form[gender]" value="0" class="validate[required]"{if $form.gender=='0'} checked="checked"{/if}>Herr</label>
		</div>

		<div class="form-group {if in_array('first_name', $error_messages)} has-error{/if}">
			<input type="text" placeholder="{$app->_('Vorname')}" id="form_first_name" name="form[first_name]" class="input_text" value="{$form.first_name}" />
		</div>

		<div class="form-group {if in_array('last_name', $error_messages)} has-error{/if}">
			<input type="text" placeholder="{$app->_('Nachname')}" id="form_last_name" name="form[last_name]" class="input_text{$form.last_name__form_input_class}" value="{$form.last_name}" />
		</div>

		<div class="form-group {if in_array('email', $error_messages)} has-error{/if}">
			<input type="email" placeholder="{$app->_('E-Mail')}" id="form_email" name="form[email]" class="input_text{$form.email__form_input_class}" value="{$form.email}" />
		</div>

		<div class="form-group {if in_array('phone', $error_messages)} has-error{/if}">
			<input type="tel" placeholder="{$app->_('Mobil')}" id="form_phone" name="form[phone]" class="input_text{$form.phone__form_input_class}" value="{$form.phone}" />
		</div>

		<div style="margin-bottom:50px;" class="form-group {if in_array('contact_preference', $error_messages)} has-error{/if}" data-error-message="Bitte wählen Sie mindestens eine Kontaktart.">
			<label>{$app->_('Falls wir Fragen zu Ihrem Termin haben, auf welchem Weg k&ouml;nnen wir Sie kontaktieren ?')}</label><br />

			<label for="contact_preference_phone" class="form-check-inline"> Telefon
				<input type="checkbox" class="form-check-input" id="contact_preference_phone" name="form[contact_preference][]" value="phone"{if in_array('phone', $form.contact_preference)} checked{/if} />
			</label>

			<label for="contact_preference_sms" class="form-check-inline"> SMS
				<input type="checkbox" class="form-check-input" id="contact_preference_sms" name="form[contact_preference][]" value="sms"{if in_array('sms', $form.contact_preference)} checked{/if} />
			</label>

			<label for="contact_preference_email" class="form-check-inline"> Email
				<input type="checkbox" class="form-check-input" id="contact_preference_email" name="form[contact_preference][]" value="email"{if in_array('email', $form.contact_preference)} checked{/if} />
			</label>

			<label for="contact_preference_whatsapp" class="form-check-inline"> WhatsApp
				<input type="checkbox" class="form-check-input" id="contact_preference_whatsapp" name="form[contact_preference][]" value="whatsapp"{if in_array('whatsapp', $form.contact_preference)} checked{/if} />
			</label>
		</div>

		<div class="radio radio-block form-group {if in_array('insurance_id', $error_messages)} has-error{/if}">
			<label>{$app->_('Versicherungsart')}</label>
			<label for="form_insurance_pkv"><input id="form_insurance_pkv" type="radio" name="form[insurance_id]" value="1" class="validate[required]"{if $form.insurance_id=='1'} checked="checked"{/if}{if !empty($available_insurance_ids) && !in_array(1,$available_insurance_ids)} disabled="disabled"{/if}>{$app->_('privat versichert')}</label>
			<label for="form_insurance_gkv"><input id="form_insurance_gkv" type="radio" name="form[insurance_id]" value="2" class="validate[required]"{if $form.insurance_id=='2'} checked="checked"{/if}{if !empty($available_insurance_ids) && !in_array(2,$available_insurance_ids)} disabled="disabled"{/if}>{$app->_('gesetzlich versichert')}</label>
		</div>

		<div class="radio form-group {if in_array('returning_visitor', $error_messages)} has-error{/if}">
			<label for="known_patient">{$app->_('Waren Sie bereits Patient/in bei')} <span class="doctor-name">{$user->getName()}</span></label>
			<label><input id="form_returning_visitor_1" type="radio" name="form[returning_visitor]" value="1" class="validate[required]"{if $form.returning_visitor=='1'} checked="checked"{/if}>{$app->_('Ja')}</label>
			<label><input id="form_returning_visitor_0" type="radio" name="form[returning_visitor]" value="0" class="validate[required] radio input_radio"{if $form.returning_visitor=='0'} checked="checked"{/if}>{$app->_('Nein')}</label>
		</div>

		<div>
			<label>{$app->_('Behandlungsgrund')} (<span class="label_optional">{$app->_('freiwillig')})</span></label>
			{html_options class="input_select" id="form_treatmenttype_id" name="form[treatmenttype_id]" options=$treatmenttypes selected=$form.treatmenttype_id}
		</div>

		<div class="form-element{if in_array('referral_code', $error_messages)} has-error{/if}">
			<label>{$app->_('Empfehlungscode')} <span class="optional-field">(optional)</span></label>
			<input type="text" name="form[referral_code]" value="{if isset($form.referral_code)}{$form.referral_code}{/if}">
		</div>
		<div class="checkbox{if in_array('form_referral_code_confirmation', $error_messages)} has-error{/if}" data-error-message="Bitte bestätigen Sie die Gutschein Einwilligung." id="referral-code-confirmation" style="display:none;">
			<input {if !isset($form.referral_code)}disabled{/if} type="hidden" name="form[referral_code_confirmation]" value="0">
			<div>
				<label for="form_referral_code_confirmation">
					<input id="form_referral_code_confirmation" type="checkbox" name="form[referral_code_confirmation]" value="1" {if $form.referral_code_confirmation=='1'} checked="checked"{/if}>

					Ich akzeptiere die Teilnahmebedingungen der <a href="/angebote-informationen">Gutscheinaktion</a>
				</label>
			</div>
		</div>

		<div class="checkbox{if in_array('agb', $error_messages)} has-error{/if}">
			<label for="form_agb">
				<input type="checkbox" id="form_agb" name="form[agb]" value="1" class="input_checkbox{$form.agb__form_input_class}"{if $form.agb=='1'} checked="checked"{/if} />
				{$booking_agb_confirmation_text}
			</label>
		</div>

		<div class="checkbox">
			<div>
				<label for="form_newsletter">
					<input type="checkbox" id="form_newsletter" name="form[newsletter_subscription]" value="1" />
					{$booking_newsletter_text}
				</label>
			</div>
		</div>

		<input type="hidden" name="form[location_id]" value="{$form.location_id}">
		<input type="hidden" name="form[user_id]" value="{$form.user_id}">
		<input type="hidden" name="form[medical_specialty_id]" value="{$form.medical_specialty_id}">
		<input type="hidden" name="form[start_at_id]" value="{$form.start_at_id}">
		<input type="hidden" name="form[end_at_id]" value="{$form.end_at_id}">
		<input type="hidden" name="form[form_once]" value="{$form.form_once}">

		<button name="ok" type="submit" class="btn btn-primary btn-block" id="ok" tabindex="4" accesskey="o" value="{$app->_('Termin buchen')}">
			<span class="icon-success"></span>
			{$app->_('Termin buchen')}
		</button>
	</fieldset>
</form>

{literal}
<script type="text/javascript">
$(document).ready(function() {
{/literal}
	{if isset($tt_insurances)}
		var tt_insurances = {$tt_insurances};
		$("#form_treatmenttype_id").change(function(){
			var privateInsuranceDisable = false;
			var publicInsuranceDisable = false;
			if(tt_insurances[$(this).val()]==="private"){
				publicInsuranceDisable = true;  
			}
			if(tt_insurances[$(this).val()]==="public"){
				privateInsuranceDisable = true;  
			}
			$("#form_insurance_pkv").attr("disabled",privateInsuranceDisable);
			$("#form_insurance_gkv").attr("disabled",publicInsuranceDisable);
		});
		$("#form_treatmenttype_id").change();
	{/if}

    $("input[name='form[referral_code]']").on('change keyup paste', function() {
        if(this.value != '') {
            $("#referral-code-confirmation").show();
            $('#referral-code-confirmation').attr('required', 'required');
        } else {
            $("#referral-code-confirmation").hide();
            $('#referral-code-confirmation').removeAttr('required');
		}
    });
	{literal}
});
</script>
{/literal}
