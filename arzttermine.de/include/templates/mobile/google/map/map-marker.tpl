<script>
var googleMap = {};
var googleMapOverlay = {};
var googleMapMarkers = [];
var googleMapBounds = {};
var googleMapBoundsTotal = 0;
var googleMapHoverOutTimeoutId = null;

var allMarkerLngLat= [];
var totalLat = 0;
var totalLng = 0;

function initGoogleMap(options) {
    {if $markers|@count<1 && $center_lat != 0 && $center_lng != 0}
    var googleMapCenter = new google.maps.LatLng({$center_lat}, {$center_lng});
    {/if}
    var mapOptions = {
        zoom: 14,
        {if $markers|@count<1 && $center_lat != 0 && $center_lng != 0}
        center: googleMapCenter,
        {/if}
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        scrollwheel: false,
        streetViewControl: false
    };
    
    options = options || {};
    
    $.extend(mapOptions, options);

    googleMap = new google.maps.Map(document.getElementById("search-map"), mapOptions);
    
    {* $markers may not be used anymore. @todo: Seek and Destroy *}
    {if $markers|@count>0}
    {foreach from=array_reverse($markers) item='marker'}
    createGoogleMapMarker({$marker.index}, {$marker.lat}, {$marker.lng}, "{$marker.googlemap_popup_id}", "{$marker.url}", "{$marker.imgsrc}", "{$marker.name}", "{$marker.street}", "{$marker.cityzip}");
    {/foreach}
    {/if}
    
    {if $providerRows}
    {foreach from=$providerRows item=providerRow name=providerLoop}
    {assign var="location" value=$providerRow->getLocation()}
    createGoogleMapMarker({$smarty.foreach.providerLoop.index}, {$location->getLat()}, {$location->getLng()}, "googlemap_popup_provider", "{$location->getUrlWithFilter($filter)}", "{$location->getProfileAssetUrl()}", "{$location->getName()}", "{$location->getStreet()}", "{$location->getCityZip()}");
    {/foreach}
    {/if}
    
    {if $markers|@count==1 || ($center_lat == 0 && $center_lng == 0)}
    googleMap.setCenter(googleMapBounds.getCenter());
    {/if}

    var markersCount = allMarkerLngLat.length;
    
    if (markersCount > 1) {
        var gmb = new google.maps.LatLngBounds();
        var midlLng = totalLng/markersCount;
        var midlLat = totalLat/markersCount;
        var midPointDistanceFromMidPoint = 0;

        for (var i=0;i<markersCount;i++) {
            var point = allMarkerLngLat[i];
            var pointDistanceFromMidPoint = getPointDistance(point[0], point[1],midlLng,midlLat);
            midPointDistanceFromMidPoint += pointDistanceFromMidPoint;
            point.push(pointDistanceFromMidPoint);
        }

        midPointDistanceFromMidPoint /= markersCount;
        for (var i=0;i<markersCount;i++) {
            var point = allMarkerLngLat[i];
            var pointDistanceFromMidPoint = point[2];
            if (pointDistanceFromMidPoint <= midPointDistanceFromMidPoint * 1.5)
            {
                gmb.extend(new google.maps.LatLng(point[1], point[0]));
            }
        }
        googleMap.setCenter(gmb.getCenter());
        googleMap.fitBounds(gmb);
        var listener = google.maps.event.addListener(googleMap, "idle", function() {
            var currentZoom = googleMap.getZoom();
            {if $zoom_twice}googleMap.setZoom(currentZoom+{$zoom_twice});{/if}
            if (googleMap.getZoom() >13) {
                googleMap.setZoom(13);
            }
            google.maps.event.removeListener(listener);
        });
    } else {
        if (googleMapBoundsTotal > 1) {
            googleMap.fitBounds(googleMapBounds);
        }
    }

    google.maps.event.addListener(googleMap, 'dragstart', googleMapHidePopup);
    google.maps.event.addListener(googleMap, 'zoom_changed', googleMapHidePopup);

    /* create the overlay to be accessed by jquery */
    function googleMapOverlayFunction(options) {
        this.setValues(options);
        var div = this.div_= document.createElement('div');
        div.className = "overlay";
    }
    
    googleMapOverlayFunction.prototype = new google.maps.OverlayView();
    googleMapOverlayFunction.prototype.onAdd = function() {
        var pane = this.getPanes().overlayLayer;
        pane.appendChild(this.div_);
    };
    googleMapOverlayFunction.prototype.onRemove = function() {
        this.div_.parentNode.removeChild(this.div_);
    };
    googleMapOverlayFunction.prototype.draw = function() {};
    googleMapOverlay = new googleMapOverlayFunction({ 'map': googleMap });
}

/*function that returns a marker to add in the map parameter
 at the lat, lng position with index index*/
function createProviderMarker(index, lat, lng, map) {
    var iconPin = createGoogleMapIcon(index, false);
    var iconPin_s = new google.maps.MarkerImage(
            "//maps.gstatic.com/mapfiles/shadow50.png",
            new google.maps.Size(37, 34),
            new google.maps.Point(0, 0),
            new google.maps.Point(9, 34)
    );
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat, lng),
        map: map,
        icon: iconPin,
        shadow: iconPin_s
    });
    
    return marker;
}

function createGoogleMapMarker(index, lat, lng, googlemap_popup_id, url, imgsrc, name, street, cityzip) {
    var iconPin = createGoogleMapIcon(index, false);
    var iconPin_h = createGoogleMapIcon(index, true);
    
    if (index == 'dot') {
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(lat, lng),
            map: googleMap,
            icon: iconPin
        });
    } else {
        var iconPin_s = new google.maps.MarkerImage(
                "//maps.gstatic.com/mapfiles/shadow50.png",
                new google.maps.Size(37, 34),
                new google.maps.Point(0, 0),
                new google.maps.Point(9, 34)
        );
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(lat, lng),
            map: googleMap,
            icon: iconPin,
            shadow: iconPin_s
        });
    }
    
    googleMapMarkers.push(marker);

    google.maps.event.addDomListener(marker, 'mouseover', function(event) {
        marker.setIcon(iconPin_h);
        googleMapUpdatePopup(googlemap_popup_id, url, imgsrc, name, street, cityzip, index);
        googleMapHoverEvent(event, marker, iconPin);
    });

    google.maps.event.addDomListener(marker, 'mouseout', function(event) {
        marker.setIcon(iconPin);

        googleMapHoverOutEvent();
    });

    if (url != '') {
        google.maps.event.addListener(marker, 'click', function(event) {
            window.location = url;
        });
    }
    
    if (url != '') {
        totalLat+=lat;
        totalLng+=lng;
        allMarkerLngLat.push([lng, lat]);
    }

    googleMapBounds.extend(new google.maps.LatLng(lat, lng));
    googleMapBoundsTotal++;
}

function getPointDistance(lon1, lat1,lon2, lat2) {
    var R = 6371; // km
    var dLat = convertAngleToRadians(lat2-lat1);
    var dLon = convertAngleToRadians(lon2-lon1);
    var lat1 = convertAngleToRadians(lat1);
    var lat2 = convertAngleToRadians(lat2);
    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    return d = R * c;
}

function convertAngleToRadians(angle) {
    return Math.PI/180 * angle;
}

function createGoogleMapIcon(index, highlighted) {
    var size;
    var location;
    var anchor;

    if (index == 'dot') {
        size = new google.maps.Size(10, 11);
        location = new google.maps.Point(4623, (highlighted ? 34 : 0));
        anchor = new google.maps.Point(5, 5);
    } else {
        size = new google.maps.Size(23, 34);
        location = new google.maps.Point(23*index, (highlighted ? 34 : 0));
        anchor = new google.maps.Point(9, 34);
    }

    return new google.maps.MarkerImage(
            '/static/img/googlemaps-sprite-pins.png?version=2',
            size, location, anchor, new google.maps.Size(4633, 68));
}

function googleMapHidePopup() {
    $('#googlemap_popup').hide();
    if (googleMapHoverOutTimeoutId != null) {
        clearTimeout(googleMapHoverOutTimeoutId);
        googleMapHoverOutTimeoutId = null;
    }
}

function googleMapHoverOutEvent() {
    if (googleMapHoverOutTimeoutId == null) {
        googleMapHoverOutTimeoutId = setTimeout(googleMapHidePopup, 800);
    }
    $(".results-container .provider-row").removeClass("hovered-in-map");
}

function googleMapHoverEvent(event, marker, icon) {
    var markerLoc = googleMapOverlay.getProjection().fromLatLngToContainerPixel(event.latLng);
    var $googleMapPopup = $('#googlemap_popup');
    var $map = $('#search-map');
    $googleMapPopup.show();
    var mapOffset = $map.offset();
    var popupTop = markerLoc.y + mapOffset.top;
    if (markerLoc.y > $map.height() - $googleMapPopup.height() - 10) {
        popupTop = popupTop - $googleMapPopup.height() - 50;
    }
    var popupLeft = mapOffset.left + Math.min($map.width() - $googleMapPopup.width(), markerLoc.x - 12);
    $googleMapPopup.offset({
        top: popupTop,
        left: popupLeft
    });
    $googleMapPopup.mouseout(googleMapHoverOutEvent);
    $googleMapPopup.mouseover(function () {
        if (googleMapHoverOutTimeoutId != null) {
            clearTimeout(googleMapHoverOutTimeoutId);
            googleMapHoverOutTimeoutId = null;
        }
    });
    if (googleMapHoverOutTimeoutId != null) {
        clearTimeout(googleMapHoverOutTimeoutId);
        googleMapHoverOutTimeoutId = null;
    }

    $(".results-container .provider-row").eq($googleMapPopup.attr("data-result-index")-1).addClass("hovered-in-map");
}

function googleMapUpdatePopup(popupcontentid, href, imgsrc, name, street, cityzip, index) {
    var $popup = $("#googlemap_popup");
    var popupcontent = $('#' + popupcontentid).html();
    $popup.html(popupcontent);
    $popup.attr("data-result-index", index);
    $popup.find("a").attr("href", href);
    $popup.find(".gfx img").attr("src", imgsrc);
    $popup.find(".name a").html(name);
    $popup.find(".street").html(street);
    $popup.find(".cityzip").html(cityzip);
}

$(function() {
    googleMapBounds = new google.maps.LatLngBounds();
    google.maps.event.addDomListener(window, 'load', initGoogleMap);
});

</script>

<div id="googlemap_popup" class="googlemap_popup"></div>

<div id="googlemap_popup_provider" style="display:none;">
    <div class="provider intro">
        <div class="gfx"><a href="" rel="nofollow"><img src="" alt="" /></a></div>
        <div class="info">
            <div class="name"><a href="" rel="nofollow"></a></div>
            <div class="address street"></div>
            <div class="address cityzip"></div>
            <div class="booking-link"><a href="" class="button normal btn_soft_green" rel="nofollow">online buchen</a></div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
</div>

<div id="googlemap_popup_self" style="display:none;">
    <div class="provider intro">
        <div class="info">
            <div class="name">Suchstandort</div>
            <div class="address street"></div>
            <div class="address cityzip"></div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
</div>
