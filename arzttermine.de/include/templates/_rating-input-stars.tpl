<div class="star-rating-wrapper">
  <input type="radio" name="{$name}" value="5" id="{$name}-5"><label for="{$name}-5">5</label
  ><input type="radio" name="{$name}" value="4" id="{$name}-4"><label for="{$name}-4">4</label
  ><input type="radio" name="{$name}" value="3" id="{$name}-3"><label for="{$name}-3">3</label
  ><input type="radio" name="{$name}" value="2" id="{$name}-2"><label for="{$name}-2">2</label
  ><input type="radio" name="{$name}" value="1" id="{$name}-1"><label for="{$name}-1">1</label>
</div>
