{strip}
    {assign var="template_body_class" value="404"}
    {include file="header.tpl"}
{/strip}

<div class="container">
    <main>
        <section id="content">
            <div id="message">
                <h1>404 - Notaufnahme</h1>
                <h2>Leider konnten wir keinen passenden Termin oder Arzt für Ihre Anfrage finden.</h2>
                <p>Bitte versuchen Sie es erneut oder wenden Sie sich an unser Service-Team:</p>
                <p><span>+</span> <a href="http://www.arzttermine.de/" title="zurück zur Startseite">Anfrage auf Arzttermine.de erneut starten</a></p>
                <p><span>+</span> <a href="https://www.arzttermine.de/kontakt">Kontakt zum Service-Team</a></p>
                <p><span>+</span> <a href="tel:08002222133">Kostenfreie Kundenhotline 0800 / 2222133</a></p>
            </div>
            <img id="error_ico" src="/static/img/error_ico.png" alt="Error-Image" width="130">
        </section>
    </main>
</div>

{include file="footer.tpl"}
