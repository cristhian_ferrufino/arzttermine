<link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.min.css" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.theme.default.min.css" rel="stylesheet" type="text/css">

<section id="doctors_magazin">
    <h2>Ärzte aufgepasst!</h2>
    <p align="center" style="margin-bottom: 1.5rem">Hier erhalten Sie Informationen rund um Ihren Service. Von der Patientenbindung über die Praxisoptimierung bis hin zu hilfreichen Tools für Ihre Website.</p>

    <div class="owl-carousel owl-theme" style="margin-bottom: 1.5rem">
        <div class="item">
            <a class="title_carousel" href="/aerzte-magazin/wie-kann-ich-die-absprungrate-auf-meiner-praxiswebsite-reduzieren"  title="Wie kann ich die Absprungrate auf meiner Praxiswebsite reduzieren?">
                <figure><img src="/media/Wie-kann-ich-die-Absprungrate-auf-meiner-Praxiswebsite-reduzieren.jpg"></figure>
                <h5 class="title_carousel">Wie kann ich die Absprungrate auf meiner Praxiswebsite reduzieren?</h5>
            </a>
        </div>
        <div class="item">
            <a class="title_carousel" href="/aerzte-magazin/der-wahre-wert-eines-neuen-patienten"  title="Der wahre Wert eines neuen Patienten">
                <figure><img src="/media/der_wahre_wert.jpg"></figure>
                <h5 class="title_carousel">Der wahre Wert eines neuen Patienten</h5>
            </a>
        </div>
        <div class="item">
            <a class="title_carousel" href="/aerzte-magazin/zu-viele-terminabsagen-wie-bringen-sie-ihre-patienten-dazu-termine-einzuhalten/"  title="Zu viele Terminabsagen - Wie bringen Sie Ihre Patienten dazu, Termine einzuhalten?">
                <figure><img src="/media/zu-viele-terminabsagen-index.jpg"></figure>
                <h5 class="title_carousel">Zu viele Terminabsagen - Wie bringen Sie Ihre Patienten dazu, Termine einzuhalten?</h5>
            </a>
        </div>
        <div class="item">
            <a class="title_carousel" href="/aerzte-magazin/wie-sollte-ich-das-e-mail-marketing-fuer-meine-praxis-strukturieren"  title="Wie sollte ich das E-Mail Marketing für meine Praxis strukturieren?">
                <figure><img src="/media/Email_Marketing.jpg"></figure>
                <h5 class="title_carousel">Wie sollte ich das E-Mail Marketing für meine Praxis strukturieren?</h5>
            </a>
        </div>
        <div class="item">
          <a class="title_carousel" href="/aerzte-magazin/gibt-es-ein-einfaches-termin-buchungstool-fuer-meine-praxiswebsite/"  title="Gibt es ein einfaches Termin-Buchungstool für meine Praxiswebsite?">

              <figure><img src="/media/Termin-Buchungstool.jpg"></figure>
              <div class="transbox">
              <h5 class="title_carousel">Gibt es ein einfaches Termin-Buchungstool für meine Praxiswebsite?</h5>
              </div>
          </a>

        </div>

        <div class="item">
            <a class="title_carousel" href="/aerzte-magazin/kann-ich-ueber-externe-portale-fuer-meine-praxis-werben"  title="Kann ich über externe Portale für meine Praxis werben?">
                <figure><img src="/media/Externe_Portale.jpg"></figure>
                <h5 class="title_carousel">Kann ich über externe Portale für meine Praxis werben?</h5>
            </a>

        </div>
        <div class="item">
            <a class="title_carousel" href="/aerzte-magazin/wie-kann-ich-meine-online-reputation-schuetzen-lassen/"  title="Wie kann ich meine Online Reputation schützen lassen?">
                <figure><img src="/media/Online_Reputation.jpg"></figure>
                <h5 class="title_carousel">Wie kann ich meine Online Reputation schützen lassen?e</h5>
            </a>

        </div>
        <div class="item">
            <a class="title_carousel" href="/aerzte-magazin/wie-kann-ich-meine-praxiswebsite-so-optimieren-dass-mich-mehr-patienten-finden"  title="Wie kann ich meine Praxiswebsite so optimieren, dass mich mehr Patienten finden?">
                <figure><img src="/media/Optimierung_Website.jpg"></figure>
                <h5 class="title_carousel">Wie kann ich meine Praxiswebsite so optimieren, dass mich mehr Patienten finden?</h5>
            </a>

        </div>
        <div class="item">
            <a class="title_carousel" href="/aerzte-magazin/wie-kann-ich-meine-jameda-bewertungen-entfernen/"  title="Wie kann ich meine Jameda Bewertungen entfernen?">
                <figure><img src="/media/JamedaBewertungen.jpg"></figure>
                <h5 class="title_carousel">Wie kann ich meine Jameda Bewertungen entfernen?</h5>
            </a>
        </div>
        <div class="item">
            <a class="title_carousel" href="/aerzte-magazin/warum-immer-mehr-aerzte-auf-telefondienstleister-setzen/"  title="Warum immer mehr Ärzte auf Telefondienstleister setzen">
                <figure><img src="/media/Telefondienstleister.jpg"></figure>
                <h5 class="title_carousel">Warum immer mehr Ärzte auf Telefondienstleister setzen</h5>
            </a>
        </div>

    </div>
</section>


<script type="text/javascript">

      $('.owl-carousel').owlCarousel({
          loop: true,
          margin: 10,
          nav: false,
          autoplay:true,
          autoplayTimeout:2000,
          autoplayHoverPause:true,
          responsiveClass:true,
          responsive: {
              0: {
                  items: 1,
                  loop:true
              },
              600: {
                  items: 3,
                  loop:true
              },
              1000: {
                  items: 5,
                  loop:true
              }
          }

      });


</script>
