<section id="press_outlets" class="">
  <div class="row">
    <div class="col-sm offset-md-1 press">
     <cite>
       <a href="https://www.netzsieger.de/p/arzttermine-de" target="_blank"  title="Netzsieger">
        <img src="../static/images/assets/press/netzsieger.png">
       </a>
     </cite>
      <blockquote class="press" cite="https://www.netzsieger.de/p/arzttermine-de">Ideale Plattform, um schnell und einfach einen Arzt zu finden und einen Termin zu vereinbaren...</blockquote>
    </div>
    <div class="col-sm press">
      <cite>
        <a href="https://www.welt.de/print/welt_kompakt/webwelt/article130375378/Krankenkasse-und-Start-Up-vermitteln-Aerzte-per-Internet.html" target="_blank"  title="Welt N24">
          <img src="../static/images/assets/press/welt_24.png">
        </a>
      </cite>
      <blockquote class="press" cite="https://www.welt.de/print/welt_kompakt/webwelt/article130375378/Krankenkasse-und-Start-Up-vermitteln-Aerzte-per-Internet.html">Krankenkasse (DAK) bietet ihren Versicherten die Onlinebuchung von Arztterminen an...</blockquote>
    </div>
  </div>

  <div class="row" >
    <div class="col-sm offset-md-1 press">
      <cite>
        <a href="https://www.deutsche-startups.de/2016/11/03/mueller-medien-arzttermine-2" target="_blank"  title="Deutsche Startups">
          <img src="../static/images/assets/press/deutsche_startups.png">
        </a>
      </cite>
      <blockquote class="press" cite="https://www.deutsche-startups.de/2016/11/03/mueller-medien-arzttermine-2">Der bekannte Telefonbuchverlag Müller Medien übernimmt [...] mehrheitlich die Berliner Terminplattform Arzttermine.de...</blockquote>
    </div>
    <div class="col-sm press">
      <cite>
        <a href="http://www.arzttermine.de/media/20140909_Tagesspiegel-Arzttermine.pdf" target="_blank"  title="Tagesspiegel">
          <img src="../static/images/assets/press/tagesspiegel.png">
        </a>
      </cite>
      <blockquote class="press" cite="http://www.arzttermine.de/media/20140909_Tagesspiegel-Arzttermine.pdf">INTERNETMEDIZIN ist ein Zukunftsmarkt. Unbemerkt von vielen, wächst er seit Jahren...</blockquote>
    </div>
  </div>
</section>
