{if $quote}
<section id="quotes" class="hidden-sm-down">
	<div class="quote">
		<h2>Das sagen Patienten über unsere Praxen</h2>
			<p class="quote-text">
				{$quote.content}
				<span class="quote-doc-name">&nbsp;&mdash;&nbsp; {$quote.user}</span>
			</p>
	</div>
</section>
{/if}