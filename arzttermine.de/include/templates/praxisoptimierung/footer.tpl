<link href="../../static/css/common.min.css" rel="stylesheet" type="text/css" >
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap-grid.min.css" rel="stylesheet" type="text/css">
{include file="footer.tpl"}

    <script type="text/javascript">
        function trackGoogleConversion(google_conversion_id,google_conversion_label) {
            var image = new Image(1,1);
            image.src = "https://www.googleadservices.com/pagead/conversion/"+google_conversion_id+"/?label="+google_conversion_label+"&script=0";
        }
    </script>

    <script type="text/javascript">
        $(function() {
            function validateForm() {
                var isValid = true;

                $('#form').find('input.is-required').removeClass('has-error').each(function () {
                    $(this).parent().removeClass('has-error');
                    if (!$(this).val()) {
                        $(this).parent().addClass('has-error');
                        isValid = false;
                    }
                });
                return isValid;
            }

            $("#submit").on('click', function( event) {
                event.preventDefault();
                if (validateForm()) {
                    $.ajax({
                        type: "POST",
                        url: "/praxisoptimierung/kontakt",
                        data: $("#form form").serialize()
                    })
                        .done(function( result ) {
                            if (result.data == 'ok') {
                                $("#submit").hide('fast');
                                $("#submit-response").hide('fast').html("<h4 style='color: green;'>Vielen Dank f&uuml;r Ihre Anfrage, wir werden uns in K&uuml;rze bei Ihnen melden!</h4>").show('fast');
{if $CONFIG.GOOGLE_ANALYTICS_ACTIVATE}
                                trackGoogleConversion(990044011, 'o4feCI_OklYQ676L2AM');
{/if}
                            } else {
                                $("#submit-response").hide('fast').html("<h4 style='color: red;'>Ein Fehler ist aufgetreten!</h4>").show('fast');
                            }
                        });
                }
                // dont send the form again
                return false;
            });
        });
    </script>

    <script type="text/javascript">
        var leady_track_key="y39FXY28qIbZZ8Ib";
        var leady_track_server=document.location.protocol+"//t.leady.com/";
        (function(){
            var l=document.createElement("script");l.type="text/javascript";l.async=true;
            l.src=leady_track_server+leady_track_key+"/L.js";
            var s=document.getElementsByTagName("script")[0];s.parentNode.insertBefore(l,s);
        })();
    </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
<script src="https://use.fontawesome.com/f7e3e96c18.js"></script>

</body>
</html>
