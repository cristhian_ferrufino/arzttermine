{assign var="pageId" value="page-atweb"}
{assign var="head_title" value="AtWeb - Websiteoptimierung"}
{assign var="head_meta_description" value="Ihre Patienten erwarten die gleiche Qualität von Ihrer Website wie von Ihren Dienstleistungen. Wir bieten Ihnen eine suchmaschinenoptimierte Website nach Ihren Wünschen."}
{assign var="head_meta_keywords" value=""}
{include file="praxisoptimierung/header.tpl"}
<link rel="stylesheet" type="text/css" href="/lp/atsearch/style_praxisoptimierung.css">

<div class="container at_marginTop">
    <div class="row">
        <div class="col-sm">
            <h1 class="text-logo logo"><span>At</span>Web</h1>
        </div>
        <div class="col">
            <img class="img-responsive" src="/static/img/arzttermine.svg" alt="Arzttermine.de" style="float: right; width: 20rem">
        </div>
    </div>
</div>
<br/>
<div class="container">
    <div class="row justify-content-center">
        <div class="col"><h1 class="title" align="center">Zeigen Sie sich von Ihrer besten Seite im Internet </h1></div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-sm">
            <div class="row margin_section1" style="margin-bottom: 1rem">
                <div class="col">
                    <img class="icons_atwidget" src="/static/img/praxisoptimierung/atwidget/warning.png">
                    <p class="margin_section1">Wenn Patienten auf Ihre Seite klicken, entscheiden Sie sich innerhalb von Sekunden, ob sie Ihnen gefällt und ob Sie in die Praxis gehen würden.
                    </p>
                </div>
            </div>
            <div class="row margin_section1" style="margin-bottom: 1rem">
                <div class="col">
                    <img class="icons_atwidget" src="/static/img/praxisoptimierung/atwidget/tick-box.png">
                    <p class="margin_section1">Mit <span class="text-logo"><span>At</span>Web</span> bieten wir Ihnen eine moderne, mobiloptimierte Homepage, die zu einer besseren Auffindbarkeit im Netz führt.</p>
                </div>
            </div>
            <div class="row margin_section1" style="margin-bottom: 1rem">
                <div class="col">
                    <img class="icons_atwidget" src="/static/img/praxisoptimierung/atwidget/question.png">
                    <p  class="margin_section1">Über 90% der Patienten suchen Ihren Arzt über Google und geben dazu Ihren Stadtteil und die Fachrichtung ein. Können Sie sich hier finden?</p>
                </div>
            </div>
        </div>
        <div class="col">
            <img class="img-responsive" src="/static/img/praxisoptimierung/atweb/section1.png" alt="atWidget" >
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-8 offset-md-2">
            <img class="img-responsive" src="/static/img/praxisoptimierung/atwidget/divider.png" alt="atWidget" >
        </div>
    </div>
</div>


<div class="container">
    <div class="row">
        <div class="col-sm">
            <span class="verticalAlign" >1.</span><span>Wir erstellen Ihnen eine Homepage nach Ihren Wünschen und den neusten Anforderungen von Google.</span>
            <img class="img-responsive" src="/static/img/praxisoptimierung/atweb/section2_img1.png" alt="atWeb" >
        </div>
        <div class="col">
            <span class="verticalAlign" >2.</span><span>Durch ständige Aktualisierungen verbessert sich das Ranking Ihrer Seite auf Google.</span>
            <img class="img-responsive" src="/static/img/praxisoptimierung/atweb/section2_img2.png" alt="atWeb" >
        </div>
    </div>
    <div class="row">
        <div class="col-sm">
            <span class="verticalAlign" >3.</span><span>Mehr Patienten aus Ihrem Umfeld kommen zu Ihnen in die Praxis.</span>
            <img class="img-responsive" src="/static/img/praxisoptimierung/atwidget/section2_img3.png" alt="atWeb" >
        </div>
        <div class="col">
            <span class="verticalAlign" >4.</span><span>Wir sind Ihr ständiger Ansprechpartner für die laufende Optimierung Ihrer Website.</span>
            <img class="img-responsive" src="/static/img/praxisoptimierung/atwidget/section2_img4.png" alt="atWeb" >
        </div>
    </div>
</div>

<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            <h1 class="title" align="center">Unsere Pakete im Überblick</h1>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-sm-7"></div>
        <div class="col at_tableTitle text-center" >Basic</div>
        <div class="col at_tableTitle text-center" >Comfort</div>
        <div class="col at_tableTitle text-center" >Deluxe</div>
    </div>

    {*Table*}
    <div class="row at_marginTop">
        <div class="col-sm-7 at_marginBottom"><i class="fa fa-chevron-right" aria-hidden="true" style="padding-right: .5rem"></i><span>Keine Einrichtungskosten</span></div>
        <div class="col text-center"><img src="/static/img/praxisoptimierung/atwidget/tick-box.png" class="table_checkbox"></div>
        <div class="col text-center"><img src="/static/img/praxisoptimierung/atwidget/tick-box.png" class="table_checkbox"></div>
        <div class="col text-center"><img src="/static/img/praxisoptimierung/atwidget/tick-box.png" class="table_checkbox"></div>
    </div>
    <div class="row at_marginTop">
        <div class="col-sm-7 at_marginBottom"><i class="fa fa-chevron-right" aria-hidden="true" style="padding-right: .5rem"></i><span>Überarbeitung, bzw. Erstellung Ihrer Website, inklusive der Einbindung Ihrer Texte und Bilder</span></div>
        <div class="col text-center"><img src="/static/img/praxisoptimierung/atwidget/tick-box.png" class="table_checkbox"></div>
        <div class="col text-center"><img src="/static/img/praxisoptimierung/atwidget/tick-box.png" class="table_checkbox"></div>
        <div class="col text-center"><img src="/static/img/praxisoptimierung/atwidget/tick-box.png" class="table_checkbox"></div>
    </div>
    <hr>
    <div class="row at_marginTop">
        <div class="col-sm-7 at_marginBottom"><i class="fa fa-chevron-right" aria-hidden="true" style="padding-right: .5rem"></i><span>Verwendung von responsiven Designs für Mobilgeräte <br/> Suchmaschinenfreundlicher Quellcode</span></div>
        <div class="col text-center"><img src="/static/img/praxisoptimierung/atwidget/tick-box.png" class="table_checkbox"></div>
        <div class="col text-center"><img src="/static/img/praxisoptimierung/atwidget/tick-box.png" class="table_checkbox"></div>
        <div class="col text-center"><img src="/static/img/praxisoptimierung/atwidget/tick-box.png" class="table_checkbox"></div>
    </div>
    <div class="row at_marginTop">
        <div class="col-sm-7 at_marginBottom"><i class="fa fa-chevron-right" aria-hidden="true" style="padding-right: .5rem"></i><span>Anpassungen von Metatexten und Metadescriptions und einfügen von Titletags und alt-Attributen</span></div>
        <div class="col text-center"><img src="/static/img/praxisoptimierung/atwidget/tick-box.png" class="table_checkbox"></div>
        <div class="col text-center"><img src="/static/img/praxisoptimierung/atwidget/tick-box.png" class="table_checkbox"></div>
        <div class="col text-center"><img src="/static/img/praxisoptimierung/atwidget/tick-box.png" class="table_checkbox"></div>
    </div>
    <hr>
    <div class="row at_marginTop">
        <div class="col-sm-7 at_marginBottom"><i class="fa fa-chevron-right" aria-hidden="true" style="padding-right: .5rem"></i><span>Kontinuierliche Aktualisierung Ihrer Website</span></div>
        <div class="col text-center"><img src="/static/img/praxisoptimierung/atwidget/tick-box.png" class="table_checkbox"></div>
        <div class="col text-center"><img src="/static/img/praxisoptimierung/atwidget/tick-box.png" class="table_checkbox"></div>
        <div class="col text-center"><img src="/static/img/praxisoptimierung/atwidget/tick-box.png" class="table_checkbox"></div>
    </div>
    <div class="row at_marginTop">
        <div class="col-sm-7 at_marginBottom"><i class="fa fa-chevron-right" aria-hidden="true" style="padding-right: .5rem"></i><span>Erstellung von professionellen Texten</span></div>
        <div class="col text-center"></div>
        <div class="col text-center"><small>1 Artikel alle 2 Monate*</small></div>
        <div class="col text-center"><small>1 Artikel pro Monate*</small></div>
    </div>
    <div class="row at_marginTop">
        <div class="col-sm-7 at_marginBottom"><i class="fa fa-chevron-right" aria-hidden="true" style="padding-right: .5rem"></i><span>Professionelle Fotos durch einen Fotografen</span></div>
        <div class="col text-center"></div>
        <div class="col text-center"><img src="/static/img/praxisoptimierung/atwidget/tick-box.png" class="table_checkbox"></div>
        <div class="col text-center"><img src="/static/img/praxisoptimierung/atwidget/tick-box.png" class="table_checkbox"></div>
    </div>
    <div class="row at_marginTop">
        <div class="col-sm-7 at_marginBottom"><i class="fa fa-chevron-right" aria-hidden="true" style="padding-right: .5rem"></i><span>Einrichtung von Google MyBusiness und Facebook</span></div>
        <div class="col text-center"></div>
        <div class="col text-center"><img src="/static/img/praxisoptimierung/atwidget/tick-box.png" class="table_checkbox"></div>
        <div class="col text-center"><img src="/static/img/praxisoptimierung/atwidget/tick-box.png" class="table_checkbox"></div>
    </div>
    <hr>
    <div class="row at_marginTop">
        <div class="col-sm-7 at_marginBottom "><i class="fa fa-chevron-right" aria-hidden="true" style="padding-right: .5rem"></i><span>Design von Visitenkarten & Briefpapier</span></div>
        <div class="col text-center"></div>
        <div class="col text-center"></div>
        <div class="col text-center"><img src="/static/img/praxisoptimierung/atwidget/tick-box.png" class="table_checkbox"></div>
    </div>

    {*End Table     *}
    <br/>
    <div class="row">
        <div class="col" style="background-color: #0090D6; padding: .5rem"></div>
    </div>
    <p>* Bei einem Artikel handelt es sich um einen Google freundlich geschriebenen Text der bis zu 500 Wörter umfasst. Hierbei kann es sich um neue Inhalte oder um verbesserte alte Inhalte der Webseite handeln.</p>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-8 offset-md-2">
            <img class="img-responsive" src="/static/img/praxisoptimierung/atwidget/divider.png" alt="atWidget" >
        </div>
    </div>
</div>

{assign var="contact_form_checkbox_selected" value='atweb'}
{include file="praxisoptimierung/_form.tpl"}

{include file="praxisoptimierung/footer.tpl"}
