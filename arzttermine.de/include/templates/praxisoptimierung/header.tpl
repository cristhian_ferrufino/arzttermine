<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

    <title>{$head_title}</title>
    <meta name="keywords" content="{$head_meta_keywords}" />
    <meta name="description" content="Ihr Kalender für ein optimales Terminmanagement" />
    <link rel="shortcut icon" href="/static/images/assets/favicon.ico">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
    <!--[if lt IE 9]>
        <script src="/static/js/html5shiv.min.js"></script>
        <script src="/static/js/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="/lp/atsearch/praxisoptimierung.css">

    <!--[if gte IE 9]><!-->
    <link href='//fonts.googleapis.com/css?family=Lato:300italic,400,400italic,700' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Gudea:400,700,900' rel='stylesheet' type='text/css'>
    <!--<![endif]-->

</head>
<body class="{$pageId}">
{include file="js/tagmanager/tagmanager.tpl"}
    <div class="bumper"></div>
    <div class="header-wrapper">
        <header class="box leading-border with-triangle">
            <div>Der schnelle Weg zu uns...</div>
            <div><a href="tel:030609840272">030 / 609 840 272</a></div>
            <div>...oder nutzen Sie unser Kontaktformular</div>
            <div class="text-center">
                <a href="#form" class="btn btn-success lightbox-trigger">Kontakt</a>
            </div>
        </header>
    </div>
