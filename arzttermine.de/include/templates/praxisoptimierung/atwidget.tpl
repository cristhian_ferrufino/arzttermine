{assign var="pageId" value="page-atwidget"}
{assign var="head_title" value="AtWidget - Online Terminkalender"}
{assign var="head_meta_description" value="Rundum-Service für Ihre Patienten. Ermöglichen Sie eine Online Terminvergabe für ein optimales Terminmanagement."}
{assign var="head_meta_keywords" value=""}
{include file="praxisoptimierung/header.tpl"}
<link rel="stylesheet" type="text/css" href="/lp/atsearch/style_praxisoptimierung.css">

<div class="container at_marginTop" >
    <div class="row">
        <div class="col-sm">
            <h1 class="text-logo logo"><span>At</span>Widget</h1>
        </div>
        <div class="col">
            <img class="img-responsive" src="/static/img/arzttermine.svg" alt="Arzttermine.de" style="float: right; width: 20rem">
        </div>
    </div>
</div>
<br/>
<div class="container">
    <div class="row justify-content-center">
        <div class="col"><h1 class="title" align="center">Ihr Kalender für ein optimales Terminmanagement</h1></div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-sm">
            <div class="row margin_section1">
                <div class="col">
                    <img class="icons_atwidget" src="/static/img/praxisoptimierung/atwidget/warning.png">
                    <p class="margin_section1">Patienten möchten schnell und einfach Termine bei Ihnen buchen um mit ihrem Anliegen sofort beim richtigen Arzt behandelt zu werden.</p>
                </div>
            </div>
            <div class="row margin_section1">
                <div class="col">
                    <img class="icons_atwidget" src="/static/img/praxisoptimierung/atwidget/tick-box.png">
                    <p class="margin_section1">Mit <span class="text-logo"><span>At</span>Widget</span> sind Sie rund um die Uhr für Ihre Patienten erreichbar. Zeigen Sie online Ihre freien Kapazitäten oder ausgewählte Terminslots für bestimmte Behandlungen.</p>
                </div>
            </div>
            <div class="row margin_section1">
                <div class="col">
                    <img class="icons_atwidget" src="/static/img/praxisoptimierung/atwidget/question.png">
                    <p  class="margin_section1">Optimieren Sie Ihr Terminmanagement mit Erinnerungen für Patienten, einem übersichtlichen Ressourcenmanagement, Bezahlfunktionen für IGeL-Leistungen und vielem mehr.</p>
                </div>
            </div>
        </div>
        <div class="col">
            <img class="img-responsive" src="/static/img/praxisoptimierung/atwidget/section1_widget.png" alt="atWidget" >
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-8 offset-md-2">
            <img class="img-responsive" src="/static/img/praxisoptimierung/atwidget/divider.png" alt="atWidget" >
        </div>
    </div>
</div>


<div class="container">
    <div class="row">
        <div class="col-sm">
            <span class="verticalAlign" >1.</span><span>Über unseren Kalender können Sie spielend leicht Ihre Termine verwalten.</span>
            <img class="img-responsive" src="/static/img/praxisoptimierung/atwidget/section2_img1.png" alt="atWidget" >
        </div>
        <div class="col">
            <span class="verticalAlign" >2.</span><span>Patienten suchen auf Ihrer Homepage den Wunschtermin für ihr konkretes Anliegen.</span>
            <img class="img-responsive" src="/static/img/praxisoptimierung/atwidget/section2_img2.png" alt="atWidget" >
        </div>
    </div>
    <div class="row">
        <div class="col-sm">
            <span class="verticalAlign" >3.</span><span>Der Patient kommt passgenau und ohne Wartezeiten zum richtigen Arzt in die Praxis.</span>
            <img class="img-responsive" src="/static/img/praxisoptimierung/atwidget/section2_img3.png" alt="atWidget" >
        </div>
        <div class="col">
            <span class="verticalAlign" >4.</span><span>Services wie z.B. Terminerinnerungen erhöhen die Zufriedenheit Ihrer Patienten.</span>
            <img class="img-responsive" src="/static/img/praxisoptimierung/atwidget/section2_img4.png" alt="atWidget" >
        </div>
    </div>
</div>

<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            <h1 class="title" align="center">Unsere Services im Überblick</h1>
        </div>
    </div>
</div>

<div class="container">
        {*Table*}
        <div class="row at_marginTop">
            <div class="col"><i class="fa fa-chevron-right" aria-hidden="true" style="padding-right: .5rem"></i>Ressourcenmanagement (Behandlungsräume/ Geräte/ Assistenten)</div>

        </div>
        <div class="row at_marginTop">
            <div class="col"><i class="fa fa-chevron-right" aria-hidden="true" style="padding-right: .5rem"></i>Terminbestätigungen und -erinnerungen (inklusive 50 SMS, je weitere 0,15 €)</div>

        </div>
        <div class="row at_marginTop">
            <div class="col"><i class="fa fa-chevron-right" aria-hidden="true" style="padding-right: .5rem"></i>Bezahlfunktion für z.B. IGeL-Leistungen (zzgl. 2,9% + 0,25 € je Transaktion)</div>

        </div>
        <div class="row at_marginTop">
            <div class="col"><i class="fa fa-chevron-right" aria-hidden="true" style="padding-right: .5rem"></i>Bewertungsmails/-SMS (inklusive 50 SMS, je weitere 0,15 €)</div>

        </div>
        <div class="row at_marginTop">
            <div class="col"><i class="fa fa-chevron-right" aria-hidden="true" style="padding-right: .5rem"></i>Zusätzliche Abfragen im Buchungsprozess</div>

        </div>
        {*End Table     *}
    <br/>
    <div class="row">
        <div class="col" style="background-color: #0090D6; padding: .5rem"></div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col">
            <p>Hiermit buche ich die oben angekreuzten Services. Mit den Konditionen erkläre ich mich einverstanden.</p>
        </div>
    </div>
</div>

{assign var="contact_form_checkbox_selected" value='atwidget'}
{include file="praxisoptimierung/_form.tpl"}

{include file="praxisoptimierung/footer.tpl"}
