{assign var="pageId" value="page-atpatient"}
{assign var="head_title" value="AtPatient - Neupatienten über Arzttermine.de gewinnen"}
{assign var="head_meta_description" value="Mit einem Profil auf Arzttermine.de gewinnen Sie neue Patienten bereits in über 15 Städten und 40 Fachrrichtungen"}
{assign var="head_meta_keywords" value=""}
{include file="praxisoptimierung/header.tpl"}

<div id="intro">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h1 class="text-logo with-subline"><span>At</span>Patient</h1>
                <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/" itemprop="url" target="_blank">
                        <span itemprop="title">Arzttermine.de</span>
                    </a>
                </span> > 
                <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/praxisoptimierung" itemprop="url">
                        <span itemprop="title">Praxisoptimierung</span>
                    </a>
                </span> > 
                <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/praxisoptimierung/atpatient" itemprop="url">
                        <span itemprop="title" content="AtPatient: Neupatienten von Arzttermine.de">AtPatient</span>
                    </a>
                </span>
                <br />
                <h3 class="boxed-text-blue">
                    <span><strong>Online Arzttermine - So finden Sie online neue Patienten</strong></span><br>
                    <span>Füllen Sie Lücken in Ihrem Terminkalender</span><br />
                    <span>Terminbuchungen rund um die Uhr möglich</span>
                </h3>
            </div>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <div class="hidden-xs hidden-sm col-md-7">
                <img class="img-responsive" src="/static/img/praxisoptimierung/atweb-intro-transparent.png" alt="" >
            </div>
        </div>
    </div>
</div>

<div class="container">
    <h2>Die Vorteile von <strong>At</strong>Patient</h2>
    <div class="row">
        <div class="col-md-7">
            <ul class="checkmarks list-unstyled row">
                <li class="checkmark-green h4-5">
                    Kostenfreie Erstellung und Pflege Ihres individuellen Praxis- und Arztprofiles
                </li>
                <li class="checkmark-green h4-5">
                    Terminkoordination durch unsere geschulten Mitarbeiter
                </li>
                <li class="checkmark-green h4-5">
                    Bedarfsgerechte Terminierung zur optimalen Auslastung Ihrer Praxis
                </li>
                <li class="checkmark-green h4-5">
                    Risikofreies und gezieltes Patientenmarketing
                </li>
                <li class="checkmark-green h4-5">
                    Starke Kooperationspartner
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="container">
    <h2> Unsere Kooperationspartner</h2> 
    <p>Um Ihre verfügbaren Arzttermine auf vielen Portalen anzubieten, arbeiten wir mit verschiedenen Portalen zusammen</p>
    <div class="row">
        <div class="col-xs-12 col-md-3">
            <img class="img-responsive" src="/static/img/praxisoptimierung/dak-logo.png" alt="DAK Gesundheit ist Kooperationspartner von Arzttermine.de" >
        </div>
        <div class="col-xs-12 col-md-3">
            <h4>DAK-Gesundheit</h4>
            <p>Ihre freien Termine werden automatisch auch für den Buchungsservice der DAK freigegeben, der von Arzttermine.de betrieben wird.</p>
        </div>
        <div class="col-xs-12 col-md-3">
            <img class="img-responsive" src="/static/img/praxisoptimierung/arztauskunft-logo.jpg" alt="Die Arzt-Website wird verknüpft mit dem Kartendienst Google Maps" >
        </div>
        <div class="col-xs-12 col-md-3">
            <h4>Arzt-Auskunft</h4>
            <p>Die große Suchmaschine für Ärzte der Stiftung Gesundheit bieten Patienten ebenfalls an, direkt auf dem Profil einen Arzttermin bei Ihnen zu vereinbaren.</p>
        </div>
    </div>
    <br />
    <br />
    <br />
    <div class="row">
        <div class="col-xs-12 col-md-3">
            <img class="img-responsive" src="/static/img/praxisoptimierung/gelbe-seiten-logo.jpg" alt="Arzttermine auf den Gelben Seiten in Köln" >
        </div>
        <div class="col-xs-12 col-md-3">
            <h4>Gelbe Seiten</h4>
            <p>In Köln werden Ihre freien Terminen zusätzlich auch in den Gelben Seiten angezeigt.</p>
        </div>
        <div class="col-xs-12 col-md-3">
            <img class="img-responsive" src="/static/img/praxisoptimierung/netdoktor-logo.png" alt="Notdoktor kooperiert mit Arzttermine.de" >
        </div>
        <div class="col-xs-12 col-md-3">
            <h4>NetDoktor</h4>
            <p>Arzttermine.de kooperiert zudem mit NetDoktor, einem der größten Internetportale für Gesundheit und Medizin.</p>
        </div>
    </div>
</div>

<div class="container">
    <h2> Technische Anbindung</h2> 
    <p>Damit Sie so einfach wie möglich neue Patienten gewinnen können, bieten wir Ihnen eine Interation über verschiedene Möglichkeiten an</p>
    <div class="row">
        <div class="col-xs-12 col-md-3">
            <img class="img-responsive" src="/static/img/praxisoptimierung/dak-logo.png" alt="DAK Gesundheit ist Kooperationspartner von Arzttermine.de" >
        </div>
        <div class="col-xs-12 col-md-3">
            <h4>Praxissoftware</h4>
            <p>Auf Ihrem Profil stehen au</p>
        </div>
        <div class="col-xs-12 col-md-3">
            <img class="img-responsive" src="/static/img/praxisoptimierung/arztauskunft-logo.jpg" alt="Die Arzt-Website wird verknüpft mit dem Kartendienst Google Maps" >
        </div>
        <div class="col-xs-12 col-md-3">
            <h4>Arzt-Auskunft</h4>
            <p>Die große Suchmaschine für Ärzte der Stiftung Gesundheit bieten Patienten ebenfalls an, direkt auf dem Profil einen Arzttermin bei Ihnen zu vereinbaren.</p>
        </div>
    </div>
    <br />
    <br />
    <br />
    <div class="row">
        <div class="col-xs-12 col-md-3">
            <img class="img-responsive" src="/static/img/praxisoptimierung/gelbe-seiten-logo.jpg" alt="Arzttermine auf den Gelben Seiten in Köln" >
        </div>
        <div class="col-xs-12 col-md-3">
            <h4>Gelbe Seiten</h4>
            <p>In Köln werden Ihre freien Terminen zusätzlich auch in den Gelben Seiten angezeigt.</p>
        </div>
        <div class="col-xs-12 col-md-3">
            <img class="img-responsive" src="/static/img/praxisoptimierung/netdoktor-logo.png" alt="Notdoktor kooperiert mit Arzttermine.de" >
        </div>
        <div class="col-xs-12 col-md-3">
            <h4>NetDoktor</h4>
            <p>Arzttermine.de kooperiert zudem mit NetDoktor, einem der größten Internetportale für Gesundheit und Medizin.</p>
        </div>
    </div>
</div>

<div class="container">
    <div id="offer">
        <h2>Neupatientengewinnung im Überblick</h2>
        <table class="full-width">
            <thead>
            <tr>
                <th class="transparent">&nbsp;</th>
                <th class="blue">Gesetzlich und privat versichert</th>
            </tr>
            </thead>
            <tbody>
              <tr>
                <td>Kostenloses Profil auf Arzttermine.de</td>
                <td><img src="/static/img/praxisoptimierung/checkmark.png" alt="Ja" /></td>
            </tr>
            <tr>
                <td>Keine Betriebskosten</td>
                <td><img src="/static/img/praxisoptimierung/checkmark.png" alt="Ja" /></td>
            </tr>
            <tr>
                <td>Kostenlose Buchung für Patienten über die Hotline</td>
                <td><img src="/static/img/praxisoptimierung/checkmark.png" alt="Ja" /></td>
            </tr>
            <tr>
                <td>Monatliches Reporting über Patienten von Arzttermine.de</td>
                <td><img src="/static/img/praxisoptimierung/checkmark.png" alt="Ja" /></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="container">
    <div class="box-grey-light  text-center ">
        <p class="boxed-text-blue">
            <span class="center-block h4-5">
                Ihre Bilanz: <strong>Mehr Neupatienten und höhere Zufriedenheit</strong>
            </span>
        </p>
    </div>
</div>

<div class="container">
    <div id="form" class="leading-border with-triangle box-grey-light radius">
        <h3>Holen Sie sich jetzt Ihr individuelles Angebot ein!</h3>

        {assign var="contact_form_show_products" value=false}
        {assign var="contact_form_checkbox_selected" value='atpatient'}
        {include file="praxisoptimierung/_contact-form.tpl"}
    </div>
</div>

{include file="praxisoptimierung/footer.tpl"}
