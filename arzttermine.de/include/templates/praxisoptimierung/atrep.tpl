{assign var="pageId" value="page-atrep"}
{assign var="head_title" value="AtRep - Ihre Reputation immer in Blick von Arzttermine.de"}
{assign var="head_meta_description" value="Steigern Sie Ihre Reputation im Internet und gewinnen Sie so neue Patienten. Zudem können Sie sich mit AtRep gegen unangemessene Kritik im Internet wehren."}
{assign var="head_meta_keywords" value=""}
{include file="praxisoptimierung/header.tpl"}

<div id="intro">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h1 class="text-logo with-subline"><span>At</span>Rep</h1>
                <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/" itemprop="url" target="_blank">
                        <span itemprop="title" content="Arzttermine.de">Arzttermine.de</span>
                    </a>
                </span> >
                <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/praxisoptimierung" itemprop="url" content="Praxisoptimierung">
                        <span itemprop="title">Praxisoptimierung</span>
                    </a>
                </span> >
                <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/praxisoptimierung/atrep" itemprop="url">
                        <span itemprop="title" content="AtRep: Ihre Bewertungen im Blick" >AtRep</span>
                    </a>
                </span>
                <h3 class="boxed-text-blue">
                    <span><strong>Haben Sie Ihre Reputation immer im Blick</strong></span><br>
                    <span>89% der Patienten ist die Meinung</span><br>
                    <span>anderer bei der Wahl des Arztes wichtig</span>
                </h3>
                <p class="lead text-left">
                    Mit dem Reputationsmonitor können Sie nachhaltig Ihre Reputation verbessern
                </p>
            </div>
            <div class="hidden-xs hidden-sm col-md-7">
                <img class="img-responsive" src="/static/img/praxisoptimierung/atrep-intro-transparent.png" alt="Reputationsmanagement für Ärzte" >
            </div>
        </div>
    </div>
</div>

<div class="container">
    <h2>Warum die eigene Reputation wichtig ist</h2>
    <p>92% aller Ärzte haben bereits erkannt, dass ihre Reputation im Internet für den Patienten eine wichtige Rolle spielt. Mit AtRep haben Sie diese immer Blick und können darauf auch mit unserer Unterstützung professionell reagieren.
        <br />
        <br />
        Vermehrt suchen Patienten ihre Ärzte bei Google. Wenn man den Namen eines Arztes in die Suchmaschine eingibt, erscheinen die Bewertungsplattformen in den obersten Suchergebnissen. Daher ist es für Praxen besonders wichtig, dass diese vor allem positive Bewertungen von Ihnen anzeigen.
    </p>
</div>

<div class="container">
    <div class="vertical-box-container leading-border with-triangle box-grey-light radius">
        <h3>Die Vorteile Ihres Reputationsmonitors</h3>
        <ul class="clearfix list-inline lead">
            <li class="col-sm-4 checkmark-green-circle">
                Sie sind immer informiert, was über Sie im Internet geschrieben wird
            </li>
            <li class="col-sm-4 checkmark-green-circle">
                Sie haben die Möglichkeit schnell auf Kritik zu reagieren
            </li>
            <li class="col-sm-4 checkmark-green-circle">
                Sie haben die Kontrolle über Ihre Bewertungen und können sich so besser repräsentieren
            </li>
            <li class="col-sm-4 checkmark-green-circle">
                Stellen Sie verifizierte Bewertungen Ihrer eigenen Patienten auf der Homepage dar
            </li>
            <li class="col-sm-4 checkmark-green-circle">
                Sie gewinnen aktiv neue Bewertungen von Patienten
            </li>
            <li class="col-sm-4 checkmark-green-circle">
                Bessere Patientenbindung, da die Patienten sehen, dass Sie sich weiter um Sie kümmern
            </li>
        </ul>
    </div>
</div>

<div class="container">
    <h2>So verbessern Sie nachhaltig Ihre Reputation im Internet</h2>
    <div class="steps-container blue-border">
        <ol class="clearfix list-unstyled">
            <li class="col-xs-12 col-sm-3">
                <img class="img-responsive hidden-xs" src="/static/img/praxisoptimierung/atrep-step1.jpg" alt="Bewertung wird abgegeben">
                <div class="leading-border with-triangle box-grey-light">
                    <div>1</div>
                    <p>Ein Patient <strong>bewertet Sie auf einer Bewertungsplattform</strong>.<p>
                </div>
            </li>
            <li class="col-xs-12 col-sm-3">
                <img class="img-responsive hidden-xs" src="/static/img/praxisoptimierung/atrep-step2.jpg" alt="Bewertung erscheint im Bewertungsmonitor">
                <div class="leading-border with-triangle box-grey-light">
                    <div>2</div>
                    <p>Sie <strong>sehen</strong> die Bewertung in Ihrem <strong>Bewertungsmonitor</strong>.</p>
                </div>
            </li>
            <li class="col-xs-12 col-sm-3">
                <img class="img-responsive hidden-xs" src="/static/img/praxisoptimierung/atrep-step3.jpg" alt="Patienten sehen Bewertungen auf Ihrer Website">
                <div class="leading-border with-triangle box-grey-light">
                    <div>3</div>
                    <p>Der Bewertungsmonitor unterstützt Sie bei der <strong>Reaktion auf negative und kritische Bewertungen</strong>.</p>
                </div>
            </li>
            <li class="col-xs-12 col-sm-3">
                <img class="img-responsive hidden-xs" src="/static/img/praxisoptimierung/atrep-step4.jpg" alt="Ihre Reputation verbessert sich">
                <div class="leading-border with-triangle box-grey-light">
                    <div>4</div>
                    <p>Sie <strong>fördern nachhaltig Ihre Reputation </strong>im Internet.</p>
                </div>
            </li>
        </ol>
    </div>
    <br />
</div>

<div class="container">
    <h2>Unser Service für Sie</h2>
    <div class="row">
        <div class="col-xs-12 col-md-3">
            <img class="img-responsive" src="/static/img/praxisoptimierung/atrep-mac-widget.jpg" alt="Der Bewertungsmonior online" >
        </div>
        <div class="col-xs-12 col-md-3">
            <h4>Ihre Bewertungen auf einen Blick</h4>
            <p>Dank der einfachen Struktur können Sie sofort sehen, welche Bewertungen für Sie abgegeben wurden. So verlieren Sie nie den Überblick und wissen, was Ihre Patienten über Sie denken.</p>
        </div>
        <div class="col-xs-12 col-md-3">
            <img class="img-responsive" src="/static/img/praxisoptimierung/atrep-report-pdf.jpg" alt="Den Bewertungsmonitor bekommen Sie einmal im Monat als PDF" width="80%" >
        </div>
        <div class="col-xs-12 col-md-3">
            <h4>Ihr monatlicher Report</h4>
            <p>Sie bekommen jeden Monat einen Bericht mit allen neuen abgegebenen Bewertungen, die im Internet zu finden sind. So verlieren Sie nie den Überblick und wissen, was Ihre Patienten über Sie denken.</p>
        </div>
    </div>
    <div class="box-grey-light  text-center ">
        <p class="boxed-text-blue">
            <span class="center-block h4-5">
                Ihre Bilanz: <strong>Stärkere Patientenbindung durch eine bessere Reputation im Netz</strong>
            </span>
        </p>
    </div>
</div>

<div class="container">
    <div id="offer">
        <h2>Unsere Pakete für Ihren Bewertungsmonitor im Überblick</h2>
        <table class="full-width">
            <thead>
            <tr>
                <th class="transparent" >&nbsp;</th>
                <th class="blue">Lite</th>
                <th class="blue">Premium</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Welche Bewertungsplattformen werden durchsucht</td>
                <td>alle</td>
                <td>alle</td>
            </tr>
            <tr>
                <td>Monatliche Reportings über abgegebene Bewertungen</td>
                <td><img src="/static/img/praxisoptimierung/checkmark.png" alt="Ja" /></td>
                <td><img src="/static/img/praxisoptimierung/checkmark.png" alt="Ja" /></td>
            </tr>
            <tr>
                <td>Umsetzung einer angemessenen Reaktion</td>
                <td><img src="/static/img/praxisoptimierung/checkmark.png" alt="Ja" /></td>
                <td><img src="/static/img/praxisoptimierung/checkmark.png" alt="Ja" /></td>
            </tr>
            <tr>
                <td>Aktives Sammeln von eigenen Bewertung</td>
                <td>&nbsp;</td>
                <td><img src="/static/img/praxisoptimierung/checkmark.png" alt="Ja" /></td>

            </tr>
            <tr>
                <td>Veröffentlichung der Bewertung auf Ihrer Website</td>
                <td>&nbsp;</td>
                <td><img src="/static/img/praxisoptimierung/checkmark.png" alt="Ja" /></td>
            </tr>
            <tr>
                <td class="blue"></td>
                <td class="blue"></td>
                <td class="blue"></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="container">
    <div id="form" class="leading-border with-triangle box-grey-light radius">
        <h3>Holen Sie sich jetzt Ihr individuelles Angebot ein!</h3>

        {assign var="contact_form_show_products" value=false}
        {assign var="contact_form_checkbox_selected" value='atrep'}
        {include file="praxisoptimierung/_contact-form.tpl"}
    </div>
</div>

{include file="praxisoptimierung/footer.tpl"}
