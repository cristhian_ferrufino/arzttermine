{assign var="pageId" value="page-atcall"}
{assign var="head_title" value="AtCall - Der Fachtelefondienst von Arzttermine.de"}
{assign var="head_meta_description" value="Mit dem Fachtelefondienst von AtCall verlieren Sie in Ihrer Arztpraxis keinen Anruf mehr. Stärkere Patientenbindung und mehr Arzttermine."}
{assign var="head_meta_keywords" value=""}
{include file="praxisoptimierung/header.tpl"}

<div id="intro">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <h1 class="text-logo with-subline"><span>At</span>Call</h1>
                <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/" itemprop="url" target="_blank">
                        <span itemprop="title">Arzttermine.de</span>
                    </a>
                </span> >
                <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/praxisoptimierung" itemprop="url">
                        <span itemprop="title">Praxisoptimierung</span>
                    </a>
                </span> >
                <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/praxisoptimierung/atcall" itemprop="url">
                        <span itemprop="title">AtCall: Ihr Fachtelefondienst</span>
                    </a>
                </span>
                <h3 class="boxed-text-blue">
                    <span>Die Entlastung für Ihre Praxis!</span><br>
                    <span>Sie kümmern sich um Ihre Patienten.</span><br>
                    <span><strong>Wir kümmern uns um Ihr Telefon…</strong></span>
                </h3>
                <p class="lead text-left">
                    Unsere <strong>jahrelange Zusammenarbeit mit Ärzten
                        und Patienten</strong> im Online-Bereich macht uns zu Ihrem
                    <strong>kompetenten Partner für Ihr Praxismarketing!</strong>
                </p>
            </div>
            <div class="hidden-xs hidden-sm col-md-5">
                <img class="img-responsive" src="/static/img/praxisoptimierung/atcall-intro-transparent.png" alt="" >
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="steps-container">
        <h2>Ihr einfacher Weg für zufriedene Patienten</h2>

        <ol class="clearfix list-unstyled">
            <li class="one-fifth">
                <img class="img-responsive hidden-xs" src="/static/img/praxisoptimierung/atcall-step1.jpg">
                <div class="leading-border with-triangle box-grey-light">
                    <div>1</div>
                    <p>Ein potentieller <strong>Patient ruft in Ihrer Praxis an.</strong></p>
                </div>
            </li>
            <li class="one-fifth">
                <img class="img-responsive hidden-xs" src="/static/img/praxisoptimierung/atcall-step2.jpg">
                <div class="leading-border with-triangle box-grey-light">
                    <div>2</div>
                    <p>Ihre <strong>Praxis ist gerade nicht besetzt</strong> oder Ihre Sprechstundenhilfe kann gerade <strong>nicht an das Telefon gehen</strong>.</p>
                </div>
            </li>
            <li class="one-fifth">
                <img class="img-responsive hidden-xs" src="/static/img/praxisoptimierung/atcall-step3.jpg">
                <div class="leading-border with-triangle box-grey-light">
                    <div>3</div>
                    <p>Unser <strong>geschultes Personal übernimmt den Anruf</strong> für Sie.</p>
                </div>
            </li>
            <li class="one-fifth">
                <img class="img-responsive hidden-xs" src="/static/img/praxisoptimierung/atcall-step4.jpg">
                <div class="leading-border with-triangle box-grey-light">
                    <div>4</div>
                    <p>Im Anschluss leiten wir Ihnen <strong>alle Informationen gebündelt</strong> weiter.</p>
                </div>
            </li>
            <li class="one-fifth">
                <img class="img-responsive hidden-xs" src="/static/img/praxisoptimierung/atcall-step5.jpg">
                <div class="leading-border with-triangle box-grey-light">
                    <div>5</div>
                    <p>Ihre <strong>Patienten sind zufrieden</strong> und Sie <strong>haben keinen Patienten verloren</strong>.</p>
                </div>
            </li>
        </ol>
    </div>
</div>

<div class="container">
    <h2>Vorteile durch den Fachtelefondienst</h2>
    <div class="row">
        <div class="col-md-7">
            <ul class="checkmarks list-unstyled row">
                <li class="checkmark-green col-xs-12 col-md-7 h4-5">
                    <strong>Ihr Nutzen</strong>
                    <ul>
                        <li>Entlastung Ihrer Praxis in Stoßzeiten</li>
                        <li>Ausweitung der Telefonzeiten</li>
                        <li>Mehr Effizienz durch Vermeidung von Überstunden</li>
                    </ul>
                </li>
                <li class="checkmark-green col-xs-12 col-md-7 h4-5">
                    <strong>Unsere Leistungen</strong>
                    <ul>
                        <li>Persönliche Telefonannahme durch geschultes Personal</li>
                        <li>Transparentes und sicheres Abrechnungssystem</li>
                        <li>Geordnete Weiterleitung der Anfragen an Sie</li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="col-md-5">
            <div class="vertical-box-container leading-border with-triangle box-grey-light radius">
                <h4><strong>Unser Angebot</strong></h4>
                <ul class="clearfix list-inline lead">
                    <li class="checkmark-green-circle">Kostenlose technische Umsetzung</li>
                    <li class="checkmark-green-circle">Keine Mindestvertragslaufzeit</li>
                    <li class="checkmark-green-circle">Flexible Tarifoptionen, angepasst an Ihre Praxis</li>
                </ul>
            </div>
        </div>
    </div>
</div>



<div class="container">
    <div class="box-grey-light text-center">
        <p class="boxed-text-blue">
            <span class="center-block h4-5">
                Ihre Bilanz: <strong>Keine verpassten Anrufe, mehr Terminvereinbarungen, höhere Patiententreue</strong>
            </span>
        </p>
    </div>
</div>

<div class="container">
    <div id="form" class="leading-border with-triangle box-grey-light radius">
        <h3>Holen Sie sich jetzt Ihr individuelles Angebot ein!</h3>

        {assign var="contact_form_show_products" value=false}
        {assign var="contact_form_checkbox_selected" value='atcall'}
        {include file="praxisoptimierung/_contact-form.tpl"}
    </div>
</div>

{include file="praxisoptimierung/footer.tpl"}
