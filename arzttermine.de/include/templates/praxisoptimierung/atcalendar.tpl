{assign var="pageId" value="page-atcalendar"}
{assign var="head_title" value="AtCalendar - Das Terminwidget von Arzttermine.de"}
{assign var="head_meta_description" value="Ihre Patienten können über das Kalender-Widget direkt auf Ihrer Homepage buchen, sodass Ihr Praxisablauf nicht weiter gestört wird."}
{assign var="head_meta_keywords" value=""}
{include file="praxisoptimierung/header.tpl"}

<div id="intro">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <h1 class="text-logo with-subline"><span>At</span>Calendar</h1>
                <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/" itemprop="url" target="_blank">
                        <span itemprop="title">Arzttermine.de</span>
                    </a>
                </span> > 
                <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/praxisoptimierung" itemprop="url">
                        <span itemprop="title">Praxisoptimierung</span>
                    </a>
                </span> > 
                <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/praxisoptimierung/atcalendar" itemprop="url">
                        <span itemprop="title" content="AtCalendar: Arzttermine auf der eigenen Website">AtCalendar</span>
                    </a>
                </span>
                <h3 class="boxed-text-blue">
                    <span><strong>Der Online-Kalender auf Ihrer eigenen Website</strong></span><br>
                    <span>Einfache Terminbuchungen auf Ihrer Website</span><br>
                    <span>Rund um die Uhr möglich</span>
                </h3>
                <p class="lead text-left">
                    Bieten Sie Patienten einen Mehrwert auf Ihrer eigenen Website mit dem Termin-Widget.<br>
                </p>
            </div>
            <div class="hidden-xs hidden-sm col-md-5">
                <img class="img-responsive" style="margin-top:12em;" src="/static/img/praxisoptimierung/solutions-kalender.png" alt="Ein neues Bild " >
            </div>
        </div>
    </div>
</div>

<div class="container">
    <h2>Vorteile des Online-Kalenders auf der eigenen Website</h2>
    <div class="row">
        <div class="vertical-box-container leading-border with-triangle box-grey-light radius">
            <ul class="clearfix list-inline lead">
                <li class="checkmark-green-circle">Entlastung der Telefonleitung in der Praxis</li><br />
                <li class="checkmark-green-circle">Terminbuchungen rund um die Uhr</li><br />
                <li class="checkmark-green-circle">Einfache Koordination der Terminbuchung über das Internet</li><br />
                <li class="checkmark-green-circle">Stärkere Patientenbindung</li><br />
                <li class="checkmark-green-circle">Gewinnung neuer Patienten durch das Anbieten von freien Arztterminen</li>
            </ul>
        </div>
    </div>
</div>

<div class="container">
    <h2>So funktioniert Ihr eigener Online-Kalender</h2>
    <div class="steps-container blue-border">
        <ol class="clearfix list-unstyled">
            <li class="col-xs-12 col-sm-3">
                <div class="leading-border with-triangle box-grey-light">
                    <div>1</div>
                    <p>Freie Termine werden im Online-Kalender abgebildet.<p>
                </div>
            </li>
            <li class="col-xs-12 col-sm-3">
                <div class="leading-border with-triangle box-grey-light">
                    <div>2</div>
                    <p>Patienten buchen einen Arzttermin einfach online ohne den Praxisablauf zu stören.</p>
                </div>
            </li>
            <li class="col-xs-12 col-sm-3">
                <div class="leading-border with-triangle box-grey-light">
                    <div>3</div>
                    <p>Direkt nach der Buchung erhalten Sie in der Praxis eine E-Mail mit allen Details zum Patienten und dem gewünschten Termin.</p>
                </div>
            </li>
            <li class="col-xs-12 col-sm-3">
                <div class="leading-border with-triangle box-grey-light">
                    <div>4</div>
                    <p>Ihre Praxis ist optimal ausgelastet und die Patienten sind zufrieden, da sie bequem online einen Termin buchen konnten.</p>
                </div>
            </li>
        </ol>
    </div>
</div>

<div class="container">
    <div id="offer">
        <h2>Unsere Pakete für Ihren Kalender im Überblick</h2>
        <table class="full-width">
            <thead>
            <tr>
                <th class="transparent" >&nbsp;</th>
                <th class="blue">1 Behandler</th>
                <th class="blue">Jeder <br />weitere Behandler</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Kostenlose technische Einbindung in Ihre Website</td>
                <td><img src="/static/img/praxisoptimierung/checkmark.png" alt="Ja" /></td>
                <td><img src="/static/img/praxisoptimierung/checkmark.png" alt="Ja" /></td>
            </tr>
               <tr>
                <td>Aufbau einer Terminstruktur nach Ihren Wünschen</td>
                <td><img src="/static/img/praxisoptimierung/checkmark.png" alt="Ja" /></td>
                <td><img src="/static/img/praxisoptimierung/checkmark.png" alt="Ja" /></td>
            </tr>
            <tr>
                <td>Einbindung einer bestehenden Praxis-Software</td>
                <td><img src="/static/img/praxisoptimierung/checkmark.png" alt="Ja" /></td>
                <td><img src="/static/img/praxisoptimierung/checkmark.png" alt="Ja" /></td>
            </tr>
            <tr>
                <td class="blue"><strong>Gesamtbetrag im Monat (zzgl. USt.)</strong></td>
                <td class="blue"><strong>29,90 &euro;</strong></td>
                <td class="blue"><strong>14,90 &euro;</strong></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="container">
    <div class="box-grey-light text-center">
        <p class="boxed-text-blue">
            <span class="center-block h4-5">
                Ihre Bilanz: <strong>Einfache Terminorganisation und stärkere Patientenbindung</strong>
            </span>
        </p>
    </div>
</div>

<div class="container">
    <div id="form" class="leading-border with-triangle box-grey-light radius">
        <h3>Holen Sie sich jetzt Ihr individuelles Angebot ein!</h3>

        {assign var="contact_form_show_products" value=false}
        {assign var="contact_form_checkbox_selected" value='atcalendar'}
        {include file="praxisoptimierung/_contact-form.tpl"}
    </div>
</div>

{include file="praxisoptimierung/footer.tpl"}
