{assign var="pageId" value="page-index"}
{assign var="head_title" value="Arzttermine.de Praxisoptimierung"}
{assign var="head_meta_description" value="123"}
{assign var="head_meta_keywords" value="456"}
{include file="praxisoptimierung/header.tpl"}

<div id="intro">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1 class="text-logo"><span>At</span>Praxisoptimierung</h1>
                <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/" itemprop="url" target="_blank">
                        <span itemprop="title">Arzttermine.de</span>
                    </a>
                </span> > 
                <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/praxisoptimierung" itemprop="url">
                        <span itemprop="title">Praxisoptimierung</span>
                    </a>
                </span>
                <h2>Wir helfen Ihnen, die Abläufe in Ihrer Praxis zu optimieren</h2>
                <div class="row">
                    <ul class="list-unstyled col-xs-12 col-sm-6 col-md-7">
                        <li class="h3 box-blue-transparent checkmark-white"><em>Gewinnen Sie mehr Neupatienten</em></li>
                        <li class="h3 box-blue-transparent checkmark-white"><em>Erhöhen Sie die Patientenbindung</em></li>
                        <li class="h3 box-blue-transparent checkmark-white"><em>Verbessern Sie die Zufriedenheit der Patienten</em></li>
                        <li class="h3 box-blue-transparent checkmark-white"><em>Erreichen Sie mehr Patienten im Internet</em></li>
                        <li class="h4-5 box-white-transparent leading-border with-triangle">
                            Seit über drei Jahren bringen wir Patienten und Ärzte über das Internet zusammen. <strong>Wir stehen Ihnen auch bei individuellen Wünschen zur Seite. Sprechen Sie uns einfach an.</strong>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div id="lead">
                <p class="col-xs-12 text-center lead ">
                    <strong>"90% aller Patienten suchen im Internet nach einem passenden Arzt"</strong>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div id="solutions">
                <h2>Unsere Lösungen</h2>
                <ul class="clearfix list-unstyled row">
                    <li class="col-xs-12 col-sm-6 col-md-4 text-center">
                        <div class="box-blue">
                            <p class="h3 text-light"><em>Finden Patienten Sie bei Google auf den exklusiven Top-Positionen?</em></p>
                            <img class="img-responsive" src="/static/img/praxisoptimierung/solutions-suche.png">
                        </div>
                        <div class="box-white">
                            <div class="h1 text-logo"><a href="/praxisoptimierung/atsearch" title="Suchmaschinen-Marketing mit AtSearch von Arzttermine.de"><span>At</span>Search</a></div>
                            <p class="h5"><strong>Online Marketing</strong></p>
                        </div>
                        <ul class="box-blue-light list-unstyled text-left h4-5 triangle-bottom-white-shadow">
                            <li class="checkmark-green">Patientenoptimierte Google AdWords Kampagnen</li>
                            <li class="checkmark-green">Inklusive Einträge in unsere Partnernetzwerke (SPIEGEL ONLINE, imedo, NetDoktor und viele mehr)</li>
                        </ul>
                    </li>

                    <li class="col-xs-12 col-sm-6 col-md-4 text-center">
                        <div class="box-blue">
                            <p class="h3 text-light"><em>Entspricht Ihre Homepage dem Stil Ihrer Praxis?</em></p>
                            <img class="img-responsive" src="/static/img/praxisoptimierung/solutions-homepage.png">
                        </div>
                        <div class="box-white">
                            <div class="h1 text-logo"><a href="/praxisoptimierung/atweb" title="Ihre eigene Homepage mit AtWeb von Arzttermine.de"><span>At</span>Web</a></div>
                            <p class="h5"><strong>Homepage</strong></p>
                        </div>
                        <ul class="box-blue-light list-unstyled text-left h4-5">
                            <li class="checkmark-green">Individuelle Layoutvorschläge nach Ihren Vorstellungen</li>
                            <li class="checkmark-green">Aktualisierung und Pflege der Homepage</li>
                        </ul>
                    </li>

                    <li class="col-xs-12 col-sm-6 col-md-4  text-center">
                        <div class="box-blue">
                            <p class="h3 text-light"><em>Nutzen Sie Ihre Bewertungen als Werbung für Ihre Praxis?</em></p>
                            <img class="img-responsive" src="/static/img/praxisoptimierung/solutions-empfehlung.png">
                        </div>
                        <div class="box-white">
                            <div class="h1 text-logo"><a href="/praxisoptimierung/atrep" title="Der Reputationsmonitor mit AtRep von Arzttermine.de"><span>At</span>Rep</a></div>
                            <p class="h5"><strong>Empfehlungsmarketing</strong></p>
                        </div>
                        <ul class="box-blue-light list-unstyled text-left h4-5">
                            <li class="checkmark-green">Protokollierung und Weiterleitung aller im Internet auffindbaren Bewertungen an Sie</li>
                            <li class="checkmark-green">Platzieren Sie verifizierte Bewertungen auf Ihrer Homepage</li>
                        </ul>
                    </li>

                    <li class="col-xs-12 col-sm-6 col-md-4 text-center">
                        <div class="box-blue">
                            <p class="h3 text-light"><em>Werden Sie online von den richtigen Patienten gefunden?</em></p>
                            <img class="img-responsive" src="/static/img/praxisoptimierung/solutions-neupatient.png">
                        </div>
                        <div class="box-white">
                            <div class="h1 text-logo"><a href="/lp/praxis-marketing/praxis-marketing.html" title="Mehr Neupatienten gewinnen mit AtPatient von Arzttermine.de"><span>At</span>Patient</a></div>
                            <p class="h5"><strong>Neupatientengewinnung</strong></p>
                        </div>
                        <ul class="box-blue-light list-unstyled text-left h4-5">
                            <li class="checkmark-green">Kostenfreie Erstellung Ihres individuellen Praxisprofils</li>
                            <li class="checkmark-green">Patienten fragen online Termine für Ihre Praxis an</li>
                        </ul>
                    </li>

                   <li class="col-xs-12 col-sm-6 col-md-4 text-center">
                        <div class="box-blue">
                            <p class="h3 text-light"><em>Können Patienten rund um die Uhr Termine bei Ihnen buchen?</em></p>
                            <img class="img-responsive" src="/static/img/praxisoptimierung/solutions-kalender.png">
                        </div>
                        <div class="box-white">
                            <div class="h1 text-logo"><a href="/praxisoptimierung/atwidget" title="Das Kalender-Widget für die eigenen Homepage von Arzttermine.de"><span>At</span>Widget</a></div>
                            <p class="h5"><strong>Online Kalender</strong></p>
                        </div>
                        <ul class="box-blue-light list-unstyled text-left h4-5">
                            <li class="checkmark-green">Durchgehende Online Terminvergabe auf Ihrer Homepage</li>
                            <li class="checkmark-green">Optimieren Sie die Auslastung der Praxis durch selbst eingestellte Termine</li>
                        </ul>
                    </li>

                    <li class="col-xs-12 col-sm-6 col-md-4 text-center">
                        <div class="box-blue">
                            <p class="h3 text-light"><em>Sind Sie immer für Ihre Patienten telefonisch erreichbar?</em></p>
                            <img class="img-responsive" src="/static/img/praxisoptimierung/solutions-fachtelefondienst.png">
                        </div>
                        <div class="box-white">
                            <div class="h1 text-logo"><a href="/praxisoptimierung/atcall" title="Keinen Anruf in Ihrer Praxis mehr verpassen mit AtCall von Arzttermine.de"><span>At</span>Call</a></div>
                            <p class="h5"><strong>Fachtelefondienst</strong></p>
                        </div>
                        <ul class="box-blue-light list-unstyled text-left h4-5">
                            <li class="checkmark-green">Verpassen Sie nie wieder Anrufe Ihrer wichtigen Patienten</li>
                            <li class="checkmark-green">Kümmern Sie sich um Ihre Patienten - wir übernehmen Ihr Telefon</li>
                        </ul>
                    </li>
                <ul>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div id="form" class="leading-border with-triangle box-grey-light radius">
                <h3>Zu welchen Lösungen wünschen Sie mehr Informationen?</h3>

                {assign var="contact_form_show_products" value=true}
                {include file="praxisoptimierung/_contact-form.tpl"}
            </div>
        </div>
    </div>
</div>

    {include file="praxisoptimierung/footer.tpl"}
