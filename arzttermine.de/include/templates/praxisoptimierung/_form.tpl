<style type="text/css">
div.form-group{
    margin-bottom: 2rem;
}
</style>
<div class="container">
    <div class="row">
        <div class="col">
            <form action="/aerzte/registrierung" name="edit" method="post">
                <div class="form-group">
                    <input type="text" name="practice_name" id="form-praxis" required>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label>Praxisname</label>
                </div>

                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <div class="form-group">
                            <input  type="tel" name="phone" id="form-phone" autocomplete="tel" required>
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label>Telefonnummer</label>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="form-group">
                            <input class="" type="email" name="email" id="form-email" autocomplete="email" required>
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label>E-Mail</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-xs-12">
                        <div class="form-group" style="padding-top: 5px;">
                            <input name="gender" type="radio" id="gender-1" value="1" style="width: 28px; display: inline-block;"> Frau
                            <input name="gender" type="radio" id="gender-0" value="0" style="width: 28px; display: inline-block; margin-left: 15px;"> Herr
                            <label>Anrede</label>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <input class="" type="text" name="first_name" id="form-zip" autocomplete="f-name" required>
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label>Vorname</label>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <input class="" type="text" name="last_name" id="form-zip" autocomplete="l-name" required>
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label>Nachname</label>
                        </div>
                    </div>
                </div>
                <button id="submit" class="btn btn-success btn-lg btn-block" type="submit">Wir rufen Sie zurück!</button>
                <div id="submit-response" style="display: none;"></div>
            </form>
        </div>
    </div>
</div>
