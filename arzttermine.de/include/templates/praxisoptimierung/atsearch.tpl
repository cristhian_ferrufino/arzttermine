{assign var="pageId" value="page-atsearch"}
{assign var="head_title" value="AtSearch - Suchmaschinenmarketing für Ärzte"}
{assign var="head_meta_description" value="Gewinnen Sie mit professionellen Anzeigen auf Google neue Patienten für Ihre Arztpraxis. Jetzt informieren."}
{assign var="head_meta_keywords" value=""}
{include file="praxisoptimierung/header.tpl"}

<div id="intro">
    <div class="container">
        <h1 class="text-logo with-subline"><span>At</span>Search</h1>
        <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
            <a href="/" itemprop="url" target="_blank">
                <span itemprop="title">Arzttermine.de</span>
            </a>
        </span> >
        <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
            <a href="/praxisoptimierung" itemprop="url">
                <span itemprop="title">Praxisoptimierung</span>
            </a>
        </span> >
        <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
            <a href="/praxisoptimierung/atsearch" itemprop="url">
                <span itemprop="title" content="AtSearch: Suchmaschinenmarketing für Ärzte">AtSearch</span>
            </a>
        </span>
        <h3 class="boxed-text-blue">
            <span>Werden Sie im Internet gefunden?</span><br>
            <span>Sichern Sie sich eine Top-Platzierung bei Google.</span><br>
            <span><strong>Wir setzen Sie ganz nach oben!</strong></span>
        </h3>
        <div class="col-md-5 text-center">
            <p class="lead text-left">
                <strong>90% der Patienten</strong> suchen über das Internet nach einem Arzt...
            </p>
            <div>
                <img id="intro-google" src="/static/img/praxisoptimierung/intro-google.png" alt="Suchbegriff Zahnarzt in Berlin">
            </div>
            <p>
                Unsere <strong>jahrelange Zusammenarbeit mit Ärzten
                    und Patienten</strong> im Online-Bereich macht uns zu Ihrem
                <strong>kompetenten Partner für Ihr Praxismarketing!</strong>
            </p>
        </div>
        <div class="col-md-7 visible-md visible-lg">
            <img class="img-responsive" src="/static/img/praxisoptimierung/intro-mac.png" alt="Suchergebnisse und Anzeigen für den Suchbegriff Zahnarzt in Berlin">
        </div>
    </div>
</div>

<div class="container">
    <div class="steps-container">
        <h2>Ihr einfacher Weg zu Neupatienten</h2>

        <p class="lead">
            Wir verwandeln diese Suchanfragen in <strong>Ihre neuen Patienten!</strong> Und so funktioniert es:
        </p>

        <ol class="clearfix list-unstyled">
            <li class="one-fifth">
                <img class="img-responsive hidden-xs" src="/static/img/praxisoptimierung/atsearch-step1.jpg" alt="Patienten nutzen Google, um Ärzte zu finden">
                <div class="leading-border with-triangle box-grey-light">
                    <div>1</div>
                    <p>Potentielle <strong>Patienten suchen online auf Google nach geeigneten Ärzten.</strong></p>
                </div>
            </li>
            <li class="one-fifth">
                <img class="img-responsive hidden-xs" src="/static/img/praxisoptimierung/atsearch-step2.jpg" alt="Patienten sehen Ihre Werbeanzeigen">
                <div class="leading-border with-triangle box-grey-light">
                    <div>2</div>
                    <p>Unser gezieltes <strong>Praxismarketing sorgt für die Auffindbarkeit Ihrer Praxis.</strong></p>
                </div>
            </li>
            <li class="one-fifth">
                <img class="img-responsive hidden-xs" src="/static/img/praxisoptimierung/atsearch-step3.jpg" alt="Patienten klicken auf die Anzeigen">
                <div class="leading-border with-triangle box-grey-light">
                    <div>3</div>
                    <p>Durch passende Anzeigentexte <strong>besuchen Neupatienten Ihre Webseite.</strong></p>
                </div>
            </li>
            <li class="one-fifth">
                <img class="img-responsive hidden-xs" src="/static/img/praxisoptimierung/atsearch-step4.jpg" alt="Der Patienten ruft in Ihrer Praxis an">
                <div class="leading-border with-triangle box-grey-light">
                    <div>4</div>
                    <p>Ihr Angebot spricht <strong>suchende Patienten</strong> an und sie <strong>kontaktieren Ihre Praxis.</strong></p>
                </div>
            </li>
            <li class="one-fifth">
                <img class="img-responsive hidden-xs" src="/static/img/praxisoptimierung/atsearch-step5.jpg" alt="Patient kommt in die Praxis, um sich von Ihnen behandeln zu lassen">
                <div class="leading-border with-triangle box-grey-light">
                    <div>5</div>
                    <p>Sie machen einen Termin aus und <strong>der Patient wird zu Ihrem Neupatienten.</strong></p>
                </div>
            </li>
        </ol>
    </div>
</div>

<div class="container">
    <div class="vertical-box-container leading-border with-triangle box-grey-light radius">
        <h4><strong>Wir sorgen dafür, dass Ihre Webseite...</strong></h4>
        <ul class="clearfix list-inline lead">
            <li class="col-sm-4 checkmark-green-circle">... <strong>richtig plaziert</strong> wird</li>
            <li class="col-sm-4 checkmark-green-circle">... <strong>Aufmerksamkeit</strong> erlangt</li>
            <li class="col-sm-4 checkmark-green-circle">... und zu <strong>Terminanfragen</strong> führt!</li>
        </ul>
    </div>
</div>

<div class="container">
    <div id="advantages">
        <h2>Vorteile durch Suchmaschinenwerbung für Ihre Praxis</h2>

        <ul class="checkmarks list-unstyled row">
            <li class="checkmark-green col-xs-12 col-md-7 h4-5">
                <strong>Lokale Auffindbarkeit</strong>
                <ul>
                    <li>Geographischer Fokus - auf Ihren jeweiligen Standort angepasst</li>
                </ul>
            </li>
            <li class="checkmark-green col-xs-12 col-md-7 h4-5">
                <strong>Steigende Kosteneffizienz</strong>
                <ul>
                    <li>Gleichmäßige Verteilung des Monatsbudgets zur Erreichung einer gleichbleibenden Sichtbarkeit und einer langfristigen Patientenbindung</li>
                </ul>
            </li>
            <li class="checkmark-green col-xs-12 col-md-7 h4-5">
                <strong>Nachhaltiges Wachstum</strong>
                <ul>
                    <li>Mit der optimalen Ausrichtung von Anzeigen steigern wir langfristig Ihre Bekanntheit im Internet</li>
                </ul>
            </li>
            <li class="checkmark-green col-xs-12 col-md-7 h4-5">
                <strong>Individuelle Keyword-Strategie</strong>
                <ul>
                    <li>Je nach Fachbereich und Ihrem individuellen Wunsch nach Neupatienten wird Ihre Kampagne stetig optimiert</li>
                </ul>
            </li>
            <li class="checkmark-green col-xs-12 col-md-7 h4-5">
                <strong>Transparenz</strong>
                <ul>
                    <li>Messung von Besuchen und konkreten Patientenanfragen, welche über die Kampagne erzielt werden</li>
                    <li>Monatliches oder wöchentliches Reporting über die Aktivitäten Ihrer Marketing-Kampagnen</li>
                </ul>
            </li>
        </ul>

        <div class="box-grey-light  text-center ">
            <p class="boxed-text-blue">
                <span class="center-block h4-5">
                    Ihre Bilanz: <strong>Steigerung der Patientenanfragen, Gewinnung von Neupatienten und Erhöhung des Bekanntheitsgrades</strong>
                </span>
            </p>
        </div>
    </div>
</div>

<div class="container">
    <div id="quality">
        <h2>Unser Qualitätsanspruch</h2>

        <section class="col-md-4">
            <img class="img-responsive" src="/static/img/praxisoptimierung/cards.png" alt="Beratung, Konzeption, Umsetzung und Optimierung aus einer Hand">
        </section>
        <section class="col-md-4">
            <div class="box-grey-light">
                <h4><strong>Service aus einer Hand</strong></h4>
                <p>Um das bestmögliche Resultat im Suchmaschinenmarketing zu erzielen, erhalten Sie bei uns den kompletten Service aus einer Hand.</p>
            </div>
        </section>
        <section class="col-md-4">
            <img class="img-responsive" src="/static/img/praxisoptimierung/macair.png" alt="Umsatzsteigerung durch die Optimierung mit Know-How">
        </section>
    </div>
</div>

<div class="container">
    <div id="form" class="leading-border with-triangle box-grey-light radius">
        <h3>Starten Sie noch heute und gewinnen Sie neue Patienten!</h3>

        {assign var="contact_form_show_products" value=false}
        {assign var="contact_form_checkbox_selected" value='atsearch'}
        {include file="praxisoptimierung/_contact-form.tpl"}
    </div>
</div>

{include file="praxisoptimierung/footer.tpl"}
