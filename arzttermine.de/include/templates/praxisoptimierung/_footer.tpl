    <footer style="max-width: none; margin-bottom: 0">
        <div class="Container">
            <div class="row">
                    <div class="col-md-5 offset-md-1">
                        <a href="/praxisoptimierung" class="h3 text-logo white"><span>At</span>Praxisoptimierung</a>
                        <ul class="list-unstyled">
                            <li>
                                <a href="/praxisoptimierung/atwidget" title="AtSeatch - Suchmaschinenmarketing für Ärzte" class="h5 text-logo white"><span>At</span>Widget</a>
                            </li>
                            <li>
                                <a href="/praxisoptimierung/atweb" title="AtWeb - Die eigene Website" class="h5 text-logo white"><span>At</span>Web</a>
                            </li>
                            <li>
                                <a href="/praxisoptimierung/atsearch" title="AtSeatch - Suchmaschinenmarketing für Ärzte" class="h5 text-logo white"><span>At</span>Search</a>
                            </li>
                            <li>
                                <a href="/praxisoptimierung/atrep" title="AtSeatch - Suchmaschinenmarketing für Ärzte" class="h5 text-logo white"><span>At</span>Rep</a>
                            </li>
                            <li>
                                <a href="/lp/praxis-marketing/praxis-marketing.html" title="AtPatient - Neue Patienten über die Plattform Arzttermine.declass=" class="h5 text-logo white"><span>At</span>Patient</a>
                            </li>
                            <li>
                                <a href="/praxisoptimierung/atcall" title="AtCall - Der Fachtelefondienst" class="h5 text-logo white"><span>At</span>Call</a>
                            </li>
                        </ul>
                    </div>

                    <div class="col-sm">
                        <br/><br/>
                        <h4>Kontakt</h4>
                        <ul class="list-unstyled">
                            <li>
                                docbiz UG (haftungsbeschränkt)<br />
                                Rosenthalerstraße 51<br />
                                10178 Berlin<br />
                                <a href="tel:030609840272" class="h4">030 / 609 840 272</a>
                            </li>
                        </ul>
                    </div>

                    <div class="col-sm">
                        <div class="social-media">
                            <a href="https://de-de.facebook.com/Arzttermine" target="_blank"><img src="/static/img/praxisoptimierung/icon_fb.png" alt="facebook"></a>
                            <a href="https://plus.google.com/share?url={$google_share_url}" target="_blank"><img src="/static/img/praxisoptimierung/icon_gp.png" alt="googleplus"></a>
                            <a href="https://twitter.com/intent/tweet?text=AtPraxisoptimierung%20%7C%20So%20optimiere%20ich%20mein%20Praxis%20im%20Internet%20@Arzttermine" target="_blank"><img src="/static/img/praxisoptimierung/icon_tw.png" alt="twitter"></a>
                        </div>
                    </div>

            </div>

            <div class="row">
                <div class="legal-info" style="width: 100%">
                    <div class="col-md-9 offset-md-1">
                        <span class="legal">
                            &copy; 2014 AtPraxisoptimierung - Alle Rechte vorbehalten
                        </span>

                            <div class="links" style="float: right">
                                <a href="/impressum" title="Impressum von Arzttermine.de" target="_blank" >Impressum</a>
                                *
                                <a href="/datenschutz" title="Datenschutz von Arzttermine.de" target="_blank" >Datenschutz</a>
                            </div>

                    </div>
                </div>
            </div>
        </div>
    </footer>

    <script type="text/javascript">
        function trackGoogleConversion(google_conversion_id,google_conversion_label) {
            var image = new Image(1,1);
            image.src = "https://www.googleadservices.com/pagead/conversion/"+google_conversion_id+"/?label="+google_conversion_label+"&script=0";
        }
    </script>

    <script type="text/javascript">
        $(function() {
            function validateForm() {
                var isValid = true;

                $('#form').find('input.is-required').removeClass('has-error').each(function () {
                    $(this).parent().removeClass('has-error');
                    if (!$(this).val()) {
                        $(this).parent().addClass('has-error');
                        isValid = false;
                    }
                });
                return isValid;
            }

            $("#submit").on('click', function( event) {
                event.preventDefault();
                if (validateForm()) {
                    $.ajax({
                        type: "POST",
                        url: "/praxisoptimierung/kontakt",
                        data: $("#form form").serialize()
                    })
                        .done(function( result ) {
                            if (result.data == 'ok') {
                                $("#submit").hide('fast');
                                $("#submit-response").hide('fast').html("<h4 style='color: green;'>Vielen Dank f&uuml;r Ihre Anfrage, wir werden uns in K&uuml;rze bei Ihnen melden!</h4>").show('fast');
{if $CONFIG.GOOGLE_ANALYTICS_ACTIVATE}
                                trackGoogleConversion(990044011, 'o4feCI_OklYQ676L2AM');
{/if}
                            } else {
                                $("#submit-response").hide('fast').html("<h4 style='color: red;'>Ein Fehler ist aufgetreten!</h4>").show('fast');
                            }
                        });
                }
                // dont send the form again
                return false;
            });
        });
    </script>

    <script type="text/javascript">
        var leady_track_key="y39FXY28qIbZZ8Ib";
        var leady_track_server=document.location.protocol+"//t.leady.com/";
        (function(){
            var l=document.createElement("script");l.type="text/javascript";l.async=true;
            l.src=leady_track_server+leady_track_key+"/L.js";
            var s=document.getElementsByTagName("script")[0];s.parentNode.insertBefore(l,s);
        })();
    </script>
</body>
</html>
