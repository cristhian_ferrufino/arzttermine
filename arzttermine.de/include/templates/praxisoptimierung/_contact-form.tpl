<form action="#" name="edit" method="post" enctype="application/x-www-form-urlencoded">
{if $contact_form_checkbox_selected}
    <input type="hidden" name="{$contact_form_checkbox_selected}" value="on-single">
{/if}
{if $contact_form_show_products}
    <div class="row">
        <div class="col-xs-12 col-sm-10">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="checkbox">
                        <label class="checkbox text-logo h4">
                            <input type="checkbox" name="atsearch"><span>At</span>Search
                        </label>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="checkbox">
                        <label class="checkbox text-logo h4">
                            <input type="checkbox" name="atweb"><span>At</span>Web
                        </label>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="checkbox">
                        <label class="checkbox text-logo h4">
                            <input type="checkbox" name="atrep"><span>At</span>Rep
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class=" col-xs-10">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="checkbox">
                        <label class="checkbox text-logo h4">
                            <input type="checkbox" name="atpatient"><span>At</span>Patient
                        </label>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="checkbox">
                        <label class="checkbox text-logo h4">
                            <input type="checkbox" name="atwidget"><span>At</span>Widget
                        </label>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="checkbox">
                        <label class="checkbox text-logo h4">
                            <input type="checkbox" name="atcall"><span>At</span>Call
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
{/if}
    <div class="row cleared">
        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="form-group">
                <label class="control-label">Ansprechpartner</label>
                <input class="form-control is-required" type="text" name="name" id="form-name">
            </div>
            <div class="form-group">
                <label class="control-label">Praxisname</label>
                <input class="form-control is-required" type="text" name="praxis" id="form-praxis">
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="form-group">
                <label class="control-label">Telefonnummer</label>
                <input class="form-control is-required" type="tel" name="phone" id="form-phone">
            </div>
            <div class="form-group">
                <label class="control-label optional">E-Mail (optional, falls wir Sie nicht erreichen)</label>
                <input class="form-control" type="email" name="email" id="form-email">
            </div>
        </div>
    </div>
    <div class="row cleared">
        <div class="col-xs-12 col-md-3">
            <div class="form-group">
                <label class="control-label optional">Gewünschter Rückruftag (optional)</label>
                <select class="form-control" name="date">
                    <option>Wochentag</option>
                    <option value="Montags">Montags</option>
                    <option value="Dienstags">Dienstags</option>
                    <option value="Mittwochs">Mittwochs</option>
                    <option value="Donnerstags">Donnerstags</option>
                    <option value="Freitags">Freitags</option>
                    <option value="Samstag">Samstag</option>
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-md-3">
            <div class="form-group">
                <label class="control-label optional">Gewünschte Uhrzeit (optional)</label>
                <select class="form-control" name="time">
                    <option>Uhrzeit</option>
                    <option value="08:00-10:00 Uhr">08:00-10:00 Uhr</option>
                    <option value="10:00-12:00 Uhr">10:00-12:00 Uhr</option>
                    <option value="12:00-16:00 Uhr">12:00-16:00 Uhr</option>
                    <option value="16:00-20:00 Uhr">16:00-20:00 Uhr</option>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-4">
            <button id="submit" class="btn btn-success btn-lg btn-block" type="submit">Wir rufen Sie zurück!</button>
            <div id="submit-response" style="display: none;"></div>
        </div>
        <div class="col-xs-12 col-md-4">
            Rückruf möglich <strong>Mo-Sa, 08:00 - 20:00 Uhr.</strong>
            <br class="hidden-xs">
            Der Rückruf ist völlig kostenfrei und unverbindlich.
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-7">
            Natürlich können Sie sich auch direkt bei uns melden, unter der:<br />
            <a href="tel:030609840272" class="h3">030 / 609 840 272</a>
        </div>
    </div>
</form>
