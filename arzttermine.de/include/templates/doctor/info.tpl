<section itemscope class="additional-info">
  <h2>Weitere Informationen zu diesem Arzt</h2>

    {if $user->showReviews()}
        {assign var="reviews" value=$user->getReviews()}
        {if $reviews|count}
          <section id="reviews" itemscope itemtype="http://schema.org/Review">
              <div class="row">
                  <div class="col-sm-3">
                      <h3>Bewertungen</h3>
                  </div>
                  <div class="col-sm-8">
                      <div class="additional-info-text">
                          {foreach from=$reviews item=review}
                              {include file="_review-block.tpl"}
                          {/foreach}
                      </div>
                  </div>
              </div>
          </section>
        {/if}
    {/if}

    {include file="_doctor-cms-info.tpl"}

  <section class="info-text-container">
    <div style="clear:both" class="non-pay profile-disclaimer">
        {if !$user->hasContract()}
            {$app->_('Diese Praxis ist noch kein Partner von Arzttermine.de, dennoch ist Ihnen unser kostenfreier Buchungsservice gerne bei der Terminvereinbarung behilflich.')}
        {/if}
    </div>
  </section>
</section>