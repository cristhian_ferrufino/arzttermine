{strip}
    {assign var="template_body_class" value="profile-page"}
    {assign var="additional_body_classes" value="doctor"}
    {assign var="activate_freespee_phonenumber_tracking" value=true}
    {include file="header.tpl"}
{/strip}

<div class="container">
    <main itemscope itemtype="http://schema.org/Physician">
        {include file="doctor/summary.tpl"}
        {include file="doctor/calendar.tpl"}
        {include file="doctor/info.tpl"}
    </main>
</div>

{include file="_referral-code-support.tpl"}

{include file="footer.tpl"}