<section class="calendar-container">
  {assign var="location" value=$providers[0]->getLocation()}
  {if $location->getIntegration()->getId() == 8}
    <h2>Fragen Sie Ihren Wunschtermin an</h2>
  {else}
    <h2>Wählen Sie Ihren Wunschtermin aus</h2>
  {/if}
  {assign var="isPKV" value=$insurance->getId() == 1}{* Refactor for a real check *}


  <div class="row" id="providers" data-date-start="{$search->getStart()->getDate()}" data-date-end="{$search->getEnd()->getDate()}" data-date-days="{$search->getDayCount()}" style="margin-top: 1rem">
    {foreach from=$providers item="provider"}
            <div class="col-sm-6 hidden-md-down">
                {assign var="location" value=$provider->getLocation()}
               <div class="row">
                   <div class="col-sm-4">
                       <img itemprop="map" src="//maps.googleapis.com/maps/api/staticmap?center={$location->getLat()},{$location->getLng()}&markers={$location->getLat()},{$location->getLng()}&zoom=13&size=160x160&sensor=false&v=3.exp&key=AIzaSyA-cGD6Wm_86W_BzBctDnMMD5j7iM0is1I">
                   </div>
                   <div class="col-sm">
                       <div class="location-info" itemscope itemtype="http://schema.org/MedicalClinic">
                           <h4>Praxis</h4>
                           <div class="name" itemprop="name"><a href="{url path="practice" params=['slug' => $location->getSlug(), 'insurance' => $insurance->getSlug()]}">{$location->getName()}</a></div>
                           <div class="address" itemprop="address">{$location->getAddress()}</div>
                           {if $location->hasPhoneVisible() && !$location->hasAppointmentEnquiryIntegration()}
                               <div><span itemprop="telephone"><a href="tel:{$location->getPhoneVisible()}">{$location->getPhoneVisible()}</a></span></div>
                           {/if}
                       </div>
                   </div>
               </div>
            </div>
            <div class="col-sm-6 hidden-md-down">
                <div class="row">
                    <div class="col">
                        {assign var="appointmentsBlock" value=$provider->getAppointmentsBlock()}
                        {include file="appointment/_appointments-block-calendar.tpl"}
                    </div>
                </div>
            </div>

            <div class="col-sm hidden-lg-up">
                {assign var="location" value=$provider->getLocation()}
                <div class="row">
                    <div class="col-sm-4">
                <img itemprop="map" src="//maps.googleapis.com/maps/api/staticmap?center={$location->getLat()},{$location->getLng()}&markers={$location->getLat()},{$location->getLng()}&zoom=13&size=160x160&sensor=false&v=3.exp&key=AIzaSyA-cGD6Wm_86W_BzBctDnMMD5j7iM0is1I">
                    </div>
                    <div class="col-sm">
                        <div class="location-info" itemscope itemtype="http://schema.org/MedicalClinic">
                            <h4>Praxis</h4>
                            <div class="name" itemprop="name"><a href="{url path="practice" params=['slug' => $location->getSlug(), 'insurance' => $insurance->getSlug()]}">{$location->getName()}</a></div>
                            <div class="address" itemprop="address">{$location->getAddress()}</div>
                            {if $location->hasPhoneVisible() && !$location->hasAppointmentEnquiryIntegration()}
                                <div><span itemprop="telephone"><a href="tel:{$location->getPhoneVisible()}">{$location->getPhoneVisible()}</a></span></div>
                            {/if}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col hidden-lg-up">
                <div class="row" style="margin-top: 1rem">
                    <div class="col">
                        {assign var="appointmentsBlock" value=$provider->getAppointmentsBlock()}
                        {include file="appointment/_appointments-block-calendar.tpl"}
                    </div>
                </div>
            </div>
    {/foreach}
  </div>
    <div class="row">
        <div class="col">
            <div class="insurance-selector">
                <a class="insurance-selector-button public {if !$isPKV} active{/if}" href="{if $isPKV}{$user->getUrl()}/gkv{else}#{/if}">gesetzlich</a>
                <a class="insurance-selector-button private {if $isPKV} active{/if}" href="{if !$isPKV}{$user->getUrl()}/pkv{else}#{/if}">privat</a>
            </div>
        </div>
    </div>
  {if $pagination}
    <div class="pagination">{$pagination}</div>
  {/if}
</section>