<section class="summary">
  <div class="photo">
    <img  itemscope itemtype="http://schema.org/ImageObject" itemprop="url" src="{$user->getProfileAssetUrl($smarty.const.ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED)}" alt="{alt($user->getName())} {$user->getMedicalSpecialityIdsText()}" width="100%">
  </div>

  <div class="right">
    {if $user->hasContract() == false}
      <div class="is-this-your-profile"><a href="/lp/praxis-marketing/praxis-marketing.html">Ist das Ihr Eintrag?</a></div>
    {/if}

    <div class="headline">
    <h1 itemprop="name">{$user->getName()}</h1>
      {if $providers[0]->getLocation()->hasPhoneVisible()}
        <div itemprop="telephone"><a href="tel:{$providers[0]->getLocation()->getPhoneVisible()}">{$providers[0]->getLocation()->getPhoneVisible()}</a></div>
      {else}
        <div itemprop="telephone"><a href="tel:08002222133">0800 - 22 22 133</a></div>
      {/if}
    </div>

    {if $user->showReviews()}
      <div class="ratings hidden-sm-down" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
        <meta itemprop="reviewcount" content="{$user->countReviews()}" />
        <h4>Bewertung</h4>
        <div>
          <div class="rating-name">{$this->_('Behandlung')}</div>
            {include file="_star-rating.tpl" rating=$user->getRating('rating_1')}
        </div>
        <div>
          <div class="rating-name">{$this->_('Wartezeit')}</div>
            {include file="_star-rating.tpl" rating=$user->getRating('rating_2')}
        </div>
        <div>
          <div class="rating-name">{$this->_('Gesamtbewertung')}</div>
            {include file="_star-rating.tpl" rating=$user->getRating('rating_3')}
        </div>
      </div>
      <div class="ratings hidden-md-up" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
        <meta itemprop="reviewcount" content="{$user->countReviews()}" />
        <h4>Bewertung</h4>
        <div class="row">
          <div class="col">
            <div class="rating-name">{$this->_('Behandlung')}</div>
              {include file="_star-rating.tpl" rating=$user->getRating('rating_1')}
          </div>
          <div class="col">
            <div class="rating-name">{$this->_('Wartezeit')}</div>
              {include file="_star-rating.tpl" rating=$user->getRating('rating_2')}
          </div>
          <div class="col">
            <div class="rating-name">{$this->_('Gesamtbewertung')}</div>
              {include file="_star-rating.tpl" rating=$user->getRating('rating_3')}
          </div>
        </div>
      </div>
    {/if}

    <div class="specialties" itemscope itemtype="http://schema.org/Physician">
      <h4>Fachgebiete</h4>
      <span itemprop="medicalSpecialty">{if $user->hasPreferredMedicalSpecialtyText()}{$user->getPreferredMedicalSpecialtyText()}{else}{$user->getMedicalSpecialityIdsText(', ')}{/if}</span>
    </div>
  </div>
</section>