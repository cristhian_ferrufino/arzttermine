<section itemscope itemtype="http://data-vocabulary.org/Person" class="result s33 x-small">
    <div class="wrapper">
        <header>
          
          <div class="left">
            <h2 itemprop="name" class="name"><a itemprop="url" href="{$providerRow->getUser()->getUrlWithFilter($filter)}">{$providerRow->getUser()->getDoctorFullName()}</a></h2>
            <p itemprop="address" itemscope itemtype="http://data-vocabulary.org/Address" class="address">
              <!-- Geo info - SEO readable -->
              <span class="microdata-hidden" itemprop="geo" itemscope itemtype="http://schema.org/Place">
                <span itemscope itemtype="http://schema.org/GeoCoordinates">
                  <span property="latitude" content="37.4149"></span>
                  <span property="longitude" content="-122.078"></span>
                </span>
              </span>
              1.8km | 
              <span  itemprop="address" itemscope itemtype="http://data-vocabulary.org/Address">
                <span itemprop="streetAddress">{$providerRow->getLocation()->getStreet()}</span>
                <span class="microdata-hidden" itemprop="addressLocality">{$providerRow->getLocation()->getCity()}</span>
              </span>
            </p>
          </div>

          <div class="right">
            <div itemprop="telephone" class="phone">{$providerRow->getLocation()->getPhoneVisible()}</div>
            <div class="rating">
              <meta itemprop="rating" content="4.5" />
            </div>
          </div>
          
        </header>

        <div class="photo">
          <img itemprop="photo" src="{$providerRow->getUser()->getProfileAssetUrl($smarty.const.ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED)}" alt="{$providerRow->getUser()->getFirstName()}">
        </div>


<!--
<a href="{$providerRow->getUser()->getUrlWithFilter($filter)}" 
   title="{$providerRow->getUser()->getFirstName()} {$providerRow->getLocation()->getCity()}">
    <img src="{$providerRow->getUser()->getProfileAssetUrl($smarty.const.ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED)}" 
         alt="{$providerRow->getUser()->getFirstName()} " />  
</a>
--> 
<!--
<a class="name"  href="{$providerRow->getUser()->getUrlWithFilter($filter)}" 
   title="{$providerRow->getUser()->getFirstName()}  {$providerRow->getLocation()->getCity()}">
    {$providerRow->getUser()->getDoctorFullName()}
</a>
-->

<div class="medical-specialties">{$providerRow->getUserMedicalSpecialityIdsText()}</div>

<!--
{$providerRow->getUser()->getRatingHtml('rating_average', '', $location->getId())}
-->

{if $providerRow->getLocation()->getIntegrationId()==8 && !$providerRow->getUser()->hasContract()}
  <div style="clear:both" class="non-pay">{$app->_('Diese Praxis ist noch kein Partner von Arzttermine.de.')}</div>
{/if}

    </div> <!-- wrapper -->
</section>
{*}
<div class="provider intro">
    <div class="gfx">
        <a href="{$providerRow->getUser()->getUrlWithFilter($filter)}" title="{$providerRow->getUser()->getName()} {$providerRow->getLocation()->getCity()}">
            <img src="{$providerRow->getUser()->getProfileAssetUrl($smarty.const.ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED)}" alt="{$providerRow->getUser()->getName()}" />
        </a>
    </div>
    <div class="info">
        <div class="name">
            <a class="name" href="{$providerRow->getUser()->getUrlWithFilter($filter)}" 
               title="{$providerRow->getUser()->getName()} {$providerRow->getLocation()->getCity()}">
                {$providerRow->getUser()->getName()}
            </a></div>
        <div class="medical-specialties">{if $user->getSlug()!=='dr-med-sibylle-greiner'}{$user->getMedicalSpecialityIdsText()}{else}Frauenärztin/<br>Gynäkologin<br>Homöopathin{/if}</div>
        {$providerRow->getUser()->getRatingHtml('rating_average', '', $location->id)}
        <div class="address street">{$providerRow->getLocation()->getStreet()}</div>
        <div class="address cityzip">{$providerRow->getLocation()->getZip()} {$providerRow->getLocation()->getCity()}</div>
        {if $providerRow->getLocation()->getIntegrationId()==8 && $providerRow->getUser()->hasContract()==false}
			<div style="clear:both" class="non-pay">{$app->_('Diese Praxis ist noch kein Partner von Arzttermine.de.')}</div>
		{/if}
       	<div class="phone"><div class="phone-icon"></div>{$providerRow->getLocation()->getPhoneVisible()}</div>
    </div>
	{if $providerRow->getLocation()->getIntegrationId()==8 && $providerRow->getUser()->hasContract()==false}
	<div style="clear:both" class="non-pay">{$app->_('Diese Praxis ist noch kein Partner von Arzttermine.de.')}</div>
	{/if}
</div>
{/*}
