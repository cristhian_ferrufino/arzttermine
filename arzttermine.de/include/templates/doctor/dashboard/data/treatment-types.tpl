<div class="specialty-treatments" id="treatment-type-{$medical_specialty->getId()}">
    <h4>{$medical_specialty->getName()} Behandlungsarten</h4>
    <ul>
        {foreach from=$treatment_types item=treatment_type}
            {assign var="isChecked" value=in_array($treatment_type->getId(), $active_treatment_types)}
            <li>
                <label>
                    <input type="checkbox" name="treatment_types[]" value="{$treatment_type->getId()}" {if $isChecked}checked{/if}>
                    {$treatment_type->getName()}
                </label>
            </li>
        {/foreach}
    </ul>
</div>
