<!DOCTYPE html>
<html>
<head>
    <title>{$this->getHeadTitle()}</title>

    <link rel="shortcut icon" href="/static/images/assets/favicon.ico">

    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

    <link rel="stylesheet" type="text/css" href="/static/css/common.min.css">
    <link rel="stylesheet" type="text/css" href="/static/css/{$template_body_class}.min.css">

    {literal}
        <script type='text/javascript'>
            var AT = AT || {};

            AT.q=[];
            AT._defer=function(f){
                AT.q.push(f);
            };
        </script>
    {/literal}

    <!--[if lte IE 9]>
    <script src="/static/js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript">
        document.createElement("header");
        document.createElement("nav");
        document.createElement("aside");
        document.createElement("main");
        document.createElement("article");
        document.createElement("section");
        document.createElement("footer");
    </script>
    <![endif]-->

    <!--[if gte IE 9]><!-->
    <link href='//fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Gudea:100,300,400,700,900' rel='stylesheet' type='text/css'>
    <!--<![endif]-->

    <meta name="keywords" content="{$this->getMetaTag('keywords')}" />
    <meta name="description" content="{$this->getMetaTag('description')}" />
</head>

{* Alert bar setup *}
{assign var="hasAlert" value=$this->hasStatusMessages()}

<body class="{$template_body_class}{if isset($additional_body_classes)} {$additional_body_classes}{/if}{if $hasAlert} has-alert{/if}"{if isset($webPageType)} itemscope itemtype="http://schema.org/{$webPageType}"{/if}>
{include file="js/tagmanager/tagmanager.tpl"}

<header>
    <div class="top-bar">

    </div>

    <div class="container">
        {include file="logo.tpl"}

        {* The alert bar *}
        {include file="_status-message.tpl"}

    </div>
</header>
