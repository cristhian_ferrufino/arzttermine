{assign var="template_body_class" value="profile-page"}
{assign var="additional_body_classes" value="doctor account-dashboard praxis"}
{include './header.tpl'}

{$this->addJsInclude($this->getStaticUrl('js/doctor-profile.js'))}

{assign var=query_slug value="?praxis=$slug"}

<div class="container">
    <main>
        <section class="summary" >
            <div class="headline">
                <h1>{$location->getName()}</h1>
                {include file="doctor/dashboard/logout.tpl"}
            </div>
            <nav>
                <ul>
                    <li><a href="{url path='doctor_account'}">Mein Arztprofil</a></li>
                    {if $location->getIntegrationId() == 1}
                        <li><a href="{url path='doctor_planner'}">Kalender</a></li>
                    {/if}
                    {if $locations}
                        <li class="active"><a href="{url path='doctor_practice'}{$query_slug}">Praxisprofil</a></li>
                        <li><a href="{url path='doctor_patients'}{$query_slug}">Buchungen</a></li>
                        <li><a href="{url path='doctor_reporting'}{$query_slug}">Reporting</a></li>
                    {/if}
                </ul>
            </nav>
        </section>

        <section>
            <form action="" method="post">
                <div class="profile-photo">
                    <img src="{$SITE_PATH}{if $profile_pic}{$profile_pic}{else}/ngs/img/default_avatar.jpg{/if}" alt="Praxis Profilbild">
                </div>

                <div class="text-data">
                    <h3>Praxisname</h3>
                    <div>
                        <select id="praxen" class="hyperlink">
                            {foreach from=$locations item=location_item}
                                <option value="?praxis={$location_item->getSlug()}" {if $location_id == $location_item->getId()}selected{/if}>{$location_item->getName()}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>

                <div class="text-data">
                    <h3>Anschrift</h3>
                    <div>{$location->getStreet()}</div>
                </div>

                <div class="text-data">
                    <h3>PLZ</h3>
                    <div>{$location->getZip()}</div>
                </div>

                <div class="text-data">
                    <h3>Ort</h3>
                    <div>{$location->getCity()}</div>
                </div>

                <div class="text-data">
                    <h3>Telefon</h3>
                    <div class="editable-container">
                        <input type="text" name="phone" value="{$location->getPhone()}">
                    </div>
                </div>

                <div class="text-data">
                    <h3>Website</h3>
                    <div class="editable-container">
                        <input type="text" name="www" value="{$location->getWww()}">
                    </div>
                </div>

                <div class="text-data">
                    <h3>Fachrichtung</h3>
                    <div class="editable-container specialties">
                        {foreach from=$all_specialties item=specialty}
                            {if in_array($specialty->getId(), $specialties)}
                                <label>
                                    <input type="checkbox" name="medical_specialties_ids[]" value="{$specialty->getId()}" checked>
                                    {assign var="specialty_selected" value="true"}
                                    {$specialty->getName()}
                                </label>
                            {/if}
                        {/foreach}

                        {if isset($specialty_selected)}
                            <a role="button" class="toggle-specialties">Zeige <span class="show-more">mehr</span><span class="show-less">weniger</span></a>
                        {/if}

                        <div class="unselected-specialties {if !isset($specialty_selected)}visible{/if}">
                            {foreach from=$all_specialties item=specialty}
                                {if !in_array($specialty->getId(), $specialties)}
                                    <label>
                                        <input type="checkbox" name="medical_specialties_ids[]" value="{$specialty->getId()}">
                                        {$specialty->getName()}
                                    </label>
                                {/if}
                            {/foreach}
                        </div>
                    </div>
                </div>

                <div class="text-data">
                    <h3>Ärzte dieser Praxis</h3>
                    <div>
                        {foreach $doctors as $praxisDoctor}
                            <section class="praxis-doctor">
                                <img src="{$praxisDoctor->getProfileAssetUrl()}" alt="{$praxisDoctor->getFullName()}">
                                <h4>{$praxisDoctor->getFullName()}</h4>
                                {foreach $praxisDoctor->getMedicalSpecialties() as $specialty}
                                    <span>{$specialty->getName()}</span>
                                {/foreach}
                                <a href="{url path="doctor_profile_no_insurance" params=['slug' => $praxisDoctor->getSlug()]}">Arztprofil ansehen</a>
                                <a href="{url path='doctor_account'}?arzt={$praxisDoctor->getSlug()}">Arztprofil bearbeiten</a>
                            </section>
                        {/foreach}
                    </div>
                </div>
                <input type="hidden" name="uid" value="{$location->getId()}" />

                <div class="buttons-container">
                    <button class="save">Speichern</button>
                </div>

            </form>
        </section>

    </main>
</div>

<script>
    var doctorsMedSpecIdsArray = {json_encode($specialties)} || [];
    var docAccountApiUrl = "{url path='doctor_api'}";
    var docAccountUrl = '{url path='doctor_practice'}';
</script>

{include './footer.tpl'}
