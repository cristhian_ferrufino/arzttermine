{assign var="template_body_class" value="two-cols-with-form"}
{assign var="additional_body_classes" value="doctor-registration confirmation"}
{assign var="isSales" value=true}
{assign var="hasFullHeader" value="false"}
{include '../header.tpl'}

<div class="container">
    <main>
        <h1>Fertig!</h1>
            <section id="info-block">
                <section class="infobox infobox-grey-light cta-phone">
                    <h4><em>Haben Sie noch Fragen?</em></h4>
                    <p>Wir sind 24 Stunden kostenfrei für Sie erreichbar:</p>
                    {include file="phone-numbers/phone-sales.tpl"}
                </section>
            </section>

            <section class="infobox infobox-blue-light what-next">
                <h4><em>So geht es weiter:</em></h4>
                <ul>
                    <li class="what-next-step-1">
                        <div><em>Schritt 1</em></div>
                        <div>Wir schicken Ihnen eine Bestätigungs-E-Mail.</div>
                    </li>
                    <li class="what-next-step-2">
                        <div><em>Schritt 2</em></div>
                        <div>Wir kontaktieren Sie persönlich um die Details Ihres Profils zu klären.</div>
                    </li>
                    <li class="what-next-step-3">
                        <div><em>Schritt 3</em></div>
                        <div>Freuen Sie sich über qualifizierte Patientenanfragen entsprechend Ihrer Spezialisierungen.</div>
                    </li>
                </ul>

                <form id="booking-form" action="{url path="doctor_register"}?step=4" method="post" enctype="application/x-www-form-urlencoded" >
                    <div class="form-element">
                        <label>Grund meiner Kontaktaufnahme/Fragen:</label>
                        <textarea name="registration_reasons" data-track="true">{$registration_reasons}</textarea>
                    </div>

                    <div class="form-element">
                        <label>Wie sind Sie auf Arzttermine.de aufmerksam geworden?</label>
                        <select name="registration_funnel" data-track="true">
                            <option value="">Bitte wählen</option>
                            <option value="Kollegen">Kollegen</option>
                            <option value="Internetrecherche">Internetrecherche</option>
                            <option value="Anzeigen">Anzeigen</option>
                            <option value="Kontaktaufnahme durch Arzttermine.de">Kontaktaufnahme durch Arzttermine.de</option>
                            <option value="Sonstige">Sonstige</option>
                        </select>
                    </div>
                    <div class="form-element hidden funnel-other">
                        <label>Sonstige</label>
                        <input type="text" name="registration_funnel_other" value="{$registration_funnel_other}">
                    </div>

                    <input type="hidden" name="uid" value="{$uid}"/>

                    <div class="button-wrapper"><button type="submit">Bestätigen</button></div>

                    <script type="text/javascript">
                        var AT = AT || { };
                        (function(_, $, doc, win, und) {
                            $('select[name=registration_funnel]').change(function(){
                                if ($(this).val() === 'Sonstige') {
                                    $('.form-element.funnel-other').removeClass('hidden');
                                } else {
                                    $('.form-element.funnel-other').addClass('hidden');
                                }
                            });
                        }(AT, jQuery, document, window));
                    </script>
                </form>

            </section>


    </main>
</div>

{include '../footer.tpl'}
