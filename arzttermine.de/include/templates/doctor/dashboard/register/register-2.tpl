{assign var="template_body_class" value="two-cols-with-form"}
{assign var="additional_body_classes" value="doctor-registration confirmation"}
{assign var="use_minimal_footer" value=true}
{assign var="isSales" value=true}
{assign var="hasFullHeader" value="false"}
{include '../header.tpl'}

<div class="container">
    <main>
        {if $data.gender}
            {if $data.gender == 1}
                {assign var="gender_title" value="Frau"}
            {else}
                {assign var="gender_title" value="Herr"}
            {/if}
        {/if}
        <h1>Herzlichen Glückwunsch, {if $gender_title}{$gender_title}{/if} {if $data.title}{$data.title}{/if} {if $data.last_name}{$data.last_name}{/if}!</h1>
        <section id="form">
            <form id="booking-form" action="{url path="doctor_register"}?step=2" method="post" enctype="application/x-www-form-urlencoded" >
                <h4>Sie haben sich erfolgreich registriert!</h4>
                <p>In Kürze wird Sie ein Mitarbeiter persönlich kontaktieren, um mit Ihnen die Details zu klären.</p>
                <p>Nutzen Sie die Zeit bis dahin, um Ihr Profil zu pflegen.</p>

                <div class="form-element {if $error_messages && in_array('medical_specialty_id', $error_messages)}error{/if}" data-error-message="Bitte wählen Sie Ihre Fachgebiet aus.">
                    <label>Ihr Fachgebiet</label>
                    <select name="medical_specialty_id" data-track="true">
                        <option value="">Bitte wählen</option>
                        {html_options options=$medicalSpecialtiesOptions selected=$data.medical_specialty_id}
                    </select>
                </div>

                <div class="form-element {if $error_messages && in_array('practice_name', $error_messages)}error{/if}" data-error-message="Bitte geben Sie Ihren Praxisnamen ein.">
                    <label>Praxisname</label>
                    <input type="text" name="practice_name" value="{$data.practice_name}" data-track="true">
                </div>

                <div class="form-element {if $error_messages && in_array('city', $error_messages)}error{/if}" data-error-message="Bitte geben Sie Ihre Stadt ein.">
                    <label>Stadt</label>
                    <input type="text" name="city" value="{$data.city}" data-track="true">
                </div>
                <input type="hidden" name="phone_mobile" value="{$data.phone_mobile}" data-track="true">

                <input type="hidden" name="uid" value="{$uid}"/>
                <input type="hidden" name="gender" value="{$data.gender}"/>
                <input type="hidden" name="title" value="{$data.title}"/>
                <input type="hidden" name="last_name" value="{$data.last_name}"/>
                
                <div class="button-wrapper"><button type="submit">Weiter</button></div>
            </form>

        </section>

        <section id="info-block"  class="infobox infobox-grey-light cta-phone">
            <h4><em>Haben Sie noch Fragen?</em></h4>
            <p>Wir sind 24 Stunden kostenfrei für Sie erreichbar:</p>
            {include file="phone-numbers/phone-sales.tpl"}
        </section>

    </main>
</div>
<!-- Google Code for Leads: AtPraxisoptimierung Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 990044011;
var google_conversion_language = "en";
var google_conversion_format = "2";
var google_conversion_color = "ffffff";
var google_conversion_label = "o4feCI_OklYQ676L2AM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/990044011/?label=o4feCI_OklYQ676L2AM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

{include '../footer.tpl'}

