{assign var="template_body_class" value="two-cols-with-form"}
{assign var="additional_body_classes" value="doctor-registration confirmation"}
{assign var="use_minimal_footer" value=true}
{assign var="isSales" value=true}
{assign var="hasFullHeader" value="false"}
{include '../header.tpl'}

<div class="container">
    <main>
        {if $data.gender}
            {if $data.gender == 1}
                {assign var="gender_title" value="Frau"}
            {else}
                {assign var="gender_title" value="Herr"}
            {/if}
        {/if}
        <h1>Herzlichen Glückwunsch, {if $gender_title}{$gender_title}{/if} {if $data.title}{$data.title}{/if} {if $data.last_name}{$data.last_name}{/if}!</h1>
        <section id="form">
            <form id="booking-form" action="{url path="doctor_register"}?step=3" method="post" enctype="application/x-www-form-urlencoded" >
                <h4>Sie haben sich erfolgreich registriert!</h4>
                <p>In Kürze wird Sie ein Mitarbeiter persönlich kontaktieren, um mit Ihnen die Details zu klären.</p>
                <p>Nutzen Sie die Zeit bis dahin, um Ihr Profil zu pflegen.</p>

                <div class="form-element {if in_array('street', $error_messages)}error{/if}" data-error-message="Bitte geben Sie Ihren Firmenadresse ein.">
                    <label>Ihre Firmenadresse</label>
                    <input type="text" name="street" value="{$data.street}" data-track="true">
                </div>

                <div class="form-element {if in_array('zip', $error_messages)}error{/if}" data-error-message="Bitte geben Sie Ihre PLZ ein." style="float:left;width:30%;">
                    <label>PLZ</label>
                    <input type="text" name="zip" value="{$data.zip}" maxlength="5" data-track="true">
                </div>

                <div class="form-element {if in_array('url', $error_messages)}error{/if}" data-error-message="Bitte geben Sie Ihre Firmen-Websiteadresse ein." style="float:right;width:65%;">
                    <label>Firmen-Websiteadresse</label>
                    <input type="text" name="url" value="{$data.url}" data-track="true">
                </div>

                <div style="clear:both" class="form-element {if in_array('doctors_count', $error_messages)}error{/if}" data-error-message="Bitte wählen Sie die Anzahl der Ärzte in Ihrer Praxis aus.">
                    <label>Anzahl Ärzte in der Praxis</label>
                    <select name="doctors_count" data-track="true">
                        <option value="">Bitte wählen</option>
                        {html_options options=array_slice(range(0,50), 1, NULL, TRUE) selected=$data.doctors_count}
                    </select>
                </div>

                <div class="form-element">
                    <label>Grund Ihrer Registrierung</label>
                    <label style="float:left;width:60%;font-weight:normal;"><input type="checkbox" name="registration_reasons[]" value="mehr Patienten bekommen" data-track="true"{if in_array('mehr Patienten bekommen', $data.registration_reasons)} checked="checked"{/if}> mehr Patienten bekommen</label>
                    <label style="float:right;width:40%;font-weight:normal;"><input type="checkbox" name="registration_reasons[]" value="SEO verbessern" data-track="true"{if in_array('SEO verbessern', $data.registration_reasons)} checked="checked"{/if}> SEO verbessern</label><br /><br />
                    <label style="float:left;width:60%;font-weight:normal;"><input type="checkbox" name="registration_reasons[]" value="mehr Bewertungen bei Google und Bewertungsseiten" data-track="true"{if in_array('mehr Bewertungen bei Google und Bewertungsseiten', $data.registration_reasons)} checked="checked"{/if}> mehr Bewertungen bei Google und Bewertungsseiten</label>
                    <label style="float:right;width:40%;font-weight:normal;"><input type="checkbox" name="registration_reasons[]" value="bessere Google Suchergebnisse" data-track="true"{if in_array('bessere Google Suchergebnisse', $data.registration_reasons)} checked="checked"{/if}> bessere Google Suchergebnisse</label>
                    <div style="clear:both;"></div>
                </div>

                <input type="hidden" name="uid" value="{$uid}"/>
                
                <div class="button-wrapper"><button type="submit">Weiter</button></div>
            </form>

        </section>

        <section id="info-block"  class="infobox infobox-grey-light cta-phone">
            <h4><em>Haben Sie noch Fragen?</em></h4>
            <p>Wir sind 24 Stunden kostenfrei für Sie erreichbar:</p>
            {include file="phone-numbers/phone-sales.tpl"}
        </section>

    </main>
</div>


{include '../footer.tpl'}

