{assign var="template_body_class" value="two-cols-with-form"}
{assign var="additional_body_classes" value="doctor-registration"}
{assign var="use_minimal_footer" value=true}
{assign var="isSales" value=true}
{assign var="hasFullHeader" value="false"}
{include '../header.tpl'}

<div class="container">
    <main>
        <div class="already-registered">Sind Sie bereits registriert? <a href="{url path="doctor_login"}">Hier einloggen.</a></div>
        <h1>Registrierung</h1>

        <section id="form">
            <form id="booking-form" action="{url path="doctor_register"}?step=1" method="post" enctype="application/x-www-form-urlencoded" >
                {if $data.practice_name != ''}
                <input type="hidden" name="practice_name" value="{$data.practice_name}">
                {/if}
                <div class="form-element {if in_array('gender', $error_messages)}error{/if}" data-error-message="Bitte wählen Sie Ihre Anrede aus.">
                    <div id="gender" class="radio-group">
                        <h4>Anrede</h4>
                        <input name="gender" type="radio" id="gender-1" value="1" data-track="true" {if $data.gender === 1}checked{/if}><label for="gender-1">Frau</label>
                        <input name="gender" type="radio" id="gender-2" value="0" data-track="true" {if $data.gender === 0}checked{/if}><label for="gender-2">Herr</label>
                    </div>
                </div>
                <div class="form-element {if in_array('title', $error_messages)}error{/if}" data-error-message="The title must be at least 2 characters.">{* @todo Language *}
                    <label>Titel</label>
                    <input type="text" name="title" value="{$data.title}" data-track="true">
                </div>
                <div class="form-element {if in_array('first_name', $error_messages)}error{/if}" data-error-message="Bitte geben Sie Ihren Vornamen ein.">
                    <label>Vorname</label>
                    <input type="text" name="first_name" value="{$data.first_name}" data-track="true">
                </div>
                <div class="form-element {if in_array('last_name', $error_messages)}error{/if}" data-error-message="Bitte geben Sie Ihren Nachnamen ein.">
                    <label>Nachname</label>
                    <input type="text" name="last_name" value="{$data.last_name}" data-track="true">
                </div>
                <div class="form-element {if in_array('email', $error_messages)}error{/if}" data-error-message="Bitte geben Sie Ihre E-Mail Adresse ein.">
                    <label>E-Mail</label>
                    <input type="email" name="email" value="{$data.email}" data-track="true">
                </div>
                <div class="form-element {if in_array('phone', $error_messages)}error{/if}" data-error-message="Bitte geben Sie eine gültige Telefonnummer ein.">
                    <label>Telefon</label>
                    <input type="tel" name="phone" value="{$data.phone}" data-track="true">
                </div>
                
                <div class="button-wrapper"><button type="submit">Registrieren</button></div>
            </form>
        </section>

        <section id="info-block">
            <div class="orange-bar">
                <div class="datetime"><span class="light">Erstellen Sie Ihr</span> kostenfreies Profil!</div>
            </div>
            <div class="infobox advantages">
                <h4><em>Gute Gründe</em> für Ihre <em>risikofreie Registrierung:</em></h4>
                <ul>
                    <li>Darstellung Ihrer Praxis im exklusiven Umfeld.</li>
                    <li>Generierung gezielter Anfragen entsprechend Ihrer Spezialisierungen.</li>
                    <li>Effizienzsteigerung durch qualifizierte Patientenanfragen.</li>
                    <li>Bedarfsgerechte Neupatientenakquise.</li>
                    <li>Moderne und patientenfreundliche Außenwirkung.</li>
                </ul>
            </div>
            <section class="infobox infobox-grey-light cta-phone">
                <h4><em>Haben Sie noch Fragen?</em></h4>
                <p>Wir sind 24 Stunden kostenfrei für Sie erreichbar:</p>
                {include file="phone-numbers/phone-sales.tpl"}
            </section>
        </section>

    </main>
</div>

{include file="footer.tpl"}
