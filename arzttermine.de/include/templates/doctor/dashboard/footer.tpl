<footer>
    {include file="logo.tpl"}
    <div class="links">
    <a href="/agb">AGB</a>&nbsp;&bull;&nbsp;<a href="/impressum">Impressum</a>&nbsp;&bull;&nbsp;<a href="/datenschutz">Datenschutzerklärung</a>
    </div>
</footer>

{* ==== JAVASCRIPT ================================================================*}
<!--[if lte IE 8]>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<![endif]-->

<!--[if gt IE 8]>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<![endif]-->

<!--[if !IE]> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<!-- <![endif]-->
<script>
    $(document).ready(function(){
        $.each(AT.q,function(index,f){
            $(f);
        });

        $(":input[data-track=true]").change(function () {
            $(this).data($(this).attr('name'), $(this).val());
        });
    })
</script>

<script type="text/javascript" src="{$this->getStaticUrl('js/vendor/jquery.formalize.min.js')}"></script>
<script type="text/javascript" src="{$this->getStaticUrl('js/at.js')}"></script>
<script type="text/javascript" src="{$this->getStaticUrl('js/calendar.js')}"></script>
{$this->getJsInclude()}

<div class="layer"></div>
</body>
</html>
