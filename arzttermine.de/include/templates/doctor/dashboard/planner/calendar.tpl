{* Central calendar view inside the planner section that displays appointments that are currently active in the system *}

{* Required libraries *}
{$this->addJsInclude($this->getStaticUrl('js/vendor/bootstrap.min.js'))}
{$this->addJsInclude($this->getStaticUrl('js/planner/planner.js'))}

{$this->addJsInclude($this->getStaticUrl('js/vendor/jquery-ui.custom.min.js'))}
{$this->addJsInclude($this->getStaticUrl('js/vendor/moment.js'))}
{$this->addJsInclude($this->getStaticUrl('js/vendor/moment-recur.js'))}
{$this->addJsInclude($this->getStaticUrl('js/vendor/fullcalendar.js'))}
{$this->addJsInclude($this->getStaticUrl('js/vendor/fullcalendar-de.js'))}

<div id="planner" data-user-id="{$doctor->getId()}" data-location-id="{$practice->getId()}">
    
</div>

{* Modal popup skeleton *}
{include file='./modal.tpl'}