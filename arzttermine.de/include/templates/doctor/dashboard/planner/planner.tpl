{assign var="template_body_class" value="profile-page"}
{assign var="additional_body_classes" value="doctor account-dashboard planner"}
{include '../header.tpl'}

{* Cache this value for readability *}
{assign var=query_slug value="?arzt=$slug"}
{assign var=profile_pic value=$doctor->getProfileAssetUrl()}

{$this->addJsInclude($this->getStaticUrl('js/doctor-profile.js'))}

<link rel="stylesheet" type="text/css" href="/static/css/profile-page.min.css?cv=0506-0">
<link rel="stylesheet" type="text/css" href="/static/css/doc-calendar.min.css" />
<link rel="stylesheet" type="text/css" href="/static/css/plugin/fullcalendar.css" />

<div class="container">
    <main>
        <section class="summary" data-doctor-slug="{$slug}">
            <div class="headline">
                <h1>{$doctor->getFullName()}</h1>
                {include file="doctor/dashboard/logout.tpl"}
            </div>
            <nav>
                <ul>
                    <li><a href="{url path='doctor_account'}{$query_slug}">Mein Arztprofil</a></li>
                    {if $locations && $locations[0]->getIntegrationId() == 1}
                        <li class="active"><a href="{url path='doctor_planner'}{$query_slug}">Kalender</a></li>
                    {/if}
                    {if $locations}
                        <li><a href="{url path='doctor_practice'}?praxis={$locations[0]->getSlug()}">Praxisprofil</a></li>
                        <li><a href="{url path='doctor_patients'}{$query_slug}">Buchungen</a></li>
                        <li><a href="{url path='doctor_reporting'}{$query_slug}">Reporting</a></li>
                    {/if}
                </ul>
            </nav>
        </section>

        <div id="dashboard-actions" class="buttons planner-actions">
            <div class="action">
                <label>Praxis</label>
                <select class="hyperlink">
                    <option value="">Bitte wählen&hellip;</option>
                    {foreach from=$locations item="location"}
                        <option value="{url path='doctor_planner'}{$query_slug}&praxis={$location->getSlug()}" {if $location->getId() == $practice->getId()}selected{/if}>{$location->getName()}</option>
                    {/foreach}
                </select>
            </div>

            <br/>
            
            <div class="action" id="booking-rules">
                <label>Terminregeln</label>
                <div class="dropdown">
                    <label>Terminvorlaufzeit</label>
                    <select id="lead-in">
                        <option value="0" {if $planner->getLeadIn() === 0}selected{/if}>Keine</option>
                        <option value="1" {if $planner->getLeadIn() === 1}selected{/if}>1 Tag</option>
                        <option value="2" {if $planner->getLeadIn() === 2}selected{/if}>2 Tage</option>
                        <option value="3" {if $planner->getLeadIn() === 3}selected{/if}>3 Tage</option>
                        <option value="4" {if $planner->getLeadIn() === 4}selected{/if}>4 Tage</option>
                        <option value="5" {if $planner->getLeadIn() === 5}selected{/if}>5 Tage</option>
                        <option value="6" {if $planner->getLeadIn() === 6}selected{/if}>6 Tage</option>
                        <option value="7" {if $planner->getLeadIn() === 7}selected{/if}>7 Tage</option>
                    </select>
                </div>
                <div class="dropdown">
                    <label>Terminverfügbarkeit</label>
                    <select id="search-period">
                        <option value="0" {if $planner->getSearchPeriod() === 0}selected{/if}>Kein Limit</option>
                        <option value="30" {if $planner->getSearchPeriod() === 30}selected{/if}>1 Monat</option>
                        <option value="60" {if $planner->getSearchPeriod() === 60}selected{/if}>2 Monate</option>
                        <option value="90" {if $planner->getSearchPeriod() === 90}selected{/if}>3 Monate</option>
                    </select>
                </div>
            </div>

            <a href="#" class="button save planner-save" id="dashboard-save" data-toggle="tooltip" data-placement="top" title="Bitte beachten Sie, dass Ihre Terminänderungen nach dem Veröffentlichen erst nach einer Verzögerung sichtbar werden.">Veröffentlichen</a>
        </div>
        
        <section>
        {if $doctor->isValid() && $practice->isValid()}
        {include file='./calendar.tpl'}
        {else}
            <div id="dashboard-notice">
                <p>Bitte wählen Sie eine Praxis aus</p>
            </div>
        {/if}
        </section>

    </main>
</div>

<script>
    var plannerBlocks = {json_encode($blocks)};
</script>

{include '../footer.tpl'}