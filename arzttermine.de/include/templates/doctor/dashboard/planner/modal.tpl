{* Block config modal *}
<div class="modal" id="planner-block-config" tabindex="-1" role="dialog" aria-labelledby="modal-label" aria-hidden="true">
    <div class="modal-dialog modal-vertical-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modal-label">Terminblock</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label class="control-label">Versicherungsarten:</label>
                        <label><input type="checkbox" class="form-control insurances" name="insurance_ids[]" id="insurance_id_2" value="2" checked> Gesetzlich</label>
                        <label><input type="checkbox" class="form-control insurances" name="insurance_ids[]" id="insurance_id_1" value="1" checked> Privat</label>
                    </div>
                    <div class="form-group">
                        <label for="block_length" class="control-label">Blocklänge (Minuten):</label>
                        <input type="number" class="form-control" id="block_length" min="15" step="15">
                    </div>
                    <div class="form-group">
                        <label for="recurrence_interval" class="control-label">Termin Wiederholen:</label>
                        <select class="form-control" id="recurrence_interval">
                            <option value="0">Nein</option>
                            <option value="1">Jeden Tag</option>
                            <option value="7">Jede Woche</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="recurrence_interval" class="control-label">Bis:</label>
                        <input type="date" id="until" value="">
                    </div>
                    <div class="form-group">
                        <label for="treatment_type_ids" class="control-label">Behandlungsgründe:</label>
                        {foreach from=$practice->getMedicalSpecialties() item="medical_specialty"}
                        <p class="subheader">{$medical_specialty->getName()}</p>
                        <div class="specialty-treatments" data-defaults='{json_encode($doctor->getTreatmentTypeIds())}'>
                            <ul>
                                {foreach from=$medical_specialty->getTreatmentTypes() item="treatment_type"}
                                    <li>
                                        <label>
                                            <input type="checkbox" class="treatment_types" id="treatment_type_id_{$treatment_type->getId()}" name="treatment_types[]" value="{$treatment_type->getId()}">
                                            {$treatment_type->getName()}
                                        </label>
                                    </li>
                                {/foreach}
                            </ul>
                        </div>
                        {/foreach}
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>