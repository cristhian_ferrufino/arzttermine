
{assign var="template_body_class" value="profile-page"}
{assign var="additional_body_classes" value="doctor account-dashboard"}
{include './header.tpl'}

{* Cache this value for readability *}
{assign var=query_slug value="?arzt=$slug"}
{assign var=profile_pic value=$doctor->getProfileAssetUrl($smarty.const.ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED)}

{$this->addJsInclude($this->getStaticUrl('js/doctor-profile.js'))}

<div class="container">
    <main>
        <section class="summary" data-doctor-slug="{$slug}">
            <div class="headline">
                <h1>{$doctor->getFullName()}</h1>
                {include file="doctor/dashboard/logout.tpl"}
            </div>
            <nav>
                <ul>
                    <li class="active"><a href="{url path='doctor_account'}{$query_slug}">Mein Arztprofil</a></li>
                    {if $locations && $locations[0]->getIntegrationId() == 1}
                        <li><a href="{url path='doctor_planner'}{$query_slug}">Kalender</a></li>
                    {/if}
                    {if $locations}
                        <li><a href="{url path='doctor_practice'}?praxis={$locations[0]->getSlug()}">Praxisprofil</a></li>
                        <li><a href="{url path='doctor_patients'}{$query_slug}">Buchungen</a></li>
                        <li><a href="{url path='doctor_reporting'}{$query_slug}">Reporting</a></li>
                    {/if}
                </ul>
            </nav>
        </section>
        
        <section>
            <form action="{url path='doctor_account'}{$query_slug}" method="post">
                <div class="profile-photo">
                    <img src="{$SITE_PATH}{if $profile_pic}{$profile_pic}{else}/static/img/default_avatar.jpg{/if}" alt="Profilbild">
                    <a href="{url path='doctor_profile_pic'}{$query_slug}">Profilbilder bearbeiten</a>
                </div>

                <div class="text-data name">
                    <h3>Name</h3>
                    <div>
                        <input type="text" name="title" value="{$doctor->getTitle()}" placeholder="Titel" />
                        <input type="text" name="first_name" value="{$doctor->getFirstName()}" placeholder="Vorname" />
                        <input type="text" name="last_name" value="{$doctor->getLastName()}" placeholder="Nachname" />
                    </div>
                </div>

                <div class="text-data praxen">
                    <h3>Praxen</h3>
                    <div>
                        {if $locations}
                            <ul>
                                {foreach from=$locations item=location}
                                    <li>
                                        <p class="praxis-name">{$location->getName()}</p>
                                        <p>{$location->getStreet()}</p>
                                        <p>{$location->getZip()}, {$location->getCity()}</p>
                                        <p>{$location->getPhone()}</p>
                                        <p><a href="{$location->getWww()}" target="_blank">{$location->getWww()}</a></p>
                                    </li>
                                {/foreach}
                            </ul>
                        {else}
                            <p>Ihr Account muss einer Praxis zugeordnet werden, bevor Sie ihn nutzen können. Bitte kontaktieren Sie uns kostenlos unter 0800 2222 133 oder support@arzttermine.de.</p>
                        {/if}
                    </div>
                </div>

                <div class="text-data">
                    <h3>Ärzte</h3>
                    <div>
                        <select id="doctor-select" class="hyperlink">
                            {foreach $doctors as $_doctor}
                                <option value="?arzt={$_doctor->getSlug()}" {if $doctor->getId() == $_doctor->getId()}selected{/if}>
                                    {$_doctor->getFullName()}
                                </option>
                            {/foreach}
                        </select>
                    </div>
                </div>

                <div class="text-data">
                    <h3>Philosophie</h3>
                    <div class="editable-container">
                        <textarea name="docinfo_1">{$doctor->getDocinfo1()}</textarea>
                    </div>
                </div>

                <div class="text-data">
                    <h3>Fachrichtung</h3>
                    <div class="editable-container specialties">
                        {foreach from=$all_specialties item=specialty}
                            {if in_array($specialty->getId(), $specialties)}
                                <label>
                                    <input type="checkbox" name="medical_specialties_ids[]" value="{$specialty->getId()}" checked>
                                    {assign var="specialty_selected" value="true"}
                                    {$specialty->getName()}
                                </label>
                            {/if}
                        {/foreach}

                        {if isset($specialty_selected)}
                            <a role="button" class="toggle-specialties">Zeige <span class="show-more">mehr</span><span class="show-less">weniger</span></a>
                        {/if}

                        <div class="unselected-specialties {if !isset($specialty_selected)}visible{/if}">
                            {foreach from=$all_specialties item=specialty}
                                {if !in_array($specialty->getId(), $specialties)}
                                    <label>
                                        <input type="checkbox" name="medical_specialties_ids[]" value="{$specialty->getId()}">
                                        {$specialty->getName()}
                                    </label>
                                {/if}
                            {/foreach}
                        </div>
                    </div>
                </div>

                <div class="text-data">
                    <h3>Leistungsspektrum</h3>
                    <div class="editable-container" id="treatments">

                    </div>
                </div>

                <div class="text-data">
                    <h3>Ausbildung</h3>
                    <div class="editable-container">
                        <textarea name="docinfo_2">{$doctor->getDocinfo2()}</textarea>
                    </div>
                </div>

                <div class="text-data">
                    <h3>Sprachen</h3>
                    <div class="editable-container">
                        <textarea name="docinfo_3">{$doctor->getDocinfo3()}</textarea>
                    </div>
                </div>

                <div class="text-data">
                    <h3>Passwort</h3>
                    <div class="change-password">
                        <input class="password" type="password" name="current_password" placeholder="Aktuelles Passwort">
                        <input class="password" type="password" name="new_password" placeholder="Neues Passwort">
                    </div>
                </div>

                <div class="buttons-container">
                    <button class="save">Speichern</button>
                </div>

                <input type="hidden" name="uid" value="{$doctor->getId()}" />
            </form>
        </section>

    </main>
</div>

<script>
    var doctorsMedSpecIdsArray = {json_encode($specialties)} || [];
    var docAccountApiUrl = "{url path='doctor_api'}";
    var docAccountUrl = '{url path='doctor_account'}';
</script>

{include './footer.tpl'}

