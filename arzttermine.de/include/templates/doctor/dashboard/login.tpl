{assign var="template_body_class" value="two-cols-with-form"}
{assign var="additional_body_classes" value="doctor-registration doctor-login"}
{include './header.tpl'}

<div class="container top_nav" >
    <main>
        <div class="already-registered">Sie haben noch keinen Ärzte-Zugang? <a href="{url path='doctor_register'}">Hier registrieren.</a></div>
        <h1>Ärzte-Management</h1>

        <section id="form">
            <form id="booking-form" action="{url path='doctor_login'}" method="post" enctype="application/x-www-form-urlencoded" >
                <div class="form-element">
                    <label>E-Mail-Adresse</label>
                    <input type="email" name="email" tabindex="1" value="{$email}">
                </div>
                <div class="form-element">
                    <label>Passwort</label>
                    <input type="password" tabindex="2" name="password">
                    <a role="button" tabindex="5" href="#">Ich habe mein Passwort vergessen</a>
                </div>
                <div class="button-wrapper"><input type="submit" tabindex="3" value="Anmelden"></div>
            </form>

            <form id="forgot-password" class="hidden" action="{url path='doctor_forgot_password'}" method="post">
                <div class="form-element">
                    <label>E-Mail-Adresse<label>
                    <input type="email" name="email">
                </div>
                <div class="button-wrapper"><input class="btn_green button large center" id="btn-pw_submit" type="submit" value="Passwort anfordern"></div>
            </form>

        </section>

        <section id="info-block"  class="infobox infobox-grey-light cta-phone">
            <h4><em>Haben Sie noch Fragen?</em></h4>
            <p>Wir sind 24 Stunden kostenfrei für Sie erreichbar:</p>
            {include file="phone-numbers/phone-sales.tpl"}
        </section>
    </main>
</div>
<div style="clear: both; padding-top: 2rem;">
    {include file="footer.tpl"}
</div>

