{assign var="template_body_class" value="profile-page"}
{assign var="additional_body_classes" value="doctor account-dashboard photo-upload"}
{include './header.tpl'}

{* Cache this value for readability *}
{assign var=query_slug value="?arzt=$slug"}

{$this->addJsInclude($this->getStaticUrl('js/doctor-profile.js'))}

<div class="container">
    <main>
        <section class="summary" data-doctor-slug="{$slug}">
            <div class="headline">
                <h1>{$doctor->getFullName()}</h1>
                {include file="doctor/dashboard/logout.tpl"}
            </div>
            <nav>
                <ul>
                    <li class="active"><a href="{url path='doctor_account'}{$query_slug}">Mein Arztprofil</a></li>
                    {if $locations && $locations[0]->getIntegrationId() == 1}
                        <li><a href="{url path='doctor_planner'}{$query_slug}">Kalender</a></li>
                    {/if}
                    {if $locations}
                        <li><a href="{url path='doctor_practice'}?praxis={$locations[0]->getSlug()}">Praxisprofil</a></li>
                        <li><a href="{url path='doctor_patients'}{$query_slug}">Patienten</a></li>
                        <li><a href="{url path='doctor_reporting'}{$query_slug}">Reporting</a></li>
                    {/if}
                </ul>
            </nav>
        </section>

        <section class="profile-images">
            {foreach from=$assets key=assetId item=asset}
                {if $asset->getUrl($smarty.const.ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED)}
                    <div class="profile-image-wrapper">
                        <img class="profile-image" src="{$asset->getUrl($smarty.const.ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED)}" />
                        <div class="actions" data-aid="{$asset->getId()}">
                            <a href="{url path="doctor_profile_pic"}{$query_slug}&action=delete">
                                Löschen
                            </a>
                            <a href="{url path="doctor_profile_pic"}{$query_slug}&action=select">
                                Als Profilbild festlegen
                            </a>
                        </div>
                    </div>
                {/if}
            {/foreach}
            <a id="upload_pic_button" role="button">Neues Foto hochladen</a>
            <div id="upload_pic">
                <form id="form-profile-edit" action="{url path="doctor_profile_pic"}{$query_slug}&action=new" method="post" enctype="multipart/form-data">
                    <input name="uid" id="uid" type="hidden" value="{$doctor->getId()}" />
                    <input class="button" type="file" name="upload">
                    <div class="buttons-container">
                        <button type="submit" id="edit-submit" class="save">Speichern</button>
                        <button type="reset" id="edit-reset" class="reset">Verwerfen</button>
                    </div>
                </form>
            </div>
        </section>

    </main>
</div>

<script type="text/javascript">
    var uid = {$doctor->getId()};
</script>
{include './footer.tpl'}