{assign var="template_body_class" value="profile-page"}
{assign var="additional_body_classes" value="doctor account-dashboard patients"}
{include './header.tpl'}

{* Cache this value for readability *}
{assign var=query_slug value="?arzt=$slug"}
{assign var=profile_pic value=$doctor->getProfileAssetUrl()}

{$this->addJsInclude($this->getStaticUrl('js/doctor-profile.js'))}

<link rel="stylesheet" type="text/css" href="/static/css/profile-page.min.css?cv=0506-0">
<link rel="stylesheet" type="text/css" href="/static/css/doc-calendar.min.css" />
<link rel="stylesheet" type="text/css" href="/static/css/plugin/fullcalendar.css" />

<div class="container">
    <main>
        <section class="summary" data-doctor-slug="{$slug}">
            <div class="headline">
                <h1>{$doctor->getFullName()}</h1>
                {include file="doctor/dashboard/logout.tpl"}
            </div>
            <nav>
                <ul>
                    <li><a href="{url path='doctor_account'}{$query_slug}">Mein Arztprofil</a></li>
                    {if $locations && $locations[0]->getIntegrationId() == 1}
                        <li><a href="{url path='doctor_planner'}{$query_slug}">Kalender</a></li>
                    {/if}
                    {if $locations}
                        <li><a href="{url path='doctor_practice'}?praxis={$locations[0]->getSlug()}">Praxisprofil</a></li>
                        <li class="active"><a href="{url path='doctor_patients'}{$query_slug}">Buchungen</a></li>
                        <li><a href="{url path='doctor_reporting'}{$query_slug}">Reporting</a></li>
                    {/if}
                </ul>
            </nav>
        </section>
        
        <div id="dashboard-actions" class="buttons">
            {* This is kinda gross. Leaving here as we need something quickly *}
            <div class="action">
                <label>Buchungen seit</label>
                <select class="hyperlink" id="booking-period">
                    <option value="{url path='doctor_patients'}{$query_slug}&month=current"{if $month == 'current'} selected{/if}>Aktueller Monat</option>
                    <option value="{url path='doctor_patients'}{$query_slug}&month=last"{if $month == 'last'} selected{/if}>Letzer Monat</option>
                </select>
            </div>
        </div>
        
        <table width="100%" id="bookings">
            <thead>
                <tr>
                    <th class="doctor">Gebucht bei</th>
                    <th class="datetime">Gebucht</th>
                    <th class="datetime">Termin</th>
                    <th class="patient">Patient</th>
                    <th class="appointment-status">Status</th>
                </tr>
            </thead>
            <tbody>
            {foreach from=$bookings item='booking'}
                <tr>
                    <td>{$doctor->getFullName()}</td>
                    <td>{calendar action="format" datetime=$booking->getCreatedAt() format="d.m.Y"}&nbsp;{calendar action="format" datetime=$booking->getCreatedAt() format="H:s"}</td>
                    <td>{calendar action="format" datetime=$booking->getAppointmentStartAt() format="d.m.Y"}&nbsp;{calendar action="format" datetime=$booking->getAppointmentStartAt() format="H:s"}</td>
                    <td>
                        <span class="patient-name">{$booking->getFirstName()} {$booking->getLastName()}</span><br/>
                        {if $booking->getReturningVisitor(true)}Bestandspatient{else}Neupatient{/if}<br/>
                        {$booking->getMedicalSpecialty()->getName()}<br/>
                        {$booking->getPhone()}<br/>
                        {$booking->getEmail()}
                    </td>
                    <td>{$booking->getReportingStatusText()}</td>
                </tr>
            {/foreach}
            </tbody>
        </table>
    </main>
</div>

{include './footer.tpl'}