<section>
    {if $statement->getBookings()|count}
    <table width="100%" id="statement" data-statement-id="{$statement->getId()}">
        <thead>
        <tr>
            <th class="doctor">Arzt</th>
            <th class="datetime">Gebucht für</th>
            <th class="patient">Patient</th>
            <th class="reporting-status">Reporting</th>
            <th class="booking-status">Status</th>
        </tr>
        </thead>
        <tbody>
        {foreach from=$statement->getBookings() item='booking'}
            <tr class="booking" data-booking-id="{$booking->getId()}">
                <td>{$booking->getUser()->getFullName()}</td>
                <td>{calendar action="format" datetime=$booking->getAppointmentStartAt() format="d.m.Y"}&nbsp;{calendar action="format" datetime=$booking->getAppointmentStartAt() format="H:s"}</td>
                <td>{$booking->getFirstName()} {$booking->getLastName()}</td>
                <td class="status">
                    <label><input type="radio" class="reporting-status" value="1" name="status[{$booking->getId()}]" {if $booking->getReportingStatus() === 1} checked{/if} /> erschienen</label><br/>
                    <label><input type="radio" class="reporting-status" value="2" name="status[{$booking->getId()}]" {if $booking->getReportingStatus() === 2} checked{/if} /> nicht erschienen</label><br/>
                    <label><input type="radio" class="reporting-status" value="4" name="status[{$booking->getId()}]" {if $booking->getReportingStatus() === 4} checked{/if} /> Termin verschoben</label><br/>
                    <input type="text" class="moved-to datetimepicker" name="moved[{$booking->getId()}]" value="{$booking->getAppointmentStartAt()}" {if $booking->getReportingStatus() !== 4}style="display:none"{/if} />
                    <label><input type="checkbox" class="reporting-returning-visitor" value="" name="status[{$booking->getId()}]" {if $booking->getReturningVisitor(true)} checked{/if} /> Bestandspatient</label><br/>
                </td>
                <td class="booking-status">{$booking->getReportingStatusText()}</td>
            </tr>
        {/foreach}
        </tbody>
    </table>
    {else}
        <div id="dashboard-notice">
            {* @todo Language *}
            Für den gewählten Zeitraum sind keine Buchungen vorhanden
        </div>
    {/if}
</section>