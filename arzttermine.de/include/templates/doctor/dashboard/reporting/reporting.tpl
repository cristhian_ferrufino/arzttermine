{assign var="template_body_class" value="profile-page"}
{assign var="additional_body_classes" value="doctor account-dashboard reporting"}
{include '../header.tpl'}

{* Cache this value for readability *}
{assign var=query_slug value="?arzt=$slug"}
{assign var=profile_pic value=$doctor->getProfileAssetUrl()}

{$this->addJsInclude($this->getStaticUrl('js/vendor/jquery.datetimepicker.js'))}
{$this->addJsInclude($this->getStaticUrl('js/doctor-profile.js'))}
{$this->addJsInclude($this->getStaticUrl('js/dashboard/reporting.js'))}

<link rel="stylesheet" type="text/css" href="{$this->getStaticUrl('css/plugin/jquery.datetimepicker.css')}">

<div class="container">
    <main>
        <section class="summary" data-doctor-slug="{$slug}">
            <div class="headline">
                <h1>{$doctor->getFullName()}</h1>
                {include file="doctor/dashboard/logout.tpl"}
            </div>
            <nav>
                <ul>
                    <li><a href="{url path='doctor_account'}{$query_slug}">Mein Arztprofil</a></li>
                    {if $locations && $locations[0]->getIntegrationId() == 1}
                        <li><a href="{url path='doctor_planner'}{$query_slug}">Kalender</a></li>
                    {/if}
                    {if $locations}
                        <li><a href="{url path='doctor_practice'}?praxis={$locations[0]->getSlug()}">Praxisprofil</a></li>
                        <li><a href="{url path='doctor_patients'}{$query_slug}">Buchungen</a></li>
                        <li class="active"><a href="{url path='doctor_reporting'}{$query_slug}">Reporting</a></li>
                    {/if}
                </ul>
            </nav>
        </section>
        
        <div id="dashboard-actions" class="buttons reporting-actions">
            <div class="action">
                <label>Praxis</label>
                <select class="hyperlink">
                    <option value="">Bitte wählen&hellip;</option>
                    {foreach from=$locations item="location"}
                        <option value="{url path='doctor_reporting'}{$query_slug}&praxis={$location->getSlug()}" {if $location->getId() == $practice->getId()}selected{/if}>{$location->getName()}</option>
                    {/foreach}
                </select>
            </div>
            
        {if $doctor->isValid() && $practice->isValid()}
            <div class="action">
                <label>Zeitraum</label>
                <select class="hyperlink" id="time-period">
                    {foreach from=range((date('Y') - 2), date('Y')) item='year'}
                        {foreach from=range(1, 12) item='month'}
                            {* Uh, don't display impossible billing periods *}
                            {if $year == date('Y') && $month >= date('m')}{continue}{/if}
                            
                            {assign var='period' value=sprintf('%04d-%02d', $year, $month)}
                            <option value="{url path='doctor_reporting'}{$query_slug}&praxis={$practice->getSlug()}&period={$period}"{if $billing_period == $period} selected{/if}>{$period}</option>
                        {/foreach}
                    {/foreach}
                </select>
            </div>
            
            <a href="#" class="button save reporting-save" id="dashboard-save">Speichern</a>
        {/if}
        </div>

        {if $doctor->isValid() && $practice->isValid()}
            {include file='./statement.tpl'}
        {else}
            <div id="dashboard-notice">
                <p>Bitte wählen Sie eine Praxis aus</p>
            </div>
        {/if}

    </main>
</div>

{include '../footer.tpl'}