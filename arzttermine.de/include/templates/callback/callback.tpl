<form id="callBackForm" action="#" method="post">
	<label>Name eintragen</label>
		<input type="text" name="name" id="name" value="" class="validate[required,funcCall[defaultVal_name]]input_text" />
	<label>Telefonnummer eintragen</label>
		<input type="text" name="phone" id="phone" value="" class="validate[required,funcCall[defaultVal_phone]]input_text" />
	<input type="hidden" name="hp" id="hp" value="" />
	<div id="sendmail">
	<input type="submit" value="Ich bitte um Rückruf" id="sendbtn" name="sendmail" />
	<span class="sendIco"></span>
	</div>
</form>
{literal}
<script type="text/javascript">
	function defaultVal_name(field){
		if( field.val() == 'Name eintragen' || field.val() == '' ){
			//return "Bitte geben Sie für den Rückruf einen Ansprechpartner an.";		
			$("#response").removeAttr("class").addClass("hint").html('<span class="hint middle">Bitte geben Sie für den Rückruf einen Ansprechpartner an.</span>');
			$.showMessage('in', 7500);		
		}		
	}
	function defaultVal_phone(field){ 	
		if( field.val() == 'Telefonnummer eintragen' || field.val() == '' ){
			//return "Bitte beachten Sie, ohne Telefonnummer ist kein Rückruf möglich.";
			$("#response").removeAttr("class").addClass("hint").html('<span class="hint middle">Bitte beachten Sie, ohne Telefonnummer ist kein Rückruf möglich.</span>');
			$.showMessage('in', 7500);	
		}
	}
	
</script>
{/literal}
