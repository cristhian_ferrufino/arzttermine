{if $assets|@count>0}

<h3>Fotogallerie</h3>
<div id="gallery-container">
	<ul class="cycle">
{foreach from=$assets item='asset'}
		<li><a class="group" rel="gallery" title="{alt($asset->getDescription())}" href="{$asset->getGfxUrl($smarty.const.ASSET_GFX_SIZE_STANDARD)}"><img src="{$asset->getGfxUrl($smarty.const.ASSET_GFX_SIZE_THUMBNAIL)}" alt="{alt($asset->getDescription())}" asset_id="{$asset->getId()}" /></a></li>
{/foreach}
	</ul>
	<div class="clear"></div>
</div>

{literal}
<script type="text/javascript">
//<![CDATA[
$(document).ready(function() {
	$("a.group").fancybox({
	'speedIn'		:	600,
	'speedOut'		:	200,
	'titlePosition'  : 'over'
});
});

//]]>
</script>
{/literal}
{/if}
