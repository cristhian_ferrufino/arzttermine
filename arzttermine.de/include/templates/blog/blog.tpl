{strip}
    {assign var="template_body_class" value="static"}
    {include file="header.tpl"}
{/strip}

<div class="container">
    <main>
        <aside>
            <nav>
                {include 'navi.tpl'}
            </nav>

            {$this->getLatestBlogPosts()}
        </aside>

        <div id="content">
            <div class="blog">
                {include file="blog/_blogposts.tpl"}
            </div>

            <div class="blog_pagination">
                {if $show_older_posts_page_num}
                    <div class="left">&laquo; <a href="{"url_blog"|trans}/p/{$show_older_posts_page_num}">{"show_older_blogposts"|trans}</a></div>
                {/if}
                {if $show_newer_posts_page_num}
                    <div class="right"><a href="{"url_blog"|trans}/p/{$show_newer_posts_page_num}">{"show_newer_blogposts"|trans}</a> &raquo;</div>
                {/if}
                <div class="clear"></div>
            </div>
        </div>
    </main>
</div>

{include file="footer.tpl"}
