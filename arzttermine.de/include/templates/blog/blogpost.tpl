{strip}
    {assign var="template_body_class" value="static"}
    {include file="header.tpl"}
{/strip}

<div class="container">
    <main>
        <aside>
            <nav>
                {include 'navi.tpl'}
            </nav>

            {$this->getLatestBlogPosts()}
        </aside>

        <div id="content">
            <div class="blog">
                {include file="blog/_blogpost.tpl"}
            </div>

            <div class="breadcrumb">
                {assign var="previousPost" value=$blogpost->getPrev()}
                {if $previousPost->isValid()}
                    <div class="left">&laquo; <a href="{$previousPost->getUrl()}">{$previousPost->getTitle()}</a></div>
                {/if}
                {assign var="nextPost" value=$blogpost->getNext()}
                {if $nextPost->isValid()}
                    <div class="right"><a href="{$nextPost->getUrl()}">{$nextPost->getTitle()}</a> &raquo;</div>
                {/if}
                <div class="clear"></div>
            </div>
        </div>
    </main>
</div>

{include file="footer.tpl"}
