<div class="section blogpost-last">
	<div class="box_gfx box_white active" id="box_blogpost-last">
		<div class="box_gfx_title box_gfx_1_white_240x26">
			<div class="arrow"></div>
			<span class="header">Die letzten News</span>
		</div>
		<div class="box_gfx_content">
			<ul>
{foreach from=$blogposts_last key=key item=item_blogpost}
				<li class="{cycle values="alternate,"}"><a href="{$item_blogpost->getUrl()}" class="colored">{$item_blogpost->getTopic()}: <span>{$item_blogpost->getTitle()}</span></a></li>
{/foreach}
			</ul>
		</div>
	</div>
</div>
{*literal}
<script type="text/javascript">
//<![CDATA[
jQuery('#box_blogpost-last .box_gfx_title').toggle(function() {
	jQuery('#box_blogpost-last').toggleClass('active');
	jQuery('#box_blogpost-last .box_gfx_content').slideToggle('fast', function(){
		jQuery('#box_blogpost-last').removeClass('background');
	});
}, function() {
	jQuery('#box_blogpost-last').addClass('background');
	jQuery('#box_blogpost-last').toggleClass('active');
	jQuery('#box_blogpost-last .box_gfx_content').slideToggle('fast');
});
//]]>
</script>
{/literal*}
