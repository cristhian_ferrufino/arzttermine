<div class="blogpost content">
	<div class="topic">{$blogpost->getTopic()}</div>
{if $page_id=='newsblogpost'}
	<h1 class="header"><a href="{$blogpost->getUrl()}">{$blogpost->getTitle()}</a></h1>
{else}
	<h2 class="header"><a href="{$blogpost->getUrl()}">{$blogpost->getTitle()}</a></h2>
{/if}
	<div class="meta">
{if $blogpost->getStatus()==0}
		<span style="color:red;">DRAFT</span> |
{/if}
{if !$blogpost->isPublishedNow()}
		<span class="published_at" style="color:orange;">{$blogpost->getPrettyDateTime('published_at')} | published in {$blogpost->getPublishedAtHumanTimeDiff()}</span> |
{else}
		<span class="published_at">{$blogpost->getPrettyDateTime('published_at','d.m.Y / H:i')} Uhr </span>
{/if}
	</div>
{if $page_id=='newsblog'}
{if $CONFIG.SOCIALMEDIA_ACTIVATE}
	<div class="socialmedia-buttons cf">
{$blogpost->getSocialBookmarksCode()}
	</div>
{/if}
{/if}
{if $page_id=='homepage'}
	<div class="body">{$blogpost->getBlogPostHtml(true)}</div>
{else if $page_id=='newsblogpost'}
	<div class="body">{$blogpost->getBlogPostHtml()}</div>
{else}
	<div class="body">{$blogpost->getBlogPostHtml(true)}</div>
{/if}
	<div class="clear"></div>
{if !$smarty.section.mysec.last}<div class="divider"></div>{/if}
{if $page_id=='newsblogpost'}
{if $CONFIG.SOCIALMEDIA_ACTIVATE}
	<div class="socialmedia-buttons cf">
{$blogpost->getSocialBookmarksCode()}
	</div>
{/if}
{/if}

</div>
