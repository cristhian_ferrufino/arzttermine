{if $profile->isValid() && $profile->checkPrivilege('system_internal_group')}
<div id="admin-navigation">
    {if $this->content && $profile->checkPrivilege('cms_admin_content_edit')}
        <a href="{$this->content->url_admin_explorer_version}">Edit</a>
    {/if}
    {if $location && $profile->checkPrivilege('location_admin')}
        <a href="{$location->getAdminEditUrl()}">Edit Location</a>
    {/if}
    {if $user && $profile->checkPrivilege('user_admin')}
        <a href="{$user->getAdminEditUrl()}">Edit User</a>
    {/if}
    {if $profile->checkPrivilege('system_internal_group')}
        <a href="/administration/dashboard">Admin</a>
    {/if}
    {* The generator breaks down because of our filenames. Hardcode logout link *}
    <a href="/logout">Logout</a>
</div>
{/if}