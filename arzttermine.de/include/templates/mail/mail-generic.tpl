<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html style="width: 100%; height: 100%">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body style="width: 100%; height: 100%" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="background-color: #e5e5e5; background-image: -webkit-linear-gradient(bottom, #e8e8e8 0%, #fff 100%); background-image: -moz-linear-gradient(bottom, #e8e8e8 0%, #fff 100%); background-image: -ms-linear-gradient(bottom, #e8e8e8 0%, #fff 100%); background-image: -o-linear-gradient(bottom, #e8e8e8 0%, #fff 100%); padding-bottom: 20px;">
    <tr>
        <td>
            <table style="margin: 0 auto"  border="0" cellpadding="0" cellspacing="0" width="600">
                <tr>
                    <td valign="top">
                        <table height="114" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td width="250">
                                    <img src="http://www.arzttermine.dev/static/images/assets/logo-2.png" width="212" height="69" alt="Arzttermine" style="-ms-interpolation-mode:bicubic;">
                                </td>
                                <td align="right" width="350" style="color:#555553;font-size:15px;font-family:Helvetica,Helvetica,Arial,sans-serif;" valign="middle" >
                                    <br>
                                    <strong><span>Support-Hotline:</span>
                                            <span>
                                                <a href="tel:08002222133" style="color:#0090d6;text-decoration:none">
                                                    0800 / 222 21 33
                                                </a>
                                            </span>
                                    </strong><span style="font-size:13px">(Kostenfrei)</span>
                                </td>
                            </tr>
                        </table>
                        <table cellpadding="0" cellspacing="0" border="0" height="783" width="100%" style="font-family: Helvetica; color: #555; font-size: 14px;padding:35px; border: 1px solid #ccc; /* stroke */border-radius: 9px 9px 0px 0px; -webkit-border-radius: 9px 9px 0px 0px;-moz-border-radius: 9px 9px 0px 0px; background-clip: padding-box; background-color: #fff; box-shadow: 0 0 5px rgba(0,0,0,.1), inset 0 0 0 1px #fff; ">
                            <tr>
                                <td valign="top">
                                    {render name='content1' method='render_content'}
                                    <table cellpadding="0" cellspacing="10" width="100%">
                                        <tr>
                                            <td align="right">
                                                <a style="border:none" href="https://www.facebook.com/Arzttermine?fref=ts">
                                                    <img width="195" height="79" src="http://www.arzttermine.dev/static/img/newsletter/facebook.jpg" alt="Arzttermine Facebook" />
                                                </a>
                                            </td>
                                            <td align="left">
                                                <a style="border:none" href="https://twitter.com/Arzttermine">
                                                    <img width="195" height="79" src="http://www.arzttermine.dev/static/img/newsletter/twitter.jpg" alt="Arzttermine Twitter" />
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                    <table cellpadding="0" cellspacing="0" width="100%" height="170">
                                        <tr>
                                            <td valign="middle" style="width: 280px; font-family: Helvetica;font-size: 14px; color: #555;">
                                                Mit freundlichen Grüßen
                                                <br/><br/>
                                                <div>
                                                    <a href="http://www.arzttermine.de" style="font-weight:bold; color: #0090d6; font-family: Helvetica;font-size: 14px;text-decoration:none">
                                                        Patientensupport - Arzttermine.de
                                                    </a>
                                                </div>

                                                <div style="font-family: Helvetica;font-size: 14px; color: #555;">Rosenthaler Straße 51<br/>10178 Berlin</div>
                                                <br/>
                                                <div style="font-family: Helvetica;font-size: 14px; color: #555;">Kostenfreie Rufnummer:
                                                        <span>
                                                            <a href="tel:08002222133" style="font-family: Helvetica;font-size: 14px;font-weight:bold; color: #0090d6;text-decoration:none">
                                                                0800 / 22 22 133
                                                            </a>
                                                        </span>
                                                </div>

                                                <span>
                                                    <a href="mailto:{if $sender_email}{$sender_email}{else}support@arzttermine.de{/if}" style="font-family: Helvetica;font-size: 14px;font-weight:bold; color: #0090d6;text-decoration:none">
                                                        {if $sender_email}{$sender_email}{else}support@arzttermine.de{/if}
                                                    </a>
                                                </span>

                                            </td>
                                            <td valign="bottom" style="text-align:center;">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <table width="600" height="64" style="border: 1px solid #ccc; /* stroke */border-radius: 0px 0px  9px 9px; -webkit-border-radius: 0px 0px  9px 9px; -moz-border-radius: 0px 0px  9px 9px; background-clip: padding-box;  box-shadow: 0 0 5px rgba(0,0,0,.1), inset 0 0 0 1px #fff; background-color: #f1f1f1; /* layer fill content */color: #0090d6; font-family: Helvetica;font-size: 12px;text-shadow: 0 1px 3px #fff;">
                            <tr>
                                <td>
                                    <table style="padding-left:15px;" height="64">
                                        <tr style="color: #0090d6; font-family: Helvetica;font-size: 12px;text-shadow: 0 1px 3px #fff;">
                                            <td valign="middle">
                                                <a href="http://www.arzttermine.dev/kontakt" style="color: #0090d6; font-family: Helvetica;font-size: 12px;text-shadow: 0 1px 3px #fff;text-decoration:none">
                                                    Kontakt
                                                </a>&nbsp;&nbsp;&nbsp;&nbsp;
                                            </td>
                                            <td valign="middle">
                                                <a href="http://www.arzttermine.dev/impressum" style="color: #0090d6; font-family: Helvetica;font-size: 12px;text-shadow: 0 1px 3px #fff;text-decoration:none">
                                                    Impressum
                                                </a>&nbsp;&nbsp;&nbsp;&nbsp;
                                            </td>
                                            <td valign="middle">
                                                <a href="http://www.arzttermine.dev/datenschutz" style="color: #0090d6; font-family: Helvetica;font-size: 12px;text-shadow: 0 1px 3px #fff;text-decoration:none">
                                                    Datenschutz
                                                </a>&nbsp;&nbsp;&nbsp;&nbsp;
                                            </td>
                                            <td valign="middle">
                                                <a href="http://www.arzttermine.dev/agb" style="color: #0090d6; font-family: Helvetica;font-size: 12px;text-shadow: 0 1px 3px #fff;text-decoration:none">
                                                    AGB
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
