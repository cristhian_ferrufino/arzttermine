<section id="partners" class="hidden-sm-down">
  <div class="row" style="margin-bottom: 1.5rem">
    <div class="col">
      <h2 >Unsere Partner</h2>
    </div>
  </div>
    <div class="row">
      <div class="col offset-md-1 ">
        <a href="https://www.tk.de/" target="_blank" title="tk">
          <img src="../static/images/assets/partners/tk.png" class="partners">
        </a>
      </div>
      <div class="col">
        <a href="https://www.dak.de/" target="_blank"  title="dak">
          <img src="../static/images/assets/partners/dak.svg" class="partners">
        </a>
      </div>
      <div class="col">
        <a href="https://www.samedi.de/"  title="samedi">
          <img src="../static/images/assets/partners/samedi.svg" class="partners">
        </a>
      </div>

      <div class="col">
        <a href="https://www.weisse-liste.de/" target="_blank"  title="weisse-liste">
          <img src="../static/images/assets/partners/weisse_liste.png" class="partners">
        </a>
      </div>
      <div class="col">
        <a href="https://www.heise.de/" target="_blank"  title="heise">
          <img src="../static/images/assets/partners/heise_online.png" class="partners">
        </a>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col offset-md-1 ">
        <a href="https://www.dastelefonbuch.de/" target="_blank"  title="dastelefonbuch">
          <img src="../static/images/assets/partners/Das_Telefonbuch_Deutschland.png" class="partners">
        </a>
      </div>
      <div class="col">
        <a href="http://www.dasoertliche.de/" target="_blank" title="dasoertliche">
          <img src="../static/images/assets/partners/lgo_oetb.png" class="partners">
        </a>
      </div>
      <div class="col">
        <a href="https://www.stiftung-gesundheit.de/de/" target="_blank"  title="stiftung-gesundheit">
          <img src="../static/images/assets/partners/stiftung_gesundheit.png" class="partners">
        </a>
      </div>

      <div class="col">
        <a href="http://www.mueller-medien.com/" target="_blank"  title="mueller-medien">
          <img src="../static/images/assets/partners/mueller_medien.png" class="partners">
        </a>
      </div>
      <div class="col">
        <a href="https://www.gelbeseiten.de/" target="_blank"  title="gelbeseiten">
          <img src="../static/images/assets/partners/gelbe_seiten.png" class="partners">
        </a>
      </div>
    </div>
</section>