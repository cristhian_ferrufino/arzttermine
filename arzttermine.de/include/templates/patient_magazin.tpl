<style>
    a.container_news_block{
        margin-bottom: .5rem;
        text-decoration:none;
        color:#000;
        
    }
    img.image_news_block{
        display: block;
        width: 100%;
        height: 100%;
        object-fit: cover;

    }

    div.title_news_block {
        position: absolute;
        bottom: -1px;
        background-color: #fff;
        width: 80%;
        margin-left: 10%;
        padding: .5rem;
    }
    div.img_container_news_block{
        height: 199px;
        margin-bottom: 15px; 
        position:relative;
    }

</style>

<section id="magazin-top-news">
    <h2>Entdecken Sie den arztermine.de Blog</h2>
        <p align="center">Lassen Sie sich informieren, inspirieren und motivieren!</p>
    <br />
    <div class="row">
        <div class="col-12 col-sm" style="margin-bottom: 1rem;">
            <a href="{$news[0].link}" target="_blank" class="container_news_block">
                <div >
                    <div style="position:relative;">
                        <div class="img_container_news_block">
                            <img class="image_news_block" src="{$news[0].img}"/>
                        </div>

                        <div class="title_news_block">
                            <h3 style="font-size:13px;">
                                {$news[0].title}
                            </h3>
                        </div>
                    </div>
                    <div >
                        {$news[0].excerpt}
                    </div>
                </div>
            </a>
        </div>
        <div class="col-11 col-sm  hidden-sm-down"  style="margin-bottom: 1rem;">
            <a href="{$news[1].link}" target="_blank" class="container_news_block">
                <div >
                    <div style="position:relative;">
                        <div class="img_container_news_block">
                                <img class="image_news_block" src="{$news[1].img}"/>
                        </div>

                        <div class="title_news_block">
                            <h3 style="font-size:13px;">
                                {$news[1].title}
                            </h3>
                        </div>
                    </div>
                    <div >
                        {$news[1].excerpt}
                    </div>
                </div>
            </a>
        </div>

        <div class="col-11 col-sm hidden-lg-down"  style="margin-bottom: 1rem;">
            <a href="{$news[2].link}" target="_blank" class="container_news_block">
                <div >
                    <div style="position:relative;">
                        <div class="img_container_news_block">
                                <img class="image_news_block" src="{$news[2].img}"/>
                        </div>

                        <div class="title_news_block">
                            <h3 style="font-size:13px;">
                                {$news[2].title}
                            </h3>
                        </div>
                    </div>
                    <div >
                        {$news[2].excerpt}
                    </div>
                </div>
            </a>
        </div>

    </div>
</section>