<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome-animation/0.0.10/font-awesome-animation.min.css">
<section id="find_doctor">
    <div class="find_doctor">
        <div class="row">
          <div class="col"> <h2>Finden Sie Ihren neuen Lieblingsarzt</h2></div>
        </div>
        <br/><br/>
        <div class="row align-items-center">
          <div class="col-md-5 offset-md-1 faa-parent animated-hover">
              <div class="background_finnd_doctor">
                <i class="fa fa-users fa-3x faa-pulse find_doctor" aria-hidden="true" ></i><span class="find_doctor">Bereits 2.000 Patienten nutzen unsere Seite täglich für Ihre Arztsuche und Terminbuchung! Machen auch Sie es sich einfach!</span>
              </div>
          </div>
          <div class="col-md-5 faa-parent animated-hover">
              <div class="background_finnd_doctor">
                <i class="fa fa-thumbs-up fa-3x faa-pulse find_doctor" aria-hidden="true"></i><span class="find_doctor">Informieren Sie sich über unsere Ärzte, lesen Sie Patientenbewertungen und finden Sie Ihren Wunscharzt!</span>
              </div>
          </div>
        </div>

        <div class="row">
          <div class="col">
              <p style="text-align: center">
                <a class="find_doctor" href="/suche?form%5Bmedical_specialty_id%5D=1&form%5Blocation%5D={$this->getUserCity()}&form%5Binsurance_id%5D=2">Ärzte in meiner Nähe anzeigen</a>
              </p>
          </div>
        </div>
    </div>
</section>

