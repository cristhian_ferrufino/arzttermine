<div id="signup" class="section">
	<div class="form_container">
		<form action="{$this->url_form}" method="post" enctype="application/x-www-form-urlencoded" style="display:inline;">
			<div class="field"><label for="form_first_name">Vorname:</label><input id="form_first_name" name="form[first_name]" class="input_text{$this->hasMessages('SIGNIN_FIRSTNAME',' input_error')}" value="{$this->form($this->form.first_name)}" />{$this->getHtmlFormMessages('SIGNIN_FIRSTNAME','field')}</div>
			<div class="field"><label for="form_last_name">Name: <span class="label_optional">(optional)</span></label><input id="form_last_name" name="form[last_name]" class="input_text{$this->hasMessages('SIGNIN_LASTNAME',' input_error')}" value="{$this->form($this->form.last_name)}" />{$this->getHtmlFormMessages('SIGNIN_LASTNAME','field')}</div>
			<div class="field"><label for="form_email">E-Mail:</label><input id="form_email" name="form[email]" class="input_text{$this->hasMessages('SIGNIN_EMAIL',' input_error')}" value="{$this->form($this->form.email)}" />{$this->getHtmlFormMessages('SIGNIN_EMAIL','field')}</div>
			<div class="field"><label for="form_password">Passwort:</label><input type="password" id="form_password" name="form[password]" class="input_text{$this->hasMessages('SIGNIN_PASSWORD',' input_error')}" maxlength="20" value="{$this->form($this->form.password)}" />{$this->getHtmlFormMessages('SIGNIN_PASSWORD','field')}</div>
			<div class="field"><label for="form_password2">Passwort wiederholen:</label><input type="password" id="form_password2" name="form[password2]" class="input_text{$this->hasMessages('SIGNIN_PASSWORD2',' input_error')}" maxlength="20" value="{$this->form($this->form.password2)}" />{$this->getHtmlFormMessages('SIGNIN_PASSWORD2','field')}</div>
			<div class="field"><label>Geschlecht:</label>
				<input id="form_gender" name="form[gender]" type="radio" value="0"{if $this->form.gender=="0"} checked="checked"{/if} style="margin:0px 4px 0px 0px;">männlich
				<input id="form_gender" name="form[gender]" type="radio" value="1"{if $this->form.gender=="1"} checked="checked"{/if} class="input_radio{$this->hasMessages('SIGNIN_GENDER',' input_error')}" style="margin:0px 4px 0px 10px;">weiblich{$this->getHtmlFormMessages('SIGNIN_GENDER','field')}</div>
			<div class="field"><input id="agb" type="checkbox" name="form[agb]" value="1" class="input_checkbox{$this->hasMessages('SIGNIN_AGB',' input_error')}">Ich habe die allgemeinen <a href="/nutzungsbedingungen.html" target="_blank" style="font-weight:bold;">Nutzungsbedingungen</a> gelesen und akzeptiere diese.{$this->getHtmlFormMessages('SIGNIN_AGB','field_checkbox')}</div>
			<div class="field"><input id="datenschutz" type="checkbox" name="form[datenschutz]" value="1" class="input_checkbox{$this->hasMessages('SIGNIN_DATENSCHUTZ',' input_error')}">Der <a href="/datenschutzerklaerung.html" target="_blank" style="font-weight:bold;">Datenschutzerklärung</a> stimme ich zu.{$this->getHtmlFormMessages('SIGNIN_DATENSCHUTZ','field_checkbox')}</div>
			<div class="field submit">
				<input type="hidden" name="action" value="new" />
				<input class="input_submit" type="submit" value="Anmelden" />
			</div>
			<div class="clear"></div>
		</form>
	</div>
	<div class="clear"></div>
</div>
