<div style="display:none;" id="login-quick" class="content">
	<h2>Login</h2>
	<form name="login_form" id="login_form" method="post" action="{$this->url_form}">
		<div class="field">
			<label for="email">E-Mail</label>
			<input class="input_text{$this->hasMessages('LOGIN_EMAIL',' input_error')}" value="{$this->form($this->email)}" id="email" name="email" type="text" />
			{$this->getHtmlFormMessages('LOGIN_EMAIL','field')}
		</div>
		<div class="field">
			<label for="password">Passwort</label>
			<input class="input_text" id="password" name="password" type="password" maxlength="20" />
		</div>
{*}		<div class="field"><a href="/login.php?a=passwort" class="password_reset_link" rel="nofollow">Passwort vergessen?</a></div>{/*}
		<div class="field submit">
			<input type="hidden" name="login" value="login" />
			<input class="input_submit button medium orange" type="submit" value="Login" />
		</div>
		<div class="clear"></div>
	</form>
</div>
{literal}
<script type="text/javascript">
//<![CDATA[
$("#login-quick-link").toggle(
function () {
	$("#login-quick").css({"display":"block"});
},function () {
	$("#login-quick").css({"display":"none"});
});
//]]>
</script>
{/literal}
