<h2 class="text_indent neuespasswort">Neues Passwort eingeben</h2>
{if $this->new_user}
<p>Hier kannst du dir ein Passwort geben und damit deinen Account freischalten.</p>
{else}
<p>Gib bitte dein neues Passwort ein.</p>
{/if}
<div id="password_reset" class="section">
	<div class="form_container">
		<form action="{$this->url_form}" method="post" enctype="application/x-www-form-urlencoded" style="display:inline;">
			<div class="field">
				<label for="form_password">Passwort:</label><input id="form_password" name="form[password]" class="input_text{$this->hasMessages('password',' input_error')}" maxlength="20" type="password" value="{$this->form($this->form.password)}" />{$this->getHtmlFormMessages('password','field')}
			</div>
			<div class="field">
				<label for="form_password2">Passwort wiederholen:</label><input id="form_password2" name="form[password2]" class="input_text{$this->hasMessages('password2',' input_error')}" maxlength="20" type="password" value="{$this->form($this->form.password2)}" />{$this->getHtmlFormMessages('password2','field')}
			</div>
			<p class="submit">
				<input type="hidden" name="c" value="{$this->code}" />
				<input type="hidden" name="n" value="{$this->new_user}" />
				<input type="hidden" name="sa" value="send" />
				<input class="input_submit" type="image" src="/media/arrow_passwort-aendern.png" value="Passwort aendern" />
			</p>
			<div class="clear"></div>
		</form>
	</div>
</div>
