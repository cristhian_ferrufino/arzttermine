<h2 class="text_indent passwortvergessen">Passwort vergessen?</h2>
<p>Du hast dein Passwort vergessen? Kein Problem! Wir schicken dir eine E-Mail, mit der du dein Passwort ändern kannst.</p>
<div id="password_reset" class="section">
	<div class="form_container">
		<form action="{$this->url_form}" method="post" enctype="application/x-www-form-urlencoded" style="display:inline;">
			<div class="field">
				<label for="form_email">E-Mail:</label><input id="form_email" name="form[email]" class="input_text{$this->hasMessages('email',' input_error')}" maxlength="100" value="{$this->form($this->form.email)}" />{$this->getHtmlFormMessages('email','field')}
			</div>
			<p class="field submit">
				<input type="hidden" name="sa" value="send" />
				<input class="input_submit button-medium orange" type="image" value="Passwort anfordern" />
			</p>
			<div class="clear"></div>
		</form>
	</div>
</div>
