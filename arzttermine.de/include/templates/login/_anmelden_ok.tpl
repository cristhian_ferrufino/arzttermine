<div id="signup_and_login">
	<h1>Vielen Dank für Ihre Anmeldung!</h1>
	Wir haben Ihnen eine E-Mail geschickt, in der Sie einen Bestätigungslink finden. Klicken Sie auf diesen Link, um Ihre angegebene E-Mailadresse zu bestätigen.<br />
	Sehen Sie bitte auch in Ihrem Spamordner nach, falls Sie die Bestätigungsmail nicht in Ihrem Eingangspostfach finden. Einige Provider sortieren gundsätzlich alle neuen Absender dort weg.<br />
	<br /><br />
	Vielen Dank und viel Spaß bei KiTa.de!</p>
</div>
