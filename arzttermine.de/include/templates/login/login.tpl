{strip}
    {assign var="template_body_class" value="static"}
    {*{$this->addCssInclude($this->getStaticUrl('old-css/login_1_0.css'))}*}
    {include file="header.tpl"}
{/strip}

<div class="container full">
    <main>
        <section id="content">
            {include file="login/_login.tpl"}
        </section>
    </main>
</div>
{include file="footer.tpl"}