<div id="discover">
  <h4>Informationen</h4>
  <ul>
    <li><a href="/faq/faq-fuer-patienten">FAQ</a></li>
    <li><a href="/fachgebiete" title="Informationen zu medizinische Fachgebiete">Medizinische Fachgebiete</a></li>
    <li><a href="/magazin/">Magazin</a></li>
    <li><a href="/news">News</a></li>
    <li><a href="/presse">Pressebereich</a></li>
  </ul>
</div>
<div id="Company">
  <h4>Unternehmen</h4>
  <ul>
    <li><a href="/kontakt">Kontakt</a></li>
    <li><a href="/jobs">Jobs</a></li>
    <li><a href="/lp/praxis-marketing/praxis-marketing.html">Als Arzt registrieren</a></li>
    <li><a href="/praxisoptimierung">Praxisoptimierung</a></li>
    <li><a href="/aerzte-magazin" target="_blank">&Auml;rzte Magazin</a></li>
    <li><a href="/lp/patienten/so-funktioniert-arzttermine/">Patienten Informationen</a></li>
    <li><a href="/angebote-informationen">Angebot Informationen</a></li>
  </ul>
</div>
<div id="social">
  <h4>Social</h4>
  <ul>
    <li><a href="http://facebook.com/arzttermine">Facebook</a></li>
    <li><a href="https://www.xing.com/companies/arzttermine.de">Xing</a></li>
    <li><a href="https://twitter.com/Arzttermine">Twitter</a></li>
    <li><a href="//plus.google.com/+ArzttermineDe">Google+</a></li>
    <li><a href="http://www.linkedin.com/company/arzttermine-de">LinkedIn</a></li>
  </ul>
</div>
