<div class="rating" {$microdata}>
    <div style="width: {$rating/5*100}%"></div>
    <meta itemprop="worstRating" content="1">
    <meta itemprop="ratingValue" content="{$rating}">
    <meta itemprop="bestRating" content="5">
</div>
