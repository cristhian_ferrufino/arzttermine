<section id="doctors">
    <h2>&Uuml;berzeugen Sie sich von unseren &Auml;rzten</h2>
    <ul class="inlined owl-carousel owl-theme">
        {foreach $featured_doctors as $doc}
            <li class="item"  itemscope itemtype="http://schema.org/Physician">
                <a href="{$doc.url}/gkv" itemprop="url" data-doc-name="{$doc.name}" data-location="{$doc.city}" data-med-name="{$doc.specialty}" data-med-id="{$doc.specialty_id}">
                    <div class="round-image">
                        <img src="{$doc.profile}" alt="" itemprop="image">
                    </div>
                    <div class="name" itemprop="name">{$doc.name}</div>
                </a>
                <div class="specialty" itemprop="medicalSpecialty">{$doc.specialty}</div>
                <div class="city" itemprop="location">{$doc.city}</div>
            </li>
        {/foreach}
    </ul>
</section>