{if $asset_thumbs|@count>0}

<div class="gallery-container">
	<ul>
{foreach from=$asset_thumbs item='asset'}
		<li><a class="group" rel="gallery" title="{alt($asset->getDescription())}" href="{$asset->getGfxUrl($smarty.const.ASSET_GFX_SIZE_STANDARD)}"><img src="{$asset->getGfxUrl($smarty.const.ASSET_GFX_SIZE_THUMBNAIL)}" alt="{alt($asset->getDescription())}" asset_id="{$asset->getId()}" /></a></li>
{/foreach}
	</ul>
{if $asset_hiddens|@count>0}
{foreach from=$asset_hiddens item='asset'}
		<a class="group hidden" rel="gallery" title="{alt($asset->getDescription())}" href="{$asset->getGfxUrl($smarty.const.ASSET_GFX_SIZE_STANDARD)}"><img src="{$asset->getGfxUrl($smarty.const.ASSET_GFX_SIZE_THUMBNAIL)}" alt="{alt($asset->getDescription())}" asset_id="{$asset->getId()}" /></a>
{/foreach}
{/if}
	<div class="clear"></div>
</div>
{/if}
