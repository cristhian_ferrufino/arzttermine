{include file="./header.html.tpl"}
<p><strong>{$booking_salutation_full_name},</strong></p>

<p><strong>vielen Dank für Ihre Terminbuchung über Arzttermine.de!</strong></p>

<p>
    Die Praxis <em>{$location_name}</em> hat uns eine Verschiebung Ihres Termins um wenige Minuten auf
    {$appointment_date} um {$appointment_time} Uhr
    gemeldet.
</p>
{include file="mailing/_contact_us_for_cancelation.html.tpl"}

{include file="./footer.html.tpl"}