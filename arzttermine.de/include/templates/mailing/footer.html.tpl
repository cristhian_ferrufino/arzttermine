                  <table cellpadding="0" cellspacing="0" width="100%" height="170">
                    <tr>
                      <td valign="middle" style="width: 280px; font-family: Helvetica;font-size: 14px; color: #313131;">
                        <br/><br/>
                        <div>
                          <a href="http://www.arzttermine.de/kontakt" style="font-weight:bold; color: #0090d6; font-family: Helvetica;font-size: 14px;text-decoration:none">
                            Patientensupport - Arzttermine.de
                          </a>
                        </div>
                        <div style="font-family: Helvetica;font-size: 14px; color: #313131;">Rosenthaler Straße 51 | 10178 Berlin</div>
                        <div style="font-family: Helvetica;font-size: 14px; color: #313131;">
                          Kostenfreie Rufnummer:
                          <span>
                            <a href="tel:08002222133" style="font-family: Helvetica;font-size: 14px;font-weight:bold; color: #0090d6;text-decoration:none">
                              0800 / 22 22 133
                            </a>
                          </span>
                        </div>
                        <span>
                          <a href="mailto:buchung@arzttermine.de" style="font-family: Helvetica;font-size: 14px;font-weight:bold; color: #0090d6;text-decoration:none" >buchung@arzttermine.de</a>
                        </span>
                      </td>
                      <td valign="bottom" style="text-align:center;">
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <table width="600" height="64" style="border: 1px solid #ccc; border-radius: 0px 0px  9px 9px; -webkit-border-radius: 0px 0px  9px 9px; -moz-border-radius: 0px 0px  9px 9px; background-clip: padding-box;  box-shadow: 0 0 5px rgba(0,0,0,.1), inset 0 0 0 1px #fff; background-color: #f1f1f1; color: #5395a7; font-family: Helvetica;font-size: 12px;text-shadow: 0 1px 3px #fff;">
              <tr>
                <td>
                  <table style="padding-left:15px;" height="64">
                    <tr style="color: #0090d6; font-family: Helvetica;font-size: 12px;text-shadow: 0 1px 3px #fff;">
                      <td valign="middle">
                        <a href="http://www.arzttermine.de/kontakt" style="color: #0090d6; font-family: Helvetica;font-size: 12px;text-shadow: 0 1px 3px #fff;text-decoration:none">
                          Kontakt
                        </a>&nbsp;&nbsp;&nbsp;&nbsp;
                      </td>
                      <td valign="middle">
                        <a href="http://www.arzttermine.de/impressum" style="color: #0090d6; font-family: Helvetica;font-size: 12px;text-shadow: 0 1px 3px #fff;text-decoration:none">
                          Impressum
                        </a>&nbsp;&nbsp;&nbsp;&nbsp;
                      </td>
                      <td valign="middle">
                        <a href="http://www.arzttermine.de/datenschutz" style="color: #0090d6; font-family: Helvetica;font-size: 12px;text-shadow: 0 1px 3px #fff;text-decoration:none">
                          Datenschutz
                        </a>&nbsp;&nbsp;&nbsp;&nbsp;
                      </td>
                      <td valign="middle">
                        <a href="http://www.arzttermine.de/agb" style="color: #0090d6; font-family: Helvetica;font-size: 12px;text-shadow: 0 1px 3px #fff;text-decoration:none">
                          AGB
                        </a>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
