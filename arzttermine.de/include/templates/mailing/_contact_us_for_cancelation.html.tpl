<p>
    <strong>Sollten Sie den Termin nicht wahrnehmen können, bitten wir um eine zeitnahe Absage.</strong>
    Antworten Sie dafür einfach auf diese Email mit dem Betreff "Absage"
    oder rufen Sie uns kostenfrei unter der Nummer 0800/22 22 133 an.
</p>
<p>
	Sollte eine Verschiebung des Termines nötig sein, übernehmen wir dies gerne für Sie. 
	Kontaktieren Sie auch dazu gerne unseren Patienten Support.
</p>

