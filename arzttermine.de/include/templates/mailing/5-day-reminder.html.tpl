{include file="./header.html.tpl"}
<p><strong>{$booking_salutation_full_name},</strong></p>

<p>bitte denken Sie an Ihren Arzttermin in 5 Tagen am {$appointment_date} um {$appointment_time} Uhr bei {$user_name}.</p>
<p>Die Adresse der Praxis lautet:</p>
<p>{$location_address}</p>

{include file="./_contact_us_for_cancelation.html.tpl"}

{include file="./footer.html.tpl"}