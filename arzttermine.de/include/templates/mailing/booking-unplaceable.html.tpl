{include file="./header.html.tpl"}

<p>
Leider konnte Ihr Termin am <span style="font-family: Helvetica; color: #555; font-size: 14px;">am <strong>{$appointment_date}</strong></span> in der Praxis: <br/><strong><span style="font-family: Helvetica; color: #555; font-size: 14px;">{$location_name}</span></strong> nicht wie gewünscht bestätigt werden. Dies kann verschiedene Ursachen haben.
</p>
<p>
	Um einen neuen Termin in Ihrer Wunschpraxis zu buchen, kontaktieren Sie gerne unseren Patienten Support unter der Rufnummer 0800 / 22 22 133 oder schreiben Sie Mail an buchung@arzttermine.de. 
	Über unsere Website Arzttermine.de können Sie ebenfalls schnell und einfach einen neuen Termin in Ihrer Wunschpraxis buchen.
</p>
<p>
    Für die Umstände bitten wir um Entschuldigung und hoffen, dass Sie unseren Service zukünftig weiterhin in Anspruch nehmen möchten.
</p>

{include file="./footer.html.tpl"}