{include file="./header.html.tpl"}
<p><strong>{$booking_salutation_full_name}</strong></p>

<p>
    Hiermit bestätigen wir Ihnen die Stornierung Ihres Termins für
    {$user_name} in der Praxis {$location_name} {$conditional_appointment_date}.
</p>

<p>
    Gerne möchten wir Ihnen einen alternativ Termin in Ihrer Wunschpraxis anbieten.
    Hierzu können Sie uns wie gewohnt über unsere Kontaktmöglichkeiten erreichen.
</p>
<p>
    Wir sind sicher, dass wir für Sie eine gute Alternative zum stornierten Termin finden werden.
</p>

{include file="./footer.html.tpl"}