{include file="./header.html.tpl"}
<p><strong>{$booking_salutation_full_name},</strong></p>

<p>vielen Dank für Ihre Terminanfrage über Arzttermine.de über unsere aktuelle Gutscheinaktion!</p>
<p>Aktuell erreichen uns sehr viele Terminanfragen von Patienten. Die Bearbeitung Ihrer Terminanfrage kann daher ein wenig länger dauern als gewöhnlich. Bitte haben Sie etwas Geduld und vermeiden Sie bitte Doppelbuchungen Ihrer Terminanfragen.<br />
Wir werden Ihre Anfrage innerhalb von 1-3 Tagen bearbeiten.<br />
Bitte beachten Sie, dass Anfragen, die am Wochenende getätigt werden, erst ab dem folgenden Montag bearbeitet werden können.</p>
<p>Mit dieser E-Mail bestätigen wir Ihnen den Empfang Ihrer Buchungsanfrage für</p>
<center>
    <table width="314" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td style="background:url('http://www.arzttermine.de/static/img/newsletter/termin_bg.jpg'); width:314px; height: 151px;">
                <table height="151px" cellspacing="0" cellpadding="0" border="0" style="padding-left: 17px;">
                    <tr>
                        <td height="42px" style="font-weight: bold; color: #fff;font-family: Helvetica;font-size: 14px;text-shadow: 0 1px 0 #2f738d;">Buchungsanfrage</td>
                    </tr>
                    <tr>
                        <td height="53px" valign="bottom"><strong><span style="font-family: Helvetica; color: #555; font-size: 14px;">{$user_name}</span></strong>
                            <br/><span style="font-family: Helvetica; color: #313131; font-size: 14px;">{$location_address}</span></td>
                    </tr>
                    <tr>
                        <td><span style="font-family: Helvetica; color: #313131; font-size: 14px;">am <strong>{$appointment_date} um {$appointment_time} Uhr.</strong></span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</center>

<p>
    Zur Einlösung des Gutscheins beachten Sie bitte unsere Teilnahmebedinungen.<br />
    Sie erhalten den Gutschein nach Ihrem wahrgenommenen Termin ausschließlich über Arzttermine.de und nicht in der jeweiligen Praxis.<br />
    Bitte beachten Sie, dass aus organisatorischen Gründen der Versand des Gutscheins in etwa 8 Wochen in Anspruch nehmen kann.<br />
    Ein wahrgenommener Termin ist die Vorraussetzung für den Versand des Gutscheins.
</p>
<p>
    <strong>Unser Patientensupport wird so schnell wie möglich mit Ihnen Kontakt aufnehmen</strong>
    <span style="font-style:italic">(Montag bis Freitag zwischen 08:00 und 18:00 Uhr)</span>.
    Wir möchten damit Ihren Besuch in der Praxis so angenehm und einfach wie möglich gestalten.
    <br/>
    <br/>Sollte Ihr Termin an einem Montag stattfinden, werden unsere Servicemitarbeiter Sie
    Montag ab 8 Uhr zur Rückbestätigung kontaktieren. Bitte haben Sie bis dahin ein wenig Geduld.
</p>

{include file="./footer.html.tpl"}