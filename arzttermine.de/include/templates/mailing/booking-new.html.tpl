{include file="./header.html.tpl"}
<p><strong>{$booking_salutation_full_name},</strong></p>

<p>vielen Dank für Ihre Terminanfrage über Arzttermine.de!</p>
<p>Mit dieser E-Mail bestätigen wir Ihnen den Empfang Ihrer <strong>Buchungsanfrage</strong> für</p>
<center>
    <table width="314" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td style="background:url('http://www.arzttermine.de/static/img/newsletter/termin_bg.jpg'); width:314px; height: 151px;">
                <table height="151px" cellspacing="0" cellpadding="0" border="0" style="padding-left: 17px;">
                    <tr>
                        <td height="42px" style="font-weight: bold; color: #fff;font-family: Helvetica;font-size: 14px;text-shadow: 0 1px 0 #2f738d;">Buchungsanfrage</td>
                    </tr>
                    <tr>
                        <td height="53px" valign="bottom"><strong><span style="font-family: Helvetica; color: #555; font-size: 14px;">{$user_name}</span></strong>
                            <br/><span style="font-family: Helvetica; color: #313131; font-size: 14px;">{$location_address}</span></td>
                    </tr>
                    <tr>
                        <td><span style="font-family: Helvetica; color: #313131; font-size: 14px;">am <strong>{$appointment_date} um {$appointment_time} Uhr.</strong></span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</center>

<p>
	<strong>Das sind die nächsten Schritte:</strong>
	<br/>
	<br/>
	Ihre Terminanfrage wird nun von uns bearbeitet. Sobald diese geprüft wurde, erhalten Sie von uns eine Terminbestätigung.
	<br/>
	Bei einer Rückfrage wird unser Patientensupport mit Ihnen Kontakt aufnehmen <span style="font-style:italic"> (Montag bis Freitag zwischen 08:00 und 18:00 Uhr)</span>.<br/>
	Wir möchten damit Ihren Besuch in der Praxis so angenehm und einfach wie möglich gestalten.
    <br/>
    <br/>Sollte Ihr Termin an einem Montag stattfinden, werden unsere Servicemitarbeiter Sie
    Montag ab 8 Uhr zur Rückbestätigung kontaktieren. Bitte haben Sie bis dahin ein wenig Geduld.
	<br/>
	<br/>
	Bei Rückfragen können Sie uns gerne über unsere kostenfreie Hotline oder auch per Mail an buchung@arzttermine.de erreichen.
</p>

{include file="./footer.html.tpl"}