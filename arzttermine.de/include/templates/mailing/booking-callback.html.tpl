{include file="./header.html.tpl"}
<p><strong>{$booking_salutation_full_name},</strong></p>

<p>vielen Dank für Ihre Terminbuchung über Arzttermine.de!</p>

<p>
Zu Ihrem Termin <span style="font-family: Helvetica; color: #555; font-size: 14px;"><strong>{$conditional_appointment_date}</strong></span> in der Praxis: <br/><strong><span style="font-family: Helvetica; color: #555; font-size: 14px;">{$location_name}</span></strong> hat sich eine wichtige Rückfrage ergeben.
</p>

<p>
<strong>Ihr Wunschtermin kann leider bis zur Klärung dieser Rückfrage nicht gebucht werden.</strong>
</p>
<p>
Bitte rufen Sie uns zwischen 7 - 20 Uhr unter der kostenfreien Rufnummer 0800 22 22 133 (0 cent/ Minute aus dem Fest- und Mobilfunknetz) zurück.
</p>
<p>
Alternativ können Sie auch auf diese Mail antworten und uns eine Zeit nennen, zu der wir Sie sicher erreichen können. Gerne rufen wir Sie dann zu dieser Zeit zurück. 
</p>
<p>
Wir werden natürlich weiter versuchen, Sie telefonisch zu erreichen, um Ihren Termin schnellst möglich bestätigen zu können.
</p>
<p>
Bitte bedenken Sie, dass es möglich ist, dass wir unter Umständen kurz vor Ihrem angefragten Termin Ihre Terminanfrage stornieren müssen, wenn wir bis dahin die offene Rückfrage nicht mit Ihnen klären konnten.
</p>

{include file="./footer.html.tpl"}