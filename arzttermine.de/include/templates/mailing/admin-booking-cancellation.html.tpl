{include file="./header.html.tpl"}

Bitte prüfen!

{$name} hat im Patientenaccount einen Termin storniert:

Arzt: {$user_name}
Praxis: {$location_name}
Termin: {$appointment_date}
BookingID: {$booking_id}

{include file="./footer.html.tpl"}