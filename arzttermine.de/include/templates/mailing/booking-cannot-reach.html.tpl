{include file="./header.html.tpl"}
<p>
	Vielen Dank für Ihre Anfrage über Arzttermine.de.
</p>
<p>
    Leider haben wir Sie unter der von Ihnen angegebenen Rufnummer telefonisch nicht erreichen können.
</p>
<p>
	Es hat sich noch eine Rückfrage zu Ihrem angefragten Termin <span style="font-family: Helvetica; color: #555; font-size: 14px;"><strong>{$conditional_appointment_date}</strong></span> in der Praxis: <br/><strong><span style="font-family: Helvetica; color: #555; font-size: 14px;">{$location_name}</span></strong> ergeben.
</p>
<p>
	<strong>Ihre Buchung konnte bis zum jetzigen Zeitpunkt noch nicht bestätigt werden.</strong>
</p>

<p>
	Wir möchten Sie daher um eine kurze Rückmeldung bitten, um eventuelle Rückfragen bezüglich Ihrer Behandlung zu besprechen und somit Ihren Arzttermin so einfach und angenehm wie möglich gestalten zu können.
</p>

<p>
	Weiterhin benötigen wir Ihre korrekte Rufnummer, damit Sie die von Ihnen ausgewählte Praxis im Notfall telefonisch erreichen kann.
</p>

<p>
    Bitte antworten Sie daher kurz auf diese Email oder rufen Sie uns kostenfrei unter der Nummer 0800 22 22 133 an.
</p>
<p>
	Gerne vereinbaren wir mit Ihnen auch gemeinsam einen Alternativtermin in Ihrer Wunschpraxis.
</p>

{include file="./footer.html.tpl"}