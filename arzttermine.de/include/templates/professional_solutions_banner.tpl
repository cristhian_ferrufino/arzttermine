<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome-animation/0.0.10/font-awesome-animation.min.css">
<section id="professional_solutions_banner">
    <div class="doctor_solutions" style="padding-left:.5rem ">
        <div class="row">

            <div class="col-md-6 offset-md-1">
                <h4 style="margin-top: 2rem; line-height: 1.6rem;">Professionelle Lösungen für Ihre Praxis:<br/> Online und Offline</h4>
                <p style="margin: .5rem 0 1.6rem 0;">Unser Komplettservice garantiert einwandfreie Praxisabläufe und stetig wachsende Erfolgsquoten!</p>
                <div class="row">
                    <div class="col-2"><img src="../static/images/assets/solutions/1.png" ></div>
                    <div class="col-2"><img src="../static/images/assets/solutions/2.png" ></div>
                    <div class="col-2"><img src="../static/images/assets/solutions/3.png" ></div>
                    <div class="col-2"><img src="../static/images/assets/solutions/4.png" ></div>
                    <div class="col-2"><img src="../static/images/assets/solutions/5.png" ></div>
                </div>

                <div class="row" >
                    <div class="col">
                        <a class="find_doctor" href="/aerzte/registrierung" style="margin: 1rem 0 1rem 0;">Jetzt Praxis kostenfrei eintragen</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>