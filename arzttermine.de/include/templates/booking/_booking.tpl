<div class="container">
  <main>
    <section id="info-block">
      {strip}
      <h3>{if $location->hasAppointmentEnquiryIntegration()}
        {$app->_('Terminanfrage')}
        {else}
        {$app->_('Ihre Terminwahl')}
        {/if}
      </h3>
      {/strip}

      <div class="orange-bar">
      {if $integration_id == 8}
      <div class="datetime unavailable">
        {$app->_('Bitte hinterlassen Sie uns Ihre Kontakdaten. Wir kümmern uns um den Rest und melden uns noch heute mit freien Terminvorschlägen bei Ihnen!')}
      </div>
      {elseif $integration_id == 9}
        <div class="datetime">{calendar action="getWeekdayDateTimeText" datetime=$form.start_at_id short=true} - {calendar action="getDateTimeFormat" datetime=$form.end_at_id format="H:i"} Uhr</div>
      {else}
        <div class="datetime"><span class="light">Am:</span> {calendar action="getWeekdayDateTimeText" datetime=$form.start_at_id short=true}</div>
      {/if}
      </div>
      <div class="doctor">
        <div class="photo">
          <img src="{$user->getProfileAssetUrl($smarty.const.ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED)}" alt="{alt($user->getName())}">
        </div>
        <div class="info">
          <div>
            <div class="conjunction">bei</div>
            <div class="name">{$user->getName()}</div>
            {if $location->hasPhoneVisible()}<div class="phone">{$location->getPhoneVisible()}</div>{/if}
          </div>
          <div class="address">
            <h4>Adresse</h4>
            <div class="practice-name">{$location->getName()}</div>
            <div class="street">{$location->getStreet()}</div>
            <div class="city">{$location->getCityZip()}</div>
          </div>
        </div>
      </div>
      <ul class="comfort-elements">
        <li class="cancelation">Sollten sich Ihre Pläne &auml;ndern, k&ouml;nnen Sie den Termin jederzeit stornieren.</li>
        <li class="contact">Unser Supportteam erreichen Sie kostenfrei unter <a href="tel:08002222133">0800/2222133</a></li>
        <li class="data-protection">Wir nutzen Ihre Daten ausschließlich wie in der <a href="/datenschutz">Datenschutzerklärung</a> festgelegt.</li>
      </ul>
   </section>

    <section id="form">
      <h3>Ihre Angaben</h3>
      {include file="booking/_form.tpl"}
    </section>
  </main>
</div>
