{strip}
{assign var="use_minimal_footer" value=true}
{assign var="template_body_class" value="two-cols-with-form"}
{assign var="additional_body_classes" value="booking"}
{assign var="hasFullHeader" value="false"}
{include file="header.tpl"}
{/strip}

{if $contentMain}
{$contentMain}
{/if}

{* In case of double booking, show a popup. *}
{if isset($patientHasSimilarBooking) && $patientHasSimilarBooking==true}
<div class="overlay-blocker">
  <section class="popup-message double-booking">
    <h1>Achtung!</h1>
    <div class="content">
      <div class="popup-message-icon"></div>
      <h2>Ein ähnlicher Termin existiert bereits</h2>
      <p>Laut unserem System haben Sie bereits eine ähnliche Terminbuchung durchgeführt. Wenn Sie Ihren bestehenden Termin verändern wollen, loggen Sie sich bitte ein und stornieren Sie den Termin, bevor Sie fortfahren.</p>
      <p>Oder wenden Sie sich für weitere Hilfe an unsere <em>kostenlose Kunden-Hotline <a href="tel:{$CONFIG.PHONE_SUPPORT_PLAIN}">{$CONFIG.PHONE_SUPPORT}</a>.</em></p>
      <p>Vielen Dank!</p>
      {*<a href="{url path='patient_login'}" target="_blank">Anmelden</a>*}
      {*<a href="{url path='patient_register'}" target="_blank">Registrieren</a>*}
      <button></button>
    </div>
  </section>
</div>


{* If the patient is blacklisted, show a popup. *}
{elseif isset($blackListPatient) && $blackListPatient==true}
<div class="overlay-blocker">
  <section class="popup-message bp-1">
    <h1>Achtung!</h1>
    <div class="content">
      <div class="popup-message-icon"></div>
      <h2>Die Buchung ist leider fehlgeschlagen!</h2>
      <p>
      {* If there's custom message, use it. Otherwise, use the default one. *}
      {if isset($blackListPatientMessage) && !empty($blackListPatientMessage)}
        {$blackListPatientMessage}
      {else}
        Leider konnte Ihr Termin nicht gebucht werden. Für mehr Informationen wenden Sie sich bitte an unseren Support und geben Sie den entsprechenden Fehlercode an.
      {/if}
      </p>
      <p><em>Kostenlose Telefonnumer Support: <a href="tel:{$CONFIG.PHONE_SUPPORT_PLAIN}">{$CONFIG.PHONE_SUPPORT}</a></em></p>
      <p>Nennen Sie uns den Fehlercode: <em>BP-1</em></p>
      <p>Vielen Dank!</p>
      <button></button>
    </div>
  </section>
</div>

{/if}

{include file="_referral-code-support.tpl"}

<script type="text/javascript" src="/static/js/validation.js" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="/static/js/vendor/chosen/chosen.min.css">

<script type="text/javascript">
var AT = AT || {};
AT._defer(function(){

  $('<script>').attr('src', '/static/js/vendor/chosen/chosen.jquery.min.js').appendTo($('body'));

  var $select = $("select[name*=insurance_provider_id]");
  var $options = $select.find('option');
  var defaultOptionIndex = 
    $options
    .filter(function(ind, el) {
      // This is not really good. Decide later how to handle multilanguage, or any different text.
      return $(el).html() == "Sonstige";
    })
    .index();
  
  var $insuranceProvidersAutocomplete;

  $select.addClass('chosen-hid-this').chosen({
    search_contains: true,
    no_results_fallback_option_index: defaultOptionIndex,
    prioritize_current_selection: false
  })
  .ready(function(){
    $insuranceProvidersAutocomplete = $('select[name*=insurance_provider_id]').siblings('.chosen-container:first');
    $insuranceProvidersAutocomplete.addClass('arzttermine');
    // When rendering's finished, update the visibility depending on the radio status (select already selected radio).
    $("input[name*=insurance_id]:checked").click();
  });



  $("input[name*=insurance_id]").on('click', function(){
    // When "gesetzlich" is (un)checked, show/hide the select box and related elements.
    if ($("#insurance-type-1").is(":checked")) {
      $insuranceProvidersAutocomplete.show();
      // We show everything except the original <select> box that is hidden by chosen.js.
      $insuranceProvidersAutocomplete.siblings(':not(select)').show();
    } else {
      $insuranceProvidersAutocomplete.hide();
      $insuranceProvidersAutocomplete.siblings().hide();
    }
  });

  var $form = $('#form form');
  AT.validation.monitor($form);

  $form.on('AT:form-validation:errors-found', function() {
    $form.attr("data-step2-error-count", $("#step-2 .form-element.error").length);
    if ($("#step-1").has(".form-element.error").length > 0) {
      $("#go-to-step1").click();
    }
  });

  $("#form #step-1 button").click(function() {
      // $("#form .step").hide();
      $("#form #step-1").addClass("inactive");
      $("#form #step-2").addClass("active");
      $("#steps li").removeClass("active");
      $("#steps li").eq(0).addClass("done");
      $("#steps li").eq(1).addClass("active");
    });

  $("#go-to-step1").click(function(){
      $("#form #step-1").removeClass("inactive");
      $("#form #step-2").removeClass("active");
    });

  // If there's value in the Gutschein field, add .referral to the form (which shows the Gutschein checkbox).
  $('#form input[name*=referral_code]').on('change paste keyup', function() {
    if($(this).val().length > 0) {
      $('#form form').addClass('referral');
      $('#form form').find("input[name*=referral_code_confirmation]").removeAttr('disabled');
    } else {
      $('#form form').removeClass('referral');
      $('#form form').find("input[name*=referral_code_confirmation]").attr('disabled', '');
    }
  });



  $(".overlay-blocker button").click(function(){
    $(".overlay-blocker").remove();
  });
});
</script>

{include file="footer.tpl"}
