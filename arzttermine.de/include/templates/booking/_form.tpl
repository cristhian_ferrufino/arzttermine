{* 
  @todo: it would be probably good if we can copy the logic for triggering the errors from the live version 
  it should do the job for now, and i'll revisit to improve it: client side validation, immediate check for
  double or blacklisted bookings etc.
*}
{* ouch, fix the error message hadling later *}
{if !isset($error_messages)}{$error_messages = []}{/if}
{if $error_messages|@count}
  {assign var="error_class" value="error"}
{else}
  {assign var="error_class" value=""}
{/if}
{assign var="isInt89" value=$location->getIntegrationId() == 9 || $location->getIntegrationId() == 8}

<form id="booking-form"
      {if $isInt89}style="height: 57rem;"{else}style="height: 47rem;"{/if}
      class="{$error_class} {if isset($form.referral_code)}referral{/if} {if $isInt89}int89{/if}"
      action="{$this->url_form_action}"
      method="post"
      enctype="application/x-www-form-urlencoded" >
  <div id="step-1" class="step">
    <div class="form-element {if in_array('gender', $error_messages)}{$error_class}{/if}" data-error-message="Bitte wählen Sie Ihre Anrede aus.">
      <div id="gender" class="radio-group">
        <h4>{$app->_('Anrede')}</h4>
        <input name="form[gender]"  type="radio" id="gender-1" value="1" required{if $form.gender == 1} checked="checked"{/if}><label for="gender-1">Frau</label>
        <input name="form[gender]"  type="radio" id="gender-2" value="0" required{if $form.gender == 0} checked="checked"{/if}><label for="gender-2">Herr</label>
      </div>
    </div>

    <div class="form-element {if in_array('first_name', $error_messages)}{$error_class}{/if}" data-error-message="Bitte geben Sie Ihren Vornamen ein.">
      <label>{$app->_('Vorname')}</label>
      <input type="text" name="form[first_name]" value="{$form.first_name}" required>
    </div>
    
    <div class="form-element {if in_array('last_name', $error_messages)}{$error_class}{/if}" data-error-message="Bitte geben Sie Ihren Nachnamen ein.">
      <label>{$app->_('Nachname')}</label>
      <input type="text" name="form[last_name]" value="{$form.last_name}" required>
    </div>
    
    <div class="form-element {if in_array('email', $error_messages)}{$error_class}{/if}" data-error-message="Bitte geben Sie Ihre E-Mail Adresse ein.">
      <label>{$app->_('E-Mail')}</label>
      <input type="text" name="form[email]" value="{$form.email}" required>
    </div>
    
    <div class="form-element {if in_array('phone', $error_messages)}{$error_class}{/if}" data-error-message="Bitte geben Sie eine gültige Telefonnummer ein.">
      <label>{$app->_('Mobil')}</label>
      <input type="text" name="form[phone]" value="{$form.phone}" required>
    </div>


    <div style="margin-bottom:50px;" class="form-element {if in_array('contact_preference', $error_messages)}{$error_class}{/if}" data-error-message="Bitte wählen Sie mindestens eine Kontaktart.">
      <label>{$app->_('Falls wir Fragen zu Ihrem Termin haben, auf welchem Weg k&ouml;nnen wir Sie kontaktieren ?')}</label>

      <label for="contact_preference_phone" class="form-check-inline" style="float:left;padding:0.1em 1.5em 0.1em 0.1em;font-weight:unset;">
          <input type="checkbox" class="form-check-input" id="contact_preference_phone" name="form[contact_preference][]" value="phone"{if in_array('phone', $form.contact_preference)} checked{/if} /> Telefon
      </label>

      <label for="contact_preference_sms" class="form-check-inline" style="float:left;padding:0.1em 1.5em 0.1em 0.1em;font-weight:unset;"> SMS
        <input type="checkbox" class="form-check-input" id="contact_preference_sms" name="form[contact_preference][]" value="sms"{if in_array('sms', $form.contact_preference)} checked{/if} />
      </label>

      <label for="contact_preference_email" class="form-check-inline" style="float:left;padding:0.1em 1.5em 0.1em 0.1em;font-weight:unset;"> Email
        <input type="checkbox" class="form-check-input" id="contact_preference_email" name="form[contact_preference][]" value="email"{if in_array('email', $form.contact_preference)} checked{/if} />
      </label>

      <label for="contact_preference_whatsapp" class="form-check-inline" style="float:left;padding:0.1em 1.5em 0.1em 0.1em;font-weight:unset;"> WhatsApp
          <input type="checkbox" class="form-check-input" id="contact_preference_whatsapp" name="form[contact_preference][]" value="whatsapp"{if in_array('whatsapp', $form.contact_preference)} checked{/if} />
      </label>
    </div>

    <div class="button-wrapper"><button type="button">Weiter</button></div>
  </div>
  
  <div id="step-2" class="step">
    <button id="go-to-step1" type="button"></button>
    
    <div class="form-element {if in_array('treatmenttype_id', $error_messages)}{$error_class}{/if}">
      <label>{$app->_('Behandlungsgrund')} <span class="optional-field">(freiwillig)</span></label>
      <select id="filter-treatment" name="form[treatmenttype_id]" >
        {html_options options=$treatmenttypes selected=$form.treatmenttype_id}
      </select>
    </div>
    
    <div class="form-element {if in_array('insurance_id', $error_messages)}{$error_class}{/if}" data-error-message="Bitte wählen Sie Ihre Versicherungsart aus.">
      <div id="insurance-type" class="radio-group">
        <h4>{$app->_('Versicherungsart')}</h4>
        <input name="form[insurance_id]" type="radio" id="insurance-type-2" value="1"{if $form.insurance_id == 1} checked{/if} required><label for="insurance-type-2">{$app->_('privat')}</label>
        <input name="form[insurance_id]" type="radio" id="insurance-type-1" value="2"{if $form.insurance_id == 2} checked{/if} required><label for="insurance-type-1">{$app->_('gesetzlich')}</label>
      </div>
    </div>
    
    <div class="form-element {if in_array('insurance_provider_id', $error_messages)}{$error_class}{/if}" data-error-message="Bitte wählen Sie Ihre Versicherung aus.">
      <label>{$app->_('Versicherung')} <span class="optional-field">(freiwillig)</span></label>
      <select name="form[insurance_provider_id]" data-placeholder="...bitte wählen...">
        {html_options options=$insuranceProviders selected=$form.insurance_provider_id}
      </select>
    </div>
    
    <div class="form-element {if in_array('returning_visitor', $error_messages)}{$error_class}{/if}" data-error-message="Bitte geben Sie an, ob Sie schon einmal bei diesem Arzt in Behandlung waren.">
      <div id="is-patient" class="radio-group">
        <h4>Waren Sie bereits Patient/in bei {$user->getName()}?</h4>
        <input name="form[returning_visitor]" type="radio" id="is-patient-2" value="0"{if $form.returning_visitor == 0} checked{/if} required><label for="is-patient-2">{$app->_('Nein')}</label>
        <input name="form[returning_visitor]" type="radio" id="is-patient-1" value="1"{if $form.returning_visitor == 1} checked{/if} required><label for="is-patient-1">{$app->_('Ja')}</label>
      </div>
    </div>
    
{* 
	Temporarily adding this condition, Axel suggests making a method in this template. 
*}
    {if $location->getIntegrationId() == 9 || $location->getIntegrationId() == 8}
    <div class="form-element">
      <label>{$app->_('Haben Sie noch einen Hinweis für uns?')}<span class="optional-field"> (freiwillig)</span></label>
      <textarea name="form[comment_patient]">{$form.comment_patient}</textarea>
    </div>
    {/if}

    <div class="form-element {if in_array('referral_code', $error_messages)}{$error_class}{/if}">
      <label>{$app->_('Empfehlungscode')} <span class="optional-field">(optional)</span></label>
      <input type="text" name="form[referral_code]" value="{if isset($form.referral_code)}{$form.referral_code}{/if}">
    </div>
    <div class="form-element {if in_array('form_referral_code_confirmation', $error_messages)}{$error_class}{/if}" data-error-message="Bitte bestätigen Sie die Gutschein Einwilligung." id="referral-code-confirmation">
      <input {if !isset($form.referral_code)}disabled{/if} type="hidden" name="form[referral_code_confirmation]" value="0">
      <input {if !isset($form.referral_code)}disabled{/if} id="form_referral_code_confirmation" type="checkbox" name="form[referral_code_confirmation]" value="1" {if $form.referral_code_confirmation=='1'} checked="checked"{/if} required>
      <label for="form_referral_code_confirmation">
        Ich akzeptiere die Teilnahmebedingungen der <a href="/angebote-informationen" target="_blank">Gutscheinaktion</a>
      </label>
    </div>
      
{*
	Temporarily adding this condition, Axel suggests making a method in this template. 
*}
    {if $location->getIntegrationId() == 9 || $location->getIntegrationId() == 8}
    <div class="form-element">
      <input id="form_flexible_location" type="checkbox" name="form[flexible_location]" value="1"{if $form.flexible_location=='1'} checked="checked"{/if}>
      <label for="form_flexible_location">{$app->_('Ich benötige den Termin dringend und bin mit einer Ausweichpraxis in der Nähe ebenfalls einverstanden.')}</label>
    </div>
    {/if}

    <div class="form-element {if in_array('agb', $error_messages)}{$error_class}{/if}" data-error-message="Bitte bestätigen Sie die Einwilligung.">
      <input type="checkbox" id="checkbox-tos" name="form[agb]" value="1" required{if $form.agb == 1} checked="checked"{/if}>
      <label for="checkbox-tos">{$booking_agb_confirmation_text}</label>
    </div>

    <input type="checkbox" id="checkbox-newsletter" name="form[newsletter_subscription]" value="1"{if $form.newsletter_subscription == 1} checked="checked"{/if}>
    <label for="checkbox-newsletter">{$booking_newsletter_text}</label>

    <input type="hidden" name="form[location_id]" value="{$form.location_id}">
    <input type="hidden" name="form[user_id]" value="{$form.user_id}">
    <input type="hidden" name="form[medical_specialty_id]" value="{$form.medical_specialty_id}">
    <input type="hidden" name="form[start_at_id]" value="{$form.start_at_id}">
    <input type="hidden" name="form[end_at_id]" value="{$form.end_at_id}">
    <input type="hidden" name="form[form_once]" value="{$form.form_once}">

    <div class="button-wrapper"><button type="submit">Termin buchen!</button></div>
  </div>
</form>   
