<section id="for-doctors">
  <h2 style="font-weight: 700;">Jetzt als Arzt kostenlos anmelden</h2>
  <form id="booking-form" action="/aerzte/registrierung?step=1" method="post" enctype="application/x-www-form-urlencoded">

      <div class="form-element" data-error-message="Bitte wählen Sie Ihre Anrede aus.">
        <div id="gender" class="radio-group">
          <h4>Anrede</h4>

              <input name="gender" type="radio" id="gender-1" value="1" data-track="true"><label for="gender-1">Frau</label>
              <input name="gender" type="radio" id="gender-2" value="0" data-track="true"><label for="gender-2">Herr</label>

        </div>
      </div>
        <div class="row">
          <div class="col-sm">
            <div class="form-element" data-error-message="The title must be at least 2 characters.">
              <label>Titel</label>
              <input type="text" name="title" value="" data-track="true">
            </div>
          </div>
          <div class="col">
            <div class="form-element" data-error-message="Bitte geben Sie Ihren Praxisnamen ein.">
              <label>Praxisname</label>
              <input type="text" name="practice_name" value="" data-track="true">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm">
            <div class="form-element" data-error-message="Bitte geben Sie Ihren Vornamen ein.">
              <label>Vorname</label>
              <input type="text" name="first_name" value="" data-track="true">
            </div>
          </div>
          <div class="col">
            <div class="form-element" data-error-message="Bitte geben Sie Ihren Nachnamen ein.">
              <label>Nachname</label>
              <input type="text" name="last_name" value="" data-track="true">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm">
            <div class="form-element" data-error-message="Bitte geben Sie Ihre E-Mail Adresse ein.">
              <label>E-Mail</label>
              <input type="email" name="email" value="" data-track="true">
            </div>
          </div>
          <div class="col">

            <div class="form-element" data-error-message="Bitte geben Sie eine gültige Telefonnummer ein.">
              <label>Telefon</label>
              <input type="tel" name="phone" value="" data-track="true">
            </div>
          </div>
        </div>
    <div class="button-wrapper"><button type="submit">Registrieren</button></div>
  </form>
</section>

