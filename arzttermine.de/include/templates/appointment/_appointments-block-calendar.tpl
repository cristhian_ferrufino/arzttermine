{assign var="isScrollable" 
	value=$appointmentsBlock.type == 'appointments' 
        || $appointmentsBlock.type == 'next-available-appointment'
        || $appointmentsBlock.type == 'integration_9'}

<div class="calendar type-{$appointmentsBlock.type}
			{if $appointmentsBlock.type != appointments} exception{/if}
			{if $isScrollable}scrollable{/if}" 
	data-user-id="{$user->getId()}"
	data-specialty-id="{implode(',',$user->getMedicalSpecialtyIds())}"
    data-insurance-id="{$search->getInsuranceId()}"
	data-location-id="{$location->getId()}">
  <div class="wrapper">
  {include file="appointment/_appointments-block.tpl"}
  </div>
  <div class="arrow left disabled"></div>
  <div class="arrow right"></div>

  {* NEXT AVAILABLE APPOINTMENT ===================================================================================== *}  
  {if $appointmentsBlock.type == 'next-available-appointment'}
  {assign var="appointment" value=$appointmentsBlock.appointment}
  <div class="action {$appointmentsBlock.type}">
    <div class="message">
      <a data-next-appointment-date="{$appointment->getDate()}" rel="nofollow">{$appointment->getDateTimeText()}</a>
      <div>Nächster freier Termin:</div>
    </div>
  </div>

  {* CUSTOM MESSAGE ================================================================================================= *}  
  {elseif $appointmentsBlock.type == 'custom-message'}
  <div class="action {$appointmentsBlock.type}">
    <div class="message">{$appointmentsBlock.customMessage}</div>
  </div>

  {* NO AVAILABLE APPOINTMENTS ====================================================================================== *}
  {elseif $appointmentsBlock.type == 'no-available-appointments'}
  <div class="action {$appointmentsBlock.type}">
    <div class="message">keine freien Termine verfügbar</div>
  </div>

  {* NO SELECTED INSURANCE ========================================================================================== *}
  {elseif $appointmentsBlock.type == 'no-selected-insurance'}
  <div class="action {$appointmentsBlock.type}">
    <div class="message">
      <div>Zum Termin buchen bitte Versicherungsart auswählen:</div>
      <a href="{$user->getUrl()}/pkv" rel="nofollow">Privat</a>
      oder
      <a href="{$user->getUrl()}/gkv" rel="nofollow">Gesetzlich</a>
    </div>
  </div>

  {* INTEGRATION 8 ================================================================================================== *}
  {elseif $appointmentsBlock.type == 'integration_8'}
    <div class="action {$appointmentsBlock.type}">
      <div class="message">
        <a href="{$appointmentsBlock.url}">{$appointmentsBlock.text}</a>
        <div>{$appointmentsBlock.customMessage}</div>
      </div>
    </div>

  {* INTEGRATION 9 ================================================================================================== *}
  {elseif $appointmentsBlock.type == 'integration_9'}
    {if $appointmentsBlock.widgetSlug == ''}
    <div class="action {$appointmentsBlock.type}">
      <div class="message">
        {$this->_("Bitte wählen Sie Ihren Wunschzeitraum")}
      </div>
    </div>
    {/if}
  
  {/if}

</div>
