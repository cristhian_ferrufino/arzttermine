<section class="cinema picture-{$cinema_index} hidden-sm-down">
  <div class="headline">
    <h1>{'content.homepage.header'|trans}</h1>
    <ul class="inlined hidden-sm-down">
      <li>{'content.homepage.byline.near'|trans}</li>
      <li>{'content.homepage.byline.appointment'|trans}</li>
      <li>{'content.homepage.byline.free_and_easy'|trans}</li>
    </ul>
  </div>
  <div>
    {include file="searchbox.tpl"}
  </div>
</section>
<div class="hidden-md-up">
  <div class="headline">
    <h1 class="mobile hidden-md-up">{'content.homepage.header'|trans}</h1>
  </div>
  <div>
      {include file="searchbox.tpl"}
  </div>
</div>