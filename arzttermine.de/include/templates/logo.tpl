<div class="logo hidden-sm-down">
  <a href="/">
    <span class="logo-part-grey">&#xe603;</span>
    <span class="logo-part-white">&#xe600;</span>
    <span class="logo-part-blue">&#xe601;</span>
    <span class="logo-part-black">&#xe602;</span>
  </a>
</div>
<div id="logo" class="logo hidden-md-up" style="left: 20%">
  <a href="/">
    <span class="logo-part-grey">&#xe603;</span>
    <span class="logo-part-white">&#xe600;</span>
    <span class="logo-part-blue">&#xe601;</span>
    <span class="logo-part-black">&#xe602;</span>
  </a>
</div>