<div id="contact">

	<div class="form_wrapper">
		<div class="form_frame">
			<div class="form_container">
				<div class="field"><label for="form_name">Name</label>{$form.name}</div>
				<div class="field"><label for="form_email">EMail</label>{$form.email}</div>
				<div class="field"><label for="form_subject">Betreff</label>{$form.subject}</div>
				<div class="field"><label for="form_subject">Ihre Nachricht</label><br /><br />
					<div class="message">{$form.message}</div>
				</div>

				<p style="margin:1em 0px 1em 220px;">
					<a class="button medium orange" href="/">Zur Startseite</a>
				</p>
				<div class="clear"></div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>
