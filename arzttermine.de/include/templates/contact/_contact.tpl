<div id="contact">
    <div class="form_wrapper">
        <div class="form_frame">
            <div class="form_container">
                <form action="{$url_form_action}" name="edit" method="post" enctype="application/x-www-form-urlencoded">
                    <div id="minor-publishing">
                        <div class="inside">
                            <div class="field">
                                <label for="form_name">{"contact_name"|trans}</label>
                                <input type="text" id="form_name" name="form[name]" class="input_text {if array_key_exists('name', $errors)}input_error{/if}" value="{$form.name}" />
                                {if array_key_exists('name', $errors)}
                                    <span>{'<br/>'|implode:$errors.name}</span>
                                {/if}
                            </div>
                            <div class="field">
                                <label for="form_email">{"contact_email"|trans}</label>
                                <input type="text" id="form_email" name="form[email]" class="input_text {if array_key_exists('email', $errors)}input_error{/if}" value="{$form.email}" />
                                {if array_key_exists('email', $errors)}
                                    <span>{'<br/>'|implode:$errors.email}</span>
                                {/if}
                            </div>
                            <div class="field">
                                <label for="form_subject">{"contact_subject"|trans}</label>
                                <input type="text" id="form_email" name="form[subject]" class="input_text {if array_key_exists('subject', $errors)}input_error{/if}" value="{$form.subject}" />
                                {if array_key_exists('subject', $errors)}
                                    <span>{'<br/>'|implode:$errors.subject}</span>
                                {/if}
                            </div>
                            <div class="field">
                                <label for="form_message">{"contact_message"|trans}</label>
                                <textarea id="form_message" name="form[message]" class="input_textarea resizable {if array_key_exists('message', $errors)}input_error{/if}">{$form.message}</textarea>
                                {if array_key_exists('message', $errors)}
                                    <span>{'<br/>'|implode:$errors.message}</span>
                                {/if}
                            </div>
                        </div>
                    </div>

                    <div id="major-publishing-actions">
                        <input name="ok" type="submit" class="button large btn_green input_submit" id="ok" tabindex="4" accesskey="o" value="Abschicken" />
                        <div class="clear"></div>
                    </div>
                </form>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="clear"></div>
</div>
