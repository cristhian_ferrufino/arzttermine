{strip}
    {assign var="template_body_class" value="static"}
    {include file="header.tpl"}
{/strip}

<div class="container">
    <main>
        <aside>
            <nav>
                {include 'navi.tpl'}
            </nav>

            {render name='content2' method='render_sidebar'}

            {$this->getLatestBlogPosts()}
        </aside>

        <section id="content">

            <h1>Nehmen Sie Kontakt zu uns auf!</h1>
            <p>Haben Sie Fragen zu unserem Buchungsablauf oder ein anderes Anliegen? <br>Dann schreiben Sie uns eine <a title="Eine Nachricht schreiben" href="mailto:info@arzttermine.de">Nachricht</a> oder rufen Sie unsere kostenlose Hotline<br>an unter <a title="Direkt anrufen" href="tel:08002222133">0800 / 2222133</a>.</p>
            <p>Schauen Sie auch in unsere <a href="/faq">FAQs</a>, dort finden Sie Antworten auf häufig gestellte Fragen zu unseren Produkten, sowie dem Patientenservice.</p>
            <p>Wir freuen uns über Ihre Nachricht oder Ihren Anruf.<br>Ihr Arzttermine Team</p>
            
            {if $mail_sent}
                {include file="contact/_contact_summary.tpl"}
            {else}
                {include file="contact/_contact.tpl"}    
            {/if}
        </section>
    </main>
</div>

{include file="footer.tpl"}
