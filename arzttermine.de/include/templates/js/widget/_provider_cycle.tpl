{literal}<script type='text/javascript'><!--
/*<![CDATA[*/

var calendarIndex = 0;
$(document).ready(function() {
    updatePrevNextButtons(0, 1);
});
var cycleDirection;
addClickHandlerOnNaaLinks();

var doctorsCalendarsHTMLs = null;

$("#prev-loading").hide();
$("#next-loading").hide();

$("#prev-link").show();
$("#next-link").show();

$("#prev-link").click(function(event) {
    event.preventDefault();
    hideNextPrevButtons();
    showCalendar(calendarIndex - 1);
});

$("#next-link").click(function(event) {
    event.preventDefault();
    hideNextPrevButtons();
    showCalendar(calendarIndex + 1);
});


function updatePrevNextButtons(prevButtonEnable, nextButtonEnable)
{
    if (prevButtonEnable == 1) {
        $("#prev-loading").hide();
        $("#prev-link").show();
    } else {
        $("#prev-loading").show();
        $("#prev-link").hide();
    }

    if (nextButtonEnable == 1) {
        $("#next-loading").hide();
        $("#next-link").show();
    } else {
        $("#next-loading").show();
        $("#next-link").hide();
    }

}

function hideNextPrevButtons()
{
    $("#prev-loading").show();
    $("#next-loading").show();
    $("#prev-link").hide();
    $("#next-link").hide();
}

function showCalendar(calendarIndex) {
        var $calendarContainer = $('.calendar-container');
        $calendarContainer.fadeOut();
        $(".provider-list .provider-row .times-container").each(function() {
            $(this).fadeOut();
        });
        var dateStart = $calendarContainer.attr('date_start');
        var dateDays = $calendarContainer.attr('date_days');
        // providers
        var user_ids = [];
        var location_ids = [];
        var msId;
        var insId;
        var ttId;
    
        $(".provider-list .provider-row").each(function() {
            var $this = $(this);
            var user_id = $this.attr('user_id');
            var location_id = $this.attr('location_id');
            var medical_specialty_id = $this.attr('medical_specialty_id');
            var treatmenttype_id = $this.attr('treatmenttype_id');
            var insurance_id = $this.attr('insurance_id');
            
            user_ids.push(user_id);
            location_ids.push(location_id);
            msId = medical_specialty_id;
            ttId = treatmenttype_id;
            insId = insurance_id;
        });
        var data = {
            widget: "{/literal}{$widget_slug}{literal}",
            index: calendarIndex,
            date_start: dateStart,
            date_days: dateDays,
            location_ids: location_ids,
            user_ids: user_ids,
            medical_specialty_id: msId,
            treatmenttype_id: ttId,
            insurance_id: insId
        };
        $.ajax({
            type: "GET",
            async: true,
            url: "{/literal}{$CONFIG.URL_API}/get-available-appointments-widgets{literal}",
            data: data,
            success: function(calendars) {
                $(".provider-list .provider-row").each(function() {
                    doctorsCalendarsHTMLs = calendars;
                });
                showSlide();
            }
        });
    }


function showSlide() {
    var l = doctorsCalendarsHTMLs.length;
    var prevButtonEnable = doctorsCalendarsHTMLs[l - 3];
    var nextButtonEnable = doctorsCalendarsHTMLs[l - 2];
    calendarIndex = parseInt(doctorsCalendarsHTMLs[l - 1]);

    updatePrevNextButtons(prevButtonEnable, nextButtonEnable);
    $calendarContainer = $('.calendar-container');
    $calendarContainer.hide();
    $calendarContainer.html('<div class="calendar-slide odd">' + doctorsCalendarsHTMLs[0] + '</div>');
    $calendarContainer.fadeIn();

    $(".provider-list .provider-row .times-container").each(function(index) {
        $(this).hide();
        $(this).html('<div class="times-slide odd">' + doctorsCalendarsHTMLs[index + 1] + '</div>');
        $(this).fadeIn();
    });

    addClickHandlerOnNaaLinks();
    //addMoreItemsLinkClickHandlers();

}

function addClickHandlerOnNaaLinks()
{
    $(".f_go_to_app_week").click(function() {
        var id = $(this).attr("id");
        var date = id.substr(id.indexOf("^") + 1);
        showCalendar(date);
    });
}

/*]]>*/
//--></script>
{/literal}
