{if isset($CONFIG) && $CONFIG.GOOGLE_ANALYTICS_ACTIVATE}
    {if !empty($googleTagManagerDataLayerJs)}{$googleTagManagerDataLayerJs}{/if}
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id={$CONFIG.GOOGLE_TAGMANAGER_ID}" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>
        (function(w,d,s,l,i){
            w[l]=w[l]||[];
            w[l].push({
                'gtm.start':new Date().getTime(),
                event:'gtm.js'
            });
            var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),
                dl=l!='dataLayer'?'&l='+l:'';
            j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;
            f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','{$CONFIG.GOOGLE_TAGMANAGER_ID}');
    </script>
{/if}
