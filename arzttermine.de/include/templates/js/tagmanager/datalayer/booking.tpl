{if isset($CONFIG) && $CONFIG.GOOGLE_ANALYTICS_ACTIVATE}
<script>
    window.dataLayer = window.dataLayer || [];
    window.dataLayer.push({
        "page": "/termin",
        "source_platform": "{$app->getSourcePlatform()}",
        "medical_specialty_id": "{$medical_specialty_id}",
        "medical_specialty_name": "{$medical_specialty_name}",
        "treatmentTypeId": "{$treatment_type_id}",
        "treatmentTypeName": "{$treatment_type_name}",
        "appointmentDate": "{$form.appointment_start_at}",
        "city": "{$location->getCity()}",
        "docName": "{$user->getFullName()}",
        "uhc": "{if $user->hasContract()}1{else}0{/if}"
    });
</script>
{/if}