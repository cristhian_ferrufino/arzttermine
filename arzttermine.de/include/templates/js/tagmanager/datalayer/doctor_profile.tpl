{if isset($CONFIG) && $CONFIG.GOOGLE_ANALYTICS_ACTIVATE}
<script>
    window.dataLayer = window.dataLayer || [];
    window.dataLayer.push({
        "page": "/doctor-profile",
        "source_platform": "{$app->getSourcePlatform()}",
        "docName": "{$user->getFullName()}",
        "medSpecialtyId": "{$user->getDefaultMedicalSpecialty()->getId()}",
        "medSpecialtyName": "{$user->getDefaultMedicalSpecialty()->getName()}",
        "location": "{$user->getDefaultLocation()->getCity()}",
    });
</script>
{/if}