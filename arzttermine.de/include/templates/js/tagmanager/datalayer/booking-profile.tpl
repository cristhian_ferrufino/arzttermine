{if isset($CONFIG) && $CONFIG.GOOGLE_ANALYTICS_ACTIVATE}
<script>
    window.dataLayer = window.dataLayer || [];
    window.dataLayer.push({
        "page": "{$dataLayerPage}",
        "source_platform": "{$app->getSourcePlatform()}",
        "medical_specialty_id": "{$booking->getMedicalSpecialtyId()}",
        "medical_specialty_name": "{$booking->getMedicalSpecialty()->getName()}",
        "treatmentTypeId": "{$booking->getTreatmenttypeId()}",
        "treatmentTypeName": "{$booking->getTreatmenttypeText()}",
        "appointmentDate": "{$booking->getAppointmentStartAt()}",
        "city": "{$booking->getLocation()->getCity()}",
        "insurance_id": "{$booking->getInsuranceId()}",
        "hours_to_appointment": "{$booking->getHoursCreatedAtToAppointment()}",
        "uhc": "{if $booking->getUser()->hasContract()}1{else}0{/if}",
        "docName": "{$booking->getUser()->getFullName()}",
        "returning_visitor": "{if $booking->getReturningVisitor()}1{else}0{/if}",
        "gender": "{$booking->getGenderText()}"
    });
</script>
{/if}