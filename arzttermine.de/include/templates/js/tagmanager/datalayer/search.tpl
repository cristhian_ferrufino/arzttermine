{if isset($CONFIG) && $CONFIG.GOOGLE_ANALYTICS_ACTIVATE}
<script>
    window.dataLayer = window.dataLayer || [];
    window.dataLayer.push({
        "page": "/suche",
        "source_platform": "{$app->getSourcePlatform()}",
        "medical_specialty_id": "{$form.medical_specialty_id}",
        "medical_specialty_name": "{$search->getMedicalSpecialty()->getName()}",
        "location": "{$form.location}",
        "search_results": "{$search->getResultCount()}",
        "insuranceType": "{$search->getInsurance()->getName()}",
        "treatmentType": "{$search->getTreatmentType()->getName()}"
    });
</script>
{/if}