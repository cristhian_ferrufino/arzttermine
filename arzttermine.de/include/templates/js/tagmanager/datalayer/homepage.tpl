{if isset($CONFIG) && $CONFIG.GOOGLE_ANALYTICS_ACTIVATE}
<script>
    window.dataLayer = window.dataLayer || [];
    window.dataLayer.push({
        "page": "/",
        "source_platform": "{$app->getSourcePlatform()}"
    });
</script>
{/if}