<!-- Facebook Pixel Code -->
<script>
{literal}!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');{/literal}

fbq('init', '1108445052517112');
fbq('init', '6028158230006');
fbq('track', "PageView");
{if isset($facebook_tracking_js)}{$facebook_tracking_js}{/if}
</script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1108445052517112&ev=PageView&noscript=1"/></noscript>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=6028158230006&ev=PageView&noscript=1" /></noscript>
<!-- End Facebook Pixel Code -->
