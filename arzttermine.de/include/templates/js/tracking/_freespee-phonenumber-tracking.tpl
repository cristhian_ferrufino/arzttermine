{* Just recycle Google config, as they're both trackers anyway *}
{if isset($CONFIG) && $CONFIG.GOOGLE_ANALYTICS_ACTIVATE}
    <script type="text/javascript">
        var __fs_conf = __fs_conf || [];
        __fs_conf.push(['setAdv', { 'id': '8188b090-946d-4037-9815-76e283b99e58' }]);
    </script>
    <script type="text/javascript" src="//analytics.freespee.com/js/external/fs.js"></script>
{/if}