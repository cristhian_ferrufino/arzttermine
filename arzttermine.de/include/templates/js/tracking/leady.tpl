{* Just recycle Google config, as they're both trackers anyway *}
{if isset($CONFIG) && $CONFIG.GOOGLE_ANALYTICS_ACTIVATE}
<script type="text/javascript">
    var leady_track_key="y39FXY28qIbZZ8Ib";
    var leady_track_server=document.location.protocol+"//t.leady.com/";
    (function(){
        var l=document.createElement("script");l.type="text/javascript";l.async=true;
        l.src=leady_track_server+leady_track_key+"/L.js";
        var s=document.getElementsByTagName("script")[0];s.parentNode.insertBefore(l,s);
    })();
</script>
{/if}