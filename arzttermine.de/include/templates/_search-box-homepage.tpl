<div id="search-box-homepage">
    <div class="form_container">
        <form action="{$app->i18nTranslateUrl('/suche')}" id="form-search-box-homepage" name="edit" method="get" enctype="application/x-www-form-urlencoded" style="display:inline;">
            <label for="form_medical_specialty_id" id="form_label_medical_specialty_id">{$this->_('Ich suche einen Arzt in')}</label>
            <select id="form_location" name="form[location]" class="validate[required] input_select{$form.location__form_input_class}"><option label="{$this->_('...bitte Ort wählen...')}" value="">{$this->_('...bitte Ort wählen...')}</option>{html_options options=$CONFIG.LOCATIONS_SELECT selected=$form.location}</select>
            <select id="form_medical_specialty_id" name="form[medical_specialty_id]" class="validate[required] input_text{$form.medical_specialty_id__form_input_class}"><option label="{$this->_('...bitte Fachrichtung wählen...')}" value="">{$this->_('...bitte Fachrichtung wählen...')}</option>{html_options options=MedicalSpecialty::getHtmlOptions(false) selected=$form.medical_specialty_id}</select>
        {*}			<input id="form_location" name="form[location]" class="validate[required] text-input input_text{$form.location__form_input_class}" value="{$form.location}" />{/*}
            <label id="form_label_insurance">{$this->_('Ich bin')}</label>
            <select id="input_insurance_select" name="form[insurance_id]">
                <option value="2" {if $form.insurance_id=='2'} selected="selected" {/if}>{$this->_('gesetzlich versichert')}</option>
                <option value="1" {if $form.insurance_id=='1'} selected="selected" {/if}>{$this->_('privat versichert')}</option>
            </select>
            <button id="form_submit" name="ok" type="submit" class="button large btn_green icon-search" tabindex="4" accesskey="o" value="{$this->_('Termin finden')}" />{$this->_('Termin finden')}</button>
		</form>
	</div>
	{*}{$form.location__message}{/*}
</div>

{literal}
<script type="text/javascript">
function update_form() {
    var select_form_location = $('#form_location').val(),
		select_options = {/literal}{$select_location_medical_specialty_id_options}{literal} || {},
	    $medical_specialty_id = $('#form_medical_specialty_id');
	
	$medical_specialty_id.empty();
	
	for (var options in select_options[select_form_location]) {
		if (select_options[select_form_location].hasOwnProperty(options)) {
			$medical_specialty_id.append($('<option value="' + select_options[select_form_location][options] + '">' + options + '</option>'));
		}
	}
}

$(document).ready(function() {
    $("#form-search-box-homepage").validationEngine('attach', {promptPosition : "centerRight", scroll: false});

    $('#form_location').change(function() {
        update_form();
        return false;
    });

});
</script>
{/literal}
