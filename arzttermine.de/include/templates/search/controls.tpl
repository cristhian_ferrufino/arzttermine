<div class="control-menu hidden-lg-up">
    <div class="row">
        <div class="col">

            <a id="filter_toggle" class="control-menu" style=" margin-right: 0rem;" ><i class="fa fa-sliders fa-2x" aria-hidden="true"></i></a>
            <a id="map_toggle" class="control-menu"><i class="fa fa-map-marker fa-2x" aria-hidden="true"></i></a>

        </div>
        <div class="col">
            {include file="search/collapse_menu.tpl"}
        </div>
    </div>
</div>
