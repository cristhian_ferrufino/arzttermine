<link rel="stylesheet" type="text/css" href="/static/css/lasik.css?cv={$cacheVersion}">
<link rel="stylesheet" type="text/css" href="/static/css/two-cols-with-form.css?cv={$cacheVersion}">
<div class="overlay-blocker lasik">
  <section class="popup-message two-cols-with-form">
    <section id="info-block">
      <h3>Ihre Wunscharzt</h3>
      <div class="orange-bar">Termin anfragen</div>
      <div class="doctor">
        <div class="photo">
          <img src="" alt="">
        </div>
        <div class="info">
          <div class="conjunction">bei</div>
          <div class="name"></div>
          <div class="phone"></div>
          <div class="address">
            <h4>Adresse</h4>
            <div class="practice-name"></div>
            <div class="street"></div>
            <div class="city"></div>
          </div>
        </div>
      </div>
      <ul class="comfort-elements">
        <li class="cancelation">
          Sollten sich Ihre Pläne &auml;ndern, k&ouml;nnen Sie den Termin jederzeit stornieren.
        </li>
        <li class="contact">
          Unser Supportteam erreichen Sie kostenfrei unter <a href="tel:08002222133">0800/2222133</a>
        </li>
        <li class="data-protection">
          Wir nutzen Ihre Daten ausschließlich wie in der <a href="/datenschutz">Datenschutzerklärung</a> festgelegt.
        </li>
      </ul>
    </section>

    <section id="form">
      <h3>Ihre Angaben</h3>
      <form id="lasik-form-1" action="" method="post" enctype="application/x-www-form-urlencoded" >
          <div class="form-element {$error_class}" data-error-message="Bitte geben Sie Ihren Vornamen ein.">
            <label>{$app->_('Vorname')}</label>
            <input type="text" name="form[first_name]" value="" required>
          </div>
          <div class="form-element {$error_class}" data-error-message="Bitte geben Sie Ihren Nachnamen ein.">
            <label>{$app->_('Nachname')}</label>
            <input type="text" name="form[last_name]" value="" required>
          </div>
          <div class="form-element {$error_class}" data-error-message="Bitte geben Sie Ihre E-Mail Adresse ein.">
            <label>{$app->_('E-Mail')}</label>
            <input type="email" name="form[email]" value="" required>
          </div>
          <div class="form-element {$error_class}" data-error-message="Bitte geben Sie eine gültige Telefonnummer ein.">
            <label>{$app->_('Telefonnummer')}</label>
            <input type="text" name="form[phone]" value="" required>
          </div>
          <div class="form-element {$error_class}">
            <label>{$app->_('Kommentar')}</label>
            <textarea type="text" name="form[comment]"></textarea>
          </div>
          <div class="button-wrapper"><button type="button" class="step-1">Weiter</button></div>
      </form>
      <form id="lasik-form-2" action="" method="post" enctype="application/x-www-form-urlencoded" >
          <div class="form-element {$error_class}">
            <label>{$app->_('Dioptrin')}</label>
            <input type="text" name="form[dioptrin]" value="">
          </div>
          <div class="form-element {$error_class}">
            <label>{$app->_('Geburtsdatum')}</label>
            <input type="text" name="form[date_of_birth]" value="">
          </div>
          <div class="button-wrapper"><button type="button" class="step-2">Bestätigen</button></div>
      </form>
      <section id="lasik-confirmation">
        <p>Ihre Anfrage wird gespeichert. Unsere Mitarbeiter werden Sie bald kontaktieren.</p>
      </section>
    </section>
    <button class="close"></button>
  </section>
</div>

<script type="text/javascript" src="/static/js/validation.min.js" charset="utf-8"></script>
<script type="text/javascript">
  {literal}
  var AT = AT || {};

  AT._defer(function(){

    var $forms = $('.lasik.overlay-blocker form');
    AT.validation.monitor($forms);
    
    $forms.on('AT:form-validation:errors-found', function() {
      // Add 1rem/16px to the default height for each erroneous field.
      var errorCount = $(this).find('.form-element.error').length;
      var $popup = $('.lasik.overlay-blocker .popup-message');
      $popup.css('height', '');
      $popup.css('height', $popup.outerHeight() + errorCount*16);
      $popup.find('#form').css('height', '');
      $popup.find('#form').css('height', $popup.find('#form').outerHeight() + errorCount*16);
    });


    $forms.on('AT:form-validation:no-errors-found', function() {
      var $popup = $('.lasik.overlay-blocker .popup-message');
      $popup.css('height', '');
      $popup.find('#form').css('height', '');
    });


    $('.book-appointment').on('click', function(){
      var $provider = $(this).parents('.result');
      var $popup = $('.lasik.overlay-blocker');
      $popup.show();

      $popup.find('.photo img').attr('src', $provider.find('.photo img').attr('src'));
      $popup.find('.name').html( $provider.find('h2 span a').html() );
      $popup.find('.phone').html( $provider.find('span[itemprop=telephone] a').html() );
      $popup.find('.address .street').html( $provider.find('span[itemprop=streetAddress]').html() );
      $popup.find('.address .city').html( 
        $provider.find('span[itemprop=postalCode]').html() +' '+ $provider.find('span[itemprop=addressLocality]').html() 
      );

    });
    
    $('.lasik.overlay-blocker button.step-1').on('click', function(){
      var $popup = $('.lasik.overlay-blocker');

      $.ajax({
        url: '/api/v1/lasik-book-step.json',
        data: $popup.find('form').serialize(),
        success: function(response){
          if (response.success === true) {
            $('#lasik-form-1').addClass('inactive');
            $('#lasik-form-2').addClass('active');
          }
        }
      });
    });
    
    $('.lasik.overlay-blocker button.step-2').on('click', function(){
      var $popup = $('.lasik.overlay-blocker');

      $.ajax({
        url: '/api/v1/lasik-book-step.json',
        data: $popup.find('form').serialize(),
        success: function(response){
          if (response.success === true) {
            $('.lasik #lasik-confirmation').fadeIn();
            $('#lasik-form-1').fadeOut();
            $('#lasik-form-2').fadeOut();
            setTimeout(function(){ 
              $('.overlay-blocker.lasik').fadeOut();
              $('.lasik #lasik-confirmation').fadeOut();
              $('#lasik-form-1').addClass('active').removeClass('inactive').show();
              $('#lasik-form-2').addClass('inactive').removeClass('active').show();
            }, 2000);
          }
        }
      });
    });

    $('.lasik.overlay-blocker .close').on('click', function(){
      $('.lasik.overlay-blocker').hide();
      $('#lasik-form-1').addClass('active').removeClass("inactive");
      $('#lasik-form-2').addClass('inactive').removeClass("active");
    });


  });
  {/literal}
</script>
