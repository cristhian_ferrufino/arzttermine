<div id="search-box" class="cf">
    <div class="form_container">
        <form action="{$app->i18nTranslateUrl('/suche')}" id="form-search-box" name="edit" method="get" enctype="application/x-www-form-urlencoded" style="display:inline;">
            <select id="form_medical_specialty_id" name="form[medical_specialty_id]" class="validate[required] input_text{$this->form.medical_specialty_id__form_input_class}">
                <option label="{$this->_('...bitte wählen...')}" value="">{$this->_('...bitte wählen...')}</option>
            {html_options options=MedicalSpecialty::getHtmlOptions() selected=$this->form.medical_specialty_id}
            </select>
            <select id="form_treatmenttype_id" name="form[treatmenttype_id]" class="input_text{$this->form.medical_specialty_id__form_input_class}">
                <option label="{$this->_('...Behandlungsgrund...')}" value="">{$this->_('...Behandlungsgrund...')}</option>
            {html_options options=$MedicalSpecialityAndTreatmentTypesMapArray.{$this->form.medical_specialty_id} selected=$this->form.treatmenttype_id}
            </select>
            <input id="form_location" name="form[location]" class="validate[required] text-input input_text{$this->form.location__form_input_class}" value="{$this->form.location}" />
            <label id="form_label_insurance">{$this->_('Ich bin')}</label>
            <select id="input_insurance_select" name="form[insurance_id]" class="input_text">
                <option value="1" {if $this->form.insurance_id=='1'} selected="selected" {/if}>{$this->_('privat versichert')}</option>
                <option value="2" {if $this->form.insurance_id=='2'} selected="selected" {/if}>{$this->_('gesetzlich versichert')}</option>
            </select>
            <button name="search" type="submit" class="alternate btn_green large button icon-search" tabindex="4" accesskey="o" value="Erneut suchen" />{$this->_('Erneut suchen')}</button>
            <div class="clear"></div>
			<input type = "hidden" id="form_selected_neighborhood" name="form[neighborhood_id]" class="input_text" value="{$selectedNeighborhoodId}"/>		
        </form>
    </div>
</div>
<div id="referral_tooltip-loadtime" style="display: none; right: 0px;">
    <div class="referral_howto">
        <img src="/static/img/loader-horizontal.gif" alt="loading" />
        <p>Wir fragen für Sie die Kalenderdaten unserer Ärzte an. Das kann bis zu 5 Sekunden dauern -<br/>bitte gedulden Sie sich.</p>
    </div>
</div>
{literal}
<script type="text/javascript">

    function load_resultPage() {
        $(".btn_green[name='search']").click();
    }

		var update_treatmenttypes = function() {
			var select_form_medical_specialty_treatmenttype = $('#form_treatmenttype_id');
			var search_scroll_header_tt = $('#search_scroll_header_tt');
			
			
			var ms_id = $('#form_medical_specialty_id').val();
			select_form_medical_specialty_treatmenttype.empty();
			select_form_medical_specialty_treatmenttype.append($('<option></option>').val("").html({/literal}'{$this->_('...bitte wählen...')}'{literal}));
			search_scroll_header_tt.empty();
			search_scroll_header_tt.append($('<option></option>').val("").html({/literal}'{$this->_('...bitte wählen...')}'{literal}));
			if (parseInt(ms_id) > 0) {
				var all_medical_specialities_treatments = eval({/literal}{$MedicalSpecialityAndTreatmentTypesMap}{literal});
				if (all_medical_specialities_treatments[ms_id]) {
					var selected_medical_speciality_treatments = all_medical_specialities_treatments[ms_id];
					$.each(selected_medical_speciality_treatments, function(val, text) {
						select_form_medical_specialty_treatmenttype.append($('<option></option>').val(val).html(text));
						search_scroll_header_tt.append($('<option></option>').val(val).html(text));
					});
				}
			}
		};
		$(document).ready(function() {

			$('#form_location').change(function() {				
				$('#form_selected_neighborhood').val('');
			});
			$('#search_scroll_header_neighborhood').change(function() {
				var selectedNeighborhood = $(this).val();
				$('#form_selected_neighborhood').val(selectedNeighborhood);
				$('#form-search-box').trigger('submit');

			});
			$('#search_scroll_header_tt').change(function() {
				var selectedTT = $(this).val();
				$('#form_treatmenttype_id').val(selectedTT);
				$('#form-search-box').trigger('submit');

			});

			$("#form_location").watermark("{/literal}{$this->_('Ort und/oder Postleitzahl')}{literal}");
			$("#form-search-box").validationEngine('attach', {promptPosition: "centerRight", scroll: false});
			$('#form_medical_specialty_id').change(function() {
				update_treatmenttypes();
				load_resultPage();
				return false;
			});
			$('#form_treatmenttype_id, #input_insurance_select').change(function() {
				load_resultPage();
				return false;
			});
			$(".btn_green[name='search']").click(function() {
				$('#referral_tooltip-loadtime').fadeIn();
			});
		});
	</script>
{/literal}
