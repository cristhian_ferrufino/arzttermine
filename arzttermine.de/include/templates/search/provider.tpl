{assign var="user" value=$providerRow->getUser()}
{assign var="location" value=$providerRow->getLocation()}
{if isset($multiple_locations)}
    {$location->getName()}
{/if}

<div class="hidden-md-down">
    <section itemscope itemtype="http://schema.org/Physician" class="result large s100 int{$location->getIntegrationId()}" data-slug="{$user->getSlug()}" data-index="{$index + $search->getStartIndex() + 1}">
        <div class="wrapper">
            <header>
                <div class="map-pin">{$index + $search->getStartIndex() + 1}</div>
                <div class="title">
                    <h2 class="name">
                    <span itemprop="name">
                        <a itemprop="url" href="{$user->getUrlWithFilter($filter)}">{$user->getName()}</a>
                    </span>
                        {* In the location profile, the doc tiles should contain the specialty of the doc *}
                        {* The search page should not display the specialty *}
                        {if $additional_body_classes == "location"}
                            <a href="{$user->getUrlWithFilter($filter)}">
                        <span class="title-specialty" itemprop="medicalSpecialty">
                        ({if $user->hasPreferredMedicalSpecialtyText()}{$user->getPreferredMedicalSpecialtyText()}{else}{$user->getMedicalSpecialityIdsText(', ')}{/if})
                        </span>
                            </a>
                        {/if}
                    </h2>

                    {if $user->showReviews()}
                        {include file="_star-rating.tpl" rating=$user->getRating() microdata="itemprop='aggregateRating' itemscope itemtype='http://schema.org/AggregateRating'"}
                    {/if}

                    {if $location->hasPhoneVisible()}
                        <span itemprop="telephone"><a href="tel:{$location->getPhoneVisible()}">{$location->getPhoneVisible()}</a>
                            {*}DIRTY HACK for a Test!{*}
                            {if ($location->getId() == 39505)}
                                <br />
                                <span style="font-size:12px;">Für unsere Schweizer Patienten:</span><br />
                                <a href="tel:0041325109419">+41 (32) 5109419</a>
                            {/if}</span>
                    {/if}
                </div>
                <p itemprop="address" itemscope itemtype="http://data-vocabulary.org/Address" class="address">
                    {* Geo info - SEO readable *}
                    <span class="microdata-hidden" itemprop="geo" itemscope itemtype="http://schema.org/Place">
          <span itemscope itemtype="http://schema.org/GeoCoordinates">
            <span property="latitude" content="{$location->getLat()}"></span>
            <span property="longitude" content="{$location->getLng()}"></span>
          </span>
        </span>
                    {* Hide the distance on location profile (it's always 0) *}
                    {if !isset($hideDistance) || $hideDistance !== true}
                        {floatval($providerRow->getDistance())|number_format:1:",":"."}km |
                    {/if}
                    <span itemprop="address" itemscope itemtype="http://data-vocabulary.org/Address">
          <span class="street-address" itemprop="streetAddress">{$location->getStreet()}</span>
          <span class="zip-city">
            <span class="zip" itemprop="postalCode">{$location->getZip()}</span>
            <span class="city" itemprop="addressLocality">{$location->getCity()}</span>
          </span>
        </span>
                </p>
            </header>

            <div class="photo">
                <img itemprop="photo" src="{$user->getProfileAssetUrl($smarty.const.ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED)}" alt="{$user->getName()}">
            </div>

            {* We don't use the calendar for the Lasik landing page *}
            {if $pageType == "custom-search"}
                {include file="search/_provider-lasik-info-block.tpl"}
            {else}
                {assign var="appointmentsBlock" value=$providerRow->getAppointmentsBlock($date_start, $date_end, $filter.insurance_id, $filter.medical_specialty_id, $filter.treatmenttype_id)}
                {include file="appointment/_appointments-block-calendar.tpl"}
            {/if}

            {if $location->hasAppointmentEnquiryIntegration() && $user->hasContract()==false}
                <div class="non-pay">{$nonpay_message}</div>
            {/if}

        </div>{* wrapper *}
    </section>
</div>

<div class="hidden-lg-up">
    <section itemscope itemtype="http://schema.org/Physician" class="result large s100 int{$location->getIntegrationId()}" data-slug="{$user->getSlug()}" data-index="{$index + $search->getStartIndex() + 1}">
        <div class="wrapper">
            <div class="row mobile_row_height">
                <div class="col photo">
                    <img itemprop="photo" src="{$user->getProfileAssetUrl($smarty.const.ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED)}" alt="{$user->getName()}">
                </div>
                <div class="col">
                    <header>
                    <div class="map-pin">{$index + $search->getStartIndex() + 1}</div>
                    <div class="title">
                        <h2 class="name">
                        <span itemprop="name">
                            <a itemprop="url" href="{$user->getUrlWithFilter($filter)}">{$user->getName()}</a>
                        </span>
                            {* In the location profile, the doc tiles should contain the specialty of the doc *}
                            {* The search page should not display the specialty *}
                            {if $additional_body_classes == "location"}
                                <a href="{$user->getUrlWithFilter($filter)}">
                            <span class="title-specialty" itemprop="medicalSpecialty">
                            ({if $user->hasPreferredMedicalSpecialtyText()}{$user->getPreferredMedicalSpecialtyText()}{else}{$user->getMedicalSpecialityIdsText(', ')}{/if})
                            </span>
                                </a>
                            {/if}
                        </h2>

                        {if $user->showReviews()}
                         <br />
                            <div style="margin-top: .2rem">{include file="_star-rating.tpl" rating=$user->getRating() microdata="itemprop='aggregateRating' itemscope itemtype='http://schema.org/AggregateRating'"}</div>
                        {/if}

                        {if $location->hasPhoneVisible()}
                            <span itemprop="telephone" class="address">
                                <a href="tel:{$location->getPhoneVisible()}">{$location->getPhoneVisible()}</a>
                                {*}DIRTY HACK for a Test!{*}
                                {if ($location->getId() == 39505)}
                                    <br />
                                    <span style="font-size:12px;">Für unsere Schweizer Patienten:</span><br />
                                    <a href="tel:0041325109419">+41 (32) 5109419</a>
                                {/if}
                            </span>
                        {/if}
                    </div>
                    <p itemprop="address" itemscope itemtype="http://data-vocabulary.org/Address" class="address">
                        {* Geo info - SEO readable *}
                        <span class="microdata-hidden" itemprop="geo" itemscope itemtype="http://schema.org/Place">
                          <span itemscope itemtype="http://schema.org/GeoCoordinates">
                            <span property="latitude" content="{$location->getLat()}"></span>
                            <span property="longitude" content="{$location->getLng()}"></span>
                          </span>
                        </span>
                                {* Hide the distance on location profile (it's always 0) *}
                                {if !isset($hideDistance) || $hideDistance !== true}
                                    {floatval($providerRow->getDistance())|number_format:1:",":"."}km |
                                {/if}
                    <span itemprop="address" itemscope itemtype="http://data-vocabulary.org/Address">
                      <span class="street-address" itemprop="streetAddress">{$location->getStreet()}, {$location->getZip()} {$location->getCity()}</span>
                      </span>
                    </span>
                    </p>
                </header>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    {* We don't use the calendar for the Lasik landing page *}
                    {if $pageType == "custom-search"}
                        {include file="search/_provider-lasik-info-block.tpl"}
                    {else}
                        {assign var="appointmentsBlock" value=$providerRow->getAppointmentsBlock($date_start, $date_end, $filter.insurance_id, $filter.medical_specialty_id, $filter.treatmenttype_id)}
                        {include file="appointment/_appointments-block-calendar.tpl"}
                    {/if}

                    {if $location->hasAppointmentEnquiryIntegration() && $user->hasContract()==false}
                        <div class="non-pay">{$nonpay_message}</div>
                    {/if}
                </div>
            </div>
        </div>{* wrapper *}
    </section>
</div>