<section id="results-list" itemscope itemtype="http://schema.org/SearchResultsPage">

    {* Search filters panel *}
    {if !isset($hideFilters)}
        {include file="search/filters.tpl"}
    {/if}
    {* END - Search filters panel *}

    {* HEADLINE *}
    {if !isset($hideFilters)}
        {if $search->getResultCount()|count}
            <div class="headline">
                <h1 itemprop="headline">
                    <span class="docs-count" itemprop="headline">{$search->getResultCount()}</span>
                    <span class="specialty" itemprop="keywords">{$search->getMedicalSpecialty()->getName(false)}</span>
                    {$this->_('in')}
                    <span class="location" itemprop="keywords">{$form.location}{if isset($district)} ({$district->getName()}){/if}</span>
                    {$this->_('haben Termine für Sie')}.
                </h1>
                {* Pagination *}
                <div class="pagination">
                    {paginate
                    count=$search->getResultCount()
                    ms=$search->getMedicalSpecialtyId()
                    tt=$search->getTreatmentTypeId()
                    insurance=$search->getInsuranceId()
                    page=$search->getPage()
                    per_page=$search->getPerPage()
                    lat=$search->getLat()
                    lng=$search->getLng()
                    location=$form.location
                    distance=$search->getRadius()
                    district=$form.district_id
                    }
                </div>
            </div>
        {else}
            <h1 itemprop="headline">
                {$this->_('Aktuell sind in dieser Region keine Ärzte bei uns angemeldet.')}
                {$this->_('Wir arbeiten daran unser Angebot in Kürze auch in Ihrer Stadt anzubieten.')}
            </h1>
        {/if}
    {/if}
    {* END - HEADLINE *}

    {* @todo: [NM] ask Axel why we need this here *}
    <div id="search-location" data-lat="{$search->getLat()}" data-lng="{$search->getLng()}" data-name="Suchstandort"></div>

    {* SEARCH RESULTS *}
    <div id="providers" data-date-start="{$date_start}" data-date-end="{$date_end}" data-date-days="{$date_days}">
        <div id="providers-wrapper">
            {foreach from=$results item=providerRow name=searchForeach}
                {assign var="index" value=$smarty.foreach.searchForeach.index}
                {include file="search/provider.tpl"}
            {/foreach}
        </div>
    </div>
    {* END - SEARCH RESULTS *}

    {* Pagination *}
    <div class="pagination justified-blocks">
        {paginate
        count=$search->getResultCount()
        ms=$search->getMedicalSpecialtyId()
        tt=$search->getTreatmentTypeId()
        insurance=$search->getInsuranceId()
        page=$search->getPage()
        per_page=$search->getPerPage()
        lat=$search->getLat()
        lng=$search->getLng()
        location=$form.location
        distance=$search->getRadius()
        district=$form.district_id
        }
    </div>

    {if isset($MedicalSpecialityAndTreatmentTypesMap)}
        <script>
            var mapSpecialtyTreatment = {$MedicalSpecialityAndTreatmentTypesMap};
        </script>
    {/if}

    {* The footer is here because of the different layout of the search page which doesn't allow it to be at the end of the document. *}
    {include "footer.tpl"}

</section>{* #results-list *}
