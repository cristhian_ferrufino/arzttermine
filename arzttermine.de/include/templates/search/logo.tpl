<div class="logo hidden-md-down" style="background: #FFFFFF; height: 3rem">
  <a href="/">
    <span class="logo-part-grey">&#xe603;</span>
    <span class="logo-part-white">&#xe600;</span>
    <span class="logo-part-blue">&#xe601;</span>
    <span class="logo-part-black">&#xe602;</span>
  </a>
</div>
