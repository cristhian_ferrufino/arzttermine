{strip}
    {assign var="template_body_class" value="search"}
    {include file="search/header.tpl"}
    {include file="search/controls.tpl"}
{/strip}
        <div id="map">
            {include file="search/search-map.tpl"}
        </div>
        <div id="list-content" >
            {include file="search/results-list.tpl"}
        </div>

{if isset($medicalSpecialtyText)}
    <article id="cms-specialty-text">
        {$medicalSpecialtyText}
    </article>
{/if}

{include file="_referral-code-support.tpl"}

{include "js/maps.js.tpl"}

{* Used to overlay popups, like the "more appointments" <div> *}
<div class="layer"></div>

{* LASIK PAGE POPUP FORM ============================================================================================ *}
{if isset($pageType) && $pageType == "custom-search"}
    {include file="search/_lasik-booking-form.tpl"}
{/if}
{* END - LASIK PAGE POPUP FORM ====================================================================================== *}

{if $CONFIG.GOOGLE_ANALYTICS_ACTIVATE}
    {include file="js/tracking/_google-remarketing-tag.tpl"}
{/if}
{if $CONFIG.GOOGLE_ANALYTICS_ACTIVATE}
    {include file="js/tracking/_freespee-phonenumber-tracking.tpl"}
{/if}
{* The closing tags are here because of the different layout of the search page (similar as footer.tpl placement, too). *}

<script type="text/javascript">
    $(window).load(function() {
        var screen = $(window);
        if (screen.width() <= 768) {
            $( "#map" ).hide();
            $("#filters").hide();
        }
        else {
            $( "#map" ).show();
            $("#filters").show();
        }
    });
    $(window).scroll( function() {
        if( $(this).scrollTop() > 0 ) {
            $("#logo").hide();
        }
        else {
            $("#logo").show();
        }
    });

    $( "#map_toggle" ).click(function() {
        $(this).toggleClass("control-menu-active");
        var screen_height = screen.height - 100;
        $( "#search" ).css('height', screen_height);
        $( "#map" ).toggle();
        initMap();

    });
    $( "#filter_toggle" ).click(function() {
        $(this).toggleClass("control-menu-active");
        $("#filters").toggle();

    });

    function initMap() {
        var map = document.getElementById('search-map');
        google.maps.event.trigger(map, 'resize');
    }

     function centerMap() {
         var center = map.getCenter();
         google.maps.event.trigger(map, 'resize');
         map.setCenter(center);
     }

</script>