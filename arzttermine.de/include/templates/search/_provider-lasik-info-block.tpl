{* Iterate all docs to get the one lasik data with the matching id. *}    
{foreach from=$doctors item=doc}
{if $user->getId() == $doc["id"]}
<div class="lasik">
  <div class="lasik-info">
    
    <div class="column">
      <button class="book-appointment">Termin anfragen</button>
      <p>Kostenfreien Termin anfragen</p>
    </div>
    
    <div class="column">
      <section class="services">
        <h4>Leistungen</h4>
        <ul>
        {foreach from=$doc["highlights"] item=highlight}
          <li>{$highlight}</li>
        {/foreach}
        </ul>
        <div class="price">Preis: {$doc["price"]}</div>
      </section>
    </div>
    
  </div>
</div>

{/if}
{/foreach}
