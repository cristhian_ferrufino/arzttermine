<form id="filters" class="hidden collapsed" action="{url path="search"}" method="get">
    <div id="filters-wrapper">

        <div class="filters-primary">


            <div class="filter essential location">
                <label for="filter-location">Ort</label>
            </div>

            <div class="filter">
                <label for="input_distance_select">Max. Entfernung vom Suchort</label>
                <select id="input_distance_select" name="form[distance]" class="input_text">
                {* Because the method for searching distances is changed, we need to manually build this option box for now. @todo Change config *}
                {foreach from=$CONFIG.SEARCH_DISTANCES_KM item="distance"}
                    <option value="{$distance}"{if $distance == $search->getRadius()} selected{/if}>{$distance}km</option>
                {/foreach}
                </select>
            </div>

            {* If there are neighbours available for the searched city, show them. *}
            {districts for=$form.location assign_to="districts"}
            {if $districts|count}
                <div class="filter">
                    <label for="filter-district">Stadtteil</label>
                    <select id="filter-district">
                        <option value="0">Stadtteil auswählen...</option>
                    {foreach from=$districts item="district"}
                        <option value="{$district->getId()}"{if $district->getId()==$form.district_id} selected="selected"{/if}>{$district->getName()}</option>
                    {/foreach}
                    </select>
                    <input type="hidden" id="filter-selected-district" name="form[district_id]" class="input_text" value="{$form.district_id}"/>
                </div>
            {/if}

            <div class="filter col-2 reset essential specialty">
                <label for="filter-specialty">Fachrichtung</label>
                {* This gets populated with JS *}
            </div>

            <div class="filter col-2">
                {strip}{* The browser renders whitespace here, so we need to remove it :/ *}
                {assign var="selectedInsurance" value=$search->getInsuranceId()}
                <label for="filter-insurance-type">Versicherungsart</label>
                
                <input type="radio" id="filter-insurance-type-1" name="form[insurance_id]" value="2" {if $selectedInsurance == 2}checked{/if}>
                <label class="s33" for="filter-insurance-type-1">gesetzlich</label>
                
                <input type="radio" id="filter-insurance-type-2" name="form[insurance_id]" value="1" {if $selectedInsurance == 1}checked{/if}>
                <label class="s33" for="filter-insurance-type-2">privat</label>
                {/strip}
            </div>

            <div class="filter col-2">
                <label for="filter-treatment">Behandlungsgrund</label>
                <select id="filter-treatment" name="form[treatmenttype_id]" >
                    <option value="">Bitte wählen</option>
                    {html_options options=$MedicalSpecialityAndTreatmentTypesMapArray[$search->getMedicalSpecialtyId()] selected=$form.treatmenttype_id}
                </select>
            </div>
        </div>{* .filters-primary *}
    </div>{* #filters-wrapper *}

    <div class="toggler"> {* new layout for filter form *}

        <button type="button" class="toggle-filters" style="max-width: 15%;">
            <span class="text">Filter</span>
            <span class="additional-text show">einblenden</span>
            <span class="additional-text hide">ausblenden</span>
        </button>

        <div id="filters-essential">

            <input id="filter-location" type="text" name="form[location]" placeholder="Ort" value="{if $form.location}{$form.location}{/if}" style="max-width: 20%;">

            <select id="filter-specialty" name="form[medical_specialty_id]" style="max-width: 50%;">
                <option value="">Bitte wählen</option>
                {html_options options=$MedicalSpecialtyOptions selected=$search->getMedicalSpecialtyId()}
            </select>

            <button type="submit" class="refresh-filters" style="max-width: 20%;">
                <span>Ergebnisse filtern</span>
            </button>


         </div>
    </div>
</form>


