<!DOCTYPE html>
<html>
<head>
  <title>{$this->getHeadTitle()}</title>

  <link rel="shortcut icon" href="/static/images/assets/favicon.ico">
  
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

  {* 
    Way to skip the cache when publishing new CSS. This should be changed when any css file is changed. 
    Convention MMDD-number, where number must be different than the previous one.
  *}

  {assign var="cacheVersion" value="2807-0"}
  <link rel="stylesheet" type="text/css" href="/static/css/common.min.css?cv={$cacheVersion}">
  <link rel="stylesheet" type="text/css" href="/static/css/{$template_body_class}.min.css?cv={$cacheVersion}">

  {if $profile->isValid() && $profile->checkPrivilege('system_internal_group')}
  <link rel="stylesheet" type="text/css" href="/static/css/admin-navigation.css?cv={$cacheVersion}">
  {/if}
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap-grid.min.css?cv={$cacheVersion}" rel="stylesheet" type="text/css">
  {literal}
  <script type='text/javascript'>
    var AT = AT || {};

    AT.q=[];
    AT._defer=function(f){
      AT.q.push(f);
    };
  </script>
  <script>
      var _prum = [['id', '58257a9a73cf627db9889fe6'],
          ['mark', 'firstbyte', (new Date()).getTime()]];
      (function() {
          var s = document.getElementsByTagName('script')[0]
              , p = document.createElement('script');
          p.async = 'async';
          p.src = '//rum-static.pingdom.net/prum.min.js';
          s.parentNode.insertBefore(p, s);
      })();
  </script>
  {/literal}

  <!--[if lte IE 9]-->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js"></script>
    <script type="text/javascript">
      document.createElement("header");
      document.createElement("nav");
      document.createElement("aside");
      document.createElement("main");
      document.createElement("article");
      document.createElement("section");
      document.createElement("footer");
    </script>
    <style>
      .search > header {
        position: fixed;
        top: 0;
      }
      .two-cols-with-form form .form-element.error:after {
        margin-top: 10px;
      }
    </style>
  <!--[endif]-->
  
  <!--[if gte IE 9]><!-->
  <link href='//fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>
  <link href='//fonts.googleapis.com/css?family=Gudea:100,300,400,700,900' rel='stylesheet' type='text/css'>
  <!--<![endif]-->

  <meta name="keywords" content="{$this->getMetaTag('keywords')}" />
  <meta name="description" content="{$this->getMetaTag('description')}" />
</head>

{* Microdata setup *}
{if $template_body_class == "search"}
  {assign var="webPageType" value="SearchResultsPage"}
{elseif $template_body_class == "profile-page"}
  {assign var="webPageType" value="ProfilePage"}
{/if}

{* Alert bar setup *}
{assign var="hasAlert" value=$this->hasStatusMessages()}

<body class="{$template_body_class}{if isset($additional_body_classes)} {$additional_body_classes}{/if}{if $hasAlert} has-alert{/if}"{if isset($webPageType)} itemscope itemtype="http://schema.org/{$webPageType}"{/if}>
{include file="js/tagmanager/tagmanager.tpl"}
{include file="_admin-navigation.tpl"}

  <header>
    <div id="top-bar" class="top-bar">
      <div class="hidden-lg-up">
        <a href="/">
          <img class="img-responsive" src="/static/img/arzttermine_lw.svg" alt="Arzttermine.de" style=" width: 7.5rem; padding-left: .5rem; float: left">
        </a>
      </div>

      <div class="top_nav hidden-sm-down">
        {* 
          ==== BOOKING & DOC REGISTRATION PAGES EXCEPTION ================================================================
          On "conversion" pages, only the logo and the phone number are displayed in the header.
        *}
        {if !isset($hasFullHeader) || (isset($hasFullHeader) && !$hasFullHeader)} 
        <nav>
          <ul>
            <li class="active"><a href="/zahnarzt/berlin">{'ui.menu.doctor_search'|trans}</a></li>
            <li>
              <a href="/lp/praxis-marketing/praxis-marketing.html">{'ui.menu.for_doctors'|trans}</a>
              <section class="top-bar-popup">
                <h4>Ärzte-Login</h4>
                <p>Sie haben schon einen Ärzte-Zugang?</p>
                <a href="{url path='doctor_login'}">Loggen Sie sich hier ein!</a>
                <section>
                  <h5>Neu registrieren</h5>
                  <p>Sie haben noch keinen Ärzte-Zugang?</p>
                  <a href="{url path='doctor_register'}">Registrieren Sie sich hier kostenlos!</a>
                </section>
                <section>
                  <h5>Mehr Patienten gewinnen</h5>
                  <p>Sorgen Sie durch erhöhte Online-Auffindbarkeit für eine bedarfsgerechte Neupatientengewinnung!</p>
                  <a role="button" href="/lp/praxis-marketing/praxis-marketing.html">Mehr Informationen</a>
                </section>
              </section>
            </li>
          </ul>
        </nav>
        {/if} {* closing booking page exception *}

        <div class="phone-container">
          <span>
            {* On our sales pages (e.g. doc registration) we show their phone number. *}
            {if isset($isSales) && $isSales}
            {'ui.menu.free_call'|trans}
            {include file="phone-numbers/phone-sales.tpl"}
            {else}
            {'ui.menu.free_booking'|trans}
            {include file="phone-numbers/phone-main.tpl"}
            {/if}
          </span>
        </div>
      </div>
    </div>

    <div class="top_nav">
      {include file="search/logo.tpl"}

      {if isset($breadcrumbs) && !empty($breadcrumbs) }
      <div class="breadcrumbs" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
        {foreach from=$breadcrumbs item=breadcrumb}
        <ul>
        {foreach from=$breadcrumb item=breadcrumbItem}
          <li>
            <a itemprop="url" href="{$breadcrumbItem.url}" {if $breadcrumbItem.title}title="{$breadcrumbItem.title}"{/if}>
              <span itemprop="title">{$breadcrumbItem.text}</span>
            </a>
          </li>
        {/foreach}
        </ul>
        {/foreach}
      </div>
      {/if}

      {* The alert bar *}
      {include file="_status-message.tpl"}

    </div>
  </header>
