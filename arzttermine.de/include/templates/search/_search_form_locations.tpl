<div id="search-location-advice" class="grid_1 padding-left-right">
	<h2>Mögliche Orte</h2>

	<div id="search_locations">
		<ul>

{foreach from=$this->locations item='location'}
			<li><a href="{$location.url}">{$location.text}</a></li>
{/foreach}
		</ul>
	</div>

	<div class="clear"></div>
</div>
<div class="border-top"></div>
