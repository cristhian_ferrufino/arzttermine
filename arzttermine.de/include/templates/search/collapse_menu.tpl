<div id="myNav" class="overlay">
  <a href="javascript:void(0)"  onclick="closeNav()"><i class="fa fa-times fa-2x closebtn" aria-hidden="true"></i></a>
  <div class="overlay-content">
    <a href="/faq">FAQ</a>
    <a href="/kontakt">Kontakt</a>
    <hr class="break_section">
    <a href="/fachgebiete">Medizinische Fachgebiete</a>
    <a href="/lp/patienten/so-funktioniert-arzttermine">Patienten Informationen</a>
    <a href="/magazin">Magazin</a>
    <hr class="break_section">
    <a href="/praxisoptimierung">Praxisoptimierung</a>
    <a href="/aerzte-magazin">Ärzte Magazin</a>
    <hr class="break_section">
    <a href="/angebote-informationen">Angebot Informationen</a>
    <hr class="break_section">
    <a href="/jobs">Jobs</a>
    <a href="/datenschutz">Datenschutz</a>
    <a href="/impressum">Impressum</a>
    <a href="/agb">AGB</a>
  </div>
</div>

<span onclick="openNav()"><a class="control-menu" style="float: right;" ><i class="fa fa-bars fa-2x" aria-hidden="true"></i></a></span>

<script>
    function openNav() {
        document.getElementById("myNav").style.height = "100%";
        $("#results-list").hide();
        $( "#map" ).hide();
    }

    function closeNav() {
        document.getElementById("myNav").style.height = "0%";
        $("#results-list").show();
        $( "#map" ).hide();
    }
</script>