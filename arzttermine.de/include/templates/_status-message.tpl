{assign var="statusMessages" value=$this->getStatusMessages()}
{if $statusMessages|@count > 0}
<div class="alert-wrapper">
{foreach from=$statusMessages key=type item=messages}
    {foreach from=$messages item=message}
    {* hack to use bootstrap classes in admin *}
    {assign var="bootstrap_class" value=""}
    {if $type == 'NOTICE'}{assign var="bootstrap_class" value="success"}{/if}
    {if $type == 'WARNING'}{assign var="bootstrap_class" value="warning"}{/if}
    {if $type == 'ERROR'}{assign var="bootstrap_class" value="danger"}{/if}
    <div class="alert alert-{strtolower($type)} alert-{$bootstrap_class}">{$message}</div>
    {/foreach}
{/foreach}
</div>
{/if}
