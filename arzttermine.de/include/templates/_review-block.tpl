<section itemscope class="review">
  <h4>
    {$review->getRatedAtDateText()} 
    {if $review->isAnonymous()}
      {"von einem verifizierten Patienten"|trans}
    {else} 
      von <span class="name" itemprop="author">{$review->getPatientName()}</span> ({"Verifizierter Patient"|trans})
    {/if}
  </h4>
  <ul class="inlined">
    <li itemprop="keyword">
      Behandlung
      {include file="_star-rating.tpl" rating=$review->getRating('rating_1')}
    </li>
    <li itemprop="keyword">
      Wartezeit
      {include file="_star-rating.tpl" rating=$review->getRating('rating_2')}
    </li>
    <li itemprop="keyword">
      Gesamtbewertung
      {include file="_star-rating.tpl" rating=$review->getRating('rating_3')}
    </li>
  </ul>
  <p itemprop="reviewBody">
    {$review->getRateText()}
  </p>
</section>
