{strip}
{assign var="template_body_class" value="profile-page"}
{assign var="additional_body_classes" value="location"}
{assign var="activate_freespee_phonenumber_tracking" value=true}
{include file="header.tpl"}
{/strip}

{assign var="medicalspecialties" value=$location->getMedicalSpecialties()}

<div class="container">
    <main itemscope itemtype="http://schema.org/MedicalOrganization">

      {include file="location/summary.tpl"}
      {include file="location/calendar.tpl"}
      {assign var="info_1" value=$location->getInfoText('info_1')}
      {assign var="info_location" value=$location->getInfoText('info_location')}
      {assign var="info_seo" value=$location->getInfoText('info_seo')}
      {if $info_1 || $info_location || $info_seo}
          {include file="location/additional_info.tpl"}
      {/if}

    </main>
</div>


{include file="footer.tpl"}
