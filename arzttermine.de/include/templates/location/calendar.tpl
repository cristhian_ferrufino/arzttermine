<section class="calendar-container">
    {if $location->getIntegration()->getId() == 8}
      <h2>Fragen Sie Ihren Wunschtermin an</h2>
    {else}
      <h2>Wählen Sie Ihren Wunschtermin aus</h2>
    {/if}
    {assign var="isPKV" value=$insurance->getId() == 1}{* Refactor for a real check *}

  <div id="providers" data-date-start="{$search->getStart()->getDate()}" data-date-end="{$search->getEnd()->getDate()}" data-date-days="{$search->getDayCount()}" style="margin-top: 1rem">
      {foreach from=$providers item=providerRow name=searchForeach}
          {assign var="index" value=$smarty.foreach.searchForeach.index}
          {assign var="hideDistance" value=true}
          {include file="search/provider.tpl"}

          {if $pagination}
            <div class="pagination">{$pagination}</div>
          {/if}
      {/foreach}
  </div>
    <div class="insurance-selector">
        <a class="insurance-selector-button public {if !$isPKV} active{/if}" href="{if $isPKV}{$location->getUrl()}/gkv{else}#{/if}">gesetzlich</a>
        <a class="insurance-selector-button private {if $isPKV} active{/if}" href="{if !$isPKV}{$location->getUrl()}/pkv{else}#{/if}">privat</a>
    </div>
</section>