<section class="additional-info">
    <h2>Weitere Informationen zu dieser Praxis</h2>
    {if $location->getInfoText('info_1')}
        <section>
            <div class="row">
                <div class="col-sm-3">
                    <h3>{$this->_('Philosophie')}</h3>
                </div>
                <div class="col-sm-8">
                    <div class="additional-info-text" itemprop="description">
                        {$location->getInfoText('info_1')}
                    </div>
                </div>
            </div>
        </section>
    {/if}

    {if $location->getInfoText('info_location')}
        <section>
            <div class="row">
                <div class="col-sm-3">
                    <h3>{$this->_('Wegbeschreibung')}</h3>
                </div>
                <div class="col-sm-8">
                    <div class="additional-info-text" itemprop="description">
                        {$location->getInfoText('info_location')}
                    </div>
                </div>
            </div>
        </section>
    {/if}

    {if $location->getInfoText('info_seo')}
        <section>
            <div class="row">
                <div class="col-sm-3">
                    <h3>{$this->_('Beschreibung')}</h3>
                </div>
                <div class="col-sm-8">
                    <div class="additional-info-text" itemprop="description">
                        {$location->getInfoText('info_seo')}
                    </div>
                </div>
            </div>
        </section>
    {/if}
</section>