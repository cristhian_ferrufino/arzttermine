<section class="summary">
  <div class="row">
    <div class="col">
      <div class="headline justified-blocks">
        <h1 itemprop="name">{$location->getName()}</h1>
        <div itemprop="telephone">
            {if $location->hasPhoneVisible()}
              <a href="tel:{$location->getPhoneVisible()}">{$location->getPhoneVisible()}</a>
            {else}
              <a href="tel:08002222133">0800 - 22 22 133</a>
            {/if}
            {*}DIRTY HACK for a Test!{*}
            {if ($location->getId() == 39505)}
              <br />
              <span style="font-size:12px;color:#313131;">Für unsere Schweizer Patienten:</span><br />
              <a href="tel:0041325109419">+41 (32) 5109419</a>
            {/if}
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm">
        <div class="photo" itemprop="photo" itemscope itemtype="http://schema.org/MediaObject">
          <img itemprop="contentUrl" src="{$location->getProfileAssetUrl($smarty.const.ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED)}" alt="{$this->alt($location->getName())}">
        </div>
      </div>
      <div class="col-sm-6">
        <div class="margin_summary">
          <div class="address" itemprop="address">
            <div itemscope itemtype="http://schema.org/PostalAddress">{$location->getStreet()}</div>
            <div>
              <span itemprop="postalCode">{$location->getZip()}</span>
              <span itemprop="addressLocality">{$location->getCity()}</span>
            </div>
          </div>
          <div itemprop="geo" itemscope itemtype="http://schema.org/GeoCoordinates">
            <meta itemprop="latitude" content="{$location->getLat()}" />
            <meta itemprop="longitude" content="{$location->getLng()}" />
          </div>

            {if $location->showReviews()}
              <div class="ratings" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                <meta itemprop="reviewcount" content="{$location->countReviews()}" />
                <h4>Bewertung</h4>
                <div>
                  <div class="rating-name">{$this->_('Verhalten des Arztes')}</div>
                    {include file="_star-rating.tpl" rating=$location->getRating('rating_1')}
                </div>
                <div>
                  <div class="rating-name">{$this->_('Wartezeit')}</div>
                    {include file="_star-rating.tpl" rating=$location->getRating('rating_2')}
                </div>
                <div>
                  <div class="rating-name">{$this->_('Gesamtbewertung')}</div>
                    {include file="_star-rating.tpl" rating=$location->getRating('rating_3')}
                </div>
              </div>
            {/if}

          <div class="specialties" itemprop="medicalSpecialty">
            <h4>Fachgebiete</h4>
              {if $location->hasPreferredMedicalSpecialtyText()}{$location->getPreferredMedicalSpecialtyText()}{else}{$location->getMedicalSpecialityIdsText()}{/if}
          </div>
        </div>
      </div>
      <div class="col">
        <div class="margin_summary">
          <img itemprop="map" src="//maps.googleapis.com/maps/api/staticmap?center={$location->getLat()},{$location->getLng()}&markers={$location->getLat()},{$location->getLng()}&zoom=11&size=247x160&sensor=false&v=3.exp&key=AIzaSyA-cGD6Wm_86W_BzBctDnMMD5j7iM0is1I">
        </div>
      </div>
    </div>
  </div>
</section>