{strip}
{assign var="template_body_class" value="home"}
{include file="header.tpl"}
{/strip}
<div class="container">
  <main>
    {include file="home_search_box.tpl"}
    {*{include file="find_doctor.tpl"}*}
    {include file="doctors-carousel.tpl"}
    {include file="call_to_action_my_area.tpl"}
    {include file="partners.tpl"}
    {include file="_quotes.tpl"}
    {include file="press.tpl"}
    {include file="patient_magazin.tpl"}
    {include file="security.tpl"}
    {include file="professional_solutions_banner.tpl"}
    {include file="product_carousel.tpl"}
    {include file="_form_for_doctors.tpl"}
    {include file="doctors_magazin.tpl"}
  </main>
</div>
{include file="_referral-code-support.tpl"}
{include file="footer.tpl"}