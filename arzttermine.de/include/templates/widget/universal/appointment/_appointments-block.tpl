{if $appointmentsBlock.type == 'appointments'}

    {assign var="appointmentDays" value=$appointmentsBlock.days}
    {foreach from=$appointmentDays item=day}
        {assign var="appointments" value=$day.appointments}
        <div class="calendar">
            {if $appointments|@count > 0}
                <div class="time">
                    {foreach from=$appointments item=appointment}
                        {if $appointment@iteration == $CONFIG.CALENDAR_TIMES_SHOW_MORE_LINK_AFTER_HOW_MANY_TIMES
                            && $appointments|@count > $CONFIG.CALENDAR_TIMES_SHOW_MORE_LINK_AFTER_HOW_MANY_TIMES}
                        <a class="btn_soft_green more_times_link" href="#">mehr</a>
                        <div class="more_times">
                        {/if}
                        <a class="btn_soft_green" rel="nofollow" href="{$appointment.url}"{if $widget_slug=='berlinde'} target="_blank"{/if}>{$appointment.time_text}</a>
                    {/foreach}

                    {if $appointments|@count > $CONFIG.CALENDAR_TIMES_SHOW_MORE_LINK_AFTER_HOW_MANY_TIMES}
                        </div>
                    {/if}
                </div>
            {/if}
        </div>
    {/foreach}

{elseif $appointmentsBlock.type == 'next-available-appointment'}
    {assign var="appointment" value=$appointmentsBlock.appointment}
    <div class="calendar-full-width"><div class="calendar-next-available">Nächster freier Termin:<br /><a class="f_go_to_app_week btn_soft_green" id="naadate^{$appointment->getDate()}" href="javascript:void(0);">{$appointment->getDateTimeText()}</a></div></div>
{elseif $appointmentsBlock.type == 'custom-message'}
    <div class="calendar-full-width"><div class="calendar-next-available" style="word-break:break-all; width:85%">{$appointmentsBlock.customMessage}</div></div>
{elseif $appointmentsBlock.type == 'no-available-appointments'}
    <div class="calendar-full-width"><div class="calendar-next-available">Keine freien Termine verfügbar</div></div>
{elseif $appointmentsBlock.type == 'no-selected-insurance'}
    <div class="calendar-full-width"></div>
{elseif $appointmentsBlock.type == 'integration_8'}
    <div class="calendar-full-width"><div class="calendar-next-available book-now">
            <a class="btn_soft_green" href="{$appointmentsBlock.url}" rel="nofollow"{if $widget_slug=='berlinde'} target="_blank"{/if}>{$appointmentsBlock.text}</a>
            <div>{$appointmentsBlock.customMessage}</div>
        </div></div>
{elseif $appointmentsBlock.type == 'integration_9'}

    {if $appointmentsBlock.widgetSlug == ''}
        <div class="calendar-full-width"><div class="calendar-message">
                <a class="btn_soft_green">{Application::_("Bitte wählen Sie Ihren Wunschzeitraum")}</a>
            </div></div>
    {/if}
    {assign var="appointmentDays" value=$appointmentsBlock.days}
    {foreach from=$appointmentDays item=day}
        {assign var="appointments" value=$day.appointments}
        <div class="calendar">
            {if $appointments|@count > 0}
                <div class="time">
                    {foreach from=$appointments item=appointment name=appointmentsForeach}
                    {assign var="appointmentsIndex" value=$smarty.foreach.appointmentsForeach.index}
                    {if $appointmentsIndex == $CONFIG.CALENDAR_TIMES_SHOW_MORE_LINK_AFTER_HOW_MANY_TIMES}
                    <a class="btn_soft_green more_times_link" href="#">mehr</a><div class="more_times">
                        {/if}
                        <a class="btn_soft_green" rel="nofollow" href="{$appointment->getUrl()}"{if $widget_slug=='berlinde'} target="_blank"{/if}>{$appointment->getCustomText()}</a>
                        {/foreach}
                        {if $appointmentsIndex >= $CONFIG.CALENDAR_TIMES_SHOW_MORE_LINK_AFTER_HOW_MANY_TIMES}
                    </div>
                    {/if}
                </div>
            {/if}
        </div>
    {/foreach}

{/if}