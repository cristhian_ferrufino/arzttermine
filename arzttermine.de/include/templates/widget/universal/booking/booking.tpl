{strip}
{assign var="template_body_class" value=" booking"}
{include file="../header.tpl"}
{/strip}

<div id="mux-widget" class="widget-container form {$widget_slug}">
    <div id="header">
        <div class="top_picto"></div>
        {strip}
        <h3>
        {if $location->hasAppointmentEnquiryIntegration()}
			{$app->_('Ihre Terminanfrage')}
		{else}
			{$app->_('Ihre Terminwahl')}
		{/if}
		</h3>
		{/strip}
    </div>

{if $contentMain}
	{$contentMain}
{/if}

{include file="../footer.tpl"}
