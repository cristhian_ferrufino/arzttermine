<div class="doctor_info">
    <div class="appointment_info">
		{if $integration_id == 8}
		<div class="appointment_starts datetime unavailable">
            {$app->_('Bitte hinterlassen Sie uns Ihre Kontakdaten. Wir kümmern uns um den Rest und melden uns noch heute mit freien Terminvorschlägen bei Ihnen!')}
		</div>
		{elseif $integration_id == 9}
		<div class="appointment_starts datetime">{calendar action="getWeekdayDateTimeText" datetime=$form.appointment_start_at short=true} - {calendar action="getDateTimeFormat" datetime=$form.appointment_end_at format="H:i"} Uhr</div>
		{else}
		<div class="appointment_starts datetime">{calendar action="getWeekdayDateTimeText" datetime=$form.appointment_start_at short=true}</div>
		{/if}
        <div class="user_info">{$user->getName()} {$user->getMedicalSpecialityIdsText(', ')}</div>
    </div>
</div>

{include file="./_form.tpl"}
