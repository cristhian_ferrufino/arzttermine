<div class="form_container">
    <form id="booking-form" action="{$app->getRequest()->getRequestUri()}" name="edit" method="post" enctype="application/x-www-form-urlencoded" style="display:inline;">
        <div class="data-container">
            <div class="field salutation-container field-text">
                <label id="salutation"{if in_array('gender', $form_errors)} class="text_error"{/if}>{$app->_('Anrede')}</label>
                <input id="form_gender_1" type="radio" name="form[gender]" value="1" class="validate[required] radio input_radio"{if $form.gender=='1'} checked="checked"{/if}>
                <label id="form_label_gender_1" class="radio-gender" for="form_gender_1">{$app->_('Frau')}</label>
                <input id="form_gender_0" type="radio" name="form[gender]" value="0" class="validate[required] radio input_radio"{if $form.gender=='0'} checked="checked"{/if}>
                <label id="form_label_gender_0" class="radio-gender" for="form_gender_0">{$app->_('Herr')}</label>
            </div>
            <input placeholder="{$app->_('Vorname')}" id="form_first_name" name="form[first_name]" class="input_text {if in_array('first_name', $form_errors)}input_error{/if}" value="{$form.first_name}" />
            <input placeholder="{$app->_('Nachname')}" id="form_last_name" name="form[last_name]" class="input_text {if in_array('last_name', $form_errors)}input_error{/if}" value="{$form.last_name}" />
            <input placeholder="{$app->_('E-Mail')}" id="form_email" name="form[email]" class="input_text {if in_array('email', $form_errors)}input_error{/if}" value="{$form.email}" />
            <input placeholder="{$app->_('Mobil')}" id="form_phone" name="form[phone]" class="input_text {if in_array('phone', $form_errors)}input_error{/if}" value="{$form.phone}" />
            <div class="insurance-container field field-select cf">
                <label class="label-mux {if in_array('insurance_id', $form_errors)}input_error{/if}">{$app->_('Versicherungsart')}</label>
                <div style="display: inline-block; vertical-align: top;">
                    <input id="form_insurance_pkv" type="radio" name="form[insurance_id]" value="1" class="validate[required] radio input_radio"{if $form.insurance_id=='1'} checked="checked"{/if}>
                    <label id="form_label_insurance_pkv" class="radio-insurance" for="form_insurance_pkv">{$app->_('privat')}</label>
                    <input id="form_insurance_gkv" type="radio" name="form[insurance_id]" value="2" class="validate[required] radio input_radio"{if $form.insurance_id=='2'} checked="checked"{/if}>
                    <label id="form_label_insurance_gkv" class="radio-insurance" for="form_insurance_gkv">{$app->_('gesetzlich')}</label>
                </div>
            </div>

            <div class="field field-select cf">
                <label class="label-mux returning_visitor {if in_array('returning_visitor', $form_errors)}text_error{/if}">
                    {$app->_('Waren Sie bereits Patient/in bei')}
                        <span class="doctor-name">{$user->getName()}</span>
                </label>
                <div style="display: inline-block; vertical-align: top;">
                    <input id="form_returning_visitor_1" type="radio" name="form[returning_visitor]" value="1" class="validate[required] radio input_radio"{if $form.returning_visitor=='1'} checked="checked"{/if}>
                    <label id="form_label_returning_visitor_1" class="radio-returning_visitor" for="form_returning_visitor_1">{$app->_('Ja')}</label>
                    <input id="form_returning_visitor_0" type="radio" name="form[returning_visitor]" value="0" class="validate[required] radio input_radio"{if $form.returning_visitor=='0'} checked="checked"{/if}>
                    <label id="form_label_returning_visitor_0" class="radio-returning_visitor" for="form_returning_visitor_0">{$app->_('Nein')}</label>
                </div>
            </div>

            <div class="field field-select cf">
                <label for="form_treatmenttype_id" class="treatmenttype_id label-mux">
                    {$app->_('Behandlungsgrund')}
                    <i style="font-weight: normal; font-size: 11px;">(freiwillig)</i>
                </label>
                {html_options class="input_select" id="form_treatmenttype_id" name="form[treatmenttype_id]" options=$treatmenttypes selected=$form.treatmenttype_id}
            </div>
        </div>
        <div class="form-acceptance">
            <div class="field agb cf">
                <input id="form_agb" type="checkbox" name="form[agb]" value="1" class="input_checkbox {if in_array('agb', $form_errors)}text_error{/if}"{if $form.agb=='1'} checked="checked"{/if}>
                <label class="agb {if in_array('agb', $form_errors)}text_error{/if}" for="form_agb">{$booking_agb_confirmation_text}</label>
            </div>

            <div class="submit-container cf">
                <input type="hidden" name="form[location_id]" value="{$form.location_id}" />
                <input type="hidden" name="form[user_id]" value="{$form.user_id}" />
                <input type="hidden" name="form[medical_specialty_id]" value="{$form.medical_specialty_id}" />
                <input type="hidden" name="form[start_at_id]" value="{$form.start_at_id}" />
                <input type="hidden" name="form[end_at_id]" value="{$form.end_at_id}" />
                <input type="hidden" name="form[form_once]" value="{$form.form_once}" />

                <span class="relative-container">
                    <button name="ok" type="submit" class="input_submit button large btn_green" id="ok" tabindex="4" accesskey="o">{$app->_('Termin vereinbaren')}</button>
                    <span class="gelbseiten-footnote">1</span>
                </span>
{if $widget->getSlug() == 'gs-widget'}
                <span id="additionalDisclaimer">1: Buchung über externe Partner</span>
{/if}
                <div class="clear"></div>
            </div>
        </div>
    </form>
</div>
<div class="clear"></div>

<script type="text/javascript">
    $(document).ready(function() {
        /*watermark inputs so they work in IE*/
        $("#form_first_name").watermark("{$app->_('Vorname')}");
        $("#form_last_name").watermark("{$app->_('Nachname')}");
        $("#form_email").watermark("{$app->_('E-Mail')}");
        $("#form_phone").watermark("{$app->_('Mobil')}");
    });
</script>
