<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html 
	xmlns="http://www.w3.org/1999/xhtml" 
	xml:lang="de" lang="de">
<head>
	<title>{$this->getHeadTitle()}</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="content-language" content="{$app->getLanguageCode()}" />
	<meta name="description" content="{$this->getMetaTag('description')}" />
	<meta name="keywords" content="{$this->getMetaTag('keywords')}" />
	<meta name="robots" content="index, follow" />

	<link href="{$this->getStaticUrl('img/favicon.ico')}" rel="shortcut icon" />

	{*}	
	<link rel="stylesheet" type="text/css" media="all" href="{$this->getStaticUrl('old-css/reset-min.css')}" />
	<link rel="stylesheet" type="text/css" media="all" href="{$this->getStaticUrl('old-css/base-min.css')}" />
	<link rel="stylesheet" type="text/css" media="all" href="{$this->getStaticUrl('old-css/fonts-min.css')}" />
	<link rel="stylesheet" type="text/css" media="all" href="{$this->getStaticUrl('old-css/widget-main.css',false,true)}" />
	{/*}
	
	{*}
	<link rel="stylesheet" type="text/css" media="all" href="{$this->getStaticUrl('old-css/widget.concat.css',false,true)}" />
	{/*}

	<link rel="stylesheet" type="text/css" media="all" href="{$this->getStaticUrl('old-css/widget.min.css',false,true)}" />
    
	{if $widget_css}	
    <link rel="stylesheet" type="text/css" media="all" href="{$this->getStaticUrl($widget_css,false,true)}" />
    {/if}

    {if $widget_min_css}
    <link rel="stylesheet" type="text/css" media="all" href="{$this->getStaticUrl($widget_min_css,false,true)}" />
    {/if}

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
	<script type="text/javascript" src="{$this->getStaticUrl('widget/jquery-plugins.js')}"></script>
	<script type="text/javascript" src="{$this->getStaticUrl('jquery/plugins/jquery.watermark_3.1.4.min.js')}"></script>

	<!--[if lte IE 8]>
	<script type="text/javascript">
		$(document).ready(function(){
			// Add .checked to all checked radio buttons.
			$("input[type=radio][checked]").addClass("checked");
			
			// When a label is clicked, remove .checked from all radio buttons in that group
			//  and add .checked to the radio button corresponding to this label.
			$("input[type=radio] + label").on("click", function() {
				var id = $(this).attr("for");
				var name = $("#" + id).attr("name");

				$("input[name='" + name + "']").removeClass("checked");
				$("#" + id).addClass("checked");
				
				// IE8 repainting bug: http://stackoverflow.com/a/13783841
				$(this).parent().addClass('z').removeClass('z');
			});
		});
	</script>
	<![endif]-->

</head>

<body class="js {$template_body_class} {$this->_('german')}">
{include file="js/tagmanager/tagmanager.tpl"}
