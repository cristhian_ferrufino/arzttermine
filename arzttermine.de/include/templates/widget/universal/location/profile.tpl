{strip}
{assign var="template_body_class" value=" profile-location"}
{include file="../header.tpl"}
{/strip}
{$date_days=$date_days-2}
{assign var="location" value=$providerRows[0]->getLocation()}
<div id="mux-widget" class="widget-container">
    <div id="header">
        <img width="122px" src="{$this->getStaticUrl('img/calendar.png')}" alt="arzttermine.de"/>
        <h3>Bitte klicken Sie auf Ihren Wunschtermin:
        {if $filter.insurance_id<1}
            <div class="provider-list-message">
                <b>{$this->_('Zum Termin buchen bitte Versicherungsart auswählen')}:</b>
                <a href="{$location->getUrl()}/pkv" class="button small orange border-grey bold" rel="nofollow">{$this->_('Privat versichert')}</a> oder <a href="{$location->getUrl()}/gkv" class="button small orange border-grey bold" rel="nofollow">{$this->_('Gesetzlich versichert')}</a>
            </div>
        {/if}
        </h3>
    </div>
    <div class="provider-list{if $filter.insurance_id<1} need-filter{/if} static-header">
        <div class="provider-list-header">
            <div class="header-prev-link">
                <div id="prev-loading" class="prev-slide-loading" style="display:none;"></div>
                <a id="prev-link" class="prev-slide-link" rel="nofollow" href="#" style="display:none;">{$this->_('Vorherige Woche')}</a>
            </div>
            <div class="header-next-link">
                <div id="next-loading" class="next-slide-loading" style="display:none;"></div>
                <a id="next-link" class="next-slide-link" rel="nofollow" href="#" style="display:none;">{$this->_('Nächste Woche')}</a>
            </div>

            <div class="calendar-container" date_start="{$date_start}" date_days="{$date_days}">
                <div class="calendar-slide odd">
                    {calendar action="getCalendarDaysHtml" datetime=$date_start date_days=$date_days}
                </div>
            </div>
        </div>
    {include file="../provider-time/_providers-times.tpl"}
    </div>

{if $filter.insurance_id>0}
{$widget=2}
{include file="js/widget/_provider_cycle.tpl"}
{/if}

{include file="../footer.tpl"}
