<div class="location profile">
	<div class="left">
		<div class="gfx">
			<a class="group" rel="gallery" title="{alt($location->getName())}" href="{$location->getProfileAssetUrl($smarty.const.ASSET_GFX_SIZE_STANDARD)}"><img src="{$location->getProfileAssetUrl($smarty.const.ASSET_GFX_SIZE_MEDIUM_LANDSCAPE_CROPPED)}" alt="{alt($location->getName())}" /></a>
		</div>
		<div class="gfx-thumbs">{$location->getAssetThumbsHtml(3)}</div>

		<div class="medical-specialties">
			<h4>{$this->_('Fachgebiete')}</h4>
			<ul>
			{$location->getMedicalSpecialityIdsText('<li>')}
			</ul>
		</div>
	</div>
	<div class="right">
		<h1 class="name">{$location->getName()}</h1>
		<div class="info">
			<div class="address">
				{$location->getAddress()}
			</div>
			{if $location->hasPhoneVisible()}
			<div class="phone"><div class="phone-icon"></div>{$location->getPhoneVisible()}</div>
			{/if}
			{if $location->getSlug() != 'optical-express-berlin'}
			<h4>{$this->_('Ärzte in dieser Praxis')}</h4>
			{/if}
			<ul class="providers">
{foreach from=$provider->users item='user'}
				<li>
					<a href="{$user->getUrlWithFilter($filter)}">{$user->getName()}</a>
				</li>
{/foreach}
			</ul>

			<h4>{$this->_('Bewertung')}</h4>
			{$location->getRatingHtml('rating_1', 'Fachkompetenz')}
			{$location->getRatingHtml('rating_2', 'Termin eingehalten')}
			{$location->getRatingHtml('rating_3', 'Vertrauensverhältnis')}
		</div>
	</div>
</div>
{literal}
<script type="text/javascript">
//<![CDATA[
$(document).ready(function() {
	$("a.group").fancybox({
	'speedIn'		:	600,
	'speedOut'		:	200,
	'titlePosition'  : 'over'
});
});

//]]>
</script>
{/literal}
