<div class="provider intro">
    <div class="mux_doctor_info">
        <div class="gfx">
            <img src="{$user->getProfileAssetUrl($smarty.const.ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED)}" alt="{$user->getName()} {$user->getMedicalSpecialityIdsText()}" />
        </div>
        <div class="info">
            <div class="name">
            {$user->getDoctorFullName()}
            </div>
            <div>
                <div style="display:inline-block; vertical-align: top;">
                {$user->getRatingHtml('rating_average', '', $location->getId())}
                    <div class="medical-specialties">{$user->getMedicalSpecialityIdsText()}</div>
                </div>
            </div>
        </div>
    </div>
    <div class="mux_appointment_times" style="display:inline-block; width: 324px;">
        <div class="times-container times-days-{$date_days}">
            <div class="times-slide odd">
            {$providerRow->getDaysTimesHtml($date_start, $date_days, $filter.insurance_id, $filter.medical_specialty_id, null,$widget_slug)}
            </div>

        </div>
    </div>

</div>
