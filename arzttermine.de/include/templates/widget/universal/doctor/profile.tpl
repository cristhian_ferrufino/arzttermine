{strip}
{assign var="template_body_class" value="profile-page"}
{include file="../header.tpl"}
{/strip}
{assign var=user value=$providers[0]->getUser()}
{assign var=location value=$providers[0]->getLocation()}
<div id="mux-widget" class="widget-container profile {$widget_slug}">
    <div id="header">
        <div class="top_picto"></div>
        <h3>Bitte klicken Sie auf Ihren Wunschtermin
        {if $insurance->getId()<1}
            <div class="provider-list-message">
                <b>{$this->_('Zum Termin buchen bitte Versicherungsart auswählen')}:</b><br /><br />
                <a href="{$user->getUrl()}/pkv" class="button small orange border-grey bold" rel="nofollow">{$this->_('Privat versichert')}</a> oder <a href="{$user->getUrl()}/gkv" class="button small orange border-grey bold" rel="nofollow">{$this->_('Gesetzlich versichert')}</a>
            </div>
        {/if}
        {if $widget_slug=='berlinde'}
            <br >bei {$user->getName()}
        {/if}
        </h3>
    </div>
    <div class="provider-list{if $insurance->getId()<1} need-filter{/if} static-header">
        <div class="provider-list-header">
            <div class="header-prev-link">
                <div id="prev-loading" class="prev-slide-loading" style="display:none;"></div>
                <a id="prev-link" class="prev-slide-link" rel="nofollow" href="#" style="display:none;">{$this->_('Vorherige Woche')}</a>
            </div>
            <div class="header-next-link">
                <div id="next-loading" class="next-slide-loading" style="display:none;"></div>
                <a id="next-link" class="next-slide-link" rel="nofollow" href="#" style="display:none;">{$this->_('Nächste Woche')}</a>
            </div>

            <div class="calendar-container" date_start="{$search->getStart()->getDate()}" date_days="{$search->getDayCount()}">
                <div class="calendar-slide odd">
                {calendar action="getCalendarDaysHtml" datetime=$search->getStart()->getDate() date_days=$search->getDayCount()}
                </div>
            </div>
        </div>
        <div class="provider-container provider-row" style="margin-top: 9px" user_id="{$user->getId()}" location_id="{$location->getId()}" medical_specialty_id="{$filter.medical_specialty_id}" insurance_id="{$insurance->getId()}">
        {include file="./doctor_profile.tpl"}
        </div>

    </div>

<script type="text/javascript">
    $('.provider-list').on('click', '.more_times_link', function(e) {
        e.preventDefault();

        var $this = $(this),
                $container_time = $this.parent(),
                $container_calendar = $container_time.parent(),
                $container_times_slide = $container_calendar.parent(),
                $container_times_container = $container_times_slide.parent();

        $this.next().show();

        $container_calendar.height($container_time.height());
        $container_times_container.children().height($container_time.height());
        $container_times_container.height($container_time.height());

        $this.hide();
    });
</script>

{if $insurance->getId()>0}
{include file="js/widget/_provider_cycle.tpl"}
{/if}

{include file="../footer.tpl"}
