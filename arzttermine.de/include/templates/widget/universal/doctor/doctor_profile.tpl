<div class="provider intro">
    <div class="mux_doctor_info">
        <div class="gfx">
            <img src="{$user->getProfileAssetUrl($smarty.const.ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED)}" alt="{$user->getName()} {$user->getMedicalSpecialityIdsText()}" />
        </div>
        <div class="info">
            <div class="name">
            {$user->getName()}
            </div>
            <div>
                <div style="display:inline-block; vertical-align: top;">
                {$user->getRatingHtml('rating_average', '', $location->getId())}
                    <div class="medical-specialties">{$user->getMedicalSpecialityIdsText()}</div>
                </div>
            </div>
        </div>
    </div>
	{foreach from=$providers item=provider}
	{if isset($multiple_locations)}
		<h3 style="text-align: center">{$provider->getLocation()->getName()}</h3>
	{/if}
    <div class="mux_appointment_times" style="display:inline-block; width: 324px;">
        <div class="times-container times-days-{$search->getDayCount()}">
            <div class="times-slide odd">
            {assign var="appointmentsBlock" value=$provider->getAppointmentsBlock($widget_slug)}
            {include file="appointment/_appointments-block.tpl"}
            </div>
        </div>
    </div>
	{/foreach}

</div>
