{strip}
{assign var="template_body_class" value=" confirmation"}
{$this->addJsInclude($this->getStaticUrl('js/at_main.js'))}
{include file="../header.tpl"}
{/strip}

<style>
    html{
        height: 400px;
        overflow: hidden;
    }
</style>
<div id="mux-widget" class="widget-container confirmation {$widget_slug}">
    <div id="header">
        <div class="top_picto"></div>
		{strip}
		<h3>
		{if $booking->hasAppointmentEnquiryIntegration()}
			{$app->_('Ihre Anfrage war erfolgreich')}
		{else}
			{$app->_('Ihre Buchung war erfolgreich')}
		{/if}
		</h3>
		{/strip}
	</div>
	<div class="appointment_info">
		<div class="user_info">
		{strip}
			{if $booking->hasAppointmentEnquiryIntegration()}
				{$app->_('Ihre Terminanfrage')}
			{else}
				{$app->_('Ihre Terminwahl')}
			{/if}
		{/strip}
		</div>
		<div class="appointment_starts">
			{if
				!$booking->hasAppointmentEnquiryIntegration()
				&&
				$booking->hasValidAppointmentStartAt()
			}
				{calendar action="getWeekdayDateTimeText" datetime=$booking->getAppointmentStartAt() short=false}
			{/if}
		</div>
	</div>
	<div id="doctor-info">
		<img src="{$user->getProfileAssetUrl($smarty.const.ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED)}" alt="{$user->getName()} | {$user->getMedicalSpecialityIdsText(', ')}" width="66" height="83">
		<div class="text_info">
			<div class="name">{$user->getName()}</div>
			<div class="specialty"><b>{$user->getMedicalSpecialityIdsText(', ')}</b></div>
	        <adress class="address">
				<p>{$location->getAddress()}<br /></p>
			</adress>
		</div>
	</div>
	<div id="hotlineBox" class="infoBox">
		<h3>{$app->_('Haben Sie noch Fragen?')}</h3>
		<p><b>{$app->_('Wir sind 24 Stunden pro Tag für Sie da')}</b></p>
		<p class="hotlineNr">
			<div class="telIcon"></div>
			<span style="position: relative;top: -5px;left: 3px;"><a href="tel:{$CONFIG.PHONE_SUPPORT}">{$CONFIG.PHONE_SUPPORT}</a></span>
		</p>
	</div>
{include file="../footer.tpl"}
