<form id="filters" class="hidden collapsed" action="{$app->i18nTranslateUrl('/w/dak/suche')}" method="get">
    <div id="filters-wrapper">
      <input type="hidden" name="a" value="search">
      
      <div class="filters-primary">
        <div class="filter essential location">
          <label for="filter-location">Ort</label>
        </div>

        <div class="filter">
          <label for="filter-distance">Max. Entfernung vom Suchort</label>
          <select id="input_distance_select" name="form[distance]" class="input_text">
            {html_options options=$CONFIG.SEARCH_DISTANCES_TEXTS selected=$form.distance}
          </select>
        </div>

        {* If there are neighbours available for the searched city, show them. *}
        {if isset($districts)}
        <div class="filter">
          <label for="filter-district">Stadtteil</label>
          <select id="filter-district">
            <option value="0">Stadtteil auswählen...</option>
            {foreach from=$districts item=item_district}
            <option value="{$item_district->getId()}"{if $item_district->getId()==$form.district_id} selected="selected"{/if}>{$item_district->getName()}</option>
            {/foreach}
          </select>
          <input type="hidden" id="filter-selected-district" name="form[district_id]" class="input_text" value="{$form.district_id}"/>
        </div>
        {/if}
        
        <div class="filter col-2 reset essential specialty">
          <label for="filter-specialty">Fachrichtung</label>
          {* This gets populated with JS *}
        </div>
        
        <div class="filter col-2">
          <label for="filter-treatment">Behandlungsgrund</label>
          <select id="filter-treatment" name="form[treatmenttype_id]" >
            <option value="">Bitte wählen</option>
            {html_options options=$MedicalSpecialityAndTreatmentTypesMapArray.{$form.medical_specialty_id} selected=$form.treatmenttype_id}
          </select>
        </div>

        <div class="filter col-2"></div>
      </div>{* .filters-primary *}

    </div>{* #filters-wrapper *}

    <div class="toggler">
      <div id="filters-essential">
        <input id="filter-location" type="text" name="form[location]" placeholder="Ort" value="{if $form['location']}{$form['location']}{/if}">
        <select id="filter-specialty" name="form[medical_specialty_id]">
          <option value="">Bitte wählen</option>
          {html_options options=$MedicalSpecialtyOptions selected=$form.medical_specialty_id}
        </select>
      </div>
      <button type="submit" class="refresh-filters"><span>Ergebnisse filtern</span></button>
      <button type="button" class="toggle-filters">
        <span class="text">Filter</span>
        <span class="show">einblenden</span>
        <span class="hide">ausblenden</span>
      </button>
    </div>

  </form>
