{assign var="user" value=$providerRow->getUser()}
{assign var="location" value=$providerRow->getLocation()}
{if isset($multiple_locations)}
    {$location->getName()}
{/if}

<section itemscope itemtype="http://schema.org/Physician" class="result large s100 int{$location->getIntegrationId()}" data-slug="{$user->getSlug()}" data-index="{$index + $search->getStartIndex() + 1}">
    <div class="wrapper">
        <header>
            <div class="map-pin">{$index + $search->getStartIndex() + 1}</div>
            <div class="title">
                <h2 class="name">
                    <span itemprop="name">
                        <span>{$user->getName()}</span>
                    </span>
{* In the location profile, the doc tiles should contain the specialty of the doc *}
{* The search page should not display the specialty *}
{if $additional_body_classes == "location"}
                    <a href="{$user->getUrlWithFilter($filter)}">
                        <span class="title-specialty" itemprop="medicalSpecialty">
                        ({if $user->hasPreferredMedicalSpecialtyText()}{$user->getPreferredMedicalSpecialtyText()}{else}{$user->getMedicalSpecialityIdsText(', ')}{/if})
                        </span>
                    </a>
{/if}
                </h2>
                {if $location->hasPhoneVisible()}
                <span class="phone" itemprop="telephone">{$location->getPhoneVisible()}</span>
                {/if}
            </div>
            <p itemprop="address" itemscope itemtype="http://data-vocabulary.org/Address" class="address">
            {* Geo info - SEO readable *}
            <span class="microdata-hidden" itemprop="geo" itemscope itemtype="http://schema.org/Place">
          <span itemscope itemtype="http://schema.org/GeoCoordinates">
            <span property="latitude" content="{$location->getLat()}"></span>
            <span property="longitude" content="{$location->getLng()}"></span>
          </span>
        </span>
                {* Hide the distance on location profile (it's always 0) *}
                {if !isset($hideDistance) || $hideDistance !== true}
                    {floatval($providerRow->getDistance())|number_format:1:",":"."}km |
                {/if}
                <span itemprop="address" itemscope itemtype="http://data-vocabulary.org/Address">
          <span class="street-address" itemprop="streetAddress">{$location->getStreet()}</span>
          <span class="zip-city">
            <span class="zip" itemprop="postalCode">{$location->getZip()}</span>
            <span class="city" itemprop="addressLocality">{$location->getCity()}</span>
          </span>
        </span>
            </p>
        </header>

        <div class="photo">
            <img itemprop="photo" src="{$user->getProfileAssetUrl($smarty.const.ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED)}" alt="{$user->getName()}">
        </div>

        {assign var="appointmentsBlock" value=$providerRow->getAppointmentsBlock($date_start, $date_end, $filter.insurance_id, $filter.medical_specialty_id, $filter.treatmenttype_id)}
        {include file="appointment/_appointments-block-calendar.tpl"}

    </div>{* wrapper *}
</section>
