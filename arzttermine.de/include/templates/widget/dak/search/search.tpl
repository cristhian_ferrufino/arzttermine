{assign var="template_body_class" value="search"}
{assign var="additional_body_classes" value="search_dak"}
{assign var="additional_body_classes" value="dak"}

{include file="header.tpl"}

<section id="search">
    {* The map container *}
    <div id="search-map"></div>
  {*  <div class="search-map-shadow top"></div>
    <div class="search-map-shadow right"></div>
    <div class="search-map-shadow blocker"></div>*}

    <button id="map-toggler">Karte umschalten</button>

    {* Placeholder for the map markers popups *}
    {include file="search/_map-popup.tpl"}
    {* END - Placeholder for the map markers popups *}
</section>

<section id="results-list" itemscope itemtype="http://schema.org/SearchResultsPage">
    
    {* Search filters panel *}
    {if !isset($hideFilters)}
    {include file="search/_filters.tpl"}
    {/if}
    {* END - Search filters panel *}

    {* HEADLINE *}
    {if !isset($hideFilters)}
        {if $search->getResultCount()|count}
            <div class="headline">
                <h1 itemprop="headline">
                    {if $search->getResultCount() < 1}
                        Kein teilnehmender
                        <span class="specialty" itemprop="keywords">{$search->getMedicalSpecialty()->getName(true)}</span>
                        in
                        <span class="location" itemprop="keywords">{$form.location}{if isset($district)} ({$district->getName()}){/if}</span>
                        hat Termine frei.
                    {else}
                        <span class="docs-count" itemprop="headline">{$search->getResultCount()}</span>
                        <span class="specialty" itemprop="keywords">{$search->getMedicalSpecialty()->getName(false)}</span>
                        {$this->_('in')}
                        <span class="location" itemprop="keywords">{$form.location}{if isset($district)} ({$district->getName()}){/if}</span>
                        {$this->_('haben Termine für Sie')}.
                    {/if}
                </h1>
                {* Pagination *}
                <div class="pagination">
                    {paginate
                    count=$search->getResultCount()
                    ms=$search->getMedicalSpecialtyId()
                    tt=$search->getTreatmentTypeId()
                    insurance=$search->getInsuranceId()
                    page=$search->getPage()
                    per_page=$search->getPerPage()
                    lat=$search->getLat()
                    lng=$search->getLng()
                    location=$form.location
                    distance=$search->getRadius()
                    district=$form.district_id
                    }
                </div>
            </div>
        {else}
            <h1 itemprop="headline">
                Aktuell sind in dieser Region keine Ärzte bei uns angemeldet.
                Wir arbeiten daran unser Angebot in Kürze auch bei Ihnen anzubieten.
            </h1>
            <div class="messagebox">
                <p>Wir möchten Ihnen gerne helfen den passenden Arzt zu finden.</p>
                <p>Besuchen Sie dazu bitte die Seite des telefonischen Terminservices unter folgendem Link: <a href="https://www.dak.de/dak/leistungen/Hotlines-1077290.1077312.html" target="_blank">https://www.dak.de/dak/leistungen/Hotlines-1077290.1077312.html</a></p>
            </div>
        {/if}
    {/if}
    {* END - HEADLINE *}

    {* @todo: [NM] ask Axel why we need this here *}
    <div id="search-location" data-lat="{$search->getLat()}" data-lng="{$search->getLng()}" data-name="Suchstandort"></div>

    {* SEARCH RESULTS *}
    <div id="providers" data-date-start="{$search->getStart()->getDate()}" data-date-end="{$search->getEnd()->getDate()}" data-date-days="{$search->getDayCount()}">
        <div id="providers-wrapper" style="100%">
            {foreach from=$results item=providerRow name=searchForeach}
                {assign var="index" value=$smarty.foreach.searchForeach.index}
                {include file="search/provider.tpl"}
            {/foreach}
        </div>
    </div>
    {* END - SEARCH RESULTS *}

    {* Pagination *}
    <div class="pagination justified-blocks">
        {paginate
        count=$search->getResultCount()
        ms=$search->getMedicalSpecialtyId()
        tt=$search->getTreatmentTypeId()
        insurance=$search->getInsuranceId()
        page=$search->getPage()
        per_page=$search->getPerPage()
        lat=$search->getLat()
        lng=$search->getLng()
        location=$form.location
        distance=$search->getRadius()
        district=$form.district_id
        }
    </div>

    <script>
        {if isset($MedicalSpecialityAndTreatmentTypesMap)}
        var mapSpecialtyTreatment = {$MedicalSpecialityAndTreatmentTypesMap};
        {/if}
    </script>

</section>{* #results-list *}
{include file="footer.tpl"}

</section>{* #results-list *}

{include "js/maps.js.tpl"}

{* Used to overlay popups, like the "more appointments" <div> *}
<div class="layer"></div>

</body>
</html>
