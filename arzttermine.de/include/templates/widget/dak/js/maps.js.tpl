<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyA-cGD6Wm_86W_BzBctDnMMD5j7iM0is1I"></script>

<script type="text/javascript" src="/static/js/search.js" charset="utf-8"></script>
<script type="text/javascript" src="/static/js/vendor/infobox.min.js" charset="utf-8"></script>
<script type="text/javascript" src="/static/js/vendor/marker.min.js" charset="utf-8"></script>
<script type="text/javascript" src="/static/js/map.js" charset="utf-8"></script>

<script>
    var mapMarkerApiUrl = "{$marker_url}";
</script>

{* ==== OVERRIDE Maps.placeMarkers method NOT to load zillion markers as on the default search page; pagination ======*}
<script type="text/javascript" src="/static/js/widget/dak/dak.js" charset="utf-8"></script>
