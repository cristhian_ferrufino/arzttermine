{strip}
{assign var="template_body_class" value="two-cols-with-form"}
{assign var="additional_body_classes" value="booking confirmation"}
{include file="header.tpl"}
{/strip}

<div class="container">
  <main>

    <section id="info-block">
      <h3>Angefragter Termin</h3>
      <div class="orange-bar">
    {if
      false == $booking->hasAppointmentEnquiryIntegration()
      &&
      $booking->hasValidAppointmentStartAt()
    }
      {calendar action="getWeekdayDateTimeText" datetime=$booking->getAppointmentStartAt() short=false}
    {else}
      <p>
        {$app->_('Vielen Dank für Ihre Anfrage, wir werden uns innerhalb der nächsten Stunden mit freien Terminen bei Ihnen melden.')}
      </p>
    {/if}
      </div>
      <div class="doctor">
        <div class="photo">
          <img src="{$user->getProfileAssetUrl($smarty.const.ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED)}" alt="{alt($user->getName())}">
        </div>
        <div class="info">
          <div>
            <div class="conjunction">bei</div>
            <div class="name">
              {$user->getName()}
            </div>
          </div>
          <div class="address">
            <h4>Adresse</h4>
            <div class="practice-name">{$location->getName()}</div>
            <div class="street">{$location->getAddress()}</div>
            <div class="city"></div>
          </div>
        </div>
      </div>
    </section>

    <section id="booking-confirmation">
      {strip}
      <h3>{if $booking->hasAppointmentEnquiryIntegration()}
        {$app->_('Ihre Anfrage war erfolgreich')}
        {else}
        {$app->_('Ihre Buchung war erfolgreich')}
        {/if}
      </h3>
      {/strip}
      <section>
        <h4>Sie brauchen nichts weiter zu tun.</h4>
        <span>Wir werden in Kürze Ihren Wunschtermin per E-Mail bestätigen. Die Terminbuchung ist erst durch diese Bestätigung vollendet.</span>
        {if $booking->showICSLinkOnBookingConfirmationPage()}
        <a href="{$booking->getUrl(true, true)}?ics" class="icon-pre ico-calendar">Termin in meinen Kalender importieren</a>
        {/if}
      </section>

      <section>
        <h4>Haben Sie noch Fragen?</h4>
        <p>Wir sind 24 Stunden kostenfrei für Sie erreichbar:</p>
        <a href="tel:08002222133">0800 2222 133</a>
      </section>

    </section>
  </main>
</div>

{include file="footer.tpl"}
