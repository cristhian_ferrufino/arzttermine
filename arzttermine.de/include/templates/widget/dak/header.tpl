<!DOCTYPE html>
<html>
<head>
  <title>{$this->getHeadTitle()}</title>

  <link rel="shortcut icon" href="/static/images/assets/favicon.ico">
  
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
  <meta name="format-detection" content="telephone=no">

  {* 
    Way to skip the cache when publishing new CSS. This should be changed when any css file is changed. 
    Convention MMDD-number, where number must be different than the previous one.
  *}

  {assign var="cacheVersion" value="2807-0"}
  <link rel="stylesheet" type="text/css" href="/static/css/common.min.css?cv={$cacheVersion}">

  {*==== DAK specific overrides ======================================================================================*}
  <link rel="stylesheet" type="text/css" href="/static/css/widget/dak/search_dak.css?cv={$cacheVersion}">
  <link rel="stylesheet" type="text/css" href="/static/css/widget/dak/dak.css?cv={$cacheVersion}">
  <link rel="stylesheet" type="text/css" href="/static/css/widget/dak/two-cols-with-form_dak.css?cv={$cacheVersion}">
  {*==== END - DAK specific overrides ================================================================================*}

  {literal}
  <script type='text/javascript'>
    var AT = AT || {};

    AT.q=[];
    AT._defer=function(f){
      AT.q.push(f);
    };
  </script>
  {/literal}
    
  <!--[if lte IE 9]>
    <script src="/static/js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript">
      document.createElement("header");
      document.createElement("nav");
      document.createElement("aside");
      document.createElement("main");
      document.createElement("article");
      document.createElement("section");
      document.createElement("footer");
    </script>
    <style>
      .search > header {
        position: fixed;
        top: 0;
      }
      .two-cols-with-form form .form-element.error:after {
        margin-top: 10px;
      }
    </style>
  <![endif]-->
  
  <!--[if gte IE 9]><!-->
  <link href='//fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>
  <link href='//fonts.googleapis.com/css?family=Gudea:100,300,400,700,900' rel='stylesheet' type='text/css'>
  <!--<![endif]-->

  <meta name="keywords" content="{$this->getMetaTag('keywords')}" />
  <meta name="description" content="{$this->getMetaTag('description')}" />

</head>

{* Microdata setup *}
{if $template_body_class == "search"}
  {assign var="webPageType" value="SearchResultsPage"}
{elseif $template_body_class == "profile-page"}
  {assign var="webPageType" value="ProfilePage"}
{/if}

{* Alert bar setup *}
{assign var="hasAlert" value=($this->hasStatusMessages())}
<body class="{$template_body_class} {$additional_body_classes} {if $hasAlert}has-alert{/if}"{if isset($webPageType)} itemscope itemtype="http://schema.org/{$webPageType}"{/if}>
{* The alert bar *}
{include file="_status-message.tpl"}
