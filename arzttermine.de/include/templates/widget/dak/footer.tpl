{* ==== JAVASCRIPT ================================================================*}
<!--[if lte IE 8]>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<![endif]-->

<!--[if gt IE 8]>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<![endif]-->

<!--[if !IE]> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<!-- <![endif]-->
<script>
  $(document).ready(function(){
    $.each(AT.q,function(index,f){
      $(f);
    });
  })
</script>

<script type="text/javascript">
    var apiUrl = "{$CONFIG.URL_API}";
    var widget = 'dak';
</script>

{* DEV JS VERSIONS }{/*}
<script type="text/javascript" src="{$this->getStaticUrl('js/vendor/jquery.formalize.min.js')}"></script>
<script type="text/javascript" src="{$this->getStaticUrl('js/at.js')}"></script>
<script type="text/javascript" src="{$this->getStaticUrl('js/calendar.js')}"></script>

{*<script type="text/javascript" src="{$this->getStaticUrl('js/common.min.js')}"></script>*}

{$this->getJsInclude()}

{* ==== END - JAVASCRIPT ================================================================ *}

</body>
</html>
