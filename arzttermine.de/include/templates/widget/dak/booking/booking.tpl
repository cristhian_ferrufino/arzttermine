{strip}
{assign var="use_minimal_footer" value=true}
{assign var="template_body_class" value="two-cols-with-form"}
{assign var="additional_body_classes" value="booking booking-form"}
{assign var="hasFullHeader" value="false"}
{include file="header.tpl"}
{/strip}

{if $contentMain}
{$contentMain}
{/if}

{* In case of double booking, show a popup. *}
{if isset($patientHasSimilarBooking) && $patientHasSimilarBooking==true}
<div class="overlay-blocker">
  <section class="popup-message double-booking">
    <h1>Achtung!</h1>
    <div class="content">
      <div class="popup-message-icon"></div>
      <h2>Ein ähnlicher Termin existiert bereits</h2>
      <p>Laut unserem System haben Sie bereits eine ähnliche Terminbuchung durchgeführt.</p>
      <p>Bitte wenden Sie sich für weitere Hilfe an unsere <em>kostenlose Kunden-Hotline <a href="tel:{$CONFIG.PHONE_SUPPORT_PLAIN}">{$CONFIG.PHONE_SUPPORT}</a>.</em></p>
      <p>Vielen Dank!</p>
    </div>
  </section>
</div>


{* If the patient is blacklisted, show a popup. *}
{elseif isset($blackListPatient) && $blackListPatient==true}
<div class="overlay-blocker">
  <section class="popup-message bp-1">
    <h1>Achtung!</h1>
    <div class="content">
      <div class="popup-message-icon"></div>
      <h2>Die Buchung ist leider fehlgeschlagen!</h2>
      <p>
      {* If there's custom message, use it. Otherwise, use the default one. *}
      {if isset($blackListPatientMessage) && !empty($blackListPatientMessage)}
        {$blackListPatientMessage}
      {else}
        Leider konnte Ihr Termin nicht gebucht werden. Für mehr Informationen wenden Sie sich bitte an unseren Support und geben Sie den entsprechenden Fehlercode an.
      {/if}
      </p>
      <p><em>Kostenlose Telefonnumer Support: <a href="tel:{$CONFIG.PHONE_SUPPORT_PLAIN}">{$CONFIG.PHONE_SUPPORT}</a></em></p>
      <p>Nennen Sie uns den Fehlercode: <em>BP-1</em></p>
      <p>Vielen Dank!</p>
      <button></button>
    </div>
  </section>
</div>

{/if}

<script type="text/javascript" src="/static/js/validation.js" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="/static/js/vendor/chosen/chosen.min.css">

<script type="text/javascript">
var AT = AT || {};
AT._defer(function(){

  var $form = $('#form form');
  AT.validation.monitor($form);

  $form.on('AT:form-validation:errors-found', function() {
    $form.attr("data-step2-error-count", $("#step-2 .form-element.error").length);
    if ($("#step-1").has(".form-element.error").length > 0) {
      $("#go-to-step1").click();
    }
  });

  $("#form #step-1 button").click(function() {
      // $("#form .step").hide();
      $("#form #step-1").addClass("inactive");
      $("#form #step-2").addClass("active");
      $("#steps li").removeClass("active");
      $("#steps li").eq(0).addClass("done");
      $("#steps li").eq(1).addClass("active");
    });

  $("#go-to-step1").click(function(){
      $("#form #step-1").removeClass("inactive");
      $("#form #step-2").removeClass("active");
    });

  // If there's value in the Gutschein field, add .referral to the form (which shows the Gutschein checkbox).
  $('#form input[name*=referral_code]').on('change paste keyup', function() {
    if($(this).val().length > 0) {
      $('#form form').addClass('referral');
      $('#form form').find("input[name*=referral_code_confirmation]").removeAttr('disabled');
    } else {
      $('#form form').removeClass('referral');
      $('#form form').find("input[name*=referral_code_confirmation]").attr('disabled', '');
    }
  });

  $("#booking-form-first-step").on('submit', function() {
    return false;
  });

  // DAK want's each step to be validated separately. So, the original form is split into two form elements.
  // On submit of the second one, if there were no validation errors, the input data from the first form element is 
  // merged with the input data of the second form element.
  $("#booking-form").on('submit', function() {
    if ($(this).hasClass('error')) {
      return false;
    } else {
      $('#booking-form-first-step :input').not(':submit').clone().hide().appendTo($(this));
    }
  });

  $(".overlay-blocker button").click(function(){
    $(".overlay-blocker").remove();
  });
});
</script>

{include file="footer.tpl"}
