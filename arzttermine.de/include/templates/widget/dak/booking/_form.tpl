{* 
  @todo: it would be probably good if we can copy the logic for triggering the errors from the live version 
  it should do the job for now, and i'll revisit to improve it: client side validation, immediate check for
  double or blacklisted bookings etc.
*}
{* ouch, fix the error message hadling later *}
{if !isset($error_messages)}{$error_messages = []}{/if}
{if $error_messages|@count}
  {assign var="error_class" value="error"}
{else}
  {assign var="error_class" value=""}
{/if}
{assign var="isInt89" value=$location->getIntegrationId() == 9 || $location->getIntegrationId() == 8}

<form id="booking-form-first-step" class="{$error_class} {if $isInt89}int89{/if}" enctype="application/x-www-form-urlencoded" >
  <div id="step-1" class="step">
    <div class="form-element {if in_array('gender', $error_messages)}{$error_class}{/if}" data-error-message="Bitte wählen Sie Ihre Anrede aus.">
      <div id="gender" class="radio-group">
        <h4>{$app->_('Anrede')}</h4>
        <input name="form[gender]"  type="radio" id="gender-1" value="1" required><label for="gender-1">Frau</label>
        <input name="form[gender]"  type="radio" id="gender-2" value="0" required><label for="gender-2">Herr</label>
      </div>
      <div class="error-notification">Bitte wählen Sie Ihre Anrede aus.</div>
    </div>
    <div class="form-element {if in_array('first_name', $error_messages)}{$error_class}{/if}" data-error-message="Bitte geben Sie Ihren Vornamen ein.">
      <label>{$app->_('Vorname')}</label>
      <input type="text" name="form[first_name]" value="{$form.first_name}" required>
      <div class="error-notification">Bitte geben Sie Ihren Vornamen ein.</div>
    </div>
    <div class="form-element {if in_array('last_name', $error_messages)}{$error_class}{/if}" data-error-message="Bitte geben Sie Ihren Nachnamen ein.">
      <label>{$app->_('Nachname')}</label>
      <input type="text" name="form[last_name]" value="{$form.last_name}" required>
      <div class="error-notification">Bitte geben Sie Ihren Nachnamen ein.</div>
    </div>
    <div class="form-element {if in_array('email', $error_messages)}{$error_class}{/if}" data-error-message="Bitte geben Sie Ihre E-Mail Adresse ein.">
      <label>{$app->_('E-Mail')}*</label>
      <input type="text" name="form[email]" value="{$form.email}" required>
      <div class="error-notification">Bitte geben Sie Ihre E-Mail Adresse ein.</div>
    </div>
    <div class="form-element {if in_array('phone', $error_messages)}{$error_class}{/if}" data-error-message="Bitte geben Sie eine gültige Telefonnummer ein.">
      <label>{$app->_('Mobil')}*</label>
      <input type="text" name="form[phone]" value="{$form.phone}" required>
      <div class="error-notification">Bitte geben Sie eine gültige Telefonnummer ein.</div>
    </div>
    <p class="clarification">
      *) Wir benötigen Ihre E-Mailadresse und Telefonnummer um Ihnen die Terminbestätigung zukommen zu lassen.
    </p>
    <div class="button-wrapper"><button>Weiter</button></div>
  </div>
</form>
<form id="booking-form" class="{$error_class} {if $isInt89}int89{/if}" action="{$this->url_form_action}" method="post" enctype="application/x-www-form-urlencoded" >
  <div id="step-2" class="step">
    <button id="go-to-step1" type="button"></button>
    <div class="form-element {if in_array('treatmenttype_id', $error_messages)}{$error_class}{/if}">
      <label>{$app->_('Behandlungsgrund')} <span class="optional-field">(freiwillig)</span></label>
      <select id="filter-treatment" name="form[treatmenttype_id]" >
        {html_options options=$treatmenttypes  selected=$form.treatmenttype_id}
      </select>
    </div>
    <div class="form-element {if in_array('returning_visitor', $error_messages)}{$error_class}{/if}" data-error-message="Bitte geben Sie an, ob Sie schon einmal bei diesem Arzt in Behandlung waren.">
      <div id="is-patient" class="radio-group">
        <h4>Waren Sie bereits Patient/in bei {$user->getName()}?</h4>
        <input name="form[returning_visitor]" type="radio" id="is-patient-2" value="0"{if $form.returning_visitor == 2} checked{/if} required><label for="is-patient-2">{$app->_('Nein')}</label>
        <input name="form[returning_visitor]" type="radio" id="is-patient-1" value="1"{if $form.returning_visitor == 1} checked{/if} required><label for="is-patient-1">{$app->_('Ja')}</label>
      </div>
      <div class="error-notification">Bitte geben Sie an, ob Sie schon einmal bei diesem Arzt in Behandlung waren.</div>
    </div>
    
{* 
	Temporarily adding this condition, Axel suggests making a method in this template. 
*}
    {if $location->getIntegrationId() == 9 || $location->getIntegrationId() == 8}
    <div class="form-element">
      <label>{$app->_('Haben Sie noch einen Hinweis für uns?')}<span class="optional-field"> (freiwillig)</span></label>
      <textarea name="form[comment_patient]">{$form.comment_patient}</textarea>
    </div>
    {/if}

{* 
	Temporarily adding this condition, Axel suggests making a method in this template. 
*}
    {if $location->getIntegrationId() == 9 || $location->getIntegrationId() == 8}
    <div class="form-element">
      <input id="form_flexible_location" type="checkbox" name="form[flexible_location]" value="1"{if $form.flexible_location=='1'} checked="checked"{/if}>
      <label for="form_flexible_location">{$app->_('Ich benötige den Termin dringend und bin mit einer Ausweichpraxis in der Nähe ebenfalls einverstanden.')}</label>
    </div>
    {/if}

    <div class="form-element {if in_array('agb', $error_messages)}{$error_class}{/if}" data-error-message="Bitte bestätigen Sie die Einwilligung.">
      <input type="checkbox" id="checkbox-tos" name="form[agb]" value="1" required>
      <label for="checkbox-tos">{$booking_agb_confirmation_text}</label>
      <div class="error-notification">Bitte bestätigen Sie die Einwilligung.</div>
    </div>
      
    <input type="hidden" name="form[insurance_id]" value="{$smarty.const.INSURANCE_TYPE_PUBLIC}">
    <input type="hidden" name="form[insurance_provider_id]" value="3">{* DAK *}
    <input type="hidden" name="form[location_id]" value="{$form.location_id}">
    <input type="hidden" name="form[user_id]" value="{$form.user_id}">
    <input type="hidden" name="form[medical_specialty_id]" value="{$form.medical_specialty_id}">
    <input type="hidden" name="form[start_at_id]" value="{$form.start_at_id}">
    <input type="hidden" name="form[end_at_id]" value="{$form.end_at_id}">
    <input type="hidden" name="form[form_once]" value="{$form.form_once}">

    <div class="button-wrapper"><button type="submit">Termin buchen!</button></div>
  </div>
</form>   

