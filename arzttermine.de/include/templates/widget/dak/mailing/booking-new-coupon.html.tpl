{include file="widget/dak/mailing/header.html.tpl"}
<p><strong>{$booking_salutation_full_name},</strong></p>
<p>vielen Dank für Ihre Terminanfrage über unsere aktuelle Gutscheinaktion!</p>
<p>Mit dieser E-Mail bestätigen wir Ihnen den Empfang Ihrer Buchungsanfrage für</p>
<center>
    <table width="314">
        <tr>
            <td style="background:url('http://www.arzttermine.de/static/img/newsletter/termin_bg.jpg'); width:314px; height: 151px;">
                <table height="151px" cellspacing="1px" style="padding-left: 17px;">
                    <tr>
                        <td height="30px" style="font-weight: bold; color: #fff;font-family: Helvetica;font-size: 14px;text-shadow: 0 1px 0 #2f738d;">Buchungsanfrage</td>
                    </tr>
                    <tr>
                        <td height="53px" valign="bottom"><strong><span style="font-family: Helvetica; color: #555; font-size: 14px;">{$user_name}</span></strong>
                            <br/><span style="font-family: Helvetica; color: #313131; font-size: 14px;">{$location_address}</span></td>
                    </tr>
                    <tr>
                        <td><span style="font-family: Helvetica; color: #313131; font-size: 14px;">am <strong>{$appointment_date} um {$appointment_time} Uhr.</strong></span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</center>

<p>
    Zur Einlösung des Gutscheins beachten Sie bitte unsere Teilnahmebedinungen.<br />
    Sie erhalten den Gutschein nach Ihrem wahrgenommenen Termin ausschließlich über Arzttermine.de und nicht in der jeweiligen Praxis.<br />
    Bitte beachten Sie, dass aus organisatorischen Gründen der Versand des Gutscheins 5 bis 6 Wochen in Anspruch nehmen kann.<br />
    Ein wahrgenommener Termin ist die Vorraussetzung für den Versand des Gutscheins.
</p>
<p>
    <strong>Unser Patientensupport wird so schnell wie möglich mit Ihnen Kontakt aufnehmen</strong>
    <span style="font-style:italic">(Montag bis Freitag zwischen 08:00 und 18:00 Uhr)</span>.
    Wir möchten damit Ihren Besuch in der Praxis so angenehm und einfach wie möglich gestalten.
    <br/>
    <br/>Sollte Ihr Termin an einem Montag stattfinden, werden unsere Servicemitarbeiter Sie
    Montag ab 8 Uhr zur Rückbestätigung kontaktieren. Bitte haben Sie bis dahin ein wenig Geduld.
</p>

{include file="widget/dak/mailing/footer.html.tpl"}