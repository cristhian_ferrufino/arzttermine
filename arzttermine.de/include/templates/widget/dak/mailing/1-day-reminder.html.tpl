{include file="widget/dak/mailing/header.html.tpl"}
<p><strong>{$booking_salutation_full_name},</strong></p>

<p>bitte denken Sie an Ihren Arzttermin morgen den {$appointment_date} um {$appointment_time} Uhr bei {$user_name}.</p>
<p>Die Adresse der Praxis lautet:</p>
<p>{$location_address}</p>

{include file="widget/dak/mailing/_contact_us_for_cancelation.html.tpl"}

{include file="widget/dak/mailing/footer.html.tpl"}