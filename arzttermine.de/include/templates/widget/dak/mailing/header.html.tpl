<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html style="width: 100%; height: 100%">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body style="width: 100%; height: 100%" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="background-color: #e5e5e5; background-image: -webkit-linear-gradient(bottom, #e8e8e8 0%, #fff 100%); background-image: -moz-linear-gradient(bottom, #e8e8e8 0%, #fff 100%); background-image: -ms-linear-gradient(bottom, #e8e8e8 0%, #fff 100%); background-image: -o-linear-gradient(bottom, #e8e8e8 0%, #fff 100%); ">
    <tr>
        <td>
            <table style="margin: 0 auto"  border="0" cellpadding="0" cellspacing="0" width="600">
                <tr>
                    <td valign="top">
                        <table height="114" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td width="250">
                                    <img src="http://{$domain}/static/images/assets/widget/dak/DAK_Logo-1-1231220.png" width="118" height="63" alt="DAK" style="-ms-interpolation-mode:bicubic;">
                                </td>
                                <td align="right" width="350" style="color:#313131;font-size:15px;font-family:Helvetica,Helvetica,Arial,sans-serif;" valign="middle">
                                    <br>
                                    <strong><span>Support-Hotline</span>
                                            <span>
                                                <a href="tel:08002222133" style="color:#FF6600;text-decoration:none">
                                                    0800 / 222 2 133
                                                </a>
                                            </span>
                                    </strong><span style="font-size:13px">(Kostenfrei)</span>
                                </td>
                            </tr>
                        </table>
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="font-family: Helvetica; color: #555; font-size: 14px;padding:35px; border: 1px solid #ccc; /* stroke */border-radius: 9px 9px 0px 0px; -webkit-border-radius: 9px 9px 0px 0px;-moz-border-radius: 9px 9px 0px 0px; background-clip: padding-box; background-color: #fff; box-shadow: 0 0 5px rgba(0,0,0,.1), inset 0 0 0 1px #fff; ">
                            <tr>
                                <td valign="top">
