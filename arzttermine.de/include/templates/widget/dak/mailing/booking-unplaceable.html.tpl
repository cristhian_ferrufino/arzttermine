{include file="widget/dak/mailing/header.html.tpl"}

<p>Leider konnte Ihr Termin nicht wie gewünscht bestätigt werden.</p>
<p>
    Für die Umstände bitten wir um Entschuldigung und hoffen,
    dass Sie unseren Service zukünftig weiterhin in Anspruch nehmen möchten.
</p>

{include file="widget/dak/mailing/footer.html.tpl"}