{include file="widget/dak/mailing/header.html.tpl"}
<p><strong>{$booking_salutation_full_name},</strong></p>
<p>vielen Dank für Ihre Terminbuchung über die DAK!</p>
<p>Mit dieser E-Mail bestätigen wir Ihnen die erfolgreiche Buchung Ihres Termins.</p>
<center>
    <table width="314">
        <tr>
            <td style="background:url('http://{$domain}/static/img/newsletter/termin_bg.jpg'); width:314px; height: 151px;">
                <table height="151px" cellspacing="1px" style="padding-left: 17px;">
                    <tr>
                        <td height="30px" style="font-weight: bold; color: #000;font-family: Helvetica;font-size: 14px;text-shadow: 0 1px 0 #2f738d;">Termin und Praxisinformationen</td>
                    </tr>
                    <tr>
                        <td height="53px" valign="bottom"><strong><span style="font-family: Helvetica; color: #555; font-size: 14px;">{$location_name}</span></strong>
                            <br/><span style="font-family: Helvetica; color: #555; font-size: 14px;">{$location_address}</span></td>
                    </tr>
                    <tr>
                        <td><span style="font-family: Helvetica; color: #555; font-size: 14px;">am <strong>{$appointment_date} um {$appointment_time} Uhr.</strong></span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</center>

{include file="widget/dak/mailing/_contact_us_for_cancelation.html.tpl"}

{include file="widget/dak/mailing/footer.html.tpl"}