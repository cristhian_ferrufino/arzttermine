{include file="widget/dak/mailing/header.html.tpl"}
<p><strong>vielen Dank für Ihre Terminbuchung über die DAK!</strong></p>

<p>Zu Ihrem Termin hat sich eine wichtige Rückfrage ergeben.</p>
<p>Bitte rufen Sie  uns zwischen 7 - 20 Uhr unter der kostenfreien
    Rufnummer 0800 22 22 133 (0 cent/ Minute aus dem Fest- und Mobilfunknetz) zurück.</p>

{include file="widget/dak/mailing/footer.html.tpl"}