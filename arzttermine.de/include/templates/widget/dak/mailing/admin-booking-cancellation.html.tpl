{include file="widget/dak/mailing/header.html.tpl"}

Bitte prüfen!

{$gender} {$last_name} hat im Patientenaccount einen Termin storniert:

Arzt: {$doctor_name}
Praxis: {$practice_name}
Termin: {$appointment_date}
BookingID: {$booking_id}

{include file="widget/dak/mailing/footer.html.tpl"}