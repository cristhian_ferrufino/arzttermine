{include file="widget/dak/mailing/header.html.tpl"}
<p><strong>Vielen Dank für Ihre Anfrage über die DAK.</strong></p>

<p>
    Leider haben wir Sie unter der von Ihnen angegebenen Rufnummer nicht erreichen können,
    vermutlich hat sich ein kleiner Fehler eingeschlichen.
</p>

<p>
    Wir möchten Sie daher um eine kurze Rückmeldung bitten, um eventuelle Rückfragen
    bezüglich Ihrer Behandlung zu besprechen und somit Ihren Arzttermin so einfach und angenehm
    wie möglich gestalten zu können.
    Ferner ist Ihre Rufnummer nötig, damit die von Ihnen ausgewählte Praxis Sie im Notfall erreichen kann.
</p>

<p>
    Bitte antworten Sie daher einfach kurz auf diese Email oder rufen Sie uns kostenfrei unter der Nummer 0800 22 22 133 an.
</p>

{include file="widget/dak/mailing/footer.html.tpl"}