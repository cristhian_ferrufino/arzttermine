{include file="widget/dak/mailing/header.html.tpl"}
<p><strong>vielen Dank für Ihre Terminbuchung über die DAK!</strong></p>

<p>
    Die Praxis hat uns eine Verschiebung Ihres Termins um wenige Minuten auf
    {$appointment_date} um {$appointment_time} Uhr
    gemeldet.
</p>
{include file="widget/dak/mailing/_contact_us_for_cancelation.html.tpl"}

{include file="widget/dak/mailing/footer.html.tpl"}