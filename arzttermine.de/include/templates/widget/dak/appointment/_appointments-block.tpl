{if $appointmentsBlock.type == 'appointments'}
  {assign var="appointmentDays" value=$appointmentsBlock.days}
  {foreach from=$appointmentDays item="day" key="date"}
  <div class="col-date {if $day.day_text == "Mo"}week-start{/if}" data-date="{$date}">
    <div class="col-header">
      <span class="day">{$day.day_text}</span>
      <span class="date">{$day.date_text}</span>
    </div>

    {assign var="appointments" value=$day.appointments}
    
    {foreach from=$appointments item=appointment}
      {if $appointment@iteration == $CONFIG.CALENDAR_TIMES_SHOW_MORE_LINK_AFTER_HOW_MANY_TIMES 
        && $appointments|@count > $CONFIG.CALENDAR_TIMES_SHOW_MORE_LINK_AFTER_HOW_MANY_TIMES}
      <div class="appointment more">
        <div>mehr</div>
        <div class="appointments">
      {/if}
      <a class="appointment" href="{$appointment.url}" rel="nofollow">{$appointment.time_text}</a>
    {/foreach}

    {if $appointments|@count > $CONFIG.CALENDAR_TIMES_SHOW_MORE_LINK_AFTER_HOW_MANY_TIMES}
        </div>
      </div>
    {/if}
  </div>
  {/foreach}

{* NEXT AVAILABLE APPOINTMENT ======================================================================================= *}  
{elseif $appointmentsBlock.type == 'next-available-appointment'}
  {assign var="appointment" value=$appointmentsBlock.appointment}
  {* 
    @todo: 14 is set because it's value for $date_days in other places like search.php or doctor_profile.php 
    consider using it as a config constant!
  *}
  {for $counter=0; $counter < 14; $counter++}
  <div class="col-date">
    <div class="col-header">
      <span class="day"></span>
      <span class="date"></span>
    </div>
  </div>
  {/for}

{* CUSTOM MESSAGE =================================================================================================== *}  
{elseif $appointmentsBlock.type == 'custom-message'}
  {for $counter=0; $counter < 14; $counter++}
  <div class="col-date">
    <div class="col-header">
      <span class="day"></span>
      <span class="date"></span>
    </div>
  </div>
  {/for}

{* NO AVAILABLE APPOINTMENTS ======================================================================================== *}
{elseif $appointmentsBlock.type == 'no-available-appointments'}
  {for $counter=0; $counter < 14; $counter++}
  <div class="col-date">
    <div class="col-header">
      <span class="day"></span>
      <span class="date"></span>
    </div>
  </div>
  {/for}

{* NO SELECTED INSURANCE ============================================================================================ *}
{elseif $appointmentsBlock.type == 'no-selected-insurance'}
  {for $counter=0; $counter < 14; $counter++}
  <div class="col-date">
    <div class="col-header">
      <span class="day"></span>
      <span class="date"></span>
    </div>
  </div>
  {/for}

{* INTEGRATION 8 ==================================================================================================== *}
{elseif $appointmentsBlock.type == 'integration_8'}
  {for $counter=0; $counter < 14; $counter++}
  <div class="col-date">
    <div class="col-header">
      <span class="day"></span>
      <span class="date"></span>
    </div>
  </div>
  {/for}

{/if}
