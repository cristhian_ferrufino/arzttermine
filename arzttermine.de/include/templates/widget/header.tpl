<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>{$widget_title}</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" media="all" href="{$this->getStaticUrl('old-css/reset-min.css')}" />
	<link rel="stylesheet" type="text/css" media="all" href="{$this->getStaticUrl('old-css/base-min.css')}" />
	<link rel="stylesheet" type="text/css" media="all" href="{$this->getStaticUrl('old-css/fonts-min.css')}" />
	<link rel="stylesheet" type="text/css" media="all" href="{$this->getStaticUrl('old-css/main.css',false,true)}" />
	<script type="text/javascript" src="{$this->getStaticUrl('jquery/jquery-1.7.min.js')}"></script>

{$widget->getHtmlHead()}

</head>
<body>
{include file="js/tagmanager/tagmanager.tpl"}
