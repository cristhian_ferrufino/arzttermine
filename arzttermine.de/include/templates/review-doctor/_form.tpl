{if false}
    {assign var="error_class" value="error"}
{else}
    {assign var="error_class" value=""}
{/if}

<form id="review-form" class="{$error_class}" action="{$url_form_action}" method="post" enctype="application/x-www-form-urlencoded" >
    <div class="form-element {$error_class}" data-error-message="Bitte geben Sie einen Namen ein, der mind. 3 Buchstaben lang ist.">
        <label>{$app->_('Ihr Name')}</label>
        <input type="text" name="form[patient_name]" value="{$form.patient_name}">
        {$form.patient_name__message}
    </div>

    <div class="form-element {$error_class}" data-error-message="Bitte beschreiben Sie Ihre Erfahrungen bei diesem Arzt.">
        <label>{$app->_('Beschreiben Sie Ihre Erfahrungen')}</label>
        <small>
            Bitte beschreiben Sie Ihre Erfahrungen bei diesem Arzt.
            <br>
            Was war gut? Wie war die Behandlung? Mussten Sie warten? Haben Sie die Arztpraxis gut erreicht?
        </small>
        <textarea name="form[rate_text]">{$form.rate_text}</textarea>
    </div>

    <div class="form-element {$error_class}" data-error-message="Bitte bewerten Sie diese Eigenschaft.">
        <label>Verhalten des Arztes</label>
        <small>Bitte beurteilen Sie das Verhalten des Arztes.</small>
        <div class="star" id="rating_1" data-score-name="form[rating_1]" data-score="{$form.rating_1}"></div>
        {include file="../_rating-input-stars.tpl" name="form[rating_1]"}
        {$form.rating_1__message}
    </div>

    <div class="form-element {$error_class}" data-error-message="Bitte bewerten Sie diese Eigenschaft.">
        <label>Wartezeit</label>
        <small>Bitte beurteilen Sie die Wartezeit.</small>
        {include file="../_rating-input-stars.tpl" name="form[rating_2]"}
        {$form.rating_2__message}
    </div>

    <div class="form-element {$error_class}" data-error-message="Bitte bewerten Sie diese Eigenschaft.">
        <label>Gesamtbewertung</label>
        <small>Bitte beurteilen Sie Ihren Gesamteindruck.</small>
        {include file="../_rating-input-stars.tpl" name="form[rating_3]"}
        {$form.rating_3__message}
    </div>

    <hr /><br />

    <div class="form-element {$error_class}" data-error-message="Bitte bewerten Sie diese Eigenschaft.">
        <label>Buchungsablauf</label>
        <small>Bitte beurteilen Sie Ihre Erfahrungen mit unserem Service.</small>
        {include file="../_rating-input-stars.tpl" name="form[rating_4]"}
        {$form.rating_4__message}
    </div>

    <div class="form-element {$error_class}" data-error-message="Bitte beschreiben Sie Ihre Erfahrungen mit unserem Service.">
        <label>{$app->_('Beschreiben Sie Ihre Erfahrungen')}</label>
        <small>
            Bitte beschreiben Sie Ihre Erfahrungen mit unserem Service.
            <br>
            Was war gut? Was k&ouml;nnten wir verbessern ?
        </small>
        <textarea name="form[rate_4_text]">{$form.rate_4_text}</textarea>
    </div>

    <div class="form-element {$error_class}">
        <input id="form-allow_sharing" type="checkbox" name="form[allow_sharing]" value="1"{if $form.allow_sharing=='1'} checked="checked"{/if} />
        <label for="form-allow_sharing">{$app->_('Meine Bewertung darf auch auf Partnerseiten von Arzttermine.de anonymisiert publiziert werden.')}</label>
    </div>

    <div class="button-wrapper"><button type="submit">Bewertung abschicken</button></div>
</form>
