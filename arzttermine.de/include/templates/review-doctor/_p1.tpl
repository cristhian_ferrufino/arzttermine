<div class="container">
  <main>
    <section id="info-block">
      <h3>Bewerten Sie diesen Arzt</h3>

      <div class="doctor">
        <div class="photo">
          <img src="{$user->getProfileAssetUrl($smarty.const.ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED)}" alt="{alt($user->getName())}">
        </div>
        <div class="info">
          <div class="name">{$user->getName()}</div>
          <div class="address">
            <h4>Adresse</h4>
            <div class="practice-name">{$location->getName()}</div>
            <div class="street">{$location->getStreet()}</div>
            <div class="city">{$location->getCityZip()}</div>
          </div>
        </div>
      </div>

      <ul class="comfort-elements">
        <li class="review">Helfen Sie anderen Patienten bei der Arztauswahl mit einer sachlichen und fairen Bewertung.</li>
        <li class="data-protection">Wir nutzen Ihre Daten ausschließlich wie in der <a href="/datenschutz">Datenschutzerklärung</a> festgelegt.</li>
      </ul>
   </section>

    <section id="form">
      <h3>Ihre Erfahrungen</h3>
      {include file="review-doctor/_form.tpl"}
    </section>
  </main>
</div>
