{strip}
{assign var="use_minimal_footer" value=true}
{assign var="template_body_class" value="two-cols-with-form"}
{include file="header.tpl"}
{/strip}

{if $contentMain}
{$contentMain}
{/if}

<script type="text/javascript">
AT._defer(function(){
    $("#form *[type=submit]").click(function() {
    	var form = $("#review-form");

        form.removeClass("error");
        form.find(".form-element").removeClass("error");

        var fields = [];
        var i = 0;
        var $name               = fields[i++] = form.find("input[name*=patient_name]");
        var $text               = fields[i++] = form.find("textarea[name*=rate_text]");
        var $rating1            = fields[i++] = form.find("input[name*=rating_1]");
        var $rating2            = fields[i++] = form.find("input[name*=rating_2]");
        var $rating3            = fields[i++] = form.find("input[name*=rating_3]");
        var $rating4            = fields[i++] = form.find("input[name*=rating_4]");

        // Check name (if used)
        if ($name.val().length < 0 || $name.val().length < 3) {
            form.addClass("error");
            $name.parents(".form-element").addClass("error");
        }
        // Check RateText
        if ($text.val().length == 0) {
            form.addClass("error");
            $text.parents(".form-element").addClass("error");
        }
        // Check if ratings are made
        if (!$rating1.is(':checked') || $rating1.val() < 1 || $rating1.val() > 5) {
            form.addClass("error");
            $rating1.parents(".form-element").addClass("error");
        }
        if (!$rating2.is(':checked') || $rating2.val() < 1 || $rating2.val() > 5) {
            form.addClass("error");
            $rating2.parents(".form-element").addClass("error");
        }
        if (!$rating3.is(':checked') || $rating3.val() < 1 || $rating3.val() > 5) {
            form.addClass("error");
            $rating3.parents(".form-element").addClass("error");
        }
        if (!$rating4.is(':checked') || $rating4.val() < 1 || $rating4.val() > 5) {
            form.addClass("error");
            $rating4.parents(".form-element").addClass("error");
        }

        if ( form.hasClass("error") ) {
          return false;
        }
      });
});
</script>

{include file="footer.tpl"}
