{strip}
{assign var="use_minimal_footer" value=true}
{assign var="template_body_class" value="two-cols-with-form"}
{include file="header.tpl"}
{/strip}

<div class="container">
    <main>
        <section id="info-block">
            <h3>Bewerten Sie diesen Arzt</h3>

            <div class="doctor">
                <div class="photo">
                    <img src="{$user->getProfileAssetUrl($smarty.const.ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED)}" alt="{alt($user->getName())}">
                </div>
                <div class="info">
                    <div class="name">{$user->getName()}</div>
                    <div class="address">
                        <h4>Adresse</h4>
                        <div class="practice-name">{$location->getName()}</div>
                        <div class="street">{$location->getStreet()}</div>
                        <div class="city">{$location->getCityZip()}</div>
                    </div>
                </div>
            </div>

            <ul class="comfort-elements">
                <li class="review">Helfen Sie anderen Patienten bei der Arztauswahl mit einer sachlichen und fairen Bewertung.</li>
                <li class="data-protection">Wir nutzen Ihre Daten ausschließlich wie in der <a href="/datenschutz">Datenschutzerklärung</a> festgelegt.</li>
            </ul>
        </section>

        <section id="form">
            <h3>Noch einen Schritt</h3>

            <form id="review-form">
                <div class="form-element">
                    <h6 class="reviewGoogle">{$app->_('Danke für Ihre Bewertung')}!</h6>
                    <br/>
                    Sie möchten lieber einen 20€ Gutschein anstatt des 5€ Gutscheins?<br/><br/>
                    Veröffentlichen Sie dazu nur Ihre Bewertung auf dem Google Profil Ihres Arztes und wir werden Ihnen einen 20€ Amazon Gutschein zusenden.
                </div>
                <div align="right">
                    <img src="//www.arzttermine.de/static/img/mailing/amazon_badge_google.png" width="70%"/>
                </div>
                <br />
                <div class="form-element">
                <h6 class="reviewGoogle">Wie es funktioniert:</h6><br />
                <ul>
                    <li class="bullet">Klicken Sie einfach auf den untenstehenden Button. In einem neuen Fenster wird sich das Formular zur Bewertung auf Google öffnen. Hinterlassen Sie hier Ihre Bewertung erneut und schicken Sie diese ab.</li>
                    <li class="bullet">Sie haben Ihre Bewertung abgeschickt? Arzttermine.de wird nun das Google Profil Ihres Arztes überprüfen und Ihnen den Gutschein zusenden, sobald die Bewertung online ist.</li>
                </ul>
                </div>
                <br />
                <div class="button-wrapper">
                    <a href="{$review}" target="_blank" title="Jetzt bewerten!">
                        <button type="button">Jetzt auf Google bewerten!</button>
                    </a>
                </div>
            </form>
        </section>
    </main>
</div>

{include file="footer.tpl"}
