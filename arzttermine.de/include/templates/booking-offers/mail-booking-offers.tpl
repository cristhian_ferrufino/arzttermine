<table>
{foreach from=$booking_offers item=booking_offer name=booking_offer_foreach}
{assign var="index" value=$smarty.foreach.booking_offer_foreach.index}
<tr><td width="350" height="60" style="background-color: #0090d6; border-radius: 15px; margin: 10px; padding: 10px 10px;"><a style="font-weight:normal; display: block; background: #0090d6; color: white; width: 350px; line-height: 20px; text-align: center; font-family: Helvetica; font-size: 14px; text-decoration:none" href="{$booking_offer->getUrl(true, true)}/1">Bitte klicken Sie hier um den neuen <b>Termin am {$booking_offer->getAppointment()->getWeekdayDateText()} / {$booking_offer->getAppointment()->getTimeText()} zu bestätigen</b></a></td></tr><tr><td height="10" style="background-color: #ffffff;"></td></tr>
{/foreach}
</table>
<br>oder<br><br>
<table><tr><td width="350" height="40" style="background-color: #999999; border-radius: 15px; margin: 10px; padding: 10px 10px;">
<a style="font-weight:normal; display: block; background: #999999; border-radius: 15px; color: white; width: 350px; line-height: 20px; text-align: center; font-family: Helvetica; font-size: 14px; text-decoration:none" href="{$booking->getCancelUrl()}">Klicken Sie bitte hier, wenn Sie diesen <b>Termin ablehnen</b> möchten<br />oder eine neuen Terminvorschlag durch uns wünschen.</a>
</td></tr></table>