{strip}
{assign var="use_minimal_footer" value=true}
{assign var="template_body_class" value="two-cols-with-form"}
{include file="header.tpl"}
{/strip}

<div class="container">
  <main>
    <section id="header">
      <h1>Entschuldigung</h1>
      <p>Leider kann die von Ihnen gewählte Praxis den Termin nicht einhalten. Daher bieten wir Ihnen folgende Terminalternativen an.</p>
    </section>
    <section id="info-block">
      {strip}
      <h3>{if $location->hasAppointmentEnquiryIntegration()}
        {$app->_('Terminanfrage')}
        {else}
        {$app->_('Ihre urspüngliche Terminwahl')}
        {/if}
      </h3>
      {/strip}

      <div class="orange-bar">
      {assign var="appointment" value=$booking->getAppointment()}
      {if $integration_id != 8 && $integration_id != 9}
        <div class="datetime"><span class="light">Am:</span> {$appointment->getDateTimeText()}</div>
      {/if}
      </div>
      <div class="doctor">
        <div class="photo">
          <img src="{$user->getProfileAssetUrl($smarty.const.ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED)}" alt="{alt($user->getName())}">
        </div>
        <div class="info">
          <div class="conjunction">bei</div>
          <div class="name">{$user->getName()}</div>
          <div class="phone">{$location->getPhoneVisible()}</div>
          <div class="address">
            <h4>Adresse</h4>
            <div class="practice-name">{$location->getName()}</div>
            <div class="street">{$location->getAddress()}</div>
            <div class="city"></div>
          </div>
        </div>
      </div>
      <ul class="comfort-elements">
        <li class="cancelation">Sollten sich Ihre Pläne ändern, können Sie den Termin jederzeit stornieren.</li>
        <li class="contact">Unser Supportteam erreichen Sie kostenfrei unter <a href="tel:08002222133">0800/2222133</a></li>
        <li class="data-protection">Wir nutzen Ihre Daten ausschließlich wie in der <a href="/datenschutz">Datenschutzerklärung</a> festgelegt.</li>
      </ul>
    </section>

    <section id="form">
      <h3>Ihre Terminalternativen</h3>
{foreach from=$booking_offers item=booking_offer}
      <ul class="items">
        <li><a href="{$booking_offer->getUrl(true, true)}/1">{$booking_offer->getAppointment()->getWeekdayDateText()} / {$booking_offer->getAppointment()->getTimeText()}</a></li>
      </ul>
{/foreach}
    </section>

  </main>
</div>

{include file="footer.tpl"}
