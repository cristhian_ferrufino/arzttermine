


{* The search page has footer with fewer links than the full version, but more than the minimal version. *}
{assign var="isSearchPage" value=$template_body_class == "search"}

{* 
  ==== MINIMAL FOOTER EXCEPTION ================================================================
  Some (conversion) pages should use the minimal footer so the user has fewer options to leave the page.
*}
{if $use_minimal_footer}
  <div class="footer2">
    <footer>
        {*
          ==== SEARCH PAGE EXCEPTION ================================================================
          In the search page we show only the legally required links.
        *}
        {include file="footer_section2.tpl"}
    </footer>
  </div>

    {*
      ==== NON-MINIMAL FOOTER ================================================================
    *}
{else}
    {if !$isSearchPage}
      <div class="footer1">
        <footer>
          <div class="row">
            <div class="col-sm-7">
              <h4>Sie suchen einen freien Arzttermin?</h4>
              <p>Nach welchem Arzt suchen Sie?</p>
              <div class="row">
                <div class="col">
                  <ul class="footer1">
                    <li class="section1"> <a href="/allergologe" class="footer1">Allergologe</a></li>
                    <li class="section1"> <a href="/allgemeinarzt" class="footer1">Allgemeinarzt / Hausarzt</a></li>
                    <li class="section1"> <a href="/augenarzt" class="footer1">Augenarzt</a></li>
                    <li class="section1"> <a href="/frauenarzt" class="footer1">Frauenarzt / Gynäkologe</a></li>
                    <li class="section1"> <a href="/hautarzt" class="footer1">Hautarzt / Dermatologe</a></li>
                    <li class="section1"> <a href="/kieferorthopaede" class="footer1">Kieferorthopäde</a></li>
                  </ul>
                </div>
                <div class="col">
                  <ul class="footer1">
                    <li class="section1"> <a href="/kinderarzt" class="footer1">Kinderarzt</a></li>
                    <li class="section1"> <a href="/orthopaede" class="footer1">Orthopäde</a></li>
                    <li class="section1"> <a href="/psychosomatiker" class="footer1">Psychosomatiker</a></li>
                    <li class="section1"> <a href="/psychiater" class="footer1">Psychiater</a></li>
                    <li class="section1"> <a href="/urologie" class="footer1">Urologe</a></li>
                    <li class="section1"> <a href="/zahnarzt" class="footer1">Zahnarzt</a></li>
                  </ul>
                </div>
              </div>
              {*<div class="row" style=" margin-top: .5rem;">
                <div class="col">
                  <p style="padding-top: .6rem">Finden Sie einen Arzt in Ihrer Stadt:</p>
                </div>
                <div class="col-sm">
                  <a class="btn-footer1" href="/suche?form%5Bmedical_specialty_id%5D=1&form%5Blocation%5D={$this->getUserCity()}&form%5Binsurance_id%5D=2">{$this->getUserCity()}</a>
                </div>
              </div>*}
              <br/>
              <p>Rufen Sie uns kostenlos an, wir kümmern uns um einen freien Termin:</p>
              <br/>
              <h4><span class="footer1"><a href="tel:08002222133" class="footer1"> 0800 - 22 22 133</a></span></h4>
              <br/>
            </div>
            <div class="col">
              <h4>Sind Sie Arzt?</h4>
              <p>Legen Sie Ihre kostenlose Profilseite an:</p>
              <ul class="footer-1">
                <li class="section-1"> Erhöhte Auffindbarkeit im Internet</li>
                <li class="section-1"> Optimierung Ihrer Praxisauslastung</li>
                <li class="section-1"> Risikofreie Neupatientengewinnung</li>
              </ul>
              <br/>
              <p></p>
              <a  href="/aerzte/registrierung" class="btn-footer1">Jetzt kostenlos als Arzt registrieren!</a>
              <p></p>
              <br/>
              <p>Für Fragen stehen wir Ihnen jederzeit gerne zur Verfügung.<br/>
                Telefonisch oder per E- Mail:<br/>
                <span class="footer1"> <a href="tel:030609840260" class="footer1"> 030 609 840 260</a></span> | <span class="footer1"><a href="mailto:info@arzttermine.de" class="footer1" target="_top" >info@arzttermine.de</a> </span></p>
            </div>
        </footer>
      </div>
      <div class="footer2">
        <footer>
          <div class="row">
            <div class="col-sm" style="margin-bottom: 1rem">
              <a href="http://facebook.com/arzttermine"><i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i></a>
              <a href="http://twitter.com/Arzttermine"><i class="fa fa-twitter-square fa-2x" aria-hidden="true"></i></a>
              <a href="https://www.instagram.com/arzttermine_de/"><i class="fa fa-instagram  fa-2x" aria-hidden="true"></i></a>
              <a href="http://www.linkedin.com/company/arzttermine-de"><i class="fa fa-linkedin-square  fa-2x" aria-hidden="true"></i></a>
              <a href="http://www.xing.com/companies/arzttermine.de"><i class="fa fa-xing-square  fa-2x" aria-hidden="true"></i></a>
            </div>
            <div class="col">
              <ul>
                <li><a href="/fachgebiete" title="Informationen zu medizinische Fachgebiete" class="footer2">Medizinische Fachgebiete</a></li>
                <li><a href="/lp/patienten/so-funktioniert-arzttermine/" class="footer2">Patienten Informationen</a></li>
                <li><a href="/angebote-informationen" class="footer2">Angebot Informationen</a></li>
              </ul>
            </div>
            <div class="col">
              <ul>
                <li><a href="/aerzte-magazin" class="footer2">Ärzte Magazin</a></li>
                <li><a href="/praxisoptimierung" class="footer2">Praxisoptimierung</a></li>
              </ul>
            </div>
            <div class="col">
              <ul>
                <li><a href="/magazin/" class="footer2">Magazin</a></li>
                <li><a href="/news" class="footer2">News</a></li>
                <li><a href="/faq" class="footer2">FAQ</a></li>
              </ul>
            </div>
          </div>
            {*
              ==== SEARCH PAGE EXCEPTION ================================================================
              In the search page we show only the legally required links.
            *}
            {include file="footer_section2.tpl"}
        </footer>
      </div>
    {else}
      <div class="footer2">
        <footer>

            {*
              ==== SEARCH PAGE EXCEPTION ================================================================
              In the search page we show only the legally required links.
            *}
            {include file="footer_section2.tpl"}
        </footer>
      </div>
    {/if}
{/if} {* close if/else use_minimal_footer *}


{* ==== JAVASCRIPT ================================================================*}
<!--[if lte IE 8]>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<![endif]-->

<!--[if gt IE 8]>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<![endif]-->

<!--[if !IE]> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<!-- <![endif]-->
<script>
    $(document).ready(function(){
        $.each(AT.q,function(index,f){
            $(f);
        });

        $("#firststep button").click(function(){
            $("#firststep").hide();
            $("#secondstep").show();
        });
    })
</script>

<script type="text/javascript">
    var apiUrl = "{$app->getConfig('URL_API')}";
    {if $app->isWidget()}
    var widget = '{$app->getWidgetContainer()->getSlug()}';
    {/if}
</script>

<script type="text/javascript" src="{$this->getStaticUrl('js/vendor/jquery.formalize.min.js')}"></script>
<script type="text/javascript" src="{$this->getStaticUrl('js/at.js')}"></script>
<script type="text/javascript" src="{$this->getStaticUrl('js/calendar.js')}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
<script src="https://use.fontawesome.com/f7e3e96c18.js"></script>


{if $app->getConfig('SOCIALMEDIA_ACTIVATE')}
  <script type="text/javascript" src="{$this->getStaticUrl('jquery/plugins/socialshareprivacy/jquery.socialshareprivacy.min.js')}"></script>
{/if}
{$this->getJsInclude()}

{if $CONFIG.GOOGLE_ANALYTICS_ACTIVATE}
    {include file="js/tracking/_google-remarketing-tag.tpl"}
{/if}
{if $CONFIG.GOOGLE_ANALYTICS_ACTIVATE && isset($activate_freespee_phonenumber_tracking)}
    {include file="js/tracking/_freespee-phonenumber-tracking.tpl"}
{/if}

{* ==== END - JAVASCRIPT ================================================================ *}
{*
  ==== SEARCH PAGE EXCEPTION ================================================================
  In the search page the footer is located inside the <section id="results-list">, so the doc closing tags should not be added.
*}
{if !$isSearchPage}
    {* Used to overlay popups, like the "more appointments" <div> *}
  <div class="layer"></div>
  </body>
  </html>
{/if}