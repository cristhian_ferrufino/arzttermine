<section id="call_to_action_my_area">
  <h2 align="center">Ihre Gesundheit liegt uns am Herzen!</h2>
  <p align="center">Machen Sie sich keine Sorgen - wir kümmern uns um Ihren Arzttermin</p>
  <p style="text-align: center">
    <img src="../static/images/assets/logo-2.png">
  </p>


  <div class="row align-items-center">
    <div class="col-md-5 offset-md-1 " style="margin-top: 1rem">
      <img src="../static/images/assets/call_to_action/1.jpg" width="100%">
      <p align="left"> Ihre Gesundheit ist unsere Priorität. Wir sind eine von Deutschland’s Top Seiten für zuverlässige Arzttermine.</p>
    </div>
    <div class="col-md-5" style="margin-top: 1rem">
      <img src="../static/images/assets/call_to_action/2.jpg" width="100%">
      <p align="left">Wir machen Arzttermine einfach! Damit Sie mehr Zeit für die schönen Dinge im Leben haben.</p>
    </div>
  </div>

  <div style="padding-left: .8rem; margin: 1rem 0 1rem 0">
    <p style="text-align: center">
        <a class="find_doctor" href="/suche?form%5Bmedical_specialty_id%5D=1&form%5Blocation%5D={$this->getUserCity()}&form%5Binsurance_id%5D=2">Ärzte in meiner Nähe anzeigen</a>
    </p>
  </div>
</section>