<style>
  .text-logo {
    color: #0090d6;
    font-style: italic;
    font-weight: 300;
  }
  .text-logo > span, .text-logo > a span {
    color: #313131;
    font-style: normal;
    font-weight: 900;
    font-family: Gudea, Arial, sans-serif;
  }
  .text-logo.white {
    color: white;
  }
  .text-logo.white span {
    color: white;
  }
  .text-logo.white:hover {
    color: white;
  }
  .text-logo.with-subline {
    margin-bottom: 0px;
  }

  header {
    position: relative;

  }
  .hide {
    display: none;
  }
  .tab-content {
    padding:25px 25px 25px 0;
  }

  #material-tabs {
    position: relative;
    display: block;
    padding:0;
    border-bottom: 1px solid #e0e0e0;
  }

  #material-tabs>a {
    position: relative;
    display:inline-block;
    text-decoration: none;
    padding: 20px;
    text-transform: uppercase;
    font-size: 14px;
    font-weight: 600;
    color: #424f5a;
    text-align: center;
    outline:;
  }

  #material-tabs>a.active {
    font-weight: 700;
    outline:none;
  }

  #material-tabs>a:not(.active):hover {
    background-color: inherit;
    color: #7c848a;
  }

  @media only screen and (max-width: 520px) {
    .nav-tabs#material-tabs>li>a {
      font-size: 11px;
    }
  }

  .nav-bar {
    position: absolute;
    z-index: 10;
    bottom: 0;
    height: 3px;
    background: #0090D6;
    display: block;
    left: 0;
    transition: left .2s ease;
    -webkit-transition: left .2s ease;
  }

  #tab1-tab.active ~ span.nav-bar {
    left: 10;
    width: 80px;
  }

  #tab2-tab.active ~ span.nav-bar {
    left: 110px;
    width: 80px;
  }
  #tab3-tab.active ~ span.nav-bar {
    left: 220px;
    width: 80px;
  }
  #tab4-tab.active ~ span.nav-bar {
    left: 330px;
    width: 80px;
  }
  #tab5-tab.active ~ span.nav-bar {
    left: 410px;
    width: 80px;
  }
  #tab6-tab.active ~ span.nav-bar {
    left: 500px;
    width: 80px;
  }

  h4.tabs{
    font-family: "gudea", sand-serif;
    font-size: 1.75rem;
    margin-bottom: 1.1rem;
    line-height: 2rem;
  }
  p.tabs {
    margin-bottom: 1.25rem;
  }
  img.tabs{
    width: 100%;
    margin-bottom: 1rem;
  }
</style>

<section id="product_carousel">
  <header>
    <div id="material-tabs">
      <a id="tab1-tab" href="#AtWidget" class="active"><span class="text-logo"><span>At</span>Widget</span></a>
      <a id="tab2-tab" href="#AtPatient"><span class="text-logo"><span>At</span>Patient</span></a>
      <a id="tab3-tab" href="#AtSearch"><span class="text-logo"><span>At</span>Search</span></a>
      <a id="tab4-tab" href="#AtWeb"><span class="text-logo"><span>At</span>Web</span></a>
      <a id="tab5-tab" href="#AtRep"><span class="text-logo"><span>At</span>Rep</span></a>
      {*<a id="tab6-tab" href="#AtCall"><span class="text-logo"><span>At</span>Call</span></a>*}
      <span class="nav-bar  hidden-sm-down"></span>
    </div>
  </header>

  <div class="tab-content">

    <div id="AtWidget">
      <div class="row">
        <div class="col-sm">
          <img src="../static/images/assets/tabs/AtWidget.png" class="tabs">
        </div>
        <div class="col">
          <h4 class="tabs">Online Terminkalender</h4>
          <p class="tabs">Rundum-Service für Ihre Patienten. Ermöglichen Sie eine Online Terminvergabe für ein optimales Terminmanagement.</p>
          <a href="/praxisoptimierung/atwidget" target="_blank" title="AtWidget">Weitere Infos zu AtWidget</a>
        </div>
      </div>
    </div>
    <div id="AtPatient">
      <div class="row">
        <div class="col-sm">
          <img src="../static/images/assets/tabs/AtPatient.png" class="tabs">
        </div>
        <div class="col">
          <h4 class="tabs">Patientengewinnung</h4>
          <p class="tabs">Neue Patienten sind neue Impulse für Ihre Praxis. Wir kümmern uns um Ihre Neukundenakquise!</p>
          <a href="/lp/praxis-marketing/praxis-marketing.html" target="_blank" title="AtPatient">Weitere Infos zu AtPatient</a>
        </div>
      </div>
    </div>
    <div id="AtSearch">
      <div class="row">
        <div class="col-sm">
          <img src="../static/images/assets/tabs/AtSearch.png" class="tabs">
        </div>
        <div class="col">
          <h4 class="tabs">Google Werbeanzeigen</h4>
          <p class="tabs">Erhalten Sie mit gezielten Werbeanzeigen eine optimale Platzierung bei Google. Wir helfen Ihnen mit einem kennzahlenbasierten Reporting.</p>
          <a href="/praxisoptimierung/atsearch" target="_blank" title="AtSearch">Weitere Infos zu AtSearch</a>
        </div>
      </div>
    </div>
    <div id="AtWeb">
      <div class="row">
        <div class="col-sm">
          <img src="../static/images/assets/tabs/AtWeb.png" class="tabs">
        </div>
        <div class="col">
          <h4 class="tabs">Websiteoptimierung</h4>
          <p class="tabs">Ihre Patienten erwarten die gleiche Qualität von Ihrer Website wie von Ihren Dienstleistungen. Wir bieten Ihnen eine suchmaschinenoptimierte Website nach Ihren Wünschen.</p>
          <a href="/praxisoptimierung/atweb" target="_blank" title="AtWeb">Weitere Infos zu AtWeb</a>
        </div>
      </div>
    </div>
    <div id="AtRep">
      <div class="row">
        <div class="col-sm">
          <img src="../static/images/assets/tabs/AtRep.png" class="tabs">
        </div>
        <div class="col">
          <h4 class="tabs">Reputationsmanagement</h4>
          <p class="tabs">Online Bewertungen immer im Blick: Wir bieten professionelle Unterstützung beim Umgang mit Ihrer Online Reputation.</p>
          <a href="/praxisoptimierung/atrep" target="_blank" title="AtRep">Weitere Infos zu AtRep</a>
        </div>
      </div>
    </div>
    {*<div id="AtCall">
      <div class="row">
        <div class="col-sm">
          <img src="../static/images/assets/tabs/AtRep.png" class="tabs">
        </div>
        <div class="col">
          <h4 class="tabs">Telefonservicer</h4>
          <p class="tabs">Mehr Zeit für Ihre Patienten vor Ort: Wir kümmern uns um Ihr Praxistelefon. Vermeiden Sie Warteschleifen, Unzufriedenheit und Patientenverlust.</p>
          <a href="/praxisoptimierung/atcall" target="_blank" title="AtCall">Weitere Infos zu AtCall</a>
        </div>
      </div>
    </div>*}
  </div>
</section>
{literal}
  <script type="text/javascript">
      $(document).ready(function() {
          $('#material-tabs').each(function() {

              var $active, $content, $links = $(this).find('a');

              $active = $($links[0]);
              $active.addClass('active');

              $content = $($active[0].hash);

              $links.not($active).each(function() {
                  $(this.hash).hide();
              });

              $(this).on('click', 'a', function(e) {

                  $active.removeClass('active');
                  $content.hide();

                  $active = $(this);
                  $content = $(this.hash);

                  $active.addClass('active');
                  $content.show();

                  e.preventDefault();
              });
          });
      });
  </script>
{/literal}