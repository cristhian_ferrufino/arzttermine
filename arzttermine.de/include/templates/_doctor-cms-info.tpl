{if $user->getInfoText('docinfo_1')}
<section itemscope itemtype="http://schema.org/Physician">
  <div class="row">
    <div class="col-sm-3">
      <h3>{"Philosophie"|trans}</h3>
    </div>
    <div class="col-sm-8">
      <div class="additional-info-text" itemprop="description">
          {$user->getInfoText('docinfo_1')}
      </div>
    </div>
  </div>
</section>
{/if}

{if $user->getInfoText('docinfo_5')}
<section itemscope itemtype="http://schema.org/Physician">
  <div class="row">
    <div class="col-sm-3">
      <h3>{"Spezialisierung"|trans}</h3>
    </div>
    <div class="col-sm-8">
      <div class="additional-info-text" itemprop="description">
          {$user->getInfoText('docinfo_5')}
      </div>
    </div>
  </div>
</section>
{/if}

{if $user->getInfoText('docinfo_6')}
<section itemscope itemtype="http://schema.org/Physician">
  <div class="row">
    <div class="col-sm-3">
      <h3>{"Behandlungsmöglichkeiten"|trans}</h3>
    </div>
    <div class="col-sm-8">
      <div class="additional-info-text" itemprop="availableService">
          {$user->getInfoText('docinfo_6')}
      </div>
    </div>
  </div>
</section>
{/if}


{if $user->getInfoText('docinfo_3')}
<section itemscope itemtype="http://schema.org/Physician">
  <div class="row">
    <div class="col-sm-3">
      <h3>{"Sprachen"|trans}</h3>
    </div>
    <div class="col-sm-8">
      <div class="additional-info-text" itemprop="description">
          {$user->getInfoText('docinfo_3')}
      </div>
    </div>
  </div>
</section>
{/if}

{if $user->getInfoText('docinfo_2')}
<section itemscope itemtype="http://schema.org/Physician">
  <div class="row">
    <div class="col-sm-3">
      <h3>{"Ausbildung"|trans}</h3>
    </div>
    <div class="col-sm-8">
      <div class="additional-info-text" itemprop="description">
          {$user->getInfoText('docinfo_2')}
      </div>
    </div>
  </div>
</section>
{/if}

{if $user->getInfoText('docinfo_4')}
<section itemscope itemtype="http://schema.org/Physician">
  <div class="row">
    <div class="col-sm-3">
      <h3>{"Auszeichnungen"|trans}</h3>
    </div>
    <div class="col-sm-8">
      <div class="additional-info-text" itemprop="description">
          {$user->getInfoText('docinfo_4')}
      </div>
    </div>
  </div>
</section>
{/if}

{if $user->getInfoText('info_seo')}
<section class="info-text-container" itemscope itemtype="http://schema.org/Physician">
  <div class="row">
    <div class="col-sm-3">
      <h3>{"Beschreibung"|trans}</h3>
    </div>
    <div class="col-sm-8">
      <div class="additional-info-text" itemprop="description">
          {$user->getInfoText('info_seo')}
      </div>
    </div>
  </div>
</section>
{/if}