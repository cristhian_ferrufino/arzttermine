<div class="hidden-sm-down">
  <form class="search-box" action="{url path="search"}" method="get" enctype="application/x-www-form-urlencoded">
    <div class="row ">
      <div class="col-sm-3">
          <label>{'ui.search.specialty.label'|trans}</label>
          <select name="form[medical_specialty_id]">
            <option value="" class="select-placeholder">Bitte wählen</option>
            {html_options options=$MedicalSpecialtyOptions}
          </select>
        </div>

        <div class="col-sm-3">
          <label>{'ui.search.place.label'|trans}</label>
          <input type="text" placeholder="{'ui.search.place.placeholder'|trans}" name="form[location]"{if $form.location != null} value="{$form.location}"{/if}>
        </div>

        <div class="col-sm-3">
          <label >{'ui.search.insurance.label'|trans}</label>
          <select name="form[insurance_id]">
            <option value="" class="select-placeholder">Bitte wählen</option>
            <option value="2">gesetzlich</option>
            <option value="1">privat</option>
          </select>
        </div>

      <div class="col-sm-3">
        <button type="submit">{'ui.search.call_to_action'|trans}</button>
      </div>
    </div>
  </form>
</div>

<div class="hidden-md-up">
  <form class="search-box" action="{url path="search"}" method="get" enctype="application/x-www-form-urlencoded" style="background-color: #F7F7F7;">
    <div class="row ">
      <div class="col-sm-3">
        <label>{'ui.search.specialty.label'|trans}</label>
        <select name="form[medical_specialty_id]">
          <option value="" class="select-placeholder">Bitte wählen</option>
            {html_options options=$MedicalSpecialtyOptions}
        </select>
      </div>

      <div class="col-sm-3">
        <label>{'ui.search.place.label'|trans}</label>
        <input type="text" placeholder="{'ui.search.place.placeholder'|trans}" name="form[location]"{if $form.location != null} value="{$form.location}"{/if}>
      </div>

      <div class="col-sm-3">
        <label >{'ui.search.insurance.label'|trans}</label>
        <select name="form[insurance_id]">
          <option value="" class="select-placeholder">Bitte wählen</option>
          <option value="2">gesetzlich</option>
          <option value="1">privat</option>
        </select>
      </div>

      <div class="col-sm-3">
        <button type="submit">{'ui.search.call_to_action'|trans}</button>
      </div>
    </div>
  </form>
</div>