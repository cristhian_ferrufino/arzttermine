<div id="ref_alert_overlay"></div>
<div id="ref_alert_message" class="light_blue_bg" align="center">
{$refMessage}
<br/><br/>
Empfehlungscode:
<form action="gutschein" method="post" id='referral_form'>
<input type="text" name="ref_code">
<button class="referral_close btn_green" id="referral_submit" type="submit">{$buttonText}</button>
</form>
</div>
<script>
	window.onload = function(){
		var closeButton = document.getElementById('close_ref');
		if(closeButton){
			closeButton.onclick = function(){
				document.getElementById('ref_alert_overlay').style.display='none';
				document.getElementById('ref_alert_message').style.display='none';
			};
		}
		$("#referral_submit").click(function() {
			var url = "gutschein";
			$.ajax({
				type: "POST",
				url: url,
				data: $("#referral_form").serialize(),
				success: function(data){
					var dataJson = eval("(" + data + ')');
					var message = dataJson[0];
					var buttonText = dataJson[1];
					$('#ref_alert_message').html(message+'<button class="referral_close btn_green btn" id="close_ref" type="button">'+buttonText+'</button>');
					$('#close_ref').click(function(){
						$('#ref_alert_overlay').css('display','none');
						$('#ref_alert_message').css('display','none');
					});
				}
			});
			return false;
		});
	};
</script>