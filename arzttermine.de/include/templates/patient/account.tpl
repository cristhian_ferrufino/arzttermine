{assign var="template_body_class" value="profile-page"}
{assign var="additional_body_classes" value="patient account-dashboard"}
{include 'header.tpl'}

<div class="container">
    <main>
        <section class="summary">
            <div class="headline justified-blocks">
                <h1>{$patient->getName()}</h1>
                <a class="logout" href="{url path='patient_login'}?action=logout">abmelden</a>
            </div>
            <nav>
                <ul>
                    <li class="active"><a href="{url path='patient_account'}">Mein Profil</a></li>
                    <li><a href="{url path='patient_appointments'}">Meine Termine</a></li>
                </ul>
            </nav>
        </section>

        <section>
            <form action="{url path='patient_account'}" method="post">
                <div class="text-data name">
                    <h3>Anrede</h3>
                    <div>
                        <input name="gender" type="radio" id="gender-1" value="1" {if $patient->getGender() === 1}checked{/if}><label for="gender-1">Frau</label>
                        <input name="gender" type="radio" id="gender-2" value="0" {if $patient->getGender() === 0}checked{/if}><label for="gender-2">Herr</label>
                    </div>
                </div>
                
                <div class="text-data name">
                    <h3>Name</h3>
                    <div>
                        <input type="text" name="first_name" value="{$patient->getFirstName()}" placeholder="Vorname" />
                        <input type="text" name="last_name" value="{$patient->getLastName()}" placeholder="Nachname" />
                    </div>
                </div>

                <div class="text-data">
                    <h3>E-Mail</h3>
                    <div>
                        {$patient->getEmail()}
                    </div>
                </div>

                <div class="text-data">
                    <h3>Telefonnummer</h3>
                    <div>
                        <input type="text" name="phone" value="{$patient->getPhone()}" placeholder="Telefonnummer" />
                    </div>
                </div>

                <div class="text-data">
                    <h3>{$app->_('Versicherungsart')}</h3>
                    <div id="insurance-type" class="radio-group">
                        <input name="insurance" type="radio" id="insurance-1" value="1" {if $patient->isPrivatelyInsured()}checked{/if}><label for="insurance-1">Privat versichert</label>
                        <input name="insurance" type="radio" id="insurance-2" value="2" {if $patient->isPublicallyInsured()}checked{/if}><label for="insurance-2">Gesetzlich versichert</label>
                    </div>
                </div>

                <div class="text-data">
                    <h3>Passwort</h3>
                    <div class="change-password">
                        <input class="password" type="password" name="current_password" placeholder="Aktuelles Passwort">
                        <input class="password" type="password" name="new_password" placeholder="Neues Passwort">
                    </div>
                </div>

                <div class="buttons-container">
                    <button class="save">Save</button>
                    <button class="reset">Reset</button>
                </div>

                <input type="hidden" name="uid" value="{$patient->getId()}" />
            </form>
        </section>

    </main>
</div>

{include 'footer.tpl'}

