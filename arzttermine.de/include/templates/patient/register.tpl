
{assign var="template_body_class" value="two-cols-with-form"}
{assign var="additional_body_classes" value="doctor-registration doctor-login patient-registration"}
{assign var="use_minimal_footer" value=true}
{assign var="isSales" value=true}
{include 'header.tpl'}

<div class="container">
    <main>
        <h1>Registrieren</h1>
        <section id="form">
            <form id="register_form" name="register_form" action="{url path='patient_register'}" method="post">
                <div class="left-block">
                    <div class="form-element {if in_array('gender', $errors)}error{/if}" data-error-message="Bitte wählen Sie Ihre Anrede aus.">
                        <div id="gender" class="radio-group">
                            <h4>Anrede</h4>
                            <input name="gender" type="radio" id="gender-1" value="1" {if $data.gender === 1}checked{/if}><label for="gender-1">Frau</label>
                            <input name="gender" type="radio" id="gender-2" value="0" {if $data.gender === 0}checked{/if}><label for="gender-2">Herr</label>
                        </div>
                    </div>
                    <div class="form-element {if in_array('first_name', $errors)}error{/if}" data-error-message="Bitte geben Sie Ihren Vornamen ein.">
                        <label>Vorname</label>
                        <input type="text" name="first_name" value="{$data.first_name}">
                    </div>
                    <div class="form-element {if in_array('last_name', $errors)}error{/if}" data-error-message="Bitte geben Sie Ihren Nachnamen ein.">
                        <label>Nachname</label>
                        <input type="text" name="last_name" value="{$data.last_name}">
                    </div>
                    <div class="form-element {if in_array('email', $errors)}error{/if}" data-error-message="Diese Mailadresse ist ungültig oder ein Account mit dieser E-Mail-Adresse besteht bereits.">
                        <label>E-Mail</label>
                        <input type="email" name="email" value="{$data.email}">
                    </div>
                    <div class="form-element {if in_array('phone', $errors)}error{/if}" data-error-message="Bitte geben Sie eine gültige Telefonnummer ein.">
                        <label>Telefon</label>
                        <input type="tel" name="phone" value="{$data.phone}">
                    </div>

                    <div class="form-element insurance">
                        <label>Versicherungsart</label>
                        <div class="radio-group">
                            <input id="form_insurance_pkv" type="radio" name="insurance" value="1" {if $data.insurance === 1}checked{/if}><label for="form_insurance_pkv">privat versichert</label>
                            <input id="form_insurance_gkv" type="radio" name="insurance" value="2" {if $data.insurance === 2}checked{/if}><label for="form_insurance_gkv">gesetzlich versichert</label>
                        </div>
                    </div>

                </div>
                <div class="form-element {if in_array('password', $errors)}error{/if}" data-error-message="Das Password sollte mindestes 8 Zeichen haben und mindestens einen Buchstaben sowie eine Zahl enthalten.">
                    <label>Passwort <a class="help" href="#">?</a></label>
                    <div class="help-tooltip">Bitte geben Sie ein Passwort ein, dass aus mind. 8 Zeichen und dabei mindestens einen Buchstaben sowie eine Zahl enthält.</div>
                    <input id="password" class="input_password" type="password" name="password" value="" autocomplete="off" />
                </div>

                <div class="form-element terms {if in_array('agb', $errors)}error{/if}" data-error-message="Bitte bestätigen Sie, dass Sie die Datenschutzbedingungen und AGB akzeptieren.">
                    <input type="checkbox" name="agb" value="1" class="terms-checkbox" id="agb" {if $data.agb === 1} checked="checked"{/if} tabindex="10"/>
                    <label for="agb">Hiermit akzeptiere ich die <a href="/agb" target="_blank">Nutzungsbedingungen</a> und <a href="/datenschutz" target="_blank">Datenschutzbedingungen.</a> </label>
                </div>
                <div class="form-element newsletter">
                    <input name="newsletter_subscription" type="checkbox"{if $data.newsletter_subscription === 1} checked="checked"{/if} value="1" class="terms-checkbox" id="newsletter_id" tabindex="10"/>
                    <label for="newsletter_id">Abonnieren Sie den Newsletter</label>
                </div>

                <div class="button-wrapper"><button type="submit">Registrieren</button></div>
            </form>

        </section>

        <section id="info-block" class="infobox infobox-grey-light cta-phone">
            <h4><em>Eine Registrierung ist zur Terminbuchung nicht zwingend notwendig.</em></h4>
        </section>
        <section id="info-block" class="infobox infobox-grey-light cta-phone">
            <h4><em>Haben Sie noch Fragen?</em></h4>
            <p>Wir sind 24 Stunden kostenfrei für Sie erreichbar:</p>
            {include file="phone-numbers/phone-sales.tpl"}
        </section>
    </main>
</div>

{include 'footer.tpl'}