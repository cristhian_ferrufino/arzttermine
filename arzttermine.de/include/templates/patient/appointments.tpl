{assign var="template_body_class" value="profile-page"}
{assign var="additional_body_classes" value="patient account-dashboard"}
{include 'header.tpl'}

{$this->addJsInclude('/ngs/js/patients/appointments.js')}

<div class="container">
    <main>
        <section class="summary">
            <div class="headline justified-blocks">
                <h1>{$patient->getName()}</h1>
                <a class="logout" href="{url path='patient_login'}?action=logout">abmelden</a>
            </div>
            <nav>
                <ul>
                    <li><a href="{url path='patient_account'}">Mein Profil</a></li>
                    <li class="active"><a href="{url path='patient_appointments'}">Meine Termine</a></li>
                </ul>
            </nav>
        </section>


        <section>
            {if $upcoming_bookings|count > 0}
            <div class="booking-group">
                <h2>Bevorstehende Termine</h2>
                <table>
                    <thead>
                        <tr>
                            <th class="doctor">Arzt</th>
                            <th class="practice">Praxis</th>
                            <th class="status">Status</th>
                            <th class="date">Datum</th>
                            <th class="action"></th>
                        </tr>
                    </thead>
                    <tbody>
                    {foreach from=$upcoming_bookings item=booking key=index}
                        {assign var=doctor value=$booking->getUser()}
                        {assign var=location value=$booking->getLocation()}
                        {if $doctor and $location}
                            <tr>
                                <td>
                                    <a href="{$doctor->getUrl()}" target="_blank">{$doctor->getName()}</a>
                                </td>
                                <td>
                                    <a href="{$location->getUrl()}" target="_blank">{$location->getName()}</a>
                                </td>
                                <td class="booking_status">{$booking->getStatusPatientText()}</td>
                                <td>
                                    {$booking->getAppointment()->getDateTimeText(true)}
                                </td>
                                <td>
                                    {assign var=date value={$booking->getAppointment()->getWeekdayDateText()}}
                                    {assign var=time value={$booking->getAppointment()->getTimeText()}}
                                    {if $booking->isCancelableByPatient()}
                                        <input class="cancel"
                                               data-date="{$date}"
                                               data-time="{$time}"
                                               data-booking_type="upcoming"
                                               data-booking_status="{$booking->getStatus()}"
                                               data-doctor_name="{$doctor->getName()}"
                                               data-booking_id="{$booking->getId()}"
                                               data-integration_id="{$location->getIntegrationId()}"
                                               type="button" value="Stornieren" />
                                    {else}
                                        <a href="{$doctor->getUrl()}" target="_blank">
                                            <input class="action" type="button" value="erneut buchen" />
                                        </a>
                                    {/if}
                                </td>
                            </tr>
                        {/if}
                    {/foreach}
                    </tbody>
                </table>
            </div>
            {/if}

            {if $pending_bookings|count > 0}
            <div class="booking-group">
                <h2>Terminanfragen</h2>
                <table>
                    <thead>
                        <tr>
                            <th class="doctor">Arzt</th>
                            <th class="practice">Praxis</th>
                            <th class="status">Status</th>
                            <th class="date">Datum</th>
                            <th class="action"></th>
                        </tr>
                    </thead>
                    <tbody>
                    {foreach from=$pending_bookings item=booking key=index}
                        {assign var=doctor value=$booking->getUser()}
                        {assign var=location value=$booking->getLocation()}
                        {if $doctor and $location}
                            <tr>
                                <td>
                                    <a href="{$doctor->getUrl()}" target="_blank">{$doctor->getName()}</a>
                                </td>
                                <td>
                                    <a href="{$location->getUrl()}" target="_blank">{$location->getName()}</a>
                                </td>

                                <td class="booking_status">{$booking->getStatusPatientText()}</td>
                                <td>{$booking->getCreatedAtFormat('w d.m.Y, H:i')}</td>
                                <td>
                                    {if $booking->isCancelableByPatient()}
                                        <input class="cancel"
                                               data-booking_type="appointment_request"
                                               data-booking_status="{$booking->getStatus()}"
                                               data-doctor_name="{$doctor->getName()}"
                                               data-booking_id="{$booking->getId()}"
                                               data-integration_id="{$location->getIntegrationId()}"
                                               type="button" value="Stornieren" />
                                    {else}
                                        <a href="{$doctor->getUrl()}" target="_blank">
                                            <input class="action" type="button" value="erneut buchen" />
                                        </a>
                                    {/if}
                                </td>
                            </tr>
                        {/if}
                    {/foreach}
                    </tbody>
                </table>
            </div>
            {/if}

            {if $past_bookings|count > 0}
            <div class="booking-group">
                <h2>Vergangene Termine</h2>
                <table>
                    <thead>
                        <tr>
                            <th class="doctor">Arzt</th>
                            <th class="practice">Praxis</th>
                            <th class="status">Status</th>
                            <th class="date">Datum</th>
                            <th class="action"></th>
                        </tr>
                    </thead>
                    <tbody>
                    {foreach from=$past_bookings item=booking}
                        {assign var=doctor value=$booking->getUser()}
                        {assign var=location value=$booking->getLocation()}
                        {if $doctor and $location}
                            <tr>
                                <td>
                                    <a href="{$doctor->getUrl()}" target="_blank">{$doctor->getName()}</a>
                                </td>
                                <td>
                                    <a href="{$location->getUrl()}" target="_blank">{$location->getName()}</a>
                                </td>
                                <td class="booking_status">{$booking->getStatusPatientText()}</td>
                                <td>
                                    {$booking->getAppointment()->getDateTimeText(true)}
                                </td>
                                <td>
                                    <a href="{$doctor->getUrl()}" target="_blank" style="text-decoration: none">
                                        <input class="action" type="button" value="erneut buchen" />
                                    </a>
                                </td>
                            </tr>
                        {/if}
                    {/foreach}
                    </tbody>
                </table>
            </div>
            {/if}
        </section>

    </main>
</div>

<div id="referral_tooltip-loadtime" style="display:none;">
    <div class="referral_howto">
        <img src="/static/img/loader-horizontal.gif" alt="loading" />
        <p>Bitte haben Sie einen Moment Geduld, während wir Ihre Stornierung an die Praxis weiterleiten. </p>
    </div>
</div>

<script>
    var patients_url = '{url path="patient_account"}';
</script>

{include 'footer.tpl'}