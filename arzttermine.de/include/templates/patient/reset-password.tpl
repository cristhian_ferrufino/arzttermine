{assign var="template_body_class" value="two-cols-with-form"}
{assign var="additional_body_classes" value="doctor-registration"}
{assign var="use_minimal_footer" value=true}
{assign var="isSales" value=true}
{include 'header.tpl'}

<div class="container">
    <main>
        <h1>Passwort zurücksetzen</h1>
        <section id="form">
            <form action="" method="post">
                <div class="form-element">
                    <label>Passwort<label>
                    {include "doctor/password-fields.tpl"}
                </div>
                <div class="form-element">
                    <label>Passwort bestätigen</label>
                    <input id="repeat_password" type="password" name="repeat_password" required/>
                </div>
                <input id="reset_password_code" type="hidden" name="reset_password_code" value="{$data.password_reset_code}"/>
                <div class="button-wrapper"><button type="submit">Speichern</button></div>
            </form>
        </section>

        <section id="info-block"  class="infobox infobox-grey-light cta-phone">
            <h4><em>Haben Sie noch Fragen?</em></h4>
            <p>Wir sind 24 Stunden kostenfrei für Sie erreichbar:</p>
        </section>

    </main>
</div>

{include 'footer.tpl'}

