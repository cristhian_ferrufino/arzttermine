<div id="referral-tooltip" {if $referral_company=='Workhub'}class="workhub_tooltip"{/if}>
{if !isset($referral_error)}
	<div class="referral_coupon"{if !isset($referral_company_image_name) or $referral_company_image_name==''} style="height:30px;"{/if}>
		{if $patientRefCode}{else}
		<span class="price">
			{if isset($referral_price)}{$referral_price}{/if}
		</span>		
		{if isset($referral_company_image_name) and $referral_company_image_name!=''}
			<img src="/static/img/referrals/{$referral_company_image_name}" />
		{else}
			<span class="price">{$referral_company}</span>
		{/if}
		{/if}
		<a id="hide_referral_tooltip" class="btn_close" >X</a>
	</div>	
	{if $referral_company!='Workhub'}
	<div id="referral_howto" class="referral_howto">
		
		{if $tooltip_page=="homepage"}
			<p>{if $patientRefCode}
					Jetzt Wunschtermin buchen
				{else}
					{if $form.referral_code!='Oralbat2013'}Jetzt Wunschtermin buchen und Gutschein sichern {else}Jetzt Wunschtermin buchen um am Gewinnspiel teilzunehmen{/if}
				{/if}
			</p>
			<ol>
				<li><span>Stadt oder Standort eingeben und Terminsuche starten</span></li>
				<li class="deactive">Wunschtermin auswählen und buchen</li>
				<li class="deactive">
					{if $patientRefCode}
						Termin wahrnehmen
					{else}
						{if $form.referral_code!='Oralbat2013'}Termin wahrnehmen und Gutschein zugesendet bekommen{else}Termin wahrnehmen um sich für die Teilnahme zu qualifizieren{/if}
					{/if}
				</li>
			</ol>
		{elseif $tooltip_page=="search"}
			<p>{if $patientRefCode}
					Jetzt Wunschtermin buchen
				{else}
					{if $form.referral_code!='Oralbat2013'}Nur noch 2 Schritte bis zum Wunschtermin und Gutschein{else}Jetzt Wunschtermin buchen um am Gewinnspiel teilzunehmen{/if}
				{/if}
			</p>
			<ol>
				<li class="done">Stadt oder Standort eingeben und Terminsuche starten</li>
				<li><span>Wunschtermin auswählen und buchen</span></li>
				<li class="deactive">
				{if $patientRefCode}
					Termin wahrnehmen
				{else}	
					{if $form.referral_code!='Oralbat2013'}Termin wahrnehmen und Gutschein zugesendet bekommen{else}Termin wahrnehmen um sich für die Teilnahme zu qualifizieren{/if}
				{/if}
				</li>
			</ol>
		{elseif $tooltip_page=="booking"}
			<p>
				{if $patientRefCode}
					Der letzte Schritt bis zum Wunschtermin
				{else}	
					{if $form.referral_code!='Oralbat2013'}Der letzte Schritte bis zum Wunschtermin und Gutschein{else}Der letzte Schritt bis zum Wunschtermin{/if}
				{/if}
			</p>
			<ol>
				{if $form.referral_code!='Oralbat2013'}<li class="done">Stadt oder Standort eingeben und Terminsuche starten</li>{else}{/if}
				<li class="done">Buchen Sie nun Ihren Wunschtermin</li>
				<li><span>
				{if $patientRefCode}
					Termin wahrnehmen
				{else}	
					{if $form.referral_code!='Oralbat2013'}Termin wahrnehmen und Gutschein zugesendet bekommen{else}Termin wahrnehmen um sich für die Teilnahme zu qualifizieren{/if}
				{/if}
				</span></li>
			</ol>
		{elseif $tooltip_page=="booking_confirmation"}
			<p>Vielen Dank für Ihre Buchung!</p>
            {if $form.referral_code!='Oralbat2013'}<p>Sie erhalten den Gutschein per E-Mail von uns, nachdem Sie den Termin wahrgenommen haben.</p>{/if}
		{/if}
        
        {if $tooltip_page != "booking_confirmation"}
            {if $form.referral_code=='Oralbat2013'}
                <a href="/verlosung-informationen" target="_blank">Teilnahmebedingungen Gewinnspiel</a>
			{elseif $form.referral_code == 'AtJuni17'}
				<a href="/gutschein-teilnahmebedingungen-0617" target="_blank">Teilnahmebedingungen</a>
			{else}
                <a href="/angebote-informationen" target="_blank">Teilnahmebedingungen</a>
            {/if}
        {/if}
	</div>
	{/if}
{else}
    <div id="referral_howto" class="referral_howto">
        {$referral_error}
    </div>
 {/if}
</div>

<script type="text/javascript">
    $( "#hide_referral_tooltip" ).click(function() {
        $( "#referral-tooltip" ).hide();
    });

</script>
