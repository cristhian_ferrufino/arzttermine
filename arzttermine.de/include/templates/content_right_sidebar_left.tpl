{strip}
    {assign var="template_body_class" value="static"}
    {include file="header.tpl"}
{/strip}

<div class="container">
    <main>
        <aside>
            <nav>
                {include 'navi.tpl'}
            </nav>

            {render name='content2' method='render_sidebar'}
            
            {if $this->contentLeft}
                {$this->contentLeft}
            {/if}

            {$this->getLatestBlogPosts()}
        </aside>

        <section id="content">
            {render name='content1' method='render_content'}
            
            {if $this->contentRight}
                {$this->contentRight}
            {/if}
        </section>
    </main>
</div>

{include file="footer.tpl"}
