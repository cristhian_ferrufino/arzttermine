<div class="row">
  <div class="col">
    <ul class="inlined">
      <li><b class="footer2">© 2017 Arzttermine.de</b></li>
      <li style="padding-left: .5rem;"><a href="/agb" class="footer2">AGB</a></li>
      <li class="section2"><a href="/impressum" class="footer2">Impressum</a></li>
      <li class="section2"><a href="/datenschutz" class="footer2">Datenschutz</a></li>
      <li class="section2"><a href="/kontakt" class="footer2">Kontakt</a></li>
      <li class="section2"><a href="/jobs" class="footer2">Jobs</a></li>
      <li class="section2"><a href="/presse" class="footer2">Presse</a></li>
    </ul>
  </div>
</div>