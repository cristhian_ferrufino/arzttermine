<div class="panel panel-default list-navigation">
	<div class="panel-heading">{$total} items</div>
	<table class="table">
		{if $navigation_type=='alpha'}
			<tr>
				<td valign="top" colspan="4">
					<div>[{$pagination_alpha_index}]:&nbsp;</div>
					<ul class="pagination pagination-sm">{$pagination_alpha}</ul>
				</td>
			</tr>
		{/if}
		<tr>
			<td>
				{if $pagination}
					{if $pagination_alpha_total}
						{$pagination_alpha_total} items in {$pagination_alpha_current}
					{/if}
					<ul class="pagination pagination-sm">
						{$pagination}
					</ul>
				{else}
					More &lt;none&gt;
				{/if}
			</td>
			<td>
				{if $allow_alpha}
					<div class="btn-group btn-group-sm">
						<a href="{$url_type_num}" class="btn btn-{if $url_para.type=='num'}primary{else}default{/if}">num</a>
						<a href="{$url_type_alpha}" class="btn btn-{if $url_para.type=='alpha'}primary{else}default{/if}">alpha</a>
					</div>
				{/if}
			</td>
			<td>
				<div class="btn-group btn-group-sm">
					<a href="{$url_para_num_per_page}&num_per_page=10" class="btn btn-{if $url_para.num_per_page==10}primary{else}default{/if}">10</a>
					<a href="{$url_para_num_per_page}&num_per_page=25" class="btn btn-{if $url_para.num_per_page==25}primary{else}default{/if}">25</a>
					<a href="{$url_para_num_per_page}&num_per_page=50" class="btn btn-{if $url_para.num_per_page==50}primary{else}default{/if}">50</a>
					<a href="{$url_para_num_per_page}&num_per_page=100" class="btn btn-{if $url_para.num_per_page==100}primary{else}default{/if}">100</a>
					<a href="{$url_para_num_per_page}&num_per_page=250" class="btn btn-{if $url_para.num_per_page==250}primary{else}default{/if}">250</a>
				</div>
			</td>
			<td>
				{$pagination_select}
			</td>
		</tr>
	</table>
</div>
