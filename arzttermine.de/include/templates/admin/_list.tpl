{$this->_list_navigation}

<table class="table table-striped table-data">
	<thead>
	<tr class="columns">
		<th class="nowrap">{$this->action_buttons}</th>
		{foreach from=$this->key_array item='key'}
			<th class="nowrap">
				{strip}
					{if $key.url_order}<a href="{$key.url_order}" title="Order by">{/if}
					{$key.name}
					{if $key.url_order}</a>{/if}
					{if $key.ordered_by_this}<img src="{$this->img_order_direction}" class="direction" />{/if}
				{/strip}
			</th>
		{/foreach}
	</tr>
	</thead>

	<tbody>
	{foreach from=$this->data_array item='data'}
		<tr class="{if $data.tr_class} tr_class {$data.tr_class}{/if}">
			<td class="nowrap">{$data.item_buttons}</td>
			{foreach from=$data.keys item='item'}
				<td{if $item.nowrap} class="nowrap"{/if}>{$item.content}</td>
			{/foreach}
		</tr>
	{/foreach}
	</tbody>

	<tfoot>
	<tr class="columns">
		<th class="nowrap">{$this->action_buttons}</th>
		{foreach from=$this->key_array item='key'}
			<th class="nowrap">
				{strip}
					{if $key.url_order}<a href="{$key.url_order}" title="Order by">{/if}
					{$key.name}
					{if $key.url_order}</a>{/if}
					{if $key.ordered_by_this}<img src="{$this->img_order_direction}" class="direction" />{/if}
				{/strip}
			</th>
		{/foreach}
	</tr>
	</tfoot>
</table>
