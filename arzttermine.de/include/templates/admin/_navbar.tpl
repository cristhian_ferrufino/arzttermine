<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/"><img src="/static/img/logo-admin.png" alt="" /></a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li><a href="{$profile->getAdminEditUrl()}"><i class="fa fa-gear fa-fw"></i> Settings</a>
                </li>
                <li class="divider"></li>
                <li><a href="{url path='logout'}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li class="sidebar-search">
                    <div class="input-group custom-search-form">
                        <input type="text" class="form-control" placeholder="Suche...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                    </div>
                    <!-- /input-group -->
                </li>

                <li><a href="{url path='admin_home'}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a></li>
                {if $profile->checkPrivilege('booking_admin')}
                <li><a href="{url path='admin_bookings'}"><i class="fa fa-clock-o fa-fw"></i> Buchungen</a></li>
                {/if}
                {if $profile->checkPrivilege('bookingoffer_admin')}
                <li><a href="{url path='admin_bookingoffers'}"><i class="fa fa-question-circle fa-fw"></i> Terminvorschläge</a></li>
                {/if}
                {if $profile->checkPrivilege('user_admin')}
                <li><a href="{url path='admin_users'}"><i class="fa fa-user-md fa-fw"></i> Ärzte</a></li>
                {/if}
                {if $profile->checkPrivilege('location_admin')}
                <li><a href="{url path='admin_locations'}"><i class="fa fa-hospital-o fa-fw"></i> Praxen</a></li>
                {/if}
                {if $profile->checkPrivilege('user_admin')}
                <li><a href="{url path='admin_temp_users'}"><i class="fa fa-user-md fa-fw"></i> Temp Ärzte</a></li>
                {/if}
                {if $profile->checkPrivilege('group_admin')}
                <li><a href="{url path='admin_groups'}"><i class="fa fa-users fa-fw"></i> Gruppen</a></li>
                {/if}
                {if $profile->checkPrivilege('integration_admin')}
                <li><a href="{url path='admin_planners'}"><i class="fa fa-calendar-check-o fa-fw"></i> Planners</a></li>
                {/if}
                {if $profile->checkPrivilege('cms_admin')}
                <li><a href="{url path='admin_cms'}"><i class="fa fa-book fa-fw"></i> CMS</a></li>
                {/if}
                {if $profile->checkPrivilege('media_admin')}
                <li><a href="{url path='admin_media'}"><i class="fa fa-file-image-o fa-fw"></i> Mediathek</a></li>
                {/if}
                {if $profile->checkPrivilege('configuration_admin')}
                <li><a href="{url path='admin_configuration'}"><i class="fa fa-question-circle fa-fw"></i> Konfiguration</a></li>
                {/if}
                {if $profile->checkPrivilege('coupon_admin')}
                <li><a href="{url path='admin_coupons'}"><i class="fa fa-eur fa-fw"></i> Gutscheine</a></li>
                <li><a href="{url path='admin_used_coupons'}"><i class="fa fa-eur fa-fw"></i> Gutscheine (Used)</a></li>
                {/if}
                {if $profile->checkPrivilege('insurance_admin')}
                <li><a href="{url path='admin_insurances'}"><i class="fa fa-ambulance fa-fw"></i> Versicherungen</a></li>
                {/if}
                {if $profile->checkPrivilege('medicalspecialty_admin')}
                <li><a href="{url path='admin_medical_specialties'}"><i class="fa fa-heart fa-fw"></i> Fachrichtungen</a></li>
                {/if}
                {if $profile->checkPrivilege('treatmenttype_admin')}
                <li><a href="{url path='admin_treatment_types'}"><i class="fa fa-heart-o fa-fw"></i> Behandlungsarten</a></li>
                {/if}
                {if $profile->checkPrivilege('review_admin')}
                <li><a href="{url path='admin_reviews'}"><i class="fa fa-bar-chart fa-fw"></i> Reviews</a></li>
                {/if}
                {if $profile->checkPrivilege('district_admin')}
                <li><a href="{url path='admin_districts'}"><i class="fa fa-map fa-fw"></i> Districts</a></li>
                {/if}
                {if $profile->checkPrivilege('contact_admin')}
                <li><a href="{url path='admin_contacts'}"><i class="fa fa-envelope fa-fw"></i> Kontakt</a></li>
                {/if}
                {if $profile->checkPrivilege('mailing_admin')}
                <li><a href="{url path='admin_mailings'}"><i class="fa fa-mail-forward fa-fw"></i> Mailings</a></li>
                {/if}
                {if $profile->checkPrivilege('login_history_admin')}
                <li><a href="{url path='admin_login_history'}"><i class="fa fa-list fa-fw"></i> Login History</a></li>
                {/if}
                {if $profile->checkPrivilege('message_admin')}
                <li><a href="{url path='admin_messages'}"><i class="fa fa-question-circle fa-fw"></i> Messageboard</a></li>
                {/if}
                {if $profile->checkPrivilege('blog_admin')}
                <li><a href="{url path='admin_news'}"><i class="fa fa-newspaper-o fa-fw"></i> News</a></li>
                {/if}
                {if $profile->checkPrivilege('widget_admin')}
                <li><a href="{url path='admin_widgets'}"><i class="fa fa-question-circle fa-fw"></i> Widgets</a></li>
                {/if}
                {if $profile->checkPrivilege('searchresulttext_admin')}
                <li><a href="{url path='admin_seo'}"><i class="fa fa-question-circle fa-fw"></i> Suchergebnis-SEO</a></li>
                {/if}
                {if $profile->checkPrivilege('blacklist_admin')}
                <li><a href="{url path='admin_blacklists'}"><i class="fa fa-outdent fa-fw"></i> Blacklist</a></li>
                {/if}
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>
