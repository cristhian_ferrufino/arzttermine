<form method="post" action="{$this->url_form_action}" name="editform" enctype="multipart/form-data">
{if $this->hidden_id}<input type="hidden" name="id" value="{$this->hidden_id}">{/if}
<input type="hidden" name="action" value="{$this->action}">
<input type="hidden" name="action_editor" value="1">
{$this->hidden_values}
<table class="table table-striped">
	<tr class="columns">
		<th><img src="{$this->img_lang}"> Edit News: {$this->title}</th>
	</tr>
	<tr>
		<td>{$this->editor}</td>
	</tr>
	{if $this->id_field_old}<input type="hidden" name="form_data[{$this->id_field_old}]" value="{$this->id}">{/if}

	<tr class="form_buttons">
		<td>
			<div><input type="submit" value="{"save"|trans}" name="send_button" class="button-primary"></form></div>
			<div>
			<form method="post" action="{$this->url_form_action}" name="editform" enctype="multipart/form-data">
			{if $this->hidden_id}<input type="hidden" name="id" value="{$this->hidden_id}">{/if}
			<input type="hidden" name="action" value="{$this->action}">
			{$this->hidden_values}
			{$this->hidden_value_unchanged_content}
			<input type="submit" value="{"cancel"|trans}" name="send_button" class="button-secondary">
			</form></div>
		</td>
	</tr>
</table>
