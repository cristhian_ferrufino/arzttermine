{strip}
{$this->addJsInclude($this->getStaticUrl('jquery/plugins/jquery.simplyCountable.js'))}
{/strip}<input type="text" id="form_data_{$this->form_data.name}" class="form_text{if $this->form_data.message} input_error{/if}" name="form_data[{$this->form_data.name}]" value="{$this->form_data.value}"{$this->form_data.optional}><span id="form_data_{$this->form_data.name}_counter" style="margin-left:1em;"></span>
{$this->form_data.message}
{literal}
<script type="text/javascript">
/* <![CDATA[ */
AT._defer(function(){

	$(function() {
{/literal}
		$('#form_data_{$this->form_data.name}').simplyCountable({literal}{
			counter: '#form_data_{/literal}{$this->form_data.name}{literal}_counter',
			countType: 'characters',
			strictMax: true,
			maxCount: 250,
			countDirection: 'up'
		});
	});

});
/* ]]> */
</script>
{/literal}
