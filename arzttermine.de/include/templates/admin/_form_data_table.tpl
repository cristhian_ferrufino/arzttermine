<table class="table table-striped data-table">
    {section name=mysec loop=$data_array}
        {assign var="data" value=$data_array[mysec]}
        {if !isset($data_custom_keys) || (isset($data_custom_keys) && $data.key|in_array:$data_custom_keys)}
            {if !$data.hidden}
                <tr id="form_row_{$data.key}" class="search_key{if $data.hide} hidden_search_key{/if}"{if $data.hide} style="display:none;"{/if}>
                    {if isset($data.show_th) && $data.show_th}
                        <th>{$data.name}</th>
                        <td>{$data.content}</td>
                    {else}
                        <td colspan="2">{$data.content}</td>
                    {/if}
                </tr>
            {/if}
        {/if}
    {/section}
</table>
