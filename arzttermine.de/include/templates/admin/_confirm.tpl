<div class="panel panel-default">
	<div class="panel-heading">{"are_you_sure"|trans}</div>
	<table class="table table-striped">
    {foreach from=$this->data_array item='data'}
	<tr>
		<th class="nowrap">{$data.key}</th>
		<td>{$data.value}</td>
	</tr>
    {/foreach}
	</table>
	<div class="panel-footer">
		<form action="{$this->url_form_action}" method="post">
			<input type="hidden" name="confirm_id" value="{$this->id_field}">
			<input type="hidden" name="action" value="{$this->action}">
			{$this->hidden_values}
			<input type="submit" value="OK" class="btn btn-primary">
			<a href="{$this->url_cancel}"><button class="btn btn-default" type="button">{"cancel"|trans}</button></a>
		</form>
	</div>
</div>
