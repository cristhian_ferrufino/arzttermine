{include file="admin/header.tpl"}

<!-- CONTENT -->

<div id="bd" class="content_full">

	<div id="content-top"></div>
	<div id="content-main">

{$this->Content}

	</div>
	<div id="content-bottom"></div>
</div>

<!-- END: CONTENT -->

{include file="admin/footer.tpl"}
