{assign var="template_body_class" value="admin"}
{include file="admin/iframe_header.tpl"}

<!-- CONTENT -->

<div id="bd" class="iframe_full">

	<div id="content-main">

{$this->ContentMain}

	</div>
</div>

<!-- END: CONTENT -->

{include file="admin/iframe_footer.tpl"}
