{$this->_list_navigation}

<table class="table table-striped" id="admin_media">
	<thead>
	<tr class="columns">
		<th class="nowrap" colspan="2">{$this->action_buttons}</th>
	</tr>
	</thead>

	<tbody>
	{foreach from=$this->data_array item='data'}
	<tr>
		<td class="media_column">
		{if $data.url_edit}<a href="{$data.url_edit}">{/if}{$data.media}{if $data.url_edit}</a>{/if}
		</td>
		<td class="data_column">
			<table>
			{foreach from=$data.keys item='item'}
				<tr>
					<td style="white-space: nowrap;">
						<a href="{$item.url_order}" title="Order by {$item.name}">{$item.name}</a>
					</td>
					<td>{if $item.ordered_by_this}
						<img src="{$this->img_order_direction}" class="direction" />
						{/if}
					</td>
					<td>{$item.content}</td>
				</tr>
			{/foreach}
				<tr>
					<td style="border-bottom-width: 0px;">&nbsp;</td>
					<td width="12" style="border-bottom-width: 0px;"></td>
					<td style="border-bottom-width: 0px;">&nbsp;{$data.item_buttons}</td>
				</tr>
			</table>
		</td>
	</tr>
{/foreach}
	</tbody>

	<tfoot>
	<tr class="columns"><th class="nowrap" colspan="2">{$this->action_buttons}</th></tr>
	</tfoot>
</table>
