{* Including CSS here is shitty but works, so this is what we'll do *}
<link rel="stylesheet" type="text/css" href="/static/css/profile-page.min.css?cv=0506-0">
<link rel="stylesheet" type="text/css" href="/static/css/doc-calendar.min.css" />
<link rel="stylesheet" type="text/css" href="/static/css/plugin/fullcalendar.css" />

<div id="planner-actions" class="admin-planner">
    <div class="dropdown">
        <label>Praxen</label>
        <select class="hyperlink" id="location-select">
            <option value="">Bitte wählen&hellip;</option>
            {foreach from=$locations item="location"}
                {* @todo Update path to use routing when available *}
                <option value="{url path="admin_planners"}?location_id={$location->getId()}" {if $location->getId() == $practice->getId()}selected{/if}>{$location->getName()} [{$location->getId()}]</option>
            {/foreach}
        </select>
        {if $practice->isValid()}
        <a href="{url path='practice_no_insurance' params=['slug'=>$practice->getSlug()]}" target="_blank">Praxisprofilseite</a>
        {/if}
    </div>
    <div class="dropdown">
        <label>Ärzte</label>
        <select class="hyperlink">
            <option value="">Bitte wählen&hellip;</option>
            {if $users|count}
            {foreach from=$users item="user"}
                <option value="{url path="admin_planners"}?location_id={$practice->getId()}&user_id={$user->getId()}" {if $user->getId() == $doctor->getId()}selected{/if}>{$user->getName()}</option>
            {/foreach}
            {/if}
        </select>
        {if $doctor->isValid()}
            <a href="{url path='doctor_profile_no_insurance' params=['slug'=>$doctor->getSlug()]}" target="_blank">Arztprofilseite</a>
        {/if}
    </div>
    {if $doctor->isValid() && $practice->isValid()}
    <br/>

    <h3>Terminregeln</h3>
    <div class="action" id="booking-rules">
        <div class="dropdown">
            <label>Terminvorlaufzeit:</label>
            <select id="lead-in">
                <option value="0" {if $planner->getLeadIn() === 0}selected{/if}>Keine</option>
                <option value="1" {if $planner->getLeadIn() === 1}selected{/if}>1 Tag</option>
                <option value="2" {if $planner->getLeadIn() === 2}selected{/if}>2 Tage</option>
                <option value="3" {if $planner->getLeadIn() === 3}selected{/if}>3 Tage</option>
                <option value="4" {if $planner->getLeadIn() === 4}selected{/if}>4 Tage</option>
                <option value="5" {if $planner->getLeadIn() === 5}selected{/if}>5 Tage</option>
                <option value="6" {if $planner->getLeadIn() === 6}selected{/if}>6 Tage</option>
                <option value="7" {if $planner->getLeadIn() === 7}selected{/if}>7 Tage</option>
            </select>
        </div>
        <div class="dropdown">
            <label>Terminverfügbarkeit:</label>
            <select id="search-period">
                <option value="0" {if $planner->getSearchPeriod() === 0}selected{/if}>Kein Limit</option>
                <option value="30" {if $planner->getSearchPeriod() === 30}selected{/if}>1 Monat</option>
                <option value="60" {if $planner->getSearchPeriod() === 60}selected{/if}>2 Monate</option>
                <option value="90" {if $planner->getSearchPeriod() === 90}selected{/if}>3 Monate</option>
            </select>
        </div>
    </div>
    
    <div class="buttons">
        <a href="#" class="button save btn btn-primary" id="save-planner">Speichern</a>
        <a href="#" class="button save btn btn-primary" id="generate-planner" data-generate="1">Speichern und veröffentlichen</a>
    </div>
    {/if}
</div>

{if $doctor->isValid() && $practice->isValid()}
    {include file='doctor/dashboard/planner/calendar.tpl'}
{else}
    <div id="dashboard-notice">
        <p>Bitte Praxis und Arzt wählen</p>
    </div>
{/if}

<script>
    var plannerBlocks = {json_encode($blocks)};
</script>
<script type="text/javascript">
    AT._defer(function() {
        $(document).ready(function() {
            $("#location-select").chosen();
        });
    });
</script>