<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="{$app->language}">
<head>
{*<base href="{$this->base}"></base>*}
<title>{$this->getHeadTitle()} - {$CONFIG.SYSTEM_COMPANY_NAME}</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" href="{$this->getStaticUrl('old-css/reset-min.css')}" type="text/css" media="all" />
<link rel="stylesheet" href="{$this->getStaticUrl('old-css/fonts-min.css')}" type="text/css" media="all" />
<!--[if IE]>
<link rel="stylesheet" href="{$this->getStaticUrl('old-css/iecompat.css')}" type="text/css" media="all" />
<![endif]-->
<link rel="stylesheet" href="{$this->getStaticUrl('old-css/admin_1_0.css')}" type="text/css" media="all" />
{$this->getCssInclude()}
<script type="text/javascript" src="{$this->getStaticUrl('jquery/jquery-1.4.2.min.js')}"></script>
<script type="text/javascript" src="{$this->getStaticUrl('jquery/jquery.easing.1.3.js')}"></script>
<script type="text/javascript" src="{$this->getStaticUrl('js/js_1_0.js')}"></script>
{$this->getJsInclude()}
{$this->getHeadHtml()}
</head>

<body class="no-js{$template_body_class}" id="iframe">
{literal}
<script type="text/javascript">
//<![CDATA[
(function(){
var c = document.body.className;
c = c.replace(/no-js/, 'js');
document.body.className = c;
})();
//]]>
</script>
{/literal}

<div style="text-align: left; margin: auto">

{include file="_status-message.tpl"}
