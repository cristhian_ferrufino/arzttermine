{if $CONFIG.ASSET_ALLOW_MULTIUPLOAD}
{strip}
{$this->addCssInclude($this->getStaticUrl('jquery/plugins/uploadify/css/default.css'))}
{$this->addCssInclude($this->getStaticUrl('jquery/plugins/uploadify/css/uploadify.css'))}
{$this->addJsInclude($this->getStaticUrl('jquery/plugins/uploadify/swfobject.js'))}
{$this->addJsInclude($this->getStaticUrl('jquery/plugins/uploadify/jquery.uploadify.v2.1.0.min.js'))}
{/strip}
{* put ALL parameters into "scriptData" !!! as they will not work in "script" *}
{* ALSO: the flash uploadify.swf MUST be referenced from the same domain as it wont work in most browsers for security reasons *}
{* so dont use getStaticUrl() here but reference it as a relative path with the same domain *}
{* be aware that the flash upload will also fail, if there is a http-auth! *}
<script type="text/javascript">{literal}
$(document).ready(function() {
	$("#uploadify").uploadify({
		'uploader'      : {/literal}'/static/jquery/plugins/uploadify/uploadify.swf',
		'script'        : '{$this->url_upload_script}',
		'cancelImg'     : '/static/jquery/plugins/uploadify/cancel.png',
		'folder'        : 'uploads',
		'fileDataName'  : 'upload',
		'auto'          : false,
		'multi'         : true,
		'scriptData'    : {literal}{{/literal}{$this->scriptData}{literal}},
		'buttonText'    : 'Bilder hochladen...',
		'onAllComplete': function (event, data) {
			var redirecturl;
			redirecturl = '{/literal}{$this->url_upload_script}{literal}&uploading_ok='+data.filesUploaded+'&uploading_errors='+data.errors;
			window.location.href=redirecturl;
		}
	});
});
</script>{/literal}

{* IF JS is activated *}
<div id="upload_form_js" class="hide-if-no-js">
	<input id="uploadify" name="uploadify" type="file" />
	<a href="javascript:$('#uploadify').uploadifyUpload();">Upload Files</a> | <a href="javascript:$('#uploadify').uploadifyClearQueue();">Clear Queue</a>
	<p>Sie können max. {$this->upload_max_filesize} pro Bild hochladen.</p>
</div>

{else}

{* IF JS is deactivated *}
<div id="upload_form_nojs" class="{if $CONFIG.ASSET_ALLOW_MULTIUPLOAD}hide-if-js{/if}">
	<div id="form_container">

		<div class="section box">
			<div class="top"><span class="tl"></span><span class="tr"></span></div>
			<div class="box-content">

				<form action="{$this->url_form_action}" method="post" enctype="multipart/form-data" style="display:inline;" class="with-tips">
					<div class="field"><label for="form_upload">Neues Bild hochladen:</label><input id="form_upload" type="file" name="upload" class="input_text" /></div>
					<input type="hidden" name="mua" value="{$this->action}" />
					<input type="hidden" name="musa" value="save" />
					<input class="input_submit" style="margin-top:20px;" type="submit" value="Hochladen" />
				</form>

				<div class="clear"></div>
			</div>
			<div class="bottom"><span class="bl"></span><span class="br"></span></div>
		</div>

	</div>
</div>
{/if}
