{literal}
<script type='text/javascript'>
/* <![CDATA[ */
var quicktagsL10n = {
	quickLinks: "(Quick Links)",
	wordLookup: "Enter a word to look up:",
	dictionaryLookup: "Dictionary lookup",
	lookup: "lookup",
	closeAllOpenTags: "Close all open tags",
	closeTags: "close tags",
	enterURL: "Enter the URL",
	enterImageURL: "Enter the URL of the image",
	enterImageDescription: "Enter a description of the image"
};
try{convertEntities(quicktagsL10n);}catch(e){};
/* ]]> */
</script>
{/literal}{strip}
{$this->addJsInclude($this->getStaticUrl('wp/utils.js'))}
{$this->addJsInclude($this->getStaticUrl('wp/quicktags.dev.js'))}
{$this->addJsInclude($this->getStaticUrl('wp/editor.js'))}
{$this->addJsInclude($this->getStaticUrl('wp/word-count.js'))}
{$this->addJsInclude($this->getStaticUrl('wp/thickbox/thickbox.js'))}
{$this->addCssInclude($this->getStaticUrl('wp/thickbox/thickbox.css'))}
{$this->addCssInclude($this->getStaticUrl('old-css/admin_cms_editor.css'))}
{$this->addJsInclude($this->getStaticUrl('wp/media-upload.js'))}
{/strip}

<div id="poststuff" style="margin:0em">
<div id="postdivrich" class="postarea">

	<div id="editor-toolbar">
		<div class="zerosize"><input accesskey="e" type="button" onclick="switchEditors.go('{$this->editor_id}')" /></div>
		<a id="edButtonHTML" class="hide-if-no-js" onclick="switchEditors.go('{$this->editor_id}', 'html');">HTML</a>
		<a id="edButtonPreview" class="active hide-if-no-js" onclick="switchEditors.go('{$this->editor_id}', 'tinymce');">Visual</a>
		<div id="media-buttons" class="hide-if-no-js">
			Insert Media <a href='/administration/media/frame' id='add_image' class='thickbox' title='Add an Image'><img src='{$this->getStaticUrl('wp/img/media-button-image.gif')}' alt='Add an Image' /></a>
		</div>
	</div>
	<div id="quicktags"><script type="text/javascript">edToolbar()</script></div>

	<div id='editorcontainer'><textarea rows='12' class='theEditor' cols='40' name='form_data[{$this->form_data.name}]' tabindex='2' id='{$this->editor_id}'>{$this->form_data.value}</textarea></div>

	<script type="text/javascript">
	edCanvas = document.getElementById('{$this->editor_id}');
	</script>

	<table id="post-status-info" cellspacing="0">
	<tbody>
		<tr>
			<td id="wp-word-count"></td>
			<td class="autosave-info">
				<span id="autosave">&nbsp;</span>
				<span id="last-edit">&nbsp;</span>
			</td>
		</tr>
	</tbody>
	</table>

</div>
</div>

{literal}
<script type='text/javascript'>
/* <![CDATA[ */
var wordCountL10n = {
	count: "Word count: %d"
};
try{convertEntities(wordCountL10n);}catch(e){};
var thickboxL10n = {
	next: "Next &gt;",
	prev: "&lt; Prev",
	image: "Image",
	of: "of",
	close: "Close",
	noiframes: "This feature requires inline frames. You have iframes disabled or your browser does not support them."
};
try{convertEntities(thickboxL10n);}catch(e){};
/* ]]> */
</script>

<script type="text/javascript">
/* <![CDATA[ */
tinyMCEPreInit = {
	base : "{/literal}{$this->getStaticUrl('wp/tinymce')}{literal}",
	suffix : "",
	query : "ver=327-1235",
	mceInit : {mode:"specific_textareas", editor_selector:"theEditor", width:"100%", theme:"advanced", skin:"wp_theme", theme_advanced_buttons1:"bold,italic,strikethrough,|,bullist,numlist,blockquote,|,justifyleft,justifycenter,justifyright,|,link,unlink,wp_more,|,spellchecker,fullscreen,wp_adv", theme_advanced_buttons2:"formatselect,underline,justifyfull,forecolor,|,pastetext,pasteword,removeformat,|,media,charmap,|,outdent,indent,|,undo,redo", theme_advanced_buttons3:"", theme_advanced_buttons4:"", language:"en", spellchecker_languages:"+English=en,Danish=da,Dutch=nl,Finnish=fi,French=fr,German=de,Italian=it,Polish=pl,Portuguese=pt,Spanish=es,Swedish=sv", theme_advanced_toolbar_location:"top", theme_advanced_toolbar_align:"left", theme_advanced_statusbar_location:"bottom", theme_advanced_resizing:"1", theme_advanced_resize_horizontal:"", dialog_type:"modal", relative_urls:"", remove_script_host:"", convert_urls:"", apply_source_formatting:"", remove_linebreaks:"1", gecko_spellcheck:"1", entities:"38,amp,60,lt,62,gt", accessibility_focus:"1", tabfocus_elements:"major-publishing-actions", media_strict:"", paste_remove_styles:"1", paste_remove_spans:"1", paste_strip_class_attributes:"all", wpeditimage_disable_captions:"", plugins:"safari,inlinepopups,spellchecker,paste,wordpress,media,fullscreen,wpeditimage,wpgallery,tabfocus", content_css:"/media/wp/tinymce/css/editor-style.css"},
	load_ext : function(url,lang){var sl=tinymce.ScriptLoader;sl.markDone(url+'/langs/'+lang+'.js');sl.markDone(url+'/langs/'+lang+'_dlg.js');}
};
/* ]]> */
</script>

<script type='text/javascript' src='{/literal}{$this->getStaticUrl('wp/tinymce/wp-tinymce.php?c=1&amp;ver=327-1235')}{literal}'></script>

<script type="text/javascript">
/* <![CDATA[ */
tinyMCEPreInit.go();
tinyMCE.init(tinyMCEPreInit.mceInit);
/* ]]> */
</script>
{/literal}
