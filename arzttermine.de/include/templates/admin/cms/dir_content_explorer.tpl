<table id="cms_dir_content_explorer">
<tr>
<td id="cms_dir">
	<table class="table table-striped">
		<tr class="columns">
			<th>{"directory"|trans}</th>
		</tr>
		<tr>
			<td><div class="actionicons">{$this->dir_buttons}</div></td>
		</tr>
		<tr>
			<td style="margin:0px; padding:0px;">{$this->dir_container}</td>
		</tr>
		<tr class="columns">
			<td><div class="actionicons">{$this->dir_buttons}</div></td>
		</tr>
	</table>
</td>

<td id="cms_content">
	<table class="table table-striped">
		<tr class="columns">
			<th>{"content"|trans}</th>
		</tr>
		<tr>
			<td><div class="actionicons">{$this->content_buttons}</div></td>
		</tr>
		<tr>
			<td style="margin:0px; padding:0px;">{$this->content_container}</td>
		</tr>
		<tr>
			<td><div class="actionicons">{$this->content_buttons}</div></td>
		</tr>
	</table>
</td>
</tr>
</table>
