<form action="{$this->url_form_action}" name="edit" method="post" enctype="application/x-www-form-urlencoded">
	<div id="dir_new" class="panel panel-default">
		<div class="panel-heading">
			{"dir_edit"|trans}
		</div>

		<div class="panel-body">

{section name=mysec loop=$this->form_lang}
{strip}
			<div class="panel panel-default">
				<div class="panel-heading"><img src="{$this->getStaticUrl($this->form_lang[mysec].icon)}" /> {$this->form_lang[mysec].lang}</div>
				<table class="table table-striped">
					<tr>
						<th>
							<label for="form_title_{$this->form_lang[mysec].code}">{"dirtitle"|trans}</label>
						</th>
						<td>
							<input id="form_title_{$this->form_lang[mysec].code}" name="form[title_{$this->form_lang[mysec].code}]" class="input_text{$this->hasMessages('FORM_DIR_NEW_TITLE',' input_error',1)}" value="{$this->form($this->form_lang[mysec].title)}" />
							{$this->getHtmlFormMessages('FORM_DIR_NEW_TITLE','field')}
						</td>
					</tr>
					<tr>
						<th>
							<label for="form_name_{$this->form_lang[mysec].code}">{"dirname"|trans}</label>
						</th>
						<td>
							<input id="form_name_{$this->form_lang[mysec].code}" name="form[name_{$this->form_lang[mysec].code}]" class="input_text{$this->hasMessages('FORM_DIR_NEW_NAME_de',' input_error',1)}" value="{$this->form($this->form_lang[mysec].name)}" />
							{$this->getHtmlFormMessages('FORM_DIR_NEW_NAME','field')}
						</td>
					</tr>
				</table>
			</div>
{/strip}
{/section}
		</div>
		<table class="table table-striped">
			<tr>
				<th>
					<label for="form_parent_id">{"subdir_of"|trans}</label>
				</th>
				<td>
					{$this->parent_id_select} {$this->getHtmlFormMessages('MSG_FORM_DIR_NEW_PARENT_ID','field')}
				</td>
			</tr>
		</table>

		<div class="panel-footer">
			<input type="hidden" name="form[id]" value="{$this->form.id}" />
			<input type="hidden" name="a" value="dir_edit" />
			<input type="hidden" name="s" value="ok" />
			<input name="ok" type="submit" class="btn btn-primary" id="ok" tabindex="4" accesskey="o" value="{"ok"|trans}" />
			<a href="{$this->url_cancel}" class="btn btn-default">{"cancel"|trans}</a>
		</div>
	</div>
</form>
