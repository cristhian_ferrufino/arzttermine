<div class="panel panel-default">
	<div class="panel-heading">
		{"do_you_want_to_delete_this_dir"|trans}<br />
		{"you_can_not_restore_this_dir"|trans}
	</div>

	<table class="table table-striped">
{section name=mysec loop=$this->form_lang}
{strip}
		<tr>
			<th class="nowrap">
				<img src="{$this->getStaticUrl($this->form_lang[mysec].icon)}" />
				&nbsp;{$this->form_lang[mysec].dirpath}
			</td>
		</tr>
{/strip}
{/section}
	</table>

	<div class="panel-footer">
		<form action="{$this->url_form_action}" name="edit" method="post" enctype="application/x-www-form-urlencoded">
			<input type="hidden" name="form[id]" value="{$this->form.id}" />
			<input type="hidden" name="a" value="dir_delete" />
			<input type="hidden" name="s" value="ok" />
			<input name="ok" type="submit" class="btn btn-primary" id="ok" tabindex="4" accesskey="o" value="{"ok"|trans}" />
			<a href="{$this->url_cancel}" class="btn btn-default">{"cancel"|trans}</a>
		</form>
	</div>
</div>
