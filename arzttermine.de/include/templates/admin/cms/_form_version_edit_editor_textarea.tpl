<div class="list-group-item">
	<h2>{$this->form_editor.name}</h2>
	<textarea id="form_{$this->form_editor.version_content_id}" name="form[{$this->form_editor.version_content_id}]" class="input_textarea resizable{$this->form_editor.version_content__form_input_class}">{$this->form_editor.version_content}</textarea>
	{$this->form_editor.version_content__message}
</div>
