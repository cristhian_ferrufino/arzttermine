<form action="{$this->url_form_action}" name="edit" method="post" enctype="application/x-www-form-urlencoded">
	<div id="version_new" class="panel panel-default">
		<div class="panel-heading">
			{"version_new"|trans}
		</div>

		<div class="panel-body">
			<div class="list-group">
				<div class="list-group-item">
					<img src="{$this->media_lang}" /> {$this->filename}
				</div>

				{$this->editors}

			</div>
		</div>

		<div class="panel-footer">
			<input type="hidden" name="form[lang]" value="{$this->form.lang}" />{if $this->form.lang__message}{$this->form.lang__message}{/if}
			<input type="hidden" name="form[content_id]" value="{$this->form.content_id}" />{if $this->form.content_id__message}{$this->form.content_id__message}{/if}
			<input type="hidden" name="form[dir_id]" value="{$this->form.dir_id}" />{if $this->form.dir_id__message}{$this->form.dir_id__message}{/if}
			<input type="hidden" name="a" value="version_new" />
			<input type="hidden" name="s" value="ok" />
			<input name="ok" type="submit" class="btn btn-primary" id="ok" tabindex="4" accesskey="o" value="{"ok"|trans}" />
			<a href="{$this->url_cancel}" class="btn btn-default">{"cancel"|trans}</a>
		</div>
	</div>
</form>
