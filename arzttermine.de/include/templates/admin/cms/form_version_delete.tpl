<div class="panel panel-default">
	<div class="panel-heading">
		{"do_you_want_to_delete_this_version"|trans}<br />
		{"you_can_not_restore_this_version"|trans}
	</div>

	<table class="table table-striped">
		<tr>
			<th>
				<img src="{$this->media_lang}" />
			</th>
			<td>
				{$this->filename}
			</td>
		</tr>
		<tr>
			<th>
				id
			</th>
			<td>
				{$this->version_nr}
			</td>
		</tr>
		<tr>
			<th>
				created_at
			</th>
			<td>
				{$this->created_at}
			</td>
		</tr>
	</table>

	<div class="panel-footer">
		<form action="{$this->url_form_action}" name="edit" method="post" enctype="application/x-www-form-urlencoded">
			<input type="hidden" name="form[id]" value="{$this->form.id}" />
			<input type="hidden" name="form[lang]" value="{$this->form.lang}" />
			<input type="hidden" name="form[content_id]" value="{$this->form.content_id}" />
			<input type="hidden" name="a" value="version_delete" />
			<input type="hidden" name="s" value="ok" />
			<input name="ok" type="submit" class="btn btn-primary" id="ok" tabindex="4" accesskey="o" value="{"ok"|trans}" />
			<a href="{$this->url_cancel}" class="btn btn-default">{"cancel"|trans}</a>
		</form>
	</div>
</div>
