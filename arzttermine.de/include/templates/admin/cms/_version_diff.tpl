<div id="diff_result">
{if $this->status}
{$this->status}
{else}

Content: {$this->content->getTitle()}<br />
Language: {$this->lang}<br />
URL: {$this->content->getUrl($this->lang)}<br />

<table class='diff'>
	<col class='ltype' /><col class='content' /><col class='ltype' /><col class='content' />
	<thead>
		<tr class='diff-title'><th colspan='4'>{$this->title}</th></tr>
		<tr class='diff-sub-title'>
			<td></td><th>{$this->title_left}</th>
			<td></td><th>{$this->title_right}</th>
		</tr>
	</thead>

	<tbody>
		{$this->diff}
	</tbody>
</table>

{/if}

</div>
