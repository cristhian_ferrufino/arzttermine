<form action="{$this->url_form_action}" name="new" method="post" enctype="application/x-www-form-urlencoded">
	<div id="content_new" class="panel panel-default">
		<div class="panel-heading">{"content_new"|trans}</div>

		<div class="panel-body">
			{section name=mysec loop=$this->form_lang}
				{strip}
					<div class="panel panel-default">
						<div class="panel-heading"><img src="{$this->getStaticUrl($this->form_lang[mysec].icon)}" /> {$this->form_lang[mysec].lang}</div>
						<table class="table table-striped">
							<tr>
								<td>
									<label for="form_title_{$this->form_lang[mysec].code}">{"title"|trans}</label>
								</td>
								<td>
									<input id="form_title_{$this->form_lang[mysec].code}" name="form[title_{$this->form_lang[mysec].code}]" class="input_text{$this->form_lang[mysec].title__form_input_class}" value="{$this->form_lang[mysec].title}" />{$this->form_lang[mysec].title__message}
								</td>
							</tr>
							<tr>
								<td>
									<label for="form_use_dirpath_{$this->form_lang[mysec].code}">{"use_dirpath"|trans}</label>
								</td>
								<td>
									<input type="checkbox" id="form_use_dirpath_{$this->form_lang[mysec].code}" name="form[use_dirpath_{$this->form_lang[mysec].code}]" class="input_checkbox{$this->form_lang[mysec].use_dirpath__form_input_class}" value="1"{if $this->form_lang[mysec].use_dirpath==1} checked="checked"{/if} /><span class="dirpath">{$this->form_lang[mysec].dirpath}</span>{$this->form_lang[mysec].use_dirpath__message}
								</td>
							</tr>
							<tr>
								<td>
									<label for="form_filename_{$this->form_lang[mysec].code}">{"filename"|trans}</label>
								</td>
								<td>
									<input id="form_filename_{$this->form_lang[mysec].code}" name="form[filename_{$this->form_lang[mysec].code}]" class="input_text{$this->form_lang[mysec].filename__form_input_class}" value="{$this->form_lang[mysec].filename}" />{$this->form_lang[mysec].filename__message}
								</td>
							</tr>
							<tr>
								<td>
									<label for="form_tags_{$this->form_lang[mysec].code}">{"tags"|trans}</label>
								</td>
								<td>
									<input id="form_tags_{$this->form_lang[mysec].code}" name="form[tags_{$this->form_lang[mysec].code}]" class="input_text{$this->form_lang[mysec].tags__form_input_class}" value="{$this->form_lang[mysec].tags}" />{$this->form_lang[mysec].tags__message}
								</td>
							</tr>
							<tr>
								<td>
									<label for="form_description_{$this->form_lang[mysec].code}">{"description"|trans}</label>
								</td>
								<td>
									<input id="form_description_{$this->form_lang[mysec].code}" name="form[description_{$this->form_lang[mysec].code}]" class="input_text{$this->form_lang[mysec].description__form_input_class}" value="{$this->form_lang[mysec].description}" />{$this->form_lang[mysec].description__message}
								</td>
							</tr>
						</table>
					</div>
				{/strip}
			{/section}
		</div>

		<table class="table table-striped">
			<tr>
				<td>
					<label for="form_template">{"template"|trans}</label>
				</td>
				<td>
					<select id="form_template_id" name="form[template_id]" class="input_text{$this->form.template_id__form_input_class}">{html_options values=$this->template_ids output=$this->template_values selected=$this->form.template_id}</select>{$this->form.template_id__message}
				</td>
			</tr>
			<tr>
				<td>
					<label for="form_engine">{"engine"|trans}</label>
				</td>
				<td>
					<select id="form_engine_id" name="form[engine_id]" class="input_text{$this->form.engine_id__form_input_class}">{html_options values=$this->engine_ids output=$this->engine_values selected=$this->form.engine_id}</select>{$this->form.engine_id__message}
				</td>
			</tr>
			<tr>
				<td>
					<label for="form_mimetype">{"mimetype"|trans}</label>
				</td>
				<td>
					<input id="form_mimetype" name="form[mimetype]" class="input_text{$this->form.mimetype__form_input_class}" value="{$this->form.mimetype}" />{$this->form.mimetype__message}
				</td>
			</tr>
		</table>

		<div class="panel-footer">
			<input type="hidden" name="form[dir_id]" value="{$this->form.dir_id}" />{if $this->form.dir_id__message}{$this->form.dir_id__message}{/if}
			<input type="hidden" name="a" value="content_new" />
			<input type="hidden" name="s" value="ok" />
			<input name="ok" type="submit" class="btn btn-primary" id="ok" tabindex="4" accesskey="o" value="{"ok"|trans}" />
			<a href="{$this->url_cancel}" class="btn btn-default">{"cancel"|trans}</a>
		</div>
	</div>
</form>
