{$this->_list_navigation}

<table class="table table-striped" id="{$this->style_id}">
	<thead>
		<tr class="columns">
			<th class="nowrap" colspan="2">{$this->action_buttons}</th>
		</tr>
	</thead>
	<tbody>
    {foreach from=$this->data_array item='data'}
<tr>
	<td class="media_column">
	{$data.media}
	</td>
	<td class="data_column">
		<form enctype="multipart/form-data" method="post" action="/administration/media/frame" class="media-upload-form validate">

		<table>
{foreach from=$data.keys item='item'}
			<tr>
				<td class="data_keys">
					<a href="{$item.url_order}" title="Order by {$item.name}">{$item.name}</a>
				</td>
				<td>
					{if $item.ordered_by_this}<img src="{$this->img_order_direction}" class="direction" />{/if}
				</td>
				<td>{$item.content}</td>
			</tr>
{/foreach}
			<tr>
				<td class="data_keys">
					Alignment
				</td>
				<td>&nbsp;</td>
				<td class="align">
					<input type='radio' name='form[align]' id='image-align-none-{$data.id}' value='none' checked='checked' /><label for='image-align-none-{$data.id}' class='align image-align-none-label'>None</label>
					<input type='radio' name='form[align]' id='image-align-left-{$data.id}' value='left' /><label for='image-align-left-{$data.id}' class='align image-align-left-label'>Left</label><br />
					<input type='radio' name='form[align]' id='image-align-center-{$data.id}' value='center' /><label for='image-align-center-{$data.id}' class='align image-align-center-label'>Center</label>
					<input type='radio' name='form[align]' id='image-align-right-{$data.id}' value='right' /><label for='image-align-right-{$data.id}' class='align image-align-right-label'>Right</label>
				</td>
			</tr>
		</table>
		<p style="margin:1em;">
		<input type='hidden' name='id' value='{$data.id}' />
		<input type='hidden' name='action' value='image_media_send_to_editor' />
		<input type='submit' class='button-primary' name='send' value='Insert into Content' />
		</p>
		</form>

	</td>
</tr>
{/foreach}
	</tbody>

	<tfoot>
	<tr class="columns"><th class="nowrap" colspan="2">{$this->action_buttons}</th></tr>
	</tfoot>
</table>
