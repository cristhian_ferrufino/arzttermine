{section name=mysec loop=$data_array}
    {assign var="data" value=$data_array[mysec]}
    {if !isset($data_custom_keys) || (isset($data_custom_keys) && $data.key|in_array:$data_custom_keys)}
        {if !$data.hidden}
            {$data.content}
        {/if}
    {/if}
{/section}
