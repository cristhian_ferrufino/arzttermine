</div>

<script type="text/javascript" src="{$this->getStaticUrl('js/admin.min.js')}"></script>

<script type="text/javascript" src="{$this->getStaticUrl('js/jquery-migrate-1.2.1.min.js')}"></script>
<script type="text/javascript" src="{$this->getStaticUrl('jquery/plugins/jquery.tooltip.min.js')}"></script>
<script type="text/javascript" src="{$this->getStaticUrl('jquery/plugins/jquery.watermark_3.1.4.min.js')}"></script>
<script type="text/javascript" src="{$this->getStaticUrl('jquery/plugins/jquery.cycle.all.min.js')}"></script>
<script type="text/javascript" src="{$this->getStaticUrl('jquery/plugins/validation-engine/jquery.validation-engine-de.js')}"></script>
<script type="text/javascript" src="{$this->getStaticUrl('jquery/plugins/validation-engine/jquery.validation-engine.js')}"></script>
<script type="text/javascript" src="{$this->getStaticUrl('jquery/plugins/jquery.MetaData.js')}"></script>
<script type="text/javascript" src="{$this->getStaticUrl('fancybox/jquery.fancybox-1.3.4.pack.js')}"></script>
<script type="text/javascript" src="{$this->getStaticUrl('vendor/bower-chosen/chosen.jquery.min.js')}"></script>

{$this->getJsInclude()}

<script type="text/javascript">
    {* This is duplicated code, so remove in a refactor *}
    $(function() {
        $(document).on('change', 'select.hyperlink', function() {
            window.location = this.options[this.selectedIndex].value;
        });
    });

    $(document).ready(function(){
        $.each(AT.q,function(index,f){
            $(f);
        });
    });

    $(function() {
        $('#side-menu').metisMenu();
    });

    // Loads the correct sidebar on window load,
    // collapses the sidebar on window resize.
    // Sets the min-height of #page-wrapper to window size
    $(function() {
        $(window).bind("load resize", function() {
            topOffset = 50;
            width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
            if (width < 768) {
                $('div.navbar-collapse').addClass('collapse');
                topOffset = 100; // 2-row-menu
            } else {
                $('div.navbar-collapse').removeClass('collapse');
            }

            height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
            height = height - topOffset;
            if (height < 1) height = 1;
            if (height > topOffset) {
                $("#page-wrapper").css("min-height", (height) + "px");
            }
        });

        var url = window.location;
        var element = $('ul.nav a').filter(function() {
            return this.href == url || url.href.indexOf(this.href) == 0;
        }).addClass('active').parent().parent().addClass('in').parent();
        if (element.is('li')) {
            element.addClass('active');
        }
    });
</script>
</body>
</html>
