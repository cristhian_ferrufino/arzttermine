<div class="row">

    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-clock-o fa-fw"></i> Buchungen
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div id="chart-bookings-daily"></div>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>

{if $showBookings}
    <div class="col-lg-4">
        <ul class="list-group">
            <li class="list-group-item active"><i class="fa fa-clock-o"></i> Buchungen</li>
            <li class="list-group-item">Neu <span class="badge">{$bookingsCountNew}</span></li>
            <li class="list-group-item">Pending <span class="badge">{$bookingsCountPending}</span></li>
            <li class="list-group-item" style="background-color:#ddd;">Gesamt<span class="badge">{$bookingsCount}</span></li>
        </ul>
    </div>
{/if}

{if $showUsers}
    <div class="col-lg-4">
        <ul class="list-group">
            <li class="list-group-item active"><i class="fa fa-user-md"></i> Ärzte</li>
            <li class="list-group-item">Mit Vertrag <span class="badge">{$usersHaveContract}</span></li>
            <li class="list-group-item">Eingeloggt &lt;24h <span class="badge">{$usersOnlineCount}</span></li>
            <li class="list-group-item">
            {foreach from=$users item=user}
                <a href="{$user->getUrl()}" target="_blank">{$user->getFullname()}</a> <span class="badge">{$user->getLastLoginAtTimeDiff()}</span><br />
            {/foreach}
            </li>
            <li class="list-group-item" style="background-color:#ddd;">Gesamt<span class="badge">{$usersCount}</span></li>
        </ul>
    </div>
{/if}

{if $showContacts}
    <div class="col-lg-4">
        <ul class="list-group">
            <li class="list-group-item active"><i class="fa fa-envelope"></i> Kontakte</li>
            <li class="list-group-item">
                Letzte Nachricht<br />
                <span class="label label-success">{$createdAtDiff}</span> <span class="label label-success">{$createdAtDate}</span><br />
                <a href="{$url}"><strong>{$subject}</strong></a><br />
                {$message}<br />
            </li>
            <li class="list-group-item" style="background-color:#ddd;">Gesamt<span class="badge">{$contactsCount}</span></li>
        </ul>
    </div>
{/if}

</div>

<script type="text/javascript">
    {literal}
    AT._defer(function() {
        $(function () {

            // Get the CSV and create the chart
            $.getJSON('{/literal}{url path='statistic' params=['action'=>'bookings_today_yesterday_lastweek']}{literal}', function (json) {

                $('#chart-bookings-daily').highcharts({
                    title: {
                        text: null
                    },

                    xAxis: {
                        tickInterval: 24 * 3600 * 1000, // one day
                        tickWidth: 0,
                        gridLineWidth: 1,
                        labels: {
                            align: 'left',
                            x: 3,
                            y: -3
                        }
                    },

                    yAxis: [{ // left y axis
                        title: {
                            text: null
                        },
                        labels: {
                            align: 'left',
                            x: 3,
                            y: 16,
                            format: '{value:.,0f}'
                        },
                        showFirstLabel: false
                    }, { // right y axis
                        linkedTo: 0,
                        gridLineWidth: 0,
                        opposite: true,
                        title: {
                            text: null
                        },
                        labels: {
                            align: 'right',
                            x: -3,
                            y: 16,
                            format: '{value:.,0f}'
                        },
                        showFirstLabel: false
                    }],

                    legend: {
                        align: 'left',
                        verticalAlign: 'top',
                        y: 20,
                        floating: true,
                        borderWidth: 0
                    },

                    tooltip: {
                        shared: true,
                        crosshairs: true
                    },

                    plotOptions: {
                        series: {
                            cursor: 'pointer',
                            point: {
                                events: {
                                    click: function (e) {
                                        hs.htmlExpand(null, {
                                            pageOrigin: {
                                                x: e.pageX || e.clientX,
                                                y: e.pageY || e.clientY
                                            },
                                            headingText: this.series.name,
                                            maincontentText: Highcharts.dateFormat('%A, %b %e, %Y', this.x) + ':<br/> ' +
                                            this.y + ' visits',
                                            width: 200
                                        });
                                    }
                                }
                            },
                            marker: {
                                lineWidth: 1
                            }
                        }
                    },

                    series: json
                });
            });

        });
    });
    {/literal}
</script>
