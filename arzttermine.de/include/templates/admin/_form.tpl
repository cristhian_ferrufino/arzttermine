<form method="post" action="{$this->url_form_action}" name="editform" enctype="multipart/form-data">
	{if $this->hidden_id}<input type="hidden" name="id" value="{$this->hidden_id}">{/if}
	<input type="hidden" name="action" value="{$this->action}">
	{$this->hidden_values}
	<fieldset>

		<div class="panel panel-default">
			<div class="panel-heading">{$this->header}
				{if $this->hide_non_searched_keys}
					<a href="#" style="float:right;font-size:90%;" onClick="$('.search_key.hidden_search_key').toggle();$(this).children().toggle();">
						<span>Show all search attributes</span>
						<span style="display: none">Hide non used search attributes</span>
					</a>
				{/if}
			</div>

			{* loop the data_array for hidden fields *}
			{foreach from=$data_array item='data'}
				{if $data.hidden}{$data.hidden}{/if}
			{/foreach}

			{* loop the data_array for visible fields *}
			{if isset($form_data_table)}
				{$form_data_table}
			{else}
				{include file="admin/_form_data_table.tpl"}
			{/if}

			{if $this->id_field_old}<input type="hidden" name="form_data[{$this->id_field_old}]" value="{$this->id}">{/if}

			<div class="panel-footer">
				<button class="btn btn-primary" type="submit" name="send_button">{$this->text_submit}</button>
				<a href="{$this->url_cancel}"><button class="btn btn-default" type="button">{$this->text_cancel}</button></a>
			</div>
		</div>
	</fieldset>
</form>
