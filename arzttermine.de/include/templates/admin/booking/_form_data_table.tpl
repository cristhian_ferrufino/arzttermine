<!-- Nav tabs -->
<ul class="nav nav-tabs">
    <li class="active"><a href="#tab-meta" data-toggle="tab">Buchung</a></li>
    <li><a href="#tab-salesforce" data-toggle="tab">Salesforce</a></li>
    <li><a href="#tab-content" data-toggle="tab">Kommentare</a></li>
    <li><a href="#tab-booking-offers" data-toggle="tab">Terminvorschläge {if $booking_offers|@count > 0}<span class="badge">{$booking_offers|@count}</span>{/if}</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    <div class="tab-pane active" id="tab-meta">

        <div class="row">
            <div class="col-xs-12 col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Stammdaten</h3>
                    </div>
                    <div class="panel-body-custom">
                        {$data_custom_keys = ['id','status','appointment_start_at', 'returning_visitor', 'flexible_time', 'flexible_location', 'source_platform', 'created_at', 'created_by', 'updated_at', 'updated_by','contact_preference']}
                        {include file="admin/_form_data_table.tpl"}
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Patient</h3>
                        <a href="{$url_doctor_review}">send review mail</a> |
			            {$url_doctor_review_google_tracker}
                    </div>
                    <div class="panel-body-custom">
                        {$data_custom_keys = ['gender', 'first_name', 'last_name', 'phone', 'email', 'insurance_id']}
                        {include file="admin/_form_data_table.tpl"}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Arzt</h3>
                    </div>
                    <div class="panel-body-custom">
                        {$data_custom_keys = ['has_contract', 'user_id', 'location_id']}
                        {include file="admin/_form_data_table.tpl"}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tab-pane" id="tab-content">
        {$data_custom_keys = ['comment_intern', 'comment_patient']}
        {include file="admin/_form_data_table.tpl"}
    </div>

    <div class="tab-pane" id="tab-salesforce">
        {$data_custom_keys = ['salesforce_id','salesforce_updated_at']}
        {include file="admin/_form_data_table.tpl"}
    </div>

    <div class="tab-pane" id="tab-booking-offers">
        <div class="row">
            <div class="col-xs-12 col-md-6">
                {$data_custom_keys = ['booking_offers']}
                {include file="admin/_form_data.tpl"}
            </div>
        </div>
    </div>
</div>
