<script type="text/javascript" src="/ngs/js/lib/jquery_1.9.min.js"></script>
<script type="text/javascript" src="/ngs/js/lib/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="/ngs/css/jquery-ui.css">
<style>
    .ui-autocomplete {
        z-index: 100000;
    }
</style>
<form method="post" action="/admin/bookings/index.php" name="editform" enctype="multipart/form-data">

<input type="hidden" name="action" value="integration_mail">
<input type="hidden" name="id" value="{$booking->id}">
<input type="hidden" name="form_data[template_filename]" value="{$template_filename}">
<input type="hidden" name="form_data[user_name]" value="{$user_name}">
<input type="hidden" name="form_data[booking_salutation_full_name]" value="{$booking_salutation_full_name}">

    {$admin_hidden}
<table class="widefat">
	<tr class="columns">
		<th colspan="2">{$title}</th>
	</tr>
	<tr>
		<th>Buchungs-ID</th>
		<td>{$booking->id}</td>
	</tr>
	<tr id="form_row_name">
		<th>Name</th>
		<td>
			<input type="text" id="form_data_name" class="form_text" name="form_data[name]" value="{$booking->getName(false)}" size="50">
		</td>
	</tr>
	<tr id="form_row_email">
		<th>E-Mail</th>
		<td>
			<input type="text" id="form_data_email" class="form_text" name="form_data[email]" value="{$booking->getEmail()}" size="50">
		</td>
	</tr>
	<tr id="form_row_subject">
		<th>Subject</th>
		<td>
			<input type="text" id="form_data_subject" class="form_text" name="form_data[subject]" value="{$subject}" size="50">
		</td>
	</tr>
    <tr id="form_row_subject">
        <th>Date</th>
        <td>
            <input type="text" id="form_data_appointment_date" class="form_text" name="form_data[appointment_date]" value="{$appointment_date}" size="50">
        </td>
    </tr>
    <tr id="form_row_subject">
        <th>Location Name</th>
        <td>
            <input type="text" id="search_location" class="form_text" name="form_data[location_name]" value="{$location_name}" size="50">
        </td>
    </tr>
    <tr id="form_row_subject">
        <th>Location Address</th>
        <td>
            <input type="text" id="location_address"  class="form_text" name="form_data[location_address]" value="{$location_address}" size="50">
        </td>
    </tr>
    <tr id="form_row_subject">
        <th>Time</th>
        <td>
            <input type="text" id="form_data_appointment_time" class="form_text" name="form_data[appointment_time]" value="{$appointment_time}" size="50">
        </td>
    </tr>
	<tr id="form_row_body">
		<th>Mailtext</th>
		<td>
			<textarea id="form_data_body" class="form_textarea" name="form_data[body]" style="width:500px;height:300px;">{$body}</textarea>
		</td>
	</tr>
	<tr class="form_buttons">
		<td colspan="2">
			<div><input type="submit" value="Abschicken" name="send_button" class="button-primary"></form></div>
{*}			<div><a href="#" class="button-secondary" OnClick="parent.jQuery.fancybox.close();">Abbrechen</a></div>{/*}
		</td>
	</tr>
</table>
{literal}

<script>
        $(document).ready(function() {
            $("#search_location").autocomplete({
                source: availableTags,
                select: function(event, ui) {
                    event.preventDefault();
                    var selectedLocation = ui.item;
                    $("#search_location").val(selectedLocation.label);
                    $("#location_address").val(selectedLocation.value);
                    if($("#form_data_subject").val().indexOf("Terminanfrage für")!==-1)
                        $("#form_data_subject").val("Terminanfrage für "+selectedLocation.label);
                },
                focus: function(event, ui) {
                    event.preventDefault();
                    var selectedLocation = ui.item;
                    $("#search_location").val(selectedLocation.label);
                    $("#location_address").val(selectedLocation.value);
                    if($("#form_data_subject").val().indexOf("Terminanfrage für")!==-1)
                        $("#form_data_subject").val("Terminanfrage für "+selectedLocation.label);
                }
            });
        });
</script>

{/literal}
<style>
    .ui-autocomplete {
        max-height: 200px;
        overflow-y: auto;
        /* prevent horizontal scrollbar */
        overflow-x: hidden;
    }
        /* IE 6 doesn't support max-height
         * we use height instead, but this forces the menu to always be this tall
         */
    * html .ui-autocomplete {
        height: 200px;
    }
</style>