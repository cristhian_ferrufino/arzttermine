{if $booking_offers|@count>0}
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Status</th>
                <th>Termin</th>
                <th>Kommentar</th>
            </tr>
        </thead>
        <tbody>
{foreach from=$booking_offers item=booking_offer}
            <tr>
                <td><a href="{$booking_offer->getAdminEditUrl()}">{$booking_offer->getId()}</a></td>
                <td>{$booking_offer->getStatusText()}</td>
                <td>{$booking_offer->getAppointmentStartAt()}</td>
                <td>{$booking_offer->getCommentIntern()}</td>
            </tr>
{/foreach}
        </tbody>
    </table>
</div>
{else}
{* @todo: add button to easily create new booking offers *}
{/if}