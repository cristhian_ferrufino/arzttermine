{strip}
{$this->addJsInclude($this->getStaticUrl('jquery/plugins/jquery-jqgalviewii-admin.js'))}
{$this->addCssInclude($this->getStaticUrl('old-css/admin_gallery_1_0.css'))}
{/strip}

<div class="postbox">
<h3 class="hndle">Gallerie</h3>

<div class="inside field_section">
{$this->form_media_upload}
</div>

<div id="gallery-container">
	<ul id="gallery">
{if $this->assets|@count < 1}
		<li><a href="{$this->asset_dummy_standard}"><img src="{$this->asset_dummy_thumbnail}" alt="Kein Anhang vorhanden" /></a></li>
{else}
    {foreach from=$this->assets item='asset'}
		<li><a href="{$asset->getGfxUrl($smarty.const.ASSET_GFX_SIZE_STANDARD)}"><img src="{$asset->getGfxUrl($smarty.const.ASSET_GFX_SIZE_THUMBNAIL)}" alt="{alt($asset->getDescription())}" asset_id="{$asset->getId()}"{$this->assets_gallery_attr_alturl}{if $this->assets_gallery_show_profile_link} showprofilelink="1"{/if} /></a></li>
    {/foreach}
{/if}
	</ul>
</div>
{literal}
<script type="text/javascript">
AT._defer(function(){
	$(document).ready(function(){
		$("#gallery").jqGalViewII({startWith:{/literal}{$this->gallery_image_start_nr}{literal}});
	});
});
</script>
{/literal}

<div class="clear"></div>
</div>
