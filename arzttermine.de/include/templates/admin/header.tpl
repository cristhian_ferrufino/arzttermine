<!DOCTYPE html>
<html lang="de">
<head>
    <title>{$this->getHeadTitle()}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="content-language" content="{$app->getLanguageCode()}" />
    <meta name="robots" content="no-index, no-follow" />

    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />

    <link href='{$this->getStaticUrl('img/favicon.ico')}' rel='shortcut icon' />
    {* external references first so that we can overwrite them if needed *}
    <link rel="stylesheet" type="text/css" media="all" href="{$this->getStaticUrl('vendor/bower-chosen/chosen.min.css')}" />
    <link rel="stylesheet" type="text/css" media="all" href="{$this->getStaticUrl('css/admin.all.min.css')}" />
{*
    <link rel="stylesheet" type="text/css" media="all" href="{$this->getStaticUrl('jquery/plugins/jquery.tooltip.css')}" />
    <link rel="stylesheet" type="text/css" media="all" href="{$this->getStaticUrl('fancybox/jquery.fancybox-1.3.4.css')}" />
*}
    {$this->getCssInclude()}
    {$this->getHeadHtml()}
</head>

<body class="no-js admin {$this->_('german')}">
{literal}
    <script type="text/javascript">
        (function(){
            var c = document.body.className;
            c = c.replace(/no-js/, 'js');
            document.body.className = c;
        })();

        var AT = AT || {};

        AT.q=[];
        AT._defer=function(f){
            AT.q.push(f);
        };
    </script>
{/literal}

<div id="wrapper">
{include file="admin/_navbar.tpl"}
