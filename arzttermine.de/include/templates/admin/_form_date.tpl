{strip}
{$this->addJsInclude($this->getStaticUrl('jquery/jquery.ui.core.min.js'))}
{$this->addJsInclude($this->getStaticUrl('jquery/jquery.ui.widget.min.js'))}
{$this->addJsInclude($this->getStaticUrl('jquery/jquery.ui.mouse.min.js'))}
{$this->addJsInclude($this->getStaticUrl('jquery/jquery.ui.slider.min.js'))}
{$this->addJsInclude($this->getStaticUrl('jquery/jquery.ui.datepicker.min.js'))}
{$this->addJsInclude($this->getStaticUrl('jquery/i18n/jquery.ui.datepicker-de.js'))}
{$this->addJsInclude($this->getStaticUrl('jquery/plugins/jquery-ui-timepicker-addon-0.5.min.js'))}
{$this->addCssInclude($this->getStaticUrl('jquery/plugins/jquery-ui-timepicker-addon-0.5.css'))}
{$this->addCssInclude($this->getStaticUrl('jquery/layout/smothness/jquery.ui.datepicker.css'))}
{/strip}<input type="text" id="{$this->form_data.name}" name="form_data[{$this->form_data.name}]" value="{$this->form_data.value}"{$this->form_data.optional}>
{$this->form_data.message}
{literal}
<script type="text/javascript">
/* <![CDATA[ */
	$(function() {
{/literal}
		$('#{$this->form_data.name}').datepicker({literal}{
				showWeek:   true,
				dateFormat: 'yy-mm-dd'
			});
		});
/* ]]> */
</script>
{/literal}
