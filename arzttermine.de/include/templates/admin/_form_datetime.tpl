{strip}
{$this->addJsInclude($this->getStaticUrl('jquery/jquery.ui.core.min.js'))}
{$this->addJsInclude($this->getStaticUrl('jquery/jquery.ui.widget.min.js'))}
{$this->addJsInclude($this->getStaticUrl('jquery/jquery.ui.mouse.min.js'))}
{$this->addJsInclude($this->getStaticUrl('jquery/jquery.ui.slider.min.js'))}
{$this->addJsInclude($this->getStaticUrl('jquery/jquery.ui.datepicker.min.js'))}
{$this->addJsInclude($this->getStaticUrl('jquery/i18n/jquery.ui.datepicker-de.js'))}
{$this->addJsInclude($this->getStaticUrl('jquery/plugins/jquery-ui-timepicker-addon-0.5.min.js'))}
{$this->addCssInclude($this->getStaticUrl('jquery/plugins/jquery-ui-timepicker-addon-0.5.css'))}
{$this->addCssInclude($this->getStaticUrl('jquery/layout/smothness/jquery.ui.datepicker.css'))}
{/strip}<input type="text" id="{$this->form_data.name}" name="form_data[{$this->form_data.name}]" value="{$this->form_data.value}"{$this->form_data.optional}><a href="#" style="font-size:0.9em;margin-left:1em;" id="{$this->form_data.name}_clear">clear</a>
{$this->form_data.message}
{literal}
<script type="text/javascript">
/* <![CDATA[ */
AT._defer(function(){

	$(function() {
{/literal}
	$('#{$this->form_data.name}').datetimepicker({literal}{
			showSecond: true,
			showWeek:   true,
			timeFormat: 'hh:mm:ss',
			dateFormat: 'yy-mm-dd'
		});
	});
    $('#{/literal}{$this->form_data.name}{literal}_clear').click(function() {
        $('#{/literal}{$this->form_data.name}{literal}').val('');
	});
});
/* ]]> */
</script>
{/literal}
