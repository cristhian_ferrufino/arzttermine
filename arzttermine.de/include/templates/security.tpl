<section id="security_logos" class="hidden-md-down">
  <h2>Ihre Datensicherheit ist unsere Priorität</h2>
  <div class="row">
    <div class="col offset-md-1 ">
      <div class="row" style="margin-bottom: 2rem">
        <div class="col">
          <a href="https://aws.amazon.com/de/compliance/csa" target="_blank"  title="Compliance CSA">
            <img src="../static/images/assets/security/csa.png" style=" width: 90px;"  class="partners">
          </a>
        </div>
        <div class="col">
          <a href="https://www.comodo.com" target="_blank"  title="Comodo">
            <img src="../static/images/assets/security/comodo.png" style=" width: 90px;"  class="partners">
          </a>
        </div>
        <div class="col">
          <a href="https://aws.amazon.com/de/compliance/iso-9001-faqs" target="_blank"  title="ISO-9001">
            <img src="../static/images/assets/security/iso_9001.png" style=" width: 90px;"  class="partners">
          </a>
        </div>
        <div class="col">
          <a href="https://aws.amazon.com/de/compliance/bsi-c5" target="_blank"  title="BSI-C5">
            <img src="../static/images/assets/security/c5.png" style=" width: 90px;"  class="partners">
          </a>
        </div>
      </div>
    </div>
  </div>
</section>