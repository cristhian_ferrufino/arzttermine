{include file="header.tpl"}

<div id="bd">
	<div class="container_3 content">
		<div class="grid_2 padding-left-20-right-20 padding-top-30">

        {render name='content1' method='render_content'}

        {if $this->contentLeft}
            {$this->contentLeft}
        {/if}

		</div>
		<div class="grid_1 padding-left-10-right-20 padding-top-30">

        {render name='content2' method='render_sidebar'}

        {if $this->contentRight}
            {$this->contentRight}
        {/if}

		</div>
	</div>
	<div class="clear"></div>

</div>

{include file="footer.tpl"}
