<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

use Arzttermine\Cms\CmsContent;

/**
 * Smarty {get_content} function plugin
 *
 * Type:     function<br>
 * Name:     getContent<br>
 * Purpose:  maps to static getContent call from the CMS
 * <br>
 *
 * @version 1.0
 * @param array $params parameters
 * @param object $template template object
 * @return string template|null
 */
function smarty_function_get_content($params, $template)
{
    if (!isset($params['content'])) {
        trigger_error("CMS Utilities: Missing 'content'");
        return '';
    }

    return CmsContent::getCmsContent($params['content'])->getContent();
}