<?php

use Arzttermine\Application\Application;

/**
 * Type:     modifier<br>
 * Name:     transchoice<br>
 * Purpose:  Translates string with numbered replacements (plurals etc)
 * Example:  {"I have %count% thing|I have %count% things"|transchoice:2:[]:de}
 * Note: "%count%" is automatically passed to transChoice
 *
 * @param string $string
 * @param int $count
 * @param array $arguments Label replacements
 * @param string $locale
 *
 * @return string
 */
function smarty_modifier_transchoice($string, $count = 0, $arguments = array(), $locale = null)
{
    /** @var \Symfony\Component\Translation\Translator $translator */
    $translator = Application::getInstance()->container->get('translator');

    return $translator->transChoice($string, $count, array_merge(array('%count%' => $count), $arguments), null, $locale);
}