<?php

use Arzttermine\Application\Application;

/**
 * Smarty plugin
 *
 * This plugin is only for Smarty3
 * @package Smarty
 * @subpackage PluginsFunction
 */

/**
 * Smarty {url} function plugin
 *
 * Type:     function<br>
 * Name:     url<br>
 * Purpose:  Generate URLs using the application router
 * Example:  {url path='route_name' params=['slug'=>'abc'] full=true}
 *
 * @param array $params parameters
 * @param object $template template object
 * @return string
 */
function smarty_function_url($params, $template)
{
    if (empty($params['path'])) {
        trigger_error("URL: missing route");
    }

    $absolute = isset($params['full']) ? (bool)$params['full'] : false;
    $route_params = isset($params['params']) ? $params['params'] : array();

    return Application::getInstance()->container->get('router')->generate($params['path'], $route_params, $absolute);
}