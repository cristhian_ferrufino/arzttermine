<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

use \Arzttermine\Calendar\Calendar;
use Arzttermine\Core\DateTime;

/**
 * Smarty {calendar} function plugin
 *
 * Type:     function<br>
 * Name:     calendar<br>
 * Purpose:  calendar utility function
 * <br>
 *
 * @version 1.0
 * @param array $params parameters
 * @param object $template template object
 * @return string render template|null
 */
function smarty_function_calendar($params, $template)
{
    if (!isset($params['action'])) {
        trigger_error("Calendar Utilities: Missing 'action'");
        return '';
    }   
    
    if (!isset($params['datetime'])) {
        trigger_error("Calendar Utilities: Missing 'datetime'");
        return '';
    }
    
    $output = '';
    
    switch ($params['action']) {
        case 'getWeekdayText':
            $short = isset($params['short']) ? (bool)$params['short'] : null;
            $output = Calendar::getWeekdayText($params['datetime'], $short);
            break;
        case 'getWeekdayDateTimeText':
            $short = isset($params['short']) ? (bool)$params['short'] : null;
            $output = Calendar::getWeekdayDateTimeText($params['datetime'], $short);
            break;
        case 'getDateTimeFormat':
            $format = isset($params['format']) ? $params['format'] : '';
            $output = Calendar::getDateTimeFormat($params['datetime'], $format);
            break;
        case 'getCalendarDaysHtml':
            $date_days = isset($params['date_days']) ? $params['date_days'] : 5;
            $output= Calendar::getCalendarDaysHtml($params['datetime'], $date_days);
            break;
        case 'format':
            $format = !empty($params['format']) ? $params['format'] : DateTime::FORMAT_DATETIME;
            $date = new DateTime($params['datetime']);
            $output = $date->format($format);
            break;
    }
    
    return $output;
}