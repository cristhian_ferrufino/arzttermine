<?php

use Arzttermine\Application\Application;
use Arzttermine\Insurance\Insurance;

/**
 * Smarty plugin
 *
 * This plugin is only for Smarty3
 * @package Smarty
 * @subpackage PluginsFunction
 */

/**
 * Smarty {paginate} function plugin
 *
 * Type:     function<br>
 * Name:     paginate<br>
 * Purpose:  Generate pagination urls
 *
 * @param array $params parameters
 * @param object $template template object
 * @return string
 */
function smarty_function_paginate($params, $template)
{
    if (empty($params['ms'])) {
        trigger_error("Paginate: missing Medical Specialty");
    }

    $app = Application::getInstance();
    $router = $app->container->get('router');
    $translator = $app->container->get('translator');

    // Pagination variables
    $medical_specialty = intval($params['ms']);
    $result_count      = isset($params['count']) ? intval($params['count']) : 0;
    $treatment_type    = !empty($params['tt']) ? intval($params['tt']) : ''; // We want "null", not 0
    $insurance         = isset($params['insurance']) ? intval($params['insurance']) : Insurance::GKV;
    $lat               = isset($params['lat']) ? $params['lat'] : '';
    $lng               = isset($params['lng']) ? $params['lng'] : '';
    $location          = isset($params['location']) ? $params['location'] : '';
    $distance          = isset($params['distance']) ? intval($params['distance']) : 0;
    $district          = !empty($params['district']) ? intval($params['district']) : '';
    $page              = isset($params['page']) ? intval($params['page']) : 1; // The *current* page we're on
    $per_page          = isset($params['per_page']) ? intval($params['per_page']) : $GLOBALS['CONFIG']['SEARCH_PAGINATION_NUM'];
    
    if ($result_count <= $per_page) {
        return '';
    }
    
    // URL params
    $params = array(
        'form' => array(
            'medical_specialty_id' => $medical_specialty,
            'treatmenttype_id'     => $treatment_type,
            'insurance_id'         => $insurance,
            'lat'                  => $lat,
            'lng'                  => $lng,
            'location'             => $location,
            'district_id'          => $district,
            'distance'             => $distance
        )
    );
    
    // Filter empty values, as they're useless
    array_filter($params['form']);
    
    // Generate URL
    $url = $router->generate('search');

    // Widget slug param
    if ($app->isWidget()) {
        $url = $router->generate('widget', array('widget_slug' => $app->getWidgetContainer()->getSlug(), 'parameter_string' => $url));
    }

    $url .= '?' . http_build_query($params);

    return paginate_links(
        array(
            'base'              => $url . '&p=%#%',
            'format'            => '',
            'total'             => ceil($result_count / $per_page), // Force pagination (we don't use individual pages anyway)
            'current'           => $page,
            'add_args'          => '',
            'next_text'         => $translator->trans('mehr Ärzte'),
            'prev_text'         => $translator->trans('zurück'),
            'show_page_numbers' => false
        )
    );
}