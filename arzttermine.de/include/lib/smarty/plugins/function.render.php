<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

use Arzttermine\Application\Application;
use Arzttermine\Cms\CmsContent;

/**
 * Smarty {render} function plugin
 *
 * Type:     function<br>
 * Name:     render<br>
 * Purpose:  renders a template
 * <br>
 *
 * @version 1.0
 * @param array $params parameters
 * @param object $template template object
 * @return string template|null
 */
function smarty_function_render($params, $template)
{
    $app = Application::getInstance();
    $view = $app->container->get('templating');
    
    if (!isset($params['name'])) {
        trigger_error("CMS Utilities: Missing 'name'");
        return '';
    }

    if (!isset($params['method'])) {
        trigger_error("CMS Utilities: Missing 'method'");
        return '';
    }
    
    if (isset($view->content)) {
        $content = $view->content;
    } else {
        $content = CmsContent::getCmsContent($app->container->get('request')->getRequestUri());
    }
    
    return $content->render($params['name'], $params['method']);
}