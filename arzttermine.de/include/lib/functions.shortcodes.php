<?php

use Arzttermine\Cms\CmsContent;

/**
 * *********************************************************
 * SHORTCODES **********************************************
 * *********************************************************
 * 
 * Be aware of: !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * http://codex.wordpress.org/Shortcode_API#Limitations
*/
add_shortcode('mailto','shortcode_mailto');
add_shortcode('facebook','shortcode_facebook');
add_shortcode('caption', 'img_caption_shortcode');
add_shortcode('asset','shortcode_asset');
add_shortcode('include','shortcode_include');
add_shortcode('cms','shortcode_cms');
add_shortcode('sitemapitem','shortcode_sitemapitem');
add_shortcode('sitemap','shortcode_sitemap');

/**
 * shortcode "cms"
 *
 * @param string content
 * @return html
*/
function shortcode_cms($parameters, $text='') {
	$html = '';
	if (isset($parameters['filename'])) {
		$content = new CmsContent();
		$content_array = $content->loadByUrl($parameters['filename']);
		if ($content->isValid()) {
			$html = $content->render('content1','render_content');
		}
	}
	return $html;
}

/**
 * shortcode "include"
 *
 * @param string content
 * @return html
*/
function shortcode_include($parameters, $text='') {
	if (isset($parameters['id'])) {
		if (isset($GLOBALS['CONFIG'][$parameters['id']]) && file_exists($GLOBALS['CONFIG'][$parameters['id']])) {
			include $GLOBALS['CONFIG'][$parameters['id']];
		}
	}
	return '';
}
/**
 * shortcode "asset"
 *
 * @param string content
 * @return html
*/
function shortcode_asset($attr, $text='') {
	// attr "trend" is inserted by the trend object if this shortcut is within a trend
	extract(shortcode_atts(array(
		'id'	=> '',
		'nr'	=> '',
		'class'	=> 'alignleft',
		'title'	=> false,
		'trend_id'	=> '',
		'case_id'	=> '',
		'size'	=> ASSET_GFX_SIZE_STANDARD,
		'src'	=> ''
	), $attr));

	if ($trend_id != '') {
		$_trend = new Trend($trend_id);
		if ($_trend->isValid()) {
			if ($nr == '') {
				$nr = 1;
			}
			$_asset = $_trend->getAssetByNr($nr);
			if (!is_object($_asset) || !$_asset->isValid()) {
				return '';
			} else {
				$_alt = $_asset->description;
				$_asset_profile = $_asset->getChild($size);
				if (!is_object($_asset_profile) || !$_asset_profile->isValid()) return '';
				$_src = $_asset_profile->url;
			}
			if ($_alt == '') {
				$_alt = $_trend->getTitle();
			}
		}
	}
	if ($case_id != '') {
		$_case = new CaseObject($case_id);
		if ($_case->isValid()) {
			if ($nr == '') {
				$nr = 1;
			}
			$_asset = $_case->getAssetByNr($nr);
			if (!is_object($_asset) || !$_asset->isValid()) {
				return '';
			} else {
				$_alt = $_asset->description;
				$_asset_profile = $_asset->getChild($size);
				if (!is_object($_asset_profile) || !$_asset_profile->isValid()) return '';
				$_src = $_asset_profile->url;
			}
			if ($_alt == '') {
				$_alt = $_case->getTitle();
			}
		}
	}
	if ($id != '') {
		$_asset = new Asset($id,'id');
		if ($_asset->isValid()) {
			$_src = $_asset->getUrl();
		}
	}
	if ($src != '') {
		$_src = $src;
	}
	if ($class != '') {
		 $_class = ' class="'.$class.'"';
	}

	if ($_src=='') return '';
	$_asset_id = '';
	if (is_object($_asset) && $_asset->isValid()) {
		$_asset_id = ' asset_id="'.$_asset->id.'"';
	}
	// wrap in <div> to avoid wpautop() to wrap it with <p>
	$_html = '<div class="img"><img src="'.$_src.'" alt="'.$_alt.'"'.$_class.$_asset_id.' /></div>';
	return $_html;
}

/**
 * shortcode "news"
 *
 * @param string content
 * @return html
*/
function shortcode_news($attr, $text='') {
	global $content;

	extract(shortcode_atts(array(
		'id'	=> '',
		'num'	=> '3',
		'categories'=> ''
	), $attr));

#	$news = new News();
#	return $news->getHtmlSidebarTeaser($categories, $num);
	return '';
}

/**
 * shortcode "facebook"
 *
 * @param string content
 * @return html
*/
function shortcode_facebook($parameters, $text='') {
	global $content, $blog, $blogpost;

	$_width='470';
	$_height='26';

	if (isset($content) && $content->isValid()) {
		$content->getUrl();
	}
	if (isset($blog) && $blog->isValid()) {
		$blog->getUrl();
	}
	if (isset($blogpost) && $blogpost->isValid()) {
		$blogpost->getUrl();
	}
	if (isset($parameters['type'])) {
		$_type = $parameters['type'];
	} else {
		$_type = 'like';
	}
	switch ($_type) {
		case 'fan':
			$_html = '<script type="text/javascript" src="http://static.ak.connect.facebook.com/connect.php/de_DE"></script><script type="text/javascript">FB.init("4680cbbef54b715d943b019767a89898");</script><fb:fan profile_id="133028340061969" stream="0" connections="8" logobar="1" width="250"></fb:fan>';
			break;
		default:
		case 'like':
			$_html = '<iframe src="http://www.facebook.com/plugins/like.php?href='.$url.'&amp;layout=standard&amp;show-faces=true&amp;width='.$width.'&amp;action=like&amp;colorscheme=light" scrolling="no" frameborder="0" allowTransparency="true" style="border:none; overflow:hidden; width:'.$width.'px; height:'.$_height.'px"></iframe>';
			break;
	}
	return $_html;
}


/**
 * shortcode "youtube_sc"
 * http://wordpress.org/extend/plugins/youtube-shortcode/
 *
 * @param string content
 * @return html
*/
function shortcode_youtube($atts, $content = null) {
	$default_width  = '560';
	$default_height = '340';
	$default_title = 'YouTube video player';

	$_atts = array(
		// custom parameters
		'url' => '',
		'title' => $default_title, // SEO & WCAG 1.0+
		'width' => $default_width, // minimum: 200
		'height' => $default_height,
		'version' => '3',
		'nocookie' => '0', // privacy mode
		// Youtube's official player parameters & default values
		// Reference: http://code.google.com/intl/en/apis/youtube/player_parameters.html
		'autohide' => '2',
		'controls' => '1',
		'modestbranding' => '0',
		'origin' => '',
		'playlist' => '',
		'rel' => '1',
		'autoplay' => '0',
		'loop' => '0',
		'enablejsapi' => '0',
		'playerapiid' => '',
		'disablekb' => '0',
		'egm' => '0',
		'border' => '0',
		'color' => 'red',
		'color1' => 'b1b1b1',
		'color2' => 'cfcfcf',
		'start' => '',
		'theme' => 'dark',
		'fs' => '0',
		'hd' => '0',
		'showsearch' => '1',
		'showinfo' => '1',
		'iv_load_policy' => '1', // or 3
		'cc_load_policy' => '' // user's account setting. To force subtitles set to 1
	);
	extract( shortcode_atts($_atts, $atts ) );

	// if url attr is not there than it will be the url for sure :)
	if (!isset($url) || $url == '') {
		$url = $atts[0];
	}

	$url	= trim($url);
	$_title	= str_replace(array('%20', '%22', '%27', '%3D', '%5B', '%5D', '%2F'), array(' ', '&quot;', "'", '=', '[', ']', '/'), (string) $title);
	$title	= ($title != $default_title) ? $_title : $default_title;
	$width	= (int) $width < 200 ? '200' : $width;
	$height	= ($width != $default_width && $height == $default_height) ? (string) floor((int) $width / 1.66) : $height;
	//$tag	= in_the_loop() ? 'p' : 'div'; // Add support for Youtube shortcode in posts
	$tag = 'div';

	// get the server host
	$_server_host = 'www.';
	if (preg_match('/^http\:\/\/([a-zA-Z0-9\-\_\.]+\.|)youtube\.com/i', $url, $matches)) {
		$_server_host = $matches[1];
	}
	// get the video id
	$_video_id = 'notfoundtheid';
	if (preg_match('/^http\:\/\/(?:(?:[a-zA-Z0-9\-\_\.]+\.|)youtube\.com\/watch\?v\=|youtu\.be\/)([a-zA-Z0-9\-\_]+)/i', $url, $matches) > 0) {
		$_video_id = $matches[1];
	} else if (preg_match('/^([a-zA-Z0-9\-\_]+)$/i', $url, $matches) > 0) {
		$_video_id = $matches[1];
	}
	$video_url = 'http://'.$_server_host.'youtube' . ((bool) $nocookie ? '-nocookie' : '') . '.com/%s/'.$_video_id.'?version='.$version .
		($autohide !== '2' ? '&amp;autohide=' . $autohide : '') .
		(!(bool) $controls ? '&amp;controls=0' : '') .
		((bool) $modestbranding ? '&amp;modestbranding=1' : '') .
		(!empty($origin) ? '&amp;origin=' . $origin : '') .
		(!empty($playlist) ? '&amp;playlist=' . $playlist : '') .
		(!(bool) $rel ? '&amp;rel=0' : '') .
		((bool) $autoplay ? '&amp;autoplay=1' : '') .
		((bool) $loop ? '&amp;loop=1' : '') .
		((bool) $enablejsapi ? '&amp;enablejsapi=1' : '') .
		(!empty($playerapiid) ? '&amp;playerapiid=' . $playerapiid : '') .
		((bool) $disablekb ? '&amp;disablekb=1' : '') .
		((bool) $egm ? '&amp;egm=1' : '') .
		((bool) $border ? '&amp;border=1' : '') .
		($color != 'red' ? '&amp;color=' . $color : '') .
		($color1 != 'b1b1b1' ? '&amp;color1=0x' . $color1 : '') .
		($color2 != 'cfcfcf' ? '&amp;color2=0x' . $color2 : '') .
		(!empty($start) ? '&amp;start=' . $start : '') .
		($theme != 'dark' ? '&amp;theme=' . $theme : '') .
		((bool) $fs ? '&amp;fs=1' : '') .
		((bool) $hd ? '&amp;hd=1' : '') .
		(!(bool) $showsearch ? '&amp;showsearch=0' : '') .
		(!(bool) $showinfo ? '&amp;showinfo=0' : '') .
		($iv_load_policy === '3' ? '&amp;iv_load_policy=' . $iv_load_policy : '') .
		(!empty($cc_load_policy) ? '&amp;cc_load_policy=' . $cc_load_policy : '');

	$flash_only_url = sprintf($video_url, 'v') . '&amp;hl=de';	// or "en-US", "de-de", etc...
	$flash_or_html5_url = sprintf($video_url, 'embed') . '&amp;wmode=transparent';

	return  '<'.$tag.' class="youtube_sc" style="width:'.$width.'px;height:'.$height.'px;">' .
			'<noscript>' .
				'<style type="text/css">.youtube_sc iframe.yp{display:none;}</style>' .
				'<object width="'.$width.'" height="'.$height.'" title="'.$title.'">' .
					 '<param name="movie" value="'.$flash_only_url.'"></param>' .
					 ((bool) $fs ? '<param name="allowfullscreen" value="true"></param>' : '') .
					 '<param name="allowscriptaccess" value="always"></param>' .
					 '<param name="wmode" value="transparent"></param>' .
					 '<embed class="yp" src="'.$flash_only_url.'" type="application/x-shockwave-flash" wmode="transparent" allowscriptaccess="always" ' . ((bool) $fs ? 'allowfullscreen="true"' : '') . 'width="'.$width.'" height="'.$height.'"></embed>' .
					 '<noembed>' .
					  '<style type="text/css">.youtube_sc{background-color:#000;color:#fff;font-size:12px}.youtube_sc a{color:blue;text-decoration:underline;}.youtube_sc embed.yp{display:none;}</style>' .
   					  'The Adobe Flash Player is required for video playback.<br><a href="http://get.adobe.com/flashplayer/" title="Install from Adobe">Get the latest Flash Player</a>' .
					 '</noembed>' .
				'</object>' .
			'</noscript>' .
			'<iframe title="'.$title.'" class="yp" type="text/html" width="'.$width.'" height="'.$height.'" src="'.$flash_or_html5_url.'" frameborder="0"></iframe>' .
		'</'.$tag.'>';
}

/**
 * shortcode "vimeo"
 * http://wordpress.org/extend/plugins/lux-vimeo-shortcode/
 *
 * @param string content
 * @return html
*/
function shortcode_vimeo($atts, $content=null) {
	$_atts = array(
		'clip_id' 	=> '',
		'width' 	=> '400',
		'height' 	=> '225',
		'title'		=> '1',
		'byline'	=> '1',
		'portrait'	=> '1',
		'color'		=> '',
		'html5' 	=> '1',
	);
	extract(shortcode_atts($_atts, $atts));

	// if url attr is not there than it will be the url for sure :)
	if (!isset($url) || $url == '') {
		$url = $atts[0];
	}
	// get the clip id
	$clip_id = '';
	if (preg_match('/^(?:http\:\/\/|)(?:(?:[a-zA-Z0-9\-\_\.]+\.|)vimeo\.com\/)([a-zA-Z0-9\-\_]+)/i', $url, $matches) > 0) {
		$clip_id = $matches[1];
	} else if (preg_match('/^([a-zA-Z0-9\-\_]+)$/i', $url, $matches) > 0) {
		$clip_id = $matches[1];
	}
	if (empty($clip_id) || !is_numeric($clip_id)) return '<!-- invalid vimeo id -->';
	if ($height && !$atts['width']) $width = intval($height * 16 / 9);
	if (!$atts['height'] && $width) $height = intval($width * 9 / 16);

	return $html5 ?
		"<iframe src='http://player.vimeo.com/video/$clip_id?title=$title&amp;byline=$byline&amp;portrait=$portrait' width='$width' height='$height' frameborder='0'></iframe>" :
		"<object width='$width' height='$height'><param name='allowfullscreen' value='true' />".
    		"<param name='allowscriptaccess' value='always' />".
    		"<param name='movie' value='http://vimeo.com/moogaloop.swf?clip_id=$clip_id&amp;server=vimeo.com&amp;show_title=$title&amp;show_byline=$byline&amp;show_portrait=$portrait&amp;color=$color&amp;fullscreen=1' />".
    		"<embed src='http://vimeo.com/moogaloop.swf?clip_id=$clip_id&amp;server=vimeo.com&amp;show_title=$title&amp;show_byline=$byline&amp;show_portrait=$portrait&amp;color=$color&amp;fullscreen=1' type='application/x-shockwave-flash' allowfullscreen='true' allowscriptaccess='always' width='$width' height='$height'></embed></object>".
    		"<br /><a href='http://vimeo.com/$clip_id'>View on Vimeo</a>.";
}

/**
 * shortcode "mailto"
 * Creates url encoded javascript powered link to email address
 * FROM: http://wordpress.org/extend/plugins/email-spam-protection/
 * 
 * @param array $atts
 * @param string content
 * @return html
*/
function shortcode_mailto($atts, $text='') {
	global $content;

	if (isset($atts['email'])) {
		$_email = $atts['email'];
		if (isset($atts['text'])) {
			$_text = $atts['text'];
		} else {
			$_text = $_email;
		}
		$string = 'document.write(\'<a href="mailto:' . $_email . '">' . $_text . '</a>\')';
		$_html =  '<script type="text/javascript"> // <!-- ' . PHP_EOL . "eval(unescape(decodeURIComponent('";
		$_html .= urlencode($string);
		$_html .= "')))" . PHP_EOL .  '// --> </script>';
		/* if degrading is enabled, create span with text and js to remove text */
		if (0) {
			$_wraps = array(
				array('[', ']'),
				array('{', '}'),
				array(':', ':'),
				array('&lt;', '&gt;'), 
				array('&laquo;', '&raquo;'),
				array('&lsaquo;', '&rsaquo;')
			);
			$wrap = $_wraps[array_rand($_wraps)];
			$at = ' ' . $wrap[0] . 'at' . $wrap[1] . ' ';
			$dot = ' ' . $wrap[0] . 'dot' . $wrap[1] . ' ';
			$text = str_replace('@', $at, $_email);
			$text = str_replace('.', $dot, $text);
			$_html .= '<span class="email_text">' . $text . '</span>'
				  . '<script type="text/javascript"> // <!-- ' . PHP_EOL
				  . 'jQuery(".email_text").each(function() { this.innerHTML = "" } )' . PHP_EOL
				  . ' // --> </script>';
		}
	}
	return $_html;
}

/**
 * The Caption shortcode.
 *
 * Allows a plugin to replace the content that would otherwise be returned. The
 * filter is 'img_caption_shortcode' and passes an empty string, the attr
 * parameter and the content parameter values.
 *
 * The supported attributes for the shortcode are 'id', 'align', 'width', and
 * 'caption'.
 *
 * @since 2.6.0
 *
 * @param array $attr Attributes attributed to the shortcode.
 * @param string $content Optional. Shortcode content.
 * @return string
 */
function img_caption_shortcode($attr, $content = null) {

	// Allow plugins/themes to override the default caption template.
	$output = apply_filters('img_caption_shortcode', '', $attr, $content);
	if ( $output != '' )
		return $output;

	extract(shortcode_atts(array(
		'id'	=> '',
		'align'	=> 'alignnone',
		'width'	=> '',
		'caption' => ''
	), $attr));

	if ( 1 > (int) $width || empty($caption) )
		return $content;

	if ( $id ) $id = 'id="' . esc_attr($id) . '" ';

	return '<div ' . $id . 'class="wp-caption ' . esc_attr($align) . '" style="width: ' . (10 + (int) $width) . 'px">'
	. do_shortcode( $content ) . '<p class="wp-caption-text">' . $caption . '</p></div>';
}

/**
 * sitemap init shortcode
 *
 * [sitemap type="blog"] - init sitemap for blog
 * [sitemap type="index"] - init sitemapindex
 * [sitemap] - get xml
 *
 * @param array $attr
 * @param string $content
 * @return void|string
 */
function shortcode_sitemap($attr, $content = null) {
	$attr = shortcode_atts(array('type'	=> '', ), $attr);

	$sitemap = \Arzttermine\View\Sitemap::factory();
	$sitemap->setSitemapType($attr['type']);

	switch($attr['type']) {
		case 'doctors':
			$sitemap->setDoctors(new \Arzttermine\User\User());
			break;

		case 'locations':
			$sitemap->setLocations(new \Arzttermine\Location\Location());
			break;

		case 'blog':
			$sitemap->setBlogs(new \Arzttermine\Blog\Post());
			break;

		case 'landingpages':
			$sitemap->setLandingpages(new \Arzttermine\Search\Text());
			break;

		case 'core':
			$sitemap->setCore();
			break;

		case 'index':
			$sitemap->setIndex();
			break;

		default:
			return trim($sitemap->getXml());
	}
    
    return '';
}

/**
 * sitemapitem shortcode
 *
 * [sitemap url="/" priority="0.8" frequency="weekly" lastmod="2012-08-28"]
 *
 * @param array $attr
 * @param string $content
 * @return void
 */
function shortcode_sitemapitem($attr, $content = null) {
	$attr = shortcode_atts(
		array(
			'url'	=> '',
			'priority'	=> 0.5,
			'frequency'	=> 'weekly',
			'lastmod'	=> null,
		),
		$attr
	);

	$sitemap = \Arzttermine\View\Sitemap::factory();
	if ($sitemap->getSitemapType() == 'index') {
		$sitemap->setIndexItem($attr['url']);
	} else {
		$sitemap->setSitemapItem(
			$attr['url'],
			$attr['priotrity'],
			$attr['frequency'],
			$attr['lastmod']
		);
	}
}