<?php

/**
 * Sanitizes a comma separated list of numeric ids
 * 
 * @param string
 * @return string
 */
function sanitize_numeric_ids($string) {
	$string = ereg_replace("[^0-9,]", "", $string);
	return $string;
}

/**
 * Sanitize a slug
 *
 * @param string
 * @return string
 */
function sanitize_slug( $slug ) {
	return sanitize_title_with_dashes($slug);
}
/**
 * Sanitize a permalink
 *
 * @param string $str
 * @return string
 */
function sanitize_permalink( $slug ) {
	return sanitize_title($slug);
}
/**
 * Sanitizes a search string for a location
 * e.g. "city, zip, street"
 *
 * @param string $text
 * @return string
 */
function sanitize_location($text) {
	$text = sanitize_title($text);
	// replace unwanted chars into spaces
	$text = preg_replace('/[^a-zA-ZäöüÖÄÜß\d\,\.\- ]/',' ', $text);
	// remove redundant spaces
	$text = preg_replace('/\s\s+/', ' ', $text);
	$text = trim($text);
	return $text;
}
/**
 * Sanitizes a phone number 
 * 
 * @param string str
 * @return string
 */
function sanitize_phonenumber($str) {
	// remove everything which is not digit
	$str = preg_replace('/\D/','', $str);
	return $str;
}
/**
 * Sanitize a string from user input or from the db
 *
 * check for invalid UTF-8,
 * Convert single < characters to entity,
 * strip all tags,
 * remove line breaks, tabs and extra whitre space,
 * strip octets.
 *
 * @since 2.9
 *
 * @param string $str
 * @return string
 */
function sanitize_tags($str) {
	$str = sanitize_text_field($str);
	// remove spaces before a kommata
	$str = preg_replace('/[\s]+,/', ',', $str);
	// remove spaces after a kommata
	$str = preg_replace('/,[\s]+/', ',', $str);
	// trim kommata
	$str = trim($str,',');

	return $str;
}
/**
 * Sanitize a blogcomment from user input
 *
 * check for invalid UTF-8,
 * Convert single < characters to entity,
 * strip all tags,
 * remove line breaks, tabs and extra whitre space,
 * strip octets.
 *
 * @param string $str
 * @return string
 */
function sanitize_blogcomment($str) {
	$filtered = wp_check_invalid_utf8( $str );

	if ( strpos($filtered, '<') !== false ) {
		$filtered = wp_pre_kses_less_than( $filtered );
		$filtered = wp_strip_all_tags( $filtered, true );
	} else {
		 $filtered = trim( preg_replace('/[\t ]+/', ' ', $filtered) );
	}

	$match = array();
	while ( preg_match('/%[a-f0-9]{2}/i', $filtered, $match) )
		$filtered = str_replace($match[0], '', $filtered);

	return $filtered;
}
/**
 * Sanitize a blogcomment from user input
 *
 * check for invalid UTF-8,
 * Convert single < characters to entity,
 * strip all tags,
 * remove line breaks, tabs and extra whitre space,
 * strip octets.
 *
 * @param string $str
 * @return string
 */
function sanitize_textarea_field($str) {
	$filtered = wp_check_invalid_utf8( $str );

	if ( strpos($filtered, '<') !== false ) {
		$filtered = wp_pre_kses_less_than( $filtered );
		$filtered = wp_strip_all_tags( $filtered, true );
	} else {
		 $filtered = trim( preg_replace('/[\t ]+/', ' ', $filtered) );
	}

	$match = array();
	while ( preg_match('/%[a-f0-9]{2}/i', $filtered, $match) )
		$filtered = str_replace($match[0], '', $filtered);

	return $filtered;
}

/**
 * Generates an excerpt from the text
 *
 * The excerpt word amount will be 55 words and if the amount is greater than
 * that, then the string ' [...]' will be appended to the excerpt. If the string
 * is less than 55 words, then the text will be returned as is.
 *
 * @param string $text The excerpt
 * @return string The excerpt.
 */
function getExcerpt($text, $excerpt_length=55, $excerpt_more=' [...]') {
	$text = strip_shortcodes($text);

	$text = str_replace(']]>', ']]&gt;', $text);
	$text = strip_tags($text);
	$words = preg_split("/[\n\r\t ]+/", $text, $excerpt_length + 1, PREG_SPLIT_NO_EMPTY);
	if ( count($words) > $excerpt_length ) {
		array_pop($words);
		$text = implode(' ', $words);
		$text = $text . $excerpt_more;
	} else {
		$text = implode(' ', $words);
	}
	return $text;
}

// *****************************************************
// convert data from database for output in html forms
// to prevent HTML-DOS
function html($html) {
	return htmlentities($html,ENT_NOQUOTES,$GLOBALS['CONFIG']['SYSTEM_CHARSET']);
}
function form($html) {
	return htmlentities($html,ENT_COMPAT,$GLOBALS['CONFIG']['SYSTEM_CHARSET']);
}
function alt($string) {
	return sanitize_attribute($string);
}
// all attributes likes alt="", content="", etc
function sanitize_attribute($string) {
	return htmlentities($string,ENT_COMPAT,$GLOBALS['CONFIG']['SYSTEM_CHARSET']);
}
function url($string) {
	return urlencode($string);
}
function sanitize_url($url) {
	return esc_url($url);
}
function sanitize_textarea_html($text) {
	$html = str_replace("\n",'<br />',sanitize_textarea_field($text));
	return $html;
}
// http://code.google.com/intl/de-DE/apis/analytics/docs/articles/gdataAnalyticsCsv.html
function sanitize_csv_field($text) {
	return str_replace('"','""',$text);
#	return escape_double($text);
}
function message($text) {
	$html = str_replace("\n",'<br />',html($text));
	return $html;
}
function message_tags($text) {
	return ereg_replace("([^>])\n","\\1<br />\n",$text);
}
function newsteaser($text) {
	$html = str_replace("\n",'<br />',$text);
	return $html;
}
function newsdescription($text) {
	$html = str_replace("\n",'<br />',$text);
	return $html;
}
function eventteaser($text) {
	$html = str_replace("\n",'<br />',$text);
	return $html;
}
function eventdescription($text) {
	$html = str_replace("\n",'<br />',$text);
	return $html;
}
function www($link) {
	return htmlentities($link,ENT_NOQUOTES,$GLOBALS['CONFIG']['SYSTEM_CHARSET']);
}
function meta($text) {
	return htmlentities($text,ENT_COMPAT,$GLOBALS['CONFIG']['SYSTEM_CHARSET']);
}
function title($text) {
	return htmlentities($text,ENT_COMPAT,$GLOBALS['CONFIG']['SYSTEM_CHARSET']);
}
function blog($text) {
	$html = str_replace("\n",'<br />',html($text));
	return $html;
}
function event_description($text) {
	$html = str_replace("\n",'<br />',html($text));
	return $html;
}
function escape_double($text) {
	$html = str_replace('"','\"',$text);
	return $html;
}
function remove_cr($text) {
	$html = str_replace("\n",'',$text);
	return $html;
}
// OUT: return a string (for titles, descriptions, names, etc)
function valid_string($text) {
	// delete all not wanted chars
	$text = ereg_replace("[^A-Za-z0-9:.,;/%()[]_ '*#?@\"-]","",$text);
	// delete all multi-whitechars
	$text = preg_replace("/[ ]+/"," ",$text);
	$text = trim($text);
	// if it ends with a single whitechar than delete it and
	// unvalid the string
	if ($text==' ') $text = '';
	return $text;
}
// IN: kilometer
// OUT: text
function km2text($km) {
	if (abs($km) >= 1) {
		return round(abs($km),2).' Kilometer';
	} else {
		return (round(abs($km),2)*1000).' Meter';
	}
}

function confirm($question) {
	return ' onClick="return confirm(\''.$question.'\');"';
}

/**
 * provide some dummy functions for wordpress
 */
function apply_filters($tag, $value, $default="", $error_msg="") {
	return $value;
}

function wp_load_alloptions() {
	return true;
}

function get_option( $setting, $default = false ) {
    $value = null;
	switch ($setting) {
		case 'gmt_offset':
			$value = $GLOBALS['CONFIG']['SYSTEM_GMT_OFFSET'];
			break;
		case 'blog_charset':
			$value = $GLOBALS['CONFIG']['SYSTEM_CHARSET'];
			break;
		default:
			break;
	}
	return $value;
}

/**
 * Merge user defined arguments into defaults array.
 *
 * This function is used throughout WordPress to allow for both string or array
 * to be merged into another array.
 *
 * @since 2.2.0
 *
 * @param string|array $args Value to merge with $defaults
 * @param array $defaults Array that serves as the defaults.
 * @return array Merged user defined values with defaults.
 */
function wp_parse_args( $args, $defaults = '' ) {
	if ( is_object( $args ) )
		$r = get_object_vars( $args );
	elseif ( is_array( $args ) )
		$r =& $args;
	else
		wp_parse_str( $args, $r );

	if ( is_array( $defaults ) )
		return array_merge( $defaults, $r );
	return $r;
}
/**
 * Build URL query based on an associative and, or indexed array.
 *
 * This is a convenient function for easily building url queries. It sets the
 * separator to '&' and uses _http_build_query() function.
 *
 * @see _http_build_query() Used to build the query
 * @link http://us2.php.net/manual/en/function.http-build-query.php more on what
 *		http_build_query() does.
 *
 * @since 2.3.0
 *
 * @param array $data URL-encode key/value pairs.
 * @return string URL encoded string
 */
function build_query( $data ) {
	return _http_build_query( $data, null, '&', '', false );
}
// from php.net (modified by Mark Jaquith to behave like the native PHP5 function)
function _http_build_query($data, $prefix=null, $sep=null, $key='', $urlencode=true) {
	$ret = array();

	foreach ( (array) $data as $k => $v ) {
		if ( $urlencode)
			$k = urlencode($k);
		if ( is_int($k) && $prefix != null )
			$k = $prefix.$k;
		if ( !empty($key) )
			$k = $key . '%5B' . $k . '%5D';
		if ( $v === NULL )
			continue;
		elseif ( $v === FALSE )
			$v = '0';

		if ( is_array($v) || is_object($v) )
			array_push($ret,_http_build_query($v, '', $sep, $k, $urlencode));
		elseif ( $urlencode )
			array_push($ret, $k.'='.urlencode($v));
		else
			array_push($ret, $k.'='.$v);
	}

	if ( NULL === $sep )
		$sep = ini_get('arg_separator.output');

	return implode($sep, $ret);
}

/**
 * Retrieve a modified URL query string.
 *
 * You can rebuild the URL and append a new query variable to the URL query by
 * using this function. You can also retrieve the full URL with query data.
 *
 * Adding a single key & value or an associative array. Setting a key value to
 * emptystring removes the key. Omitting oldquery_or_uri uses the $_SERVER
 * value.
 *
 * @since 1.5.0
 *
 * @param mixed $param1 Either newkey or an associative_array
 * @param mixed $param2 Either newvalue or oldquery or uri
 * @param mixed $param3 Optional. Old query or uri
 * @return string New URL query string.
 */
function add_query_arg() {
	$ret = '';
	if ( is_array( func_get_arg(0) ) ) {
		if ( @func_num_args() < 2 || false === @func_get_arg( 1 ) )
			$uri = $_SERVER['REQUEST_URI'];
		else
			$uri = @func_get_arg( 1 );
	} else {
		if ( @func_num_args() < 3 || false === @func_get_arg( 2 ) )
			$uri = $_SERVER['REQUEST_URI'];
		else
			$uri = @func_get_arg( 2 );
	}

	if ( $frag = strstr( $uri, '#' ) )
		$uri = substr( $uri, 0, -strlen( $frag ) );
	else
		$frag = '';

	if ( preg_match( '|^https?://|i', $uri, $matches ) ) {
		$protocol = $matches[0];
		$uri = substr( $uri, strlen( $protocol ) );
	} else {
		$protocol = '';
	}

	if ( strpos( $uri, '?' ) !== false ) {
		$parts = explode( '?', $uri, 2 );
		if ( 1 == count( $parts ) ) {
			$base = '?';
			$query = $parts[0];
		} else {
			$base = $parts[0] . '?';
			$query = $parts[1];
		}
	} elseif ( !empty( $protocol ) || strpos( $uri, '=' ) === false ) {
		$base = $uri . '?';
		$query = '';
	} else {
		$base = '';
		$query = $uri;
	}

	wp_parse_str( $query, $qs );
	$qs = urlencode_deep( $qs ); // this re-URL-encodes things that were already in the query string
	if ( is_array( func_get_arg( 0 ) ) ) {
		$kayvees = func_get_arg( 0 );
		$qs = array_merge( $qs, $kayvees );
	} else {
		$qs[func_get_arg( 0 )] = func_get_arg( 1 );
	}

	foreach ( (array) $qs as $k => $v ) {
		if ( $v === false )
			unset( $qs[$k] );
	}

	$ret = build_query( $qs );
	$ret = trim( $ret, '?' );
	$ret = preg_replace( '#=(&|$)#', '$1', $ret );
	$ret = $protocol . $base . $ret . $frag;
	$ret = rtrim( $ret, '?' );
	return $ret;
}

/**
 * Converts MySQL DATETIME field to user specified date format.
 *
 * If $dateformatstring has 'G' value, then gmmktime() function will be used to
 * make the time. If $dateformatstring is set to 'U', then mktime() function
 * will be used to make the time.
 *
 * The $translate will only be used, if it is set to true and it is by default
 * and if the $wp_locale object has the month and weekday set.
 *
 * @since 0.71
 *
 * @param string $dateformatstring Either 'G', 'U', or php date format.
 * @param string $mysqlstring Time from mysql DATETIME field.
 * @param bool $translate Optional. Default is true. Will switch format to locale.
 * @return string Date formated by $dateformatstring or locale (if available).
 */
function mysql2date( $dateformatstring, $mysqlstring, $translate = true ) {
	$m = $mysqlstring;
	if ( empty( $m ) )
		return false;

	if ( 'G' == $dateformatstring ) {
		return strtotime( $m . ' +0000' );
	}

	$i = strtotime( $m );

	if ( 'U' == $dateformatstring )
		return $i;

// ak:100709 
//	if ( $translate)
	if (0)
		return date_i18n( $dateformatstring, $i );
	else
	    return date( $dateformatstring, $i );
}
/**
 * Retrieve the plural or single form based on the amount.
 *
 * If the domain is not set in the $l10n list, then a comparison will be made
 * and either $plural or $single parameters returned.
 *
 * If the domain does exist, then the parameters $single, $plural, and $number
 * will first be passed to the domain's ngettext method. Then it will be passed
 * to the 'ngettext' filter hook along with the same parameters. The expected
 * type will be a string.
 *
 * @since 2.8.0
 * @uses $l10n Gets list of domain translated string (gettext_reader) objects
 * @uses apply_filters() Calls 'ngettext' hook on domains text returned,
 *		along with $single, $plural, and $number parameters. Expected to return string.
 *
 * @param string $single The text that will be used if $number is 1
 * @param string $plural The text that will be used if $number is not 1
 * @param int $number The number to compare against to use either $single or $plural
 * @param string $domain Optional. The domain identifier the text should be retrieved in
 * @return string Either $single or $plural translated text
 * 
 * CHANGED: ak:100713
 */
function _n( $single, $plural, $number, $domain = 'default' ) {
	if ($number == 1) return $single;
	else return $plural;
}

/**
 * Displays a human readable HTML representation of the difference between two strings.
 *
 * The Diff is available for getting the changes between versions. The output is
 * HTML, so the primary use is for displaying the changes. If the two strings
 * are equivalent, then an empty string will be returned.
 *
 * The arguments supported and can be changed are listed below.
 *
 * 'title' : Default is an empty string. Titles the diff in a manner compatible
 *		with the output.
 * 'title_left' : Default is an empty string. Change the HTML to the left of the
 *		title.
 * 'title_right' : Default is an empty string. Change the HTML to the right of
 *		the title.
 *
 * @since 2.6
 * @see wp_parse_args() Used to change defaults to user defined settings.
 * @uses Text_Diff
 * @uses WP_Text_Diff_Renderer_Table
 *
 * @param string $left_string "old" (left) version of string
 * @param string $right_string "new" (right) version of string
 * @param string|array $args Optional. Change 'title', 'title_left', and 'title_right' defaults.
 * @return string Empty string if strings are equivalent or HTML with differences.
 */
function wp_text_diff( $left_string, $right_string, $args = null ) {
	$defaults = array( 'title' => '', 'title_left' => '', 'title_right' => '' );
	// ak:20100713
	// $args = wp_parse_args( $args, $defaults );
	if ($args==null) {
		$args = $defaults;
	}

	if ( !class_exists( 'WP_Text_Diff_Renderer_Table' ) )
		require( $GLOBALS['CONFIG']['SYSTEM_FUNCTIONS_WP_DIFF'] );

	$left_string  = normalize_whitespace($left_string);
	$right_string = normalize_whitespace($right_string);

	$left_lines  = explode("\n", $left_string);
	$right_lines = explode("\n", $right_string);

	$text_diff = new Text_Diff($left_lines, $right_lines);
	$renderer  = new WP_Text_Diff_Renderer_Table();
	$diff = $renderer->render($text_diff);

	if ( !$diff )
		return '';

	return $diff;
}
