<?php

use Arzttermine\Application\Application;
use Arzttermine\Media\Asset;

/**
 * Function for asset handling in admin area
 *
 */

function form_gallery($object, $owner_type, $privilege_id, $form, $selected_id=0) {
    $app = Application::getInstance();
    $view = $app->getView();
    $profile = $app->getCurrentUser();
    $request = $app->container->get('request');
    $pathInfo = $request->getPathInfo();

	$_asset = new Asset();
	// if no asset is selected than show the profile_gfx or the last uploaded asset
	$selected_id = (($selected_id==0)?$object->getProfileAssetId():$selected_id);
	$_asset->load($selected_id);
	if (!$_asset->isValid()) {
		$selected_id = 0;
	}
	// default: select the first
	if ($selected_id == 0) {
		$selected_id = $_asset->findKey('parent_id=0 AND owner_id='.$object->getId().' AND owner_type='.$owner_type.' ORDER BY created_at DESC LIMIT 1','id');
	}

	// show fotos of the selected event ******************
	$_assets = $_asset->findObjects('owner_id='.$object->getId().' AND owner_type='.$owner_type.' AND parent_id=0 ORDER BY created_at ASC;');
	if (empty($_assets)) {
		$view->set('asset_dummy_standard',getStaticUrl($GLOBALS['CONFIG']['ASSET_GFX_TYPES'][$owner_type]['SIZES'][ASSET_GFX_SIZE_STANDARD]['default']));
		$view->set('asset_dummy_thumbnail',getStaticUrl($GLOBALS['CONFIG']['ASSET_GFX_TYPES'][$owner_type]['SIZES'][ASSET_GFX_SIZE_THUMBNAIL]['default']));
		$selected_id = 0;
	}
	$_alturl = '';
	$_show_profile_link = false;
	if ($profile->checkPrivilege($privilege_id.'_admin_edit')) {
        $data = array(
            'action'    => 'edit',
            'id'        => $object->getId()
        );
        $queryString =  http_build_query($data);
        $_url = $pathInfo.((strpos($pathInfo, '?') === false) ? '?' : '&').$queryString;
		$_alturl = ' alturl="'.esc_attr($_url).'"';
		$_show_profile_link = true;
	}
	$view->set('assets_gallery_attr_alturl',$_alturl);
	$view->set('assets_gallery_show_profile_link',$_show_profile_link);
	$view->set('gallery_image_start_nr',$selected_id);
	$view->set('assets',$_assets);
	return $view->fetch('admin/_gallery.tpl');
}
// **********************************************************************
function form_media_upload($url, $id, $form) {
    $app = Application::getInstance();
    $view = $app->getView();

	$view->set('action','asset_new');
	$view->set('url_upload_script',$GLOBALS['CONFIG']['URL_HTTP_LIVE'].$url);
	$view->set('scriptData','\''.$GLOBALS['CONFIG']['SYSTEM_SESSION_NAME'].'\':\''.session_id().'\',\'action\':\'edit\',\'id\':\''.$id.'\',\'mua\':\'assets_new\',\'musa\':\'save\'');
	$view->set('form',$form);
	$view->set('url_form_action',$url);
	$view->set('upload_max_filesize',ini_get("upload_max_filesize"));
	return $view->fetch('admin/_gallery_form_media_new.tpl');
}
// **********************************************************************
function check_parameters_media($form) {
    $app = Application::getInstance();
    $view = $app->getView();

	// check the mime-type/extension
	if (isset($_FILES['upload']['tmp_name'])) {
		$_tmp_name=$_FILES['upload']['tmp_name'];
		$_filename=$_FILES['upload']['name'];
		$_pathinfo = pathinfo($_filename);
		if ($_tmp_name=='') {
			$view->addMessage('MSG_GFX_FILE','ERROR',$app->static_text('Der Dateiname ist ungültig!'));
		} elseif (!in_array(mb_strtolower($_pathinfo["extension"]), $GLOBALS["CONFIG"]["ASSET_POSSIBLE_FILEEXTENSIONS"])) {
			$view->addMessage('MSG_GFX_FILE','ERROR',$app->static_text('Die Dateiendung ist ungültig!'));
		} else {
			// save the uploaded asset and create the thumbnail
			$_data['filesize']=$_FILES['upload']['size'];
			if ($_data['filesize']==0) {
				$view->addMessage('MSG_GFX_FILE','ERROR',$app->static_text('Die Dateigröße ist fehlerhaft!'));
			}
		}
	}

    return $form;
}
// **********************************************************************
function get_gallery(\Arzttermine\Core\Basic $object, $privilege_id, $owner_type) {
    $app = Application::getInstance();
    $view = $app->container->get('templating');
    $profile = $app->getCurrentUser();
    $request = $app->container->get('request');
    $pathInfo = $request->getPathInfo();

	$_uploading_ok = getParam('uploading_ok');
	$_uploading_errors = getParam('uploading_errors');
	if ($_uploading_ok || $_uploading_errors) {
		if ($_uploading_errors > 0) {
			$view->addMessage('STATUS','ERROR',$app->static_text('Beim Bilderupload sind Fehler aufgetreten<br />Erfolgreich hochgeladen: '.$_uploading_ok.'<br />Nicht hochgeladen: '.$_uploading_errors));
		} else {
			$view->addMessage('STATUS','NOTICE',$app->static_text('Alle '.$_uploading_ok.' Bilder wurden erfolgreich hochgeladen!'));
		}
	}

	$form = getParam('form');
	$_mediaupload_action = getParam('mua');
	$_mediaupload_sub_action = getParam('musa');
    $data = array(
        'action'    => 'edit',
        'id'        => $object->getId()
    );
    $queryString =  http_build_query($data);
    $_url = $pathInfo.((strpos($pathInfo, '?') === false) ? '?' : '&').$queryString;
    $html='';

	switch ($_mediaupload_action) {
		case 'asset_new':
		case 'assets_new':
			if (!$profile->isValid()) {
				$view->addMessage('STATUS','ERROR','Du musst eingeloggt sein!');
				if ($_mediaupload_action == 'assets_new') {
					// multiupload: send error_code and exit
					echo "0";
					exit;
				}
				break;
			}
			// check if this user is allowed to upload asset
			$_user_can_create_assets = $profile->checkPrivilege($privilege_id.'_admin_media');
			if (!$_user_can_create_assets) {
				$view->addMessage('STATUS','NOTICE',$app->static_text('TXT_PRIVILEGE_NO_ACCESS'));
				if ($_mediaupload_action == 'assets_new') {
					// multiupload: send error_code and exit
					echo "0";
					exit;
				}
				break;
			}
			if ($_mediaupload_sub_action=="save") {
				$_asset_id=0;
				// save the asset to fs
				$form = check_parameters_media($form);
				if ($view->countMessages()) {
					$view->addMessage('STATUS','ERROR',$app->static_text('Beachte bitte die Fehler!'));
					$html .= form_media_upload($_url, 0, $form);
				} else {
					// everything seems fine => store the asset *************************************
					if (isset($_FILES['upload']['tmp_name'])) {
						$_data = array();
						$_data['gallery_id'] = 0;
						$_data['description'] = isset($form['description']) ? $form['description'] : '';
						$_asset = new Asset();
						$_asset->saveNewParent($owner_type, $object->getId(), $_data, $_FILES['upload']);
						if (!$_asset->isValid()) {
							$view->addMessage('STATUS','ERROR',$app->static_text('Beim Speichern ist ein Fehler aufgetreten!'));
							if ($_mediaupload_action == 'assets_new') {
								// multiupload: send error_code and exit
								echo "0";
								exit;
							}
						} else {
							$_asset_id = $_asset->getId();
							$view->addMessage('STATUS','NOTICE',$app->static_text('Das Bild wurde hochgeladen!'));
							if ($_mediaupload_action == 'assets_new') {
								// multiupload: send error_code and exit
								echo "1";
								exit;
							}
						}
					}
					// the profile_asset should be recalculated now
					$object->loadProfileAsset();
				}
			} else {
				// show ONLY the form
				$html .= form_media_upload($_url, $object->getId(), $form);
			}
			break;

		case 'asset_update_description':
			if (!$profile->isValid()) {
				$view->addMessage('STATUS','ERROR',$app->static_text('Du musst eingeloggt sein!'));
				break;
			}
			$_asset = new Asset(getParam("asset_id"));
			if (!$_asset->isValid()) {
				$view->addMessage('STATUS','ERROR',$app->static_text('Das Bild exitiert nicht!'));
			} else {
				// check if this user is allowed to edit the description of this asset
				if ($_asset->getCreatedBy() != $profile->getId() && !$profile->checkPrivilege($privilege_id.'_admin_edit')) {
					$view->addMessage('STATUS','ERROR',$app->static_text('Du kannst nur deine eigenen Assets bearbeiten!'));
				} else {
					// update the properties
					$_asset->setDescription($form["description"]);
					$_asset->setUpdatedAt($app->now());
					$_asset->setUpdatedBy($profile->getId());
					$_asset->save(array("description","updated_at",'updated_by'));
					$view->addMessage('STATUS','NOTICE',$app->static_text('Die Beschreibung wurde geändert!'));
					$_asset_id = $_asset->getId();
				}
			}
			break;

		case 'asset_update_gallery_profile':
		case 'asset_update_profile':
			if (!$profile->isValid()) {
				$view->addMessage('STATUS','ERROR',$app->static_text('Du musst eingeloggt sein!'));
				break;
			}
			$_asset = new Asset(getParam("asset_id"));
			if (!$_asset->isValid() || $_asset->getParentId()!=0) {
				$view->addMessage('STATUS','ERROR',$app->static_text('Das Bild existiert nicht!'));
			} else {
				// check if this user is allowed to set the default asset
				if (!$profile->checkPrivilege($privilege_id.'_admin_edit')) {
					$view->addMessage('STATUS','ERROR',$app->static_text('Du kannst nur deine eigenen Assets festlegen!'));
				} else {
					// update the properties
					$object->setProfileAssetId($_asset->getId());
					$object->setUpdatedAt($app->now());
					$object->setUpdatedBy($profile->getId());
					$object->save(array("profile_asset_id","updated_at",'updated_by'));
					$object->loadProfileAsset();
					$view->addMessage('STATUS','NOTICE',$app->static_text('Das Asset ist nun das neue Profilbild!'));
					$_asset_id = $_asset->getId();
				}
			}
			break;

		case 'asset_delete':
			if (!$profile->isValid()) {
				$view->addMessage('STATUS','ERROR',$app->static_text('Du musst eingeloggt sein!'));
				break;
			}
			$_asset = new Asset(getParam("asset_id"));
			if (!$_asset->isValid()) {
				$view->addMessage('STATUS','ERROR',$app->static_text('Das Bild exitiert nicht!'));
			} else {
				if ($_asset->getCreatedBy() != $profile->getId() && !$profile->checkPrivilege($privilege_id.'_admin_edit')) {
					$view->addMessage('STATUS','ERROR',$app->static_text('Du kannst nur deine eigenen Bilder bearbeiten!'));
				} else {
					// is this the user profile asset?
					if ($_asset->getId() == $object->getProfileAssetId()) {
						$_delete_profile_gfx = true;
					}
					if ($_asset->deleteWithChildren()) {
						$view->addMessage('STATUS','NOTICE',$app->static_text('Das Asset wurde gelöscht!'));
						if (isset($_delete_profile_gfx) && $_delete_profile_gfx) {
							$object->setProfileAssetId(0);
                            $object->setUpdatedAt($app->now());
                            $object->setUpdatedBy($profile->getId());
							$object->save(array("profile_asset_id","updated_at",'updated_by'));
						}
					}
				}
			}
			break;

		case 'gfx_rotate_left':
		case 'gfx_rotate_right':
			if (!$profile->isValid()) {
				$view->addMessage('STATUS','ERROR',$app->static_text('Du musst eingeloggt sein!'));
				break;
			}
			$_asset = new Asset(getParam("asset_id"));
			if (!$_asset->isValid() || $_asset->getParentId()!=0) {
				$view->addMessage('STATUS','ERROR',$app->static_text('Das Bild exitiert nicht!'));
			} else {
				// check if this user is allowed to rotate the gfx
				if (!$profile->checkPrivilege($privilege_id.'_admin_edit')) {
					$view->addMessage('STATUS','ERROR',$app->static_text('Du kannst nur deine eigenen Bilder bearbeiten!'));
				} else {
					$_degrees = ($_mediaupload_action=='gfx_rotate_left')?90:270;
					if ($_asset->rotateOriginGfx($_asset->getId(), $_degrees)) {
						$view->addMessage('STATUS','NOTICE',$app->static_text('Das neue Profilbild wurde festgelegt!'));
					}
					$_asset_id = $_asset->getId();
				}
			}
			break;

		// ************************************************
	}

	$view->set('form_media_upload', form_media_upload($_url, $object->getId(), $form));
    
	return form_gallery($object, $owner_type, $privilege_id, $form, isset($_asset_id) ? $_asset_id : 0);
}
