<?php

use Arzttermine\Application\Application;

/**
 * @param array $source_array
 * @param array $return
 *
 * @return array
 */
function array_flatten($source_array, $return = [])
{
    foreach ($source_array as $v) {
        if (is_array($v)) {
            $return = array_flatten($v, $return);
        } else {
            if (isset($v)) {
                $return[] = $v;
            }
        }
    }

    return $return;
}

function formatBytes($bytes, $precision = 2) {
    $units = array('B', 'KB', 'MB', 'GB', 'TB');

    $bytes = max($bytes, 0);
    $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
    $pow = min($pow, count($units) - 1);
    
    $bytes /= (1 << (10 * $pow)); 

    return round($bytes, $precision) . ' ' . $units[$pow];
}

/**
 * @param int $number Number in Base10
 *
 * @return string
 */
function base36_encode($number)
{
    return base_convert($number, 10, 36);
}

/**
 * @param string $number Number in Base36
 *
 * @return string
 */
function base36_decode($number)
{
    return base_convert($number, 36, 10);
}

/**
 * returns the url for a media
 *
 * @param string $name
 * @param bool $absolute_url = false
 * @param bool $add_timestamp = false
 *                            
 * @return string
 */
function getMediaUrl($name, $absolute_url=false, $add_timestamp=false) {
	$url = '';
	if ($absolute_url) {
		$url .= $GLOBALS['CONFIG']['URL_HTTP_LIVE'];
	}
	$url .= $GLOBALS['CONFIG']['MEDIA_URL'].$name;
	if ($add_timestamp) {
		$_filename = $GLOBALS['CONFIG']['STATIC_PATH'].$name;
		if (file_exists($_filename)) {
			$url .= '?'.filemtime($_filename);
		}
	}
	return $url;
}

/**
 * returns the url for an asset
 *
 * @param string $name
 * @param bool $absoute url = false
 * @param bool $add_timestamp = false
 * @return string
 **/
function getAssetUrl($name, $absolute_url=false, $add_timestamp=false) {
	$url = '';
	if ($absolute_url) {
		$url .= $GLOBALS['CONFIG']['URL_HTTP_LIVE'];
	}
	$url .= $GLOBALS['CONFIG']['ASSET_URL'].$name;
	if ($add_timestamp) {
		$_filename = $GLOBALS['CONFIG']['ASSET_PATH'].$name;
		if (file_exists($_filename)) {
			$url .= '?'.filemtime($_filename);
		}
	}
	return $url;
}

/**
 * returns the url for a static asset
 *
 * @param string $name
 * @param bool $absoute url = false
 * @param bool $add_timestamp = false
 * @return string
 **/
function getStaticUrl($name, $absolute_url=false, $add_timestamp=false) {
	$url = '';
	if ($absolute_url) {
		$url .= $GLOBALS['CONFIG']['URL_HTTP_LIVE'];
	}
	$url .= $GLOBALS['CONFIG']['STATIC_URL'].$name;
	if ($add_timestamp) {
		$_filename = $GLOBALS['CONFIG']['STATIC_PATH'].$name;
		if (file_exists($_filename)) {
			$url .= '?'.filemtime($_filename);
		}
	}
	return $url;
}

/**
 * Returns a _GET or _POST parameter from _REQUEST var
 * 
 * @deprecated Get the field using the Request object instead
 * 
 * @param string $parameter
 * @param mixed $default If paramter is not set than return the default
 *                       
 * @return mixed
 */ 
function getParam($parameter, $default='')
{
    return Application::getInstance()->getRequest()->getParam($parameter, $default);
}
/**
 * checks if a _GET or _POST parameter from _REQUEST var is set
 * 
 * @deprecated Don't use param checking
 * 
 * @param string $parameter
 * 
 * @return bool
 */
function issetParam($parameter)
{
    return Application::getInstance()->getRequest()->hasParam($parameter);
}

/**
 * @deprecated Use a dedicated logger
 * 
 * @param string $type
 * @param string $text
 *
 * @return bool
 */
function debug($type="1", $text="") {
	global $CONFIG, $CONTENT;

	$ok = ($_SERVER["REMOTE_ADDR"]==$CONFIG["SYSTEM_ALLOW_DEBUG_IP"]);
	$ok = ($ok && (getParam("DEBUG")==$type));
	if ($ok && $text!='') {
		echo '<div class="left" style="clear: both; padding: 6px; margin: 2px; background-color:#EE0000; color: #FFFFFF; font-weight: bold;">';
		echo $text.'</div>';
	}
	return $ok;
}

function microtime_float() {
	list($usec, $sec) = explode(" ", microtime());
	return ((float)$usec + (float)$sec);
}

function get_timestamp_from_datetime($string) {
	preg_match('#([0-9]{1,4})-([0-9]{1,2})-([0-9]{1,2}) ([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})#', $string, $matches);
	$string_time = gmmktime($matches[4], $matches[5], $matches[6], $matches[2], $matches[3], $matches[1]);
	return $string_time;
}

// to prevent SQL-DOS
function sqlsave($sql_string) {
    $db = Application::getInstance()->container->get('database');
	if (get_magic_quotes_gpc()) {
		$sql_string = stripslashes($sql_string);
	}
	return mysqli_real_escape_string($db->getLink(), $sql_string);
}
function sqlunsave($sql_string) {
	if (get_magic_quotes_gpc()) {
		$sql_string = stripslashes($sql_string);
	}
	return $sql_string;
}

function getCategoriesText($category_ids, $values, $delimiter=', ', $delimiter_close = '') {
	if (!is_array($category_ids)) {
		// then treat it as a comma separated value
		$category_ids = explode(',',$category_ids);
	}
	$_html = '';
	foreach ($values as $_id=>$_category) {
		if (in_array($_id, $category_ids)) {
            if (!(empty($delimiter_close))) {
                $_html .= $delimiter.$values[$_id].$delimiter_close;
            } elseif ($delimiter == '<li>') {
				$_html .= '<li>'.$values[$_id].'</li>';
			} else {
				if ($_html != '') $_html .= $delimiter;
				$_html .= $values[$_id];
			}
		}
	}
	return $_html;
}

function getCategoriesSelect($category_id, $values, $id, $name='Kategorien') {
	$_categories = explode(',',$category_id);
	$_html = '';

	$_html .= '<table class="table table-striped">';
	$_html .= '<tr class="headers"><th colspan="2">'.$name.'</th></tr>';
	$_class = '';
	foreach ($values as $_id=>$_category) {
		$_html .= '<tr><td width="20"><input type="checkbox" name="form_data['.$id.']['.$_id.']" value="1"'.(in_array($_id, $_categories)?' checked="checked"':'').'></td><td>'.$values[$_id].'</td></tr>';
	}
	$_html .= "</table>\n";
	return $_html;
}

/**
 * Retrieve paginated link for archive post pages.
 *
 * Technically, the function can be used to create paginated link list for any
 * area. The 'base' argument is used to reference the url, which will be used to
 * create the paginated links. The 'format' argument is then used for replacing
 * the page number. It is however, most likely and by default, to be used on the
 * archive post pages.
 *
 * The 'type' argument controls format of the returned value. The default is
 * 'plain', which is just a string with the links separated by a newline
 * character. The other possible values are either 'array' or 'list'. The
 * 'array' value will return an array of the paginated link list to offer full
 * control of display. The 'list' value will place all of the paginated links in
 * an unordered HTML list.
 *
 * The 'total' argument is the total amount of pages and is an integer. The
 * 'current' argument is the current page number and is also an integer.
 *
 * An example of the 'base' argument is "http://example.com/all_posts.php%_%"
 * and the '%_%' is required. The '%_%' will be replaced by the contents of in
 * the 'format' argument. An example for the 'format' argument is "?page=%#%"
 * and the '%#%' is also required. The '%#%' will be replaced with the page
 * number.
 *
 * You can include the previous and next links in the list by setting the
 * 'prev_next' argument to true, which it is by default. You can set the
 * previous text, by using the 'prev_text' argument. You can set the next text
 * by setting the 'next_text' argument.
 *
 * If the 'show_all' argument is set to true, then it will show all of the pages
 * instead of a short list of the pages near the current page. By default, the
 * 'show_all' is set to false and controlled by the 'end_size' and 'mid_size'
 * arguments. The 'end_size' argument is how many numbers on either the start
 * and the end list edges, by default is 1. The 'mid_size' argument is how many
 * numbers to either side of current page, but not including current page.
 *
 * It is possible to add query vars to the link by using the 'add_args' argument
 * and see {@link add_query_arg()} for more information.
 *
 * @since 2.1.0
 *
 * @param string|array $args Optional. Override defaults.
 * @return array|string String of page links or array of page links.
 */
function paginate_links( $args = '' ) {
    $defaults = array(
        'base' => '%_%', // http://example.com/all_posts.php%_% : %_% is replaced by format (below)
        'format' => '?page=%#%', // ?page=%#% : %#% is replaced by the page number
        'total' => 1,
        'current' => 0,
        'show_all' => false,
        'prev_next' => true,
        'prev_text' => '&laquo;',
        'next_text' => '&raquo;',
        'end_size' => 1,
        'mid_size' => 2,
        'type' => 'platform',
        'add_args' => false, // array of query args to add
        'add_fragment' => '',
        'show_page_numbers' => true
    );

    $args = wp_parse_args( $args, $defaults );
    extract($args, EXTR_SKIP);

    // Who knows what else people pass in $args
    $total = (int) $total;
    if ( $total < 2 ) {
        return '';
    }
    $current  = (int) $current;
    $end_size = 0  < (int) $end_size ? (int) $end_size : 1; // Out of bounds?  Make it the default.
    $mid_size = 0 <= (int) $mid_size ? (int) $mid_size : 2;
    $add_args = is_array($add_args) ? $add_args : false;
    $r = '';
    $page_links = array();
    $n = 0;
    $dots = false;

    if ( $prev_next && $current && 1 < $current ) :
        $link = str_replace('%_%', 2 == $current ? '' : $format, $base);
        $link = str_replace('%#%', $current - 1, $link);
        if ( $add_args )
            $link = add_query_arg( $add_args, $link );
        $link .= $add_fragment;
        switch ($type) {
            case 'bootstrap':
                $page_links[] = '<li><a href="'.$link.'">'.$prev_text.'</a></li>';
                break;
            case 'platform':
            default :
                $page_links[] = "<a class='prev page-numbers' href='".$link."'><span class='text'>$prev_text</span></a>";
                break;
        }
    endif;
    if ($show_page_numbers) {
        for ( $n = 1; $n <= $total; $n++ ) :
            $n_display = $n;
            // $n_display = number_format_i18n($n);
            if ( $n == $current ) :
                switch ($type) {
                    case 'bootstrap':
                        $page_links[] = '<li class="active"><span>'.$n_display.'<span class="sr-only">(current)</span></span></li>';
                        break;
                    case 'platform':
                    default :
                        $page_links[] = "<span class='page-numbers current'>$n_display</span>";
                        break;
                }
                $dots = true;
            else :
                if ( $show_all || ( $n <= $end_size || ( $current && $n >= $current - $mid_size && $n <= $current + $mid_size ) || $n > $total - $end_size ) ) :
                    $link = str_replace('%_%', 1 == $n ? '' : $format, $base);
                    $link = str_replace('%#%', $n, $link);
                    if ( $add_args ) {
                        $link = add_query_arg( $add_args, $link );
                    }
                    $link .= $add_fragment;
                    switch ($type) {
                        case 'bootstrap':
                            $page_links[] = '<li><a href="'.$link.'">'.$n_display.'</a></li>';
                            break;
                        case 'platform':
                        default :
                            $page_links[] = "<a class='page-numbers' href='".$link."'>$n_display</a>";
                            break;
                    }
                    $dots = true;
                elseif ( $dots && !$show_all ) :
                    switch ($type) {
                        case 'bootstrap':
                            $page_links[] = '<li class="disabled"><span>... <span class="sr-only">(dots)</span></span></li>';
                            break;
                        case 'platform':
                        default :
                            $page_links[] = "<span class='page-numbers dots'>...</span>";
                            break;
                    }
                    $dots = false;
                endif;
            endif;
        endfor;
    }
    if ( $prev_next && $current && ( $current < $total || -1 == $total ) ) :
        $link = str_replace('%_%', $format, $base);
        $link = str_replace('%#%', $current + 1, $link);
        if ( $add_args ) {
            $link = add_query_arg( $add_args, $link );
        }
        $link .= $add_fragment;
        // @todo: put this in a template where the parent element would get a class depending whether
        // it contains only prev, only next or both links. it's important for the justification and
        // a workaround is to add one empty span element.
        // of course, it would be better to have the template for number of other reasons, as well
        switch ( $type ) :
            case 'bootstrap':
                $page_links[] = '<li><a href="'. $link.'">'.$next_text.'</a></li>';
                break;
            default :
                $page_links[] = "<a class='next page-numbers' href='".$link."'><span class='text'>$next_text</span></a>";
                break;
        endswitch;
    endif;

    return join("\n\t", $page_links);
}