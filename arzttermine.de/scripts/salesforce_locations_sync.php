<?php
/*
 * IMPORTANT!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * Whenever you want to fetch NEW custom fields which are NOT in the enterprise.wsdl.xml
 * then you need to create a NEW WSDL file!!!
 * In Salesforce go to Setup->Develop->API->Generate Enterprise WSDL->Button "Generate" and store this
 * file to enterprise.wsdl.xml
 * OR try to just call https://eu1.salesforce.com/soap/wsdl.jsp?type=*&ver_eLoader_4F=1.6&ver_myfaxv3=1.9&ver_SFGA=166.1
 * 
 * Sync the bookings from Salesforce to the backend
 * 1. If there are new bookings in the backend which are not in salesforce the
 *    backend bookings are synced to Salesforce.
 * 2. If there are UPDATED bookings in Salesforce then the booking will be updated
 *    with the Salesforce's content
 *
 * Changes in the backend are pushed to Salesforce immediately.
 * 
 * This scripts runs every 4 minutes on one production machine (web01) via cron
 *
 * Issues:
 * - The server and Salesforce should have almost the same server time to make sure
 *   that both are talking about the same changes
 * - If this script is not running for any reasons or missing a cronjob call that
 *   changes or inserts in Salesforce will not be seen from this script as it checks
 *   only the last CALL_INTERVAL minutes
 *
 * 
 * WARNING:
 * If this script is used in non production (like staging or development) that the upsert()
 * call could update the wrong Backend_ID__c to Salesforce. This link needs to be fixed then!
 * 
 */

use Arzttermine\Application\Application;
use Arzttermine\Booking\Booking;
use Arzttermine\Booking\Offer;
use Arzttermine\Calendar\Calendar;
use Arzttermine\Mail\Mailing;
use Arzttermine\Mail\SMS;
use Arzttermine\MedicalSpecialty\TreatmentType;
use Arzttermine\User\User;
use Arzttermine\Location\Location;
use Arzttermine\Widget\WidgetFactory;

require_once __DIR__ . '/shell.php';

// BEGIN
// *********************************************************************************************************************
show_notice('Attempting to log in to Salesforce', 2, CONSOLE_HEAD_2);

$salesforceInterface = new SalesforceInterface();
$salesforceConnection = $salesforceInterface->getConnection();


show_notice('Syncing locations with Salesforce accounts', 2, CONSOLE_HEAD_2);
_e("Start: " . date('Y-m-d H:i:s'), 2);

$sync_update_start_time = microtime(true);

// now execute actions if checked in SF bookings

executeActions($salesforceConnection);


_e('End: ' . profile_time($sync_update_start_time), 2);

shell_end();



function executeActions(SforceEnterpriseClient $salesforceConnection)
{
    // Action Terminanfrage_an_Patienten_schicken__c
    $query = "SELECT
				Id,
				PKV_Selbstzahler_only__c,
				Backend_ID__c
			FROM
				Account
			WHERE PKV_Selbstzahler_only__c = true";

    $response = $salesforceConnection->query($query);
    foreach ($response->records as $record) {
        if(isset($record->Backend_ID__c)) {
            $location = new Location($record->Backend_ID__c);
        } else {
            $location = new Location($record->Id, 'salesforce_id');
        }

        if ($location->isValid()) {
            $location->setPkvOnly(true);
            $location->save(array('pkv_only'));
            _e("Updated Location: ".$record->Id." - ".$record->Backend_ID__c,2);
        }
    }
    _e('Found records: ' . count($response->records), 2);
}