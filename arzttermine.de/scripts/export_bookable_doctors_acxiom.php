<?php
use Arzttermine\Application\Application;
use League\Flysystem\Sftp\SftpAdapter;
use League\Flysystem\Filesystem;

require_once __DIR__ . '/shell.php';


show_notice("All bookable doctors", 2, CONSOLE_HEAD_2);


$adapter = new SftpAdapter([
    'host' => 'ftp.eu.acxiom.com',
    'port' => 22,
    'username' => 'diagg9',
    'password' => 'Ja4Awpd8Km',
    'privateKey' => __DIR__ . '/acxiom_rsa_key',
    'root' => '/home',
    'timeout' => 10,
    'directoryPerm' => 0755
]);
$filesystem = new Filesystem($adapter);
_e('Connection initialized');


$db = Application::getInstance()->getSql();
$sql = "select users.id as 'ID-PERS', locations.id as 'ID-INST', 'P' as KNZ, if(users.gender=1,'Frau', 'Herr') as ANREDE, TRIM(users.title) as TITLE, users.first_name as VORNAME, users.last_name as NACHNAME, '' as NAMEN_KOMBI, locations.name as INST, '' as STRASSE, '' as HAUSNUMMER, locations.street as STRASSE_KOMBI, locations.zip as PLZ, locations.city as ORT, locations.phone_visible as TEL, 'arzttermine.de' as OTV_NAME, concat('https://www.arzttermine.de/arzt/',users.slug) as OTV_LINK
from users
left join location_user_connections on users.id = location_user_connections.user_id
left join locations on locations.id = location_user_connections.location_id
where users.status=3 
and locations.country_code='DE'
and users.first_name !=''";
$db->query($sql);
_e('Bookings count: ' . $db->nf());

//create CSV file
$headersAdded = false;
$filename = 'acxiom_'.date('Y-m-d').'.csv';
$file = '/tmp/'.$filename;
$fh = @fopen($file, 'w');
foreach($db->getResult() as $result) {

    if($headersAdded === false) {
        fputcsv($fh, array_keys($result),';',chr(0));
        $headersAdded = true;
    }
    fputcsv($fh, $result,';',chr(0));
}
fclose($fh);
file_put_contents($file, str_replace(chr(0), '', file_get_contents($file)));
_e('CSV file '.$filename.' generated');


//upload via sftp tp acxiom server

$filesystem->put($filename, file_get_contents($file));
_e('CSV file '.$filename.' uploaded');

if($filesystem->has($filename)) {
    _e('Upload successful');
} else {
    _e('Upload failed');
}

//echo $filesystem->get($filename)->read();

_e('');

shell_end();