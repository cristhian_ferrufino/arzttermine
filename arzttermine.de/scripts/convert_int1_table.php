<?php

include_once 'shell.php';

require_once $_SERVER['DOCUMENT_ROOT'] . "/../ngs/conf/constants.php";
require_once CLASSES_PATH . "/managers/UsersAaManager.class.php";
require_once CLASSES_PATH . "/managers/Integration1DatasManager.class.php";
require_once CLASSES_PATH . "/managers/LocationsManager.class.php";
require_once CLASSES_PATH . "/managers/DoctorsTreatmentTypesManager.class.php";

$locationsManager = LocationsManager::getInstance();
$locationDoctorsDtos = $locationsManager->getIntegrationLocationsWithDoctors(1);
echo("Total Arzttermine Doctors Count: " . count($locationDoctorsDtos) . "\n");

$usersAaManager = UsersAaManager::getInstance();
$integration1DatasManager = Integration1DatasManager::getInstance();
$doctorsTreatmentTypesManager = DoctorsTreatmentTypesManager::getInstance();



ini_set('display_errors', false);
$allDtos = $integration1DatasManager->selectAll();

$lineNumber = 0;
$totalRowsCount = count($allDtos);

foreach ($allDtos as $dto) {
    $percentDone = intval($lineNumber++ * 100 / $totalRowsCount);
    if ($percentDone != $lastPrintedPercentDone) {
        echo($percentDone . "% done\n");
        $lastPrintedPercentDone = $percentDone;
    }
    $userId = $dto->getUserId();
    $locationId = $dto->getLocationId();
    $weekday = $dto->getWeekday();
    $startAt = $dto->getStartAt();
    $endAt = $dto->getEndAt();
    $blockInMinuts = $dto->getBlockInMinutes();
    $date = $usersAaManager->getFirstComingDateByWeekday($weekday);
    $createdBy = $dto->getCreatedBy();
    $createdAt = $dto->getCreatedAt();
    $updatedBy = $dto->getUpdatedBy();
    $updatedAt = $dto->getUpdatedAt();

    $i1AaDto = $usersAaManager->createEmptyDto();
    $i1AaDto->setUserId($userId);
    $i1AaDto->setLocationId($locationId);
    $i1AaDto->setDate($date);
    $i1AaDto->setStartAt($startAt);
    $i1AaDto->setEndAt($endAt);
    $i1AaDto->setBlockInMinutes($blockInMinuts);
    $i1AaDto->setInsuranceTypes('public,private,cash');
    $ttIdsArray = $doctorsTreatmentTypesManager->getDoctorTreatmentTypesIds($userId);
    $ttIds = implode(',', $ttIdsArray);
    $i1AaDto->setTreatmentTypeIds($ttIds);
    $i1AaDto->setCreatedBy($createdBy);
    $i1AaDto->setCreatedAt($createdAt);
    $i1AaDto->setUpdatedBy($updatedBy);
    $i1AaDto->setUpdatedAt($updatedAt);
    $usersAaManager->addDoctorFixedWeekAppointment($i1AaDto);
}

shell_end();
?>
