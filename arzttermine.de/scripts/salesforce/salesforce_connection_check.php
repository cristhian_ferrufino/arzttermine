<?php
require_once __DIR__.'/../shell.php';

// CONFIG
// *********************************************************************************************************************
ini_set('soap.wsdl_cache_enabled', '0');
setlocale(LC_TIME, 'de_DE');

show_notice('Attempting to log in to Salesforce', 2, CONSOLE_HEAD_2);

$salesforceConnection = null;
// Attempt to login to the salesforce account
try {
	// Create a generic SFI objects
	$salesforceInterface = new SalesforceInterface();
	$salesforceConnection = $salesforceInterface->getConnection();
	_e('OK', 2);


    // send a simple query and check if we get results
    $query = "SELECT
				Id,
				Status__c,
				Backend_ID__c
			FROM
				Patienten__c
			WHERE
				Status__c = 'In Bearbeitung'
			LIMIT 1";

    $response = $salesforceConnection->query($query);
    print_r($response);

} catch (Exception $e) {
	_e('FAILED. Exception: ' . print_r($e, true));
}

shell_end();
