<?php
require_once __DIR__.'/../shell.php';

require_once $_SERVER['DOCUMENT_ROOT'] . "/../ngs/conf/constants.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/../include/lib/interfaces/salesforce/SalesforceInterface.php";

require_once $GLOBALS['CONFIG']['SYSTEM_CLASS_MAILING'];
require_once $GLOBALS['CONFIG']['SYSTEM_CLASS_SMS'];

require_once CLASSES_PATH . "/managers/BookingsManager.class.php";
require_once CLASSES_PATH . "/managers/TreatmentTypesManager.class.php";
require_once CLASSES_PATH . "/managers/LocationsManager.class.php";
require_once CLASSES_PATH . "/managers/UsersManager.class.php";

// CONFIG
// *********************************************************************************************************************
ini_set('soap.wsdl_cache_enabled', '0');
setlocale(LC_TIME, 'de_DE');

// Interval of calling script in minutes
define('CALL_INTERVAL', 5);

// Set the Date threshold under which do not change Bookings
$threshold = date('Y-m-d', strtotime('2013-08-09'));

// Get the time when the script starts
$current_time = mktime();

// Set period of checking for deletions / updates  to the last CALL_INTERVAL minutes
$start_time = $current_time - (60 * CALL_INTERVAL);

/**
 * the end time of the script checking should be set to the time the script starts.
 * Because the end time gets rounded to the bottom we add +60 seconds to the end time
 * so we cover for sure the desired period
 */
$end_time = $current_time + 60;

// BEGIN
// *********************************************************************************************************************

// Managers
$bm  = BookingsManager::getInstance();
$ttm = TreatmentTypesManager::getInstance();
$lm  = LocationsManager::getInstance();
$um  = UsersManager::getInstance();

show_notice('Attempting to log in to Salesforce', 2, CONSOLE_HEAD_2);

$salesforceConnection = null;
// Attempt to login to the salesforce account
try {
	// Create a generic SFI objects
	$salesforceInterface = new SalesforceInterface();
	$salesforceConnection = $salesforceInterface->getConnection();
	_e('OK', 2);
} catch (Exception $e) {
	_e('FAILED. Exception: ' . print_r($e, true));
}

// Sync updated records in Salesforce with records in mysqldb
// *********************************************************************************************************************

show_notice('Syncing updated bookings from Salesforce', 2, CONSOLE_HEAD_2);
_e("Start: " . date('Y-m-d H:i:s'), 2);
_e('Getting updated bookings...');

$sync_update_start_time = microtime(true);

executeActions($salesforceConnection);

_e('End: ' . profile_time($sync_update_start_time), 2);

shell_end();

/* ************************************************** */
function syncRecords($salesforceConnection) {
	try {
		// get all updated records from the last CALL_INTERVAL minutes
		$get_updated_response = $salesforceConnection->getUpdated('Patienten__c', $start_time, $end_time);
		_e('OK', 2);
	} catch (Exception $e) {
		_e('Exception: ' . print_r($e, true));
	}
	
	// describe fields of Patienten__c:
	# print_r($salesforceConnection->describeSObject('Patienten__c'));
	
	if (!empty($get_updated_response) && isset($get_updated_response->ids)) {
		$retrieveResponse = $salesforceConnection->retrieve("Id,
														Anrede__c,
														Arzt__c,
														Backend_ID__c,
														Behandlungsgrund__c,
														Email__c,
														Name__c,
														Status__c,
														Telefon__c,
														Termin_am__c,
														Versicherungsart__c,
														Vorname__c,
														Bestandspatient__c,
														Terminanfrage_an_Patienten_schicken__c,
														Terminvorschlag__c,
														In_Praxis_gebucht__c,
														CreatedDate",
				"Patienten__c",
				$get_updated_response->ids);
	
		print_r($retrieveResponse);
	
		foreach ((array)$retrieveResponse as $updatedRecord) {
			print_r($updatedRecord);
			echo "\n\n";
		}
	}

}

/**
 * Check for selected actions and execute them accordingly
 * 
 * 1. Terminanfrage_an_Patienten_schicken__c:
 *    - fetch all bookings that have Status__c = 'In Bearbeitung'
 *      AND
 *      Terminanfrage_an_Patienten_schicken__c = true
 *    - uncheck the action in SF
 *    - create a booking offer
 *    - send email to patient
 * 
 * @param SalesforceConnection
 */
function executeActions($salesforceConnection) {
	// Action Terminanfrage_an_Patienten_schicken__c
	$query = "SELECT
				Id,
				Status__c,
				Backend_ID__c,
				Terminvorschlag__c,
				In_Praxis_gebucht__c
			FROM
				Patienten__c
			WHERE
				Status__c = 'In Bearbeitung'
			AND
				Terminanfrage_an_Patienten_schicken__c = true";

	$response = $salesforceConnection->query($query);
	foreach ($response->records as $record) {
		// first step: uncheck the action to make sure it will not be executed again if something fails
		$sObject = new stdClass();
		$sObject->Id = $record->Id;
		$sObject->Terminanfrage_an_Patienten_schicken__c = false;
		// UNCHECK action IN SF !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		$upsertResponse = $salesforceConnection->upsert('Id', array($sObject), 'Patienten__c');
		$success = (isset($upsertResponse[0]->success) && $upsertResponse[0]->success = 1) ? true : false;

		if (!$success) {
			$message = 'Action "Terminanfrage_an_Patienten_schicken" failed for booking '.$record->Id.'! Action could not be unchecked in Salesforce. CHECK THIS BOOKING!';
			show_warning($message);
			send_warning("salesforce_sync.php warning", $message);
		} else {
			// get the corresponding Booking
			$bookingId = $record->Backend_ID__c;
			$booking = new Booking($bookingId);
			if (!$booking->isValid()) {
				$message = 'Action "Terminanfrage_an_Patienten_schicken" failed for booking '.$record->Id.'! Action has been unchecked in Salesforce but the BookingId '.$booking->getId().' (Backend_ID__c) seems to be unvalid. CHECK THIS BOOKING!';
				show_warning($message);
				send_warning("salesforce_sync.php warning", $message);
			} else {
				$data = array(
					'booking_id'			=> $booking->getId(),
					'user_id'				=> $booking->getUserId(),
					'location_id'			=> $booking->getLocationId(),
					'appointment_start_at'	=> Calendar::UTC2SystemDateTime($record->Terminvorschlag__c),
					'booked_in_practice'	=> ($record->In_Praxis_gebucht__c ? 1 : 0)
				);
				// create the booking offer
				$bookingOffer = BookingOffer::create($data);
				if (!$bookingOffer->isValid()) {
					$message = 'Action "Terminanfrage_an_Patienten_schicken" failed for booking '.$record->Id.'! Action has been unchecked in Salesforce but BookingOffers could not be created. CHECK THIS BOOKING!';
					show_warning($message);
					send_warning("salesforce_sync.php warning", $message);
				} else {
					// send the booking offer to the patient
					if (!BookingOffer::sendBookingOffersEmail($booking)) {
						$message = 'Action "Terminanfrage_an_Patienten_schicken" failed for booking '.$record->Id.'! Action has been unchecked in Salesforce and BookingOffers has been created, but sending the email to the patient failed. CHECK THIS BOOKING!';
						show_warning($message);
						send_warning("salesforce_sync.php warning", $message);
					} else {
						_e("Send BookingOffer for SF BookingId ".$record->Id." / bookingId ".$booking->getId());
					}
				}
			}
		}
	}
}
