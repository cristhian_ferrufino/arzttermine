<?php
/**
 * shell.php
 *
 * @author Axel Kuzmik <axel@kuzmik.de>
 * @author Tim Neill <neill.tim@gmail.com>
 */
error_reporting(E_ERROR | E_PARSE);
date_default_timezone_set('Europe/Berlin');

// CONFIG
// **********************************************************

$ENVIRONMENTS = array(
    'development' => __DIR__ . '/../',
    'staging'     => '/www/test.arzttermine.de/current',
    'production'  => '/www/www.arzttermine.de/current'
);

// HELPERS
// **********************************************************

// Header styles for console messages
define('CONSOLE_HEAD_1', '=');
define('CONSOLE_HEAD_2', '-');
define('CONSOLE_NOTICE', '*');

// Helper that just uses the other output funcs to show a consistent warning label
function show_warning($msg)
{
    $msg = trim($msg);

    if ($msg) {
        show_notice('WARNING', 1, CONSOLE_NOTICE);
        _e($msg);
    }
}

// Send a warning email to the administrator
function send_warning_mail($message, $subject, $source = 'CLI', $line = 0)
{
    Arzttermine\Mail\AdminMailer::send(
        $GLOBALS['CONFIG']["SALESFORCE_ALERT_MESSAGES_EMAILS"],
        $message,
        $subject,
        $source,
        $line
    );
}

// Writes a message with padding (centered), and adds new lines
function show_notice($msg = '', $line_height = 1, $character = CONSOLE_HEAD_1)
{
    $msg = trim($msg);
    _e(str_pad($msg ? " {$msg} " : '', 80, $character, STR_PAD_BOTH), $line_height);
}

// Echo with newlines
function _e($str = '', $line_height = 1)
{
    echo $str . str_repeat("\n", intval($line_height));
}

// Start the shell process
function shell_start($environment_forced = false, $flags = array())
{
    if (SILENT_SHELL) {
        ob_start();
    }

    show_notice('BEGIN');
    _e('Time: ' . date('Y-m-d H:i:s'));
    show_notice('', 2);

    if ($environment_forced) {
        show_notice("WARNING! No environment was detected. Using 'development'", 2, CONSOLE_NOTICE);
    }

    if (DEBUG_MODE) {
        show_notice('DEBUG MODE ACTIVE', 2, CONSOLE_HEAD_2);
    }

    _e("ENVIRONMENT: {$_ENV['SYSTEM_ENVIRONMENT']}");
    _e("SCRIPT: " . __FILE__);
    _e('FLAGS: ' . print_r($flags, true), 1);
}

// Terminate the shell process and display a message
function shell_end()
{
    show_notice();
    _e('Time: ' . date('Y-m-d H:i:s') . ' (' . profile_time($GLOBALS['time_start']) . ')');
    show_notice('END', 2);

    if (SILENT_SHELL) {
        ob_end_clean();
    }
}

// Easy way to get time diffs etc
function profile_time($start_time = null, $current_time = null, $significant_digits = 9)
{
    if (!$start_time && $start_time !== 0) {
        $start_time = microtime(true);
    }

    if (!$current_time && $current_time !== 0) {
        $current_time = microtime(true);
    }

    return number_format(floatval($current_time) - floatval($start_time), intval($significant_digits)) . 's';
}

// WARM UP
// **********************************************************

// Profiling
$time_start = microtime(true);

// Application options
// s:silent (no output) / a:action / e:environment / d:debug mode
$options = getopt('sa:e:d');

// Application action
$action = $options['a'];

// Set debug mode (true if the flag exists at all)
define('DEBUG_MODE', isset($options['d']));

// Silent mode
if (!defined('SILENT_SHELL')) {
    define('SILENT_SHELL', isset($options['s']));
}

// Get the environment, and warn if not available
$environment_forced = false;
if ((!$environment = strtolower(trim($options['e']))) || !in_array($environment, array_keys($ENVIRONMENTS))) {
    $environment = 'development';
    $environment_forced = true;
}

// Set the global
$_ENV['SYSTEM_ENVIRONMENT'] = $environment;

// Set root path according to environment
$ROOT = $ENVIRONMENTS[$environment];

if (!isset($_SERVER['DOCUMENT_ROOT']) || $_SERVER['DOCUMENT_ROOT'] === '') {
    $_SERVER['DOCUMENT_ROOT'] = "{$ROOT}/htdocs";
}

// $CONFIG eventually comes from here
require_once "{$ROOT}/include/bootstrap.php";

// BEGIN
// **********************************************************

shell_start($environment_forced, $options);