<?php
/**
 * Centralized Cache script
 *
 * Calculates appointments for integrations 1,2,5 and puts them into the
 * table search_result_cache_tmp
 * When this is done it swaps the table with
 * search_result_cache
 *
 * @deprecated
 */

use NGS\Managers\LocationsManager;
use NGS\Managers\SearchResultCacheTmpManager;
use NGS\Managers\TempDataManager;
use NGS\Util\ProviderRow;

require_once 'shell.php';

$locationsManager = LocationsManager::getInstance();
$cacheDtos = $locationsManager->findAllVisibleDoctors(array(8, 9, 10));
_e('All locations-doctors count: ' . count($cacheDtos), 2);

//creating tmp table start
_e('Creating temporary table');
$searchResultCacheTmpManager = SearchResultCacheTmpManager::getInstance();
$tempDataManager = TempDataManager::getInstance();
$tempDataManager->emptyValue(SEARCH_CACHE_ALL_CHANGED_DOCTORS_IDS_KEY);

$searchResultCacheTmpManager->truncateTempTable();

_e('Table ready', 2);
_e('Caching doctors...');

$process_index = 0;
foreach ($cacheDtos as $cacheDto) {
	try {
		_e(++$process_index . ') ' . $cacheDto->getId() . '-' . $cacheDto->getUserDto()->getId());
		ProviderRow::cacheDoctorSearchResultInfo($cacheDto->getUserDto()->getId(), $cacheDto->getId());
	} catch (Exception $e) {
		show_warning('Exception occurred (ID ' . $cacheDto->getId() . ')');
	}
}

$changedDoctorsIds = trim($tempDataManager->getValue(SEARCH_CACHE_ALL_CHANGED_DOCTORS_IDS_KEY));
while (!empty($changedDoctorsIds)) {
	$tempDataManager->emptyValue(SEARCH_CACHE_ALL_CHANGED_DOCTORS_IDS_KEY);
	$changedDoctorsIds = explode(' ', $changedDoctorsIds);
	$changedDoctorsIds = array_unique($changedDoctorsIds);
	foreach ($cacheDtos as $cacheDto) {
		if (in_array($cacheDto->getUserDto()->getId(), $changedDoctorsIds)) {
			_e('Updating doctor data (ID ' . $cacheDto->getUserDto()->getId() . ')');
			$searchResultCacheTmpManager->deleteDoctorCaches($cacheDto->getUserDto()->getId(), $cacheDto->getId());
			ProviderRow::cacheDoctorSearchResultInfo($cacheDto->getUserDto()->getId(), $cacheDto->getId());
		}
	}
	$changedDoctorsIds = trim($tempDataManager->getValue(SEARCH_CACHE_ALL_CHANGED_DOCTORS_IDS_KEY));
}

$searchResultCacheTmpManager->swapWithOriginalTable();

_e('Caching doctors complete.');

shell_end();