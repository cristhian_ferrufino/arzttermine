<?php
/*
 * IMPORTANT!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * Whenever you want to fetch NEW custom fields which are NOT in the enterprise.wsdl.xml
 * then you need to create a NEW WSDL file!!!
 * In Salesforce go to Setup->Develop->API->Generate Enterprise WSDL->Button "Generate" and store this
 * file to enterprise.wsdl.xml
 * OR try to just call https://eu1.salesforce.com/soap/wsdl.jsp?type=*&ver_eLoader_4F=1.6&ver_myfaxv3=1.9&ver_SFGA=166.1
 * 
 * Sync the bookings from Salesforce to the backend
 * 1. If there are new bookings in the backend which are not in salesforce the
 *    backend bookings are synced to Salesforce.
 * 2. If there are UPDATED bookings in Salesforce then the booking will be updated
 *    with the Salesforce's content
 *
 * Changes in the backend are pushed to Salesforce immediately.
 * 
 * This scripts runs every 4 minutes on one production machine (web01) via cron
 *
 * Issues:
 * - The server and Salesforce should have almost the same server time to make sure
 *   that both are talking about the same changes
 * - If this script is not running for any reasons or missing a cronjob call that
 *   changes or inserts in Salesforce will not be seen from this script as it checks
 *   only the last CALL_INTERVAL minutes
 *
 * 
 * WARNING:
 * If this script is used in non production (like staging or development) that the upsert()
 * call could update the wrong Backend_ID__c to Salesforce. This link needs to be fixed then!
 * 
 */

use Arzttermine\Application\Application;
use Arzttermine\Booking\Booking;
use Arzttermine\Booking\Offer;
use Arzttermine\Calendar\Calendar;
use Arzttermine\Mail\Mailing;
use Arzttermine\Mail\SMS;
use Arzttermine\MedicalSpecialty\TreatmentType;
use Arzttermine\User\User;
use Arzttermine\Location\Location;
use Arzttermine\Widget\WidgetFactory;

require_once __DIR__ . '/shell.php';

// HELPERS
// *********************************************************************************************************************
// Send an email and SMS notification
// @todo: refactor so that there is only the template mails and no more CMS
/**
 * @param array $data
 * @param string $template
 */
function send_mail_notification($data, $template) {
    global $emailTemplates;

	// Send email
	if (isset($data['email']) && isset($emailTemplates[$template])) {

        $data['domain'] = $GLOBALS['CONFIG']['DOMAIN'];

        $_mailing = new Mailing();
		$_mailing->type             = MAILING_TYPE_HTML;
		$_mailing->source_id        = $data['id'];
		$_mailing->source_type_id   = MAILING_SOURCE_TYPE_ID_BOOKING;
		$_mailing->from             = $GLOBALS['CONFIG']["BOOKINGS_EMAIL_ADDRESS"];
		$_mailing->from_name        = $GLOBALS['CONFIG']["BOOKINGS_EMAIL_NAME"];
		$_mailing->data             = $data;
		$_mailing->content_filename = $emailTemplates[$template];
		$_mailing->addAddressTo($data['email'], $data['name']);

        if ($_mailing->send(true)) {
			_e('Mail sent to address: ' . $data['email'], 2);
		} else {
			show_notice('WARNING', 1, CONSOLE_NOTICE);
			_e('Mail could not be sent to address: ' . $data['email'], 2);
		}
	}
}

/*
 * Send a booking confirmation SMS to the given booking owner
 *
 * @todo: as long as we use the Salesforce sync this way the doctor_name should not be communicated
 * as for telefonbookings the name is not always correct. Use location_name instead.
 */

function send_sms_notification($fields, $template = 'booking') {
	if (!empty($fields['id']) && !empty($fields['phone'])) {
		switch ($template) {
			case 'rueckruf':
				_e("Sending an SMS to {$fields['phone']}...", 2);
				SMS::sendSmsToGermany(
					$fields['phone'],
					"Rückrufbitte von Arzttermine.de: Bitte rufen Sie uns kostenlos unter 0800 2222133 zurück, es gibt eine wichtige Rückfrage zu Ihrem Termin.",
					"Booking ID: " . $fields['id']
				);
				break;
			case 'vorlaeufig':
				_e("Sending an SMS to {$fields['phone']}...", 2);
				SMS::sendSmsToGermany($fields['phone'],
					// "Ihr Arzttermin bei {$fields['location_name']} wurde um wenige Minuten auf {$fields['short_date']} {$fields['appointment_time']}h verschoben. Absage kostenlos unter 0800/2222133. Arzttermine.de"
                    // Shortening the SMS to try and reduce the chance of failure
                    "Ihr Arzttermin bei {$fields['location_name']} wurde um wenige Minuten auf {$fields['short_date']} {$fields['appointment_time']}h verschoben. Absage unter 0800/2222133"
				);
				break;
			case 'nicht-erreicht':
				_e("Sending an SMS to {$fields['phone']}...", 2);
				SMS::sendSmsToGermany($fields['phone'],
					"Leider konnte Ihr Arzttermin nicht gebucht werden, da wir sie nicht erreicht haben. Bitte rufen Sie uns kostenlos unter 0800 2222133 zurück."
				);
				break;
			case 'bekommen':
			default:
				_e("Sending an SMS to {$fields['phone']}...", 2);
				SMS::sendBookingConfirmationSms($fields['booking']);
				break;
		}
	}
}

// Send the appropriate notifications
function send_notifications(array $fields, $status, $previous_status = 0) {
	switch ($status) {
		case "In Bearbeitung - Rückrufbitte":
			if (!($previous_status == Booking::STATUS_PENDING_CALLBACK)) {
				send_mail_notification($fields, 'rueckruf');
				send_sms_notification($fields, 'rueckruf');
			}
			break;
		case "Termin bekommen":
		case "Ausweichtermin bekommen":
			if (
				!($previous_status == Booking::STATUS_RECEIVED)
				&&
				!($previous_status == Booking::STATUS_RECEIVED_ALTERNATIVE)
			) {
				send_mail_notification($fields, 'bekommen');
				send_sms_notification($fields, 'bekommen');
			}
			break;
		case "Ausweichtermin bekommen (vorläufig)":
			if (!($previous_status == Booking::STATUS_RECEIVED_ALTERNATIVE_PENDING)) {
				send_mail_notification($fields, 'vorlaeufig');
				send_sms_notification($fields, 'vorlaeufig');
			}
			break;
		case "Abgebrochen - nicht erreicht":
			if (!($previous_status == Booking::STATUS_ABORTED_UNREACHABLE)) {
				send_mail_notification($fields, 'nicht-erreicht');
				send_sms_notification($fields, 'nicht-erreicht');
			}
			break;
		case "Abgebrochen":
			if (!($previous_status == Booking::STATUS_ABORTED)) {
				send_mail_notification($fields, 'abgebrochen');
			}
			break;
        case "Termin storniert":
            if (!($previous_status == Booking::STATUS_CANCELLED_BY_PATIENT)) {
                send_mail_notification($fields, 'termin-storniert');
            }
            break;
	}
}

/**
 * Check for selected actions and execute them accordingly
 *
 * 1. Terminanfrage_an_Patienten_schicken__c:
 *    - fetch all bookings that have Status__c = 'In Bearbeitung'
 *      AND
 *      Terminanfrage_an_Patienten_schicken__c = true
 *    - uncheck the action in SF
 *    - create a booking offer
 *    - send email to patient
 *
 * @param SforceEnterpriseClient
 */
function executeActions(SforceEnterpriseClient $salesforceConnection) {
    // Action Terminanfrage_an_Patienten_schicken__c
    $query = "SELECT
				Id,
				Status__c,
				Backend_ID__c,
				Terminvorschlag__c,
				Terminvorschlag_Rueckmeldefrist__c,
				In_Praxis_gebucht__c
			FROM
				Patienten__c
			WHERE
				(
	               Status__c = 'In Bearbeitung'
	               OR
	               Status__c = 'In Bearbeitung - Rückrufbitte'
	               OR
	               Status__c = 'In Bearbeitung - Praxis nicht erreicht'
				)
			AND
				Terminanfrage_an_Patienten_schicken__c = true";

    $response = $salesforceConnection->query($query);
    foreach ($response->records as $record) {
        // First step: uncheck the action to make sure it will not be executed again if something fails
        $sObject = new stdClass();
        $sObject->Id = $record->Id;
        $sObject->Terminanfrage_an_Patienten_schicken__c = false;
        
        // @todo UNCHECK action IN SF
        $upsertResponse = $salesforceConnection->upsert('Id', array($sObject), 'Patienten__c');
        $success = (bool)(isset($upsertResponse[0]->success) && $upsertResponse[0]->success = 1);

        if (!$success) {
            $message = "Action 'Terminanfrage_an_Patienten_schicken' failed for booking {$record->Id}.\nhttps://eu1.salesforce.com/{$record->Id}\nAction could not be unchecked in Salesforce.";
            show_warning($message);
            send_warning_mail(
                $message,
                'executeActions',
                basename(__FILE__),
                __LINE__
            );
        } else {
            // Get the corresponding Booking
            $bookingId = $record->Backend_ID__c;
            $booking = new Booking($bookingId);
            if (!$booking->isValid()) {
                $message = "Action 'Terminanfrage_an_Patienten_schicken' failed for booking {$record->Id}.\nhttps://eu1.salesforce.com/{$record->Id}\nAction has been unchecked in Salesforce but the BookingId {$bookingId} (Backend_ID__c) seems to be invalid.";
                show_warning($message);
                send_warning_mail(
                    $message,
                    'executeActions',
                    basename(__FILE__),
                    __LINE__
                );
            } else {
                $sendEmail = true;
                $data = array(
                    'booking_id'			=> $booking->getId(),
                    'user_id'				=> $booking->getUserId(),
                    'location_id'			=> $booking->getLocationId(),
                    'booked_in_practice'	=> (bool)($record->In_Praxis_gebucht__c)
                );
                // Only add the properties if they are set or the date will be set to the current date which is wrong!
                if (!empty($record->Terminvorschlag__c)) {
                    $data['appointment_start_at'] = Calendar::UTC2SystemDateTime($record->Terminvorschlag__c);
                }
                if (!empty($record->Terminvorschlag_Rueckmeldefrist__c)) {
                    $data['expires_at'] = Calendar::UTC2SystemDateTime($record->Terminvorschlag_Rueckmeldefrist__c);
                }

                // first check if there is already a booking offer with the same settings
                // if so only send out the email again
                $bookingOffer = new Offer();
                if ( 0 == $bookingOffer->count(
                        'booking_id='.$data['booking_id'].
                        ' AND '.
                        'user_id='.$data['user_id'].
                        ' AND '.
                        'location_id='.$data['location_id'].
                        ' AND '.
                        'appointment_start_at="'.$data['appointment_start_at'].'"'
                    )) {
                    // create the booking offer
                    $bookingOffer = Offer::create($data);
                    if (!$bookingOffer->isValid()) {
                        $message = "Action 'Terminanfrage_an_Patienten_schicken failed for booking {$record->Id}.\nhttps://eu1.salesforce.com/{$record->Id}\nAction has been unchecked in Salesforce but BookingOffers could not be created.";
                        show_warning($message);
                        send_warning_mail(
                            $message,
                            'executeActions',
                            basename(__FILE__),
                            __LINE__
                        );
                        $sendEmail = false;
                    }
                }
                if ($sendEmail) {
                    // send the booking offer to the patient
                    if (!Offer::sendBookingOffersEmail($booking)) {
                        $message = "Action 'Terminanfrage_an_Patienten_schicken' failed for booking {$record->Id}.\nhttps://eu1.salesforce.com/{$record->Id}\nAction has been unchecked in Salesforce and BookingOffers has been created, but sending the email to the patient failed.";
                        show_warning($message);
                        send_warning_mail(
                            $message,
                            'executeActions',
                            basename(__FILE__),
                            __LINE__
                        );
                    } else {
                        _e("Send BookingOffer for SFBookingId {$record->Id} and bookingId {$booking->getId()}");
                    }
                }
            }
        }
    }
}

/**
 * Check for expired booking offers.
 * Needs functions from shell.php, so this can be only executed within a cronjob for now!
 */
function checkExpiredBookingOffers()
{
    show_notice('Checking for expired booking offers', 2, CONSOLE_HEAD_2);

    $expired_offers = Offer::getExpiredOffers();

    // Collect all bookings that are affected
    $bookingIds = array();
    foreach ($expired_offers as $expired_offer) {
        $bookingIds[] = $expired_offer->getBookingId();
    }

    // Remove redundant bookings
    $bookingIds = array_unique($bookingIds);
    if (empty($bookingIds)) {
        return;
    }

    // Send out the emails at once for each booking (there can be more than one booking offer)
    foreach ($bookingIds as $bookingId) {
        $booking = new Booking($bookingId);
        if($booking->hasStatus(Booking::STATUS_RECEIVED_ALTERNATIVE)) {
            show_warning("Booking with ID {$bookingId} is in status RECEIVED_ALTERNATIVE which is not correct status to be set to PENDING_BOOKING_OFFER_EXPIRED");
            continue;
        }

        if (!$booking->saveStatus(Booking::STATUS_PENDING_BOOKING_OFFER_EXPIRED, true)) {
            $message = "Booking with ID {$bookingId} could not be synced to Salesforce.";
            show_warning($message);
            send_warning_mail(
                $message,
                'checkExpiredBookingOffers',
                basename(__FILE__),
                __LINE__
            );
        }
        // Send the email to the patient
        if (!Offer::sendExpiredBookingOffersEmail($booking)) {
            $message = "Booking {$bookingId}";
            show_warning($message);
            send_warning_mail(
                $message,
                'checkExpiredBookingOffers',
                basename(__FILE__),
                __LINE__
            );
        } else {
            // Mark all connected bookingOffers as EXPIRED and set the booking status to ?..?
            $bookingOffer = new Offer();
            /** @var Offer[] $bookingOffers */
            $bookingOffers = $bookingOffer->findObjects('booking_id=' . $bookingId . ' AND status=' . Offer::STATUS_PENDING);
            if (!empty($bookingOffers)) {
                foreach ($bookingOffers as $bookingOffer) {
                    $bookingOffer->setStatus(Offer::STATUS_EXPIRED);
                    $bookingOffer->save(array('status'));
                }
            }
            _e("Updated status and send BookingOffer expired mail for bookingId ".$booking->getId());
        }
    }
}

/**
 * Check for deprecated pending bookingOffers where the booking has changed the status (to booked):
 * - BookingOffer was created and email was send to patient. BookingOffer has STATUS_PENDING(_xxx)
 * - Then patient support changed the booking status
 * - Fetch all deprecated booking offers and disable them
 */
function checkDeprecatedBookingOffers()
{
    show_notice('Checking for deprecated booking offers', 2, CONSOLE_HEAD_2);
    
    $app = Application::getInstance();    
    $db = $app->getSql();
    
    /**
     * find all booking_ids where the status is Booking::getBookedStatuses()
     * and BookingOffer::STATUS_PENDING
     */
    $sql = '
            SELECT
                '.DB_TABLENAME_BOOKINGS.'.id AS booking_id, '.DB_TABLENAME_BOOKING_OFFERS.'.id AS booking_offer_id
            FROM '.DB_TABLENAME_BOOKINGS.', '.DB_TABLENAME_BOOKING_OFFERS.'
            WHERE
                '.DB_TABLENAME_BOOKINGS.'.id = '.DB_TABLENAME_BOOKING_OFFERS.'.booking_id
            AND
                ! ('.Booking::getStatusesSql(Booking::getPendingStatuses(), 'OR').')
            AND
                '.DB_TABLENAME_BOOKING_OFFERS.'.status = '.Offer::STATUS_PENDING;

    $db->query($sql);
    $bookingIds = array();
    if ($db->nf() > 0) {
        while ($db->next_record()) {
            $bookingIds[] = $db->f('booking_id');
        }
    }
    $bookingIds = array_unique($bookingIds);
    if (empty($bookingIds)) {
        return;
    }

    // disable all pending bookingOffers
    foreach ($bookingIds as $bookingId) {
        $booking = new Booking($bookingId);
        if (!$booking->isValid()) {
            $message = 'Booking ' . $booking->getId() . ' is not valid.';
            send_warning_mail(
                $message,
                'checkDeprecatedBookingOffers',
                basename(__FILE__),
                __LINE__
            );
        } else {
            // mark all connected bookingOffers as STATUS_ANSWERED
            $bookingOffer = new Offer();
            /** @var Offer[] $bookingOffers */
            $bookingOffers = $bookingOffer->findObjects('booking_id='.$bookingId.' AND status=' . Offer::STATUS_PENDING);
            if (!empty($bookingOffers)) {
                foreach ($bookingOffers as $bookingOffer) {
                    $bookingOffer->setStatus(Offer::STATUS_ANSWERED);
                    $bookingOffer->save(array('status'));
                }
            }
            _e("Updated bookingOffer statuses connected to bookingId ".$booking->getId());
        }
    }
}


function syncBookingsToSalesforce()
{
    //sync new bookings still without salesforce_id
    show_notice('Checking for bookings without salesfore_id', 2, CONSOLE_HEAD_2);
    $app = Application::getInstance();
    $db = $app->getSql();

    /**
     * find all booking_ids where the status is Booking::STATUS_NEW
     * and Booking::salesforce_id IS NULL
     */
    $sql = '
            SELECT '.DB_TABLENAME_BOOKINGS.'.id AS booking_id
            FROM '.DB_TABLENAME_BOOKINGS.'
            WHERE '.DB_TABLENAME_BOOKINGS.'.status='.Booking::STATUS_NEW.'
            AND ('.DB_TABLENAME_BOOKINGS.'.salesforce_id IS NULL OR '.DB_TABLENAME_BOOKINGS.'.salesforce_id="")';
    $db->query($sql);
    $bookingIds = array();
    if ($db->nf() > 0) {
        while ($db->next_record()) {
            $bookingIds[] = $db->f('booking_id');
        }
    }
    $bookingIds = array_unique($bookingIds);
    if (empty($bookingIds)) {
        return;
    }

    foreach ($bookingIds as $bookingId) {
        $booking = new Booking($bookingId);
        if ($booking->isValid()) {
            if ($booking->createInSalesforce()) {
                _e('The booking '.$bookingId.' was successfully created in salesforce');
            } else {
                $booking->saveStatus(Booking::STATUS_INTEGRATION_INVALID);
                _e('The booking '.$bookingId.' could not be created in salesforce');
            }
        }
    }
}

// CONFIG
// *********************************************************************************************************************

setlocale(LC_TIME, 'de_DE');

$emailTemplates = array(
	// booking confirmed
	'bekommen'          => '/mailing/integrations/1/booking-new-confirmed.html',
	// callback
	'rueckruf'          => '/mailing/integrations/1/booking-callback.html',
	// booking was modified
	'vorlaeufig'        => '/mailing/integrations/1/booking-modified.html',
	// user can't be reached
	'nicht-erreicht'    => '/mailing/integrations/1/booking-cannot-reach.html',
	// booking can't be placed
	'abgebrochen'       => '/mailing/integrations/1/booking-unplaceable.html',
	// booking was canceled
	'termin-storniert'  => '/mailing/integrations/1/booking-cancellation-confirmation.html'
);

// Interval of calling script in minutes
define('CALL_INTERVAL', 5);

// Set the Date threshold under which do not change Bookings
$threshold = date('Y-m-d', strtotime('2013-08-09'));

// Get the time when the script starts
$current_time = mktime();

// Set period of checking for deletions / updates  to the last CALL_INTERVAL minutes
$start_time = $current_time - (60 * CALL_INTERVAL);

/**
 * the end time of the script checking should be set to the time the script starts.
 * Because the end time gets rounded to the bottom we add +60 seconds to the end time
 * so we cover for sure the desired period
 */
$end_time = $current_time + 60;

// BEGIN
// *********************************************************************************************************************
show_notice('Attempting to log in to Salesforce', 2, CONSOLE_HEAD_2);

$salesforceConnection = null;
// Attempt to login to the salesforce account

if ($GLOBALS['CONFIG']['SALESFORCE_ENABLE']) {
	try {
		// Create a generic SFI objects
		$salesforceInterface = new SalesforceInterface();
		$salesforceConnection = $salesforceInterface->getConnection();
		_e('OK', 2);
	} catch (Exception $e) {
		_e('FAILED. Exception: ' . print_r($e, true));
	}
}

// Sync updated records in Salesforce with records in mysqldb
// *********************************************************************************************************************

show_notice('Syncing updated bookings from Salesforce', 2, CONSOLE_HEAD_2);
_e("Start: " . date('Y-m-d H:i:s'), 2);
_e('Getting updated bookings...');

$sync_update_start_time = microtime(true);

$get_updated_response = null;

if ($GLOBALS['CONFIG']['SALESFORCE_ENABLE']) {
	try {
	    if($salesforceConnection instanceof SforceEnterpriseClient === false) {
	        throw new \Exception('no connection to salesforce');
        }
		// get all updated records from the last CALL_INTERVAL minutes
		$get_updated_response = $salesforceConnection->getUpdated('Patienten__c', $start_time, $end_time);
		_e('OK', 2);
	} catch (Exception $e) {
        send_warning_mail(
            "Could not get updated bookings on salesforce. Possibly invalid login data.\nException: " . print_r($e, true),
            'Salesforce login failed',
            basename(__FILE__),
            __LINE__
        );
		_e('Exception: ' . print_r($e, true));
	}
}

if (!empty($get_updated_response) && isset($get_updated_response->ids)) {
	$retrieveResponse = $salesforceConnection->retrieve("Id,
														Anrede__c,
														Arzt__c,
														Backend_ID__c,
														Fachgebiet__c,
														Behandlungsgrund__c,
														Email__c,
														Name__c,
														Status__c,
														Telefon__c,
														Termin_am__c,
														Versicherungsart__c,
														Vorname__c,
														Bestandspatient__c,
														CreatedDate,
														LastModifiedDate",
														"Patienten__c",
														$get_updated_response->ids);

	foreach ((array)$retrieveResponse as $updatedRecord) {

/*
	    //skip salesforce entries shouldn't show up on getUpdated request
        $createdDate = new DateTime($updatedRecord->CreatedDate);
        $lastModifiedDate = new DateTime($updatedRecord->LastModifiedDate);
        $startDate = new DateTime();
        $startDate->setTimestamp($start_time);
        $endDate = new DateTime();
        $endDate->setTimestamp($end_time);
        if(($createdDate > $startDate && $createdDate < $endDate) == false || ($lastModifiedDate > $startDate && $lastModifiedDate < $endDate) == false) {
            show_warning('Booking with salesforce_id '.$updatedRecord->Id.' created_at '.$updatedRecord->CreatedDate.' and last_modified '.$updatedRecord->LastModifiedDate.' should not show up on getUpdated request with startDate '.$startDate->format('c').' and endDate '.$endDate->format('c'));
            continue;
        }
*/

		$should_create = empty($updatedRecord->Backend_ID__c);

		// Create the $fields array that key is fieldName and value is fieldValue
		$fields = array();
		$fields['id'] = $should_create ? '' : $updatedRecord->Backend_ID__c;

		// Get bookingDto of the booking to update
		$booking = new Booking(intval($updatedRecord->Backend_ID__c));

		// TWO conditions to update.
		// 1 - the existing booking is after the threshold
		// 2 - there is no existing booking
		if (
			($booking->isValid() && (date('Y-m-d', strtotime($booking->getCreatedAt())) > $threshold)) ||
			$should_create
		) {
			_e("----------------------------");
			if ($should_create) {
				_e("Creating new record in DB...");
			} else {
				_e("Updating record with ID: {$updatedRecord->Backend_ID__c}...");
			}

			//convert $terminam to xsd format
			date_default_timezone_set('Europe/Berlin');
            $fields['appointment_start_at'] = Calendar::UTC2SystemDateTime($updatedRecord->Termin_am__c);

            if ($should_create) {
				// If the record doesn't exist, it's probably a manual addition
				$fields['source_platform'] = 'telephone';
				// store the salesforce created_at datetime to flag in the db that this booking is available in salesforce
				$created_at = date('Y-m-d H:i:s', strtotime($updatedRecord->CreatedDate));
				$fields['salesforce_updated_at'] = $created_at;
			}

			if ($updatedRecord->Anrede__c == "Herr") {
				$fields['gender'] = 0;
			} elseif ($updatedRecord->Anrede__c == "Frau") {
				$fields['gender'] = 1;
			}
			
			// Basic fields
			$fields['first_name'] = $updatedRecord->Vorname__c;
			$fields['last_name']  = $updatedRecord->Name__c;
			$fields['email']      = $updatedRecord->Email__c;
			$fields['phone']      = $updatedRecord->Telefon__c;

			// Is the customer known to the doctor?
			$fields['returning_visitor'] = intval($updatedRecord->Bestandspatient__c == 'Ja');
			
			// Insurance
			if ($updatedRecord->Versicherungsart__c == "Privat versichert") {
				$fields['insurance_id'] = 1;
			} elseif ($updatedRecord->Versicherungsart__c == "Gesetzlich versichert") {
				$fields['insurance_id'] = 2;
			}
			
			// Get the treatmenttype
            $treatmenttype = new TreatmentType();
            $fields['treatmenttype_id'] = $treatmenttype->getIdByMedicalSpecialtyNameSingularDeAndTreatmenttypeNameDe($updatedRecord->Fachgebiet__c, $updatedRecord->Behandlungsgrund__c);

			// get the status id from the Salesforce status text
			$fields['status'] = $booking->getStatus($updatedRecord->Status__c);
			$previous_status = $booking->getStatus();

			// Set our internal SF id to the booking ID we get back
			$fields['salesforce_id'] = $updatedRecord->Id;

			/**
			 * In order to synchronize a change in location
			 * First we check if the location has really been changed in SF
			 */
			// Get location id from salesforce_id
			if ($updatedRecord->Arzt__c) {
                $location = new Location();
				if ($locationId = $location->getIdBySalesforceId($updatedRecord->Arzt__c)) {
					$location->load($locationId);
				}
				
				if (
					$location->isValid()
                    &&
                    (($booking->isValid() && intval($booking->getLocationId()) !== intval($locationId)) || $should_create) // If the booking exists OR we need to create one
				) {
					// Add to fields array the new values
					$fields['location_id'] = empty($locationId) ? null : $locationId;
					$fields['user_id'] = $location->getPrimaryMemberId();
				}
			}

			if ($should_create) {
				$app = Application::getInstance();

				/* 1. create the booking in db
				 * 2. store the booking id in salesforce
				 */

				$fields['slug'] = $booking->getNewSlug();
				$fields['created_at'] = $app->now('datetime');

				$booking = new Booking();
				// Make sure the id is not set or creating the new Object will fail
				unset($fields['id']);
                
				// load by salesforce_id
                $booking->load($updatedRecord->Id, 'salesforce_id');
                // Create in DB
                if (!$booking->isValid()) {
                    $backend_id = $booking->saveNew($fields);
                    $booking->load($backend_id);

                    if (!$booking->isValid()) {
                        show_warning('Booking could not be created. Skipping upsert to Salesforce. Ignoring notifications.');
                        send_warning_mail(
                            "Booking could not be created. Skipping upsert to Salesforce. Ignoring notifications. salesforce_id {$updatedRecord->Id}.",
                            'Booking could not be created',
                            basename(__FILE__),
                            __LINE__
                        );
                        continue;
                    }
                } else {
                    $backend_id = $booking->getId();
                }


				// Store the backend_id in Salesforce
				$sObject = new stdClass();
				$sObject->Id            = $updatedRecord->Id;
				$sObject->Backend_ID__c = $backend_id;
				// Update in Salesforce
				$upsertResponse = $salesforceConnection->upsert('Id', array($sObject), 'Patienten__c');

				// Set the fields ID to match the one we just assigned
				$fields['id'] = $backend_id;

				// http://www.salesforce.com/us/developer/docs/api/Content/sforce_api_calls_upsert_upsertresult.htm#topic-title
				if ($upsertResponse[0]->success) {
					$result = true; // OK
				} else {
                    show_warning('ERROR ON BOOKING '.$backend_id.' with salesforce_id '.$updatedRecord->Id.': '.$upsertResponse[0]->errors[0]->message);
					$result = false; // Fail
				}
			} else {
                $result = false;
				// Update in DB
                $booking = new Booking($fields['id']);
                if (!$booking->isValid()) {
                    show_warning('Booking ID ' . $fields['id'] . ' seems to be invalid. Please check.');
                } else {
                    $saveKeys = array(
                        'gender',
                        'appointment_start_at',
                        'first_name',
                        'last_name',
                        'email',
                        'phone',
                        'returning_visitor',
                        'insurance_id',
                        'treatmenttype_id',
                        'status',
                        'salesforce_id'
                    );

                    // did the location change?
                    if (isset($fields['location_id'])) {
                        $booking->setLocationId($fields['location_id']);
                        $saveKeys[] = 'location_id';
                        // did the doctor change?
                        if (isset($fields['user_id'])) {
                            $booking->setUserId($fields['user_id']);
                            $saveKeys[] = 'user_id';
                        }
                    }
                    if (0 === $booking
                        ->setGender($fields['gender'])
                        ->setAppointmentStartAt($fields['appointment_start_at'])
                        ->setFirstName($fields['first_name'])
                        ->setLastName($fields['last_name'])
                        ->setEmail($fields['email'])
                        ->setPhone($fields['phone'])
                        ->setReturningVisitor($fields['returning_visitor'])
                        ->setInsuranceId($fields['insurance_id'])
                        ->setTreatmenttypeId($fields['treatmenttype_id'])
                        ->setStatus($fields['status'])
                        ->setSalesforceId($fields['salesforce_id'])
                        ->save($saveKeys)
                    ) {
                        show_warning("There have been no changes for bookingID {$booking->getId()} / salesforce_id {$updatedRecord->Id}");
                    }
                    $result = true;
                }
			}

            /**
             * Check if the booking was made inside the DAK widget
             * If yes, then initialize the widget and set the widget in the app
             * so that the mailing class sends out the correct templates
             *
             * @todo refactor and solve this for all widgets
             */
            $app = Application::getInstance();
            // Switch off the widget setting
            $app->setIsWidget(false);
            if ($booking->getSourcePlatform() == 'widget-dak') {
                if (!($app->getWidgetContainer() instanceof Arzttermine\Widget\Container)) {
                    $app->setWidgetContainer(WidgetFactory::getWidgetContainer('dak'));
                }
                $app->setIsWidget(true);
            }

            /**
             * send out the mail & SMS notifications
             */
            if (!empty($location)) {
				$user = new User($location->getPrimaryMemberId());
				$booking = new Booking($fields['id']);

				if ($user->isValid() && $booking->isValid()) {
                    $fields['booking']          = $booking;
					$fields['booking_salutation_full_name'] = $booking->getSalutationName();
					$fields['name']             = $booking->getName();
					$fields['appointment_date'] = strftime('%A %d.%m.%Y', strtotime($updatedRecord->Termin_am__c));
					$fields['appointment_time'] = strftime('%R', strtotime($updatedRecord->Termin_am__c));
					$fields['short_date']       = strftime('%d.%m.%Y', strtotime($updatedRecord->Termin_am__c));
					$fields['user_name']        = $user->getName();
					$fields['doctor_name']      = $fields['user_name'];
					$fields['location_name']    = $location->getName();
					$fields['location_address'] = "{$location->getStreet()}, {$location->getZip()} {$location->getCity()}";
					$fields['practice_name']    = $fields['location_name'];

                    $fields['conditional_appointment_date'] = '';
                    if ($booking->getAppointment()->isValid()) {
                        $fields['conditional_appointment_date'] = ' '.$app->_('am').' '.$booking->getAppointment()->getDateFormat('d.m.Y');
                    }

                    send_notifications($fields, $updatedRecord->Status__c, $previous_status);
				} else {
					show_warning('Doctor could not be loaded by Location ID. Ignoring notifications.');
				}
			} else {
				show_warning('Location not found in database. Ignoring notifications.');
			}

			if ($result) {
				_e("Updated booking with salesforce_id {$updatedRecord->Id}");
			} else {
				show_warning("Could not update booking with salesforce_id {$updatedRecord->Id}");
                send_warning_mail(
                    "Could not update booking with salesforce_id {$updatedRecord->Id}.",
                    'Booking could not be updated',
                    basename(__FILE__),
                    __LINE__
                );
			}
		}
	}
}

// now execute actions if checked in SF bookings
if ($GLOBALS['CONFIG']['SALESFORCE_ENABLE']) {
	executeActions($salesforceConnection);
}


// check for deprecated booking offers (must be before BookingOffer::checkExpiredBookingOffers())
checkDeprecatedBookingOffers();

// check for expired booking offers
checkExpiredBookingOffers();

syncBookingsToSalesforce();



_e('End: ' . profile_time($sync_update_start_time), 2);

shell_end();