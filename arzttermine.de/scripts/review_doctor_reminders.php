<?php
/**
 * Sends out emails to patients which appointments took place in the last 7, 14 and 21 days
 * Should be called as a cronjob every day once
 */

use Arzttermine\Booking\Booking;
use Arzttermine\Review\Review;

require_once __DIR__ . '/shell.php';

// use all received statuses but the unconfirmed one
$bookingStatuses = Booking::getReceivedStatuses(array(Booking::STATUS_RECEIVED_UNCONFIRMED));
$notificationPeriods = array(7, 14, 21); // in days
$integrationIds = array(1, 2, 5);

$bookings = Review::getBookingsForReviewReminders($notificationPeriods, $integrationIds, $bookingStatuses);

setlocale(LC_TIME, 'de_DE');

show_notice('All supplier reviews reminders', 2, CONSOLE_HEAD_2);
_e('Bookings count: ' . count($bookings));

foreach ($bookings as $booking) {
	_e("Booking ID: {$booking->getId()}, Patient email: {$booking->getEmail()}");

	$mailing_template = '/mailing/patients/review-doctor.html';
    if($booking->getUser()->hasPaidReview()) {
        $mailing_template = '/mailing/patients/paid-review-doctor.html';
    }

	if (Review::sendPatientEmail($mailing_template, $booking)) {
		_e("Email sent to {$booking->getEmail()}", 2);
	} else {
		show_notice('WARNING', 1, CONSOLE_NOTICE);
		_e("Could not send email to {$booking->getEmail()}", 2);
	}
}
_e();
shell_end();
