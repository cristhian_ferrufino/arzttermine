<?php

include_once 'shell.php';

require_once $_SERVER['DOCUMENT_ROOT'] . "/../ngs/conf/constants.php";
require_once CLASSES_PATH . "/managers/TempManager.class.php";
require_once CLASSES_PATH . "/managers/UsersManager.class.php";
require_once CLASSES_PATH . "/util/UtilFunctions.class.php";
require_once CLASSES_PATH . "/managers/AssetsManager.class.php";

$tempManager = TempManager::getInstance();
$usersManager = UsersManager::getInstance();
$assetsManager = AssetsManager::getInstance();
$allTempDtos = $tempManager->selectAll();

//groupping by locations
echo "Total doctors numbers to be edited: " . count($allTempDtos) . "\n";
echo "\n";

foreach ($allTempDtos as $index => $dto) {
	if ($index > 201) {
		//break;
	}
	$title = $dto->getTitle();
	$fname = $dto->getFname();
	$lname = $dto->getLname();

	$userSlug = UtilFunctions::createSlugFromString($title . ' ' . $fname . ' ' . $lname, 200);
	$userBySlug = $usersManager->getUserBySlug($userSlug);

	if (isset($userBySlug)) {
		//adding random image for the doctor
		$userId = $userBySlug->getId();
		//checking if asset is already inserted for this doctor 
		$asset = $assetsManager->getProfileAsset($userId);
		if($asset){
			echo "$index) asset already exists for doctor with ID: $userId" . "\n";
			continue;
		}
		$userGender = $userBySlug->getGender();
		if ($userGender == 0) {
			$tempImagesSubfolder = 'male';
		} else {
			$tempImagesSubfolder = 'female';
		}
		$randomImagesDir = NGS_ROOT . '/data/temp_images/' . $tempImagesSubfolder;
		$randomImagePath = getRandomImage($randomImagesDir);
		$randomImageName = basename($randomImagePath);

		$ownerType = ASSET_OWNERTYPE_USER;
		$ownerId = $userId;
		$filesize = filesize($randomImagePath);
		$filesUpload['size'] = $filesize;

		$_FILES["upload"] = array("name" => $randomImageName, "type" => "image/png", 'tmp_name' => $randomImagePath, "error" => 0, 'size' => $filesize);
		$assetSaved = $assetsManager->saveAsset($ownerType, $ownerId, $filesUpload, true);
		if($assetSaved){
			echo "$index) asset succesfully added for doctor with ID: $userId" . "\n";
		}else{
			echo "$index) asset adding problem for doctor with ID: $userId" . "\n";
		}
	}else{
		echo "$index) skipping doctor '$title $fname $lname', it does not exist in datebase!" . "\n";
		continue;
	}
}


shell_end();

function getRandomImage($dir) {
	$files = glob($dir . '/*.*');
	$file = array_rand($files);
	return $files[$file];
}
?>


