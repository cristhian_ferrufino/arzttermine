<?php

include_once 'shell.php';
require_once $_SERVER['DOCUMENT_ROOT'] . "/../include/config.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/../ngs/conf/constants.php";
require_once CLASSES_PATH . "/managers/UsersAaManager.class.php";
require_once CLASSES_PATH . "/managers/DoctorsTreatmentTypesManager.class.php";


$file1 = "termine.sql";

$fp = @fopen(__DIR__ . "/" . $file1, "r") or die("Kann Datei nicht lesen.");  // read file 

$usersAaManager = UsersAaManager::getInstance();
$doctorsTreatmentTypesManager = DoctorsTreatmentTypesManager::getInstance();

$user_location_ids = array();

while ($line = fgets($fp, 1024)) {
    $fieldsValuesArray = explode(',', $line);
    $user_location_ids [] = array($fieldsValuesArray[0], $fieldsValuesArray[1]);
}
$user_location_ids = unique_matrix($user_location_ids);

foreach ($user_location_ids as $user_location_pair) {    
    $userId = $user_location_pair[0];
    $locationId = $user_location_pair[1];
    $usersAaManager->deleteUserAllAvaialableTimesByUserIdAndLocationId($userId, $locationId);
}

rewind($fp);
while ($line = fgets($fp, 1024)) {
    
    $fieldsValuesArray = explode(',', $line);
    $weekday = $fieldsValuesArray [2];
    $date = UsersAaManager::getFirstComingDateByWeekday($weekday);
    $dto = $usersAaManager->createEmptyDto();
    $dto->setUserId($fieldsValuesArray[0]);
    $dto->setLocationId($fieldsValuesArray[1]);
    $dto->setDate($date);
    $dto->setStartAt($fieldsValuesArray[3]);
    $dto->setEndAt($fieldsValuesArray[4]);
    $dto->setBlockInMinutes($fieldsValuesArray[5]);
    $dto->setInsuranceTypes('public,private,cash');
    $ttIdsArray = $doctorsTreatmentTypesManager->getDoctorTreatmentTypesIds($dto->getUserId());
    $ttIds = implode(',', $ttIdsArray);    
    $dto->setTreatmentTypeIds($ttIds);
    $dto->setCreatedBy(281);
    $dto->setCreatedAt(date('Y-m-d H:i:s'));
    $dto->setUpdatedAt('0000-00-00 00:00:00');
    $dto->setUpdatedBy(null);    
    echo  "userID:".$dto->getUserId()." locationId:".$dto->getLocationId()."\n";
    $usersAaManager->addDoctorFixedWeekAppointment($dto);
}
fclose($fp);
echo("end time:".date('Y-m-d H:i:s'). "\n");


function unique_matrix($matrix) {
    $matrixAux = $matrix;

    foreach ($matrix as $key => $subMatrix) {
        unset($matrixAux[$key]);

        foreach ($matrixAux as $subMatrixAux) {
            if ($subMatrix === $subMatrixAux) {
                // Or this
                //if($subMatrix[0] === $subMatrixAux[0]) {
                unset($matrix[$key]);
            }
        }
    }

    return $matrix;
}

?>