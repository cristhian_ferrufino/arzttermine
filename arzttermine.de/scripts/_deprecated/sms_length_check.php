<?php

include_once 'shell.php';

require_once $_SERVER['DOCUMENT_ROOT'] . "/../ngs/conf/constants.php";
require_once CLASSES_PATH . "/managers/UsersManager.class.php";
require_once CLASSES_PATH . "/managers/SentSmsManager.class.php";
require_once CLASSES_PATH . "/managers/LocationsManager.class.php";

ini_set('display_errors', true);

$locationsManager = LocationsManager::getInstance();
$usersManager = UsersManager::getInstance();

$integration1LocationsWithDoctors = $locationsManager->getIntegrationLocationsWithDoctors(1);
$integration2LocationsWithDoctors = $locationsManager->getIntegrationLocationsWithDoctors(2);
$integration5LocationsWithDoctors = $locationsManager->getIntegrationLocationsWithDoctors(5);

$integrationLocationsWithDoctors = array_merge($integration1LocationsWithDoctors, $integration2LocationsWithDoctors);
$integrationLocationsWithDoctors = array_merge($integrationLocationsWithDoctors, $integration5LocationsWithDoctors);


//"u" is users table object,  "l" is location table object
$sms = "{u.first_name} {l.name} Terminbestätigung";

$args = array();
while (true) {
    $ret = replaceFirstVariableBlockWithS($sms);
    if ($ret) {
        $args [] = $ret;
    } else {
        break;
    }
}

echo("\n");
foreach ($integrationLocationsWithDoctors as $l) {
    $u = $l->getUserDto();
    $cargs = array();
    foreach ($args as $pair) {        
        $fname = getFunctionName($pair[1]);
        $cargs[] = $$pair[0]->$fname;
    }
    $currSms = vsprintf($sms, $cargs);
    $currSms = SMS::convertGermanSymbolsToEnglish($currSms);
    $smsLength = strlen($currSms );
    if ($smsLength >160)
    {
       echo "user id:".$u->getId()." ; location id:".$l->getId()." - ". $currSms."\n\n" ;        
    }
}

echo("\n");
echo("\n");
shell_end();

function replaceFirstVariableBlockWithS(&$text) {
    $firstVariableBlockString = getFirstVariableBlockString($text);
    if ($firstVariableBlockString === false) {
        return false;
    }
    $text = preg_replace("/$firstVariableBlockString/", '%s', $text, 1);
    switch ($firstVariableBlockString[1]) {
        case 'u':
            return array('u', substr($firstVariableBlockString, 3, -1));
        case 'l':
            return array('l', substr($firstVariableBlockString, 3, -1));
        default :
            return null;
    }
}

function getFirstVariableBlockString($text) {
    $p1 = strpos($text, '{');
    if ($p1 === false) {
        return false;
    }
    $p2 = strpos($text, '}');
    return substr($text, $p1, $p2 - $p1 + 1);
}

function getFunctionName($fieldName) {
    $arr = explode('_', $fieldName);
    array_walk($arr, "each_ucfirst");
    return lcfirst(implode('', $arr));
}

function each_ucfirst(&$item1, $key) {
    $item1 = ucfirst($item1);
}

?>
