<?php
/*
 * Test if there are bookings in Salesforce that have not been fetched or updated by
 * the salesforce_sync script
 * 
 * This is only the test if bookings that are in salesforce which are not in the backend.
 * @todo: Have a script that checks if there are bookings in the backend that are not in Salesforce
 *
 */

require_once 'shell.php';

require_once $_SERVER['DOCUMENT_ROOT'] . "/../include/lib/interfaces/salesforce/SalesforceInterface.php";

// Interval of calling script in minutes
define('CALL_INTERVAL', 60*24*10);

// Set the Date threshold under which do not change Bookings
$threshold = date('Y-m-d', strtotime('2013-08-09'));

// Get the time when the script starts
$current_time = mktime();

// Set period of checking for deletions / updates  to the last CALL_INTERVAL minutes
$start_time = $current_time - (60 * CALL_INTERVAL);

/**
 * the end time of the script checking should be set to the time the script starts.
 * Because the end time gets rounded to the bottom we add +60 seconds to the end time
 * so we cover for sure the desired period
 */
$end_time = $current_time + 60;

// BEGIN
// *********************************************************************************************************************

// Create a generic SFI objects
$_salesforce_interface = new SalesforceInterface();
$_sforce_connection = null;

show_notice('Attempting to log in to Salesforce', 2, CONSOLE_HEAD_2);

// Attempt to login to the salesforce account
try {
	$_sforce_connection = $_salesforce_interface->login()->getConnection();
	_e('OK', 2);
} catch (Exception $e) {
	_e('FAILED. Exception: ' . print_r($e, true));
}

// Sync updated records in Salesforce with records in mysqldb
// *********************************************************************************************************************

show_notice('Testing bookings in Salesforce', 2, CONSOLE_HEAD_2);
_e("Start: " . date('Y-m-d H:i:s'), 2);
_e('Getting updated bookings...');

$sync_update_start_time = microtime(true);

try {
	// get all updated records from the last CALL_INTERVAL minutes
	$get_updated_response = $_sforce_connection->getUpdated('Patienten__c', $start_time, $end_time);
	_e('OK', 2);
} catch (Exception $e) {
	_e('Exception: ' . print_r($e, true));
}

if (!empty($get_updated_response) && isset($get_updated_response->ids)) {
	$retrieveResponse = $_sforce_connection->retrieve("Id,
														Backend_ID__c,
														Name__c,
														Status__c,
														Termin_am__c,
														Vorname__c,
														CreatedDate",
														"Patienten__c",
														$get_updated_response->ids);

	_e('Checking '.sizeof($get_updated_response->ids).' Bookings from Salesforce for the last '.CALL_INTERVAL.' minutes.');
	$oks = 0;
	$errors = 0;
	foreach ((array)$retrieveResponse as $updatedRecord) {
		$id = $updatedRecord->Backend_ID__c;
		$salesforce_id = $updatedRecord->Id;
		$booking = new Booking($id);
		if ($booking->isValid() && $booking->salesforce_id == $salesforce_id) {
			$oks++;
		} else {
			echo "\nERROR: SalesforceData: salesforce_id: ".$salesforce_id." / backend_id:".$id."\n";
			if (!$booking->isValid()) {
				echo " Problem: backend_id in Salesforce is not valid: backend_id: ".$id."\n";
			}
			if ($booking->salesforce_id != $salesforce_id) {
				echo ' Problem: salesforce_id in backend is not set or not in sync: booking->salesforce_id='.$booking->salesforce_id."\n";
			}
			$errors++;
		}
	}
	_e("\nOKs: ".$oks);
	_e("ERRORs: ".$errors, 2);
}

_e('End: ' . profile_time($sync_update_start_time), 2);

shell_end();
