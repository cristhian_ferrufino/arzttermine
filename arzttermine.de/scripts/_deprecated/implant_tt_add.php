<?php

include_once 'shell.php';

require_once $_SERVER['DOCUMENT_ROOT'] . "/../ngs/conf/constants.php";
require_once (CLASSES_PATH . "/managers/LocationsManager.class.php");
require_once (CLASSES_PATH . "/managers/UsersAaManager.class.php");
require_once (CLASSES_PATH . "/managers/DoctorsTreatmentTypesManager.class.php");

ini_set('memory_limit', '500M');

$locationsManager = LocationsManager::getInstance();
$usersAaManager = UsersAaManager::getInstance();
$doctorsTreatmentTypesManager =DoctorsTreatmentTypesManager::getInstance();
$cacheDtos = $locationsManager->getIntegrationLocationsWithDoctors(1);
$int1DentistsBarlin = filterDoctorsByMedSpecAntAptTypeAndCity($cacheDtos, 1, 1, 'berlin');
$int1DentistsFrankfurt = filterDoctorsByMedSpecAntAptTypeAndCity($cacheDtos, 1, 1, 'frankfurt');
$cacheDtos = $locationsManager->getIntegrationLocationsWithDoctors(2);
$int2DentistsBerlin = filterDoctorsByMedSpecAntAptTypeAndCity($cacheDtos, 1, 1, 'berlin');
$int2DentistsFrankfurt = filterDoctorsByMedSpecAntAptTypeAndCity($cacheDtos, 1, 1, 'frankfurt');
$cacheDtos = $locationsManager->getIntegrationLocationsWithDoctors(8);
$int8DentistsFrankfurt = filterDoctorsByMedSpecAntAptTypeAndCity($cacheDtos, 1, 1, 'frankfurt');

echo('all int1 berlin dentists count: ' . count($int1DentistsBarlin) . "\n");
echo('all int1 frankfurt dentists count: ' . count($int1DentistsFrankfurt) . "\n");
echo('all int2 berlin dentists count: ' . count($int2DentistsBerlin ) . "\n");
echo('all int2 frankfurt dentists count: ' . count($int2DentistsFrankfurt ) . "\n");
echo('all int8 frankfurt dentists count: ' . count($int8DentistsFrankfurt ) . "\n");
exit;
$allInt1Dentists = array_merge($int1DentistsBarlin, $int1DentistsFrankfurt);
$allInt2Dentists = array_merge($int2DentistsBerlin, $int2DentistsFrankfurt);
$int1int2Dentists = array_merge($allInt1Dentists, $allInt2Dentists);
$allDentist = array_merge($int1int2Dentists, $int8DentistsFrankfurt);
echo('all dentists count: ' . count($allDentist ) . "\n");
$added = 0;
foreach ($allDentist as $userDto) {
	if ($doctorsTreatmentTypesManager->addTreatmentType($userDto->getId(), 1 , 142))
	{
		$added++;		
	}
	$userAAs = $usersAaManager->getByUserId($userDto->getId());
	
	foreach ($userAAs as $userAa) {
		$treatmentTypeIds = trim($userAa->getTreatmentTypeIds());		
		if (!empty($treatmentTypeIds))
		{	
			$treatmentTypeIdsArray =  explode(',', $treatmentTypeIds);
			if (!in_array(142, $treatmentTypeIdsArray ))
			{
				$treatmentTypeIdsArray[] = 142;
				$treatmentTypeIds = implode(',', $treatmentTypeIdsArray);
				$userAa->setTreatmentTypeIds($treatmentTypeIds);
				$usersAaManager->updateByPK($userAa);
			}
		}
	}
}
echo('Implant is added for ' . $added . " doctors"."\n");
echo("\n");
echo("\n");
shell_end();




function filterDoctorsByMedSpecAntAptTypeAndCity($locationDtos, $medSpecId, $hasContract, $city) {
	$ret = array();
	foreach ($locationDtos as $loc) {
		$userDtos = $loc->getUserDtos();
		foreach ($userDtos as $userDto) {
			$medicalSpecialtyIds = trim($userDto->getMedicalSpecialtyIds());
			$medicalSpecialtyIdsArray = array();
			if (!empty($medicalSpecialtyIds)) {
				$medicalSpecialtyIdsArray = explode(',', $medicalSpecialtyIds);
			}
			if (in_array($medSpecId, $medicalSpecialtyIdsArray) && $hasContract == $userDto->hasContract() && strpos( strtolower($loc->getCity()),$city)!== false) {
				$ret [] = $userDto;
			}
		}
	}
	return $ret;
}
?>