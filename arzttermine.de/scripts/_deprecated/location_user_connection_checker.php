<?php

include_once 'shell.php';

require_once $_SERVER['DOCUMENT_ROOT'] . "/../ngs/conf/constants.php";
require_once CLASSES_PATH . "/managers/LocationUserConnectionsManager.class.php";
require_once CLASSES_PATH . "/managers/UsersManager.class.php";
require_once CLASSES_PATH . "/managers/LocationsManager.class.php";

$locationsManager = LocationsManager::getInstance();
$usersManager = UsersManager::getInstance();
$locationUserConnectionsManager = LocationUserConnectionsManager::getInstance();

$allLocations = $locationsManager->selectAll();
$loc_member_ids_map1 = array();
foreach ($allLocations as $locDto) {
	$locMemeberIds = trim($locDto->getMemberIds());
	if (empty($locMemeberIds)) {
		$loc_member_ids_map1[$locDto->getId()] = array();
	} else {
		$loc_member_ids_map1[$locDto->getId()] = explode(',', $locMemeberIds);
	}
}

$allLocUserConnDtos = $locationUserConnectionsManager->selectAll();
$loc_member_ids_map2 = array();
foreach ($allLocUserConnDtos as $locUserConDto) {
	$loc_member_ids_map2[$locUserConDto->getLocationId()][] = $locUserConDto->getUserId();
}



$loc_missing_in_loc_user_conn_table = array_diff_key($loc_member_ids_map1, $loc_member_ids_map2);
$locIdsMissingInLocationUserConnTable = array_keys($loc_missing_in_loc_user_conn_table);
if (!empty($locIdsMissingInLocationUserConnTable)) {
	_e("Following locations are missing from location_user_table!!!!");
	_e(implode(', ', $locIdsMissingInLocationUserConnTable), 2);
	_e("fixing started...");
	foreach ($locIdsMissingInLocationUserConnTable as $locId) {
		$loc = $locationsManager->getById($locId);
		$memberIds = trim($loc->getMemberIds());
		if (!empty($memberIds)) {
			$memberIdsArray = explode(',', $memberIds);
			foreach ($memberIdsArray as $memberId) {
				$userById = $usersManager->getUserById($memberId);
				if (isset($userById)) {
					_e("User #" . $memberId, " and location #" . $locId . ' connection added into location_user_connection table!');
					$locationUserConnectionsManager->addConnection($memberId, $locId);
				} else {
					_e("Warning: User #" . $memberId . " doesn't exists (location #" . $locId . " used this user id in it's member_ids field)!");
				}
			}
		} else {
			_e("Warning: location #" . $locId . " doesn't have any member!");
		}
	}
	_e("fixing end");
	_e('', 3);
}



$missing_locations_dif = array_diff_key($loc_member_ids_map2, $loc_member_ids_map1);
$missingLocationsIds = array_keys($missing_locations_dif);
if (!empty($missingLocationsIds)) {
	_e("Following locations are not exists in locations table and the ids are still exists in location_user_table!!!!");
	_e(implode(', ', $missingLocationsIds), 2);
	_e("fixing started...");
	$locationUserConnectionsManager->removeByLocationIds($missingLocationsIds);
	_e("fixing end");
	_e('', 3);
}


_e("Following locations member_ids are not correspnding to location_user_connection table!!!!");
$allLocationsIds = array_unique(array_merge(array_keys($loc_member_ids_map1), array_keys($loc_member_ids_map2)));
asort($allLocationsIds, SORT_NUMERIC);
foreach ($allLocationsIds as $locId) {
	if (!array_key_exists($locId, $loc_member_ids_map1) || empty($loc_member_ids_map1[$locId]) || !array_key_exists($locId, $loc_member_ids_map2)) {
		$diffs[] = $locId;
		if (!array_key_exists($locId, $loc_member_ids_map1) || empty($loc_member_ids_map1[$locId])) {
			_e("Warning: location #" . $locId . " member_ids field is empty!");
			if (array_key_exists($locId, $loc_member_ids_map2)) {
				_e("and as location_user_connection table it has following users in it " . implode(', ', $loc_member_ids_map2[$locId]));
				_e('', 1);
				continue;
			}
			_e('', 1);
		}
		if (!array_key_exists($locId, $loc_member_ids_map2)) {
			_e("Warning: location #" . $locId . " doesn't have any connection in location_user_connection table!");
			if (array_key_exists($locId, $loc_member_ids_map1) && !empty($loc_member_ids_map1[$locId])) {
				_e("and member_ids field has this value " . implode(', ', $loc_member_ids_map1[$locId]));
				_e('', 1);
				continue;
			}
			_e('', 1);
		}
		continue;
	}
	$dif = array_diff($loc_member_ids_map1[$locId], $loc_member_ids_map2[$locId]);
	if (!empty($dif)) {
		_e("Warning: location #" . $locId . " member_ids:". implode(', ', $loc_member_ids_map1[$locId])." and in location_user_connection table has this members ". implode(', ', $loc_member_ids_map2[$locId]), 2 );
		$diffs[] = $locId;
	}
}
if ($diffs) {
	//_e(implode(', ', (array) $diffs));
}
shell_end();
?>


