<?php
include_once 'shell.php';

ini_set('memory_limit','500M');

require_once $_SERVER['DOCUMENT_ROOT'] . "/../ngs/conf/constants.php";
require_once (CLASSES_PATH . "/managers/SearchResultCacheTmpManager.class.php");
require_once (CLASSES_PATH . "/managers/TempDataManager.class.php");
require_once (CLASSES_PATH . "/managers/LocationsManager.class.php");
require_once (CLASSES_PATH . "/managers/UserLocationProfileInfoCachesManager.class.php");



$locationsManager = LocationsManager::getInstance();
$userLocationProfileInfoCachesManager = UserLocationProfileInfoCachesManager::getInstance();
$cacheDtos = $locationsManager->findAllVisibleDoctors();

echo('all locations-doctors count: ' . count($cacheDtos) . "\n");

$userLocationProfileInfoCachesManager->truncateTable();
//cacheing doctors data start
echo('cacheing doctors/locations data started' . "\n");

try {
	$userLocationProfileInfoCachesManager->cacheUserLocationProfileInfos($cacheDtos);
} catch (Exception $e) {
	// politely continue
	error_log('WARNING: Exception occurred');
}

//refreshing changed doctors data by system END
echo("\n");
echo('cacheing doctors data end' . "\n");
//cacheing doctors data end

echo("\n");
echo("\n");
shell_end();
?>
