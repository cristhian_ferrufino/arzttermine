<?php

//this file should be put under cronjob in the system to call periodically.
//this program is caching the samedi doctors available appointments.

include_once 'shell.php';

$locationsManager = \NGS\Managers\LocationsManager::getInstance();
$usersManager = \NGS\Managers\UsersManager::getInstance();

$activeLocations = $locationsManager->getLocationsByStatus(3);
$activeUsers = $usersManager->getUsersByStatus(3);

$log = "";

//ticket 1535 
$locationsPhoneVisibleArrayMap = array();
foreach ($activeLocations as $location) {
	if ($location->getIntegrationId() == 7) {
		continue;
	}
	$locPhoneVisible = preg_replace("/[^0-9]/", "", $location->getPhoneVisible());
	if (strlen($locPhoneVisible) > 3) {
		//at least 3 digit for valid phone number
		$locationsPhoneVisibleArrayMap[$location->getId()] = $locPhoneVisible;
	}
}

$similarPhoneNumbersLocationIds = array();
foreach ($locationsPhoneVisibleArrayMap as $key => $locationPhoneVisible) {
	$findSimilarPhoneNumbers = findSimilarPhoneNumbers($locationPhoneVisible, $locationsPhoneVisibleArrayMap);	
	if (count($findSimilarPhoneNumbers) > 1) {
		$similarPhoneNumbersLocationIds[] = implode(',', $findSimilarPhoneNumbers);
	}
}
$similarPhoneVisibleLocationsIdsGroup = array_unique($similarPhoneNumbersLocationIds);
echo "<br><br>";
$log .= "Similar visible phones number founded in following locations:" . "  \n<br>";
foreach ($similarPhoneVisibleLocationsIdsGroup as $locationIdsText) {
	$locationIdsArray = explode(',', $locationIdsText);
	foreach ($locationIdsArray as $key => $lid) {		
		$l = $locationsManager->getById($lid);		
		$log .= $key . ". location id:" . $lid . ". location name:" . $l->getName() . " location phone visible:" . $l->getPhoneVisible() . "  \n<br>";
	}
}

function findSimilarPhoneNumbers($pn, $locationsPhoneVisibleArrayMap) {
	$simillarItemsIds = array();
	foreach ($locationsPhoneVisibleArrayMap as $locId => $lpn) {
		$searchPhoneNumberRightPart = substr($pn, -5);
		$locationPhoneNumberRightPart = substr($lpn, -5);
		if (strpos($locationPhoneNumberRightPart, $searchPhoneNumberRightPart) !== false) {
			$simillarItemsIds[] = $locId;
		}
	}
	return $simillarItemsIds;
}
$log .= "# of active doctors: " . count($activeUsers) . "<br>";
$log .= "# of active locations: " . count($activeLocations) . "<br>";

foreach ($activeUsers as $user) {
    $ums = trim($user->getMedicalSpecialtyIds());
    if (empty($ums)) {
        $log .= "doctor #" . $user->getId() . " has no medical specialties." . "<br>";
    }
}
foreach ($activeLocations as $location) {
    if ($location->getIntegrationId() == 7) {
        continue;
    }
    $lms = trim($location->getMedicalSpecialtyIds());
    if (empty($lms)) {
        $log .= "location #" . $user->getId() . " has no medical specialties." . "<br>";
    }
}

$allLocationsWithDoctors = $locationsManager->getAllLocationsWithDoctors(3, 3);
foreach ($allLocationsWithDoctors as $loc) {
    if ($loc->getIntegrationId() == 7) {        
        continue;
    }
    $doc = $loc->getUserDto();
    $locMedSpec = $loc->getMedicalSpecialtyIds();
    $locMedSpecArray = explode(',', $locMedSpec);
    $docMedSpec = $doc->getMedicalSpecialtyIds();
    $docMedSpecArray = explode(',', $docMedSpec);
    $contains = count(array_intersect($locMedSpecArray, $docMedSpecArray)) == count($docMedSpecArray);
    if (!$contains) {
        $log .= "location #" . $loc->getId() . " doesn't contains medical specialties of doctor #" . $doc->getId() . "<br>";
    }
}


$medicalSpecialtiesManager = \NGS\Managers\MedicalSpecialtiesManager::getInstance();
$allMedicalSpecialtiesWithTreatmentTypes = $medicalSpecialtiesManager->getAllMedicalSpecialtiesWithTreatmentTypes();
$allMedWithTtMapped = array();
foreach ($allMedicalSpecialtiesWithTreatmentTypes as $med) {
    $allMedWithTtMapped [$med->getId()] = $med->getTreatmentTypesDtos();
}


$usersThatNotHaveTT = $usersManager->getUsersThatNotHaveTT(3);
foreach ($usersThatNotHaveTT as $doc) {
    $locationsByUserId = $locationsManager->getLocationsByUserId($doc->getId());
	foreach ($locationsByUserId as $locationByUserId) {
		if ($locationByUserId && $locationByUserId->getIntegrationId() != 7) {
			$docMedSpec = $doc->getMedicalSpecialtyIds();
			$docMedSpecArray = explode(',', $docMedSpec);
			$medHasTT = false;
			foreach ($docMedSpecArray as $msid) {
				if (!empty($allMedWithTtMapped[$msid])) {
					$medHasTT = true;
					break;
				}
			}
			if ($medHasTT) {
				$log .= "doctor #" . $doc->getId() . " doesn't have treatmenttype specified" . "<br>";
			}
		}
	}
}


foreach ($activeLocations as $location) {
    if ($location->getIntegrationId() == 7) {
        continue;
    }
    $locPhoneVisible = trim($location->getPhoneVisible());
    if (empty($locPhoneVisible)) {
        $log .= "location #" . $location->getId() . " has phone visible empty." . "<br>";
    }
    if (strpos($locPhoneVisible, '2222') !== false && strpos($locPhoneVisible, '0800') !== false && strpos($locPhoneVisible, '133') !== false) {
        $log .= "location #" . $location->getId() . " has phone visible 08002222133." . "<br>";
    }
}

echo("<br>");
echo("<br>");
echo $log;
echo("<br>");
echo("<br>");
$_mailing = new \Arzttermine\Mail\Mailing();
$_mailing->body_text = $log;
$_mailing->subject = "SYSTEM STATUS";
$_mailing->type = MAILING_TYPE_TEXT;
$_mailing->addAddressTo("th@arzttermine.de");
$_mailing->addAddressTo("bk@arzttermine.de");
$_mailing->addAddressTo("fb@arzttermine.de");
$_mailing->sendPlain();

shell_end();