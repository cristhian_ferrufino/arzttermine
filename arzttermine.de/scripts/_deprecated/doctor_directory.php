<?php

include_once 'shell.php';

require_once $_SERVER['DOCUMENT_ROOT'] . "/../ngs/conf/constants.php";
require_once CLASSES_PATH . "/managers/TempManager.class.php";
require_once CLASSES_PATH . "/managers/UsersManager.class.php";
require_once CLASSES_PATH . "/managers/LocationsManager.class.php";
require_once CLASSES_PATH . "/managers/LocationUserConnectionsManager.class.php";
require_once CLASSES_PATH . "/util/UtilFunctions.class.php";
require_once CLASSES_PATH . "/managers/DoctorsTreatmentTypesManager.class.php";
require_once CLASSES_PATH . "/managers/AssetsManager.class.php";

$MedSpecTreatmentTypeMap = array(
	1 => '1,2,3,5,6,41,43,44',
	2 => '15,166,187',
	3 => '9,10,11,12,13,14,48,56,59,137,140,168,182,183',
	4 => '16,52,83,84,87,131,150,169',
	5 => '17,61,62,65,67,120,125,145',
	6 => '18,70,71,126,146,167',
	7 => '19,98,99,133,171',
	9 => '21,135,155,172,176',
	12 => '20',
	15 => '178',
	17 => '',
	20 => '124,132,139,151,170,171',
	30 => '173'
);
$berlinZahnarztPhones = array(
	"030 2089 83110",
	"030 2089-83111",
	"030 208 983 112",
	"030 208983113",
	"030 208-983 114",
	"030 208 9831 15",
	"030 208-983 116",
	"030 208 98 31 17",
	"030 208-983 118",
	"030 208 983-119",
	"030 208 98-3120",
	"030 208 983 121",
	"030 2089 83122",
	"030 2089-83123",
	"030 208 98 3124",
	"030 208 983125"
);
$frankfurtZahnarztPhones = array(
	"069 5977 21710",
	"069 597 721 711",
	"069 5977-217 12",
	"069 59 77 21-713",
	"069 597 721714",
	"069 597 72 17 15",
	"069 597 721 716",
	"069 597721717",
	"069 597-721 718",
	"069 5977 21719",
	"069 3487-97460",
	"069 348 797 461",
	"069 348-797-462",
	"069 3487 97463",
	"069 348 797464",
	"069 348 79 7465"
);
$berlinHumanMedicinePhones = array(
	"030 6098 35150",
	"030 6098-35151",
	"030 609 835 152",
	"030 609835153",
	"030 609-835 154",
	"030 609 835155",
	"030 6098 35156",
	"030 609 835 157",
	"030 6098-35158",
	"030 577022459",
	"030 577 022 470",
	"030 5770 22471",
	"030 577 022 472",
	"030 5770-22473",
	"030 577 02 2474",
	"030 577 022475"
);

$tempManager = TempManager::getInstance();
$locationsManager = LocationsManager::getInstance();
$usersManager = UsersManager::getInstance();
$locationUserConnectionsManager = LocationUserConnectionsManager::getInstance();
$doctorsTreatmentTypesManager = DoctorsTreatmentTypesManager::getInstance();
$assetsManager = AssetsManager::getInstance();
$allTempDtos = $tempManager->selectAll();

//groupping by locations
echo "Total doctors numbers to be added: ".count($allTempDtos) ."\n";
echo "\n";

$locationCounter = 1;
$usersCounter = 1;
foreach ($allTempDtos as $index=>$dto) {

	$locationName = $dto->getLocationName();
	$gender = $dto->getGender();
	$title = $dto->getTitle();
	$fname = $dto->getFname();
	$lname = $dto->getLname();
	$street = $dto->getStreet();
	$zip = $dto->getZip();
	$city = $dto->getCity();
	$med_spec_id = $dto->getMedSpecId();



	$userSlug = UtilFunctions::createSlugFromString($title . ' ' . $fname . ' ' . $lname, 200);
	$userBySlug = $usersManager->getUserBySlug($userSlug);
	if (!empty($userBySlug)) {
		echo "skipping doctor '$title $fname $lname', it's already exists!" . "\n";
		continue;
	}

//creating user (doctor)
	$usersCounter++;
	$userDto = $usersManager->createDto();
	$userDto->setSlug($userSlug);
	$userDto->setStatus(USER_STATUS_VISIBLE_APPOINTMENTS);
	$userDto->setGroupId(GROUP_DOCTOR);
	$userDto->setGender((strtolower(substr($gender, 0, 1)) == 'f') ? 1 : 0);
	$userDto->setTitle($title);
	$userDto->setFirstName($fname);
	$userDto->setLastName($lname);
	$userDto->setMedicalSpecialtyIds(intval($med_spec_id));
	$userDto->setCreatedAt(date('Y-m-d H:i:s'));
	$userId = $usersManager->addUser($userDto);

	/*
	//adding random image for the doctor
	$userGender = $userDto->getGender();
	if ($userGender == 0) {
		$tempImagesSubfolder = 'male';
	} else {
		$tempImagesSubfolder = 'female';
	}
	$randomImagesDir = NGS_ROOT . '/data/temp_images/'.$tempImagesSubfolder;
	$randomImagePath = getRandomImage($randomImagesDir);
	$randomImageName = basename($randomImagePath);

	$ownerType = ASSET_OWNERTYPE_USER;
	$ownerId = $userId;
	$filesize = filesize($randomImagePath);
	$filesUpload['size'] = $filesize;

	$_FILES["upload"] = array("name" => $randomImageName, "type" => "image/png", 'tmp_name' => $randomImagePath, "error" => 0, 'size' => $filesize);
	$assetsManager->saveAsset($ownerType, $ownerId, $filesUpload, true);
    */
	//adding treatment types for doctor
	$treatmentTypesString = $MedSpecTreatmentTypeMap[$med_spec_id];
	$treatmentTypesArray = array();
	if (isset($treatmentTypesString)) {
		$treatmentTypesArray = explode(',', $treatmentTypesString);
	}
	$doctorsTreatmentTypesManager->addTreatmentTypesForDoctor($userId, $treatmentTypesArray);

	$locationSlug = UtilFunctions::createSlugFromString($locationName, 200);
	$locationBySlug = $locationsManager->getBySlug($locationSlug);


//creating location if needed
	if (isset($locationBySlug) && $locationBySlug->getZip() == $zip) {
		$memberIds = trim($locationBySlug->getMemberIds());
		
		if (!empty($memberIds)) {
			$memberIdsArray = explode(',', $memberIds);
			$memberIdsArray [] = $userId;
		} else {
			$memberIdsArray = array($userId);
		}		
		$locationsManager->updateTextField($locationBySlug->getId(), 'member_ids', implode(',', array_unique($memberIdsArray)));
		$locationUserConnectionsManager->addConnection($userId, $locationBySlug->getId());
		
		$medSpecIds = trim($locationBySlug->getMedicalSpecialtyIds());
		if (!empty($medSpecIds)) {
			$medSpecIdsArray = explode(',', $medSpecIds);
			$medSpecIdsArray [] = intval($med_spec_id);
		} else {
			$medSpecIdsArray = array(intval($med_spec_id));
		}	
		$locationsManager->updateTextField($locationBySlug->getId(),'medical_specialty_ids', implode(',', array_unique($medSpecIdsArray)));
		echo "Location updated, id:" . $locationBySlug->getId() . ", member added, memberid:" . $userId . "\n";
	} else {
		if (isset($locationBySlug)) {
			echo "skipping doctor '$title $fname $lname', location slug is duplicated, please add this doctor manually!" . "\n";
			$usersManager->deleteUserById($userId);
			continue;
		}
		$locationDto = $locationsManager->createDto();
		$locationDto->setCity($city);
		$locationDto->setSlug($locationSlug);
		$locationDto->setStatus(LOCATION_STATUS_VISIBLE_APPOINTMENTS);
		$locationDto->setIntegrationId(8);
		$locationDto->setName($locationName);
		$locationDto->setStreet($street);
		$locationDto->setZip($zip);
		$locationDto->setMemberIds($userId);
		$locationDto->setMedicalSpecialtyIds(intval($med_spec_id));
		$locationDto->setPositionFactor(-1);
		$locationDto->setCreatedAt(date('Y-m-d H:i:s'));
		//adding random phone number 
		if (strpos(strtolower($locationDto->getCity()),'berlin') !== false && $userDto->getMedicalSpecialtyIds() == 1) {
			$randomIndex = array_rand($berlinZahnarztPhones);
			$randomPhoneNumber = $berlinZahnarztPhones[$randomIndex];
			$locationDto->setPhoneVisible($randomPhoneNumber);
		} elseif (strpos(strtolower($locationDto->getCity()),'frankfurt') !== false && $userDto->getMedicalSpecialtyIds() == 1) {
			$randomIndex = array_rand($frankfurtZahnarztPhones);
			$randomPhoneNumber = $frankfurtZahnarztPhones[$randomIndex];
			$locationDto->setPhoneVisible($randomPhoneNumber);
		} else {
			$randomIndex = array_rand($berlinHumanMedicinePhones);
			$randomPhoneNumber = $berlinHumanMedicinePhones[$randomIndex];
			$locationDto->setPhoneVisible($randomPhoneNumber);
		}
		$locId = $locationsManager->addLocation($locationDto);
		$locationUserConnectionsManager->addConnection($userId, $locId);
		echo $locationCounter . ") Location added, id:" . $locId . ", member added, memberid:" . $userId . "\n";
		$locationCounter++;
	}
	echo "\n";
}


shell_end();

function getRandomImage($dir) {
	$files = glob($dir . '/*.*');
	$file = array_rand($files);
	return $files[$file];
}
?>


