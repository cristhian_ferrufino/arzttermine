<?php

require_once __DIR__ . '/../console.php';

use Arzttermine\Core\Console;
use Arzttermine\Location\Location;
use Arzttermine\User\User;
use Arzttermine\User\Doctor;

$console = Console::getInstance();

$console
    ->start()
    ->writeHeader('Setting dentist statuses', 2);

// Dentist MSIDs
$excluded_ids = array(1, 12, 13, 15, 17, 30, 92, 93, 94, 96);

// Query objects
$_user = new User();
$_location = new Location();

$console->writeLine('Getting all locations...');

// Get all locations so we can filter the dataset
$all_locations = $_location->findObjects('integration_id=8');

$console->writeSubheader('Found ' . count($all_locations) . ' locations');
$updates_made = 0;

/** @var Location[] $all_locations */
foreach ($all_locations as $location) {
    $doctors = $location->getAllMembers();

    $console->writeLine('Filtering doctors with medical specialties...');

    // Filter out medical specialties
    $doctors = array_filter($doctors, function($loc) use ($excluded_ids) {
        /** @var Doctor $loc */
        $msids = $loc->getMedicalSpecialtyIds();
        $intersection = array_intersect($excluded_ids, $msids);
        return empty($intersection);
    });

    /** @var User[] $doctors */
    foreach ($doctors as $doctor) {
        if (!$doctor->hasContract()) {
            $console
                ->writeLine('Doctor found [' . $doctor->getId() . ']: ' . $doctor->getFullName() . '. Specialties (' . implode(', ', $doctor->getMedicalSpecialtyIds()) . '). Updating doctor status');
            
            if ($doctor->setStatus(Doctor::STATUS_VISIBLE)->save('status')) {
                $console->writeLine('Updated doctor ID ' . $doctor->getId());
            } else {
                $console->writeWarning('No change for doctor ID ' . $doctor->getId());
            }
            
            $updates_made++;
        }
    }
}

$console
    ->writeNotice('Updated ' . $updates_made . ' doctors')
    ->stop();