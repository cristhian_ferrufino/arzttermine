<?php
/**
 * Update the location->integration_id for users with no contract
 */

require_once __DIR__ . '/../console.php';

use Arzttermine\Core\Console;
use Arzttermine\Location\Location;
use Arzttermine\User\User;

$console = Console::getInstance();

$console
    ->start()
    ->writeHeader('Setting integration id', 2);

// Query objects
$_user = new User();
$_location = new Location();

$console->writeLine('Getting all locations...');

// Get all locations so we can filter the dataset
$all_locations = $_location->findObjects('integration_id != 8');

$console->writeSubheader('Found ' . count($all_locations) . ' locations');
$updates_made = 0;

/** @var Location[] $all_locations */
foreach ($all_locations as $location) {
    $update = true;
    $doctors = $location->getAllMembers();

    /** @var User[] $doctors */
    foreach ($doctors as $doctor) {
        if ($doctor->hasContract()) {
            $update = false;
        }
    }

    if ($update) {
        if ($location->setIntegrationId(8)->save('integration_id')) {
            $console->writeLine('Updated location ID ' . $location->getId());
        } else {
            $console->writeWarning('No change for location ID ' . $location->getId());
        }
        $updates_made++;
    }
}

$console
    ->writeNotice('Updated ' . $updates_made . ' locations')
    ->stop();