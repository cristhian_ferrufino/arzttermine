<?php
/**
 * Import the coda data
 *
 * 1. transcode the file to UTF8 if needed:
 * iconv -f ISO_8859-1 -t UTF-8 arzttermin_Bestand_20160223.txt > coda.utf8.csv
 *
 * 2. get all sectorgs texts "printAllSectorgsTexts()" and build the array $VALID_SECTORGS_TEXTS
 *
 * 3. Import data
 *
 * 4. Check Import if there are any Slugs with "-XXXXXX" -> FullNameParser() or Slugify() failed
 *
 * 5. Get geocodes
 *
 * 6. Get genders for users: http://php.net/manual/de/gender.installation.php
 */

require_once __DIR__ . '/../console.php';
require_once __DIR__ . '/php-name-parser.php';

use Arzttermine\Core\Console;
use Arzttermine\Location\Location;
use Arzttermine\User\User;
use Arzttermine\Integration\IntegrationConnection;
use Cocur\Slugify\Slugify;

$filename = __DIR__ . '/../../coda-20160223.utf8.csv';

// map sector text to medical specialty id
$VALID_SECTORGS_TEXTS = array(
    'Zahnärzte' => 1,
    'Zahnärzte: Kieferorthopädie (Fachzahnärzte)' => 12,
    'Zahnärzte: Oralchirurgie (Fachzahnärzte)' => 17,
    'Ärzte: Allgemeinmedizin und Praktische Ärzte' => 2,
    'Ärzte: Anatomie' => 97,
    'Ärzte: Anästhesiologie' => 25,
    'Ärzte: Augenheilkunde' => 6,
    'Ärzte: Chirurgie' => 38,
    'Ärzte: Frauenheilkunde und Geburtshilfe' => 3,
    'Ärzte: Gefäßchirurgie' => 14,
    'Ärzte: Hals-Nasen-Ohrenheilkunde' => 4,
    'Ärzte: Haut- und Geschlechtskrankheiten' => 5,
    'Ärzte: Herzchirurgie' => 35,
    'Ärzte: Innere Medizin' => 20,
    'Ärzte: Kinder- und Jugendmedizin' => 34,
    'Ärzte: Kinder- und Jugendpsychiatrie und -psychotherapie' => 34,
    'Ärzte: Kinderchirurgie' => 34,
    'Ärzte: Lungen- und Bronchialheilkunde' => 43,
    'Ärzte: Mund-Kiefer-Gesichtschirurgie' => 15,
    'Ärzte: Neurochirurgie' => 46,
    'Ärzte: Neurologie' => 11,
    'Ärzte: Nuklearmedizin' => 60,
    'Ärzte: Orthopädie' => 7,
    'Ärzte: Osteopathie' => 77,
    'Ärzte: Pathologie' => 52,
    'Ärzte: Physikalische und Rehabilitative Medizin' => 63,
    'Ärzte: Physiologie' => 33,
    'Ärzte: Plastische und Ästhetische Chirurgie' => 10,
    'Ärzte: Psychiatrie und Psychotherapie' => 21,
    'Ärzte: Psychosomatische Medizin und Psychotherapie' => 8,
    'Ärzte: Radiologie' => 18,
    'Ärzte: Strahlentherapie' => 56,
    'Ärzte: Urologie' => 9
);

/**
 * NOT USED AS SOME SECTORGS_CODES HAVE NO CORRESPONDING SECTORGS_TEXT!!!
 * SO WE USE SECTORGS_TEXT INSTEAD!!!
 * LEFT FOR DEBUGGING AND TESTING
 *
 * @param string $filename
 * @return array
 *
 * CODA_ID;NAME;STREET;STREETNR;PLZ;CITY;FON;FAX;MOBILE;MAIL;WWW;SECTOR;SECTORGS_CODE;SECTORGS_TEXT;
 */
function getAllSectorgsCodes($filename)
{
    $allCodes = array();
    $row = 1;
    if (($handle = fopen($filename, "r")) !== FALSE) {
        while (($datas = fgetcsv($handle, 1000, ";")) !== FALSE) {
            // remove last empty field from source
            array_pop($datas);
            $data = array_combine(
                array('coda_id', 'name', 'street', 'streetnr', 'plz', 'city', 'fon', 'fax', 'mobile', 'mail', 'www', 'sector', 'sectorgs_code', 'sectorgs_text'),
                $datas
            );
            echo $data['coda_id'] . "\n";
            $sectorgs_codes = explode(';', $data['sectorgs_code']);
            // filter out wrong sectorgs_code with no sectorgs_text
            // 062513 Physiotherapie
            // 058475, 000263, 007560 Allgemeinarzt
            // 054631 Zahnarzt
            // 002792 Chirurgie
            $to_remove = array('062513','058475','000263','007560','054631','002792');
            $result = array_diff($sectorgs_codes, $to_remove);
            $sectorgs_codes = $result;

            if ($data['coda_id'] == 301511111) {
                print_r($sectorgs_codes);
                exit;
            }
            $sectorgs_texts = explode(';', $data['sectorgs_text']);
            $sectorgs_codes_and_texts = array_combine($sectorgs_codes, $sectorgs_texts);
            foreach ($sectorgs_codes_and_texts as $code => $text) {
                $allCodes[$code] = $text;
            }
            $row++;
        }
        fclose($handle);
    }
    ksort($allCodes);
    return $allCodes;
}

/**
 * Filter all unique sector gs texts
 * @param Console $console
 * @param string $filename
 * @return bool
 *
 * CODA_ID;NAME;STREET;STREETNR;PLZ;CITY;FON;FAX;MOBILE;MAIL;WWW;SECTOR;SECTORGS_CODE;SECTORGS_TEXT;
 */
function printAllSectorgsTexts($console, $filename)
{
    $console->writeLine('Extracting all sector texts...');

    $allTexts = array();
    $row = 1;
    if (($handle = fopen($filename, "r")) !== FALSE) {
        while (($datas = fgetcsv($handle, 1000, ";")) !== FALSE) {
            // remove last empty field from source
            array_pop($datas);
            $data = array_combine(
                array('coda_id', 'name', 'street', 'streetnr', 'plz', 'city', 'fon', 'fax', 'mobile', 'mail', 'www', 'sector', 'sectorgs_code', 'sectorgs_text'),
                $datas
            );
            $sectorgs_texts = explode(';', $data['sectorgs_text']);
            foreach ($sectorgs_texts as $text) {
                $allTexts[$text] = $text;
            }
            $row++;
        }
        fclose($handle);
    }
    asort($allTexts);

    foreach ($allTexts as $text) {
        echo $text ."\n";
    }

    return true;
}

/**
 * @param Console $console
 * @param string $filename
 *
 * CODA_ID;NAME;STREET;STREETNR;PLZ;CITY;FON;FAX;MOBILE;MAIL;WWW;SECTOR;SECTORGS_CODE;SECTORGS_TEXT;
 */
function importCsv($console, $filename) {
    $console->writeLine('Importing CSV: ' . $filename);

    $validItems = 0;
    $unvalidItems = 0;
    $row = 1;
    if (($handle = fopen($filename, "r")) !== FALSE) {
        while (($datas = fgetcsv($handle, 1000, ";")) !== FALSE) {
            // remove last empty field from source
            array_pop($datas);
            $data = array_combine(
                array('coda_id', 'name', 'street', 'streetnr', 'plz', 'city', 'fon', 'fax', 'mobile', 'mail', 'www', 'sector', 'sectorgs_code', 'sectorgs_text'),
                $datas
            );

            $console->writeLine('-------------------------------------------------');
            $console->writeLine('Handling row '.$row.': ' . $data['coda_id'] . ' / Name: ' . $data['name'] . ' / Sectors: ' . $data['sectorgs_text']);

            // check for valid sectors *****************************************************
            $console->writeLine('Checking for valid sector: '.$data['sectorgs_text']);

            $medicalSpecialtyIds = array();
            $sectorgs_texts = explode(';', $data['sectorgs_text']);
            foreach ($sectorgs_texts as $sectorgs_text) {
                if (array_key_exists($sectorgs_text, $GLOBALS['VALID_SECTORGS_TEXTS'])) {
                    $medicalSpecialtyIds[] = $GLOBALS['VALID_SECTORGS_TEXTS'][$sectorgs_text];
                }
            }
            asort($medicalSpecialtyIds);

            if (empty($medicalSpecialtyIds)) {
                $console->writeLine('SKIPPING: No valid sector');
                $unvalidItems++;
                continue;
            }
            $console->writeLine('Found valid sector(s)');

            // exclude some items by name **************************************************
            $exclude = false;
            if (preg_match('/klinik/i', $data['name'])) $exclude = true;
            if ($exclude) {
                $console->writeLine('SKIPPING: Excluding by name');
                $unvalidItems++;
                continue;
            }

            // import **********************************************************************

            // parse name to title, first_name, last_name *************
            $nameparser = new FullNameParser();
            $name = $data['name'];
            // handle some weired short cuts to bring name and php-name-parser-settings in sync
            $name = str_replace('M.Sc.', 'MSc.', $name);    // master of sience has a ununsual syntax, so rewrite it

            // add white spaces where needed
            // after a dot '.' e.g. at "Prof.Dr.Dr.med." -> "Prof. Dr. Dr. med."
            $name = preg_replace('/(\.)([[:alpha:]]{2,})/', '$1 $2', $name);

            // don't parse the name if it includes more than one person
            if (
                preg_match('/ und /i', $name)
                ||
                preg_match('/,/i', $name)
            ) {
                $name = array(
                    'salutation'    => '',
                    'fname'         => '',
                    'lname'         => $name,
                );
                $console->writeLine('INFO: Item includes more than one person');
            } else {
                $name = $nameparser->parse_name($name);
            }

            // create a valid slug ************************************
            $user = new User();

            $console->writeLine('Setup user...');
            $console->writeLine('Creating Slug for user...');

            $slugify = new Slugify();
            $slug = $slugify->slugify($name['salutation'].' '.$name['fname'].' '.$name['lname']);
            if (empty($slug)) {
                $console->writeLine('SKIPPING: No valid slug possible');
                $unvalidItems++;
                continue;
            }
            $postfix = 1;
            $tmpSlug = $slug;
            while ($user->finds('slug="'.$tmpSlug.'"')) {
                $postfix++;
                $tmpSlug = $slug.'-'.$postfix;
            }
            if ($slug != $tmpSlug) {
                $console->writeLine('WARNING: Redundant slug. Adding unique id');
            }
            $slug = $tmpSlug;

            $console->writeLine('Creating User with slug: "'.$slug.'"');

            $isDentist = false;
            foreach ($medicalSpecialtyIds as $medicalSpecialtyId) {
                if (in_array($medicalSpecialtyId, array(1,12,17))) {
                    $isDentist = true;
                    break;
                }
            }

            $userId = $user
                ->setStatus($isDentist ? USER_STATUS_VISIBLE_APPOINTMENTS : USER_STATUS_VISIBLE)
                ->setGroupId(GROUP_DOCTOR)
                ->setSlug($slug)
                ->setTitle($name['salutation'])
                ->setFirstName($name['fname'])
                ->setLastName($name['lname'])
                ->setEmail($data['mail'])
                ->setPhone($data['fon'])
                ->setMedicalSpecialtyIds($medicalSpecialtyIds)
                ->setDataSource('coda-v1-20160506:'.$data['coda_id'])
                ->saveNewObject();

            if ($userId == false) {
                $console->writeLine('ERROR: Could not create user. Skipping to next user.');
                continue;
            }

            // create a valid slug ************************************
            $location = new Location();

            $console->writeLine('Setup location...');
            $console->writeLine('Creating Slug for location...');

            $slugify = new Slugify();
            $slug = $slugify->slugify($name['salutation'].' '.$name['fname'].' '.$name['lname']);
            if (empty($slug)) {
                $console->writeLine('SKIPPING: No valid slug possible');
                $unvalidItems++;
                continue;
            }
            $postfix = 1;
            $tmpSlug = $slug;
            while ($location->finds('slug="'.$tmpSlug.'"')) {
                $postfix++;
                $tmpSlug = $slug.'-'.$postfix;
            }
            if ($slug != $tmpSlug) {
                $console->writeLine('WARNING: Redundant slug. Adding unique id');
            }
            $slug = $tmpSlug;

            $console->writeLine('Creating Location with slug: "'.$slug.'"');

            $locationId = $location
                ->setStatus($isDentist ? LOCATION_STATUS_VISIBLE_APPOINTMENTS: LOCATION_STATUS_VISIBLE)
                ->setSlug($slug)
                ->setIntegrationId(8)
                ->setName($data['name'])
                ->setEmail($data['mail'])
                ->setPhone($data['fon'])
                ->setWww($data['www'])
                ->setStreet($data['street'] . ' ' . $data['streetnr'])
                ->setZip($data['plz'])
                ->setCity($data['city'])
                ->setMedicalSpecialtyIds($medicalSpecialtyIds)
                ->setMemberIds($userId)
                ->setPrimaryMemberId($userId)
                ->setDataSource('coda-v1-20160506:'.$data['coda_id'])
                ->saveNewObject();

            if ($locationId == false) {
                $console->writeLine('ERROR: Could not create location. Skipping to next user.');
                $unvalidItems++;
                continue;
            }
            $integrationConnection = new IntegrationConnection();
            if (!$integrationConnection->connect($userId, $locationId, false)) {
                $console->writeLine('ERROR: Could not connect user and location');
            }

            $console->writeLine('OK');

            $validItems++;
            $row++;
        }
        fclose($handle);
    }

    $console->writeLine('Valid items: ' . $validItems);
    $console->writeLine('Unvalid items: ' . $unvalidItems);
}

/* ****************************************************************** */
$console = Console::getInstance();

$console
    ->start()
    ->writeHeader('Import coda data', 2);

/**
 * Extract all unique sector texts
 */
//printAllSectorgsTexts($console, $filename);

/**
 * Import all doctors with valid sector texts
 */
importCsv($console, $filename);

$console
    ->writeNotice('DONE')
    ->stop();