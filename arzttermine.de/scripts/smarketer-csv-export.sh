#!/bin/sh
FILE='/tmp/smarketer.csv'
if [ -f $FILE ]; then
   #get last run date
   modDate=$(stat -c %y $FILE)
   modDate=${modDate%% *}
else
   modDate='2016-11-01'
fi

echo "Getting doctors registered after $modDate"

# generate CSV file
mysql -uawsdbuser -p'#LiCcfgbJk2&sD7' --port=3306 --host=atdb.cmkf7bow2nry.eu-central-1.rds.amazonaws.com  -e "select count(*) as appointments, (select count(*) from appointments
where appointments.user_id=users.id and appointments.location_id=locations.id and appointments.start_time BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 5 DAY)) as appointments_next_5_days, locations.zip, locations.city, users.slug as doctor_slug, locations.slug as practice_slug
                                                                                              from appointments
                                                                                              left join users on appointments.user_id=users.id
                                                                                              left join locations on locations.id=appointments.location_id
                                                                                              where users.status=3
                                                                                              and locations.status=3
                                                                                              and users.created_at > '$modDate'
                                                                                              and appointments.start_time >= '$modDate'
                                                                                              and lower(users.slug) not like '%fake%'
                                                                                              and lower(locations.slug) not like '%fake%'
                                                                                              group by appointments.user_id
                                                                                              order by count(*) desc
                                                                                              limit 100;" www_arzttermine_de | sed 's/\t/","/g;s/^/"/;s/$/"/' > $FILE

if [ -s $FILE ]
then
    echo "uploading $FILE to smarketer"
    IP_address="www.smarketer-it.de"
    username="arzttermine@smarketer-it.de"
    password='p?8t5#Dq\2wc'

ftp -pvn $IP_address <<END_SCRIPT
user $username "$password"
put $FILE /smarketer.csv
bye
END_SCRIPT
exit 0

else
    echo "no doctors registered after $modDate"
fi