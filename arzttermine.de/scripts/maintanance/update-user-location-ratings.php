<?php

/**
 * Updates the ratings for all users and locations.
 * Usually this is done after a new rating so this would never be needed.
 */

require_once __DIR__.'/../shell.php';

$review = new \Arzttermine\Review\Review();
$reviews = $review->findObjects('1=1 GROUP BY user_id');

if (!empty($reviews)) {
    /** @var Arzttermine\Review\Review[] $reviews */
	foreach ($reviews as $review) {
	    $userId = $review->getUserId();
        $user = new \Arzttermine\User\User($userId);
        if (!$user->isValid()) {
            _e("ERROR: User with id ".$userId." failed");
        } else {
            _e("Updating user {$user->getName()}");
    		$user->updateRatings();
    		_e("Updating users locations");
    		$user->updateRatingsForLocations();
        }
	}
}

shell_end();
