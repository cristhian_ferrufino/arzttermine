<?php

/**
 * Updates the geocoordinates for all districts.
 * Actually this script is only needed if the districts are inserted via SQL
 * as the backend calculates the geocoordinates after inserting/changing.
 */

require_once __DIR__ . '/../shell.php';

$district = new \Arzttermine\Location\District();
$districts = $district->findObjects('lat IS NULL OR lng IS NULL OR lat=0 OR lng=0');

if (!empty($districts)) {
	foreach ($districts as $district) {
		_e("Updating district {$district->name} in {$district->city}");
		$district->updateGeocoordinates();
	}
}

shell_end();
