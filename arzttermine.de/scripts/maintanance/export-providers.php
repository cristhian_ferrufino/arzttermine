<?php
/**
 * export users and locations as csv
 */

define('SILENT_SHELL', true);

require_once __DIR__ . '/../shell.php';

Arzttermine\Location\Location::echoExportProviders();

shell_end();