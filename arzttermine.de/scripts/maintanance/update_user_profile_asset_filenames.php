<?php

include "shell.php";

$user = new User();
$users = $user->findObjects('1=1');
if (!empty($users)) {
	foreach ($users as $user) {
		if ($user->updateProfileAssetFilename()) {
			echo "OK: ".$user->id.':'.$user->first_name." ".$user->last_name."\n";
		} else {
			echo "ERROR: ".$user->id.':'.$user->first_name." ".$user->last_name."\n";
		}
	}
}

shell_end();
?>
