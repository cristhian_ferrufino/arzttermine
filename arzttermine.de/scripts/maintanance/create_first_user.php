#!/usr/bin/env php
<?php

require_once __DIR__ . '/../shell.php';

$app = \Arzttermine\Application\Application::getInstance();

$user = new \Arzttermine\User\User();
$password = 'password';
$data = array(
    'email'        => 'admin@arzttermine.dev',
    'first_name'   => 'Admin',
    'last_name'    => 'Development',
    'group_id'     => 1,
    'status'       => \Arzttermine\User\User::STATUS_ACTIVE,
    'activated_at' => $app->now(),
    'created_at'   => $app->now()
);

$id = $user->saveNew($data);
$user->load($id);

if (!$user->isValid()) {
    show_warning('Could not create user.');
} else {
    if ($user->changePassword($password)) {
        show_warning('User successfully created (ID ' . $user->getId() . ')');
    } else {
        show_warning('Could not save user (ID ' . $user->getId() . ')');
    }
}

shell_end();