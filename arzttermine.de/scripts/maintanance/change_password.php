#!/usr/bin/env php
<?php

require_once __DIR__ . '/../shell.php';

$password = 'password';
$user = new \Arzttermine\User\User('admin@arzttermine.dev', 'email');

if (!$user->isValid()) {
    show_warning('Could not create user.');
} else {
    if ($user->changePassword($password)) {
        show_warning('User successfully updated (ID ' . $user->getId() . ')');
    } else {
        show_warning('Could not update user (ID ' . $user->getId() . ')');
    }
}

shell_end();