<?php

require_once __DIR__ . '/../console.php';

use Arzttermine\Booking\Booking;
use Arzttermine\Core\Console;
use Arzttermine\Coupon\Coupon;
use Arzttermine\Coupon\UsedCoupon;
use Arzttermine\Referral\Referral;

$console = Console::getInstance();

$console
    ->start()
    ->writeHeader('Migrating referral codes', 2);

// Object for querying
$_rc = new Referral();

// Referral code (master codes) are rows without booking IDs
$referrals = $_rc->findObjects('booking_id IS NULL AND referral_code NOT LIKE "workhub%"');

$console->writeSubheader('Available referral codes: ' . count($referrals), 2);

/** @var Referral[] $referrals */
foreach ($referrals as $referral) {
    $console
        ->writeLine('Processing referral code [' . $referral->getReferralCode() . ']', 2)
        ->writeLine('Migrating to coupons table...');
    
    // Handle bad JSON data
    $restrictions = $referral->getRestrictions();
    if (empty($restrictions)) {
        $restrictions = array();
    }
    
    // Check the integrity of our data. Yes, stupid duplicates
    $existing_coupon = new Coupon($referral->getReferralCode(), 'code');
    if ($existing_coupon->isValid()) {
        $console->writeWarning('Coupon with code "' . $referral->getReferralCode() . '" already exists. Skipping');
    }
    unset($existing_coupon);

    // Copy over relevant fields
    $coupon = new Coupon();
    $coupon
        ->setCode($referral->getReferralCode())
        ->setType($referral->getType())
        ->setNote($referral->getData())
        ->setRestrictions($restrictions)
        ->setOfferType($referral->getPriceType())
        ->setCreatedBy($GLOBALS['CONFIG']['BOOKINGS_EMAIL_ADDRESS'])
        ->setId($referral->getId())
        ->saveNewObject(true);
    
    // Reload from the database to check that it migrated
    $coupon->load($coupon->getId());
    
    if ($coupon->isValid()) {
        $console->writeLine('Migration successful');
    } else {
        $console->writeWarning('Migration not completed [' . $referral->getReferralCode() . ']');
        continue;
    }

    // Find all used referral codes. These are codes with a booking_id
    $used_referrals = $_rc->findObjects('booking_id IS NOT NULL AND referral_code="' . $referral->getReferralCode() . '"');
    $console
        ->writeLine('Processing used referrals [' . $referral->getReferralCode() . ']')
        ->writeNotice('Found ' . count($used_referrals) . ' used referrals');
    
    /** @var Referral[] $used_referrals */
    foreach($used_referrals as $used_referral) {
        $console->writeLine('Migrating to bookings_coupons table...');
        
        $used_coupon = new UsedCoupon();
        // We also need the associated booking, as there's info there
        $booking = new Booking($used_referral->getBookingId());
        
        // If the associated booking cannot be found, skip
        if (!$booking->isValid()) {
            $console->writeWarning('Booking not found for ID [' . $used_referral->getId() . ']');
            continue;
        }
        
        // Copy to the bookings_coupons table
        $used_coupon
            ->setBookingId($booking->getId())
            ->setCouponId($coupon->getId())
            ->setEmail($booking->getEmail())
            ->setMedicalSpecialtyId($used_referral->getMedicalSpecialtyId())
            ->setStatus($used_referral->getStatus())
            ->saveNewObject();
        
        if ($used_coupon->isValid()) {
            $console->writeLine('Migration successful');
        } else {
            $console->writeWarning('Migration not completed for referral ' . $used_referral->getReferralCode() . ' [ID#' . $used_referral->getId() . ']');
        }
    }
}

$console->stop();