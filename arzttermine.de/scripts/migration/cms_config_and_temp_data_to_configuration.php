<?php

require_once __DIR__ . '/../console.php';

use Arzttermine\Application\Application;
use Arzttermine\Core\Console;
use Arzttermine\Core\ConfigurationData;

$console = Console::getInstance();

$console
    ->start()
    ->writeHeader('Migrating cms_config and temp_data to configuration', 2);

// Directly query the DB
$db = Application::getInstance()->getSql();

/*
 * cms_config
 */
$console->writeSubheader('Migrating cms_config to configuration');

$db->query('SELECT `var`, `value` FROM `cms_config`');

$cms_configs = $db->getResult();

$console
    ->writeNotice('Fetched ' . count($cms_configs) . ' rows')
    ->writeSubheader('Creating configuration data');

foreach ($cms_configs as $cms_config) {
    $configurationData = new ConfigurationData();
    $configurationData
        ->setOptionName($cms_config['var'])
        ->setOptionValue($cms_config['value']);

    $configurationData->saveNewObject();
}

/*
 * temp_data
 */
$console->writeSubheader('Migrating temp_data to configuration');

$db->query('SELECT `name`, `value` FROM `temp_data`');

$temp_datas = $db->getResult();

$console
    ->writeNotice('Fetched ' . count($temp_datas) . ' rows')
    ->writeSubheader('Creating configuration data');

foreach ($temp_datas as $temp_data) {
    $configurationData = new ConfigurationData();
    $configurationData
        ->setOptionName($temp_data['name'])
        ->setOptionValue($temp_data['value']);

    $configurationData->saveNewObject();
}

$console
    ->writeNotice('Migrated temp_data and cms_config to configuration')
    ->writeLine('Complete!')
    ->stop();