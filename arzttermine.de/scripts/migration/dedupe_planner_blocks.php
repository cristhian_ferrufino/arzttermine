<?php

/**
 * There are 20 weeks of recurring planner blocks that fill the same block. They
 * are treated as if there WAS no repeating event to begin with. We need to
 * filter out pointless repetitions when they match the original.
 **/

require_once __DIR__ . '/../console.php';

use Arzttermine\Application\Application;
use Arzttermine\Appointment\Planner;
use Arzttermine\Core\Console;
use Arzttermine\Core\DateTime;
use When\When;

$console = Console::getInstance();

$console
    ->start()
    ->writeHeader('Deduping Planner Blocks...', 2);

// Our DB
$db = Application::getInstance()->getSql();

// Get the FIRST date in the Planner Blocks table. We'll do this for generation
$db->query('SELECT start_time FROM planner_blocks ORDER BY start_time ASC LIMIT 1');
$result = $db->getResult();
$start_date = new DateTime($result[0]['start_time']);

// Now get the LAST date
$db->query('SELECT end_time FROM planner_blocks ORDER BY end_time DESC LIMIT 1');
$result = $db->getResult();
$final_date = new DateTime($result[0]['end_time']);

$console
    ->writeLine('Earliest date is ' . $start_date->getDate())
    ->writeLine('Latest date is '   . $final_date->getDate());

// Query all objects
$_p = new Planner();
$planners = $_p->findObjects();

// This will be used to generate recurring dates to test against
$recurrence_rule = new When();
$recurrence_rule
    ->startDate($start_date)
    ->until($final_date);

$console->writeNotice('Found ' . count($planners) . ' planners');

/** @var Planner $planner */
foreach ($planners as $planner) {
    $blocks = $planner->getBlocks();
    
    // Ignore if this planner is empty
    if (!$blocks) {
        continue;
    }
    
    $console->writeNotice('Planner#' . $planner->getId() . ' has ' . count($blocks) . ' blocks');
    
    $previous_count = count($blocks);
    
    foreach ($blocks as $template) {
        // We only want to check RECURRING events, as these cause problems inside the generator
        if (!$template->getRecurrenceInterval()) {
            continue;
        }
        
        // Our recurrence rule decides the dates
        $recurrence_rule
            ->freq($template->getRecurrenceInterval() === 7 ? 'weekly' : 'daily') // Kludge the frequency as we have to use constants
            ->generateOccurrences();

        // Test EACH BLOCK AGAIN. Yes, it's that bad
        foreach ($blocks as $index => $block) {
            // Ignore if it's the same object
            if ($template->getId() === $block->getId()) {
                continue;
            }
            
            $console->writeLine('Testing PlannerBlock#' . $block->getId());
            
            /** @var \DateTime $datetime */
            foreach ($recurrence_rule->occurrences as $datetime) {
                $console->ch('.');
                // Only check dates that are AHEAD of us, as dates behind us don't count
                if ($datetime->format('Y-m-d') <= $template->getStartTime()->getDate()) {
                    continue;
                }
                
                $test_block = clone $block;
                
                // We obviously assume the appointment block fits within 1 day
                $test_block
                    ->setStartTime($datetime->format('Y-m-d') . ' ' . $block->getStartTime()->getTime())
                    ->setEndTime($datetime->format('Y-m-d') . ' ' . $block->getEndTime()->getTime());
                
                // If we already have a recurring block for this date, delete the copy
                // The Dates need to fall on what would normally be a RECURRING date,
                // and also they need to have EXACTLY THE SAME insurances and TTs
                if (
                    $test_block->getStartTime()   == $block->getStartTime()    &&
                    $test_block->getEndTime()     == $block->getEndTime()      &&
                    $block->getInsuranceIds()     == $template->getInsuranceIds() &&
                    $block->getTreatmentTypeIds() == $template->getTreatmentTypeIds()
                ) {
                    unset($blocks[$index]);
                }
            }

            $console->writeLine('');
        }
    }
    
    $new_count = count($blocks);
    
    $console->writeNotice('Filtered out ' . ($previous_count - $new_count) . ' blocks for Planner#' . $planner->getId());
    
    $planner
        ->clearBlocks()
        ->setBlocks($blocks)
        ->saveBlocks();
}

$console
    ->writeNotice('Planner Blocks deduped')
    ->writeLine('Complete!')
    ->stop();