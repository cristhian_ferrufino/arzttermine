<?php

require_once __DIR__ . '/../console.php';

use Arzttermine\Appointment\Appointment;
use Arzttermine\Core\Console;
use NGS\DAL\Mappers\SearchResultCacheTmpMapper;

$console = Console::getInstance();

$console
    ->start()
    ->writeHeader('Migrating appointment formats', 2);

$old_appointments = array();
$new_appointments = array();

// Search Result Cache = Actually not a cache and holds all of our appointments
$search_result_cache = SearchResultCacheTmpMapper::getInstance();

$console->writeSubheader('Searching for old appointments');

// LOL performance
// For some stupid reason this whole thing has the wrong table name, so swap first
$old_appointments = $search_result_cache->swapTableName()->selectAll();

$console
    ->writeNotice('Found ' . count($old_appointments) . ' old appointments')
    ->writeSubheader('Generating new appointments');

/** @var NGS\DAL\DTO\SearchResultCacheDto $old_appointment */
foreach ($old_appointments as $old_appointment) {
    // Show progress
    $console->ch('.');
    
    // Create a new appointment object and assign relevant details. We'll push to the generator later
    $appointment = new Appointment();
    $appointment
        ->setUserId($old_appointment->getUserId())
        ->setLocationId($old_appointment->getLocationId())
        ->setInsuranceIds(explode(',', $old_appointment->getInsuranceIds()))
        ->setTreatmentTypeIds(explode(',', $old_appointment->getTreatmenttypeIds()))
        ->setDate($old_appointment->getDate())
        ->setStartTime($old_appointment->getAppointment())
        ->setEndTime($old_appointment->getAppointment()); // Make the start the same as the end or we get weird bugs
    
    $new_appointments[] = $appointment;
}

$console
    ->writeLine('') // To "break" the progress bar
    ->writeNotice('Generated ' . count($new_appointments) . ' new appointments')
    ->writeSubheader('Saving new appointments');

/** @var Appointment $appt */
foreach ($appointments as $appt) {
    $appt->save();
}

$console
    ->writeNotice('Migrated ' . count($new_appointments) . ' appointments')
    ->writeLine('Complete!')
    ->stop();