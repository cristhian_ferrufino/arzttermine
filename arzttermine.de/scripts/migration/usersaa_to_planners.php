<?php

require_once __DIR__ . '/../console.php';

use Arzttermine\Application\Application;
use Arzttermine\Appointment\Planner;
use Arzttermine\Appointment\PlannerBlock;
use Arzttermine\Core\Console;
use Arzttermine\Core\DateTime;
use Arzttermine\Insurance\Insurance;
use Arzttermine\Location\Location;

$console = Console::getInstance();

$console
    ->start()
    ->writeHeader('Migrating Users AA to Planners', 2);

// Directly query the DB as the UsersAA DAL sucks
$db = Application::getInstance()->getSql();

// We need to create default planners for each user/location, and then add dates to that planner
// All location/user combinations
$console->writeSubheader('Fetching all User/Location combinations');

$db->query('SELECT DISTINCT `user_id`, `location_id` FROM `users_aa`');

$users_locations = $db->getResult();
$planners = array();

$console
    ->writeNotice('Fetched ' . count($users_locations) . ' rows')
    ->writeSubheader('Creating default Planners');

foreach ($users_locations as $user_location) {
    // Handle only integration 1 locations
    $location = new Location($user_location['location_id']);
    if (intval($location->getIntegrationId()) !== 1) {
        $console->writeLine("Skipping Planner for User#{$user_location['user_id']} and Location#{$user_location['location_id']}. Wrong integration.");
        continue;
    }

    $planner = new Planner();
    $planner
        ->setUserId($user_location['user_id'])
        ->setLocationId($user_location['location_id']);

    $console->writeLine("Creating Planner for User#{$user_location['user_id']} and Location#{$user_location['location_id']}");
    $planner->saveNewObject();
    
    $planners[] = $planner;
}

$console->writeSubheader('Fetching UsersAA blocks');

// Used to filter blocks if they're recurring
function getBlocks($filtered) {
    $blocks = array();
    // @todo LOL
    foreach($filtered as $day) {
        foreach($day as $start) {
            foreach ($start as $end) {
                foreach ($end as $insurance) {
                    // We need to convert the date to "next week's" date as they're all Monday-Friday days and recurring
                    $date = new DateTime('next ' . $insurance['day']);
                    $insurance['date'] = $date->getDate();
                    $blocks[] = $insurance;
                    unset($date);
                }
            }
        }
    }
    return $blocks;
}

/** @var Planner $planner */
foreach ($planners as $planner) {
    $console->writeLine("Creating blocks for Planner#{$planner->getId()} with User#{$planner->getUserId()} and Location#{$planner->getLocationId()}");
    $blocks = array();

    // We need to get the fixed week flag :/
    $doctor = $planner->getUser();
    
    $console->writeLine('Fetching current blocks...');

    $db->query("SELECT * FROM `users_aa` WHERE `user_id` = {$planner->getUserId()} AND `location_id` = {$planner->getLocationId()}");
    
    $users_aa_blocks = $db->getResult();

    // If we have a fixed week calendar, make sure we filter out one appointment PER DAY OF WEEK
    if ($doctor->getFixedWeek()) {
        $filtered_blocks = array();
        foreach ($users_aa_blocks as $block) {
            $date = new DateTime($block['date']);
            $block['day'] = $date->format('l'); // Add this for later
            // This crap line means that if ALL the columns match, the array item is replaced (i.e. no duplicates)
            $filtered_blocks
                [$date->format('l')]
                [$block['start_at']]
                [$block['end_at']]
                [$block['insurance_types']]
                ['tt' . $block['treatment_type_ids']] = $block;
        }
        
        // Clear old block array before repopulating with filtered
        $users_aa_blocks = array();
        
        foreach ($filtered_blocks as $filtered_block) {
            $users_aa_blocks = array_merge(array_values(getBlocks($filtered_block)), array_values($users_aa_blocks));
        }
        
        unset($filtered_blocks);
    }
    
    $console
        ->writeLine('Found ' . count($users_aa_blocks) . ' blocks')
        ->writeLine('Migrating...');
    
    foreach($users_aa_blocks as $users_aa_block) {
        // Show progress
        $console->ch('.');
        
        $block = new PlannerBlock();
        $block->setPlanner($planner);
        
        //  Map the insurance types to actual values because for some reason they're stored as strings SMDH
        // NOTE: "Cash" type is converted to "Private"
        $insurance_ids = array_map(function($insurance) {
            $insurance = strtolower(trim($insurance));
            return $insurance === 'public' ? Insurance::GKV : Insurance::PKV;
        }, explode(',', $users_aa_block['insurance_types']));
        
        $block
            ->setLength($users_aa_block['block_in_minutes'])
            ->setStartTime("{$users_aa_block['date']} {$users_aa_block['start_at']}")
            ->setEndTime("{$users_aa_block['date']} {$users_aa_block['end_at']}")
            ->setInsuranceIds($insurance_ids)
            ->setTreatmentTypeIds(explode(',', $users_aa_block['treatment_type_ids']));
        
        // If the doctor has a recurring calendar, make the block itself recur weekly
        if ($doctor->getFixedWeek()) {
            $block->setRecurrenceInterval(7);
        }
        
        $blocks[] = $block;
    }

    $console->writeLine('')->writeLine('Saving Planner Blocks');
    
    $planner
        ->clearBlocks()
        ->setBlocks($blocks)
        ->saveBlocks();
    
    $console->writeNotice('Planner Blocks saved!');
}

$console
    ->writeNotice('Migrated Users AA to Planners')
    ->writeLine('Complete!')
    ->stop();