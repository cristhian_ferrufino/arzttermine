<?php

include_once 'shell.php';

require_once $_SERVER['DOCUMENT_ROOT'] . "/../ngs/conf/constants.php";

$fileLines = preg_split('/(%0[AD]|\n|\r)/i', file_get_contents(__DIR__ . "/111.csv"));
$patients = array();
foreach ($fileLines as $fileLine) {
	$fileLine = preg_replace('/\s+/', '', $fileLine);
	if (strpos($fileLine, '|') !== false) {
		$lineElements = explode('|', $fileLine);
		$lineElements = trimArrayElemets($lineElements, array(' ', ','));
		$patients[] = new ekomi_patient($lineElements[0], $lineElements[1], $lineElements[2], $lineElements[3], $lineElements[4]);
	}
}
foreach ($patients as $patient) {
	echo 'sending email: '. $patient->slug.' '.$patient->fname.' '.$patient->lname.' ('.$patient->email.')'."\n";
	sendEmail("/mailing/ekomi/patient-email.txt", $patient);
}

shell_end();
/////////END//////////////////////////////










function trimArrayElemets($array, $delimiters) {
	$ret = array();
	foreach ($array as $value) {
		foreach ($delimiters as $d) {
			$value = trim($value, $d);
		}
		$ret [] = $value;
	}
	return $ret;
}

function sendEmail($content_filename, $patient) {
	$_mailing = new Mailing();
	$_mailing->type = MAILING_TYPE_TEXT;
	$_mailing->from = $GLOBALS["CONFIG"]["BOOKINGS_EMAIL_ADDRESS"];
	$_mailing->from_name = $GLOBALS['CONFIG']["BOOKINGS_EMAIL_NAME"];
	$_mailing->data['domain'] = $GLOBALS['CONFIG']['DOMAIN'];
	$_mailing->data['id'] = $patient->id;
	$_mailing->data['first_name'] = $patient->fname;
	$_mailing->data['last_name'] = $patient->lname;
	$_mailing->data['slug'] = $patient->slug;
	$_mailing->data['email'] = $patient->email;
	$_mailing->content_filename = $content_filename;
	$_mailing->addAddressTo('39777-arztterminede@connect.ekomi.de');
	$_mailing->send();
}

class ekomi_patient {

	public $id;
	public $slug;
	public $fname;
	public $lname;
	public $email;

	function __construct($id, $slug, $fname, $lname, $email) {
		$this->id = $id;
		$this->slug = $slug;
		$this->fname = $fname;
		$this->lname = $lname;
		$this->email = $email;
	}

}

?>


