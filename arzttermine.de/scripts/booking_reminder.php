<?php
/**
 * This script sends out emails and SMS to patients depending by the integration
 * [1] arzttermine: 1 [EMail+SMS] and 5 [EMail+SMS] days before the appointment
 * [2] terminland:  1 [EMail+SMS] and 5 [EMail+SMS] days before the appointment
 * [5] samedi:      2 [EMail+SMS] and 5 [EMail] days before the appointment
 *      Some doctors disable the cancelation function 24 hours in advance in samedi,
 *      so we inform the patients 48 hours in advance so they still have 24h left to cancel
 */

use Arzttermine\Application\Application;
use Arzttermine\Booking\Booking;
use Arzttermine\Calendar\Calendar;
use Arzttermine\Mail\Mailing;
use Arzttermine\Mail\SMS;
use Arzttermine\Widget\WidgetFactory;

require_once 'shell.php';

// CONFIG
// **********************************************************

// Store the integration IDs as an array for the purpose of this script
// @todo: Refactor
$integration = array(
	'arzttermine' => 1,
	'terminland'  => 2,
	'samedi'      => 5
);

// HELPERS
// **********************************************************
/**
 * @param string $content_filename
 * @param Booking $booking
 */
function sendEmail($content_filename, $booking) {
	$user = $booking->getUser();
	$_mailing = new Mailing();
	$_mailing->type = MAILING_TYPE_HTML_AUTOTEXT;
	$_mailing->from = $GLOBALS["CONFIG"]["BOOKINGS_EMAIL_ADDRESS"];
	$_mailing->from_name = $GLOBALS['CONFIG']["BOOKINGS_EMAIL_NAME"];
	$_mailing->content_filename = $content_filename;
	$_mailing->data['domain'] = $GLOBALS['CONFIG']['DOMAIN'];
	$_mailing->data['booking_salutation_full_name'] = $booking->getSalutationName();
	$_mailing->data['user_name'] = $user->getName();
	$_mailing->data['appointment_date'] = Calendar::getWeekdayDateText($booking->getAppointmentStartAt());
	$_mailing->data['appointment_time'] = date('H:i', strtotime($booking->getAppointmentStartAt()));
	$_mailing->data['location_address'] = $booking->getLocation()->getName() . "<br>\r\n"
        .$booking->getLocation()->getStreet()."<br>\r\n"
        .$booking->getLocation()->getZip().' '.$booking->getLocation()->getCity();
	$_mailing->addAddressTo($booking->getEmail());
	$_mailing->send();
}

/**
 * Check if the booking was made inside the DAK widget
 * If yes, then initialize the widget and set the widget in the app
 * so that the mailing class sends out the correct templates
 *
 * @param Booking $booking
 */
function handleWidgetDAK($booking) {
    $app = Application::getInstance();
    if ($booking->getSourcePlatform() == 'widget-dak') {
        if (!$app->isWidget()) {
            if (!($app->getWidgetContainer() instanceof Arzttermine\Widget\Container)) {
                $app->setWidgetContainer(WidgetFactory::getWidgetContainer('dak'))->setIsWidget(true);
            }
            $app->setIsWidget(true);
        }
    } elseif ($app->isWidget()) {
        $app->setIsWidget(false);
    }
    return;
}

// BEGIN
// **********************************************************

setlocale(LC_TIME, 'de_DE');

$booking = new Booking();
$app = Application::getInstance();

// use all received statuses but the unconfirmed one
$booking_statuses = Booking::getReceivedStatuses(array(Booking::STATUS_RECEIVED_UNCONFIRMED));

// Reminders
$reminder_arzttermine_one  = $booking->getBookingsByDaysAfter(1, $integration['arzttermine'], $booking_statuses);
$reminder_arzttermine_five = $booking->getBookingsByDaysAfter(5, $integration['arzttermine'], $booking_statuses);

$reminder_terminland_one   = $booking->getBookingsByDaysAfter(1, $integration['terminland'], $booking_statuses);
$reminder_terminland_five  = $booking->getBookingsByDaysAfter(5, $integration['terminland'], $booking_statuses);

$reminder_samedi_two       = $booking->getBookingsByDaysAfter(2, $integration['samedi'], $booking_statuses);
$reminder_samedi_five      = $booking->getBookingsByDaysAfter(5, $integration['samedi'], $booking_statuses);

// Arzttermine (Integration 1)
// **********************************************************

// 5 days
_e();
show_notice('Arzttermine - 5 day reminders', 2, CONSOLE_HEAD_2);
_e('Bookings count: ' . count($reminder_arzttermine_five), 2);
_e('Processing reminders...');

foreach ($reminder_arzttermine_five as $booking) {
    // set widget to apply the correct mail templates
    handleWidgetDAK($booking);

	_e("Patient Email: {$booking->getEmail()} <{$booking->getPhone()}> (ID {$booking->getId()})");
    sendEmail("/mailing/integrations/{$integration['arzttermine']}/5-day-reminder.html", $booking);

    // dont send SMS to widget-dak bookings unless we have a way to change the texts for SMS as well
    if ($booking->getSourcePlatform() !== 'widget-dak') {
        $date = Calendar::getWeekdayDateText($booking->getAppointmentStartAt(), true);
        $time = date('H:i', strtotime($booking->getAppointmentStartAt()));
        $message = "Bitte denken Sie an Ihren Arzttermin am {$date} um {$time}. Zur Absage rufen Sie uns bitte kostenlos unter Tel. 0800 2222 133 an.";
        $message = SMS::convertGermanSymbolsToEnglish($message);

        SMS::sendSmsToGermany($booking->getPhone(), $message, "Booking ID: {$booking->getId()}");
    }

}

// 1 day
_e();
show_notice('Arzttermine - 1 day reminders', 2, CONSOLE_HEAD_2);
_e('Bookings count: ' . count($reminder_arzttermine_one), 2);
_e('Processing reminders...');

foreach ($reminder_arzttermine_one as $booking) {
    // set widget to apply the correct mail templates
    handleWidgetDAK($booking);

	_e("Patient Email: {$booking->getEmail()} <{$booking->getPhone()}> (ID {$booking->getId()})");
    sendEmail("/mailing/integrations/{$integration['arzttermine']}/1-day-reminder.html", $booking);

    // dont send SMS to widget-dak bookings unless we have a way to change the texts for SMS as well
    if ($booking->getSourcePlatform() !== 'widget-dak') {
        $time = date('H:i', strtotime($booking->getAppointmentStartAt()));
        $message = "Bitte denken Sie an Ihren Arzttermin morgen um {$time}. Zur Absage rufen Sie uns bitte kostenlos unter Tel. 0800 2222 133 an.";
        $message = SMS::convertGermanSymbolsToEnglish($message);

    	SMS::sendSmsToGermany($booking->getPhone(), $message, "Booking ID: {$booking->getId()}");
    }
}

// Terminland (Integration 2)
// **********************************************************

// 5 day
_e();
show_notice('Terminland - 5 day reminders', 2, CONSOLE_HEAD_2);
_e('Bookings count: ' . count($reminder_terminland_five), 2);
_e('Processing reminders...');

foreach ($reminder_terminland_five as $booking) {
    // set widget to apply the correct mail templates
    handleWidgetDAK($booking);

    _e("Patient Email: {$booking->getEmail()} <{$booking->getPhone()}> (ID {$booking->getId()})");
    sendEmail("/mailing/integrations/{$integration['terminland']}/5-day-reminder.html", $booking);

    // dont send SMS to widget-dak bookings unless we have a way to change the texts for SMS as well
    if ($booking->getSourcePlatform() !== 'widget-dak') {
        $time = date('H:i', strtotime($booking->getAppointmentStartAt()));
        $message = "Bitte denken Sie an Ihren Arzttermin am {$date} um {$time}. Zur Absage rufen Sie uns bitte kostenlos unter Tel. 0800 2222 133 an.";
        $message = SMS::convertGermanSymbolsToEnglish($message);

        SMS::sendSmsToGermany($booking->getPhone(), $message, "Booking ID: {$booking->getId()}");
    }
}

// 1 day
_e();
show_notice('Terminland - 1 day reminders', 2, CONSOLE_HEAD_2);
_e('Bookings count: ' . count($reminder_terminland_one), 2);
_e('Processing reminders...');

foreach ($reminder_terminland_one as $booking) {
    // set widget to apply the correct mail templates
    handleWidgetDAK($booking);

	_e("Patient Email: {$booking->getEmail()} <{$booking->getPhone()}> (ID {$booking->getId()})");
    sendEmail("/mailing/integrations/{$integration['terminland']}/1-day-reminder.html", $booking);

    // dont send SMS to widget-dak bookings unless we have a way to change the texts for SMS as well
    if ($booking->getSourcePlatform() !== 'widget-dak') {
        $time = date('H:i', strtotime($booking->getAppointmentStartAt()));
        $message = "Bitte denken Sie an Ihren Arzttermin morgen um {$time}. Zur Absage rufen Sie uns bitte kostenlos unter Tel. 0800 2222 133 an.";
        $message = SMS::convertGermanSymbolsToEnglish($message);

        SMS::sendSmsToGermany($booking->getPhone(), $message, "Booking ID: {$booking->getId()}");
    }
}

// Samedi (Integration 5)
// **********************************************************

// 5 days
_e();
show_notice('Samedi - 5 day reminders', 2, CONSOLE_HEAD_2);
_e('Bookings count: ' . count($reminder_samedi_five), 2);
_e('Processing reminders...');

foreach ($reminder_samedi_five as $booking) {
    // set widget to apply the correct mail templates
    handleWidgetDAK($booking);

	_e("Patient Email: {$booking->getEmail()} (ID {$booking->getId()})");
	sendEmail("/mailing/integrations/{$integration['samedi']}/5-day-reminder.html", $booking);
}

// 2 days
_e();
show_notice('Samedi - 2 day reminders', 2, CONSOLE_HEAD_2);
_e('Bookings count: ' . count($reminder_samedi_two), 2);
_e('Processing reminders...');

foreach ($reminder_samedi_two as $booking) {
    // set widget to apply the correct mail templates
    handleWidgetDAK($booking);

	_e("Patient Email: {$booking->getEmail()} <{$booking->getPhone()}> (ID {$booking->getId()})");
    sendEmail("/mailing/integrations/{$integration['samedi']}/2-day-reminder.html", $booking);

    // dont send SMS to widget-dak bookings unless we have a way to change the texts for SMS as well
    if ($booking->getSourcePlatform() !== 'widget-dak') {
        $time = date('H:i', strtotime($booking->getAppointmentStartAt()));
        $message = "Bitte denken Sie an Ihren Arzttermin übermorgen um {$time}. Zur Absage rufen Sie uns bitte kostenlos unter Tel. 0800 2222 133 an.";
        $message = SMS::convertGermanSymbolsToEnglish($message);

        SMS::sendSmsToGermany($booking->getPhone(), $message, "Booking ID: {$booking->getId()}");
    }
}

show_notice('Finished processing reminders', 2, CONSOLE_HEAD_2);

shell_end();
