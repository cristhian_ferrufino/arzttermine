<?php
/**
 * Sends out emails to patients which appointments took place in the last 5 hours
 * Should be called as a cronjob where the diff between the execution time should be at least as
 * long as 5 hours
 */

use Arzttermine\Booking\Booking;
use Arzttermine\Review\Review;

require_once __DIR__ . '/shell.php';

// use all received statuses but the unconfirmed one
$booking_statuses = Booking::getReceivedStatuses(array(Booking::STATUS_RECEIVED_UNCONFIRMED));
$notification_period = 5; // in hours

$bookings = Review::getBookingsForReviewNotifications($notification_period, array(1, 2, 5), $booking_statuses);

// @todo: do we really need this?
setlocale(LC_TIME, 'de_DE');

show_notice("All supplier reviews {$notification_period} hours before bookings", 2, CONSOLE_HEAD_2);
_e('Bookings count: ' . count($bookings));

foreach ($bookings as $booking) {
    _e("Booking ID: {$booking->getId()}, Patient email: {$booking->getEmail()}");

    $mailing_template = '/mailing/patients/review-doctor.html';
    if($booking->getUser()->hasPaidReview()) {
        $mailing_template = '/mailing/patients/paid-review-doctor.html';
    }

    if (Review::sendPatientEmail($mailing_template, $booking)) {
        _e("Email sent to {$booking->getEmail()}", 2);
    } else {
        show_notice('WARNING', 1, CONSOLE_NOTICE);
        _e("Could not send email to {$booking->getEmail()}", 2);
    }
}

_e('');

shell_end();