<?php
/**
 * Sends out one emails to a booking patients for debuging
 */

use Arzttermine\Booking\Booking;
use Arzttermine\Review\Review;

require_once __DIR__ . '/../shell.php';

$bookingId = 1;

$mailing_template = '/mailing/patients/review-doctor.html';

$booking = new Booking($bookingId);
if ($booking->isValid()) {
    _e('Sending review booking');
    _e("Booking ID: {$booking->getId()}, Patient email: {$booking->getEmail()}");
    if (Review::sendPatientEmail($mailing_template, $booking)) {
        _e("Email sent to {$booking->getEmail()}", 2);
    } else {
        show_notice('WARNING', 1, CONSOLE_NOTICE);
        _e("Could not send email to {$booking->getEmail()}", 2);
    }
} else {
    show_notice('WARNING', 1, CONSOLE_NOTICE);
    _e("Could not send email to {$booking->getEmail()}", 2);
}

_e('');

shell_end();