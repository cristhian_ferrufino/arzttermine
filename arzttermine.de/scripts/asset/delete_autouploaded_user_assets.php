<?php
/**
 * RUN THIS SCRIPT AS USER apache OR root TO let the script delete the assets in the FS!!!
 * Removes all assets that have been uploaded and assigned to the doctors automatically
 * with default avatars
 */

use Arzttermine\Application\Application;
use Arzttermine\Media\Asset;
use Arzttermine\User\User;

require_once __DIR__.'/../shell.php';

$app = Application::getInstance();
$sql = $app->getSql();

// get all auto uploaded assets for users
// 39.jpg is a real image and the only one #lucky
$sql_query = "
SELECT id FROM assets
WHERE parent_id = 0
AND owner_type = 0
AND filename != '39.jpg'
AND
(filename = 'a.jpg'
OR `filename` = 'b.jpg'
OR `filename` = 'c.jpg'
OR `filename` = 'd.jpg'
OR `filename` = 'e.jpg'
OR `filename` = 'f.jpg'
OR `filename` = 'g.jpg'
OR `filename` = 'h.jpg'
OR `filename` = 'i.jpg'
OR `filename` = 'j.jpg'
OR `filename` = 'k.jpg'
OR `filename` = 'l.jpg'
OR `filename` = 'm.jpg'
OR `filename` = 'n.jpg'
OR `filename` = 'o.jpg'
OR `filename` = 'p.jpg'
OR `filename` = 'q.jpg'
OR `filename` = 'r.jpg'
OR `filename` = 's.jpg'
OR `filename` = 't.jpg'
OR `filename` = 'u.jpg'
OR `filename` = 'v.jpg'
OR `filename` = 'w.jpg'
OR `filename` = 'x.jpg'
OR `filename` LIKE 'female_doc_%'
OR `filename` LIKE 'male_doc_%'
OR `filename` REGEXP BINARY '^[0-9]{1,2}\.jpg'
)
;";
$sql->query($sql_query);
$ids = array();
if ($sql->nf() > 0) {
	while ($sql->next_record()) {
		$ids[] = $sql->f('id');
	}
}

foreach ($ids as $id) {
	$asset = new Asset($id);
    if (!$asset->isValid()) {
        _e('Error: Invalid asset id: '.$id);
        continue;
    }
    $userId = $asset->getOwnerId();
    $user = new User($userId);
    if (!$user->isValid()) {
        // spit out a warning but delete the assets anyway
        _e('Error: Invalid user id: '.$userId.' with asset id: '.$id);
    } else {
        if (-1 == $user
            ->setProfileAssetId(0)
            ->setProfileAssetFilename('')
            ->save(array('profile_asset_id', 'profile_asset_filename'))) {

            _e('Error: Error on saving user id: '.$userId.' with asset id: '.$id);
            continue;
        }
    }

    $filename = $asset->getFilename();
    if (!$asset->deleteWithChildren()) {
        echo "Error: ";
    } else {
        echo "OK: ";
    }
    echo $id.' / '.$filename."\n";

    // if the user has other fotos assigned that update the users profile_asset_id/profile_asset_filename
    $user->updateProfileAssetFilename();
}

shell_end();
