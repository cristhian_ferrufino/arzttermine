<?php
/**
 * RUN THIS SCRIPT AS USER apache OR root TO let the script delete the assets in the FS!!!
 * Removes all assets that point/reference to non existant users or locations
 */

use Arzttermine\Application\Application;
use Arzttermine\Media\Asset;
use Arzttermine\User\User;

require_once __DIR__.'/../shell.php';

$app = Application::getInstance();
$sql = $app->getSql();

$sql_query = "
SELECT
    a.id, a.owner_id, a.owner_type, a.filename
FROM assets a
WHERE
    a.parent_id = 0
    AND
    (
        (
            a.owner_type = 0
            AND a.owner_id NOT
            IN (SELECT id FROM users)
        ) OR (
            a.owner_type = 1
            AND a.owner_id NOT
            IN (SELECT id FROM locations)
        )
    )
;";
$sql->query($sql_query);
$ids = array();
if ($sql->nf() > 0) {
	while ($sql->next_record()) {
		$ids[] = $sql->f('id');
	}
}

foreach ($ids as $id) {
	$asset = new Asset($id);
    if (!$asset->isValid()) {
        _e('Error: Invalid asset id: '.$id);
        continue;
    }

    $filename = $asset->getFilename();
    if (!$asset->deleteWithChildren()) {
        echo "Error: ";
    } else {
        echo "OK: ";
    }
    echo $id.' / '.$filename."\n";
}

shell_end();
