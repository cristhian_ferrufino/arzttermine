<?php
/**
 * Updates the asset filenames (cache) for the locations
 */

use Arzttermine\Location\Location;

require_once __DIR__.'/../shell.php';

$location = new Location();
$locations = $location->findObjects('1=1');
if (!empty($locations)) {
    /**
     * @var Location $location
     */
    foreach ($locations as $location) {
		if ($location->updateProfileAssetFilename()) {
			echo "OK: ".$location->getId().':'.$location->getName()."\n";
		} else {
			echo "ERROR: ".$location->getId().':'.$location->getName()."\n";
		}
	}
}

shell_end();
