<?php

/**
 * Finds assets where the parent asset has been deleted and
 * creates the parent by using the childs attributes
 */

use Arzttermine\Application\Application;
use Arzttermine\Media\Asset;

require_once __DIR__.'/../shell.php';

$app = Application::getInstance();
$sql = $app->getSql();

// get all referenced parent_ids (existant or not)
$sql_query = "SELECT parent_id FROM assets GROUP BY parent_id;";
$sql->query($sql_query);
$keys = array();
if ($sql->nf() > 0) {
	while ($sql->next_record()) {
		$keys[] = $sql->f('parent_id');
	}
}

foreach ($keys as $parent_id) {
	$asset = new Asset();
	$found = $asset->findKey('id='.$parent_id);
	// skip found assets, they are fine
	if (!$found) {
        // work with child size=1
		$asset = $asset->findObject('parent_id='.$parent_id.' AND size=1');

        /**
         * @var Asset $asset
         */
        if ($asset->isValid()) {
            // remove the _1 from the filename to get the original filename
			if (preg_match('/(.*)\_1\.(.*)/', $asset->getFilename(), $matches)) {
				$filename = $matches[1].'.'.$matches[2];

				$data = array(
						'id' => $asset->getParentId(),
						'gallery_id' => 0,
						'owner_id' => $asset->getOwnerId(),
						'owner_type' => $asset->getOwnerType(),
						'parent_id' => 0,
						'size' => 0,
						'filesize' => $asset->getFilesize(),
						'width' => 0,
						'height' => 0,
						'description' => '',
						'filename' => $filename,
						'mime_type' => $asset->getMimeType(),
						'created_at' => $asset->getCreatedAt(),
						'created_by' => $asset->getCreatedBy()
						);

				if (!$asset->saveNew($data, true)) {
					echo "Error: ";
				} else {
                    echo "OK: ";
                }
                echo $data['id']."\n";
            }
		}
	}
}

shell_end();
