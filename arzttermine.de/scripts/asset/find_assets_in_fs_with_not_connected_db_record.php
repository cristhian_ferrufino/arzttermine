<?php
/**
 * RUN THIS SCRIPT AS USER apache OR root TO let the script delete the assets in the FS!!!
 * Finds all assets in the filesystem that have no record in the db.
 * In fact the script doesn't deletes but prints out the commands for each directory to be deleted.
 * So you need to output that into a file and then execute that file.
 *
 * Just to make sure that nothing gets deleted what should not!!!
 */

use Arzttermine\Media\Asset;

require_once __DIR__.'/../shell.php';

function rscandir($dir) {
    $dirs = array_fill_keys( array_diff( scandir( $dir ), array( '.', '..' ) ), array());
    foreach ( $dirs as $d => $v ) {
        $current = $dir."/".$d;
        if ( is_file($current)) {
            // echo "$current\n";
        }
        if ( is_dir($current) ) {
            // only use full asset dirs with 3 dir parts
            if (preg_match('#(\d{3})/(\d{3})/(\d{3})$#', $current, $matches)) {
                $id = ltrim($matches[1].$matches[2].$matches[3], '0');
                $asset = new Asset($id);
                if (!$asset->isValid()) {
                    // only print the command but don't do it!!!!!!!!!!
                    _e('rm -rf '.$current);
                }
            }
            $dirs[$d] = rscandir($current);
        }
    }
    return $dirs;
}

$dir    = realpath($GLOBALS['CONFIG']['ASSET_PATH']);
$dirs = rscandir($dir);

shell_end();
