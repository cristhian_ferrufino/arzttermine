<?php
/**
 * Finds assets where the file is not existant in the filesystem (but in the db)
 */

use Arzttermine\Application\Application;
use Arzttermine\Media\Asset;
use Arzttermine\User\User;

require_once __DIR__.'/../shell.php';

$app = Application::getInstance();
$sql = $app->getSql();

$asset = new Asset();
$assetIds = $asset->findKeys('1=1', 'id');

foreach ($assetIds as $id) {
	$asset = new Asset($id);
    if (!$asset->isValid()) {
        _e('Error: Invalid asset id: '.$id);
        continue;
    }

    $filepath = $asset->getFilepath();
    if (!file_exists($filepath)) {
        echo "Error: File does not exist: ";
        echo $id.' / '.$filepath."\n";
    }
}

shell_end();
