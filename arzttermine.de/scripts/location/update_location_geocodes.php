<?php
/**
 * build a tunnel to production db (on web01) to avoid reaching googles maps api limits
 * ssh -nNT -L 3307:localhost:3306 deploy@91.203.200.141
 * and configure the development config to use production settings.
 */

require_once __DIR__ . '/../console.php';

use Arzttermine\Core\Console;
use Arzttermine\Location\Location;

$console = Console::getInstance();

$console
    ->start()
    ->writeHeader('Import coda data', 2);

$oks = 0;
$errors = 0;

$location = new Location();
$locationIds = $location->findKeys('(lat="0" OR lng="0") AND id > 300000', 'id', 'id');

if (is_array($locationIds) && !empty($locationIds)) {
    
    $console->writeLine("Number of locations: " . count($locations));
    
    foreach ($locationIds as $locationId) {
        $location = new Location($locationId);
        if (!$location->isValid()) continue;

        $_address = $location->getStreet() . ',' . $location->getZip() . ' ' . $location->getCity();
        $_coords = \Arzttermine\Maps\GoogleMap::getGeoCoords($_address);
        if (!empty($_coords)) {
            $location
                ->setLat($_coords['lat'])
                ->setLng($_coords['lng'])
                ->save(array('lat', 'lng'));
            $console->writeLine("Updated: [{$location->getId()}] {$location->getName()}");
        } else {
            $console->writeLine("Error: [{$location->getId()}] {$location->getName()}");
        }
    }
}

$console
    ->writeNotice('DONE')
    ->stop();