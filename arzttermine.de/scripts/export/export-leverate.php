<?php
/**
 * export users and locations as csv
 * usage: php scripts/maintanance/export-universal-widget.php -e development
 */

#define('SILENT_SHELL', true);

require_once __DIR__ . '/../shell.php';

use Arzttermine\Application\Application;
use Arzttermine\User\User;
use Arzttermine\Location\Location;

$db = Application::getInstance()->getSql();

$sql
    = '
			SELECT
				u.id AS user_id, l.id AS location_id
			FROM ' . DB_TABLENAME_USERS . ' u, ' . DB_TABLENAME_LOCATIONS . ' l, ' . DB_TABLENAME_LOCATION_USER_CONNECTIONS . ' luc
		 	WHERE
				luc.user_id = u.id
			AND
				luc.location_id = l.id
			AND
				u.status = ' . USER_STATUS_VISIBLE_APPOINTMENTS . '
			AND
				u.has_contract=1
			AND
				(
				l.city = "Berlin"
				OR
				l.city = "München"
				OR
				l.city = "Köln"
				OR
				l.city = "Hamburg"
				OR
				l.city LIKE "Frankfurt%"
				)
			AND
                l.country_code = "DE"
            AND
                u.comment_intern NOT LIKE "%REG%"
			ORDER BY l.city ASC, l.id ASC';

$db->query($sql);
$records = array();
if ($db->nf() > 0) {
    while ($db->next_record()) {
        $records[] = $db->Record;
    }
}

if (empty($records)) {
    $data = 'ERROR: No available records';
} else {
    $data = '"UserId";"LocationCity";"LocationZip";"LocationZip2";"Category";"CategoryName";"UserGender";"UserName";"CategoryArtice";"Ihr";"PostText";"PostTitle";"PostDescription";"ImageUrl";"DestinationUrl";"DomainCaption";"Price";"Currency"' . "\n";
    foreach ($records as $record) {
        $userId = $record['user_id'];
        $locationId = $record['location_id'];

        $user = new User($userId);
        $location = new Location($locationId);
        $providerId = $location->getId() . '-' . $user->getId();

        if ($user->isValid() && $location->isValid()) {
            if (!in_array(1, $user->getMedicalSpecialtyIds())) {
                continue;
            }
            $data .= sprintf(
                    '"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s"',
                    sanitize_csv_field($user->getId()),
                    sanitize_csv_field($location->getCity()),
                    sanitize_csv_field($location->getZip()),
                    sanitize_csv_field(''),
                    sanitize_csv_field('zahnarzt'),
                    sanitize_csv_field('Zahnarzt'),
                    sanitize_csv_field($user->getGenderText()),
                    sanitize_csv_field($user->getName()),
                    sanitize_csv_field(''),
                    sanitize_csv_field(''),
                    sanitize_csv_field(''),
                    sanitize_csv_field(''),
                    sanitize_csv_field($location->getStreet().','.$location->getZip().' '.$location->getCity()),
                    sanitize_csv_field('https://www.arzttermine.de'.$user->getProfileAssetUrl(ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED)),
                    sanitize_csv_field($user->getUrl(true).'/gkv'),
                    sanitize_csv_field('www.arzttermine.de'),
                    sanitize_csv_field(''),
                    sanitize_csv_field('')
                ) . "\n";
        }
    }
    echo $data;
}

shell_end();