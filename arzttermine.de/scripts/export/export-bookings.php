<?php
/**
 * Export bookings
 * 
 * Spit out the data serialized to reduce memory allocation
 * 
 */

define('SILENT_SHELL', true);

require_once __DIR__.'/../shell.php';

echo '"BookingId","BookingCreatedAt","BookingAppointment","BookingStatusId","BookingStatusText","BookingInsuranceId","BookingInsuranceText",'.
		'"BookingReturningVisitor","BookingTreatmentType","BookingSourcePlatform",'.
		'"PatientGender","PatientFirstName","PatientLastName","PatientEmail","PatientPhone",'.
		'"LocationId","LocationName","LocationStreet","LocationZIP","LocationCity","LocationIntegrationId","LocationIntegrationText",'.
		'"UserId","UserName","UserPays","UserMedicalSpecialties"'.
		"\n";

$booking = new \Arzttermine\Booking\Booking();
$bookingIds = $booking->findKeys();
if (!empty($bookingIds)) {
	foreach ($bookingIds as $bookingId) {
		$booking->load($bookingId);
		if (!$booking->isValid()) {
			continue;
		}
		$user = $booking->getUser();
		$location = $booking->getLocation();

		$userId = 'n/a';
		$userName = 'n/a';
		$userIsPaying = 'n/a';
		$userMedicalSpecialties = 'n/a';

		$locationId = 'n/a';
		$locationName = 'n/a';
		$locationStreet = 'n/a';
		$locationZip = 'n/a';
		$locationCity = 'n/a';
		$locationIntegrationId = 'n/a';
		
		if ($user->isValid()) {
			$userId = $user->getId();
			$userName = $user->getName();
			$userIsPaying = $user->hasContract() ? 'Ja' : 'Nein';
			$userMedicalSpecialties = $user->getMedicalSpecialityIdsText(',');
		}
		if ($location->isValid()) {
			$locationId = $location->getId();
			$locationName = $location->getName();
			$locationStreet = $location->getStreet();
			$locationZip = $location->getZip();
			$locationCity = $location->getCity();
			$locationIntegrationId = $location->getIntegrationId();
		}

		$integration = $booking->getIntegration();
		$bookingIntegrationName = 'n/a';
		if (is_object($integration)) {
			$bookingIntegrationName = $booking->getIntegration()->getName();
		}

		$csv = sprintf('"%d","%s","%s","%d","%s","%d","%s",'.
				'"%s","%s","%s",'.
				'"%s","%s","%s","%s","%s",'.
				'"%d","%s","%s","%s","%s","%d","%s",'.
				'"%d","%s","%s","%s"',
				$booking->getId(),
				$booking->getCreatedAtFormat('Y-m-d H:i:s'),
				$booking->getAppointment()->getDateTime(),
				$booking->getStatus(),
				$booking->getStatusText(),
				$booking->getInsuranceId(),
				$booking->getInsuranceText(),

				$booking->getReturningVisitorText(),
				$booking->getTreatmenttypeText(),
				$booking->getSourcePlatform(),

				$booking->getGenderText(),
				$booking->getFirstName(),
				$booking->getLastName(),
				$booking->getEmail(),
				$booking->getPhone(),

				$locationId,
				$locationName,
				$locationStreet,
				$locationZip,
				$locationCity,
				$locationIntegrationId,
				$bookingIntegrationName,

				$userId,
				$userName,
				$userIsPaying,
				$userMedicalSpecialties
		);
		echo $csv."\n";
	}
}

shell_end();
