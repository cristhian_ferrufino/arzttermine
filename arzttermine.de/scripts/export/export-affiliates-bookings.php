<?php
/**
 * Export bookings anonymized for affiliates
 *
 */

define('SILENT_SHELL', true);

$email = 'gs@arzttermine.de';

require_once __DIR__ . '/../shell.php';

use Arzttermine\Booking\Booking;

$booking = new Booking();
$affiliate = new \Arzttermine\User\Affiliate($email, 'email');

if ($affiliate->isValid()) {
    $statuses = Booking::getChargeableStatuses();
    $statuses_sql = Booking::getStatusesSql($statuses, 'OR');
    $bookings = $booking->findObjects($statuses_sql . ' AND source_platform="' . $affiliate->getSourcePlatform() . '"');
    if (!empty($bookings)) {
        /** @var Booking[] $bookings */
        foreach ($bookings as $booking) {
            $user = $booking->getUser();
            $location = $booking->getLocation();
            if (!$user->isValid() || !$location->isValid()) {
                continue;
            }

            $csv .= sprintf(
                '"%s","%s","%s","%s","%s","%s"',
                $booking->getAppointment()->getDate(),
                $booking->getInsuranceText(),
                $user->getHasContract(),
                $location->getZip(),
                $location->getCity(),
                $user->getMedicalSpecialityIdsText()
            );
            $csv .= "\n";
        }
        echo '"Termin_Datum","Patient_Versicherung","Doktor_Mit_Vertrag","Praxis_PLZ","Praxis_Ort","Doktor_Fachrichtungen"' . "\n";
        echo $csv;
    }
}

shell_end();
