<?php
/**
 * export users and locations as csv
 * usage: php scripts/maintanance/export-universal-widget.php -e development
 */

#define('SILENT_SHELL', true);

require_once __DIR__ . '/../shell.php';

use Arzttermine\Application\Application;
use Arzttermine\User\User;
use Arzttermine\Location\Location;

$db = Application::getInstance()->getSql();

$sql
    = '
			SELECT
				u.id AS user_id, l.id AS location_id
			FROM ' . DB_TABLENAME_USERS . ' u, ' . DB_TABLENAME_LOCATIONS . ' l, ' . DB_TABLENAME_LOCATION_USER_CONNECTIONS . ' luc
		 	WHERE
				luc.user_id = u.id
			AND
				luc.location_id = l.id
			AND
				u.status = ' . USER_STATUS_VISIBLE_APPOINTMENTS . '
			AND
				u.has_contract=1
			AND
                l.country_code = "DE"
            AND
                u.comment_intern NOT LIKE "%REG%"
			ORDER BY l.id ASC';

$db->query($sql);
$records = array();
if ($db->nf() > 0) {
    while ($db->next_record()) {
        $records[] = $db->Record;
    }
}

if (empty($records)) {
    $data = 'ERROR: No available records';
} else {
    $data = '"ProviderId";"LocationName";"LocationStreet";"LocationStreetWONumber";"LocationStreetNumber";"LocationStreetExtension";"LocationZIP";"LocationCity";"LocationPhone";"LocationFaxNumber";"UserGender";"UserTitle";"UserFirstName";"UserLastName";"UserUrl";"ImageUrl"' . "\n";
    foreach ($records as $record) {
        $userId = $record['user_id'];
        $locationId = $record['location_id'];

        $user = new User($userId);
        $location = new Location($locationId);
        $providerId = $location->getId() . '-' . $user->getId();

        if ($user->isValid() && $location->isValid()) {
            $data .= sprintf(
                    '"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s"',
                    sanitize_csv_field($providerId),
                    sanitize_csv_field($location->getName()),
                    sanitize_csv_field($location->getStreet()),
                    sanitize_csv_field($location->getStreetWONumber()),
                    sanitize_csv_field($location->getStreetNumber()),
                    sanitize_csv_field($location->getStreetExtension()),
                    sanitize_csv_field($location->getZip()),
                    sanitize_csv_field($location->getCity()),
                    sanitize_csv_field($location->getPhone()),
                    sanitize_csv_field($location->getFax()),
                    sanitize_csv_field($user->getGenderText()),
                    sanitize_csv_field($user->getTitle()),
                    sanitize_csv_field($user->getFirstName()),
                    sanitize_csv_field($user->getLastName()),
                    sanitize_csv_field($user->getUrl(true).'/gkv'),
                    sanitize_csv_field('https://www.arzttermine.de'.$user->getProfileAssetUrl(ASSET_GFX_SIZE_MEDIUM_PORTRAIT_CROPPED))
                ) . "\n";
        }
    }
    echo $data;
}

shell_end();