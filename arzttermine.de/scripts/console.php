<?php

error_reporting(E_ERROR | E_PARSE);
date_default_timezone_set('Europe/Berlin');

// CONFIG
// **********************************************************

$ENVIRONMENTS = array(
    'development' => __DIR__ . '/../',
    'qatest'      => '/www/qatest.arzttermine.de/current',
    'staging'     => '/www/test.arzttermine.de/current',
    'production'  => '/www/www.arzttermine.de/current'
);

// Application options
// s:silent (no output) / a:action / e:environment / d:debug mode
$options = getopt('sa:e:d');

// Get the environment, and warn if not available
if ((!$environment = strtolower(trim($options['e']))) || !in_array($environment, array_keys($ENVIRONMENTS))) {
    $environment = 'development';
}

// Set the global
$_ENV['SYSTEM_ENVIRONMENT'] = $environment;

// Set root path according to environment
$ROOT = $ENVIRONMENTS[$environment];

if (!isset($_SERVER['DOCUMENT_ROOT']) || $_SERVER['DOCUMENT_ROOT'] === '') {
    $_SERVER['DOCUMENT_ROOT'] = "{$ROOT}/htdocs";
}

// $CONFIG eventually comes from here
require_once "{$ROOT}/include/bootstrap.php";