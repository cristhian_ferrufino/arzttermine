#!/bin/sh

php /www/www.arzttermine.de/current/scripts/integrations/2/update_appointments.php -e production
exit

SERVICE='update_integration_caches.sh'
NUM_SERVICES=`ps ax | grep -v "grep" | grep -c $SERVICE`
if [ $NUM_SERVICES -gt 0 ]
then
    echo "$SERVICE is still running!" | mail -s "$SERVICE still running" webmaster
else
    /usr/local/bin/php /www/www.arzttermine.de/current/scripts/update_integration_2_cache.php -e production
fi
