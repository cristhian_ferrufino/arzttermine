<?php
/**
 * 1. Pulls appointments from Terminland and puts them into the table integration_2_appointments.
 * 2. Deletes all integration 2 appointments from appointments
 * 3. Pushes the appointments from integration_2_appointments to appointments
 */

use Arzttermine\Core\Console;
use Arzttermine\Calendar\Calendar;
use Arzttermine\Integration\Integration\Terminland\Integration as TerminlandIntegration;
use Arzttermine\Integration\Integration\Terminland\Data as TerminlandData;
use Arzttermine\Location\Location;
use Arzttermine\User\User;

require_once __DIR__ . '/../../console.php';

$console = Console::getInstance();

$console
    ->start()
    ->writeHeader('Updating integration 2 aka Terminland appointments', 2);

$terminland = new TerminlandIntegration();
$integrationId = $terminland->getId();
$terminlandData = new TerminlandData();
$terminlandDatas = $terminlandData->findObjects();

// first comming month
$startDateTime = Calendar::getNextAvailableSearchDate();
// 120 days cache (119+today), ~4 months
$endDateTime = clone $startDateTime;
$endDateTime
    ->addDays(119)
    ->setTime(23, 59, 59);

$console->writeLine('StartDateTime: ' . $startDateTime->__toString() . ' / EndDateTime: ' . $endDateTime->__toString(), 1);

$providers = array();
if (!empty($terminlandDatas)) {
    /**
     * @var TerminlandData $terminlandData
     */
    foreach ($terminlandDatas as $terminlandData) {
        $location = $terminlandData->getLocation();
        $user = $terminlandData->getUser();
        if (!$location->isValid()) {
            $console->writeWarning('Location is not valid! Skipping LocationId: ' . $terminlandData->getLocationId());
            continue;
        }
        if ($location->getStatus() != LOCATION_STATUS_VISIBLE_APPOINTMENTS) {
            $console->writeWarning('Location has wrong status! Skipping LocationId: ' . $location->getId());
            continue;
        }
        if ($location->getIntegrationId() != $integrationId) {
            $console->writeWarning('Location has wrong integration set! Skipping LocationId: ' . $location->getId() . ' with integrationId=' . $location->getIntegrationId());
            continue;
        }
        if (!$user->isValid()) {
            $console->writeWarning('User is not valid! Skipping UserId: ' . $terminlandData->getUserId());
            continue;
        }
        if ($user->getStatus() != USER_STATUS_VISIBLE_APPOINTMENTS) {
            $console->writeWarning('User has wrong status! Skipping UserId: ' . $user->getId());
            continue;
        }

        $providers[] = array(
            'location'  => $location,
            'user'      => $user
        );
    }
}

$console->writeNotice('Total Terminland Doctors Count: ' . count($providers));

$doctorIndex = 1;
/*
 * fetch all appointments for each doctor and location from terminland
 * and store this in the integration_cache_aa
*/
foreach ($providers as $provider) {
    /**
     * @var Location $location
     */
    $location = $provider['location'];
    /**
     * @var User $user
     */
    $user = $provider['user'];
    $console->writeLine($doctorIndex++ . ': LocationId: '.$location->getId().' / UserId: ' . $user->getId());
    $terminland->updateAvailableAppointments($user, $location, $startDateTime, $endDateTime);
    $console->writeLine('   Updated '.$terminland->getCountLastUpdatedAppointments().' appointment(s).');
}

$console->stop();