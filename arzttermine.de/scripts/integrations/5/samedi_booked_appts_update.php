<?php

require_once __DIR__ . '/../../shell.php';

use Arzttermine\Booking\Booking;
use Arzttermine\Integration\Integration\Samedi\Types\SBooking;
use Arzttermine\Integration\Integration\Samedi\Types\SBookingShort;
use NGS\Managers\BookingsManager;
use NGS\Managers\DoctorsManager;
use NGS\Managers\SamediBookingsManager;
use NGS\Managers\SamediDatasManager;

// ---------------------------------------------------------------------------------------------------------
/**
 * @param array $allPendingBookedAppointments
 * @param string $attendantId
 *
 * @return SBookingShort|null
 */
function findBookedApptByAttendantId($allPendingBookedAppointments, $attendantId)
{
    foreach ($allPendingBookedAppointments as $bookObj) {
        $sBookObj = new SBookingShort($bookObj);
        if (is_object($sBookObj) && $sBookObj->getAttendantId() === $attendantId) {
            return $sBookObj;
        }
    }

    return null;
}

// ---------------------------------------------------------------------------------------------------------
/**
 * @param int $categoryId
 * @param int $practiceId
 *
 * @return array
 */
function getUserIdBySamediCategoryAndPracticeId($categoryId, $practiceId)
{
    $samediDatasManager = SamediDatasManager::getInstance();
    $dto = $samediDatasManager->getByCategoryIdAndPracticeId($categoryId, $practiceId);

    if (isset($dto)) {
        return array($dto->getUserId(), $dto->getLocationId());
    }

    return array();
}

// ---------------------------------------------------------------------------------------------------------

$bookingsManager = BookingsManager::getInstance();
$doctorsManager = DoctorsManager::getInstance();
$samediBookingsManager = SamediBookingsManager::getInstance();

$allPendingBookedAppointments = $doctorsManager->getAllPendingBookedAppointments(0, 150);
$booking_statuses = Booking::getReceivedStatuses(array(Booking::STATUS_RECEIVED_UNCONFIRMED));
$samediBookingsAfterNow = $bookingsManager->getIntegrationBookingsAfterGivenDatetime(5, date('Y-m-d H:i:s'), $booking_statuses);

show_notice("Remote Samedi bookings: " . count($allPendingBookedAppointments), 2);

/** @var NGS\DAL\DTO\BookingsDto[] $samediBookingsAfterNow */
foreach ($samediBookingsAfterNow as $bookingDto) {
    
    /** @var NGS\DAL\DTO\SamediBookingsDto $samediBookingDto */
    $samediBookingDto = $bookingDto->getIntegrationBookingDto();

    $resultJson = $samediBookingDto->getResultJson();
    $bookData = json_decode($resultJson);
    
    if (is_object($bookData)) {
        $sBookingObject = new SBooking($bookData);
        
        // To identify the samedi booking get the attendant id
        $attendantId = @$sBookingObject->getAttendant()->id;

        // Find the booking amongst the Samedi bookings
        $sShortBookingObject = findBookedApptByAttendantId($allPendingBookedAppointments, $attendantId);
        
        if (!is_null($sShortBookingObject)) {

            // Our booking data
            $previousStartTime   = $sBookingObject->getStartsAt();
            $previousCategoryId  = $sBookingObject->getEventCategoryId();
            $previousEventTypeId = $sBookingObject->getEventTypeId();
            $previousPracticeId  = $sBookingObject->getPracticeId();
            
            // Remote booking data
            $newStartTime   = $sShortBookingObject->getStartsAt();
            $newCategoryId  = $sShortBookingObject->getEventCategoryId();
            $newEventTypeId = $sShortBookingObject->getEventTypeId();
            $newPracticeId  = $sShortBookingObject->getPracticeId();
            
            // If any of fields above are changed then update the local fields
            if ($previousStartTime   != $newStartTime   ||
                $previousCategoryId  != $newCategoryId  ||
                $previousEventTypeId != $newEventTypeId ||
                $previousPracticeId  != $newPracticeId
            ) {
                show_notice("Booking #" . $bookingDto->getId() . ". The following attributes are different:", 2, CONSOLE_HEAD_2);
                
                // Set up a visual table
                $table_format = "| %-14.14s | %-33.33s | %-33.33s";
                _e(sprintf($table_format, 'VARIABLE', 'OLD', 'NEW'));
                _e(str_repeat(CONSOLE_HEAD_2, 80));

                $sBookingObject->setCancelableUntil($sShortBookingObject->getCancelableUntil());
                $cancelableUntilDateTimeObj = date_create_from_format('Y-m-d?H:i:sP', $sShortBookingObject->getCancelableUntil());
                $cancelableUntilDateTimeString = null;
                
                if ($cancelableUntilDateTimeObj !== false) {
                    $cancelableUntilDateTimeString = date_format($cancelableUntilDateTimeObj, 'Y-m-d H:i:s');
                }
                
                $samediBookingsManager->setBookingCancelableUntil($samediBookingDto->getId(), $cancelableUntilDateTimeString);

                // START TIME
                if ($previousStartTime != $newStartTime) {
                    _e(sprintf($table_format, 'startTime', $previousStartTime, $newStartTime));

                    $sBookingObject->setStartsAt($newStartTime);
                    $bookingdateTimeObj = date_create_from_format('Y-m-d?H:i:sP', $newStartTime);
                    $bookingdateTimeString = null;
                    
                    if ($bookingdateTimeObj !== false) {
                        $bookingdateTimeString = date_format($bookingdateTimeObj, 'Y-m-d H:i:s');
                    }
                    
                    $bookingsManager->setAppointmentStartAt($bookingDto->getId(), $bookingdateTimeString);
                }

                // CATEGORY ID
                if ($previousCategoryId != $newCategoryId) {
                    _e(sprintf($table_format, 'categoryId', $previousCategoryId, $newCategoryId));
                    
                    $sBookingObject->setEventCategoryId($newCategoryId);
                    $userLocIdPair = getUserIdBySamediCategoryAndPracticeId($newCategoryId, $newPracticeId);
                    $samediBookingsManager->setCategoryId($samediBookingDto->getId(), $newCategoryId);

                    if (isset($userLocIdPair[0]) && isset($userLocIdPair[1])) {
                        $bookingsManager->setUserId($bookingDto->getId(), $userLocIdPair[0]);
                        $bookingsManager->setLocationId($bookingDto->getId(), $userLocIdPair[1]);
                    } else {
                        show_warning("WARNING: Category ID does not exist in the system. Samedi category id: {$newCategoryId}");
                    }
                }

                // PRACTICE ID
                if ($previousPracticeId != $newPracticeId) {
                    _e(sprintf($table_format, 'practiceId', $previousPracticeId, $newPracticeId));
                    
                    $sBookingObject->setPracticeId($newPracticeId);
                    $userLocIdPair = getUserIdBySamediCategoryAndPracticeId($newCategoryId, $newPracticeId);
                    $samediBookingsManager->setPracticeId($samediBookingDto->getId(), $newPracticeId);
                    
                    if (isset($userLocIdPair[0]) && isset($userLocIdPair[1])) {
                        $bookingsManager->setUserId($bookingDto->getId(), $userLocIdPair[0]);
                        $bookingsManager->setLocationId($bookingDto->getId(), $userLocIdPair[1]);
                    } else {
                        show_warning("WARNING: Category ID does not exist in the system. Samedi category id: {$newCategoryId}");
                    }
                }

                // EVENT TYPE ID
                if ($newEventTypeId != $previousEventTypeId) {
                    _e(sprintf($table_format, 'eventTypeId', $previousEventTypeId, $newEventTypeId));

                    $sBookingObject->setEventTypeId($newEventTypeId);
                    $samediBookingsManager->setEventTypeId($samediBookingDto->getId(), $newEventTypeId);
                }
                
                // Update the JSON object associated with the bookin
                $samediBookingsManager->setBookingResultJson($samediBookingDto->getId(), $sBookingObject->getData());

                _e('');
            }
        }
    }
}

shell_end();