<?php
/**
 * Fetchs all appointments from samedi and stores them
 * in the tables integration_5_appointments and appointments.
 *
 * Should be called periodically via a cron job.
 *
 */

require_once __DIR__ . '/../../console.php';

use Arzttermine\Core\Console;
use Arzttermine\Calendar\Calendar;
use Arzttermine\Integration\Integration\Samedi\IntegrationService;
use Arzttermine\Integration\Integration\Samedi\Data;
use Arzttermine\Integration\Integration\Samedi\EventType;

$console = Console::getInstance();

$console
	->start()
	->writeHeader('Updating integration 5 aka Samedi appointments', 2);

$samediData = new Data();
$samediDatas = $samediData->findObjects();

// first comming month
$startDateTime = Calendar::getNextAvailableSearchDate();
// 120 days cache (119+today), ~4 months
$endDateTime = clone $startDateTime;
$endDateTime
	->addDays(119)
	->setTime(23, 59, 59);

$console->writeLine('StartDateTime: ' . $startDateTime->__toString() . ' / EndDateTime: ' . $endDateTime->__toString(), 1);

$integrationService = IntegrationService::getInstance();
$integrationId = $integrationService->getIntegration()->getId();

$doctorIndex = 1;
if (!empty($samediDatas)) {
	/**
	 * @var Data $samediData
	 */
	foreach ($samediDatas as $samediData) {
		$location = $samediData->getLocation();
		$user = $samediData->getUser();
		if (!$location->isValid()) {
			$console->writeWarning('Location is not valid! Skipping LocationId: ' . $samediData->getLocationId());
			continue;
		}
		if ($location->getStatus() != LOCATION_STATUS_VISIBLE_APPOINTMENTS) {
			$console->writeWarning('Location has wrong status! Skipping LocationId: ' . $location->getId());
			continue;
		}
		if ($location->getIntegrationId() != $integrationId) {
			$console->writeWarning('Location has wrong integration set! Skipping LocationId: ' . $location->getId() . ' with integrationId=' . $location->getIntegrationId());
			continue;
		}
		if (!$user->isValid()) {
			$console->writeWarning('User is not valid! Skipping UserId: ' . $samediData->getUserId());
			continue;
		}
		if ($user->getStatus() != USER_STATUS_VISIBLE_APPOINTMENTS) {
			$console->writeWarning('User has wrong status! Skipping UserId: ' . $user->getId());
			continue;
		}

		$console->writeLine($doctorIndex++ . ': UpdateEventTypes: LocationId: '.$location->getId().' / UserId: ' . $user->getId());
		$integrationService->updateEventTypes($samediData);

		// check if the eventType exists. If that happens the API call will result in empty appointments
		$eventType = EventType::getEventTypeFor(
			$samediData->getPracticeId(),
			$samediData->getCategoryId()
		);
		$eventTypes = $eventType->getEventTypes();
		if (empty($eventTypes)) {
			$console->writeWarning('User has empty samedi eventTypes! Skipping UserId: ' . $user->getId() . ', SamediEventTypeId: '.$eventType->getId());
			continue;
		}

		$console->writeLine($doctorIndex++ . ': UpdateAppointments: LocationId: '.$location->getId().' / UserId: ' . $user->getId());
		$integrationService->updateAvailableAppointments($samediData, $startDateTime, $endDateTime);
		$console->writeLine('   Updated '.$integrationService->getCountLastUpdatedAppointments().' appointment(s).');
	}
}

$console->writeNotice('Finished');

$console->stop();
