<?php
/**
 * Tests. No bookings, nothing harmfull. Just run and test.
 * 1. Samedi Login()
 * 2. Samedi ifConnected()
 * 3. Samedi getReferrers() => /practices/<practice-id>/referrers.json
 */

require_once __DIR__ . '/../../shell.php';

use Arzttermine\Core\Console;
use Arzttermine\Integration\Integration\Samedi\Connection\SamediAdapter;

$console = Console::getInstance();
$console
    ->start()
    ->writeHeader('Testing Samedi Login', 2);

$samediAdapter = SamediAdapter::getInstance();

$console->writeLine('Testing Login:');
if ($samediAdapter->login() === false) {
    $console->writeWarning('Login failed with Message: ' . $samediAdapter->getLastError());
} else {
    $console->writeNotice('Login success');
}

$console->writeLine('Testing Connection:');
if ($samediAdapter->isConnected() === false) {
    $console->writeWarning('Not connected');
} else {
    $console->writeNotice('Connected');
}

$console->writeLine('Testing Referrers:');
$referrers = $samediAdapter->getReferrers();
if ($referrers === false) {
    $console->writeWarning('Error');
    $console->writeLine($samediAdapter->getLastError());
} else {
    print_r($referrers);
}

$console->stop();