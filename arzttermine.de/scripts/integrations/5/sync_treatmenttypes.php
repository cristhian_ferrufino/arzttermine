<?php

require_once __DIR__ . '/../../shell.php';

use Arzttermine\Integration\Integration\Samedi\Types\CategoryEventType;

show_notice('Updating Samedi Treatment Types');

$samediEventTypesCachesManager = \NGS\Managers\SamediEventTypesCachesManager::getInstance();
$samediTreatmentTypesMapper = \NGS\DAL\Mappers\SamediTreatmentTypesMapper::getInstance();
$allEventTypes = $samediEventTypesCachesManager->selectAll();

foreach ($allEventTypes as $etDto) {
    _e("Updating TreatmentType for Category ID: {$etDto->getCategoryId()}");
    $etsJson = $etDto->getEventTypes();
    $ets = json_decode($etsJson);
    foreach ($ets as $eventType) {
        $eventType = new CategoryEventType($eventType);
        $ttDto = $samediTreatmentTypesMapper->selectByPK($eventType->getId());
        $exists = false;
        if (isset($ttDto)) {
            $exists = true;
        } else {
            $ttDto = $samediTreatmentTypesMapper->createDto();
        }
        $ttDto->setId($eventType->getId());
        $ttDto->setName($eventType->getName());
        $ttDto->setDuration($eventType->getDurationInMinutes());
        $ttDto->setInsuranceIds($eventType->getArzttermineFormattedInsuranceIds());
        $ttDto->setCreatedAt(date("Y-m-d H:i:s"));
        if ($exists) {
            $samediTreatmentTypesMapper->updateByPK($ttDto);
        } else {
            $samediTreatmentTypesMapper->insertDto($ttDto);
        }
    }
}

shell_end();