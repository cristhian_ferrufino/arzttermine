<?php
/**
 * Synchronize samedi data with Arzttermine tables
 * integration_5_network
 * integration_5_practices
 * integration_5_practice_categories
 * integration_5_event_types
 *
 */
require_once __DIR__ . '/../../shell.php';

use Arzttermine\Integration\Integration\Samedi\IntegrationService;
use Arzttermine\Integration\Integration\Samedi\Types\SPractice;
use Arzttermine\Integration\Integration\Samedi\Types\SPracticeCategory;

use NGS\Managers\DoctorsManager;
use NGS\Managers\SamediPracticeCategoriesManager;
use NGS\Managers\SamediPracticesManager;

$doctorsManager = DoctorsManager::getInstance();
$samediPracticesManager = SamediPracticesManager::getInstance();
$samediPracticeCategoriesManager = SamediPracticeCategoriesManager::getInstance();

// NETWORK CONTACTS
// ---------------------------------------------------------------------------------------------------------

_e('Network Contacts Cache Started');

IntegrationService::syncNetwork();

_e('Network Contacts Cache End', 2);
exit;
// PRACTICES
// ---------------------------------------------------------------------------------------------------------
_e('Practices Cache Started');

$allSamediPractices = $doctorsManager->getAllSamediPractices();
$samediPracticesManager->truncateTable();

foreach ((array)$allSamediPractices as $practice) {
    $samediPracticesManager->addRow(json_encode($practice));
}

_e('Practices Cache End', 2);

// PRACTICE CATEGORIES
// ---------------------------------------------------------------------------------------------------------
_e('Practices Categories Cache Started');

$samediPracticeCategoriesManager->truncateTable();
foreach ((array)$allSamediPractices as $practice) {
    $samedi_practice = new SPractice($practice);
    $samediPracticeCategories = $doctorsManager->getSamediPracticeCategories($samedi_practice);
    foreach ($samediPracticeCategories as $practice_category) {
        $samediPracticeCategoriesManager->addRow($samedi_practice->getId(), json_encode($practice_category));
    }
}

_e('Practices Categories Cache End', 2);

// EVENT TYPES
// ---------------------------------------------------------------------------------------------------------
_e('Event Types Cache Started');

// better: IntegrationService::syncEventTypes();

$samediPracticeCategories = $samediPracticeCategoriesManager->selectAll();

foreach ((array)$samediPracticeCategories as $category) {
    $samedi_practice_category = new SPracticeCategory(json_decode($category->getData()));
//    $samediPracticeCategories = $doctorsManager->getPracticeCategoryEventTypes($category->getSamediPracticeId(), $samedi_practice_category->getId(), true);
    // should be now UPDATEEventTypes()
    $samediPracticeCategories = $integrationService->updateEventTypes($category->getSamediPracticeId(), $samedi_practice_category->getId());
}

_e('Event Types Cache End', 2);

shell_end();