<?php

require_once __DIR__ . '/../../console.php';

use Arzttermine\Calendar\Calendar;
use Arzttermine\Core\Console;
use Arzttermine\Appointment\AppointmentGeneratorService;
use Arzttermine\Appointment\Planner;
use Arzttermine\User\User;

$console = Console::getInstance();

$console
    ->start()
    ->writeHeader('Appointments generator', 2);

// Objects for querying
$_planner = new Planner();
$generator = AppointmentGeneratorService::getInstance();

$console->writeSubheader('Finding all planners');
$planner_ids = array_map(function($e) { return $e['id']; }, $_planner->finds()); // Get just the indices

/** @var int[] $plannerIds */
foreach($planner_ids as $planner_id) {
    $planner = new Planner($planner_id);
    
    // If the doctor doesn't have appointments enabled, don't generate them
    // Note: This is a poor attempt at an optimisation
    // @todo Better handling of relationships
    if ($planner->getUser()->getStatus() !== User::STATUS_VISIBLE_APPOINTMENTS) {
        continue;
    }

    $console->writeSubheader("Generating and saving appointments for Planner [{$planner->getId()}]");
    $appointments = $generator
        ->clear($planner)     // Clear previous appointments out...
        ->generate($planner); // ... and generate new ones

    // Memory management
    unset($planner);
    
    $console->writeNotice('Appointments saved');   
}

$console->writeNotice('Planner appointments generated');

$console->stop();