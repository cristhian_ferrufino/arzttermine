#!/usr/bin/env php
<?php
/**
 * Migrates the appointments from the uploaded charly ics files
 */

require_once __DIR__.'/../../shell.php';

require_once $GLOBALS['CONFIG']['INTEGRATIONS'][10]['CLASS_FILENAME'];

Integration10::updateAvailableAppointments(true);

shell_end();
