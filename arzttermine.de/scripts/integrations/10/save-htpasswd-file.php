#!/usr/bin/env php
<?php
/**
 * Test script to save the htpasswd file.
 * Usually this file is written when the Integration_10_WebdavAccount DB is changed.
 */

require_once __DIR__.'/../../shell.php';

require_once $GLOBALS['CONFIG']['INTEGRATIONS'][10]['CLASS_FILENAME'];

if (Integration_10_Webdavaccount::saveHtpasswdFile()) {
	echo "OK\n";
} else {
	echo "ERROR\n";
}

shell_end();
