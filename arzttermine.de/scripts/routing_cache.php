<?php

require_once __DIR__ . '/console.php';

use Arzttermine\Cms\CmsContent;
use Arzttermine\Core\Console;

$console = Console::getInstance();

$console
    ->start()
    ->writeHeader('Generating route cache', 2);

$cache_file = SHARED_PATH . '/cache/cms_routing.php';
$cache_file_contents = <<<'FILE'
<?php
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

$collection = new RouteCollection();

FILE;

$cms_content = new CmsContent();
$contents = $cms_content->findObjects();

$console->writeNotice('Found ' . count($contents) . ' CMS content pages', 2);

// Build the cache contents
/** @var CmsContent $content */
foreach ($contents as $content) {
    $console->writeLine('Generating route for: ' . $content->getUrl());

    $cache_file_contents .= '$collection->add("' . $content->getUrl() . '", ';
    $cache_file_contents .= <<<ROUTE

    new Route(
        '{$content->getUrl()}',
        array(
            '_controller' => '\Arzttermine\Controller\CMSController::indexAction'
        )
    ));

ROUTE;

    $cache_file_contents .= '$collection->add("' . $content->getUrl() . '/", ';
    $cache_file_contents .= <<<ROUTE

    new Route(
        '{$content->getUrl()}/',
        array(
            '_controller' => '\Arzttermine\Controller\CMSController::indexAction'
        )
    ));

ROUTE;
}

$cache_file_contents .= 'return $collection;';

$console->writeLine('');

// Output to cache directory
$console->writeNotice('Exporting cache to file', 2);
file_put_contents($cache_file, $cache_file_contents);

$console->stop();