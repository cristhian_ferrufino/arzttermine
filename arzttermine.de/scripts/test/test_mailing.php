#!/usr/bin/env php
<?php
/*
 * Tests mailing
**/

require_once __DIR__.'/../shell.php';

require_once $GLOBALS['CONFIG']['SYSTEM_CLASS_MAILING'];

$app->setLanguageCode('de');
$GLOBALS['CONFIG']['SYSTEM_MAIL_SMTP_DEBUG']=false;
$_mailing = new Mailing();
$_mailing->type = MAILING_TYPE_TEXT;
$_mailing->content_filename = '/mailing/booking.txt';
$_mailing->addAddressTo('axel.kuzmik@arzttermine.de');
if ($_mailing->send(false)) {
    echo "OK\n";
} else {
    echo "ERROR\n";
}
?>