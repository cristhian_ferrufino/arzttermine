#!/usr/bin/env php
<?php
/*
 * Tests mailing
**/

use Arzttermine\Booking\Booking;
use Arzttermine\Widget\WidgetFactory;

require_once __DIR__.'/../shell.php';

$app->setLanguageCode('de');
$GLOBALS['CONFIG']['SYSTEM_MAIL_SMTP_DEBUG']=false;

try {
    $app->setWidgetContainer(WidgetFactory::getWidgetContainer('dak'))->setIsWidget(true);
} catch (\Exception $e) {
    // Ignore
}

$booking = new Booking('049f21dc6aad', 'slug');

if ($booking->isValid()) {
    if (Booking::sendBookingEmail($booking)) {
        echo "OK\n";
    } else {
        echo "ERROR\n";
    }
}
