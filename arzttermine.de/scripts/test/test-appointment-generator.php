<?php

require_once __DIR__ . '/../console.php';

use Arzttermine\Calendar\Calendar;
use Arzttermine\Core\Console;
use Arzttermine\Appointment\Appointment;
use Arzttermine\Appointment\AppointmentGeneratorService;
use Arzttermine\Appointment\Planner;

$console = Console::getInstance();

$console
    ->start()
    ->writeHeader('Appointments generator', 2);

$planner = new Planner(1);

$generator = AppointmentGeneratorService::getInstance();

$console->writeSubheader('Generating appointments for Planner');
$appointments = $generator->generate($planner);

$console->writeNotice('Got ' . count($appointments) . ' appointments');

$console->writeNotice('Appointments saved');

$console->stop();