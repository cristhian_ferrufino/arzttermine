<?php

require_once __DIR__ . '/../console.php';

use Arzttermine\Core\Console;

$console = Console::getInstance();

$console
    ->start()
    ->writeHeader('Reporting', 2)
    ->writeSubheader('Doing the report', 2)
    ->writeWarning('Something went wrong', 2)
    ->writeNotice('Recovering from error')
    ->writeLine('Done', 2)
    ->stop();