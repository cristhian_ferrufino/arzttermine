<?php

require_once __DIR__ . '/../console.php';

use Arzttermine\Core\Console;
use Arzttermine\Booking\Booking;
use Arzttermine\Mail\SMS;

$console = Console::getInstance();

// Test sending SMS
// Test logging SMS

$console
    ->start()
    ->writeHeader('Testing SMS', 2);

$result = true;
$booking = new Booking(1);
if ($booking->isValid()) {
    // this also writes to SMSLog
    $result = SMS::sendBookingConfirmationSms($booking);
}

if ($result) {
    $console->writeNotice('OK: Send SMS to ' . $booking->getPhone());
} else {
    $console->writeNotice('ERROR: Could not send SMS to ' . $booking->getPhone());
}

$console->stop();