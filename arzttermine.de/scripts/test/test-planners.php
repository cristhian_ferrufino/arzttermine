<?php

require_once __DIR__ . '/../console.php';

use Arzttermine\Core\Console;
use Arzttermine\Core\DateTime;
use Arzttermine\Appointment\Planner;
use Arzttermine\Appointment\PlannerBlock;
use Arzttermine\Insurance\Insurance;
use Arzttermine\User\User;
use Arzttermine\Location\Location;

$console = Console::getInstance();

// Test the Planner object
// Test Planner Event objects
// Test appointment generation
// Test Appointment object
// Test DB access of appointments

$console
    ->start()
    ->writeHeader('Generating planner events', 2);

$planner = new Planner();
$planner
    ->setUser(new User(8)) // Kurosch Schafei...
    ->setLocation(new Location(2)) // ... and his practice
    ->saveNewObject();

$date = new DateTime();

for ($i = 0; $i < 5; $i++) {
    $block = new PlannerBlock();
    
    // Start time
    $start = clone $date;
    $start->setTime(9, 0);
    $block->setStartTime($start);
    
    // End time
    $end = clone $date;
    $end->setTime(11, 0);
    $block->setEndTime($end);

    $block
        // Block in minutes
        ->setLength(30)
        // Insurances (both, in this case)
        ->setInsuranceIds(array(Insurance::GKV, Insurance::PKV));
    
    $console->writeLine('Creating block on ' . $GLOBALS['CONFIG']['CALENDAR_WEEKDAY_TEXTS'][$block->getStartTime()->format('N')] . ' (' . $block->getStartTime()->format('H:i:s') . ' to ' . $block->getEndTime()->format('H:i:s') . ') with length of ' . $block->getLength() . ' mins');
    
    $planner->addBlock($block);
    
    $date->addDays(1);
}

$console->writeSubheader('Saving blocks');

$planner->saveBlocks();

$console->writeNotice('Saved ' . count($planner->getBlocks()) . ' blocks');
    
$console->stop();