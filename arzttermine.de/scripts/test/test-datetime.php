<?php

require_once __DIR__ . '/../console.php';

use Arzttermine\Core\Console;
use Arzttermine\Core\DateTime;

$console = Console::getInstance();

$console
    ->start()
    ->writeHeader('Testing datetime', 2);

$date1 = new DateTime();
$date2 = new DateTime();
$date2->modify('+2 days');

// Test comparisons
error_log(print_r($date2 > $date1, true)); // TRUE
error_log(print_r($date2 < $date1, true)); // FALSE
error_log(print_r($date2 == $date1, true)); // FALSE

// Test output
$date_zero = new DateTime();
error_log(print_r($date_zero, true));

$console->writeSubheader('Testing modify()');
$i = 10000;
$start = microtime(true);
while($i) {
    $date1->modify('+1 day');
    $i--;
}
$console->writeLine('Speed: ' . $console->profile($start));

$console->writeSubheader('Testing add()');
$i = 10000;
$start = microtime(true);
while($i) {
    $date1->add(new DateInterval('P1D'));
    $i--;
}
$console->writeLine('Speed: ' . $console->profile($start));

$console->stop();