<?php
/**
 * Get all doctors with status=3 AND fixed_week=1
 * and calculate the appointments for the next intval($GLOBALS['CONFIG']['CALENDAR_MAX_DAYS_AHEAD'] / 7) weeks
 * and store them in table users_aa.
 * !!!If it doesn't find appointments in the last week from now() then it will delete all appointments for this user!!!
 * So make sure that this script runs once a month at least.
 */

require_once __DIR__ . '/shell.php';

$usersManager = \NGS\Managers\UsersManager::getInstance();
$usersAaManager = \NGS\Managers\UsersAaManager::getInstance();
$locationsManager = \NGS\Managers\LocationsManager::getInstance();
$offset = 0;
$limit = 10;
$lastMondayDate = date('Y-m-d', strtotime('monday last week'));
$lastFridayDate = date('Y-m-d', strtotime($lastMondayDate . ' +4 days'));

$userCounter = 0;

while ($fixedWeekUsers = $usersManager->getFixedWeekUsers($offset, $limit)) {
    /** @var NGS\DAL\DTO\UsersDto[] $fixedWeekUsers */
    foreach ($fixedWeekUsers as $userDto) {
        $userCounter++;
        echo $userCounter . " Updating user appointments: user_id = " . $userDto->getId() . "\n";
        $locationsDtos = $locationsManager->getLocationsByUserId($userDto->getId());
        if (!empty($locationsDtos) && count($locationsDtos) === 1) {
            $locationsDto = $locationsDtos[0];
            if ($locationsDto->getIntegrationId() == 2 || $locationsDto->getIntegrationId() == 5 || $locationsDto->getIntegrationId() == 8 || $locationsDto->getIntegrationId() == 9) {
                echo $userCounter . " This location skipped : location_id = " . $locationsDto->getId() . "\n";
                continue;
            }
        }
        $locationIdsArray = array();
        foreach ($locationsDtos as $locationDto) {
            $locationIdsArray[] = $locationDto->getId();
        }
        $dtosToBeInserted = $usersAaManager->getAvailableAppointmentsNotExtracted($lastMondayDate, $lastFridayDate, $userDto->getId(), $locationIdsArray);
        $usersAaManager->deleteUserAllAvaialableTimesByUserIdAndLocationId($userDto->getId());
        foreach ($dtosToBeInserted as $dto) {
            $usersAaManager->addDoctorFixedWeekAppointment($dto);
        }
    }
    $offset += $limit;
}

shell_end();