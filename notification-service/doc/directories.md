# Directories

Directories are basically a list of phone numbers or email addresses. 
They can be used to listen on new notifications with certain property values.
If all directory bindings are supplied the notification will be forwarded to all recipients in the list.

### Example

1. Create a [new directory](http://localhost:15400/swagger-ui.html#!/directory-controller/patchDirectoriesUsingPOST)
  - This endpoint can also be used to update directories
  - Add property `bindings` when creating the directory (eg: `practiceId: 42`)
  - If `EMAIL` directory is created, supplied as many email addresses if you want using `forwarding`
  - If `SMS` directory is created, supplied as many phone numbers if you want using `forwarding`
  - Additionally you can listen on certain "topics" (eg: `BOOKING_CREATED`)
2. Send notification (normally publishing a notification message to the queue)
  - Provide required parameters with corresponding value (eg: `practice` with `42` or `"42"`)
  - Use the same notification type as the directory was created with (eg: `EMAIL`)
3. New tasks will be created for each recipient in the directory, including the original notification recipient


See all [available topics](https://github.com/arzttermine/notification-service/blob/master/web/src/main/java/com/arzttermine/service/notifications/web/struct/Notification.java#L7)

Example request payload:
```json
{
	"directories": [
		{
			"uuid": "supply uuid of directory if update is desired",
			"type": "EMAIL",
			"forwarding": [
				"forward-to@example.com"
			],
			"topics": ["BOOKING_CREATED"],
			"bindings": {
				"practiceId": 42
			}
		}		
	]
}
```
