# about notifications

What can this service do for you? 

This service...
- takes care about the scheduling of notification tasks
- sends out notification at a certain time or instantly if desired
- provides templates and renders an email/sms template etc.
- allows you to create bindings on multiple properties to forward notification to a directory
- rejects messages after a day (only if expected time of send is set)
- does **not** provide an API 
- does **not** know about our business rules
- does **not** know about other services

Please define your templates here! All required parameters can be set to required using our database. 
In that case a message has to provide the required values . Beware that parameters might changes over the time.
Therefore please store the corresponding messages to create new notifications tasks in this service as well.
The purpose is that everyone is aware of the fact that changing a template or how a notification is processed might require changes in other services.
