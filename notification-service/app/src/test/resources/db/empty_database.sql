DELETE FROM directory_notification_bindings;
DELETE FROM directory_notification_topics;
DELETE FROM notifications_forwarding;
DELETE FROM notification_directory;

DELETE FROM notification_task_properties;
DELETE FROM notification_tasks;
DELETE FROM notification_template_params;
DELETE FROM notification_templates;
