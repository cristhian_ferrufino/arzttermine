package com.arzttermine.service.notifications.config.settings.model;

import com.arzttermine.library.suport.templating.exchange.UrlRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class RemoteUrlTest
{
    @Test
    public void buildWith_should_not_set_port_if_equal_to_80()
    {
        RemoteUrl url = new RemoteUrl();
        url.setHost("host");

        assertEquals("https://host", url.buildWith(new UrlRequest()));
    }

    @Test
    public void buildWith_should_add_all_paths_with_correct_order()
    {
        RemoteUrl url = new RemoteUrl();
        url.setHost("host");
        url.getPath().add("one");
        url.getPath().add("two");

        UrlRequest request = new UrlRequest();
        request.setPath(new String[]{"three", "four"});

        assertEquals("https://host/one/two/three/four", url.buildWith(request));
    }

    @Test
    public void buildWith_should_add_fragments()
    {
        RemoteUrl url = new RemoteUrl();
        url.setHost("host");
        url.getPath().add("one");

        UrlRequest request = new UrlRequest();
        request.setFragments(new String[]{"frag1", "frag2"});

        assertEquals("https://host/one#/frag1/frag2", url.buildWith(request));
    }

    @Test
    public void buildWith_should_automatically_encocde_query_and_path_segments()
    {
        RemoteUrl url = new RemoteUrl();
        url.setHost("host");
        url.getPath().add("one with & special characters");
        url.getQuery().put("test", Collections.singletonList("this is a test"));

        UrlRequest request = new UrlRequest();
        request.setPath(new String[0]);

        assertEquals("https://host/one%20with%20&%20special%20characters?test=this%20is%20a%20test", url.buildWith(request));
    }

    @Test
    public void buildWith_should_merge_query_params()
    {
        RemoteUrl url = new RemoteUrl();
        url.setHost("host");
        url.getPath().add("test");
        url.getQuery().put("test", Collections.singletonList("this test"));
        url.getQuery().put("test2", Collections.singletonList("another"));

        UrlRequest request = new UrlRequest();
        request.setPath(new String[0]);
        request.getQuery().add("1", "1");
        request.getQuery().add("1", "2");
        request.getQuery().add("2", "3");

        assertEquals("https://host/test?1=1&1=2&2=3&test=this%20test&test2=another", url.buildWith(request));
    }
}
