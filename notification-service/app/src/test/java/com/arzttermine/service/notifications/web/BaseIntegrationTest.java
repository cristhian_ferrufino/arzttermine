package com.arzttermine.service.notifications.web;

import com.arzttermine.service.notifications.Application;
import com.arzttermine.service.profile.security.support.AuthBuilderUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.PostConstruct;
import java.util.UUID;
import java.util.function.Supplier;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

@ActiveProfiles("testing")
@RunWith(SpringRunner.class)
@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
    classes = Application.class
)
public abstract class BaseIntegrationTest
{
    protected MockMvc mockMvc;

    @Autowired
    protected ObjectMapper objectMapper;

    @Autowired
    private WebApplicationContext applicationContext;

    @Autowired
    private FilterChainProxy filterChainProxy;

    @Autowired
    private RemoteTokenServices remoteTokenServices;

    private final AuthBuilderUtil authBuilder = new AuthBuilderUtil();

    @PostConstruct
    public void baseIntegrationSetup()
    {
        mockMvc = MockMvcBuilders.webAppContextSetup(applicationContext)
            .addFilter(filterChainProxy).build();
    }

    protected String sendDto(Object dto) throws JsonProcessingException
    {
        return objectMapper.writeValueAsString(dto);
    }

    private MockHttpServletRequestBuilder configureAuthenticatedRequest(long customerId, String[] roles, Supplier<MockHttpServletRequestBuilder> requestAction)
    {
        final OAuth2Authentication oauth = authBuilder.buildAuthForCustomer(customerId, roles);
        final UUID token = UUID.randomUUID();

        when(remoteTokenServices.loadAuthentication(eq(token.toString()))).thenReturn(oauth);

        final MockHttpServletRequestBuilder request = requestAction.get();
        request.header("Authorization", "Bearer " + token);
        SecurityContextHolder.getContext().setAuthentication(oauth);

        return request;
    }

    protected MockHttpServletRequestBuilder customerRequest(long customer, Supplier<MockHttpServletRequestBuilder> requestAction)
    {
        return configureAuthenticatedRequest(customer, new String[]{"ROLE_CUSTOMER", "ROLE_CLIENT"}, requestAction);
    }

    protected MockHttpServletRequestBuilder employeeRequest(long employee, Supplier<MockHttpServletRequestBuilder> requestAction)
    {
        return configureAuthenticatedRequest(employee, new String[]{"ROLE_EMPLOYEE", "ROLE_CUSTOMER", "ROLE_CLIENT"}, requestAction);
    }

    protected MockHttpServletRequestBuilder systemRequest(Supplier<MockHttpServletRequestBuilder> requestAction)
    {
        return configureAuthenticatedRequest(42L, new String[]{"ROLE_SYSTEM", "ROLE_CLIENT"}, requestAction);
    }

    protected MockHttpServletRequestBuilder clientRequest(Supplier<MockHttpServletRequestBuilder> requestAction)
    {
        return configureAuthenticatedRequest(23L, new String[]{"ROLE_CLIENT"}, requestAction);
    }
}
