package com.arzttermine.service.notifications.service.processor.templating;

import com.arzttermine.library.suport.templating.exchange.UrlRequest;
import com.arzttermine.service.notifications.config.settings.RemoteUrlSettings;
import com.arzttermine.service.notifications.config.settings.model.RemoteUrl;
import com.arzttermine.service.notifications.domain.entity.NotificationTask;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class ProcessedNotificationTaskTest
{
    @Mock
    private RemoteUrlSettings urlSettings;

    @Test
    public void getParams_should_return_processed_parameters()
    {
        NotificationTask task = new NotificationTask();
        task.getParams().put("exampleUrl", "url://FRONTEND:test");
        task.getParams().put("somethingElse", "value");

        RemoteUrl url = new RemoteUrl();
        url.setHost("host");

        when(urlSettings.getByType(UrlRequest.Type.FRONTEND))
            .thenReturn(url);

        ProcessedNotificationTask processedTask = new ProcessedNotificationTask(task, urlSettings);
        Map<String, String> processedParams = processedTask.getParams();

        assertEquals(2, processedParams.size());
        assertEquals("https://host/test", processedParams.get("exampleUrl"));
        assertEquals("value", processedParams.get("somethingElse"));
    }
}
