package com.arzttermine.service.notifications.web.controller;

import com.arzttermine.service.notifications.web.BaseIntegrationTest;
import com.arzttermine.service.notifications.web.DirectoryDto;
import com.arzttermine.service.notifications.web.request.DirectoryPatchDto;
import com.arzttermine.service.notifications.web.struct.Notification;
import com.arzttermine.service.notifications.web.struct.NotificationType;
import org.junit.Test;
import org.springframework.http.MediaType;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class DirectoryControllerTest extends BaseIntegrationTest
{
    @Test
    public void patchDirectories_should_create_new_directories_on_demand() throws Exception
    {
        Map<String, String> bindings = new HashMap<>(1);
        bindings.put("type", "my-binding");

        DirectoryDto dto = new DirectoryDto();
        dto.setForwarding(Collections.singletonList("to@peter.de"));
        dto.getBindings().putAll(bindings);
        dto.setType(NotificationType.EMAIL);
        dto.getTopics().add(Notification.BOOKING_CONFIRMATION);

        DirectoryPatchDto request = new DirectoryPatchDto();
        request.setDirectories(Collections.singletonList(dto));

        DirectoryDto result = objectMapper.readValue(mockMvc.perform(systemRequest(() -> post("/directories"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString(), DirectoryDto[].class)[0];

        mockMvc.perform(systemRequest(() -> get("/directories/" + result.getUuid())))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.uuid", is(result.getUuid().toString())))
            .andExpect(jsonPath("$.type", is(NotificationType.EMAIL.name())))
            .andExpect(jsonPath("$.forwarding[*]", hasSize(1)))
            .andExpect(jsonPath("$.forwarding[0]", is("to@peter.de")))
            .andExpect(jsonPath("$.bindings[*]", hasSize(1)))
            .andExpect(jsonPath("$.bindings.type", is("my-binding")))
            .andExpect(jsonPath("$.topics[*]", hasSize(1)))
            .andExpect(jsonPath("$.topics[0]", is("BOOKING_CONFIRMATION")));
    }

    @Test
    public void patchDirectories_should_update_existing_and_create_new_one() throws Exception
    {
        DirectoryDto dto = new DirectoryDto();
        dto.getForwarding().add("to@peter.de");
        dto.setType(NotificationType.EMAIL);
        dto.getBindings().put("x", "y");

        DirectoryPatchDto request = new DirectoryPatchDto();
        request.getDirectories().add(dto);

        DirectoryDto result = objectMapper.readValue(mockMvc.perform(systemRequest(() -> post("/directories"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString(), DirectoryDto[].class)[0];

        dto.setUuid(result.getUuid());
        dto.getForwarding().add("another@forwarding.de");

        DirectoryDto dto2 = new DirectoryDto();
        dto2.setForwarding(Collections.singletonList("to@example.de"));
        dto2.setType(NotificationType.EMAIL);
        dto2.getBindings().put("y", "x");
        request.getDirectories().add(dto2);

        DirectoryDto newOne = Arrays.stream(objectMapper.readValue(mockMvc.perform(systemRequest(() -> post("/directories"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString(), DirectoryDto[].class))
            .filter(d -> !d.getUuid().equals(dto.getUuid()))
            .findFirst()
            .orElse(null);

        mockMvc.perform(systemRequest(() -> get("/directories/" + dto.getUuid())))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.type", is(NotificationType.EMAIL.name())))
            .andExpect(jsonPath("$.forwarding[*]", hasSize(2)))
            .andExpect(jsonPath("$.forwarding[0]", is("to@peter.de")))
            .andExpect(jsonPath("$.forwarding[1]", is("another@forwarding.de")))
            .andExpect(jsonPath("$.bindings.x", is("y")));

        mockMvc.perform(systemRequest(() -> get("/directories/" + newOne.getUuid())))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.forwarding[*]", hasSize(1)))
            .andExpect(jsonPath("$.forwarding[0]", is("to@example.de")))
            .andExpect(jsonPath("$.bindings.y", is("x")));
    }

    @Test
    public void deleteDirectory_should_delete_directory() throws Exception
    {
        DirectoryDto dto = new DirectoryDto();
        dto.setForwarding(Collections.singletonList("to@peter.de"));
        dto.setType(NotificationType.EMAIL);
        dto.getBindings().put("x", "y");

        DirectoryPatchDto request = new DirectoryPatchDto();
        request.setDirectories(Collections.singletonList(dto));

        DirectoryDto result = objectMapper.readValue(mockMvc.perform(systemRequest(() -> post("/directories"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString(), DirectoryDto[].class)[0];

        mockMvc.perform(systemRequest(() -> get("/directories/" + result.getUuid())))
            .andExpect(status().isOk());

        mockMvc.perform(systemRequest(() -> delete("/directories/" + result.getUuid())))
            .andExpect(status().isNoContent());

        mockMvc.perform(systemRequest(() -> get("/directories/" + result.getUuid())))
            .andExpect(status().isNotFound());
    }
}
