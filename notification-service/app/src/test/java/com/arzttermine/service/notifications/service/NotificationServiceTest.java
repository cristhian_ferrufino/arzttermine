package com.arzttermine.service.notifications.service;

import com.arzttermine.service.notification.messages.notification.NotificationCancellationRequestDto;
import com.arzttermine.service.notifications.domain.entity.NotificationDirectory;
import com.arzttermine.service.notifications.domain.entity.NotificationTask;
import com.arzttermine.service.notifications.domain.entity.NotificationTemplate;
import com.arzttermine.service.notifications.domain.reposiory.NotificationDirectoryRepository;
import com.arzttermine.service.notifications.domain.reposiory.NotificationTaskRepository;
import com.arzttermine.service.notifications.domain.reposiory.NotificationTemplateRepository;
import com.arzttermine.service.notifications.service.mapper.NotificationMapper;
import com.arzttermine.service.notifications.web.struct.Notification;
import com.arzttermine.service.notifications.web.struct.NotificationRequestDto;
import com.arzttermine.service.notifications.web.struct.NotificationType;
import com.querydsl.core.types.Predicate;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.ApplicationEventPublisher;

import java.time.Clock;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class NotificationServiceTest
{
    @Mock
    private NotificationDirectoryRepository directoryRepository;

    @Mock
    private NotificationTemplateRepository templateRepository;

    @Mock
    private NotificationTaskRepository taskRepository;

    @Mock
    private ApplicationEventPublisher eventPublisher;

    @Spy
    private NotificationMapper mapper = new NotificationMapper(Clock.systemUTC());

    @InjectMocks
    private NotificationService service;

    @Test
    public void createTask_should_search_for_existing_directory()
    {
        Map<String, String> map = new HashMap<>();
        map.put("x", "y");

        UUID directory = UUID.randomUUID();

        NotificationDirectory obj = new NotificationDirectory();
        obj.getTopics().add(Notification.PASSWORD_RESET);
        obj.getForwarding().add("example@com.de");
        obj.setType(NotificationType.EMAIL);
        obj.getBindings().put("x", "y");
        obj.setUuid(directory);

        NotificationRequestDto request = new NotificationRequestDto();
        request.setType(NotificationType.EMAIL);
        request.setTemplate(Notification.PASSWORD_RESET);
        request.getParams().put("x", "y");

        when(templateRepository.findOne(request.getTemplate().getTemplateId())).thenReturn(new NotificationTemplate());
        when(directoryRepository.findApplicable(map.keySet(), map.values().stream().collect(Collectors.toList()), NotificationType.EMAIL)).thenReturn(Collections.singletonList(obj));

        when(taskRepository.save(anyCollectionOf(NotificationTask.class))).thenAnswer(invocation -> {
            List<NotificationTask> tasks = invocation.getArgumentAt(0, ArrayList.class);
            Assert.assertEquals(1, tasks.size());
            Assert.assertEquals("example@com.de", tasks.get(0).getRecipient());
            return tasks;
        });

        service.createTask(request);

        verify(directoryRepository, times(1)).findApplicable(map.keySet(), map.values().stream().collect(Collectors.toList()), NotificationType.EMAIL);
        verify(taskRepository, times(1)).save(anyCollectionOf(NotificationTask.class));
    }

    @Test(expected = IllegalStateException.class)
    public void createTask_should_ensure_binding()
    {
        UUID directory = UUID.randomUUID();

        NotificationDirectory obj = new NotificationDirectory();
        obj.getTopics().add(Notification.PASSWORD_RESET);
        obj.getForwarding().add("example@com.de");
        obj.setType(NotificationType.EMAIL);
        obj.getBindings().put("x", "y");
        obj.setUuid(directory);

        NotificationRequestDto request = new NotificationRequestDto();
        request.setType(NotificationType.EMAIL);
        request.setTemplate(Notification.PASSWORD_RESET);

        // this is not valid as combination of x and y is required
        request.getParams().put("x", "z");
        request.getParams().put("z", "y");

        when(templateRepository.findOne(request.getTemplate().getTemplateId())).thenReturn(new NotificationTemplate());
        when(directoryRepository.findApplicable(obj.getBindings().keySet(), obj.getBindings().values().stream().collect(Collectors.toList()), NotificationType.EMAIL)).thenReturn(Collections.singletonList(obj));

        service.createTask(request);
    }

    @Test
    public void createTask_should_ensure_binding_independent_on_data_type()
    {
        UUID directory = UUID.randomUUID();

        NotificationDirectory obj = new NotificationDirectory();
        obj.getTopics().add(Notification.BOOKING_CONFIRMATION);
        obj.getForwarding().add("example@com.de");
        obj.setType(NotificationType.EMAIL);
        obj.getBindings().put("x", "1");
        obj.setUuid(directory);

        NotificationRequestDto request = new NotificationRequestDto();
        request.setType(NotificationType.EMAIL);
        request.setTemplate(Notification.BOOKING_CONFIRMATION);

        // this should be valid even its an int and binding is typeof string
        request.getParams().put("x", 1);

        when(templateRepository.findOne(request.getTemplate().getTemplateId())).thenReturn(new NotificationTemplate());
        when(directoryRepository.findApplicable(obj.getBindings().keySet(), obj.getBindings().values().stream().collect(Collectors.toList()), NotificationType.EMAIL)).thenReturn(Collections.singletonList(obj));

        service.createTask(request);

        verify(taskRepository, times(1)).save(anyCollectionOf(NotificationTask.class));
    }

    @Test(expected = IllegalStateException.class)
    public void createTask_should_ensure_correct_topics_only()
    {
        UUID directory = UUID.randomUUID();

        NotificationDirectory obj = new NotificationDirectory();
        obj.getForwarding().add("example@com.de");
        obj.setType(NotificationType.EMAIL);
        obj.setUuid(directory);

        NotificationRequestDto request = new NotificationRequestDto();
        request.setType(NotificationType.EMAIL);
        request.setTemplate(Notification.BOOKING_CONFIRMATION);

        when(templateRepository.findOne(request.getTemplate().getTemplateId())).thenReturn(new NotificationTemplate());
        when(directoryRepository.findApplicable(obj.getBindings().keySet(), obj.getBindings().values().stream().collect(Collectors.toList()), NotificationType.EMAIL)).thenReturn(Collections.singletonList(obj));

        service.createTask(request);
    }

    @Test
    public void cancelTask_should_delete_tasks() {
        List<NotificationCancellationRequestDto> requests = new ArrayList<>();
        NotificationCancellationRequestDto cancelRequest = new NotificationCancellationRequestDto();
        cancelRequest.setTemplate(Notification.REVIEW_REMINDER);
        NotificationCancellationRequestDto cancelRequest2 = new NotificationCancellationRequestDto();
        cancelRequest2.setTemplate(Notification.BOOKING_REMINDER);

        requests.add(cancelRequest);
        requests.add(cancelRequest2);

        NotificationTask task = new NotificationTask();
        task.setType(NotificationType.SMS);
        NotificationTemplate template = new NotificationTemplate();
        template.setTemplatePath("dummy/path");
        task.setTemplate(template);

        when (taskRepository.findAll(any(Predicate.class))).thenReturn(Collections.nCopies(2, task));
        service.cancelTasks(requests);
        verify(taskRepository, times(4)).delete(any(NotificationTask.class));
    }

    @Test
    public void cancelTask_should_not_delete_tasks_with_no_criteria()
    {
        List<NotificationCancellationRequestDto> requests = new ArrayList<>();
        NotificationCancellationRequestDto cancelRequest = new NotificationCancellationRequestDto();
        requests.add(cancelRequest);

        when (taskRepository.findAll(any(Predicate.class))).thenThrow(new IllegalStateException());

        service.cancelTasks(requests);

        verify(taskRepository, times(0)).delete(any(NotificationTask.class));
    }
}
