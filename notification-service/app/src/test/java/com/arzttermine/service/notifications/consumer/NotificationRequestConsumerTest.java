package com.arzttermine.service.notifications.consumer;

import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.arzttermine.library.messaging.processor.MessageBus;
import com.arzttermine.library.suport.DateFormatterUtil;
import com.arzttermine.library.web.struct.Language;
import com.arzttermine.service.notification.messages.booking.BookingConfirmationMessage;
import com.arzttermine.service.notification.messages.booking.BookingReminderMessage;
import com.arzttermine.service.notification.messages.notification.CancelNotificationsMessage;
import com.arzttermine.service.notification.messages.notification.NotificationCancellationRequestDto;
import com.arzttermine.service.notifications.web.BaseIntegrationTest;
import com.arzttermine.service.notifications.web.struct.Notification;
import com.arzttermine.service.notifications.web.struct.NotificationType;
import com.arzttermine.service.profile.web.dto.LocationDto;
import com.twilio.exception.ApiException;
import com.twilio.http.Response;
import com.twilio.http.TwilioRestClient;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Sql({"/db/empty_database.sql", "/db/booking_templates.sql"})
public class NotificationRequestConsumerTest extends BaseIntegrationTest
{
    @Autowired
    private MessageBus messageBus;

    @Autowired
    private AmazonSimpleEmailService amazonSimpleEmailService;

    @Autowired
    private TwilioRestClient restClient;

    @Before
    public void setup() {
        reset(amazonSimpleEmailService, restClient);
    }

    @After
    public void teardown() throws Exception {
        Thread.sleep(50);
    }

    @Test
    public void requestBookingReminder_should_reject_task_if_params_are_invalid() throws Exception
    {
        BookingReminderMessage.ReminderBag bag = new BookingReminderMessage.ReminderBag();
        bag.practiceLocation = new LocationDto();
        bag.practiceLocation.setCity("Berlin");
        bag.practiceLocation.setZip("12345");
        bag.start = Instant.now();
        bag.created = Instant.now();

        BookingReminderMessage message = new BookingReminderMessage(bag, NotificationType.SMS, "call me baby", Language.GERMAN);
        messageBus.send(message);

        Thread.sleep(100);

        mockMvc.perform(employeeRequest(42L, () -> get("/notifications")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.elements[*]", hasSize(0)));

        verify(amazonSimpleEmailService, never()).sendEmail(any());
        verify(restClient, never()).request(any()); // no lookup as request was already invalid
    }

    @Test
    public void requestBookingReminder_should_create_task_for_reminder() throws Exception
    {
        OffsetDateTime bookingStart = Instant.now().atOffset(ZoneOffset.UTC).plusSeconds(50000);
        BookingReminderMessage.ReminderBag bag = createBookingReminderBag(bookingStart);

        BookingReminderMessage message = new BookingReminderMessage(bag, NotificationType.SMS, "call me baby", Language.GERMAN);
        messageBus.send(message);

        Thread.sleep(150);

        mockMvc.perform(employeeRequest(42L, () -> get("/notifications")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.elements[*]", hasSize(1)))
            .andExpect(jsonPath("$.elements[0].recipient", is("call me baby")))
            .andExpect(jsonPath("$.elements[0].type", is(NotificationType.SMS.name())))
            .andExpect(jsonPath("$.elements[0].templateId", is(1)))
            .andExpect(jsonPath("$.elements[0].sendAt").doesNotExist())
            .andExpect(jsonPath("$.elements[0].expectedAt", is(DateFormatterUtil.format(bookingStart.minusMinutes(30).toInstant()))));
    }

    @Test
    public void requestBookingConfirmation_should_create_and_send_task_for_confirmation() throws Exception
    {
        when(restClient.request(any())).thenReturn(new Response("", 200));

        BookingConfirmationMessage.ConfirmationBag bag = createBookingConfirmationBag();
        BookingConfirmationMessage message = new BookingConfirmationMessage(bag, NotificationType.EMAIL, "test@example.com", Language.GERMAN);
        messageBus.send(message);

        Thread.sleep(250);

        mockMvc.perform(employeeRequest(42L, () -> get("/notifications")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.elements[*]", hasSize(1)))
            .andExpect(jsonPath("$.elements[0].recipient", is("test@example.com")))
            .andExpect(jsonPath("$.elements[0].type", is(NotificationType.EMAIL.name())))
            .andExpect(jsonPath("$.elements[0].templateId", is(2)))
            .andExpect(jsonPath("$.elements[0].expectedAt", is(DateFormatterUtil.format(bag.start.minusSeconds(50000)))))
            .andExpect(jsonPath("$.elements[0].sendAt", notNullValue()))
            .andExpect(jsonPath("$.elements[0].invalid", is(false)));

        verify(amazonSimpleEmailService, times(1)).sendEmail(any());
        verify(restClient, never()).request(any());
    }

    @Test
    public void requestBookingConfirmation_should_not_send_out_sms_if_number_is_invalid() throws Exception
    {
        when(restClient.request(any())).thenThrow(new ApiException(""));
        BookingConfirmationMessage.ConfirmationBag bag = createBookingConfirmationBag();

        BookingConfirmationMessage message = new BookingConfirmationMessage(bag, NotificationType.SMS, "invalid-number", Language.GERMAN);
        messageBus.send(message);

        Thread.sleep(250);

        mockMvc.perform(employeeRequest(42L, () -> get("/notifications")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.elements[*]", hasSize(1)))
            .andExpect(jsonPath("$.elements[0].recipient", is("invalid-number")))
            .andExpect(jsonPath("$.elements[0].type", is(NotificationType.SMS.name())))
            .andExpect(jsonPath("$.elements[0].templateId", is(2)))
            .andExpect(jsonPath("$.elements[0].expectedAt", is(DateFormatterUtil.format(bag.start.minusSeconds(50000)))))
            .andExpect(jsonPath("$.elements[0].sendAt").doesNotExist())
            .andExpect(jsonPath("$.elements[0].invalid", is(true)));

        verify(amazonSimpleEmailService, never()).sendEmail(any());
        verify(restClient, times(1)).request(any());
    }

    @Test
    public void cancelNotificationMessage_should_cancel_messages() throws Exception
    {
        OffsetDateTime bookingStart = Instant.now().atOffset(ZoneOffset.UTC).plusDays(23);
        BookingReminderMessage.ReminderBag bag = createBookingReminderBag(bookingStart);

        BookingReminderMessage smsMessage = new BookingReminderMessage(bag, NotificationType.SMS, "01010101", Language.GERMAN);
        BookingReminderMessage emailMessage = new BookingReminderMessage(bag, NotificationType.EMAIL, "not_a_real@email.address", Language.GERMAN);
        messageBus.send(smsMessage);
        messageBus.send(emailMessage);

        Thread.sleep(150);

        mockMvc.perform(employeeRequest(42L, () -> get("/notifications")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.elements[*]", hasSize(2)));

        NotificationCancellationRequestDto cancelRequest = new NotificationCancellationRequestDto();
        cancelRequest.setTemplate(Notification.BOOKING_REMINDER);
        CancelNotificationsMessage cancelMessages = new CancelNotificationsMessage(cancelRequest);
        messageBus.send(cancelMessages);

        Thread.sleep(150);

        mockMvc.perform(employeeRequest(42L, () -> get("/notifications")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.elements[*]", hasSize(0)));

    }

    @Test
    public void cancelNotificationMessage_should_cancel_only_sms_messages() throws Exception
    {
        OffsetDateTime bookingStart = Instant.now().atOffset(ZoneOffset.UTC).plusDays(23);
        BookingReminderMessage.ReminderBag bag = createBookingReminderBag(bookingStart);
        BookingConfirmationMessage.ConfirmationBag confirmBag = createBookingConfirmationBag();

        BookingConfirmationMessage confirmMessage = new BookingConfirmationMessage(confirmBag, NotificationType.SMS, "confirmme", Language.GERMAN);
        BookingReminderMessage smsMessage = new BookingReminderMessage(bag, NotificationType.SMS, "01010101", Language.GERMAN);
        BookingReminderMessage emailMessage = new BookingReminderMessage(bag, NotificationType.EMAIL, "not_a_real@email.address", Language.GERMAN);
        messageBus.send(smsMessage);
        messageBus.send(emailMessage);
        messageBus.send(confirmMessage);

        Thread.sleep(150);

        mockMvc.perform(employeeRequest(42L, () -> get("/notifications")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.elements[*]", hasSize(3)));

        NotificationCancellationRequestDto cancelRequest = new NotificationCancellationRequestDto();
        cancelRequest.setType(NotificationType.SMS);
        CancelNotificationsMessage cancelMessages = new CancelNotificationsMessage(cancelRequest);
        messageBus.send(cancelMessages);

        Thread.sleep(150);

        mockMvc.perform(employeeRequest(42L, () -> get("/notifications")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.elements[*]", hasSize(1)))
            .andExpect(jsonPath("$.elements[0].recipient", is("not_a_real@email.address")))
            .andExpect(jsonPath("$.elements[0].type", is(NotificationType.EMAIL.name())));

    }

    @Test
    public void cancelNotificationMessage_should_cancel_only_messages_matching_parameters() throws Exception
    {
        OffsetDateTime bookingStart = Instant.now().atOffset(ZoneOffset.UTC).plusDays(23);
        BookingReminderMessage.ReminderBag bag = createBookingReminderBag(bookingStart);
        BookingConfirmationMessage.ConfirmationBag confirmBag = createBookingConfirmationBag();

        BookingConfirmationMessage confirmMessage = new BookingConfirmationMessage(confirmBag, NotificationType.SMS, "confirmme", Language.GERMAN);
        BookingReminderMessage smsMessage = new BookingReminderMessage(bag, NotificationType.SMS, "01010101", Language.GERMAN);
        BookingReminderMessage emailMessage = new BookingReminderMessage(bag, NotificationType.EMAIL, "not_a_real@email.address", Language.GERMAN);
        messageBus.send(smsMessage);
        messageBus.send(emailMessage);
        messageBus.send(confirmMessage);

        Thread.sleep(150);

        mockMvc.perform(employeeRequest(42L, () -> get("/notifications")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.elements[*]", hasSize(3)));

        NotificationCancellationRequestDto cancelRequest = new NotificationCancellationRequestDto();
        cancelRequest.getParams().put("bookingId", 66l);
        CancelNotificationsMessage cancelMessages = new CancelNotificationsMessage(cancelRequest);
        messageBus.send(cancelMessages);

        Thread.sleep(150);

        mockMvc.perform(employeeRequest(42L, () -> get("/notifications")))
            .andExpect(status().isOk())
            .andDo(print())
            .andExpect(jsonPath("$.elements[*]", hasSize(1)))
            .andExpect(jsonPath("$.elements[0].recipient", is("confirmme")))
            .andExpect(jsonPath("$.elements[0].type", is(NotificationType.SMS.name())));

    }

    @Test
    public void cancelNotificationMessage_should_cancel_only_messages_matching_recipient() throws Exception
    {
        OffsetDateTime bookingStart = Instant.now().atOffset(ZoneOffset.UTC).plusDays(23);
        BookingReminderMessage.ReminderBag bag = createBookingReminderBag(bookingStart);
        BookingConfirmationMessage.ConfirmationBag confirmBag = createBookingConfirmationBag();

        BookingConfirmationMessage confirmSmsMessage = new BookingConfirmationMessage(confirmBag, NotificationType.SMS, "confirmmeEN", Language.ENGLISH);
        BookingConfirmationMessage confirmEmailMessage = new BookingConfirmationMessage(confirmBag, NotificationType.EMAIL, "confirmmeCH", Language.CHINESE);
        BookingReminderMessage smsMessage = new BookingReminderMessage(bag, NotificationType.SMS, "01010101", Language.GERMAN);
        BookingReminderMessage emailMessage = new BookingReminderMessage(bag, NotificationType.EMAIL, "not_a_real@email.address", Language.GERMAN);
        messageBus.send(smsMessage);
        messageBus.send(emailMessage);
        messageBus.send(confirmSmsMessage);
        messageBus.send(confirmEmailMessage);

        Thread.sleep(150);

        mockMvc.perform(employeeRequest(42L, () -> get("/notifications")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.elements[*]", hasSize(4)));

        NotificationCancellationRequestDto cancelRequest = new NotificationCancellationRequestDto();
        cancelRequest.setRecipient("not_a_real@email.address");
        CancelNotificationsMessage cancelMessages = new CancelNotificationsMessage(cancelRequest);
        messageBus.send(cancelMessages);

        Thread.sleep(150);

        mockMvc.perform(employeeRequest(42L, () -> get("/notifications")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.elements[*]", hasSize(3)))
            .andExpect(jsonPath("$.elements[*].recipient", containsInAnyOrder("confirmmeEN","confirmmeCH", "01010101")));

    }

    @Test
    public void cancelNotificationMessage_should_cancel_only_messages_matching_language() throws Exception
    {
        OffsetDateTime bookingStart = Instant.now().atOffset(ZoneOffset.UTC).plusDays(23);
        BookingReminderMessage.ReminderBag bag = createBookingReminderBag(bookingStart);
        BookingConfirmationMessage.ConfirmationBag confirmBag = createBookingConfirmationBag();

        BookingConfirmationMessage confirmSmsMessage = new BookingConfirmationMessage(confirmBag, NotificationType.SMS, "confirmmeEN", Language.ENGLISH);
        BookingConfirmationMessage confirmEmailMessage = new BookingConfirmationMessage(confirmBag, NotificationType.EMAIL, "confirmmeCH", Language.CHINESE);
        BookingReminderMessage smsMessage = new BookingReminderMessage(bag, NotificationType.SMS, "01010101", Language.GERMAN);
        BookingReminderMessage emailMessage = new BookingReminderMessage(bag, NotificationType.EMAIL, "not_a_real@email.address", Language.GERMAN);
        messageBus.send(smsMessage);
        messageBus.send(emailMessage);
        messageBus.send(confirmSmsMessage);
        messageBus.send(confirmEmailMessage);

        Thread.sleep(150);

        mockMvc.perform(employeeRequest(42L, () -> get("/notifications")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.elements[*]", hasSize(4)));

        NotificationCancellationRequestDto cancelRequest = new NotificationCancellationRequestDto();
        cancelRequest.setLanguage(Language.GERMAN);
        CancelNotificationsMessage cancelMessages = new CancelNotificationsMessage(cancelRequest);
        messageBus.send(cancelMessages);

        Thread.sleep(150);

        mockMvc.perform(employeeRequest(42L, () -> get("/notifications")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.elements[*]", hasSize(2)))
            .andExpect(jsonPath("$.elements[*].recipient", containsInAnyOrder("confirmmeEN","confirmmeCH")))
            .andExpect(jsonPath("$.elements[*].type", containsInAnyOrder(NotificationType.SMS.name(), NotificationType.EMAIL.name())));

    }

    @Test
    public void cancelNotificationMessage_should_cancel_nothing() throws Exception
    {
        OffsetDateTime bookingStart = Instant.now().atOffset(ZoneOffset.UTC).plusDays(23);
        BookingReminderMessage.ReminderBag bag = createBookingReminderBag(bookingStart);
        BookingConfirmationMessage.ConfirmationBag confirmBag = createBookingConfirmationBag();

        BookingConfirmationMessage confirmSmsMessage = new BookingConfirmationMessage(confirmBag, NotificationType.SMS, "confirmmeEN", Language.ENGLISH);
        BookingConfirmationMessage confirmEmailMessage = new BookingConfirmationMessage(confirmBag, NotificationType.EMAIL, "confirmmeCH", Language.CHINESE);
        BookingReminderMessage smsMessage = new BookingReminderMessage(bag, NotificationType.SMS, "01010101", Language.GERMAN);
        BookingReminderMessage emailMessage = new BookingReminderMessage(bag, NotificationType.EMAIL, "not_a_real@email.address", Language.GERMAN);
        messageBus.send(smsMessage);
        messageBus.send(emailMessage);
        messageBus.send(confirmSmsMessage);
        messageBus.send(confirmEmailMessage);

        Thread.sleep(150);

        mockMvc.perform(employeeRequest(42L, () -> get("/notifications")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.elements[*]", hasSize(4)));

        NotificationCancellationRequestDto cancelRequest = new NotificationCancellationRequestDto();
        CancelNotificationsMessage cancelMessages = new CancelNotificationsMessage(cancelRequest);
        messageBus.send(cancelMessages);

        Thread.sleep(150);

        mockMvc.perform(employeeRequest(42L, () -> get("/notifications")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.elements[*]", hasSize(4)));

    }

    public BookingConfirmationMessage.ConfirmationBag createBookingConfirmationBag() {
        BookingConfirmationMessage.ConfirmationBag bag = new BookingConfirmationMessage.ConfirmationBag();
        bag.practiceLocation = new LocationDto();
        bag.practiceLocation.setCity("Berlin");
        bag.practiceLocation.setZip("12345");
        bag.practiceLocation.setLongitude(42f);
        bag.practiceLocation.setLatitude(42f);
        bag.practiceLocation.setLocation("location 1");
        bag.customerId = 42;
        bag.customerName = "test";
        bag.start = Instant.now().plusSeconds(50000);
        bag.treatmentType = "treat me";
        bag.bookingId = 63;
        bag.practiceName = "practice";
        bag.doctorName = "doctor";
        return bag;
    }

    public BookingReminderMessage.ReminderBag createBookingReminderBag(OffsetDateTime bookingStart) {
        BookingReminderMessage.ReminderBag bag = new BookingReminderMessage.ReminderBag();
        bag.practiceLocation = new LocationDto();
        bag.practiceLocation.setCity("Berlin");
        bag.practiceLocation.setZip("12345");
        bag.practiceLocation.setLongitude(42f);
        bag.practiceLocation.setLatitude(42f);
        bag.practiceLocation.setLocation("location 1");
        bag.practiceName = "practice";
        bag.customerId = 42;
        bag.customerName = "test";
        bag.start = bookingStart.toInstant();
        bag.sendBeforeMinutes = 30;
        bag.created = Instant.now();
        bag.bookingId = 66;
        bag.doctorName = "doctor";
        bag.treatmentType = "treat me";

        return bag;
    }
}
