package com.arzttermine.service.notifications.service.processor.strategy;

import com.arzttermine.service.notifications.config.settings.TwilioSettings;
import com.arzttermine.service.notifications.domain.entity.NotificationTask;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twilio.exception.ApiException;
import com.twilio.http.Response;
import com.twilio.http.TwilioRestClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class SmsNotificationStrategyTest
{
    @Mock
    private TwilioRestClient restClient;

    @Mock
    private TwilioSettings settings;

    @InjectMocks
    private SmsNotificationStrategy strategy;

    private ObjectMapper objectMapper = new ObjectMapper();

    private static final String LOOKUP_RESPONSE_WITHOUT_PHONE = "{"+
    "    \"country_code\": \"US\","+
    "    \"national_format\": \"(415) 701-2311\","+
    "    \"url\": \"https://lookups.twilio.com/v1/PhoneNumber/+14157012311\","+
    "    \"caller_name\": {}," +
    "    \"carrier\": {"+
    "        \"type\": \"landline\","+
    "        \"error_code\": null,"+
    "        \"mobile_network_code\": null,"+
    "        \"mobile_country_code\": null,"+
    "        \"name\": \"Pacific Bell\""+
    "    }"+
    "}";

    private static final String MESSAGE_RESPONSE = "{" +
    "    \"sid\": \"SM5ec46c8ca6aa4e189f753a481b1236a8\"," +
    "    \"date_created\": \"Mon, 20 Mar 2017 09:57:00 +0000\"," +
    "    \"date_updated\": \"Mon, 20 Mar 2017 09:57:00 +0000\"," +
    "    \"date_sent\": null," +
    "    \"account_sid\": \"ACe42644041b391839dfb173b498acae09\"," +
    "    \"to\": \"+4917612345678\"," +
    "    \"from\": \"+4915735992133\"," +
    "    \"messaging_service_sid\": \"MGf0e365eefa9637981cf387544c4f6ee5\"," +
    "    \"body\": \"heyho\"," +
    "    \"status\": \"accepted\"," +
    "    \"num_segments\": \"0\"," +
    "    \"num_media\": \"0\"," +
    "    \"direction\": \"outbound-api\"," +
    "    \"api_version\": \"2010-04-01\"," +
    "    \"price\": null," +
    "    \"price_unit\": null," +
    "    \"error_code\": null," +
    "    \"error_message\": null," +
    "    \"uri\": \"/2010-04-01/Accounts/ACe42644041b391839dfb173b498acae09/Messages/SM5ec46c8ca6aa4e189f753a481b1236a8.json\"," +
    "    \"subresource_uris\": {" +
    "        \"media\": \"/2010-04-01/Accounts/ACe42644041b391839dfb173b498acae09/Messages/SM5ec46c8ca6aa4e189f753a481b1236a8/Media.json\"" +
    "    }" +
    "}";

    @Before
    public void setup() {
        when(settings.getNotificationNumber()).thenReturn("+4917612345678");
        when(restClient.getObjectMapper()).thenReturn(objectMapper);
    }

    @Test
    public void send_should_validate_phone_number_and_invalidate_task_if_invalid()
    {
        when(restClient.request(any())).thenThrow(new ApiException(""));

        NotificationTask task = new NotificationTask();
        task.setRecipient("test");

        boolean result = strategy.send(task);

        assertFalse(result);
        assertTrue(task.isInvalid());
    }

    @Test
    public void send_should_report_non_existing_mobile_number() throws Exception
    {
        when(restClient.request(any())).thenReturn(new Response(LOOKUP_RESPONSE_WITHOUT_PHONE, 200));

        NotificationTask task = new NotificationTask();
        task.setRecipient("test");

        boolean result = strategy.send(task);

        assertFalse(result);
        assertTrue(task.isInvalid());

        verify(restClient, times(1)).request(any());
    }

    @Test
    public void send_send_out_sms() throws Exception
    {
        when(restClient.request(any())).thenReturn(new Response(MESSAGE_RESPONSE, 200));
        when(settings.isTesting()).thenReturn(true);

        NotificationTask task = new NotificationTask();
        task.setRecipient("test");

        boolean result = strategy.send(task);

        assertTrue(result);
        assertFalse(task.isInvalid());
        verify(restClient, times(1)).request(any());
    }
}
