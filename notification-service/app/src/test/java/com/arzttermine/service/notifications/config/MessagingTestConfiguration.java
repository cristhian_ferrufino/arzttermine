package com.arzttermine.service.notifications.config;

import com.arzttermine.library.messaging.config.settings.MessagingConnectionSettings;
import com.arzttermine.library.messaging.processor.MessageBus;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.concurrent.ExecutorService;

@Configuration
@Profile("testing")
public class MessagingTestConfiguration extends MessagingConfiguration
{
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ExecutorService executorService;

    @Bean
    public MessageBus messageBus(MessagingConnectionSettings settings) {
        settings.setExecutorService(executorService);

        return super.messageBus(objectMapper, settings);
    }
}
