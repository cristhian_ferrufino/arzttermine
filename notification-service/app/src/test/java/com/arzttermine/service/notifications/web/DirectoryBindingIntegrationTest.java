package com.arzttermine.service.notifications.web;

import com.arzttermine.library.messaging.processor.MessageBus;
import com.arzttermine.library.web.struct.InsuranceType;
import com.arzttermine.library.web.struct.Language;
import com.arzttermine.service.notification.messages.booking.BookingCreatedMessage;
import com.arzttermine.service.notifications.web.request.DirectoryPatchDto;
import com.arzttermine.service.notifications.web.struct.Notification;
import com.arzttermine.service.notifications.web.struct.NotificationType;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;

import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class DirectoryBindingIntegrationTest extends BaseIntegrationTest
{
    @Autowired
    private MessageBus messageBus;

    @Test
    @Sql({"/db/empty_database.sql", "/db/booking_templates.sql"})
    public void notification_should_be_forwarded_to_directories() throws Exception
    {
        Map<String, String> bindings = new HashMap<>(1);
        bindings.put("practiceId", "12345");

        DirectoryDto dto = new DirectoryDto();
        dto.setForwarding(Arrays.asList("forward-to@example.com", "another@example.com"));
        dto.getBindings().putAll(bindings);
        dto.setType(NotificationType.EMAIL);
        dto.setTopics(Collections.singletonList(Notification.BOOKING_CREATED));

        DirectoryPatchDto request = new DirectoryPatchDto();
        request.setDirectories(Collections.singletonList(dto));

        mockMvc.perform(systemRequest(() -> post("/directories"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isOk());

        BookingCreatedMessage.CreatedBag bag = new BookingCreatedMessage.CreatedBag();
        bag.city = "Berlin";
        bag.treatmentType = "treat me";
        bag.practiceId = 12345;
        bag.practiceName = "homebase";
        bag.returningPatient = false;
        bag.insuranceType = InsuranceType.COMPULSORY;
        bag.bookingId = 42;
        bag.customerMail = "ibookedanappointment@mail.com";
        bag.customerPhone = "call me";
        bag.customerName = "hans";
        bag.doctorName = "dr prof prof dr herr prof doctorant";
        bag.end = Instant.now();
        bag.start = Instant.now();

        BookingCreatedMessage message = new BookingCreatedMessage(bag, NotificationType.EMAIL, "original-target@example.com", Language.GERMAN);
        messageBus.send(message);

        Thread.sleep(250);

        mockMvc.perform(systemRequest(() -> get("/notifications")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.elements[*]", Matchers.hasSize(3)))
            .andExpect(jsonPath("$.elements[*].recipient",
                Matchers.containsInAnyOrder("original-target@example.com", "another@example.com", "forward-to@example.com")));
    }
}
