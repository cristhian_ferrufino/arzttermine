package com.arzttermine.service.notifications.service.processor;

import com.arzttermine.library.suport.templating.TemplateTask;
import com.arzttermine.library.suport.templating.TemplateContentTransformer;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("testing")
public class TemplateTestTransformer implements TemplateContentTransformer
{
    @Override
    public String getContent(TemplateTask task) {
        return "";
    }
}
