package com.arzttermine.service.notifications.service.processor;

import com.arzttermine.library.suport.templating.TemplateContentTransformer;
import com.arzttermine.service.notifications.domain.entity.NotificationTask;
import com.arzttermine.service.notifications.domain.reposiory.NotificationTaskRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class NotificationTaskProcessorTest
{
    @Mock
    private NotificationTaskRepository repository;

    @Mock
    private TemplateContentTransformer contentTransformer;

    @InjectMocks
    private NotificationTaskProcessor processor;

    @Test
    public void process_should_reject_tasks_which_are_too_old()
    {
        NotificationTask task = new NotificationTask();
        task.setExpectedAt(Instant.now().minus(30, ChronoUnit.HOURS));
        assertFalse(task.isInvalid());

        when(repository.findOne(42L)).thenReturn(task);

        processor.process(42L);

        assertTrue(task.isInvalid());
        verify(contentTransformer, never()).getContent(any());
    }
}
