package com.arzttermine.service.notifications.config;

import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.twilio.http.TwilioRestClient;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("testing")
public class ExternalProviderTestConfiguration
{
    @Bean
    public TwilioRestClient twilioRestClient()
    {
        return Mockito.mock(TwilioRestClient.class);
    }

    @Bean
    public AmazonSimpleEmailService amazonSimpleEmailService()
    {
        return Mockito.mock(AmazonSimpleEmailService.class);
    }
}
