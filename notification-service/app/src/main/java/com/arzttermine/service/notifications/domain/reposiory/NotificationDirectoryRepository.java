package com.arzttermine.service.notifications.domain.reposiory;

import com.arzttermine.service.notifications.domain.entity.NotificationDirectory;
import com.arzttermine.service.notifications.web.struct.NotificationType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.UUID;

@Repository
public interface NotificationDirectoryRepository extends JpaRepository<NotificationDirectory, UUID>
{
    @Query("SELECT DISTINCT nd FROM NotificationDirectory nd JOIN nd.bindings bd WHERE KEY(bd) IN (:keys) AND VALUE(bd) IN (:values) AND nd.type = :type")
    Collection<NotificationDirectory> findApplicable(@Param("keys") Collection<String> keys, @Param("values") Collection<String> values, @Param("type") NotificationType type);
}
