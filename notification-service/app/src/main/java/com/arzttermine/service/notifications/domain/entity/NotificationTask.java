package com.arzttermine.service.notifications.domain.entity;

import com.arzttermine.library.web.struct.Language;
import com.arzttermine.library.suport.templating.TemplateTask;
import com.arzttermine.service.notifications.support.InstantToTimestampConverter;
import com.arzttermine.service.notifications.web.struct.NotificationType;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity
@Table(
    name = "notification_tasks",
    indexes = {
        @Index(name = "id_notification_task_processed", columnList = "send")
    }
)
public class NotificationTask implements TemplateTask
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false, insertable = false)
    private long id;

    @Column(nullable = false)
    private boolean send = false;

    @Column(nullable = false)
    private boolean invalid = false;

    @OrderBy
    @Column(name = "expected_at", nullable = false)
    @Convert(converter = InstantToTimestampConverter.class)
    private Instant expectedAt = Instant.now();

    @Column(name = "send_at", nullable = true)
    @Convert(converter = InstantToTimestampConverter.class)
    private Instant sendAt;

    @Column(name = "customer_id", updatable = false)
    private Long customerId;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, updatable = false)
    private NotificationType type;

    @Column(name = "value", updatable = false)
    @ElementCollection(fetch = FetchType.EAGER)
    @MapKeyColumn(name = "property", updatable = false)
    @CollectionTable(name = "notification_task_properties")
    private Map<String, String> params = new HashMap<>();

    @ElementCollection(fetch = FetchType.EAGER)
    @OneToMany( cascade=  CascadeType.ALL)
    @JoinColumn(name = "notification_task_id", nullable = false)
    private List<NotificationAttachment> attachments;

    @ManyToOne(optional = false, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "template_id", nullable = false, updatable = false)
    private NotificationTemplate template;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Language language = Language.GERMAN;

    @NotNull
    @Length(min = 3, max = 256)
    private String recipient;

    @Transient
    private transient String content;

    public NotificationTask() {
    }

    public NotificationTask(NotificationTask task) {
        this.id = task.id;
        this.send = task.send;
        this.invalid = task.invalid;
        this.expectedAt = task.expectedAt;
        this.sendAt = task.sendAt;
        this.customerId = task.customerId;
        this.type = task.type;
        this.params = task.params;
        this.attachments = task.attachments;
        this.template = task.template;
        this.language = task.language;
        this.recipient = task.recipient;
        this.content = task.content;
    }

    public long getId() {
        return id;
    }

    public boolean isSend() {
        return send;
    }

    public Instant getExpectedAt() {
        return expectedAt;
    }

    public void setExpectedAt(Instant expectedAt) {
        this.expectedAt = expectedAt;
    }

    public Instant getSendAt() {
        return sendAt;
    }

    public void sendAt(Instant sendAt) {
        this.send = true;
        this.sendAt = sendAt;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public NotificationType getType() {
        return type;
    }

    public void setType(NotificationType type) {
        this.type = type;
    }

    public NotificationTemplate getTemplate() {
        return template;
    }

    public void setTemplate(NotificationTemplate template) {
        this.template = template;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public boolean isInvalid() {
        return invalid;
    }

    public void invalidate() {
        invalid = true;
    }

    public String getPath() {
        final String path = String.format(
            "%s/%s",
            getType().name().toLowerCase(),
            getTemplate().getTemplatePath()
        );

        return path;
    }

    public List<NotificationAttachment> getAttachments() {
        return this.attachments;
    }

    public void setAttachments(List<NotificationAttachment> attachments) {
        this.attachments = attachments;
    }
}
