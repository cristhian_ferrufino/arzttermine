package com.arzttermine.service.notifications.service.event.model;

import com.arzttermine.service.notifications.domain.entity.NotificationTask;

import java.util.Arrays;
import java.util.Collection;

public class NotificationCreatedEvent
{
    private final Collection<NotificationTask> tasks;

    NotificationCreatedEvent(Collection<NotificationTask> tasks) {
        this.tasks = tasks;
    }

    public static NotificationCreatedEvent of(NotificationTask... tasks) {
        return new NotificationCreatedEvent(Arrays.asList(tasks));
    }

    public static NotificationCreatedEvent of(Collection<NotificationTask> tasks) {
        return new NotificationCreatedEvent(tasks);
    }

    public Collection<NotificationTask> getTasks() {
        return tasks;
    }
}
