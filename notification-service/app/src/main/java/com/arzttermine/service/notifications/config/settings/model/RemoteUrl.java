package com.arzttermine.service.notifications.config.settings.model;

import com.arzttermine.library.suport.templating.exchange.UrlRequest;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class RemoteUrl
{
    private String scheme = "https";
    private int port = 80;
    private String host;
    private List<String> path = new LinkedList<>();
    private Map<String, List<String>> query = new LinkedMultiValueMap<>();

    public String getScheme() {
        return scheme;
    }

    public void setScheme(final String scheme) {
        this.scheme = scheme;
    }

    public String getHost() {
        return host;
    }

    public void setHost(final String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(final int port) {
        this.port = port;
    }

    public List<String> getPath() {
        return path;
    }

    public void setPath(final List<String> path) {
        this.path = path;
    }

    public Map<String, List<String>> getQuery() {
        return query;
    }

    public void setQuery(final Map<String, List<String>> query) {
        this.query = query;
    }

    /**
     * You don't need to care about encoding.
     * All pathSegments and queryParams are automatically encoded.
     * To use fragments (eg: #/appointment) use {@see UrlRequestDto.fragments}
     *
     * @param request to merge with
     * @return full qualified URI with merged path and query segments as well as fragments and the corresponding target settings
     */
    public String buildWith(UrlRequest request) {
        final UriComponentsBuilder builder = UriComponentsBuilder.newInstance()
            .scheme(scheme)
            .host(host)
            .pathSegment(path.toArray(new String[path.size()]))
            .pathSegment(request.getPath())
            .queryParams(request.getQuery());

        if (port != 80)
            builder.port(port);

        if (null != request.getFragments() && request.getFragments().length > 0) {
            builder.fragment("/" + String.join("/", request.getFragments()));
        }

        query.forEach((key, value) ->
            builder.queryParam(key, value.toArray()));

        return builder.toUriString();
    }
}
