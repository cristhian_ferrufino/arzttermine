package com.arzttermine.service.notifications.config.settings;


import com.arzttermine.library.suport.templating.exchange.UrlRequest;
import com.arzttermine.service.notifications.config.settings.model.RemoteUrl;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

@Configuration
@ConfigurationProperties(
    locations = "classpath:${spring.profiles.active:default}/targets.yml"
)
public class RemoteUrlSettings
{
    private Map<String, RemoteUrl> targets;

    public Map<String, RemoteUrl> getTargets() {
        return targets;
    }

    public void setTargets(final Map<String, RemoteUrl> targets) {
        this.targets = targets;
    }

    public RemoteUrl getByType(UrlRequest.Type type) {
        final String search = type.name().toLowerCase();

        return targets.entrySet().stream()
            .filter(e -> e.getKey().equals(search))
            .findFirst()
            .orElseThrow(() -> new MissingConfigurationException(type))
            .getValue();
    }

    private static final class MissingConfigurationException extends RuntimeException
    {
        private MissingConfigurationException(UrlRequest.Type type) {
            super("Missing configuration for " + type.name());
        }
    }
}
