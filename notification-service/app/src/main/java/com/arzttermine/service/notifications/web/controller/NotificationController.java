package com.arzttermine.service.notifications.web.controller;

import com.arzttermine.service.notifications.service.NotificationService;
import com.arzttermine.service.notifications.web.response.NotificationsPage;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/notifications")
public class NotificationController
{
    @Autowired
    private NotificationService service;

    @ApiOperation("Returns page of notifications")
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "page returned")
    })
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyRole('ROLE_EMPLOYEE', 'ROLE_SYSTEM')")
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public NotificationsPage getNotifications(Pageable pageable)
    {
        return service.get(pageable);
    }
}
