package com.arzttermine.service.notifications.service.event;

import com.arzttermine.service.notifications.service.event.model.NotificationCreatedEvent;
import com.arzttermine.service.notifications.service.processor.NotificationTaskProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

import java.time.Instant;
import java.util.concurrent.ExecutorService;

@Component
public class NotificationCreatedListener
{
    private static final Logger log = LoggerFactory.getLogger(NotificationCreatedListener.class);

    @Autowired
    private NotificationTaskProcessor taskProcessor;

    @Autowired
    private ExecutorService executorService;

    @Async
    @TransactionalEventListener
    public void notificationCreated(NotificationCreatedEvent event)
    {
        log.info("created {} notification tasks", event.getTasks().size());

        final Instant now = Instant.now().plusSeconds(1);

        event.getTasks().stream()
            .filter(t -> t.getExpectedAt().isBefore(now))
            .forEach(t -> executorService.submit(() -> taskProcessor.process(t.getId())));
    }
}
