package com.arzttermine.service.notifications.service.processor.strategy;

import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.model.*;
import com.arzttermine.library.suport.AmazonStorage;
import com.arzttermine.service.notifications.domain.entity.NotificationAttachment;
import com.arzttermine.service.notifications.domain.entity.NotificationTask;
import com.arzttermine.service.notifications.web.struct.NotificationType;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailConstants;
import org.apache.commons.mail.HtmlEmail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.activation.DataSource;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.*;

@Component
public class EmailNotificationStrategy implements NotificationStrategy
{
    private static final Logger log = LoggerFactory.getLogger(EmailNotificationStrategy.class);

    private static final String fromAddress = "arzttermine.de <no-reply@arzttermine.de>";
    private static final String hostName = "arzttermine.de";

    @Autowired
    private AmazonStorage amazonStorage;

    @Autowired
    private AmazonSimpleEmailService amazonSimpleEmailService;

    private boolean sendSimpleEmail(final NotificationTask task) {
        final Destination destination = new Destination(Collections.singletonList(task.getRecipient()));
        final Content content = new Content()
            .withCharset(StandardCharsets.UTF_8.name())
            .withData(task.getContent());

        final Message message = new Message(new Content(task.getTemplate().getSubject()), new Body().withHtml(content));
        final SendEmailRequest request = new SendEmailRequest(fromAddress, destination, message);

        log.info("sending email to {} about {}", task.getRecipient(), message.getSubject().getData());

        try {
            amazonSimpleEmailService.sendEmail(request);
            return true;
        } catch (Exception e) {
            log.error("could not send email to {}", task.getRecipient(), e);
        }

        return false;
    }

    private boolean sendMimeEmail(final NotificationTask task) {
        try {
            HtmlEmail email = new HtmlEmail();
            email.setCharset(EmailConstants.UTF_8);

            email.setSubject(task.getTemplate().getSubject());
            email.setFrom(fromAddress);
            email.addReplyTo(fromAddress);
            email.addTo(task.getRecipient());
            email.setHtmlMsg(task.getContent());
            email.setHostName(hostName);

            for (NotificationAttachment attachment : task.getAttachments()) {
                S3Object s3Object = amazonStorage.download(attachment.getAttachmentKey());
                if (null != s3Object) {
                    DataSource data = new ByteArrayDataSource(s3Object.getObjectContent(), s3Object.getObjectMetadata().getContentType());
                    email.attach(data, attachment.getAttachmentKey(), attachment.getAttachmentKey(), EmailAttachment.ATTACHMENT);
                }
            }
            // Convert the email from apache.commons.email to javax.mail so it can be processed by Amazon. Yes thats how good Amazon's sdk is.
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            email.buildMimeMessage();
            MimeMessage mimeMessage = email.getMimeMessage();
            mimeMessage.writeTo(outputStream);
            RawMessage rawMessage = new RawMessage(ByteBuffer.wrap(outputStream.toByteArray()));
            log.info("Sending email: {}, {}", task.getRecipient(), task.getTemplate().getSubject());

            SendRawEmailRequest rawEmailRequest = new SendRawEmailRequest(rawMessage);
            amazonSimpleEmailService.sendRawEmail(rawEmailRequest);
            return true;
        }
        catch (Exception e) {
            // TODO : Something much better than catching all this crap.
            log.error("Sending email With attachment failed: {}",e);
        }

        return false;
    }

    @Override
    public boolean send(final NotificationTask task) {
        boolean success = false;

        String subject = task.getTemplate().getSubject();

        if (task.getAttachments() == null || task.getAttachments().isEmpty()) {
            success = sendSimpleEmail(task);
        }
        else {
            success = sendMimeEmail(task);
        }

        return success;
    }

    @Override
    public boolean supports(NotificationType type) {
        return type.equals(NotificationType.EMAIL);
    }
}
