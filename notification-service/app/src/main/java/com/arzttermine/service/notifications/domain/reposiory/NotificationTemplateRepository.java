package com.arzttermine.service.notifications.domain.reposiory;

import com.arzttermine.service.notifications.domain.entity.NotificationTemplate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NotificationTemplateRepository extends JpaRepository<NotificationTemplate, Long>
{
}
