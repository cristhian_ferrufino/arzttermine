package com.arzttermine.service.notifications.support;

import javax.persistence.AttributeConverter;
import java.sql.Timestamp;
import java.time.Instant;

public final class InstantToTimestampConverter implements AttributeConverter<Instant, Timestamp>
{
    public Timestamp convertToDatabaseColumn(Instant attribute)
    {
        return attribute == null ? null : new Timestamp(attribute.toEpochMilli());
    }

    public Instant convertToEntityAttribute(Timestamp dbData)
    {
        return dbData == null ? null : Instant.ofEpochMilli(dbData.getTime());
    }
}
