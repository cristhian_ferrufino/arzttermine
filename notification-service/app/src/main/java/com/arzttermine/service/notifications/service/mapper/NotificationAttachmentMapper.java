package com.arzttermine.service.notifications.service.mapper;

import com.arzttermine.service.notifications.domain.entity.NotificationAttachment;
import com.arzttermine.service.notifications.web.request.NotificationAttachmentDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class NotificationAttachmentMapper {

    public List<NotificationAttachment> fromDtos(List<NotificationAttachmentDto> attachmentDtos) {

        return attachmentDtos.stream()
            .map(attachment -> fromDto(attachment))
            .collect(Collectors.toList());

    }

    public NotificationAttachment fromDto(NotificationAttachmentDto attachmentDto) {
        return new NotificationAttachment(
            attachmentDto.getAttachmentKey(),
            attachmentDto.getBucketName(),
            attachmentDto.getMimetype()
        );
    }
}
