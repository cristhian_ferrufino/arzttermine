package com.arzttermine.service.notifications.service.processor;

import com.arzttermine.service.notifications.domain.reposiory.NotificationTaskRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.Clock;
import java.time.Instant;
import java.util.Collection;
import java.util.concurrent.ExecutorService;

@Component
public class NotificationScheduler
{
    private static final Logger log = LoggerFactory.getLogger(NotificationScheduler.class);

    @Autowired
    private NotificationTaskRepository taskRepository;

    @Autowired
    private ExecutorService executorService;

    @Autowired
    private NotificationTaskProcessor taskProcessor;

    @Autowired
    private Clock clock;

    @Scheduled(fixedRate = 300000)
    public void sendNotifications() {
        log.info("scanning for new pending notifications ...");

        final Collection<Long> tasks = taskRepository.findTaskIds(Instant.now(clock));

        if (tasks.isEmpty()) {
            log.info("no further notifications needs to be send");
            return;
        }

        log.info("found {} notification tasks to process ...", tasks.size());
        tasks.forEach(t -> executorService.submit(() -> taskProcessor.process(t)));
    }
}
