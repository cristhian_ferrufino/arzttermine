package com.arzttermine.service.notifications.config;

import com.arzttermine.service.profile.security.config.RemoteTokenServiceConfigurer;
import com.arzttermine.service.profile.security.config.ResourceServerConfigurer;
import com.arzttermine.service.profile.security.config.settings.RemoteTokenServicesSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;

@Configuration
public class ResourceServerConfiguration extends ResourceServerConfigurer
{
    @Autowired
    private RemoteTokenServices remoteTokenServices;

    @Value("${spring.application.name}")
    private String applicationName;

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception
    {
        resources.resourceId(applicationName);
        resources.tokenServices(remoteTokenServices);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception
    {
        http.authorizeRequests()
            .regexMatchers(
                "/error.*",
                ".*swagger-ui.*",
                "/health",
                "/.*\\.html",
                "/.*\\.css",
                "/.*\\.js",
                "/.*\\.png",
                "/.*\\.jpe?g",
                "/.*\\.woff2?",
                "/.*\\.ttf",
                "/swagger-resources.*",
                "/v.+/api-docs.*"
            )
            .permitAll()
            .and()
            .authorizeRequests()
            .anyRequest()
            .authenticated()
            .and()
            .sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    @Configuration
    @Profile("!testing")
    public static class RemoteTokenServicesConfigration extends RemoteTokenServiceConfigurer
    {
        @Autowired
        private RemoteTokenServicesSettings tokenServicesSettings;

        @Bean
        public RemoteTokenServices remoteTokenServices()
        {
            return configure(tokenServicesSettings);
        }
    }
}
