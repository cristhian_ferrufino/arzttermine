package com.arzttermine.service.notifications.service.processor;

import com.arzttermine.library.suport.exception.InvalidArgumentException;
import com.arzttermine.library.suport.templating.TemplateContentTransformer;
import com.arzttermine.library.suport.templating.TemplateTask;
import com.arzttermine.service.notifications.config.settings.RemoteUrlSettings;
import com.arzttermine.service.notifications.domain.entity.NotificationTask;
import com.arzttermine.service.notifications.domain.reposiory.NotificationTaskRepository;
import com.arzttermine.service.notifications.service.processor.strategy.NotificationStrategy;
import com.arzttermine.service.notifications.service.processor.templating.ProcessedNotificationTask;
import com.twilio.exception.ApiException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.ZoneOffset;
import java.util.List;

@Component
public class NotificationTaskProcessor
{
    private static final Logger log = LoggerFactory.getLogger(NotificationScheduler.class);

    private final List<NotificationStrategy> strategies;
    private final NotificationTaskRepository taskRepository;
    private final TemplateContentTransformer transformer;
    private final RemoteUrlSettings urlSettings;

    @Autowired
    public NotificationTaskProcessor(NotificationTaskRepository taskRepository, TemplateContentTransformer transformer, List<NotificationStrategy> strategies, RemoteUrlSettings urlSettings) {
        this.strategies = strategies;
        this.taskRepository = taskRepository;
        this.transformer = transformer;
        this.urlSettings = urlSettings;
    }

    @Transactional(readOnly = false, noRollbackFor = {ApiException.class, InvalidArgumentException.class})
    public void process(final long taskId) {
        final NotificationTask task = taskRepository.findOne(taskId);

        if (null != task.getExpectedAt()) {
            // reject task if its a day behind the expectation
            final Instant rejectAfter = task.getExpectedAt().atOffset(ZoneOffset.UTC).plusDays(1).toInstant();

            if (rejectAfter.isBefore(Instant.now())) {
                log.info("rejecting task {} at {} as we could not send out the notification before {}", task.getId(), task.getExpectedAt(), rejectAfter);
                task.invalidate();
                return;
            }
        }

        final NotificationStrategy strategy = strategies.stream()
            .filter(s -> s.supports(task.getType()))
            .findFirst()
            .orElseThrow(() -> new UnsupportedOperationException(task.getType() + " cannot be processed"));

        // pass a new copy of the NotificationTask including an override of getParams()
        final String content = transformer.getContent(new ProcessedNotificationTask(task, urlSettings));
        task.setContent(content);

        log.info("processing notification task {} of type {}", task.getId(), task.getType());
        final boolean send = strategy.send(task);

        if (send) {
            task.sendAt(Instant.now());
        }
    }
}
