package com.arzttermine.service.notifications.consumer;

import com.arzttermine.library.messaging.consumer.Consumer;
import com.arzttermine.service.notification.messages.notification.CancelNotificationsMessage;
import com.arzttermine.service.notifications.service.NotificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class NotificationCancellationConsumer {

    private static Logger logger = LoggerFactory.getLogger(NotificationCancellationConsumer.class);

    @Autowired
    private NotificationService notificationService;

    @Consumer(routingKey = "notification.cancellation.*")
    public void notificationCancellationMessage(CancelNotificationsMessage cancelMessage) {

        logger.info("got notification cancellation request {} ", cancelMessage.getTopicValue());
        notificationService.cancelTasks(cancelMessage.getCancellationRequests());

    }

}
