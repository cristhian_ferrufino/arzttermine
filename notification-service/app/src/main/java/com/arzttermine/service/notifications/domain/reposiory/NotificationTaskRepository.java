package com.arzttermine.service.notifications.domain.reposiory;

import com.arzttermine.service.notifications.domain.entity.NotificationTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.Collection;

@Repository
public interface NotificationTaskRepository extends JpaRepository<NotificationTask, Long>, QueryDslPredicateExecutor<NotificationTask> {

    @Query("SELECT t.id FROM NotificationTask t WHERE t.send IS FALSE AND t.invalid IS FALSE AND t.expectedAt <= :now")
    Collection<Long> findTaskIds(@Param("now") Instant now);

}
