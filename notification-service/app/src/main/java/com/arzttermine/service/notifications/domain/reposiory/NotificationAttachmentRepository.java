package com.arzttermine.service.notifications.domain.reposiory;

import com.arzttermine.service.notifications.domain.entity.NotificationAttachment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NotificationAttachmentRepository extends JpaRepository<NotificationAttachment, Long>{
}
