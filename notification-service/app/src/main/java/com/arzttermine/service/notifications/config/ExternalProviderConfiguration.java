package com.arzttermine.service.notifications.config;

import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.model.SetIdentityDkimEnabledRequest;
import com.arzttermine.service.notifications.config.settings.TwilioSettings;
import com.twilio.http.TwilioRestClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("!testing")
public class ExternalProviderConfiguration
{
    @Autowired
    private TwilioSettings settings;

    @Value("${cloud.aws.dkimIdent}")
    private String dkimIdentity;

    @Bean
    public TwilioRestClient twilioRestClient()
    {
        return new TwilioRestClient.Builder(settings.getUsername(), settings.getPassword())
            .build();
    }

    @Autowired
    public AmazonSimpleEmailService amazonSimpleEmailService(AmazonSimpleEmailService emailService)
    {
        final SetIdentityDkimEnabledRequest dkimRequest = new SetIdentityDkimEnabledRequest();
        dkimRequest.setIdentity(dkimIdentity);
        dkimRequest.setDkimEnabled(true);

        emailService.setIdentityDkimEnabled(dkimRequest);

        return emailService;
    }
}
