package com.arzttermine.service.notifications.service.processor.strategy;

import com.arzttermine.library.suport.exception.InvalidArgumentException;
import com.arzttermine.service.notifications.config.settings.TwilioSettings;
import com.arzttermine.service.notifications.domain.entity.NotificationTask;
import com.arzttermine.service.notifications.web.struct.NotificationType;
import com.twilio.exception.ApiException;
import com.twilio.http.TwilioRestClient;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.rest.api.v2010.account.MessageCreator;
import com.twilio.type.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SmsNotificationStrategy implements NotificationStrategy
{
    private static final Logger log = LoggerFactory.getLogger(SmsNotificationStrategy.class);

    private static final String FIXED_COUNTRY_CODE = "DE";

    @Autowired
    private TwilioRestClient restClient;

    @Autowired
    private TwilioSettings settings;

    @Override
    public boolean send(final NotificationTask task)
    {
        final MessageCreator builder;

        try {
            final PhoneNumber originalRecipient = new PhoneNumber(task.getRecipient());
            // lookups are rejected during testing
            final PhoneNumber recipient = (settings.isTesting()) ? originalRecipient : com.twilio.rest.lookups.v1.PhoneNumber
                .fetcher(originalRecipient)
                .setCountryCode(FIXED_COUNTRY_CODE)
                .setType(com.twilio.rest.lookups.v1.PhoneNumber.Type.MOBILE.name())
                .fetch(restClient)
                .getPhoneNumber();

            if (null == recipient) { // paranoia
                throw new InvalidArgumentException("phoneMobile", task.getRecipient() + " is not a valid mobile phone number");
            }

            builder = Message.creator(recipient, new PhoneNumber(settings.getNotificationNumber()), task.getContent());
        } catch (Exception e) {
            log.warn("error while trying to validate phone number " + task.getRecipient(), e);
            task.invalidate();
            return false;
        }

        final Message message;

        try {
            message = builder.create(restClient);
        } catch (ApiException e) {
            log.error("could not send sms notification for task {}", task.getId(), e);
            return false;
        }

        log.info("sms request send using sid {}, status: {}", message.getSid(), message.getStatus());
        return true;
    }

    @Override
    public boolean supports(NotificationType type) {
        return type.equals(NotificationType.SMS);
    }
}
