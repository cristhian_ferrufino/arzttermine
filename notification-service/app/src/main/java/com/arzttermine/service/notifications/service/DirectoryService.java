package com.arzttermine.service.notifications.service;

import com.arzttermine.library.suport.exception.EntityNotFoundException;
import com.arzttermine.service.notifications.domain.entity.NotificationDirectory;
import com.arzttermine.service.notifications.domain.reposiory.NotificationDirectoryRepository;
import com.arzttermine.service.notifications.service.mapper.DirectoryMapper;
import com.arzttermine.service.notifications.web.DirectoryDto;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class DirectoryService
{
    @Autowired
    private NotificationDirectoryRepository repository;

    @Autowired
    private DirectoryMapper mapper;

    @Transactional(readOnly = false)
    public Collection<DirectoryDto> patchDirectories(final Collection<DirectoryDto> updates)
    {
        final Map<UUID, DirectoryDto> recipients = updates.stream()
            .filter(e -> null != e.getUuid())
            .collect(Collectors.toMap(DirectoryDto::getUuid, Function.identity()));

        final Map<UUID, NotificationDirectory> directories;

        if (recipients.isEmpty()) {
            directories = Maps.newHashMap();
        }
        else {
            directories = repository.findAll(recipients.keySet()).stream()
                .collect(Collectors.toMap(NotificationDirectory::getUuid, Function.identity()));
        }

        final List<NotificationDirectory> newDirectories = new ArrayList<>();
        final List<NotificationDirectory> updatedDirectories = new ArrayList<>();

        updates.forEach((update) -> {
            final NotificationDirectory directory;
            final UUID id = update.getUuid();

            if (null == id || !directories.containsKey(id)) {
                directory = new NotificationDirectory();
                directory.getForwarding().addAll(update.getForwarding());
                directory.getTopics().addAll(update.getTopics());
                directory.setType(update.getType());

                newDirectories.add(directory);
            } else {
                directory = directories.get(id);

                merge(directory.getForwarding(), update.getForwarding());
                merge(directory.getTopics(), update.getTopics());
                directory.getBindings().clear();

                updatedDirectories.add(directory);
            }

            update.getBindings().forEach((property, bindings) ->
                directory.getBindings().put(property, String.valueOf(bindings)));

        });

        if (!newDirectories.isEmpty()) {
            repository.save(newDirectories);
        }

        updatedDirectories.addAll(newDirectories);
        return mapper.toDtos(updatedDirectories);
    }

    @Transactional(readOnly = false)
    public void deleteDirectory(final UUID uuid)
    {
        repository.delete(uuid);
    }

    @Transactional(readOnly = true)
    public DirectoryDto getDirectory(final UUID uuid)
    {
        return Optional.ofNullable(repository.findOne(uuid))
            .map(mapper::toDto)
            .orElseThrow(() -> new EntityNotFoundException("NotificationDirectory", uuid));
    }

    private <T> void merge(Collection<T> l1, Collection<T> l2) {
        l1.removeIf(i -> !l2.contains(i));
        l1.addAll(l2.stream().filter(e -> !l1.contains(e)).collect(Collectors.toList()));
    }
}
