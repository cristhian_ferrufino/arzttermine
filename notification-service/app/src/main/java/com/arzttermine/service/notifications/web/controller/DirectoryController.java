package com.arzttermine.service.notifications.web.controller;

import com.arzttermine.service.notifications.service.DirectoryService;
import com.arzttermine.service.notifications.web.DirectoryDto;
import com.arzttermine.service.notifications.web.request.DirectoryPatchDto;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Collection;
import java.util.UUID;

@RestController
@RequestMapping("/directories")
public class DirectoryController
{
    @Autowired
    private DirectoryService service;

    @ApiOperation("updates provided directories by recipient")
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "directories updated")
    })
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyRole('ROLE_EMPLOYEE', 'ROLE_SYSTEM') or #oauth2.hasScope('directories')")
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<DirectoryDto> patchDirectories(@Valid @RequestBody DirectoryPatchDto updates)
    {
        return service.patchDirectories(updates.getDirectories());
    }

    @ApiOperation("returns single directory")
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "directories returned")
    })
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyRole('ROLE_CUSTOMER', 'ROLE_CLIENT', 'ROLE_SYSTEM')")
    @RequestMapping(value = "/{uuid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public DirectoryDto getDirectories(@PathVariable UUID uuid)
    {
        return service.getDirectory(uuid);
    }

    @ApiOperation("removes given directory completely")
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_NO_CONTENT, message = "directory removed")
    })
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAnyRole('ROLE_EMPLOYEE', 'ROLE_SYSTEM') or #oauth2.hasScope('directories')")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void removeDirectory(@PathVariable UUID id)
    {
        service.deleteDirectory(id);
    }
}
