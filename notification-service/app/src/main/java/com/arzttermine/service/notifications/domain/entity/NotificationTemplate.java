package com.arzttermine.service.notifications.domain.entity;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(
    name = "notification_templates"
)
public class NotificationTemplate
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false, insertable = false)
    private long id;

    @Column(name = "path", nullable = false)
    private String templatePath;

    @Column(nullable = false)
    private String subject;

    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    @CollectionTable(
        name = "notification_template_params",
        joinColumns = @JoinColumn(name = "template_id", nullable = false)
    )
    private List<String> requiredParams = new ArrayList<>();

    public long getId() {
        return id;
    }

    public String getTemplatePath() {
        return templatePath;
    }

    public void setTemplatePath(String templatePath) {
        this.templatePath = templatePath;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public List<String> getRequiredParams() {
        return requiredParams;
    }

}
