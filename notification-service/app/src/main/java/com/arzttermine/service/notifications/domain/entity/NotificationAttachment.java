package com.arzttermine.service.notifications.domain.entity;

import javax.persistence.*;

@Entity
@Table( name = "notification_task_attachments" )
public class NotificationAttachment {

    @Id
    @Column(nullable = false, updatable = false, insertable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String bucketName;

    @Column(nullable = false)
    private String attachmentKey;

    @Column(nullable = false)
    private String mimetype;

    protected NotificationAttachment() {

    }

    public NotificationAttachment(String attachmentKey, String bucketName, String mimetype) {
        this.bucketName = bucketName;
        this.attachmentKey = attachmentKey;
        this.mimetype = mimetype;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getAttachmentKey() {
        return attachmentKey;
    }

    public void setAttachmentKey(String attachmentKey) {
        this.attachmentKey = attachmentKey;
    }

    public String getMimetype() {
        return mimetype;
    }

    public void setMimetype(String mimetype) {
        this.mimetype = mimetype;
    }
}
