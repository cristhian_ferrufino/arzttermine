package com.arzttermine.service.notifications.config;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.arzttermine.library.suport.AmazonStorage;
import com.arzttermine.library.suport.DateFormatterUtil;
import com.arzttermine.library.suport.templating.TemplateContentTransformer;
import com.arzttermine.library.suport.templating.TemplateTransformer;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.extras.java8time.dialect.Java8TimeDialect;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Clock;
import java.util.TimeZone;

@Configuration
public class ComponentConfiguration
{
    @Bean
    public ObjectMapper objectMapper()
    {
        final ObjectMapper mapper = new ObjectMapper();

        DateFormat dateFormat = new SimpleDateFormat(DateFormatterUtil.applicationFormat);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        mapper.setDateFormat(dateFormat);

        mapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.PUBLIC_ONLY); // do not return private properties
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL); // ignore null values in response
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

        return mapper;
    }

    @Bean
    public Clock clock() {
        return Clock.systemUTC();
    }

    @Bean
    public TemplateEngine templateEngine(TemplateEngine templateEngine) {
        templateEngine.addDialect(new Java8TimeDialect());
        return templateEngine;
    }

    @Bean
    @Profile("!testing")
    public TemplateContentTransformer templateContentTransformer(TemplateEngine templateEngine) {
        return new TemplateTransformer(templateEngine(templateEngine));
    }


    @Value("${cloud.aws.storage.accessKey}")
    private String storageAccessKey;

    @Value("${cloud.aws.storage.secretKey}")
    private String storageSecretKey;

    @Value("${cloud.aws.storage.region}")
    private String storageRegion;

    @Value("${cloud.aws.storage.defaultBucket}")
    private String storageDefaultBucket;

    @Bean
    public AmazonStorage amazonStorage() {
        AmazonS3 s3Client = AmazonS3ClientBuilder
            .standard()
            .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(storageAccessKey, storageSecretKey)))
            .withRegion(storageRegion)
            .build();
        return new AmazonStorage(s3Client, storageDefaultBucket);
    }

}
