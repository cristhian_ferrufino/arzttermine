package com.arzttermine.service.notifications;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableScheduling
@EnableJpaRepositories
@EnableTransactionManagement
@ComponentScans({
    @ComponentScan("com.arzttermine.service.notifications"),
    @ComponentScan("com.arzttermine.library.spring"),
    @ComponentScan("org.thymeleaf")
})
public class Application
{
    public static void main(String[] args) {
        new SpringApplicationBuilder()
            .sources(Application.class)
            .run(args);
    }
}
