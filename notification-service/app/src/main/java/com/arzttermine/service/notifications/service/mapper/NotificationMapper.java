package com.arzttermine.service.notifications.service.mapper;

import com.arzttermine.service.notifications.domain.entity.NotificationDirectory;
import com.arzttermine.service.notifications.domain.entity.NotificationTask;
import com.arzttermine.service.notifications.domain.entity.NotificationTemplate;
import com.arzttermine.service.notifications.web.response.NotificationDto;
import com.arzttermine.service.notifications.web.struct.NotificationRequestDto;
import com.arzttermine.service.notifications.web.struct.NotificationType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import java.time.Clock;
import java.time.Instant;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class NotificationMapper
{
    private final Clock clock;

    @Autowired
    private NotificationAttachmentMapper attachmentMapper;

    @Autowired
    public NotificationMapper(Clock clock) {
        this.clock = clock;
    }

    /**
     * Creates based on the request and the template the corresponding {@link NotificationTask}s.
     * A directory can contain multiple forwarding to send the notification to. Null or an empty list of {@code directories} only sends the message to the requested target.
     * If {@code directories} are provided, we will generate a task for each forwarding.
     *
     * <b>Please note:</b> We still do not provide BCC or CC options, as each task created must be send independent on the others.
     *
     * @param request NotificationRequestDto
     * @param template NotificationTemplate
     * @param directories Collection<NotificationDirectory>
     * @return Collection<NotificationTask>
     */
    public Collection<NotificationTask> fromRequest(final NotificationRequestDto request, final NotificationTemplate template, final @Nullable Collection<NotificationDirectory> directories)
    {
        if (null == directories || directories.isEmpty()) {
            return generateTaskForEachRecipient(request, template, null);
        }

        final List<NotificationTask> createdTasks = directories.stream()
            .filter(d -> d.getType().equals(request.getType()))
            .map(d -> generateTaskForEachRecipient(request, template, d))
            .flatMap(Collection::stream)
            .collect(Collectors.toList());

        if (null != request.getTarget() && !request.getTarget().isEmpty()) {
            // adding a task to send to the original target
            createdTasks.add(generateTask(request, template, request.getTarget()));
        }

        return createdTasks;
    }

    public NotificationDto toDto(final NotificationTask task)
    {
        final NotificationDto dto = new NotificationDto();
        dto.setSendAt(Optional.ofNullable(task.getSendAt()).map(Date::from).orElse(null));
        dto.setExpectedAt(Date.from(task.getExpectedAt()));
        dto.setTemplateId(task.getTemplate().getId());
        dto.setRecipient(task.getRecipient());
        dto.setInvalid(task.isInvalid());
        dto.setType(task.getType());

        return dto;
    }

    private Collection<NotificationTask> generateTaskForEachRecipient(final NotificationRequestDto request, final NotificationTemplate template, final NotificationDirectory directory)
    {
        if (null == directory) {
            return Collections.singletonList(generateTask(request, template, request.getTarget()));
        }

        return directory.getForwarding().stream()
            .map(target -> generateTask(request, template, target))
            .collect(Collectors.toList());
    }

    private NotificationTask generateTask(final NotificationRequestDto request, final NotificationTemplate template, final String target)
    {
        final NotificationTask task = new NotificationTask();
        task.setExpectedAt(Optional.ofNullable(request.getExpectedAt()).map(Date::toInstant).orElse(Instant.now(clock)));
        task.setCustomerId(request.getCustomerId());
        task.setLanguage(request.getLanguage());
        task.setType(request.getType());
        task.setTemplate(template);
        task.setRecipient(target);

        task.getParams().putAll(request.getParams().entrySet()
            .stream().filter(e -> null != e.getValue()).collect(Collectors.toMap(Map.Entry::getKey, v -> String.valueOf(v.getValue()))));

        // TODO: Move this elsewhere. Its not efficient.
        if (request.getType() == NotificationType.EMAIL && request.getAttachments() != null)
            task.setAttachments(attachmentMapper.fromDtos(request.getAttachments()));

        return task;
    }
}
