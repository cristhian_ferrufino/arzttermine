package com.arzttermine.service.notifications.service;

import com.arzttermine.library.suport.exception.EntityNotFoundException;
import com.arzttermine.library.suport.exception.InvalidArgumentException;
import com.arzttermine.library.web.dto.response.paging.PageDto;
import com.arzttermine.service.notifications.domain.entity.NotificationDirectory;
import com.arzttermine.service.notifications.domain.entity.NotificationTask;
import com.arzttermine.service.notifications.domain.entity.QNotificationTask;
import com.arzttermine.service.notifications.domain.entity.NotificationTemplate;
import com.arzttermine.service.notifications.domain.reposiory.NotificationDirectoryRepository;
import com.arzttermine.service.notifications.domain.reposiory.NotificationTaskRepository;
import com.arzttermine.service.notifications.domain.reposiory.NotificationTemplateRepository;
import com.arzttermine.service.notifications.service.event.model.NotificationCreatedEvent;
import com.arzttermine.service.notifications.service.mapper.NotificationMapper;
import com.arzttermine.service.notification.messages.notification.NotificationCancellationRequestDto;
import com.arzttermine.service.notifications.web.response.NotificationsPage;
import com.arzttermine.service.notifications.web.struct.NotificationRequestDto;
import com.querydsl.core.BooleanBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class NotificationService
{
    private static final Logger log = LoggerFactory.getLogger(NotificationService.class);

    @Autowired
    private NotificationMapper mapper;

    @Autowired
    private NotificationTaskRepository taskRepository;

    @Autowired
    private NotificationTemplateRepository templateRepository;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private NotificationDirectoryRepository directoryRepository;

    @Transactional(readOnly = false)
    public void createTask(final NotificationRequestDto request)
    {
        final NotificationTemplate template = templateRepository.findOne(request.getTemplate().getTemplateId());

        if (null == template) {
            throw new EntityNotFoundException("NotificationTemplate", request.getTemplate());
        }

        final List<String> validParams = request.getParams().entrySet().stream()
            .filter(e -> null != e.getValue())
            .map(Map.Entry::getKey)
            .collect(Collectors.toList());

        if (!validParams.containsAll(template.getRequiredParams())) {
            throw new InvalidArgumentException("requiredParameters", "requested template requires the following parameters: " + template.getRequiredParams().stream()
                .collect(Collectors.joining(",")));
        }

        final Collection<NotificationDirectory> directories = directoryRepository.findApplicable(
            request.getParams().keySet(),
            request.getParams().values().stream().map(String::valueOf).collect(Collectors.toList()),
            request.getType()
        );

        final Collection<NotificationTask> tasks = mapper.fromRequest(request, template, directories.stream()
                .filter(nd -> nd.getBindings().entrySet().stream()
                    .filter(b -> request.getParams().containsKey(b.getKey()))
                    .filter(b -> String.valueOf(request.getParams().get(b.getKey())).equals(b.getValue()))
                    .count() > 0)
                .filter(nd -> nd.getTopics().contains(request.getTemplate()))
                .collect(Collectors.toList()))
            .stream()
            .filter(n -> null != n.getRecipient())
            .collect(Collectors.toList());

        if (tasks.isEmpty()) {
            throw new IllegalStateException("no tasks are created. seems like the specified directory is empty");
        }

        log.info("trying to create {} tasks using template {}", tasks.size(), request.getTemplate());

        taskRepository.save(tasks);
        eventPublisher.publishEvent(NotificationCreatedEvent.of(tasks));
    }

    @Transactional(readOnly = false)
    public void cancelTasks(final List<NotificationCancellationRequestDto> cancelRequests) {


        for (NotificationCancellationRequestDto cancelRequest : cancelRequests) {
            BooleanBuilder builder = new BooleanBuilder();
            String logMessageCriteria = new String();

            if (cancelRequest.getTemplate() != null) {
                builder.and(QNotificationTask.notificationTask.template.id.eq(cancelRequest.getTemplate().getTemplateId()));
                logMessageCriteria += " TemplateId: " + cancelRequest.getTemplate().getTemplateId();
            }

            if (cancelRequest.getLanguage() != null) {
                builder.and(QNotificationTask.notificationTask.language.eq(cancelRequest.getLanguage()));
                logMessageCriteria += " Language: " + cancelRequest.getLanguage().toString();
            }

            if (cancelRequest.getType() != null) {
                builder.and(QNotificationTask.notificationTask.type.eq(cancelRequest.getType()));
                logMessageCriteria += " Type: " + cancelRequest.getType().toString();
            }

            if (cancelRequest.getRecipient() != null) {
                builder.and(QNotificationTask.notificationTask.recipient.eq(cancelRequest.getRecipient()));
                logMessageCriteria += " Recipient: " + cancelRequest.getRecipient();
            }

            for (String key : cancelRequest.getParams().keySet()) {
                builder.and(QNotificationTask.notificationTask.params.containsKey(key));
                Object value = cancelRequest.getParams().get(key);
                builder.and(QNotificationTask.notificationTask.params.containsValue(value.toString()));
                logMessageCriteria += " Param: " + key + "/" + value.toString();
            }

            if (logMessageCriteria.isEmpty())
                log.info("No criteria to use for message cancellation. Ignoring.");
            else {
                log.info("Cancelling messages with criteria : " + logMessageCriteria);
                Iterable<NotificationTask> tasks = taskRepository.findAll(builder.getValue());

                // Should really send the iterable to the repository, but we need to log individually....
                tasks.forEach(task -> {
                    log.info("Cancelling message - Id: {}, Path: {} ", task.getId(), task.getPath());
                    taskRepository.delete(task);
                });
            }
        }

    }

    @Transactional(readOnly = true)
    public NotificationsPage get(final Pageable pageable)
    {
        final Page<NotificationTask> notifications = taskRepository.findAll(pageable);

        final PageDto page = new PageDto();
        page.setTotalItems(notifications.getTotalElements());
        page.setTotalPages(notifications.getTotalPages());
        page.setCurrentPage(pageable.getPageNumber());
        page.setPageSize(pageable.getPageSize());

        final NotificationsPage result = new NotificationsPage();
        result.setPage(page);
        result.setElements(notifications.getContent().stream().map(mapper::toDto).collect(Collectors.toList()));

        return result;
    }
}
