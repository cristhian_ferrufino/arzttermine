package com.arzttermine.service.notifications.service.processor.strategy;

import com.arzttermine.service.notifications.domain.entity.NotificationTask;
import com.arzttermine.service.notifications.web.struct.NotificationType;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public interface NotificationStrategy
{
    @Transactional(propagation = Propagation.REQUIRED)
    boolean send(NotificationTask task);

    boolean supports(NotificationType type);
}
