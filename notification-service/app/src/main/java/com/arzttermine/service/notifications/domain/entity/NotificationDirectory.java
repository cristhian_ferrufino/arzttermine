package com.arzttermine.service.notifications.domain.entity;

import com.arzttermine.service.notifications.web.struct.Notification;
import com.arzttermine.service.notifications.web.struct.NotificationType;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Type;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyColumn;
import javax.persistence.Table;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.UUID;

@Entity
@Table(name = "notification_directory")
public class NotificationDirectory
{
    @Id
    @Type(type = "uuid-char")
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(nullable = false, updatable = false, insertable = false)
    // using uuid here to make it harder to identify a single entity
    // as we can't guarantee certain security grants on this
    private UUID uuid;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, updatable = false)
    private NotificationType type;

    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    @CollectionTable(
        name = "notifications_forwarding",
        joinColumns = @JoinColumn(name = "directory_id", nullable = false)
    )
    private Collection<String> forwarding = new HashSet<>();

    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    @Column(name = "requires", updatable = false)
    @MapKeyColumn(name = "property", updatable = false)
    @CollectionTable(name = "directory_notification_bindings")
    private Map<String, String> bindings = new HashMap<>();

    @ElementCollection
    @Enumerated(EnumType.STRING)
    @LazyCollection(LazyCollectionOption.FALSE)
    @CollectionTable(
        name = "directory_notification_topics",
        joinColumns = @JoinColumn(name = "directory_id", nullable = false)
    )
    private Collection<Notification> topics = new HashSet<>();

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public UUID getUuid() {
        return uuid;
    }

    public Collection<String> getForwarding() {
        return forwarding;
    }

    public Map<String, String> getBindings() {
        return bindings;
    }

    public NotificationType getType() {
        return type;
    }

    public void setType(NotificationType type) {
        this.type = type;
    }

    public Collection<Notification> getTopics() {
        return topics;
    }
}
