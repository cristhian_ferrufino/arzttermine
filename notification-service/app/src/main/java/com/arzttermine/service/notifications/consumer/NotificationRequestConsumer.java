package com.arzttermine.service.notifications.consumer;

import com.arzttermine.library.messaging.consumer.Consumer;
import com.arzttermine.service.notification.messages.NotificationInitialisation;
import com.arzttermine.service.notifications.service.NotificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class NotificationRequestConsumer
{
    private static final Logger log = LoggerFactory.getLogger(NotificationRequestConsumer.class);

    @Autowired
    private NotificationService notificationService;

    @Consumer(routingKey = "notification.request.*")
    public void notificationRequestMessage(NotificationRequestImpl message)
    {
        final String task = message.getClass().getSimpleName();

        log.info("got notification request {}, trying to register notification task for {}", message.getTopicValue(), task);
        notificationService.createTask(message.request);
    }

    // this helper class provides a generic message type to consume notification requests
    public static final class NotificationRequestImpl extends NotificationInitialisation
    {
        private String topicValue;

        @Override
        public String getTopicValue() {
            return topicValue;
        }

        public void setTopicValue(String topicValue) {
            this.topicValue = topicValue;
        }
    }
}
