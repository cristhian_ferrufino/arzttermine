package com.arzttermine.service.notifications.service.processor.templating;

import com.arzttermine.library.suport.templating.exchange.UrlRequest;
import com.arzttermine.service.notifications.config.settings.RemoteUrlSettings;
import com.arzttermine.service.notifications.domain.entity.NotificationTask;
import com.arzttermine.library.suport.templating.TemplateTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * This class is passed to the {@link TemplateTransformer}.
 * The transformer calls for example the {@code getParams} method.
 * You can override any method from {@link com.arzttermine.library.suport.templating.TemplateTask}
 *
 * {@link UrlRequest}
 */
public class ProcessedNotificationTask extends NotificationTask
{
    private static final Logger log = LoggerFactory.getLogger(ProcessedNotificationTask.class);

    private final RemoteUrlSettings urlSettings;

    public ProcessedNotificationTask(NotificationTask originalTask, RemoteUrlSettings urlSettings) {
        super(originalTask);
        this.urlSettings = urlSettings;
    }

    /**
     * We need to avoid updating the {@link NotificationTask} entity.
     * Therefore we need to copy the params before processing the values ({@see processValue}).
     *
     * @return copy of {@see super.getParams} with processed parameters
     */
    @Override
    public Map<String, String> getParams() {
        final Map<String, String> params = new HashMap<>(super.getParams().size());

        super.getParams().forEach((key, value) ->
            params.put(key, processValue(value)));

        return params;
    }

    private String processValue(String value) {
        if (UrlRequest.isUrlRequest(value)) {
            final UrlRequest request = UrlRequest.fromString(value);
            final String newValue = urlSettings.getByType(request.getType()).buildWith(request);

            if (log.isDebugEnabled() && !newValue.equals(value)) {
                log.debug("changing notification property value from {} to {}", value, newValue);
            }

            return newValue;
        } else {
            return value;
        }
    }
}
