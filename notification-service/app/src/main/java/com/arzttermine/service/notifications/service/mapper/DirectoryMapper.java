package com.arzttermine.service.notifications.service.mapper;

import com.arzttermine.service.notifications.domain.entity.NotificationDirectory;
import com.arzttermine.service.notifications.web.DirectoryDto;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.stream.Collectors;

@Component
public class DirectoryMapper
{
    public DirectoryDto toDto(final NotificationDirectory directory)
    {
        final DirectoryDto dto = new DirectoryDto();
        dto.getBindings().putAll(directory.getBindings());
        dto.setForwarding(directory.getForwarding());
        dto.setTopics(directory.getTopics());
        dto.setUuid(directory.getUuid());
        dto.setType(directory.getType());

        return dto;
    }

    public Collection<DirectoryDto> toDtos(final Collection<NotificationDirectory> directories)
    {
        return directories.stream().map(this::toDto).collect(Collectors.toList());
    }
}
