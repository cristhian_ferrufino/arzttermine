USE notification;

INSERT INTO `notification_templates` (`id`, `subject`, `path`) VALUES
    (8, 'Buchungsänderung', 'booking-moved');

INSERT INTO `notification_template_params` (`template_id`, `required_params`) VALUES
    (8, 'customerName'),
    (8, 'doctorName'),
    (8, 'start'),
    (8, 'location'),
    (8, 'zip'),
    (8, 'city'),
    (8, 'longitude'),
    (8, 'latitude'),
    (8, 'city'),
    (8, 'bookingId'),
    (8, 'googleMaps'),
    (8, 'treatmentType'),
    (8, 'practiceName');
