USE notification;

ALTER TABLE notification_tasks
    ADD COLUMN invalid bit(1) NOT NULL DEFAULT FALSE;
