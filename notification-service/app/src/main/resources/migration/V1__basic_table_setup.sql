USE notification;

CREATE TABLE `notification_templates` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `notification_tasks` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` bigint(20) unsigned DEFAULT NULL,
  `expected_at` datetime NOT NULL,
  `send` bit(1) NOT NULL DEFAULT FALSE,
  `send_at` datetime DEFAULT NULL,
  `type` varchar(56) NOT NULL,
  `template_id` bigint(20) unsigned NOT NULL,
  `language` varchar(255) NOT NULL,
  `recipient` varchar(256) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `id_notification_task_processed` (`send`),
  INDEX `id_notification_task_template_id` (`template_id`),
  CONSTRAINT `fk_notification_task_template_id` FOREIGN KEY (`template_id`) REFERENCES `notification_templates` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `notification_task_properties` (
  `notification_task_id` bigint(20) unsigned NOT NULL,
  `value` varchar(1024) DEFAULT NULL,
  `property` varchar(255) NOT NULL,
  PRIMARY KEY (`notification_task_id`, `property`),
  CONSTRAINT `fk_notification_task_property_task_id` FOREIGN KEY (`notification_task_id`) REFERENCES `notification_tasks` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `notification_template_params` (
  `template_id` bigint(20) unsigned NOT NULL,
  `required_params` varchar(255) NOT NULL,
  KEY `id_notification_template_param_template_id` (`template_id`),
  CONSTRAINT `fk_notification_template_param_template_id` FOREIGN KEY (`template_id`) REFERENCES `notification_templates` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

