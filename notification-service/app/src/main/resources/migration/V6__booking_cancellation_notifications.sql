USE notification;

INSERT INTO `notification_templates` (`id`, `subject`, `path`) VALUES
    (6, 'Buchungsstornierung', 'booking-cancellation');

INSERT INTO `notification_template_params` (`template_id`, `required_params`) VALUES
    (6, 'customerName'),
    (6, 'doctorName'),
    (6, 'start'),
    (6, 'location'),
    (6, 'zip'),
    (6, 'city'),
    (6, 'longitude'),
    (6, 'latitude'),
    (6, 'city'),
    (6, 'bookingId'),
    (6, 'googleMaps'),
    (6, 'treatmentType'),
    (6, 'practiceName');
