USE notification;

INSERT INTO `notification_templates` (`id`, `subject`, `path`) VALUES
    (7, 'Review Erinnerung', 'review-reminder');

INSERT INTO `notification_template_params` (`template_id`, `required_params`) VALUES
(7,'customerName'),
(7,'reviewUrl'),
(7,'startTime'),
(7,'doctorName'),
(7,'treatmentType'),
(7,'location'),
(7,'city'),
(7,'zip'),
(7,'practiceName'),
(7,'practiceMail'),
(7,'practicePhone'),
(7,'bookingId'),
(7,'googleMaps');

