USE notification;

INSERT INTO `notification_templates` (`id`, `subject`, `path`) VALUES
    (4, 'Neues Passwort', 'password-reset');

INSERT INTO `notification_template_params` (`template_id`, `required_params`) VALUES
    (4, 'resetLink'),
    (4, 'customerName');
