USE notification;

CREATE TABLE `notification_directory` (
  `uuid` varchar(255) NOT NULL,
  `type` varchar(56) NOT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `directory_notification_bindings` (
  `notification_directory_uuid` varchar(255) NOT NULL,
  `requires` varchar(255) DEFAULT NULL,
  `property` varchar(255) NOT NULL,
  PRIMARY KEY (`notification_directory_uuid`, `property`),
  CONSTRAINT `fk_property_binding_directory_id` FOREIGN KEY (`notification_directory_uuid`) REFERENCES `notification_directory` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `notifications_forwarding` (
  `directory_id` varchar(255) NOT NULL,
  `forwarding` varchar(255) NOT NULL,
  PRIMARY KEY (`directory_id`, `forwarding`),
  CONSTRAINT `fk_forwarding_directory_id` FOREIGN KEY (`directory_id`) REFERENCES `notification_directory` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `directory_notification_topics` (
  `directory_id` varchar(255) NOT NULL,
  `topics` varchar(128) NOT NULL,
  PRIMARY KEY (`directory_id`, `topics`),
  CONSTRAINT `fk_topic_binding_directory_id` FOREIGN KEY (`directory_id`) REFERENCES `notification_directory` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;


-- new notification to inform doctors about bookings
INSERT INTO `notification_templates` (`id`, `subject`, `path`) VALUES
    (5, 'Neue Buchung', 'booking-created');

INSERT INTO `notification_template_params` (`template_id`, `required_params`) VALUES
    (5, 'start'),
    (5, 'end'),
    (5, 'duration'),
    (5, 'treatmentType'),
    (5, 'customerName'),
    (5, 'customerMail'),
    (5, 'customerPhone'),
    (5, 'returningPatient'),
    (5, 'insurance'),
    (5, 'doctorName'),
    (5, 'bookingId'),
    (5, 'practiceId'),
    (5, 'practiceName'),
    (5, 'city');
