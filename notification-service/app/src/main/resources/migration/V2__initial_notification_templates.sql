USE notification;

INSERT INTO `notification_templates` (`id`, `subject`, `path`) VALUES
    (1, 'Buchungserinnerung', 'booking-reminder'),
    (2, 'Buchungsbestätigung', 'booking-confirmation'),
    (3, 'Rechnung', 'transaction-invoice');

INSERT INTO `notification_template_params` (`template_id`, `required_params`) VALUES
    (1, 'customer'),
    (1, 'start'),
    (1, 'doctor'),
    (1, 'location'),
    (1, 'zip'),
    (1, 'city'),
    (1, 'longitude'),
    (1, 'latitude'),
    (1, 'city'),
    (1, 'created'),
    (1, 'bookingId'),
    (1, 'googleMaps'),
    (1, 'treatmentType'),
    (1, 'practiceName'),

    (2, 'customer'),
    (2, 'start'),
    (2, 'doctor'),
    (2, 'location'),
    (2, 'zip'),
    (2, 'city'),
    (2, 'longitude'),
    (2, 'latitude'),
    (2, 'city'),
    (2, 'created'),
    (2, 'bookingId'),
    (2, 'googleMaps'),
    (2, 'treatmentType'),
    (2, 'practiceName'),

    (3, 'bookingId'),
    (3, 'created'),
    (3, 'transactionId'),
    (3, 'transactionConfirmedAt'),
    (3, 'price'),
    (3, 'currency'),
    (3, 'payerFullName'),
    (3, 'payerEmailAddress');
