USE notification;

CREATE TABLE `notification_task_attachments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `notification_task_id` bigint(20) unsigned NOT NULL,
  `attachment_key` varchar(1024) NOT NULL,
  `bucket_name` varchar(63) NOT NULL,
  `mimetype` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_notification_task_attachment_key_id` FOREIGN KEY (`notification_task_id`) REFERENCES `notification_tasks` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;


