[![Build Status](https://travis-ci.com/arzttermine/notification-service.svg?token=XHsxG8wHND1xUrHu4SwY&branch=master)](https://travis-ci.com/arzttermine/notification-service)

# notification-service

- Want to know how this service is [processing notifications](doc/about.md) for you?  
- Using directories to [broadcast messages](doc/directories.md)

### API Doc
[Swagger-UI](http://localhost:15400/swagger-ui.html)  
Make sure you are using the correct host. Swagger is not enabled during production or testing mode.


### Documentation
[Read more](https://github.com/arzttermine/documentation/wiki/Java-in-Detail) about how to manage java applications
