#!/bin/bash

DIR="$(dirname "${BASH_SOURCE[0]}")"
source ${DIR}/env

/opt/service/make_jar_service.sh ${SERVICE_NAME}

ERROR_CODE=$?
if [ ${ERROR_CODE} != 0 ]; then
    echo "Could not create initial service script. See details above. Aborting."
    exit ${ERROR_CODE}
fi

/etc/init.d/${SERVICE_NAME} start
