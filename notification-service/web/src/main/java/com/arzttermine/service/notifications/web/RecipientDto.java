package com.arzttermine.service.notifications.web;

import com.arzttermine.service.notifications.web.struct.NotificationType;

import java.util.EnumMap;

public class RecipientDto extends EnumMap<NotificationType, String>
{
    public RecipientDto() {
        super(NotificationType.class);
    }
}
