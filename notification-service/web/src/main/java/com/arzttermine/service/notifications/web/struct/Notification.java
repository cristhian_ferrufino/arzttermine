package com.arzttermine.service.notifications.web.struct;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;

public enum Notification
{
    BOOKING_REMINDER(1L),
    BOOKING_CONFIRMATION(2L),
    BOOKING_CREATED(5L),
    BOOKING_CANCELLED(6L),
    BOOKING_MOVED(8L),
    REVIEW_REMINDER(7L),
    TRANSACTION_INVOICE(3L),
    PASSWORD_RESET(4L);

    public static final Collection<Notification> topics = Arrays.asList(Notification.values());

    private long templateId;

    Notification(long templateId) {
        this.templateId = templateId;
    }

    public static Optional<Notification> fromTemplateId(long id) {
        return topics.stream()
            .filter(t -> t.getTemplateId() == id)
            .findFirst();
    }

    public long getTemplateId() {
        return templateId;
    }
}
