package com.arzttermine.service.notifications.web.struct;

import com.arzttermine.library.web.struct.Language;
import com.arzttermine.service.notifications.web.request.NotificationAttachmentDto;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.*;

public class NotificationRequestDto
{
    private Long customerId;

    @NotNull
    @ApiModelProperty("template type which should be used to deliver the content")
    private Notification template;

    @NotEmpty
    @ApiModelProperty("templates may provide multiple ways to deliver notifications, choose the one here")
    private NotificationType type;

    @ApiModelProperty("the desired time of the notification, or even null if instant delivering is desired")
    private Date expectedAt;

    @NotNull
    private Language language = Language.GERMAN;

    @NotNull
    @ApiModelProperty("templates requires certain parameters, please provide them all here")
    private Map<String, Object> params = new HashMap<>();

    @NotNull
    @ApiModelProperty("may contains a phone number or email address")
    private String target;

    @ApiModelProperty("List of attachments to get from Amazon S3 to attach to this notification.")
    private List<NotificationAttachmentDto> attachments;

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Notification getTemplate() {
        return template;
    }

    public void setTemplate(Notification template) {
        this.template = template;
    }

    public NotificationType getType() {
        return type;
    }

    public void setType(NotificationType type) {
        this.type = type;
    }

    public Date getExpectedAt() {
        return expectedAt;
    }

    public void setExpectedAt(Date expectedAt) {
        this.expectedAt = expectedAt;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public List<NotificationAttachmentDto> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<NotificationAttachmentDto> attachments) {
        this.attachments = attachments;
    }

    public void addAttachment(NotificationAttachmentDto attachment) {
        if (null == this.attachments)
            attachments = new ArrayList<>();

        attachments.add(attachment);
    }

}
