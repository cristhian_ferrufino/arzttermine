package com.arzttermine.service.notifications.web.struct;

public enum NotificationType
{
    EMAIL,
    SMS,
    WHATSAPP,
}
