package com.arzttermine.service.notifications.web.request;

import com.arzttermine.service.notifications.web.DirectoryDto;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

public class DirectoryPatchDto
{
    @Valid
    @NotEmpty
    private List<DirectoryDto> directories = new ArrayList<>();

    public List<DirectoryDto> getDirectories() {
        return directories;
    }

    public void setDirectories(List<DirectoryDto> directories) {
        this.directories = directories;
    }
}
