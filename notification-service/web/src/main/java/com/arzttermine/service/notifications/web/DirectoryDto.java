package com.arzttermine.service.notifications.web;

import com.arzttermine.service.notifications.web.struct.Notification;
import com.arzttermine.service.notifications.web.struct.NotificationType;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.UUID;

public class DirectoryDto
{
    // only for response
    private UUID uuid;

    @NotEmpty
    private Map<String, Object> bindings = new HashMap<>();

    @NotEmpty
    private Collection<String> forwarding = new HashSet<>();

    @NotNull
    private Collection<Notification> topics = new ArrayList<>();

    @NotNull
    private NotificationType type;

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public Map<String, Object> getBindings() {
        return bindings;
    }

    public void setBindings(Map<String, Object> bindings) {
        this.bindings = bindings;
    }

    public Collection<String> getForwarding() {
        return forwarding;
    }

    public void setForwarding(Collection<String> forwarding) {
        this.forwarding = forwarding;
    }

    public NotificationType getType() {
        return type;
    }

    public void setType(NotificationType type) {
        this.type = type;
    }

    public Collection<Notification> getTopics() {
        return topics;
    }

    public void setTopics(Collection<Notification> topics) {
        this.topics = topics;
    }
}
