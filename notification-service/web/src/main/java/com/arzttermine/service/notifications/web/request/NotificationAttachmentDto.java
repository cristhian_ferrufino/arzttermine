package com.arzttermine.service.notifications.web.request;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

public class NotificationAttachmentDto {

    @NotNull
    @ApiModelProperty("Unique identifier for the attachment within specified Amazon S3 bucket.")
    private String attachmentKey;

    @NotNull
    @ApiModelProperty("Unique identifier for the Amazon S3 bucket containing the attachment.")
    private String bucketName;

    @NotNull
    @ApiModelProperty("Mimetype in the form 'typename/subtypename' (see RFCs 4288 & 6838)")
    private String mimetype;

    public String getAttachmentKey() {
        return attachmentKey;
    }

    public void setAttachmentKey(String attachmentKey) {
        this.attachmentKey = attachmentKey;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getMimetype() {
        return mimetype;
    }

    public void setMimetype(String mimetype) {
        this.mimetype = mimetype;
    }
}
