package com.arzttermine.service.notification.messages.templating;
import com.arzttermine.service.profile.web.dto.response.ReviewSiteDto;

public class ReviewSiteTemplate {

    public static String getUrl(ReviewSiteDto reviewSite) {

        String reviewUrl;

        // TODO: ENUM
        switch (reviewSite.getType()) {
            case ("google") :
                // https://support.google.com/business/answer/7035772?hl=en
                reviewUrl = "https://search.google.com/local/writereview?placeid=" + reviewSite.getData();
                break;
            case ("jameda") :
                reviewUrl = reviewSite.getData();
                break;
            default:
                reviewUrl = reviewSite.getData();
                break;
        }

        return reviewUrl;
    }
}