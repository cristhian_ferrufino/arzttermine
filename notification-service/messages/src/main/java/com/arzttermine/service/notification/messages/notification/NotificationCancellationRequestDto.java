package com.arzttermine.service.notification.messages.notification;

import com.arzttermine.library.web.struct.Language;
import com.arzttermine.service.notification.messages.NotificationInitialisation;
import com.arzttermine.service.notifications.web.struct.Notification;
import com.arzttermine.service.notifications.web.struct.NotificationType;

import java.util.HashMap;
import java.util.Map;

public class NotificationCancellationRequestDto {

    private Map<String, Object> params = new HashMap<>();

    private Language notificationLanguage;

    private NotificationType notificationType;

    private Notification template;

    private String recipient;


    public NotificationCancellationRequestDto(NotificationInitialisation message) {
        this.setTemplate(message.request.getTemplate());
        this.setType(message.request.getType());
        this.setLanguage(message.request.getLanguage());
        this.setRecipient(message.request.getTarget());
    }

    public NotificationCancellationRequestDto() {

    }


    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    public Notification getTemplate() {
        return this.template;
    }

    public void setTemplate(Notification template) {
        this.template = template;
    }

    public Language getLanguage() {
        return notificationLanguage;
    }

    public void setLanguage(Language notificationLanguage) {
        this.notificationLanguage = notificationLanguage;
    }

    public NotificationType getType() {
        return notificationType;
    }

    public void setType(NotificationType notificationType) {
        this.notificationType = notificationType;
    }

    public String getRecipient() {
        return this.recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

}
