package com.arzttermine.service.notification.messages.templating;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.Map;

public class GoogleUrlShortener {

    private final static Logger logger = LoggerFactory.getLogger(GoogleUrlShortener.class);

    /**
     * that makes pandas sad. But atm we are not able to send the shortener request somewhere else
     * therefore we cant make use of our {@link com.arzttermine.service.notification.messages.UrlRequestDto}
     * @TODO make it work without hardcoded key
     */
    private final static String googleKey = "AIzaSyClQh-x5V3jTQz8EffGDUertB6Uc1P3YU4";

    public static String getShortUrl(String longUrl) {

        String address = "https://www.googleapis.com/urlshortener/v1/url?key="+googleKey;
        String jsonString = "{\"longUrl\": \""+longUrl+"\"}";
        String shortUrl = longUrl;
        HttpPost httpPost;
        HttpClient httpClient;

        try {
            httpClient = HttpClientBuilder.create().build();
            httpPost = new HttpPost(address);

            httpPost.setEntity(new StringEntity(jsonString));
            httpPost.setHeader("Content-Type", "application/json");
            HttpResponse httpResponse = httpClient.execute(httpPost);

            HttpEntity httpEntity = httpResponse.getEntity();
            InputStream responseStream = httpEntity.getContent();

            String responseBody = CharStreams.toString(new InputStreamReader(responseStream, Charsets.ISO_8859_1));

            ObjectMapper objectMapper = new ObjectMapper();
            Map<String, String> responseMap = objectMapper.readValue(responseBody, new TypeReference<Map<String, String>>(){});

            shortUrl = responseMap.get("id");

        } catch (IOException e) {
            logger.error("Google Url Shortening Failed: ", e);
        }
        return shortUrl;
    }

}
