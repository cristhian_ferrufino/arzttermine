package com.arzttermine.service.notification.messages;

import com.arzttermine.library.messaging.model.TopicMessage;
import com.arzttermine.library.web.struct.Language;
import com.arzttermine.service.notifications.web.struct.Notification;
import com.arzttermine.service.notifications.web.struct.NotificationRequestDto;
import com.arzttermine.service.notifications.web.struct.NotificationType;

import java.util.Collection;
import java.util.Collections;

public abstract class NotificationInitialisation extends TopicMessage
{
    public final NotificationRequestDto request = new NotificationRequestDto();

    protected NotificationInitialisation() {
    }

    public NotificationInitialisation(Notification template, NotificationType type, Long customerId, String target, Language language) {
        request.setCustomerId(customerId);
        request.setTemplate(template);
        request.setLanguage(language);
        request.setTarget(target);
        request.setType(type);
    }

    protected NotificationInitialisation(Notification template, NotificationType type, String target, Language language) {
        this(template, type, null, target, language);
    }

    public Collection<String> getCancellationParameters() {
        return Collections.emptyList();
    }

    @Override
    public String getTopicKind() {
        return "notification";
    }

    @Override
    public String getTopic() {
        return "request";
    }

    @Override
    public abstract String getTopicValue();
}
