package com.arzttermine.service.notification.messages.booking;

import com.arzttermine.library.suport.templating.exchange.UrlRequest;
import com.arzttermine.library.web.dto.PriceDto;
import com.arzttermine.library.web.struct.InsuranceType;
import com.arzttermine.library.web.struct.Language;
import com.arzttermine.service.notification.messages.NotificationInitialisation;
import com.arzttermine.service.notifications.web.struct.Notification;
import com.arzttermine.service.notifications.web.struct.NotificationType;

import java.time.Instant;
import java.util.Optional;

public final class BookingCreatedMessage extends NotificationInitialisation
{
    public BookingCreatedMessage() {
    }

    public BookingCreatedMessage(CreatedBag bag, NotificationType type, String target, Language language) {

        super(Notification.BOOKING_CREATED, type, target, language);
        request.getParams().put("customerName", bag.customerName);
        request.getParams().put("customerMail", bag.customerMail);
        request.getParams().put("customerPhone", bag.customerPhone);
        request.getParams().put("returningPatient", bag.returningPatient);
        request.getParams().put("insurance", bag.insuranceType.name());
        request.getParams().put("start", bag.start.getEpochSecond() * 1000);
        request.getParams().put("end", bag.end.getEpochSecond() * 1000);
        request.getParams().put("duration", Math.round(bag.end.minusSeconds(bag.start.getEpochSecond()).getEpochSecond() / 60));
        request.getParams().put("bookingId", bag.bookingId);
        request.getParams().put("treatmentType", bag.treatmentType);
        request.getParams().put("doctorName", bag.doctorName);
        request.getParams().put("practiceName", bag.practiceName);
        request.getParams().put("city", bag.city);

        if (bag.price != null) {
            request.getParams().put("paymentCost", bag.price.getAmount());
            request.getParams().put("paymentType", bag.paymentType);
            request.getParams().put("paymentTax", 0);
            request.getParams().put("paymentTotal", bag.price.getAmount());
            request.getParams().put("paymentSuccess", bag.paymentSuccess);
        }

        request.getParams().put("practiceId", bag.practiceId);
        request.getParams().put("notificationType", Notification.BOOKING_CREATED.name());

        Optional.ofNullable(bag.cancellationUrl)
            .ifPresent(url -> request.getParams().put("cancellationUrl", url.toString()));
    }

    @Override
    public String getTopicValue() {
        return "booking-created";
    }

    public static final class CreatedBag
    {
        public Instant start;
        public Instant end;
        public long bookingId;
        public long practiceId;
        public String practiceName;
        public String city;
        public String customerName;
        public String customerMail;
        public String customerPhone;
        public String doctorName;
        public String treatmentType;
        public String paymentType;
        public PriceDto price;
        public InsuranceType insuranceType;
        public boolean returningPatient;
        public boolean paymentSuccess;
        public UrlRequest cancellationUrl;
    }
}
