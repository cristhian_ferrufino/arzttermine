package com.arzttermine.service.notification.messages.booking;

import com.arzttermine.library.web.dto.PriceDto;
import com.arzttermine.library.web.struct.Language;
import com.arzttermine.service.notification.messages.NotificationInitialisation;
import com.arzttermine.service.notification.messages.templating.GoogleMapsTemplate;
import com.arzttermine.service.notifications.web.struct.Notification;
import com.arzttermine.service.notifications.web.struct.NotificationType;
import com.arzttermine.service.profile.web.dto.LocationDto;

import java.time.Instant;

public final class BookingConfirmationMessage extends NotificationInitialisation
{
    public BookingConfirmationMessage() {
    }

    public BookingConfirmationMessage(ConfirmationBag bag, NotificationType type, String target, Language language) {
        super(Notification.BOOKING_CONFIRMATION, type, bag.customerId, target, language);
        request.getParams().put("customer", bag.customerName);
        request.getParams().put("start", bag.start.getEpochSecond() * 1000);
        request.getParams().put("created", Instant.now().getEpochSecond() * 1000);
        request.getParams().put("bookingId", bag.bookingId);
        request.getParams().put("treatmentType", bag.treatmentType);
        request.getParams().put("doctor", bag.doctorName);
        request.getParams().put("practiceName", bag.practiceName);
        request.getParams().put("practiceMail", bag.practiceLocation.getContactMail());
        request.getParams().put("practicePhone", bag.practiceLocation.getPhone());
        request.getParams().put("location", bag.practiceLocation.getLocation());
        request.getParams().put("city", bag.practiceLocation.getCity());
        request.getParams().put("zip", bag.practiceLocation.getZip());
        request.getParams().put("longitude", bag.practiceLocation.getLongitude());
        request.getParams().put("latitude", bag.practiceLocation.getLatitude());
        request.getParams().put("googleMaps", GoogleMapsTemplate.getMap(bag.practiceLocation));

        if (null != bag.totalPrice) {
            request.getParams().put("paymentFailed", !bag.paymentInitialised); // if not initialised, it failed
        }
    }

    @Override
    public String getTopicValue() {
        return "booking-confirmation";
    }

    public static final class ConfirmationBag
    {
        public Instant start;
        public long bookingId;
        public long customerId;
        public String customerName;
        public String doctorName;
        public String practiceName;
        public String treatmentType;
        public LocationDto practiceLocation;
        public boolean paymentInitialised;
        public PriceDto totalPrice;
    }
}
