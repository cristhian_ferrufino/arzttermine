package com.arzttermine.service.notification.messages.booking;

import com.arzttermine.library.web.struct.Language;
import com.arzttermine.service.notification.messages.NotificationInitialisation;
import com.arzttermine.service.notification.messages.templating.GoogleMapsTemplate;
import com.arzttermine.service.notification.messages.templating.GoogleUrlShortener;
import com.arzttermine.service.notifications.web.struct.Notification;
import com.arzttermine.service.notifications.web.struct.NotificationType;
import com.arzttermine.service.profile.web.dto.LocationDto;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public final class ReviewReminderMessage extends NotificationInitialisation
{
    public ReviewReminderMessage() {
    }

    public ReviewReminderMessage(ReviewReminderBag bag, NotificationType type, String target, Language language) {

        super(Notification.REVIEW_REMINDER, type, bag.customerId, target, language);

        request.setExpectedAt(Date.from(bag.start.plus(bag.sendAfterMinutes, ChronoUnit.MINUTES)));
        request.getParams().put("customerName", bag.customerName);
        request.getParams().put("reviewUrl", GoogleUrlShortener.getShortUrl(bag.reviewUrl));
        request.getParams().put("startTime", bag.start.getEpochSecond() * 1000);
        request.getParams().put("doctorName", bag.doctorName);
        request.getParams().put("treatmentType", bag.treatmentType);
        request.getParams().put("location", bag.practiceLocation.getLocation());
        request.getParams().put("city", bag.practiceLocation.getCity());
        request.getParams().put("zip", bag.practiceLocation.getZip());
        request.getParams().put("practiceName", bag.practiceName);
        request.getParams().put("practiceMail", bag.practiceLocation.getContactMail());
        request.getParams().put("practicePhone", bag.practiceLocation.getPhone());
        request.getParams().put("bookingId", bag.bookingId);
        request.getParams().put("googleMaps", GoogleMapsTemplate.getMap(bag.practiceLocation));
    }

    @Override
    public String getTopicValue() {
        return "review-reminder";
    }

    public static final class ReviewReminderBag
    {
        public int sendAfterMinutes;
        public Instant start;
        public long bookingId;
        public long customerId;
        public String customerName;
        public String doctorName;
        public String practiceName;
        public String treatmentType;
        public LocationDto practiceLocation;
        public String reviewUrl;
    }
}
