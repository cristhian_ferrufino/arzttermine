package com.arzttermine.service.notification.messages.booking;

import com.arzttermine.library.web.dto.PriceDto;
import com.arzttermine.library.web.struct.Language;
import com.arzttermine.service.notification.messages.NotificationInitialisation;
import com.arzttermine.service.notifications.web.struct.Notification;
import com.arzttermine.service.notifications.web.struct.NotificationType;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

public class BookingInvoiceMessage extends NotificationInitialisation
{
    public BookingInvoiceMessage() {
    }

    public BookingInvoiceMessage(InvoiceBag bag, Language language) {
        super(Notification.TRANSACTION_INVOICE, NotificationType.EMAIL, bag.payerId, bag.payerEmailAddress, language);
        request.getParams().put("bookingId", bag.bookingId);
        request.getParams().put("transactionId", bag.transactionId);
        request.getParams().put("created", bag.createdAt.getEpochSecond() * 1000);
        request.getParams().put("transactionConfirmedAt", bag.paidAt.getEpochSecond() * 1000);
        request.getParams().put("price", bag.price.getAmount());
        request.getParams().put("currency", bag.price.getCurrency().name());
        request.getParams().put("payerEmailAddress", bag.payerEmailAddress);
        request.getParams().put("payerFullName", bag.payerFullName);
        request.getParams().put("articleDescription", bag.articleDescription);
        request.getParams().put("practiceName", bag.practiceName);
        request.getParams().putAll(bag.paymentDetails);
    }

    @Override
    public String getTopicValue() {
        return "booking-invoice";
    }

    public static final class InvoiceBag
    {
        public long bookingId;
        public long transactionId;
        public Instant createdAt;
        public Instant paidAt;
        public PriceDto price;
        public long payerId;
        public String payerEmailAddress;
        public String payerFullName;
        public String articleDescription;
        public String practiceName;
        public Map<String, String> paymentDetails = new HashMap<>();
    }
}
