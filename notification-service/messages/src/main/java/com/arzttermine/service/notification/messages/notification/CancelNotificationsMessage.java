package com.arzttermine.service.notification.messages.notification;

import com.arzttermine.library.messaging.model.TopicMessage;
import com.arzttermine.service.notification.messages.NotificationInitialisation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CancelNotificationsMessage extends TopicMessage {

    private List<NotificationCancellationRequestDto> cancelRequests;

    public CancelNotificationsMessage(List<NotificationCancellationRequestDto> cancellations) {
        this.cancelRequests = cancellations;
    }

    public CancelNotificationsMessage(NotificationCancellationRequestDto cancellation) {
        this.cancelRequests = new ArrayList<>();
        cancelRequests.add(cancellation);
    }

    public CancelNotificationsMessage() {
        cancelRequests = new ArrayList<>();
    }


    public String getTopicKind() {
        return "notification";
    }

    public String getTopic() {
        return "cancellation";
    }

    public String getTopicValue(){
        return "messages";
    }

    public void addMessage(NotificationInitialisation message, Map<String, Object> params) {
        if (! message.getCancellationParameters().isEmpty()) {

            final List<String> cleanParams = params.entrySet().stream()
                .filter(e -> null != e.getValue())
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());

            if (cleanParams.containsAll(message.getCancellationParameters())) {
                NotificationCancellationRequestDto cancelRequest = new NotificationCancellationRequestDto(message);
                cancelRequest.setParams(params);
                this.addCancellationRequest(cancelRequest);
            }
        }
    }

    public void addCancellationRequest(NotificationCancellationRequestDto cancellation) {
        cancelRequests.add(cancellation);
    }

    public List<NotificationCancellationRequestDto> getCancellationRequests() {
        return this.cancelRequests;
    }

}
