package com.arzttermine.service.notification.messages.templating;

import com.arzttermine.library.suport.templating.exchange.UrlRequest;
import com.arzttermine.service.profile.web.dto.LocationDto;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

public class GoogleMapsTemplate
{
    public static String getMap(final LocationDto location) {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>(2);
        map.add("center", String.format("%f,%f", location.getLatitude(), location.getLongitude()));
        map.add("markers", String.format("size:mid|color:red|%f,%f", location.getLatitude(), location.getLongitude()));

        UrlRequest request = new UrlRequest(UrlRequest.Type.GOOGLE_MAPS, new String[0], new String[0], map);

        return request.toString();
    }
}
