package com.arzttermine.service.notification.messages.validation;

import com.arzttermine.library.messaging.processor.MessageBus;
import com.arzttermine.service.notification.messages.NotificationInitialisation;
import com.arzttermine.service.notifications.web.struct.NotificationType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

public abstract class AbstractNotificationValidationProxy {

    private final MessageBus messageBus;

    public AbstractNotificationValidationProxy(final MessageBus messageBus) {
        this.messageBus = messageBus;
    }

    protected abstract boolean canSend(NotificationInitialisation message);

    protected boolean supports(NotificationType type) {
        return null != type;
    }

    public Future<?> sendAsync(Collection<NotificationInitialisation> messages) {

        return messageBus.sendAsync(messages.stream()
                .filter(message -> !supports(message.request.getType()) || canSend(message))
                .collect(Collectors.toCollection(ArrayList::new)));
    }

    public void send(NotificationInitialisation message) {
        if (!supports(message.request.getType()) || canSend(message)) {
            messageBus.send(message);
        }
    }
}
