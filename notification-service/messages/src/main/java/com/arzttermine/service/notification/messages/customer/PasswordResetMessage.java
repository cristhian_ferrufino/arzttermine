package com.arzttermine.service.notification.messages.customer;

import com.arzttermine.library.web.struct.Language;
import com.arzttermine.service.notification.messages.NotificationInitialisation;
import com.arzttermine.service.notifications.web.struct.Notification;
import com.arzttermine.service.notifications.web.struct.NotificationType;

public class PasswordResetMessage extends NotificationInitialisation
{
    public PasswordResetMessage() {
    }

    public PasswordResetMessage(PasswordResetBag bag, Long customerId, String email, Language language) {
        super(Notification.PASSWORD_RESET, NotificationType.EMAIL, customerId, email, language);
        request.getParams().put("resetLink", bag.resetLink);
        request.getParams().put("customerName", bag.customerName);
    }

    @Override
    public String getTopicValue() {
        return "password-change";
    }

    public static final class PasswordResetBag
    {
        public String resetLink;
        public String customerName;
    }
}
