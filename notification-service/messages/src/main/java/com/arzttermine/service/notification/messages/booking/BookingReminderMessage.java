package com.arzttermine.service.notification.messages.booking;

import com.arzttermine.library.web.struct.Language;
import com.arzttermine.service.notification.messages.NotificationInitialisation;
import com.arzttermine.service.notification.messages.templating.GoogleMapsTemplate;
import com.arzttermine.service.notifications.web.struct.Notification;
import com.arzttermine.service.notifications.web.struct.NotificationType;
import com.arzttermine.service.profile.web.dto.LocationDto;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public final class BookingReminderMessage extends NotificationInitialisation
{
    public BookingReminderMessage() {
    }

    public BookingReminderMessage(ReminderBag bag, NotificationType type, String target, Language language) {
        super(Notification.BOOKING_REMINDER, type, bag.customerId, target, language);
        request.setExpectedAt(Date.from(bag.start.minus(bag.sendBeforeMinutes, ChronoUnit.MINUTES)));
        request.getParams().put("customer", bag.customerName);
        request.getParams().put("start", bag.start.getEpochSecond() * 1000);
        request.getParams().put("created", bag.created.getEpochSecond() * 1000);
        request.getParams().put("bookingId", bag.bookingId);
        request.getParams().put("treatmentType", bag.treatmentType);
        request.getParams().put("doctor", bag.doctorName);
        request.getParams().put("practiceName", bag.practiceName);
        request.getParams().put("practiceMail", bag.practiceLocation.getContactMail());
        request.getParams().put("practicePhone", bag.practiceLocation.getPhone());
        request.getParams().put("location", bag.practiceLocation.getLocation());
        request.getParams().put("city", bag.practiceLocation.getCity());
        request.getParams().put("zip", bag.practiceLocation.getZip());
        request.getParams().put("longitude", bag.practiceLocation.getLongitude());
        request.getParams().put("latitude", bag.practiceLocation.getLatitude());
        request.getParams().put("googleMaps", GoogleMapsTemplate.getMap(bag.practiceLocation));
    }

    @Override
    public String getTopicValue() {
        return "booking-reminder";
    }

    public static final class ReminderBag
    {
        public int sendBeforeMinutes;
        public String customerName;
        public String doctorName;
        public long customerId;
        public long bookingId;
        public Instant start;
        public Instant created;
        public String treatmentType;
        public String practiceName;
        public LocationDto practiceLocation;
    }
}
