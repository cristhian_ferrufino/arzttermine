package com.arzttermine.service.notification.messages.validation;

import com.arzttermine.library.messaging.processor.MessageBus;
import com.arzttermine.library.web.struct.Language;
import com.arzttermine.service.notification.messages.NotificationInitialisation;
import com.arzttermine.service.notification.messages.customer.PasswordResetMessage;
import com.arzttermine.service.notifications.web.struct.Notification;
import com.arzttermine.service.notifications.web.struct.NotificationType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;

import static junit.framework.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AbstractNotificationValidationProxyTest
{
    @Mock
    private MessageBus messageBus;

    @Test
    public void canSend_should_return_false_for_canSend_if_validator_does_not_grant_notification()
    {
        AbstractNotificationValidationProxy proxy = new NotificationValidationProxyImpl(messageBus);
        PasswordResetMessage message = new PasswordResetMessage(new PasswordResetMessage.PasswordResetBag(), 42L, "not granted", Language.GERMAN);

        boolean result = proxy.canSend(message);

        assertFalse(result);
    }

    @Test
    public void canSend_should_return_true_for_canSend_if_validator_grants_notification()
    {
        AbstractNotificationValidationProxy proxy = new NotificationValidationProxyImpl(messageBus);
        PasswordResetMessage message = new PasswordResetMessage(new PasswordResetMessage.PasswordResetBag(), 42L, "granted", Language.GERMAN);

        boolean result = proxy.canSend(message);

        assertTrue(result);
    }

    @Test
    public void send_should_filter_messages_by_applicable_validators_only()
    {
        AbstractNotificationValidationProxy proxy = new NotificationValidationProxyImpl(messageBus);
        NotificationInitialisation message = new NotificationInitialisationImpl(NotificationType.SMS, "not granted");
        NotificationInitialisation message2 = new NotificationInitialisationImpl(NotificationType.EMAIL, "not granted");

        proxy.send(message);
        proxy.send(message2);

        verify(messageBus, times(1)).send(message);
        verify(messageBus, never()).send(message2);
    }

    @Test
    public void sendAsync_should_filter_messages_by_applicable_validators_only()
    {
        AbstractNotificationValidationProxy proxy = new NotificationValidationProxyImpl(messageBus);
        NotificationInitialisation message = new NotificationInitialisationImpl(NotificationType.SMS, "not granted");
        NotificationInitialisation message2 = new NotificationInitialisationImpl(NotificationType.EMAIL, "not granted");

        proxy.sendAsync(Arrays.asList(message, message2));

        verify(messageBus, times(1)).sendAsync(Collections.singletonList(message));
        verify(messageBus, never()).send(any());
    }

    public static final class NotificationValidationProxyImpl extends AbstractNotificationValidationProxy
    {
        public NotificationValidationProxyImpl(MessageBus messageBus) {
            super(messageBus);
        }

        @Override
        protected boolean canSend(NotificationInitialisation message) {
            return message.request.getTarget().equals("granted");
        }

        @Override
        protected boolean supports(NotificationType type) {
            return NotificationType.EMAIL.equals(type);
        }
    }

    private static final class NotificationInitialisationImpl extends NotificationInitialisation
    {
        public NotificationInitialisationImpl(NotificationType type, String target) {
            super(Notification.BOOKING_MOVED, type, target, Language.ENGLISH);
        }

        @Override
        public String getTopicValue() {
            return "test";
        }
    }
}
