'use strict';

const s3 = require('s3');
const aws = require('aws-sdk');

const prompt = require('prompt');
const awsCredentials = {
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_KEY
};

const missing = [];
if (!awsCredentials.accessKeyId) {
    missing.push({ name: 'accessKeyId', empty: false });
}
if (!awsCredentials.secretAccessKey) {
    missing.push({ name: 'secretAccessKey', empty: false });
}

if (missing.length) {
    prompt.start();
    prompt.get(missing, (err, params) => {
        awsCredentials.accessKeyId = params.accessKeyId;
        awsCredentials.secretAccessKey = params.secretAccessKey;

        doUpload();
    });
} else {
    doUpload();
}

function doUpload() {
    const awsClient = new aws.S3({
        signatureVersion: 'v4',
        region: 'eu-central-1',
        accessKeyId: awsCredentials.accessKeyId,
        secretAccessKey: awsCredentials.secretAccessKey
    });

    const client = s3.createClient({
        multipartUploadThreshold: 5242880,
        multipartUploadSize: 5242880,
        s3Client: awsClient
    });

    process.argv.forEach((val, index) => {
        if (index < 2) {
            return;
        }

        uploadTemplate(client, val);
    });
}

function uploadTemplate(client, template) {
    if (!template) {
        throw new Error('missing template name');
    }

    const localFile = `${__dirname}/dist/${template}/${template}.min.css`;
    console.log('trying to upload template file ' + localFile);

    const uploadParams = {
        localFile,
        s3Params: {
            Bucket: 'widget.arzttermine.de',
            ACL: 'public-read',
            Key: `styles/${template}.min.css`,
            Body: require('fs').createReadStream(localFile)
        }
    };

    const upload = client.uploadFile(uploadParams);

    upload.on('error', (error) => {
        console.log('');
        console.error('unable to upload file ' + localFile, error);
        process.exit(1);
    });

    upload.on('progress', () => {
        process.stdout.write('.');
    });

    upload.on('end', () => {
        console.log('');
        console.log(`template ${template} uploaded successfully (file: ${localFile})`);
        process.exit(0);
    });
}
