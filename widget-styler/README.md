# widget-styler

This project is used to generate templates and color-schemas for our [widget-booking](https://github.com/arzttermine/widget-booking) project.
Avoid to define colors in the widget project directly. They should be customizable! 

This project creates based on `.json` files proper `.css` files which can be used to override styles from our widget.
All templates are stored in `/templates/*.json`.

## How to use

To use this project, run `npm install` before.  
The generated (ugly- and minified) sources can be found in `dist/{template-name}/{template-name}.min.css`. 

#### Compile existing template
If you want to build the **default** template just run `npm start`.

Otherwise for any other template run `npm run {template-name}` to build the dist file (see [Create a new template](#create-a-new-template)).

#### Create a new template
To create a new template follow these instructions:
1. Create a new template file under `/templates/{template-name}.json`
2. Copy the content from the `default.json` for example
3. Adjust the style settings to your templates needs
4. Compile & upload the template: `npm run upload -- --template {template-name}`
5. If you only want to compile the template run `npm run compile -- --template {template-name}` instead
