'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const header = require('gulp-header');
const rimraf = require('gulp-rimraf');
const concat = require('gulp-concat');
const uglify = require('gulp-uglifycss');
const rename = require('gulp-rename');
const shell = require('gulp-shell');
const argv = require('yargs').argv;

const template = argv.template || 'default';
const templatePath = `templates/${template}.json`;
const config = JSON.parse(require('fs').readFileSync(templatePath, 'utf8').toString());
console.log('compiling template ' + template);

gulp.task('template:compile', ['dist:clean'], () => {
    let variables = `
        /** buttons and inputs **/
        $primary-background: ${config.primaryBackground};
        $primary-color: ${config.primaryColor};
        $primary-font: ${config.primaryFont.size}${config.primaryFont.unit} ${config.primaryFont.family};
        $primary-button-default-background: ${config.primaryButton.default.backgroundColor};
        $primary-button-default-color: ${config.primaryButton.default.color};
        $primary-button-default-border: ${config.primaryButton.default.borderColor};
        $primary-button-active-background: ${config.primaryButton.active.backgroundColor};
        $primary-button-active-color: ${config.primaryButton.active.color};
        $primary-button-outline-background: ${config.primaryButton.outline.backgroundColor};
        $primary-button-outline-border: ${config.primaryButton.outline.borderColor};
        $primary-button-outline-color: ${config.primaryButton.outline.color};
        $primary-select-background: ${config.primarySelect.backgroundColor};
        $primary-select-color: ${config.primarySelect.color};
        $primary-select-font: ${config.primarySelect.font};

        /** alerts **/
        $info-alert-background: ${config.alerts.info.backgroundColor};
        $info-alert-border: ${config.alerts.info.borderColor};
        $info-alert-color: ${config.alerts.info.color};
        $warning-alert-background: ${config.alerts.info.backgroundColor};
        $warning-alert-border: ${config.alerts.info.borderColor};
        $warning-alert-color: ${config.alerts.info.color};
        $danger-alert-background: ${config.alerts.danger.backgroundColor};
        $danger-alert-border: ${config.alerts.danger.borderColor};
        $danger-alert-color: ${config.alerts.danger.color};

        /** menu **/
        $menu-circle-background: ${config.menu.circle.backgroundColor};
        $menu-circle-border-color: ${config.menu.circle.borderColor};
        $menu-circle-color: ${config.menu.circle.color};
        $menu-circle-label-background: ${config.menu.label.backgroundColor};
        $menu-circle-label-color: ${config.menu.label.color};
        $menu-done-background: ${config.menu.done.backgroundColor};
        $menu-done-border-color: ${config.menu.done.borderColor};
        $menu-done-color: ${config.menu.done.color};
        $menu-outer-color: ${config.menu.outer.color};
        $menu-outer-active-color: ${config.menu.outer.activeColor};

        /** utils **/
        $divider-color: ${config.divider.color};
        $spinner-background: ${config.spinner.backgroundColor};
        $fullscreen-background: ${config.fullscreen.backgroundColor};

        /** success **/
        $success-header-color: ${config.success.header.color};
        $success-header-background: ${config.success.header.backgroundColor};
    `;

    // conditional supplies

    if (config.zoom) {
        const fullWidth = 100 + (100 - (100 * config.zoom));
        // changing the viewport if necessary to scale differently
        variables += `
            $primary-zoom: ${config.zoom};
            $full-width-with-zoom: ${fullWidth}%;
        `;
    }

    if (config.calendar.event) {
        variables += '/** calendar **/';
        if (config.calendar.event.titleFont) {
            variables += `
                $calendar-event-title-font: ${config.calendar.event.titleFont.weight} ${config.calendar.event.titleFont.size}${config.calendar.event.titleFont.unit} ${config.calendar.event.titleFont.family};
                $calendar-event-title-color: ${(config.calendar.event.titleFont || {color: 'inherit'}).color};

            `;
        }
        if (config.calendar.event.backgroundColor) {
            variables += `
                $calendar-event-background: ${config.calendar.event.backgroundColor || 'inherit'};
                $calendar-event-color: ${config.calendar.event.color || 'inherit'};
            `;
        }

        if (config.calendar.event.hover) {
            variables += `
                $calendar-event-hover-background: ${config.calendar.event.hover.backgroundColor};
                $calendar-event-hover-color: ${config.calendar.event.hover.color};
        `;
        }

        if (config.calendar.event.borderColor) {
            variables += `
                $calendar-event-border-color: ${config.calendar.event.borderColor};
            `;
        }
    }

    if (config.primaryButton.active.borderColor) {
        variables += `
            $primary-button-active-border: ${config.primaryButton.active.borderColor};
        `;
    }

    if (config.headlineFont) {
        variables += `
            /** headline **/
            $headline-font: ${config.headlineFont.weight} ${config.headlineFont.size}${config.headlineFont.unit} ${config.headlineFont.family};
        `;
    }

    if (config.headlineFont.color) {
      variables += `
          $headline-color: ${config.headlineFont.color};
      `;
    }

    if (config.primaryFont.weight) {
      variables += `
            /** primary font weight **/
            $primary-font-weight: ${config.primaryFont.weight};
      `
    }

    return gulp.src(['styles/main.scss'])
        .pipe(header(variables + '\n\n'))
        .pipe(sass())
        .pipe(uglify({
            uglyComments: true
        }))
        .pipe(rename(template + '.min.css'))
        .pipe(gulp.dest(`dist/${template}/`));
});

gulp.task('template:upload', ['template:compile'],
    shell.task('node uploader.js ' + template));

gulp.task('dist:clean', () => {
    gulp.src('dist/' + template + '/**', { readonly: false })
        .pipe(rimraf());
});
