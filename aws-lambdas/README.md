# AWS Lambdas

This project stores all our AWS Lambdas' code. AWS Lambdas allow to create microservice-type applications where one does not have to worry about hosting / the infrastructure the code is executed on, for more details [check the offical documentation](https://aws.amazon.com/de/documentation/lambda/).

## Our Lambdas

1. [Deployment Approval Notification](approval-notification)
2. [Image Resizing](image-resize)

## Code Styleguides

### Node.js

When developing node applications define a script ``package`` that can be executed to create a ZIP-archive in the ``dist`` subdirectory containing the lambda's code. Use this archive to provide your code to AWS.
