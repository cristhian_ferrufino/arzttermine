/*
* If err is set this func logs error message to console and to callback, true is
* returned. Else nothing happens and false is returned.
*/
module.exports = function(err, action, callback) {
  if (err) {
    var message = `Error ${action}: ${err}`;
    console.log(message)
    callback(message);
    return true;
  }
  return false;
}
