const AWS = require('aws-sdk');
const s3 = new AWS.S3();
const imageProcessor = require('./image-processor.js');
const reportError = require('./report-error.js');
const config = require('./config.json');

/*
* Applies operations configured in OPERATION_PARAMS to data using imageProcessor and
* puts result to S3.
*/
const processData = function(key, data, operations, callback) {

  if (typeof data.Body === 'undefined') {
    reportError('data.Body not set!', 'getting image data', callback);
    return;
  }

  //For each operation and output format ...
  operations.forEach( (operation) => {
    operation.output.forEach( (outputFormat) => {

      let path = key.substring(0, key.lastIndexOf('/')+1);

      //Set filename as defined in config, if exists
      let filename;
      if (typeof operation.filename === 'undefined') {
        filename = `${operation.operation}_${operation.width ? operation.width : ''}${operation.height ? 'x'+operation.height : ''}`;
      } else {
        filename = operation.filename;
      }

      imageProcessor.process(Buffer.from(data.Body), operation, function(result) {
          publishOnS3(result, `${path}${filename}.${outputFormat.toLowerCase()}`, callback);
      }, callback);

    });
  });

}

/*
* Puts object on S3 with public read access.
*/
const publishOnS3 = function(data, path, callback) {
  s3.putObject({Bucket: config.targetBucket, Key: path, Body: data, ACL: 'public-read'}, function(err) {
    if (reportError(err, 'writing object to s3', callback)) return;
    callback(null, data);
  });
}

/*
* Entry point for our lambda. Reads image-file (key) from SOURCE_BUCKET and
* passes it to the scaleImage function.
*/
exports.handler = (event, context, callback) => {

  // Gate to prevent recursion
  if (event.Records[0].userIdentity.principalId.endsWith(':image-resizing')) {
    return;
  }

  const key = event.Records[0].s3.object.key;

  // Construct selection of relevant operations by applying condition-regex
  const operations = [];
  config.operations.filter(elem => key.match(new RegExp(elem.condition + config.fileTypes, 'i')))
  .forEach(elem => operations.push(...elem.params));

  if (operations.length < 1) {
    reportError('no suitable operations found!', 'processing image', callback);
  }

  s3.getObject({ Bucket: config.sourceBucket, Key: event.Records[0].s3.object.key }, function(err, data) {
    if (reportError(err, 'loading object from s3', callback)) return;
    processData(key, data, operations, callback);
  });

};
