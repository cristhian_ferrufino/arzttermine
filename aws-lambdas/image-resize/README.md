# image-resize

...has access to the atcalendar.arzttermine.de S3 bucket. This lambda is configured to be triggered on any file that is uploaded to the bucket's "media" (or any sub-)directory and performs a set of selected image operations based on the path and filetype of uploaded file.

Specific image operations, their corresponding "trigger-paths" (conditions) and supported file types may be configured using the config.json file.
