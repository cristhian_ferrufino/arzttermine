const gm = require('gm').subClass({ imageMagick: true });
const reportError = require('./report-error.js');

const scale = function(imageBuffer, params) {
  return gm(imageBuffer).scale(params.width, params.height);
}

const scaleWidth = function(imageBuffer, params) {
  return gm(imageBuffer).scale(params.width);
}

const scaleWidthCrop = function(imageBuffer, params) {
  return scaleWidth(imageBuffer, params).crop(params.width, params.height);
}

const operations = {
  'scale': scale,
  'scaleWidth': scaleWidth,
  'scaleWidthCrop': scaleWidthCrop
}

/*
* Applies image operation on provided imageBuffer and passes result to
* success-callback.
*/
exports.process = function(imageBuffer, params, success, error) {

  if (!params.hasOwnProperty('operation') || !params.hasOwnProperty('output')) {
    reportError('operation and output must be set.', 'missing parameters', error);
    return;
  }

  // Get operation
  const operation = operations[params.operation];
  if (typeof operation === 'undefined') {
    reportError(params.operation, 'unknown operation', error);
    return;
  }

  // Apply operation and write image to buffer
  const gmImage = operation(imageBuffer, params);
  gmImage.quality(100).toBuffer(params.outputFormat, function(err, result) {
    if (reportError(err, 'writing image', error)) return;
    success(result);
  });
}
