const https = require('https');
const util = require('util');

const OPTIONS = {
    method: 'POST',
    hostname: 'hooks.slack.com',
    port: 443,
    path: '/services/T1YK15H38/B6BCHJJ01/cx3XlPVEvCsnOpIuRfUs6Zpo'
};

exports.handler = function(event, context) {
    /*console.log(JSON.stringify(event, null, 2));
     console.log('From SNS:', event.Records[0].Sns.Message);*/

    var message = JSON.parse(event.Records[0].Sns.Message);
    var stage = message.approval.stageName;
    var externalLink = message.approval.externalEntityLink;
    var approvalLink = message.approval.approvalReviewLink;

    var body = 'Deployed version: ' + externalLink + '\n';
    body += 'Deployed app to stage: ' + stage + '\n';
    body += 'Approve or reject here: ' + approvalLink + '\n';
    body += 'Approval request expires at: ' + message.approval.expires;

    var postData = {
        channel: '#at-it',
        username: 'Deployment :: DevQa Cloud :: ' + stage,
        text: '*New Revision deployed to staging. Waiting for approval.*',
        icon_emoji: ':v:',
        attachments: [
            {
                color: 'good',
                text: body
            }
        ]
    };

    var req = https.request(OPTIONS, function(res) {
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            context.done(null);
        });
    });

    req.on('error', function(e) {
        console.log('problem with request: ' + e.message);
    });

    req.write(util.format('%j', postData));
    req.end();
};
