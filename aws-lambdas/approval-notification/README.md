# Approval Notification

...sends out notification after a successful staging deployment.
At the moment we only send out the notification to the at-it chat in Slack.
