import {Component, Input} from '@angular/core';
import {PaymentService} from '../service/payment-service';

@Component({
    selector: 'payment-details',
    template: `
        <div [style.fontSize.em]="fontSize">
            <strong>{{ 'payment.types.credit_card' | translate }}</strong> ({{ payments.details.card.brand }})<br />
            {{ payments.details.card.name }}<br />
            <span style="vertical-align: sub;">{{ getHiddenNumber() }}</span><br />
            {{ payments.details.card.exp_month }}/{{ payments.details.card.exp_year }}<br />
            <br />
        </div>
        <a *ngIf="editable" class="btn at-btn" (click)="payments.editDetails = true"> Ändern </a>
    `
})
export class PaymentDetailsComponent
{
    @Input() public editable: boolean = false;
    @Input() public fontSize: number = 1;

    constructor(public payments: PaymentService) {
    }

    public getHiddenNumber(): string {
        return `****-****-****-${this.payments.details.card.last4}`;
    }
}
