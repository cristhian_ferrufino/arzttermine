import {Component} from '@angular/core';
import {BookingService} from '../../checkout/service/booking-service';
import {WidgetService, steps} from '../../main/service/widget-service';
import {PaymentService} from '../service/payment-service';

@Component({
    selector: 'payment',
    template: `
    <layout>
        <div class="row">
          <div class="col-sm-6 col-xs-12">
              <h4>{{ 'headline.credit_card_info' | translate }}</h4>
              <credit-card *ngIf="!paymentService.details || paymentService.editDetails" 
                           [(tokenRequest)]="tokenRequest"
                           (done)="continueToSummary(true)"></credit-card>

              <div *ngIf="paymentService.details && !paymentService.editDetails" class="text-center">
                <div class="at-element--highlight">
                    <div class="at-grid">
                        {{ 'payment.your_details' | translate }}:<br />
                        <payment-details [fontSize]=".9" [editable]="true"></payment-details>
                    </div>
                </div>
              </div>
          </div>
          <div class="col-sm-6 col-xs-12">
              <h4>{{ 'headline.costs_overview' | translate }}</h4>
              <table class="table">
                  <tr>
                      <td>{{ 'wording.treatment' | translate }}</td>
                      <td align="right">{{ charge | currency:'EUR':true:'1.2-2' }}</td>
                  </tr>
                  <tr>
                      <td>{{ 'wording.tax' | translate }}</td>
                      <td align="right">{{ 'wording.none' | translate }}</td>
                  </tr>
                  <tr>
                      <td>{{ 'payment.pay_now' | translate }}</td>
                      <td align="right">{{ charge | currency:'EUR':true:'1.2-2' }}</td>
                  </tr>
              </table>
              <p *ngIf="bookingService.treatment.comment" class="at-grid">{{ bookingService.treatment.comment }}</p>
              <div *ngIf="!bookingService.treatment.forcePay" style="margin-top: 10px;">
                  {{ 'you_can_also' | translate }}<br />
                  <a  class="btn btn-link" (click)="continueToSummary(false)">{{ 'payment.no_payment' | translate }}</a>
              </div>
              <div class="alert at-alert-info" *ngIf="bookingService.treatment.forcePay">
                  {{ 'payment.force_pay' | translate }}
              </div>
          </div>
        </div>
        <div class="at-divider"></div>
        <form (submit)="continueToSummary(bookingService.paidInAdvance())">
            <continue [complete]="bookingService.skippedPayment() || bookingService.paidInAdvance()"
                      [continueText]="getContinueHint()"
                      [previous]="previous"></continue>
        </form>
    </layout>
  `
})
export class PaymentComponent
{
    public tokenRequest: TokenRequest = {};
    public previous = (<any> steps).patient;

    constructor(public bookingService: BookingService,
                public paymentService: PaymentService,
                private widget: WidgetService) {
    }

    get charge(): number {
        return (this.bookingService.treatment && this.bookingService.treatment.charge.amount / 100) || 0;
    }

    public getContinueHint(): string {
        return (this.paymentService.editDetails || this.paymentService.details)
            ? 'payment.continue_with'
            : (this.paymentService.skipPayment) ? 'payment.without' : 'pages.to_summary';
    }

    public continueToSummary(withPayment: boolean = true): void {
        if (!withPayment) {
            if (this.bookingService.treatment.forcePay) {
                return;
            }
            this.paymentService.details = null;
            this.bookingService.bookingRequest.capture = null;
            this.bookingService.skipPayment();
        } else {
            this.bookingService.usePayment();
        }

        this.widget.goTo('summary');
    }
}
