import {
    Component, AfterViewInit, Input, OnInit, ViewChild, Output, NgZone, OnDestroy, ElementRef
} from '@angular/core';
import {BookingService} from '../../../checkout/service/booking-service';
import {Subject} from 'rxjs/Subject';
import {WidgetService} from '../../../main/service/widget-service';
import {environment} from '../../../../environments/environment';
import {PaymentService} from '../../service/payment-service';
import {NgForm} from '@angular/forms';
import {CreditCardDetails} from '../../../model/payment';

@Component({
    selector: 'credit-card',
    template: `
    <form (submit)="validatePayment()" autocomplete="on" #payment="ngForm">
        <div class="form-group">
            <label for="ccCardHolder">{{ 'payment.card_owner' | translate }}:</label>
            <input placeholder="{{ 'payment.card_owner' | translate }}" 
                   class="form-control"
                   [(ngModel)]="tokenRequest.name"
                   id="ccCardHolder"
                   type="text" 
                   name="name"
                   minlength="2" 
                   maxlength="35" 
                   required
                   autofocus
                   autocomplete="cc-name">
        </div>
        <div class="form-group">
            <label for="ccCardNumber">{{ 'payment.card_number' | translate }}:</label>
            <input placeholder="{{ 'payment.card_number' | translate }}" 
                   class="form-control"
                   id="ccCardNumber"
                   [(ngModel)]="tokenRequest.number"  
                   type="text"
                   minlength="2" 
                   maxlength="20" 
                   name="number" 
                   required
                   autocomplete="cc-number">
        </div>
        <label>{{ 'payment.expire_date' | translate }}:</label>
        <div class="row">
            <div class="col-4 form-group">
                  <select [(ngModel)]="tokenRequest.exp_month" name="exp_month" class="form-control" autocomplete="cc-exp-month" required>
                      <option value="" disabled selected>MM</option>
                      <option *ngFor="let m of months" value="{{ m }}">{{ m }}</option>
                  </select>
              </div>
              <div class="col-4 form-group">
                  <select [(ngModel)]="tokenRequest.exp_year" name="exp_year" class="form-control" autocomplete="cc-exp-year" required>
                      <option value="" disabled selected>YYYY</option>
                      <option *ngFor="let y of years" value="{{ y }}">{{ y }}</option>
                  </select>
              </div>
        </div>
        <div class="row">
            <div class="col-4 form-group">
                <label for="ccCardCVC">{{ 'payment.card_cvc' | translate }}:</label>
                <input placeholder="{{ 'payment.card_cvc' | translate }}"  
                       class="form-control"
                       [(ngModel)]="tokenRequest.cvc" 
                       name="cvc" 
                       type="text"
                       pattern="\\d*"
                       minlength="3"
                       maxlength="4"
                       id="ccCardCVC"
                       required
                       inputmode="numeric"
                       autocomplete="cc-csc">
            </div>
        </div>
        <br />
        <div *ngIf="message" [ngClass]="{
                'alert': true, 
                'at-alert-danger': !loading, 
                'at-alert-info': loading
            }">
            {{ message | translate }}
        </div>
        <button type="submit"
                [attr.disabled]="!paymentForm.valid ? '' : null"
                [ngClass]="{
                    'btn': true,
                    'at-btn-outline': !paymentForm.valid,
                    'at-btn': paymentForm.valid,
                    'disabled': !paymentForm.valid
                }"> 
            {{ 'form.check_payment_data' | translate }} 
        </button>
    </form>
    `
})
export class CreditCardComponent implements AfterViewInit, OnInit, OnDestroy
{
    @Input() public tokenRequest: TokenRequest = {};
    @Output() public done: Subject<void> = new Subject<void>();
    @ViewChild('payment') public paymentForm: NgForm;

    public message: string;
    public loading: boolean = false;
    public months: string[] = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
    public years: number[] = [];
    private ignoreChars = /[-,\s]/g;

    constructor(private _zone: NgZone,
                private widget: WidgetService,
                private bookingService: BookingService,
                public paymentService: PaymentService,
                private element: ElementRef) {
    }

    public ngAfterViewInit(): void {
        if (!this.widget.isPresent('stripe')) {
            this.widget.requested('stripe');
            this.initStripePayment();
        }
    }

    public ngOnInit(): void {
        const currentYear = new Date().getFullYear();

        for (let y = currentYear; y < currentYear + 7; y++) {
            this.years.push(y);
        }

        if (!this.bookingService.hasPersonalData()) {
            return;
        }

        const customer = this.bookingService.bookingRequest.customer;
        this.tokenRequest.name = `${customer.firstName} ${customer.lastName}`;
    }

    public ngOnDestroy(): void {
        this.paymentService.editDetails = false;
    }

    public validatePayment(): boolean {
        this.tokenRequest.number = this.tokenRequest.number.replace(this.ignoreChars, '');
        if (!this.paymentForm.valid) {
            return false;
        }

        const validNumber = window.Stripe.card.validateCardNumber(this.tokenRequest.number);

        if (!validNumber) {
            this.message = 'payment.validation.invalid_number';
            return false;
        }

        this.loading = true;
        this.message = 'payment.validation.validating';
        window.Stripe.card.createToken(this.tokenRequest, (status: number, response: any) => {
            this._zone.run(() => this.treatResponse(status, response));
        });

        return false;
    }

    private treatResponse(status: number, response: {id?: string, card?: CreditCardDetails, error?: {code: string}}): void {
        this.message = null;
        this.loading = false;

        if (status === 200) {
            this.bookingService.bookingRequest.capture = response.id;
            this.paymentService.creditCardPayment(response.card);
            this.done.next(undefined);
        } else if (status == 429) {
            this.message = 'payment.validation.too_many_requests';
        } else {
            this.message = 'payment.validation.' + response.error.code;
        }
    }

    private initStripePayment(): void {
        const tag: HTMLScriptElement = document.createElement('script');
        tag.src = 'https://js.stripe.com/v2/';
        tag.id = 'stripePayment';
        tag.async = true;
        tag.onload = () => {
            window.Stripe && (<any> window.Stripe).setPublishableKey(environment.stripe.publicKey);
        };

        this.element.nativeElement.appendChild(tag);
    }
}
