import {Injectable} from '@angular/core';
import {PaymentDetails, CreditCardDetails} from '../../model/payment';

@Injectable()
export class PaymentService
{
    public details: PaymentDetails;
    public editDetails: boolean = false;
    public skipPayment: boolean = false;

    public creditCardPayment(ccDetails: CreditCardDetails): void {
        this.details = {
            type: 'CREDIT_CARD',
            card: ccDetails
        };
    }

    public flush(): void {
        this.details = null;
    }
}
