import './vendor/polyfills';

import {enableProdMode} from '@angular/core';
import {environment} from '../environments/environment';

//if (environment.production) {
    enableProdMode();
//}

import {BaseAppModule} from './app.module';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
platformBrowserDynamic().bootstrapModule(BaseAppModule);
