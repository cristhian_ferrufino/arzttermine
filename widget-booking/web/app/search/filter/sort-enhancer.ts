import {SearchTermEnhancer} from './search-filter';

export class SortEnhancer implements SearchTermEnhancer
{
    constructor(private field: string, private sort: string|{order: string, mode?: string}) {
    }

    public enhance(search: SearchBody): void {
        if (!search.sort) {
            search.sort = [];
        }

        const item: any = {};
        item[this.field] = this.sort;
        search.sort.push(item);
    }
}
