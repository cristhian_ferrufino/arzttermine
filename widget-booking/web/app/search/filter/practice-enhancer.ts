import {SearchTermEnhancer} from "./search-filter";

export class PracticeEnhancer implements SearchTermEnhancer
{
    constructor(private practiceId: number) {
    }

    public enhance(search: SearchBody)
    {
        if (!search.query.bool) {
            search.query.bool = {};
        }
        if (!search.query.bool.must) {
            search.query.bool.must = [];
        }

        search.query.bool.must.push({
            match: {
                "practice.id": this.practiceId
            }
        });
    }
}
