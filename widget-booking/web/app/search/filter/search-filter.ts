export interface SearchTermEnhancer
{
    enhance(search: SearchBody): void;
}
