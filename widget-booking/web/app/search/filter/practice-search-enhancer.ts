import {SearchTermEnhancer} from './search-filter';

export class PracticeSearchEnhancer implements SearchTermEnhancer
{
    constructor(private practiceId: number) {
    }

    public enhance(search: SearchBody)
    {
        if (!search.query.bool) {
            search.query.bool = {};
        }
        if (!search.query.bool.must) {
            search.query.bool.must = [];
        }

        search.query.bool.must.push({
            match: {
                'id': this.practiceId
            }
        });
    }
}
