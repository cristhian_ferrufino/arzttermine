import {SearchTermEnhancer} from './search-filter';

export class LanguageEnhancer implements SearchTermEnhancer
{
    constructor(private languages: string[]) {
    }

    public enhance(search: SearchBody)
    {
        if (!search.query.bool) {
            search.query.bool = {};
        }
        if (!search.query.bool.must) {
            search.query.bool.must = [];
        }

        const supported: any = {
            bool: {
                should: []
            }
        };

        for (const lang in this.languages) {
            supported.bool.should.push({
                match: { availableLanguages: this.languages[lang] }
            });
        }

        search.query.bool.must.push(supported);
    }
}
