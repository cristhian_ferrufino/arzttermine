import {SearchTermEnhancer} from './search-filter';

export class DocumentSourceEnhancer implements SearchTermEnhancer
{
    constructor(private fields: string[]) {
    }

    public enhance(search: SearchBody): void {
        if (!search._source && this.fields.length) {
            search._source = [];
        }

        if (this.fields.length) {
            search._source.push(... this.fields);
        }
    }
}
