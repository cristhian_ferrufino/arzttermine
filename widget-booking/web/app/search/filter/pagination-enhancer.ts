import {SearchTermEnhancer} from "./search-filter";

export class PaginationEnhancer implements SearchTermEnhancer
{
    constructor(private page: number, private size: number) {
    }

    public enhance(search: SearchBody)
    {
        search.from = this.page * this.size;
        search.size = this.size;
    }
}
