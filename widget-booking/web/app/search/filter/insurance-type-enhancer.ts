import {AbstractTypeEnhancer} from './abstract-type-enhancer';

export class InsuranceTypeEnhancer extends AbstractTypeEnhancer
{
    constructor(private insurance: string) {
        super('availableInsuranceTypes', insurance);
    }
}
