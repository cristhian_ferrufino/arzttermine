import {SearchTermEnhancer} from "./search-filter";

export class AbstractTypeEnhancer implements SearchTermEnhancer
{
    constructor(protected type: string, private termValue: any) {
    }

    public enhance(search: SearchBody)
    {
        if (!search.query.bool) {
            search.query.bool = {};
        }
        if (!search.query.bool.must) {
            search.query.bool.must = [];
        }

        const filter = { match: {} };
        filter.match[this.type] = this.termValue;

        search.query.bool.must.push(filter);
    }
}
