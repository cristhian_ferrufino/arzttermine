import {SearchTermEnhancer} from "./search-filter";

export class DateRangeEnhancer implements SearchTermEnhancer
{
    constructor(public from: Date, public to: Date, public timezone?: string) {
        if (!this.timezone) {
            this.timezone = `+0${(-1 * new Date().getTimezoneOffset() / 60)}:00`;
        }
    }

    public enhance(search: SearchBody) {
        if (!search.query.bool) {
            search.query.bool = {};
        }
        if (!search.query.bool.must) {
            search.query.bool.must = [];
        }

        search.query.bool.must.push({
            range: {
                start: {
                    gte: this.from,
                    lte: this.to,
                    time_zone: this.timezone
                }
            }
        });
    }
}
