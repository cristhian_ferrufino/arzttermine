import {AbstractTypeEnhancer} from './abstract-type-enhancer';

export class TreatmentTypeEnhancer extends AbstractTypeEnhancer
{
    constructor(private id: number) {
        super('availableTreatmentTypes', id);
    }
}
