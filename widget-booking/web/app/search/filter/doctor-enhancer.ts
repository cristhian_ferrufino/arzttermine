import {SearchTermEnhancer} from "./search-filter";

export class DoctorEnhancer implements SearchTermEnhancer
{
    constructor(private doctorId: number) {
    }

    public enhance(search: SearchBody)
    {
        if (!search.query.bool) {
            search.query.bool = {};
        }
        if (!search.query.bool.must) {
            search.query.bool.must = [];
        }

        search.query.bool.must.push({
            match: {
                "doctor.id": this.doctorId
            }
        });

        search.query.bool.must.push({
            match: {
                "doctor.bookable": true
            }
        });
    }
}
