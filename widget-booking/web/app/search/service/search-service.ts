import {Injectable} from '@angular/core';
import {ElasticClient} from './elastic-client';
import {Observable} from 'rxjs/Observable';
import {SearchTermEnhancer} from '../filter/search-filter';
import {Practice} from '../../model/practice';
import {Appointment} from '../../model/appointment';
import {PaginationEnhancer} from '../filter/pagination-enhancer';
import {DocumentSourceEnhancer} from '../filter/document-source-enhancer';
import {SortEnhancer} from '../filter/sort-enhancer';

/**
 * <h2>Usage</h2>
 * Inject the service
 * constructor(private search: SearchService) {}
 *
 * Subscribing on search result:
 * const subscription = this.search.searchAppointments([new PracticeEnhancer(1)]).subscribe((result) => {
 *     console.log(JSON.stringify(result, null, 4));
 *     subscription.unsubscribe(); // release subscription or cancel if necessary
 * });
 */
@Injectable()
export class SearchService
{
    constructor(private elastica: ElasticClient) {
    }

    public searchAppointments(filters: SearchTermEnhancer[] = []): Observable<Appointment[]> {
        return Observable.create((observer: any) => {
            this.elastica.search({ index: 'appointments' }, filters).take(1).subscribe(result => {
                observer.next(result.hits.hits.map(h => new Appointment(h._source)));
            }, err => {
                window.console && console.error(err);
                observer.error(err);
            });
        });
    }

    public findNextAvailableAppointment(filters: SearchTermEnhancer[] = []): Observable<Date[]> {
        filters.push(new PaginationEnhancer(0, 1));
        filters.push(new DocumentSourceEnhancer(['start']));
        filters.push(new SortEnhancer('start', 'asc'));

        return Observable.create((observer: any) => {
            this.elastica.search({ index: 'appointments' }, filters).take(1).subscribe(result => {
                observer.next(result.hits.hits.map(h => new Date(h._source.start)));
            }, err => {
                window.console && console.error(err);
                observer.error(err);
            });
        });
    }

    public getPractice(practiceId: number, country: string = 'germany'): Observable<Practice> {
        return Observable.create((observer: any) => {
            this.elastica.get('practices', country, practiceId).take(1).subscribe(result => {
                if (result._source) {
                    observer.next(new Practice(result._source));
                }
            }, err => {
                window.console && console.error(err);
                observer.error(err);
            });
        });
    }
}
