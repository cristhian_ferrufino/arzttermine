import {SearchTermEnhancer} from '../filter/search-filter';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs/Observable';
import {Http} from '@angular/http';
import {Injectable} from '@angular/core';

@Injectable()
export class ElasticClient
{
    private client: {host: string, prefix?: string};

    constructor(private http: Http) {
        this.client = environment.elasticsearch;
    }

    public get(index: string, type: string = null, id: number|string): Observable<ElasticDocument> {
        const url: string = (type) ? `/${this.client.prefix}${index}/${type}/${id}` : `/${index}/${id}`;
        return this.http.get(this.client.host + url).map(res => res.json());
    }

    public search(request: SearchQuery, filters: SearchTermEnhancer[] = []): Observable<SearchResponse> {
        const searchBody: SearchBody = {
            query: {},
            from: 0,
            size: 20
        };

        filters.forEach(f => f.enhance(searchBody));

        const url: string = `/${this.client.prefix}` + ((request.type) ? `${request.index}/${request.type}/_search` : `${request.index}/_search`);
        return this.http.post(this.client.host + url, searchBody).map(res => res.json());
    }
}
