import {TranslateLoader} from "@ngx-translate/core";
import {Observable} from "rxjs/Observable";
import {translations} from "../../translatons";

export class TranslationSearchService implements TranslateLoader
{
    public getTranslation(lang: string): Observable<any> {
        let locale: string = lang.split('_')[0].toLowerCase();
        if (!translations[locale]) {
            locale = 'de';
        }

        // always return the same locale without detecting it multiple times
        return Observable.create(observer => observer.next(translations[locale]));
    }
}
