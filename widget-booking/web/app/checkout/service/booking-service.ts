import {Injectable, Inject} from '@angular/core';
import {Http, RequestOptions, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {TreatmentType} from '../../model/treatment-type';
import {Appointment} from '../../model/appointment';
import {PracticeService} from './practice-service';
import {BookingDto, BookingRequest, Booking} from '../../model/booking';
import {environment} from '../../../environments/environment';
import {RestoreUtility} from '../../main/util/restore-utility';
import {PaymentService} from '../../payment/service/payment-service';
import {LocalStorage} from "../../main/util/local-storage";
import {DoctorDto} from '../../model/doctor';

@Injectable()
export class BookingService extends RestoreUtility
{
    public bookingRequest = new BookingRequest();
    public bookedDoctor: DoctorDto;
    public treatment: TreatmentType;
    public appointment: Appointment;
    public result: Booking;

    constructor(private practices: PracticeService,
                private payments: PaymentService,
                private http: Http,
                @Inject(LocalStorage) storage: LocalStorage) {
        super(storage);
    }

    set key(key: string) {
    }

    get key(): string {
        if (!this.practices.getPractice()) {
            return null;
        }
        return `booking_${this.practices.getPractice().id}`;
    }

    public isComplete(): boolean {
        return this.hasAppointment() && this.hasPersonalData()
            && (!this.treatment.charge || (this.paidInAdvance() || (!this.treatment.forcePay && this.payments.skipPayment)));
    }

    public hasAppointment(): boolean {
        return this.appointment && this.hasAppointmentDetails();
    }

    public hasAppointmentDetails(): boolean {
        return Boolean(this.treatment && this.bookingRequest.treatmentTypeId > 0
            && this.practices.getPractice()
            && this.bookingRequest.customer
            && (this.bookedDoctor || this.practices.getPractice().doctors.length < 2)
            && this.bookingRequest.customer.insuranceType
            && this.bookingRequest.customer.returning !== null);
    }

    public hasPersonalData(): boolean {
        return Boolean(this.bookingRequest.customer
            && this.bookingRequest.customer.email
            && this.bookingRequest.customer.firstName
            && this.bookingRequest.customer.lastName
            && this.bookingRequest.customer.phoneMobile
            && this.bookingRequest.customer.gender);
    }

    public setTreatmentType(treatmentTypeId: number): void {
        if (!treatmentTypeId) {
            this.treatment = null;
            this.bookingRequest.treatmentTypeId = 0;
            return;
        }

        const type: TreatmentType = this.practices.getPractice().treatmentTypes.find(tt => tt.id == treatmentTypeId);
        if (!type) {
            return;
        }

        this.treatment = type;
        this.bookingRequest.treatmentTypeId = type.id;
    }

    public setDoctor(doctor: DoctorDto): void {
        if (this.bookingRequest.treatmentTypeId) {
            if (! doctor.treatmentTypes.some(tt => tt.id === this.bookingRequest.treatmentTypeId)) {
                this.setTreatmentType(null);
            }
        }

        this.bookedDoctor = doctor;
    }

    public setAppointment(appointment: Appointment): void {
        this.appointment = appointment;
        this.bookingRequest.appointmentId = appointment.id;
    }

    public paidInAdvance(): boolean {
        return !this.payments.skipPayment && this.treatment && this.treatment.charge && Boolean(this.bookingRequest.capture);
    }

    public skipPayment(): void {
        this.payments.details = null;
        this.payments.skipPayment = true;
        this.bookingRequest.capture = null;
    }

    public usePayment(): void {
        this.payments.skipPayment = false;
    }

    public skippedPayment(): boolean {
        return this.payments.skipPayment;
    }

    public setResult(booking: BookingDto): BookingDto {
        this.result = new Booking(booking);
        this.bookingRequest = new BookingRequest();
        this.bookedDoctor = null;
        this.appointment = null;

        return booking;
    }

    public submit(): Observable<BookingDto> {
        const options: RequestOptions = new RequestOptions({ headers: new Headers() });
        options.headers.append('Authorization', `${(window.at_auth_type || 'Basic')} ${window.at_token}`);

        this.bookingRequest.practiceId = this.practices.getPractice().id;
        this.bookingRequest.platform = 'WIDGET';
        this.bookingRequest.title =
            `${this.treatment.type}, ${this.bookingRequest.customer.firstName} ${this.bookingRequest.customer.lastName}`;

        const url = `${environment.bookingService.host}/${environment.bookingService.path}`;
        return this.http.post(url, this.bookingRequest, options).map(r => {
            const result = this.setResult(r.json());
            this.persistChoice(result.id);

            return result;
        });

    }

    protected select(choice: string|number, noPersist?: boolean): void {
        // load booking from service ?? -> in guard?
    }
}
