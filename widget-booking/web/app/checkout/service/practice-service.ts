import {Injectable, NgZone} from '@angular/core';
import {Practice} from '../../model/practice';
import {SearchService} from '../../search/service/search-service';
import {environment} from '../../../environments/environment';
import {ReplaySubject} from 'rxjs/ReplaySubject';

@Injectable()
export class PracticeService
{
    public loading: ReplaySubject<Practice> = new ReplaySubject<Practice>(1);

    private practice: Practice;
    private error: boolean = false;
    private googleMapsLink: string;
    private showMap: boolean = !window.at_hide_map;

    constructor(private search: SearchService, private _zone: NgZone) {
    }

    public init(practiceId: number): void {
        this.search.getPractice(practiceId).take(1).subscribe((result) => {
            this._zone.run(() => {
                if (!result) {
                    this.error = true;
                    return;
                }

                this.practice = result;
                this.loading.next(result);
            });
        }, (err) => {
            this._zone.run(() => this.error = true);
        });
    }

    public hasError(): boolean {
        return this.error;
    }

    public getPractice(): Practice {
        return this.practice;
    }

    public isMapVisible(): boolean {
        return this.showMap;
    }

    public getGoogleLocation(practice: Practice): string {
        return (this.showMap) ? this.googleMapsLink || (this.googleMapsLink = `https://maps.googleapis.com/maps/api/staticmap?center=${practice.location.latitude},${practice.location.longitude}` +
            '&zoom=17&scale=1&size=300x225&maptype=roadmap&format=jpg&visual_refresh=true' +
            `&markers=${encodeURIComponent(`size:mid|color:red|${practice.location.latitude},${practice.location.longitude}`)}` +
            `&key=${environment.google.staticMapsKey}`) : null;
    }

    public flush(): void {
        this.practice = null;
    }
}
