import {Component} from '@angular/core';
import {BookingService} from '../service/booking-service';

@Component({
    selector: 'payment-table',
    template: `
        <table class="table">
          <tr>
              <td>{{ 'wording.treatment' | translate }}</td>
              <td align="right">{{ charge | currency:'EUR':true:'1.2-2' }}</td>
          </tr>
          <tr>
              <td>{{ 'wording.tax' | translate }}</td>
              <td align="right">keine</td>
          </tr>
          <tr>
              <td>{{ 'payment.pay_later' | translate }}</td>
              <td align="right">{{ charge - toBePaidNow() | currency:'EUR':true:'1.2-2' }}</td>
          </tr>
          <tr>
              <td>{{ 'payment.pay_now' | translate }}</td>
              <td align="right">{{ toBePaidNow() | currency:'EUR':true:'1.2-2' }}</td>
          </tr>
      </table>
      <div *ngIf="bookingService.paidInAdvance()" class="text-right at-hidden-md">
            {{ 'payment.payment_method' | translate }}:<br />
            <payment-details></payment-details>
      </div>
      <div *ngIf="bookingService.paidInAdvance()" class="text-center at-visible-md">
            {{ 'payment.payment_method' | translate }}:<br />
            <payment-details></payment-details>
      </div>
    `
})
export class PaymentTableComponent
{
    constructor(public bookingService: BookingService) {
    }

    get charge(): number {
        return this.bookingService.treatment.charge.amount / 100;
    }

    public toBePaidNow(): number {
        if (this.bookingService.paidInAdvance()) {
            return this.charge;
        }
        return 0;
    }
}
