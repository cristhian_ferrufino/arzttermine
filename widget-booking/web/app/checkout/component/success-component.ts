import {Component, OnDestroy} from '@angular/core';
import {BookingService} from '../service/booking-service';
import {PaymentService} from '../../payment/service/payment-service';
import {Booking} from '../../model/booking';
import {PracticeService} from '../service/practice-service';
import {Practice} from '../../model/practice';

@Component({
    selector: 'confirmation',
    template: `
        <div class="at-success">
            <header class="at-success__header">
                <div class="at-grid">
                    <h1>{{ 'headline.done' | translate }}</h1>
                    {{ 'success.booking_id' | translate }}: <strong>#{{ booking.id }}</strong>
                </div>
            </header>
            <div class="at-success__body at-element--highlight text-left">
                <div class="at-grid">
                    <div><i class="fa fa-check-circle-o"></i> {{ 'hints.time_booked' | translate }}</div>
                    <div><i class="fa fa-check-circle-o"></i> {{ 'hints.confirmation_sent' | translate : {mail: booking.customer.details.email} }}.</div>
                    <div *ngIf="bookingService.treatment?.charge">
                        <div *ngIf="!booking.paymentInitialised"><i class="fa fa-times-circle-o"></i> {{ 'hints.no_payment_initialised' | translate }}</div>
                        <div *ngIf="!booking.paid && booking.paymentInitialised === true"><i class="fa fa-check-circle-o"></i> {{ 'hints.payment_initialised' | translate }}</div>
                        <div *ngIf="booking.paid"><i class="fa fa-check-circle-o"></i> {{ 'hints.payment_done' | translate }}</div>
                    </div>
                </div>
            </div>
            <div class="at-grid">
                <br />
                <section class="at-success__body text-left">
                    {{ 'success.completed' | translate : {customer: booking.customer.details.fullName} }}
                    <span [innerHTML]="'success.contact_us' | translate : {phone: practice.location.phone}"></span>
                    <br />
                    <h3 style="margin-top: 7px;">{{ 'headline.appointment_details' | translate }}:</h3>
                    <appointment-details [grid]="true" [appointment]="booking.appointment"></appointment-details>
                </section>
                <section class="at-success__body text-left">
                    <practice-details [grid]="true" [practice]="practices.getPractice()"></practice-details>
                    <br />
                </section>
                <section *ngIf="payments.details" class="at-success__body text-left">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <h3>{{ 'headline.payment_details' | translate }}</h3>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <payment-details></payment-details>
                        </div>
                    </div>
                </section>
                <section class="text-center" style="line-height: 210%;">
                    <span *ngIf="practice.config && practice.config.success && practice.config.success.text">{{ practice.config.success.text }}<br /></span>
                    <a *ngIf="practice.config && practice.config.success && practice.config.success.url" href="{{ practice.config.success.url }}" class="btn at-btn"> Weiter </a>
                    <button class="btn at-btn" [routerLink]="['/']"> {{ 'pages.to_beginning' | translate }} </button>
                </section>
                <br />
            </div>
        </div>
    `
})
export class SuccessComponent implements OnDestroy
{
    constructor(public bookingService: BookingService, public practices: PracticeService, public payments: PaymentService) {
    }

    get practice(): Practice {
        return this.practices.getPractice();
    }

    get booking(): Booking {
        return this.bookingService.result;
    }

    ngOnDestroy(): void {
        this.bookingService.result = null;
        this.bookingService.treatment = null;
        this.payments.flush();
    }
}
