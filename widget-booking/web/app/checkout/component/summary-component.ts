import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {BookingService} from '../service/booking-service';
import {steps} from '../../main/service/widget-service';
import {PracticeService} from '../service/practice-service';

@Component({
    selector: 'summary',
    template: `
    <layout>
        <form (submit)="createBooking()" class="at-summary" *ngIf="bookingService.appointment">
            <h2>{{ 'headline.close_to_done' | translate }}</h2>
            <div class="row at-overview">
                <div [ngClass]="{
                        'col-lg-6': bookingService.treatment.charge, 
                        'col-xs-12': !bookingService.treatment.charge, 
                        'col-md-12': true 
                    }">
                    {{ 'hints.check_details' | translate }}<br />
                    <br />
                    <div class="panel">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12">
                                <h4 translate>wording.appointment</h4>
                            </div>
                            <div class="col-sm-8 col-xs-12">
                                <appointment-details [appointment]="bookingService.appointment"></appointment-details>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12">
                                <h4 translate>wording.practice</h4>
                            </div>
                            <div class="col-sm-8 col-xs-12">
                                <practice-details [practice]="practices.getPractice()"></practice-details>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12">
                                <h4 translate>headline.personal_details</h4>
                            </div>
                            <div class="col-sm-8 col-xs-12">
                                <span>{{ 'patient' | translate : {gender: 'wording.short_' + customer.gender.toLowerCase() | translate, name: customer.firstName + ' ' + customer.lastName} }}</span>
                                <br />
                                {{ bookingService.bookingRequest.customer.email }}<br />
                                {{ 'hints.insured' | translate : {insurance: 'wording.insurances.' + bookingService.bookingRequest.customer.insuranceType.toLowerCase() | translate } }}<br />
                                <span *ngIf="!bookingService.bookingRequest.customer.returning">{{ 'wording.new_customer' | translate }}<br /></span>
                                <span *ngIf="bookingService.bookingRequest.customer.returning">{{ 'wording.existing_customer' | translate }}<br /></span>
                                <span *ngIf="bookingService.bookingRequest.customer.phone">{{ bookingService.bookingRequest.customer.phone }}<br /></span>
                                <span *ngIf="bookingService.bookingRequest.customer.phoneMobile">{{ bookingService.bookingRequest.customer.phoneMobile }}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 offset-lg-1 at-hidden-md">
                    <br />
                    <div *ngIf="bookingService.treatment?.charge" class="panel">
                        <h4 translate>headline.payment_details</h4>
                        <payment-table></payment-table>
                    </div>
                </div>
            </div>
            <div *ngIf="bookingService.treatment?.charge" class="at-visible-md">
                <br />
                <div class="row">
                    <div class="col-sm-4 col-xs-12">
                        <h4 translate>headline.payment_details</h4>
                    </div>
                    <div class="col-sm-8 col-xs-12">
                        <payment-table></payment-table>
                    </div>
                </div>
            </div>
            <br />
            <div class="text-center">
                <div class="at-fullscreen" [style.display]="loading ? 'block' : 'none'">
                    <div class="spinner">
                      <div class="rect1"></div>
                      <div class="rect2"></div>
                      <div class="rect3"></div>
                      <div class="rect4"></div>
                      <div class="rect5"></div>
                    </div>
                </div>
                <div *ngIf="errors.length" class="alert at-alert-danger text-left">
                    <ul style="margin: 0; padding: 0; list-style: none;">
                        <li *ngFor="let error of errors" translate>{{ error }}</li>
                    </ul>
                    <div *ngIf="showPracticePhone">{{ 'hints.further_support' | translate : {phone: practice.location.phone || practice.location.phoneMobile} }}</div>
                </div>
                <div class="at-divider"></div>
                <continue *ngIf="!loading" 
                          [previous]="getPreviousStep()"
                          [continueText]="getBookingText()"
                          [complete]="true"></continue>
            </div>
            <br />
            <label>
                <small [innerHTML]="'hints.agree_and_confirm_terms' | translate"></small>
            </label>
        </form>
    </layout>
    `
})
export class SummaryComponent
{
    public loading: boolean = false;
    public errors: string[] = [];
    public showPracticePhone: boolean = false;

    constructor(public bookingService: BookingService,
                public practices: PracticeService,
                private router: Router) {
    }

    get customer() {
        return this.bookingService.bookingRequest.customer;
    }

    get charge() {
        return this.bookingService.treatment.charge.amount / 100;
    }

    get practice() {
        return this.practices.getPractice();
    }

    public getBookingText(): string {
        if (this.bookingService.paidInAdvance()) {
            return 'booking.with_payment';
        } else {
            return 'booking.without_payment';
        }
    }

    public getPreviousStep(): any {
        return (<any> steps)[(this.bookingService.treatment.charge) ? 'payment' : 'patient'];
    }

    public createBooking(): void {
        // do not show loading screen if response is there within 30ms
        const timer = setTimeout(() => this.loading = true, 30);
        const request = this.bookingService.submit();
        this.showPracticePhone = false;

        if (null == request) {
            return;
        }

        request.take(1).subscribe(
            result => this.router.navigate(['success']),
            error => {
                clearTimeout(timer);
                this.loading = false;
                this.errors.length = 0;
                this.populateErrors(error.json());
            }
        );
    }

    private populateErrors(json: any): void {
        const isList: boolean = json instanceof Array;

        if (!isList) {
            // no clue how to treat this properly
            this.errors.push('booking.validation.server_error');
            return;
        }

        json.forEach((err: {code: string, reference: string}) => {
             switch (err.code) {
                 case 'ALREADY_BOOKED':
                    this.errors.push('booking.validation.already_booked');
                    break;
                 case 'INVALID_ARGUMENT':
                     this.populateNullsafe(this.findErrorInBadRequest(err));
                     break;
                 case 'ACCESS_DENIED':
                     this.errors.push('booking.validation.security_violation');
                     break;
                 case 'SERVICE_UNAVAILABLE':
                     this.errors.push('booking.validation.server_unavailable');
                     break;
                 case 'PAYMENT_REQUIRED':
                     if (err.reference === 'paymentFailure') {
                         this.errors.push('booking.validation.payment_failed');
                         this.showPracticePhone = true;
                     } else {
                         this.errors.push('booking.validation.payment_required');
                     }
                     break;
                 case 'TREATMENT_TYPE_NOT_SUPPORTED':
                     this.errors.push('booking.validation.invalid_treatment');
                     break;
                 case 'APPOINTMENT_NOT_AVAILABLE':
                     this.errors.push('booking.validation.invalid_appointment');
                     break;
                 case 'INVALID_INSURANCE':
                 case 'LANGUAGE_NOT_SUPPORTED':
                     this.errors.push('booking.validation.invalid_patient');
                     break;
                 default:
                     if (this.errors.length < 0)
                         this.errors.push('booking.validation.server_error');
                     break;
             }
        });
    }

    private findErrorInBadRequest(err: {code: string, reference: string}): string {
        const plainReference = err.reference.replace('customer.', ''); // treat them all on the same nested level

        if (plainReference === 'password') {
            return 'security_violation';
        } else if (['gender', 'firstName', 'lastName'].indexOf(plainReference) > -1) {
            return 'invalid_personal_data';
        } else if ('email' === plainReference) {
            return 'invalid_email';
        } else if (['phoneMobile', 'phone'].indexOf(plainReference) > -1) {
            return 'invalid_phone';
        } else {
            return null;
        }
    }

    private populateNullsafe(errorKey: string): void {
        if (null === errorKey) {
            return;
        }
        this.errors.push('booking.validation.' + errorKey);
    }
}
