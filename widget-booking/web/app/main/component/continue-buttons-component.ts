import {Component, Input} from '@angular/core';
import {Router} from '@angular/router';

@Component({
    selector: 'continue',
    template: `
        <div class="text-center">
            <button *ngIf="previous"
                    type="button"
                    tabindex="-1"
                    (click)="router.navigate([previous.path])"
                    class="btn btn-ghost float-left">
                <strong>&lt;</strong> {{ 'wording.back' | translate }}
            </button>
            <button [attr.disabled]="canContinue() ? null : ''"
                    type="submit"
                    [ngClass]="{
                        'btn': true,
                        'float-right': true,
                        'at-btn': canContinue(),
                        'at-btn-outline': !canContinue(),
                        'disabled': !canContinue()
                    }">
                <span>{{ getText() | translate }}</span>
            </button>
        </div>
    `,
    host: {
        'class': 'at-form__footer'
    }
})
export class ContinueButtonsComponent
{
    @Input() public granted: () => boolean;
    @Input() public previous: {path: string};
    @Input() public complete: boolean;
    @Input() public continueText: string;

    constructor(public router: Router) {
    }

    public getText(): string {
        return (this.continueText) ? this.continueText : 'wording.continue';
    }

    public canContinue(): boolean {
        if (typeof this.granted === 'function') {
            return this.granted();
        }
        return this.complete;
    }
}
