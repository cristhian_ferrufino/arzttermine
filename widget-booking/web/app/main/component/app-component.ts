import {Component} from '@angular/core';
import {BookingService} from '../../checkout/service/booking-service';

@Component({
    selector: 'atwidgetmain',
    template: `
        <div class="at-widget-ns">
            <div [ngClass]="{'at-widget': true, 'container-fluid': !bookingService.result }">
                <router-outlet></router-outlet>
            </div>
        </div>
    `
})
export class AppComponent
{
    constructor(public bookingService: BookingService) {
    }
}
