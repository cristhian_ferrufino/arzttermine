import {Component} from '@angular/core';
import {BookingService} from '../../checkout/service/booking-service';
import {WidgetService} from '../service/widget-service';

@Component({
    selector: 'menu',
    template: `
        <div tabindex="1" (click)="widget.goTo('home')" [ngClass]="{
                'at-progress-circle': true, 
                'at-progress-done': widget.isDone('home') && !widget.isActive('home'), 
                'at-progress-active': widget.isActive('home') 
            }">
            <span class="at-progress-label">1</span>
            <span class="at-progress-title" translate>menu.appointment</span>
        </div>
        <span [ngClass]="{
            'at-progress-bar': true, 
            'at-progress-done': widget.isDone('home')
        }"></span>
        <div tabindex="2" (click)="widget.goTo('patient')" [ngClass]="{
                'at-progress-circle': true, 
                'at-progress-done': widget.isDone('patient') && !widget.isActive('patient'), 
                'at-progress-active': widget.isActive('patient') 
            }">
            <span class="at-progress-label">2</span>
            <span class="at-progress-title" translate>menu.personal_data</span>
        </div>
        <span [ngClass]="{
            'at-progress-bar': true, 
            'at-progress-done': widget.isDone('patient')
        }"></span>
        <div tabindex="3" *ngIf="bookingService.treatment?.charge" (click)="widget.goTo('payment')" [ngClass]="{
                'at-progress-circle': true, 
                'at-progress-done': widget.isDone('payment') && !widget.isActive('payment'), 
                'at-progress-active': widget.isActive('payment') 
            }">
            <span class="at-progress-label">3</span>
            <span class="at-progress-title" translate>menu.payment</span>
        </div>
        <span *ngIf="bookingService.treatment?.charge" [ngClass]="{
            'at-progress-bar': true, 
            'at-progress-done': widget.isActive('summary') || bookingService.isComplete()
        }"></span>
        <div [tabindex]="(bookingService.treatment && bookingService.treatment.charge) ? 4 : 3" (click)="widget.goTo('summary')" [ngClass]="{
                'at-progress-circle': true,  
                'at-progress-active': widget.isActive('summary')
            }">
            <span class="at-progress-label" *ngIf="!bookingService.treatment?.charge">3</span>
            <span class="at-progress-label" *ngIf="bookingService.treatment?.charge">4</span>
            <span class="at-progress-title" translate>menu.overview</span>
        </div>
    `,
    host: {
        'class': 'at-menu'
    }
})
export class MenuComponent
{
    constructor(public bookingService: BookingService, public widget: WidgetService) {
    }
}
