import {Component, Input, OnInit, Inject} from '@angular/core';
import {RestoreUtility} from '../util/restore-utility';
import {LocalStorage} from "../util/local-storage";

@Component({
    selector: 'two-col-button',
    template: `
        <div class="row">
            <div class="col-sm-6 col-xs-12">
                <div class="at-tooltip">
                    <span *ngIf="buttons.btn1.disabled" class="at-tooltip-text">
                        {{buttons.btn1.disabledTooltipText}}
                    </span>
                    <button type="button"
                            (click)="select('btn1')"
                            [disabled]="buttons.btn1.disabled"
                            [ngClass]="{
                            'btn': true,
                            'at-two-col-btn': true,
                            'at-btn-outline': !buttons.btn1.active,
                            'at-btn': buttons.btn1.active,
                            'active': buttons.btn1.active
                        }">
                        {{ buttons.btn1.text | translate }}
                    </button>
                </div>
            </div>
            <div class="col-sm-6 col-xs-12">
                <div class="at-tooltip">
                    <span *ngIf="buttons.btn2.disabled" class="at-tooltip-text">
                        {{buttons.btn2.disabledTooltipText}}
                    </span>
                    <button type="button"
                            (click)="select('btn2')"
                            [disabled]="buttons.btn2.disabled"
                            [ngClass]="{
                            'btn': true,
                            'at-two-col-btn': true,
                            'at-btn-outline': !buttons.btn2.active,
                            'at-btn': buttons.btn2.active,
                            'active': buttons.btn2.active
                        }">
                        {{ buttons.btn2.text | translate }}
                    </button>
                </div>
            </div>
        </div>
    `,
    styles: [`
        .at-two-col-btn {
            width: 99.5%;
            text-align: center;
        }
    `],
    host: {
        'class': 'at-two-col'
    }
})
export class TwoColumnButtonComponent extends RestoreUtility implements OnInit
{
    @Input() public buttons: {btn1: Button, btn2: Button};
    @Input() public key: string;

    constructor(storage: LocalStorage) {
        super(storage);
    }

    public select(btn: string, persist?: boolean): void {
        const current = Object.keys(this.buttons)
            .filter(k => this.buttons[k] !== null)
            .find(k => this.buttons[k].active);

        const activate: Button = this.buttons[btn];

        if (activate && btn !== current) {
            if (current) {
                this.buttons[current].active = false;
            }

            activate.active = true;

            if (persist !== true)
                this.persistChoice(btn);

            if (typeof activate.onClick === 'function') {
                activate.onClick(btn, activate);
            }
        }
    }

    ngOnInit(): void {
        // If active is set on any button, ignore local store
        if (!this.buttons.btn1.active && !this.buttons.btn2.active) {
            this.onInit(this.buttons);
        } else if (this.buttons.btn1.active) {
            this.buttons.btn1.onClick(this.buttons.btn1.value, this.buttons.btn1);
        } else if (this.buttons.btn2.active) {
            this.buttons.btn2.onClick(this.buttons.btn2.value, this.buttons.btn2);
        }
    }
}

export interface Button {
    text: string;
    active?: boolean;
    value?: string|number|boolean;
    onClick?: (btn: string|number|boolean, button: Button) => void;
    disabled?: boolean;
    disabledTooltipText?: string;
}
