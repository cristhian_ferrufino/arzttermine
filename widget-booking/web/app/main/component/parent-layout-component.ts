import {Component, OnInit, NgZone, ElementRef} from '@angular/core';
import {PracticeService} from '../../checkout/service/practice-service';
import {Practice} from '../../model/practice';
import {BookingService} from '../../checkout/service/booking-service';
import {environment} from '../../../environments/environment';
import {WidgetService} from '../service/widget-service';
import {TranslateService} from "@ngx-translate/core";

@Component({
    selector: 'layout',
    template: `
        <div *ngIf="practice" [style.maxWidth.px]="maxWidth">
            <menu *ngIf="!bookingService.result"></menu>
            <br />
            <ng-content></ng-content>
        </div>
        <div *ngIf="!practice || errored" class="alert at-alert-warning" translate>errors.service_outage</div>
    `
})
export class ParentLayoutComponent implements OnInit
{
    maxWidth: number = 6000;

    constructor(public bookingService: BookingService,
                private practiceService: PracticeService,
                private translator: TranslateService,
                private widget: WidgetService,
                private element: ElementRef,
                private _zone: NgZone) {
    }

    get practice(): Practice {
        return this.practiceService.getPractice();
    }

    get errored(): boolean {
        return this.practiceService.hasError();
    }

    ngOnInit(): void {
        this.detectLocale();
        this.requestStyleIfNecessary();
        this.initFontAwesome();
        this.onWindowResize();

        window.onresize = () => {
            this.onWindowResize();
        };
    }

    private detectLocale(): void {
        this.translator.setDefaultLang('de');
        this.translator.use(this.translator.getBrowserLang() || 'de');
    }

    private requestStyleIfNecessary(): void {
        if (!this.widget.isPresent('style')) {
            this.widget.requested('style');

            this.practiceService.loading.take(1).subscribe((practice: Practice) => {
                this._zone.run(() => {
                    const stylesheetPath: string = `${environment.external.s3Link}/${environment.external.buckets.styles}/`;
                    const templateId = window.at_style_template || practice.config.templateId || 'default';

                    const tag: HTMLLinkElement = document.createElement('link');
                    tag.type = 'text/css';
                    tag.rel = 'stylesheet';
                    tag.href = stylesheetPath + templateId + '.min.css';
                    (document.head || document.getElementsByTagName('head')[0]).appendChild(tag);
                });
            });
        }
    }

    private onWindowResize(): void {
        this._zone.run(() => {
            this.maxWidth = window.innerWidth - 30;
        });
    }

    private initFontAwesome(): void {
        if (!this.widget.isPresent('fontawesome')) {
            this.widget.requested('fontawesome');

            const fontA: HTMLScriptElement = document.createElement('script');
            fontA.src = 'https://use.fontawesome.com/f7e3e96c18.js';
            fontA.async = true;

            this.element.nativeElement.appendChild(fontA);
        }
    }
}
