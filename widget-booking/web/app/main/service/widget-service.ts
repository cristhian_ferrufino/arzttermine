import {Injectable} from '@angular/core';
import {Router, NavigationEnd} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import {appointment, payment, patient, success, summary} from '../../routing/step.validator';
import {BookingService} from '../../checkout/service/booking-service';

export const steps: Map<WidgetStep> = {
    home: { path: 'appointment', optional: false, active: true, valid: appointment },
    patient: { path: 'patient', optional: false, valid: patient},
    payment: { path: 'payment', optional: true, valid: payment },
    summary: { path: 'summary', optional: false, valid: summary },
    success: { path: 'success', optional: false, valid: success }
};

const keySet = Object.keys(steps);

@Injectable()
export class WidgetService
{
    private subscription: Subscription;
    private externalSources = new Map<String, Boolean>();

    constructor(private router: Router, private bookingService: BookingService) {
        this.subscription = this.router.events.subscribe((event) => {
            if (event instanceof NavigationEnd) {
                keySet.forEach(s => {
                    steps[s].active = event.url.endsWith(steps[s].path);
                });
            }
        });
    }

    public isActive(page: string): boolean {
        return steps[page].active;
    }

    public requested(source: string): void {
        this.externalSources.set(source, true);
    }

    public isPresent(source: string): boolean {
        return this.externalSources.get(source) === true;
    }

    public isDone(page: string): boolean {
        return keySet.filter(k => k === page)
            .some(k => steps[k].valid(this.bookingService));
    }

    public goTo(page: string): void {
        this.router.navigate([steps[page].path]);
    }

    private flush(): void {
        this.externalSources.clear();
        if (this.subscription && !this.subscription.closed) {
            this.subscription.unsubscribe();
        }
    }
}

export interface WidgetStep {
    path: string;
    optional?: boolean;
    active?: boolean;
    valid: (bookingService: BookingService) => boolean;
}

interface Map<T> {
    [key: string]: T;
}
