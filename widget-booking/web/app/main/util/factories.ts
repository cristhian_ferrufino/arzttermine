import {LocalStorage} from './local-storage';
import {TranslateLoader, TranslateService} from '@ngx-translate/core';
import {TranslationSearchService} from '../../search/service/translation-service';

export function localStorageFactory(): LocalStorage {
    try {
        const result: any = window.localStorage.getItem('at_availability_test');
        if (!result) {
            window.localStorage.setItem('at_availability_test', 'works');
        }

        return window.localStorage as LocalStorage;
    } catch (e) {
        return new LocalStorage();
    }
}

export function translationServiceFactory(): TranslateLoader {
    return new TranslationSearchService();
}

export function localeFactory(service: TranslateService): string {
    return service.getBrowserLang();
}
