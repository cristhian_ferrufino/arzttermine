import {LocalStorage} from "./local-storage";

export abstract class RestoreUtility
{
    public key: string;

    constructor(protected storage: LocalStorage, key?: string) {
        this.key = key;
    }

    protected abstract select(choice: any, noPersist?: boolean): void;

    protected onInit(values: any): void {
        const choice: string = this.reloadChoice();

        if (choice && values[choice]) {
            this.select(choice, true);
        }
    }

    public persistChoice(value: any): void {
        this.storage.setItem('at_' + this.key, String(value));
    }

    private reloadChoice(): string {
        return this.storage.getItem('at_' + this.key);
    }
}
