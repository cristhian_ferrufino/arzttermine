import {Component, ViewChild, OnInit, Inject} from '@angular/core';
import {BookingService} from '../../checkout/service/booking-service';
import {steps, WidgetService} from '../../main/service/widget-service';
import {RestoreUtility} from '../../main/util/restore-utility';
import {NgForm, NgModel} from '@angular/forms';
import {LocalStorage} from "../../main/util/local-storage";

@Component({
    selector: 'patient',
    template: `
    <layout>
    <form (submit)="nextStep()" autocomplete="on" #patient="ngForm" >
        <div class="at-grid--small">
            <div [ngClass]="{'form-group': true,
                            'has-danger': isInvalid(gender)}">
                <label for="patientGender">{{ 'wording.gender' | translate }}</label>
                <select class="form-control" 
                        id="patientGender" 
                        name="gender"
                        #gender="ngModel"
                        required
                        tabindex="5"
                        [(ngModel)]="bookingService.bookingRequest.customer.gender"
                        (change)="persistGender($event.target.value)"
                        autocomplete="sex">
                    <option disabled selected>{{ 'wording.please_choose' | translate }}</option>
                    <option *ngFor="let g of sex"
                            [value]="g.key"
                            [attr.selected]="g.key === bookingService.bookingRequest.customer.gender ? '' : null">
                            {{ g.value | translate }}
                    </option>
                </select>
                <div class="form-control-feedback" [hidden]="!isInvalid(gender)">
                    <small>
                        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> 
                        {{ 'form.required' | translate : {field: 'wording.gender' | translate} }}
                    </small>
                </div>
            </div>
            <div [ngClass]="{'form-group': true,
                        'has-danger': isInvalid(firstName)}">
                <label for="patientFirstName">{{ 'wording.first_name' | translate }}</label>
                <input type="text" 
                       name="firstName"
                       #firstName="ngModel"
                       class="form-control" 
                       placeholder="{{ 'wording.first_name' | translate }}"
                       id="patientFirstName"
                       required
                       minlength="2"
                       tabindex="6"
                       autofocus
                       autocomplete="given-name"
                       [(ngModel)]="bookingService.bookingRequest.customer.firstName"
                       [ngClass]="{'form-control': true,
                            'form-control-danger': isInvalid(firstName)}">
                <div class="form-control-feedback" [hidden]="!isInvalid(firstName)">
                    <small>
                        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                        {{ 'form.required' | translate : {field: 'wording.first_name' | translate} }}
                    </small>
                </div>
            </div>
            <div [ngClass]="{'form-group': true,
                            'has-danger': isInvalid(lastName)}">
                <label for="patientLastName">{{ 'wording.last_name' | translate }}</label>
                <input type="text" 
                       name="lastName"
                       #lastName="ngModel"
                       class="form-control" 
                       placeholder="{{ 'wording.last_name' | translate }}"
                       id="patientLastName"
                       required
                       minlength="2"
                       tabindex="7"
                       autocomplete="family-name"
                       [(ngModel)]="bookingService.bookingRequest.customer.lastName"
                       [ngClass]="{'form-control': true,
                            'form-control-danger': isInvalid(lastName)}">
                <div class="form-control-feedback" [hidden]="!isInvalid(lastName)">
                    <small>
                        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                        {{ 'form.required' | translate : {field: 'wording.last_name' | translate} }}
                    </small>
                </div>
                
            </div>
            <div [ngClass]="{'form-group': true,
                            'has-danger': isInvalid(email)}">
                <label for="patientEMail">{{ 'wording.e_mail' | translate }}</label>
                <input type="email" 
                       name="email"
                       #email="ngModel"
                       class="form-control" 
                       placeholder="mail@example.com"
                       id="patientEMail"
                       minlength="4"
                       required
                       tabindex="8"
                       autocomplete="email"
                       [(ngModel)]="bookingService.bookingRequest.customer.email"
                       [ngClass]="{'form-control': true,
                            'form-control-danger': isInvalid(email)}">
                <div class="form-control-feedback" [hidden]="!isInvalid(email)">
                    <small>
                        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                        {{ 'form.required' | translate : {field: 'wording.e_mail' | translate} }}
                    </small>
                </div>
               
            </div>
            <div [ngClass]="{'form-group': true,
                            'has-danger': isInvalid(phone)}">
                <label for="patientPhoneMobile">
                    {{ 'wording.phone_number' | translate }}<br />
                    <small>{{ 'hints.suggest_mobile' | translate }}</small>
                </label>
                <input type="tel" 
                       name="phone"
                       #phone="ngModel"
                       class="form-control" 
                       placeholder="017600000000"
                       id="patientPhoneMobile"
                       tabindex="10"
                       required
                       autocomplete="tel"
                       pattern="[\\(]?[+49,0][\\)]?([\\(]?\\d{2}[\\)]?)?\\d{4,6}[\\-]?\\d{4,6}"
                       [(ngModel)]="bookingService.bookingRequest.customer.phoneMobile"
                       [ngClass]="{'form-control': true,
                            'form-control-danger': isInvalid(phone)}">
                <div class="form-control-feedback" [hidden]="!isInvalid(phone)">
                    <small>
                        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                        {{ 'form.required' | translate : {field: 'form.phone_number' | translate} }}
                    </small>
                </div>
                
            </div>
        </div>
        <br style="clear: both;" />
        <div class="at-divider"></div>
        <continue [complete]="patientForm.valid"
                  [continueText]="getNextPageHint()"
                  [previous]="previous"></continue>
    </form>
    </layout>
    `
})
export class PatientComponent extends RestoreUtility implements OnInit
{
    key: string = 'patient_sex';

    @ViewChild('patient') public patientForm: NgForm;

    public previous = (<any> steps).home;
    public sex: {key: string, value: string}[] = [{
        key: 'MALE',
        value: 'wording.male'
    }, {
        key: 'FEMALE',
        value: 'wording.female'
    }];


    constructor(public bookingService: BookingService, private widget: WidgetService, @Inject(LocalStorage) storage: LocalStorage) {
        super(storage);
    }

    ngOnInit(): void {
        this.onInit({'MALE': 1, 'FEMALE': 2});
    }

    public isInvalid(control: NgModel): boolean {
        return !control.valid && control.touched;
    }

    public persistGender(choice: string): void {
        this.persistChoice(choice)
    }

    public getNextPageHint(): string {
        return (this.bookingService.treatment.charge) ? 'pages.to_payment' : 'pages.to_summary';
    }

    protected select(choice: 'MALE'|'FEMALE'): void {
        this.bookingService.bookingRequest.customer.gender = choice;
    }

    public nextStep(): void {
        if (this.bookingService.treatment && this.bookingService.treatment.charge) {
            this.widget.goTo((<any> steps).payment.path);
        } else {
            this.widget.goTo((<any> steps).summary.path);
        }
    }
}
