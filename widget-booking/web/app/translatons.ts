// We keep it simple for now, adding only an abstraction layer to define and use translations.
// We want to avoid network outages or something similar to prevent us getting translations.
// Also having auto-complete in IDEA and in-time updates is easier to work with.
// But this also implies a bigger build size and no yaml format (which would fit better)
// This is something for optimisations later on. (Fetch from remote, maybe from our search having a backend service to configure translations)

export const translations: { de: any } = {
    de: {
        headline: {
            book_here: 'Buchen Sie jetzt online einen Termin',
            credit_card_info: 'Kreditkarteninformationen',
            costs_overview: 'Kostenübersicht',
            payment_details: 'Zahlungsdaten',
            personal_details: 'persönliche Daten',
            appointment_details: 'Termindetails',
            close_to_done: 'Fast geschafft!',
            done: 'Geschafft!',
            personal_appointment: 'Ihr persönlicher Termin'
        },
        appointment: {
            choose_first_visit: 'Ist dies Ihr erster Besuch in unserer Praxis?',
            choose_insurance: 'Wie sind Sie versichert?',
            choose_doctor: 'Bei welchem Arzt möchten Sie den Termin buchen?',
            choose_treatment: 'Für welche Behandlung möchten Sie uns besuchen?',
            change_appointment: 'Anderen Termin wählen',
            continue_to_book: 'Fahren Sie fort um diesen Termin zu buchen',
            next_available: 'Nächster freier Termin',
            no_found: 'Es wurden im angegebenen Zeitraum keine Termine gefunden. Bitte versuchen Sie eine andere Auswahl!'
        },
        calendar: {
            month: 'Monat',
            week: 'Woche',
            today: 'Heute',
        },
        payment: {
            pay_in_advance_or_later: 'Sie können diese direkt unter Schritt 3 online bezahlen oder direkt vor Ort in der Praxis.',
            pay_in_advance: `
                Um diesen Termin wahrnehmen zu können, müssen Sie unter Schritt 3 zuerst die Zahlung einleiten. 
                Eine verbindliche Buchungszusage entsteht sobald Sie die Rechnung erhalten haben.
            `,
            card_owner: 'Karteninhaber',
            card_number: 'Kartennummer',
            expire_date: 'Ablaufdatum',
            card_cvc: 'CVC',
            your_details: 'Deine Zahlungsdaten',
            pay_now: 'Jetzt zu zahlen',
            pay_later: 'In der Praxis zu zahlen',
            continue_with: 'Weiter mit Zahlung',
            continue_without: 'Weiter ohne Zahlung',
            no_payment: 'ohne Zahlung fortfahren',
            force_pay: 'Bezahlen Sie Ihre Behandlung im Voraus.',
            payment_method: 'Zu belastendes Zahlungsmittel',
            types: {
                credit_card: 'Kreditkarte'
            },
            validation: {
                validating: 'Überprüfe Zahlungsinformationen...',
                invalid_number: 'Die eingegebene Nummer ist keine gültige Kreditkartennummer.',
                invalid_expiry_month: 'Das eingegebene Ablaufdatum der Karte ist nicht gültig.',
                invalid_expiry_year: 'Das eingegebene Ablaufdatum der Karte ist nicht gültig.',
                invalid_cvc: 'Der eingegebene CVC ist nicht gültig.',
                invalid_swipe_data: 'Swipe Daten sind nicht gültig.',
                incorrect_number: 'Die eingegebene Nummer ist keine gültige Kreditkartennummer.',
                incorrect_cvc: 'Der CVC ist für diese Kreditkarte nicht gültig.',
                incorrect_zip: 'Der Postcode ist nicht gültig mit dieser Kreditkarte.',
                expired_card: 'Deine Kreditkarte ist nicht mehr aktiv bzw. bereits abgelaufen.',
                card_declined: 'Deine Kreditkarte wurde leider nicht akzeptiert.',
                missing: '',
                processing_error: 'Oups, da ist etwas schief gelaufen. Versuche es nochmal.',
                too_many_requests: 'Aktuell können wir deine Anfrage leider nicht verarbeiten. Versuche es später erneut oder fahre ohne Zahlung fort.'
            }
        },
        booking: {
            payment_required: 'Für diese Buchung fallen Gebühren an.',
            with_payment: 'Jetzt kostenpflichtig buchen',
            without_payment: 'Jetzt kostenlos buchen',
            validation: {
                server_error: 'Oups! Da ist etwas schief gelaufen. Bitte versuche es später erneut.',
                server_unavailable: 'Oups! Der Dienst scheint aktuell nicht verfügbar zu sein.',
                already_booked: 'Dieser Termin wurde leider bereits von einem anderen Patienten gebucht.',
                security_violation: 'Du bist nicht berechtigt mit diesem Kundenkonto eine Termin zu buchen.',
                invalid_personal_data: 'Deine persönlichen Daten sind nicht gültig. Bitte prüfe sie erneut!',
                payment_required: 'Um diesen Termin zu buchen, musst du zuerst eine Zahlung einleiten.',
                payment_failed: 'Die Zahlung konnte leider nicht erfolgreich authorisiert werden.',
                invalid_treatment: 'Diese Behandlung ist in Kombination mit dem ausgewählten Termin leider nicht verfügbar.',
                invalid_appointment: 'Diese Behandlung ist in Kombination mit dem ausgewählten Termin leider nicht verfügbar.',
                invalid_patient: 'Leider bietet dieser Doktor diesen Termin nicht für deinen Versicherungstyp oder in deiner Sprache an.',
                invalid_phone: 'Die angegebene Telefonnummer ist nicht gültig.',
                invalid_email: 'Die angegebene E-Mail Adresse ist nicht gültig.',
            }
        },
        form: {
            check_payment_data: 'Zahlungsdaten prüfen',
            phone_number: 'Telefon- oder Mobilfunknummer',
            required: '{{field}} ist erforderlich.'
        },
        menu: {
            appointment: 'Termin',
            personal_data: 'Daten',
            payment: 'Zahlung',
            overview: 'Übersicht',
        },
        wording: {
            address: 'Adresse',
            please_choose: 'Bitte wählen',
            notice: 'Hinweis',
            clock: 'Uhr',
            at: 'am',
            by: 'um',
            duration: 'Dauer',
            minutes: 'Minuten',
            with: 'bei',
            yes: 'Ja',
            no: 'Nein',
            none: 'Keine',
            continue: 'Weiter',
            back: 'Zurück',
            treatment: 'Behandlung',
            tax: 'Mehrwertsteuer',
            gender: 'Geschlecht',
            first_name: 'Vorname',
            last_name: 'Nachname',
            e_mail: 'E-Mail',
            phone_number: 'Telefonnummer',
            male: 'Männlich',
            female: 'Weiblich',
            short_male: 'Herr',
            short_female: 'Frau',
            new_customer: 'Neukunde',
            existing_customer: 'Bestandspatient',
            practice: 'Praxis',
            appointment: 'Termin',
            insurances: {
                private: 'Privat',
                compulsory: 'Gesetzlich'
            }
        },
        pages: {
            to_summary: 'Weiter zur Bestätigung',
            to_payment: 'zur Zahlungsseite',
            to_beginning: 'Zum Anfang'
        },
        hints: {
            insured: '{{insurance}} versichert',
            suggest_mobile: 'Wir bevorzugen Mobilfunknummern',
            time_booked: 'Termin wurde reserviert',
            confirmation_sent: 'Bestätigung wurde versand an {{mail}}',
            no_payment_initialised: 'Zahlung wurde nicht eingeleitet',
            payment_initialised: 'Zahlung wurde erfolgreich eingeleitet',
            payment_done: 'Zahlung wurde erfolgreich durchgeführt',
            further_support: 'Bitte kontaktieren Sie uns für weitere Unterstützung unter {{phone}}',
            agree_and_confirm_terms: `
                Mit dem Klick auf "Jetzt buchen" stimmst du unseren <a href="https://arzttermine.de/agb" target="_blank">Nutzungs-</a> 
                und <a href="https://arzttermine.de/datenschutz" target="_blank">Datenschutzbestimmungen</a> zu.
                Die Buchung ist verbindlich, kann jedoch storniert werden.
            `,
            check_details: 'Bevor Sie Ihre Buchung abschicken, prüfen Sie bitte vorher nochmal sorgfältig alle folgenden Daten:',
        },
        success: {
            booking_id: 'Buchungs-Nr.',
            completed: 'Danke {{customer}} für Ihre Buchung.',
            contact_us: 'Bei Problemen zögern Sie nicht, uns unter der Nummmer <strong>{{phone}}</strong> zu kontaktieren.',
        },
        errors: {
            service_outage: 'Leider ist ein Fehler aufgetreten. Eine Terminbuchung kann derzeit nicht vorgenommen werden. Bitte versuchen Sie es später erneut.',
        },
        you_can_also: 'Du kannst auch',
        patient: '{{gender}} {{name}}',
        medicalSpecialties: {
            ALLERGY_AND_IMMUNOLOGY: 'Allergie und Immunologie',
            SPINAL_SURGERY: 'Wirbensäulenoperationen',
            CARDIOTHORACIC_SURGERY: 'Cardiothoracische Chirugie',
            ORAL_AD_MAXILLOFACIAL_SURGERY: 'Kiefer- und Gesichtschirugie',
            PSYCHOTHERAPY: 'Psychotherapie',
            NEUROPSYCHOTHERAPY: 'Neuropsychotherapie',
            NEUROLOGY: 'Neurologie',
            CARDIOLOGY: 'Kardiologie',
            NEUROSURGERY: 'Neurochirurgie',
            ORTHOPAEDICS: 'Orthopädie',
            ORTHODONTICS: 'Kieferortopädie',
            PSYCHIATRY: 'Psychatrie',
            RADIOLOGY: 'Radiology',
            SPORTS_MEDICINE: 'Sportmedizin',
            NEURORADIOLOGY: 'Neuroradiologie',
            UROLOGY: 'Urologie',
            RHEUMATOLOGY: 'Rheumatherapie',
            GYNECOLOGIST: 'Gynäkologie',
            PEDIATRICIAN: 'Kinderarzt',
            DENTIST: 'Zahnmedizin',
            PATHOLOGY: 'Pathalogie',
            DIABETOLOGIST: 'Diabetologie',
            AESTHETICS: 'Ästhetik',
            OPHTHALMOLOGIST: 'Augenarzt',
            FAMILY_DOCTOR: 'Hausarzt',
            DERMATOLOGST: 'Dermatologie',
            IMPLANTOLOGY: 'Implantologie',
            NUTRITIONAL_MEDICINE: 'Ernährungsmedizin',
            PREVENTIVE_MEDICINE: 'Präventivmedizin',
            NATUROPATHY: 'Naturheilkunde',
            SPEECH_THERAPY: 'Logopädie',
            LASER_DENTISTRY: 'Laserheilkunde',
            NONE: 'Keine Fachrichtung',
        }
    }
};
