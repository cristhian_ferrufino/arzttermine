import {TreatmentTypeDto, TreatmentType} from './treatment-type';
import {LocationDto} from './location';
import {DoctorDto} from './doctor';

export interface PracticeDto
{
    id: number;
    name: string;
    location: LocationDto;
    doctors: DoctorDto[];
    medicalSpecialties: string[];
    treatmentTypes: TreatmentTypeDto[];
    widgetConfiguration?: WidgetConfigurationDto;
    insuranceTypes: string[];
}

export class Practice
{
    public id: number;
    public name: string;
    public location: LocationDto;
    public medicalSpecialties: string[];
    public treatmentTypes: TreatmentType[];
    public config: WidgetConfigurationDto;
    public doctors: DoctorDto[];
    public insuranceTypes: string[];

    constructor(dto?: PracticeDto) {
        if (!dto) return;

        this.id = dto.id;
        this.name = dto.name;
        this.location = dto.location;
        this.config = dto.widgetConfiguration || {};
        this.medicalSpecialties = dto.medicalSpecialties;
        this.treatmentTypes = dto.treatmentTypes.map(t => new TreatmentType(t));
        this.doctors = dto.doctors;
        this.insuranceTypes = dto.insuranceTypes || ['COMPULSORY', 'PRIVATE'];
    }
}

export interface WidgetConfigurationDto {
    templateId?: string;
    success?: {
        text?: string;
        url?: string;
    }
}
