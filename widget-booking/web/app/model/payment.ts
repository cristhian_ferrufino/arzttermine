export interface PaymentDetails {
    type: 'CREDIT_CARD';
    card: CreditCardDetails;
}

export interface CreditCardDetails {
    last4: string;
    exp_month: number;
    exp_year: number;
    brand: string;
    name: string;
}
