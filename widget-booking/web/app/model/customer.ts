export interface CustomerDto
{
    id?: number;
    email: string;
    firstName: string;
    lastName: string;
    insuranceType: string;
    phone: string;
    phoneMobile: string;
    language?: string;
    enabled?: boolean;
    fullName?: string;
    gender: 'MALE'|'FEMALE';
}
