import {TreatmentTypeDto} from './treatment-type';
import {CustomerDto} from './customer';

export interface DoctorDto extends CustomerDto
{
    treatmentTypes: TreatmentTypeDto[];
    medicalSpecialties: string[];
    bookingTimeOffset: number;
    bookable: boolean;
}
