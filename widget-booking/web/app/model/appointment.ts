import {TreatmentTypeDto} from './treatment-type';
import {DoctorDto} from './doctor';
import {PracticeDto} from './practice';

export class Appointment
{
    public id: number;
    public start: Date;
    public end: Date;
    public duration: number;
    public enabled: boolean;
    public booked: boolean;
    public doctor: DoctorDto;
    public practice: PracticeDto;
    public availableInsuranceTypes: string[];
    public availableLanguages: string[];
    public availableTreatmentTypes: TreatmentTypeDto[];
    public  innerWidth: any;

    constructor(dto?: AppointmentDto) {
        if (!dto) return;

        this.id = dto.id;
        this.duration = dto.duration;
        this.enabled = dto.enabled;
        this.booked = dto.booked;
        this.doctor = dto.doctor;
        this.practice = dto.practice;
        this.availableInsuranceTypes = dto.availableInsuranceTypes;
        this.availableTreatmentTypes = dto.availableTreatmentTypes;
        this.availableLanguages = dto.availableLanguages;
        this.start = new Date(dto.start);
        this.end = new Date(dto.end);
    }

    public toTimingString(): string {
        this.innerWidth = (window.screen.width);
        if (this.innerWidth >= 530){
            return `${this.start.toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'})} - ${this.end.toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'})}`;
        }else{
            return `${this.start.toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'})}`;
        }
    }

    public toDateTimeString(locale: string): string {
        return this.start.toLocaleString(locale, {
            hour: '2-digit',
            minute:'2-digit',
            year: 'numeric',
            month: 'short',
            day: '2-digit'
        });
    }

    public toStartDateString(): string {
        return `${this.start.toLocaleDateString([], {year: 'numeric', month:'2-digit', day:'2-digit'})}`;
    }

    public toStartTimeString(): string {
        return `${this.start.toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'})}`;
    }

}

export interface AppointmentDto
{
    id: number;
    start: string;
    end: string;
    duration: number;
    enabled: boolean;
    booked: boolean;
    doctor: DoctorDto;
    practice: PracticeDto;
    availableInsuranceTypes: string[];
    availableLanguages: string[];
    availableTreatmentTypes: TreatmentTypeDto[];
}
