import {PriceDto} from './price';

export interface TreatmentTypeDto
{
    id: number;
    type: string;
    charge?: PriceDto;
    forcePay: boolean;
    comment?: string;
    doctorIds: number[];
    enabled?: boolean;
}

export class TreatmentType
{
    public id: number;
    public type: string;
    public charge: PriceDto;
    public forcePay: boolean = false;
    public comment: string;
    public doctorIds: number[];
    public enabled: boolean;

    constructor(dto?: TreatmentTypeDto) {
        if (!dto) return;

        this.id = dto.id;
        this.type = dto.type;
        this.charge = dto.charge;
        this.forcePay = dto.forcePay === true;
        this.comment = dto.comment;
        this.doctorIds = dto.doctorIds || [];
        this.enabled = dto.enabled;
    }
}
