import {CustomerDto} from './customer';
import {AppointmentDto, Appointment} from './appointment';
import {DoctorDto} from './doctor';

export class BookingRequest {
    appointmentId: number;
    practiceId: number;
    title: string;
    customer: BookingCustomer = {
        returning: false,
        email: null,
        firstName: null,
        lastName: null,
        insuranceType: null,
        phone: null,
        phoneMobile: null,
        gender: null
    };

    notificationSettings: {smsRecipient: string, emailRecipient: string} = {
        smsRecipient: null,
        emailRecipient: null
    };

    treatmentTypeId: number = 0;
    capture: string;
    platform: string;

}

export interface BookingCustomer extends CustomerDto {
    returning: boolean;
}

export class Booking {
    public id: number;
    public appointment: Appointment;
    public customer: {
        returning: boolean,
        details: CustomerDto,
    };
    public paid: boolean = false;
    public paymentInitialised: boolean = false;
    public status: string = 'CREATED';

    constructor(dto?: BookingDto) {
        if (!dto) return;
        this.id = dto.id;
        this.appointment = new Appointment(dto.appointment);
        this.customer = dto.customer;
        this.paid = dto.paid;
        this.paymentInitialised = dto.paymentInitialised;
        this.status = dto.status;
    }
}

export interface BookingDto {
    id: number;
    customer: {
        returning: boolean,
        details: CustomerDto,
    };
    paid: boolean;
    paymentInitialised: boolean;
    appointment: AppointmentDto;
    status: string;
}
