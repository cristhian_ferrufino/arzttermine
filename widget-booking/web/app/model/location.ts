export interface LocationDto
{
    id: number;
    country: string;
    location: string;
    longitude: number;
    latitude: number;
    zip: string;
    city: string;
    phone?: string;
    phoneMobile?: string;
    contactMail?: string;
    primary?: boolean;
}
