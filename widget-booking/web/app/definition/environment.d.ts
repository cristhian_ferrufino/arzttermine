interface Environment {
    production: boolean;
    stripe?: {
        publicKey: string;
    };
    external?: {
        s3Link: string,
        buckets: {
            styles: string;
            avatars: string;
        }
    },
    bookingService?: {
        host: string;
        path: string;
    };
    elasticsearch?: {
        host: string;
        log: string;
        prefix?: string;
    };
    google?: {
        staticMapsKey: string;
    };
}

declare module 'Environment' {
    export = Environment;
}

