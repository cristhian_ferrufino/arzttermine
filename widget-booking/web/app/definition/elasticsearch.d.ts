
interface SearchResponse
{
    took: number;
    timed_out: boolean;
    hits: {
        total: number;
        max_score: number;
        hits: ElasticDocument[]
    }
}

interface ElasticDocument
{
    _index: string;
    _type: string;
    _id: string;
    _score: number;
    _source: any;
}

interface SearchQuery
{
    index: string;
    type?: string;
    q?: string;
    body?: SearchBody;
}

interface SearchBody
{
    _source?: string[];
    sort?: any[];
    query: any;
    from?: number;
    size?: number;
}
