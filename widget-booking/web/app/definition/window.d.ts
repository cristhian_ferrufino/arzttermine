interface Window {
    at_practice: number;
    at_token: string;
    at_hide_map?: boolean;
    at_auth_type?: string;
    at_style_template?: string;
    Stripe: StripeApi;
}

declare module 'window' {
    export = Window;
}

interface StripeApi {
    setPublishableKey: (key: string) => void;
    card: {
        createToken: (req: TokenRequest, fn: Function) => void;
        validateCardNumber: (number: string) => boolean;
    };
}

interface TokenRequest {
    name?: string;
    number?: string;
    exp_month?: number;
    exp_year?: number;
    cvc?: string;
}
