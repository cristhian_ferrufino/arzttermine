import {LOCALE_ID, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule, PreloadAllModules} from '@angular/router';
import {AppComponent} from './main/component/app-component';
import {routes} from './routing/app.routes';
import {AppointmentComponent} from './appointment/component/appointment-component';
import {TwoColumnButtonComponent} from './main/component/two-column-component';
import {MenuComponent} from './main/component/menu-component';
import {BookingService} from './checkout/service/booking-service';
import {PracticeService} from './checkout/service/practice-service';
import {CalendarComponent} from './appointment/component/calendar-component';
import {CalendarModule} from 'angular-calendar';
import {SearchService} from './search/service/search-service';
import {ElasticClient} from './search/service/elastic-client';
import {TreatmentSelectionComponent} from './appointment/component/treatment-selection';
import {PatientComponent} from './patient/component/patient-component';
import {ContinueButtonsComponent} from './main/component/continue-buttons-component';
import {WidgetService} from './main/service/widget-service';
import {PaymentComponent} from './payment/component/payment.component';
import {SummaryComponent} from './checkout/component/summary-component';
import {PatientGuard} from './routing/guard/patient-guard';
import {PaymentGuard} from './routing/guard/payment-guard';
import {SummaryGuard} from './routing/guard/summary-guard';
import {SuccessGuard} from './routing/guard/success-guard';
import {CreditCardComponent} from './payment/component/provider/credit-card-component';
import {ParentLayoutComponent} from './main/component/parent-layout-component';
import {AppointmentDetailsComponent} from './appointment/component/appointment-details-component';
import {PracticeDetailsComponent} from './appointment/component/practice-details-component';
import {SuccessComponent} from './checkout/component/success-component';
import {PaymentTableComponent} from './checkout/component/payment-table-component';
import {AppointmentSelectionComponent} from './appointment/component/appointment-selection-component';
import {RestoreBookingGuard} from './routing/guard/restore-guard';
import {PaymentService} from './payment/service/payment-service';
import {PaymentDetailsComponent} from './payment/component/payment-details-component';
import {CommonModule} from '@angular/common';
import {LocalStorage} from './main/util/local-storage';
import {localStorageFactory, localeFactory, translationServiceFactory} from './main/util/factories';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DoctorSelectionComponent} from './appointment/component/doctor-selection';
import {NgSelectModule} from 'pe-ng-toolbox/ngselect/select.module';
import {PracticeGuard} from './routing/guard/practice-guard';
import {TranslateModule, TranslateLoader, TranslateService} from '@ngx-translate/core';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        CommonModule,
        BrowserAnimationsModule,
        CalendarModule.forRoot(),
        RouterModule.forRoot(routes, { useHash: true, preloadingStrategy: PreloadAllModules }),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: translationServiceFactory
            }
        }),
        NgSelectModule,
    ],
    providers: [
        ElasticClient,
        SearchService,
        PracticeService,
        BookingService,
        PaymentService,
        WidgetService,

        PracticeGuard,
        PatientGuard,
        PaymentGuard,
        SummaryGuard,
        SuccessGuard,
        RestoreBookingGuard,

        { provide: LocalStorage, useFactory: localStorageFactory },
        { provide: LOCALE_ID, deps: [TranslateService], useFactory: localeFactory }
    ],
    declarations: [
        ParentLayoutComponent,
        TwoColumnButtonComponent,
        ContinueButtonsComponent,

        AppComponent,
        MenuComponent,
        AppointmentComponent,
        AppointmentDetailsComponent,
        AppointmentSelectionComponent,
        PracticeDetailsComponent,
        TreatmentSelectionComponent,
        DoctorSelectionComponent,
        CalendarComponent,
        PatientComponent,
        PaymentComponent,
        PaymentTableComponent,
        PaymentDetailsComponent,
        CreditCardComponent,
        SummaryComponent,
        SuccessComponent,
    ],
    bootstrap: [
        AppComponent,
    ]
})
export class BaseAppModule
{
}
