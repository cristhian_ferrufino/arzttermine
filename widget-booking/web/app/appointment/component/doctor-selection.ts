import {Component, EventEmitter, Output, Input, OnInit} from '@angular/core';
import {BookingService} from '../../checkout/service/booking-service';
import {DoctorDto} from '../../model/doctor';
import {environment} from '../../../environments/environment';
import {PracticeService} from '../../checkout/service/practice-service';
import {NgSelectBox, NgSelectOption} from 'pe-ng-toolbox/ngselect/selectbox';
import {TranslateService} from "@ngx-translate/core";

@Component({
    selector: 'doctors',
    template: `
        <ng-select class="at-doctor-select" [box]="selectBox"
                   (change)="select($event.item)">
        </ng-select>
    `
})
export class DoctorSelectionComponent implements OnInit
{
    @Output() public change: EventEmitter<number|string> = new EventEmitter<number|string>();
    @Input() public doctors: DoctorDto[];
    selectBox: NgSelectBox<DoctorDto>;

    constructor(public bookingService: BookingService,
                private translator: TranslateService,
                private practices: PracticeService) {
    }

    ngOnInit(): void {
        const items: NgSelectOption<DoctorDto>[] = [];

        for (const entry in this.doctors) {
            const doctor: DoctorDto = this.doctors[entry];
            items.push({
                value: doctor.id,
                active: this.bookingService.bookedDoctor && this.bookingService.bookedDoctor.id === doctor.id,
                item: doctor,
                image: {
                    path: this.getDoctorImageUrl(doctor.id)
                }
            });
        }

        this.selectBox = {
            items,
            defaultText: this.translator.instant('wording.please_choose'),
            toString: (entry) => {
                return `${entry.item.fullName}<br /><small>${this.getMedicalSpecialties(entry.item)}</small>`;
            }
        };
    }

    public select(doctor: DoctorDto): void {
        if (!doctor || (this.bookingService.bookedDoctor && doctor.id === this.bookingService.bookedDoctor.id)) {
            return;
        }

        this.bookingService.setDoctor(doctor);
        this.change.emit(doctor.id);
    }

    public getMedicalSpecialties(doctor: DoctorDto): string {

        return (doctor.medicalSpecialties && doctor.medicalSpecialties.length)
            ? doctor.medicalSpecialties.map(ms => this.translator.instant('medicalSpecialties.' + ms.toUpperCase())).join(', ')
            : this.translator.instant('medicalSpecialties.NONE');
    }

    private getDoctorImageUrl(doctorId: number): string {
        return `${environment.external.s3Link}/${environment.external.buckets.avatars}/p${this.practices.getPractice().id}/doctors/${doctorId}/avatars/scaleWidth_56.jpg`;
    }
}


