import {Component, Input, Output, OnInit, OnDestroy, OnChanges} from '@angular/core';
import {Appointment} from '../../model/appointment';
import {CalendarEvent, CalendarEventTitleFormatter} from 'angular-calendar';
import {CustomEventTitleFormatter} from "./custom-event-title-formatter.provide";
import {BookingService} from '../../checkout/service/booking-service';
import {SearchService} from '../../search/service/search-service';
import {SearchTermEnhancer} from '../../search/filter/search-filter';
import {Subscription} from 'rxjs/Subscription';
import {ReplaySubject} from 'rxjs/ReplaySubject';
import {Subject} from 'rxjs/Subject';
import {ActivatedRoute, Params} from '@angular/router';
import {DateRangeEnhancer} from '../../search/filter/date-range-enhancer';
import {Observable} from 'rxjs/Observable';

@Component({
    selector: 'calendar',
    template: `
        <div class="at-widget-calendar__head">
            <div class="at-widget-calendar__navigation text-center">
                <div class="row">
                    <div class="col-md-4 col-xs-12 text-md-left text-sm-center">
                        <button type="button"
                                class="btn at-btn"
                                (viewDateChange)="adjustViewDate($event)"
                                mwlCalendarPreviousView
                                [view]="view"
                                [viewDate]="viewDate"><</button>
                        <button type="button"
                                [ngClass]="{'btn': true, 'at-btn': true, 'active': viewDate === today}"
                                (click)="switchView(today, 'week')">{{ 'calendar.today' | translate }}</button>
                        <button type="button"
                                [view]="view"
                                class="btn at-btn"
                                mwlCalendarNextView
                                [viewDate]="viewDate"
                                (viewDateChange)="adjustViewDate($event)">></button>
                    </div>
                    <div [ngSwitch]="view" class="col-md-4 col-xs-12 text-center" style="padding: 10px 0;">
                        <span *ngSwitchCase="'month'"> {{ formattedDate | date:'MMMM y' }} </span>
                        <span *ngSwitchCase="'week'"> {{ formattedDate | date:'dd. MMMM' }} - {{ endWeek | date:'dd. MMMM y'  }} </span>
                        <span *ngSwitchCase="'day'"> {{ formattedDate | date:'dd. MMMM y' }} </span>
                    </div>
                    <div class="col-md-4 col-xs-12 text-md-right text-sm-center">
                        <button [ngClass]="{'btn': true, 'at-btn': true, 'active': view === 'week'}"
                                id="calendarWeekView"
                                type="button"
                                style="width: 90px"
                                (click)="switchView(viewDate, 'week')">
                            {{ 'calendar.week' | translate }}
                        </button>
                        <button [ngClass]="{'btn': true, 'at-btn': true, 'active': view === 'month'}"
                                id="calendarMonthView"
                                type="button"
                                style="width: 90px"
                                (click)="switchView(viewDate, 'month')">
                            {{ 'calendar.month' | translate }}
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div *ngIf="events.length; else noEventsContainer" [ngSwitch]="view">
            <mwl-calendar-month-view
                    *ngSwitchCase="'month'"
                    (eventClicked)="eventClicked($event.event)"
                    [excludeDays]="excludeDays"
                    [refresh]="calendarTick"
                    [viewDate]="viewDate"
                    [events]="events"
                    (dayClicked)="switchView($event.day.date, 'week')">
            </mwl-calendar-month-view>
            <mwl-calendar-week-view
                    *ngSwitchCase="'week'"
                    (eventClicked)="eventClicked($event.event)"
                    [refresh]="calendarTick"
                    [weekStartsOn]="viewDate.getDay()"
                    [excludeDays]="excludeDays"
                    [viewDate]="viewDate"
                    [events]="events"
                    (dayClicked)="switchView($event.date, 'week')">
            </mwl-calendar-week-view>
            <mwl-calendar-day-view
                    *ngSwitchCase="'day'"
                    (eventClicked)="eventClicked($event.event)"
                    [viewDate]="viewDate"
                    [events]="events"
                    hourSegments="2"
                    dayStartHour="6"
                    dayEndHour="18">
            </mwl-calendar-day-view>
        </div>
        <ng-template #noEventsContainer>
            <div *ngIf="nextAvailableAppointment | async; let nextDate">
                <div *ngIf="nextDate && nextDate.length" class="text-center">
                    <p>&nbsp;</p>
                    <button class="btn btn-sm at-btn" style="line-height: 200%;" (click)="switchView(nextDate[0], 'week')"> 
                        <small>{{ 'appointment.next_available' | translate }}:</small><br />
                        {{ nextDate[0] | date:'dd. MMMM y HH:mm' }}
                    </button>
                </div>
                <div *ngIf="!nextDate?.length" class="alert at-alert-info">
                    {{ 'appointment.no_found' | translate }}
                </div>
            </div>
        </ng-template>
    `,
    host: {
        'class': 'at-widget-calendar'
    },
    providers: [
        {
            provide: CalendarEventTitleFormatter,
            useClass: CustomEventTitleFormatter
        },
    ]
})
export class CalendarComponent implements OnInit, OnDestroy, OnChanges
{
    public excludeDays: number[] = [0, 6];

    @Input() public viewDate: Date = new Date();
    public nextAvailableAppointment: Observable<Date[]>;
    public formattedDate: Date;
    public endWeek: Date;
    public format: string;

    @Input() public reload: ReplaySubject<SearchTermEnhancer[]>;
    private subscription: Subscription;
    private timer: any;

    public view: string;
    public events: CalendarAppointmentEvent[] = [];
    public calendarTick: Subject<void> = new Subject<void>();
    @Output() public dateChanged: Subject<DateRangeChanged> = new Subject<DateRangeChanged>();

    public readonly today: Date = new Date();

    constructor(public bookingService: BookingService,
                private search: SearchService,
                private route: ActivatedRoute) {
    }

    public switchView(date: Date, target: string): void {
        this.view = target;
        this.viewDate = date || new Date();
        this.nextAvailableAppointment = null;
        this.adjustViewDate(this.viewDate);
    }

    ngOnInit(): void {
        this.route.queryParams.subscribe((params: Params) => {
            if (!params['c']) {
                return;
            }

            const date: Date = new Date(params['c']);
            date.setDate(date.getDate() -2); // center in week
            this.switchView(date, 'week');
        });

        if (this.reload && !this.subscription) {
            this.enableReloadOnChange();
        }
    }

    ngOnDestroy(): void {
        if (this.subscription && !this.subscription.closed) {
            this.subscription.unsubscribe();
            this.subscription = null;
        }
    }

    ngOnChanges(changes: any): void {

        if (changes.viewDate || changes.view) {
            this.format = this.getFormat((changes.view || {currentValue: this.view}).currentValue);
            this.formattedDate = (changes.viewDate || {currentValue: this.viewDate}).currentValue;

            // if not monday we always need to skip the weekend (2days)
            const offsetInDays: number = (this.formattedDate.getDate() === 1) ? 4 : 6;
            this.endWeek = new Date(this.formattedDate.getTime() + (86400000 * offsetInDays));
        }

    }

    private getFormat(view: string): string {
        return (view !== 'day') ? 'dd. MMMM yyyy' : 'dd.MMMM EEEE';
    }

    public adjustViewDate(eventDate: Date): void {
        const daysInView: number = (this.view === 'week') ? 7 : (this.view === 'day') ? 1 : 40;
        let newValue: Date = eventDate;

        if (eventDate < this.today) {
            newValue = this.today;
        }

        this.dateChanged.next({
            newValue,
            daysInView,
            view: this.view
        });
    }

    public eventClicked(event: CalendarAppointmentEvent): void {
        this.bookingService.setAppointment(event.appointment);
    }

    private enableReloadOnChange(): void {
        if (this.subscription) {
            this.ngOnDestroy();
        }

        this.subscription = this.reload.subscribe(filters => {
            let callable: Function;

            // the initial call should not wait
            // additional (duplicate) calls should be prevented within XXX milliseconds
            if (this.timer) {
                clearTimeout(this.timer);
                callable = () => this.fetchEvents(filters);
            } else {
                this.fetchEvents(filters);
                callable = () => {};
            }

            this.timer = setTimeout(() => {
                this.timer = null;
                callable();
            }, 350);
        });
    }

    private fetchEvents(filters: SearchTermEnhancer[]): void {
        if (!this.view) {
            this.view = 'week';
        }

        this.search.searchAppointments(filters).take(1)
            .subscribe((result) => this.fillCalendar(result, filters));
    }

    private fillCalendar(result: Appointment[], filters: SearchTermEnhancer[]): void {
        this.events.length = 0;

        if (!result.length) {
            this.findNextAvailableAppointment(filters);
            return;
        }

        this.events.push(... result.map(appointment => ({
            appointment: appointment,
            start: appointment.start,
            end: appointment.end,
            title: appointment.toTimingString(),
            color: {
                primary: null,
                secondary: null
            }
        })));

        this.calendarTick.next(undefined);
    }

    private findNextAvailableAppointment(filters: SearchTermEnhancer[]): void {
        const rangeIndex: number = filters.findIndex(f => f instanceof DateRangeEnhancer);
        const currentRangeFilter: DateRangeEnhancer = <DateRangeEnhancer> filters[rangeIndex];
        const filtersCopy: SearchTermEnhancer[] = [...filters];

        // see whats the next appointment in the next year
        const endDate: Date = new Date();
        endDate.setFullYear(endDate.getFullYear() + 1);

        filtersCopy[rangeIndex] = new DateRangeEnhancer(currentRangeFilter.from, endDate, currentRangeFilter.timezone);

        this.nextAvailableAppointment = this.search.findNextAvailableAppointment(filtersCopy).take(1);
    }
}

export interface DateRangeChanged {
    newValue: Date;
    daysInView: number;
    view: string;
    updated?: boolean;
}

interface CalendarAppointmentEvent extends CalendarEvent {
    appointment: Appointment;
}
