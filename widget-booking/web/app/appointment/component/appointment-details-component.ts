import {Component, Input,LOCALE_ID} from '@angular/core';
import {Appointment} from '../../model/appointment';
import {BookingService} from '../../checkout/service/booking-service';

@Component({
    selector: 'appointment-details',
    template: `
        <div *ngIf="appointment && !grid">
            {{ 'wording.at' | translate }} 
            {{ appointment.start | date:'dd. MMMM yyyy' }} 
            {{ 'wording.by' | translate }} 
            {{ appointment.start | date:'HH:mm' }}{{ 'wording.clock' | translate }}<br />
            {{ 'wording.with' | translate }}  {{ bookingService.bookedDoctor.fullName }}<br />
            {{ 'wording.treatment' | translate }}: {{ bookingService.treatment.type }}<br />
            <span *ngIf="bookingService.treatment.comment">
                {{ bookingService.treatment.comment }}
            </span>
        </div>
        <div *ngIf="appointment && grid" style="font-size: 1.1em;">
            <div class="row">
                <div class="col">
                    <strong>{{ appointment.toStartTimeString() }}</strong>
                </div>
                <div class="col">
                    {{ appointment.toStartDateString() }}
                </div>
            </div>
            <br />
            {{ 'wording.with' | translate }}  {{ appointment.doctor.fullName }}<br />
            {{ 'wording.treatment' | translate }}: {{ bookingService.treatment.type }}<br />
            <span *ngIf="bookingService.treatment.comment">
                {{ bookingService.treatment.comment }}
            </span>
        </div>
    `
})

export class AppointmentDetailsComponent
{
    @Input() public appointment: Appointment;
    @Input() public grid: boolean = false;

    constructor(public bookingService: BookingService) {
    }
}
