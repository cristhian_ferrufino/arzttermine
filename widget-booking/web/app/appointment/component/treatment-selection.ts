import {Component, EventEmitter, Output, Input} from '@angular/core';
import {BookingService} from '../../checkout/service/booking-service';
import {TreatmentType} from '../../model/treatment-type';

@Component({
    selector: 'treatment-types',
    template: `
        <select class="form-control" (change)="change.emit($event.target.value)" autocomplete="off" name="treatmentOptions">
            <option [attr.selected]="(!bookingService.bookingRequest.treatmentTypeId) ? '': null">{{ 'wording.please_choose' | translate }}</option>
            <option *ngFor="let type of treatmentTypes"
                    [value]="type.id"
                    [attr.selected]="type.id === bookingService.bookingRequest.treatmentTypeId ? '' : null">
                    {{ type.type }}
            </option>
        </select>
    `
})
export class TreatmentSelectionComponent
{
    @Output() public change: EventEmitter<number|string> = new EventEmitter<number|string>();
    @Input() public treatmentTypes: TreatmentType[];

    constructor(public bookingService: BookingService) {
    }
}
