import {Component, OnInit} from '@angular/core';
import {BookingService} from '../../checkout/service/booking-service';
import {PracticeService} from '../../checkout/service/practice-service';
import {InsuranceTypeEnhancer} from '../../search/filter/insurance-type-enhancer';
import {TreatmentTypeEnhancer} from '../../search/filter/treatment-type-enhancer';
import {SearchTermEnhancer} from '../../search/filter/search-filter';
import {PracticeEnhancer} from '../../search/filter/practice-enhancer';
import {DateRangeEnhancer} from '../../search/filter/date-range-enhancer';
import {PaginationEnhancer} from '../../search/filter/pagination-enhancer';
import {WidgetService} from '../../main/service/widget-service';
import {Appointment} from '../../model/appointment';
import {DateRangeChanged} from './calendar-component';
import {Practice} from '../../model/practice';
import {ReplaySubject} from 'rxjs/ReplaySubject';
import {Button} from '../../main/component/two-column-component';
import {DoctorEnhancer} from '../../search/filter/doctor-enhancer';
import {DoctorDto} from '../../model/doctor';
import {TreatmentType} from '../../model/treatment-type';

@Component({
    selector: 'appointment',
    template: `
    <layout>
    <form (submit)="nextStep()" *ngIf="practice" autocomplete="off">
        <div *ngIf="!appointment && buttons">
            <h2 translate>headline.book_here</h2>
            <div class="row" style="margin-top: 12px;">
                <div class="col-xl-8 col-lg-7 col-md-6 col-sm-5 col-xs-12">
                    <div class="at-question-text">
                        {{ 'appointment.choose_first_visit' | translate }}
                    </div>
                </div>
                <div class="col-xl-4 col-lg-5 col-md-6 col-sm-7 col-xs-12">
                    <two-col-button [key]="getAlreadyBookedKey()"
                                    [buttons]="buttons.firstBooking">
                    </two-col-button>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-xl-8 col-lg-7 col-md-6 col-sm-5 col-xs-12">
                    <div class="at-question-text">
                        {{ 'appointment.choose_insurance' | translate }}
                    </div>
                </div>
                <div class="col-xl-4 col-lg-5 col-md-6 col-sm-7 col-xs-12">
                    <two-col-button key="insurance"
                                    [buttons]="buttons.insurance">
                    </two-col-button>
                </div>
            </div>
            <br *ngIf="getEligibleDoctors().length > 1" />
            <div class="row" *ngIf="getEligibleDoctors().length > 1">
                <div class="col-xl-8 col-lg-7 col-md-6 col-sm-5 col-xs-12">
                    <div class="at-question-text">
                        {{ 'appointment.choose_doctor' | translate }}
                    </div>
                </div>
                <div class="col-xl-4 col-lg-5 col-md-6 col-sm-7 col-xs-12">
                    <doctors (change)="doctor($event)"
                             [doctors]="getEligibleDoctors()"></doctors>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-xl-8 col-lg-7 col-md-6 col-sm-5 col-xs-12">
                    <div class="at-question-text">
                        {{ 'appointment.choose_treatment' | translate }}
                    </div>
                </div>
                <div class="col-xl-4 col-lg-5 col-md-6 col-sm-7 col-xs-12">
                    <treatment-types (change)="treatment($event)"
                                     [treatmentTypes]="getTreatmentTypes()"></treatment-types>
                </div>
            </div>
            <br />
            <calendar [style.display]="bookingService.hasAppointmentDetails() ? 'block' : 'none'"
                      [reload]="reload"
                      [viewDate]="currentTime"
                      (dateChanged)="adjustDateRange($event)"></calendar>
        </div>
        <div *ngIf="appointment" class="text-center">
            <appointment-selection [appointment]="appointment"></appointment-selection>
            <div class="at-divider"></div>
            <div class="text-right">
                <continue [granted]="bookingService.hasAppointment.bind(bookingService)"></continue>
            </div>
        </div>
        <!-- preload -->
        <img *ngIf="practiceService.isMapVisible()" [src]="practiceService.getGoogleLocation(practice)" class="at-google-map at-hidden" />
    </form>
    </layout>
    `
})
export class AppointmentComponent implements OnInit
{
    public reload: ReplaySubject<SearchTermEnhancer[]> = new ReplaySubject<SearchTermEnhancer[]>(1);
    public currentTime: Date;
    public rangeEnd: Date;
    public eligibleDoctors: DoctorDto[];

    private lastViewChange: DateRangeChanged;

    private filters: SearchTermEnhancer[] = [
        new PracticeEnhancer(window.at_practice),
        new PaginationEnhancer(0, 150),
    ];

    public buttons: {
        firstBooking: {btn1: Button, btn2: Button},
        insurance: {btn1: Button, btn2: Button}
    } = {
        firstBooking: {
            btn1: {
                text: 'wording.yes', onClick: (b) => this.bookingService.bookingRequest.customer.returning = false
            },
            btn2: {
                text: 'wording.no', onClick: (b) => this.bookingService.bookingRequest.customer.returning = true
            }
        },
        insurance: {
            btn1: {
                text: 'wording.insurances.private',
                onClick: (b) => this.insurance('PRIVATE'),
                disabled: this.practice.insuranceTypes.indexOf('PRIVATE') === -1,
                active: (this.practice.insuranceTypes.length < 2 && this.practice.insuranceTypes.indexOf('PRIVATE') > -1),
                disabledTooltipText: 'Leider können Sie mit dieser Versicherungsart keine Buchung in unserer Praxis vornehmen. Wenn Sie Selbstzahler sind, können Sie auch Privat auswählen.'
            },
            btn2: {
                text: 'wording.insurances.compulsory',
                onClick: (b) => this.insurance('COMPULSORY'),
                disabled: this.practice.insuranceTypes.indexOf('COMPULSORY') === -1,
                active: (this.practice.insuranceTypes.length < 2 && this.practice.insuranceTypes.indexOf('COMPULSORY') > -1),
                disabledTooltipText: 'Leider können Sie mit dieser Versicherungsart keine Buchung in unserer Praxis vornehmen. Wenn Sie Selbstzahler sind, können Sie auch Privat auswählen.'
            }
        }
    };

    private returningCustomerKey: string;

    constructor(private widget: WidgetService,
                public practiceService: PracticeService,
                public bookingService: BookingService) {
    }

    ngOnInit(): void {
        this.adjustDateRange({view: 'week', daysInView: 7, newValue: new Date() }, true);
    }

    get appointment(): Appointment {
        return this.bookingService.appointment;
    }

    get practice(): Practice {
        return this.practiceService.getPractice();
    }

    public nextStep(): void {
        this.widget.goTo('patient');
    }

    public treatment(typeId: number): void {
        if (isNaN(Number(typeId)) || typeId === this.bookingService.bookingRequest.treatmentTypeId) {
            return;
        }

        this.bookingService.setTreatmentType(typeId);
        this.removeIfExists(TreatmentTypeEnhancer);

        if (this.bookingService.treatment) {
            this.filters.push(new TreatmentTypeEnhancer(this.bookingService.treatment.id));
        }

        this.reloadIfComplete();
    }

    public doctor(doctorId: number): void {
        this.removeIfExists(DoctorEnhancer);
        this.filters.push(new DoctorEnhancer(doctorId));

        // recalculate date range for this doctor (to force offset to be included)
        this.adjustDateRange({
            newValue: new Date(),
            view: this.lastViewChange.view,
            daysInView: this.lastViewChange.daysInView
        });
    }

    public insurance(type: string): void {
        this.bookingService.bookingRequest.customer.insuranceType = type;

        this.removeIfExists(InsuranceTypeEnhancer);
        this.filters.push(new InsuranceTypeEnhancer(type));
        this.reloadIfComplete();
    }

    public adjustDateRange(event: DateRangeChanged, noReload: boolean = false): void {
        // prevent multiple requests for the same date if already sent
        if (this.lastViewChange && this.lastViewChange.updated === true && this.lastViewChange.newValue === event.newValue) {
            return;
        }

        this.lastViewChange = event;

        const begin: Date = event.newValue;
        // reset the day to midnight
        begin.setHours(0);
        begin.setMinutes(0);

        const now: Date = new Date();
        const bookableDoctors: DoctorDto[] = (this.practice && this.practice.doctors.filter(d => d.bookable)) || [];

        if (!this.bookingService.bookedDoctor && bookableDoctors.length && bookableDoctors.length < 2) {
            this.bookingService.bookedDoctor = bookableDoctors.shift();
            this.doctor(this.bookingService.bookedDoctor.id);
            return; // will be called again
        }

        if (this.bookingService.bookedDoctor && this.bookingService.bookedDoctor && null !== this.bookingService.bookedDoctor.bookingTimeOffset) {
            const hours: number = this.bookingService.bookedDoctor.bookingTimeOffset;
            // increase minimum date (now) by required offset
            now.setHours(now.getHours() + hours);
        }

        let filterBegin = new Date(begin.getFullYear(), begin.getMonth(), (event.view === 'month') ? 1 : begin.getDate());

        if (filterBegin < now) {
            filterBegin = now;
        }

        const filterEnd = new Date(filterBegin.getFullYear(), filterBegin.getMonth(), filterBegin.getDate() + event.daysInView);

        this.removeIfExists(DateRangeEnhancer);
        this.filters.push(new DateRangeEnhancer(filterBegin, filterEnd));

        this.currentTime = filterBegin;
        this.rangeEnd = filterEnd;

        if (noReload !== true) {
            this.lastViewChange.updated = this.reloadIfComplete();
        }
    }

    public getEligibleDoctors(): DoctorDto[] {
        return this.eligibleDoctors || (this.eligibleDoctors = this.practice.doctors
            .filter(doc => doc.bookable !== false));
    }

    public getTreatmentTypes(): TreatmentType[] {
        const result: TreatmentType[] = this.practice.treatmentTypes.filter(tt => tt.enabled !== false);

        if (this.bookingService.bookedDoctor) {
            return result.filter(tt => tt.doctorIds.some(id => id === this.bookingService.bookedDoctor.id));
        } else {
            return result;
        }
    }

    public getAlreadyBookedKey(): string {
        return this.returningCustomerKey || (this.returningCustomerKey = 'already_booked_' + this.practice.id);
    }

    private removeIfExists(filterType: any): void {
        const existing: number = this.filters.findIndex(f => f instanceof filterType);

        if (existing > -1) {
            this.filters.splice(existing, 1);
        }
    }

    private reloadIfComplete(): boolean {
        if (this.bookingService.hasAppointmentDetails()) {
            this.reload.next(this.filters);
            return true;
        }
        else {
            return false;
        }
    }
}

