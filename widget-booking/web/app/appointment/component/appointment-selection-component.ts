import {Component, Input} from '@angular/core';
import {Appointment} from '../../model/appointment';
import {BookingService} from '../../checkout/service/booking-service';
import {PracticeService} from "../../checkout/service/practice-service";

@Component({
    selector: 'appointment-selection',
    template: `
        <h2 style="padding: 0 0 15px;">
            {{ 'headline.personal_appointment' | translate }}:<br />
            <small style="font-size: 11px;">{{ 'appointment.continue_to_book' | translate }}</small>
        </h2>
        <div class="row">
            <div class="col-sm-6 col-xs-12">
                <p>
                    <strong>
                        <appointment-details [appointment]="appointment"></appointment-details>
                    </strong>
                    {{ 'wording.duration' | translate }}: {{ appointment.duration }} {{ 'wording.minutes' | translate }}<br />
                </p>
                <button type="button"
                        class="btn btn-link at-hidden-xs" 
                        (click)="resetAppointment()">
                    <i class="fa fa-pencil" aria-hidden="true"></i>
                    {{ 'appointment.change_appointment' | translate }}
                </button>
                <button type="button" 
                        class="btn at-btn at-visible-xs"
                        style="width: 100%; margin-bottom: 10px;"
                        (click)="resetAppointment()">
                    <i class="fa fa-pencil" aria-hidden="true"></i>
                    {{ 'appointment.change_appointment' | translate }}
                </button>
                <p *ngIf="bookingService.treatment?.charge">
                    <br />
                    <strong>{{ 'wording.notice' | translate }}:</strong><br />
                    {{ 'booking.payment_required' | translate }}
                    <span *ngIf="!bookingService.treatment.forcePay" translate>payment.pay_in_advance_or_later</span>
                    <span *ngIf="bookingService.treatment.forcePay" translate>payment.pay_in_advance</span>
                </p>
            </div>
            <div class="col-sm-6 col-xs-12">
                <practice-details [practice]="practices.getPractice()"></practice-details>
            </div>
        </div>
    `
})
export class AppointmentSelectionComponent
{
    @Input() public appointment: Appointment;

    constructor(public bookingService: BookingService, public practices: PracticeService) {
    }

    public resetAppointment(): void {
        this.bookingService.appointment = null;
        this.bookingService.bookingRequest.appointmentId = null;
    }
}
