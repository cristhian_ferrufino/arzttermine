import {Component, Input} from '@angular/core';
import {Practice} from '../../model/practice';
import {PracticeService} from '../../checkout/service/practice-service';

@Component({
    selector: 'practice-details',
    template: `
        <div *ngIf="practice" [ngClass]="{'row': grid }">
            <div [ngClass]="{'col-sm-6': grid, 'col-xs-12': grid}">
                {{ practice.name }}<br />
                <strong>{{ 'wording.address' | translate }}:</strong><br />
                {{ practice.location.location }}<br />
                {{ practice.location.zip }} {{ practice.location.city }}<br />
            </div>
            <div [ngClass]="{'col-sm-6': grid, 'col-xs-12': grid}">
                <br *ngIf="!grid" />
                <img *ngIf="showMap" [src]="practiceGoogleLocation" class="at-google-map" />
            </div>
        </div>
    `
})
export class PracticeDetailsComponent
{
    @Input() public practice: Practice;
    @Input() public grid: boolean = false;

    constructor(private practiceService: PracticeService) {
    }

    get showMap(): boolean {
        return this.practiceService.isMapVisible();
    }

    get practiceGoogleLocation(): string {
        return this.practiceService.getGoogleLocation(this.practice);
    }
}
