import {BookingService} from '../checkout/service/booking-service';

export const appointment = (bookingService: BookingService): boolean => {
    return bookingService.hasAppointment();
};

export const patient = (bookingService: BookingService): boolean => {
    return bookingService.hasPersonalData();
};

export const payment = (bookingService: BookingService): boolean => {
    return patient(bookingService)
        && (bookingService.paidInAdvance() || (!bookingService.treatment.forcePay && bookingService.skippedPayment()));
};

export const summary = (bookingService: BookingService): boolean => {
    return appointment(bookingService) && payment(bookingService);
};

export const success = (bookingService: BookingService) => {
    return Boolean(bookingService.result);
};
