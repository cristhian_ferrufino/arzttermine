import {Injectable} from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {PracticeService} from '../../checkout/service/practice-service';
import {Observable} from 'rxjs/Observable';
import {Practice} from '../../model/practice';

@Injectable()
export class PracticeGuard implements CanActivate
{
    constructor(private practices: PracticeService, private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>|boolean {
        if (this.practices.getPractice()) {
            return true;
        }

        this.practices.init(window.at_practice);

        return this.practices.loading.take(1).map((p: Practice) => {
            if (null === p) {
                this.router.navigate(['404']);
                return false;
            }

            return true;
        });
    }
}
