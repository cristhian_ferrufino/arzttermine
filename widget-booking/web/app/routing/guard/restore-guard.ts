import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {BookingService} from '../../checkout/service/booking-service';
import {PracticeService} from '../../checkout/service/practice-service';

@Injectable()
export class RestoreBookingGuard implements CanActivate
{
    constructor(private bookingService: BookingService, private practiceService: PracticeService, private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        if (this.bookingService.result) {
            return true;
        }

        if (!this.practiceService.getPractice()) {
            this.router.navigate(['appointment']);
            return false;
        }

        const previous = window.localStorage && window.localStorage.getItem('booking_' + this.practiceService.getPractice().id);
        // @TODO fetch previous booking so the customer is able to reload the page without loosing the booking data

        return false;
    }
}
