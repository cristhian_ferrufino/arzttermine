import {Injectable} from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {BookingService} from '../../checkout/service/booking-service';
import {payment, patient, appointment} from '../step.validator';

@Injectable()
export class SummaryGuard implements CanActivate
{
    constructor(private bookingService: BookingService, private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        if (!appointment(this.bookingService)) {
            this.router.navigate(['appointment']);
            return false;
        }
        if (!patient(this.bookingService)) {
            this.router.navigate(['patient']);
            return false;
        }

        if (this.bookingService.treatment.charge && !payment(this.bookingService)) {
            this.router.navigate(['payment']);
            return false;
        }

        return true;
    }
}
