import {Injectable} from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {BookingService} from '../../checkout/service/booking-service';
import {appointment} from '../step.validator';

@Injectable()
export class PatientGuard implements CanActivate
{
    constructor(private bookingService: BookingService, private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        const granted = appointment(this.bookingService);
        if (!granted) {
            this.router.navigate(['appointment']);
        }

        return granted;
    }
}
