import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {BookingService} from '../../checkout/service/booking-service';
import {PracticeService} from '../../checkout/service/practice-service';

@Injectable()
export class SuccessGuard implements CanActivate
{
    constructor(private bookingService: BookingService, private practiceService: PracticeService, private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        if (!this.bookingService.result) {
            window.console && window.console.warn('booking not found, trying to continue with checkout');
            this.router.navigate(['summary']);
            return false;
        }

        this.router.navigate(['booking', this.bookingService.result.id]);
        return false;
    }
}
