import {Injectable} from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {BookingService} from '../../checkout/service/booking-service';
import {patient} from '../step.validator';

@Injectable()
export class PaymentGuard implements CanActivate
{
    constructor(private bookingService: BookingService, private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        const result = patient(this.bookingService);
        if (!result) {
            this.router.navigate(['patient']);
            return false;
        }

        if (!this.bookingService.treatment.charge) {
            this.router.navigate(['summary']);
            return false;
        }

        return true;
    }
}
