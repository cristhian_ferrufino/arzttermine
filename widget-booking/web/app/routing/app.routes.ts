import {AppointmentComponent} from '../appointment/component/appointment-component';
import {PatientComponent} from '../patient/component/patient-component';
import {PaymentComponent} from '../payment/component/payment.component';
import {SummaryComponent} from '../checkout/component/summary-component';
import {PatientGuard} from './guard/patient-guard';
import {PaymentGuard} from './guard/payment-guard';
import {SummaryGuard} from './guard/summary-guard';
import {SuccessComponent} from '../checkout/component/success-component';
import {SuccessGuard} from './guard/success-guard';
import {RestoreBookingGuard} from './guard/restore-guard';
import {PracticeGuard} from './guard/practice-guard';

export const routes = [
    {
        path: '',
        component: AppointmentComponent,
        canActivate: [PracticeGuard]
    },
    {
        path: 'appointment',
        component: AppointmentComponent,
        canActivate: [PracticeGuard]
    },
    {
        path: 'patient',
        component: PatientComponent,
        canActivate: [PracticeGuard, PatientGuard]
    },
    {
        path: 'payment',
        component: PaymentComponent,
        canActivate: [PracticeGuard, PaymentGuard]
    },
    {
        path: 'summary',
        component: SummaryComponent,
        canActivate: [PracticeGuard, SummaryGuard]
    },
    {
        path: 'success',
        component: SuccessComponent,
        canActivate: [PracticeGuard, SuccessGuard]
    },
    {
        path: 'booking/:bookingId',
        component: SuccessComponent,
        canActivate: [PracticeGuard, RestoreBookingGuard]
    },
    {
        path: '**',
        redirectTo: 'summary',
        pathMatch: 'full'
    }
];
