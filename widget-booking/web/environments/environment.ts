// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

export const environment: Environment = {
    production: false,
    stripe: {
        publicKey: 'pk_test_34ftYPNHC1BZtcoGo5cHWh8u'
    },
    bookingService: {
        host: 'http://localhost:15200',
        path: 'bookings'
    },
    external: {
        s3Link: 'https://s3.eu-central-1.amazonaws.com',
        buckets: {
            styles: 'widget.arzttermine.de/styles',
            avatars: 'atcalendar.arzttermine.de/media'
        }
    },
    elasticsearch: {
        host: 'http://localhost:9200',
        prefix: '',
        log: 'debug'
    },
    google: {
        staticMapsKey: 'AIzaSyClQh-x5V3jTQz8EffGDUertB6Uc1P3YU4'
    },
};
