export const environment: Environment = {
    production: true,
    stripe: {
        publicKey: 'pk_test_34ftYPNHC1BZtcoGo5cHWh8u'
    },
    bookingService: {
        host: 'https://staging-booking.arzttermine.de',
        path: 'bookings'
    },
    external: {
        s3Link: 'https://s3.eu-central-1.amazonaws.com',
        buckets: {
            styles: 'widget.arzttermine.de/styles',
            avatars: 'atcalendar.arzttermine.de/media'
        }
    },
    elasticsearch: {
        host: 'https://search.arzttermine.de',
        prefix: 'staging-',
        log: 'debug'
    },
    google: {
        staticMapsKey: 'AIzaSyClQh-x5V3jTQz8EffGDUertB6Uc1P3YU4'
    },
};
