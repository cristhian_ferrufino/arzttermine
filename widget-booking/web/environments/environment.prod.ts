export const environment: Environment = {
    production: true,
    stripe: {
        publicKey: 'pk_live_dJcuMAPOYdbRnm25V7jDVu3L'
    },
    bookingService: {
        host: 'https://booking.arzttermine.de',
        path: 'bookings'
    },
    external: {
        s3Link: 'https://s3.eu-central-1.amazonaws.com',
        buckets: {
            styles: 'widget.arzttermine.de/styles',
            avatars: 'atcalendar.arzttermine.de/media'
        }
    },
    elasticsearch: {
        host: 'https://search.arzttermine.de',
        prefix: '',
        log: 'error'
    },
    google: {
        staticMapsKey: 'AIzaSyClQh-x5V3jTQz8EffGDUertB6Uc1P3YU4'
    },
};
