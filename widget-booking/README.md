## widget-booking

### Tools
- Angular4 / Typescript
- Angular CLI & webpack
- NPM (>=3.3) & Node (>=5) -> latest supported: node 8
- To customize the widget we are using our [widget-styler](https://github.com/arzttermine/widget-styler)

### Install
- `npm install`

##### Webserver
`npm run serve` and open `localhost:5000`  
Serves `web/index.html`

##### Production
Try `npm start` to execute tests and building the production stage.
To only build the sources, use `npm run prod`.

##### Development
Run `npm run dev` to start dev environment including a watcher and live-reload.

### Deploy
If you want to deploy to production, simply merge into `master`-branch.

If you want to deploy to staging follow these instructions:

- `npm run staging`
- Install aws-cli
- Set AWS_ACCESS_KEY and AWS_SECRET_KEY
- `aws s3 cp dist/staging-widget.min.js s3://widget.arzttermine.de/ --metadata-directive REPLACE --expires 2034-01-01T00:00:00Z --content-type 'application/x-javascript;charset=utf-8' --cache-control max-age=300,public --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers`

## Notes:
- Please do not apply any colors on the elements! 
- Please do not use bootstraps extended classes (eg: `btn-primary`, only use `btn`)
- If you create something which requires a new color, update the [widget-styler](https://github.com/arzttermine/widget-styler) first
