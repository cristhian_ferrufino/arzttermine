function isInvalidIE() {
    var ua = window.navigator.userAgent || '', msie = ua.indexOf('MSIE '), trident = ua.indexOf('Trident/');
    if (msie > 0)
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    else {
        if (trident > 0) {
            var rv = ua.indexOf('rv:');
            return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        } else {
            return false;
        }
    }
}
function createIfNecessary(query, value, properties, selector) {
    var element, usingSelector = selector || 'meta';
    if ((usingSelector === 'meta' && !(!document.querySelector('meta[' + query + '=' + value + ']')))
        || document.getElementsByTagName(usingSelector).length > 0)
        element = null;
    else {
        element = document.createElement(usingSelector);
        Object.keys(properties).forEach(function (key) {
            element.setAttribute(key, properties[key]);
        });
    }
    return element;
}
(function(ctx, params) {
    var displayText = '';
    if (!isInvalidIE()) {
        var tags = [];
        tags.push(createIfNecessary('charset', 'utf-8', { 'charset': 'utf-8' })); // html5
        tags.push(createIfNecessary('http-equiv', 'content-type', { 'http-equiv': 'content-type', 'content': 'text/html; charset=UTF-8' }));
        tags.push(createIfNecessary('name', 'viewport', { 'name': 'viewport', 'content': 'width=device-width, initial-scale=1.0' }));
        tags.push(createIfNecessary(null, null, { 'href': ctx.location.pathname }, 'base'));

        var environment = (params.at_environment === 'staging') ? 'widget-staging.min.js' : 'widget.min.js';
        var widgetScript = ctx.createElement('script');
        widgetScript.src = 'https://widget.arzttermine.de/' + environment;
        widgetScript.async = 'async';
        tags.push(widgetScript);

        var doc = (ctx.head || ctx.getElementsByTagName('head')[0]);
        for (var i = 0; i < tags.length; i++)
            if (tags[i]) doc.appendChild(tags[i]);

        displayText += 'Loading...';
    } else {
        displayText += '<p style="text-align: center; padding: 10px; border: 1px solid #666; width: 98%;">';
        displayText += 'Leider können Sie keine Terminbuchung vornehmen, da der Internet Explorer dieses Programm nicht unterstützt. ';
        displayText += 'Bitte nutzen Sie stattdessen die Browser Chrome, Firefox, Safari, Opera oder den Microsoft Edge Browser um eine Terminbuchung vorzunehmen. ';
        displayText += 'Vielen lieben Dank für Ihr Verständnis.';
        displayText += '</p>';
    }

    // creating the element to display text or widget
    var main = ctx.createElement('atwidgetmain');
    main.innerHTML = displayText;
    var currentScript = ctx.currentScript || (function () {
        var script = ctx.getElementById('atwidget_script');
        if (script !== null) return script;
        else console.log('missing id atwidget_script on widget loader');
    })();
    currentScript.parentNode.insertBefore(main, currentScript.nextSibling);
})(document, window);
