
server {
    listen 80;
    server_name  staging-atcalendar.arzttermine.de atcalendar.arzttermine.de *.atcalendar.arzttermine.de;

    root   /www/atcalendar.arzttermine.de/web;
    index  app.php;

    access_log  /var/log/nginx/atcalendar.access.log;
    error_log  /var/log/nginx/atcalendar.error.log;

    location /elb-status {
        access_log off;
        return 200 'A-OK!';
        add_header Content-Type text/plain;
    }

    location ~* \.(?:jpg|jpeg|png)$ {
      add_header Vary Accept;
      access_log off;
      add_header Cache-Control "public";
      try_files $uri$webp_suffix $uri =404;
    }

    location ~* \.(?:gif|ico|cur|gz|svg|svgz|mp4|ogg|ogv|webm|htc|css|js|lang)$ {
      access_log off;
      add_header Cache-Control "public";
    }

    location ~* \.(?:ttf|ttc|otf|eot|woff|woff2)$ {
      add_header "Access-Control-Allow-Origin" "*";
      access_log off;
      add_header Cache-Control "public";
    }

    location / {
         if ($http_x_forwarded_proto != "https") {
           rewrite ^(.*)$ https://$host$1 permanent;
         }

         try_files $uri /app.php$is_args$args;
    }

    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }

    location ~ ^/.+\.php(/|$) {
        proxy_read_timeout     300;
        proxy_connect_timeout  300;
        proxy_http_version 1.1;
        proxy_set_header Connection "";
        fastcgi_pass php7;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        fastcgi_read_timeout 300;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $realpath_root;
    }
}
