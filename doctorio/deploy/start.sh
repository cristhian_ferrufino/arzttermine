#!/bin/bash

DIR="$(dirname "${BASH_SOURCE[0]}")"

yes | sudo cp -rf ${DIR}/nginx/* /etc/nginx/conf.d
sudo rm -rf ${DIR}/../../var/cache/prod # remove symfony cache manually

sudo service nginx restart
sudo service php7.0-fpm restart

