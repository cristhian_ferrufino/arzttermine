module.exports = function(grunt) {
    require('time-grunt')(grunt);

    var
      jsResources       = 'app/Resources/js/',
      jsDestination     = 'web/js/',

      scssResources     = 'app/Resources/scss/',
      cssDestination    = 'web/css/',

      bower_components  = 'bower_components/',
      node_modules      = 'node_modules/',
      vendorDestination = 'web/vendor/',
      bundleDestination = 'web/bundles/';

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        sass: {
            options: {
                style: 'expanded'
            },
            'administration': {
                files: [{
                    expand: true,
                    cwd: scssResources,
                    src: ['administration.sass'],
                    dest: cssDestination,
                    ext: '.css'
                }]
            },
            'admin': {
                files: [{
                    expand: true,
                    cwd: scssResources,
                    src: ['admin.sass'],
                    dest: cssDestination,
                    ext: '.css'
                }]
            },
            'application': {
                files: [{
                    expand: true,
                    cwd: scssResources+'application/',
                    src: ['application.sass'],
                    dest: cssDestination,
                    ext: '.css'
                }]
            },
            'dashboard': {
                files: [{
                    expand: true,
                    cwd: scssResources+'application/',
                    src: ['dashboard.sass'],
                    dest: cssDestination,
                    ext: '.css'
                }]
            },
            'dashboard_calendar': {
                files: [{
                    expand: true,
                    cwd: scssResources+'application/',
                    src: ['dashboard_calendar.sass'],
                    dest: cssDestination,
                    ext: '.css'
                }]
            },
            'widget': {
                files: [{
                    expand: true,
                    cwd: scssResources+'widget/',
                    src: ['widget.sass'],
                    dest: cssDestination,
                    ext: '.css'
                }]
            }
        },

        cssmin: {
            admin: {
                options: {
                    keepSpecialComments: '*',
                    //noAdvanced: true, // turn advanced optimizations off until the issue is fixed in clean-css
                    report: 'min',
                    selectorsMergeMode: 'ie8'
                },
                src: [
                      cssDestination + 'administration.css'
                ],
                dest: cssDestination + 'administration.min.css'
            },
            'application': {
                options: {
                    keepSpecialComments: '*',
                    noAdvanced: true, // turn advanced optimizations off until the issue is fixed in clean-css
                    report: 'min',
                    selectorsMergeMode: 'ie8'
                },
                src: [
                    cssDestination + 'application.css'
                ],
                dest: cssDestination + 'application.min.css'
            },
            'dashboard': {
                options: {
                    keepSpecialComments: '*',
                    noAdvanced: true, // turn advanced optimizations off until the issue is fixed in clean-css
                    report: 'min',
                    selectorsMergeMode: 'ie8'
                },
                src: [
                    cssDestination + 'dashboard.css'
                ],
                dest: cssDestination + 'dashboard.min.css'
            },
            'dashboard_calendar': {
                options: {
                    keepSpecialComments: '*',
                    noAdvanced: true, // turn advanced optimizations off until the issue is fixed in clean-css
                    report: 'min',
                    selectorsMergeMode: 'ie8'
                },
                src: [
                    cssDestination + 'dashboard_calendar.css'
                ],
                dest: cssDestination + 'dashboard_calendar.min.css'
            },
            'widget': {
                options: {
                    keepSpecialComments: '*',
                    noAdvanced: true, // turn advanced optimizations off until the issue is fixed in clean-css
                    report: 'min',
                    selectorsMergeMode: 'ie8'
                },
                src: [
                    cssDestination + 'widget.css'
                ],
                dest: cssDestination + 'widget.min.css'
            }
        },

        slim: {
            dist: {
                files: [
                    {
                        expand: true,
                        cwd: 'web/template/slim/',
                        src: ['{,*/}*.slim'],
                        dest: 'web/template/',
                        ext: '.php'
                    }
                ],
                options: {
                    pretty: true
                }
            }
        },

        uglify: {
            'dev': {
                options: {
                    compress: {
                        drop_console: true
                    }
                },
                files: [{
                    expand: true,
                    cwd: jsDestination,
                    src: ['*.js', '!*.min.js'],
                    dest: jsDestination,
                    ext: '.min.js',
                    extDot: 'last'
                }]
            },
            'bower-simplecolorpicker': {
                options: {
                    compress: {
                        drop_console: true
                    }
                },
                files: [{
                    expand: true,
                    cwd: bower_components + 'jquery-simplecolorpicker/',
                    src: ['*.js', '!*.min.js'],
                    dest: bower_components + 'jquery-simplecolorpicker/',
                    ext: '.min.js',
                    extDot: 'last'
                }]
            },
            'bower-google-infobox': {
                options: {
                    compress: {
                        drop_console: true
                    }
                },
                files: [{
                    expand: true,
                    cwd: bower_components + 'google-infobox/',
                    src: ['*.js', '!*.min.js'],
                    dest: bower_components + 'google-infobox/',
                    ext: '.min.js',
                    extDot: 'last'
                }]
            },
            'bower-jqnotifybar': {
                options: {
                    compress: {
                        drop_console: true
                    }
                },
                files: [{
                    expand: true,
                    cwd: bower_components + 'jqnotifybar/',
                    src: ['*.js', '!*.min.js'],
                    dest: bower_components + 'jqnotifybar/',
                    ext: '.min.js',
                    extDot: 'last'
                }]
            },
            'bower-form-validation': {
                options: {
                    compress: {
                        drop_console: true
                    }
                },
                files: [{
                    expand: true,
                    cwd: bower_components + 'form.validation/dist/js/',
                    src: ['*.js', '!*.min.js'],
                    dest: bower_components + 'form.validation/dist/js/',
                    ext: '.min.js',
                    extDot: 'last'
                }]
            },
            'bower-easy-richmarker': {
                options: {
                    compress: {
                        drop_console: true
                    }
                },
                files: [{
                    expand: true,
                    cwd: bower_components + 'easy-richmarker/src/',
                    src: ['*.js', '!*.min.js'],
                    dest: bower_components + 'easy-richmarker/src/',
                    ext: '.min.js',
                    extDot: 'last'
                }]
            },
            'bower-moment-locale': {
                options: {
                    compress: {
                        drop_console: true
                    }
                },
                files: [{
                    expand: true,
                    cwd: bower_components + 'moment/locale/',
                    src: ['*.js', '!*.min.js'],
                    dest: bower_components + 'moment/locale/',
                    ext: '.min.js',
                    extDot: 'last'
                }]
            },

            'resources': {
                options: {
                    compress: {
                        drop_console: true
                    }
                },
                files: [{
                    expand: true,
                    cwd: jsResources + 'application/',
                    src: ['*.js', '!*.min.js'],
                    dest: jsResources + 'application/',
                    ext: '.min.js',
                    extDot: 'last'
                }]
            }
        },

        concat: {
            options: {
                stripBanners: true,
                banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
                '<%= grunt.template.today("yyyy-mm-dd") %> */'
            },
            'css-admin': {
                src: [
                    bower_components + 'jquery-ui/themes/smoothness/jquery-ui.min.css',
                    bower_components + 'eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
                    bower_components + 'select2/dist/css/select2.min.css',
                    bower_components + 'fullcalendar/dist/fullcalendar.min.css',
                    bower_components + 'font-awesome/css/font-awesome.min.css',
                    bower_components + 'uploadify/uploadify.css',
                    bower_components + 'form.validation/dist/css/formValidation.min.css',
                    cssDestination + 'administration.min.css'
                ],
                dest: cssDestination + 'administration.all.min.css'
            },
            'css-application': {
                src: [
                    bower_components + 'select2/dist/css/select2.min.css',
                    bower_components + 'form.validation/dist/css/formValidation.min.css',
                    bower_components + 'jqnotifybar/css/jquery.notifyBar.css',
                    cssDestination + 'application.min.css'
                ],
                dest: cssDestination + 'application.all.min.css'
            },
            'css-dashboard': {
                src: [
                    bower_components + 'select2/dist/css/select2.min.css',
                    bower_components + 'jquery-simplecolorpicker/jquery.simplecolorpicker.css',
                    bower_components + 'toastr/toastr.min.css',
                    cssDestination + 'dashboard.min.css'
                ],
                dest: cssDestination + 'dashboard.all.min.css'
            },
            'css-dashboard_calendar': {
                src: [
                    cssDestination + 'dashboard_calendar.min.css',
                ],
                dest: cssDestination + 'dashboard_calendar.all.min.css'
            },
            'css-widget': {
                src: [
                    cssDestination + 'widget.min.css',
                    bower_components + 'Swiper/dist/css/swiper.min.css'
                ],
                dest: cssDestination + 'widget.all.min.css'
            },
            'js-common': {
                options: {
                    separator: ";"
                },
                src: [
                    bower_components + 'jquery/dist/jquery.min.js',
                    bower_components + 'tether/dist/js/tether.min.js',
                    bower_components + 'bootstrap/dist/js/bootstrap.min.js',
                    bower_components + 'moment/min/moment.min.js',
                    bower_components + 'moment/locale/de.min.js',
                    bower_components + 'moment-recur/moment-recur.min.js',
                    bower_components + 'form.validation/dist/js/formValidation.min.js',
                    bower_components + 'form.validation/dist/js/framework/bootstrap.min.js',
                    bower_components + 'form.validation/dist/js/language/de_DE.min.js',
                    jsDestination + '/intro.js',
                    jsDestination + '/at.min.js'
                ],
                dest: jsDestination + 'common.min.js'
            },
            'js-searchmap': {
                src: [
                    bower_components + 'google-infobox/google-infobox.min.js',
                    bower_components + 'easy-richmarker/src/richmarker.min.js',
                    jsDestination + 'calendar.min.js',
                    jsDestination + 'search.min.js',
                    jsDestination + 'map.min.js'
                ],
                dest: jsDestination + 'searchmap.min.js'
            },
            'js-dashboard_calendar': {
                src: [
                    jsDestination + '/intro.js'
                ],
                dest: jsDestination + 'dashboard_calendar.min.js'
            },
            'js-application': {
                // tether.js: Tooltips (in bootstrap4) rely on on the 3rd party library tether for positioning.
                //  You must include tether.min.js before bootstrap.js in order for tooltips to work!
                src: [
                    jsResources + 'application/tooltip.min.js',
                    jsResources + 'application/copyContent.min.js',
                    jsResources + 'application/collapseMap.min.js',
                    jsResources + 'application/enableTextarea.min.js',
                    bower_components + 'jqnotifybar/jquery.notifyBar.min.js',
                ],
                dest: jsDestination + 'application.js'
            },
            'js-dashboard': {
                src: [
                    bower_components + 'jquery-ui/ui/minified/core.min.js',
                    bower_components + 'jquery-ui/ui/minified/widget.min.js',
                    bower_components + 'jquery-ui/ui/minified/mouse.min.js',
                    bower_components + 'jquery-ui/ui/minified/sortable.min.js',
                    bower_components + 'select2/dist/js/select2.full.min.js',
                    bower_components + 'select2/dist/js/i18n/es.js',
                    bower_components + 'jquery-stupid-table/stupidtable.min.js',
                    bower_components + 'eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                    bower_components + 'restful.js/dist/restful.standalone.min.js',
                    bower_components + 'jquery-simplecolorpicker/jquery.simplecolorpicker.min.js',
                    bower_components + 'toastr/toastr.min.js',
                    jsResources + 'application/select-box.min.js',
                    jsDestination + 'doctor-profile.js',
                    bundleDestination + 'bazingajstranslation/js/translator.min.js'
                ],
                dest: jsDestination + 'dashboard.js'
            },
            'js-administration': {
                src: [
                    bower_components + 'jquery-ui/ui/minified/core.min.js',
                    bower_components + 'jquery-ui/ui/minified/widget.min.js',
                    bower_components + 'jquery-ui/ui/minified/mouse.min.js',
                    bower_components + 'jquery-ui/ui/minified/slider.min.js',
                    bower_components + 'highcharts/highcharts.js',
                    bower_components + 'eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                    bower_components + 'fullcalendar/dist/fullcalendar.min.js',
                    bower_components + 'fullcalendar/dist/lang/de.js',
                    bower_components + 'jquery-simplyCountable/jquery.simplyCountable.js',
                    bower_components + 'jquery-metadata/jquery.metadata.js',
                    bower_components + 'jquery.validation-engine/js/jquery.validation-engine-de.js',
                    bower_components + 'jquery.validation-engine/js/jquery.validation-engine.js',
                    bower_components + 'uploadify/jquery.uploadify.min.js',
                    bower_components + 'speakingurl/speakingurl.min.js',
                    bower_components + 'jquery-slugify/dist/slugify.min.js',
                    bower_components + 'select2/dist/js/select2.full.min.js',
                    bower_components + 'select2/dist/js/i18n/es.js',
                    jsResources + 'application/select-box.js',
                    jsResources + 'admin/select-box-planner.js',
                    jsResources + 'admin/sidebar.js',
                    jsResources + 'admin/jquery-jqgalviewii-admin.js',
                    jsResources + 'admin/slugify.js',
                    jsResources + 'admin/form.js',
                    jsDestination + 'planner/planner.js'
                ],
                dest: jsDestination + 'administration.min.js'
            },
            'js-admin': {
                src: [
                    bower_components + 'highcharts/highcharts.js',
                    jsResources + "admin/slugify.js",
                    jsResources + "admin/shims.js"
                ],
                dest: jsDestination + 'admin.min.js'
            },
            'js-widget': {
                src: [
                    bower_components + 'jquery/dist/jquery.min.js',
                    bower_components + 'tether/dist/js/tether.min.js',
                    bower_components + 'bootstrap/dist/js/bootstrap.min.js',
                    bower_components + 'Swiper/dist/js/swiper.jquery.min.js'
                ],
                dest: jsDestination + 'widget.min.js'
            }
        },

        copy: {
            'admin': {
                expand: true,
                cwd: bower_components,
                src: [
                    'uploadify/uploadify.swf',
                    'uploadify/uploadify-cancel.png'
                ],
                dest: vendorDestination
            },
            jquery: {
                expand: true,
                cwd: bower_components + 'jquery/dist/',
                src: 'jquery.min.map',
                dest: jsDestination,
                flatten: true,
                filter: 'isFile'
            },
            toastr: {
                expand: true,
                cwd: bower_components + 'toastr/',
                src: 'toastr.js.map',
                dest: jsDestination,
                flatten: true,
                filter: 'isFile'
            },
            'calendar': {
                expand: true,
                cwd: node_modules + 'docbiz_calendar/dist/',
                src: '**',
                dest: 'web/calendar/',
                filter: 'isFile'
            }
        },

        watch: {
            'scss': {
                files: [scssResources + '**/*.scss', scssResources + '**/*.sass'],
                tasks: ['css-application', 'css-dashboard', 'css-dashboard_calendar', 'css-widget']
            },

            'slim': {
                files: [
                    '**/*slim'
                ],
                tasks: [ 'newer:slim' ]
            },

            'js': {
                files: [jsDestination + '/at.js',
                    jsDestination + '/map.js',
                    jsDestination + '/calendar.js',
                    jsDestination + '/search.js'],
                tasks: ['uglify', 'concat']
            }
        }
    });

    // These plugins provide necessary tasks.
    require('load-grunt-tasks')(grunt, {scope: 'devDependencies'});

    grunt.registerTask('dist-html', ['slim']);


    /* GLOBAL *********************************************** */

    // Admin ***********************************************
    grunt.registerTask('css-admin', [
        'sass:admin',
        'cssmin:admin',
        'concat:css-admin'
    ]);

    grunt.registerTask('js-administration', [
        'newer:uglify',
        'newer:concat:js-common',
        'concat:js-administration',
        'copy:admin'
    ]);

    grunt.registerTask('js-admin', [
        'newer:uglify',
        'concat:js-admin'
    ]);

    // Application ***********************************************
    grunt.registerTask('css-application', [
        'sass:application',
        'cssmin:application',
        'concat:css-application'
    ]);

    grunt.registerTask('js-application', [
        'newer:uglify',
        'newer:concat:js-common',
        'concat:js-application',
        'concat:js-searchmap'
    ]);

    // Dashboard ***********************************************
    grunt.registerTask('css-dashboard', [
        'sass:dashboard',
        'cssmin:dashboard',
        'concat:css-dashboard'
    ]);

    grunt.registerTask('css-dashboard_calendar', [
        'sass:dashboard_calendar',
        'cssmin:dashboard_calendar',
        'concat:css-dashboard_calendar'
    ]);

    grunt.registerTask('js-dashboard', [
        'newer:uglify',
        'newer:concat:js-common',
        'concat:js-dashboard'
    ]);

    grunt.registerTask('js-dashboard_calendar', [
        'concat:js-dashboard_calendar',
        'copy:calendar'
    ]);

    // Widget ***********************************************
    grunt.registerTask('css-widget', [
        'sass:widget',
        'cssmin:widget',
        'concat:css-widget'
    ]);

    grunt.registerTask('js-widget', [
        'newer:uglify',
        'newer:concat:js-common',
        'concat:js-widget'
    ]);

    grunt.registerTask('admin', ['css-admin', 'js-administration']);

    grunt.registerTask('app', ['css-application', 'js-application']);

    grunt.registerTask('dashboard', ['css-dashboard', 'js-dashboard', 'css-dashboard_calendar', 'js-dashboard_calendar']);

    grunt.registerTask('widget', ['css-widget', 'js-widget']);

    // Full distribution task ******************************
    grunt.registerTask('all', ['admin', 'app', 'dashboard','widget']);

    grunt.registerTask('default', ['all']);
};
