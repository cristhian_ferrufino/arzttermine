$(document).ready(function() {

  $('[data-behavior~=copy-content]').on('click', function() {
    this.focus();
    this.select();
  });

});
