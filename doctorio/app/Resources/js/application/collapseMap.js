$(function(doc, win) {

  $('[data-behavior~=collapse-map]').on('click', function(e) {
    e.preventDefault();
    $button = $(this);
    $target = $($button.data('target'));
    $target.toggleClass('is-expanded');
    $button.toggleClass('is-active');

    // There is a css animation, so kludge the trigger using a timeout
    win.setTimeout(function() {$(doc).trigger('AT:Maps:update'); }, 250);
  });

}(document, window));
