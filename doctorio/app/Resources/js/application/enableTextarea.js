$(document).ready(function() {

  $('[data-behavior~=enable-textarea]').on('click', function() {

    target = $($(this).data('target'));
    publish_button = $($(this).data('button-replacement'));

    target.prop('disabled', false);
    publish_button.show();
    $(this).hide();

  });

});
