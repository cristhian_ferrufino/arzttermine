var calendarApp = angular.module('calendarApp', [
  'ngResource',
  'ngSanitize',
  'ngAnimate',
  'ui.bootstrap',
  'ui.calendar',
  'ui.router',
  'ui.select',
  'ui.timepicker',
  'pikaday',
  'pascalprecht.translate',
  'angularMoment',
  'ngCookies',
  'ngToast',
  'ngSnakeCamel'
]);

calendarApp.constant('defaultCalendarConfig', {
  defaultDuration: 3600,
  stepInterval: 600,
  openingTimes: {
    monday: null,
    tuesday: null,
    wednesday: null,
    thursday: null,
    friday: null,
    saturday: null,
    sunday: null
  },
  eventDurationEditable: false,
  eventStartEditable: false,
  disableResizing: true,
  disableDragging: true,
  apiEndpoint: null,
  widgets: ['eventCreate', 'monthNavigation', 'resourceNavigation'],
  fallbackLanguage: 'de',
  availableLanguageKeys: {
    languageKeys: ['de'],
    aliases: {
      'en_US': 'en',
      'en_UK': 'en',
      'de_DE': 'de',
      'de_CH': 'de',
      'es_ES': 'es'
    }
  },
  timezone: 'Europe/Berlin',
  timeFormat: 'DD.MM.YYYY HH:mm',
  firstDayOfWeek: 1,
  popoverEventParams: {},
  apiToken: null,
  rootPath: ''
});

calendarApp.run(function ($http, calendarConfig) {
  if (calendarConfig.apiToken) {
    if (calendarConfig.apiToken) {
      $http.defaults.headers.common['Authorization'] = 'Bearer ' + calendarConfig.apiToken;
    }
    $http.defaults.headers.common['Content-Type'] = 'application/json';
  }
});

calendarApp.provider('calendarConfig', function (defaultCalendarConfig) {
  var config = angular.extend({}, defaultCalendarConfig);

  if (typeof CALENDAR_CONFIG !== 'undefined') {
    config = angular.extend(config, CALENDAR_CONFIG);
  }

  this.$get = function () {
    return config;
  };
});

calendarApp.factory('apiEndpoint', function (calendarConfig) {
  return calendarConfig.apiEndpoint;
});

calendarApp.factory('rootPath', function (calendarConfig) {
  return calendarConfig.rootPath;
});

calendarApp.factory('ApiTransform', function ($filter) {
  return {
    request: function (data) {
      return $filter('snake')(angular.toJson(data));
    },
    response: function (data) {
      return $filter('camel')(angular.fromJson(data));
    }
  };
});

calendarApp.constant('dateFormat', 'YYYY-MM-DD');
calendarApp.constant('timeFormat', 'HH:MM');

calendarApp.config(function ($compileProvider) {
  $compileProvider.debugInfoEnabled(false);
});

// Configure angular-translate
calendarApp.config(function ($translateProvider, angularMomentConfig, calendarConfigProvider) {
  var config = calendarConfigProvider.$get();
  var rootPath = config.rootPath;

  $translateProvider
    .useStaticFilesLoader({
      prefix: rootPath + 'config/languages/locale-',
      suffix: '.json'
    })
    .fallbackLanguage(config.fallbackLanguage)
    .registerAvailableLanguageKeys(config.availableLanguageKeys.languageKeys, config.availableLanguageKeys.aliases)
    .useSanitizeValueStrategy('escape');

  if (config.language) {
    $translateProvider.use(config.language);
  } else {
    $translateProvider.determinePreferredLanguage();
    config.language = $translateProvider.preferredLanguage();
  }
});

// Configure Moment.js
calendarApp.config(function (calendarConfigProvider, moment) {
  var config = calendarConfigProvider.$get();
  moment.locale(config.language);
  moment.tz.setDefault(config.timezone);
});

// Configure ngToast
calendarApp.config(function (ngToastProvider) {
  ngToastProvider.configure({
    horizontalPosition: 'center',
    animation: 'slide',
    timeout: 3500
  });
});

// Configure pikaday-angular
calendarApp.run(function (pikadayConfig, calendarConfig, $translate) {
  var momentTranslations = moment().localeData();
  var pickadayKeys = ['PICKADAY.PREVIOUS_MONTH', 'PICKADAY.NEXT_MONTH'];
  var weekdaysShort = _.map(momentTranslations._weekdaysShort, function (wd) {
    return wd.slice(0, 2);
  });

  $translate(pickadayKeys).then(function (t) {
    pikadayConfig['firstDay'] = calendarConfig.firstDayOfWeek;
    pikadayConfig['showDaysInNextAndPreviousMonths'] = true;

    pikadayConfig['i18n'] = {
      previousMonth: t['PICKADAY.PREVIOUS_MONTH'],
      nextMonth: t['PICKADAY.NEXT_MONTH'],
      months: momentTranslations._months,
      weekdays: momentTranslations._weekdays,
      weekdaysShort: weekdaysShort
    };
  });
});

// Configure jquery-timepicker
calendarApp.run(function (uiTimepickerConfig, calendarConfig) {
  var timeFormat = moment().localeData().longDateFormat('LT');

  // 24 hours format?
  if (timeFormat == 'HH:mm') {
    uiTimepickerConfig.show2400 = true;
    uiTimepickerConfig.timeFormat = 'H:i';
  }

  uiTimepickerConfig.step = calendarConfig.stepInterval / 60;
  uiTimepickerConfig.asMoment = true;
  uiTimepickerConfig.forceRoundTime = true;
});

// Configure sce whitelist
calendarApp.config(function ($sceDelegateProvider) {
    $sceDelegateProvider.resourceUrlWhitelist([
        // Allow same origin resource loads.
        'self',
        // Allow loading from our assets domain.  Notice the difference between * and **.
        'https://s3.eu-central-1.amazonaws.com/atcalendar.arzttermine.de/calendar/**',
        'https://atcalendarstatic.arzttermine.de/calendar/**',
        'https://atcalendarstatic.arzttermine.de/staging-calendar/**'
    ]);
});

calendarApp.run(function ($window, NoConnectionModal) {
  $window.addEventListener('offline', function () {
    NoConnectionModal.open();
  }, false);
});

window.paceOptions = {
  ajax: {
    trackMethods: ['GET', 'POST', 'DELETE', 'PUT', 'PATCH']
  }
};
