calendarApp.config(function ($stateProvider, $urlRouterProvider, calendarConfigProvider) {
  var config = calendarConfigProvider.$get();
  var rootPath = config.rootPath;

  $urlRouterProvider.otherwise(function () {
      var existingFragments = window.location.href.split('#');

      if (existingFragments.length > 1) {
          return existingFragments.pop();
      } else {
          return '/calendar/month';
      }
  });

  $stateProvider
    .state('calendar', {
      url: '/calendar',
      templateUrl: rootPath + 'scripts/components/calendar/calendar.html',
      controller: 'CalendarController',
      abstract: true,
      resolve: {
        resources: function (ResourceService) {
          return ResourceService.query();
        },
        calendarResources: function (ResourceService) {
          return ResourceService.query({filter_type: 'none'});
        }
      }
    })
    .state('calendar.month', {
      url: '/month',
      data: {
        show: 'month'
      }
    })
    .state('calendar.agendaWeek', {
      url: '/agendaWeek',
      data: {
        show: 'agendaWeek'
      }
    })
    .state('calendar.agendaDay', {
      url: '/agendaDay',
      data: {
        show: 'agendaDay'
      }
    })
    .state('event', {
      url: '/event',
      controller: 'EventNewController',
      templateUrl: rootPath + 'scripts/components/event/form/event-form.html',
      params: {event: null},
      resolve: {
        resources: function (ResourceService) {
          return ResourceService.query();
        },
        treatments: function(TreatmentService) {
          return TreatmentService.query();
        }
      }
    })
    .state('eventEdit', {
      url: '/event/:eventId',
      controller: 'EventEditController',
      templateUrl: rootPath + 'scripts/components/event/form/event-form.html',
      resolve: {
        event: function ($stateParams, EventService) {
          return new Promise(function(resolve) {
              EventService.get($stateParams.eventId).then(resolve, function () {
                  resolve(null);
              });
          });
        },
        resources: function (ResourceService) {
          return ResourceService.query();
        },
        treatments: function(TreatmentService) {
          return TreatmentService.query();
        }
      }
    })
    .state('eventCancel', {
      url: '/event/:eventId/cancel',
      controller: 'EventEditController',
      templateUrl: rootPath + 'scripts/components/event/form/event-form.html',
      resolve: {
        event: function ($stateParams, EventService) {
          return new Promise(function(resolve) {
            EventService.get($stateParams.eventId).then(resolve, function () {
                resolve(null);
            });
          });
        },
        resources: function (ResourceService) {
          return ResourceService.query();
        },
        treatments: function(TreatmentService) {
          return TreatmentService.query();
        }
      }
    });

});

calendarApp.run(function ($rootScope, NoConnectionModal) {
  NoConnectionModal.preloadTemplate();

  $rootScope.$on('$stateChangeError', function () {
    NoConnectionModal.open();
  });
});
