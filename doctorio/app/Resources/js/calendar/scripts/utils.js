calendarApp.directive('endDateValidator', function ($timeout) {
  return {
    require: 'ngModel',
    link: function (scope, elm, attrs, ctrl) {

      var validator = function () {
        $timeout(function () {
          var valid = true;

          if (scope.showEndDate === false) {
            // if in week view, validation depends on time, not on date
            var startTime = scope.eventForm.startTime.$modelValue;
            var endTime = scope.eventForm.endTime.$modelValue;
            valid = !startTime.isAfter(endTime) && !startTime.isSame(endTime);

          } else {
            var modelValue = scope.event.endDate.seconds(0);
            var compareValue = scope.event.startDate.seconds(0);

            var diff = modelValue.diff(compareValue, 'minutes');
            if (modelValue.format('YYYY/MM/DD') == compareValue.format('YYYY/MM/DD')) {
              valid = (diff >= 0);
            } else {
              valid = (diff > 0);
            }
          }

          scope.eventForm.endDate.$setValidity('endDateValidator', valid);
          if (!scope.event.allDay) {
            scope.eventForm.endTime.$setValidity('endDateValidator', valid);
          }
        });
      };

      scope.$watch('event.startDate', validator);

      ctrl.$parsers.unshift(function (modelValue) {
        validator();
        return modelValue;
      });
    }
  };
});

calendarApp.directive('ieSelectFix', function () {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function (scope, element, attributes) {
      var isIE = document.documentMode;

      if (!isIE) {
        return;
      }

      var control = element[0];
      scope.$watch(attributes.ieSelectFix, function () {
        setTimeout(function () {
          var option = document.createElement('option');
          control.add(option, null);
          control.remove(control.options.length - 1);
        }, 0);
      });
    }
  };
});

calendarApp.directive('uiSelectRequired', function () {
  return {
    require: 'ngModel',
    link: function (scope, elm, attrs, ctrl) {
      scope.$watch(attrs.ngModel, function (value) {
        var enabled = (attrs.uiSelectRequired !== 'false');
        var valid = (!enabled || (value && value.length > 0)) || false;

        ctrl.$setValidity('uiSelectRequired', valid);
      });
    }
  };
});

// Validates that either a placeholder or value was set
calendarApp.directive('placeholderFallback', function () {
  return {
    require: 'ngModel',
    scope: {
      'placeholder': '@'
    },
    link: function (scope, elm, attrs, ctrl) {
      ctrl.$validators.placeholderFallback = function (modelValue) {
        var emptyPlaceholder = (scope.placeholder == '');
        var emptyValue = ctrl.$isEmpty(modelValue);
        return (!emptyPlaceholder || !emptyValue);
      };

      scope.$watch('placeholder', function (newValue, oldValue) {
        if (newValue !== oldValue) {
          ctrl.$validate();
        }
      });
    }
  };
});

calendarApp.directive('placeholder', function ($document, $timeout) {
    var propName, needsShimByNodeName;
    propName = 'placeholder';
    needsShimByNodeName = {};

    angular.forEach(['INPUT', 'TEXTAREA'], function (val) {
      needsShimByNodeName[val] = $document[0].createElement(val)[propName] === undefined;
    });

    function isPasswordType(type) {
      return type && type.toLowerCase() === 'password';
    }

    return {
      require: '^ngModel',
      restrict: 'A',
      link: function ($scope, $element, $attributes, $controller) {
        var className, needsShim;
        className = $attributes[propName + 'Class'] || propName;
        needsShim = needsShimByNodeName[$element[0].nodeName];

        $element.bind('blur', function () {
          var currentValue;
          currentValue = $element.val();

          if (!currentValue) {
            $element.addClass(className);

            if (needsShim) {
              $timeout(function () {
                $element.val($attributes[propName]);
              }, 1);
            }
          }
        });

        $attributes.$observe('placeholder', function () {
          $controller.$viewValue = $controller.$formatters.reduceRight(function (prev, fn) {
            return fn(prev);
          }, $controller.$modelValue);
          $controller.$render();
        });

        $element.bind('focus', function () {
          if (needsShim && $element.hasClass(className)) {
            $element.val('');
          }

          $element.removeClass(className);
        });

        if (needsShim) {
          $controller.$formatters.unshift(function (val) {
            if (isPasswordType($element.prop('type'))) {
              return val;
            }

            if (val) {
              $element.removeClass(className);
              return val;
            }

            $element.addClass(className);
            return $attributes[propName];
          });
        }
      }
    };
  }
);

calendarApp.directive('hoverColor', function () {
    return {
      restrict: 'A',
      scope: {
        hoverColor: '='
      },
      link: function ($scope, $element) {
        var originalColor = $element.css('color');

        $element.hover(function() {
          $element.css('color', $scope.hoverColor);
        }, function() {
          $element.css('color', originalColor);
        });
      }
    };
  });
