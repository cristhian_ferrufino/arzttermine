calendarApp.factory('ResourceService', function ($rootScope, $resource, $cacheFactory, $q, apiEndpoint, ApiTransform) {
  var cache = $cacheFactory('resources');

  var transformResources = function (data) {
    return angular.fromJson(ApiTransform.response(data)).data;
  };

  var apiResource = $resource(apiEndpoint + 'resources/:id', {id: '@id'}, {
    'query': {method: 'GET', isArray: true, cache: cache, transformResponse: transformResources}
  });

  return {
    query: function (params) {
      return apiResource.query(params).$promise;
    },
    get: function (ids) {
      return this.query().then(function (resourceGroups) {
        var resources = _.flatten(_.map(resourceGroups, 'resources'));

        return _.filter(resources, function(resource) {
          return _.includes(ids, resource.id);
        });
      });
    }
  };
});
