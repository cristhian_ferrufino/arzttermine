calendarApp.factory('PaidFeatureService', function ($window) {

    function hasPaidFeature(key) {
        return $window.at_paidFeatures && $window.at_paidFeatures.some(function (feature) {
            return feature.name === key;
        });
    }

    return {

        canSendBookingSmsReminder: function () {
            return hasPaidFeature('sms_booking_reminder')
        },
        canSendBookingMovedSms: function () {
            return hasPaidFeature('sms_booking_movement')
        }

    };
});
