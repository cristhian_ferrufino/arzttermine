calendarApp.factory('ToastService', function ($translate, ngToast) {
  return {
    settings: ngToast.settings,
    messages: ngToast.messages,
    dismiss: ngToast.dismiss,
    create: function (msg, fallback) {
      if (!angular.isString(msg) || msg == '') {
        msg = fallback;
      }

      $translate(msg).then(function (translatedMsg) {
        return ngToast.create(translatedMsg);
      });
    },
    success: function (msg, fallback) {
      if (!angular.isString(msg) || msg == '') {
        msg = fallback;
      }

      $translate(msg).then(function (translatedMsg) {
        return ngToast.success(translatedMsg);
      });
    },
    info: function (msg, fallback) {
      if (!angular.isString(msg) || msg == '') {
        msg = fallback;
      }

      $translate(msg).then(function (translatedMsg) {
        return ngToast.info(translatedMsg);
      });
    },
    warning: function (msg, fallback) {
      if (!angular.isString(msg) || msg == '') {
        msg = fallback;
      }

      $translate(msg).then(function (translatedMsg) {
        return ngToast.warning(translatedMsg);
      });
    },
    danger: function (msg, fallback) {
      if (!angular.isString(msg) || msg == '') {
        msg = fallback;
      }

      $translate(msg).then(function (translatedMsg) {
        return ngToast.danger(translatedMsg);
      });
    }
  };
});
