calendarApp.controller('NoConnectionModalController', function ($scope, $window) {

  $scope.reload = function () {
    $window.location.reload();
  };

});

calendarApp.factory('NoConnectionModal', function ($uibModal, $http, $templateCache, calendarConfig) {
  var templatePath = calendarConfig.rootPath + 'scripts/components/modal/no-connection-modal.html';

  return {
    preloadTemplate: function () {
      $http.get(templatePath, {cache: $templateCache});
    },
    open: function () {
      return $uibModal.open({
        animation: true,
        templateUrl: templatePath,
        controller: 'NoConnectionModalController'
      }).result;
    }
  };
});
