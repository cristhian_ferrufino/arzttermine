calendarApp.controller('ConfirmModalController', function ($scope, $uibModalInstance, meta) {
  $scope.title = meta.title;
  $scope.description = meta.description;
  $scope.action = meta.action;

  $scope.confirm = function () {
    $uibModalInstance.close();
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});

calendarApp.factory('ConfirmModal', function ($uibModal, calendarConfig) {
  var templatePath = calendarConfig.rootPath + 'scripts/components/modal/confirm-modal.html';

  return {
    open: function (meta) {
      return $uibModal.open({
        animation: true,
        templateUrl: templatePath,
        controller: 'ConfirmModalController',
        resolve: {
          meta: meta
        }
      }).result;
    }
  };
});
