calendarApp.factory('EventService', function ($rootScope, $resource, $cacheFactory, $q, apiEndpoint, $filter, ApiTransform) {
  var cache = $cacheFactory('events');

  var clearCache = function () {
    cache.removeAll();
    $rootScope.$broadcast('eventCacheCleared');
  };

  var cacheInterceptor = {
    response: function (response) {
      clearCache();
      return response;
    }
  };

  var transformEvent = function (data) {
    var event = angular.fromJson(ApiTransform.response(data));

    event.startDate = moment(event.startDate);
    event.endDate = moment(event.endDate);

    if (event.repeatOptions && angular.isNumber(event.repeatOptions.repeatInterval)) {
      event.repeatOptions.repeatInterval = String(event.repeatOptions.repeatInterval);
    }

    return event;
  };

  var transformEvents = function (data) {
    return angular.fromJson(ApiTransform.response(data)).data;
  };

  var transformResources = function (data) {
    var event = angular.fromJson(ApiTransform.request(data));

    event.resources = _.flatten(event.resources);

    return angular.toJson(event);
  };

  var updateTitle = function(event) {
    if (event.title == '' || angular.isUndefined(event.title)) {
      event.title = event.titlePlaceholder;
    }
    event.titlePlaceholder = undefined;
  };

  var apiResource = $resource(apiEndpoint + 'events/:id', {id: '@id'}, {
    'get': {method: 'GET', transformResponse: transformEvent},
    'query': {method: 'GET', isArray: true, cache: cache, transformResponse: transformEvents},
    'create': {method: 'POST', interceptor: cacheInterceptor, transformRequest: transformResources},
    'update': {method: 'PUT', interceptor: cacheInterceptor, transformRequest: ApiTransform.request},
    'move': {method: 'POST', interceptor: cacheInterceptor, transformRequest: angular.toJson, url: apiEndpoint + 'events/:id/move'},
    'delete': {method: 'POST', interceptor: cacheInterceptor, transformRequest: angular.toJson}
  });

  return {
    clearCache: clearCache,
    queryResources: function (resourceGroups, selectedResources, startDate, endDate) {
      var deferred = $q.defer();
      var queue = [];

      angular.forEach(resourceGroups, function (resourceGroup) {
        _.each(resourceGroup.resources, function (resource) {
          if (!_.includes(selectedResources, resource.id)) {
            return;
          }

          var params = {
            startDate: startDate.format('YYYY-MM-DD'),
            endDate: endDate.format('YYYY-MM-DD'),
            resourceId: resource.id
          };

          var eventDeferred = $q.defer();
          apiResource.query(angular.fromJson(ApiTransform.request(params))).$promise.then(function (apiEvents) {

            var events = apiEvents.map(function (event) {
              var start = moment(event.startDate);
              var end = (event.allDay) ? moment(event.endDate).add(1, 'day') : event.endDate;

              return {
                title: event.title,
                id: event.id,
                start: start,
                end: end,
                allDay: event.allDay,
                backgroundColor: resource.color
              };
            });

            eventDeferred.resolve(events);
          }, eventDeferred.reject);

          queue.push(eventDeferred.promise);
        });
      });

      $q.all(queue).then(function (loadedResources) {
        deferred.resolve(_.flatten(loadedResources));
      }, deferred.reject);

      return deferred.promise;
    },
    get: function (id) {
      return apiResource.get({id: id}).$promise;
    },
    create: function (event) {
      updateTitle(event);
      return apiResource.create({}, event).$promise;
    },
    update: function (event) {
      updateTitle(event);
      return apiResource.update({id: event.id}, event).$promise;
    },
    move: function(id, event) {
      return apiResource.move({id: id}, event).$promise;
    },
    delete: function (id, body) {
      return apiResource.delete({id: id}, body).$promise;
    }
  };
});
