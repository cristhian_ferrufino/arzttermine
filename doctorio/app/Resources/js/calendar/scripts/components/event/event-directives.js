var tzOffset = -1 * new Date().getTimezoneOffset();

calendarApp.directive('eventRange', function () {
  return {
    scope: {
      event: '=eventRange'
    },
    link: function ($scope, $element) {
      var refreshCallback = function () {
        var html = '';
        var dayFormat = 'LL';
        var timeFormat = 'HH:mm';

        var startDay = moment($scope.event.startDate).format(dayFormat);
        var endDay = ($scope.event.endDate) ? moment($scope.event.endDate).format(dayFormat) : null;
        var allDay = $scope.event.allDay;

        if (endDay && startDay != endDay) {
          html += [startDay, endDay].join(' - ');
        } else {
          html += startDay;
        }

        if (!allDay) {
          var startTime = moment($scope.event.startDate).utcOffset(tzOffset).format(timeFormat);
          var endTime = moment($scope.event.endDate).utcOffset(tzOffset).format(timeFormat);

          html += ', ';
          html += startTime + ' - ' + endTime;
        }

        $element.html(html);
      };

      var watchedAttributes = [
        'event.startDate',
        'event.endDate'
      ];

      $scope.$watchCollection(watchedAttributes, refreshCallback);
    }
  };
});

calendarApp.directive('eventTitle', function () {
  return {
    link: function ($scope, $element) {
      var color = $scope.event.backgroundColor;
      var html = '<h3 style="color:' + color + '; margin-top: -7px;">' + $scope.event.title + '</h3>';
      $element.html(html);
    }
  };
});

calendarApp.directive('eventRepeatSummary', function ($translate, $filter) {
  var weekdayFields = ['repeatMonday', 'repeatTuesday', 'repeatWednesday', 'repeatThursday', 'repeatFriday', 'repeatSaturday', 'repeatSunday'];

  function getWeekDayCountOfMonth(start) {
    var day = moment(start).date();
    var count = 0;
    while (day > 0) {
      count++;
      day -= 7;
    }
    return count;
  }

  return {
    restrict: 'EA',
    scope: {
      event: '='
    },
    link: function ($scope, $element) {
      $scope.$watch('event', function (event) {
        var summary = null;
        var options = event.repeatOptions;

        if (options.repeatInterval == 1) {
          summary = $translate.instant('EVENT_FORM.REPEAT.' + $filter('uppercase')(options.repeatFrequency));
        } else {
          summary = $translate.instant('EVENT_FORM.REPEAT.ALL') + ' ' + options.repeatInterval + ' ' + $translate.instant('EVENT_FORM.REPEAT.INTERVALS.' + $filter('uppercase')(options.repeatFrequency));
        }

        if (options.repeatFrequency == 'weekly') {
          var repeatDays = [];

          angular.forEach(weekdayFields, function (day) {
            if (options[day]) {
              repeatDays.push($translate.instant('EVENT_FORM.REPEAT.REPEAT_DAYS.' + $filter('uppercase')(day.replace('repeat', ''))));
            }
          });

          if (repeatDays.length == 7) {
            summary += ' ' + $translate.instant('EVENT_FORM.REPEAT.ON_ALL_DAYS');
          } else if (repeatDays.length > 0) {
            summary += ' ' + $translate.instant('EVENT_FORM.REPEAT.AT') + ' ' + repeatDays.join(', ');
          }
        } else if (options.repeatFrequency == 'monthly') {
          if (options.repeatMonthlyMode == 'day') {
            summary += ' ' + $translate.instant('EVENT_FORM.REPEAT.AT') + ' ' + moment(event.startDate).format('Do');
          } else if (options.repeatMonthlyMode == 'weekday') {
            summary += ' ' + $translate.instant('EVENT_FORM.REPEAT.AT') + ' ' + getWeekDayCountOfMonth(event.startDate)
              + '. ' + moment(event.startDate).format('dddd');
          }
        } else if (options.repeatFrequency == 'yearly') {
          summary += ' ' + $translate.instant('EVENT_FORM.REPEAT.AT') + ' ' + moment(event.startDate).format('Do MMMM');
        }

        if (options.repeatEnd == 'interval' && options.repeatEndInterval) {
          summary += ', ' + $translate.instant('EVENT_FORM.REPEAT.END_AFTER_INTERVAL', {interval: options.repeatEndInterval});
        } else if (options.repeatEnd == 'date' && options.repeatEndDate) {
          summary += ', ' + $translate.instant('EVENT_FORM.REPEAT.END_AT', {date: moment(options.repeatEndDate).format('Do MMMM YYYY')});
        }

        $element.html(summary);
      }, true);
    }
  };
});
