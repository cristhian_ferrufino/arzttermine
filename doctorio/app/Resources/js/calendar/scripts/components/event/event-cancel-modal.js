calendarApp.factory('EventCancelModal', function ($templateRequest, $compile, CancelBookingModal, $q) {
  return {
    open: function (event) {
      var deferred = $q.defer();

      CancelBookingModal.open({
        bookingId: event.id,
        cancelReasons: [
          'APPOINTMENT_RESCHEDULED',
          'DOCTOR_REQUESTED_CANCELLATION',
          'PATIENT_REQUESTED_CANCELLATION',
          'BOOKING_ACCIDENT',
          'BOOKING_DUPLICATE'
        ]
      }).then(deferred.resolve.bind(deferred));

      return deferred.promise;
    }
  };
});
