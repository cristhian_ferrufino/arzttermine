calendarApp.controller('EventNewPopoverController', function ($scope, $filter, $rootScope, $state, EventService, PopoverService, FilterService, calendarConfig, ToastService, ResourceService) {
  ResourceService.get($scope.event.resources).then(function (resources) {
    $scope.resources = resources;
  });

  $scope.submitEvent = function () {
    EventService.create($scope.event).then(function (response) {
      ToastService.success(response.resource.message, 'TOAST.SUCCESS.SAVE');
      FilterService.addResources(response.resource.event.resources);
      PopoverService.hide();
    }, function (error) {
      ToastService.danger((error.resource || {}).message, 'TOAST.ERRORS.SAVE');
    });
  };

  $scope.editEvent = function () {
    PopoverService.hide();
    $state.go('event', {event: $scope.event});
  };
});
