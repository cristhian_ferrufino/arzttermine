calendarApp.controller('EventNewController', function ($scope, $filter, $uibModal, $state, $stateParams, PaidFeatureService, ToastService, FilterService, EventService, resources, treatments, calendarConfig) {
    var defaultEvent = {
      repeat: false,
      repeatOptions: {
        repeatFrequency: 'monthly',
        repeatInterval: '1',
        repeatMonthlyMode: 'day',
        repeatEnd: 'never'
      },
      patient: {},
      startDate: moment(),
      endDate: moment().add(calendarConfig.stepInterval, 'seconds')
    };
  
    $scope.event = angular.extend({}, defaultEvent, $stateParams.event || {});
    $scope.event.resources = _.uniq($scope.event.resources);
    $scope.disableSubmit = false;

    // set default resources
    angular.forEach(resources, function (resourceGroup) {
      if (resourceGroup.multiple) {
        resourceGroup.selectedResources = [];
      }

      if (resourceGroup.required && resourceGroup.resources.length < 2) {
        var resourceId = _.map(resourceGroup.resources, 'id')[0];
        resourceGroup.disabled = true;

        if (resourceGroup.multiple) {
          resourceGroup.selectedResources.push(resourceId);
        } else {
          resourceGroup.selectedResourceId = resourceId;
        }
      }
    });

    $scope.canSendBookingSmsReminder = PaidFeatureService.canSendBookingSmsReminder();

    $scope.resourceGroups = resources;
    $scope.treatments = treatments;

     $scope.submit = function (eventForm) {
       var emailIsEmpty = angular.isUndefined($scope.event.patient.email) || $scope.event.patient.email == '';
       var validFirstName = angular.isDefined($scope.event.patient.firstName) && $scope.event.patient.firstName != '';
       var validLastName = angular.isDefined($scope.event.patient.lastName) && $scope.event.patient.lastName != '';
       var nameIsEmpty = !validFirstName || !validLastName;

       if ($scope.event.patient.notifyEmail && emailIsEmpty) {
         ToastService.danger('TOAST.ERRORS.EMAIL_MISSING');
       } else if (!emailIsEmpty && nameIsEmpty) {
         ToastService.danger('TOAST.ERRORS.NAME_MISSING');
       } else if (eventForm.$valid) {
         $scope.disableSubmit = true;
         EventService.create($scope.event).then(function (response) {
           ToastService.success(response.resource.message, 'TOAST.SUCCESS.SAVE');
           FilterService.addResources(response.resource.event.resources);
           $state.go('calendar.month');
         }, function (error) {
           $scope.disableSubmit = false;
           ToastService.danger((error.resource || {}).message, 'TOAST.ERRORS.SAVE');
         });
       }
    };

    $scope.cancel = function () {
      $state.go('calendar.month');
    };
  }
);
