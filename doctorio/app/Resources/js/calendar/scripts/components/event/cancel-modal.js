calendarApp.controller('CancelBookingModalController', function ($state, $scope, $uibModalInstance, meta, PaidFeatureService, EventService, ToastService) {
    $scope.noNotificationsPrompt = false;
    $scope.cancelReasons = meta.cancelReasons;
    $scope.canSendBookingSmsReminder = PaidFeatureService.canSendBookingSmsReminder();
    $scope.formData = {
        reason: '-',
        appointmentAvailability: 1,
        notificationTypes: {
            email: true,
            sms: PaidFeatureService.canSendBookingSmsReminder(),
            hasNotification: function() {
                return this.email || this.sms;
            }
        }
    };

    $scope.confirmNotificationsPrompt = function() {
        $scope.noNotificationsPrompt = true;
    };

    $scope.reasonChanged = function() {
        if (this.formData.reason === 'BOOKING_DUPLICATE' || this.formData.reason === 'DOCTOR_REQUESTED_CANCELLATION') {
            this.formData.appointmentAvailability = 0;
        } else {
            this.formData.appointmentAvailability = 1;
        }
    };

    $scope.confirm = function (e) {
        e.preventDefault();
        e.stopPropagation();

        if (this.formData.reason === '-') {
            $scope.reasonValid = true;
            return;
        }

        var notifications = [];

        if (this.formData.notificationTypes.email) {
            notifications.push('EMAIL');
        }
        if (this.formData.notificationTypes.sms) {
            notifications.push('SMS');
        }
        if (!notifications.length && $scope.noNotificationsPrompt !== true) {
            return;
        }

        EventService.delete(meta.bookingId, {
            reason: this.formData.reason,
            appointmentAvailability: this.formData.appointmentAvailability,
            notificationTypes: notifications
        }).then(function () {
            ToastService.success('TOAST.SUCCESS.DESTROY');

            setTimeout(function () {
                $uibModalInstance.close();
                $state.go('calendar.month');
            }, 1000);
        }, function (error) {
            var single = (error instanceof Array) ? error.shift() : error;
            ToastService.danger((single || {}).code, 'TOAST.ERRORS.DESTROY');
        });

        // older browsers
        return false;
    };

    $scope.abort = function () {
        $uibModalInstance.dismiss('cancel');
    };
});

calendarApp.factory('CancelBookingModal', function ($uibModal, calendarConfig) {
    var templatePath = calendarConfig.rootPath + 'scripts/components/modal/cancel-booking-modal.html';

    return {
        open: function (meta) {
            return $uibModal.open({
                animation: true,
                templateUrl: templatePath,
                controller: 'CancelBookingModalController',
                resolve: {
                    meta: meta
                }
            }).result;
        }
    };
});
