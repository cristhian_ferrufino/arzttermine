// This directive forces a sync with ui-time-pickers value to unsure the rounded time is set
calendarApp.directive('timeInput', function () {
  var isAMoment = function (date) {
    return moment !== undefined && moment.isMoment(date) && date.isValid();
  };

  return {
    restrict: 'A',
    require: 'ngModel',
    link: function (scope, element, attrs, ngModel) {
      var asDate = function () {
        var baseDate = ngModel.$modelValue ? ngModel.$modelValue : scope.baseDate;
        return isAMoment(baseDate) ? baseDate.toDate() : baseDate;
      };

      scope.$evalAsync(function () {
        var date = element.timepicker('getTime', asDate());
        ngModel.$setViewValue(date);
        ngModel.$commitViewValue();
        ngModel.$setPristine();
      });
    }
  };
});
