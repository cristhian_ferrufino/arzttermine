calendarApp.controller('MoveBookingModalController', function ($state, $scope, $uibModalInstance, meta, EventService, ToastService, PaidFeatureService) {

    $scope.canSendBookingMovedSms = PaidFeatureService.canSendBookingMovedSms();
    $scope.dayOfAppointment = meta.time.display;
    $scope.noNotificationsPrompt = false;
    $scope.aleadyBlockedError = false;
    $scope.invalidTimeRangeError = false;
    $scope.startBeforeEndError = false;

    $scope.formData = {
        notificationTypes: {
            email: true,
            sms: $scope.canSendBookingSmsReminder,
            hasNotification: function() {
                return this.email || this.sms;
            }
        }
    };

    $scope.confirmNotificationsPrompt = function() {
        $scope.noNotificationsPrompt = true;
    };

    $scope.confirm = function (e) {
        e.preventDefault();
        e.stopPropagation();

        $scope.aleadyBlockedError = false;
        $scope.invalidTimeRangeError = false;
        $scope.startBeforeEndError = false;

        var notifications = [];
        if (this.formData.notificationTypes.email) {
            notifications.push('EMAIL');
        }
        if (this.formData.notificationTypes.sms) {
            notifications.push('SMS');
        }
        if (!notifications.length && $scope.noNotificationsPrompt !== true) {
            return;
        }

        EventService.move(meta.event.appointmentId, {
            notificationTypes: notifications,
            start: meta.time.start,
            end: meta.time.end
        }).then(function () {
            ToastService.success('TOAST.SUCCESS.MOVED');

            // give elasticsearch some time
            setTimeout(function () {
                $uibModalInstance.close();
                $state.go('calendar.month');
            }, 1000);
        }, function (error) {
            var single = (error.data instanceof Array) ? error.data.shift() : error.data;
            if (single && single.code === 'RANGE_ALREADY_BLOCKED') {
                $scope.aleadyBlockedError = true;
            } else if (single && single.code === 'INVALID_ARGUMENT') {
                if (single.reference === 'end' || single.reference === 'start') {
                    $scope.invalidTimeRangeError = true;
                } else if (single.reference === 'startBeforeEnd') {
                    $scope.startBeforeEndError = true;
                }
            } else {
                ToastService.danger((single || {}).code, 'TOAST.ERRORS.DESTROY');
            }
        });

        // older browsers
        return false;
    };

    $scope.abort = function () {
        $uibModalInstance.dismiss('cancel');
    };
});

calendarApp.factory('MoveBookingModal', function ($uibModal, calendarConfig) {
    var templatePath = calendarConfig.rootPath + 'scripts/components/modal/move-booking-modal.html';

    return {
        open: function (meta) {
            return $uibModal.open({
                animation: true,
                templateUrl: templatePath,
                controller: 'MoveBookingModalController',
                resolve: {
                    meta: meta
                }
            }).result;
        }
    };
});
