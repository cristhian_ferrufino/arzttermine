calendarApp.controller('EventEditController', function ($scope, $filter, $uibModal, $state, $stateParams, ToastService, FilterService, event, resources, treatments, EventService, EventCancelModal, MoveBookingModal, PaidFeatureService) {
  if (!event) {
    $scope.notFound = true;
  }

  $scope.event = event;
  $scope.disableSubmit = false;
  $scope.time = { changed: false };

  $scope.resourceGroups = resources;
  $scope.treatments = treatments;

  $scope.canSendBookingSmsReminder = PaidFeatureService.canSendBookingSmsReminder();

  $scope.redirectToCalendar = function() {
    $state.go('calendar.month');
  };

  $scope.submit = function () {
    if ($scope.time.changed) {
      var date = $scope.time.date;
      var start = date.clone().set({
        hour: $scope.time.start.get('hour'),
        minute: $scope.time.start.get('minute')
      });


      var end = start.clone()
          .add($scope.time.duration, 'minutes');

      MoveBookingModal.open({
          event: $scope.event,
          canSendBookingMovementSms: PaidFeatureService.canSendBookingMovedSms(),
          time: {
            display: start.format('DD.MM.YYYY HH:mm') + 'Uhr - ' + end.format('HH:mm') + 'Uhr',
            start: start.format('YYYY-MM-DDTHH:mmZ'),
            end: end.format('YYYY-MM-DDTHH:mmZ')
          },
      });
    }
  };

  $scope.deleteEvent = function () {
    EventCancelModal.open($scope.event);
  };

  $scope.cancel = function () {
    $state.go('calendar.month');
  };

  // trigger cancelModal when route is eventCancel (/event/id/cancel)
  if ($state.current.name === 'eventCancel' && $scope.event) {
    $scope.deleteEvent();
  }
});
