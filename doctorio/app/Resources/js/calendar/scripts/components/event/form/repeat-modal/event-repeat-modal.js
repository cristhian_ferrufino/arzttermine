calendarApp.controller('EventRepeatModalController', function ($scope, $uibModalInstance) {

  if (angular.isUndefined($scope.event.repeatOptions)) {
    $scope.event.repeatOptions = {};
  }

  $scope.close = function () {
    $uibModalInstance.close();
  };

  $scope.cancel = function () {
    $scope.event.repeat = false;
    $uibModalInstance.close();
  };

  $scope.$watch('event.repeatOptions.repeatEnd', function (newValue) {
    $scope.event.repeatOptions.repeatEndInterval = '';
    $scope.event.repeatOptions.repeatEndDate = '';

    if (newValue == 'interval') {
      $scope.event.repeatOptions.repeatEndInterval = 35;
    }
    if (newValue == 'date') {
      $scope.event.repeatOptions.repeatEndDate = $scope.event.startDate.clone().add(1, 'months');
    }
  });
});

calendarApp.factory('EventRepeatModal', function ($templateRequest, $compile, $uibModal, $q, calendarConfig) {
  return {
    open: function (scope) {
      var deferred = $q.defer();
      var rootPath = calendarConfig.rootPath;

      var modal = $uibModal.open({
        animation: true,
        templateUrl: rootPath + 'scripts/components/event/form/repeat-modal/event-repeat-modal.html',
        scope: scope,
        size: 'lg',
        controller: 'EventRepeatModalController'
      });

      modal.result.then(function () {
        deferred.resolve();
      }, function () {
        deferred.reject(null);
      });

      return deferred.promise;
    }
  };
});
