calendarApp.controller('EventFormController', function ($scope, uiTimepickerConfig) {
  $scope.pendingResourceGroups = {};
  $scope.eventHasComment = (angular.isString($scope.event.comment) && ($scope.event.comment != ''));

  // write events resources id's into resourceGroups
  angular.forEach($scope.resourceGroups, function (resourceGroup) {
    angular.forEach($scope.event.resources, function (resourceId) {
      if (_.includes(_.map(resourceGroup.resources, 'id'), resourceId)) {
        if (resourceGroup.multiple) {
          if (!angular.isArray(resourceGroup.selectedResources)) {
            resourceGroup.selectedResources = [];
          }

          resourceGroup.selectedResources.push(resourceId);
        } else {
          resourceGroup.selectedResourceId = resourceId;
        }
      }
    });
  });

  // {@see https://stackoverflow.com/a/149099/3703307}
  var preCompiled = /(\d{3})(?=\d)/g;
  $scope.formatPrice = function(price, digits, d, t) {
      var n = price.amount / 100,
          c = isNaN(c = Math.abs(digits)) ? 2 : digits,
          d = d == undefined ? "." : d,
          t = t == undefined ? "," : t,
          s = n < 0 ? "-" : "",
          i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
          j = (j = i.length) > 3 ? j % 3 : 0;
      return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(preCompiled, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "") + ' €';
  };

  // write selected resource id's flattened into event on any changes
  $scope.$watch('resourceGroups', function (resourceGroups) {
    var resources = [];

    angular.forEach(resourceGroups, function (resourceGroup) {
      if (resourceGroup.multiple) {
        resources = resources.concat(_.compact(resourceGroup.selectedResources));
      } else if (resourceGroup.selectedResourceId) {
        resources.push(resourceGroup.selectedResourceId);
      }
    });

    $scope.event.resources = resources;
  }, true);

  if ($.fn.timepicker.defaults) {
    $.fn.timepicker.defaults.roundingFunction(1000, uiTimepickerConfig);
  }

  $scope.time.date = $scope.event.startDate;
  $scope.time.start = $scope.event.startDate;
  $scope.time.duration = $scope.event.duration;

  $scope.$watch('time.start', function (newValue, oldValue) {
    if (oldValue && newValue) {
      if (newValue.format('HH:mm') !== oldValue.format('HH:mm')) {
        $scope.time.changed = true;
      }
    }
  });

  $scope.$watch('time.duration', function (newValue, oldValue) {
    if (newValue && newValue !== oldValue) {
      $scope.time.changed = true;
    }
  });

  $scope.$watch('time.date', function (newValue, oldValue) {
    if (oldValue && newValue) {
      if (newValue !== oldValue) {
        $scope.time.changed = true;
      }
    }
  });

  $scope.generatePlaceholder = function () {
    if (angular.isUndefined($scope.event.patient)) {
      $scope.event.patient = {};
    }

    var treatmentObject = _.find($scope.treatments, {id: $scope.event.treatmentType});
    var treatment = '';
    var firstName = $scope.event.patient.firstName || '';

    if (treatmentObject) {
      treatment = treatmentObject.title;
      if ($scope.event.patient.firstName || $scope.event.patient.lastName) {
        treatment = ', ' + treatmentObject.title;
      }
    }

    if ($scope.event.patient.lastName) {
      firstName = $scope.event.patient.firstName + ' ';
    }

    return firstName + ($scope.event.patient.lastName || '') + treatment;
  };

  // placeholder generator
  $scope.$watch('event', function (newValue, oldValue) {
    if (newValue === oldValue) {
      return;
    }

    $scope.event.titlePlaceholder = $scope.generatePlaceholder();
  }, true);

  $scope.setTitleValue = function (titlePlaceholder) {
    if ($scope.event.title == '' || angular.isUndefined($scope.event.title)) {
      $scope.event.title = titlePlaceholder;
    }
  };

  if ($scope.event.title == $scope.generatePlaceholder()) {
    $scope.event.title = '';
  }

});
