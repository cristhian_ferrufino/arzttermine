calendarApp.directive('dateInput', function ($window) {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function (scope, element, attrs, ngModel) {
      var moment = $window.moment;

      var formatter = function (value) {
        var date = moment(value);
        var valid = date.isValid();
        return (valid) ? date.format('L') : value;
      };

      ngModel.$formatters.push(formatter);

      element.on('change', function (e) {
        var element = e.target;
        element.value = formatter(ngModel.$modelValue);
      });

      ngModel.$parsers.push(function (value) {
        var date = moment(value);
        var valid = date.isValid();
        var oldDate = ngModel.$modelValue;

        if (moment.isMoment(oldDate)) {
          date = date.hours(oldDate.hours()).minutes(oldDate.minutes()).seconds(oldDate.seconds());
        }

        ngModel.$setValidity('datetime', valid);
        return (valid) ? date : value;
      });
    }
  };
});
