calendarApp.controller('EventEditPopoverController', function ($scope, $filter, $rootScope, $state, EventService, ResourceService, TreatmentService, PopoverService, EventCancelModal) {
  $scope.patientLoaded = false;
  $scope.resourcesLoaded = false;
  $scope.treatmentLoaded = false;

  EventService.get($scope.event.id).then(function (response) {
    $scope.event.patient = response.patient;
    $scope.patientLoaded = true;

    ResourceService.get(response.resources).then(function (resources) {
      $scope.event.resources = resources;
      $scope.resourcesLoaded = true;
    });

    TreatmentService.get([response.treatmentType]).then(function (treatments) {
      $scope.event.treatment = treatments[0];
      $scope.treatmentLoaded = true;
    });
  });

  $scope.editEvent = function () {
    PopoverService.hide();
    $state.go('eventEdit', {eventId: $scope.event.id});
  };

  $scope.deleteEvent = function () {
    PopoverService.hide();
    EventCancelModal.open($scope.event);
  };
});
