calendarApp.config(function (widgetServiceProvider) {
  widgetServiceProvider.registerWidget('resourceNavigation', {
    templateUrl: 'scripts/components/widget/resource-navigation-widget.html'
  });
});

calendarApp.controller('ResourceNavigationWidgetController', function ($scope, $cookies, $timeout, FilterService) {
  var activeResources = FilterService.get('resources');

  $scope.isActiveResource = function (resource) {
    return _.includes(activeResources, resource.id);
  };

  $scope.toggleResource = function (resource) {
    FilterService.toggleResource(resource);
  };
});
