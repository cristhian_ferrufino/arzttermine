calendarApp.config(function (widgetServiceProvider) {
  widgetServiceProvider.registerWidget('eventCreate', {
    templateUrl: 'scripts/components/widget/event-create-widget.html'
  });
});

calendarApp.directive('eventCreateButton', function ($state) {
  return {
    restrict: 'E',
    template: '<a href="' + window.docbiz_calendar_booking_target + '" class="btn btn-primary full-width padding-bottom-20" translate="ACTION.CREATE_EVENT"></a>',
    controller: function ($scope) {
      $scope.createEvent = function () {
        $state.go('event');
      };
    }
  };
});
