calendarApp.config(function (widgetServiceProvider) {
  widgetServiceProvider.registerWidget('monthNavigation', {
    templateUrl: 'scripts/components/widget/month-navigation-widget.html'
  });
});

calendarApp.controller('MonthNavigationWidgetController', function ($scope, $filter, uiCalendarConfig) {
  $scope.date = {};

  $scope.$watch('date.selectedDate', function (newValue, oldValue) {
    if (oldValue !== newValue) {
      var calendar = uiCalendarConfig.calendars.calendarObject;
      calendar.fullCalendar('gotoDate', newValue);

      if (calendar.fullCalendar('getView').name == 'month') {
        calendar.fullCalendar('changeView', 'agendaWeek');
      }
    }
  });

});
