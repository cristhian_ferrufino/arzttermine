calendarApp.provider('widgetService', function (calendarConfigProvider) {
  var widgets = {};
  var config = calendarConfigProvider.$get();
  var rootPath = config.rootPath;

  this.registerWidget = function (key, options) {
    widgets[key] = options;
  };

  this.$get = function ($rootScope, $templateRequest, $compile, $q) {

    function compileTemplate(html, scope) {
      html = '<div>' + html + '</div>';
      var el = angular.element(html);

      return $compile(el)(scope);
    }

    return {
      render: function (keys, scope) {
        var queue = [];

        angular.forEach(keys, angular.bind(this, function (key) {
          queue.push(this.compileWidget(key, scope));
        }));

        return $q.all(queue);
      },
      compileWidget: function (key, scope) {
        var deferred = $q.defer();

        if (!scope) {
          scope = $rootScope.$new();
        }

        var options = widgets[key];
        if (!options) {
          throw 'Unregistered widget';
        }

        if (options.templateUrl) {
          var templateRequest = $templateRequest(rootPath + options.templateUrl);
          templateRequest.then(function (html) {
            deferred.resolve(compileTemplate(html, scope));
          });
        } else if (options.template) {
          deferred.resolve(compileTemplate(options.template, scope));
        }

        return deferred.promise;
      }
    };
  };
});

calendarApp.directive('widgets', function (widgetService, calendarConfig) {
  return {
    restrict: 'E',
    link: function ($scope, $element) {
      var keys = calendarConfig.widgets;

      widgetService.render(keys, $scope).then(function (nodes) {
        $element.append(nodes);
      });
    }
  };
});
