calendarApp.factory('PopoverService', function ($templateRequest, $compile, calendarConfig) {
  var rootPath = calendarConfig.rootPath;

  var closeCallback = function (e) {
    var $target = $(e.target);

    if (!$target.hasClass('popover') && !$target.closest('.popover').length) {
      serviceDef.hide();
    }
  };

  var serviceDef = {
    hide: function () {
      $('.popover').popover('hide');
      $(document).unbind('click', closeCallback);
    },
    show: function (target, scope, options) {
      this.hide();
      var templateRequest = $templateRequest(rootPath + options.templateUrl);
      templateRequest.then(function (html) {
        var el = angular.element(html);
        $compile(el)(scope);

        $(target).popover({
          title: options.title,
          container: 'body',
          content: el,
          html: true,
          size: 'lg',
          trigger: 'focus',
          placement: options.placement
        }).popover('show');

        if (angular.isString(options.class)) {
          $('.popover-content').addClass(options.class);
        }

        $(target).on('shown.bs.popover', function () {
          $(document).bind('click', closeCallback);
        });
      });
    }
  };

  return serviceDef;
});

calendarApp.directive('hidePopover', function (PopoverService) {
  return {
    restrict: 'A',

    link: function ($scope, $element) {
      $element.click(function (e) {
        e.preventDefault();
        PopoverService.hide();
      });
    }
  };
});
