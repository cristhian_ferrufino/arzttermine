calendarApp.factory('CalendarService', function (calendarConfig, $templateRequest, $compile, $timeout, uiCalendarConfig) {
  var timelineRefreshInterval = null;
  var rootPath = calendarConfig.rootPath;

  function secondsToTime(seconds) {
    var sec_num = parseInt(seconds, 10);
    var hours = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours < 10) {
      hours = '0' + hours;
    }
    if (minutes < 10) {
      minutes = '0' + minutes;
    }
    if (seconds < 10) {
      seconds = '0' + seconds;
    }
    var time = hours + ':' + minutes + ':' + seconds;
    return time;
  }

  var dayOfWeekMapping = {
    sunday: 0,
    monday: 1,
    tuesday: 2,
    wednesday: 3,
    thursday: 4,
    friday: 5,
    saturday: 6
  };

  var defaultEvent = {
    rendering: 'background',
    color: '#c3c3c3'
  };

  var dayInSeconds = 86400;

  return {
    getClosedTimes: function () {
      var events = [];

      // convert opening times into events that highlight the closed times
      angular.forEach(calendarConfig.openingTimes, function (durations, day) {
        var dow = [dayOfWeekMapping[day]];
        var times = [];

        for (var i = 0; i < durations.length; i++) {
          var duration = durations[i];
          var nextDuration = durations[i + 1];

          // time until today's opening
          if (i == 0 && duration.start != 0) {
            times.push({startTime: 0, endTime: duration.start});
          }

          times.push({
            startTime: duration.end,
            endTime: nextDuration ? nextDuration.start : dayInSeconds
          });
        }

        times.forEach(function (time) {
          var event = angular.extend({}, defaultEvent, {
            start: secondsToTime(time.startTime),
            end: secondsToTime(time.endTime),
            dow: dow
          });

          events.push(event);
        });
      });

      return events;
    },
    addSlotDurationSelect: function (scope) {
      if (!angular.element('#slot-duration-select').length) {
        var templateURL = rootPath + 'scripts/components/calendar/slot-duration-select.html';
        var templateRequest = $templateRequest(templateURL);
        templateRequest.then(function (html) {
          var el = angular.element(html);
          $compile(el)(scope);
          angular.element('.fc-right').prepend(el);
        });
      }
    },
    renderTimeline: function (calendar) {
      if (!calendar) return;
      var view = calendar.fullCalendar('getView');
      if (!view.intervalStart || view.intervalStart.clone !== 'function') {
        return;
      }

      var currentDate = $.fullCalendar.moment();
      var intervalStart = view.intervalStart.clone()
        .utcOffset(currentDate.utcOffset())
        .startOf('day');
      var intervalEnd = view.intervalEnd.clone()
        .utcOffset(currentDate.utcOffset())
        .startOf('day');

      if (view.name === 'month' || !currentDate.isBetween(intervalStart, intervalEnd)) {
        return;
      }

      var timeGrid = calendar.find('.fc-time-grid');
      var timeline = angular.element('<hr class="timeline" />');
      timeGrid.find('hr.timeline').remove();
      timeGrid.prepend(timeline);

      var calMinTimeInMinutes = moment.duration(view.opt('minTime')).asMinutes();
      var calMaxTimeInMinutes = moment.duration(view.opt('maxTime')).asMinutes();
      var curSeconds = (( ((currentDate.hours() * 60) + currentDate.minutes()) - calMinTimeInMinutes) * 60) + currentDate.seconds();
      var percentOfDay = curSeconds / ((calMaxTimeInMinutes - calMinTimeInMinutes) * 60);
      var topLoc = Math.floor(timeGrid.height() * percentOfDay);
      var timeCol = calendar.find('.fc-time:visible');
      timeline.css({top: topLoc + 'px', left: timeCol.outerWidth(true) + 'px'});

      if (view.name === 'agendaWeek') { // Week view, don't want the timeline to go the whole way across.
        var dayCol = calendar.find('.fc-time-grid .fc-bg .fc-today');
        var left = dayCol.position().left + 1;
        var width = dayCol.width() + 1;
        timeline.css({left: left, width: width});
      }
    },
    stopTimelineRefresh: function () {
      if (timelineRefreshInterval) {
        $timeout.cancel(timelineRefreshInterval);
        timelineRefreshInterval = null;
      }
    },
    refreshTimeline: function ($scope, calendar) {
      this.stopTimelineRefresh();

      var calendarView = calendar || uiCalendarConfig.calendars.calendarObject;
      var callback = this.renderTimeline;

      timelineRefreshInterval = $timeout(function () {
        callback(calendarView);
      }, 1000);

      callback(calendarView);
      $scope.$on('$destroy', this.stopTimelineRefresh);
    }
  };
});
