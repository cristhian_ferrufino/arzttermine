calendarApp.controller('CalendarController', function ($window, $state, $scope, $rootScope, $filter, $controller, $timeout, $translate, $compile, $location, $templateRequest, uiCalendarConfig, EventService, FilterService, CalendarService, PopoverService, calendarConfig, ToastService, resources, calendarResources) {
  $scope.calendarResources = calendarResources;
  $scope.calendarFilter = {};

  $scope.defaultEvents = CalendarService.getClosedTimes();
  $scope.eventSources = [];

  $scope.slotDuration = FilterService.get('slotDuration');
  $scope.calendarView = FilterService.get('view') || $state.current.data.show;

  // mark all resources as selected by default
  FilterService.addDefaultResources(calendarResources, true);

  $scope.getCalendar = function () {
    return uiCalendarConfig.calendars.calendarObject;
  };

  $scope.createEventOnRange = function (start, end) {
    $window.location.href = window.docbiz_calendar_booking_target + '#/appointment?c=' + start.format();
  };

  $scope.moveEvent = function (event) {
    var endDate = event.start;

    // For allDay events
    if (event.end) {
      endDate = (event.allDay) ? moment(event.end).subtract(1, 'day') : event.end;
    }

    var eventHash = {
      startDate: event.start.format(),
      endDate: endDate.format(),
      allDay: event.allDay
    };

    EventService.move(event.id, eventHash);
  };

  $scope.setEvents = function (events) {
    $scope.eventSources[0] = $scope.defaultEvents.concat(events);
  };

  $scope.updateEvents = function () {
    PopoverService.hide();

    EventService.queryResources(calendarResources,
      FilterService.get('resources'),
      FilterService.getDate('intervalStart'),
      FilterService.getDate('intervalEnd')).then($scope.setEvents, function () {
      ToastService.danger('TOAST.ERRORS.QUERY');
    });
  };

  $scope.$on('eventCacheCleared', $scope.updateEvents);
  $scope.$on('filterUpdated', $scope.updateEvents);

  $scope.showEventInformation = function (event, target) {
    var popoverScope = $scope.$new(true);

    var endDate = (event.allDay) ? event.end.subtract(1, 'day') : event.end;

    popoverScope.event = {
      title: event.title,
      id: event.id,
      startDate: event.start,
      endDate: endDate,
      allDay: event.allDay,
      backgroundColor: event.backgroundColor
    };

    PopoverService.show(target, popoverScope, {
      templateUrl: 'scripts/components/event/popover/event-edit-popover.html',
      placement: 'auto bottom'
    });
  };

  $scope.$watch('slotDuration', function (slotDuration, oldSlotDuration) {
    if (slotDuration != oldSlotDuration) {
      angular.extend($scope.calendarOptions, {
        slotDuration: FilterService.set('slotDuration', slotDuration, true),
        defaultView: FilterService.get('view'),
        defaultDate: FilterService.getDate('date')
      });
    }
  });

  $scope.setSelectConstraint = function() {
    var selectConstraint = {};
    if ($scope.calendarView == 'agendaWeek') {
      selectConstraint = {
        start: '00:00',
        end: '24:00',
        dow: [0, 1, 2, 3, 4, 5, 6, 7]
      };
    }

    angular.extend($scope.calendarOptions, {
      selectConstraint: selectConstraint,
      defaultView: FilterService.get('view'),
      defaultDate: FilterService.getDate('date')
    });
  };

  $scope.calendarOptions = {
    lang: calendarConfig.language,
    firstDay: calendarConfig.firstDayOfWeek,
    timezone: calendarConfig.timezone,
    defaultDate: FilterService.getDate('date'),
    defaultView: $scope.calendarView,
    editable: true,
    snapDuration: '00:' + calendarConfig.stepInterval / 60,
    slotDuration: $scope.slotDuration,
    timeFormat: 'H:mm',
    slotEventOverlap: false,
    allDayHtml: 'GMT ' + moment().format('Z'),
    selectable: true,
    eventDurationEditable: false,
    eventStartEditable: false,
    disableResizing: true,
    disableDragging: true,
    aspectRatio: 1.35,
    header: {
      left: 'today prev,next',
      center: 'title',
      right: 'month agendaWeek agendaDay'
    },
    viewRender: function (view) {
      $scope.calendarView = view.name;
      $state.transitionTo('calendar.' + view.name, {}, {notify: false});
      FilterService.setCalendarView(view);

      $scope.setSelectConstraint();

      CalendarService.refreshTimeline($scope, $scope.getCalendar());
      CalendarService.addSlotDurationSelect($scope);
    },
    select: function (start, end, event) {
      $scope.createEventOnRange(start, end, event.target);
    },
    eventResize: function (event) {
      $scope.moveEvent(event);
    },
    eventClick: function (event, element) {
      if ($('.popover').length == 0) {
        $scope.showEventInformation(event, element.target);
      }
    },
    eventDrop: function (event) {
      $scope.moveEvent(event);
    }
  };

  $scope.$on('$stateChangeSuccess', function () {
    if ($state.current.data) {
      var calendar = $scope.getCalendar();

      if (calendar) {
        calendar.fullCalendar('changeView', $state.current.data.show);
      }
    }
  });
});



