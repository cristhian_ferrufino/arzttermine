calendarApp.factory('TreatmentService', function ($rootScope, $resource, $cacheFactory, $q, apiEndpoint, ApiTransform) {
  var cache = $cacheFactory('treatment-types');

  var transformResources = function (data) {
    return angular.fromJson(ApiTransform.response(data)).data;
  };

  var apiResource = $resource(apiEndpoint + 'treatment-types/:id', {id: '@id'}, {
    'query': {method: 'GET', isArray: true, cache: cache, transformResponse: transformResources}
  });

  return {
    query: function (params) {
      return apiResource.query(angular.fromJson(ApiTransform.request(params))).$promise;
    },
    get: function (ids) {
      return this.query().then(function (treatments) {
        return _.filter(treatments, function(resource) {
          return _.includes(ids, resource.id);
        });
      });
    }
  };
});
