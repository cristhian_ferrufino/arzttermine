calendarApp.factory('FilterService', function ($cookies, $rootScope, dateFormat) {

  var cookieKey = 'calendarFilter';
  var defaults = {resources: [], defaultsAdded: false, slotDuration: '00:15'};

  // these filters will be overwritten on page reload
  var overwrites = {intervalStart: null, intervalEnd: null, date: null, view: null};

  var filter = angular.extend({}, defaults, $cookies.getObject(cookieKey), overwrites);

  var saveFilter = function (silent) {
    $cookies.putObject(cookieKey, filter);
    if (!silent) {
      $rootScope.$broadcast('filterUpdated');
    }
  };

  return {
    toggleResource: function (resource, silent) {
      var id = resource.id;
      var response = false;

      if (!angular.isDefined(filter.resources)) {
        filter.resources = [];
      }

      if (_.includes(filter.resources, id)) {
        var index = filter.resources.indexOf(id);
        filter.resources.splice(index, 1);
      } else {
        filter.resources.push(id);
        response = true;
      }

      saveFilter(silent);
      return response;
    },
    addDefaultResources: function (resourceGroups) {
      if (filter.defaultsAdded === false) {
        var resourceIds = [];

        angular.forEach(resourceGroups, function (resourceGroup) {
          resourceIds = _.concat(_.map(resourceGroup.resources, 'id'), resourceIds);
        });

        this.addResources(resourceIds, true);
        this.set('defaultsAdded', true, true);
      }
    },
    addResources: function (resources, silent) {
      angular.forEach(resources, function (id) {
        if (!_.includes(filter.resources, id)) {
          filter.resources.push(id);
        }
      });

      saveFilter(silent);
      return true;
    },
    get: function (key) {
      return (key) ? filter[key] : filter;
    },
    setCalendarView: function (view, silent) {
      filter.intervalStart = view.intervalStart;
      filter.intervalEnd = view.intervalEnd;
      filter.date = view.calendar.getDate();
      filter.view = view.name;
      saveFilter(silent);
      return filter;
    },
    set: function (key, value, silent) {
      filter[key] = value;
      saveFilter(silent);
      return filter[key];
    },
    setDate: function (key, value, silent) {
      if (moment.isMoment(value)) {
        value = value.format(dateFormat);
      }

      return this.set(key, value, silent);
    },
    getDate: function (key) {
      return angular.isString(filter[key]) ? moment(filter[key], dateFormat) : filter[key];
    }
  };

});
