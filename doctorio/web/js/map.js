// Arzttermine Maps
var AT = AT || {};

AT._defer(function(){
    if(typeof google === 'object' && typeof google.maps === 'object') {
        (function (_, GMaps, $, doc, win, und) {
            'use strict';

            // Icon list. All high-res, to be scaled automatically by GMaps
            var assets = {
                marker: '/images/map/dot-blue.png',
                marker_active: '/images/map/map-center.png'
            };

            var icons = {};

            // Default options
            var opts = {
                mapContainer: 'search-map', // Must be an ID
                resultItem: '.result',
                zoom: 6,
                initialZoom: true,
                mapTypeControl: true,
                mapTypeControlOptions: {style: GMaps.MapTypeControlStyle.DROPDOWN_MENU},
                navigationControl: true,
                mapTypeId: GMaps.MapTypeId.ROADMAP,
                streetViewControl: false,
                styles: [{
                    featureType: "poi.business",
                    elementType: "labels",
                    stylers: [{visibility: "off"}]
                }],
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE
                }
            };

            // The main map object
            var map;

            // Raw data
            var markersData = {};

            // Generic markers
            var markers = [];

            // Results markers
            var pins = [];

            // Map centering deferred objects
            var pinsCreated = new $.Deferred();
            var originCreated = new $.Deferred();

            // The bounds of the search results (+ the search location point) are used to determine the center of the map.
            var mapBoundaries;

            // The shared popup
            var popup;

            // The timeout used to hide the result tooltip when the mouse exists the marker or the tooltip itself.
            var popupTimer;

            _.Maps = {
                getMap: function () {
                    return map;
                },

                // Set an option if you need to reload a map
                setOption: function (option, value) {
                    opts[option] = value;
                },

                // Keep the popup open by invalidating the timer
                persistPopup: function () {
                    clearTimeout(popupTimer);
                    popupTimer = null;
                },

                // Show a popup inside the map
                showPopup: function (marker) {
                    var slug = marker.slug || '';

                    // Don't try to build a popup if the slug is bad
                    if (slug === '') {
                        return;
                    }

                    self.persistPopup();

                    // If we don't already have a popup, build one
                    if (!popup) {
                        popup = new InfoBox({
                            infoBoxClearance: new GMaps.Size(10, 10), // 10px in every direction
                            pixelOffset: new GMaps.Size(-90, -40),
                            closeBoxURL: '',
                            boxClass: 'popup-window',
                            disableAutoPan: false,
                            alignBottom: true,
                            enableEventPropagation: true,
                            zIndex: 10
                        });
                    }

                    // Grab the placeholder popup
                    var $popupPlaceholder = $('#map-popup');

                    // Only make changes if we're activating a different/new marker
                    if (popup.owner === undefined || popup.owner != marker) {
                        // If there's an owner, deactivate it
                        if (popup.owner && typeof popup.owner.removeClass == typeof Function) {
                            popup.owner.removeClass('active');
                        }

                        // Build the placeholder popup based on our marker data
                        $popupPlaceholder.find('.photo img').attr({
                            src: markersData[slug].imgsrc,
                            alt: markersData[slug].name
                        });
                        $popupPlaceholder.find('.name a').attr({href: markersData[slug].url}).html(markersData[slug].name);
                        $popupPlaceholder.find('.street').html(markersData[slug].street);
                        $popupPlaceholder.find('.cityzip').html(markersData[slug].cityzip);
                        $popupPlaceholder.find('a[role=button]').attr({href: markersData[slug].url});

                        // Set the new popup content and location, and tag it with the owning marker
                        popup.setContent($popupPlaceholder.html());
                        popup.setPosition(new GMaps.LatLng(location.lat, location.lng));
                        popup.owner = marker;

                        // Show the popup. We assume here the marker has been assigned to the right map
                        popup.open(marker.get('map'), marker);

                        // Promote the active marker
                        marker.setZIndex(marker.getZIndex() + 5);

                        // Change the marker icon (if it's a dot - skip)
                        if (marker instanceof RichMarker) {
                            marker.addClass('active');
                            $(marker.getContent()).addClass('active');
                        }
                    }

                    $('#' + opts.mapContainer)
                        .off('mouseenter mouseleave', '.popup-window')
                        .on('mouseenter', '.popup-window', self.persistPopup)
                        .on('mouseleave', '.popup-window', self.cancelPopup);
                },

                // Ask the map to start closing the popup
                cancelPopup: function () {
                    // Reset the timer
                    clearTimeout(popupTimer);
                    popupTimer = setTimeout(self.hidePopup, 500);
                },

                // We don't need the popup, so hide it
                hidePopup: function () {
                    // We can occasionally fire a timed event while cancelling the timer. This checks that that isn't the case
                    if (!popupTimer) {
                        return;
                    }

                    if (popup) {
                        // Demote the active marker
                        if (typeof popup.owner.removeClass == typeof Function) {
                            popup.owner.removeClass('active');
                        }
                        popup.owner.setZIndex(popup.owner.getZIndex() - 5);

                        popup.close();
                        popup = null;
                    }

                    $('#map-popup').hide();

                    // Clear the timer and destroy the popup
                    clearTimeout(popupTimer);
                    popupTimer = null;
                },

                // Center the map based on the boundaries provided after loading all markers
                centerMap: function () {
                    GMaps.event.addListenerOnce(map, 'bounds_changed', function () {
                        if (map.initialZoom) {
                            // Don't zoom too far out...
                            if (map.getZoom() < 12) {
                                map.setZoom(12);
                            }
                            // ... or too far in
                            if (map.getZoom() > 15) {
                                map.setZoom(15);
                            }
                            map.initialZoom = false;
                        }
                    });

                    map.setCenter(mapBoundaries.getCenter());
                    map.fitBounds(mapBoundaries);
                },

                placeSearchLocation: function () {
                    // Special case for the searchLocation data
                    var $source = $('#search-location');

                    if (!$source.length) {
                        return;
                    }

                    var searchLocation = new GMaps.LatLng(parseFloat($source.data('lat')), parseFloat($source.data('lng')));

                    if (isNaN(searchLocation.lat()) || isNaN(searchLocation.lng())) {
                        return;
                    }

                    mapBoundaries.extend(searchLocation);

                    var searchLocationMarker = new GMaps.Marker({
                        title: $source.data('name'),
                        map: map,
                        position: searchLocation,
                        icon: icons.marker_active,
                        zIndex: 1
                    });

                    markers.push(searchLocationMarker);
                },

                // Create the primary markers. These change.
                placePins: function () {
                    $(pins).each(function () {
                        this.onRemove();
                    });

                    // Iterate through the search results, parse their geo-coordinates, and create a marker
                    $(opts.resultItem).each(function () {
                        var $result = $(this),
                            coords = new GMaps.LatLng(
                                parseFloat($result.find('[property=latitude]').attr('content')),
                                parseFloat($result.find('[property=longitude]').attr('content'))
                            );

                        if (!isNaN(coords.lat()) && !isNaN(coords.lng())) {
                            mapBoundaries.extend(coords);

                            var searchIndex = ($result.data('index') && $('.practice-profile').length == 0) ? parseInt($result.data('index'), 10) : '';

                            var pin = new RichMarker({
                                map: map,
                                position: coords,
                                flat: true,
                                zIndex: 3,
                                content: '<div class="marker">' +
                                '<div class="active"></div>' +
                                '<div class="inactive"></div>' +
                                '<span class="search-index">' + searchIndex + '</span>' +
                                '</div>'
                            });

                            pin.slug = $result.data('slug');

                            GMaps.event.addListener(pin, 'mouseover', function () {
                                self.showPopup(pin);
                            });
                            GMaps.event.addListener(pin, 'mouseout', self.cancelPopup);

                            // Hovering over a corresponding search result will also show the popup
                            if ($('.practice-profile').length == 0 && $('.doctor-profile').length == 0) {
                                $result
                                    .mouseenter(function () {
                                        GMaps.event.trigger(pin, 'mouseover');
                                    })
                                    .mouseleave(function () {
                                        GMaps.event.trigger(pin, 'mouseout');
                                    });
                            }
                            pins.push(pin);
                        }
                    });

                    $(doc).triggerHandler('AT:Maps:pinsCreated');
                },

                // Load the (generic) markers from the server
                loadMarkersData: function () {
                    if (win.searchData) {
                        markersData = searchData.markers;


                        // // use searchData.data == limited search results
                        // markersData = [];
                        // $.each(searchData.data, function (index, data) {
                        //     markersData[data.doctor.slug] = {
                        //         lat: data.location.lat,
                        //         lng: data.location.lng,
                        //         imgsrc: data.doctor.img,
                        //         name: data.doctor.name,
                        //         street: data.location.street,
                        //         cityzip: data.location.zip + ' ' + data.location.city,
                        //         available: data.doctor.available,
                        //         url: '/medico/' + data.doctor.slug
                        //     };
                        // });
                    }

                    if (win.mapMarkerApiUrl) {
                        $.get(win.mapMarkerApiUrl, function (response) {
                            markersData = response.markers || {};
                            $(doc).triggerHandler('AT:Maps:dataLoaded');
                        });
                    } else {
                        $(doc).triggerHandler('AT:Maps:dataLoaded');
                    }
                },

                // Create the generic (dot) markers
                placeMarkers: function () {
                    for (var location in markersData) {
                        if (markersData.hasOwnProperty(location)) {
                            if (!!markersData[location].available) {
                                // Wrap the marker creation in a closure to ensure the correct scope
                                (function () {
                                    var marker = new GMaps.Marker({
                                        map: map,
                                        position: new GMaps.LatLng(markersData[location].lat, markersData[location].lng),
                                        icon: icons.marker,
                                        zIndex: 2
                                    });

                                    marker.slug = location;

                                    GMaps.event.addListener(marker, 'click', (function () {
                                        self.showPopup(marker);
                                    }));

                                    markers.push(marker);
                                })();
                            }
                        }
                    }

                    $(doc).triggerHandler('AT:Maps:markersCreated');
                },

                // Load assets that will be used. Usually icons
                loadAssets: function () {
                    icons = {
                        marker: {
                            url: assets.marker,
                            size: new google.maps.Size(8, 8),
                            scaledSize: new google.maps.Size(8, 8)
                        },
                        marker_active: {
                            url: assets.marker_active,
                            size: new google.maps.Size(24, 24),
                            scaledSize: new google.maps.Size(24, 24)
                        }
                    };

                    // Add some functions to make GMaps markers compatible with ours
                    RichMarker.prototype.addClass = function (className) {
                        if (className) {
                            this.markerWrapper_.className += ' ' + className;
                        }
                    };
                    RichMarker.prototype.removeClass = function (className) {
                        if (className && this.markerWrapper_.className) {
                            this.markerWrapper_.className = this.markerWrapper_.className.replace(new RegExp('(?:\s+)?' + className + '(?:\s+)?', 'g'), '');
                        }
                    };
                },

                // Bind the event listeners for the maps
                setListeners: function () {
                    // Initial marker creation
                    $(doc)

                    // Init events
                        .on('AT:Maps:initialized', self.loadAssets)
                        .on('AT:Maps:initialized', self.loadMarkersData)
                        .on('AT:Maps:initialized', self.placeSearchLocation)
                        .on('AT:Maps:initialized', self.placePins)

                        // Data events
                        .on('AT:Maps:dataLoaded', self.placeMarkers)

                        // Marker events
                        .on('AT:Maps:pinsCreated', function () {
                            pinsCreated.resolve();
                        })
                        .on('AT:Maps:markersCreated', function () {
                            originCreated.resolve();
                        })

                        .on('AT:Maps:update', function () {
                            GMaps.event.trigger(self.getMap(), 'resize');
                            self.centerMap();
                        });

                    $.when(originCreated, pinsCreated).done(self.centerMap);
                },

                // Initializer
                initialize: function () {
                    // Don't initialize if we don't have a container
                    if (!doc.getElementById(opts.mapContainer)) {
                        return;
                    }

                    // Obviously, we need this
                    map = new GMaps.Map(doc.getElementById(opts.mapContainer), opts);

                    // The outer boundaries of the map
                    mapBoundaries = new GMaps.LatLngBounds();

                    // Bind everything together
                    self.setListeners();

                    // Let everything know we're done
                    $(doc).triggerHandler('AT:Maps:initialized');
                }

            };

            // So we always have "this" available in the correct context. Also aids in readability
            var self = _.Maps;

        })(AT, google.maps, jQuery, document, window);

        // Kludgey way to trigger an initialize because Google is balls
        google.maps.event.addDomListener(window, 'load', function () {
            AT.Maps.initialize();
        });
    }
});