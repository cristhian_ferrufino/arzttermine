$(document).ready(function() {

  $("[data-behavior~=swiper]").each(function() {
    var prev_button = $(".swiper-button-prev");
    var next_button = $(".swiper-button-next");

    var swiper = new Swiper ($(this).find(".swiper-container"), {
      prevButton: prev_button,
      nextButton: next_button
    });
  });

});
