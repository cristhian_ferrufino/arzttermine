// Arzttermine Generic
var AT = AT || {};

AT._defer(
    (function(_, $, doc, win, und) {

        // Calendar transformations for resize event. Makes them easier to see
        // ========================================================================================
        var $calendars = $('.calendar'),
            $calendar = $calendars.eq(0);

        // Checks the size of the calendars. Assumption: all calendars on a page are of the same width.
        // At certain breakpoints, add/remove classes. 
        // Used for different layout of messages with buttons (integration 8 or next available appointment).
        var setCalendarsSize = function() {
            if ($calendar.length) {
                var width = $calendar.width();

                if (width > 670) {
                    $calendars.removeClass('medium small').addClass('large');
                } else if (width < 670 && width > 500) {
                    $calendars.removeClass('small large').addClass('medium');
                } else if (width < 500) {
                    $calendars.removeClass('medium large').addClass('small');
                }
            }
        };

        var resizeTimer;
        $(win).resize(function() {
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(setCalendarsSize, 25);
        });
        
        // Prefill empty form elements
        // @todo: Currently hardcoded. Make dynamic or fix :/
        // ========================================================================================
        var $form      = $('form'),
            $specialty = $form.find('[name="search[medical_specialty_id]"]'),
            $location  = $form.find('[name="search[location]"]');
        
        $form.submit(function() {
            if (!$specialty.val()) {
                $specialty.val(14); // dentista
            }

            if (!$location.val()) {
                $location.val('Berlin');
            }
        });

        // Placeholder styling cases
        // ========================================================================================
        var selectPlaceholder = function() {
            var $this = $(this);

            if ($this.val() == '') {
                $this.addClass('placeholder');
            } else {
                $this.removeClass('placeholder');
                $this.find('.select-placeholder').remove();
            }
        };
        
        // Doctor login page - toggling different forms
        // ========================================================================================
        $('.doctor-login input[name=email] + a').click(function(e){
            e.preventDefault();
            $('form[name=login]').hide();
            $('#forgot-email').show();
        });

        $('#login_password + a').click(function(e){
            e.preventDefault();
            $('form[name=login]').hide();
            $("form[name=forgot_password]").show();
        });

        // Help tooltips - toggling tooltips
        // ========================================================================================
        $('a.help').click(function(e){
            e.preventDefault();
            $(this).parent().next('.help-tooltip').toggle();
        });

        // onLoad
        // ========================================================================================
        $(function() {
            $('select').change(selectPlaceholder).change();

            if ($.isFunction($('form').formValidation)) {
                $('form').not('.fv-validation-disabled')
                    .formValidation({ locale: 'de_DE', excluded: ':disabled' })
                    .on('err.form.fv', function(e, data) {
                        // data.element --> The field element

                        if (!data) {
                            return;
                        }

                        var $carouselTab = $(data.element).parents('.carousel-inner');

                        if (data.fv.isValidContainer($('.carousel-item',$carouselTab).first())) {
                            $carouselTab.carousel(1).carousel('pause');
                        } else {
                            $carouselTab.carousel(0).carousel('pause');
                        }
                    });
            }

            // Initially set the classes for the calendar width.
            setCalendarsSize();

        });

    }(AT, jQuery, document, window))
);
