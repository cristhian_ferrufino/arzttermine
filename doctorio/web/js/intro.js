$(document).ready(function(){
    if(typeof window.steps == "undefined") {
        $('#help').hide();
    } else {
        $('#help').show();
        $('#help').on('click', function (e) {
            e.preventDefault();
            startIntro();
        });

        match = document.cookie.match(new RegExp('calendar_show=.[^;]*'));
        if(match === null) {
            document.cookie = "calendar_show=1;path=/";
            startIntro();
        }
    }

    $('.dropdown.keep-open').on({
        "shown.bs.dropdown": function() { this.closable = false; },
        "click":             function() { this.closable = true; },
        "hide.bs.dropdown":  function() { return this.closable; }
    });
});

function startIntro(){
    var intro = introJs();
    var steps = window.steps;

    intro.setOptions({
        'tooltipPosition': 'auto',
        'positionPrecedence': ['left', 'right', 'bottom', 'top'],
        'disableinteraction': false,
        'exitOnOverlayClick': false,
        'steps': steps
    }).onafterchange(function (targetElement) {
        if ($(targetElement).hasClass('settings')) {
            setTimeout(function () {
                if ($('.navigation .text-navigation li .dropdown').is('.open') === false) {
                    $('.navigation .text-navigation li .dropdown-toggle').click();
                }
            });
        } else if ($('.navigation .text-navigation li .dropdown').is('.open') === true) {
            $('.navigation .text-navigation li .dropdown-toggle').click();
        }
    });

    intro.start();
}