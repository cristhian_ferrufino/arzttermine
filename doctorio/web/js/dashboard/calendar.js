/**
 * Hacky calendar file to glue all components together
 */
;(function ($, doc, und) {

    // Simple JavaScript Templating
    // John Resig - http://ejohn.org/ - MIT Licensed
    (function () {
        var cache = {};

        this.tmpl = function tmpl(str, data) {
            // Figure out if we're getting a template, or if we need to
            // load the template - and be sure to cache the result.
            var fn = !/\W/.test(str) ?
                cache[str] = cache[str] ||
                    tmpl(document.getElementById(str).innerHTML) :

                // Generate a reusable function that will serve as a template
                // generator (and which will be cached).
                new Function("obj",
                    "var p=[],print=function(){p.push.apply(p,arguments);};" +

                        // Introduce the data as local variables using with(){}
                    "with(obj){p.push('" +

                        // Convert the template into pure JavaScript
                    str
                        .replace(/[\r\t\n]/g, " ")
                        .split("<%").join("\t")
                        .replace(/((^|%>)[^\t]*)'/g, "$1\r")
                        .replace(/\t=(.*?)%>/g, "',$1,'")
                        .split("\t").join("');")
                        .split("%>").join("p.push('")
                        .split("\r").join("\\'")
                    + "');}return p.join('');");

            // Provide some basic currying to the user
            return data ? fn(data) : fn;
        };
    })();

    var STATUS = {
        NONE: 1, // Nothing to update
        OK: 2, // Save ok
        ERROR: 4 // Save not ok
    };

    var restClient;
    var authToken; // The API key/auth token
    var endpoint; // This module's endpoints, as { method: route, ... }
    var module; // Name of the page's module
    var rowTemplate; // If applicable, will contain the template string for new items
    var rowContainer; // As above, but for receiving new items
    var formContainer; // Empty form to act as a validation container

    // Promises to track data loading
    var hasBooted = new $.Deferred();

    $(function () {
        bindListeners();

        $.when(hasBooted).done(preloadData);

        boot();
    });

    var requestInterceptor = function(config) {
        var query = $('#query');
        var params = '';
        if (query && query.length) {
            params = query.attr('data-params');
        }

        params += '&envelope=0';
        var hasParams = config.url.indexOf('?') > -1;
        config.url += (((hasParams) ? '' : '?') + params).replace('?&', '?');
        return config;
    };

    // Pull initial app data from the dom
    var boot = function () {
        var $token = $('#token');
        if ($token.length) {
            authToken = $token.data('token');
        }

        var $endpoint = $('#endpoint');
        var $practice = $('#practice');
        if ($endpoint.length) {
            endpoint = $endpoint.data('endpoint');
            restClient = restful(endpoint);
            restClient.header('Authorization', 'Bearer ' + authToken);
            if ($practice.length) {
                restClient.header('X-Practice-Id', $practice.data('id'));
            }
            restClient.addRequestInterceptor(requestInterceptor);
        }
        var $module = $('#module');
        if ($module.length) {
            module = $module.data('module');
        }

        // These are optional
        rowTemplate = $('#data-row-template');
        rowContainer = $('#data-row-container');

        formContainer = $('#validation-container');
        if (formContainer.length) {
            formContainer.formValidation();
        }

        // Only resolve if we have the two important pieces of info
        if (authToken && endpoint && module) {
            hasBooted.resolve();
        } else if (!authToken || !endpoint) { // no error if module is not supplied
            // @todo Language
            toastr.warning(Translator.trans('notice.error.ups'));
        }
    };

    // Load current data from the server
    var preloadData = function() {
        if (!endpoint) {
            return;
        }

        $(doc).trigger('dashboard:calendar:loading', true);

        restClient.all(module).getAll().then(function(response) {
            $(doc).trigger('dashboard:calendar:data-preload', [response]);
            $(doc).trigger('dashboard:calendar:loading', false);
        })
        .then(function() {
            // Bind drag and drop to newly created table elements
            rowContainer.sortable({
                items: 'tr',
                handle: '.handle'
            });
        });
    };

    // Event listeners. Only apply if we have an API key
    var bindListeners = function () {
        var $doc = $(doc);

        // The default row add just adds a blank template. Otherwise, use provided data
        $doc.on('dashboard:calendar:add-row', function (e, templateData) {
            templateData = templateData || {};
            var dataRow = insertRow(templateData);

            $doc.trigger('dashboard:calendar:row-added', [dataRow, templateData]);
        });

        $(doc).on('dashboard:calendar:loading', function(e, on) {
            var callable = (on !== false) ? 'fadeIn' : 'fadeOut';
            $('.loading')[callable](100);
        });

        $doc.on('dashboard:calendar:row-added', function(e, dataRow) {
            addValidation($(dataRow));
        });

        $doc.on('dashboard:calendar:reload', function(e, dataRow) {
            preloadData();
        });

        $doc.on('dashboard:calendar:remove-row', function (e, $row) {
            // Remove it from the validator list
            removeValidation($row);

            // If we haven't persisted this row, no need to send a delete, so just remove
            if (!$row.data('id')) {
                $row.remove();
            } else {
                removeEntity($row);
            }
        });

        $doc.on('dashboard:calendar:pre-save', function() {
            $('.loading').show();
        });

        // Free up the save button after we've saved, for better or worse
        $doc.on('dashboard:calendar:post-save', function() {
            $('.save')
                .data('busy', false)
                .removeClass('disabled');
        });

        // Bind popups
        $doc.on('dashboard:calendar:post-save', function(e, data) {
            if (!Translator) {
                return; // hack to avoid js error and infinite loading
            }
            var status = data.status || STATUS.NONE;

            // @todo Language
            if (status == STATUS.OK) {
                toastr.success(Translator.trans('notice.info.action_was_successfully'));
            }

            if (status == STATUS.ERROR) {
                toastr.error(Translator.trans('notice.error.general_error', { message: (data.error || Translator.trans('notice.error.unknown')) }));
            }
        });

        $doc.on('dashboard:calendar:post-save', function() {
            $('.loading').hide();
        });

        // Lock the save button to prevent spamming requests
        $('.save').click(function (e) {
            e.preventDefault();

            var $this = $(this);

            if ($this.data('busy')) {
                return;
            }

            $this
                .data('busy', true)
                .addClass('disabled');

            save();
        });

        $doc.on('dashboard:calendar:save', function() {
            var data = $doc.triggerHandler('dashboard:calendar:payload');

            // Don't bother with doing anything if we have no data
            if (!data || !data.length) {
                $doc.trigger('dashboard:calendar:post-save', [{ status: STATUS.NONE }]);
                return;
            }

            Promise.all(data.map(saveEntity)).then(
                function() {
                    $doc.trigger('dashboard:calendar:post-save', [{ status: STATUS.OK }]);
                },
                function(err) {
                    $doc.trigger('dashboard:calendar:post-save', [{ status: STATUS.ERROR, error: err }]);
                }
            );
        });

        $doc.on('dashboard:calendar:validate', function() {
            return !!formContainer.data('formValidation').validate().isValid();
        });

        // Add a blank data row if available
        $('#data-row-add').click(function(e) {
            e.preventDefault();
            $doc.trigger('dashboard:calendar:add-row');
            rowContainer.find('.data-row').last().find(':input').first().focus(); // Focus new input fields
        });

        // Dump rows if available
        $doc.on('click', '.remove-row', function(e) {
            e.preventDefault();
            $doc.trigger('dashboard:calendar:remove-row', [$(this).closest('.data-row')]);
        });
    };

    var save = function() {
        if (!validate()) {
            $(doc).trigger('dashboard:calendar:post-save', [{ status: STATUS.NONE }]);
            toastr.warning(Translator.trans('notice.error.check_your_form_data'));
            return;
        }

        $(doc).trigger('dashboard:calendar:pre-save');
        $(doc).trigger('dashboard:calendar:save');
    };

    var addValidation = function($dataRow) {
        $dataRow.find(':input').each(function() {
            formContainer.formValidation('addField', $(this));
        });
    };

    var removeValidation = function($dataRow) {
        $dataRow.find(':input').each(function () {
            formContainer.formValidation('removeField', $(this));
        });
    };

    var validate = function () {
        if (!formContainer.length) {
            return true;
        }

        var isValid = ($(doc).triggerHandler('dashboard:calendar:validate'));

        // If it's empty, we assume it passes the test
        return isValid === und || !!isValid;
    };

    // Insert new row
    var insertRow = function (templateData) {
        templateData = templateData || {};

        if (rowContainer.length && rowTemplate.length) {
            var $dataRow = $(tmpl(rowTemplate.html(), templateData));

            if (templateData.id || $dataRow.attr('data-allow-copy') !== 'false') {
                $dataRow.appendTo(rowContainer);
            }

            return $dataRow;
        }

        return null;
    };

    // Delete a record if there's a bin on the page
    var removeEntity = function ($item) {
        restClient.one(module, $item.data('id')).delete().then(function() {
            $item.remove();
        });
    };

    // Send the data
    var saveEntity = function (data) {
        var dataRow = data.dataRow;
        delete data.dataRow;

        if (data.id) {
            var id = data.id;
            delete data.id; // Detach the ID so we don't get form errors on the server side
            return restClient.one(module, id).put(data);
        } else {
            return restClient.all(module).post(data).then(function(response) {
                $(doc).trigger('dashboard:calendar:new-entity', [dataRow, response.body(false)]);
            });
        }
    };

}(jQuery, document));
