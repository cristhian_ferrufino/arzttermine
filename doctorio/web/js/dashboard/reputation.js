// Source for the Reputation management tool (dashboard)
;
(function ($, win) {

    var $reputation = $('#reputation');
    var comment_box = 'textarea.comment';
    var is_saving = false;

    // Commit the review comment
    var save = function (review_id) {
        // Sanitise review_id
        review_id = parseInt(review_id, 10);

        // Try and find the related table row
        var $review = $('tr[data-review-id=' + review_id + ']');

        if (is_saving || !$review.length) {
            return;
        }

        is_saving = true;

        // Grab the comment from the active row
        var comment = $review.find(comment_box).val();

        $.ajax({
            url: win.location,
            type: 'post',
            data: {
                review_id: review_id,
                comment: comment
            }
        })
        .done(function() {
            is_saving = false;
            $review.find('.saved').fadeIn().delay(1500).fadeOut();
        })
    };

    // Toolbox actions
    $reputation.find('tr').on('click', '.tool-save', function() { save($(this).closest('.review').data('review-id')); });

    $(function() {
        $reputation.stupidtable();
        $reputation.find('.saved').hide();
    });

}(jQuery, window));