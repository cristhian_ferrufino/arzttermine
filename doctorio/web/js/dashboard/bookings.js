/**
 * NOTE: This thing is a dog. It is really badly hacked together as NOTHING on the dashboard
 * is using anything CLOSE to a modern framework/system. Some crappy methods here are basically
 * a poor-man's version of ReactJS or some other Model-View library.
 *
 * @todo Refactor the entire Dashboard
 */
;(function($, doc, win) {

    // Polyfills
    if (!Array.prototype.indexOf) {
        Array.prototype.indexOf = function (obj, start) {
            for (var i = (start || 0), j = this.length; i < j; i++) {
                if (this[i] === obj) {
                    return i;
                }
            }
            return -1;
        }
    }
    
    // Booking statuses
    var STATUS = {
        NEW:       1,
        PENDING:   2,
        CONFIRMED: 4,
        CANCELLED: 8
    };
    
    // Labels
    // @todo External label loading
    var LABELS = {
        STATUS: {  },
        REPORTING_STATUS: { }
    };
    
    // For moment formatting
    var DATE_FORMAT = 'L LTS';

    // Locale used by plugins
    // @todo Dynamic
    var LOCALE = 'es';

    // All available modals on the page
    var popup = {
        confirm: $('#modal-confirm'),
        cancel:  $('#modal-cancel'),
        change:  $('#modal-change-appointment')
    };
    
    // Save the booking data from the markup here to prevent manipulation
    var bookingData = {};
    
    // Only update the bookings in here (array of booking IDs)
    var dirtyBookings = [];
    
    // Any created booking offers (for a given ID) will be here
    var bookingOffers = {};
    
    // Medical specialty IDs/labels
    var medicalSpecialties = {};

    // The booking currently being displayed in the modal (ID)
    var activeBooking = 0;

    // Send the data
    var save = function() {
        // Gather up ONLY changed bookings
        var bookings = {};

        for (var i = 0, j = dirtyBookings.length; i < j; i++) {
            // Mapped to id => {}
            bookings[dirtyBookings[i]] = bookingData[dirtyBookings[i]];
        }
        
        // Get the payload
        var payload = {
            bookings: bookings,
            offers: bookingOffers
        };
        
        $.ajax({
            url: win.location,
            type: 'post',
            data: {
                payload: JSON.stringify(payload) // We need to JSONify this to ensure we don't get param count problems
            }
        })
        .done(function () {
            win.location.href = win.location.href;
        });
    };

    // Update a booking (internally) and the UI that corresponds to it
    var updateInterface = function(bookingId, data) {
        var $dataRow = $('tr[data-booking-id=' + bookingId + ']');

        if (!$dataRow.length || !bookingData.hasOwnProperty(bookingId)) {
            return;
        }

        var booking = bookingData[bookingId];
        var appointment_start_at = moment(booking.appointment_start_at);

        $dataRow.find('.booking-status')
            .text(LABELS.STATUS[booking.status])
            .updateSortVal(booking.status);
        
        
        $dataRow.find('.datetime')
            .updateSortVal(appointment_start_at.format('x')) // Unix timestamp
            .find('span.time').text(appointment_start_at.format(DATE_FORMAT));
    };

    // Update the raw data for a booking
    var updateBooking = function(bookingId, data) {
        if (!bookingData.hasOwnProperty(bookingId)) {
            return;
        }

        bookingData[bookingId] = $.extend(bookingData[bookingId], data);
    };

    // Get booking data from the DOM and populate the locally scoped version here
    var preloadData = function() {
        var $bookings = $('#bookings');
        $bookings.find('.booking').each(function() {
            var booking = $(this).data('booking-data');
            bookingData[booking.id] = booking;

            // Add appointment enquiry settings
            // @todo Less hacky
            bookingData[booking.id].supports_enquiry = $(this).data('supports-enquiry');
        });

        medicalSpecialties = $bookings.data('medical-specialties');
    };

    // Fill booking data from the active booking onto the modal
    var getDetails = function() {
        // If there's no active booking or the ID doesn't exist, just fail
        if (!activeBooking || !bookingData.hasOwnProperty(activeBooking)) {
            return;
        }

        var booking = bookingData[activeBooking];
        var $bookingDetails = $(this).find('.booking-details');
        var $patientDetails = $(this).find('.patient-details');

        // Booking details
        if (!$bookingDetails.length) {
            return;
        }

        $bookingDetails.find('.datetime').html(moment(booking.appointment_start_at).format(DATE_FORMAT));

        // Patient details
        if (!$patientDetails.length) {
            return;
        }

        $patientDetails.find('.name').text(booking.first_name + ' ' + booking.last_name);
        $patientDetails.find('.medical-specialty').text(booking.medical_specialty.name); // @todo LOL ID
        $patientDetails.find('.phone').text(booking.phone);
        $patientDetails.find('.email').text(booking.email);
    };

    var loadLabels = function() {
        // Labels
        // @todo translation
        LABELS.STATUS[STATUS.NEW]       = 'Nuevo';
        LABELS.STATUS[STATUS.PENDING]   = 'En proceso';
        LABELS.STATUS[STATUS.CONFIRMED] = 'Confirmado';
        LABELS.STATUS[STATUS.CANCELLED] = 'Cancelado';
    };

    // Event listeners
    // --------------------------------------------------------------------------------------------------------------
    var bookingChanged = function(e, bookingId, data) {
        bookingId = parseInt(bookingId, 10);
        data = data || {};

        updateBooking(bookingId, data);
        updateInterface(bookingId, data);
        
        if (dirtyBookings.indexOf(bookingId) < 0) {
            dirtyBookings.push(bookingId);
        }
        
        // Clear activeBooking as we're not messing with it anymore
        activeBooking = 0;
    };
    
    var closeConfirm = function() {
        bookingData[activeBooking].status = STATUS.CONFIRMED;

        $(doc).trigger('AT:Dashboard:bookingChanged', [activeBooking]);

        // Close the modal (manual dismissal)
        $(popup.confirm).modal('hide');
    };

    var closeCancel = function() {
        bookingData[activeBooking].status = STATUS.CANCELLED;

        $(doc).trigger('AT:Dashboard:bookingChanged', [activeBooking]);
        
        // Close the modal (manual dismissal)
        $(popup.cancel).modal('hide');
    };

    // This method is messy thanks to the Enquiry integration
    var showChange = function() {
        var $popup = $(popup.change);
        var appointment_start = moment(bookingData[activeBooking].appointment_start_at);
        var suggestion = moment(appointment_start).add(2, 'day');
        var enquiry = bookingData[activeBooking].supports_enquiry;

        $popup.toggleClass('enquiry', enquiry);
        $popup.find('.moved-to').val(appointment_start.format(DATE_FORMAT));
        $popup.find('.offer-time').val(suggestion.format(DATE_FORMAT));

        $popup.find('.datetimepicker').change();

        // Is either "change" or "suggest"
        $popup.data('mode', 'change');

        // Hack to force proper tab
        // @todo Better detection/execution
        if (enquiry) {
            $popup.find('.nav-tabs').find('a[href="#booking-suggest"]').click();
        }
    };

    var closeChange = function() {
        var $popup = $(popup.change);
        var mode = $popup.data('mode');
        var appointment = moment($popup.find('.moved-to').val(), DATE_FORMAT);

        if (mode === 'change') {
            bookingData[activeBooking].status = STATUS.CONFIRMED;
            bookingData[activeBooking].appointment_start_at = appointment.format(DATE_FORMAT);
        } else if (mode === 'suggest') {
            bookingData[activeBooking].status = STATUS.PENDING;
            bookingOffers[activeBooking] = {
                appointment_start_at: $popup.find('.offer-time').val(),
                expiry: parseInt($popup.find('.offer-expiry').val(), 10)
            };
        }

        $(doc).trigger('AT:Dashboard:bookingChanged', [activeBooking]);
        
        // Close the modal (manual dismissal)
        $(popup.change).modal('hide');
    };

    var setBookingActive = function(booking_id) {
        activeBooking = parseInt(booking_id);
    };
    
    var tabChange = function(e) {
        var $popup = $(popup.change);
        
        $popup.find('.btn-primary').text($(e.target).data('button'));
        $popup.data('mode', $(e.target).data('mode'));
    };

    // Event listeners
    $('.dashboard-actions').on('click', '.save', function(e) {
        e.preventDefault();

        var $this = $(this);

        if ($this.data('busy')) {
            return;
        }

        $this
            .data('busy', true)
            .addClass('disabled');

        save();
    });

    // onLoad
    // --------------------------------------------------------------------------------------------------------------
    $(function() {
        // Load UI elements
        loadLabels();
        
        // Find all booking data on the page
        preloadData();

        // The datetime helper locale
        moment.locale(LOCALE);
        
        // Make the table sortable
        $('table#bookings').stupidtable();

        // Hook up DT pickers which are better than dropdowns
        // @todo: Dynamic language
        $('.datetimepicker').datetimepicker({
            locale: LOCALE,
            stepping: 15
        });

        // Load tabs
        $('a[role="tab"]').on('show.bs.tab', tabChange);

        // Bind all booking actions
        var beforeModalShow = function(e) {
            setBookingActive($(e.relatedTarget).parents('.booking').data('booking-id'));
        };

        $(popup.confirm).on('show.bs.modal', beforeModalShow);
        $(popup.cancel).on('show.bs.modal', beforeModalShow);
        $(popup.change).on('show.bs.modal', beforeModalShow);

        // Load booking/patient data when *any* modal on the page is triggered
        $('.modal')
            .on('show.bs.modal', getDetails);

        // Individual data population hooks
        $(popup.change).on('show.bs.modal', showChange);

        // Individual data transformation hooks
        $(popup.confirm).find('.btn-primary').on('click', closeConfirm);
        $(popup.cancel).find('.btn-primary').on('click', closeCancel);
        $(popup.change).find('.btn-primary').on('click', closeChange);
        
        $(doc).on('AT:Dashboard:bookingChanged', bookingChanged);
    });

}(jQuery, document, window));