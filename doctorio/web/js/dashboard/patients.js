// Source for patients bookings section
;(function ($, win) {

    var $summary = $('.booking-summary');
    var endpoint = '';

    // Send the current report to the server
    var cancelBooking = function (booking_id) {
        $.ajax({
            url: endpoint,
            type: 'post',
            data: {
                booking_id: booking_id,
                type: 'cancel_appointment'
            }
        })
        .done(function () {
            win.location.href = win.location.href;
        });
    };

    // Event listeners
    // --------------------------------------------------------------------------------------------------------------
    $summary.find('.cancel.button').on('click', function (e) {
        e.preventDefault();

        if (!confirm('Are you sure you want to cancel?')) {
            return;
        }

        var $this = $(this);

        if ($this.data('busy')) {
            return;
        }

        $this
            .data('busy', true)
            .addClass('disabled');

        cancelBooking($this.data('booking_id'));
    });

    // onLoad
    // --------------------------------------------------------------------------------------------------------------
    $(function () {
        endpoint = $summary.data('endpoint');
    });

}(jQuery, window));