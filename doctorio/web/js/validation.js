var AT = AT || {};

AT._defer(function(){
    
    AT.validation = {
        // Sets up monitoring of a form submission. 
        // Will trigger validation whenever submit action is executed, whether as explicit or via ajax.
        // If the validation fails, aborts submission.
        monitor: function($form) {
            $form.attr("data-to-validate", "true");

            // Disable default browser HTML5 validation.
            $form.attr('novalidate', 'novalidate');
            
            // On "normal" submit (like POST request), validate the form and, if needed, stop submission.
            $form.on('submit', function(e){
                var $activeForm = $(this);
                var errorCount = self.validate($activeForm);
                if (errorCount > 0) {
                    $form.triggerHandler('AT:form-validation:submit-aborted');
                    return false;
                }
                $form.triggerHandler('AT:form-validation:submitting');
            });
            
            // Check every ajax request and if its origin is a form that needs validation, 
            // then validate and, if needed, abort ajax. 
            // A bit clunky to check every ajax request, but didn't find better way to keep this module "transparent".
            // Since we don't have many ajax requests - it's good enough.
            $(document).ajaxSend(function(e, xhr, settings){
                var $activeForm = $(e.currentTarget.activeElement).parents("form");
                if ($activeForm.length > 0 && $activeForm.attr("data-to-validate") === "true") {
                    var errorCount = self.validate($activeForm);
                    if (errorCount > 0) {
                        xhr.abort();
                        $form.triggerHandler('AT:form-validation:ajax-aborted');
                    } else {
                        $form.triggerHandler('AT:form-validation:submitting');
                    }
                }
            })
        },

        validate: function($form) {
            // Reset
            $form.removeClass('error');
            $form.find('.form-element').removeClass('error');
            $form.find('.chosen-container').removeClass('error');

            // Email fields should be validated for a valid email if they contain a value.
            $emailFields = $form.find('input[type=email]:visible');
            $emailFields.each(function(ind, el) {
                $el = $(el);
                var val = $.trim($el.val());

                if (val && !self.isEmail(val)) {
                    self.markAsError($el);
                }
            });

            // The required fields should all have some value.
            $fields = $form.find('input[required]:visible');
            $fields.each(function(ind, el) {
                $el = $(el);
                switch ($el.attr('type')) {
                    case 'radio':
                        var name = $el.attr('name');
                        $radio = $form.find('input[name="' + name + '"]');
                        if (!$radio.is(':checked')) {
                            self.markAsError($radio);
                        }
                        break;

                    case 'checkbox':
                        if (!$el.is(':checked')) {
                            self.markAsError($el);
                        }
                        break;

                    case 'text':
                    case 'phone':
                    case 'email':
                    default:
                        var val = $.trim($el.val());
                        if (!val) {
                            self.markAsError($el);
                        }
                        break;
                }
            });

            // Validate the <select> boxes.
            $selects = $form.find('select[required]:visible');
            $selects.each(function(ind, el) {
                $el = $(el);
                // On many places we put a default option, but without a default "non-selected" value.
                // So, we have to test against that option :(
                if ($el.val().length == 0 || $el.find("option:selected").html() == "...bitte wählen...") {
                    self.markAsError($el);
                }
            });

            // Some select boxes are replaced with chosen.jquery.js which hides the default select box, but it will
            // still keep the actual value.
            // We validate against the original <select> and .error class is added to the automplete, if needed.
            $selectsChosen = $form.find('select[required].chosen-hid-this').filter(function(ind, el){
                return $(el).siblings('.chosen-container:visible').length != 0;
            });
            $selectsChosen.each(function(ind, el) {
                $el = $(el);
                if ($el.val().length == 0 || $el.find("option:selected").html() == "...bitte wählen...") {
                    self.markAsError($el);
                    $el.siblings('.chosen-container:first').addClass('error');
                }
            });


            var errorCount = $form.find(".form-element.error").length;

            // Fire events.
            if (errorCount > 0) {
                $form.triggerHandler('AT:form-validation:errors-found');
            } else {
                $form.triggerHandler('AT:form-validation:no-errors-found');
            }

            return errorCount;
        },

        // Mark the form and the field as erroneous.
        markAsError: function($el) {
            $el.parents("form").addClass("error");
            $el.parents(".form-element").addClass("error");
        },

        // A regex that should check whether an email is valid.
        isEmail: function(email) {
            var regex = /^[a-z0-9!#$%&'*+/=?^_{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;
            return regex.test(email);
        }
    };

    var self = AT.validation;

});
