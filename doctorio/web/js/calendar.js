var AT = AT || {};

AT._defer(function () {
    (function (_, $, doc, win, und) {

        // Full width of the calendar
        var calendarWidth = 0;

        // Column width inside the container
        var colWidth = 1;

        // The value for which the calendar is scrolled when the arrows are clicked.
        var step = 0;

        var DIRECTION = {
            RIGHT: 1,
            LEFT: -1
        };

        // Calendar scroll offset
        var currX = 0;

        _.Calendar = {
            /**
             *  Final preparations for the ajax call.
             *  In the callback, append the new HTML, update the metadata and execute the provided user callback.
             */
            fetchAppointments: function (dateStart, dateEnd, specialtyId, callback) {
                if (!dateStart || !dateEnd) {
                    return;
                }

                var providers = [],
                    providersString;

                callback = callback || $.noop;

                // Loop all scrollable calendars, take their user and location ids, generate the providers string.
                $('.appointments-block.scrollable').each(function () {
                    var $this = $(this);
                    providers.push($this.data('user-id') + '|' + $this.data('location-id'));
                });

                // Remove the trailing comma or the API returns an error.
                providersString = providers.join(',');

                // When the necessary params are ready, call the API.
                $.ajax({
                    url: apiUrl + '/available-appointments',
                    data: {
                        date_start: moment(dateStart).format('YYYY-MM-DD'),
                        date_end: moment(dateEnd).format('YYYY-MM-DD'),
                        medical_specialty_id: specialtyId,
                        providers: providersString,
                        widget: win.widget || ''
                    },
                    success: function (response) {
                        var providers = $('#providers');

                        // Update the metadata.
                        providers.data('date-start', response.date_start);
                        providers.data('date-end', response.date_end);
                        providers.data('date-days', response.days);

                        // There's an error, so just ignore for now
                        if (response.error !== und) {
                            // @todo Notify client
                            return;
                        }

                        // Loop scrollable calendars. Based on the user and location ids,
                        // take the new content from the response and append it.
                        $('.appointments-block.scrollable').each(function () {
                            var $this = $(this);
                            var userId = $this.data('user-id');
                            var locationId = $this.data('location-id');
                            var html = response.providers[userId + '|' + locationId].html;

                            $this.find('.wrapper').append(html);
                        });

                        self.setWrappersWidth();

                        if (typeof callback === 'function') {
                            callback.call();
                        }
                    }
                });
            },

            // Set the wrapper width so we can calculate scroll values
            setWrappersWidth: function () {
                // Update scrollStep and columnWidth (should be unnecessary except for edge cases)
                colWidth = $('.appointments-block').find('.col').first().outerWidth(true);
                step = 3 * colWidth;

                // Manually set each wrapper width thanks to the new
                $('.scrollable .wrapper').each(function () {
                    var $this = $(this);
                    var $columns = $this.find('.col');

                    $this.width(colWidth * $columns.length);
                });
            },

            // Scroll the calendar left or right
            scrollCalendar: function (direction, $calendar) {
                direction = direction || DIRECTION.RIGHT;

                var $wrapper = $calendar.find('.wrapper');

                // If the previous scrolling is not finished yet => abort.
                if (!$wrapper.length || $wrapper.is(':animated')) {
                    return;
                }

                if (direction === DIRECTION.RIGHT) {
                    // Moving away from the start, enable the left arrow.
                    $('.appointments-block.scrollable .arrow.left').removeClass('disabled');

                    // If the arrow of a "next-available-appointment" type of calendar is clicked,
                    // it's the same as clicking the message button itself => scroll to the next available appointment.
                    var $nextAvailableAppointmentButton = $calendar.find('a[data-next-appointment-date]');
                    if ($nextAvailableAppointmentButton.length != 0 && !$calendar.hasClass('hide-action')) {
                        $nextAvailableAppointmentButton.click();
                        return;
                    }

                    var wrapperWidth = $wrapper.width();

                    // If there is no more available days in the calendar, we need to call the API.
                    // Otherwise, if there is enough data, just scroll for the value of step.
                    if (calendarWidth - currX + step > wrapperWidth) {
                        var dateStart = moment($('#providers').data('date-end')); // The end becomes the new start
                        dateStart = dateStart.add(1, 'd');
                        var dateEnd = moment(dateStart).add(14, 'd');

                        // Gather the data needed for the API call.
                        var specialtyId = $calendar.data('specialty-id');

                        // Once we have the ajax response and the new HTML is appended, scroll the calendar.
                        self.fetchAppointments(dateStart, dateEnd, specialtyId, function () {
                            currX -= step;
                            self.translateWrappers();
                        });
                    } else {
                        currX -= step;
                        self.translateWrappers();
                    }
                } else if (direction === DIRECTION.LEFT) {
                    $('.appointments-block.scrollable .arrow.right').removeClass('disabled');

                    // Scroll by the value of step, or 0 (if step would take us before the calendar's start).
                    currX += step;
                    if (currX > 0) {
                        currX = 0;
                    }

                    self.translateWrappers();
                }
            },

            // Translates the calendar wrappers.
            // If the browser supports 3d, then transform3d is used. Otherwise, just plain animate.
            // Note: jquery.transit does not use 3d, just 2d translations - so no GPU acceleration, which sucks on iPads.
            translateWrappers: function () {
                var $wrappers = $('.appointments-block.scrollable > .wrapper');

                if (self.has3d) {
                    $wrappers.css({
                        '-webkit-transform': 'translate3d(' + currX + 'px,0,0)',
                        '-moz-transform': 'translate3d(' + currX + 'px,0,0)',
                        '-ms-transform': 'translate3d(' + currX + 'px,0,0)',
                        '-o-transform': 'translate3d(' + currX + 'px,0,0)',
                        'transform': 'translate3d(' + currX + 'px,0,0)'
                    });
                    $wrappers.on('transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd', function () {
                        $(doc).trigger('AT:Calendar:scrollend');
                    });
                } else {
                    $wrappers.animate({ marginLeft: currX }, 300, function () {
                        $(doc).trigger('AT:Calendar:scrollend');
                    });
                }
            },

            // Checks whether the browser has transform3d capabilities.
            // To be evaluated on init only.
            // https://gist.github.com/lorenzopolidori/3794226
            has3d: (function () {
                var el = document.createElement('p'),
                    has3d,
                    transforms = {
                        'webkitTransform': '-webkit-transform',
                        'OTransform': '-o-transform',
                        'msTransform': '-ms-transform',
                        'MozTransform': '-moz-transform',
                        'transform': 'transform'
                    };

                // Add it to the body to get the computed style
                document.body.insertBefore(el, null);

                for (var t in transforms) {
                    if (el.style[t] !== undefined) {
                        el.style[t] = 'translate3d(1px,1px,1px)';
                        has3d = win.getComputedStyle(el).getPropertyValue(transforms[t]);
                    }
                }

                document.body.removeChild(el);

                return (has3d !== undefined && has3d.length > 0 && has3d !== "none");
            })(),

            // Decides whether to show "show next appt", depending on the position of the appt (on or off screen).
            updateShowNextApptButton: function ($wrappers) {
                $('.appointments-block.scrollable .arrow.left').removeClass('disabled');

                $wrappers.each(function() {
                    // if it's next-available-appointment, decide whether to show the shortcut button or not
                    var $calendar = $(this).parents('.appointments-block');
                    var $firstAppt = $calendar.find('.appointment').first();

                    if ($firstAppt.length > 0) {
                        // Calendar div bottom-right
                        var calRight = $calendar.outerWidth();

                        // Really terrible manual HTML building here
                        if ($firstAppt.position().left > calRight) {
                            var nearestDate = $firstAppt.closest('.col').data('date');

                            // Create appointment button
                            var $nextAppointmentButton = $('<div class="appointment-enquiry show-next-appointment message"><a href="#" class="btn btn-primary" data-next-appointment-date="' + nearestDate + '">' + $calendar.data('show-next-appointment-label') + ' ' + nearestDate + ' ' + $firstAppt.html() + '</a></div>');
                            $nextAppointmentButton
                                .prependTo($calendar.find('.wrapper'))
                                .css({
                                    left: (calendarWidth / 2) - ($nextAppointmentButton.width() / 2)
                                });
                        }
                    }
                });
            },

            showNextAppointment: function (e) {
                e.preventDefault();

                var $this = $(this); // The button
                var $calendar = $this.closest('.appointments-block');
                var $providers = $('#providers');

                var currDateStart = moment($providers.data('date-start')),
                    currDateEnd = moment($providers.data('date-end'));
                    currDateStart.add(1, 'd');

                // Pivot date is the one that has the first available appointment.
                var pivotDate = moment($this.data('next-appointment-date'));

                // Remove the button
                $this.remove();

                var scrollToAppointment = function() {
                    var $pivotColumn = $calendar.find('.col[data-date=' + pivotDate.format('YYYY-MM-DD') + ']');
                    currX = -1 * $pivotColumn.position().left;
                    currX += step; // Offset it so we can see dates around the pivot date
                    self.translateWrappers();
                };

                // If the pivot date is outside of the [dateStart, dateEnd] period => ask for more data.
                // Formula: index of the column * columnWidth * -1 (-1 because of the scrolling direction)
                if (!moment.range(currDateStart, currDateEnd).contains(pivotDate)) {
                    var dateStart = currDateEnd > pivotDate ? currDateEnd : pivotDate;
                    var dateEnd = pivotDate > dateStart ? pivotDate.add(14, 'd') : dateStart.add(14, 'd');

                    var specialtyId = $calendar.data('specialty-id');

                    self.fetchAppointments(dateStart, dateEnd, specialtyId, scrollToAppointment);
                } else {
                    scrollToAppointment();
                }
            },

            // "MORE" APPOINTMENTS BUTTON
            bindPopovers: function () {
                var $calendar = $('.appointments-block');
                var $more_buttons = $calendar.find('.more');
                $more_buttons.off();

                $more_buttons.each(function () {
                    var $this = $(this);
                    var $parent = $this.closest('.appointments-block');
                    var contentList = $this.data('content-list');

                    $this.popover({
                        content: $parent.find(contentList).html(),
                        html: true,
                        animation: false,
                        trigger: 'manual',
                        placement: 'bottom',
                        template: '<div class="popover appointment-popover" role="tooltip"><div class="popover-content"></div></div>'
                    }).on('mouseenter', function () {
                        $(this).popover('show');
                        $('.popover').on('mouseleave', $calendar, function () {
                            $(this).popover('hide');
                        });
                    }).on('mouseleave', function () {
                        var self = this;
                        setTimeout(function () {
                            if (!$('.popover:hover').length) {
                                $(self).popover('hide')
                            }
                        }, 25);

                    });
                });
            },

            // Create search result items
            loadResultItems: function (searchData) {

                var perPage = searchData.per_page || 20;
                var currentPage = searchData.current_page || 1;

                $.each(searchData.data, function (index, data) {

                    //copy template
                    var item = $('#searchresultitem .result').clone();

                    //set data for each result item
                    var number = perPage * (currentPage - 1) + (index + 1);
                    item.data('slug', ($('.doctor-profile').length > 0)?data.location.slug:data.doctor.slug);
                    item.data('index', number);
                    item.find('.fullsize-image').attr('src', ($('.doctor-profile').length > 0)?data.location.img:data.doctor.img);
                    item.find('.profile-picture-and-calendar .picture').css('backgroundImage', 'url("' + (($('.doctor-profile').length > 0)?data.location.img:data.doctor.img) + '")');
                    item.find('span[property="latitude"]').attr('content', data.location.lat);
                    item.find('span[property="longitude"]').attr('content', data.location.lng);
                    item.find('.row h3 a').text(($('.doctor-profile').length > 0)?data.location.name:data.doctor.name).attr('href', ($('.doctor-profile').length > 0)?'/centro-medico/' + data.location.slug:'/medico/' + data.doctor.slug);
                    item.find('.row .icon .number').text(number);
                    item.find('.stars div').attr('style', 'width:' + (((($('.doctor-profile').length > 0)?data.location.stars:data.doctor.stars) / 5) * 100) + '%');
                    item.find('meta[itemprop="ratingValue"]').attr('content', ($('.doctor-profile').length > 0)?data.location.stars:data.doctor.stars);

                    //doctors profile integration
                    if($('.doctor-profile').length == 1) {
                        item.find('.row .address small').text(data.location.distance + 'km | ' + data.location.street + ' ' + data.location.zip + ' ' + data.location.city);
                        if (data.location.phone != '') {
                            item.find('.row span.fa-phone').parent('a[href^="tel:"]').attr('href', 'tel:' + data.location.phone).html('<span class="fa fa-phone"></span> ' + data.location.phone);
                        } else {
                            item.find('.row span.fa-phone').parent('a[href^="tel:"]').remove();
                        }
                    //practice profile integration
                    } else if($('.practice-profile').length == 1) {
                        if (data.doctor.phone != '') {
                            item.find('.row span.fa-phone').parent('a[href^="tel:"]').attr('href', 'tel:' + data.doctor.phone).html('<span class="fa fa-phone"></span> ' + data.doctor.phone);
                        } else {
                            item.find('.row span.fa-phone').parent('a[href^="tel:"]').remove();
                        }
                    //search integration
                    } else {
                        item.find('.row .address small').text(data.location.distance + 'km | ' + data.location.street + ' ' + data.location.zip + ' ' + data.location.city);
                        if (data.doctor.phone != '') {
                            item.find('.row span.fa-phone').parent('a[href^="tel:"]').attr('href', 'tel:' + data.doctor.phone).html('<span class="fa fa-phone"></span> ' + data.doctor.phone);
                        } else {
                            item.find('.row span.fa-phone').parent('a[href^="tel:"]').remove();
                        }
                    }

                    //create calendar
                    var wrapper = item.find('.wrapper');

                    //for enquiry integration
                    if (data.location.integration == 8) {
                        for (var i = 0; i < 15; i++) {
                            wrapper.append($('<div>', {class: 'col is-a-dummy'}));
                        }
                        wrapper.append($('<div>', {class: 'appointment-enquiry'}).append($('<a>', {
                            class: 'btn btn-primary',
                            text: 'Solicitar citas disponibles',
                            href: '/cita-previa?u=' + data.doctor.id + '&l=' + data.location.id + '&m=' + Object.keys(data.medical_specialties)[0]
                        })));
                        //for calendar integration
                    } else if (data.location.integration == 1 && data.appointments.length > 0) {
                        var dayObj = moment();
                        for (var i = 0; i < 15; i++) {
                            //create day column
                            if (i > 0) {
                                dayObj.add(1, 'days');
                            }
                            var day = $('<div>', {class: 'col' + ((dayObj.format('d') == 1) ? ' week-start' : '')}).data('date', dayObj.format('YYYY-MM-DD'));

                            //column header
                            day.append($('<div>', {class: 'col-header'})
                                .append($('<span>', {class: 'day', text: dayObj.format('dd')}))
                                .append($('<span>', {class: 'date', text: dayObj.format('DD.MM')}))
                            );

                            var more = $('<div>', {class: 'more-appointments'}).attr('data-behavior', 'more-appointments-' + dayObj.format('YYYY-MM-DD'));
                            //time cell
                            var x = 0;
                            $.each(data.appointments, function (index, value) {
                                var appointmentDay = moment(value.start_date);
                                if (appointmentDay.format('YYYY-MM-DD') == dayObj.format('YYYY-MM-DD')) {
                                    var timelink = $('<a>', {
                                        class: 'appointment',
                                        text: appointmentDay.format('HH:mm'),
                                        href: '/cita-previa?u=' + data.doctor.id + '&l=' + data.location.id + '&m=14&s=' + encodeURIComponent(value.start_date) + '&e=' + encodeURIComponent(value.end_date)
                                    });
                                    if (x < 2) {
                                        day.append(timelink);
                                    } else {
                                        more.append(timelink);
                                    }
                                    x++;
                                } else {
                                    x = 0;
                                }
                            });

                            //add more time cell if it has appointments
                            if (more.children('.appointment').length > 0) {
                                day.append($('<div>', {class: 'appointment more'})
                                    .attr('data-content-list', '[data-behavior=more-appointments-' + dayObj.format('YYYY-MM-DD') + ']')
                                    .attr('data-placement', 'left')
                                    .attr('data-toggle', 'popover')
                                    .attr('data-original-title', '')
                                    .append($('<div>', {text: 'más'}))
                                    .append(more)
                                );
                            }

                            wrapper.append(day);

                        }
                        //no appointments available
                    } else {
                        for (var i = 0; i < 15; i++) {
                            wrapper.append($('<div>', {class: 'col is-a-dummy'}));
                        }
                        wrapper.append($('<div>', {
                            class: 'no-appointment-available',
                            text: 'sin fecha disponible'
                        }));
                    }

                    $('#providers').find('.footer').before(item);
                });
            },

            // Bind all the event listeners for the module
            setListeners: function () {
                $(doc)

                .on('AT:Calendar:initialized', function() {
                    self.loadResultItems(win.searchData || {});
                    self.bindPopovers();
                    self.setWrappersWidth();
                    self.updateShowNextApptButton($('.appointments-block.scrollable .wrapper'));
                })

                .on('click', '.show-next-appointment a', self.showNextAppointment)

                .on('AT:Calendar:scrollend', function () {
                    self.bindPopovers();
                })

                .on('touchstart click', '.appointments-block.scrollable .arrow.right', function (e) {
                    // Touch overrides click on tablets. Otherwise, the response is very slow.
                    // http://stackoverflow.com/a/13214114
                    e.stopPropagation();
                    e.preventDefault();

                    if ($(this).hasClass('disabled')) {
                        return;
                    }

                    self.scrollCalendar(DIRECTION.RIGHT, $(this).closest('.appointments-block'));
                })

                .on('touchstart click', '.appointments-block.scrollable .arrow.left', function (e) {
                    // Touch overrides click on tablets. Otherwise, the response is very slow.
                    // http://stackoverflow.com/a/13214114
                    e.stopPropagation();
                    e.preventDefault();

                    if ($(this).hasClass('disabled')) {
                        return;
                    }

                    self.scrollCalendar(DIRECTION.LEFT, $(this).closest('.appointments-block'));
                });
            },

            initialize: function () {
                // Set some important UI numbers
                calendarWidth = $('.appointments-block').first().width();

                self.setListeners();

                $(doc).trigger('AT:Calendar:initialized');
            }
        };

        var self = _.Calendar;

        $(function () {
            if ($('.appointments-block').length) {
                self.initialize();
            }
        });

    }(AT, jQuery, document, window))

});