// Arzttermine Search
var AT = AT || {};

AT._defer(function(){
    (function(_, $, doc, win, und) {

        // Filters
        var filtersExpanded = false;

        _.Search = {
            moveEssentialFiltersToTop: function() {
                $('#filter-location').appendTo('.essential.location');
                $('#filter-specialty').appendTo('.essential.specialty');
                
                $('#filters')
                    .removeClass('hidden collapsed')
                    .addClass('expanded')
                    .css('height', '');
                
                $('.toggle-filters')
                    .removeClass('to-show')
                    .addClass('to-hide');
            },

            moveEssentialFiltersToBottom: function() {
                $('#filters-essential')
                    .append($('#filter-location'))
                    .append($('#filter-specialty'));
                
                $('#filters')
                    .removeClass('expanded')
                    .addClass('hidden collapsed');
                
                $('.toggle-filters')
                    .removeClass('to-hide')
                    .addClass('to-show');
            },

            updateTreatmentServices: function() {
                var treatmentService = $('#filter-treatment'),
                    specialtyId = parseInt($('#filter-specialty').val(), 10);

                treatmentService.empty();

                var specialty_treatments = win.mapSpecialtyTreatment || {};
                
                if (specialtyId && typeof specialty_treatments[specialtyId] === 'object') {
                    // Append the placeholder element
                    treatmentService.append(
                        $('<option></option>').val('').html('Bitte wählen')
                    );
                    
                    for (var specialty in specialty_treatments[specialtyId]) {
                        if (specialty_treatments[specialtyId].hasOwnProperty(specialty)) {
                            // Append the specialty from the JSON
                            treatmentService.append(
                                $('<option></option>').val(specialty).html(specialty_treatments[specialtyId][specialty])
                            );
                        }
                    }
                }
            },
            
            setListeners: function() {
                // Various filter bindings
                $('#filter-specialty').change(self.updateTreatmentServices);
                $('#filter-location').change(function() { $('#filter-selected-district').val(''); });
                $('#filter-district').change(function() { $('#filter-selected-district').val($(this).val()); });

                // @todo: A cleaner way to handle this
                $('.toggle-filters').click(function(e) {
                    e.preventDefault();
                    if (filtersExpanded) {
                        self.moveEssentialFiltersToBottom();
                        filtersExpanded = false;
                    } else {
                        self.moveEssentialFiltersToTop();
                        filtersExpanded = true;
                    }
                });
            },

            initialize: function() {
                self.setListeners();
                $(doc).trigger('AT:Search:initialized');
            }
        };

        var self = _.Search;

        if ($('body').hasClass('search')) {
            $(self.initialize);
        }

    }(AT, jQuery, document, window))
});
