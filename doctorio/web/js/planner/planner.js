// Source for the Planner section
;(function($, win, doc, und) {

    // Polyfills
    if (!Array.prototype.indexOf) {
        Array.prototype.indexOf = function (obj, start) {
            for (var i = (start || 0), j = this.length; i < j; i++) {
                if (this[i] === obj) {
                    return i;
                }
            }
            return -1;
        }
    }

    // Production steps of ECMA-262, Edition 5, 15.4.4.18
    // Reference: http://es5.github.io/#x15.4.4.18
    if (!Array.prototype.forEach) {

        Array.prototype.forEach = function(callback, thisArg) {

            var T, k;

            if (this == null) {
                throw new TypeError(' this is null or not defined');
            }

            // 1. Let O be the result of calling ToObject passing the |this| value as the argument.
            var O = Object(this);

            // 2. Let lenValue be the result of calling the Get internal method of O with the argument "length".
            // 3. Let len be ToUint32(lenValue).
            var len = O.length >>> 0;

            // 4. If IsCallable(callback) is false, throw a TypeError exception.
            // See: http://es5.github.com/#x9.11
            if (typeof callback !== "function") {
                throw new TypeError(callback + ' is not a function');
            }

            // 5. If thisArg was supplied, let T be thisArg; else let T be undefined.
            if (arguments.length > 1) {
                T = thisArg;
            }

            // 6. Let k be 0
            k = 0;

            // 7. Repeat, while k < len
            while (k < len) {

                var kValue;

                // a. Let Pk be ToString(k).
                //   This is implicit for LHS operands of the in operator
                // b. Let kPresent be the result of calling the HasProperty internal method of O with argument Pk.
                //   This step can be combined with c
                // c. If kPresent is true, then
                if (k in O) {

                    // i. Let kValue be the result of calling the Get internal method of O with argument Pk.
                    kValue = O[k];

                    // ii. Call the Call internal method of callback with T as the this value and
                    // argument list containing kValue, k, and O.
                    callback.call(T, kValue, k, O);
                }
                // d. Increase k by 1.
                k++;
            }
            // 8. return undefined
        };
    }

    /**
     * Fills for moment.js support in datetimepicker
     */
    Date.parseDate = function (input, format) {
        return moment(input, format).toDate();
    };

    Date.prototype.dateFormat = function (format) {
        return moment(this).format(format);
    };

    // Quick hacky little default
    // --------------------------------------------------------------------------------------------------------------
    var PlannerBlock = function(startTime, endTime) {
        this.start = startTime;
        this.end   = endTime;
        this.className = 'planner-block';
        this.metadata = {
            blockLength: 15,
            treatmentServices: [],
            interval: 0,
            until: '',
            exclusions: []
        };
    };

    // --------------------------------------------------------------------------------------------------------------

    var $planner = $('#planner'),
        $popup   = $('#planner-block-config'),
        timeFormat = 'HH:mm';

    // Callbacks/helpers
    // --------------------------------------------------------------------------------------------------------------

    // Return the units to use for displaying block length
    var getBlockLabel = function(blockLength) {
        blockLength = parseInt(blockLength, 10);
        // Show hours if it looks more convenient
        return blockLength >= 60 ? (blockLength / 60) + 'St' : blockLength + 'Min';
    };

    // Calculate the event's display title
    var recalculateTitle = function(event) {
        event.title = event.start.format(timeFormat) + ' - ' + event.end.format(timeFormat) + (event.metadata.blockLength ? ' (' + getBlockLabel(event.metadata.blockLength) + ')' : '');
    };

    // Remove an element from the calendar
    var removeEvent = function(event) {
        $planner.fullCalendar('removeEvents', event._id); // No better way to do this :/
    };
    
    // Prevent an event from repeating on this date (still appears, just semi-transparent)
    var hideEvent = function(event, element) {
        // This is the easy part
        element.toggleClass('excluded');
        
        // Toggle the current date inside the event's exclusions array
        var hiddenDate = event.start.format('YYYY-MM-DD'),
            existingDate = event.metadata.exclusions.indexOf(hiddenDate);
        
        if (existingDate > -1) {
            // If it exsts, remove it from the array...
            delete event.metadata.exclusions[existingDate];
        } else {
            // ...otherwise add it
            event.metadata.exclusions.push(hiddenDate);
        }
    };

    // Generate the appearance of recurring events
    var generateRecurringEvents = function(event) {
        var recurringEvents = [],
            weeksToRepeat = 12, // @todo: Put in some sort of config
            repeatDate = moment(event.start).recur().every(event.metadata.interval).days(); // Our recurring event data
        
        // Set the recurring offset to today's date
        repeatDate.fromDate(moment(event.start).isAfter(Date.now(), 'day') ? event.start : Date.now());
        
        // If we have a "repeat until" date, use it, otherwise just add the given weeks
        if (event.metadata.until && moment(event.metadata.until).isAfter(event.start)) {
            repeatDate.endDate(moment(event.metadata.until));
        } else {
            // Repeat until max [weeksToRepeat] from *today*
            repeatDate.endDate(moment().add(weeksToRepeat, 'weeks'));
        }
        
        var recurringDates = repeatDate.all();

        // Build a new series of planner block objects that represent repeated events
        for (var i = 0, j = recurringDates.length; i < j; i++) {
            var plannerBlock = new PlannerBlock();
            plannerBlock.id = event.id;
            plannerBlock.metadata = event.metadata;
            
            // We need to manually rebuild the date + time due to Moment's handling of dates
            // @todo: Cleanup if possible
            plannerBlock.start = moment(moment(recurringDates[i]).format('YYYY-MM-DD') + ' ' + moment(event.start).format(timeFormat));
            plannerBlock.end   = moment(moment(recurringDates[i]).format('YYYY-MM-DD') + ' ' + moment(event.end).format(timeFormat));

            recalculateTitle(plannerBlock);

            recurringEvents.push(plannerBlock);
        }

        return recurringEvents;
    };

    // Get the actual array of events to send to the server, filtered accordingly
    var getClientEvents = function() {
        var clientEvents = $planner.fullCalendar('clientEvents'),
            eventIds  = [], // Just the IDs
            events    = []; // Final returned array

        for (var i = 0, j = clientEvents.length; i < j; i++) {
            // If this is a recurring event and it has already been added, don't add it again
            if (clientEvents[i].id) {
                if (eventIds.indexOf(clientEvents[i].id) > -1) {
                    continue;
                }
                eventIds.push(clientEvents[i].id);
            }

            events.push(clientEvents[i]);
        }

        return events;
    };

    // Show the popup used for editing the event metadata
    var showMetadata = function(event) {

        // Clear all previous checkbox data
        $popup.find('input[type=checkbox]').prop('checked', false);
        
        // Set treatment services
        if (event.metadata.treatmentServices.length === 0) {
            // Pre-check Treatment Services as defaults
            $popup.find('.specialty-treatments').data('defaults').forEach(function(element) {
                $('#treatment_service_id_' + element).prop('checked', true);
            });
        } else {
            event.metadata.treatmentServices.forEach(function(element) {
                $('#treatment_service_id_' + element).prop('checked', true);
            });
        }

        // Adjust the block length ticker
        $popup.find('#block_length').val(event.metadata.blockLength);

        // Recurrence interval (how often the event repeats
        $popup.find('#recurrence_interval').val(event.metadata.interval);

        // Enable a limit to event recurrence
        if (event.metadata.until) {
            $popup.find('#until').val(moment(event.metadata.until, 'YYYY-MM-DD').format('DD-MM-YYYY'));
        }

        // *Now* display the modal
        $popup.data('event', event._id).modal('show');
    };
    
    // This function will colour-code blocks that don't have evenly divisible blocks vs block lengths
    var checkBlockLength = function(event, element) {
        // If the block length doesn't exist, then there's nothing to check
         if (!event.metadata.blockLength) {
             return;
         }

        var duration = parseInt(event.start.diff(event.end, 'minutes')),
            isWarning = !!(duration % event.metadata.blockLength); // It's valid if we have no remainder

        element.toggleClass('warning', isWarning);
    };

    // Send the planner to the server
    var savePlanner = function(generateAppointments) {
        var allEvents = getClientEvents(),
            events = [];

        generateAppointments = !!generateAppointments || false;
        
        // Map the objects to simple arrays
        for (var i = 0, j = allEvents.length; i < j; i++) {
            events.push({
                start: allEvents[i].start.format(),
                end: allEvents[i].end.format(),
                metadata: allEvents[i].metadata
            });
        }

        $.ajax({
            url: win.location,
            type: 'post',
            data: {
                payload: JSON.stringify(events), // We need to JSONify this to ensure we don't get param count problems
                user_id: $planner.data('user-id'),
                location_id: $planner.data('location-id'),
                generate: generateAppointments,
                search_period: parseInt($('#search-period').val(), 10),
                lead_in: parseInt($('#lead-in').val(), 10)
            }
        })
        .done(function () {
            win.location.href = win.location.href;
        });
    };

    // When the modal closes, update the event attached to it
    $(doc).on('hide.bs.modal', function() {
        var treatment_services = [],
            event = $planner.fullCalendar('clientEvents', $popup.data('event')).shift(), // It's an array of 1
            recur_until = '';

        // Compile treatment services
        $popup.find('.treatment_services').each(function() {
            if (this.checked) {
                treatment_services.push($(this).val());
            }
        });
        
        // Get recurrence end date
        if ($popup.find('#until').val()) {
            recur_until = moment($popup.find('#until').val(), 'DD-MM-YYYY HH:mm').format('YYYY-MM-DD');
        }

        // Update event data
        event.metadata.blockLength    = parseInt($popup.find('#block_length').val(), 10);
        event.metadata.interval       = parseInt($popup.find('#recurrence_interval').val(), 10);
        event.metadata.until          = recur_until;
        event.metadata.treatmentServices = treatment_services;

        recalculateTitle(event);

        // Update the event
        $planner.fullCalendar('updateEvent', event);

        // Clear the event ID
        $popup.data('event', '');
    });

    // --------------------------------------------------------------------------------------------------------------

    // Event listeners
    $('.dashboard-actions').on('click', '.save', function (e) {
        e.preventDefault();
        
        var $this = $(this);

        if ($this.data('busy')) {
            return;
        }

        if (!$planner.data('user-id') || !$planner.data('location-id')) {
            alert('Nothing to save');
        } else {
            $this
                .data('busy', true)
                .addClass('disabled');

            savePlanner($this.data('generate'));
        }
    });

    // onLoad
    // --------------------------------------------------------------------------------------------------------------
    $(function() {
        // Check if we have any recurring events
        var plannerBlocks = win.plannerBlocks || [],
            calendarEvents = []; // Holds events we're going to use

        for (var i = 0, j = plannerBlocks.length; i < j; i++) {
            if (plannerBlocks[i].metadata.interval) {
                // Gather recurring events
                calendarEvents = calendarEvents.concat(generateRecurringEvents(plannerBlocks[i]));
            } else {
                calendarEvents.push(plannerBlocks[i]);
            }
        }

        // Initialise the calendar
        $planner.fullCalendar({
            lang: $planner.data('locale'),
            columnFormat: {
                day: 'dddd',
                week: 'ddd D.M.',
                month: 'ddd'
            },
            defaultView: 'agendaWeek',
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek'
            },
            snapDuration: '00:05:00',
            editable: true,
            droppable: true,
            selectable: true,
            selectHelper: true,
            weekends: true,
            allDaySlot: false,
            axisFormat: 'HH:mm',
            slotEventOverlap: false,
            minTime: '06:00',
            maxTime: '23:00',
            contentHeight: 'auto',
            events: calendarEvents,
            // The "event" here is the block, not the JS event
            eventClick: function(event) {
                showMetadata(event);
            },
            eventResize: function(event, element) {
                recalculateTitle(event);
            },
            eventRender: function(event, element) {
                // Add a remove event link
                $('<a href="#" class="remove-event">&#9747</a>')
                    .click(function(e) {
                        e.preventDefault();
                        e.stopPropagation();
                        removeEvent(event);
                    })
                    .appendTo(element);
                
                if (event.metadata !== und && event.metadata.interval) {
                    $('<a href="#" class="hide-event">&#9021</a>')
                        .click(function(e) {
                            e.preventDefault();
                            e.stopPropagation();
                            hideEvent(event, element);
                        })
                        .appendTo(element);
                }
            },
            eventAfterRender: function(event, element) {
                // Check if a recurring event has a list of exclusions and hide the ones we
                // don't want to see. The parent will still exist, the CSS is all that changes
                if (event.metadata.interval && event.metadata.exclusions.length) {
                    for (var i = 0, j = event.metadata.exclusions.length; i < j; i++) {
                        if (event.start.isSame(event.metadata.exclusions[i], 'day')) {
                            element.addClass('excluded');    
                        }
                    }
                }

                // Check block length whenever anything changes for an event
                checkBlockLength(event, element);
            },
            select: function(start, end) {
                var event = new PlannerBlock(start, end);
                recalculateTitle(event);

                $planner.fullCalendar('renderEvent', event, true);
                $planner.fullCalendar('unselect');
            }
        });
        
        // Activate bootstrap tooltip
        $('[data-toggle="tooltip"]').tooltip();

        $popup.find('.datetimepicker').datetimepicker({
            format: 'DD-MM-YYYY',
            extraFormats: ['YYYY-MM-DD']
        });

        $popup.on('touch click', '.input-group-addon', function(e){
            $('input[type="text"]', $(this).parent()).focus();
        });
    });

}(jQuery, window, document));