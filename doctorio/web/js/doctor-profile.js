// Arzttermine Generic
var AT = AT || {};

AT._defer((function($, doc, win, und) {

    $(function() {
        $(doc).on('change', 'select.hyperlink', function() {
            win.location = this.options[this.selectedIndex].value;
        });
    });

    $(function() {
        $('select').not(':hidden').select2({
            minimumResultsForSearch: 10
        });
    });

    // For widget copy/paste
    var updateWidget = function() {
        var $modal     = $('#widget-config'),
            $location  = $('#widget-practice'),
            $treatment = $('#widget-treatment_service');
        var src = $modal.data('widget');
        var params = {
            l: $location.val(),
            tt: $treatment.val()
        };

        src = src + '?' + $.param(params);

        var widget_source = '<iframe seamless="seamless" width="100%" height="380px" src="' + src + '"></iframe>';

        $modal.find('textarea').val(widget_source);
    };

    $('#widget-config').find('select').change(updateWidget);

    $('#upload_pic_button').on('click', function() {
        $('#upload_pic').show();
    });

    $('.actions').find('a').on('click', function(e) {
        e.preventDefault();

        $.ajax({
            url: $(this).attr('href'),
            type: 'post',
            data: {
                uid: $(this).parent().data('uid'),
                aid: $(this).parent().data('aid')
            }
        })
        .done(function () {
            win.location.href = win.location.href;
        })
    });

    // This should be unnecessary, but the anchor tag is used for style
    $('table').find('th > a').click(function (e) { e.preventDefault(); });

}(jQuery, document, window)));
