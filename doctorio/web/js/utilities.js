function buildAlert(type, message, field, errors) {
    if (field && errors) {
        errors.push(field);
    }
    return '<div class="alert alert-' + type + '">' + message + '</div>';
}

function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}

function hasErrorResponse(json, err) {
    var errors = (json.errors) ? json.errors : json;
    if (!errors || !errors.length) return [];

    return errors.filter(function (e) {
        return e.code === err || endsWith(e.reference, err);
    });
}
