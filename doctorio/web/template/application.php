<?php if (!isset($_GET['page'])) { $page = "home"; } else { $page = $_GET['page']; } ?>
<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8" />
    <meta content="IE=edge" http-equiv="X-UA-Compatible" />
    <title>💻 doctorio.es > Application</title>
    <link href="/css/application.css" rel="stylesheet" type="text/css" />
    <link href="//fonts.googleapis.com/css?family=Gudea:100,300,400,700,900" rel="stylesheet" type="text/css" />
    <meta content="width=device-width, initial-scale=1" name="viewport" />
  </head>
  <body>
    <?php if ($page == "search"): ?><header class="is-fixed"><?php else: ?><header><?php endif; ?>
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-4 col-lg-3">
          <button aria-expanded="false" class="navbar-toggler pull-right hidden-md-up collapsed" data-target="[data-name=mobile-navigation]" data-toggle="collapse" type="button">☰</button><a class="logo" href="?page=user"><img src="/images/logo/doctorio-logo.svg" /></a>
        </div>
        <div class="col-sm-9 col-md-8 col-lg-9 navigation hidden-sm-down">
          <!--for default navigation-->
          <nav>
            <ul class="list-unstyled text-navigation">
              <li class="active">
                <a class="text-uppercase" href="#">Product</a>
              </li>
              <li>
                <span class="hidden-md-down">pide cita previa</span> <span class="fa fa-phone"></span><span>932201665</span>
              </li>
            </ul>
            <ul class="list-unstyled button-navigation">
              <li>
                <a class="text-uppercase btn btn-primary is-thinner" href="#"><Cerrar>Sign in</Cerrar></a>
              </li>
              <li>
                <a class="text-uppercase btn btn-secondary is-thinner" href="#"><Cerrar>Sign up</Cerrar></a>
              </li>
            </ul>
          </nav>
          <!--for join page navigation--><!--nav.text-right--><!--  ul.list-unstyled--><!--    li--><!--      span> pide cita previa--><!--      span.fa.fa-phone--><!--      span 932201665-->
        </div>
        <div class="col-sm-12">
          <nav class="stacked-navigation collapse" data-name="mobile-navigation">
            <ul class="list-unstyled level-1">
              <li>
                <a href="#">Buscar Doctor</a>
              </li>
              <li>
                <a href="#">Dashboard</a>
              </li>
              <li>
                <a href="#">Iniciar sesión para doctor</a>
              </li>
              <li>
                <a href="#">Iniciar sesión para pacientes</a>
              </li>
              <li>
                <a href="#">¡Registrarse aquí gratis para doctor</a>
              </li>
              <li>
                <a href="#">¡Registrarse aquí gratis para pacientes</a>
              </li>
              <li>
                <a href="#">Cerrar Sesión</a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
    </header>
    <div class="content-wrapper">
      <?php include("_application/_$page.php"); ?>
      <footer>
        <div class="container">
          <div class="row">
            <div class="col-sm-12">
                <img src="{{ asset('images/logo/arzttermine-logo.svg') }}" alt="arzttermine logo" height="39" width="203" />
            </div>
            <div class="col-sm-3">
              <h3>
                Legal
              </h3>
              <nav>
                <ul>
                  <li>
                    <a href="#">Términos y condiciones</a>
                  </li>
                  <li>
                    <a href="#">Aviso Legal</a>
                  </li>
                  <li>
                    <a href="#">Protección de datos</a>
                  </li>
                </ul>
              </nav>
            </div>
            <div class="col-sm-3">
              <h3>
                Empresa
              </h3>
              <nav>
                <ul>
                  <li>
                    <a href="#">Contacto</a>
                  </li>
                </ul>
              </nav>
            </div>
            <div class="col-sm-3">
              <h3>
                Any other link collection
              </h3>
            </div>
            <div class="col-sm-3">
              <h3>
                Social Media
              </h3>
              <nav>
                <ul>
                  <li>
                    <a href="#">Facebook</a>
                  </li>
                  <li>
                    <a href="#">Twitter</a>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </footer>
    </div>
    <div class="❤"><a href="?page=home#home" id="home">                home</a><a href="?page=search#search" id="search">            search</a><a href="?page=book#book" id="book">                book</a><a href="?page=confirm#confirm" id="confirm">          confirm</a><a href="?page=rating#rating" id="rating">            rating</a><a href="?page=doctor#doctor" id="doctor">            doctor</a><a href="?page=practice#practice" id="practice">        practice</a><a href="?page=login#login" id="login">              login</a><a href="?page=register#register" id="register">        register</a><a href="?page=doctors_list#doctors_list" id="doctors_list">doctors_list</a><a href="?page=error#error" id="error">              error</a><a href="?page=static#static" id="static">            static</a>
    <script src="/js/application.js" type="text/javascript"></script>
  </body>
</html>
