<div class="text-right m-b-2">
  <span>Ratings: 1 – 25 of 63</span>
  <div class="btn-group m-l-1">
    <div class="btn btn-primary-outline btn-sm">
      <span class="fa fa-chevron-left"></span>
    </div>
    <div class="btn btn-primary-outline btn-sm">
      <span class="fa fa-chevron-right"></span>
    </div>
  </div>
</div>
<div class="table-responsive">
  <table class="table dashboard-table">
    <thead>
      <tr>
        <th>
          Rating
        </th>
        <th>
          <a href="#">Average 
            <div class="fa fa-chevron-down icon"></div>
          </a>
        </th>
        <th>
          Patient Review
        </th>
        <th>
          Patient
        </th>
        <th>
          <a href="#">Date 
            <div class="fa fa-chevron-up icon"></div>
          </a>
        </th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>
          <div class="rating-star is-inline">
            <div class="rating-name">
              Conducta del doctor
            </div>
            <div class="stars">
              <div style="width: 91%"></div>
            </div>
          </div>
          <div class="rating-star is-inline">
            <div class="rating-name">
              Puntualidad
            </div>
            <div class="stars">
              <div style="width: 51%"></div>
            </div>
          </div>
          <div class="rating-star is-inline">
            <div class="rating-name">
              Valoración general
            </div>
            <div class="stars">
              <div style="width: 9%"></div>
            </div>
          </div>
        </td>
        <td class="text-xs-center">
          5.00 
        </td>
        <td>
          <p>
            Lorem ipsum In aute fugiat eiusmod exercitation ut occaecat nulla. Lorem ipsum Ut dolor esse commodo in consectetur aute. Lorem ipsum Cupidatat dolore tempor labore Excepteur enim consequat veniam officia ut. 
          </p>
          <div class="text-right">
            <span class="simulated-link" data-target="[data-name=comment-0]" data-toggle="collapse">Comment</span>
          </div>
          <div class="collapse comment-form" data-name="comment-0">
            <div class="inner">
              <textarea class="form-control" cols="40" placeholder="Your comment" rows="5"></textarea>
              <div class="pull-right">
                <a class="btn btn-secondary-outline btn-sm btn-sm-smaller" data-target="[data-name=comment-0]" data-toggle="collapse">Cancel</a><a class="btn btn-primary btn-sm btn-sm-smaller">Publish</a>
              </div>
            </div>
          </div>
        </td>
        <td>
          &nbsp; 
        </td>
        <td>
          15.09.2015 
        </td>
      </tr>
      <tr>
        <td>
          <div class="rating-star is-inline">
            <div class="rating-name">
              Conducta del doctor
            </div>
            <div class="stars">
              <div style="width: 28%"></div>
            </div>
          </div>
          <div class="rating-star is-inline">
            <div class="rating-name">
              Puntualidad
            </div>
            <div class="stars">
              <div style="width: 91%"></div>
            </div>
          </div>
          <div class="rating-star is-inline">
            <div class="rating-name">
              Valoración general
            </div>
            <div class="stars">
              <div style="width: 16%"></div>
            </div>
          </div>
        </td>
        <td class="text-xs-center">
          5.00 
        </td>
        <td>
          <p>
            Lorem ipsum In aute fugiat eiusmod exercitation ut occaecat nulla. Lorem ipsum Ut dolor esse commodo in consectetur aute. Lorem ipsum Cupidatat dolore tempor labore Excepteur enim consequat veniam officia ut. 
          </p>
          <textarea class="form-control comment" data-name="textarea-1" disabled="disabled">This is the doctor's comment. </textarea>
          <div class="text-right">
            <span class="simulated-link" data-behavior="enable-textarea" data-button-replacement="[data-name=publish-button-1]" data-target="[data-name=textarea-1]">Edit</span><a class="btn btn-primary btn-sm btn-sm-smaller" data-name="publish-button-1" style="display: none">Publish</a>
          </div>
          <div class="collapse comment-form" data-name="comment-1">
            <div class="inner">
              <textarea class="form-control" cols="40" placeholder="Your comment" rows="5"></textarea>
              <div class="pull-right">
                <a class="btn btn-secondary-outline btn-sm btn-sm-smaller" data-target="[data-name=comment-1]" data-toggle="collapse">Cancel</a><a class="btn btn-primary btn-sm btn-sm-smaller">Publish</a>
              </div>
            </div>
          </div>
        </td>
        <td>
          &nbsp; 
        </td>
        <td>
          15.09.2015 
        </td>
      </tr>
      <tr>
        <td>
          <div class="rating-star is-inline">
            <div class="rating-name">
              Conducta del doctor
            </div>
            <div class="stars">
              <div style="width: 3%"></div>
            </div>
          </div>
          <div class="rating-star is-inline">
            <div class="rating-name">
              Puntualidad
            </div>
            <div class="stars">
              <div style="width: 48%"></div>
            </div>
          </div>
          <div class="rating-star is-inline">
            <div class="rating-name">
              Valoración general
            </div>
            <div class="stars">
              <div style="width: 92%"></div>
            </div>
          </div>
        </td>
        <td class="text-xs-center">
          5.00 
        </td>
        <td>
          <p>
            Lorem ipsum In aute fugiat eiusmod exercitation ut occaecat nulla. Lorem ipsum Ut dolor esse commodo in consectetur aute. Lorem ipsum Cupidatat dolore tempor labore Excepteur enim consequat veniam officia ut. 
          </p>
          <div class="text-right">
            <span class="simulated-link" data-target="[data-name=comment-2]" data-toggle="collapse">Comment</span>
          </div>
          <div class="collapse comment-form" data-name="comment-2">
            <div class="inner">
              <textarea class="form-control" cols="40" placeholder="Your comment" rows="5"></textarea>
              <div class="pull-right">
                <a class="btn btn-secondary-outline btn-sm btn-sm-smaller" data-target="[data-name=comment-2]" data-toggle="collapse">Cancel</a><a class="btn btn-primary btn-sm btn-sm-smaller">Publish</a>
              </div>
            </div>
          </div>
        </td>
        <td>
          &nbsp; 
        </td>
        <td>
          15.09.2015 
        </td>
      </tr>
      <tr>
        <td>
          <div class="rating-star is-inline">
            <div class="rating-name">
              Conducta del doctor
            </div>
            <div class="stars">
              <div style="width: 69%"></div>
            </div>
          </div>
          <div class="rating-star is-inline">
            <div class="rating-name">
              Puntualidad
            </div>
            <div class="stars">
              <div style="width: 27%"></div>
            </div>
          </div>
          <div class="rating-star is-inline">
            <div class="rating-name">
              Valoración general
            </div>
            <div class="stars">
              <div style="width: 44%"></div>
            </div>
          </div>
        </td>
        <td class="text-xs-center">
          5.00 
        </td>
        <td>
          <p>
            Lorem ipsum In aute fugiat eiusmod exercitation ut occaecat nulla. Lorem ipsum Ut dolor esse commodo in consectetur aute. Lorem ipsum Cupidatat dolore tempor labore Excepteur enim consequat veniam officia ut. 
          </p>
          <div class="text-right">
            <span class="simulated-link" data-target="[data-name=comment-3]" data-toggle="collapse">Comment</span>
          </div>
          <div class="collapse comment-form" data-name="comment-3">
            <div class="inner">
              <textarea class="form-control" cols="40" placeholder="Your comment" rows="5"></textarea>
              <div class="pull-right">
                <a class="btn btn-secondary-outline btn-sm btn-sm-smaller" data-target="[data-name=comment-3]" data-toggle="collapse">Cancel</a><a class="btn btn-primary btn-sm btn-sm-smaller">Publish</a>
              </div>
            </div>
          </div>
        </td>
        <td>
          &nbsp; 
        </td>
        <td>
          15.09.2015 
        </td>
      </tr>
      <tr>
        <td>
          <div class="rating-star is-inline">
            <div class="rating-name">
              Conducta del doctor
            </div>
            <div class="stars">
              <div style="width: 5%"></div>
            </div>
          </div>
          <div class="rating-star is-inline">
            <div class="rating-name">
              Puntualidad
            </div>
            <div class="stars">
              <div style="width: 65%"></div>
            </div>
          </div>
          <div class="rating-star is-inline">
            <div class="rating-name">
              Valoración general
            </div>
            <div class="stars">
              <div style="width: 19%"></div>
            </div>
          </div>
        </td>
        <td class="text-xs-center">
          5.00 
        </td>
        <td>
          <p>
            Lorem ipsum In aute fugiat eiusmod exercitation ut occaecat nulla. Lorem ipsum Ut dolor esse commodo in consectetur aute. Lorem ipsum Cupidatat dolore tempor labore Excepteur enim consequat veniam officia ut. 
          </p>
          <div class="text-right">
            <span class="simulated-link" data-target="[data-name=comment-4]" data-toggle="collapse">Comment</span>
          </div>
          <div class="collapse comment-form" data-name="comment-4">
            <div class="inner">
              <textarea class="form-control" cols="40" placeholder="Your comment" rows="5"></textarea>
              <div class="pull-right">
                <a class="btn btn-secondary-outline btn-sm btn-sm-smaller" data-target="[data-name=comment-4]" data-toggle="collapse">Cancel</a><a class="btn btn-primary btn-sm btn-sm-smaller">Publish</a>
              </div>
            </div>
          </div>
        </td>
        <td>
          &nbsp; 
        </td>
        <td>
          15.09.2015 
        </td>
      </tr>
    </tbody>
  </table>
</div>
