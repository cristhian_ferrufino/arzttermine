<div class="row">
  <div class="col-sm-4">
    <div class="form-group">
      <label>Clínica o Consulta</label><select class="form-control">
        <option>
          Por favor selecciona
        </option>
        <option>
          Clínica Dental Monte Del Pilar
        </option>
      </select>
    </div>
  </div>
  <div class="col-sm-12">
    <div class="form-group">
      <h3>
        Reglas para asignar citas
      </h3>
      <div class="row">
        <div class="col-sm-4">
          <label class="is-decent">Días de antelación para reserva de citas</label><select class="form-control">
            <option>
              Ninguno
            </option>
            <option>
              1 Día
            </option>
            <option>
              2 Días
            </option>
            <option>
              3 Días
            </option>
            <option>
              4 Días
            </option>
            <option>
              5 Días
            </option>
            <option>
              6 Días
            </option>
            <option>
              7 Días
            </option>
          </select>
        </div>
        <div class="col-sm-4">
          <label class="is-decent">Limitar disponibilidad de citas a:</label><select class="form-control">
            <option>
              Sin límite
            </option>
            <option>
              Un mes
            </option>
            <option>
              Dos meses
            </option>
            <option>
              Tres meses
            </option>
            <option>
              Publicar
            </option>
            <option>
              HoyMesSemana
            </option>
          </select>
        </div>
        <div class="col-sm-4">
          <a class="btn btn-primary pull-right m-t" data-toggle="tooltip" href="#" title="Atención: Después de su publicación los cambios tardarán unos minutos en ser visibles.">Publicar</a>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-12">
    <div class="btn btn-secondary hidden-xs-down">
      <span class="fa fa-chevron-left icon"></span>April 2015 
    </div>
    <div class="btn-group dropdown">
      <div class="btn-group">
        <button class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" type="button">Mai </button>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="#"><span>Januar</span><span class="label label-default label-pill pull-right">3</span></a><a class="dropdown-item" href="#"><span>Februar</span><span class="label label-default label-pill pull-right">31</span></a><a class="dropdown-item" href="#"><span>März</span><span class="label label-default label-pill pull-right">13</span></a><a class="dropdown-item" href="#"><span>April</span><span class="label label-default label-pill pull-right">0</span></a><a class="dropdown-item" href="#"><span>Mai</span><span class="label label-default label-pill pull-right">3</span></a><a class="dropdown-item" href="#"><span>Juni</span></a><a class="dropdown-item" href="#"><span>Juli</span><span class="label label-default label-pill pull-right">3</span></a><a class="dropdown-item" href="#"><span>August</span><span class="label label-default label-pill pull-right">1</span></a><a class="dropdown-item" href="#"><span>September</span><span class="label label-default label-pill pull-right">3</span></a><a class="dropdown-item" href="#"><span>Oktober</span><span class="label label-default label-pill pull-right">45</span></a><a class="dropdown-item" href="#"><span>November</span><span class="label label-default label-pill pull-right">3</span></a><a class="dropdown-item" href="#"><span>Dezember</span><span class="label label-default label-pill pull-right">2</span></a>
        </div>
      </div>
      <div class="btn-group">
        <button class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" type="button">2015 </button>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="#"><span>2011</span><span class="label label-default label-pill pull-right">3</span></a><a class="dropdown-item" href="#"><span>2012</span><span class="label label-default label-pill pull-right">31</span></a><a class="dropdown-item" href="#"><span>2013</span><span class="label label-default label-pill pull-right">13</span></a><a class="dropdown-item" href="#"><span>2014</span><span class="label label-default label-pill pull-right">0</span></a><a class="dropdown-item" href="#"><span>2015</span><span class="label label-default label-pill pull-right">3</span></a><a class="dropdown-item" href="#"><span>2016</span></a>
        </div>
      </div>
    </div>
    <div class="btn btn-secondary hidden-xs-down m-r">
      Juni 2015 <span class="fa fa-chevron-right icon icon-right"></span>
    </div>
    <a href="#">Hoy</a>
  </div>
  <div class="col-sm-12">
    <div class="calendar">
      – Calendar – 
    </div>
  </div>
</div>
