<div class="row">
  <div class="col-sm-12">
    <form class="sidebar-box colored">
      <div class="row">
        <div class="col-md-6">
          <fieldset class="form-group"></fieldset>
          <label>Praxisname</label><input class="form-control" type="text" />
        </div>
        <div class="col-sm-12">
          <hr class="m-y-2" />
        </div>
        <div class="col-md-6">
          <h5 class="m-b-1">
            Praxisdaten
          </h5>
          <div class="form-group">
            <label>Straße</label><input class="form-control" type="text" />
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-sm-3 col-md-4 col-lg-3">
                <label>PLZ</label><input class="form-control" type="text" />
              </div>
              <div class="col-sm-9 col-md-8 col-lg-9">
                <label>Ort</label><input class="form-control" type="text" />
              </div>
            </div>
          </div>
          <div class="form-group">
            <label>Telefon</label><input class="form-control" type="text" />
          </div>
        </div>
        <div class="col-md-6">
          <h5 class="m-b-1">
            Account
          </h5>
          <div class="form-group">
            <div class="row">
              <div class="col-sm-6">
                <label>Vorname</label><input class="form-control" type="text" />
              </div>
              <div class="col-sm-6">
                <label>Nachname</label><input class="form-control" type="text" />
              </div>
            </div>
          </div>
          <div class="form-group">
            <label>E-Mail-Adresse</label><input class="form-control" type="text" />
          </div>
          <div class="form-group has-error">
            <label>Passwort</label><input class="form-control" type="text" /><small class="help-block">Por favor introduce un valor</small>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <div class="checkbox">
              <label><input autocomplete="off" type="checkbox">Ich akzeptiere die AGB der docbiz UG. Ich habe sie vollständig gelesen und kann sie jederzeit fehlerfrei zitieren.</input></label>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <div class="checkbox has-error">
              <label><input autocomplete="off" type="checkbox">Ich akzeptiere die AGB der docbiz UG. Ich habe sie vollständig gelesen und kann sie jederzeit fehlerfrei zitieren.</input></label>
              <div class="help-block">
                This checkbox is missing.
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-12">
          <hr class="m-y-2" />
          <button class="a btn btn-secondary pull-right" type="submit">Join</button>
        </div>
      </div>
    </form>
    <p class="m-t-1">
      <strong>¿Tienes alguna pregunta?</strong> Contacta con nosotros 24 horas del dia: <a href="tel:+900666666"> <span class="fa fa-phone"></span> 93 22 01 665 </a>
    </p>
  </div>
</div>
