<form class="form-horizontal">
  <fieldset class="form-group row">
    <label class="col-sm-4 col-md-2 form-control-label">Nombre de la consulta o clínica</label>
    <div class="col-sm-4">
      <select class="form-control">
        <option>
          Clínica Dental Monte Del Pilar
        </option>
      </select>
    </div>
  </fieldset>
  <hr />
  <fieldset class="form-group row">
    <label class="col-sm-4 col-md-2 form-control-label">Dirección</label>
    <div class="col-sm-8">
      <div class="form-control-static">
        Calle Mayor 3
      </div>
    </div>
  </fieldset>
  <hr />
  <fieldset class="form-group row">
    <label class="col-sm-4 col-md-2 form-control-label">Código Postal</label>
    <div class="col-sm-8">
      <div class="form-control-static">
        28221
      </div>
    </div>
  </fieldset>
  <hr />
  <fieldset class="form-group row">
    <label class="col-sm-4 col-md-2 form-control-label">Código Postal</label>
    <div class="col-sm-8">
      <div class="form-control-static">
        28221
      </div>
    </div>
  </fieldset>
  <hr />
  <fieldset class="form-group row">
    <label class="col-sm-4 col-md-2 form-control-label">Localidad</label>
    <div class="col-sm-8">
      <div class="form-control-static">
        Madrid
      </div>
    </div>
  </fieldset>
  <hr />
  <fieldset class="form-group row">
    <label class="col-sm-4 col-md-2 form-control-label">Complemento de dirección</label>
    <div class="col-sm-8">
      <div class="form-control-static">
        Madrid
      </div>
    </div>
  </fieldset>
  <hr />
  <fieldset class="form-group row form-inline">
    <label class="col-sm-4 col-md-2 form-control-label">Type of practice</label>
    <div class="col-sm-8">
      <div class="radio">
        <label><input name="radio-1" type="radio" />Self-employed </label>
      </div>
      <div class="radio">
        <label><input checked="checked" name="radio-1" type="radio" />Unemployed </label>
      </div>
      <div class="radio">
        <label><input name="radio-1" type="radio" />Closed for Maintenance </label>
      </div>
    </div>
  </fieldset>
  <hr />
  <fieldset class="form-group row">
    <label class="col-sm-4 col-md-2 form-control-label">Teléfono</label>
    <div class="col-sm-3">
      <input class="form-control" placeholder="+34 - 800 - 1000000" type="tel" />
    </div>
  </fieldset>
  <hr />
  <fieldset class="form-group row">
    <label class="col-sm-4 col-md-2 form-control-label">Página web</label>
    <div class="col-sm-4">
      <input class="form-control" placeholder="http://" type="url" />
    </div>
  </fieldset>
  <hr />
  <fieldset class="form-group row">
    <label class="col-sm-4 col-md-2 form-control-label">Especialidades</label>
    <div class="col-sm-8 col-md-10 form-inline">
      <div class="checkbox">
        <label><input type="checkbox">Dentista</input></label>
      </div>
      <a class="btn btn-secondary btn-sm collapsed" data-target="[data-name=all-specialties]" data-toggle="collapse" href="#"><span class="show-when-collapsed">Mostrar más</span><span class="show-when-not-collapsed">Mostrar menos</span></a>
      <div class="collapse checkbox-list-in-3-rows" data-name="all-specialties">
        <div class="inner">
          <div class="checkbox">
            <label><input type="checkbox" />Acupuntor </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Alergólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Analista clínico </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Anestesista </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Angiólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Cardiólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Cirujano cardiovascular </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Cirujano general </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Cirujano oral y maxilofacial </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Cirujano pedriátrico </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Cirujano plástico </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Cirujano torácico </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Dermatólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Dietista - Nutricionista </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Digestólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Endocrino </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Enfermero </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Especialista en medicina del deporte </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Especialista en medicina del trabajo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Especialista en medicina nuclear </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Especialista en medicina preventiva </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Farmacólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Fisioterapeuta </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Forense </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Geriatra </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Ginecólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Hematólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Homeópata </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Inmunólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Intensivista </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Internista </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Logopeda </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Médico de familia </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Medicina Estética </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Médico general </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Médico rehabilitador </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Microbiólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Nefrólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Neumólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Neurocirujano </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Neurofisiólogo clínico </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Neurólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Oftalmólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Oncólogo médico </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Oncólogo radioterapéutico </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Óptico </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Osteópata </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Otorrinoralingologo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Patólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Pediatra </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Podólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Profesional en medicina alternativa </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Psicólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Psiquiatra </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Radiólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Reumatólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Terapeuta ocupacional </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Traumatólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Urgenciólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Urólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Cirugía Menor Ambulatoria </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Depilación Láser diodo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Ecografia </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Matrona </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Medicina Interna </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Psicología Clínica </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Reconocimentos psicotécnicos </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Valoración de daño corporal </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Cirugía Vascular y Angología </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Rehabilitación </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Dermatología Clínica </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Dermatología Quirúrgica </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Dermatología Estética </label>
          </div>
        </div>
      </div>
    </div>
  </fieldset>
  <hr />
  <fieldset class="form-group row">
    <label class="col-sm-4 col-md-2 form-control-label">Médicos</label>
    <div class="col-sm-8">
      <div class="media">
        <div class="media-left">
          <div class="media-object">
            <img src="/images/application/placeholder.png" width="100" />
          </div>
        </div>
        <div class="media-body">
          <h3>
            Doctorio Roman Cieslik <br />Dentista 
          </h3>
          <small><a href="#">Ver perfil de doctor</a><br /><a href="?page=user">Editar perfil de doctor</a></small>
        </div>
      </div>
    </div>
  </fieldset>
  <hr />
</form>
