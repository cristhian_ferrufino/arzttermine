<div class="table-responsive">
  <table class="table dashboard-table">
    <thead>
      <tr>
        <th>
          Registro hecho por
        </th>
        <th>
          <a href="#">Día del registro
            <div class="fa fa-chevron-down icon"></div>
          </a>
        </th>
        <th>
          <a href="#">Día y hora de la cita
            <div class="fa fa-chevron-down icon"></div>
          </a>
        </th>
        <th>
          Paciente
        </th>
        <th>
          <a href="#">Estado
            <div class="fa fa-chevron-down icon"></div>
          </a>
        </th>
        <th>
          Acciones
        </th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>
          Doctorio Romano Cíeslik
        </td>
        <td>
          2015-07-02 15:05:10
        </td>
        <td>
          2015-07-03 11:00:00
        </td>
        <td>
          <strong>Axel Tester</strong><br />Paciente nuevo <br />Dentista <br />0175-7334455 <a href="mailto:axel@kuzmik.de">axel@kuzmik.de</a>
        </td>
        <td>
          Confirmado
        </td>
        <td>
          <div class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Editar</a>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item" data-target="[data-name=modal-confirmation]" data-toggle="modal"><span class="fa-stack fa-lg icon icon-positive"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-check fa-stack-1x"></i></span>Confirmar cita </a><a class="dropdown-item" data-target="[data-name=modal-cancellation]" data-toggle="modal"><span class="fa-stack fa-lg icon icon-negative"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-ban fa-stack-1x"></i></span>Cancelar cita </a><a class="dropdown-item" data-target="[data-name=modal-edit]" data-toggle="modal"><span class="fa-stack fa-lg icon icon-danger"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-pencil fa-stack-1x"></i></span>Modificar cita </a>
            </div>
          </div>
        </td>
      </tr>
      <tr>
        <td>
          Doctorio Romano Cíeslik
        </td>
        <td>
          2015-07-02 15:05:10
        </td>
        <td>
          2015-07-03 11:00:00
        </td>
        <td>
          <strong>Axel Tester</strong><br />Paciente nuevo <br />Dentista <br />0175-7334455 <a href="mailto:axel@kuzmik.de">axel@kuzmik.de</a>
        </td>
        <td>
          Confirmado
        </td>
        <td>
          <div class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Editar</a>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item" data-target="[data-name=modal-confirmation]" data-toggle="modal"><span class="fa-stack fa-lg icon icon-positive"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-check fa-stack-1x"></i></span>Confirmar cita </a><a class="dropdown-item" data-target="[data-name=modal-cancellation]" data-toggle="modal"><span class="fa-stack fa-lg icon icon-negative"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-ban fa-stack-1x"></i></span>Cancelar cita </a><a class="dropdown-item" data-target="[data-name=modal-edit]" data-toggle="modal"><span class="fa-stack fa-lg icon icon-danger"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-pencil fa-stack-1x"></i></span>Modificar cita </a>
            </div>
          </div>
        </td>
      </tr>
      <tr>
        <td>
          Doctorio Romano Cíeslik
        </td>
        <td>
          2015-07-02 15:05:10
        </td>
        <td>
          2015-07-03 11:00:00
        </td>
        <td>
          <strong>Axel Tester</strong><br />Paciente nuevo <br />Dentista <br />0175-7334455 <a href="mailto:axel@kuzmik.de">axel@kuzmik.de</a>
        </td>
        <td>
          Confirmado
        </td>
        <td>
          <div class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Editar</a>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item" data-target="[data-name=modal-confirmation]" data-toggle="modal"><span class="fa-stack fa-lg icon icon-positive"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-check fa-stack-1x"></i></span>Confirmar cita </a><a class="dropdown-item" data-target="[data-name=modal-cancellation]" data-toggle="modal"><span class="fa-stack fa-lg icon icon-negative"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-ban fa-stack-1x"></i></span>Cancelar cita </a><a class="dropdown-item" data-target="[data-name=modal-edit]" data-toggle="modal"><span class="fa-stack fa-lg icon icon-danger"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-pencil fa-stack-1x"></i></span>Modificar cita </a>
            </div>
          </div>
        </td>
      </tr>
      <tr>
        <td>
          Doctorio Romano Cíeslik
        </td>
        <td>
          2015-07-02 15:05:10
        </td>
        <td>
          2015-07-03 11:00:00
        </td>
        <td>
          <strong>Axel Tester</strong><br />Paciente nuevo <br />Dentista <br />0175-7334455 <a href="mailto:axel@kuzmik.de">axel@kuzmik.de</a>
        </td>
        <td>
          Confirmado
        </td>
        <td>
          <div class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Editar</a>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item" data-target="[data-name=modal-confirmation]" data-toggle="modal"><span class="fa-stack fa-lg icon icon-positive"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-check fa-stack-1x"></i></span>Confirmar cita </a><a class="dropdown-item" data-target="[data-name=modal-cancellation]" data-toggle="modal"><span class="fa-stack fa-lg icon icon-negative"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-ban fa-stack-1x"></i></span>Cancelar cita </a><a class="dropdown-item" data-target="[data-name=modal-edit]" data-toggle="modal"><span class="fa-stack fa-lg icon icon-danger"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-pencil fa-stack-1x"></i></span>Modificar cita </a>
            </div>
          </div>
        </td>
      </tr>
      <tr>
        <td>
          Doctorio Romano Cíeslik
        </td>
        <td>
          2015-07-02 15:05:10
        </td>
        <td>
          2015-07-03 11:00:00
        </td>
        <td>
          <strong>Axel Tester</strong><br />Paciente nuevo <br />Dentista <br />0175-7334455 <a href="mailto:axel@kuzmik.de">axel@kuzmik.de</a>
        </td>
        <td>
          Confirmado
        </td>
        <td>
          <div class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Editar</a>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item" data-target="[data-name=modal-confirmation]" data-toggle="modal"><span class="fa-stack fa-lg icon icon-positive"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-check fa-stack-1x"></i></span>Confirmar cita </a><a class="dropdown-item" data-target="[data-name=modal-cancellation]" data-toggle="modal"><span class="fa-stack fa-lg icon icon-negative"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-ban fa-stack-1x"></i></span>Cancelar cita </a><a class="dropdown-item" data-target="[data-name=modal-edit]" data-toggle="modal"><span class="fa-stack fa-lg icon icon-danger"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-pencil fa-stack-1x"></i></span>Modificar cita </a>
            </div>
          </div>
        </td>
      </tr>
    </tbody>
  </table>
</div>
<div aria-hidden="true" class="modal fade" data-name="modal-confirmation" role="dialog" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button class="close" data-dismiss="modal" type="button">&times;</button>
        <h2>
          Confirmar cita
        </h2>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-6">
            <label>Cita:</label>
          </div>
          <div class="col-sm-6">
            2015-07-03 11:00:00
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <label>Paciente:</label>
          </div>
          <div class="col-sm-6">
            <p>
              <strong>Axel Tester</strong><br />Dentista <br /><span class="fa fa-phone icon"></span><a href="tel:+491757334455">0175 - 733 44 55</a><br /><span class="fa fa-envelope icon"></span><a href="mailto:axel@kuzmik.de">axel@kuzmik.de</a>
            </p>
          </div>
        </div>
        <hr />
        <div class="row">
          <div class="col-sm-12">
            <p>
              El paciente acudirá a su consulta en el día y hora arriba mencionados y se le enviará un recordatorio automatico para esta cita.
            </p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-secondary-outline" data-dismiss="modal" type="button">Cancelar</button><button class="btn btn-primary" data-dismiss="modal" type="button">Confirmar cita</button>
      </div>
    </div>
  </div>
</div>
<div aria-hidden="true" class="modal fade" data-name="modal-cancellation" role="dialog" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button class="close" data-dismiss="modal" type="button">&times;</button>
        <h2>
          Cancelar cita
        </h2>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-6">
            <label>Cita actual:</label>
          </div>
          <div class="col-sm-6">
            2015-07-03 11:00:00
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <label>Paciente:</label>
          </div>
          <div class="col-sm-6">
            <p>
              <strong>Axel Tester</strong><br />Dentista <br /><span class="fa fa-phone icon"></span><a href="tel:+491757334455">0175 - 733 44 55</a><br /><span class="fa fa-envelope icon"></span><a href="mailto:axel@kuzmik.de">axel@kuzmik.de</a>
            </p>
          </div>
        </div>
        <hr />
        <div class="row">
          <div class="col-sm-12">
            <p>
              <strong>¿No puede atender el paciente en el día y la hora indicados?</strong> Puede cancelar esta cita. El paciente recibirá una notificación de cancelación y no acudirá.
            </p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-secondary-outline" data-dismiss="modal" type="button">Cancelar</button><button class="btn btn-danger" data-dismiss="modal" type="button">Cancelar cita</button>
      </div>
    </div>
  </div>
</div>
<div aria-hidden="true" class="modal fade" data-name="modal-edit" role="dialog" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button class="close" data-dismiss="modal" type="button">&times;</button>
        <h2>
          Cambiar cita
        </h2>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-6">
            <label>Cita actual:</label>
          </div>
          <div class="col-sm-6">
            2015-07-03 11:00:00
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <label>Paciente:</label>
          </div>
          <div class="col-sm-6">
            <p>
              <strong>Axel Tester</strong><br />Dentista <br /><span class="fa fa-phone icon"></span><a href="tel:+491757334455">0175 - 733 44 55</a><br /><span class="fa fa-envelope icon"></span><a href="mailto:axel@kuzmik.de">axel@kuzmik.de</a>
            </p>
          </div>
        </div>
        <hr />
        <div class="row">
          <div class="col-sm-12">
            <ul class="nav nav-tabs" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#edit-appointment" role="tab">Confirmar cita nueva</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#propose-appointment" role="tab">Sugerir cita nueva</a>
              </li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="edit-appointment" role="tabpanel">
                <p>
                  <strong>¿Ha acordado telefónicamente una nueva fecha y hora para la cita con el paciente?</strong> Incluya aquí la información de la nueva cita acordada.
                </p>
                <div class="row">
                  <div class="col-sm-6">
                    <label>Nueva cita:</label>
                  </div>
                  <div class="col-sm-4">
                    <input class="form-control" type="text" />
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="propose-appointment" role="tabpanel">
                <p>
                  <strong>¿Todavía no ha acordado una fecha y hora para la nueva cita con el paciente?</strong> Aquí puede sugerir una opción para concretar otra cita previa con este paciente.
                </p>
                <div class="row m-b-1">
                  <div class="col-sm-6">
                    <label>Sugerir nueva cita:</label>
                  </div>
                  <div class="col-sm-4">
                    <input class="form-control" type="text" />
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <label>Tiempo para la respuesta en horas:</label>
                  </div>
                  <div class="col-sm-2">
                    <input class="form-control" placeholder="24" type="text" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-secondary-outline" data-dismiss="modal" type="button">Cancelar</button><button class="btn btn-warning" data-dismiss="modal" type="button">Cancelar cita</button>
      </div>
    </div>
  </div>
</div>
