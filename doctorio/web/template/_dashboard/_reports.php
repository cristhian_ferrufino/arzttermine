<div class="row">
  <div class="col-sm-4">
    <label>Consulta:</label><select>
      <option>
        Por favor selecciona
      </option>
      <option>
        Clínica Dental Monte Del Pilar
      </option>
    </select>
    <hr class="vertical-space only-a-bit hidden-sm-up" />
  </div>
  <div class="col-sm-6 col-md-3 col-lg-2">
    <label>Periodo</label><select>
      <option>
        2015-01
      </option>
      <option>
        2015-02
      </option>
      <option>
        2015-03
      </option>
      <option>
        2015-04
      </option>
      <option>
        2015-05
      </option>
      <option>
        2015-06
      </option>
      <option>
        2015-07
      </option>
      <option>
        2015-08
      </option>
      <option>
        2015-09
      </option>
    </select>
  </div>
  <div class="col-sm-2">
    <label class="hidden-xs-down" style="visibility: hidden; display: block">Guardar</label>
    <hr class="vertical-space only-a-bit hidden-sm-up" />
    <a class="btn btn-primary btn-sm btn-sm-smaller" href="#">Guardar</a>
  </div>
</div>
<div class="row">
  <div class="col-md-10 col-md-offset-1">
    <div class="no-result-hint">
      <p>
        Para el periodo seleccionado no existe ninguna reserva ☹
      </p>
    </div>
  </div>
</div>
<div class="report">
  <div class="table-responsive">
    <table class="table dashboard-table">
      <thead>
        <tr>
          <th>
            Médico
          </th>
          <th>
            Día de la Reserva
          </th>
          <th>
            Paciente
          </th>
          <th>
            Asistencia
          </th>
          <th>
            Estado
          </th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            Doctorio Romano Cíeslik
          </td>
          <td>
            03.07.2015 11:00
          </td>
          <td>
            Axel Tester
          </td>
          <td>
            <div class="radio">
              <label><input name="radio-button-1" type="radio">asistió</input></label>
            </div>
            <div class="radio">
              <label><input name="radio-button-1" type="radio">no asistió</input></label>
            </div>
            <div class="radio">
              <label><input name="radio-button-1" type="radio">cita pospuesta</input></label>
            </div>
            <div class="checkbox">
              <label><input type="checkbox"> Paciente existente</input></label>
            </div>
          </td>
          <td>
            Cancelado
          </td>
        </tr>
        <tr>
          <td>
            Doctorio Romano Cíeslik
          </td>
          <td>
            03.07.2015 11:00
          </td>
          <td>
            Axel Tester
          </td>
          <td>
            <div class="radio">
              <label><input name="radio-button-1" type="radio">asistió</input></label>
            </div>
            <div class="radio">
              <label><input name="radio-button-1" type="radio">no asistió</input></label>
            </div>
            <div class="radio">
              <label><input name="radio-button-1" type="radio">cita pospuesta</input></label>
            </div>
            <div class="checkbox">
              <label><input type="checkbox"> Paciente existente</input></label>
            </div>
          </td>
          <td>
            Cancelado
          </td>
        </tr>
        <tr>
          <td>
            Doctorio Romano Cíeslik
          </td>
          <td>
            03.07.2015 11:00
          </td>
          <td>
            Axel Tester
          </td>
          <td>
            <div class="radio">
              <label><input name="radio-button-1" type="radio">asistió</input></label>
            </div>
            <div class="radio">
              <label><input name="radio-button-1" type="radio">no asistió</input></label>
            </div>
            <div class="radio">
              <label><input name="radio-button-1" type="radio">cita pospuesta</input></label>
            </div>
            <div class="checkbox">
              <label><input type="checkbox"> Paciente existente</input></label>
            </div>
          </td>
          <td>
            Cancelado
          </td>
        </tr>
        <tr>
          <td>
            Doctorio Romano Cíeslik
          </td>
          <td>
            03.07.2015 11:00
          </td>
          <td>
            Axel Tester
          </td>
          <td>
            <div class="radio">
              <label><input name="radio-button-1" type="radio">asistió</input></label>
            </div>
            <div class="radio">
              <label><input name="radio-button-1" type="radio">no asistió</input></label>
            </div>
            <div class="radio">
              <label><input name="radio-button-1" type="radio">cita pospuesta</input></label>
            </div>
            <div class="checkbox">
              <label><input type="checkbox"> Paciente existente</input></label>
            </div>
          </td>
          <td>
            Cancelado
          </td>
        </tr>
        <tr>
          <td>
            Doctorio Romano Cíeslik
          </td>
          <td>
            03.07.2015 11:00
          </td>
          <td>
            Axel Tester
          </td>
          <td>
            <div class="radio">
              <label><input name="radio-button-1" type="radio">asistió</input></label>
            </div>
            <div class="radio">
              <label><input name="radio-button-1" type="radio">no asistió</input></label>
            </div>
            <div class="radio">
              <label><input name="radio-button-1" type="radio">cita pospuesta</input></label>
            </div>
            <div class="checkbox">
              <label><input type="checkbox"> Paciente existente</input></label>
            </div>
          </td>
          <td>
            Cancelado
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
