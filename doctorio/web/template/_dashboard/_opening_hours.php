<div class="row">
  <div class="col-sm-12">
    <div class="table-responsive">
      <table class="table table-striped m-t-3">
        <tbody>
          <tr>
            <td>
              0
            </td>
            <td>
              <strong>Tage:</strong>
            </td>
            <td>
              <code>Tagesauswahl</code>
            </td>
            <td>
              <strong>von:</strong>
            </td>
            <td class="form-inline">
              <div class="input-group">
                <input class="form-control form-control-sm" style="min-width: 4rem" type="text" value="00:00" /><span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
              </div>
            </td>
            <td>
              <strong>bis:</strong>
            </td>
            <td class="form-inline">
              <div class="input-group">
                <input class="form-control form-control-sm" style="min-width: 4rem" type="text" value="00:00" /><span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
              </div>
            </td>
            <td>
              <a class="btn btn-primary-outline btn-sm"><span class="fa fa-trash"></span></a>
            </td>
          </tr>
          <tr>
            <td>
              1
            </td>
            <td>
              <strong>Tage:</strong>
            </td>
            <td>
              <code>Tagesauswahl</code>
            </td>
            <td>
              <strong>von:</strong>
            </td>
            <td class="form-inline">
              <div class="input-group">
                <input class="form-control form-control-sm" style="min-width: 4rem" type="text" value="01:00" /><span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
              </div>
            </td>
            <td>
              <strong>bis:</strong>
            </td>
            <td class="form-inline">
              <div class="input-group">
                <input class="form-control form-control-sm" style="min-width: 4rem" type="text" value="01:00" /><span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
              </div>
            </td>
            <td>
              <a class="btn btn-primary-outline btn-sm"><span class="fa fa-trash"></span></a>
            </td>
          </tr>
          <tr>
            <td>
              2
            </td>
            <td>
              <strong>Tage:</strong>
            </td>
            <td>
              <code>Tagesauswahl</code>
            </td>
            <td>
              <strong>von:</strong>
            </td>
            <td class="form-inline">
              <div class="input-group">
                <input class="form-control form-control-sm" style="min-width: 4rem" type="text" value="02:00" /><span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
              </div>
            </td>
            <td>
              <strong>bis:</strong>
            </td>
            <td class="form-inline">
              <div class="input-group">
                <input class="form-control form-control-sm" style="min-width: 4rem" type="text" value="02:00" /><span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
              </div>
            </td>
            <td>
              <a class="btn btn-primary-outline btn-sm"><span class="fa fa-trash"></span></a>
            </td>
          </tr>
          <tr>
            <td>
              3
            </td>
            <td>
              <strong>Tage:</strong>
            </td>
            <td>
              <code>Tagesauswahl</code>
            </td>
            <td>
              <strong>von:</strong>
            </td>
            <td class="form-inline">
              <div class="input-group">
                <input class="form-control form-control-sm" style="min-width: 4rem" type="text" value="03:00" /><span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
              </div>
            </td>
            <td>
              <strong>bis:</strong>
            </td>
            <td class="form-inline">
              <div class="input-group">
                <input class="form-control form-control-sm" style="min-width: 4rem" type="text" value="03:00" /><span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
              </div>
            </td>
            <td>
              <a class="btn btn-primary-outline btn-sm"><span class="fa fa-trash"></span></a>
            </td>
          </tr>
          <tr>
            <td>
              4
            </td>
            <td>
              <strong>Tage:</strong>
            </td>
            <td>
              <code>Tagesauswahl</code>
            </td>
            <td>
              <strong>von:</strong>
            </td>
            <td class="form-inline">
              <div class="input-group">
                <input class="form-control form-control-sm" style="min-width: 4rem" type="text" value="04:00" /><span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
              </div>
            </td>
            <td>
              <strong>bis:</strong>
            </td>
            <td class="form-inline">
              <div class="input-group">
                <input class="form-control form-control-sm" style="min-width: 4rem" type="text" value="04:00" /><span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
              </div>
            </td>
            <td>
              <a class="btn btn-primary-outline btn-sm"><span class="fa fa-trash"></span></a>
            </td>
          </tr>
          <tr>
            <td>
              5
            </td>
            <td>
              <strong>Tage:</strong>
            </td>
            <td>
              <code>Tagesauswahl</code>
            </td>
            <td>
              <strong>von:</strong>
            </td>
            <td class="form-inline">
              <div class="input-group">
                <input class="form-control form-control-sm" style="min-width: 4rem" type="text" value="05:00" /><span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
              </div>
            </td>
            <td>
              <strong>bis:</strong>
            </td>
            <td class="form-inline">
              <div class="input-group">
                <input class="form-control form-control-sm" style="min-width: 4rem" type="text" value="05:00" /><span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
              </div>
            </td>
            <td>
              <a class="btn btn-primary-outline btn-sm"><span class="fa fa-trash"></span></a>
            </td>
          </tr>
          <tr>
            <td>
              6
            </td>
            <td>
              <strong>Tage:</strong>
            </td>
            <td>
              <code>Tagesauswahl</code>
            </td>
            <td>
              <strong>von:</strong>
            </td>
            <td class="form-inline">
              <div class="input-group">
                <input class="form-control form-control-sm" style="min-width: 4rem" type="text" value="06:00" /><span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
              </div>
            </td>
            <td>
              <strong>bis:</strong>
            </td>
            <td class="form-inline">
              <div class="input-group">
                <input class="form-control form-control-sm" style="min-width: 4rem" type="text" value="06:00" /><span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
              </div>
            </td>
            <td>
              <a class="btn btn-primary-outline btn-sm"><span class="fa fa-trash"></span></a>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <a class="btn btn-primary-outline btn-sm"><span class="fa fa-plus"></span> Hinzufügen </a>
    <hr />
    <button class="btn btn-primary pull-right" type="submit">Guardar</button>
  </div>
</div>
