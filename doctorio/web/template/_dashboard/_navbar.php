<?php if ($page !== "join"): ?>

  <nav class="navbar navbar-light<?php if (($page == 'calendar') || ($page == 'opening_hours')) echo ' has-submenu'; ?>">
    <ul class="nav navbar-nav">
      <li class="nav-item<?php if ($page == 'user') echo ' active'; ?>">
        <a class="nav-link" href="?page=user">Mi perfil médico</a>
      </li>
      <li class="nav-item<?php if ($page == 'practice') echo ' active'; ?>">
        <a class="nav-link" href="?page=practice">Perfil de la consulta</a>
      </li>
      <li class="nav-item<?php if (($page == 'calendar') || ($page == 'opening_hours')) echo ' active'; ?>">
        <a class="nav-link" href="?page=calendar">Calendario</a>
        <ul class="list-unstyled">
          <li><a href="#">Calendar</a></li>
          <li><a href="?page=opening_hours">Öffnungszeiten</a></li>
          <li class="active"><a href="#">Ressourcen</a></li>
          <li><a href="#">Treatmenttypes</a></li>
        </ul>
      </li>
      <li class="nav-item<?php if ($page == 'appointments') echo ' active'; ?>">
        <a class="nav-link" href="?page=appointments">Reservas de cita</a>
      </li>
      <li class="nav-item<?php if ($page == 'reports') echo ' active'; ?>">
        <a class="nav-link" href="?page=reports">Reportes</a>
      </li>
      <li class="nav-item<?php if ($page == 'ratings') echo ' active'; ?>">
        <a class="nav-link" href="?page=ratings">Reputation</a>
      </li>
    </ul>
  </nav>

<?php endif; ?>
