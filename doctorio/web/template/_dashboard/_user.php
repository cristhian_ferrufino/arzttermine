<form class="form-horizontal">
  <fieldset class="form-group row">
    <label class="col-sm-4 col-md-2 form-control-label">Nombre</label>
    <div class="clearfix hidden-md-up"></div>
    <div class="col-sm-6 col-md-2 m-b-1">
      <input class="form-control" placeholder="Título" type="text" />
    </div>
    <div class="col-sm-6 col-md-3 m-b-1">
      <input class="form-control" placeholder="Nombre" type="text" />
    </div>
    <div class="col-sm-12 col-md-3 m-b-1">
      <input class="form-control" placeholder="Apellido" type="text" />
    </div>
    <div class="col-sm-2">
      <img src="/images/application/placeholder.png" width="100" /><br /><small><a href="#">Editar foto del perfil</a></small>
    </div>
  </fieldset>
  <hr />
  <fieldset class="form-group row">
    <label class="col-sm-4 col-md-2 form-control-label">Consulta</label>
    <div class="col-sm-8">
      <p class="form-control-static">
        Clínica Dental Monte Del Pilar <br />Calle Mayor 3 <br />28221 Madrid 
      </p>
    </div>
  </fieldset>
  <hr />
  <fieldset class="form-group row">
    <label class="col-sm-4 col-md-2 form-control-label">Médicos</label>
    <div class="col-sm-6">
      <select class="form-control">
        <option>
          Doctorio Roman Cieslik
        </option>
      </select><a data-target="[data-name=modal-calendar]" data-toggle="modal" href="#">Obtener Calendario Digital</a>
    </div>
  </fieldset>
  <hr />
  <fieldset class="form-group row">
    <label class="col-sm-4 col-md-2 form-control-label">Filosofía</label>
    <div class="col-sm-8">
      <textarea class="form-control" placeholder="Your text goes here…"></textarea>
    </div>
  </fieldset>
  <hr />
  <fieldset class="form-group row">
    <label class="col-sm-4 col-md-2 form-control-label">Especialidades</label>
    <div class="col-sm-8 col-md-10">
      <div class="checkbox m-r-1" style="display: inline-block">
        <label><input type="checkbox">Dentista</input></label>
      </div>
      <a class="btn btn-secondary btn-sm collapsed" data-target="[data-name=all-specialties]" data-toggle="collapse" href="#"><span class="show-when-collapsed">Mostrar más</span><span class="show-when-not-collapsed">Mostrar menos</span></a>
      <div class="collapse checkbox-list-in-3-rows" data-name="all-specialties">
        <div class="inner">
          <div class="checkbox">
            <label><input type="checkbox" />Acupuntor </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Alergólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Analista clínico </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Anestesista </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Angiólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Cardiólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Cirujano cardiovascular </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Cirujano general </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Cirujano oral y maxilofacial </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Cirujano pedriátrico </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Cirujano plástico </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Cirujano torácico </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Dermatólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Dietista - Nutricionista </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Digestólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Endocrino </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Enfermero </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Especialista en medicina del deporte </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Especialista en medicina del trabajo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Especialista en medicina nuclear </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Especialista en medicina preventiva </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Farmacólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Fisioterapeuta </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Forense </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Geriatra </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Ginecólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Hematólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Homeópata </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Inmunólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Intensivista </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Internista </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Logopeda </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Médico de familia </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Medicina Estética </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Médico general </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Médico rehabilitador </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Microbiólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Nefrólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Neumólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Neurocirujano </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Neurofisiólogo clínico </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Neurólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Oftalmólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Oncólogo médico </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Oncólogo radioterapéutico </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Óptico </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Osteópata </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Otorrinoralingologo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Patólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Pediatra </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Podólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Profesional en medicina alternativa </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Psicólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Psiquiatra </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Radiólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Reumatólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Terapeuta ocupacional </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Traumatólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Urgenciólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Urólogo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Cirugía Menor Ambulatoria </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Depilación Láser diodo </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Ecografia </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Matrona </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Medicina Interna </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Psicología Clínica </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Reconocimentos psicotécnicos </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Valoración de daño corporal </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Cirugía Vascular y Angología </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Rehabilitación </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Dermatología Clínica </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Dermatología Quirúrgica </label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Dermatología Estética </label>
          </div>
        </div>
      </div>
    </div>
  </fieldset>
  <hr />
  <fieldset class="form-group row">
    <label class="col-sm-4 col-md-2 form-control-label">Tratamientos</label>
    <div class="col-sm-8 col-md-10">
      <h3>
        Dentista tratamientos médicos
      </h3>
      <div class="checkbox-list-in-3-rows">
        <div class="checkbox">
          <label><input type="checkbox" />Primera visita </label>
        </div>
        <div class="checkbox">
          <label><input type="checkbox" />Visitas sucesivas </label>
        </div>
        <div class="checkbox">
          <label><input type="checkbox" />Ortodoncia </label>
        </div>
        <div class="checkbox">
          <label><input type="checkbox" />Implantología </label>
        </div>
        <div class="checkbox">
          <label><input type="checkbox" />Odontología </label>
        </div>
        <div class="checkbox">
          <label><input type="checkbox" />Periodoncia </label>
        </div>
        <div class="checkbox">
          <label><input type="checkbox" />Endodoncia </label>
        </div>
        <div class="checkbox">
          <label><input type="checkbox" />Odontopediatría </label>
        </div>
        <div class="checkbox">
          <label><input type="checkbox" />Cirugía Oral y Maxilofacial </label>
        </div>
        <div class="checkbox">
          <label><input type="checkbox" />Prótesis Dental (Fija) </label>
        </div>
        <div class="checkbox">
          <label><input type="checkbox" />Prótesis Dental (Removible) </label>
        </div>
        <div class="checkbox">
          <label><input type="checkbox" />Estética Dental </label>
        </div>
        <div class="checkbox">
          <label><input type="checkbox" />ATM (articulación temporomandibular) </label>
        </div>
      </div>
    </div>
  </fieldset>
  <hr />
  <fieldset class="form-group row">
    <label class="col-sm-4 col-md-2 form-control-label">Formación</label>
    <div class="col-sm-8 col-md-6">
      <textarea class="form-control" cols="40" placeholder="Your text goes here…" rows="5"></textarea>
    </div>
  </fieldset>
  <hr />
  <fieldset class="form-group row">
    <label class="col-sm-4 col-md-2 form-control-label">Idiomas</label>
    <div class="col-sm-8 col-md-6">
      <textarea class="form-control" cols="40" placeholder="Your text goes here…" rows="5"></textarea>
    </div>
  </fieldset>
  <hr />
  <fieldset class="form-group row">
    <label class="col-sm-4 col-md-2 form-control-label">Contraseña</label>
    <div class="col-sm-8 col-md-6">
      <input class="form-control" placeholder="Contraseña actual" type="password" /><br /><input class="form-control" placeholder="Contraseña nueva" type="password" />
    </div>
  </fieldset>
  <hr />
  <fieldset class="form-group row">
    <label class="col-sm-4 col-md-2 form-control-label">Correo electónico de notificación</label>
    <div class="col-sm-8 col-md-6">
      <input class="form-control" placeholder="Correo electónico" type="email" />
    </div>
  </fieldset>
  <hr />
  <button class="btn btn-primary pull-right" type="submit">Guardar</button>
</form>
<div aria-hidden="true" class="modal fade" data-name="modal-calendar" role="dialog" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button class="close" data-dismiss="modal" type="button">&times;</button>
        <h2>
          Calendario Digital
        </h2>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-2">
            <label>Clínica o Consulta</label>
          </div>
          <div class="col-sm-10">
            <div class="row">
              <div class="col-sm-6">
                <select class="form-control">
                  <option>
                    Por favor selecciona
                  </option>
                  <option>
                    Clínica Dental Monte Del Pilar
                  </option>
                </select>
              </div>
              <div class="col-sm-6">
                <select class="form-control">
                  <option>
                    Por favor selecciona
                  </option>
                </select>
              </div>
            </div>
            <hr />
            <textarea class="copy-content form-control" cols="40" data-behavior="copy-content" rows="5"><iframe seamless="seamless" width="100%" height="380px" src="http://www.doctorio.dev/widget/medico/ana-elvira-calderon?l=&tt="></iframe> </textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" data-dismiss="modal" type="button">Okay</button>
      </div>
    </div>
  </div>
</div>
