<?php if (!isset($_GET['page'])) { $page = "user"; } else { $page = $_GET['page']; } ?>
<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8" />
    <meta content="IE=edge" http-equiv="X-UA-Compatible" />
    <title>💻 doctorio.es > Application</title>
    <link href="/css/application.css" rel="stylesheet" type="text/css" />
    <link href="//fonts.googleapis.com/css?family=Gudea:100,300,400,700,900" rel="stylesheet" type="text/css" />
    <meta content="width=device-width, initial-scale=1" name="viewport" />
  </head>
  <body>
    <header>
      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <a class="logo" href="?page=user"><img src="{{ asset('images/logo/arzttermine-logo.svg') }}" alt="arzttermine logo" height="39" width="203" /></a>
          </div>
          <div class="col-sm-6 navigation">
            <nav>
              <ul class="list-unstyled">
                <li>
                  <a href="?page=user">Dashboard</a>
                </li>
                <li>
                  <a href="/">Cerrar Sesión</a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </header>
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h1>
            Application
          </h1>
        </div>
      </div>
      <div class="row content">
        <div class="col-sm-12">
          <?php include("_$page.php"); ?>
        </div>
      </div>
    </div>
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
              <img src="{{ asset('images/logo/arzttermine-logo.svg') }}" alt="arzttermine logo" height="39" width="203" />
          </div>
          <div class="col-sm-3">
            <h3>
              Legal
            </h3>
            <nav>
              <ul>
                <li>
                  <a href="#">Términos y condiciones</a>
                </li>
                <li>
                  <a href="#">Aviso Legal</a>
                </li>
                <li>
                  <a href="#">Protección de datos</a>
                </li>
              </ul>
            </nav>
          </div>
          <div class="col-sm-3">
            <h3>
              Empresa
            </h3>
            <nav>
              <ul>
                <li>
                  <a href="#">Contacto</a>
                </li>
              </ul>
            </nav>
          </div>
          <div class="col-sm-3">
            <h3>
              Any other link collection
            </h3>
          </div>
          <div class="col-sm-3">
            <h3>
              Social Media
            </h3>
            <nav>
              <ul>
                <li>
                  <a href="#">Facebook</a>
                </li>
                <li>
                  <a href="#">Twitter</a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </footer>
    <script src="/js/application.js" type="text/javascript"></script>
  </body>
</html>
