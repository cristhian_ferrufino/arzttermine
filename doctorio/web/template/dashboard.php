<?php if (!isset($_GET['page'])) { $page = "user"; } else { $page = $_GET['page']; } ?>
<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8" />
    <meta content="IE=edge" http-equiv="X-UA-Compatible" />
    <title>🔨 doctorio.es > Dashboard</title>
    <link href="/css/dashboard.css" rel="stylesheet" type="text/css" />
    <link href="//fonts.googleapis.com/css?family=Gudea:100,300,400,700,900" rel="stylesheet" type="text/css" />
    <meta content="width=device-width, initial-scale=1" name="viewport" />
  </head>
  <body>
    <header>
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-3">
            <button class="navbar-toggler pull-right hidden-md-up" data-target="[data-name=mobile-navigation]" data-toggle="collapse" type="button">☰</button><a class="logo" href="?page=user"><img src="/images/logo/doctorio-logo.svg" /></a>
          </div>
          <div class="col-md-9 navigation hidden-sm-down">
            <nav>
              <ul class="list-unstyled button-navigation">
                <li>
                  <a href="?page=user">Mi perfil médico</a>
                </li>
                <li>
                  <a class="btn btn-primary is-thinner" href="/">Cerrar Sesión</a>
                </li>
              </ul>
            </nav>
          </div>
          <div class="col-sm-12">
            <nav class="stacked-navigation collapse" data-name="mobile-navigation">
              <ul class="list-unstyled level-1">
                <li>
                  <a href="#">Buscar Doctor</a>
                </li>
                <li class="active">
                  <a href="#">Dashboard</a>
                  <ul class="list-unstyled level-2">
                    <li>
                      <a href="?page=user">Mi perfil médico</a>
                    </li>
                    <li>
                      <a href="?page=practice">Perfil de la consulta</a>
                    </li>
                    <li class="active">
                      <a href="?page=calendar">Calendario</a>
                      <ul class="level-3 list-unstyled">
                        <li>
                          <a href="#">Calendar</a>
                        </li>
                        <li>
                          <a href="?page=opening_hours">Öffnungszeiten</a>
                        </li>
                        <li class="active">
                          <a href="#">Ressourcen</a>
                        </li>
                        <li>
                          <a href="#">Treatmenttypes</a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a href="?page=appointments">Reservas de cita</a>
                    </li>
                    <li>
                      <a href="?page=reports">Reportes</a>
                    </li>
                    <li>
                      <a href="?page=ratings">Reputation</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="#">Iniciar sesión para doctor</a>
                </li>
                <li>
                  <a href="#">Iniciar sesión para pacientes</a>
                </li>
                <li>
                  <a href="#">¡Registrarse aquí gratis para doctor</a>
                </li>
                <li>
                  <a href="#">¡Registrarse aquí gratis para pacientes</a>
                </li>
                <li>
                  <a href="#">Cerrar Sesión</a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </header>
    <div class="container">
      <?php if ($page !== 'join'): ?>
      <div class="row">
        <div class="col-sm-12">
          <h1>
            Doctorio Romano Cíeslik
          </h1>
          <?php include('_dashboard/_navbar.php'); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class="row content">
        <div class="col-sm-12">
          <?php include("_dashboard/_$page.php"); ?>
        </div>
      </div>
    </div>
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
              <img src="{{ asset('images/logo/arzttermine-logo.svg') }}" alt="arzttermine logo" height="39" width="203" />
          </div>
          <div class="col-sm-3">
            <h3>
              Legal
            </h3>
            <nav>
              <ul>
                <li>
                  <a href="#">Términos y condiciones</a>
                </li>
                <li>
                  <a href="#">Aviso Legal</a>
                </li>
                <li>
                  <a href="#">Protección de datos</a>
                </li>
              </ul>
            </nav>
          </div>
          <div class="col-sm-3">
            <h3>
              Empresa
            </h3>
            <nav>
              <ul>
                <li>
                  <a href="#">Contacto</a>
                </li>
              </ul>
            </nav>
          </div>
          <div class="col-sm-3">
            <h3>
              Any other link collection
            </h3>
          </div>
          <div class="col-sm-3">
            <h3>
              Social Media
            </h3>
            <nav>
              <ul>
                <li>
                  <a href="#">Facebook</a>
                </li>
                <li>
                  <a href="#">Twitter</a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </footer>
    <script src="/js/application.js" type="text/javascript"></script>
    <script src="/js/dashboard.js" type="text/javascript"></script>
  </body>
</html>
