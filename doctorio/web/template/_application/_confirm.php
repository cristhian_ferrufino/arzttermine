<div class="container">
  <div class="row">
    <div class="col-sm-6">
      <h2>
        Tu cita
      </h2>
      <div class="highlight-bar table-for-vertical-alignment">
        <div class="table-row">
          <div class="table-cell">
            <strong>Martes, 09.06.2015 a las 09:00</strong>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-4">
          <img class="fullsize-image" src="/asset/000/001/872/Alija Kadic_4.jpg" />
        </div>
        <div class="col-sm-8">
          <p class="text-uppercase">
            Con el doctor
          </p>
          <h3>
            Dra. Marian González Faria
          </h3>
          <p>
            <strong>Clinica Dental Sonrisas</strong><br />Calle de Joaquín María López, 28 <br />28015 Madrid 
          </p>
        </div>
      </div>
    </div>
    <div class="col-sm-6">
      <h2>
        Tu cita ha sido reservada con éxito
      </h2>
      <div class="sidebar-box m-b-1">
        <h4>
          No necesitas que hacer nada más.
        </h4>
        <p>
          Muy pronto te enviaremos una confirmación de tu cita por correo electrónico, con esta notificación se concluye el proceso de reserva de cita.
        </p>
      </div>
      <div class="sidebar-box m-b-1">
        <h4>
          ¿Tienes alguna pregunta?
        </h4>
        <p>
          Contactanos en cualquier momento
        </p>
        <p class="large-text">
          <a href="#"><span class="fa fa-phone"></span> <span>932201665</span></a>
        </p>
      </div>
      <div class="sidebar-box m-b-1">
        <h4>
          doctorio en las redes sociales
        </h4>
        <div class="row">
          <div class="col-lg-6">
            <div class="media">
              <div class="media-left">
                <span class="fa fa-facebook-square fa-3x icon icon-facebook"></span>
              </div>
              <div class="media-body">
                <p>
                  Comparte con tus amigos lo fácil y rápido que es encontrar al médico más adecuado.
                </p>
              </div>
            </div>
            <div class="m-b-1 hidden-lg-up"></div>
          </div>
          <div class="col-lg-6">
            <div class="media">
              <div class="media-left">
                <span class="fa fa-twitter-square fa-3x icon icon-twitter"></span>
              </div>
              <div class="media-body">
                <p>
                  Síguenos en twitter y recibe consejos de salud.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
