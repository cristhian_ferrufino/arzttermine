<div class="container">
  <div class="row">
    <div class="col-sm-6">
      <h2>
        Fecha y hora de mi cita
      </h2>
      <div class="highlight-bar table-for-vertical-alignment">
        <div class="table-row">
          <div class="table-cell">
            El día:  <strong>Lorem ipsum Incididunt reprehenderit esse esse sunt id nostrud minim non quis ad. Lorem ipsum Sit elit culpa nisi fugiat ad Excepteur Duis velit consectetur ullamco ea Ut.</strong>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-4">
          <img class="fullsize-image" src="/asset/000/001/872/Alija Kadic_4.jpg" />
        </div>
        <div class="col-sm-8">
          <p class="text-uppercase">
            Tu Doctor
          </p>
          <h3>
            Dra. Marian González Faria
          </h3>
          <span class="fa fa-phone"></span><a href="tel:+493080619380">932201665</a>
          <h4>
            UBICACIÓN:
          </h4>
          <p>
            <strong>Clinica Dental Sonrisas</strong><br />Calle de Joaquín María López, 28 <br />28015 Madrid 
          </p>
        </div>
        <div class="col-sm-12 m-t-2">
          <ul class="list-unstyled">
            <li>
              ¿Han cambiado tus planes? No hay problema, cancela o modifica tu cita en cualquier momento.
            </li>
            <li>
              Contacta con nuestro servicio de atención a clientes en el 93 220 16 65
            </li>
            <li>
              Tus datos son utilizados conforme a lo establecido en nuestra Política de privacidad de datos.
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="col-sm-6">
      <h2>
        Tus datos
      </h2>
      <form class="sidebar-box colored carousel slide" data-interval="false" data-name="booking-form" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <fieldset class="form-group">
              <label>Soy</label><br />
              <div class="radio-inline">
                <label><input name="radio-1" type="radio" />Mujer </label>
              </div>
              <div class="radio-inline">
                <label><input name="radio-1" type="radio" />Hombre </label>
              </div>
            </fieldset>
            <fieldset class="form-group">
              <label>Nombre(s)</label><input class="form-control" type="text" />
            </fieldset>
            <fieldset class="form-group">
              <label>Apellido(s)</label><input class="form-control" type="text" />
            </fieldset>
            <fieldset class="form-group">
              <label>Correo electrónico</label><input class="form-control" type="text" />
            </fieldset>
            <fieldset class="form-group">
              <label>Número de teléfono movil/fijo:</label><input class="form-control" type="text" />
            </fieldset>
            <fieldset class="form-group">
              <a class="btn btn-primary pull-right" data-slide-to="1" data-target="[data-name=booking-form]">Continuar <span class="fa fa-chevron-right"></span></a>
            </fieldset>
          </div>
          <div class="carousel-item">
            <fieldset class="form-group">
              <label>Motivo de la visita <small opcional=""></small></label><select class="form-control form-control-lg">
                <option>
                  Por favor selecciona
                </option>
                <option>
                  Por favor selecciona 2
                </option>
              </select>
            </fieldset>
            <fieldset class="form-group">
              <label>Aseguradora <small opcional=""></small></label><select class="form-control form-control-lg">
                <option>
                  Por favor selecciona
                </option>
                <option>
                  Por favor selecciona 2
                </option>
              </select>
            </fieldset>
            <fieldset class="form-group">
              <label>¿Has visitado previamente la consulta del médico? SDS Centro Dental?</label>
              <div class="form-inline">
                <div class="radio">
                  <label><input name="radio-2" type="radio" />No </label>
                </div>
                <div class="radio">
                  <label><input name="radio-2" type="radio" />Si </label>
                </div>
              </div>
            </fieldset>
            <fieldset class="form-group">
              <div class="checkbox">
                <label><input type="checkbox" />He leído y acepto las condiciones descritas en la  <a href="#">política de privacidad de datos</a> de doctorio en relación al uso de mis datos para pedir cita médica. Doctorio.es podrá hacer llegar mis datos principalmente a doctores y estos podrán contactarme para validar mis peticiones de cita. </label>
              </div>
              <div class="checkbox">
                <label><input type="checkbox" />Suscribete al boletín de información </label>
              </div>
            </fieldset>
            <fieldset class="form-group">
              <span class="fa fa-chevron-left icon icon-back" data-slide-to="0" data-target="[data-name=booking-form]"></span><a class="btn btn-primary pull-right" data-slide-to="1" data-target="[data-name=booking-form]">Pedir Cita <span class="fa fa-check-circle"></span></a>
            </fieldset>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
