<div class="container">
  <div class="row">
    <div class="col-sm-12">
      <ol class="breadcrumb">
        <li>
          <a href="#">doctorio.es</a>
        </li>
        <li>
          <a href="#">Cirujano oral y maxilofacial en Madrid</a>
        </li>
        <li class="acive">
          <a href="#">SDS Centro Dental</a>
        </li>
      </ol>
      <ol class="breadcrumb">
        <li>
          <a href="#">doctorio.es</a>
        </li>
        <li>
          <a href="#">Dentista en Madrid</a>
        </li>
        <li class="acive">
          <a href="#">SDS Centro Dental</a>
        </li>
      </ol>
      <h1>
        SDS Centro Dental <a class="pull-right" href="#"><span class="fa fa-phone">932201665</span></a>
      </h1>
    </div>
    <div class="row">
      <div class="col-sm-2">
        <code>Image</code>
      </div>
      <div class="col-sm-7">
        <p>
          Calle Rafael Calvo, 5 <br />Madrid 
        </p>
        <h4>
          VALORACIÓN
        </h4>
        <div class="row">
          <div class="col-sm-4">
            <div class="rating-star">
              <div class="rating-name">
                Valoración general 
              </div>
              <div class="rating">
                <div style="width: 16%"></div>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="rating-star">
              <div class="rating-name">Valoración total 
              </div>
              <div class="rating">
                <div style="width: 33%"></div>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="rating-star">
              <div class="rating-name">Valoración total 
              </div>
              <div class="rating">
                <div style="width: 62%"></div>
              </div>
            </div>
          </div>
        </div>
        <h4>
          ÁREAS DE ESPECIALIZACIÓN
        </h4>
        <p>
          Cirujano oral y maxilofacial, Dentista
        </p>
      </div>
      <div class="col-sm-3">
        <img src="http://maps.googleapis.com/maps/api/staticmap?center=almargo&amp;zoom=14&amp;scale=false&amp;size=160x160&amp;maptype=roadmap&amp;format=png&amp;visual_refresh=true&amp;markers=size:mid%7Ccolor:0xff0000%7Clabel:1%7Calmargo" />
      </div>
    </div>
    <h2>
      Elige la fecha más conveniente para tu cita
    </h2>
    <div class="media">
      <div class="media-left">
        <span class="fa fa-map-marker"></span>
      </div>
      <div class="media-body">
        <h3>
          SDS Centro Dental (Dentista) <a class="pull-right" href="#"><span class="fa fa-phone"></span>932201665 </a>
        </h3>
        <p>
          <small>Calle Rafael Calvo, 5 28010 Madrid</small>
        </p>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-2">
        <code>Image</code>
      </div>
      <div class="col-sm-10">
        <div class="calendar">
          — Calendar –
        </div>
      </div>
    </div>
  </div>
</div>
