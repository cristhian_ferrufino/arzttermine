<div class="container has-margin-to-header">
  <div class="row">
    <div class="col-sm-3">
      <nav class="stacked-navigation">
        <ul class="list-unstyled level-1">
          <li>
            <a href="#">Comunícate con nosotros</a>
          </li>
          <li>
            <a href="#">Aviso legal</a>
          </li>
          <li class="active">
            <a href="#">Términos y condiciones</a>
          </li>
          <li>
            <a href="#">Protección de datos</a>
          </li>
        </ul>
      </nav>
    </div>
    <div class="col-sm-9">
      <h1>
        Términos y Condiciones
      </h1>
      <p>
        Los servicios y contenidos ofrecidos en el portal www. doctorio.es son exclusivamente para la intermediación de las citas con el médico y no pueden sustituir para nada la consulta o el tratamiento profesional. doctorio.es solo muestra una selección de los médicos en las ciudades correspondientes y no se ofrece garantía alguna sobre su totalidad o exactitud.
      </p>
      <table class="label-value-table">
        <tr>
          <td>
            <strong>Información de dirección</strong>
          </td>
          <td>
            C/ Juan Bravo, 3 - A <br />28006 Madrid, España
          </td>
        </tr>
        <tr>
          <td>
            <strong>Teléfono</strong>
          </td>
          <td>
            93 220 16 65
          </td>
        </tr>
        <tr>
          <td>
            <strong>Fax</strong>
          </td>
          <td>
            93 220 39 92
          </td>
        </tr>
        <tr>
          <td>
            <strong>E-Mail</strong>
          </td>
          <td>
            <a href="#">info@doctorio.es</a>
          </td>
        </tr>
      </table>
      <h4>
        §1 Objetivos de los Términos y Condiciones Generales
      </h4>
      <p>
        Estos son los términos y condiciones de doctorio.es. Se refieren al servicio ofrecido, los cuales están en Internet, en la dirección URL http://www.doctorio.es y otros subdominios correspondientes. El Objetivo de los Términos y Condiciones Generales es regular la relación jurídica entre doctorio.es y todas las personas físicas o jurídicas que utilizan doctorio.es. La relación jurídica entre doctorio.es y sus miembros está determinada por los presentes Términos y Condiciones Generales, las directrices para la creación de perfiles, la redacción y poner comentarios. Los Términos y Condiciones Generales, así como las directrices también se aplican cuando se utilizan partes de la página web doctorio.es en otros sitios web.
      </p>
      <img class="fullsize-image m-b-1" src="/images/application/static-image.jpg" />
      <p>
        Estos son los términos y condiciones de doctorio.es. Se refieren al servicio ofrecido, los cuales están en Internet, en la dirección URL http://www.doctorio.es y otros subdominios correspondientes. El Objetivo de los Términos y Condiciones Generales es regular la relación jurídica entre doctorio.es y todas las personas físicas o jurídicas que utilizan doctorio.es. La relación jurídica entre doctorio.es y sus miembros está determinada por los presentes Términos y Condiciones Generales, las directrices para la creación de perfiles, la redacción y poner comentarios. Los Términos y Condiciones Generales, así como las directrices también se aplican cuando se utilizan partes de la página web doctorio.es en otros sitios web.Estos son los términos y condiciones de doctorio.es. Se refieren al servicio ofrecido, los cuales están en Internet, en la dirección URL http://www.doctorio.es y otros subdominios correspondientes. El Objetivo de los Términos y Condiciones Generales es regular la relación jurídica entre doctorio.es y todas las personas físicas o jurídicas que utilizan doctorio.es. La relación jurídica entre doctorio.es y sus miembros está determinada por los presentes Términos y Condiciones Generales, las directrices para la creación de perfiles, la redacción y poner comentarios. Los Términos y Condiciones Generales, así como las directrices también se aplican cuando se utilizan partes de la página web doctorio.es en otros sitios web.
      </p>
      <img class="halfsize-image pull-right m-b-1 m-l-1" src="/images/application/static-image.jpg" />
      <p>
        Estos son los términos y condiciones de doctorio.es. Se refieren al servicio ofrecido, los cuales están en Internet, en la dirección URL http://www.doctorio.es y otros subdominios correspondientes. El Objetivo de los Términos y Condiciones Generales es regular la relación jurídica entre doctorio.es y todas las personas físicas o jurídicas que utilizan doctorio.es. La relación jurídica entre doctorio.es y sus miembros está determinada por los presentes Términos y Condiciones Generales, las directrices para la creación de perfiles, la redacción y poner comentarios. Los Términos y Condiciones Generales, así como las directrices también se aplican cuando se utilizan partes de la página web doctorio.es en otros sitios web.Estos son los términos y condiciones de doctorio.es. Se refieren al servicio ofrecido, los cuales están en Internet, en la dirección URL http://www.doctorio.es y otros subdominios correspondientes. El Objetivo de los Términos y Condiciones Generales es regular la relación jurídica entre doctorio.es y todas las personas físicas o jurídicas que utilizan doctorio.es. La relación jurídica entre doctorio.es y sus miembros está determinada por los presentes Términos y Condiciones Generales, las directrices para la creación de perfiles, la redacción y poner comentarios. Los Términos y Condiciones Generales, así como las directrices también se aplican cuando se utilizan partes de la página web doctorio.es en otros sitios web.
      </p>
      <img class="thirdsize-image pull-left m-b-1 m-r-1" src="/images/application/static-image.jpg" />
      <p>
        Estos son los términos y condiciones de doctorio.es. Se refieren al servicio ofrecido, los cuales están en Internet, en la dirección URL http://www.doctorio.es y otros subdominios correspondientes. El Objetivo de los Términos y Condiciones Generales es regular la relación jurídica entre doctorio.es y todas las personas físicas o jurídicas que utilizan doctorio.es. La relación jurídica entre doctorio.es y sus miembros está determinada por los presentes Términos y Condiciones Generales, las directrices para la creación de perfiles, la redacción y poner comentarios. Los Términos y Condiciones Generales, así como las directrices también se aplican cuando se utilizan partes de la página web doctorio.es en otros sitios web.Estos son los términos y condiciones de doctorio.es. Se refieren al servicio ofrecido, los cuales están en Internet, en la dirección URL http://www.doctorio.es y otros subdominios correspondientes. El Objetivo de los Términos y Condiciones Generales es regular la relación jurídica entre doctorio.es y todas las personas físicas o jurídicas que utilizan doctorio.es. La relación jurídica entre doctorio.es y sus miembros está determinada por los presentes Términos y Condiciones Generales, las directrices para la creación de perfiles, la redacción y poner comentarios. Los Términos y Condiciones Generales, así como las directrices también se aplican cuando se utilizan partes de la página web doctorio.es en otros sitios web.
      </p>
      <form class="sidebar-box m-b-3">
        <fieldset class="form-group">
          <label>Nombre</label><input class="form-control" />
        </fieldset>
        <fieldset class="form-group">
          <label>Correo electrónico</label><input class="form-control" />
        </fieldset>
        <fieldset class="form-group">
          <label>Título</label><input class="form-control" />
        </fieldset>
        <fieldset class="form-group">
          <label>Tu mensaje</label><textarea class="form-control" cols="50" placeholder="Your Message goes here." rows="10"></textarea>
        </fieldset>
        <fieldset class="form-group">
          <a class="btn btn-primary pull-right">Enviar <span class="fa fa-check-send"></span></a>
        </fieldset>
      </form>
      <h4>
        §2 Servicios de doctorio.es
      </h4>
      <ol>
        <li>
          doctorio.es es una oferta que permite a sus usuarios y miembros consultar informaciones sobre los médicos (citas disponibles, ubicación, comentarios, etc.).
        </li>
        <li>
          Los miembros reciben un perfil de usuario, lo que les permite utilizar la oferta de comunicación de doctorio.es.
        </li>
      </ol>
      <h4>
        §3 El uso del servicio de información de Internet de doctorio.es
      </h4>
      <ol>
        <li>
          El servicio de información por Internet doctorio.es permite a todos los usuarios y miembros de doctorio.es encontrar citas y datos de la dirección, así como informaciones públicas adicionales (en los consiguientes datos maestros) del ofertante consultado. El uso del servicio de información y los datos maestros implicados sólo está permitido exclusivamente para este fin y para uso privado. Se prohíbe cualquier uso para fines extraños, en particular para el intercambio comercial de la información, así como cualquier otra evaluación o uso comercial. El uso o procesamiento de los datos maestros consultados para otros fines está permitido solo bajo ciertas condiciones estrictas bajo las leyes alemanas de protección de datos.
        </li>
        <li>
          Para la disponibilidad de los servicios de información, así como para la completitud y exactitud de los datos contenidos doctorio.es no asume ninguna responsabilidad.
        </li>
      </ol>
      <h4>
        §4 Conclusión de contrato
      </h4>
      <ol>
        <li>
          Todos los usuarios de Internet pueden acceder a la oferta publicada en doctorio.es.
        </li>
        <li>
          También existe la posibilidad de una membresía gratis. Los usuarios que quieran utilizar esta li rta adicional, deben registrarse primero en doctorio.es y luego pueden acceder a las funciones li conjunto de doctorio.es.
        </li>
        <li>
          Al registrarse, cada miembro asegura que sus datos personales están al día, completos y veraces li ue los actualizará también después de su registro durante la duración del contrato.
        </li>
        <li>
          El Miembro asegura, además, que o bien ha alcanzado la edad de 18 años o que ha recibido el li sentimiento de sus padres o tutores, para usar los como en el § 2 de los Términos y Condiciones li erales, los servicios mencionados, incluidos los servicios de comunicación y hacer evaluaciones.
        </li>
        <li>
          Los evaluaciones de los servicios médicos solo los pueden realizar los pacientes que han li ervado previamente una cita a través doctorio.es.
        </li>
        <li>
          El uso para el paciente es generalmente gratis. Los médicos que se registran, recibirán un li trato por correo electrónico, que debe ser acordado por escrito.
        </li>
        <li>
          Solo por acuerdo bilateral, se completa el registro y entra en una relación contractual.
        </li>
      </ol>
    </div>
  </div>
</div>
