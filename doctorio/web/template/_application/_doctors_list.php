<div class="container">
  <div class="row">
    <div class="col-sm-12">
      <div class="flash alert alert-info">
        <strong>Wusstest Du schon?</strong> In 12 Minuten loggen wir Dich aus. 
      </div>
      <div class="flash alert alert-success">
        <strong>Glückwunsch!</strong> Du hast Dich erfolgreich eingeloggt. 
      </div>
      <div class="flash alert alert-warning">
        <strong>Huch!</strong> Willst Du Dich wirklich ausloggen? 
      </div>
      <div class="flash alert alert-danger">
        <strong>Schade!</strong> Das Ausloggen ist fehlgeschlagen. Du wirst nun für immer eingeloggt bleiben. 
      </div>
    </div>
    <div class="col-sm-12">
      <h1>
        Directorio médico
      </h1>
      <h2>
        Encuentra dentistas y consultorios médicos a través de nuestro directorio médico.
      </h2>
      <ul class="list-unstyled list-inline m-b-3">
        <li>
          <a href="#">a</a>
        </li>
        <li>
          <a href="#">b</a>
        </li>
        <li>
          <a href="#">c</a>
        </li>
        <li>
          <a href="#">d</a>
        </li>
        <li>
          <a href="#">e</a>
        </li>
        <li>
          <a href="#">f</a>
        </li>
        <li>
          <a href="#">g</a>
        </li>
        <li>
          <a href="#">h</a>
        </li>
        <li>
          <a href="#">i</a>
        </li>
        <li>
          <a href="#">j</a>
        </li>
        <li>
          <a href="#">k</a>
        </li>
        <li>
          <a href="#">l</a>
        </li>
        <li>
          <a href="#">m</a>
        </li>
        <li>
          <a href="#">n</a>
        </li>
        <li>
          <a href="#">o</a>
        </li>
        <li>
          <a href="#">p</a>
        </li>
        <li>
          <a href="#">q</a>
        </li>
        <li>
          <a href="#">r</a>
        </li>
        <li>
          <a href="#">s</a>
        </li>
        <li>
          <a href="#">t</a>
        </li>
        <li>
          <a href="#">u</a>
        </li>
        <li>
          <a href="#">v</a>
        </li>
        <li>
          <a href="#">w</a>
        </li>
        <li>
          <a href="#">x</a>
        </li>
        <li>
          <a href="#">y</a>
        </li>
        <li>
          <a href="#">z</a>
        </li>
        <li>
          <a href="#">a - z</a>
        </li>
      </ul>
      <div class="phone-book-list m-b-3">
        <div>
          <h3>
            Tal Como Eres
          </h3>
          <p>
            Dr. Gómez Ferrer, 3 <br />46010 - Valencia <br /><a href="#">Tal Como Eres</a>
          </p>
        </div>
        <div>
          <h3>
            Tal Como Eres
          </h3>
          <p>
            Dr. Gómez Ferrer, 3 <br />46010 - Valencia <br /><a href="#">Tal Como Eres</a>
          </p>
        </div>
        <div>
          <h3>
            Tal Como Eres
          </h3>
          <p>
            Dr. Gómez Ferrer, 3 <br />46010 - Valencia <br /><a href="#">Tal Como Eres</a>
          </p>
        </div>
        <div>
          <h3>
            Tal Como Eres
          </h3>
          <p>
            Dr. Gómez Ferrer, 3 <br />46010 - Valencia <br /><a href="#">Tal Como Eres</a>
          </p>
        </div>
        <div>
          <h3>
            Tal Como Eres
          </h3>
          <p>
            Dr. Gómez Ferrer, 3 <br />46010 - Valencia <br /><a href="#">Tal Como Eres</a>
          </p>
        </div>
        <div>
          <h3>
            Tal Como Eres
          </h3>
          <p>
            Dr. Gómez Ferrer, 3 <br />46010 - Valencia <br /><a href="#">Tal Como Eres</a>
          </p>
        </div>
        <div>
          <h3>
            Tal Como Eres
          </h3>
          <p>
            Dr. Gómez Ferrer, 3 <br />46010 - Valencia <br /><a href="#">Tal Como Eres</a>
          </p>
        </div>
        <div>
          <h3>
            Tal Como Eres
          </h3>
          <p>
            Dr. Gómez Ferrer, 3 <br />46010 - Valencia <br /><a href="#">Tal Como Eres</a>
          </p>
        </div>
        <div>
          <h3>
            Tal Como Eres
          </h3>
          <p>
            Dr. Gómez Ferrer, 3 <br />46010 - Valencia <br /><a href="#">Tal Como Eres</a>
          </p>
        </div>
        <div>
          <h3>
            Tal Como Eres
          </h3>
          <p>
            Dr. Gómez Ferrer, 3 <br />46010 - Valencia <br /><a href="#">Tal Como Eres</a>
          </p>
        </div>
        <div>
          <h3>
            Tal Como Eres
          </h3>
          <p>
            Dr. Gómez Ferrer, 3 <br />46010 - Valencia <br /><a href="#">Tal Como Eres</a>
          </p>
        </div>
        <div>
          <h3>
            Tal Como Eres
          </h3>
          <p>
            Dr. Gómez Ferrer, 3 <br />46010 - Valencia <br /><a href="#">Tal Como Eres</a>
          </p>
        </div>
        <div>
          <h3>
            Tal Como Eres
          </h3>
          <p>
            Dr. Gómez Ferrer, 3 <br />46010 - Valencia <br /><a href="#">Tal Como Eres</a>
          </p>
        </div>
        <div>
          <h3>
            Tal Como Eres
          </h3>
          <p>
            Dr. Gómez Ferrer, 3 <br />46010 - Valencia <br /><a href="#">Tal Como Eres</a>
          </p>
        </div>
        <div>
          <h3>
            Tal Como Eres
          </h3>
          <p>
            Dr. Gómez Ferrer, 3 <br />46010 - Valencia <br /><a href="#">Tal Como Eres</a>
          </p>
        </div>
        <div>
          <h3>
            Tal Como Eres
          </h3>
          <p>
            Dr. Gómez Ferrer, 3 <br />46010 - Valencia <br /><a href="#">Tal Como Eres</a>
          </p>
        </div>
        <div>
          <h3>
            Tal Como Eres
          </h3>
          <p>
            Dr. Gómez Ferrer, 3 <br />46010 - Valencia <br /><a href="#">Tal Como Eres</a>
          </p>
        </div>
        <div>
          <h3>
            Tal Como Eres
          </h3>
          <p>
            Dr. Gómez Ferrer, 3 <br />46010 - Valencia <br /><a href="#">Tal Como Eres</a>
          </p>
        </div>
        <div>
          <h3>
            Tal Como Eres
          </h3>
          <p>
            Dr. Gómez Ferrer, 3 <br />46010 - Valencia <br /><a href="#">Tal Como Eres</a>
          </p>
        </div>
        <div>
          <h3>
            Tal Como Eres
          </h3>
          <p>
            Dr. Gómez Ferrer, 3 <br />46010 - Valencia <br /><a href="#">Tal Como Eres</a>
          </p>
        </div>
        <div>
          <h3>
            Tal Como Eres
          </h3>
          <p>
            Dr. Gómez Ferrer, 3 <br />46010 - Valencia <br /><a href="#">Tal Como Eres</a>
          </p>
        </div>
        <div>
          <h3>
            Tal Como Eres
          </h3>
          <p>
            Dr. Gómez Ferrer, 3 <br />46010 - Valencia <br /><a href="#">Tal Como Eres</a>
          </p>
        </div>
        <div>
          <h3>
            Torres Diez Y Hernandez-gil S.L.
          </h3>
          <p>
            Av. Presidente Adólfo Suárez, 10 <br />46010 - Valencia <br /><a href="#">Tu Desarrollo Personal</a>
          </p>
        </div>
        <div>
          <h3>
            Torres Diez Y Hernandez-gil S.L.
          </h3>
          <p>
            Av. Presidente Adólfo Suárez, 10 <br />46010 - Valencia <br /><a href="#">Tu Desarrollo Personal</a>
          </p>
        </div>
        <div>
          <h3>
            Torres Diez Y Hernandez-gil S.L.
          </h3>
          <p>
            Av. Presidente Adólfo Suárez, 10 <br />46010 - Valencia <br /><a href="#">Tu Desarrollo Personal</a>
          </p>
        </div>
        <div>
          <h3>
            Torres Diez Y Hernandez-gil S.L.
          </h3>
          <p>
            Av. Presidente Adólfo Suárez, 10 <br />46010 - Valencia <br /><a href="#">Tu Desarrollo Personal</a>
          </p>
        </div>
        <div>
          <h3>
            Torres Diez Y Hernandez-gil S.L.
          </h3>
          <p>
            Av. Presidente Adólfo Suárez, 10 <br />46010 - Valencia <br /><a href="#">Tu Desarrollo Personal</a>
          </p>
        </div>
        <div>
          <h3>
            Torres Diez Y Hernandez-gil S.L.
          </h3>
          <p>
            Av. Presidente Adólfo Suárez, 10 <br />46010 - Valencia <br /><a href="#">Tu Desarrollo Personal</a>
          </p>
        </div>
        <div>
          <h3>
            Torres Diez Y Hernandez-gil S.L.
          </h3>
          <p>
            Av. Presidente Adólfo Suárez, 10 <br />46010 - Valencia <br /><a href="#">Tu Desarrollo Personal</a>
          </p>
        </div>
        <div>
          <h3>
            Torres Diez Y Hernandez-gil S.L.
          </h3>
          <p>
            Av. Presidente Adólfo Suárez, 10 <br />46010 - Valencia <br /><a href="#">Tu Desarrollo Personal</a>
          </p>
        </div>
        <div>
          <h3>
            Torres Diez Y Hernandez-gil S.L.
          </h3>
          <p>
            Av. Presidente Adólfo Suárez, 10 <br />46010 - Valencia <br /><a href="#">Tu Desarrollo Personal</a>
          </p>
        </div>
        <div>
          <h3>
            Torres Diez Y Hernandez-gil S.L.
          </h3>
          <p>
            Av. Presidente Adólfo Suárez, 10 <br />46010 - Valencia <br /><a href="#">Tu Desarrollo Personal</a>
          </p>
        </div>
        <div>
          <h3>
            Torres Diez Y Hernandez-gil S.L.
          </h3>
          <p>
            Av. Presidente Adólfo Suárez, 10 <br />46010 - Valencia <br /><a href="#">Tu Desarrollo Personal</a>
          </p>
        </div>
        <div>
          <h3>
            Torres Diez Y Hernandez-gil S.L.
          </h3>
          <p>
            Av. Presidente Adólfo Suárez, 10 <br />46010 - Valencia <br /><a href="#">Tu Desarrollo Personal</a>
          </p>
        </div>
        <div>
          <h3>
            Torres Diez Y Hernandez-gil S.L.
          </h3>
          <p>
            Av. Presidente Adólfo Suárez, 10 <br />46010 - Valencia <br /><a href="#">Tu Desarrollo Personal</a>
          </p>
        </div>
        <div>
          <h3>
            Torres Diez Y Hernandez-gil S.L.
          </h3>
          <p>
            Av. Presidente Adólfo Suárez, 10 <br />46010 - Valencia <br /><a href="#">Tu Desarrollo Personal</a>
          </p>
        </div>
        <div>
          <h3>
            Torres Diez Y Hernandez-gil S.L.
          </h3>
          <p>
            Av. Presidente Adólfo Suárez, 10 <br />46010 - Valencia <br /><a href="#">Tu Desarrollo Personal</a>
          </p>
        </div>
        <div>
          <h3>
            Torres Diez Y Hernandez-gil S.L.
          </h3>
          <p>
            Av. Presidente Adólfo Suárez, 10 <br />46010 - Valencia <br /><a href="#">Tu Desarrollo Personal</a>
          </p>
        </div>
        <div>
          <h3>
            Torres Diez Y Hernandez-gil S.L.
          </h3>
          <p>
            Av. Presidente Adólfo Suárez, 10 <br />46010 - Valencia <br /><a href="#">Tu Desarrollo Personal</a>
          </p>
        </div>
        <div>
          <h3>
            Torres Diez Y Hernandez-gil S.L.
          </h3>
          <p>
            Av. Presidente Adólfo Suárez, 10 <br />46010 - Valencia <br /><a href="#">Tu Desarrollo Personal</a>
          </p>
        </div>
        <div>
          <h3>
            Torres Diez Y Hernandez-gil S.L.
          </h3>
          <p>
            Av. Presidente Adólfo Suárez, 10 <br />46010 - Valencia <br /><a href="#">Tu Desarrollo Personal</a>
          </p>
        </div>
        <div>
          <h3>
            Torres Diez Y Hernandez-gil S.L.
          </h3>
          <p>
            Av. Presidente Adólfo Suárez, 10 <br />46010 - Valencia <br /><a href="#">Tu Desarrollo Personal</a>
          </p>
        </div>
        <div>
          <h3>
            Torres Diez Y Hernandez-gil S.L.
          </h3>
          <p>
            Av. Presidente Adólfo Suárez, 10 <br />46010 - Valencia <br /><a href="#">Tu Desarrollo Personal</a>
          </p>
        </div>
        <div>
          <h3>
            Torres Diez Y Hernandez-gil S.L.
          </h3>
          <p>
            Av. Presidente Adólfo Suárez, 10 <br />46010 - Valencia <br /><a href="#">Tu Desarrollo Personal</a>
          </p>
        </div>
        <div>
          <h3>
            Torres Diez Y Hernandez-gil S.L.
          </h3>
          <p>
            Av. Presidente Adólfo Suárez, 10 <br />46010 - Valencia <br /><a href="#">Tu Desarrollo Personal</a>
          </p>
        </div>
        <div>
          <h3>
            Torres Diez Y Hernandez-gil S.L.
          </h3>
          <p>
            Av. Presidente Adólfo Suárez, 10 <br />46010 - Valencia <br /><a href="#">Tu Desarrollo Personal</a>
          </p>
        </div>
        <div>
          <h3>
            Torres Diez Y Hernandez-gil S.L.
          </h3>
          <p>
            Av. Presidente Adólfo Suárez, 10 <br />46010 - Valencia <br /><a href="#">Tu Desarrollo Personal</a>
          </p>
        </div>
        <div>
          <h3>
            Torres Diez Y Hernandez-gil S.L.
          </h3>
          <p>
            Av. Presidente Adólfo Suárez, 10 <br />46010 - Valencia <br /><a href="#">Tu Desarrollo Personal</a>
          </p>
        </div>
        <div>
          <h3>
            Torres Diez Y Hernandez-gil S.L.
          </h3>
          <p>
            Av. Presidente Adólfo Suárez, 10 <br />46010 - Valencia <br /><a href="#">Tu Desarrollo Personal</a>
          </p>
        </div>
        <div>
          <h3>
            Torres Diez Y Hernandez-gil S.L.
          </h3>
          <p>
            Av. Presidente Adólfo Suárez, 10 <br />46010 - Valencia <br /><a href="#">Tu Desarrollo Personal</a>
          </p>
        </div>
        <div>
          <h3>
            Torres Diez Y Hernandez-gil S.L.
          </h3>
          <p>
            Av. Presidente Adólfo Suárez, 10 <br />46010 - Valencia <br /><a href="#">Tu Desarrollo Personal</a>
          </p>
        </div>
        <div>
          <h3>
            Torres Diez Y Hernandez-gil S.L.
          </h3>
          <p>
            Av. Presidente Adólfo Suárez, 10 <br />46010 - Valencia <br /><a href="#">Tu Desarrollo Personal</a>
          </p>
        </div>
        <div>
          <h3>
            Torres Diez Y Hernandez-gil S.L.
          </h3>
          <p>
            Av. Presidente Adólfo Suárez, 10 <br />46010 - Valencia <br /><a href="#">Tu Desarrollo Personal</a>
          </p>
        </div>
        <div>
          <h3>
            Torres Diez Y Hernandez-gil S.L.
          </h3>
          <p>
            Av. Presidente Adólfo Suárez, 10 <br />46010 - Valencia <br /><a href="#">Tu Desarrollo Personal</a>
          </p>
        </div>
        <div>
          <h3>
            Torres Diez Y Hernandez-gil S.L.
          </h3>
          <p>
            Av. Presidente Adólfo Suárez, 10 <br />46010 - Valencia <br /><a href="#">Tu Desarrollo Personal</a>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>
