<div class="container m-y-3">
  <div class="row m-y-3">
    <div class="col-sm-2 hidden-xs-down">
      <img class="fullsize-image" src="/images/page/404/error_ico.png" />
    </div>
    <div class="col-sm-9 col-sm-offset-1">
      <h1>
        404 – Urgencias
      </h1>
      <h2>
        Desgraciadamente no hemos podido encontrar la consulta o doctor que has solicitado.
      </h2>
      <p>
        Por favor intenta nuevamente o comunícate con nuestro servicio de atención a clientes:
      </p>
      <ul class="list-unstyled">
        <li>
          + <a href="#">Iniciar nueva solicitud en doctorio.es</a>
        </li>
        <li>
          + <a href="#">Contactar servicio de atención a clientes</a>
        </li>
        <li>
          + <a href="#"><span class="fa fa-phone"></span> Atención al cliente 93 220 16 65 </a>
        </li>
      </ul>
    </div>
  </div>
</div>
