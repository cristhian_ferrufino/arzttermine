<div class="container">
  <div class="row">
    <div class="col-sm-6">
      <h1>
        Registro
      </h1>
    </div>
    <div class="col-sm-6 text-right m-b-1 text-right-except-mobile">
      <span>¿Ya te has registrado?</span> <a href="#">Inicia sesión aquí</a>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6">
      <form class="sidebar-box colored">
        <fieldset class="form-group">
          <label>Soy</label>
          <div class="form-inline">
            <div class="radio">
              <label><input name="radio-10" type="radio" />Mjuer </label>
            </div>
            <div class="radio">
              <label><input name="radio-10" type="radio" />Hombre </label>
            </div>
          </div>
        </fieldset>
        <fieldset class="form-group">
          <label>Título</label><input class="form-control" type="text" />
        </fieldset>
        <fieldset class="form-group">
          <label>Nombre(s)</label><input class="form-control" type="text" />
        </fieldset>
        <fieldset class="form-group">
          <label>Por favor introduce tus apellidos.</label><input class="form-control" type="text" />
        </fieldset>
        <fieldset class="form-group">
          <label>Por favor introduce tu dirección de correo electrónico.</label><input class="form-control" type="email" />
        </fieldset>
        <fieldset class="form-group">
          <label>Por favor introduce eun número de teléfono válido. <a data-placement="top" data-toggle="tooltip" title="Por favor, seleccione una contrasela que tiene al menos 8 caracteres con al menos una letra mayúscula y un número."><span class="fa fa-question-circle"></span></a></label><input class="form-control" type="tel" />
        </fieldset>
        <fieldset class="form-group">
          <div class="checkbox">
            <label><input type="checkbox" />He leido y acepto las  <a href="#">Términos y Condiciones de Uso y Politica de Protección de Datos.</a></label>
          </div>
          <div class="checkbox">
            <label><input type="checkbox" />Suscribete al boletín de información </label>
          </div>
        </fieldset>
        <fieldset class="form-group">
          <a class="btn btn-primary pull-right">Registrarse <span class="fa fa-check-circle"></span></a>
        </fieldset>
      </form>
    </div>
    <div class="col-sm-6">
      <div class="highlight-bar table-for-vertical-alignment arrow-is-left">
        <div class="table-row">
          <div class="table-cell">
            Crea tu perfil  <strong>de forma gratuita</strong>
          </div>
        </div>
      </div>
      <p>
        <strong>Las ventajas</strong> de tu perfil  <strong>¡Regístrate sin compromiso!</strong>
      </p>
      <ul>
        <li>
          Date a conocer entre los pacientes que buscan un especialista médico como tú. <span class="fa fa-check icon icon-positive"></span>
        </li>
        <li>
          Recibe peticiones de pacientes que buscan tratamientos médicos como los que tu ofertas. <span class="fa fa-check icon icon-positive"></span>
        </li>
        <li>
          Incrementa la eficiencia en tus consultas dedicando menos tiempo al registro y gestión de citas. <span class="fa fa-check icon icon-positive"></span>
        </li>
        <li>
          Adquiere constantmente pacientes nuevos y llena espacios ocasionados por cambios o cancelaciones. <span class="fa fa-check icon icon-positive"></span>
        </li>
        <li>
          Atiende todas las solicitudes de tus pacientes y simplfica la comunicación sin atender el teléfono. <span class="fa fa-check icon icon-positive"></span>
        </li>
      </ul>
      <div class="sidebar-box">
        <h4>
          ¿Tienes alguna pregunta?
        </h4>
        <p>
          Contacta con nosotros 24 horas del día:
        </p>
        <p class="large-text">
          <a href="#"><span class="fa fa-phone"></span> <span>932201665</span></a>
        </p>
      </div>
    </div>
  </div>
</div>
