<div class="container">
  <div class="row">
    <div class="col-sm-6">
      <h2>
        Valora tu visita con este doctor
      </h2>
      <div class="row">
        <div class="col-sm-4">
          <img class="fullsize-image" src="/asset/000/001/872/Alija Kadic_4.jpg" />
        </div>
        <div class="col-sm-8">
          <p class="text-uppercase">
            DIRECCIÓN
          </p>
          <h3>
            Dr Bjöern Test
          </h3>
          <p>
            <strong>Clinica Dental Sonrisas</strong><br />Calle de Joaquín María López, 28 <br />28015 Madrid
          </p>
        </div>
        <div class="clearfix"></div>
        <div class="col-sm-10 m-t-3">
          <div class="row">
            <div class="col-xs-2 text-xs-center">
              <span class="fa fa-commenting fa-2x icon icon-decent"></span>
            </div>
            <div class="col-xs-10">
              <p>
                Valora de forma objetiva tu experiencia con este doctor. Tu opinión ayuda a otros pacientes a elegir adecuadamente.
              </p>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-2 text-xs-center">
              <span class="fa fa-lock fa-3x icon icon-decent"></span>
            </div>
            <div class="col-xs-10">
              <p>
                Tus datos serán utilizados de acuerdo a lo establecido en nuestra  <a href="#">Política de privacidad de datos</a>.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-6">
      <h2>
        Tu experiencia
      </h2>
      <form class="sidebar-box colored">
        <fieldset class="form-group">
          <label>Tu nombre</label><input class="form-control" type="text" />
        </fieldset>
        <fieldset class="form-group">
          <label>Describe tu experiencia</label><textarea class="form-control" cols="50" placeholder="Por favor describe tu experiencia con este doctor. ¿Qué te ha parecido el servicio? ¿Qué te pareció la atención médica que recibiste? ¿Fue largo el tiempo de espera? ¿Ha sido fácil encontrar la consulta médica?" rows="5"></textarea>
        </fieldset>
        <fieldset class="form-group">
          <label>Conducta del doctor</label>
          <p>
            <em>Por favor valora la conducta y comportamiento del doctor.</em>
          </p>
          <div class="rating-star editable">
            <input id="radio-3-0" name="radio-3" type="radio" /><label for="radio-3-0">i</label>
            <input id="radio-3-1" name="radio-3" type="radio" /><label for="radio-3-1">i</label>
            <input id="radio-3-2" name="radio-3" type="radio" /><label for="radio-3-2">i</label>
            <input id="radio-3-3" name="radio-3" type="radio" /><label for="radio-3-3">i</label>
            <input id="radio-3-4" name="radio-3" type="radio" /><label for="radio-3-4">i</label>
          </div>
          <label>Puntualidad</label>
          <p>
            <em>Por favor valora la puntualidad de tu cita.</em>
          </p>
          <div class="rating-star editable">
            <input id="radio-4-0" name="radio-4" type="radio" /><label for="radio-4-0">i</label>
            <input id="radio-4-1" name="radio-4" type="radio" /><label for="radio-4-1">i</label>
            <input id="radio-4-2" name="radio-4" type="radio" /><label for="radio-4-2">i</label>
            <input id="radio-4-3" name="radio-4" type="radio" /><label for="radio-4-3">i</label>
            <input id="radio-4-4" name="radio-4" type="radio" /><label for="radio-4-4">i</label>
          </div>
          <label>Valoración general</label>
          <p>
            <em>Evalúa en términos generales tu visita con este doctor.</em>
          </p>
          <div class="rating-star editable">
            <input id="radio-5-0" name="radio-5" type="radio" /><label for="radio-5-0">i</label>
            <input id="radio-5-1" name="radio-5" type="radio" /><label for="radio-5-1">i</label>
            <input id="radio-5-2" name="radio-5" type="radio" /><label for="radio-5-2">i</label>
            <input id="radio-5-3" name="radio-5" type="radio" /><label for="radio-5-3">i</label>
            <input id="radio-5-4" name="radio-5" type="radio" /><label for="radio-5-4">i</label>
          </div>
        </fieldset>
        <fieldset class="form-group">
          <div class="checkbox">
            <label><input type="checkbox" />Mi opinión puede ser publicada de forma anónima en sitios con los que doctorio colabore. </label>
          </div>
        </fieldset>
        <fieldset class="form-group">
          <a class="btn btn-primary pull-right">Pedir Cita <span class="fa fa-check-circle"></span></a>
        </fieldset>
      </form>
    </div>
  </div>
</div>
