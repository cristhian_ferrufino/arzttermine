<div class="container">
  <div class="row">
    <div class="col-sm-6">
      <h1>
        Acceso Profesionales Médicos
      </h1>
    </div>
    <div class="col-sm-6 m-b-1 text-right text-right-except-mobile">
      <span>¿Todavía no tienes cuenta con nosotros?</span> <a href="#">Regístrate aquí.</a>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6 m-b-1">
      <form class="sidebar-box colored">
        <fieldset class="form-group">
          <label>Correo electrónico</label><input class="form-control" type="email" />
        </fieldset>
        <fieldset class="form-group">
          <label>Contraseña</label><input class="form-control" type="password" /><small><a href="#">He olvidado mi contraseña</a></small>
        </fieldset>
        <fieldset class="form-group">
          <a class="btn btn-primary pull-right">Iniciar sesión <span class="fa fa-check-chevron-right"></span></a>
        </fieldset>
      </form>
    </div>
    <div class="col-sm-6 m-b-1">
      <div class="sidebar-box">
        <h4>
          ¿Tienes alguna pregunta?
        </h4>
        <p>
          Ponte en contacto con nosotros
        </p>
        <p class="large-text">
          <a href="#"><span class="fa fa-phone"></span> <span>932201665</span></a>
        </p>
      </div>
    </div>
  </div>
</div>
