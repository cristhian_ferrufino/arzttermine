<div class="home-banner">
  <div class="container">
    <div class="row content">
      <div class="col-xs-12 col-lg-6">
        <h1>
          Reserva ahora una cita médica
        </h1>
        <ul class="benefit-list list-unstyled hidden-sm-down">
          <li>
            Cerca de tí
          </li>
          <li>
            El día y la hora que mejor te venga
          </li>
          <li>
            Fácil y gratis
          </li>
        </ul>
      </div>
      <div class="col-xs-12" style="position: static">
        <form class="search-form is-large">
          <fieldset class="form-group m-b-0">
            <div class="col-md-5">
              <label class="form-control-label" for="specialization">Especialista Médico</label><select class="form-control" id="specialization">
                <option>
                  Por facor selecciona
                </option>
                <option>
                  Geriatra
                </option>
                <option>
                  Endocrino
                </option>
              </select>
            </div>
            <div class="col-md-4">
              <label class="form-control-label" for="location">Ciudad o Código Postal</label><input class="form-control" id="location" placeholder="ej. Madrid o 28080" type="text" />
            </div>
            <div class="col-md-3">
              <button class="btn btn-secondary btn-block text-uppercase" type="submit">Buscar cita previa</button>
            </div>
          </fieldset>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="container">
  <div class="row content">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-md-7 col-lg-6 p-t-1">
          <hr class="vertical-space only-a-bit hidden-md-down" />
          <h2 class="is-prominent">
            Your benefits with doctorio.es
          </h2>
          <ul class="list-unstyled benefit-list-with-icons">
            <li>
              Time saving in appointment booking
            </li>
            <li>
              Less no shows
            </li>
            <li>
              Be found through Google
            </li>
          </ul>
          <a class="btn btn-secondary text-uppercase has-larger-padding" href="#">Join</a>
        </div>
        <div class="col-md-5 col-lg-6">
          <img class="fullsize-image m-t-3" src="/images/page/homepage/preview-on-devices.png" width="" />
        </div>
      </div>
    </div>
  </div>
</div>
<div class="testimonial-card m-t-1 p-t-3">
  <div class="container">
    <div class="row content">
      <div class="col-sm-6">
        <div class="testimonial">
          <div class="row">
            <div class="col-xs-12 col-lg-4 text-xs-center">
              <div class="author-image"></div>
              <div class="author-name">
                Jillian-Peter N.
              </div>
              <div class="author-location">
                Mönchengladbach
              </div>
            </div>
            <div class="col-xs-12 col-lg-8">
              <blockquote>I use doctorio to book all of my appointments. The app just makes it so easy.</blockquote>
              <div class="stars">
                ★★★★★
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6 copytext">
        <h2 class="is-prominent">
          Lorem ipsum Consectetur incididunt magna.
        </h2>
        <p>
          Lorem ipsum Duis sint nulla Excepteur ut magna laboris laboris incididunt. Lorem ipsum Eiusmod veniam ullamco quis eu quis qui ex fugiat cillum fugiat non velit. Lorem ipsum Incididunt veniam nostrud aliqua dolore ullamco.
        </p>
        <a class="btn btn-secondary text-uppercase has-larger-padding">Book now!</a>
      </div>
    </div>
  </div>
</div>
