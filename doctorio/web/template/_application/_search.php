<div class="page-search">
  <div class="map" data-name="collapsible-map">
    <h2 style="border: 0">
      – Map –
    </h2>
  </div>
  <div class="container-fluid results">
    <div class="row">
      <div class="col-md-8 col-md-offset-4">
        <div class="results">
          <form class="search-form on-light-background m-b-3">
            <fieldset class="form-group m-b-0">
              <div class="col-md-6 col-lg-4">
                <label class="form-control-label" for="specialization">Especialista Médico</label><select class="form-control" id="specialization">
                  <option>
                    Por facor selecciona
                  </option>
                  <option>
                    Geriatra
                  </option>
                  <option>
                    Endocrino
                  </option>
                </select>
              </div>
              <div class="col-md-6 col-lg-4">
                <label class="form-control-label" for="location">Ciudad o Código Postal</label><input class="form-control" id="location" placeholder="ej. Madrid o 28080" type="text" />
              </div>
              <div class="col-md-3 col-lg-4 hidden-md-down">
                <label class="form-control-label invisible">Buscar </label><button class="btn btn-secondary btn-block" type="submit">Ordenar resultados</button>
              </div>
            </fieldset>
            <fieldset class="form-group m-b-0 search-filter collapse" data-name="search-filter">
              <div class="inner">
                <div class="col-md-6 col-lg-5">
                  <label class="form-control-label" for="distance">Máxima distancia de búsqueda</label><select class="form-control" id="distance">
                    <option>
                      5 km
                    </option>
                    <option>
                      10 km
                    </option>
                    <option>
                      50 km
                    </option>
                  </select>
                </div>
                <div class="col-md-6 col-lg-4">
                  <label class="form-control-label" for="treatment">Tipo de tratamiento</label><select class="form-control" id="treatment">
                    <option>
                      Visitas sucesivas
                    </option>
                    <option>
                      Ortodoncia
                    </option>
                    <option>
                      Implantología
                    </option>
                    <option>
                      Odontología
                    </option>
                    <option>
                      Periodoncia
                    </option>
                    <option>
                      Endodoncia
                    </option>
                    <option>
                      Odontopediatría
                    </option>
                    <option>
                      Cirugía Oral y Maxilofacial
                    </option>
                    <option>
                      Prótesis Dental (Fija
                    </option>
                    <option>
                      Prótesis Dental (Removible
                    </option>
                    <option>
                      Sedación
                    </option>
                    <option>
                      Estética Dental
                    </option>
                    <option>
                      ATM (articulación temporomandibular)
                    </option>
                  </select>
                </div>
              </div>
            </fieldset>
            <fieldset class="form-group search-filter-toggle">
              <div class="col-sm-12">
                <a data-target="[data-name=search-filter]" data-toggle="collapse" href="#">Mostrar Más Filtros</a><button class="btn btn-secondary btn-block hidden-lg-up m-t" type="submit">Ordenar resultados</button>
              </div>
            </fieldset>
          </form>
          <div class="text-xs-center hidden-md-up">
            <a class="btn btn-primary-outline btn-block collapse-map-button m-b-1" data-behavior="collapse-map" data-target="[data-name=collapsible-map]"><i class="fa fa-map-o"></i><span class="enable-map">Ver mapa</span><span class="disable-map">Ocultar mapa</span></a>
          </div>
          <div class="result-list">
            <div class="headline">
              <div class="headline-text">
                <strong>803 Dentistas</strong> en  <strong>Madrid</strong> tienen citas para ti.
              </div>
              <div class="buttons btn-group">
                <a class="btn btn-primary-outline btn-sm">back</a><a class="btn btn-primary-outline btn-sm">más profesionales</a>
              </div>
            </div>
            <div class="result m-t-2">
              <div class="row">
                <div class="col-sm-9">
                  <span class="icon icon-map"><span class="number">0</span></span>
                  <div>
                    <h3 class="m-b-0 m-r-1">
                      Dr. Diego Heras Rodriguez
                    </h3>
                    <div class="rating-star">
                      <div class="stars">
                        <div style="width: 10%"></div>
                      </div>
                    </div>
                    <p class="m-b-0">
                      <small>0,3km | C./ Atocha 5 28012 Madrid</small>
                    </p>
                  </div>
                </div>
                <div class="col-sm-3">
                  <a class="pull-right-sm-up" href="tel:+49932201665"><span class="fa fa-phone"></span> 932201665 </a>
                </div>
              </div>
              <div class="profile-picture-and-calendar m-t-1">
                <div class="picture" style="background-image: url(&#39;/asset/000/001/858/Fotolia_38181170_XS_4.jpg&#39;);"></div>
                <div class="calendar-wrapper">
                  <div class="calendar">
                    – Calendar –
                  </div>
                </div>
              </div>
            </div>
            <div class="result m-t-2">
              <div class="row">
                <div class="col-sm-9">
                  <span class="icon icon-map"><span class="number">1</span></span>
                  <div>
                    <h3 class="m-b-0 m-r-1">
                      Dr. Diego Heras Rodriguez
                    </h3>
                    <div class="rating-star">
                      <div class="stars">
                        <div style="width: 10%"></div>
                      </div>
                    </div>
                    <p class="m-b-0">
                      <small>0,3km | C./ Atocha 5 28012 Madrid</small>
                    </p>
                  </div>
                </div>
                <div class="col-sm-3">
                  <a class="pull-right-sm-up" href="tel:+49932201665"><span class="fa fa-phone"></span> 932201665 </a>
                </div>
              </div>
              <div class="profile-picture-and-calendar m-t-1">
                <div class="picture" style="background-image: url(&#39;/asset/000/001/858/Fotolia_38181170_XS_4.jpg&#39;);"></div>
                <div class="calendar-wrapper">
                  <div class="calendar">
                    – Calendar –
                  </div>
                </div>
              </div>
            </div>
            <div class="result m-t-2">
              <div class="row">
                <div class="col-sm-9">
                  <span class="icon icon-map"><span class="number">2</span></span>
                  <div>
                    <h3 class="m-b-0 m-r-1">
                      Dr. Diego Heras Rodriguez
                    </h3>
                    <div class="rating-star">
                      <div class="stars">
                        <div style="width: 10%"></div>
                      </div>
                    </div>
                    <p class="m-b-0">
                      <small>0,3km | C./ Atocha 5 28012 Madrid</small>
                    </p>
                  </div>
                </div>
                <div class="col-sm-3">
                  <a class="pull-right-sm-up" href="tel:+49932201665"><span class="fa fa-phone"></span> 932201665 </a>
                </div>
              </div>
              <div class="profile-picture-and-calendar m-t-1">
                <div class="picture" style="background-image: url(&#39;/asset/000/001/858/Fotolia_38181170_XS_4.jpg&#39;);"></div>
                <div class="calendar-wrapper">
                  <div class="calendar">
                    – Calendar –
                  </div>
                </div>
              </div>
            </div>
            <div class="result m-t-2">
              <div class="row">
                <div class="col-sm-9">
                  <span class="icon icon-map"><span class="number">3</span></span>
                  <div>
                    <h3 class="m-b-0 m-r-1">
                      Dr. Diego Heras Rodriguez
                    </h3>
                    <div class="rating-star">
                      <div class="stars">
                        <div style="width: 10%"></div>
                      </div>
                    </div>
                    <p class="m-b-0">
                      <small>0,3km | C./ Atocha 5 28012 Madrid</small>
                    </p>
                  </div>
                </div>
                <div class="col-sm-3">
                  <a class="pull-right-sm-up" href="tel:+49932201665"><span class="fa fa-phone"></span> 932201665 </a>
                </div>
              </div>
              <div class="profile-picture-and-calendar m-t-1">
                <div class="picture" style="background-image: url(&#39;/asset/000/001/858/Fotolia_38181170_XS_4.jpg&#39;);"></div>
                <div class="calendar-wrapper">
                  <div class="calendar">
                    – Calendar –
                  </div>
                </div>
              </div>
            </div>
            <div class="result m-t-2">
              <div class="row">
                <div class="col-sm-9">
                  <span class="icon icon-map"><span class="number">4</span></span>
                  <div>
                    <h3 class="m-b-0 m-r-1">
                      Dr. Diego Heras Rodriguez
                    </h3>
                    <div class="rating-star">
                      <div class="stars">
                        <div style="width: 10%"></div>
                      </div>
                    </div>
                    <p class="m-b-0">
                      <small>0,3km | C./ Atocha 5 28012 Madrid</small>
                    </p>
                  </div>
                </div>
                <div class="col-sm-3">
                  <a class="pull-right-sm-up" href="tel:+49932201665"><span class="fa fa-phone"></span> 932201665 </a>
                </div>
              </div>
              <div class="profile-picture-and-calendar m-t-1">
                <div class="picture" style="background-image: url(&#39;/asset/000/001/858/Fotolia_38181170_XS_4.jpg&#39;);"></div>
                <div class="calendar-wrapper">
                  <div class="calendar">
                    – Calendar –
                  </div>
                </div>
              </div>
            </div>
            <div class="result m-t-2">
              <div class="row">
                <div class="col-sm-9">
                  <span class="icon icon-map"><span class="number">5</span></span>
                  <div>
                    <h3 class="m-b-0 m-r-1">
                      Dr. Diego Heras Rodriguez
                    </h3>
                    <div class="rating-star">
                      <div class="stars">
                        <div style="width: 10%"></div>
                      </div>
                    </div>
                    <p class="m-b-0">
                      <small>0,3km | C./ Atocha 5 28012 Madrid</small>
                    </p>
                  </div>
                </div>
                <div class="col-sm-3">
                  <a class="pull-right-sm-up" href="tel:+49932201665"><span class="fa fa-phone"></span> 932201665 </a>
                </div>
              </div>
              <div class="profile-picture-and-calendar m-t-1">
                <div class="picture" style="background-image: url(&#39;/asset/000/001/858/Fotolia_38181170_XS_4.jpg&#39;);"></div>
                <div class="calendar-wrapper">
                  <div class="calendar">
                    – Calendar –
                  </div>
                </div>
              </div>
            </div>
            <div class="result m-t-2">
              <div class="row">
                <div class="col-sm-9">
                  <span class="icon icon-map"><span class="number">6</span></span>
                  <div>
                    <h3 class="m-b-0 m-r-1">
                      Dr. Diego Heras Rodriguez
                    </h3>
                    <div class="rating-star">
                      <div class="stars">
                        <div style="width: 10%"></div>
                      </div>
                    </div>
                    <p class="m-b-0">
                      <small>0,3km | C./ Atocha 5 28012 Madrid</small>
                    </p>
                  </div>
                </div>
                <div class="col-sm-3">
                  <a class="pull-right-sm-up" href="tel:+49932201665"><span class="fa fa-phone"></span> 932201665 </a>
                </div>
              </div>
              <div class="profile-picture-and-calendar m-t-1">
                <div class="picture" style="background-image: url(&#39;/asset/000/001/858/Fotolia_38181170_XS_4.jpg&#39;);"></div>
                <div class="calendar-wrapper">
                  <div class="calendar">
                    – Calendar –
                  </div>
                </div>
              </div>
            </div>
            <div class="footer">
              <div class="btn-group pull-right pull-right-sm-up">
                <a class="btn btn-primary-outline btn-sm">back</a><a class="btn btn-primary-outline btn-sm">más profesionales</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
