<div class="container">
  <div class="row">
    <div class="col-sm-12">
      <div class="row m-b">
        <div class="col-sm-2">
          <img class="fullsize-image" src="/asset/000/001/872/Alija Kadic_4.jpg" />
        </div>
        <div class="col-sm-6 m-b">
          <div class="profile-header">
            <h1>
              Dr. Rómano Cieslikowicz
            </h1>
            <div class="phone">
              <a href="tel:+49932201665"><span class="fa fa-phone"></span> <span>932201665</span></a>
            </div>
          </div>
          <table class="table-top-aligned-with-nonbreaking-labels no-table-when-mobile">
            <tr>
              <td>
                <h4>
                  Especialidades:
                </h4>
              </td>
              <td class="p-l-1">
                Dentista, Urólogo, Cardiólogo, Dentista, Urólogo, Cardiólogo, Dentista, Urólogo, Cardiólogo, Dentista, Urólogo, Cardiólogo, Dentista, Urólogo, Cardiólogo, Dentista, Urólogo, Cardiólogo, Dentista, Urólogo, Cardiólogo
              </td>
            </tr>
            <tr>
              <td>
                <h4>
                  Valoración general
                </h4>
              </td>
              <td class="rating-star p-l-1">
                <div class="stars m-l-0">
                  <div style="width: 10%"></div>
                </div>
              </td>
            </tr>
            <tr>
              <td>
                <h4>
                  Valoración total
                </h4>
              </td>
              <td class="rating-star p-l-1">
                <div class="stars m-l-0">
                  <div style="width: 33%"></div>
                </div>
              </td>
            </tr>
            <tr>
              <td>
                <h4>
                  Valoración total
                </h4>
              </td>
              <td class="rating-star p-l-1">
                <div class="stars m-l-0">
                  <div style="width: 78%"></div>
                </div>
              </td>
            </tr>
          </table>
          <div class="m-t-1">
            <a class="btn btn-primary btn-sm">Rate this doctor</a>
          </div>
        </div>
        <div class="col-sm-4">
          <img class="fullsize-image" src="http://maps.googleapis.com/maps/api/staticmap?center=almargo&amp;zoom=17&amp;scale=false&amp;size=600x600&amp;maptype=roadmap&amp;format=png&amp;visual_refresh=true&amp;markers=size:mid%7Ccolor:0xff0000%7Clabel:1%7Calmargo" />
        </div>
      </div>
      <h2>
        Elige la cita que mejor te acomode
      </h2>
      <div class="row">
        <div class="col-sm-9">
          <span class="icon icon-map"><span class="number">1</span></span>
          <div>
            <h3 class="m-b-0 m-r-1">
              <a href="#">SDS Centro Dental</a>
            </h3>
            <p class="m-b-0">
              <small><span class="address">Calle Rafael Calvo, 0815, 28010 Madrid Saint Raphaêl </span></small>
            </p>
          </div>
        </div>
        <div class="col-sm-3">
          <a class="pull-right-sm-up" href="#"><span class="fa fa-phone"></span> <span>932201665</span></a>
        </div>
      </div>
      <div class="profile-picture-and-calendar m-t-1 m-b-2">
        <div class="picture" style="background-image: url(&#39;/images/application/practice.png&#39;);"></div>
        <div class="calendar-wrapper">
          <div class="calendar m-y-0">
            – Calendar –
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-9">
          <span class="icon icon-map"><span class="number">1</span></span>
          <div>
            <h3 class="m-b-0 m-r-1">
              <a href="#">SDS Centro Dental</a>
            </h3>
            <p class="m-b-0">
              <small><span class="address">Calle Rafael Calvo, 0815, 28010 Madrid Saint Raphaêl </span></small>
            </p>
          </div>
        </div>
        <div class="col-sm-3">
          <a class="pull-right-sm-up" href="#"><span class="fa fa-phone"></span> <span>932201665</span></a>
        </div>
      </div>
      <div class="profile-picture-and-calendar m-t-1 m-b-2">
        <div class="picture" style="background-image: url(&#39;/images/application/practice.png&#39;);"></div>
        <div class="calendar-wrapper">
          <div class="calendar m-y-0">
            – Calendar –
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-9">
          <span class="icon icon-map"><span class="number">1</span></span>
          <div>
            <h3 class="m-b-0 m-r-1">
              <a href="#">SDS Centro Dental</a>
            </h3>
            <p class="m-b-0">
              <small><span class="address">Calle Rafael Calvo, 0815, 28010 Madrid Saint Raphaêl </span></small>
            </p>
          </div>
        </div>
        <div class="col-sm-3">
          <a class="pull-right-sm-up" href="#"><span class="fa fa-phone"></span> <span>932201665</span></a>
        </div>
      </div>
      <div class="profile-picture-and-calendar m-t-1 m-b-2">
        <div class="picture" style="background-image: url(&#39;/images/application/practice.png&#39;);"></div>
        <div class="calendar-wrapper">
          <div class="calendar m-y-0">
            – Calendar –
          </div>
        </div>
      </div>
      <h2>
        Información adicional del médico
      </h2>
      <div class="row m-y-2">
        <div class="col-sm-3">
          <h5>
            <em>Prices</em>
          </h5>
        </div>
        <div class="col-sm-9">
          <table class="table table-with-background">
            <tr>
              <td>
                Treatment 0
              </td>
              <td class="text-right">
                Free of charge
              </td>
            </tr>
            <tr>
              <td>
                Treatment 0
              </td>
              <td class="text-right">
                <a href="#">Ask for the price</a>
              </td>
            </tr>
            <tr>
              <td>
                Treatment 1
              </td>
              <td class="text-right">
                Free of charge
              </td>
            </tr>
            <tr>
              <td>
                Treatment 1
              </td>
              <td class="text-right">
                <a href="#">Ask for the price</a>
              </td>
            </tr>
            <tr>
              <td>
                Treatment 2
              </td>
              <td class="text-right">
                Free of charge
              </td>
            </tr>
            <tr>
              <td>
                Treatment 2
              </td>
              <td class="text-right">
                <a href="#">Ask for the price</a>
              </td>
            </tr>
            <tr>
              <td>
                Treatment 3
              </td>
              <td class="text-right">
                Free of charge
              </td>
            </tr>
            <tr>
              <td>
                Treatment 3
              </td>
              <td class="text-right">
                <a href="#">Ask for the price</a>
              </td>
            </tr>
          </table>
        </div>
      </div>
      <div class="row m-y-2">
        <div class="col-sm-3">
          <h5>
            <em>Philosophy</em>
          </h5>
        </div>
        <div class="col-sm-9">
          <p>
            Lorem ipsum In consequat irure aliquip est dolor eiusmod eiusmod in aliquip. Lorem ipsum Excepteur ad adipisicing quis nulla elit et Ut ut. Lorem ipsum Duis pariatur qui aliqua id in cupidatat irure cupidatat tempor fugiat adipisicing elit.
          </p>
        </div>
      </div>
      <div class="row m-y-2">
        <div class="col-sm-3">
          <h5>
            <em>Treatments</em>
          </h5>
        </div>
        <div class="col-sm-9">
          <p>
            Lorem ipsum In consequat irure aliquip est dolor eiusmod eiusmod in aliquip. Lorem ipsum Excepteur ad adipisicing quis nulla elit et Ut ut. Lorem ipsum Duis pariatur qui aliqua id in cupidatat irure cupidatat tempor fugiat adipisicing elit.
          </p>
        </div>
      </div>
      <div class="row m-y-2">
        <div class="col-sm-3">
          <h5>
            <em>Languages</em>
          </h5>
        </div>
        <div class="col-sm-9">
          <p>
            Lorem ipsum In consequat irure aliquip est dolor eiusmod eiusmod in aliquip. Lorem ipsum Excepteur ad adipisicing quis nulla elit et Ut ut. Lorem ipsum Duis pariatur qui aliqua id in cupidatat irure cupidatat tempor fugiat adipisicing elit.
          </p>
        </div>
      </div>
      <div class="row m-y-2">
        <div class="col-sm-3">
          <h5>
            <em>Ratings</em>
          </h5>
        </div>
        <div class="col-sm-9">
          <div class="rating">
            <div class="rating-sender">
              Fr, 14.8.2015 von Huang, Juei-min (Verifizierter Patient) 
            </div>
            <div class="rating-content">
              <div class="rating-star is-inline multiple-ratings-inline">
                <h4 class="rating-name">
                  Conducta del doctor
                </h4>
                <div class="stars">
                  <div style="width: 10%"></div>
                </div>
              </div>
              <div class="rating-star is-inline multiple-ratings-inline">
                <h4 class="rating-name">
                  Puntualidad
                </h4>
                <div class="stars">
                  <div style="width: 43%"></div>
                </div>
              </div>
              <div class="rating-star is-inline multiple-ratings-inline">
                <h4 class="rating-name">
                  Valoración general
                </h4>
                <div class="stars">
                  <div style="width: 85%"></div>
                </div>
              </div>
              <blockquote>
                <p>
                  Lorem ipsum Deserunt adipisicing aliquip Ut irure qui adipisicing consequat qui. Lorem ipsum Aliqua sunt ad dolore minim esse dolor. 
                </p>
              </blockquote>
              <div class="rating-comment">
                <blockquote>
                  <p>
                    Lorem ipsum Aute aliquip id aute ex amet labore consectetur in. Lorem ipsum Aliqua minim ex adipisicing cupidatat eiusmod laborum veniam sint magna anim. Lorem ipsum Ea ad ea in aliqua tempor reprehenderit labore sit qui labore irure in.
                  </p>
                </blockquote>
                <p class="quote-sender">
                  Dr. Diego Heras Rodriguez &bull; Mo 8. April 2015
                </p>
              </div>
            </div>
            <div class="rating-sender">
              Fr, 14.8.2015 von Huang, Juei-min (Verifizierter Patient) 
            </div>
            <div class="rating-content">
              <div class="rating-star is-inline multiple-ratings-inline">
                <h4 class="rating-name">
                  Conducta del doctor
                </h4>
                <div class="stars">
                  <div style="width: 10%"></div>
                </div>
              </div>
              <div class="rating-star is-inline multiple-ratings-inline">
                <h4 class="rating-name">
                  Puntualidad
                </h4>
                <div class="stars">
                  <div style="width: 43%"></div>
                </div>
              </div>
              <div class="rating-star is-inline multiple-ratings-inline">
                <h4 class="rating-name">
                  Valoración general
                </h4>
                <div class="stars">
                  <div style="width: 85%"></div>
                </div>
              </div>
              <blockquote>
                <p>
                  Lorem ipsum Deserunt adipisicing aliquip Ut irure qui adipisicing consequat qui. Lorem ipsum Aliqua sunt ad dolore minim esse dolor. 
                </p>
              </blockquote>
            </div>
            <div class="rating-sender">
              Fr, 14.8.2015 von Huang, Juei-min (Verifizierter Patient) 
            </div>
            <div class="rating-content">
              <div class="rating-star is-inline multiple-ratings-inline">
                <h4 class="rating-name">
                  Conducta del doctor
                </h4>
                <div class="stars">
                  <div style="width: 10%"></div>
                </div>
              </div>
              <div class="rating-star is-inline multiple-ratings-inline">
                <h4 class="rating-name">
                  Puntualidad
                </h4>
                <div class="stars">
                  <div style="width: 43%"></div>
                </div>
              </div>
              <div class="rating-star is-inline multiple-ratings-inline">
                <h4 class="rating-name">
                  Valoración general
                </h4>
                <div class="stars">
                  <div style="width: 85%"></div>
                </div>
              </div>
              <blockquote>
                <p>
                  Lorem ipsum Deserunt adipisicing aliquip Ut irure qui adipisicing consequat qui. Lorem ipsum Aliqua sunt ad dolore minim esse dolor. 
                </p>
              </blockquote>
              <div class="rating-comment">
                <blockquote>
                  <p>
                    Lorem ipsum Aute aliquip id aute ex amet labore consectetur in. Lorem ipsum Aliqua minim ex adipisicing cupidatat eiusmod laborum veniam sint magna anim. Lorem ipsum Ea ad ea in aliqua tempor reprehenderit labore sit qui labore irure in.
                  </p>
                </blockquote>
                <p class="quote-sender">
                  Dr. Diego Heras Rodriguez &bull; Mo 8. April 2015
                </p>
              </div>
            </div>
            <div class="rating-sender">
              Fr, 14.8.2015 von Huang, Juei-min (Verifizierter Patient) 
            </div>
            <div class="rating-content">
              <div class="rating-star is-inline multiple-ratings-inline">
                <h4 class="rating-name">
                  Conducta del doctor
                </h4>
                <div class="stars">
                  <div style="width: 10%"></div>
                </div>
              </div>
              <div class="rating-star is-inline multiple-ratings-inline">
                <h4 class="rating-name">
                  Puntualidad
                </h4>
                <div class="stars">
                  <div style="width: 43%"></div>
                </div>
              </div>
              <div class="rating-star is-inline multiple-ratings-inline">
                <h4 class="rating-name">
                  Valoración general
                </h4>
                <div class="stars">
                  <div style="width: 85%"></div>
                </div>
              </div>
              <blockquote>
                <p>
                  Lorem ipsum Deserunt adipisicing aliquip Ut irure qui adipisicing consequat qui. Lorem ipsum Aliqua sunt ad dolore minim esse dolor. 
                </p>
              </blockquote>
            </div>
            <div class="rating-sender">
              Fr, 14.8.2015 von Huang, Juei-min (Verifizierter Patient) 
            </div>
            <div class="rating-content">
              <div class="rating-star is-inline multiple-ratings-inline">
                <h4 class="rating-name">
                  Conducta del doctor
                </h4>
                <div class="stars">
                  <div style="width: 10%"></div>
                </div>
              </div>
              <div class="rating-star is-inline multiple-ratings-inline">
                <h4 class="rating-name">
                  Puntualidad
                </h4>
                <div class="stars">
                  <div style="width: 43%"></div>
                </div>
              </div>
              <div class="rating-star is-inline multiple-ratings-inline">
                <h4 class="rating-name">
                  Valoración general
                </h4>
                <div class="stars">
                  <div style="width: 85%"></div>
                </div>
              </div>
              <blockquote>
                <p>
                  Lorem ipsum Deserunt adipisicing aliquip Ut irure qui adipisicing consequat qui. Lorem ipsum Aliqua sunt ad dolore minim esse dolor. 
                </p>
              </blockquote>
              <div class="rating-comment">
                <blockquote>
                  <p>
                    Lorem ipsum Aute aliquip id aute ex amet labore consectetur in. Lorem ipsum Aliqua minim ex adipisicing cupidatat eiusmod laborum veniam sint magna anim. Lorem ipsum Ea ad ea in aliqua tempor reprehenderit labore sit qui labore irure in.
                  </p>
                </blockquote>
                <p class="quote-sender">
                  Dr. Diego Heras Rodriguez &bull; Mo 8. April 2015
                </p>
              </div>
            </div>
            <div class="rating-sender">
              Fr, 14.8.2015 von Huang, Juei-min (Verifizierter Patient) 
            </div>
            <div class="rating-content">
              <div class="rating-star is-inline multiple-ratings-inline">
                <h4 class="rating-name">
                  Conducta del doctor
                </h4>
                <div class="stars">
                  <div style="width: 10%"></div>
                </div>
              </div>
              <div class="rating-star is-inline multiple-ratings-inline">
                <h4 class="rating-name">
                  Puntualidad
                </h4>
                <div class="stars">
                  <div style="width: 43%"></div>
                </div>
              </div>
              <div class="rating-star is-inline multiple-ratings-inline">
                <h4 class="rating-name">
                  Valoración general
                </h4>
                <div class="stars">
                  <div style="width: 85%"></div>
                </div>
              </div>
              <blockquote>
                <p>
                  Lorem ipsum Deserunt adipisicing aliquip Ut irure qui adipisicing consequat qui. Lorem ipsum Aliqua sunt ad dolore minim esse dolor. 
                </p>
              </blockquote>
            </div>
            <div class="rating-sender">
              Fr, 14.8.2015 von Huang, Juei-min (Verifizierter Patient) 
            </div>
            <div class="rating-content">
              <div class="rating-star is-inline multiple-ratings-inline">
                <h4 class="rating-name">
                  Conducta del doctor
                </h4>
                <div class="stars">
                  <div style="width: 10%"></div>
                </div>
              </div>
              <div class="rating-star is-inline multiple-ratings-inline">
                <h4 class="rating-name">
                  Puntualidad
                </h4>
                <div class="stars">
                  <div style="width: 43%"></div>
                </div>
              </div>
              <div class="rating-star is-inline multiple-ratings-inline">
                <h4 class="rating-name">
                  Valoración general
                </h4>
                <div class="stars">
                  <div style="width: 85%"></div>
                </div>
              </div>
              <blockquote>
                <p>
                  Lorem ipsum Deserunt adipisicing aliquip Ut irure qui adipisicing consequat qui. Lorem ipsum Aliqua sunt ad dolore minim esse dolor. 
                </p>
              </blockquote>
              <div class="rating-comment">
                <blockquote>
                  <p>
                    Lorem ipsum Aute aliquip id aute ex amet labore consectetur in. Lorem ipsum Aliqua minim ex adipisicing cupidatat eiusmod laborum veniam sint magna anim. Lorem ipsum Ea ad ea in aliqua tempor reprehenderit labore sit qui labore irure in.
                  </p>
                </blockquote>
                <p class="quote-sender">
                  Dr. Diego Heras Rodriguez &bull; Mo 8. April 2015
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
