CHANGELOG
=========

This changelog references the relevant changes (bugs and features) done.

To get the diff for a specific change, go to https://github.com/arzttermine/arzttermine/commit/XXX where XXX is the change hash
To get the diff between two versions, go to https://github.com/arzttermine/arzttermine/compare/v1.1.0...v1.2.0
* [Unreleased] - 2016-06-?? ([full changelog](https://github.com/arzttermine/arzttermine/compare/1.8.0...master))
  * feature [#20](https://github.com/arzttermine/arzttermine/issues/20) - [admin] refactor admin interface
  * feature [#6](https://github.com/arzttermine/arzttermine/issues/6) - [admin] use translator for messages/labels in the admin interface
  * feature [#385](https://github.com/arzttermine/arzttermine/issues/385) - [calendar settings] add a tour which explains our settings section
  * feature [#425](https://github.com/arzttermine/arzttermine/issues/425) - [database] repair bad data
  * feature [#155](https://github.com/arzttermine/arzttermine/issues/155) - [users] migrate asset handling
  * bug [#409](https://github.com/arzttermine/arzttermine/issues/409) - [admin dashboard] optimize "Users without locations"-card
  * bug [#412](https://github.com/arzttermine/arzttermine/issues/412) - [admin] doctors editor validation is broken
  * bug [#365](https://github.com/arzttermine/arzttermine/issues/365) - [admin dashboard] "Events created by user"-card only shows one doctor per day
* [1.8.0](https://github.com/arzttermine/arzttermine/tree/1.8.0) - 2016-05-26 ([full changelog](https://github.com/arzttermine/arzttermine/compare/1.7.0...1.8.0))
  * feature [#415](https://github.com/arzttermine/arzttermine/issues/415) - [calendar] hours format
  * feature [#385](https://github.com/arzttermine/arzttermine/issues/385) - [calendar settings] add a tour which explains our settings section 
  * feature [#405](https://github.com/arzttermine/arzttermine/issues/405) - [doctor registration] set doctor status to STATUS_VISIBLE
  * feature [#408](https://github.com/arzttermine/arzttermine/issues/408) - [admin search] add some columns to search result
  * bug [#410](https://github.com/arzttermine/arzttermine/issues/410) - [admin dashboard] optimize "Doctors without Users"-card
  * bug [#409](https://github.com/arzttermine/arzttermine/issues/409) - [admin dashboard] optimize "Users without locations"-card
  * bug [#420](https://github.com/arzttermine/arzttermine/issues/420) - [admin] link doc <-> location
* [1.7.0](https://github.com/arzttermine/arzttermine/tree/1.7.0) - 2016-05-18 ([full changelog](https://github.com/arzttermine/arzttermine/compare/1.6.0...1.7.0))
  * feature [#392](https://github.com/arzttermine/arzttermine/issues/392) - [caldav] test calendar clients 
  * feature [#360](https://github.com/arzttermine/arzttermine/issues/360) - [admin dashboard] doctors without user
  * feature [#361](https://github.com/arzttermine/arzttermine/issues/361) - [admin] error "please check your data"
  * bug [#336](https://github.com/arzttermine/arzttermine/issues/336) - [admin] booking export - server error
  * bug [#359](https://github.com/arzttermine/arzttermine/issues/359) - [admin doctor] cannot upload profile pucture on doc account
  * bug [#388](https://github.com/arzttermine/arzttermine/issues/388) - [admin] can´t save profile changes (philosophy, education, etc)
  * bug [#394](https://github.com/arzttermine/arzttermine/issues/394) - [calendar] email+name validation is wrong
* [1.6.0](https://github.com/arzttermine/arzttermine/tree/1.6.0) - 2016-05-11 ([full changelog](https://github.com/arzttermine/arzttermine/compare/1.5.0...1.6.0))
  * feature [#378](https://github.com/arzttermine/arzttermine/issues/378) - [calendar] sms reminder should get send 2 hours before event
  * feature [#376](https://github.com/arzttermine/arzttermine/issues/376) - [e-mail] text of confirmation email
  * feature [#375](https://github.com/arzttermine/arzttermine/issues/375) - [server] Error in Sitemap on google webmaster tools
  * feature [#350](https://github.com/arzttermine/arzttermine/issues/350) - [calendar] on event editor add a comment tab
  * feature [#370](https://github.com/arzttermine/arzttermine/issues/370) - [calendar event editor] use opening-hours for startTime
  * feature [#369](https://github.com/arzttermine/arzttermine/issues/369) - [calendar event editor] endDate js validation fails
  * feature [#368](https://github.com/arzttermine/arzttermine/issues/368) - [calendar event editor] endDateTime should get updated automatically
  * feature [#349](https://github.com/arzttermine/arzttermine/issues/349) - [calendar] on event editor move the "ganztägig" checkbox
  * feature [#345](https://github.com/arzttermine/arzttermine/issues/345) - [calendar] check all resources on the widget by default
  * feature [#327](https://github.com/arzttermine/arzttermine/issues/327) - [calendar] check-box name
  * feature [#351](https://github.com/arzttermine/arzttermine/issues/351) - [calendar] implement a caldav server
  * bug [#386](https://github.com/arzttermine/arzttermine/issues/386) - [sitemap] in our sitemap are doctors listed which are not available anymore
  * bug [#346](https://github.com/arzttermine/arzttermine/issues/346) - [admin dashboard] unknown doctor
  * bug [#391](https://github.com/arzttermine/arzttermine/issues/391) - [calendar settings] SMS notification for patients
* [1.5.0](https://github.com/arzttermine/arzttermine/tree/1.5.0) - 2016-04-27 ([full changelog](https://github.com/arzttermine/arzttermine/compare/1.4.1...1.5.0))
  * bug [#328](https://github.com/arzttermine/arzttermine/issues/328) - [server] Ranking with desktop and mobile on google
  * bug [#366](https://github.com/arzttermine/arzttermine/issues/366) - [platform settings] user email shouldn't get changed
  * bug [#373](https://github.com/arzttermine/arzttermine/issues/373) - [calendar] translation
  * feature [#285](https://github.com/arzttermine/arzttermine/issues/285) - [server] add apache rewriterule for assets versioning
  * feature [#344](https://github.com/arzttermine/arzttermine/issues/344) - [calendar dashboard] new settings main navigation
  * feature [#327](https://github.com/arzttermine/arzttermine/issues/327) - [calendar] check-box name
* [1.4.1](https://github.com/arzttermine/arzttermine/tree/1.4.1) - 2016-04-20 ([full changelog](https://github.com/arzttermine/arzttermine/compare/1.4.0...1.4.1))
  * feature [#348](https://github.com/arzttermine/arzttermine/issues/348) - [admin dashboard] optimize "Events created by user"-card
  * feature [#347](https://github.com/arzttermine/arzttermine/issues/347) - [admin dashboard] hide some number cards for now
  * bug [#352](https://github.com/arzttermine/arzttermine/issues/352) - [admin] booking status not saving
* [1.4.0](https://github.com/arzttermine/arzttermine/tree/1.4.0) - 2016-04-20 ([full changelog](https://github.com/arzttermine/arzttermine/compare/1.3.2...1.4.0))
  * feature [#254](https://github.com/arzttermine/arzttermine/issues/254) - [calendar frontend] implement a "list" view
  * feature [#323](https://github.com/arzttermine/arzttermine/issues/323) - [admin] create a new card on dashboard for doctors without location
  * bug [#330](https://github.com/arzttermine/arzttermine/issues/330) - [admin bookings] comments don´t save
  * bug [#337](https://github.com/arzttermine/arzttermine/issues/337) - [doctor log in] pass reset
  * bug [#335](https://github.com/arzttermine/arzttermine/issues/335) - [widgets] fatal error on widgets
  * bug [#336](https://github.com/arzttermine/arzttermine/issues/336) - [admin] booking export - server error
* [1.3.2](https://github.com/arzttermine/arzttermine/tree/1.3.2) - 2016-04-15 ([full changelog](https://github.com/arzttermine/arzttermine/compare/1.3.1...1.3.2))
  * feature [#324](https://github.com/arzttermine/arzttermine/issues/324) - [calendar] send notification via sms
* [1.3.1](https://github.com/arzttermine/arzttermine/tree/1.3.1) - 2016-04-14 ([full changelog](https://github.com/arzttermine/arzttermine/compare/1.3.0...1.3.1))
  * feature [#315](https://github.com/arzttermine/arzttermine/issues/315) - [chat] integration
  * feature [#314](https://github.com/arzttermine/arzttermine/issues/314) - [calendar event] send event confirmation to patient
  * feature [#318](https://github.com/arzttermine/arzttermine/issues/318) - [calendar event] attach ical file to event confirmation email
  * feature [#312](https://github.com/arzttermine/arzttermine/issues/312) - [email notification] change subject line
* [1.3.0](https://github.com/arzttermine/arzttermine/tree/1.3.0) - 2016-04-13 ([full changelog](https://github.com/arzttermine/arzttermine/compare/1.2.0...1.3.0))
  * feature [#65](https://github.com/arzttermine/arzttermine/issues/65) - [admin] fix all admin tools by using doctrine in the actions new, edit, search
  * feature [#12](https://github.com/arzttermine/arzttermine/issues/12) - [search] use json as response format on the appointments api
  * feature [#293](https://github.com/arzttermine/arzttermine/issues/293) - [calendar] add practice settings tab
  * feature [#129](https://github.com/arzttermine/arzttermine/issues/192) - [admin statistics] we need some numbers to monitor the usage of our new calendar
  * bug [#263](https://github.com/arzttermine/arzttermine/issues/263) - [admin mailings] mailings body_html is not shown
  * bug [#294](https://github.com/arzttermine/arzttermine/issues/294) - [calendar] navigation icons are not shown anymore
  * bug [#298](https://github.com/arzttermine/arzttermine/issues/298) - [security] logged-in users shouldn't be able to access login/register controller action
  * bug [#301](https://github.com/arzttermine/arzttermine/issues/301) - [admin] search bar - leads to error 500
* [1.2.0](https://github.com/arzttermine/arzttermine/tree/1.2.0) - 2016-04-06 ([full changelog](https://github.com/arzttermine/arzttermine/compare/1.1.0...1.2.0))
  * bug [#278](https://github.com/arzttermine/arzttermine/issues/278) - [calendar] missing default resource
  * bug [#277](https://github.com/arzttermine/arzttermine/issues/277) - [calendar] translation
  * bug [#276](https://github.com/arzttermine/arzttermine/issues/276) - [calendar settings] wrong wording
  * bug [#275](https://github.com/arzttermine/arzttermine/issues/275) - [doctor profile front-end] missing phone number
  * bug [#273](https://github.com/arzttermine/arzttermine/issues/273) - [doctor backend] error when filling in phone and wep page
  * bug [#272](https://github.com/arzttermine/arzttermine/issues/272) - [doctor backend] platform settings appear empty
  * feature [#224](https://github.com/arzttermine/arzttermine/issues/224) -  [calendar] implement an email reminder system
  * feature [#288](https://github.com/arzttermine/arzttermine/issues/288) -  [security] add a "remember me"-feature to our login function
* [1.1.1](https://github.com/arzttermine/arzttermine/tree/1.1.1) - 2016-03-30 ([full changelog](https://github.com/arzttermine/arzttermine/compare/1.1.0...1.1.1))
  * bug [#271](https://github.com/arzttermine/arzttermine/issues/271) - [admin] error when accessing locations
  * bug [#260](https://github.com/arzttermine/arzttermine/issues/260) - [users] remove old groups
* [1.1.0](https://github.com/arzttermine/arzttermine/tree/1.1.0) - 2016-03-30 ([full changelog](https://github.com/arzttermine/arzttermine/compare/1.0.35...1.1.0))
  * bug [#216](https://github.com/arzttermine/arzttermine/issues/216) - [doctrine] remove helpers from entites
  * bug [#258](https://github.com/arzttermine/arzttermine/issues/258) - [admin users] last_login value is wrong
  * bug [#202](https://github.com/arzttermine/arzttermine/issues/202) - [mobile] enquiry appointment calendar not working on mobile
  * bug [#262](https://github.com/arzttermine/arzttermine/issues/262) - [doctor dashboard] password reset wrong from header
  * bug [#117](https://github.com/arzttermine/arzttermine/issues/117) - [admin planner] should show a doctors calendar
  * feature [#256](https://github.com/arzttermine/arzttermine/issues/256) - [calendar settings] add label "Other" for "Type of resource"
  * feature [#81](https://github.com/arzttermine/arzttermine/issues/81) - [all] remove static dir from static files url
  * feature [#266](https://github.com/arzttermine/arzttermine/issues/266) - [maintenance] migrate to new sf3 dir structure
