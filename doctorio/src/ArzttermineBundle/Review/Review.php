<?php

namespace ArzttermineBundle\Review;

use ArzttermineBundle\Booking\Booking;
use ArzttermineBundle\Entity\Basic;
use ArzttermineBundle\DependencyInjection\VirtualContainer;

class Review extends Basic {

    /**
     * @var string
     */
    protected $tablename = DB_TABLENAME_REVIEWS;

    /**
     * @var int
     */
    protected $booking_id;

    /**
     * @var int
     */
    protected $user_id;

    /**
     * @var string
     */
    protected $patient_email = '';

    /**
     * @var string
     */
    protected $patient_name = '';

    /**
     * @var float
     */
    protected $rating_1;

    /**
     * @var float
     */
    protected $rating_2;

    /**
     * @var float
     */
    protected $rating_3;

    /**
     * @var string
     */
    protected $rate_text = '';

    /**
     * @var string
     */
    protected $rated_at = '';

    /**
     * @var string
     */
    protected $approved_at = '';

    /**
     * @var string
     */
    protected $approved = '';

    /**
     * @var string
     */
    protected $url = '';

    /**
     * @var string
     */
    protected $comment = '';

    /**
     * @var array
     */
    protected $all_keys
        = array(
            "id",
            "booking_id",
            "user_id",
            "patient_email",
            "patient_name",
            "rating_1",
            "rating_2",
            "rating_3",
            "rate_text",
            "rated_at",
            "approved_at",
            "comment"
        );

    /**
     * @return int
     */
    public function getBookingId()
    {
        return $this->booking_id;
    }

    /**
     * @param int $booking_id
     *
     * @return self
     */
    public function setBookingId($booking_id)
    {
        $this->booking_id = $booking_id;

        return $this;
    }

    /**
     * @param int $user_id
     *
     * @return self
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Returns the email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->patient_email;
    }

    /**
     * Returns the booking object
     *
     * @return Booking
     */
    public function getBooking()
    {
        $booking = new Booking($this->booking_id);

        return $booking;
    }

    /**
     * Returns if the review was made anonymous
     *
     * @return string
     */
    public function isAnonymous()
    {
        return ($this->patient_name == '');
    }

    /**
     * Returns the rate_text
     *
     * @return string
     */
    public function getRateText()
    {
        return $this->rate_text;
    }

    /**
     * @param string $rate_text
     *
     * @return self
     */
    public function setRateText($rate_text)
    {
        $this->rate_text = $rate_text;

        return $this;
    }

    /**
     * Returns the rate value
     *
     * @param string $key
     *
     * @return float
     */
    public function getRating($key)
    {
        $value = 0;
        switch ($key) {
            case 'rating_1':
            case 'rating_2':
            case 'rating_3':
                $value = $this->$key;
                break;
            case 'rating_average':
                $value = ($this->rating_1 + $this->rating_2 + $this->rating_3) / 3;
                break;
        }

        return $value;
    }

    /**
     * Returns the patients name
     *
     * @return string
     */
    public function getPatientName()
    {
        return $this->patient_name;
    }

    /**
     * @param string $patient_name
     *
     * @return self
     */
    public function setPatientName($patient_name)
    {
        $this->patient_name = $patient_name;

        return $this;
    }

    /**
     * @return string
     */
    public function getPatientEmail()
    {
        return $this->patient_email;
    }

    /**
     * @param string $patient_email
     *
     * @return self
     */
    public function setPatientEmail($patient_email)
    {
        $this->patient_email = $patient_email;

        return $this;
    }

    /**
     * @return float
     */
    public function getRating1()
    {
        return $this->rating_1;
    }

    /**
     * @param float $rating_1
     *
     * @return self
     */
    public function setRating1($rating_1)
    {
        $this->rating_1 = $rating_1;

        return $this;
    }

    /**
     * @return float
     */
    public function getRating2()
    {
        return $this->rating_2;
    }

    /**
     * @param float $rating_2
     *
     * @return self
     */
    public function setRating2($rating_2)
    {
        $this->rating_2 = $rating_2;

        return $this;
    }

    /**
     * @return float
     */
    public function getRating3()
    {
        return $this->rating_3;
    }

    /**
     * @param float $rating_3
     *
     * @return self
     */
    public function setRating3($rating_3)
    {
        $this->rating_3 = $rating_3;

        return $this;
    }

    /**
     * @return string
     */
    public function getRatedAt()
    {
        return $this->rated_at;
    }

    /**
     * @param string $rated_at
     *
     * @return self
     */
    public function setRatedAt($rated_at)
    {
        $this->rated_at = $rated_at;

        return $this;
    }

    /**
     * @return string
     */
    public function getApprovedAt()
    {
        return $this->approved_at;
    }

    /**
     * @param string $approved_at
     *
     * @return self
     */
    public function setApprovedAt($approved_at)
    {
        $this->approved_at = $approved_at;

        return $this;
    }

    /**
     * @return string
     */
    public function getApproved()
    {
        return $this->approved;
    }

    /**
     * @param string $approved
     *
     * @return self
     */
    public function setApproved($approved)
    {
        $this->approved = $approved;

        return $this;
    }

    /**
     * Returns the review date
     *
     * @return string
     */
    public function getRatedAtDateText()
    {
        return VirtualContainer::get()->get('arzttermine.datetime_utilities')->getWeekdayDateText($this->rated_at, true);
    }

    /**
     * Returns the reviews for a user
     *
     * @param int $user_id
     *
     * @return array Review
     */
    public static function getReviewsForUser($user_id, $approved_only = true)
    {
        $review = new self();
        $approved = '';

        if ($approved_only) {
            $approved = ' AND approved_at IS NOT NULL AND approved_at!="0000-00-00 00:00:00"';
        }

        return $review->findObjects('user_id=' . $user_id . $approved . ' ORDER BY rated_at DESC');
    }

    /**
     * Returns the number of reviews for a user
     *
     * @param int $user_id
     *
     * @return int
     */
    public static function countReviewsForUser($user_id)
    {
        $review = new self();

        return $review->count('user_id=' . $user_id . ' AND approved_at IS NOT NULL AND approved_at!="0000-00-00 00:00:00"');
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     *
     * @return self
     */
    public function setComment($comment)
    {
        $this->comment = trim($comment);

        return $this;
    }

    /**
     * Returns the ratings array for a user
     *
     * @param int $userId
     *
     * @return array [float rating_1, float rating_2, float rating_3, float rating_average]
     */
    public function getRatingsAverageArrayForUser($userId)
    {
        return $this->getRatingsAverageArray('user_id=' . $userId);
    }

    /**
     * Returns the ratings array for a user or location depending on the where clause
     *
     * @param string $where
     *
     * @return array [float rating_1, float rating_2, float rating_3, float rating_average]
     */
    public function getRatingsAverageArray($where)
    {
        $db = VirtualContainer::get()->get('arzttermine.database');

        $ratingsAverage1 = 0;
        $ratingsAverage2 = 0;
        $ratingsAverage3 = 0;

        $sql
            = 'SELECT
					AVG(rating_1) AS ratings_average_1,
					AVG(rating_2) AS ratings_average_2,
					AVG(rating_3) AS ratings_average_3
				FROM ' . $this->tablename . '
				WHERE
					' . $where . '
					AND
					approved_at IS NOT NULL
					AND approved_at!="0000-00-00 00:00:00"';

        $ratingsAverage = 0;
        $db->executeQuery($sql);

        if ($db->affected_rows()) {
            foreach ($db->fetchAll() as $data) {
                $ratingsAverage1 = $data['ratings_average_1'];
                $ratingsAverage2 = $data['ratings_average_2'];
                $ratingsAverage3 = $data['ratings_average_3'];
                $ratingsAverage = ($ratingsAverage1 + $ratingsAverage2 + $ratingsAverage3) / 3;
            }
        }

        return array(
            'rating_1'       => $ratingsAverage1,
            'rating_2'       => $ratingsAverage2,
            'rating_3'       => $ratingsAverage3,
            'rating_average' => $ratingsAverage
        );
    }

    /**
     * Returns the reviews for a location
     *
     * @param array $userIds
     * @param int $rowCount (optional)
     * @param int $offset (optional)
     *
     * @return array Review
     */
    public static function getReviewsForLocation($userIds, $rowCount = null, $offset = null)
    {
        $limit = '';

        // check if LIMIT should be used
        if (!empty($rowCount) && is_numeric($rowCount)) {
            $limit = ' LIMIT ';
            if (!empty($offset) && is_numeric($offset)) {
                $limit .= $offset . ',';
            }
            $limit .= $rowCount;
        }

        // get all users ratings
        $usersString = '';
        foreach ($userIds as $userId) {
            if ($usersString != '') {
                $usersString .= ' OR ';
            }
            $usersString .= $userId;
        }

        return (new Review)->findObjects('(' . $usersString . ') AND approved_at IS NOT NULL AND approved_at!="0000-00-00 00:00:00" ORDER BY rated_at DESC' . $limit);
    }

    /**
     * Returns the number of reviews for a location
     *
     * @param array $userIds
     *
     * @return int
     */
    public static function countReviewsForLocation($userIds)
    {
        // get all users ratings
        $usersString = '';
        foreach ($userIds as $userId) {
            if (!empty($usersString)) {
                $usersString .= ' OR ';
            }
            $usersString .= $userId;
        }

        return (new Review)->count('user_id IN (' . $usersString . ') AND approved_at IS NOT NULL AND approved_at!="0000-00-00 00:00:00"');
    }

    /**
     * Returns the ratings array for a location
     *
     * @param array $userIds
     *
     * @return array [float rating_1, float rating_2, float rating_3, float rating_average]
     */
    public function getRatingsAverageArrayForLocation($userIds)
    {
        if (empty($userIds)) {
            return 0;
        }

        // get all users ratings
        $usersString = '';
        foreach ($userIds as $userId) {
            if (!empty($usersString)) {
                $usersString .= ' OR ';
            }
            $usersString .= 'user_id=' . $userId;
        }

        return $this->getRatingsAverageArray('(' . $usersString . ')');
    }

    /**
     * Returns all bookings where the appointment took place between now and X hours ago
     *
     * Returns all bookings matching:
     * - a certain integration
     * - having a certain status
     * - where the appointment is between NOW() and NOW()-$hours (=> not older than $hours from script execution)
     * - not allready reviewed
     *
     * @param array $days
     * @param array $integrationIds
     * @param array $bookingStatuses
     *
     * @return Booking[]
     */
    public static function getBookingsForReviewReminders($days, $integrationIds, $bookingStatuses)
    {
        $bookings = array();

        if (!is_array($days) || empty($days)) {
            return $bookings;
        }

        // build the query for the statuses
        $bookingStatusesSql = Booking::getStatusesSql($bookingStatuses, 'OR');
        $bookingStatusesSql = ($bookingStatusesSql == '') ? '' : ' AND (' . $bookingStatusesSql . ')';

        // build the query for the integrations
        $integrationSql = '';
        if (!empty($integrationIds)) {
            foreach ($integrationIds as $integrationId) {
                $integrationSql .= ($integrationSql == '' ? '' : ' OR ');
                $integrationSql .= DB_TABLENAME_LOCATIONS . ".`integration_id`=" . $integrationId;
            }
            $integrationSql = ($integrationSql == '' ? '' : ' AND (' . $integrationSql . ')');
        }
        // build the query for the days
        $dateSql = '';
        if (!empty($days)) {
            foreach ($days as $day) {
                $dateSql .= ($dateSql == '' ? '' : ' OR ');
                $dateSql .= "DATE(`appointment_start_at`) = DATE_SUB(CURRENT_DATE, INTERVAL " . $day . " DAY)";
            }
            $dateSql = ($dateSql == '' ? '' : ' AND (' . $dateSql . ')');
        }

        $sql
            = "SELECT
					" . DB_TABLENAME_BOOKINGS . ".id
				FROM
					" . DB_TABLENAME_BOOKINGS . "
				LEFT JOIN
					" . DB_TABLENAME_USERS . " ON " . DB_TABLENAME_BOOKINGS . ".`user_id`=" . DB_TABLENAME_USERS . ".`id`
				LEFT JOIN
					" . DB_TABLENAME_LOCATIONS . " ON " . DB_TABLENAME_BOOKINGS . ".`location_id`=" . DB_TABLENAME_LOCATIONS . ".`id`
				WHERE
				" . DB_TABLENAME_BOOKINGS . ".`email` NOT LIKE '%%buchung%%@arzttermine.de'
				" . $dateSql . "
				" . $integrationSql . "
				" . $bookingStatusesSql . "
				AND
				" . DB_TABLENAME_BOOKINGS . ".id NOT IN (
					SELECT booking_id FROM reviews
				)
                ";

        return (new Booking)->findObjectsSql($sql);
    }

    /**
     * Returns all bookings where the appointment took place between now and X hours ago
     *
     * Returns all bookings matching:
     * - a certain integration
     * - having a certain status
     * - where the appointment is between NOW() and NOW()-$hours (=> not older than $hours from script execution)
     * - not allready reviewed
     *
     * @param string $hours
     * @param array $integrationIds
     * @param array $bookingStatuses
     *
     * @return Booking[]
     */
    public static function getBookingsForReviewNotifications($hours, $integrationIds, $bookingStatuses)
    {
        $bookings = array();

        if (!is_numeric($hours)) {
            return $bookings;
        }

        // build the query for the statuses
        $bookingStatusesSql = Booking::getStatusesSql($bookingStatuses, 'OR');
        $bookingStatusesSql = ($bookingStatusesSql == '') ? '' : ' AND (' . $bookingStatusesSql . ')';

        // build the query for the integrations
        $integrationSql = '';
        if (!empty($integrationIds)) {
            foreach ($integrationIds as $integrationId) {
                $integrationSql .= ($integrationSql == '' ? '' : ' OR ');
                $integrationSql .= DB_TABLENAME_LOCATIONS . ".`integration_id`=" . $integrationId;
            }
            $integrationSql = ($integrationSql == '' ? '' : ' AND (' . $integrationSql . ')');
        }

        $sql
            = "SELECT
					" . DB_TABLENAME_BOOKINGS . ".id
				FROM
					" . DB_TABLENAME_BOOKINGS . "
				LEFT JOIN
					" . DB_TABLENAME_USERS . " ON " . DB_TABLENAME_BOOKINGS . ".`user_id`=" . DB_TABLENAME_USERS . ".`id`
				LEFT JOIN
					" . DB_TABLENAME_LOCATIONS . " ON " . DB_TABLENAME_BOOKINGS . ".`location_id`=" . DB_TABLENAME_LOCATIONS . ".`id`
				WHERE
				" . DB_TABLENAME_BOOKINGS . ".`email` NOT LIKE '%%buchung%%@arzttermine.de'
				AND
				`appointment_start_at` >= DATE_SUB(NOW(), INTERVAL " . $hours . " HOUR)
				AND
				`appointment_start_at` <= NOW()
				" . $integrationSql . "
				" . $bookingStatusesSql . "
				AND
				" . DB_TABLENAME_BOOKINGS . ".id NOT IN (
					SELECT booking_id FROM reviews
				)
				";

        return (new Booking)->findObjectsSql($sql);
    }
}
