<?php

namespace ArzttermineBundle\Core;

use ArzttermineBundle\DependencyInjection\VirtualContainer;


class Configuration
{
    /**
     * @var self
     */
    protected static $instance;

    /**
     * This will hold EVERYTHING that we've loaded
     *
     * @var array
     */
    private $container = array();

    /**
     * Used as a query proxy
     *
     * @var ConfigurationData
     */
    private $configuration_data;

    /**
     * Init autoload options
     */
    protected function __construct()
    {
        $this->configuration_data = new ConfigurationData();
        /*
         * autoload some (most) of the values
         */
        $this->load($this->configuration_data->findValues('autoload=1', 'option_value', 'option_name'));
    }

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Load the given resource into the container.
     * If array, assumed to be data and (destructively) merged.
     * If string, assumed to be a filename and contents loaded with require_once. NOTE: Be careful with this shit, yo
     *
     * @param string|array $resource
     *
     * @return self
     */
    public function load($resource)
    {
        // If it's not an array, it's probably a string, so try and load it
        if (!is_array($resource) && file_exists($resource)) {
            $resource = require_once $resource;
        }

        // Destructively merge config data
        if (is_array($resource)) {
            $this->container = array_merge($this->container, $resource);
        }

        return $this;
    }

    /**
     * @param string $option_name
     * @param mixed $option_value
     * 
     * @return self
     */
    public static function set($option_name, $option_value)
    {
        return self::getInstance()->optionSet($option_name, $option_value);
    }

    /**
     * @param string $option
     * 
     * @return mixed
     */
    public static function get($option)
    {
        $cache = VirtualContainer::get()->get('app.cache');
        if (false === ($configoption = $cache->fetch($option))) {
            $configoption  = self::getInstance()->optionGet($option);
            $cache->save($option, $configoption, 3600);
        }
        return $configoption;
    }

    /**
     * Load the given option
     *
     * @param string|array $option
     * @param bool $force_load
     *
     * @return self
     */
    public function optionLoad($option, $force_load = false)
    {
        if ($force_load || !$this->optionExists($option)) {
            $option_value = $this->configuration_data->findKey('option_name="' . $option . '"', 'option_value', 'option_value');
            if (!empty($option_value)) {
                $this->optionSet($option, $option_value);
            }
        }
        return $this;
    }

    /**
     * Set a value by array key
     *
     * @param string|int $option
     * @param mixed $value
     * @param bool $persist Save changes to DB
     * 
     * @return self
     */
    public function optionSet($option, $value, $persist = false)
    {
        $this->container[$option] = $value;
        if ($persist) {
            $this->optionSave($option, $value);
        }
        return $this;
    }

    /**
     * Stores a value permanent
     *
     * @param string|int $option The name
     * @param mixed $value
     * 
     * @return self
     */
    public function optionSave($option, $value)
    {
        $configurationData = new ConfigurationData($option, 'option_name');
        if ($configurationData->isValid()) {
            $configurationData
                ->setOptionValue($value)
                ->save();
        }
        return $this;
    }

    /**
     * Deletes an option permanent
     *
     * @param string|int $option The name
     * 
     * @return self
     */
    public function optionDelete($option)
    {
        $configurationData = new ConfigurationData($option, 'option_name');
        if ($configurationData->isValid()) {
            $configurationData
                ->delete();
        }
        return $this;
    }

    /**
     * @param string|int $option
     * 
     * @return mixed
     */
    public function optionGet($option)
    {
        return $this->optionLoad($option)->optionExists($option) ? $this->container[$option] : null;
    }

    /**
     * @param string|int $option
     *
     * @return bool
     */
    public function optionExists($option)
    {
        return isset($this->container[$option]);
    }

    /**
     * @param string|int $option
     * @param bool $persist Save changes to DB
     * 
     * @return self
     */
    public function optionUnset($option, $persist = false)
    {
        unset($this->container[$option]);

        if ($persist) {
            $this->optionDelete($option);
        }
        return $this;
    }
}
