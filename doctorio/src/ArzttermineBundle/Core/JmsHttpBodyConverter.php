<?php

namespace ArzttermineBundle\Core;

use AT\Component\Common\Exception\BadRequestException;
use JMS\Serializer\SerializerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class JmsHttpBodyConverter implements ParamConverterInterface
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * {@inheritdoc}
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        try {
            $data = $this->serializer->deserialize(
                $request->getContent(),
                $configuration->getClass(),
                'json'
            );
        } catch (\Exception $e) {
            // @TODO logging
            throw new BadRequestException([['code' => 'INVALID_REQUEST']], $e);
        }

        $request->attributes->set($configuration->getName(), $data);
    }

    /**
     * {@inheritdoc}
     */
    public function supports(ParamConverter $configuration)
    {
        $className = $configuration->getClass();
        return isset($className);
    }
}
