<?php

namespace ArzttermineBundle\Core;

use ArzttermineBundle\Entity\Basic;

class ConfigurationData extends Basic
{
    /**
     * @var string
     */
    protected $tablename = DB_TABLENAME_CONFIGURATION;

    /**
     * @var string db:varchar(64)
     */
    protected $option_name = '';

    /**
     * @var string
     */
    protected $option_value = '';

    /**
     * @var int Actually a tinyint/bool
     */
    protected $autoload;

    /**
     * @var array
     */
    protected $all_keys
        = array(
            "id",
            "option_name",
            "option_value",
            "autoload"
        );

    /**
     * @return string
     */
    public function getOptionName()
    {
        return trim($this->option_name);
    }

    /**
     * @param string $option_name
     *
     * @return self
     */
    public function setOptionName($option_name)
    {
        $this->option_name = trim($option_name);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOptionValue()
    {
        return $this->option_value;
    }

    /**
     * @param string $option_value
     * @return self
     */
    public function setOptionValue($option_value)
    {
        $this->option_value = $option_value;
        return $this;
    }

    /**
     * @return bool
     */
    public function getAutoload()
    {
        return (bool)$this->autoload;
    }

    /**
     * @param bool|int $autoload
     *
     * @return self
     */
    public function setAutoload($autoload)
    {
        $this->autoload = (bool)$autoload;
        return $this;
    }
}
