<?php

namespace ArzttermineBundle\Core;

class DateTime extends \DateTime {

    const FORMAT_TIME = 'H:i:s';
    const FORMAT_DATE = 'Y-m-d';
    const FORMAT_DATETIME = 'Y-m-d H:i:s';
    const FORMAT_DATETIME_TIMEZONE = 'c';
    
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getDateTime();
    }

    /**
     * @return string
     */
    public function getTime()
    {
        return $this->format(self::FORMAT_TIME);
    }

    /**
     * @return string
     */
    public function getDate()
    {
        return $this->format(self::FORMAT_DATE);
    }

    /**
     * @return string
     */
    public function getDateTime()
    {
        return $this->format(self::FORMAT_DATETIME);
    }

    /**
     * @return string
     */
    public function getDateTimeTimezone()
    {
        return $this->format(self::FORMAT_DATETIME_TIMEZONE);
    }

    /**
     * NOTE: THIS IS DIFFERENT TO THE DEFAULT IMPLEMENTATION OF \DateTime::setTime()!
     * 
     * The standard setTime takes 3 parameters, all of which are components of a time of day.
     * This method instead copies the time from one DateTime and sets it accordingly.
     * You can still use the old method, as this method will detect the parameter types.
     * 
     * @param int|DateTime|\DateTime $hour Take note of the difference here
     * @param int $minute
     * @param int $second
     *
     * @return self
     */
    public function setTime($hour, $minute = 0, $second = 0)
    {
        if ($hour instanceof \DateTime) {
            // Dumb, but you can thank PHP for that
            $this->setTime(
                intval($hour->format('H')),
                intval($hour->format('i')),
                intval($hour->format('s'))
            );

            return $this;
        }
        
        parent::setTime($hour, $minute, $second);
        
        return $this;
    }

    /**
     * NOTE: THIS IS DIFFERENT TO THE DEFAULT IMPLEMENTATION OF \DateTime::setDate()!
     *
     * The standard setDate takes 3 parameters, all of which are components of a date.
     * This method instead copies the date from one DateTime and sets it accordingly.
     * You can still use the old method, as this method will detect the parameter types.
     *
     * @param int|DateTime|\DateTime $year Take note of the difference here
     * @param int $month
     * @param int $day
     *
     * @return self
     */
    public function setDate($year, $month = 1, $day = 1)
    {
        if ($year instanceof \DateTime) {
            // Dumb, but you can thank PHP for that
            $this->setDate(
                intval($year->format('Y')),
                intval($year->format('m')),
                intval($year->format('d'))
            );

            return $this;
        }

        parent::setDate($year, $month, $day);
        
        return $this;
    }

    /**
     * @param int $days
     *
     * @return self
     */
    public function addDays($days = 0)
    {
        $days = intval($days);
        parent::modify("+{$days} days");
        
        return $this;
    }
}