<?php

namespace ArzttermineBundle\Core;

use Symfony\Component\Translation\TranslatorInterface;

/**
 * Calendar utility class. Dumping ground for datetime-related helpers
 * @todo Cleanup and/or remove
 */
class DateTimeUtilities {

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * Returns the first possible datetime where to search for free times
     *
     * @deprecated Use the \DateTime version where possible
     *
     * @return DateTime
     */
    public function getFirstPossibleDateTimeForSearch()
    {
        if (date('H:i') >= SHOW_NEXT_DAY_APPS_AFTER_THIS_TIME) {
            return date('Y-m-d', strtotime('tomorrow'));
        } else {
            return date('Y-m-d');
        }
    }

    /**
     * If the time of day is sufficiently late, we just make the search look for "the next day"
     * 
     * @return DateTime
     */
    public function getNextAvailableSearchDate()
    {
        $date = new DateTime('tomorrow'); // Get tomorrow's time, as we don't take appointments for "today"
        
        if ($date->format('H:i') >= SHOW_NEXT_DAY_APPS_AFTER_THIS_TIME) {
            $date->modify('+1 day'); // Miss the next day as well
        }
        
        return $date;
    }

    /**
     * checks if a date time is valid
     *
     * @param string $datetime
     *
     * @return bool
     */
    public function isValidDateTime($datetime)
    {
        if (preg_match("/^(\d{4})-(\d{2})-(\d{2}) ([01][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/", $datetime, $matches)) {
            return strtotime($datetime) > 0;
        }

        return false;
    }

    /**
     * checks if a date is valid
     *
     * @param string $date
     *
     * @return bool
     */
    public function isValidDate($date)
    {
        return strtotime($date) > 0;
    }

    /**
     * Returns the last possible datetime where to search for free times
     *
     * @return string
     */
    public function getLastPossibleDateTimeForSearch()
    {
        return date('Y-m-d', strtotime('+' . intval($GLOBALS['CONFIG']['CALENDAR_MAX_DAYS_AHEAD']) . ' day'));
    }

    /**
     * checks if a datetime or date is valid
     *
     * @param string|\Datetime $datetime_or_date
     *
     * @return array|bool
     */
    public static function getDateTimeArray($datetime_or_date)
    {
        if (preg_match("/^(\d{4})-(\d{2})-(\d{2}) ([01][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/", $datetime_or_date, $matches)) {
            return array(
                'year'   => $matches[1],
                'month'  => $matches[2],
                'day'    => $matches[3],
                'hour'   => $matches[4],
                'minute' => $matches[5],
                'second' => $matches[6]
            );
        }
        if (preg_match("/^(\d{4})-(\d{2})-(\d{2})$/", $datetime_or_date, $matches)) {
            return array(
                'year'   => $matches[1],
                'month'  => $matches[2],
                'day'    => $matches[3],
                'hour'   => '00',
                'minute' => '00',
                'second' => '00'
            );
        }

        return false;
    }

    /**
     * converts the utc to datetime
     *
     * @param datetime_id
     *
     * @return datetime or bool
     */
    public function UTC2DateTime($datetime_id)
    {
        $date_create_from_format = date_create_from_format('Ymd\THis', $datetime_id);
        if (!$date_create_from_format) {
            $date_create_from_format = date_create_from_format('Ymd\THis\Z', $datetime_id);
        }
        if (!$date_create_from_format) {
            return false;
        }

        return $date_create_from_format->format('Y-m-d H:i:s');
    }

    /**
     * Returns the datetime in yyyymmddThhiissZ format
     *
     * @param string $datetime
     *
     * @return string datetime or bool
     */
    public function dateTime2UTC($datetime)
    {
        $_dta = DateTimeUtilities::getDateTimeArray($datetime);
        if (!is_array($_dta)) {
            return false;
        }

        return $_dta['year'] . $_dta['month'] . $_dta['day'] . 'T' . $_dta['hour'] . $_dta['minute'] . $_dta['second'] . 'Z';
    }

    /**
     * @param string $datetime
     * @param int $daysCount
     * @param bool $dateOnly
     *
     * @return bool|string
     */
    public function addDays($datetime, $daysCount, $dateOnly = false)
    {
        return date(
            $dateOnly ? 'Y-m-d' : 'Y-m-d H:i:s',
            strtotime('+' . $daysCount . ' day', strtotime($datetime))
        );
    }

    /**
     * Returns an array with days that are between two dates/datetimes
     * AND filters out the days that should not be shown in the calendar
     * Here it filters out the weekends
     * 
     * @deprecated use getDatesInRange
     *
     * @param datetime
     * @param datetime
     *
     * @return array
     */
    public function getDatesInAreaWithFilter($date_start, $date_end)
    {
        $dates = $this->getDatesInRange($date_start, $date_end);
        
        return array_map(function($date) {
            /** @var DateTime $date */
            return $date->getDate();
        }, $dates);
    }

    /**
     * Returns an array with days that are between two dates/datetimes
     * 
     * @param string|DateTime $date_start
     * @param string|DateTime $date_end
     * @param bool $with_filter
     *
     * @return DateTime[]
     */
    public function getDatesInRange($date_start, $date_end, $with_filter = false)
    {
        $dates = array();
        
        if (!($date_start instanceof DateTime)) {
            $date_start = new DateTime($date_start);
        }

        if (!($date_end instanceof DateTime)) {
            $date_end = new DateTime($date_end);
        }

        $date_range = new \DatePeriod($date_start, new \DateInterval('P1D'), $date_end);

        /** @var DateTime $date */
        foreach ($date_range as $date) {
            // "N" gives us an int with Monday = 1 to Sunday = 7
            if ($with_filter && $date->format('N') > 5) {
                continue;
            }

            $dates[] = clone $date;
        }

        return $dates;
    }

    /**
     * @param $datetime_start
     * @param $datetime_end
     * @param string $format
     *
     * @return string
     */
    public function getDiff($datetime_start, $datetime_end, $format = '%a')
    {
        $datetime1 = new DateTime($datetime_start);
        $datetime2 = new DateTime($datetime_end);
        $interval = $datetime1->diff($datetime2);

        return $interval->format($format);
    }

    /**
     * @param $date
     *
     * @return bool
     */
    public function isWorkingDay($date)
    {
        $holidaysString = Configuration::get('holidays');
        $holidaysArray = explode(',', $holidaysString);
        $weekday = date('N', strtotime($date));

        return $weekday <= 5 && !in_array($date, $holidaysArray);
    }

    /**
     * @param $datetime
     *
     * @return bool|string
     */
    public function getWeekday($datetime)
    {
        return date('w', strtotime($datetime));
    }

    /**
     * @param string $date_start
     * @param string $date_days
     * @param bool|false $with_filter
     * @return string
     */
    public function getCalendarDaysHtml($date_start, $date_days, $with_filter = false)
    {
        $_html = '';
        $_offset = 0;
        $_visible_days = 0;
        while ($_visible_days < $date_days) {
            $_date_start = $this->addDays($date_start, $_offset);
            $_offset++;
            if ($with_filter) {
                $_weekday = $this->getWeekday($_date_start);
                if ($_weekday == 0 || $_weekday == 6) {
                    continue;
                }
            }

            $_visible_days++;
        }

        return $_html;
    }

    /**
     * Returns the date in text form
     *
     * @param DateTime|string $datetime
     * @param bool $short
     *
     * @return string
     */
    public function getDateText($datetime, $short = false)
    {
        $format = $this->translator->trans('datetime.format.date'.($short ? '_short' : ''));

        if ($datetime instanceof \DateTime) {
            return $datetime->format($format);
        }

        return date($format, strtotime($datetime));
    }

    /**
     * Return the time
     *
     * @param DateTime|string $datetime
     * @param bool $short
     *
     * @return string
     */
    public function getTimeText($datetime, $short = false)
    {
        $format = $this->translator->trans('datetime.format.time'.($short ? '_short' : ''));

        if ($datetime instanceof \DateTime) {
            return $datetime->format($format);
        }
        return date($format, strtotime($datetime));
    }

    /**
     * @param $datetime
     * @param bool $short_weekday
     *
     * @return string
     */
    public function getWeekdayDateText($datetime, $short_weekday = false)
    {
        return $this->translator->trans(
            'datetime.text.weekday_date',
            array(
                '%weekday%' => $this->getWeekdayTextByDateTime($datetime, $short_weekday),
                '%date%' => $this->getDateText($datetime, false)
            )
        );
    }

    /**
     * @param string $datetime
     * @param bool $short
     *
     * @return string
     */
    public function getWeekdayDateTimeText($datetime, $short = false)
    {
        return $this->translator->trans(
            'datetime.text.weekday_date_time',
            array(
                '%weekday%' => $this->getWeekdayTextByDateTime($datetime, $short),
                '%date%' => $this->getDateText($datetime, $short),
                '%time%' => $this->getTimeText($datetime, true),
            )
        );
    }

    /**
     * returns the weekday text
     *
     * @param string $date
     * @param bool $short weekday (=false)
     *
     * @return string
     */
    public function getWeekdayTextByDateTime($date, $short = false)
    {
        $weekday = ($date instanceof \DateTime)?$date->format('w'):date('w', strtotime($date));

        return $this->getWeekdayText($weekday, $short);
    }

    /**
     * Returns the weekday text
     *
     * @param int $weekday (0=Sun - 6=Sat)
     * @param bool $short (=false)
     *
     * @return string
     */
    public function getWeekdayText($weekday, $short = false)
    {
        switch ($weekday) {
            case 0:
            case 7: // ISO-8601
                $slug = 'sunday';
                break;
            case 1:
                $slug = 'monday';
                break;
            case 2:
                $slug = 'tuesday';
                break;
            case 3:
                $slug = 'wednesday';
                break;
            case 4:
                $slug = 'thursday';
                break;
            case 5:
                $slug = 'friday';
                break;
            case 6:
                $slug = 'saturday';
                break;
            default:
                $slug = '';
        }

        $key = 'datetime.weekday'.($short ? '_short' : '') . '.' . $slug;

        return $this->translator->trans($key);
    }

    /**
     * @param string $datetime
     * @param string $format
     *
     * @return bool|string
     */
    public function getDateTimeFormat($datetime, $format = 'Y-m-d H:i:s')
    {
        return date($format, strtotime($datetime));
    }
}

