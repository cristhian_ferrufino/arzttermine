<?php
namespace ArzttermineBundle\Twig;

use Arzttermine\BookingBundle\Entity\Booking;
use Arzttermine\ReviewBundle\Entity\Review;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class AppExtension extends \Twig_Extension
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(
        ContainerInterface $container
    ) {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('slug_to_url', [$this, 'slugToUrl']),
            new \Twig_SimpleFunction('getBreadcrumbs', [$this, 'getBreadcrumbs']),
            new \Twig_SimpleFunction('isAnonymous', [$this, 'isAnonymous']),
            new \Twig_SimpleFunction('aggregateReviews', [$this, 'aggregateReviews']),
            new \Twig_SimpleFunction('isCancellable', [$this, 'isCancellable']),
            new \Twig_SimpleFunction('practiceId', [$this, 'getCurrentPracticeId']),
            new \Twig_SimpleFunction('lastMonday', [$this, 'getLastMonday']),
            new \Twig_SimpleFunction('s3PracticeUrl', [$this, 'getS3PracticeAssetLink']),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return [
            // weekday_date_time
            new \Twig_SimpleFilter('date_format', [$this, 'dateFormat'])
        ];
    }

    /**
     * @return int
     */
    public function getCurrentPracticeId(): int
    {
        return $this->container->get('arzttermine_service.practices')->getPracticeId();
    }

    /**
     * @return int
     */
    public function getLastMonday(): int
    {
        return strtotime('last monday', strtotime('tomorrow'));
    }

    /**
     * @param Booking $booking
     * @return string
     */
    public function getSalutationName(Booking $booking)
    {
        $translator = $this->container->get('translator');

        // Add salutation
        $salutation = [
            $translator->trans('word.gender.salutation.male'),
            $translator->trans('word.gender.salutation.female')
        ][intval($booking->getGender())];

        $name = "{$salutation} {$booking->getFirstName()} {$booking->getLastName()}";

        return trim($name);
    }

    /**
     * @param Booking $booking
     * @return bool
     */
    public function isCancellable(Booking $booking)
    {
        return $booking->getStatus() != Booking::STATUS_CANCELLED;
    }

    /**
     * @param Booking $booking
     * @return string
     */
    public function getStatusLabel(Booking $booking)
    {
        if (!$booking->getStatus()) {
            return '';
        }

        $translator = $this->container->get('translator');

        return [
            Booking::STATUS_NEW        => $translator->trans('ui.booking.status1'),
            Booking::STATUS_PENDING    => $translator->trans('ui.booking.status2'),
            Booking::STATUS_CONFIRMED  => $translator->trans('ui.booking.status4'),
            Booking::STATUS_CANCELLED  => $translator->trans('ui.booking.status8')
        ][$booking->getStatus()];
    }

    /**
     * @param string $slug
     * @param string $route
     * @param bool $absolute_url
     * @return string
     */
    public function slugToUrl($slug, $route = 'doctor_profile', $absolute_url = false)
    {
        $url = '/'; // Default to home (redirect), as any other URL won't work

        if (!empty($slug)) {
            $router = $this->container->get('router');
            $compiledRoute = $router->getOriginalRouteCollection()->get($route)->compile();
            $params = $compiledRoute->getVariables();

            if (in_array('slug', $params)) {
                $url = $router->generate(
                    $route,
                    ['slug' => $slug],
                    ($absolute_url ? UrlGeneratorInterface::ABSOLUTE_URL : UrlGeneratorInterface::ABSOLUTE_PATH)
                );
            }
        }

        return $url;
    }

    /**
     * @param string|\DateTime $value
     * @param string|null $format
     * @param bool|null $short
     * @return string
     */
    public function dateFormat($value, $format = null, $short = null)
    {
        switch ($format) {
            case 'weekday_text':
                $output = $this->container->get('arzttermine.datetime_utilities')->getWeekdayTextByDateTime($value, $short);
                break;
            case 'weekday_datetime_text':
                $output = $this->container->get('arzttermine.datetime_utilities')->getWeekdayDateTimeText($value, $short);
                break;
            default:
                $format = $format ? $format : 'Y-m-d H:i:s';
                $output = (is_string($value)) ? date($format, strtotime($value)) : $value->format($format);
                break;
        }

        return $output;
    }

    /**
     * @param string $name
     * @param string $slug
     * @return array
     */
    public function getBreadcrumbs($name, $slug)
    {
        $translator = $this->container->get('translator');

        // home / ms+city / user

        return [
            [
                'url' => '/',
                'title' => $translator->trans('word.homepage'),
                'text' => $this->container->getParameter('app.name')
            ],
            [
                'url' => $this->slugToUrl($slug),
                'text' => $name
            ]
        ];
    }

    /**
     * @param Review $review
     * @return float
     */
    public function isAnonymous(Review $review)
    {
        return $this->container->get('arzttermine_review.review')->isAnonymous($review);
    }

    /**
     * @return string
     */
    public function getS3PracticeAssetLink()
    {
        $s3BaseUrl = $this->container->getParameter('aws.s3config')['baseUrl'];
        $practiceId = $this->getCurrentPracticeId();

        return sprintf('%s/p%d', $s3BaseUrl, $practiceId);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'app_extension';
    }
}
