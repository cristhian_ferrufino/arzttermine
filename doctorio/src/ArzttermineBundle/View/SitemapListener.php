<?php

namespace ArzttermineBundle\View;

use Presta\SitemapBundle\Event\SitemapPopulateEvent;
use Presta\SitemapBundle\Service\AbstractGenerator;
use Presta\SitemapBundle\Service\SitemapListenerInterface;
use Presta\SitemapBundle\Sitemap\Url\UrlConcrete;
use Symfony\Component\Routing\RouterInterface;

class SitemapListener implements SitemapListenerInterface {

    const SECTION_CORE = 'core';
    const SECTION_DOCTORS = 'doctors';
    const SECTION_LOCATIONS = 'locations';

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var array
     */
    protected $config = [];

    /**
     * @param RouterInterface $router
     * @param array $config
     */
    public function __construct(
        RouterInterface $router,
        $config = []
    ) {
        $this->router = $router;
        $this->config = $config;
    }

    /**
     * NOTE: There's a bug in the dispatcher that doesn't set the section properly.
     * We have to basically build everything and hope for the best
     *
     * @param SitemapPopulateEvent $event
     */
    public function populateSitemap(SitemapPopulateEvent $event)
    {
        $generator = $event->getGenerator();

        $this->addHomepage($generator);
    }

    /**
     * @param AbstractGenerator $generator
     */
    protected function addHomepage(AbstractGenerator $generator)
    {
        $generator->addUrl(
            new UrlConcrete(
                $this->router->generate('home', [], RouterInterface::ABSOLUTE_URL),
                new \DateTime(),
                UrlConcrete::CHANGEFREQ_MONTHLY,
                1
            ),
            self::SECTION_CORE
        );
    }
}
