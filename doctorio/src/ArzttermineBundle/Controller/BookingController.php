<?php

namespace ArzttermineBundle\Controller;

use Arzttermine\BookingBundle\Entity\Booking;
use Arzttermine\LocationBundle\Entity\Location;
use Arzttermine\UserBundle\Entity\Doctor;
use Arzttermine\UserBundle\Entity\MedicalSpecialty;
use Arzttermine\UserBundle\Entity\TreatmentService;
use ArzttermineBundle\Core\DateTime;
use ArzttermineBundle\DependencyInjection\VirtualContainer;
use ArzttermineBundle\Integration\Integration;
use Eluceo\iCal\Component\Calendar;
use Eluceo\iCal\Component\Event;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class BookingController extends Controller {

    /**
     * @param Request $request
     * @param FormInterface $form
     * @param Doctor $doctor
     * @param Location $location
     * @return Booking
     * @throws \Exception
     */
    private function placeBooking(Request $request, FormInterface $form, Doctor $doctor, Location $location)
    {
        $session = $this->getSession();

        /** @var Booking $booking */
        $booking = $form->getData();

        if (!$form->isValid()) {
            $this->addFlash('danger', $this->trans('notice.error.check_your_form_data'));
            return $booking;
        }

        $booking->setAppointmentStartAt(new \DateTime($booking->getAppointmentStartAt()));
        $booking->setIntegrationId($location->getIntegrationId());
        $booking->setStatus(Booking::STATUS_NEW);
        $booking->setSlug(substr(md5(uniqid()), 0, 12));
        $booking->setSourcePlatform($request->attributes->get('_source-platform', 'web'));
        $booking->setIpAddress($request->getClientIp());
        
        // Don't proceed if patient is blacklisted
        if ($this->get('arzttermine_booking.booking')->isBlacklisted($booking)) {
            $this->addFlash('danger', $this->trans('notice.error.booking_patient_is_blacklisted_default'));
            return $booking;
        }

        // Check if there is already a booking with this form_once
        if ($this->getDoctrine()->getManager()->getRepository('ArzttermineBookingBundle:Booking')->findByFormOnce($form->get('formOnce')->getData()) === null) {
            $this->addFlash('danger', $this->trans('ui.booking.old_nonce'));

            return $booking;
        }

        $msid = (int)$form->get('medical_specialty_id')->getData();
        $tsid = (int)$form->get('treatment_service_id')->getData();

        // Force the medical specialty to match the treatment service
        if ($tsid) {
            /** @var TreatmentService $treatmentService */
            $treatmentService = $this->getDoctrine()->getManager()->getRepository('ArzttermineUserBundle:TreatmentService')->find($tsid);
            if ($treatmentService !== null) {
                $msid = $treatmentService->getMedicalSpecialty()->getId();
            }
        }
        
        // If the MS is *still* empty, force a default
        if (empty($msid)) {
            $msids = $doctor->getMedicalSpecialtyIds();
            // csv -> array
            $msids = array_filter(explode(',', trim($msids)));
            // get first
            $msid = array_shift($msids);
        }

        // Don't book if a similar booking exists
        if ($this->get('arzttermine_booking.booking')->hasSimilarBooking(new \DateTime($form->get('appointmentStartAt')->getData()), $form->get('email')->getData(), $form['medical_specialty_id']->getData())) {
            $similar_bookings_message = $this->trans('ui.booking.duplicate_booking', array('%phone%' => VirtualContainer::get()->getParameter('app.phone.patient_support')));
            $this->addFlash('danger', $similar_bookings_message);

            return $booking;
        }

        $em = $this->getDoctrine()->getManager();
        $medicalSpecialty = $em->getRepository('ArzttermineUserBundle:MedicalSpecialty')->find($msid);
        $booking->setMedicalSpecialty($medicalSpecialty);
        $em->persist($booking);
        $em->flush();

        // set a flag that the booking should show the google data layer code for "page" only once
        $session->set('Booking_First_Page_View', true);

        return $booking;
    }

    /**
     * Handles the common requests
     *
     * @todo: POST to a separate action
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws \Exception
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->get('arzttermine.form.booking');

        // Populate initial form from query
        if ($request->isMethod('post')) {
            $form->handleRequest($request);
        } else {
            $doctor = $em->getRepository('ArzttermineUserBundle:Doctor')->find($request->query->get('u'));
            $location = $em->getRepository('ArzttermineLocationBundle:Location')->find($request->query->get('l'));
            $medicalSpecialty = $em->getRepository('ArzttermineUserBundle:MedicalSpecialty')->find($request->query->get('m'));

            $booking = new Booking();
            $booking->setLocation($location);
            $booking->setDoctor($doctor);

            $booking->setMedicalSpecialty($medicalSpecialty);
            if ($request->query->has('ts')) {
                $treatmentService = $em->getRepository('ArzttermineUserBundle:TreatmentService')->find($request->query->get('ts'));
                $booking->setTreatmentService($treatmentService);
            }
            $booking->setAppointmentStartAt(new \DateTime($request->query->get('s')));
            $booking->setFormOnce(bin2hex(random_bytes(4)));

            $form->setData($booking);
        }

        /** @var Doctor $doctor */
        /** @var Location $location */
        $doctor = $em->getRepository('ArzttermineUserBundle:Doctor')->find($form->get('doctor_id')->getData());
        $location = $em->getRepository('ArzttermineLocationBundle:Location')->find($form->get('location_id')->getData());

        if (
            $doctor === null ||
            $location === null ||
            !$doctor->getLocations()->contains($location)
        ) {
            $this->addFlash('danger', $this->trans('notice.error.ups'));
            return $this->redirectToRoute('home');
        }

        // If it's a POST, make the booking
        if ($request->isMethod('post')) {
            $booking = $this->placeBooking($request, $form, $doctor, $location);

            if ($booking !== null && $booking->getId() > 0) {
                return $this->redirect($this->get('router')->generate('booking_profile', ['slug' => $booking->getSlug()]));
            }
        }

        $seoPage = $this->get('sonata.seo.page');
        $seoPage->setTitle($this->trans('ui.booking.title', ['%name%' => trim("{$doctor->getTitle()} {$doctor->getFirstName()} {$doctor->getLastName()}")]));
        $seoPage->addMeta('name', 'robots', 'noindex, nofollow');

        return $this->render(
            '@widget/booking/booking.html.twig',
            [
                'form' => $form->createView(),
                'doctor' => $doctor,
                'location' => $location,
                'medical_specialty_id' => $form->get('medical_specialty_id')->getData(),
                'medical_specialty_name' => $em->getRepository('ArzttermineUserBundle:MedicalSpecialty')->find($form->get('medical_specialty_id')->getData())->getName(),
                'source_platform' => $request->attributes->get('_source-platform', 'web')
            ]
        );
    }

    /**
     * Stub
     *
     * @todo Transfer post-only actions here
     *
     * @param Request $request
     */
    public function placeAction(Request $request)
    {
        $this->indexAction($request);
    }

    /**
     * View a previously made booking (booking profile)
     *
     * @todo Refactor
     *
     * @param Request $request
     * @param $slug
     * @return Response
     * @throws \Exception
     */
    public function viewAction(Request $request, $slug)
    {
        // These are optional and are just added with a query string
        $mode = $request->query->get('type', '');
        $action = $request->query->get('action', 'view');

        /** @var Booking $booking */
        $booking = $this->getDoctrine()->getManager()->getRepository('ArzttermineBookingBundle:Booking')->findOneBySlug($slug);

        // If we have the ICS request, then offload handling to a hack
        if ($mode == 'ics') {
            return $this->handleICS($booking);
        }

        if ($booking !== null && $booking->getId() > 0) {
            // @todo Translations
            // Check for cancellation action
            if ($action == 'cancel') {
                if (!$booking->isCancelable()) {
                    $this->addFlash('danger', 'Ihre Buchung konnte nicht storniert werden. Bitte rufen Sie unseren kostenlosen Support an: '.$this->getParameter('app.phone.patient_support'));
                    return $this->redirectToRoute('home');
                }

                if ($booking->cancel()) {
                    $this->addFlash('info', 'Ihre Buchung wurde storniert');

                    return $this->redirectToRoute('home');
                } else {
                    $this->addFlash('danger', 'Ihre Buchung konnte nicht storniert werden. Bitte rufen Sie unseren kostenlosen Support an: '.$this->getParameter('app.phone.patient_support'));

                    return $this->redirectToRoute('home');
                }
            }
        } else {
            // Bad booking slug
            throw $this->createNotFoundException();
        }

        $doctor = $booking->getDoctor();
        $location = $booking->getLocation();

        $seoPage = $this->get('sonata.seo.page');
        $seoPage->setTitle($this->trans('ui.booking_profile.title', ['%name%' => trim("{$doctor->getTitle()} {$doctor->getFirstName()} {$doctor->getLastName()}")]));

        return $this->render(
            '@widget/booking/booking_profile.html.twig',
            [
                'doctor' => $doctor,
                'location' => $location,
                'booking' => $booking,
                'source_platform' => $request->attributes->get('_source-platform', 'web'),
                'weekDate' => $booking->getAppointmentStartAt()->format('Y-m-d H:i:s'),
                'weekDay' => $booking->getAppointmentStartAt()->format('d')
            ]
        );

    }

    /**
     * Create an ICS file
     *
     * @param Booking $booking
     * @return Response
     */
    private function handleICS(Booking $booking)
    {
        $bookingManager = $this->get('arzttermine_booking.booking');
        $locationManager = $this->get('arzttermine_location.location');

        $doctor = $booking->getDoctor();
        $location = $booking->getLocation();

        $doctorName = trim("{$doctor->getTitle()} {$doctor->getFirstName()} {$doctor->getLastName()}");
        $location_address = $locationManager->getAddress($location, ', ');
        $appointment = $bookingManager->getAppointment($booking);

        // Specify timezone so the calendar doesn't convert it unnecessarily
        date_timezone_set($appointment->getStartTime(), new \DateTimeZone('UTC'));

        // Add 30 min just to have an end
        $datetimeEnd = new DateTime($appointment->getStartTime());
        $datetimeEnd->add(new \DateInterval('PT30M'));
        $appointment->setEndTime($datetimeEnd);

        // @todo check prodId is fine
        $calendar = new Calendar($this->getParameter('app.name'));
        $calendar->setMethod(Calendar::METHOD_PUBLISH);
        $calendar->setName($this->trans('ui.booking_profile.ics.x_wr_calname'));
        $calendar->setDescription($this->trans('ui.booking_profile.ics.x_wr_caldesc'));
        $calendar->setTimezone('UTC');

        $event = new Event();
        $event->setDtStart($appointment->getStartTime());
        $event->setDtEnd($appointment->getEndTime());
        $event->setLocation($location_address);
        $event->setUrl($this->get('router')->generate('practice', ['slug' => $location->getSlug()], UrlGeneratorInterface::ABSOLUTE_URL));
        $event->setSummary($this->trans('ui.booking_profile.ics.summary', ['%name%' => $doctorName]));
        $event->setDescription($this->trans('ui.booking_profile.ics.description'));

        $calendar->addComponent($event);

        // @todo check filename is ok
        $filename = $appointment->getStartTime()->format('Ymd\THis').'.ics';

        $response = new Response();
        $response->headers->add([
            'Content-Type' => 'text/calendar; charset=utf-8',
            'Content-Disposition' => 'attachment; filename="'.$filename.'"'
        ]);
        $response->setContent($calendar->render());

        return $response;
    }
}
