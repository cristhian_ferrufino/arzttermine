<?php

namespace ArzttermineBundle\Controller;

use ArzttermineBundle\Form\DoctorAvatarUploadType;
use ArzttermineBundle\Media\Avatar;
use AT\Component\Upload\Model\FileUploadDescriptor;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AvatarUploadController extends Controller
{
    /**
     * @param int $doctorId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function uploadFormAction(int $doctorId = null)
    {
        $data = new Avatar();
        $data->doctorId = $doctorId;
        $data->practiceId = $this->get('arzttermine_service.practices')->getPracticeId();

        $form = $this->createForm(DoctorAvatarUploadType::class, $data);

        return $this->render(
            ':doctor/dashboard/profile-picture:avatar.html.twig',
            [
                'form' => $form->createView(),
                'doctor' => $this->getUser(),
            ]
        );
    }

    /**
     * @Security("has_role('ROLE_CUSTOMER')")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function uploadAction(Request $request)
    {
        $form = $this->createForm(DoctorAvatarUploadType::class);
        $form->handleRequest($request);

        if (!$form->isSubmitted()) {
            return new JsonResponse(['success' => false]);
        }
        if (!$form->isValid()) {
            return new JsonResponse(['success' => false, 'errors' => $form->getErrors(true)]);
        }

        /** @var UploadedFile $file */
        $file = $form->getData()->file;
        $pathSource = $file->getPath() . '/' . $file->getFilename();
        $pathTarget = sprintf('media/p%d/doctors/%d/avatars/profile.jpg', $form->getData()->practiceId, $form->getData()->doctorId);

        /** @var FileUploadDescriptor $upload */
        $upload = new FileUploadDescriptor();
        $upload->bucket = 'atcalendar.arzttermine.de';
        $upload->contentType = $file->getMimeType();
        $upload->targetKey = $pathTarget;

        $success = $this->get('arzttermine.file_uploader')->uploadFromPath($upload, $pathSource);

        return new JsonResponse(['success' => $success]);
    }
}
