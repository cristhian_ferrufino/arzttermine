<?php

namespace ArzttermineBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;

class HomepageController extends Controller {

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function indexAction()
    {
        if ($this->container->get('security.authorization_checker')->isGranted('ROLE_DOCTOR')) {
            return $this->redirectToRoute('doctor_calendar');
        }

        return $this->redirectToRoute('login');
    }

    /**
     * @Security("has_role('ROLE_CUSTOMER')")
     *
     * @return Response
     */
    public function noPracticeAction()
    {
        return $this->render(
            ':doctor/dashboard/practice:no-practice.html.twig',
            [
                'doctor' => $this->getUser(),
            ]
        );
    }
}
