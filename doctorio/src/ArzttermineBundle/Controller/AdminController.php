<?php

namespace ArzttermineBundle\Controller;

use ArzttermineBundle\Admin\Admin;
use ArzttermineBundle\Admin\AdminModuleInterface;
use ArzttermineBundle\Admin\Module\Blacklists;
use ArzttermineBundle\Admin\Module\BookingOffers;
use ArzttermineBundle\Admin\Module\Bookings;
use ArzttermineBundle\Admin\Module\Configuration;
use ArzttermineBundle\Admin\Module\Contacts;
use ArzttermineBundle\Admin\Module\Coupons;
use ArzttermineBundle\Admin\Module\Dashboard;
use ArzttermineBundle\Admin\Module\Districts;
use ArzttermineBundle\Admin\Module\InsuranceProviders;
use ArzttermineBundle\Admin\Module\Locations;
use ArzttermineBundle\Admin\Module\Mailings;
use ArzttermineBundle\Admin\Module\MedicalSpecialties;
use ArzttermineBundle\Admin\Module\PendingDoctors;
use ArzttermineBundle\Admin\Module\Planners;
use ArzttermineBundle\Admin\Module\Reviews;
use ArzttermineBundle\Admin\Module\Search;
use ArzttermineBundle\Admin\Module\SEO;
use ArzttermineBundle\Admin\Module\TreatmentServices;
use ArzttermineBundle\Admin\Module\TreatmentTypes;
use ArzttermineBundle\Admin\Module\UsedCoupons;
use ArzttermineBundle\Admin\Module\Doctors;
use ArzttermineBundle\Admin\Module\Users;
use ArzttermineBundle\Admin\Module\Patients;
use ArzttermineBundle\Media\Asset;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

/**
 * This class wraps procedural admin modules
 *
 * @package ArzttermineBundle\Controller\Admin
 */
class AdminController extends Controller {

    /**
     * @var Admin
     */
    protected $admin;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->admin = new Admin();
    }

    /**
     * Check a user is logged in/allowed to be here and redirect if not
     */
    private function checkIdentity()
    {
        // If the current profile isn't a doctor or isn't "active", then show an access denied message
        if (
            !$this->isGranted(['ROLE_ADMIN', 'ROLE_SUPPORT_USER', 'ROLE_SUPPORT_ADMIN'])
        ) {
            $message = $this->trans('notice.error.you_must_be_logged_in_to_view_that_page');

            throw new AuthenticationException($message);
        }
    }

    /**
     * @param AdminModuleInterface $module
     * 
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function showModule(AdminModuleInterface $module)
    {
        // Bump users away if they aren't logged in
        $this->checkIdentity();

        // Container
        $this->admin->setContainer($this->container);

        // Load the module into the Admin wrapper
        $this->admin->setModule($module);
        
        // Manually boot config here as we can modify the class further
        $this->admin->initialise();
        
        return $this->admin->display();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function searchAction()
    {
        return $this->showModule(new Search());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dashboardAction()
    {
        return $this->showModule(new Dashboard());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function blacklistsAction()
    {
        return $this->showModule(new Blacklists());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function bookingsAction()
    {
        return $this->showModule(new Bookings());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function bookingOffersAction()
    {
        return $this->showModule(new BookingOffers());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function contactsAction()
    {
        return $this->showModule(new Contacts());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function configurationAction()
    {
        return $this->showModule(new Configuration());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function couponsAction()
    {
        return $this->showModule(new Coupons());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function usedCouponsAction()
    {
        return $this->showModule(new UsedCoupons());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function districtsAction()
    {
        return $this->showModule(new Districts());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function insuranceProvidersAction()
    {
        return $this->showModule(new InsuranceProviders());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function locationsAction()
    {
        return $this->showModule(new Locations());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function mailingsAction()
    {
        return $this->showModule(new Mailings());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function medicalSpecialtiesAction()
    {
        return $this->showModule(new MedicalSpecialties());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function reviewsAction()
    {
        return $this->showModule(new Reviews());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function seoAction()
    {
        return $this->showModule(new SEO());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function pendingDoctorsAction()
    {
        return $this->showModule(new PendingDoctors());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function treatmentServicesAction()
    {
        return $this->showModule(new TreatmentServices());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function treatmentTypesAction()
    {
        return $this->showModule(new TreatmentTypes());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function doctorsAction()
    {
        $em = $this->container->get('doctrine')->getManager();
        $request = $this->container->get('request_stack')->getCurrentRequest();

        //handle profile picture upload
        if(isset($_FILES['upload'])) {
            $id = $request->get('id');
            $doctor = $em->getRepository('ArzttermineUserBundle:Doctor')->find($id);

            $upload_name = explode('.', $_FILES['upload']['name']);
            $extension = end($upload_name);
            if (
                in_array($extension, $GLOBALS["CONFIG"]["ASSET_POSSIBLE_FILEEXTENSIONS"]) &&
                $_FILES['upload']['error'] == UPLOAD_ERR_OK
            ) {
                $asset = new Asset();
                $asset->saveNewParent(ASSET_OWNERTYPE_USER, $doctor->getId(), ['gallery_id' => 0], $_FILES['upload']);
                if ($asset->isValid()) {
                    $doctor->setProfileAssetId($asset->getId());
                    $doctor->setProfileAssetFilename($asset->getFilename());
                    $em->flush();
                }
            }
        }

        //handle profile picture description change
        if(isset($_POST['form']['description'])) {
            $id = $request->get('id');
            $doctor = $em->getRepository('ArzttermineUserBundle:Doctor')->find($id);
            if($doctor->getProfileAssetId()) {
                $asset = new Asset($doctor->getProfileAssetId());
                $asset->setDescription($_POST['form']['description']);
                $asset->save(array('description'));
            }
        }

        return $this->showModule(new Doctors());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function patientsAction()
    {
        return $this->showModule(new Patients());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function usersAction()
    {
        return $this->showModule(new Users());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function plannersAction()
    {
        return $this->showModule(new Planners());
    }
    
    /**
     * The user has been disallowed access, so push them here
     */
    public function deniedAction()
    {
        // Container
        $this->admin->setContainer($this->container);

        return $this->render(
            'admin/content_left_right.html.twig',
            [
                'ContentRight' => $this->get('templating')->render(':admin:denied.html.twig'),
                'module' => 'denied'
            ]
        );
    }
}