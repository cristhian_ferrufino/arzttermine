<?php

namespace ArzttermineBundle\Controller;

class Controller extends \Symfony\Bundle\FrameworkBundle\Controller\Controller {

    /**
     * @param string $message
     * @param array $parameters
     * @param string $domain
     * @param string $locale
     * @return string
     * @throws \Exception
     */
    public function trans($message, $parameters = array(), $domain = null, $locale = null)
    {
        return $this->getTranslator()->trans($message, $parameters, $domain, $locale);
    }

    /**
     * Shortcut to return the request service.
     *
     * @throws \Exception
     * @return \Symfony\Component\Routing\Router
     */
    public function getRouter()
    {
        if (!$this->container->has('router')) {
            throw new \Exception('Router not found');
        }

        return $this->container->get('router');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Session\Session
     * @throws \Exception
     */
    public function getSession()
    {
        if (!$this->container->has('session')) {
            throw new \Exception('Session not found');
        }

        return $this->container->get('session');
    }

    /**
     * @return \Symfony\Component\Translation\Translator
     * @throws \Exception
     */
    public function getTranslator()
    {
        if (!$this->container->has('translator')) {
            throw new \Exception('Translator not found');
        }

        return $this->container->get('translator');
    }

    /**
     * Use the Security method of loading a User.
     * Ensures we ALWAYS have a user (even if empty)
     *
     * Used to retain BC
     */
    public function getUser()
    {
        return $this->get('security.token_storage')->getToken()->getUser();

/*
        $user = parent::getUser();
        if($user === null) {
            return;
        }

        return $user->getUserData();
*/
    }
}