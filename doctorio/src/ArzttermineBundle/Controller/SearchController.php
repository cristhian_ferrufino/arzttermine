<?php

namespace ArzttermineBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SearchController extends Controller {

    /**
     * @param Text $seo_text
     */
    private function generateMeta(Text $seo_text)
    {
        $seo = $this->get('sonata.seo.page');

        if ($seo_text->getStatus() == Text::STATUS_ACTIVE && $seo_text->getPermalink()) {
            // Title
            if ($title = $seo_text->getHtmlTitle()) {
                $seo->addTitle($title);
            }

            // Description
            if ($description = $seo_text->getHtmlMetaDescription()) {
                $seo->addMeta('name', 'description', $description);
            }

            // Keywords
            if ($keywords = $seo_text->getHtmlMetaKeywords()) {
                $seo->addMeta('name', 'keywords', $keywords);
            }
        } else {
            $medical_specialty = $seo_text->getMedicalSpecialty();
            $location = $seo_text->getAddressSearch();

            if (!empty($location) && !empty($medical_specialty)) {
                $seo->addTitle(
                    $this->trans(
                        'ui.search.title', [
                        '%medical_specialty%' => $medical_specialty->getName(),
                        '%in_location%' => $location
                    ])
                );
            }

            if (!$seo->hasMeta('name', 'description')) {
                if (!empty($medical_specialty)) {
                    $location_title = '';
                    if (!empty($location)) {
                        $location_title = $this->trans('word.in').' '.$location;
                    }
                    $description = $this->trans('ui.search.meta.description.in_location', [
                        '%medical_specialty%' => $medical_specialty->getName(),
                        '%in_location%' => $location_title
                    ]);
                } else {
                    $description = $this->trans('ui.search.meta.description.default');
                }

                $seo->addMeta('name', 'description', $description);
            }

            if (!$seo->hasMeta('name', 'keywords')) {
                $seo->addMeta('name', 'keywords', $this->trans('ui.search.meta.keywords.default'));
            }
        }
    }

    /**
     * @param Request $request
     * @param Text|null $seo_text
     * @return Response
     */
    public function searchAction(Request $request, Text $seo_text = null)
    {
        $search = $this->get('arzttermine_search.search');
        $form = $request->query->get('search', []);

        // Default form fields
        $form_fields = [
            'medical_specialty_id' => 0,
            'location' => ''
        ];
        $form = array_merge($form_fields, $form);

        if (empty($seo_text)) {
            $seo_text = new Text();
        }

        // From preconfigured seo text
        $medical_specialty = $seo_text->getMedicalSpecialty();

        if ($medical_specialty === null) {
            // From passed params
            $medical_specialty = $this->getDoctrine()->getRepository('ArzttermineUserBundle:MedicalSpecialty')->find($form['medical_specialty_id']);
        }

        if ($medical_specialty !== null) {
            $search->setMedicalSpecialty($medical_specialty);
        } else {
            throw $this->createNotFoundException(); // Catch-all routes die appropriately with this
        }

        // Update seo text for meta
        $seo_text->setMedicalSpecialty($search->getMedicalSpecialty());

        // TreatmentService
        if (!empty($form['treatment'])) {
            $treatmentService = $this->getDoctrine()->getRepository('ArzttermineUserBundle:TreatmentService')->find(intval($form['treatment']));
            $search->setTreatmentService($treatmentService);
        }

        // Location (from parameter)
        if ($location = $seo_text->getAddressSearch()) {
            $form['location'] = $location;
        }

        // Prepend exact location to city location if user has selected a city along with an exact location
        // NOTE: This is only for mobile(!), as we're using GPS data
        if ($exact_location = $request->query->get('exact-location')) {
            $form['location'] = $exact_location.($form['location'] ? ', '.$form['location'] : '');
        }

        // If the location is somehow still empty, set a default
        if (empty($form['location'])) {
            $form['location'] = $this->trans('ui.search.filter.location.default');
        }

        // Update search info (mainly for meta)
        $seo_text->setAddressSearch($form['location']);

        // if there is a district selected than overwrite the lat & lng
        if (!empty($form['district_id'])) {
            $district = $this->getDoctrine()->getRepository('ArzttermineLocationBundle:District')->find(intval($form['district_id']));

            if (!is_null($district)) {
                $search->setSearchOrigin([
                    'lat' => $district->getLat(),
                    'lng' => $district->getLng()
                ]);
            }
        }

        // Set the start date of the search
        if ($date_start = $request->query->get('date_start')) {
            $search->setDateBounds($date_start);
        }

        // Do the search
        $page = $request->query->get('p', 1);

        // Try get the geo coordinates
        // @todo Detect failures
        if ($coords = $search->getCoordinatesForAddress($form['location'])) {
            $search->setSearchOrigin($coords);
        }

        // Set the radius of the search (in km)
        if (!empty($form['distance'])) {
            $coords['radius'] = $form['distance'];
            $search->setRadius($form['distance']);
        }

        $search->setDateBounds($date_start);
        $search->setLat($coords['lat']);
        $search->setLng($coords['lng']);

        // Get providers within radius
        try {
            $locations = $search->searchProviders();
        } catch (MedicalSpecialtyException $mse) {
            throw $this->createNotFoundException();
        }
/*
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $locations,
            $page, // Page number
            $this->getParameter('app.search.per_page'), // Limit
            ['pageParameterName' => 'p']
        );
*/
        // Get items for current page and determine appointments for them
        //$providers = $pagination->getItems();
        $unfilteredSearch = clone $search;
        $unfilteredSearch->searchAppointments()->getProviders();
        $results = null;//$search->searchAppointments($providers)->getProviders();

        if (is_array($results)) {
            //$pagination->setItems($results);
        }

        // Get medical specialty values used for form elements
        $calendar = $this->get('arzttermine.datetime_utilities');
        $date_calendar_days = 20;

        if (empty($date_start)) {
            $date_start = $calendar->getFirstPossibleDateTimeForSearch();
        }

        // minus 1 to include the date_start
        $date_end = $calendar->addDays($date_start, ($date_calendar_days - 1), true);
        $date_days = count($calendar->getDatesInAreaWithFilter($date_start, $date_end));

        // Set default tags if possible
        // @todo this replaces the title text from an seo text result and needs to be patched
        $this->generateMeta($seo_text);

        $getMedicalSpecialtyTextCallable = $this->get('twig')->getFunction('getMedicalSpecialtyText')->getCallable();
        $response = $search->getJsonForProviders($results, $getMedicalSpecialtyTextCallable, $pagination, $unfilteredSearch);

        return $this->render(
            ':search:searchjson.html.twig',
            [
                'data' => $response->getContent(),
                'total_entries' => $pagination->getTotalItemCount(),
                'pagination' => $pagination,
                'map' => ['lat' => $search->getLat(), 'lng' => $search->getLng(), 'location' => $form['location']],
                'medical_specialty' => $search->getMedicalSpecialty()->getName(),
                'date_start' => $date_start,
                'date_end' => $date_end,
                'date_days' => $date_days,
                'seo_text' => $seo_text,
            ]
        );
    }
}
