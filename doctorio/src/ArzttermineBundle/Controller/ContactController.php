<?php

namespace ArzttermineBundle\Controller;

use ArzttermineBundle\Form\ContactType;
use Symfony\Component\HttpFoundation\Request;

class ContactController extends Controller {

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function indexAction(Request $request)
    {
        $form = $this->createForm(ContactType::class);

        if ($request->isMethod('post')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                try {
                    $contact = $form->getData();
                    $contact->setLang($request->getLocale());
                    $contact->setCreatedBy($this->getUser());
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($contact);
                    $em->flush();

                    $this->addFlash('success', $this->trans('ui.contact.created_successfully'));
                } catch (\Exception $e) {
                    // Bad save
                    $this->addFlash('danger', $this->trans('ui.contact.could_not_create'));
                }
            }
        }

        $seoPage = $this->get('sonata.seo.page');
        $seoPage->setTitle($this->trans('ui.contact.title'));

        return $this->render(
            '::contact.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }
}
