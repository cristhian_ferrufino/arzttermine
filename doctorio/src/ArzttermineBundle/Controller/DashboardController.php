<?php

namespace ArzttermineBundle\Controller;

use Arzttermine\ServiceBundle\Form\CreateBulkAppointmentsType;
use Arzttermine\ServiceBundle\Form\CreateHolidayType;
use Arzttermine\ServiceBundle\Form\ResourceType;
use Arzttermine\ServiceBundle\Form\TreatmentTypeType;
use Arzttermine\UserBundle\Form\UpdateUserType;
use ArzttermineBundle\Form\DoctorAvatarUploadType;
use AT\Component\Search\Filter\ProjectorFilter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Security("has_role('ROLE_DOCTOR')")
 */
class DashboardController extends Controller {

    /**
     * Doctor's area kpi page
     *
     * @return Response
     */
    public function dashboardAction()
    {
        return $this->render(
            ':doctor/dashboard:dashboard.html.twig',
            [
                'doctor' => $this->getUser()
            ]
        );
    }

    /**
     * This action is used to redirect to a certain page.
     * Call this endpoint with an encoded {@code _tp} query parameter:
     * /app/epr?_tp={encoded_path}
     *
     * This is useful when you want to redirect to a page independent on
     * the accessibility of the page and authentication of the current frontend user.
     * You might also need to set the practice id using the {@code _pid} query parameter.
     * Defaults to /app/calendar (if _tp is not present)
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function entrypointAction(Request $request)
    {
        $targetPath = $request->query->has('_tp')
            ? urldecode($request->query->get('_tp'))
            : $this->getRouter()->generate('doctor_calendar');

        if ($request->query->has('_pid')) {
            $practiceService = $this->get('arzttermine_service.practices');
            $practiceService->setPracticeId($request->query->getInt('_pid'));
        }

        return new RedirectResponse($targetPath);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws \Exception
     */
    public function calendarAction()
    {
        $seo = $this->get('sonata.seo.page');
        $seo->addTitle('Calendar');
        $seo->addHtmlAttributes('class','page-calendar');

        $practiceId = $this->get('arzttermine_service.practices')->getPracticeId();
        $practice = $this->get('at_client.practices.search')->getPractice($practiceId, 'germany', [new ProjectorFilter('paidFeatures')]);

        return $this->render(
            ':doctor/dashboard/calendar:calendar.html.twig',
            [
                'doctor' => $this->getUser(),
                'paidFeatures' => $practice['paidFeatures'],
            ]
        );
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws \Exception
     */
    public function calendarTreatmentsAction()
    {
        $seo = $this->get('sonata.seo.page');
        $seo->addTitle($this->trans('ui.doctor.dashboard.calendar.navigation.treatment_types'));

        $practiceId = $this->get('arzttermine_service.practices')->getPracticeId();
        $practice = $this->get('at_client.practices.search')->getPractice($practiceId, 'germany', [new ProjectorFilter('paidFeatures')]);

        $features = array_filter($practice['paidFeatures'], [$this, 'checkForOnlinePaymentFeature']);

        return $this->render(
            ':doctor/dashboard/calendar:treatment-types.html.twig',
            [
                'doctor' => $this->getUser(),
                'hasPaidTreatments' => count($features) > 0,
                'form' => $this->createForm(TreatmentTypeType::class)->createView()
            ]
        );
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws \Exception
     */
    public function calendarResourcesAction()
    {
        $seo = $this->get('sonata.seo.page');
        $seo->addTitle($this->trans('ui.doctor.dashboard.calendar.navigation.resources'));

        return $this->render(
            ':doctor/dashboard/calendar:resources.html.twig',
            [
                'doctor' => $this->getUser(),
                'form' => $this->createForm(ResourceType::class)->createView()
            ]
        );
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws \Exception
     */
    public function widgetBookingAction()
    {
        $seo = $this->get('sonata.seo.page');
        $seo->addTitle($this->trans('ui.doctor.dashboard.calendar.navigation.widget_booking'));

        return $this->render(
            ':doctor/dashboard/calendar:widget-booking.html.twig',
            [
                'doctor' => $this->getUser(),
                'practiceId' => $this->get('arzttermine_service.practices')->getPracticeId(),
                'token' => $this->get('at_customer.token_store')->getAccessToken(),
                'widgetUrl' => $this->getParameter('at_widget')['url'],
            ]
        );
    }

    /**
     * @return Response
     */
    public function calendarDoctorManagementAction()
    {
        $seo = $this->get('sonata.seo.page');
        $seo->addTitle($this->trans('ui.doctor.dashboard.calendar.navigation.doctor_management'));

        $translator = $this->get('translator');
        $suggestions = $this->get('arzttermine_service.suggestions');

        $practiceId = $this->get('arzttermine_service.practices')->getPracticeId();
        $practice = $this->get('at_client.practices.search')->getPractice($practiceId, 'germany', [new ProjectorFilter('treatmentTypes')]);

        $languages = array_map(function ($lang) use ($translator) {
            return ['id' => $lang, 'name' => $translator->trans('languages.' . strtoupper($lang))];
        }, $suggestions->getLanguages());

        usort($languages, [$this, 'sortLanguages']);

        return $this->render(
            ':doctor/dashboard/calendar:doctor-management.html.twig',
            [
                'doctor' => $this->getUser(),
                'photoUploadForm' => $this->createForm(DoctorAvatarUploadType::class)->createView(),
                'customerForm' => $this->createForm(UpdateUserType::class)->createView(),
                'treatmentTypes' => array_map(function ($type) use ($translator) {
                    return ['id' => $type['id'], 'name' => $type['type']];
                }, $practice['treatmentTypes']),
                'languages' => $languages,
                'medicalSpecialties' => array_map(function ($specialty) use ($translator) {
                    return ['id' => $specialty, 'name' => $translator->trans('medical_specialties.' . strtoupper($specialty))];
                }, $suggestions->getMedicalSpecialties()),
            ]
        );
    }

    /**
     * @return Response
     */
    public function holidayManagementAction()
    {
        $seo = $this->get('sonata.seo.page');
        $seo->addTitle($this->trans('ui.doctor.dashboard.calendar.navigation.holidays'));

        $token = $this->get('at_customer.token_store')->getAccessToken();
        $practiceId = $this->get('arzttermine_service.practices')->getPracticeId();

        $projectors = [new ProjectorFilter('resources.id'), new ProjectorFilter('resources.name')];
        $practice = $this->get('at_client.practices.search')->getPractice($practiceId, 'germany', $projectors);
        $holidays = $this->get('at_client.appointments')->getHolidays($token, $practiceId);
        $doctorDetails = [];

        foreach ($practice['resources'] as $item) {
            $doctorDetails[$item['id']] = $item['name'];
        }

        return $this->render(
            ':doctor/dashboard/calendar:holidays.html.twig',
            [
                'doctor' => $this->getUser(),
                'holidays' => $holidays->elements,
                'doctorResourceMapping' => $doctorDetails,
                'availableDoctors' => array_map(function ($item) {
                    return ['id' => $item['id'], 'name' => $item['name']];
                }, $practice['resources']),
            ]
        );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function createHolidayModalAction(Request $request)
    {
        $publicHoliday = $request->query->get('public', 0);

        return $this->render(
            ':doctor/dashboard/calendar/modal:holiday-form.html.twig',
            [
                'publicHoliday' => $publicHoliday,
                'form' => $this->createForm(CreateHolidayType::class)->createView(),
            ]
        );
    }

    /**
     * @return Response
     */
    public function calendarCreateDoctorModalAction()
    {
        $practiceId = $this->get('arzttermine_service.practices')->getPracticeId();
        $practice = $this->get('at_client.practices.search')->getPractice($practiceId, 'germany', [new ProjectorFilter('paidFeatures'), new ProjectorFilter('doctors.id')]);

        //Derive limit from additional_resources paidfeature, else assume limit = 1
        $limit = 1;
        $features = array_filter($practice['paidFeatures'], [$this, 'checkForAdditionalResourcesFeature']);
        $feature = (count($features) > 0) ? array_pop($features) : null;
        if ($feature && array_key_exists('options', $feature) && array_key_exists('limit', $feature['options'])) {
            $limit = $feature['options']['limit'];
        }

        //check wether associated ressources exceed limit
        if (count($practice['doctors']) >= $limit) {
            $form = NULL;
        } else {
            $form = $this->createForm(UpdateUserType::class)->createView();
        }

        return $this->render(
            ':doctor/dashboard/calendar/modal:doctor-form.html.twig',
            [
                'form' => $form
            ]
        );
    }

    /**
     * @return Response
     */
    public function calendarAvailabilitiesAction()
    {
        $seo = $this->get('sonata.seo.page');
        $seo->addTitle($this->trans('ui.doctor.dashboard.calendar.navigation.availabilities'));

        $projectors = [
            new ProjectorFilter('resources'),
            new ProjectorFilter('doctors'),
        ];

        $practiceId = $this->get('arzttermine_service.practices')->getPracticeId();
        $practice = $this->get('at_client.practices.search')->getPractice($practiceId, 'germany', $projectors);
        $resources = $practice['resources'];

        return $this->render(
            ':doctor/dashboard/calendar:availabilities.html.twig',
            [
                'doctor' => $this->getUser(),
                'doctors' => array_map(function ($doctor) use ($resources) {
                    $resource = array_filter($resources, function ($r) use ($doctor) {
                        return $r['systemId'] === $doctor['id'] && strtoupper($r['type']) === 'DOCTOR';
                    });

                    return [
                        'doctor' => $doctor,
                        'resource' => (count($resource) > 0) ? array_shift($resource) : null,
                    ];
                }, $practice['doctors']),
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $resourceId
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws \Exception
     */
    public function createAvailabilitiesModalAction(Request $request, int $resourceId)
    {
        $practiceId = $this->get('arzttermine_service.practices')->getPracticeId();

        $practice = $this->get('at_client.practices.search')->getPractice($practiceId, 'germany', [
            new ProjectorFilter('resources'),
            new ProjectorFilter('doctors.id'),
            new ProjectorFilter('doctors.fullName'),
            new ProjectorFilter('treatmentTypes'),
            new ProjectorFilter('insuranceTypes'),
        ]);

        $resource = null;

        foreach ($practice['resources'] as $res) {
            if ($res['id'] == $resourceId) {
                $resource = $res;
                break;
            }
        }

        if (null === $resource) {
            throw new NotFoundHttpException();
        }

        $doctor = array_filter($practice['doctors'], function ($doctor) use ($resource) {
            return $resource['systemId'] === $doctor['id'] && $resource['type'] === 'DOCTOR';
        });

        if (count($doctor) < 1) {
            throw new NotFoundHttpException('doctor not found');
        }

        $doctor = array_shift($doctor);
        $date = new \DateTime($request->query->get('active') ?: 'now');

        $selectedTreatments = [];
        $selectedInsurances = [];
        $data = $this->get('arzttermine_service.appointments')
            ->prepareBulkForm($date, $resource['id'], $practiceId, $request->query->get('aid', []));

        foreach ($data->rows as $entry) {
            foreach ($entry->treatmentTypes as $tt) {
                if (!in_array($tt, $selectedTreatments)) {
                    $selectedTreatments[] = $tt;
                }
            }

            foreach ($entry->insuranceTypes as $it) {
                if (!in_array($it, $selectedInsurances)) {
                    $selectedInsurances[] = $it;
                }
            }
        }

        $assignedTreatments = array_filter($practice['treatmentTypes'], function ($tt) use ($doctor) {
            return in_array($doctor['id'], $tt['doctorIds']);
        });

        //Get and translate supported insurance types, if non are set assume all
        $insuranceTypeMapper = $this->get('arzttermine.mapper.insurance_types');
        if (isset($practice['insuranceTypes'])) {
            $rawInsuranceTypes = $practice['insuranceTypes'];
        } else {
            $rawInsuranceTypes = array_keys($insuranceTypeMapper::VALUE_TO_TRANS_ID_MAPPING);
        }
        $insuranceTypes = $insuranceTypeMapper->getInsuranceTranslationIdMapping($rawInsuranceTypes);

        $form = $this->createForm(CreateBulkAppointmentsType::class, $data, [
            'doctorId' => (count($selectedTreatments) > 0) ? null : $doctor['id'],
            'treatmentTypes' => $assignedTreatments,
            'selectedTreatments' => $selectedTreatments,
            'selectedInsurances' => $selectedInsurances,
            'insuranceTypes' => $insuranceTypes,
        ]);

        $form->handleRequest($request);

        return $this->render(
            ':doctor/dashboard/calendar/modal:availability-form.html.twig',
            [
                'resource' => $resource,
                'selected' => $date,
                'doctor' => $doctor,
                'form' => $form->createView(),
                'hasTreatments' => count($assignedTreatments) > 0,
                'hasMultipleInsuranceTypes' => count($insuranceTypes) > 1,
            ]
        );
    }

    /**
     * @return Response
     */
    public function choosePracticeAction()
    {
        $doctor = $this->getUser();
        $practiceSearch = $this->get('at_client.practices.search');
        $practices = $practiceSearch->getPracticeIdsByDoctor($doctor->getId());

        $practiceService = $this->get('arzttermine_service.practices');
        $currentPractice = null;

        if ($practiceService->hasPracticeId()) {
            $projectors = [
                new ProjectorFilter('id'),
                new ProjectorFilter('name'),
                new ProjectorFilter('location'),
            ];

            $currentPractice = $practiceSearch->getPractice($practiceService->getPracticeId(), 'germany', $projectors);
        }

        return $this->render(
            ':doctor/dashboard/practice:choose.html.twig',
            [
                'doctor' => $doctor,
                'practices' => $practices,
                'practice' => $currentPractice,
            ]
        );
    }

    /**
     * @param int $id
     * @return Response
     */
    public function setPracticeAction(int $id)
    {
        $doctor = $this->getUser();
        $practices = $this->get('at_client.practices.search')->getPracticeIdsByDoctor($doctor->getId());

        if (!isset($practices[$id])) {
            return $this->redirectToRoute('doctor_calendar_no_practice');
        }

        $practiceService = $this->get('arzttermine_service.practices');
        $practiceService->setPracticeId($id);

        return $this->redirectToRoute('doctor_calendar');
    }

    private function checkForOnlinePaymentFeature(&$feature) {
        return $feature['name'] == 'online_payments';
    }

    private function checkForAdditionalResourcesFeature(&$feature) {
        return $feature['name'] == 'additional_resources';
    }

    private function sortLanguages($a, $b) {
        return strcmp($a['name'], $b['name']);
    }
}
