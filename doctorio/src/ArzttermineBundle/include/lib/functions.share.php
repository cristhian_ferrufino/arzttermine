<?php

use ArzttermineBundle\DependencyInjection\VirtualContainer;

/**
 * Shim to replace copy/paste nightmares
 *
 * @return string
 */
function get_charset()
{
    return VirtualContainer::get()->getParameter('app.charset');
}

/**
 * Shim to replace old crappy is_email check
 *
 * @param string $email
 * @return bool
 */
function is_email($email)
{
    return (VirtualContainer::get()->get('validator')->validate($email, new \Symfony\Component\Validator\Constraints\Email()));
}

/**
 * Shim to replace old crappy WP sanitise
 *
 * @param string $email
 * @return string
 */
function sanitize_email($email)
{
    return filter_var($email, FILTER_SANITIZE_EMAIL);
}

/**
 * @param int $length
 *
 * @return string
 */
function create_uid($length = 6)
{
    srand((double)(microtime() * 1000000));
    $valid_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
    $uid = '';

    while ($length > 0) {
        $uid .= $valid_chars[rand(0, strlen($valid_chars) - 1)];
        $length--;
    }

    return $uid;
}

function camelize($str, $include_first = false)
{
    $str = str_replace(' ', '', ucwords(str_replace('_', ' ', strtolower($str))));

    if (!$include_first) {
        $str = lcfirst($str);
    }

    return $str;
}

/**
 * @param array $source_array
 * @param array $return
 *
 * @return array
 */
function array_flatten($source_array, $return = array())
{
    foreach ($source_array as $v) {
        if (is_array($v)) {
            $return = array_flatten($v, $return);
        } else {
            if (isset($v)) {
                $return[] = $v;
            }
        }
    }

    return $return;
}

/**
 * @param int $bytes
 * @param int $precision
 *
 * @return string
 */
function format_bytes($bytes, $precision = 2)
{
    $units = array('B', 'KB', 'MB', 'GB', 'TB');

    $bytes = max($bytes, 0);
    $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
    $pow = min($pow, count($units) - 1);

    $bytes /= (1 << (10 * $pow));

    return round($bytes, $precision) . ' ' . $units[$pow];
}

/**
 * @param int $number Number in Base10
 *
 * @return string
 */
function base36_encode($number)
{
    return base_convert($number, 10, 36);
}

/**
 * @param string $number Number in Base36
 *
 * @return string
 */
function base36_decode($number)
{
    return base_convert($number, 36, 10);
}

/**
 * returns the url for a media
 *
 * @param string $name
 * @param bool $absolute_url = false
 * @param bool $add_timestamp = false
 *
 * @return string
 */
function get_media_url($name, $absolute_url = false, $add_timestamp = false)
{
    $url = '';
    if ($absolute_url) {
        $url .= VirtualContainer::get()->getParameter('app.url.https');
    }
    $url .= $GLOBALS['CONFIG']['MEDIA_URL'] . $name;
    if ($add_timestamp) {
        $_filename = $GLOBALS['CONFIG']['STATIC_PATH'] . $name;
        if (file_exists($_filename)) {
            $url .= '?' . filemtime($_filename);
        }
    }

    return $url;
}

/**
 * returns the url for a static asset
 *
 * @param string $name
 * @param bool $absoute url = false
 * @param bool $add_timestamp = false
 *
 * @return string
 **/
function getStaticUrl($name, $absolute_url = false, $add_timestamp = false)
{
    $url = '';
    if ($absolute_url) {
        $url .= VirtualContainer::get()->getParameter('app.url.https');
    }
    $url .= $GLOBALS['CONFIG']['STATIC_URL'] . $name;
    if ($add_timestamp) {
        $_filename = $GLOBALS['CONFIG']['STATIC_PATH'] . $name;
        if (file_exists($_filename)) {
            $url .= '?' . filemtime($_filename);
        }
    }

    return $url;
}

/**
 * Returns a _GET or _POST parameter from _REQUEST var
 *
 * @deprecated Get the field using the Request object instead
 *
 * @param string $parameter
 * @param mixed $default If parameter is not set than return the default
 *
 * @return mixed
 */
function getParam($parameter, $default = '')
{
    return VirtualContainer::get()->get('request_stack')->getCurrentRequest()->get($parameter, $default);
}

/**
 * @param string $category_ids
 * @param array $values
 * @param string $delimiter
 * @param string $delimiter_close
 * @return string
 */
function getCategoriesText($category_ids, $values, $delimiter = ', ', $delimiter_close = '')
{
    if (!is_array($category_ids)) {
        // then treat it as a comma separated value
        $category_ids = explode(',', $category_ids);
    }

    $_html = '';

    foreach ($values as $_id => $_category) {
        if (in_array($_id, $category_ids)) {
            if (!(empty($delimiter_close))) {
                $_html .= $delimiter . $values[$_id] . $delimiter_close;
            } elseif ($delimiter == '<li>') {
                $_html .= '<li>' . $values[$_id] . '</li>';
            } else {
                if ($_html != '') {
                    $_html .= $delimiter;
                }
                $_html .= $values[$_id];
            }
        }
    }

    return $_html;
}

// Completely shit and awful stealing of WP code below
// @todo Burn this
// ===================================================

/**
 * @param mixed $value
 * @return array|string
 */
function urlencode_deep($value)
{
    return is_array($value) ? array_map('urlencode_deep', $value) : urlencode($value);
}

/**
 * @return mixed|string
 */
function add_query_arg()
{
    $args = func_get_args();
    if (is_array($args[0])) {
        if (count($args) < 2 || false === $args[1]) {
            $uri = $_SERVER['REQUEST_URI'];
        } else {
            $uri = $args[1];
        }
    } else {
        if (count($args) < 3 || false === $args[2]) {
            $uri = $_SERVER['REQUEST_URI'];
        } else {
            $uri = $args[2];
        }
    }

    if ($frag = strstr($uri, '#')) {
        $uri = substr($uri, 0, -strlen($frag));
    } else {
        $frag = '';
    }

    if (0 === stripos($uri, 'http://')) {
        $protocol = 'http://';
        $uri = substr($uri, 7);
    } elseif (0 === stripos($uri, 'https://')) {
        $protocol = 'https://';
        $uri = substr($uri, 8);
    } else {
        $protocol = '';
    }

    if (strpos($uri, '?') !== false) {
        list($base, $query) = explode('?', $uri, 2);
        $base .= '?';
    } elseif ($protocol || strpos($uri, '=') === false) {
        $base = $uri.'?';
        $query = '';
    } else {
        $base = '';
        $query = $uri;
    }

    parse_str($query, $qs);
    $qs = urlencode_deep($qs); // this re-URL-encodes things that were already in the query string
    if (is_array($args[0])) {
        foreach ($args[0] as $k => $v) {
            $qs[$k] = $v;
        }
    } else {
        $qs[$args[0]] = $args[1];
    }

    foreach ($qs as $k => $v) {
        if ($v === false) {
            unset($qs[$k]);
        }
    }

    $ret = build_query($qs);
    $ret = trim($ret, '?');
    $ret = preg_replace('#=(&|$)#', '$1', $ret);
    $ret = $protocol.$base.$ret.$frag;
    $ret = rtrim($ret, '?');

    return $ret;
}

/**
 * @param array $data
 * @return string
 */
function build_query($data)
{
    return _http_build_query($data, null, '&', '', false);
}

/**
 * @param array $data
 * @param null $prefix
 * @param null $sep
 * @param string $key
 * @param bool|true $urlencode
 * @return string
 */
function _http_build_query($data, $prefix = null, $sep = null, $key = '', $urlencode = true)
{
    $ret = [];

    foreach ((array)$data as $k => $v) {
        if ($urlencode) {
            $k = urlencode($k);
        }
        if (is_int($k) && $prefix != null) {
            $k = $prefix.$k;
        }
        if (!empty($key)) {
            $k = $key.'%5B'.$k.'%5D';
        }
        if ($v === null) {
            continue;
        } elseif ($v === false) {
            $v = '0';
        }

        if (is_array($v) || is_object($v)) {
            array_push($ret, _http_build_query($v, '', $sep, $k, $urlencode));
        } elseif ($urlencode) {
            array_push($ret, $k.'='.urlencode($v));
        } else {
            array_push($ret, $k.'='.$v);
        }
    }

    if (null === $sep) {
        $sep = ini_get('arg_separator.output');
    }

    return implode($sep, $ret);
}

function paginate_links($args = '')
{
    $defaults = [
        'base' => '%_%', // http://example.com/all_posts.php%_% : %_% is replaced by format (below)
        'format' => '?page=%#%', // ?page=%#% : %#% is replaced by the page number
        'total' => 1,
        'current' => 0,
        'show_all' => false,
        'prev_next' => true,
        'prev_text' => '&laquo;',
        'next_text' => '&raquo;',
        'end_size' => 1,
        'mid_size' => 2,
        'type' => 'platform',
        'add_args' => false, // array of query args to add
        'add_fragment' => '',
        'show_page_numbers' => true
    ];

    $args = array_merge($defaults, $args);
    extract($args, EXTR_SKIP);

    // Who knows what else people pass in $args
    $total = (int)$total;
    if ($total < 2) {
        return '';
    }
    $current = (int)$current;
    $end_size = 0 < (int)$end_size ? (int)$end_size : 1; // Out of bounds?  Make it the default.
    $mid_size = 0 <= (int)$mid_size ? (int)$mid_size : 2;
    $add_args = is_array($add_args) ? $add_args : false;
    $r = '';
    $page_links = [];
    $dots = false;

    if ($prev_next && $current && 1 < $current) :
        $link = str_replace('%_%', 2 == $current ? '' : $format, $base);
        $link = str_replace('%#%', $current - 1, $link);
        if ($add_args) {
            $link = add_query_arg($add_args, $link);
        }
        $link .= $add_fragment;
        switch ($type) {
            case 'bootstrap':
                $page_links[] = '<li class="page-item"><a class="page-link" href="'.$link.'">'.$prev_text.'</a></li>';
                break;
            case 'platform':
            default :
                $page_links[] = "<a class='page-link prev page-numbers' href='".$link."'><span class='page-link text'>$prev_text</span></a>";
                break;
        }

    endif;
    if ($show_page_numbers) {
        for ($n = 1; $n <= $total; $n++) :
            $n_display = $n;
            // $n_display = number_format_i18n($n);
            if ($n == $current) :
                switch ($type) {
                    case 'bootstrap':
                        $page_links[] = '<li class="page-item active"><span class="page-link">'.$n_display.'<span class="sr-only">(current)</span></span></li>';
                        break;
                    case 'platform':
                    default :
                        $page_links[] = "<span class='page-link page-numbers current'>$n_display</span>";
                        break;
                }
                $dots = true;
            else :
                if ($show_all || ($n <= $end_size || ($current && $n >= $current - $mid_size && $n <= $current + $mid_size) || $n > $total - $end_size)) :
                    $link = str_replace('%_%', 1 == $n ? '' : $format, $base);
                    $link = str_replace('%#%', $n, $link);
                    if ($add_args) {
                        $link = add_query_arg($add_args, $link);
                    }
                    $link .= $add_fragment;
                    switch ($type) {
                        case 'bootstrap':
                            $page_links[] = '<li class="page-item"><a class="page-link" href="'.$link.'">'.$n_display.'</a></li>';
                            break;
                        case 'platform':
                        default :
                            $page_links[] = "<a class='page-link page-numbers' href='".$link."'>$n_display</a>";
                            break;
                    }
                    $dots = true;
                elseif ($dots && !$show_all) :
                    switch ($type) {
                        case 'bootstrap':
                            $page_links[] = '<li class="page-item disabled"><span class="page-link">... <span class="sr-only">(dots)</span></span></li>';
                            break;
                        case 'platform':
                        default :
                            $page_links[] = "<span class='page-link page-numbers dots'>...</span>";
                            break;
                    }
                    $dots = false;
                endif;
            endif;
        endfor;
    }
    if ($prev_next && $current && ($current < $total || -1 == $total)) :
        $link = str_replace('%_%', $format, $base);
        $link = str_replace('%#%', $current + 1, $link);
        if ($add_args) {
            $link = add_query_arg($add_args, $link);
        }
        $link .= $add_fragment;
        // @todo: put this in a template where the parent element would get a class depending whether
        // it contains only prev, only next or both links. it's important for the justification and
        // a workaround is to add one empty span element.
        // of course, it would be better to have the template for number of other reasons, as well
        switch ($type) :
            case 'bootstrap':
                $page_links[] = '<li class="page-item"><a class="page-link" href="'. $link.'">'.$next_text.'</a></li>';
                break;
            case 'platform':
            default :
                $page_links[] = "<a class='page-link next page-numbers' href='".$link."'><span class='text'>$next_text</span></a>";
                break;
        endswitch;
    endif;

    return join("\n\t", $page_links);
}