<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

use \ArzttermineBundle\Calendar\DateTimeUtilities;
use ArzttermineBundle\Core\DateTime;

/**
 * Smarty {calendar} function plugin
 *
 * Type:     function<br>
 * Name:     calendar<br>
 * Purpose:  calendar utility function
 * <br>
 *
 * @version 1.0
 * @param array $params parameters
 * @param object $template template object
 * @return string render template|null
 */
function smarty_function_calendar($params, $template)
{
    if (!isset($params['action'])) {
        trigger_error("Calendar Utilities: Missing 'action'");
        return '';
    }   
    
    if (!isset($params['datetime'])) {
        trigger_error("Calendar Utilities: Missing 'datetime'");
        return '';
    }
    
    $output = '';
    
    switch ($params['action']) {
        case 'getWeekdayText':
            $short = isset($params['short']) ? (bool)$params['short'] : null;
            $output = DateTimeUtilities::getWeekdayText($params['datetime'], $short);
            break;
        case 'getWeekdayDateTimeText':
            $short = isset($params['short']) ? (bool)$params['short'] : null;
            $output = DateTimeUtilities::getWeekdayDateTimeText($params['datetime'], $short);
            break;
        case 'getDateTimeFormat':
            $format = isset($params['format']) ? $params['format'] : '';
            $output = DateTimeUtilities::getDateTimeFormat($params['datetime'], $format);
            break;
        case 'getCalendarDaysHtml':
            $date_days = isset($params['date_days']) ? $params['date_days'] : 5;
            $output= DateTimeUtilities::getCalendarDaysHtml($params['datetime'], $date_days);
            break;
        case 'format':
            $format = !empty($params['format']) ? $params['format'] : DateTime::FORMAT_DATETIME;
            $date = new DateTime($params['datetime']);
            $output = $date->format($format);
            break;
    }
    
    return $output;
}