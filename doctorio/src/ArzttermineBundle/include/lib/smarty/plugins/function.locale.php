<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty {locale} function plugin
 *
 * Type:     function<br>
 * Name:     locale<br>
 * Purpose:  renders the current locale
 * <br>
 *
 * @version 1.0
 * @param array $params parameters
 * @param object $template template object
 * @return string template|null
 */
function smarty_function_locale($params, $template)
{
    return \ArzttermineBundle\DependencyInjection\VirtualContainer::get()->get('translator')->getLocale();
}