<?php

use ArzttermineBundle\DependencyInjection\VirtualContainer;

/**
 * Smarty plugin
 *
 * This plugin is only for Smarty3
 * @package Smarty
 * @subpackage PluginsFunction
 */

/**
 * Smarty {paginate} function plugin
 *
 * Type:     function<br>
 * Name:     paginate<br>
 * Purpose:  Generate pagination urls
 *
 * @param array $params parameters
 * @param object $template template object
 * @return string
 */
function smarty_function_paginate($params, $template)
{
    if (empty($params['ms'])) {
        trigger_error("Paginate: missing Medical Specialty");
    }

    $router = VirtualContainer::get()->get('router');
    $translator = VirtualContainer::get()->get('translator');

    // Pagination variables
    $medical_specialty = intval($params['ms']);
    $result_count      = isset($params['count']) ? intval($params['count']) : 0;
    $treatment_service    = !empty($params['tt']) ? intval($params['tt']) : ''; // We want "null", not 0
    $lat               = isset($params['lat']) ? $params['lat'] : '';
    $lng               = isset($params['lng']) ? $params['lng'] : '';
    $location          = isset($params['location']) ? $params['location'] : '';
    $distance          = isset($params['distance']) ? intval($params['distance']) : 0;
    $district          = !empty($params['district']) ? intval($params['district']) : '';
    $page              = isset($params['page']) ? intval($params['page']) : 1; // The *current* page we're on
    $per_page          = isset($params['per_page']) ? intval($params['per_page']) : $GLOBALS['CONFIG']['SEARCH_PAGINATION_NUM'];
    
    if ($result_count <= $per_page) {
        return '';
    }
    
    // URL params
    $params = array(
        'form' => array(
            'medical_specialty_id' => $medical_specialty,
            'treatment_service_id'     => $treatment_service,
            'lat'                  => $lat,
            'lng'                  => $lng,
            'location'             => $location,
            'district_id'          => $district,
            'distance'             => $distance
        )
    );
    
    // Filter empty values, as they're useless
    array_filter($params['form']);

    // Widget slug param
    if (VirtualContainer::get()->get('request_stack')->getCurrentRequest()->attributes->has('_widget')) {
        $url = $router->generate('user_widget', array('parameter_string' => $router->generate('search')), true);
    } else {
        $url = $router->generate('search', [], true);
    }

    $url .= '?' . http_build_query($params);

    return paginate_links(
        array(
            'base'              => $url . '&p=%#%',
            'format'            => '',
            'total'             => ceil($result_count / $per_page), // Force pagination (we don't use individual pages anyway)
            'current'           => $page,
            'add_args'          => '',
            'next_text'         => $translator->trans('ui.search.results.more'),
            'prev_text'         => $translator->trans('ui.search.results.back'),
            'type'              => 'platform',
            'show_page_numbers' => false
        )
    );
}