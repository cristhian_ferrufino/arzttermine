<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

use ArzttermineBundle\DependencyInjection\VirtualContainer;

/**
 * Smarty {get} function plugin
 *
 * Type:     function<br>
 * Name:     get<br>
 * Purpose:  fetches static param from the Container
 * <br>
 *
 * @version 1.0
 * @param array $params parameters
 * @param object $template template object
 * @return string template|null
 */
function smarty_function_get($params, $template)
{
    if (!isset($params['param'])) {
        trigger_error("VirtualContainer: missing 'param'");

        return '';
    }

    if (!VirtualContainer::get()->hasParameter($params['param'])) {
        trigger_error("VirtualContainer: container does not contain param {$params['param']}");

        return '';
    }

    $val = VirtualContainer::get()->getParameter($params['param']);

    // if this parameter is set then return null as it is only used to assign a smarty var
    if (isset($params['set']) && $params['set']) {
        $template->assign($params['set'], $val);
        return '';
    }

    return $val;
}