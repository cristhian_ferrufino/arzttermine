<?php

use ArzttermineBundle\Location\District;

/**
 * Smarty plugin
 *
 * This plugin is only for Smarty3
 * @package Smarty
 * @subpackage PluginsFunction
 */

/**
 * Smarty {districts} function plugin
 *
 * Type:     function<br>
 * Name:     districts<br>
 * Purpose:  Get Districts from the system
 *
 * @param array $params parameters
 * @param object $template template object
 * @return string
 */
function smarty_function_districts($params, $template)
{
    if (!isset($params['assign_to'])) {
        trigger_error("Districts: No data target (missing 'assign_to')");
        return '';
    }
    
    // Var name. Be reasonable
    $assign = $params['assign_to'];
    
    // Filter can be "all" or "one"
    $filter = strtolower(trim(isset($params['show']) ? $params['show'] : 'all'));
    $city   = isset($params['for']) ? $params['for'] : '';
    $id     = isset($params['id']) ? $params['id'] : 0; // This needs to be set if filter is "one"

    $districts = array(); // The result
    $district = new District(); // For querying
    
    // If we want everything...
    if ($filter === 'all') {
        // ... for a city
        if ($city) {
            $districts = District::getCityDistricts($city);
        } else {
            // JUST GIVE ME EVERYTHING
            $districts = $district->findObjects();
        }
    // If we want one thing...
    } elseif ($filter === 'one') {
        $districts = $district->findObject(intval($id));
    }
    
    $template->assign($assign, $districts);
    
    return '';
}