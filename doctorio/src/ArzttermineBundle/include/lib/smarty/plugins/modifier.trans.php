<?php

use ArzttermineBundle\DependencyInjection\VirtualContainer;

/**
 * Type:     modifier<br>
 * Name:     trans<br>
 * Purpose:  Translates string
 * Example:  {"Hello"|trans}
 *
 * @param string $string
 * @param array $params Label replacements
 * @param string $domain
 * @param string $locale
 * 
 * @return string
 */
function smarty_modifier_trans($string, $params = array(), $domain = null, $locale = null)
{
    /** @var \Symfony\Component\Translation\Translator $translator */
    $translator = VirtualContainer::get()->get('translator');

    return $translator->trans($string, $params, $domain, $locale);
}