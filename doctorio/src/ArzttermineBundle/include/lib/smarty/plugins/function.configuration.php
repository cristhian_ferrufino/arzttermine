<?php

use ArzttermineBundle\Core\Configuration;

/**
 * Smarty plugin
 *
 * This plugin is only for Smarty3
 * @package Smarty
 * @subpackage PluginsFunction
 */

/**
 * Smarty {url} function plugin
 *
 * Type:     function<br>
 * Name:     url<br>
 * Purpose:  Return config options
 * Example:  {configuration name='docdir_pay_calendar_button_text'}
 *
 * @param array $params parameters
 * @param object $template template object
 * @return string
 */
function smarty_function_configuration($params, $template)
{
    if (empty($params['name'])) {
        trigger_error("URL: missing name");
    }

    return Configuration::get($params['name']);
}