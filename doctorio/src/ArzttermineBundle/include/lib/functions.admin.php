<?php

use ArzttermineBundle\DependencyInjection\VirtualContainer;
use ArzttermineBundle\Media\Asset;

/**
 * @param ArzttermineBundle\Location\Location|ArzttermineBundle\User\User $object
 * @param $owner_type
 * @param $privilege_id
 * @param $form
 * @param int $selected_id
 * @return string
 */
function form_gallery($object, $owner_type, $privilege_id, $form, $selected_id=0, $viewvars) {
    //if we're out of a request scope do nothing
    if (PHP_SAPI == 'cli') {
        return '';
    }

    $view = VirtualContainer::get()->get('templating');
    $profile = VirtualContainer::get()->get('security.token_storage')->getToken()->getUser();
    $request = VirtualContainer::get()->get('request_stack')->getCurrentRequest();
    $router = VirtualContainer::get()->get('router');

    $_asset = new Asset();
    // if no asset is selected than show the profile_gfx or the last uploaded asset
    $selected_id = (($selected_id==0)?$object->getProfileAssetId():$selected_id);
    $_asset->load($selected_id);
    if (!$_asset->getId()) {
        $selected_id = 0;
    }
    // default: select the first
    if ($selected_id == 0) {
        $selected_id = $_asset->findKey('parent_id=0 AND owner_id='.$object->getId().' AND owner_type='.$owner_type.' ORDER BY created_at DESC LIMIT 1','id');
    }

    // show fotos of the selected event ******************
    $_assets = $_asset->findObjects('owner_id='.$object->getId().' AND owner_type='.$owner_type.' AND parent_id=0 ORDER BY created_at ASC;');
    if (empty($_assets)) {
        $viewvars['asset_dummy_standard'] = getStaticUrl($GLOBALS['CONFIG']['ASSET_GFX_TYPES'][$owner_type]['SIZES'][Asset::GFX_SIZE_STANDARD]['default']);
        $viewvars['asset_dummy_thumbnail'] = getStaticUrl($GLOBALS['CONFIG']['ASSET_GFX_TYPES'][$owner_type]['SIZES'][Asset::GFX_SIZE_THUMBNAIL]['default']);
        $selected_id = 0;
    }

    $data = array(
        'action'    => 'edit',
        'id'        => $object->getId()
    );
    $queryParameters = $request->query->all();
    $_url = $router->generate($request->get('_route'), array_merge($queryParameters, $data));

    $_alturl = ' alturl="'.$_url.'"';
    $_show_profile_link = true;

    $viewvars['assets_gallery_attr_alturl'] = $_alturl;
    $viewvars['assets_gallery_show_profile_link'] = $_show_profile_link;
    $viewvars['gallery_image_start_nr'] = $selected_id;
    $viewvars['assets'] = $_assets;

    return $view->render('admin/_gallery.html.twig', $viewvars);
}
// **********************************************************************
function form_media_upload($url, $id, $form) {
    $view = VirtualContainer::get()->get('templating');

    $viewvars['action'] = 'asset_new';
    $viewvars['url_upload_script'] = $url;
    $viewvars['scriptData'] = '\''.$GLOBALS['CONFIG']['SYSTEM_SESSION_NAME'].'\':\''.session_id().'\',\'action\':\'edit\',\'id\':\''.$id.'\',\'mua\':\'assets_new\',\'musa\':\'save\'';
    $viewvars['form'] = $form;
    $viewvars['url_form_action'] = $url;
    $viewvars['upload_max_filesize'] = ini_get("upload_max_filesize");

    return $view->render('admin/_gallery_form_media_new.html.twig', $viewvars);
}
// **********************************************************************
function check_parameters_media($form) {
    $view = VirtualContainer::get()->get('templating');
    $translator = VirtualContainer::get()->get('translator');

    // check the mime-type/extension
    if (isset($_FILES['upload']['tmp_name'])) {
        $_tmp_name=$_FILES['upload']['tmp_name'];
        $_filename=$_FILES['upload']['name'];
        $_pathinfo = pathinfo($_filename);
        if ($_tmp_name=='') {
            addFlash('danger', $translator->trans('Der Dateiname ist ungültig!'));
        } elseif (!in_array(mb_strtolower($_pathinfo["extension"]), $GLOBALS["CONFIG"]["ASSET_POSSIBLE_FILEEXTENSIONS"])) {
            addFlash('danger', $translator->trans('Die Dateiendung ist ungültig!'));
        } else {
            // save the uploaded asset and create the thumbnail
            $_data['filesize']=$_FILES['upload']['size'];
            if ($_data['filesize']==0) {
                addFlash('danger', $translator->trans('Die Dateigröße ist fehlerhaft!'));
            }
        }
    }

    return $form;
}

/**
 * Adds a flash message to the current session for type.
 *
 * @param string $type    The type
 * @param string $message The message
 *
 * @throws \LogicException
 */
function addFlash($type, $message)
{
    if (!VirtualContainer::get()->has('session')) {
        throw new \LogicException('You can not use the addFlash method if sessions are disabled.');
    }

    VirtualContainer::get()->get('session')->getFlashBag()->add($type, $message);
}

/**
 * Adds a flash message to the current session for type.
 *
 * @param string $type    The type
 * @param string $message The message
 *
 * @throws \LogicException
 */
function hasFlash()
{
    if (!VirtualContainer::get()->has('session')) {
        throw new \LogicException('You can not use the hasFlash method if sessions are disabled.');
    }

    return count(VirtualContainer::get()->get('session')->getFlashBag()->keys()) > 0;
}

// **********************************************************************
/**
 * @param \Arzttermine\UserBundle\Entity\Doctor||Arzttermine\LocationBundle\Entity\Location $object
 * @param string $privilege_id
 * @param int $owner_type
 * @return string
 */
function get_gallery($object, $privilege_id, $owner_type) {
    //if we're out of a request scope do nothing
    if (PHP_SAPI == 'cli') {
        return '';
    }
    $profile = VirtualContainer::get()->get('security.token_storage')->getToken()->getUser();
    $request = VirtualContainer::get()->get('request_stack')->getCurrentRequest();
    $translator = VirtualContainer::get()->get('translator');
    $pathInfo = $request->getPathInfo();

    // Temporary workaround until we refactor Assets/admin
    // Only doctors and locations have assets so far
    if ($object instanceof \Arzttermine\UserBundle\Entity\Doctor) {
        $objectManager = VirtualContainer::get()->get('arzttermine_user.user');
    } else {
        $objectManager = VirtualContainer::get()->get('arzttermine_location.location');
    }

    $_uploading_ok = $request->get('uploading_ok');
    $_uploading_errors = $request->get('uploading_errors');
    if ($_uploading_ok || $_uploading_errors) {
        if ($_uploading_errors > 0) {
            addFlash('danger', $translator->trans('Beim Bilderupload sind Fehler aufgetreten<br />Erfolgreich hochgeladen: '.$_uploading_ok.'<br />Nicht hochgeladen: '.$_uploading_errors));
        } else {
            addFlash('notice', $translator->trans('Alle '.$_uploading_ok.' Bilder wurden erfolgreich hochgeladen!'));
        }
    }

    $form = $request->get('form');
    $_mediaupload_action = $request->get('mua');
    $_mediaupload_sub_action = $request->get('musa');
    $data = array(
        'action'    => 'edit',
        'id'        => $object->getId()
    );
    $queryString =  http_build_query($data);
    $_url = $pathInfo.((strpos($pathInfo, '?') === false) ? '?' : '&').$queryString;
    $html='';

    switch ($_mediaupload_action) {
        case 'asset_new':
        case 'assets_new':
            if (!$profile->getId()) {
                addFlash('danger', $translator->trans('Du musst eingeloggt sein!'));
                if ($_mediaupload_action == 'assets_new') {
                    // multiupload: send error_code and exit
                    echo "0";
                    exit;
                }
                break;
            }

            if ($_mediaupload_sub_action=="save" && $request->isMethod('post')) {
                $_asset_id=0;
                // save the asset to fs
                $form = check_parameters_media($form);
                if (hasFlash()) {
                    addFlash('danger', $translator->trans('Beachte bitte die Fehler!'));
                    $html .= form_media_upload($_url, 0, $form);
                } else {
                    // everything seems fine => store the asset *************************************
                    if (isset($_FILES['upload']['tmp_name'])) {
                        $_data = array();
                        $_data['gallery_id'] = 0;
                        $_data['description'] = isset($form['description']) ? $form['description'] : '';
                        $_asset = new Asset();
                        $_asset->saveNewParent($owner_type, $object->getId(), $_data, $_FILES['upload']);
                        if (!$_asset->getId()) {
                            addFlash('danger', $translator->trans('Beim Speichern ist ein Fehler aufgetreten!'));
                            if ($_mediaupload_action == 'assets_new') {
                                // multiupload: send error_code and exit
                                echo "0";
                                exit;
                            }
                        } else {
                            $_asset_id = $_asset->getId();
                            addFlash('notice', $translator->trans('Das Bild wurde hochgeladen!'));
                            if ($_mediaupload_action == 'assets_new') {
                                // multiupload: send error_code and exit
                                echo "1";
                                exit;
                            }
                        }
                    }
                    // the profile_asset should be recalculated now
                    #$object->loadProfileAsset();
                }
            } else {
                // show ONLY the form
                $html .= form_media_upload($_url, $object->getId(), $form);
            }
            break;

        case 'asset_update_description':
            if (!$profile->getId()) {
                addFlash('danger', $translator->trans('Du musst eingeloggt sein!'));
                break;
            }
            $_asset = new Asset($request->get("asset_id"));
            if (!$_asset->getId()) {
                addFlash('danger', $translator->trans('Das Bild existiert nicht!'));
            } else {
                // check if this user is allowed to edit the description of this asset
                if ($_asset->getCreatedBy() != $profile->getId()) {
                    addFlash('danger', $translator->trans('Du kannst nur deine eigenen Assets bearbeiten!'));
                } else {
                    // update the properties
                    $_asset->setDescription($form["description"]);
                    $_asset->setUpdatedAt((new \ArzttermineBundle\Core\DateTime())->getDateTime());
                    $_asset->setUpdatedBy($profile->getId());
                    $_asset->save(array("description","updated_at",'updated_by'));
                    addFlash('notice', $translator->trans('Die Beschreibung wurde geändert!'));
                    $_asset_id = $_asset->getId();
                }
            }
            break;

        case 'asset_update_gallery_profile':
        case 'asset_update_profile':
            if (!$profile->getId()) {
                addFlash('danger', $translator->trans('Du musst eingeloggt sein!'));
                break;
            }
            $_asset = new Asset($request->get('asset_id'));
            if (!$_asset->getId() || $_asset->getParentId()!=0) {
                addFlash('danger', $translator->trans('Das Bild existiert nicht!'));
            } else {
                $objectManager->updateProfileAsset($object, $_asset);

                addFlash('notice', $translator->trans('Das Asset ist nun das neue Profilbild!'));
                $_asset_id = $_asset->getId();
            }
            break;

        case 'asset_delete':
            if (!$profile->getId()) {
                addFlash('danger', $translator->trans('Du musst eingeloggt sein!'));
                break;
            }
            $_asset = new Asset($request->get("asset_id"));
            if (!$_asset->getId()) {
                addFlash('danger', $translator->trans('Das Bild exitiert nicht!'));
            } else {
                if ($_asset->getCreatedBy() != $profile->getId()) {
                    addFlash('danger', $translator->trans('Du kannst nur deine eigenen Bilder bearbeiten!'));
                } else {
                    $_delete_profile_gfx = false;
                    // is this the user profile asset?
                    if ($_asset->getId() == $object->getProfileAssetId()) {
                        $_delete_profile_gfx = true;
                    }
                    if ($_asset->deleteWithChildren()) {
                        addFlash('notice', $translator->trans('Das Asset wurde gelöscht!'));
                        if ($_delete_profile_gfx) {
                            $em = VirtualContainer::get()->get('doctrine')->getManager();
                            $object->setProfileAssetId(0);
                            $object->setProfileAssetFilename('');
                            $em->persist($object);
                            $em->flush();
                        }
                    }
                }
            }
            break;

        case 'gfx_rotate_left':
        case 'gfx_rotate_right':
            if (!$profile->getId()) {
                addFlash('danger', $translator->trans('Du musst eingeloggt sein!'));
                break;
            }
            $_asset = new Asset($request->get("asset_id"));
            if (!$_asset->getId() || $_asset->getParentId()!=0) {
                addFlash('danger', $translator->trans('Das Bild exitiert nicht!'));
            } else {
                // check if this user is allowed to rotate the gfx
                $_degrees = ($_mediaupload_action=='gfx_rotate_left')?90:270;
                if ($_asset->rotateOriginGfx($_asset->getId(), $_degrees)) {
                    addFlash('notice', $translator->trans('Das neue Profilbild wurde festgelegt!'));
                }
                $_asset_id = $_asset->getId();
            }
            break;

        // ************************************************
    }

    $viewvars['form_media_upload'] = form_media_upload($_url, $object->getId(), $form);

    return form_gallery($object, $owner_type, $privilege_id, $form, isset($_asset_id) ? $_asset_id : 0, $viewvars);
}
