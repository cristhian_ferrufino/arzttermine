<?php

namespace ArzttermineBundle\Form;

use ArzttermineBundle\User\User;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

class PatientProfileType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options = [])
    {
        $builder
            ->add('gender', ChoiceType::class, [
                'choices' => [
                    'word.gender.male' => User::GENDER_MALE,
                    'word.gender.female' => User::GENDER_FEMALE
                ],
                'expanded' => true,
                'block_name' => 'gender_option',
                'label' => 'ui.form.gender.label',
                'constraints' => [new NotBlank()],
                'invalid_message' => 'ui.form.gender.error_message',
                'attr' => [
                    'data-fv-message' => 'ui.form.gender.error_message'
                ]
            ])
            ->add('first_name', TextType::class, [
                'label' => 'ui.form.first_name.label',
                'constraints' => [new NotBlank(), new Length(['min' => 2])],
                'invalid_message' => 'ui.form.first_name.error_message',
                'attr' => [
                    'data-fv-message' => 'ui.form.first_name.error_message'
                ]
            ])
            ->add('last_name', TextType::class, [
                'label' => 'ui.form.last_name.label',
                'constraints' => [new NotBlank(), new Length(['min' => 2])],
                'invalid_message' => 'ui.form.last_name.error_message',
                'attr' => [
                    'data-fv-message' => 'ui.form.last_name.error_message'
                ]
            ])
            ->add('email', EmailType::class, [
                'label' => 'ui.patient.dashboard.account.form.email.label',
                'constraints' => [new NotBlank(), new Email()],
                'disabled' => true
            ])
            ->add('phone', TextType::class, [
                'label' => 'ui.patient.dashboard.account.form.phone.label',
                'constraints' => [new NotBlank(), new Length(['min' => 6])]
            ])
            ->add('new_password', PasswordType::class, [
                'label' => 'ui.form.new_password.label',
                'invalid_message' => 'ui.form.new_password.error_message',
                'required' => false,
                'mapped' => false,
                'constraints' => [
                    new Length(['min' => 8]), // Minimum 8 characters
                    new Regex(['pattern' => '/[[:alpha:]]+/']), // with 1 letter
                    new Regex(['pattern' => '/[[:digit:]]+/']) // and 1 number
                ]
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Arzttermine\UserBundle\Entity\Patient'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'patient_profile';
    }
}