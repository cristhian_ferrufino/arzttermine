<?php

namespace ArzttermineBundle\Form;

use ArzttermineBundle\Media\Avatar;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;

class DoctorAvatarUploadType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options = [])
    {
        $builder
            ->add('file', FileType::class, [
                'required' => true,
                'constraints' => [
                    new Image([
                        'minWidth' => 60,
                        'maxWidth' => 560,
                        'minHeight' => 80,
                        'maxHeight' => 600,
                        'mimeTypes' => ['image/jpeg', 'image/png'],
                        'minHeightMessage' => 'ui.file.error.min_height',
                        'maxHeightMessage' => 'ui.file.error.max_height',
                        'minWidthMessage' => 'ui.file.error.min_width',
                        'maxWidthMessage' => 'ui.file.error.max_width',
                        'mimeTypesMessage' => 'ui.file.error.mime_type',
                    ])
                ]
            ])
            ->add('doctorId', HiddenType::class, [
                'required' => true,
            ])
            ->add('practiceId', HiddenType::class, [
                'required' => true,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Avatar::class,
        ]);
    }
}
