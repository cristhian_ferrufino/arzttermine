<?php

namespace ArzttermineBundle\Form;

use ArzttermineBundle\Form\Validator\Constraint\EmailAvailable;
use ArzttermineBundle\MedicalSpecialty\MedicalSpecialty;
use ArzttermineBundle\User\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

class DoctorRegistrationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options = [])
    {
        $builder
            ->add(
                'gender', ChoiceType::class, [
                'choices' => [
                    'word.gender.male' => User::GENDER_MALE,
                    'word.gender.female' => User::GENDER_FEMALE
                ],
                'expanded' => true,
                'label' => 'ui.form.gender.label',
                'constraints' => [new NotBlank()],
                'invalid_message' => 'ui.form.gender.error_message',
                'attr' => [
                    'class' => 'form-inline',
                    'data-fv-message' => 'ui.form.gender.error_message'
                ],
            ])
            ->add('title', TextType::class, [
                'label' => 'ui.doctor.dashboard.register.form.title.label',
                'constraints' => [new Length(['min' => 2])]
            ])
             ->add('first_name', TextType::class, [
                'label' => 'ui.form.first_name.label',
                'constraints' => [new NotBlank(), new Length(['min' => 2])],
                'invalid_message' => 'ui.form.first_name.error_message',
                 'attr' => [
                     'data-fv-message' => 'ui.form.first_name.error_message'
                 ]
            ])
            ->add('last_name', TextType::class, [
                'label' => 'ui.form.last_name.label',
                'constraints' => [new NotBlank(), new Length(['min' => 2])],
                'invalid_message' => 'ui.form.last_name.error_message',
                'attr' => [
                    'data-fv-message' => 'ui.form.last_name.error_message'
                ]
            ])
            ->add('email', EmailType::class, [
                'label' => 'ui.doctor.dashboard.register.form.email.label',
                'constraints' => [new NotBlank(), new Email(), new EmailAvailable()]
            ])
            ->add('phone', TextType::class, [
                'label' => 'ui.doctor.dashboard.register.form.phone.label',
                'constraints' => [new NotBlank(), new Length(['min' => 6])]
            ])
            ->add('location_name', TextType::class, [
                'label' => 'ui.doctor.dashboard.register.form.practice.label',
                'constraints' => [new NotBlank(), new Length(['min' => 2])]
            ])
            ->add('location_city', TextType::class, [
                'label' => 'ui.doctor.dashboard.register.form.city.label',
                'constraints' => [new NotBlank(), new Length(['min' => 2])]
            ])
            ->add('location_street', TextType::class, [
                'label' => 'ui.doctor.dashboard.register.form.street.label',
                'constraints' => [new NotBlank(), new Length(['min' => 2])]
            ])
            ->add('location_zip', TextType::class, [
                'label' => 'ui.doctor.dashboard.register.form.zip.label',
                'constraints' => [new NotBlank(), new Length(['min' => 2])]
            ])
            ->add('phone_mobile', TextType::class, [
                'label' => 'ui.doctor.dashboard.register.form.mobile.label',
                'constraints' => [new Length(['min' => 6])]
            ])
            // But this *is* required at registration
            ->add('medical_specialty_ids', ChoiceType::class, [
                'choices' => array_flip(MedicalSpecialty::getOptions()),
                'label' => 'ui.doctor.dashboard.register.form.medical_specialty.label',
                'placeholder' => 'form.please_choose',
                'constraints' => [new NotBlank()],
                'choice_translation_domain' => false //disables automatic options translation, because currently we store these in DB
            ])
            ->add('registration_reasons', TextareaType::class, [
                'label' => 'ui.doctor.dashboard.register.step3.registration_reasons',
                'required' => false
            ])
            ->add('registration_funnel', ChoiceType::class, [
                'choices' => [
                    'ui.doctor.dashboard.register.step3.registration_funnel.options.colleagues' => 'colleagues',
                    'ui.doctor.dashboard.register.step3.registration_funnel.options.internet' => 'internet',
                    'ui.doctor.dashboard.register.step3.registration_funnel.options.advertising' => 'advertising',
                    'ui.doctor.dashboard.register.step3.registration_funnel.options.website' => 'website',
                    'ui.doctor.dashboard.register.step3.registration_funnel.options.other' => 'other'
                ],
                'label' => 'ui.doctor.dashboard.register.step3.registration_funnel.label',
                'placeholder' => 'form.please_choose',
                'required' => false
            ])
            ->add('registration_funnel_other', TextType::class, [
                'attr' => ['class' => 'hidden'],
                'label' => false,
                'required' => false
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Arzttermine\UserBundle\Entity\PendingDoctor',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'doctor_registration';
    }
}