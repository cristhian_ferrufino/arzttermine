<?php

namespace ArzttermineBundle\Form;

use ArzttermineBundle\Form\Validator\Constraint\EmailAvailable;
use ArzttermineBundle\User\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class PatientRegistrationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options = [])
    {
        $builder
            ->add('gender', ChoiceType::class, [
                'choices' => [
                    'word.gender.male' => User::GENDER_MALE,
                    'word.gender.female' => User::GENDER_FEMALE
                ],
                'expanded' => true,
                'block_name' => 'gender_option',
                'label' => 'ui.form.gender.label',
                'constraints' => [new NotBlank()],
                'invalid_message' => 'ui.form.gender.error_message',
                'attr' => [
                    'class' => 'form-inline',
                    'data-fv-message' => 'ui.form.gender.error_message'
                ]
            ])
            ->add('first_name', TextType::class, [
                'label' => 'ui.form.first_name.label',
                'constraints' => [new NotBlank(), new Length(['min' => 2])],
                'invalid_message' => 'ui.form.first_name.error_message',
                'attr' => [
                    'data-fv-message' => 'ui.form.first_name.error_message'
                ]
            ])
            ->add('last_name', TextType::class, [
                'label' => 'ui.form.last_name.label',
                'constraints' => [new NotBlank(), new Length(['min' => 2])],
                'invalid_message' => 'ui.form.last_name.error_message',
                'attr' => [
                    'data-fv-message' => 'ui.form.first_name.error_message'
                ]
            ])
            ->add('email', TextType::class, [
                'label' => 'ui.form.email.label',
                'constraints' => [new NotBlank(), new Email(), new EmailAvailable()]
            ])
            ->add('phone', TextType::class, [
                'label' => 'ui.form.phone.label',
                'constraints' => [new NotBlank(), new Length(['min' => 6])]
            ])
            ->add(
                'password', PasswordType::class, [
                    'constraints' => [
                        new NotBlank(),
                        new Length(['min' => 8]), // Minimum 8 characters
                        new Regex(['pattern' => '/[[:alpha:]]+/']), // with 1 letter
                        new Regex(['pattern' => '/[[:digit:]]+/']) // and 1 number
                    ],
                    'label' => 'ui.form.password.label'
                ]
            )
            ->add(
                'agb', CheckboxType::class, [
                'label' => 'ui.form.legal_confirmation.label',
                'constraints' => [new IsTrue(), new NotBlank()]
            ]
            )
            ->add('newsletter_subscription', CheckboxType::class, [
                'label' => 'ui.form.subscribe_newsletter.label',
                'required' => false
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'patient_registration';
    }
}