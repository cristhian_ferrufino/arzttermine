<?php

namespace ArzttermineBundle\Form\Validator\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class EmailAvailable extends Constraint
{
    /**
     * @var string
     */
    public $message = 'This email address is unavailable';
}