<?php

namespace ArzttermineBundle\Form\Validator\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class DoesNotContainValidator extends ConstraintValidator
{
    /**
     * @var array
     */
    protected $patterns = [
        DoesNotContain::TYPE_URL => '/((https?\:\/\/)|(www[ ]*[.]{1})){1}[ -a-z0-9]+[.]{1}[ ]*[a-z]{2,4}/i',
        DoesNotContain::TYPE_EMAIL_ADDRESS => '/([\s]*)([_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*([ ]+|)@([ ]+|)([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,}))([\s]*)/i',
        DoesNotContain::TYPE_PHONE_NUMBER => '/^([\+][0-9]{1,3}[ \.\-])?([\(]{1}[0-9]{1,6}[\)])?([0-9 \.\-\/]{3,20})((x|ext|extension)[ ]?[0-9]{1,4})?$/'

    ];

    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint)
    {
        /** @var DoesNotContain $constraint */
        foreach ([DoesNotContain::TYPE_URL, DoesNotContain::TYPE_EMAIL_ADDRESS, DoesNotContain::TYPE_PHONE_NUMBER] as $type) {
            if (preg_match($this->patterns[$type], $value) === 1) {
                $this->context->addViolation($constraint->getMessage($type));
            }
        }
    }

}