<?php

namespace ArzttermineBundle\Form\Validator\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class DoesNotContain extends Constraint
{
    // Types to check for
    const TYPE_URL = 1;
    const TYPE_EMAIL_ADDRESS = 2;
    const TYPE_PHONE_NUMBER = 4;

    /**
     * @var array
     */
    protected $messages = [
        self::TYPE_URL => 'doesnotcontain.url',
        self::TYPE_EMAIL_ADDRESS => 'doesnotcontain.email',
        self::TYPE_PHONE_NUMBER => 'doesnotcontain.phone'
    ];

    /**
     * @var string
     */
    public $message = 'This field contains invalid data';

    /**
     * @var int
     */
    public $flags = 0;

    /**
     * @param int $type
     *
     * @return string
     */
    public function getMessage($type)
    {
        if (array_key_exists($type, $this->messages)) {
            return $this->messages[$type];
        }

        return '';
    }
}