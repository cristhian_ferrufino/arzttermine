<?php

namespace ArzttermineBundle\Form\Validator\Constraint;

use ArzttermineBundle\User\User;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class EmailAvailableValidator extends ConstraintValidator
{
    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint)
    {
        // @todo Decouple User
        if (!User::isEmailAvailable($value)) {
            $this->context->addViolation($constraint->message);
        }
    }

}