<?php

namespace ArzttermineBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ContactType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options = [])
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'ui.contact.form.name',
                'constraints' => [new NotBlank(), new Length(['min' => 2])],
                'invalid_message' => 'ui.form.first_name.error_message',
                'attr' => [
                    'data-fv-message' => 'ui.form.first_name.error_message'
                ]
            ])
            ->add('email', EmailType::class, [
                'label' => 'ui.contact.form.email',
                'constraints' => [new NotBlank(), new Email()],
                'invalid_message' => 'ui.form.email.error_message',
                'attr' => [
                    'data-fv-message' => 'ui.form.email.error_message'
                ]
            ])
            ->add('subject', TextType::class, [
                'label' => 'ui.contact.form.subject',
                'constraints' => [new NotBlank(), new Length(['min' => 2])]
            ])
            ->add('message', TextareaType::class, [
                'label' => 'ui.contact.form.message',
                'constraints' => [new NotBlank(), new Length(['min' => 4])]
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Arzttermine\UserBundle\Entity\Contact',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'contact';
    }
}