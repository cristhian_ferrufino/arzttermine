<?php

namespace ArzttermineBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class ReviewType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options = [])
    {
        $builder
            ->add('patient_name', TextType::class, [
                'label' => 'ui.review_doctor.form.patient_name.label',
                'invalid_message' => 'ui.review_doctor.form.patient_name.error_message',
                'constraints' => [new Length(['min' => 2])], // If this is empty, it becomes "anonymous"
                'attr' => [
                    'data-fv-message' => 'ui.review_doctor.form.patient_name.error_message'
                ]
            ])
            ->add('rate_text', TextareaType::class, [
                'label' => 'ui.review_doctor.form.rate_text.label',
                'invalid_message' => 'ui.review_doctor.form.rate_text.error_message',
                'constraints' => [new NotBlank(), new Length(['min' => 2])],
                'attr' => [
                    'placeholder' => 'ui.review_doctor.form.rate_text.description',
                    'data-fv-message' => 'ui.review_doctor.form.rate_text.error_message',
                    'cols' => 50,
                    'rows' => 5
                ]
            ])
            ->add('rating_1', ChoiceType::class, [
                'expanded' => true,
                'choices' => [1, 2, 3, 4, 5],
                'choice_label' => function() { return 'i'; },
                'label' => 'ui.review_doctor.form.rating_1.label',
                'invalid_message' => 'ui.review_doctor.form.rating_1.error_message',
                'attr' => [
                    'data-fv-message' => 'ui.review_doctor.form.rating_1.error_message'
                ]
            ])
            ->add('rating_2', ChoiceType::class, [
                'expanded' => true,
                'choices' => [1, 2, 3, 4, 5],
                'choice_label' => function() { return 'i'; },
                'label' => 'ui.review_doctor.form.rating_2.label',
                'invalid_message' => 'ui.review_doctor.form.rating_2.error_message',
                'attr' => [
                    'data-fv-message' => 'ui.review_doctor.form.rating_2.error_message'
                ]
            ])
            ->add('rating_3', ChoiceType::class, [
                'expanded' => true,
                'choices' => [1, 2, 3, 4, 5],
                'choice_label' => function() { return 'i'; },
                'label' => 'ui.review_doctor.form.rating_3.label',
                'invalid_message' => 'ui.review_doctor.form.rating_3.error_message',
                'attr' => [
                    'data-fv-message' => 'ui.review_doctor.form.rating_3.error_message'
                ]
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Arzttermine\ReviewBundle\Entity\Review',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'review';
    }
}