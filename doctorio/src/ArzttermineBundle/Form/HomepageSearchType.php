<?php

namespace ArzttermineBundle\Form;

use ArzttermineBundle\MedicalSpecialty\MedicalSpecialty;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class HomepageSearchType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options = [])
    {
        $builder
            ->add('medical_specialty_id', ChoiceType::class, [
                'placeholder' => 'form.please_choose',
                'choices' => [],
                'label' => 'ui.search.specialty.label',
                'required' => false,
                'attr' => ['class' => 'form-control'],
                'label_attr' => ['class' => 'form-control-label'],
                'choice_translation_domain' => false //disables automatic options translation, because currently we store these in DB
            ])
            ->add('location', TextType::class, [
                'label' => 'ui.search.place.label',
                'attr' => [
                    'placeholder' => 'ui.search.place.placeholder',
                    'class' => 'form-control'
                ],
                'required' => false,
                'label_attr' => ['class' => 'form-control-label']
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'search';
    }

}
