<?php

namespace ArzttermineBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

class LoginType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options = [])
    {
        $builder
            ->add('_username', EmailType::class, [
                'label' => 'ui.form.email.label',
                'constraints' => [new NotBlank(), new Email()]
            ])
            ->add('_password', PasswordType::class, [
                'label' => 'ui.form.password.label',
                'invalid_message' => 'ui.form.password.error_message'
            ])
            ->add('referrer', HiddenType::class);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }

}