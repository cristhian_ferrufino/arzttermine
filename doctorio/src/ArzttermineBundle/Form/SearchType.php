<?php

namespace ArzttermineBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;


class SearchType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options = [])
    {
        $builder
            // Location fields
            ->add('location', TextType::class, ['constraints' => [new Length(['min' => 2]), new NotBlank()]])
            ->add('district_id', IntegerType::class)
            ->add('distance', IntegerType::class)

            // Medical fields
            ->add('medical_specialty_id', IntegerType::class, ['constraints' => new NotBlank()])
            ->add('treatment_service_id', IntegerType::class);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'search';
    }
}