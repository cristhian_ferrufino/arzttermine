<?php
namespace AppBundle\Tests\Controller;

use Arzttermine\AppointmentBundle\Entity\Appointment;
use Arzttermine\BookingBundle\Entity\Booking;
use Arzttermine\ReviewBundle\Entity\Review;
use Arzttermine\ReviewBundle\Repository\ReviewRepository;
use Arzttermine\UserBundle\Entity\Doctor;

class BookingControllerTest extends AbstractControllerTest
{
    protected $url = 'appointment';

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        parent::setUp();

        $this->urlParams = $this->getBookableAppointmentParams();
        $this->email = time().$this->email;
    }

    private function getBookableAppointmentParams()
    {
        $search = $this->container->get('arzttermine_search.search');

        $medicalSpecialtySlug = 'dentista';
        $locationSlug = 'madrid';
        $dateStart = (new \DateTime('now'))->format('Y-m-d');
        $coords = $this->container->get('arzttermine_search.search')->getCoordinatesForAddress($locationSlug);
        $medicalSpecialty = $this->container->get('doctrine')->getRepository('ArzttermineUserBundle:MedicalSpecialty')->findOneBySlug($medicalSpecialtySlug);

        $search->setDateBounds($dateStart);
        $search->setLat($coords['lat']);
        $search->setLng($coords['lng']);
        $search->setMedicalSpecialty($medicalSpecialty);
        $search->setTreatmentService($medicalSpecialty->getTreatmentServices()->first());

        $results = $search->searchAppointments()->getProviders();

        /* @var \Arzttermine\SearchBundle\Model\Provider $searchItem */
        foreach ($results as $searchItem) {
            $appointments = $searchItem->getAppointments();
            if (count($appointments) > 0) {
                break;
            }
        }

        /** @var Appointment $appointment */
        $appointment = array_shift($appointments);

        if (empty($appointment)) {
            $this->fail('No appointment data to test');
        }

        $treatmentServices = $appointment->getTreatmentServices();
        if ($treatmentService = array_shift($treatmentServices)) {
            $bookingparams['tt'] = $treatmentService->getId();
        }

        $bookingparams['u'] = $appointment->getDoctor()->getId();
        $bookingparams['l'] = $appointment->getLocation()->getId();
        $bookingparams['m'] = $medicalSpecialty->getId();
        $bookingparams['s'] = $appointment->getStartTime()->format('Ymd\THis');

        return $bookingparams;
    }

    public function testPageHasSmallFooterNavigation()
    {
        $crawler = $this->client->request('GET', $this->router->generate($this->url, $this->urlParams));

        $this->assertEquals(3, $crawler->filter('footer')->eq(0)->filter('a')->count());
        $this->assertEquals(1, $crawler->filter('footer a:contains("'.$this->translator->trans('ui.footer.terms_of_use').'")')->count());
        $this->assertEquals($this->router->generate('page_terms_of_use'), $crawler->filter('footer a:contains("'.$this->translator->trans('ui.footer.terms_of_use').'")')->attr('href'));
        $this->assertEquals(1, $crawler->filter('footer a:contains("'.$this->translator->trans('ui.footer.imprint').'")')->count());
        $this->assertEquals($this->router->generate('page_imprint'), $crawler->filter('footer a:contains("'.$this->translator->trans('ui.footer.imprint').'")')->attr('href'));
        $this->assertEquals(1, $crawler->filter('footer a:contains("'.$this->translator->trans('ui.footer.data_security').'")')->count());
        $this->assertEquals($this->router->generate('page_data_security'), $crawler->filter('footer a:contains("'.$this->translator->trans('ui.footer.data_security').'")')->attr('href'));

        $this->assertEquals(0, $crawler->filter('footer a:contains("'.$this->translator->trans('ui.footer.contact').'")')->count());
        $this->assertEquals(0, $crawler->filter('footer a:contains("Facebook")')->count());
        $this->assertEquals(0, $crawler->filter('footer a:contains("Twitter")')->count());
    }

    public function testBookingWithoutDataShowsAlertBox()
    {
        $crawler = $this->client->request('GET', $this->router->generate($this->url, $this->urlParams));
        $form = $crawler->selectButton($this->translator->trans('ui.booking.button_book_appointment'))->form();

        // submit the form
        $crawler = $this->client->submit($form);

        $this->assertEquals($this->translator->trans('notice.error.check_your_form_data'), $crawler->filter('.alert-danger')->text());
    }

    public function testBookingAnAppointmentWithData()
    {
        $crawler = $this->client->request('GET', $this->router->generate($this->url, $this->urlParams));
        $form = $crawler->selectButton($this->translator->trans('ui.booking.button_book_appointment'))->form();

        $form['booking_form[gender]'] = 0;
        $form['booking_form[firstName]'] = $this->firstName;
        $form['booking_form[lastName]'] = $this->lastName;
        $form['booking_form[email]'] = $this->email;
        $form['booking_form[phone]'] = '0123456789';
        $form['booking_form[treatment_service_id]'] = isset($this->urlParams['tt']) ? $this->urlParams['tt'] : null;
        $form['booking_form[insuranceProvider]'] = 1;
        $form['booking_form[returningVisitor]'] = 0;
        $form['booking_form[agb]'] = 1;
        $form['booking_form[newsletterSubscription]'] = 1;

        // submit the form
        $this->client->submit($form);

        //load created booking
        /** @var Booking $booking */
        $booking = $this->container->get('doctrine')->getManager()->getRepository(
            'ArzttermineBookingBundle:Booking'
        )->findOneByEmail($this->email);

        $this->assertInstanceOf('Arzttermine\BookingBundle\Entity\Booking', $booking);
        $this->assertTrue(
            $this->client->getResponse()->isRedirect(
                $this->router->generate('booking_profile', ['slug' => $booking->getSlug()])
            )
        );
        $crawler = $this->client->followRedirect();
        //echo $this->client->getResponse()->getContent();

        $this->assertEquals(0, $crawler->filter('.alert-danger')->count());
        if ($booking->getIntegrationId() == 8) {
                $this->assertEquals(1, $crawler->filter('.row h2:contains("' . $this->translator->trans('ui.booking_profile.confirmation.appointment_enquiry') . '")')->count());
                $this->assertEquals($this->translator->trans('ui.booking_profile.info_block.appointment_enquiry_statement'), trim($crawler->filter('.row .highlight-bar strong')->text()));
        }

        if ($booking->getIntegrationId() == 1) {
            $this->assertEquals(1, $crawler->filter('.row h2:contains("' . $this->translator->trans('ui.booking_profile.confirmation.booking') . '")')->count());
            $this->assertEquals($this->container->get('arzttermine.datetime_utilities')->getWeekdayDateTimeText($booking->getAppointmentStartAt()), trim($crawler->filter('.row .highlight-bar strong')->text()));
        }

        return $booking;
    }

    /**
     * @depends testBookingAnAppointmentWithData
     */
    public function testReviewPageForBooking(Booking $booking)
    {
        /** @var Doctor $doctor */
        $doctor = $booking->getDoctor();

        $crawler = $this->client->request('GET', $this->router->generate('doctor_review', ['slug' => $booking->getSlug()]));

        $this->assertEquals(0, $crawler->filter('.alert-danger')->count());
        $this->assertEquals(1, $crawler->filter('.row h2:contains("'.$this->translator->trans('ui.review_doctor.header').'")')->count());
        $this->assertEquals(1, $crawler->filter('.row h3:contains("'.$doctor->getFirstName().' '.$doctor->getLastName().'")')->count());

        $form = $crawler->selectButton($this->translator->trans('ui.review_doctor.form.button_submit'))->form();

        $form['review[patient_name]'] = $this->firstName.' '.$this->lastName;
        $form['review[rate_text]'] = 'this is a test review';
        $form['review[rating_1]'] = 4;
        $form['review[rating_2]'] = 3;
        $form['review[rating_3]'] = 2;

        // submit the form
        $this->client->submit($form);
        $this->assertTrue($this->client->getResponse()->isRedirect($this->router->generate('doctor_profile', ['slug' => $doctor->getSlug()])));
        $crawler = $this->client->followRedirect();
        $this->assertEquals(1, $crawler->filter('.alert-success')->count());
        $this->assertEquals($this->translator->trans('ui.review_doctor.controller.thank_you'), $crawler->filter('.alert-success')->text());

        //approve review is shown on doctors profile page
        /** @var ReviewRepository $reviewRepository */
        $em = $this->container->get('doctrine')->getManager();
        $reviewRepository = $this->container->get('arzttermine_review.review_repository');

        /** @var Review $review */
        $review = $reviewRepository->findOneByBooking($booking);
        $review->setApprovedAt(new \DateTime('yesterday'));
        $em->persist($review);
        $em->flush();

        $doctor = $review->getDoctor();

        // $user->updateRatings(); // @todo Update when ratings are implemented
        $crawler = $this->client->request('GET', $this->router->generate('doctor_profile', ['slug' => $doctor->getSlug()]));
        $this->assertEquals($form['review[rate_text]']->getValue(), trim($crawler->filter('.rating .rating-content blockquote[itemprop=reviewBody]')->last()->text()));

        //remove test data from db
        $this->deleteLoginData($this->email);
        $this->deleteBookingData($this->email);
    }
}
