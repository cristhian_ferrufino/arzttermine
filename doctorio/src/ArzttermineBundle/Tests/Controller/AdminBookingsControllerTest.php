<?php
namespace AppBundle\Tests\Controller;


use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class AdminBookingsControllerTest extends AbstractControllerTest
{
    protected $url = 'admin_bookings';
    protected $forceHttps = true;

    public function tearDown()
    {
        parent::tearDown();

        $this->deleteLoginData($this->email);
    }

    public function testPageIsSuccessful()
    {
        $this->loginUser();

        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }

    public function testPageHasHeaderNavigation()
    {
        $crawler = $this->loginUser();

        $this->assertEquals(1, $crawler->filter('.dropdown a:contains("'.$this->translator->trans('admin.sidebar.settings').'")')->count());
        $this->assertEquals($this->router->generate('admin_users', ['action' => 'edit', 'id' => $this->user->getId()]), $crawler->filter('.dropdown a:contains("'.$this->translator->trans('admin.sidebar.settings').'")')->attr('href'));

        $this->assertEquals(1, $crawler->filter('.dropdown a:contains("'.$this->translator->trans('admin.sidebar.logout').'")')->count());
        $this->assertEquals($this->router->generate('fos_user_security_logout'), $crawler->filter('.dropdown a:contains("'.$this->translator->trans('admin.sidebar.logout').'")')->attr('href'));

        $this->assertEquals(1, $crawler->filter('#side-menu .nav-link.active')->count());
        $this->assertEquals($this->router->generate($this->url), $crawler->filter('#side-menu .nav-link.active')->attr('href'));
    }

    public function testPageHasBookingListNavigation()
    {
        $crawler = $this->loginUser();

        $this->assertEquals(1, $crawler->filter('#content-right .list-navigation')->count());
        $this->assertGreaterThanOrEqual(3, $crawler->filter('#content-right .list-navigation nav .pagination')->count());
        $this->assertRegExp('/(\d*) items/', $crawler->filter('#content-right .list-navigation .card-header')->text());
        $this->assertEquals(1, $crawler->filter('#content-right .list-navigation nav form')->count());
    }

    public function testPageHasBookingListItems()
    {
        $crawler = $this->loginUser();

        $this->assertEquals(1, $crawler->filter('#content-right table thead .columns')->count());
        $this->assertEquals(17, $crawler->filter('#content-right table thead .columns th')->count());
        $this->assertEquals(50, $crawler->filter('#content-right .list-navigation nav form input[name=num_per_page]')->attr('value'));
        $this->assertEquals($crawler->filter('#content-right .list-navigation nav form input[name=num_per_page]')->attr('value'), $crawler->filter('#content-right table tbody tr')->count());
    }
}
