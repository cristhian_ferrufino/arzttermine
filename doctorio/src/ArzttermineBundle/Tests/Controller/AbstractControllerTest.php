<?php
namespace AppBundle\Tests\Controller;

require_once __DIR__.'/../../../../app/legacy_bootstrap.php';

use Arzttermine\UserBundle\Entity\Doctor;
use Arzttermine\UserBundle\Entity\Patient;
use Arzttermine\UserBundle\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Arzttermine\UserBundle\Entity\User;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * @backupGlobals disabled
 */
abstract class AbstractControllerTest extends WebTestCase
{
    /**
     * @var \Symfony\Bundle\FrameworkBundle\Client
     */
    protected $client;

    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected $container;

    /**
     * @var \Symfony\Component\Translation\TranslatorInterface
     */
    protected $translator;

    /**
     * @var \Symfony\Component\Routing\RouterInterface
     */
    protected $router;

    /**
     * @var Doctor|Patient
     */
    protected $user;

    /**
     * @var array
     */
    protected $urlParams = [];

    /**
     * @var string
     */
    protected $url = '';

    /**
     * @var bool
     */
    protected $forceHttps = false;

    /**
     * @var string
     */
    protected $firstName = 'test';

    /**
     * @var string
     */
    protected $lastName = 'tester';

    /**
     * @var string
     */
    protected $locationName = 'practice test';

    /**
     * @var string
     */
    protected $email = 'test@test.com';

    /**
     * @var string
     */
    protected $password = 'thisisawrongt3stingpassword';


    public function setUp()
    {
        global $kernel;
        $kernel = static::createKernel();
        $kernel->boot();

        $this->client = self::createClient([], ['HTTP_USER_AGENT' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:45.0) Gecko/20100101 Firefox/45.0']);
        $this->container = $this->client->getContainer();
        $this->translator = $this->container->get('translator');
        $this->router = $this->container->get('router');

        //make sure that password is unique
        if (isset($this->password)) {
            $this->password .= time();
        }
        //make sure that email is unique
        if (isset($this->email)) {
            $this->email = time().'-'.$this->email;
        }

        if (isset($this->locationName)) {
            $this->locationName = time().'-'.$this->locationName;
        }

        if (isset($this->firstName)) {
            $this->firstName = time().'-'.$this->firstName;
        }

        if (isset($this->lastName)) {
            $this->lastName = time().'-'.$this->lastName;
        }
    }

    protected function login($email, $password)
    {
        /** @var UserRepository $userRepository */
        $userRepository = $this->container->get('arzttermine_user.user_repository');
        $session = $this->container->get('session');

        $user = $userRepository->findOneByEmail($email);
        $token = new UsernamePasswordToken($user, $password, 'main', $user->getRoles());
        $session->set('_security_main', serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
    }

    protected function loginUser($referrer = null, $email = null, $password = null)
    {
        if ($email === null) {
            $email = $this->email;
        }
        if ($password === null) {
            $password = $this->password;
        }
        $this->createLoginData($email, $password, true, 'ROLE_ADMIN', $this->firstName, $this->lastName);

        $this->client->request('GET', ((is_null($referrer)) ? $this->router->generate($this->url) : $referrer), [], [], ['HTTPS' => $this->forceHttps]);
        $this->assertTrue($this->client->getResponse()->isRedirect($this->router->generate('login', [], UrlGeneratorInterface::ABSOLUTE_URL)));
        $crawler = $this->client->followRedirect();

        $form = $crawler->selectButton($this->translator->trans('ui.login.form.button_submit'))->form();

        // set some values
        $form['_username'] = $email;
        $form['_password'] = $password;

        // submit the form
        $this->client->submit($form);

        return $this->client->followRedirect();
    }

    /**
     * @param string $email
     * @param string $password
     * @param bool $activate
     * @param string $group
     * @param string $firstName
     * @param string $lastName
     */
    protected function createLoginData(string $email, string $password, bool $activate = true, string $group = 'ROLE_PATIENT', string $firstName = 'test', string $lastName = 'tester')
    {
        $em = $this->container->get('doctrine')->getManager();
        $userService = $this->container->get('arzttermine_user.user');
        $slugify = $this->container->get('slugify');

        $loginUser = new User();
        $loginUser->setUsername($email);
        $loginUser->setPassword('test');
        $loginUser->setEmail($email);
        $date = new \DateTime();
        $loginUser->setActivatedAt($date->sub(new \DateInterval('P1D')));
        $loginUser->addRole($group);
        $loginUser->setEnabled($activate);
        $loginUser->setApikey(bin2hex(random_bytes(40)));

        if ($group == 'ROLE_DOCTOR') {
            $this->user = new Doctor();
            $this->user->setFirstName($firstName);
            $this->user->setLastName($lastName);
            $this->user->setGender(GENDER_MALE);
            $this->user->setEmail($email);
            $this->user->setPhone('1234567890');
            $this->user->setUser($loginUser);
            $this->user->setMedicalSpecialtyIds([]);
            $this->user->setHasContract(1);
            $this->user->setSlug($slugify->slugify(trim("{$this->user->getTitle()} {$this->user->getFirstName()} {$this->user->getLastName()}")));
            $this->user->setNotificationEmail($email);
            if ($activate) {
                $this->user->setActivatedAt(new \DateTime('yesterday'));
                $this->user->setStatus(Doctor::STATUS_ACTIVE);
            } else {
                $this->user->setActivatedAt(new \DateTime('tomorrow'));
                $this->user->setStatus(Doctor::STATUS_DRAFT);
            }
        } elseif ($group == 'ROLE_PATIENT') {
            $this->user = new Patient();
            $this->user->setFirstName($firstName);
            $this->user->setLastName($lastName);
            $this->user->setEmail($email);
            $this->user->setPhone('1234567890');
            $this->user->setGender(1);
            $this->user->setUser($loginUser);

            if ($activate) {
                $this->user->getUser()->setActivatedAt(new \DateTime('yesterday'));
            } else {
                $this->user->getUser()->setActivatedAt(new \DateTime('tomorrow'));
            }
        } else {
            $this->user = $loginUser;
        }

        $em->persist($this->user);
        $em->flush();

        $userService->changePassword($loginUser, $password);
    }

    protected function deleteLoginData($email)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine')->getManager();

        $user = $em->createQueryBuilder()
            ->select('u.id')
            ->from("ArzttermineUserBundle:User", "u")
            ->where('u.email  = :email')->setParameter("email", $email)
            ->getQuery()->setMaxResults(1)->getOneOrNullResult();
        $userid = $user['id'];

        $doctor = $em->createQueryBuilder()
            ->select('d.id')
            ->from("ArzttermineUserBundle:Doctor", "d")
            ->where('d.user  = :userid')->setParameter("userid", $userid)
            ->getQuery()->setMaxResults(1)->getOneOrNullResult();
        $doctorid = $doctor['id'];

        $patient = $em->createQueryBuilder()
            ->select('d.id')
            ->from("ArzttermineUserBundle:Patient", "d")
            ->where('d.user  = :userid')->setParameter("userid", $userid)
            ->getQuery()->setMaxResults(1)->getOneOrNullResult();
        $patientid = $patient['id'];

        $calendar = $em->createQueryBuilder()
            ->select('c.id')
            ->from("ArzttermineCalendarBundle:Calendar", "c")
            ->where('c.user  = :userid')->setParameter("userid", $userid)
            ->getQuery()->setMaxResults(1)->getOneOrNullResult();
        $calendarid = $calendar['id'];

        $em->createQueryBuilder()
            ->delete("ArzttermineCalendarBundle:Resource", "r")
            ->where('r.calendar  = :calendarid')->setParameter("calendarid", $calendarid)
            ->getQuery()->execute();

        $em->createQueryBuilder()
            ->delete("ArzttermineCalendarBundle:Calendar", "c")
            ->where('c.user  = :userid')->setParameter("userid", $userid)
            ->getQuery()->execute();

        $statement = $em->getConnection()->prepare('DELETE FROM locations_doctors WHERE doctor_id = :doctorid');
        $statement->bindValue('doctorid', $doctorid);
        $statement->execute();

        $statement = $em->getConnection()->prepare('DELETE FROM doctors WHERE user_id = :userid');
        $statement->bindValue('userid', $userid);
        $statement->execute();

        $statement = $em->getConnection()->prepare('DELETE FROM events WHERE patient_id = :patientid');
        $statement->bindValue('patientid', $patientid);
        $statement->execute();

        $statement = $em->getConnection()->prepare('DELETE FROM patients WHERE user_id = :userid');
        $statement->bindValue('userid', $userid);
        $statement->execute();

        $statement = $em->getConnection()->prepare('DELETE FROM users WHERE id = :userid');
        $statement->bindValue('userid', $userid);
        $statement->execute();
    }

    protected function deleteBookingData($email)
    {
        $em = $this->container->get('doctrine')->getManager();
        $bookingRepository = $em->getRepository('ArzttermineBookingBundle:Booking');
        $bookings = $bookingRepository->findByEmail($email);
        foreach ($bookings as $booking) {
            $em->remove($booking);
        }
        $em->flush();
    }

    protected function deleteContactData($email)
    {
        $em = $this->container->get('doctrine')->getManager();
        $contacts = $em->getRepository('ArzttermineUserBundle:Contact')->findByEmail($email);
        foreach ($contacts as $contact) {
            $em->remove($contact);
        }
        $em->flush();
    }

    public function testPageIsSuccessful()
    {
        $this->client->request('GET', $this->router->generate($this->url, $this->urlParams), [], [], ['HTTPS' => $this->forceHttps]);
        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }

    public function testPageHasHeaderNavigation()
    {
        $crawler = $this->client->request('GET', $this->router->generate($this->url, $this->urlParams), [], [], ['HTTPS' => $this->forceHttps]);

        /*
         * todo: enable again as soon as this navigation point is implemented
        $this->assertEquals(1, $crawler->filter('.navigation a:contains("'.$this->translator->trans('ui.menu.practice-management_page').'")')->count());
        $this->assertEquals($this->router->generate('page_practice-management'), $crawler->filter('.navigation a:contains("'.$this->translator->trans('ui.menu.practice-management_page').'")')->attr('href'));
        $this->assertEquals(1, $crawler->filter('.navigation nav li')->count());
        */

        $this->assertEquals($this->router->generate('login'), $crawler->filter('.navigation a:contains("'.$this->translator->trans('ui.menu.login').'")')->attr('href'));
        $this->assertEquals($this->router->generate('doctor_register'), $crawler->filter('.navigation a:contains("'.$this->translator->trans('ui.menu.register').'")')->attr('href'));

        $this->assertEquals($this->container->getParameter('app.phone.central'), $crawler->filter('.navigation li span')->last()->text());

    }
}
