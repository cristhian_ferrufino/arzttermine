<?php
namespace AppBundle\Tests\Controller;

class DoctorRegisterControllerTest extends AbstractControllerTest
{
    protected $url = 'doctor_register';
    protected $forceHttps = true;

    /**
     * {@inheritdoc}
     */
    public function tearDown()
    {
        parent::tearDown();

        $this->deleteLoginData($this->email);
    }

    public function testPageHasSmallFooterNavigation()
    {
        $crawler = $this->client->request('GET', $this->router->generate($this->url), [], [], ['HTTPS' => $this->forceHttps]);

        $this->assertEquals(3, $crawler->filter('footer')->eq(0)->filter('a')->count());
        $this->assertEquals(1, $crawler->filter('footer a:contains("'.$this->translator->trans('ui.footer.terms_of_use').'")')->count());
        $this->assertEquals($this->router->generate('page_terms_of_use'), $crawler->filter('footer a:contains("'.$this->translator->trans('ui.footer.terms_of_use').'")')->attr('href'));
        $this->assertEquals(1, $crawler->filter('footer a:contains("'.$this->translator->trans('ui.footer.imprint').'")')->count());
        $this->assertEquals($this->router->generate('page_imprint'), $crawler->filter('footer a:contains("'.$this->translator->trans('ui.footer.imprint').'")')->attr('href'));
        $this->assertEquals(1, $crawler->filter('footer a:contains("'.$this->translator->trans('ui.footer.data_security').'")')->count());
        $this->assertEquals($this->router->generate('page_data_security'), $crawler->filter('footer a:contains("'.$this->translator->trans('ui.footer.data_security').'")')->attr('href'));

        $this->assertEquals(0, $crawler->filter('footer a:contains("'.$this->translator->trans('ui.footer.contact').'")')->count());
        $this->assertEquals(0, $crawler->filter('footer a:contains("Facebook")')->count());
        $this->assertEquals(0, $crawler->filter('footer a:contains("Twitter")')->count());
    }

    public function testPageHasHeaderNavigation()
    {
        $crawler = $this->client->request('GET', $this->router->generate($this->url), [], [], ['HTTPS' => $this->forceHttps]);

        //on registration page header navigation should be hidden
        $this->assertEquals(1, $crawler->filter('.navigation')->count());
    }

    public function testFillOutRegistrationForm()
    {
        $crawler = $this->client->request('GET', $this->router->generate($this->url), [], [], ['HTTPS' => $this->forceHttps]);

        $form = $crawler->selectButton($this->translator->trans('ui.doctor.dashboard.register.form.button_register'))->form();

        // set some values
        $form['registration[doctor][gender]'] = 1;
        $form['registration[doctor][first_name]'] = $this->firstName;
        $form['registration[doctor][last_name]'] = $this->lastName;
        $form['registration[doctor][agb]'] = 1;
        $form['registration[user][email]'] = $this->email;
        $form['registration[user][password]'] = $this->password;
        $form['registration[location][phone]'] = '0123456789';
        $form['registration[location][name]'] = $this->locationName;
        $form['registration[location][city]'] = 'madrid';
        $form['registration[location][street]'] = 'route 69';
        $form['registration[location][zip]'] = '12345';

        // submit the form
        $this->client->submit($form);
        $this->assertTrue($this->client->getResponse()->isRedirect($this->router->generate('login')));
        $crawler = $this->client->followRedirect();

        $this->assertEquals($this->translator->trans('notice.info.thank_you_for_registration'), $crawler->filter('.alert-info')->text());
    }

    public function testRegisteredDoctorAndLocationDuplicateCheck()
    {
        //register doctor/location 1st time
        $crawler = $this->client->request('GET', $this->router->generate($this->url), [], [], ['HTTPS' => $this->forceHttps]);
        $form = $crawler->selectButton($this->translator->trans('ui.doctor.dashboard.register.form.button_register'))->form();
        // set some values
        $form['registration[doctor][gender]'] = 1;
        $form['registration[doctor][first_name]'] = $this->firstName;
        $form['registration[doctor][last_name]'] = $this->lastName;
        $form['registration[doctor][agb]'] = 1;
        $form['registration[user][email]'] = $this->email;
        $form['registration[user][password]'] = $this->password;
        $form['registration[location][phone]'] = '0123456789';
        $form['registration[location][name]'] = $this->locationName;
        $form['registration[location][city]'] = 'madrid';
        $form['registration[location][street]'] = 'route 69';
        $form['registration[location][zip]'] = '12345';
        // submit the form
        $this->client->submit($form);
        $this->assertTrue($this->client->getResponse()->isRedirect($this->router->generate('login')));
        $crawler = $this->client->followRedirect();
        $this->assertEquals($this->translator->trans('notice.info.thank_you_for_registration'), $crawler->filter('.alert-info')->text());

        //register same doctor/location 2nd time
        $crawler = $this->client->request('GET', $this->router->generate($this->url), [], [], ['HTTPS' => $this->forceHttps]);
        $form = $crawler->selectButton($this->translator->trans('ui.doctor.dashboard.register.form.button_register'))->form();
        // set some values
        $form['registration[doctor][gender]'] = 1;
        $form['registration[doctor][first_name]'] = $this->firstName;
        $form['registration[doctor][last_name]'] = $this->lastName;
        $form['registration[doctor][agb]'] = 1;
        $form['registration[user][email]'] = $this->email;
        $form['registration[user][password]'] = $this->password;
        $form['registration[location][phone]'] = '0123456789';
        $form['registration[location][name]'] = $this->locationName;
        $form['registration[location][city]'] = 'madrid';
        $form['registration[location][street]'] = 'route 69';
        $form['registration[location][zip]'] = '12345';
        // submit the form
        $crawler = $this->client->submit($form);
        $this->assertEquals('This value is already used.', trim($crawler->filter('.has-error .help-block')->text()));
    }

    public function testRegisteredDoctorHasCorrectSettingsOnAdmin()
    {
        //register doctor
        $crawler = $this->client->request('GET', $this->router->generate($this->url), [], [], ['HTTPS' => $this->forceHttps]);
        $form = $crawler->selectButton($this->translator->trans('ui.doctor.dashboard.register.form.button_register'))->form();

        // set some values
        $form['registration[doctor][gender]'] = 1;
        $form['registration[doctor][first_name]'] = $this->firstName;
        $form['registration[doctor][last_name]'] = $this->lastName;
        $form['registration[doctor][agb]'] = 1;
        $form['registration[user][email]'] = $this->email;
        $form['registration[user][password]'] = $this->password;
        $form['registration[location][phone]'] = '0123456789';
        $form['registration[location][name]'] = $this->locationName;
        $form['registration[location][city]'] = 'madrid';
        $form['registration[location][street]'] = 'route 69';
        $form['registration[location][zip]'] = '12345';

        // submit the form
        $this->client->submit($form);
        $this->assertTrue($this->client->getResponse()->isRedirect($this->router->generate('login')));
        $crawler = $this->client->followRedirect();
        $this->assertEquals($this->translator->trans('notice.info.thank_you_for_registration'), $crawler->filter('.alert-info')->text());

        //check doctor on admin editor
        $crawler = $this->loginUser($this->router->generate('admin_doctors'), time().$this->email, time().$this->password);

        $this->assertEquals(1, $crawler->filter('#content-right table thead .columns')->count());
        $this->assertEquals(15, $crawler->filter('#content-right table thead .columns th')->count());
        $this->assertEquals(50, $crawler->filter('#content-right .list-navigation nav form input[name=num_per_page]')->attr('value'));
        $this->assertEquals($crawler->filter('#content-right .list-navigation nav form input[name=num_per_page]')->attr('value'), $crawler->filter('#content-right table tbody tr')->count());
        $this->assertEquals(50, $crawler->filter('#content-right .table tbody tr')->count());

        //get second (first is our test user which gets removed by tearDown) list items edit link
        $doctorEditLink = $crawler->filter('#content-right .table tbody tr')->eq(0)->filter('a')->attr('href');
        $crawler = $this->client->request('GET', $doctorEditLink, [], [], ['HTTPS' => $this->forceHttps]);

        $this->assertEquals($this->translator->trans('admin.ui.actions.edit'), trim($crawler->filter('#content-right .card .card-header')->text()));
        $this->assertEquals(2, $crawler->filter('#form_data_status option[selected="selected"]')->attr('value'));
        $this->assertEquals($this->email, $crawler->filter('#user-selector option[selected="selected"]')->text());
        $this->assertEquals(1, $crawler->filter('#form_data_gender option[selected="selected"]')->attr('value'));
        $this->assertEquals($this->firstName, $crawler->filter('#form_data_first_name ')->attr('value'));
        $this->assertEquals($this->lastName, $crawler->filter('#form_data_last_name ')->attr('value'));
        $this->assertEquals($this->locationName, $crawler->filter('#location-selector option[selected="selected"]')->text());
    }
}
