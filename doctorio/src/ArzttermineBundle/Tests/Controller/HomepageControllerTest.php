<?php
namespace AppBundle\Tests\Controller;

class HomepageControllerTest extends AbstractControllerTest
{
    protected $url = 'home';

    public function testPageHasSearchBox()
    {
        $crawler = $this->client->request('GET', $this->router->generate($this->url));
        $this->assertEquals(1, $crawler->filter('.search-form')->count());
        $this->assertEquals(1, $crawler->filter('#search_medical_specialty_id')->count());
        $this->assertGreaterThan(10, $crawler->filter('#search_medical_specialty_id option')->count());
        $this->assertEquals(1, $crawler->filter('#search_location')->count());
        $this->assertEquals($this->translator->trans('ui.search.place.placeholder'), $crawler->filter('#search_location')->attr('placeholder'));
        $this->assertEquals(1, $crawler->filter('.search-form button[type=submit]')->count());
    }

    public function testPageHasFooterNavigation()
    {
        $crawler = $this->client->request('GET', $this->router->generate($this->url));

        $this->assertEquals(1, $crawler->filter('footer h3:contains("'.$this->translator->trans('ui.footer.legal').'")')->count());

        $this->assertEquals(3, $crawler->filter('footer nav')->eq(0)->filter('li')->count());
        $this->assertEquals(1, $crawler->filter('footer nav li:contains("'.$this->translator->trans('ui.footer.terms_of_use').'")')->count());
        $this->assertEquals($this->router->generate('page_terms_of_use'), $crawler->filter('footer nav li a:contains("'.$this->translator->trans('ui.footer.terms_of_use').'")')->attr('href'));
        $this->assertEquals(1, $crawler->filter('footer nav li:contains("'.$this->translator->trans('ui.footer.imprint').'")')->count());
        $this->assertEquals($this->router->generate('page_imprint'), $crawler->filter('footer nav li a:contains("'.$this->translator->trans('ui.footer.imprint').'")')->attr('href'));
        $this->assertEquals(1, $crawler->filter('footer nav li:contains("'.$this->translator->trans('ui.footer.data_security').'")')->count());
        $this->assertEquals($this->router->generate('page_data_security'), $crawler->filter('footer nav li a:contains("'.$this->translator->trans('ui.footer.data_security').'")')->attr('href'));

        $this->assertEquals(1, $crawler->filter('footer nav')->eq(1)->filter('li')->count());
        $this->assertEquals(1, $crawler->filter('footer nav li:contains("'.$this->translator->trans('ui.footer.contact').'")')->count());
        $this->assertEquals($this->router->generate('contact'), $crawler->filter('footer nav li a:contains("'.$this->translator->trans('ui.footer.contact').'")')->attr('href'));

        $this->assertEquals(2, $crawler->filter('footer nav')->eq(2)->filter('li')->count());
        $this->assertEquals(1, $crawler->filter('footer nav li:contains("Facebook")')->count());
        $this->assertEquals($this->container->getParameter('social.facebook.url'), $crawler->filter('footer nav li a:contains("Facebook")')->attr('href'));
        $this->assertEquals(1, $crawler->filter('footer nav li:contains("Twitter")')->count());
        $this->assertEquals($this->container->getParameter('social.twitter.url'), $crawler->filter('footer nav li a:contains("Twitter")')->attr('href'));

        $this->assertEquals(1, $crawler->filter('footer h3:contains("'.$this->translator->trans('ui.footer.company').'")')->count());
        $this->assertEquals(1, $crawler->filter('footer h3:contains("'.$this->translator->trans('ui.footer.social').'")')->count());
    }
}
