<?php
namespace AppBundle\Tests\Controller;

use Arzttermine\UserBundle\Entity\PendingDoctor;
use Doctrine\ORM\EntityManager;

class AdminPendingDoctorControllerTest extends AbstractControllerTest
{
    protected $url = 'admin_pending_doctors';
    protected $forceHttps = true;

    /**
     * {@inheritdoc}
     */
    public function tearDown()
    {
        parent::tearDown();

        // Delete shared data
        $this->deleteLoginData($this->email);
    }

    protected function createPendingDoctor()
    {
        $em = $this->container->get('doctrine')->getManager();

        $pendingDoctor = new PendingDoctor();

        $pendingDoctor->setGender(0);
        $pendingDoctor->setTitle('testtitle');
        $pendingDoctor->setFirstName($this->firstName);
        $pendingDoctor->setLastName($this->lastName);
        $pendingDoctor->setEmail($this->email);
        $pendingDoctor->setPhone('0123456789');
        $pendingDoctor->setLocationName('practice test');
        $pendingDoctor->setLocationCity('madrid');
        $pendingDoctor->setLocationStreet('route 69');
        $pendingDoctor->setLocationZip('12345');
        $pendingDoctor->setMedicalSpecialtyIds(14);
        $pendingDoctor->setRegistrationReasons('this is a test');
        $pendingDoctor->setRegistrationFunnel('advertising');

        $em->persist($pendingDoctor);
        $em->flush();
    }

    public function testPageIsSuccessful()
    {
        $this->loginUser();

        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }

    public function testPageHasHeaderNavigation()
    {
        $crawler = $this->loginUser();

        $this->assertEquals(1, $crawler->filter('.dropdown a:contains("'.$this->translator->trans('admin.sidebar.settings').'")')->count());
        $this->assertEquals($this->router->generate('admin_users', ['action' => 'edit', 'id' => $this->user->getId()]), $crawler->filter('.dropdown a:contains("'.$this->translator->trans('admin.sidebar.settings').'")')->attr('href'));

        $this->assertEquals(1, $crawler->filter('.dropdown a:contains("'.$this->translator->trans('admin.sidebar.logout').'")')->count());
        $this->assertEquals($this->router->generate('fos_user_security_logout'), $crawler->filter('.dropdown a:contains("'.$this->translator->trans('admin.sidebar.logout').'")')->attr('href'));

        $this->assertEquals(1, $crawler->filter('#side-menu .nav-link.active')->count());
        $this->assertEquals($this->router->generate($this->url), $crawler->filter('#side-menu .nav-link.active')->attr('href'));
    }

    public function testPageHasPendingDoctorListNavigation()
    {
        $crawler = $this->loginUser();

        $this->assertEquals(1, $crawler->filter('#content-right .list-navigation')->count());
        $this->assertGreaterThanOrEqual(3, $crawler->filter('#content-right .list-navigation nav .pagination')->count());
        $this->assertRegExp('/(\d*) items/', $crawler->filter('#content-right .list-navigation .card-header')->text());
        $this->assertEquals(1, $crawler->filter('#content-right .list-navigation nav form')->count());
    }

    public function testCreatingPendingDoctorListItem()
    {
        $this->markTestSkipped('Need to refactor fitting new registration form');

        //create a pending doctor via the registration form
        $crawler = $this->client->request('GET', $this->router->generate('doctor_register'));

        $form = $crawler->selectButton($this->translator->trans('ui.doctor.dashboard.register.form.button_register'))->form();

        // set some values
        $form['doctor_registration[gender]'] = 0;
        $form['doctor_registration[title]'] = 'testtitle';
        $form['doctor_registration[first_name]'] = $this->firstName;
        $form['doctor_registration[last_name]'] = $this->lastName;
        $form['doctor_registration[email]'] = $this->email;
        $form['doctor_registration[phone]'] = '0123456789';
        $form['doctor_registration[location_name]'] = 'practice test';
        $form['doctor_registration[location_city]'] = 'madrid';
        $form['doctor_registration[location_street]'] = 'route 69';
        $form['doctor_registration[location_zip]'] = '12345';
        $form['doctor_registration[medical_specialty_ids]'] = 14;
        $form['doctor_registration[registration_reasons]'] = 'this is a test';
        $form['doctor_registration[registration_funnel]'] = 'advertising';

        // submit the form
        $this->client->submit($form);
        $this->assertTrue($this->client->getResponse()->isRedirect($this->router->generate('login')));
        $crawler = $this->client->followRedirect();
        $this->assertEquals($this->translator->trans('notice.info.thank_you_for_registration'), $crawler->filter('.alert-info')->text());

        //check that the pending doctor is visible on admin pending_doctors list
        $crawler = $this->loginUser();
        $this->assertEquals($this->firstName, $crawler->filter('#content-right .table tbody tr')->eq(0)->filter('td')->eq(4)->text());
        $this->assertEquals($this->lastName, $crawler->filter('#content-right .table tbody tr')->eq(0)->filter('td')->eq(5)->text());
        $this->assertEquals($this->email, $crawler->filter('#content-right .table tbody tr')->eq(0)->filter('td')->eq(7)->text());
        $this->assertEquals('route 69', $crawler->filter('#content-right .table tbody tr')->eq(0)->filter('td')->eq(9)->text());
        $this->assertEquals('/img/admin/version_activate.png', $crawler->filter('#content-right .table tbody tr .button-action')->eq(2)->attr('src'));
    }

    public function testPageHasPendingDoctorListItems()
    {
        $crawler = $this->loginUser();
        $this->createPendingDoctor();

        $this->assertEquals(1, $crawler->filter('#content-right table thead .columns')->count());
        $this->assertEquals(15, $crawler->filter('#content-right table thead .columns th')->count());
        $this->assertEquals(50, $crawler->filter('#content-right .list-navigation nav form input[name=num_per_page]')->attr('value'));
        $this->assertEquals($crawler->filter('#content-right .list-navigation nav form input[name=num_per_page]')->attr('value'), $crawler->filter('#content-right table tbody tr')->count());
        $this->assertEquals(50, $crawler->filter('#content-right .table tbody tr')->count());

        return $crawler->filter('#content-right .table tbody tr')->eq(0)->filter('a')->attr('href');
    }

    /**
     * @depends testPageHasPendingDoctorListItems
     */
    public function testPageSimpleSavePendingDoctorListItem($editlink)
    {
        $crawler = $this->loginUser($editlink);

        $this->assertEquals($this->translator->trans('admin.ui.actions.edit'), trim($crawler->filter('#content-right .card .card-header')->text()));

        //check that simple user save works
        $form = $crawler->selectButton('send_button')->form();
        $crawler = $this->client->submit($form);

        $this->assertEquals($this->translator->trans('notice.info.action_was_successfully'), $crawler->filter('.alert')->text());
    }

    /**
     * @depends testPageHasPendingDoctorListItems
     */
    public function testPageEditPendingDoctorListItem($editlink)
    {
        $crawler = $this->loginUser($editlink);

        $this->assertEquals($this->translator->trans('admin.ui.actions.edit'), trim($crawler->filter('#content-right .card .card-header')->text()));

        $form = $crawler->selectButton('send_button')->form();
        $form['form_data[last_name]'] = 'testlast';
        $crawler = $this->client->submit($form);

        $this->assertEquals($this->translator->trans('notice.info.action_was_successfully'), $crawler->filter('.alert')->text());

        return $crawler->filter('#content-right .table tbody tr a')->eq(2)->attr('href');
    }

    /**
     * @depends testPageEditPendingDoctorListItem
     */
    public function testActivatePendingDoctorListItem($activateLink)
    {
        $this->markTestSkipped('Need to refactor fitting new registration form');

        $crawler = $this->loginUser($activateLink);

        $this->assertEquals('ui.form.are_you_sure', $crawler->filter('#content-right .table-striped tr th')->eq(0)->text());

        $form = $crawler->selectButton('OK')->form();

        // submit the form
        $crawler = $this->client->submit($form);
        $this->assertEquals($this->translator->trans('admin.ui.pending_doctors.activated'), $crawler->filter('.alert-info')->text());
    }

    // Shim to clear out testing data until we move to fixtures
    public function testRemovePendingDoctors()
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine')->getManager();

        // Delete pending users created by the tests (sharing nonsense testing email)
        $statement = $em->getConnection()->prepare('DELETE FROM pending_doctors WHERE email = :email');
        $statement->bindValue('email', $this->email);
        $statement->execute();
    }
}
