<?php
namespace AppBundle\Tests\Controller;

class PatientControllerTest extends AbstractControllerTest
{
    protected $url = 'patient_register';
    protected $phone = '1234567890';
    protected $forceHttps = true;

    /**
     * {@inheritdoc}
     */
    public function tearDown()
    {
        parent::tearDown();

        $this->deleteLoginData($this->email);
    }

    public function testPageHasSmallFooterNavigation()
    {
        $crawler = $this->client->request('GET', $this->router->generate($this->url), [], [], ['HTTPS' => $this->forceHttps]);

        $this->assertEquals(3, $crawler->filter('footer')->eq(0)->filter('a')->count());
        $this->assertEquals(1, $crawler->filter('footer a:contains("'.$this->translator->trans('ui.footer.terms_of_use').'")')->count());
        $this->assertEquals($this->router->generate('page_terms_of_use'), $crawler->filter('footer a:contains("'.$this->translator->trans('ui.footer.terms_of_use').'")')->attr('href'));
        $this->assertEquals(1, $crawler->filter('footer a:contains("'.$this->translator->trans('ui.footer.imprint').'")')->count());
        $this->assertEquals($this->router->generate('page_imprint'), $crawler->filter('footer a:contains("'.$this->translator->trans('ui.footer.imprint').'")')->attr('href'));
        $this->assertEquals(1, $crawler->filter('footer a:contains("'.$this->translator->trans('ui.footer.data_security').'")')->count());
        $this->assertEquals($this->router->generate('page_data_security'), $crawler->filter('footer a:contains("'.$this->translator->trans('ui.footer.data_security').'")')->attr('href'));

        $this->assertEquals(0, $crawler->filter('footer a:contains("'.$this->translator->trans('ui.footer.contact').'")')->count());
        $this->assertEquals(0, $crawler->filter('footer a:contains("Facebook")')->count());
        $this->assertEquals(0, $crawler->filter('footer a:contains("Twitter")')->count());
    }

    /**
     * {@inheritdoc}
     */
    public function testPageHasHeaderNavigation()
    {
        $crawler = $this->client->request('GET', $this->router->generate($this->url), [], [], ['HTTPS' => $this->forceHttps]);
        $this->assertEquals(1, $crawler->filter('.navigation')->count());
    }

    public function testFillOutRegistrationForm()
    {
        $crawler = $this->client->request('GET', $this->router->generate($this->url), [], [], ['HTTPS' => $this->forceHttps]);
        $form = $crawler->selectButton($this->translator->trans('ui.form.button_submit'))->form();

        $form['registration[patient][gender]'] = 1;
        $form['registration[patient][first_name]'] = $this->firstName;
        $form['registration[patient][last_name]'] = $this->lastName;
        $form['registration[patient][phone]'] = $this->phone;
        $form['registration[patient][agb]'] = 1;
        $form['registration[user][email]'] = $this->email;
        $form['registration[user][password]'] = $this->password;

        $this->client->submit($form);

        $this->assertTrue($this->client->getResponse()->isRedirect($this->router->generate('login')));
        $crawler = $this->client->followRedirect();

        $this->assertEquals($this->translator->trans('notice.info.thank_you_for_registration'), $crawler->filter('.alert-info')->text());
    }

    public function testDuplicateRegistration()
    {
        // Initial registration
        $crawler = $this->client->request('GET', $this->router->generate($this->url), [], [], ['HTTPS' => $this->forceHttps]);
        $form = $crawler->selectButton($this->translator->trans('ui.form.button_submit'))->form();

        $form['registration[patient][gender]'] = 1;
        $form['registration[patient][first_name]'] = $this->firstName;
        $form['registration[patient][last_name]'] = $this->lastName;
        $form['registration[patient][phone]'] = $this->phone;
        $form['registration[patient][agb]'] = 1;
        $form['registration[user][email]'] = $this->email;
        $form['registration[user][password]'] = $this->password;

        $this->client->submit($form);
        $this->assertTrue($this->client->getResponse()->isRedirect($this->router->generate('login')));
        $crawler = $this->client->followRedirect();
        $this->assertEquals($this->translator->trans('notice.info.thank_you_for_registration'), $crawler->filter('.alert-info')->text());

        // Duplicate registration
        $crawler = $this->client->request('GET', $this->router->generate($this->url), [], [], ['HTTPS' => $this->forceHttps]);
        $form = $crawler->selectButton($this->translator->trans('ui.form.button_submit'))->form();

        $form['registration[patient][gender]'] = 1;
        $form['registration[patient][first_name]'] = $this->firstName;
        $form['registration[patient][last_name]'] = $this->lastName;
        $form['registration[patient][phone]'] = $this->phone;
        $form['registration[patient][agb]'] = 1;
        $form['registration[user][email]'] = $this->email;
        $form['registration[user][password]'] = $this->password;

        $crawler = $this->client->submit($form);
        $this->assertEquals('This value is already used.', trim($crawler->filter('.has-error .help-block')->text()));
    }
}
