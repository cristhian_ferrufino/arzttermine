<?php
namespace AppBundle\Tests\Controller;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class LoginControllerTest extends AbstractControllerTest
{
    protected $url = 'login';
    protected $forceHttps = true;

    /**
     * {@inheritdoc}
     */
    public function tearDown()
    {
        parent::tearDown();

        $this->deleteLoginData($this->email);
    }

    public function testPageHasSmallFooterNavigation()
    {
        $crawler = $this->client->request('GET', $this->router->generate($this->url), [], [], ['HTTPS' => $this->forceHttps]);

        $this->assertEquals(3, $crawler->filter('footer')->eq(0)->filter('a')->count());
        $this->assertEquals(1, $crawler->filter('footer a:contains("'.$this->translator->trans('ui.footer.terms_of_use').'")')->count());
        $this->assertEquals($this->router->generate('page_terms_of_use'), $crawler->filter('footer a:contains("'.$this->translator->trans('ui.footer.terms_of_use').'")')->attr('href'));
        $this->assertEquals(1, $crawler->filter('footer a:contains("'.$this->translator->trans('ui.footer.imprint').'")')->count());
        $this->assertEquals($this->router->generate('page_imprint'), $crawler->filter('footer a:contains("'.$this->translator->trans('ui.footer.imprint').'")')->attr('href'));
        $this->assertEquals(1, $crawler->filter('footer a:contains("'.$this->translator->trans('ui.footer.data_security').'")')->count());
        $this->assertEquals($this->router->generate('page_data_security'), $crawler->filter('footer a:contains("'.$this->translator->trans('ui.footer.data_security').'")')->attr('href'));

        $this->assertEquals(0, $crawler->filter('footer a:contains("'.$this->translator->trans('ui.footer.contact').'")')->count());
        $this->assertEquals(0, $crawler->filter('footer a:contains("Facebook")')->count());
        $this->assertEquals(0, $crawler->filter('footer a:contains("Twitter")')->count());
    }

    public function testLostPasswordLinkIsVisible()
    {
        $crawler = $this->client->request('GET', $this->router->generate($this->url), [], [], ['HTTPS' => $this->forceHttps]);
        $this->assertEquals(1, $crawler->selectLink($this->translator->trans('ui.login.form.lost_password.label'))->count());
    }

    public function testLogout()
    {
        $this->client->request('GET', $this->router->generate('fos_user_security_logout'), [], [], ['HTTPS' => $this->forceHttps]);
        $this->assertTrue($this->client->getResponse()->isRedirect($this->router->generate('home', [], UrlGeneratorInterface::ABSOLUTE_URL)));
        $crawler = $this->client->followRedirect();
        $this->assertEquals($this->router->generate('login'), $crawler->filter('.navigation a:contains("'.$this->translator->trans('ui.menu.login').'")')->attr('href'));
        $this->assertEquals($this->router->generate('doctor_register'), $crawler->filter('.navigation a:contains("'.$this->translator->trans('ui.menu.register').'")')->attr('href'));
    }

    public function testLoginWithWrongCredentials()
    {
        $crawler = $this->client->request('GET', $this->router->generate($this->url), [], [], ['HTTPS' => $this->forceHttps]);
        $form = $crawler->selectButton($this->translator->trans('ui.login.form.button_submit'))->form();

        // set some values
        $form['_username'] = 'bla@blubber.com';
        $form['_password'] = '1234567890';

        // submit the form
        $this->client->submit($form);
        $crawler = $this->client->followRedirect();

        $this->assertEquals('Invalid credentials.', $crawler->filter('.alert-danger')->text());
    }

    public function testLoginWithCorrectButInActiveCredentials()
    {
        $this->createLoginData($this->email, $this->password, false);

        $crawler = $this->client->request('GET', $this->router->generate($this->url), [], [], ['HTTPS' => $this->forceHttps]);
        $form = $crawler->selectButton($this->translator->trans('ui.login.form.button_submit'))->form();

        // set some values
        $form['_username'] = $this->email;
        $form['_password'] = $this->password;

        // submit the form
        $this->client->submit($form);
        $this->assertTrue($this->client->getResponse()->isRedirect());
        $crawler = $this->client->followRedirect();
        $this->assertEquals('Account is disabled.', $crawler->filter('.alert-danger')->text());
    }

    public function testLoginWithCorrectPatientCredentials()
    {
        $this->createLoginData($this->email, $this->password);

        $crawler = $this->client->request('GET', $this->router->generate($this->url), [], [], ['HTTPS' => $this->forceHttps]);
        $form = $crawler->selectButton($this->translator->trans('ui.login.form.button_submit'))->form();

        // set some values
        $form['_username'] = $this->email;
        $form['_password'] = $this->password;

        // submit the form
        $this->client->submit($form);
        $this->assertTrue($this->client->getResponse()->isRedirect($this->router->generate('patient_account', [], UrlGeneratorInterface::ABSOLUTE_URL)));
        $crawler = $this->client->followRedirect();

        $this->assertEquals($this->email, $crawler->filter('#patient_profile_email')->attr('value'));
        $this->assertEquals(1, $crawler->selectButton($this->translator->trans('ui.patient.dashboard.account.form.submit'))->count());
    }

    public function testLoginWithCorrectDoctorCredentials()
    {
        $this->createLoginData($this->email, $this->password, true, 'ROLE_DOCTOR', $this->firstName, $this->lastName);

        $crawler = $this->client->request('GET', $this->router->generate($this->url), [], [], ['HTTPS' => $this->forceHttps]);
        $form = $crawler->selectButton($this->translator->trans('ui.login.form.button_submit'))->form();

        // set some values
        $form['_username'] = $this->email;
        $form['_password'] = $this->password;

        // submit the form
        $this->client->submit($form);
        $this->assertTrue($this->client->getResponse()->isRedirect($this->router->generate('doctor_calendar', [], UrlGeneratorInterface::ABSOLUTE_URL)));
        $crawler = $this->client->followRedirect();

        $this->assertEquals(1, $crawler->filter('.navigation a:contains("'.$this->translator->trans('ui.doctor.dashboard.menu.account').'")')->count());
        $this->assertEquals($this->router->generate('doctor_profile', ['slug' => $this->user->getSlug()]), $crawler->filter('.navigation a:contains("'.$this->translator->trans('ui.doctor.dashboard.menu.account').'")')->attr('href'));
        $this->assertEquals(1, $crawler->filter('.navigation a:contains("'.$this->translator->trans('ui.doctor.dashboard.menu.logout').'")')->count());
        $this->assertEquals($this->router->generate('fos_user_security_logout'), $crawler->filter('.navigation a:contains("'.$this->translator->trans('ui.doctor.dashboard.menu.logout').'")')->attr('href'));

        //check that we get a calendar for the doctor with correct automatic ressources
        $calendar = $this->container->get('doctrine')->getManager()->getRepository('ArzttermineCalendarBundle:Calendar')->findOneByUser($this->user->getUser()->getId());

        $this->assertInstanceOf('Arzttermine\CalendarBundle\Entity\Calendar', $calendar);
        $defaultRessource = $calendar->getDefaultResources()->first();
        $this->assertInstanceOf('Arzttermine\CalendarBundle\Entity\Resource', $defaultRessource);
        $this->assertEquals('default', $defaultRessource->getType());
        $this->assertEquals('Practice', $defaultRessource->getTitle());
        $this->assertEquals('#c2c2c2', $defaultRessource->getColor());

        $doctorResource = $calendar->getResources()->first();
        $this->assertInstanceOf('Arzttermine\CalendarBundle\Entity\Resource', $doctorResource);
        $this->assertEquals('doctor', $doctorResource->getType());
        $this->assertEquals($this->firstName.' '.$this->lastName, $doctorResource->getTitle());
        $this->assertEquals('#16a765', $doctorResource->getColor());
    }

    public function testLoginWithCorrectAdminCredentials()
    {
        $this->createLoginData($this->email, $this->password, true, 'ROLE_ADMIN', $this->firstName, $this->lastName);

        $crawler = $this->client->request('GET', $this->router->generate($this->url), [], [], ['HTTPS' => $this->forceHttps]);
        $form = $crawler->selectButton($this->translator->trans('ui.login.form.button_submit'))->form();

        // set some values
        $form['_username'] = $this->email;
        $form['_password'] = $this->password;

        // submit the form
        $this->client->submit($form);
        $this->assertTrue($this->client->getResponse()->isRedirect($this->router->generate('admin_dashboard', [], UrlGeneratorInterface::ABSOLUTE_URL)));
        $crawler = $this->client->followRedirect();
        
        $this->assertEquals(1, $crawler->filter('.dropdown a:contains("'.$this->translator->trans('admin.sidebar.settings').'")')->count());
        $this->assertEquals($this->router->generate('admin_users', ['action' => 'edit', 'id' => $this->user->getId()]), $crawler->filter('.dropdown a:contains("'.$this->translator->trans('admin.sidebar.settings').'")')->attr('href'));

        $this->assertEquals(1, $crawler->filter('.dropdown a:contains("'.$this->translator->trans('admin.sidebar.logout').'")')->count());
        $this->assertEquals($this->router->generate('fos_user_security_logout'), $crawler->filter('.dropdown a:contains("'.$this->translator->trans('admin.sidebar.logout').'")')->attr('href'));

        $this->assertEquals(1, $crawler->filter('#side-menu .nav-link.active')->count());
        $this->assertEquals($this->router->generate('admin_dashboard'), $crawler->filter('#side-menu .nav-link.active')->attr('href'));
    }

    public function testLoginWithCorrectAdminCredentialsAndGivenReferrer()
    {
        $this->createLoginData($this->email, $this->password, true, 'ROLE_ADMIN', $this->firstName, $this->lastName);
        $this->client->request('GET', $this->router->generate('admin_bookings'), [], [], ['HTTPS' => $this->forceHttps]);
        $this->assertTrue($this->client->getResponse()->isRedirect($this->router->generate($this->url, [], UrlGeneratorInterface::ABSOLUTE_URL)));
        $crawler = $this->client->followRedirect();

        $form = $crawler->selectButton($this->translator->trans('ui.login.form.button_submit'))->form();

        // set some values
        $form['_username'] = $this->email;
        $form['_password'] = $this->password;

        // submit the form
        $this->client->submit($form);
        $this->assertTrue($this->client->getResponse()->isRedirect($this->router->generate('admin_bookings', [], UrlGeneratorInterface::ABSOLUTE_URL)));
        $crawler = $this->client->followRedirect();

        $this->assertEquals(1, $crawler->filter('.dropdown a:contains("'.$this->translator->trans('admin.sidebar.settings').'")')->count());
        $this->assertEquals($this->router->generate('admin_users', ['action' => 'edit', 'id' => $this->user->getId()]), $crawler->filter('.dropdown a:contains("'.$this->translator->trans('admin.sidebar.settings').'")')->attr('href'));

        $this->assertEquals(1, $crawler->filter('.dropdown a:contains("'.$this->translator->trans('admin.sidebar.logout').'")')->count());
        $this->assertEquals($this->router->generate('fos_user_security_logout'), $crawler->filter('.dropdown a:contains("'.$this->translator->trans('admin.sidebar.logout').'")')->attr('href'));

        $this->assertEquals(1, $crawler->filter('#side-menu .nav-link.active')->count());
        $this->assertEquals($this->router->generate('admin_bookings'), $crawler->filter('#side-menu .nav-link.active')->attr('href'));
    }
}
