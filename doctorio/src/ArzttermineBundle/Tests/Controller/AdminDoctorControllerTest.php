<?php
namespace AppBundle\Tests\Controller;

class AdminDoctorControllerTest extends AbstractControllerTest
{
    protected $url = 'admin_doctors';
    protected $forceHttps = true;

    public function tearDown()
    {
        parent::tearDown();

        $this->deleteLoginData($this->email);
    }

    public function testPageIsSuccessful()
    {
        $this->loginUser();

        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }

    public function testPageHasHeaderNavigation()
    {
        $crawler = $this->loginUser();

        $this->assertEquals(1, $crawler->filter('.dropdown a:contains("'.$this->translator->trans('admin.sidebar.settings').'")')->count());
        $this->assertEquals($this->router->generate('admin_users', ['action' => 'edit', 'id' => $this->user->getId()]), $crawler->filter('.dropdown a:contains("'.$this->translator->trans('admin.sidebar.settings').'")')->attr('href'));

        $this->assertEquals(1, $crawler->filter('.dropdown a:contains("'.$this->translator->trans('admin.sidebar.logout').'")')->count());
        $this->assertEquals($this->router->generate('fos_user_security_logout'), $crawler->filter('.dropdown a:contains("'.$this->translator->trans('admin.sidebar.logout').'")')->attr('href'));

        $this->assertEquals(1, $crawler->filter('#side-menu .nav-link.active')->count());
        $this->assertEquals($this->router->generate($this->url), $crawler->filter('#side-menu .nav-link.active')->attr('href'));
    }

    public function testPageHasDoctorListNavigation()
    {
        $crawler = $this->loginUser();

        $this->assertEquals(1, $crawler->filter('#content-right .list-navigation')->count());
        $this->assertGreaterThanOrEqual(3, $crawler->filter('#content-right .list-navigation nav .pagination')->count());
        $this->assertRegExp('/(\d*) items/', $crawler->filter('#content-right .list-navigation .card-header')->text());
        $this->assertEquals(1, $crawler->filter('#content-right .list-navigation nav form')->count());
    }

    public function testPageHasDoctorListItems()
    {
        //register doctor
        $crawler = $this->client->request('GET', $this->router->generate('doctor_register'), [], [], ['HTTPS' => $this->forceHttps]);
        $form = $crawler->selectButton($this->translator->trans('ui.doctor.dashboard.register.form.button_register'))->form();

        // set some values
        $form['registration[doctor][gender]'] = 1;
        $form['registration[doctor][first_name]'] = $this->firstName;
        $form['registration[doctor][last_name]'] = $this->lastName;
        $form['registration[doctor][agb]'] = 1;
        $form['registration[user][email]'] = 'test'.$this->email;
        $form['registration[user][password]'] = $this->password;
        $form['registration[location][phone]'] = '0123456789';
        $form['registration[location][name]'] = 'test'.$this->locationName;
        $form['registration[location][city]'] = 'madrid';
        $form['registration[location][street]'] = 'route 69';
        $form['registration[location][zip]'] = '12345';

        // submit the form
        $this->client->submit($form);
        $this->assertTrue($this->client->getResponse()->isRedirect($this->router->generate('login')));
        $crawler = $this->client->followRedirect();
        $this->assertEquals($this->translator->trans('notice.info.thank_you_for_registration'), $crawler->filter('.alert-info')->text());


        $crawler = $this->loginUser();

        $this->assertEquals(1, $crawler->filter('#content-right table thead .columns')->count());
        $this->assertEquals(15, $crawler->filter('#content-right table thead .columns th')->count());
        $this->assertEquals(50, $crawler->filter('#content-right .list-navigation nav form input[name=num_per_page]')->attr('value'));
        $this->assertEquals($crawler->filter('#content-right .list-navigation nav form input[name=num_per_page]')->attr('value'), $crawler->filter('#content-right table tbody tr')->count());
        $this->assertEquals(50, $crawler->filter('#content-right .table tbody tr')->count());

        //get second (first is our test user which gets removed by tearDown) list items edit link
        return $crawler->filter('#content-right .table tbody tr')->eq(0)->filter('a')->attr('href');
    }

    /**
     * @depends testPageHasDoctorListItems
     */
    public function testPageSimpleSaveDoctorListItem($editlink)
    {
        $crawler = $this->loginUser($editlink);

        $this->assertEquals($this->translator->trans('admin.ui.actions.edit'), trim($crawler->filter('#content-right .card .card-header')->text()));

        //check that simple user save works
        $form = $crawler->selectButton('send_button')->form();
        $crawler = $this->client->submit($form);

        $this->assertEquals($this->translator->trans('notice.info.action_was_successfully'), $crawler->filter('.alert')->text());
    }

    /**
     * @depends testPageHasDoctorListItems
     */
    public function testPageEditDoctorListItem($editlink)
    {
        $crawler = $this->loginUser($editlink);

        $this->assertEquals($this->translator->trans('admin.ui.actions.edit'), trim($crawler->filter('#content-right .card .card-header')->text()));

        //check that simple user save works
        $form = $crawler->selectButton('send_button')->form();
        $activatedAt = date('d.m.Y H:i:s');
        $form['form_data[activated_at]'] = $activatedAt;
        $crawler = $this->client->submit($form);

        $this->assertEquals($this->translator->trans('notice.info.action_was_successfully'), $crawler->filter('.alert')->text());
        $this->assertEquals('word.yes ('.date_create($activatedAt)->format('Y-m-d H:i:s').')', $crawler->filter('#content-right .table tbody tr')->eq(0)->filter('td')->eq(14)->text());
    }
}
