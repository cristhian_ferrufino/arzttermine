<?php
namespace AppBundle\Tests\Controller;

class ContactControllerTest extends AbstractControllerTest
{
    protected $url = 'contact';

    public function testPageHasFooterNavigation()
    {
        $crawler = $this->client->request('GET', $this->router->generate($this->url));

        $this->assertEquals(1, $crawler->filter('footer h3:contains("'.$this->translator->trans('ui.footer.legal').'")')->count());

        $this->assertEquals(3, $crawler->filter('footer nav')->eq(0)->filter('li')->count());
        $this->assertEquals(1, $crawler->filter('footer nav li:contains("'.$this->translator->trans('ui.footer.terms_of_use').'")')->count());
        $this->assertEquals($this->router->generate('page_terms_of_use'), $crawler->filter('footer nav li a:contains("'.$this->translator->trans('ui.footer.terms_of_use').'")')->attr('href'));
        $this->assertEquals(1, $crawler->filter('footer nav li:contains("'.$this->translator->trans('ui.footer.imprint').'")')->count());
        $this->assertEquals($this->router->generate('page_imprint'), $crawler->filter('footer nav li a:contains("'.$this->translator->trans('ui.footer.imprint').'")')->attr('href'));
        $this->assertEquals(1, $crawler->filter('footer nav li:contains("'.$this->translator->trans('ui.footer.data_security').'")')->count());
        $this->assertEquals($this->router->generate('page_data_security'), $crawler->filter('footer nav li a:contains("'.$this->translator->trans('ui.footer.data_security').'")')->attr('href'));

        $this->assertEquals(1, $crawler->filter('footer nav')->eq(1)->filter('li')->count());
        $this->assertEquals(1, $crawler->filter('footer nav li:contains("'.$this->translator->trans('ui.footer.contact').'")')->count());
        $this->assertEquals($this->router->generate('contact'), $crawler->filter('footer nav li a:contains("'.$this->translator->trans('ui.footer.contact').'")')->attr('href'));

        $this->assertEquals(2, $crawler->filter('footer nav')->eq(2)->filter('li')->count());
        $this->assertEquals(1, $crawler->filter('footer nav li:contains("Facebook")')->count());
        $this->assertEquals($this->container->getParameter('social.facebook.url'), $crawler->filter('footer nav li a:contains("Facebook")')->attr('href'));
        $this->assertEquals(1, $crawler->filter('footer nav li:contains("Twitter")')->count());
        $this->assertEquals($this->container->getParameter('social.twitter.url'), $crawler->filter('footer nav li a:contains("Twitter")')->attr('href'));

        $this->assertEquals(1, $crawler->filter('footer h3:contains("'.$this->translator->trans('ui.footer.company').'")')->count());
        $this->assertEquals(1, $crawler->filter('footer h3:contains("'.$this->translator->trans('ui.footer.social').'")')->count());
    }

    public function testPageHasStaticSidebarNavigation()
    {
        $crawler = $this->client->request('GET', $this->router->generate($this->url));

        $this->assertEquals(1, $crawler->filter('.content-wrapper .stacked-navigation .list-unstyled li.active:contains("'.$this->translator->trans('ui.contact.title').'")')->count());
        $this->assertEquals(4, $crawler->filter('.content-wrapper .stacked-navigation .list-unstyled li')->count());
    }

    public function testFormSubmit()
    {
        $this->createLoginData($this->email, $this->password);
        $this->login($this->email, $this->password);

        $crawler = $this->client->request('GET', $this->router->generate($this->url));

        $form = $crawler->selectButton($this->translator->trans('ui.contact.button_submit'))->form();
        $form['contact[name]'] = 'testname';
        $form['contact[email]'] = $this->email;
        $form['contact[subject]'] = 'testsubject';
        $form['contact[message]'] = 'testmessage';
        $crawler = $this->client->submit($form);
        $this->assertTrue($this->client->getResponse()->isSuccessful());
        $this->assertEquals($this->translator->trans('ui.contact.created_successfully'), $crawler->filter('.alert-success')->text());

        $em = $this->container->get('doctrine')->getManager();
        $contacts = $em->getRepository('ArzttermineUserBundle:Contact')->findByEmail($this->email);
        $this->assertInstanceOf('Arzttermine\UserBundle\Entity\Contact', $contacts[0]);
        $this->assertEquals(1, count($contacts));

        $this->deleteContactData($this->email);
    }
}
