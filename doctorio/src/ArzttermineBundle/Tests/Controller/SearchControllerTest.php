<?php
namespace AppBundle\Tests\Controller;

class SearchControllerTest extends AbstractControllerTest
{
    protected $url = 'search';

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        parent::setUp();

        $this->urlParams = ['search[medical_specialty_id]' => 1, 'search[location]' => 'Madrid'];
    }

    public function testPageHasSmallFooterNavigation()
    {
        $crawler = $this->client->request('GET', $this->router->generate($this->url, $this->urlParams));

        $this->assertEquals(3, $crawler->filter('footer')->eq(0)->filter('a')->count());
        $this->assertEquals(1, $crawler->filter('footer a:contains("'.$this->translator->trans('ui.footer.terms_of_use').'")')->count());
        $this->assertEquals($this->router->generate('page_terms_of_use'), $crawler->filter('footer a:contains("'.$this->translator->trans('ui.footer.terms_of_use').'")')->attr('href'));
        $this->assertEquals(1, $crawler->filter('footer a:contains("'.$this->translator->trans('ui.footer.imprint').'")')->count());
        $this->assertEquals($this->router->generate('page_imprint'), $crawler->filter('footer a:contains("'.$this->translator->trans('ui.footer.imprint').'")')->attr('href'));
        $this->assertEquals(1, $crawler->filter('footer a:contains("'.$this->translator->trans('ui.footer.data_security').'")')->count());
        $this->assertEquals($this->router->generate('page_data_security'), $crawler->filter('footer a:contains("'.$this->translator->trans('ui.footer.data_security').'")')->attr('href'));

        $this->assertEquals(0, $crawler->filter('footer a:contains("'.$this->translator->trans('ui.footer.contact').'")')->count());
        $this->assertEquals(0, $crawler->filter('footer a:contains("Facebook")')->count());
        $this->assertEquals(0, $crawler->filter('footer a:contains("Twitter")')->count());
    }

    public function testPageHasCorrectSearchFilterSettings()
    {
        $medical_specialty_id = 1;
        $location = 'Madrid';
        $this->urlParams = ['search[medical_specialty_id]' => $medical_specialty_id, 'search[location]' => $location];

        $crawler = $this->client->request('GET', $this->router->generate($this->url, $this->urlParams));

        $form = $crawler->selectButton($this->translator->trans('ui.search.filters.filter_results'))->form();

        $this->assertEquals($medical_specialty_id, $form['search[medical_specialty_id]']->getValue());
        $this->assertEquals($location, $form['search[location]']->getValue());
        $this->assertEquals(1, $crawler->filter('.result-list .headline:contains("'.$location.'")')->count());

        //test that filter settings are still correct after submit
        $this->client->submit($form);
        $form = $crawler->selectButton($this->translator->trans('ui.search.filters.filter_results'))->form();
        $this->assertEquals($medical_specialty_id, $form['search[medical_specialty_id]']->getValue());
        $this->assertEquals($location, $form['search[location]']->getValue());
        $this->assertEquals(1, $crawler->filter('.result-list .headline:contains("'.$location.'")')->count());
    }

    public function testResultItemStructure()
    {
        $crawler = $this->client->request('GET', $this->router->generate($this->url, $this->urlParams));

        // We have something
        $this->assertNotEquals(0, $crawler->filter('.result')->count());

        // We have the important components
        $firstResult = $crawler->filter('.result')->eq(0);

        $this->assertEquals(1, $firstResult->filter('.icon')->count()); // Map marker icon
        $this->assertEquals(1, $firstResult->filter('.icon')->text()); // Map marker number
        $this->assertEquals(1, $firstResult->filter('h3 a')->count()); // Doctor's name
        $this->assertEquals(1, $firstResult->filter('.address')->count()); // Address text
        $this->assertEquals(1, $firstResult->filter('.profile-picture-and-calendar .picture')->count()); // Profile
        $this->assertEquals(1, $firstResult->filter('.appointments-block')->count()); // Appointments (empty or not)

        // @todo Add appointment block testing with fixtures
    }
}
