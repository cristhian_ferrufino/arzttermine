<?php

namespace ArzttermineBundle\Command\Import;

use ArzttermineBundle\Location\Location;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateLocationUserCsvCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('import:update-location-user-csv')
            ->setDescription('Import a csv file with location informations and update corresponding locations and users')
            ->addArgument(
                'filename',
                InputArgument::REQUIRED,
                'Filename'
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // can't find out why this needs so much memory :(
        ini_set('memory_limit', '256M');
        $filename = trim($input->getArgument('filename'));

        if (!is_readable($filename)) {
            $output->writeln('<error>filename cannot be blank</error>');
            return;
        }

        $csv_position = array_flip(array('location_name', 'location_street', 'address_additional', 'location_zip', 'location_city', 'phone', 'medical_specialty_text', 'comment'));
        $medical_specialty_texts = array(
            1 => 'Acupuntor',
            2 => 'Alergólogo',
            3 => 'Analista clínico',
            4 => 'Anestesista',
            5 => 'Angiólogo',
            7 => 'Cardiólogo',
            8 => 'Cirujano cardiovascular',
            9 => 'Cirujano general',
            10 => 'Cirujano oral y maxilofacial',
            11 => 'Cirujano pedriátrico',
            12 => 'Cirujano plástico',
            13 => 'Cirujano torácico',
            14 => 'Dentista',
            15 => 'Dermatólogo',
            16 => 'Dietista - Nutricionista',
            17 => 'Digestólogo',
            18 => 'Endocrino',
            19 => 'Enfermero',
            20 => 'Especialista en medicina del deporte',
            21 => 'Especialista en medicina del trabajo',
            22 => 'Especialista en medicina nuclear',
            23 => 'Especialista en medicina preventiva',
            24 => 'Farmacólogo',
            25 => 'Fisioterapeuta',
            26 => 'Medicina Legal y Forense',
            27 => 'Geriatra',
            28 => 'Ginecólogo',
            29 => 'Hematólogo',
            30 => 'Homeópata',
            31 => 'Inmunólogo',
            32 => 'Intensivista',
            33 => 'Internista',
            34 => 'Logopeda',
            35 => 'Médico de familia',
            36 => 'Medicina Estética',
            37 => 'Médico general',
            38 => 'Médico rehabilitador',
            39 => 'Microbiólogo',
            40 => 'Nefrólogo',
            41 => 'Neumólogo',
            42 => 'Neurocirujano',
            43 => 'Neurofisiólogo clínico',
            44 => 'Neurólogo',
            45 => 'Oftalmólogo',
            46 => 'Oncólogo médico',
            47 => 'Oncólogo radioterapéutico',
            48 => 'Óptico',
            49 => 'Osteópata',
            50 => 'Otorrinoralingologo',
            51 => 'Patólogo',
            52 => 'Pediatra',
            53 => 'Podólogo',
            54 => 'Medicina alternativa',
            55 => 'Psicólogo',
            56 => 'Psiquiatra',
            57 => 'Radiólogo',
            58 => 'Reumatólogo',
            59 => 'Terapeuta ocupacional',
            60 => 'Traumatólogo',
            61 => 'Urgenciólogo',
            62 => 'Urólogo',
            63 => 'Cirugía Menor Ambulatoria',
            64 => 'Depilación Láser diodo',
            65 => 'Ecografia',
            66 => 'Matrona',
            67 => 'Medicina Interna',
            68 => 'Psicología Clínica',
            69 => 'Reconocimentos psicotécnicos',
            70 => 'Valoración de daño corporal',
            71 => 'Cirugía Vascular y Angología',
            72 => 'Rehabilitación',
            73 => 'Dermatología Clínica',
            74 => 'Dermatología Quirúrgica',
            75 => 'Dermatología Estética',
            76 => 'Obstetricia',
            77 => 'Citología',
            78 => 'Sexología',
            79 => 'Ortopedía',
            80 => 'Musicoterapia',
            81 => 'Gastroenterologia'
        );

        $row = 0;
        $imported_doctors = 0;

        if (($handle = fopen($filename, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $row++;

                $output->writeln('<comment>Updating line '.$row.'</comment>');

                // $medical_specialty = array_search($data[$csv_position['medical_specialty_text']], $medical_specialty_texts);

                $comment = trim($data[$csv_position['comment']]);
                $location = new Location($comment, 'comment_intern');
                if ($location->isValid()) {
                    $location
                        ->setCity($data[$csv_position['location_city']])
                        ->setZip($data[$csv_position['location_zip']])
                        ->setAddressAdditional($data[$csv_position['address_additional']])
                        ->setUpdatedAt(date('Y-m-d H:i:s'))
                        ->save(array('city','zip','address_additional','updated_at'));

                    $output->writeln('<comment>Location updated: '.$location->getId().' / '.$location->getName().' / new City: '.$data[$csv_position['location_city']].'</comment>');

                    /*
                    $users = $location->getMembers();
                    foreach ($users as $user) {
                        if ($user->isValid()) {
                            $user
                                ->setMedicalSpecialtyIds(array($medical_specialty))
                                ->setUpdatedAt(date('Y-m-d H:i:s'))
                                ->save(array('medical_specialty_ids','updated_at'));
                        } else {
                            $output->writeln('<comment>User not valid with id: ' . $user->getId(). '</comment>');
                        }
                        $output->writeln('<comment>User updated: '.$user->getId().' / '.$user->getName().' / new MSID: '.$medical_specialty.'</comment>');
                    }
                    */
                    $imported_doctors++;
                } else {
                    $output->writeln('<comment>Location not found with comment_intern: '.$comment.'</comment>');
                }
            }
            fclose($handle);
        }

        $output->writeln('<comment>Updated ' . $imported_doctors. ' doctors out of '. $row . '</comment>');
    }
}