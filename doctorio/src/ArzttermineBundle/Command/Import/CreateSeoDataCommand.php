<?php

namespace ArzttermineBundle\Command\Import;

use ArzttermineBundle\MedicalSpecialty\MedicalSpecialty;
use ArzttermineBundle\Search\Text;
use Cocur\Slugify\Slugify;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CreateSeoDataCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('import:create-seo-data')
            ->setDescription('Create seo data from medical specialties');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $cities = array(
            'Albacete',
            'Alicante',
            'Almería',
            'Ávila',
            'Badajoz',
            'Barcelona',
            'Bilbao',
            'Burgos',
            'Castellón de la Plana',
            'Ceuta',
            'Ciudad Real',
            'Ciudad',
            'Cuenca',
            'Cáceres',
            'Cádiz',
            'Córdoba',
            'Gerona',
            'Granada',
            'Guadalajara',
            'Huelva',
            'Huesca',
            'Jaén',
            'La Coruña',
            'Las Palmas de Gran Canaria',
            'León',
            'Logroño',
            'Lugo',
            'Lérida',
            'Madrid',
            'Melilla',
            'Murcia',
            'Málaga',
            'Mérida',
            'Orense',
            'Oviedo',
            'Palencia',
            'Palma de Mallorca',
            'Pamplona',
            'Pontevedra',
            'Salamanca',
            'San Sebastián',
            'Santa Cruz de Tenerife',
            'Santander',
            'Santiago de Compostela',
            'Segovia',
            'Sevilla',
            'Soria',
            'Tarragona',
            'Teruel',
            'Toledo',
            'Valencia',
            'Valladolid',
            'Vitoria',
            'Zamora',
            'Zaragoza'
        );

        $row = 0;
        $slugger = new Slugify();

        $medical_specialty = new MedicalSpecialty();
        $medical_specialties = $medical_specialty->getMedicalSpecialties();

        foreach ($cities as $city) {
            foreach ($medical_specialties as $medical_specialty) {
                $output->writeln('<comment>Adding '.$city.' and '. $medical_specialty->getName() .'</comment>');
                $city_slug = $slugger->slugify($city);
                $slug = '/'.$medical_specialty->getSlug() . '/' . $city_slug;
                $seo = new Text();
                $seo
                    ->setStatus(Text::STATUS_ACTIVE)
                    ->setMedicalSpecialtyId($medical_specialty->getId())
                    ->setAddressSearch($city)
                    ->setPermalink($slug)
                    ->setCreatedAt(date('Y-m-d H:i:s'))
                    ->saveNewObject();

                $row++;
            }
        }

        $output->writeln('<comment>Imported ' . $row. ' items</comment>');
    }
}