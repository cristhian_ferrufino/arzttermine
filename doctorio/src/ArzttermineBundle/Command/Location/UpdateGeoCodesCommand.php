<?php

namespace ArzttermineBundle\Command\Location;

use ArzttermineBundle\Location\Location;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateGeoCodesCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('location:update-geocodes')
            ->setDescription('Updates geocodes for all locations where lat or lng are empty');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $oks = 0;
        $errors = 0;

        $location = new Location();
        $locations = $location->findObjects('lat="0" OR lng="0" OR lat IS NULL OR lng IS NULL');

        if (is_array($locations) && !empty($locations)) {

            $output->writeln('<comment>Number of locations to update: ' . count($locations).'</comment>');

            /* @var $searchService \Arzttermine\SearchBundle\Services\Search */
            $searchService = $this->getContainer()->get('arzttermine_search.search');
            /** @var Location[] $locations */
            foreach ($locations as $location) {
                $_address = $location->getStreet() . ',' . $location->getZip() . ' ' . $location->getCity();
                $_coords = $searchService->getCoordinatesForAddress($_address);
                if (!empty($_coords)) {
                    $location
                        ->setLat($_coords['lat'])
                        ->setLng($_coords['lng'])
                        ->save(array('lat', 'lng'));

                    $output->writeln('<comment>Updated: ['.$location->getId().'] '.$location->getName().'</comment>');
                    $oks++;
                } else {
                    $output->writeln('<comment>Error: ['.$location->getId().'] '.$location->getName().'</comment>');
                    $errors++;
                }
            }
        }

        $output->writeln('<comment>Finished: OK: '.$oks.' / Errors: '.$errors.'</comment>');
    }
}