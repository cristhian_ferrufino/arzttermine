<?php

namespace ArzttermineBundle\Command\Review;

use ArzttermineBundle\Review\Review;
use ArzttermineBundle\User\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateUsersReviewCacheCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('review:update-users-review-cache')
            ->setDescription('Updates the users review cache');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $oks = 0;
        $errors = 0;

        $review = new Review();
        $reviews = $review->findObjects('1=1');

        if (is_array($reviews) && !empty($reviews)) {

            $output->writeln('<comment>Number of reviews to update: ' . count($reviews).'</comment>');

            /** @var Review[] $reviews */
            foreach ($reviews as $review) {
                $user = new User($review->getUserId());
                if ($user->isValid()) {
                    $user->updateRatings();
                    $user->updateRatingsForLocations();

                    $output->writeln('<comment>Updated: Review['.$review->getId().'] with User ['.$user->getId().'] '.$user->getName().'</comment>');
                    $oks++;
                } else {
                    $output->writeln('<comment>Error: Review['.$review->getId().'] with User ['.$user->getId().'] '.$user->getName().'</comment>');
                    $errors++;
                }
            }
        }

        $output->writeln('<comment>Finished: OK: '.$oks.' / Errors: '.$errors.'</comment>');
    }
}