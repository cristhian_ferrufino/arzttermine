<?php

namespace ArzttermineBundle\Command;

use Arzttermine\UserBundle\Entity\Doctor;
use ArzttermineBundle\Appointment\Planner;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateAppointmentsCommand extends ContainerAwareCommand {

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('appointment:generate')
            ->setAliases(['generate:appointments'])
            ->setDescription('Generate appointments from Planners');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Objects for querying
        $_planner = new Planner();
        $generator = $this->getContainer()->get('arzttermine.appointment_generator');

        $output->writeln('<info>Finding all planners</info>');
        $planner_ids = array_map(function ($e) { return $e['id']; }, $_planner->finds()); // Get just the indices

        /** @var int[] $plannerIds */
        foreach ($planner_ids as $planner_id) {
            $planner = new Planner($planner_id);

            // If the doctor doesn't have appointments enabled, don't generate them
            // Note: This is a poor attempt at an optimisation
            // @todo Better handling of relationships
            if ($planner->getDoctor()->getStatus() != Doctor::STATUS_VISIBLE_APPOINTMENTS) {
                continue;
            }
            $output->writeln("Generating and saving appointments for Planner [{$planner->getId()}]");
            $generator
                ->clear($planner)// Clear previous appointments out...
                ->generate($planner); // ... and generate new ones

            // Memory management
            unset($planner);

            $output->writeln('Appointments saved');
        }

        $output->writeln('<info>Planner appointments generated</info>');
    }
}