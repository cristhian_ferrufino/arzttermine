<?php

namespace ArzttermineBundle\Command\Migration;

use ArzttermineBundle\Core\DateTime;
use ArzttermineBundle\User\Group;
use ArzttermineBundle\User\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MigratePatientsCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('migration:patients-to-users')
            ->setDescription('Migrate Patients to Users');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $db = $this->getContainer()->get('arzttermine.database');
        $query = "SELECT * FROM `patients`";

        // For tracking results
        $migrated = [];
        $not_migrated = [];

        $all_patients = $db->executeQuery($query)->fetchAll();

        $output->writeln('<info>Found '.count($all_patients).' patients for migration</info>');

        foreach ($all_patients as $patient) {
            $pending_patient = new User($patient['email'], 'email');

            // The email/account exists. Skip it
            if ($pending_patient->isValid()) {
                $not_migrated = $patient['email'];
                continue;
            }

            $pending_patient->setGender($patient['gender']);
            $pending_patient->setFirstName($patient['first_name']);
            $pending_patient->setLastName($patient['last_name']);
            $pending_patient->setEmail($patient['email']);
            $pending_patient->setPhone($patient['phone']);
            $pending_patient->setNewsletterSubscription($patient['newsletter_subscription']);
            $pending_patient->setPasswordHash($patient['password_hash']);
            $pending_patient->setPasswordHashSalt($patient['password_hash_salt']);
            $pending_patient->setResetPasswordCode($patient['password_reset_code']);
            $pending_patient->setGroupId(Group::PATIENT);

            if ($patient['active']) {
                $pending_patient->setStatus(User::STATUS_ACTIVE);
                $pending_patient->setActivatedAt((new DateTime())->getDateTime());
            }

            $pending_patient->saveNewObject();

            $migrated[] = $pending_patient->getEmail();
        }

        $output->writeln('<comment>Completed: '.count($migrated).' / Not completed: '.count($not_migrated).'</comment>');
    }
}