<?php

namespace ArzttermineBundle\Command\Test;

use ArzttermineBundle\Booking\Booking;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class TestMailSendBookingConfirmationToPatientCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('test:send-booking-confirmation-to-patient')
            ->setDescription('Send booking confirmation to patient')
            ->addArgument(
                'booking_id',
                InputArgument::REQUIRED,
                'Booking Id'
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $mailer = $this->getContainer()->get('arzttermine.mail_handler');
        $booking_id = trim($input->getArgument('booking_id'));

        if (!is_numeric($booking_id)) {
            $output->writeln('<error>booking_id cannot be blank</error>');
            return;
        }

        $booking = new Booking($booking_id);

        if (!$booking->isValid()) {
            $output->writeln('<error>Booking with ID#'.$booking_id.' not found.</error>');
        } else {
            $output->writeln('<comment>Sending email to '.$booking->getEmail().'</comment>');
            if ($mailer->sendConfirmationToPatient($booking)) {
                $output->writeln('Email successfully sent');
            } else {
                $output->writeln('<error>Could not send email</error>');
            }
        }
    }
}