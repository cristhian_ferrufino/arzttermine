<?php

namespace ArzttermineBundle\Command\Test;

use ArzttermineBundle\User\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class TestMailSendActivationEmailToPatientCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('test:send-activation-email-to-patient')
            ->setDescription('Send activation email to patient')
            ->addArgument(
                'user_id',
                InputArgument::REQUIRED,
                'User Id'
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $mailer = $this->getContainer()->get('arzttermine.mail_handler');
        $user_id = trim($input->getArgument('user_id'));

        if (!is_numeric($user_id)) {
            $output->writeln('<error>user_id cannot be blank</error>');
            return;
        }

        $user = new User($user_id);

        if (!$user->isValid()) {
            $output->writeln('<error>User with ID#'.$user_id.' not found.</error>');
        } else {
            $output->writeln('<comment>Sending email to '.$user->getEmail().'</comment>');
            if ($mailer->sendActivationEmailToPatient($user)) {
                $output->writeln('Email successfully sent');
            } else {
                $output->writeln('<error>Could not send email</error>');
            }
        }
    }
}