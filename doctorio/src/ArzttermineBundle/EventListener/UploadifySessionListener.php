<?php

namespace ArzttermineBundle\EventListener;

use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

/**
 * Get the session id from query params on uploadify request
 *
 * @todo Separate
 *
 * @package ArzttermineBundle\EventListener
 */
class UploadifySessionListener
{
    public function onKernelRequest(GetResponseEvent $event)
    {
        if ($event->getRequestType() !== HttpKernelInterface::MASTER_REQUEST) {
            return;
        }

        $request = $event->getRequest();
        if ($request->isMethod('POST') && $request->request->has('PHPSESSID') && $request->request->has('Filename')) {
            $request->cookies->set(session_name(), $request->get('PHPSESSID'));
            session_id($request->get('PHPSESSID'));
            $session = $request->getSession();
            $session->start();
        }
    }
}