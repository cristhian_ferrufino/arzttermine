<?php

namespace ArzttermineBundle\EventListener;

use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

/**
 * This currently catches ALL exceptions in the app
 *
 * @todo Separate
 *
 * @package ArzttermineBundle\EventListener
 */
class ExceptionListener
{
    /**
     * @var EngineInterface
     */
    private $view;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @param EngineInterface $view
     * @param RouterInterface $router
     */
    public function __construct(EngineInterface $view, RouterInterface $router)
    {
        $this->view = $view;
        $this->router = $router;
    }

    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        if($event->getRequest()->getRequestFormat() == 'json') {
            return;
        }

        $exception = $event->getException();

        // Not found. Only applies to stuff that's actually missing
        // Other errors will fall through as normal
        if ($exception instanceof NotFoundHttpException) {
            $response = new Response();

            if (!empty($this->view)) {
                $response->setContent($this->view->render('static/404.html.twig'));
            }

            $response->setStatusCode(Response::HTTP_NOT_FOUND);

            $event->setResponse($response);

        } elseif (
            // User is not logged in
            $exception instanceof AccessDeniedException ||
            $exception instanceof AuthenticationException
        ) {
            $request = $event->getRequest();
            $referrer = $request->getRequestUri();

            $response = new RedirectResponse(
                $this->router->generate('login').'?'.http_build_query(['referrer' => $referrer])
            );

            if ($message = $exception->getMessage()) {
                $request->getSession()->getFlashBag()->add('danger', $message);
            }

            $event->setResponse($response);
        }
    }
}
