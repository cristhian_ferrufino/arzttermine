<?php

namespace ArzttermineBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerInterface;

class VirtualContainer {

    /**
     * Super hacky wrapper method to inject the container into any entity
     *
     * @return ContainerInterface
     */
    public static function get()
    {
        global $kernel;

        if ('AppCache' == get_class($kernel)) {
            $kernel = $kernel->getKernel();
        }

        return $kernel->getContainer();

    }

}