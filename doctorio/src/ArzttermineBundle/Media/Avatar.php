<?php

namespace ArzttermineBundle\Media;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class Avatar
{
    /**
     * @var UploadedFile
     */
    public $file;

    /**
     * @var integer
     */
    public $doctorId;

    /**
     * @var integer
     */
    public $practiceId;
}
