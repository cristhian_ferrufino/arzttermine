<?php

namespace Arzttermine\ServiceBundle\Controller;

use AT\Component\Common\Exception\ServiceException;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Workflow\Exception\InvalidArgumentException;

/**
 * @Security("has_role('ROLE_DOCTOR')")
 */
class TreatmentTypeController extends FOSRestController
{
    /**
     * @Annotations\Get("/treatment-types", name = "get_treatment_types")
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @Annotations\View()
     * @QueryParam(name="limit", nullable=true, strict=true, requirements="\d+", description="Limits the number of list items")
     * @QueryParam(name="offset", nullable=true, requirements="\d+", description="Offset of the limited number of list items")
     * @QueryParam(name="envelope", default=true, description="add meta data to response object")
     * @QueryParam(name="sort", default="type", description="sort by given field")
     *
     * @param ParamFetcher $paramFetcher
     * @return array
     */
    public function cgetAction(ParamFetcher $paramFetcher)
    {
        $limit = $paramFetcher->get('limit') ?: 100;
        $offset = $paramFetcher->get('offset') ?: 0;
        $envelope = (bool) $paramFetcher->get('envelope');
        $sort = $paramFetcher->get('sort');

        return $this->get('arzttermine_service.treatments')->getTreatments($limit, $offset, $sort, $envelope);
    }

    /**
     * @Annotations\Post("/treatment-types", name = "create_treatment_type")
     *
     * #### Example payload
     * ```
     * {
     * "name": "test_tt",
     * "duration": 10
     * }
     * ```
     *
     * @ApiDoc(
     *   input = "Arzttermine\ServiceBundle\Form\TreatmentTypeType",
     *   output = "Arzttermine\ServiceBundle\Entity\TreatmentType",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     422 = "Returned when the form data is invalid"
     *   }
     * )
     * @Annotations\View()
     *
     * @param Request $request
     * @return View
     * @throws NotFoundHttpException when event not exist
     */
    public function postAction(Request $request)
    {
        $form = $this->container->get('arzttermine_service.form.treatment_type');
        $form->handleRequest($request);

        if ($form->isValid()) {
            $type = $this->get('arzttermine_service.treatments')->addTreatment($form->getData());

            $response = [
                'success' => true,
                'message' => '',
                'errors' => [],
                'treatmenttype' => $type,
            ];

            return $this->view($response, Response::HTTP_CREATED);
        } else {
            $response = [
                'success' => false,
                'message' => ($form->getErrors(true, false) != '') ? $form->getErrors(true, false) : 'Invalid input data',
                'errors' => $form->getErrors()
            ];

            return $this->view($response, Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * Delete a single consultation type.
     * @Annotations\Delete("/treatment-types/{id}", name = "delete_treatment_type")
     *
     * @ApiDoc(
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the event is not found"
     *   }
     * )
     * @Annotations\View()
     *
     * @param int $id
     *
     * @return View
     */
    public function deleteAction(int $id)
    {
        $practiceId = $this->get('arzttermine_service.practices')->getPracticeId();
        $client = $this->get('at_client.practices');

        try {
            $client->removeTreatmentType($this->getUser()->getAccessToken(), $practiceId, $id);
        } catch (ServiceException $e) {
            return $this->view(['success' => false, 'errors' => $e->getErrors()]);
        }

        return $this->view(['success' => true]);
    }

    /**
     * @Annotations\Put(
     *     "/treatment-types/{id}",
     *     requirements = {"id" = "\d+"},
     *     name = "update_treatment_type",
     *     options = {"expose" = true, "method_prefix" = false}
     * )
     *
     * @ApiDoc(
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the event is not found"
     *   }
     * )
     * @Annotations\View()
     *
     * @param Request $request
     * @param int $id
     *
     * @return View
     * @throws NotFoundHttpException when resource not exist
     */
    public function putAction(Request $request, int $id)
    {
        $form = $this->container->get('arzttermine_service.form.treatment_type_update');
        $form->handleRequest($request);

        if ($form->isValid()) {
            $success = $this->get('arzttermine_service.treatments')->updateTreatment($id, $form->getData());

            return $this->view();
        } else {
            $response = [
                'success' => false,
                'message' => ($form->getErrors(true, false) != '') ? $form->getErrors(true, false) : 'Invalid input data',
                'errors' => $form->getErrors()
            ];

            return $this->view($response, Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }
}
