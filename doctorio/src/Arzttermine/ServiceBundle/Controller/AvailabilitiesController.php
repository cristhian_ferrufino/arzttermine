<?php

namespace Arzttermine\ServiceBundle\Controller;

use Arzttermine\ServiceBundle\Form\CreateBulkAppointmentsType;
use Arzttermine\ServiceBundle\Model\CreateAppointmentRange;
use AT\Component\Appointment\Transfer\RecurringConfigDto;
use AT\Component\Appointment\Transfer\Request\CreateAvailabilityDto;
use AT\Component\Appointment\Transfer\Request\CreateBulkAvailabilitiesDto;
use AT\Component\Appointment\Transfer\Request\DeleteAvailabilityDto;
use AT\Component\Appointment\Transfer\Request\UpdateBulkAvailabilitiesDto;
use AT\Component\Appointment\Transfer\ResourceRequestDto;
use AT\Component\Common\Exception\ServiceException;
use AT\Component\Search\Filter\ProjectorFilter;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Security("has_role('ROLE_DOCTOR')")
 */
class AvailabilitiesController extends FOSRestController
{
    /**
     * @Get(
     *     "/availabilities",
     *     name = "get_appointment_availabilities",
     *     options = {"expose" = true, "method_prefix" = false}
     * )
     *
     * @param Request $request
     * @return View
     */
    public function getAppointmentAvailabilitiesAction(Request $request)
    {
        $now = new \DateTime(($request->query->has('active'))
            ? $request->query->get('active')
            : '@' . strtotime('last monday', strtotime('tomorrow')));

        $end = clone $now;
        $end->add(new \DateInterval('P8D'));

        $appointments = $this->get('arzttermine_service.appointments')->getAppointmentRanges($now, $end);

        return $this->view($appointments, 200);
    }

    /**
     * @Delete(
     *     "/availabilities/{id}",
     *     name = "delete_availability",
     *     options = {"expose" = true, "method_prefix" = false}
     * )
     *
     * @param int $id
     * @param DeleteAvailabilityDto $request
     * @return View
     */
    public function deleteAvailabilityAction(int $id, DeleteAvailabilityDto $request = null)
    {
        try {
            $this->get('arzttermine_service.appointments')->deleteAvailability($id, $request);
            return $this->view(null, 204);
        } catch (ServiceException $e) {
            return $this->view($e->getErrors(), $e->getStatus());
        }
    }

    /**
     * @Post(
     *     "/availabilities",
     *     name = "create_appointment_ranges",
     *     options = {"expose" = true, "method_prefix" = false}
     * )
     *
     * @param Request $request
     * @return View
     */
    public function createAvailabilityAction(Request $request)
    {
        $practiceId = $this->get('arzttermine_service.practices')->getPracticeId();
        $practice = $this->get('at_client.practices.search')->getPractice($practiceId, 'germany', [
            new ProjectorFilter('treatmentTypes'),
            new ProjectorFilter('insuranceTypes'),
        ]);

        $insuranceTypes = $this->get('arzttermine.mapper.insurance_types')->getInsuranceTranslationIdMapping($practice['insuranceTypes'] ?: ['COMPULSORY', 'PRIVATE']);

        $form = $this->createForm(CreateBulkAppointmentsType::class, null, [
            'treatmentTypes' => $practice['treatmentTypes'],
            'insuranceTypes' => $insuranceTypes,
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $createDto = new CreateBulkAvailabilitiesDto();
            $updateDto = new UpdateBulkAvailabilitiesDto();

            /** @var CreateAppointmentRange */
            foreach ($form->getData()->rows as $entry) {
                /** @TODO mapper */
                $dto = new CreateAvailabilityDto();
                $dto->practiceId = $entry->practiceId;
                $dto->duration = $entry->duration;
                $dto->treatmentTypes = $form->getData()->treatmentTypes;
                $dto->insuranceTypes = $form->getData()->insuranceTypes;
                $dto->resourceRequests = array_map(function ($id) {
                    $request = new ResourceRequestDto();
                    $request->resourceId = $id;
                    return $request;
                }, $entry->resourceIds);

                // should later on be send from the client
                $zonedDate = new \DateTime($entry->date->format('Y-m-d'), new \DateTimeZone('Europe/Berlin'));

                $begin = explode(':', $entry->rangeStart);
                $dto->from = clone $zonedDate;
                $dto->from->add(new \DateInterval($this->getInterval($begin)));

                $end = explode(':', $entry->rangeEnd);
                $dto->to = clone $zonedDate;
                $dto->to->add(new \DateInterval($this->getInterval($end)));

                if (empty($entry->id)) {
                    $createDto->requests[] = $dto;
                } else {
                    $updateDto->updates[$entry->id] = $dto;
                }
            }

            /** @var RecurringConfigDto $recurringConfig */
            $recurringConfig = $form->getData()->recurringConfig;
            if (null !== $recurringConfig && $recurringConfig->type !== 'NEVER') {

                $createDto->recurringConfig = $recurringConfig;
                $updateDto->recurringConfig = $recurringConfig;
            } else {
                $updateDto->disableRecurringAt = $form->getData()->onDate;
            }

            $success = true;

            try {
                $client = $this->get('arzttermine_service.appointments');

                // updates first, otherwise creations can trigger id change on merge
                if (count($updateDto->updates) > 0) {
                    $success = $client->updateAvailabilities($updateDto);
                }
                if (count($createDto->requests) > 0) {
                    $success = $client->createAvailabilities($createDto);
                }
            } catch (ServiceException $e) {
                return $this->view([
                    'success' => false,
                    'errors' => $e->getErrors() ?: []
                ], $e->getStatus());
            }

            return $this->view([
                'success' => $success,
            ], 201);
        }

        return $this->view([
            'success' => false,
            'errors' => $form->getErrors(),
        ], 400);
    }

    private function getInterval(array $parts): string {
        $hour = 1* $parts[0];
        $minute = 1* $parts[1];

        return sprintf('PT%dH%s', $hour, ($minute === 0) ? '' : $minute . 'M');
    }
}
