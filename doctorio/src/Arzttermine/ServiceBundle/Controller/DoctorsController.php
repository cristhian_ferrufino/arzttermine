<?php

namespace Arzttermine\ServiceBundle\Controller;

use Arzttermine\UserBundle\Form\UpdateUserType;
use AT\Component\Common\Exception\ServiceException;
use AT\Component\Practice\Transfer\Request\UpdateDoctorDto;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Security("has_role('ROLE_DOCTOR')")
 */
class DoctorsController extends FOSRestController
{
    /**
     * @Get(
     *     "/doctors",
     *     name = "get_doctors",
     *     options = {"expose" = true, "method_prefix" = false}
     * )
     */
    public function getAction()
    {
        $practiceId = $this->get('arzttermine_service.practices')->getPracticeId();
        $practice = $this->get('at_client.practices.search')->getPractice($practiceId);
        usort($practice['doctors'], function ($a, $b) {
            return $a['id'] <=> $b['id'];
        });

        return $this->view($practice['doctors'], 200);
    }

    /**
     * @Put(
     *     "/doctors/{doctor}",
     *     name = "update_doctor",
     *     options = {"expose" = true, "method_prefix" = false}
     * )
     *
     * @param int $doctor
     * @param UpdateDoctorDto $dto
     *
     * @return \FOS\RestBundle\View\View
     */
    public function updateAction(int $doctor, UpdateDoctorDto $dto = null)
    {
        if (null === $dto) {
            return $this->view(['success' => false], 400);
        }

        $practice = $this->get('arzttermine_service.practices')->getPracticeId();
        $client = $this->get('at_client.practices');

        try {
            $client->updateDoctor($this->getUser()->getAccessToken(), $practice, $doctor, $dto);
        } catch (ServiceException $e) {
            // @TODO logging
            return $this->view(['success' => false, 'errors' => $e->getErrors()], $e->getStatus());
        }

        return $this->view(['success' => true], 201);
    }

    /**
     * @Post(
     *     "/doctors",
     *     name = "create_doctor",
     *     options = {"expose" = true, "method_prefix" = false}
     * )
     *
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function createAction(Request $request)
    {
        $practice = $this->get('arzttermine_service.practices')->getPracticeId();
        $client = $this->get('at_client.practices');

        $form = $this->createForm(UpdateUserType::class);
        $form->handleRequest($request);

        try {
            $client->createDoctor($this->getUser()->getAccessToken(), $practice, $form->getData());
        } catch (ServiceException $e) {
            return $this->view(['success' => false, 'errors' => $e->getErrors()], $e->getStatus());
        }

        return $this->view(['success' => true], 201);
    }
}
