<?php

namespace Arzttermine\ServiceBundle\Controller;

use AT\Component\Appointment\Transfer\Request\MoveAppointmentDto;
use AT\Component\Booking\Filter\AppointmentTimeRangeFilter;
use AT\Component\Booking\Filter\ResourcesFilter;
use AT\Component\Booking\Transfer\Request\CancelBookingDto;
use AT\Component\Common\Exception\ServiceException;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Security("has_role('ROLE_DOCTOR')")
 */
class EventController extends FOSRestController
{
    /**
     * @Annotations\Get("/events", name = "get_events")
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @Annotations\View()
     * @QueryParam(name="start_date", nullable=true, description="Filters by start_date")
     * @QueryParam(name="end_date", nullable=true, description="Filters by end_date")
     * @QueryParam(name="resource_id", nullable=true, description="Filters by resource_id")
     * @QueryParam(name="limit", nullable=true, strict=true, requirements="\d+", description="Limits the number of list items")
     * @QueryParam(name="offset", nullable=true, requirements="\d+", description="Offset of the limited number of list items")
     * @QueryParam(name="envelope", default=true, description="add meta data to response object")
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return array
     */
    public function cgetAction(ParamFetcher $paramFetcher)
    {
        $limit = $paramFetcher->get('limit') ?: 100;
        $offset = $paramFetcher->get('offset') ?: 0;
        $envelope = (bool) $paramFetcher->get('envelope');

        $filters = [];

        $startDate = $paramFetcher->get('start_date');
        $endDate = $paramFetcher->get('end_date');
        $resourceId = $paramFetcher->get('resource_id');

        if (null !== $startDate && null !== $endDate) {
            $filters[] = new AppointmentTimeRangeFilter(\DateTime::createFromFormat('Y-m-d', $startDate), \DateTime::createFromFormat('Y-m-d', $endDate));
        }
        if (null !== $resourceId) {
            $filters[] = new ResourcesFilter($resourceId);
        }

        try {
            return $this->get('arzttermine_service.bookings')->findBookings($filters, $limit, $offset, $envelope);
        } catch(\Exception $e) {
            return ['error' => $e->getMessage()];
        }
    }

    /**
     * @Annotations\Get("/events/{id}", name = "get_event")
     *
     * * #### Example payload
     * ```
     * {
     *   "title": "test",
     *   "description": "test desc",
     *   "comment": "test comment",
     *   "start_date": "2015-12-02T08:00:00+01:00",
     *   "end_date": "2015-12-02T08:00:00+01:00",
     *   "all_day": false,
     *   "patient": {
     *       "first_name": "Simon",
     *       "last_name": "Chrzanowski",
     *       "phone": "0123456789",
     *       "mobile": "0123-456789",
     *       "email": "simon.chrzanowski@arzttermine.de",
     *       "notify_email": true,
     *       "notify_sms": false
     *   },
     *   "treatment_type": 1,
     *   "repeat": false,
     *   "resources": [2,3]
     *   }
     * ```
     *
     * @ApiDoc(
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the event is not found"
     *   }
     * )
     * @Annotations\View()
     *
     * @return array|Response
     * @throws NotFoundHttpException when event not exist
     */
    public function getAction($id)
    {
        try {
            return $this->get('arzttermine_service.bookings')->getBooking($id);
        } catch (\Exception $e) {
            return $this->handleView($this->view([], 404));
        }
    }

    /**
     * angularjs is not able to send a DELETE request with body
     * @Annotations\Post("/events/{id}", name="cancel_event")
     *
     * @ApiDoc(
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the event is not found"
     *   }
     * )
     *
     * @param $id
     * @param CancelBookingDto $request
     * @return Response
     */
    public function cancelAction($id, CancelBookingDto $request)
    {
        try {
            $success = $this->get('arzttermine_service.bookings')->cancelBooking($id, $request);
        } catch (ServiceException $e) {
            return $this->handleView($this->view($e->getErrors(), $e->getStatus()));
        }

        return $this->handleView($this->view(['success' => $success], 200));
    }

    /**
     * @Annotations\Post("/events/{id}/move", name="move_event")
     *
     * @ApiDoc(
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the event is not found"
     *   }
     * )
     *
     * @param $id
     * @param MoveAppointmentDto $request
     * @return Response
     */
    public function moveAction($id, MoveAppointmentDto $request)
    {
        try {
            $success = $this->get('arzttermine_service.appointments')->moveAppointment($id, $request);
        } catch (ServiceException $e) {
            return $this->handleView($this->view($e->getErrors(), $e->getStatus()));
        }

        return $this->handleView($this->view(['success' => $success], 200));
    }
}
