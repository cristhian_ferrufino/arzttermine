<?php

namespace Arzttermine\ServiceBundle\Controller;

use Arzttermine\ServiceBundle\Form\ResourceType;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Security("has_role('ROLE_DOCTOR')")
 */
class ResourceController extends FOSRestController
{
    /**
     * @Get("/resources", name = "get_resources")
     * @QueryParam(name="envelope", default=true, description="add meta data to response object")
     * @QueryParam(name="limit", nullable=true, strict=true, requirements="\d+", description="Limits the number of list items")
     * @QueryParam(name="offset", nullable=true, requirements="\d+", description="Offset of the limited number of list items")
     * @QueryParam(name="sort", nullable=true, description="sort by given field")
     *
     * @Annotations\View()
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @param ParamFetcher $params
     * @return array
     */
    public function getAction(ParamFetcher $params)
    {
        $limit = $params->get('limit') ?: 100;
        $offset = $params->get('offset') ?: 0;
        $sort = $params->get('sort');
        $wrap = (bool) $params->get('envelope');

        return $this->get('arzttermine_service.resources')->getResources($limit, $offset, $sort, true, $wrap);
    }

    /**
     * @Post("/resources", name = "create_resource")
     *
     * @ApiDoc(
     *   input = "Arzttermine\CalendarBundle\Form\ResourceType",
     *   output = "Arzttermine\CalendarBundle\Entity\Resource",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the resource is not found"
     *   }
     * )
     * @Annotations\View()
     *
     * @param Request $request
     * @return View
     * @throws NotFoundHttpException when resource not exist
     */
    public function postAction(Request $request)
    {
        $form = $this->createForm(ResourceType::class, null, ['resourceTypes' => ['DEVICE', 'ROOM', 'ASSISTANT', 'DOCTOR']]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $resource = $this->get('arzttermine_service.resources')->addResource($form->getData());

            $response = [
                'success' => true,
                'message' => '',
                'errors' => [],
                'resource' => $resource
            ];

            return $this->view($response, Response::HTTP_CREATED);
        } else {
            $response = [
                'success' => false,
                'message' => ($form->getErrors(true, false) != '') ? $form->getErrors(true, false) : 'Invalid input data',
                'errors' => $form->getErrors()
            ];
            return $this->view($response, Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * @Delete("/resources/{id}", name = "delete_resource")
     *
     * @ApiDoc(
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the event is not found"
     *   }
     * )
     * @Annotations\View()
     *
     * @return array
     * @throws NotFoundHttpException when resource not exist
     */
    public function deleteAction($id)
    {
        return $this->get('arzttermine_service.resources')->deleteResource($id);
    }

    /**
     * Update a single resource.
     *
     * @Put("/resources/{id}", name = "update_resource")
     *
     * @ApiDoc(
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the event is not found"
     *   }
     * )
     * @Annotations\View()
     *
     * @param Request $request
     * @param int $id
     *
     * @return View
     * @throws NotFoundHttpException when resource not exist
     */
    public function putAction(Request $request, $id)
    {
        $form = $this->container->get('arzttermine_service.form.resource_update');
        $form->handleRequest($request);

        if ($form->isValid()) {
            $success = $this->get('arzttermine_service.resources')->updateResource($id, $form->getData());

            return $this->view();
        } else {
            $response = [
                'success' => false,
                'message' => ($form->getErrors(true, false) != '') ? $form->getErrors(true, false) : 'Invalid input data',
                'errors' => $form->getErrors()
            ];

            return $this->view($response, Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }
}
