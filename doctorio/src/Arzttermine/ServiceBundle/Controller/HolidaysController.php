<?php

namespace Arzttermine\ServiceBundle\Controller;

use Arzttermine\ServiceBundle\Form\CreateHolidayType;
use AT\Component\Appointment\Transfer\Request\CreateHolidaysDto;
use AT\Component\Common\Exception\ServiceException;
use FOS\RestBundle\Controller\Annotations as Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Security("has_role('ROLE_DOCTOR')")
 */
class HolidaysController extends FOSRestController
{
    /**
     * @Annotations\View()
     * @Annotations\Post(
     *     "/holidays",
     *     name = "post_holidays",
     *     options = {"expose" = true, "method_prefix" = false}
     * )
     *
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function createAction(Request $request)
    {
        $form = $this->createForm(CreateHolidayType::class);
        $form->handleRequest($request);

        $token = $this->getUser()->getAccessToken();
        $result = null;

        $request = new CreateHolidaysDto();
        $request->practiceId = $this->get('arzttermine_service.practices')->getPracticeId();
        $request->times = [$form->getData()];

        try {
            $result = $this->get('at_client.appointments')->createHolidays($token, $request);
        } catch (ServiceException $e) {
            return $this->view(['success' => false, 'errors' => $e->getErrors()], $e->getStatus());
        }

        return $this->view(['success' => true, 'entry' => $result], 201);
    }

    /**
     * @Annotations\View()
     * @Annotations\Delete(
     *     "/holidays/{id}",
     *     name = "delete_holiday",
     *     options = {"expose" = true, "method_prefix" = false}
     * )
     *
     * @param int $id
     * @return \FOS\RestBundle\View\View
     */
    public function deleteAction(int $id)
    {
        $token = $this->getUser()->getAccessToken();

        try {
            $this->get('at_client.appointments')->deleteHoliday($token, $id);
        } catch (ServiceException $e) {
            return $this->view(['success' => false, 'errors' => $e->getErrors()], $e->getStatus());
        }

        return $this->view(['success' => true]);
    }
}
