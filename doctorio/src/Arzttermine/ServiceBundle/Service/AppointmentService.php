<?php

namespace Arzttermine\ServiceBundle\Service;

use Arzttermine\ServiceBundle\Model\BulkAppointmentsAction;
use Arzttermine\ServiceBundle\Model\CreateAppointmentRange;
use Arzttermine\ServiceBundle\Model\RecurringConfiguration;
use AT\Bundle\CustomerBundle\Service\OAuth2TokenStorage;
use AT\Component\Appointment\AppointmentClient;
use AT\Component\Appointment\Transfer\AppointmentDto;
use AT\Component\Appointment\Transfer\AppointmentsPageDto;
use AT\Component\Appointment\Transfer\RecurringConfigDto;
use AT\Component\Appointment\Transfer\Request\CreateAppointmentsDto;
use AT\Component\Appointment\Transfer\Request\CreateAvailabilityDto;
use AT\Component\Appointment\Transfer\Request\CreateBulkAvailabilitiesDto;
use AT\Component\Appointment\Transfer\Request\DeleteAvailabilityDto;
use AT\Component\Appointment\Transfer\Request\MoveAppointmentDto;
use AT\Component\Appointment\Transfer\Request\RequestPlannedAvailabilitiesDto;
use AT\Component\Appointment\Transfer\Request\UpdateBulkAvailabilitiesDto;
use AT\Component\Common\Exception\ServiceException;
use AT\Component\Common\PageRequest;
use AT\Component\Common\RequestOptions;
use AT\Component\Common\SortRequest;

class AppointmentService
{
    /**
     * @var PracticeService
     */
    private $practiceService;

    /**
     * @var AppointmentClient
     */
    private $client;

    /**
     * @var OAuth2TokenStorage
     */
    private $tokenStorage;

    /**
     * @param PracticeService $practiceService
     * @param AppointmentClient $client
     * @param OAuth2TokenStorage $tokenStorage
     */
    public function __construct(PracticeService $practiceService, AppointmentClient $client, OAuth2TokenStorage $tokenStorage)
    {
        $this->practiceService = $practiceService;
        $this->client = $client;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param \DateTime $date
     * @param int $resourceId
     * @param int $practiceId
     * @param int[] $ids
     * @return BulkAppointmentsAction
     */
    public function prepareBulkForm(\DateTime $date, int $resourceId, int $practiceId, array $ids = [])
    {
        $data = new BulkAppointmentsAction();
        /** @var RecurringConfigDto|null $recurringConfig */
        $recurringConfig = null;

        if (count($ids) > 0) {
            try {
                $resourceMap = $this->getAvailabilities($ids);
            } catch (ServiceException $e) {
                // @TODO logging
                $resourceMap = [];
            }

            foreach ($resourceMap as $resourceId => $availabilities) {
                foreach ($availabilities as $availability) {
                    /** @TODO move to data transformer? */
                    $entry = new CreateAppointmentRange();
                    $entry->id = $availability->id;
                    $entry->date = $availability->begin;
                    $entry->rangeStart = sprintf('%02d:%02d', $availability->begin->format('H'), $availability->begin->format('i'));
                    $entry->rangeEnd = sprintf('%02d:%02d', $availability->end->format('H'), $availability->end->format('i'));
                    $entry->treatmentTypes = $availability->treatmentTypes;
                    $entry->insuranceTypes = $availability->insuranceTypes;
                    $entry->duration = $availability->duration;
                    $entry->practiceId = $practiceId;
                    $entry->resourceIds = [$resourceId];
                    $data->rows[] = $entry;

                    if (null === $recurringConfig && isset($availability->recurringConfig)) {
                        $recurringConfig = $availability->recurringConfig;
                    }
                }
            }
        }

        if (empty($data->rows)) {
            $entry = new CreateAppointmentRange();
            $entry->resourceIds = [$resourceId];
            $entry->practiceId = $practiceId;
            $entry->rangeStart = '09:00';
            $entry->rangeEnd = '14:00';
            $entry->date = $date;
            $data->rows[] = $entry;
        }

        if (null !== $recurringConfig) {
            $config = new RecurringConfiguration();
            $config->type = $recurringConfig->type;
            $config->endsAt = $recurringConfig->endsAt;
            $data->recurringConfig = $config;
        }

        return $data;
    }

    /**
     * @param CreateBulkAvailabilitiesDto $requests
     * @return bool
     */
    public function createAvailabilities(CreateBulkAvailabilitiesDto $requests)
    {
        $token = $this->tokenStorage->getAccessToken();
        return $this->client->createAvailabilities($token, $requests);
    }

    /**
     * @param int $appointmentId
     * @param MoveAppointmentDto $request
     * @return bool
     */
    public function moveAppointment(int $appointmentId, MoveAppointmentDto $request)
    {
        $token = $this->tokenStorage->getAccessToken();
        return $this->client->moveAppointment($token, $appointmentId, $request) != null;
    }

    /**
     * @param UpdateBulkAvailabilitiesDto $requests
     * @return bool
     */
    public function updateAvailabilities(UpdateBulkAvailabilitiesDto $requests)
    {
        $token = $this->tokenStorage->getAccessToken();
        return $this->client->updateAvailabilities($token, $requests);
    }

    /**
     * @param int $id
     * @param DeleteAvailabilityDto $request
     * @return bool
     */
    public function deleteAvailability(int $id, DeleteAvailabilityDto $request = null)
    {
        $token = $this->tokenStorage->getAccessToken();
        return $this->client->deleteAvailability($token, $id, $request);
    }

    /**
     * @param CreateAppointmentsDto $request
     * @return array|AppointmentDto[]
     */
    public function createAppointments(CreateAppointmentsDto $request): array
    {
        $token = $this->tokenStorage->getAccessToken();
        return $this->client->createAppointments($token, $request);
    }

    /**
     * @param \DateTime $from
     * @param \DateTime $to
     * @param integer[] $doctorIds
     * @param integer[] $appointmentIds
     * @return AppointmentsPageDto
     */
    public function getAppointments(\DateTime $from, \DateTime $to, array $doctorIds = [], array $appointmentIds = []): AppointmentsPageDto
    {
        $options = new RequestOptions();
        $options->setPageRequest(new PageRequest(0, 500)); // @TODO loop until no further pages are there
        $options->addSortRequest(new SortRequest('start'));
        $options->addFilter('practiceId', $this->practiceService->getPracticeId());
        $options->addFilter('start', $from->format('Y-m-d\TH:i:s') . 'Z');
        $options->addFilter('end', $to->format('Y-m-d\TH:i:s') . 'Z');
        foreach ($doctorIds as $id) {
            $options->addFilter('doctor.id', $id);
        }
        foreach($appointmentIds as $id) {
            $options->addFilter('id', $id);
        }

        $token = $this->tokenStorage->getAccessToken();
        return $this->client->getAppointments($token, $options);
    }

    /**
     * @param \DateTime $from
     * @param \DateTime $to
     * @param integer[] $resourceIds
     * @return array
     */
    public function getAppointmentRanges(\DateTime $from, \DateTime $to, array $resourceIds = []): array
    {
        $token = $this->tokenStorage->getAccessToken();
        $practiceId = $this->practiceService->getPracticeId();

        $from->setTimezone(new \DateTimeZone('UTC'));
        $to->setTimezone(new \DateTimeZone('UTC'));

        $dto = new RequestPlannedAvailabilitiesDto();
        $dto->resourceIds = $resourceIds;
        $dto->practiceId = $practiceId;
        $dto->start = $from;
        $dto->end = $to;

        return $this->client->getPlannedAvailabilities($token, $dto);
    }

    /**
     * @param array $ids
     * @return array
     */
    public function getAvailabilities(array $ids): array
    {
        $token = $this->tokenStorage->getAccessToken();
        $practiceId = $this->practiceService->getPracticeId();

        $options = new RequestOptions();
        $options->addSortRequest(new SortRequest('begin'));

        foreach ($ids as $id) {
            $options->addFilter('id', $id);
        }
        return $this->client->getAvailabilities($token, $practiceId, $options);
    }
}
