<?php

namespace Arzttermine\ServiceBundle\Service\Mapper;

use AT\Component\Practice\Transfer\PracticeDto;

class InsuranceTypeMapper
{

    const VALUE_TO_TRANS_ID_MAPPING = [
        'COMPULSORY' => 'ui.appointments.insurance.compulsory',
        'PRIVATE' => 'ui.appointments.insurance.private',
    ];

    /**
     * Returns a map that relates insurance types to their translation ids
     * @param $insurancesTypes array<string>
     * @return array<string,string>
     */
    public function getInsuranceTranslationIdMapping(array $insurancesTypes): array
    {
        return array_map(function ($insuranceType)  {
            return array(self::VALUE_TO_TRANS_ID_MAPPING[$insuranceType] => $insuranceType);
        }, $insurancesTypes);
    }

}
