<?php

namespace Arzttermine\ServiceBundle\Service;

use AT\Component\Common\Transfer\Languages;
use AT\Component\Practice\Transfer\MedicalSpecialtyType;
use Doctrine\Common\Cache\Cache;

class SuggestionService
{
    const MEDICAL_SPECIALTIES_CACHE_KEY = 'at_medical_specialties_suggestions';
    const TREATMENT_TYPES_CACHE_KEY = 'at_treatment_types_suggestions';
    const LANGUAGES_CACHE_KEY = 'at_languages_suggestions';
    const TTL = (60 * 60) * 2;

    /**
     * @var Cache
     */
    private $cache;

    /**
     * @param Cache $cache
     */
    public function __construct(Cache $cache)
    {
        $this->cache = $cache;
    }

    /**
     * @return string[]
     */
    public function getMedicalSpecialties(): array {
        $specialties = $this->cache->fetch(self::MEDICAL_SPECIALTIES_CACHE_KEY);

        if (false === $specialties) {
            $reflector = new \ReflectionClass(MedicalSpecialtyType::class);
            $specialties = array_values($reflector->getConstants());

            $this->cache->save(self::MEDICAL_SPECIALTIES_CACHE_KEY, $specialties, self::TTL);
        }

        return $specialties;
    }

    /**
     * @return string[]
     */
    public function getLanguages(): array {
        $languages = $this->cache->fetch(self::LANGUAGES_CACHE_KEY);

        if (false === $languages) {
            $reflector = new \ReflectionClass(Languages::class);
            $languages = array_values($reflector->getConstants());

            $this->cache->save(self::LANGUAGES_CACHE_KEY, $languages, self::TTL);
        }

        return $languages;
    }
}
