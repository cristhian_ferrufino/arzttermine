<?php

namespace Arzttermine\ServiceBundle\Service;

use AT\Bundle\CustomerBundle\Service\OAuth2TokenStorage;
use AT\Component\Common\PageRequest;
use AT\Component\Common\RequestOptions;
use AT\Component\Common\SortRequest;
use AT\Component\Practice\PracticeClient;
use AT\Component\Practice\PracticeSearchClient;
use AT\Component\Practice\Transfer\Request\CreateTreatmentTypeDto;
use AT\Component\Practice\Transfer\TreatmentTypeDto;
use AT\Component\Practice\Transfer\TreatmentTypePageDto;

class TreatmentService
{
    /**
     * @var OAuth2TokenStorage
     */
    private $tokenStorage;

    /**
     * @var PracticeClient
     */
    private $client;

    /**
     * @var PracticeService
     */
    private $practiceService;

    /**
     * @var PracticeSearchClient
     */
    private $practiceSearch;

    /**
     * @param PracticeService $practiceService
     * @param PracticeClient $client
     * @param OAuth2TokenStorage $tokenStorage
     * @param PracticeSearchClient $practiceSearchClient
     */
    public function __construct(PracticeService $practiceService, PracticeClient $client, OAuth2TokenStorage $tokenStorage, PracticeSearchClient $practiceSearchClient)
    {
        $this->client = $client;
        $this->tokenStorage = $tokenStorage;
        $this->practiceService = $practiceService;
        $this->practiceSearch = $practiceSearchClient;
    }

    /**
     * @param CreateTreatmentTypeDto $data
     * @return TreatmentTypeDto
     */
    public function addTreatment(CreateTreatmentTypeDto $data): TreatmentTypeDto
    {
        $practiceId = $this->practiceService->getPracticeId();
        return $this->client->addTreatmentType($this->tokenStorage->getAccessToken(), $practiceId, $data);
    }

    /**
     * @param int $treatmentId
     * @param CreateTreatmentTypeDto $patch
     * @return bool
     */
    public function updateTreatment(int $treatmentId, CreateTreatmentTypeDto $patch): bool
    {
        $practiceId = $this->practiceService->getPracticeId();
        return $this->client->updateTreatmentType($this->tokenStorage->getAccessToken(), $practiceId, $treatmentId, $patch);
    }

    /**
     * @param int $limit
     * @param int $offset
     * @param string|null $sortField
     * @param bool $envelope
     * @return array|TreatmentTypePageDto
     */
    public function getTreatments(int $limit, int $offset, string $sortField = null, $envelope = true)
    {
        $options = new RequestOptions();
        $options->setPageRequest(new PageRequest(floor($offset / $limit), $limit));
        $options->addFilter('enabled',true);


        if (null !== $sortField) {
            $options->addSortRequest(new SortRequest($sortField));
        }

        $token = $this->tokenStorage->getAccessToken();
        $practiceId = $this->practiceService->getPracticeId();

        $result = $this->client->getTreatmentTypes($token, $practiceId, $options);

        if (false !== $envelope) {
            return [
                'total_entries' => $result->page->totalItems,
                'current_page' => $result->page->currentPage + 1,
                'offset' => $offset,
                'data' => $result->elements,
            ];
        }

        return $result->elements;
    }

}
