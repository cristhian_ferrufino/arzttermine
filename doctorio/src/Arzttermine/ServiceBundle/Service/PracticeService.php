<?php

namespace Arzttermine\ServiceBundle\Service;

use AT\Bundle\CustomerBundle\Model\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Validator\Exception\MissingOptionsException;

class PracticeService
{
    /**
     * @var int|null
     */
    private $practiceId;

    /**
     * @return int
     * @throws MissingOptionsException
     */
    public function getPracticeId(): int {
        if (!$this->hasPracticeId()) {
            throw new MissingOptionsException('missing practice id', ['practiceId']);
        }

        return $this->practiceId;
    }

    /**
     * @param int $id
     */
    public function setPracticeId(int $id) {
        $this->practiceId = $id;
    }

    public function hasPracticeId(): bool {
        return null !== $this->practiceId;
    }

    /**
     * @param TokenInterface $token
     * @return bool
     */
    public function isDoctor(TokenInterface $token): bool
    {
        if (null === $token || !($token->getUser() instanceof User)) {
            return false;
        }

        foreach ($token->getRoles() as $role) {
            if ($role->getRole() === 'ROLE_DOCTOR') {
                return true;
            }
        }

        return false;
    }
}
