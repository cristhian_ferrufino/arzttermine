<?php

namespace Arzttermine\ServiceBundle\Service;

use AT\Bundle\CustomerBundle\Service\OAuth2TokenStorage;
use AT\Component\Common\PageRequest;
use AT\Component\Common\RequestOptions;
use AT\Component\Common\SortRequest;
use AT\Component\Practice\PracticeClient;
use AT\Component\Practice\Transfer\Request\CreateResourceDto;
use AT\Component\Practice\Transfer\Request\UpdateDoctorDto;
use AT\Component\Practice\Transfer\ResourceDto;
use AT\Component\Practice\Transfer\ResourcePageDto;

class ResourceService
{
    /**
     * @var OAuth2TokenStorage
     */
    private $tokenStorage;

    /**
     * @var PracticeClient
     */
    private $client;

    /**
     * @var PracticeService
     */
    private $practiceService;

    /**
     * @param PracticeService $practiceService
     * @param PracticeClient $client
     * @param OAuth2TokenStorage $tokenStorage
     */
    public function __construct(PracticeService $practiceService, PracticeClient $client, OAuth2TokenStorage $tokenStorage)
    {
        $this->client = $client;
        $this->tokenStorage = $tokenStorage;
        $this->practiceService = $practiceService;
    }

    /**
     * @param CreateResourceDto $data
     * @return ResourceDto
     */
    public function addResource(CreateResourceDto $data): ResourceDto
    {
        $data->type = strtoupper($data->type);
        $practiceId = $this->practiceService->getPracticeId();
        return $this->client->addResource($this->tokenStorage->getAccessToken(), $practiceId, $data);
    }

    /**
     * @param int $resourceId
     * @param CreateResourceDto $patch
     * @return bool
     */
    public function updateResource(int $resourceId, CreateResourceDto $patch): bool
    {
        $patch->type = strtoupper($patch->type);
        $practiceId = $this->practiceService->getPracticeId();
        return $this->client->updateResource($this->tokenStorage->getAccessToken(), $practiceId, $resourceId, $patch);
    }

    /**
     * @param int $resourceId
     * @return ResourceDto
     */
    public function getResource(int $resourceId): ResourceDto
    {
        $practiceId = $this->practiceService->getPracticeId();
        return $this->client->getResource($this->tokenStorage->getAccessToken(), $practiceId, $resourceId);
    }

    /**
     * @param int $customerId
     * @param array $treatmentTypeIds
     * @return bool
     */
    public function assignTreatments(int $customerId, array $treatmentTypeIds): bool
    {
        $practiceId = $this->practiceService->getPracticeId();
        $token = $this->tokenStorage->getAccessToken();

        $dto = new UpdateDoctorDto();
        $dto->treatmentTypes = $treatmentTypeIds;

        return $this->client->updateDoctor($token, $practiceId, $customerId, $dto);
    }

    /**
     * @param int $limit
     * @param int $offset
     * @param string|null $sortField
     * @param bool $groupByType
     * @param bool $paging
     * @return array|ResourcePageDto
     */
    public function getResources(int $limit, int $offset, string $sortField = null, $groupByType = false, $paging = false)
    {
        $options = new RequestOptions();
        $options->setPageRequest(new PageRequest(floor($offset / $limit), $limit));

        if (null !== $sortField) {
            $options->addSortRequest(new SortRequest($sortField));
        }

        $token = $this->tokenStorage->getAccessToken();
        $practiceId = $this->practiceService->getPracticeId();
        $result = $this->client->getResources($token, $practiceId, $options);

        if (!$groupByType) {
            return $result->elements;
        }

        $resourceGroups = [];

        foreach ($result->elements as $resource) {
            $resourceType = [
                'multiple' => true,
                'required' => false,
                'type' => $resource->type,
                'title' => $resource->type,
                'systemId' => $resource->systemId,
                'resources' => []
            ];

            $index = array_search($resource->type, array_column($resourceGroups, 'title'));

            if (false === $index) {
                $resourceGroups[] = $resourceType;
                $index = count($resourceGroups) -1;
            }

            $resourceGroups[$index]['resources'][] = [
                'name' => $resource->name,
                'type' => $resourceType['type'],
                'color' => $resource->color,
                'systemId' => $resource->systemId,
                'id' => $resource->id,
            ];
        }

        if ($paging || !isset($paging)) {
            return [
                'total_entries' => $result->page->totalItems,
                'current_page' => $result->page->currentPage + 1,
                'offset' => $offset,
                'data' => $resourceGroups,
            ];
        }

        return $resourceGroups;
    }

    /**
     * @param int $id
     */
    public function deleteResource(int $id)
    {
        $practiceId = $this->practiceService->getPracticeId();
        $this->client->deleteResource($this->tokenStorage->getAccessToken(), $practiceId, $id);
    }
}
