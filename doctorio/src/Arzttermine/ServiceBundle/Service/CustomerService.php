<?php

namespace Arzttermine\ServiceBundle\Service;

use Arzttermine\UserBundle\Model\ResetPassword;
use AT\Component\Customer\CustomerClient;
use AT\Component\Customer\TokenClientInterface;
use AT\Component\Customer\Transfer\PasswordResetDto;
use AT\Component\Customer\Transfer\Request\PasswordResetRequestDto;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CustomerService
{
    /**
     * @var CustomerClient
     */
    private $client;

    /**
     * @var TokenClientInterface
     */
    private $tokenService;

    /**
     * @param CustomerClient $client
     * @param TokenClientInterface $tokenClient
     */
    public function __construct(CustomerClient $client, TokenClientInterface $tokenClient)
    {
        $this->client = $client;
        $this->tokenService = $tokenClient;
    }

    /**
     * @param PasswordResetRequestDto $requestDto
     * @return bool
     */
    public function requestPasswordReset(PasswordResetRequestDto $requestDto): bool
    {
        try {
            $token = $this->tokenService->getToken();
            return $this->client->requestPasswordReset($token->token, $requestDto);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param string $token
     * @param string $email
     * @return PasswordResetDto
     */
    public function getResetRequest(string $token, string $email): PasswordResetDto
    {
        try {
            $authToken = $this->tokenService->getToken();
            return $this->client->getPasswordResetRequest($authToken->token, Uuid::fromString($token), $email);
        } catch(\InvalidArgumentException $e) {
            throw new BadRequestHttpException(null, $e);
        } catch (\Exception $e) {
            throw new NotFoundHttpException(null, $e);
        }
    }

    /**
     * @param ResetPassword $requestDto
     * @return bool
     */
    public function resetPassword(ResetPassword $requestDto): bool
    {
        try {
            if (null === $requestDto->repeatPassword) {
                $requestDto->repeatPassword = $requestDto->password;
            }

            $token = $this->tokenService->getToken();
            return $this->client->resetPassword($token->token, Uuid::fromString($requestDto->token), $requestDto->customerId, $requestDto);
        } catch (\InvalidArgumentException $e) {
            throw new BadRequestHttpException(null, $e);
        } catch (\Exception $e) {
            return false;
        }
    }
}
