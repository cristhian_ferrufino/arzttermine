<?php

namespace Arzttermine\ServiceBundle\Service;

use AT\Bundle\CustomerBundle\Service\OAuth2TokenStorage;
use AT\Component\Booking\BookingClient;
use AT\Component\Booking\BookingSearchClient;
use AT\Component\Booking\Transfer\Request\CancelBookingDto;
use AT\Component\Booking\Transfer\Request\UpdateBookingDto;
use AT\Component\Search\Filter\PagingFilter;

class BookingService
{
    /**
     * @var BookingClient
     */
    private $client;

    /**
     * @var BookingSearchClient
     */
    private $searchClient;

    /**
     * @var PracticeService
     */
    private $practiceService;

    /**
     * @var OAuth2TokenStorage
     */
    private $tokenStorage;

    /**
     * @param BookingClient $client
     * @param BookingSearchClient $searchClient
     * @param PracticeService $practiceService
     * @param OAuth2TokenStorage $tokenStorage
     */
    public function __construct(
        BookingClient $client,
        BookingSearchClient $searchClient,
        PracticeService $practiceService,
        OAuth2TokenStorage $tokenStorage
    ) {
        $this->client = $client;
        $this->searchClient = $searchClient;
        $this->practiceService = $practiceService;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param int $id
     * @return array
     */
    public function getBooking(int $id): array
    {
        $practiceId = $this->practiceService->getPracticeId();
        $booking = $this->searchClient->getBooking($practiceId, $id);

        if (isset($booking)) {
            return $this->map($booking);
        } else {
            return null;
        }
    }

    /**
     * @param array $filters
     * @param int $limit
     * @param int $offset
     * @param bool $envelope
     * @return array
     */
    public function findBookings(array $filters = [], int $limit, int $offset, bool $envelope): array
    {
        $practiceId = $this->practiceService->getPracticeId();
        $filters[] = new PagingFilter(floor($offset / $limit), $limit);

        $result = $this->searchClient->getBookingsByPractice($practiceId, $filters);

        if (false !== $envelope) {
            return [
                'offset' => $offset,
                'total_entries' => count($result),
                'data' => array_map([$this, 'map'], $result),
            ];
        }

        return $result;
    }

    /**
     * @param int $booking
     * @param UpdateBookingDto $update
     * @return bool
     */
    public function updateBooking(int $booking, UpdateBookingDto $update): bool
    {
        $token = $this->tokenStorage->getAccessToken();

        return $this->client->updateBooking($token, $booking, $update);
    }

    /**
     * @param int $booking
     * @param CancelBookingDto $request
     * @return bool
     */
    public function cancelBooking(int $booking, CancelBookingDto $request): bool
    {
        $token = $this->tokenStorage->getAccessToken();

        return $this->client->cancelBooking($token, $booking, $request);
    }

    /**
     * @param array $data
     * @return array
     */
    private function map(array $data): array
    {
        $notifications = array_key_exists('notificationSettings', $data) ? $data['notificationSettings'] : [];
        $startDate = new \DateTime($data['appointment']['start']);
        $endDate = new \DateTime($data['appointment']['end']);

        $duration = $startDate->diff($endDate);
        $durationInMinutes = $duration->i + ($duration->h * 60);

        return [
            'id' => $data['id'],
            'appointment_id' => $data['appointment']['id'],
            'patient' => array_key_exists('customer', $data) ? [
                'firstName' => $data['customer']['details']['firstName'],
                'lastName' => $data['customer']['details']['lastName'],
                'email' => $data['customer']['details']['email'],
                'insuranceType' => array_key_exists('insurance', $data['customer']['details']) ?
                    (array_key_exists('type', $data['customer']['details']['insurance']) ? $data['customer']['details']['insurance']['type'] : null) : null,
                'phone' => $this->getContactDetail($notifications, 'smsRecipient', $this->getContactDetail($data['customer']['details'], 'phone', null)),
                'mobile' => $this->getContactDetail($notifications, 'smsRecipient', $this->getContactDetail($data['customer']['details'], 'phoneMobile', null)),
                'notifySms' => !empty($notifications['smsRecipient']),
                'notifyEmail' => !empty($notifications['emailRecipient']),
                'returningPatient' => $data['customer']['returning'],
            ] : [],
            'payment' => array_key_exists('totalPrice', $data) && null !== $data['totalPrice'] ? [
                'success' => $data['paid'] === true,
                'pending' => $data['paid'] === false && $data['paymentInitialised'] === true,
                'rejected' => $data['paymentInitialised'] === false,
                'price' => $data['totalPrice'],
            ] : null,
            'comment' => array_key_exists('practiceComment', $data) ? $data['practiceComment'] : null,
            'treatment_type' => array_key_exists('treatmentTypeId', $data) ? $data['treatmentTypeId'] : null,
            'title' => array_key_exists('title', $data) ? $data['title'] : sprintf('Buchung %d', $data['id']),
            'resources' => array_map(function ($resource) {
                return $resource['resourceId'];
            }, $data['appointment']['resourceRequests']),
            'start_date' => $startDate->format('Y-m-d H:i:sP'),
            'end_date' => $endDate->format('Y-m-d H:i:sP'),
            'duration' => $durationInMinutes,
        ];
    }

    /**
     * @param array $data
     * @param string $details
     * @param $default
     * @return mixed
     */
    private function getContactDetail(array $data, string $details, $default)
    {
        if (array_key_exists($details, $data)) {
            return $data[$details];
        }
        return $default;
    }
}
