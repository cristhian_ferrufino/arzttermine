<?php

namespace Arzttermine\ServiceBundle\Model;

class BulkAppointmentsAction
{
    /**
     * @var CreateAppointmentRange[]
     */
    public $rows = [];

    /**
     * @var int[]
     */
    public $treatmentTypes = [];

    /**
     * @var string[]
     */
    public $insuranceTypes = [];

    /**
     * @var RecurringConfiguration
     */
    public $recurringConfig;

    /**
     * @var \DateTime
     */
    public $onDate;
}
