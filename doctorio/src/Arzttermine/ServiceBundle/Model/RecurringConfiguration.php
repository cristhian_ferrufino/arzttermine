<?php

namespace Arzttermine\ServiceBundle\Model;

use AT\Component\Appointment\Transfer\RecurringConfigDto;

class RecurringConfiguration extends RecurringConfigDto
{
    public $isEnding = false;
}
