<?php

namespace Arzttermine\ServiceBundle\Model;

class CreateAppointmentRange
{
    /**
     * @var integer
     */
    public $id;

    /**
     * @var integer
     */
    public $practiceId;

    /**
     * @var string
     */
    public $rangeStart;

    /**
     * @var string
     */
    public $rangeEnd;

    /**
     * @var \DateTime
     */
    public $date;

    /**
     * @var integer
     */
    public $duration;

    /**
     * @var integer[]
     */
    public $resourceIds;

    /**
     * @var integer[]
     */
    public $treatmentTypes = [];

    /**
     * @var string[]
     */
    public $insuranceTypes = [];
}
