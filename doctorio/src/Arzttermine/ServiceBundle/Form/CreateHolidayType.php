<?php

namespace Arzttermine\ServiceBundle\Form;

use AT\Component\Appointment\Transfer\Request\HolidayTimeRangeDto;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreateHolidayType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('publicHoliday', HiddenType::class, [
                'required' => false,
            ])
            ->add('name', TextType::class, [
                'required' => true,
            ])
            ->add('begin', DateType::class, [
                'required' => true,
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'dd.MM.yyyy',
                'placeholder' => 'dd.mm.yyyy',
                'attr' => [
                    'data-provide' => 'datepicker',
                    'class' => 'js-datepicker',
                ],
            ])
            ->add('end', DateType::class, [
                'required' => true,
                'widget' => 'single_text',
                'html5' => false,
                'placeholder' => 'dd.mm.yyyy',
                'format' => 'dd.MM.yyyy',
                'attr' => [
                    'data-provide' => 'datepicker',
                    'class' => 'js-datepicker',
                ],
            ])
            ->add('resourceIds', CollectionType::class, [
                'entry_type' => IntegerType::class,
                'required' => true,
                'allow_add' => true,
                'allow_delete' => false,
                'attr' => [
                    'class' => 'resource-id-collection',
                ],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => HolidayTimeRangeDto::class,
        ]);
    }
}
