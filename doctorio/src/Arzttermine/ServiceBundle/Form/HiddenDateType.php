<?php

namespace Arzttermine\ServiceBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class HiddenDateType extends DateType
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'hidden_datetime';
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return HiddenType::class;
    }
}
