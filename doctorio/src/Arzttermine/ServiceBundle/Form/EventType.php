<?php

namespace Arzttermine\ServiceBundle\Form;

use AT\Component\Booking\Transfer\Request\UpdateBookingDto;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Arzttermine\UserBundle\Form\UpdateUserType;

class EventType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('practiceComment')
            ->add('resources', CollectionType::class, [
                'entry_type' => IntegerType::class,
                'mapped' => false,
            ])
            ->add('patient', UpdateUserType::class, [
                'mapped' => false,
                'required' => false,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UpdateBookingDto::class,
            'allow_extra_fields' => true,
        ]);
    }
}
