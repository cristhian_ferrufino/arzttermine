<?php

namespace Arzttermine\ServiceBundle\Form;

use AT\Component\Common\Transfer\PriceDto;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Range;

class PriceType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('amount', IntegerType::class, [
                'attr' => array('style' => 'width: 6.25rem'),
                'label' => 'ui.doctor.treatment_type.form.charge.label',
                'required' => false,
                'constraints' => [
                    new Range(['min' => 50, 'max' => 999999])
                ],
            ])
            ->add('currency', HiddenType::class, [
                'data' => 'EUR',
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PriceDto::class,
        ]);
    }
}
