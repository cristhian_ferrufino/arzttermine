<?php

namespace Arzttermine\ServiceBundle\Form;

use AT\Component\Practice\Transfer\Request\CreateTreatmentTypeDto;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;

class TreatmentTypeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', TextType::class, [
                'label' => 'ui.doctor.treatment_type.form.title.label',
                'invalid_message' => 'ui.doctor.treatment_type.form.title.error',
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 2])
                ]
            ])
            ->add('duration', IntegerType::class, [
                'label' => 'ui.doctor.treatment_type.form.duration.label',
                'invalid_message' => 'ui.doctor.treatment_type.form.duration.error',
                'mapped' => false,
                'constraints' => [
                    new Range(['min' => 1, 'max' => 1440])
                ]
            ])
            ->add('charge', PriceType::class, [
                'label' => false,
                'invalid_message' => 'ui.doctor.treatment_type.form.charge.error',
                'required' => false,
            ])
            ->add('forcePay', ChoiceType::class, [
                'label' => 'ui.doctor.treatment_type.form.force_pay.label',
                'invalid_message' => 'ui.doctor.treatment_type.form.force_pay.error',
                'choices' => [
                    'word.no' => 0,
                    'word.yes' => 1,
                ],
                'required' => false,
                'placeholder' => false,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CreateTreatmentTypeDto::class,
            'csrf_protection' => false,
        ]);
    }
}
