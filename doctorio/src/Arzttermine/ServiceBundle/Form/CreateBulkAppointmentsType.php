<?php

namespace Arzttermine\ServiceBundle\Form;

use Arzttermine\ServiceBundle\Model\BulkAppointmentsAction;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Count;

class CreateBulkAppointmentsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $types = array_map(function ($type) {
            return [$type['type'] => $type['id']];
        }, $options['treatmentTypes']);

        if (isset($options['doctorId'])) {
            $selected = array_map(function ($type) {
                return $type['id'];
            }, array_filter($options['treatmentTypes'], function ($type) use ($options) {
                return in_array($options['doctorId'], $type['doctorIds']);
            }));
        } else {
            $selected = $options['selectedTreatments'];
        }


        if (empty($options['selectedInsurances'])) {
            $options['selectedInsurances'] = ['COMPULSORY', 'PRIVATE'];
        }

        $insuranceFieldType = 'block';
        if (count($options['insuranceTypes']) === 1) {
            $insuranceFieldType = 'none';
        }

        $builder
            ->add('rows', CollectionType::class, [
                'entry_type' => CreateAvailabilitiesType::class,
                'allow_delete' => true,
                'allow_add' => true,
                'delete_empty' => true,
                'label' => false,
            ])
            ->add('onDate', HiddenDateType::class, [
                'format' => DateType::HTML5_FORMAT,
                'widget' => 'single_text',
                'required' => true,
                'label' => false,
            ])
            ->add('treatmentTypes', ChoiceType::class, [
                'expanded' => true,
                'multiple' => true,
                'choices' => $types,
                'data' => $selected,
                'constraints' => [
                    new Count(['min' => 1]),
                ],
                'label' => false,
            ])
            ->add('insuranceTypes', ChoiceType::class, [
                'expanded' => true,
                'multiple' => true,
                'choices' => $options['insuranceTypes'],
                'data' => $options['selectedInsurances'],
                'attr' => [
                    'style' => 'display: ' . $insuranceFieldType,
                ],
                'constraints' => [
                    new Count(['min' => 1]),
                ],
                'label' => false,
            ])
            ->add('recurringConfig', RecurringConfigType::class, [
                'required' => false,
                'label' => false,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'ui.form.button_submit',
                'attr' => [
                    'class' => 'btn btn-primary',
                ],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BulkAppointmentsAction::class,
            'selectedTreatments' => [],
            'selectedInsurances' => [],
            'insuranceTypes' => [],
            'treatmentTypes' => null,
            'doctorId' => null,
        ]);
    }
}
