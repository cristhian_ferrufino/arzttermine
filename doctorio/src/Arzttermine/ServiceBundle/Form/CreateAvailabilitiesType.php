<?php

namespace Arzttermine\ServiceBundle\Form;

use Arzttermine\ServiceBundle\Model\CreateAppointmentRange;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Count;

class CreateAvailabilitiesType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $times = [];
        for ($hour = 5; $hour < 23; $hour++) {
            $max = ($hour === 22) ? 1 : 60;
            for ($minute = 0; $minute < $max; $minute += 15) {
                $time = sprintf('%02d:%02d', $hour, $minute);
                $times[$time] = $time;
            }
        }

        $builder
            ->add('id', HiddenType::class, [
                'required' => false,
            ])
            ->add('resourceIds', CollectionType::class, [
                'entry_type' => HiddenType::class,
                'label' => false,
                'constraints' => [new Count(['min' => 1])],
                'allow_add' => true,
            ])
            ->add('practiceId', HiddenType::class, [
                'required' => true,
            ])
            ->add('date', DateType::class, [
                'required' => true,
            ])
            ->add('rangeStart', ChoiceType::class, [
                'label' => 'ui.appointments.form.range_start',
                'choices' => $times,
                'required' => true,
            ])
            ->add('rangeEnd', ChoiceType::class, [
                'label' => 'ui.appointments.form.range_end',
                'choices' => $times,
                'required' => true,
            ])
            ->add('duration', IntegerType::class, [
                'label' => 'ui.appointments.form.duration',
                'required' => true,
                'attr' => [
                    'step' => 5,
                    'min' => 0,
                    'max' => 720,
                ]
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CreateAppointmentRange::class,
        ]);
    }
}
