<?php

namespace Arzttermine\ServiceBundle\Form;

use AT\Component\Practice\Transfer\Request\CreateResourceDto;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\NotBlank;

class ResourceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'constraints' => [new NotBlank()],
                'label' => false
            ])
            ->add('type', ChoiceType::class, [
                'constraints' => [new NotBlank()],
                'choices' => $options['resourceTypes'],
                // @TODO remove when doctors are not shown on resources page anymore
                'required' => false, // might not be set if doctor is updated
                'label' => 'ui.doctor.dashboard.calendar.resource_type',
                'choice_label' => function($val) {
                    $lower = strtolower($val);
                    return "ui.doctor.dashboard.calendar.resource_types.{$lower}";
                },
                'placeholder' => 'form.please_choose'
            ])
            ->add('color', TextType::class);


        $builder->get('type')->resetViewTransformers();
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CreateResourceDto::class,
            'csrf_protection' => false,
            'resourceTypes' => ['ASSISTANT', 'DEVICE', 'ROOM'],
        ]);
    }
}
