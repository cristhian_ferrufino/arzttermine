<?php

namespace Arzttermine\ServiceBundle\Form;

use Arzttermine\ServiceBundle\Model\RecurringConfiguration;
use AT\Component\Appointment\Transfer\RecurringConfigDto;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RecurringConfigType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', ChoiceType::class, [
                'label' => 'ui.appointments.form.repeat',
                'attr' => [
                    'onchange' => 'recurringConfigUpdated(this.value)'
                ],
                'choices' => [
                    'datetime.recurring.never' => RecurringConfigDto::NEVER,
                    'datetime.recurring.daily' => RecurringConfigDto::DAILY,
                    'datetime.recurring.weekly' => RecurringConfigDto::WEEKLY,
                    'datetime.recurring.monthly' => RecurringConfigDto::MONTHLY,
                ]
            ])
            ->add('isEnding', ChoiceType::class, [
                'label' => 'ui.appointments.form.recurring_ends_at',
                'attr' => [
                    'onchange' => 'infiniteRecurringOptionUpdated(this.value)'
                ],
                'choices' => [
                    'datetime.recurring.ends_never' => 0,
                    'datetime.recurring.ends_at' => 1
                ],
            ])
            ->add('endsAt', DateType::class, [
                'label' => false,
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'dd.MM.yyyy',
                'attr' => [
                    'data-provide' => 'datepicker',
                    'class' => 'js-datepicker',
                ],
            ]);

        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
            if (null !== $event->getData()) {
                if (!empty($event->getData()->endsAt)) {
                    $event->getData()->isEnding = 1;
                } else {
                    $event->getData()->isEnding = 0;
                }
            }
        });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => RecurringConfiguration::class,
        ]);
    }
}
