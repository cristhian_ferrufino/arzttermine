<?php

namespace Arzttermine\ServiceBundle\EventListener;

use Arzttermine\ServiceBundle\Service\PracticeService;
use AT\Component\Practice\PracticeSearchClient;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class PracticeListener
{
    const PRACTICE_ID_COOKIE = 'atpid';
    const PRACTICE_ID_HEADER = 'x-practice-id';

    const EXCLUDED_ROUTES = [
        'doctor_calendar_choose_practice',
        'doctor_calendar_no_practice'
    ];

    const UNSUBSCRIBE = [
        'doctor_calendar_set_practice',
        'doctor_entry_point',
    ];

    /**
     * @var PracticeService
     */
    private $practiceService;

    /**
     * @var PracticeSearchClient
     */
    private $practiceClient;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @param PracticeService $practiceService
     * @param PracticeSearchClient $practiceClient
     * @param TokenStorageInterface $tokenStorage
     * @param RouterInterface $router
     */
    public function __construct(PracticeService $practiceService, PracticeSearchClient $practiceClient, TokenStorageInterface $tokenStorage, RouterInterface $router)
    {
        $this->practiceService = $practiceService;
        $this->practiceClient = $practiceClient;
        $this->tokenStorage = $tokenStorage;
        $this->router = $router;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if ($this->practiceService->hasPracticeId()) {
            return;
        }

        $id = $this->getExistingPracticeId($event->getRequest());

        if (null !== $id) {
            $this->practiceService->setPracticeId((int) $id);
            return;
        }

        $route = $event->getRequest()->attributes->get('_route');

        if (in_array($route, self::EXCLUDED_ROUTES) || in_array($route, self::UNSUBSCRIBE)) {
            return;
        }

        $token = $this->tokenStorage->getToken();
        if (!($token instanceof UsernamePasswordToken)) {
            return;
        }

        try {
            $practices = $this->practiceClient->getPracticeIdsByDoctor($token->getUser()->getId());
        } catch (\Exception $e) {
            // @TODO log
            return;
        }

        $this->handlePracticeSelection($event, $practices);
    }

    private function getExistingPracticeId(Request $request) {
        try {
            if ($request->cookies->has(self::PRACTICE_ID_COOKIE)) {
                return (int) $request->cookies->get(self::PRACTICE_ID_COOKIE);
            } else if ($request->headers->has(self::PRACTICE_ID_HEADER)) {
                return (int) $request->headers->get(self::PRACTICE_ID_HEADER);
            }
        } catch (\Exception $e) {
            // @TODO log
        }

        return null;
    }

    private function handlePracticeSelection(GetResponseEvent $event, $practices) {
        if (count($practices) === 0) {
            $url = $this->router->generate('doctor_calendar_no_practice');
            $event->setResponse(new RedirectResponse($url));
        } else if (count($practices) > 1) {
            $chooseUrl = $this->router->generate('doctor_calendar_choose_practice');
            $event->setResponse(new RedirectResponse($chooseUrl));
        } else {
            $ids = array_keys($practices);
            $this->practiceService->setPracticeId(array_pop($ids));
        }
    }
}
