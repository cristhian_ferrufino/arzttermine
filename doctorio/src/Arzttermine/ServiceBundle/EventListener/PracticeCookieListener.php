<?php

namespace Arzttermine\ServiceBundle\EventListener;

use Arzttermine\ServiceBundle\Service\PracticeService;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

class PracticeCookieListener
{
    /**
     * @var PracticeService
     */
    private $practiceService;

    /**
     * @var string
     */
    private $cookieDomain;

    /**
     * @param PracticeService $practiceService
     * @param string $cookieDomain
     */
    public function __construct(PracticeService $practiceService, $cookieDomain)
    {
        $this->practiceService = $practiceService;
        $this->cookieDomain = $cookieDomain;
    }

    /**
     * @param FilterResponseEvent $event
     */
    public function onKernelResponse(FilterResponseEvent $event)
    {
        if (!$this->practiceService->hasPracticeId()) {
            return;
        }

        $request = $event->getRequest();
        $route = $request->attributes->get('_route');

        if (in_array($route, PracticeListener::EXCLUDED_ROUTES)) {
            return;
        }

        $existingId = null;
        if ($request->cookies->has(PracticeListener::PRACTICE_ID_COOKIE)) {
            $existingId = $request->cookies->getInt(PracticeListener::PRACTICE_ID_COOKIE);
        } else if ($request->headers->has(PracticeListener::PRACTICE_ID_HEADER)) {
            $existingId = $request->headers->get(PracticeListener::PRACTICE_ID_HEADER);
        }

        $newPracticeId = $this->practiceService->getPracticeId();

        if (!$existingId || $existingId != $newPracticeId) {
            $cookie = new Cookie(PracticeListener::PRACTICE_ID_COOKIE, $newPracticeId, 0, '/', $this->cookieDomain);
            $event->getResponse()->headers->setCookie($cookie);
        }
    }
}
