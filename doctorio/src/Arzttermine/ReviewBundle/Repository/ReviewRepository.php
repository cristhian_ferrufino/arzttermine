<?php

namespace Arzttermine\ReviewBundle\Repository;

use Arzttermine\ReviewBundle\Entity\Review;
use Doctrine\ORM\EntityRepository;

/**
 * @method Review findOneBySlug($slug)
 * @method Review findOneByBooking($booking)
 * @method Review findByDoctor($doctor)
 */
class ReviewRepository extends EntityRepository
{
}