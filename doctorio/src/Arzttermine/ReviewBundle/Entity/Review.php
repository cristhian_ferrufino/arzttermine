<?php

namespace Arzttermine\ReviewBundle\Entity;

use Arzttermine\BookingBundle\Entity\Booking;
use Arzttermine\UserBundle\Entity\Doctor;
use Arzttermine\UserBundle\Entity\User;

class Review {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $patientEmail;

    /**
     * @var string
     */
    private $patientName;

    /**
     * @var string
     */
    private $rateText;

    /**
     * @var \DateTime
     */
    private $ratedAt;

    /**
     * @var \DateTime
     */
    private $approvedAt;

    /**
     * @var integer
     */
    private $bookingId;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var string
     */
    private $rating_1;

    /**
     * @var string
     */
    private $rating_2;

    /**
     * @var string
     */
    private $rating_3;

    /**
     * @var Booking
     */
    private $booking;

    /**
     * @var Doctor
     */
    private $doctor;

    /**
     * @var bool
     */
    private $allowSharing;

    /**
     * @return string
     */
    public function __toString()
    {
        return "{$this->booking->getLocation()}";
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $patientEmail
     *
     * @return Review
     */
    public function setPatientEmail($patientEmail)
    {
        $this->patientEmail = $patientEmail;

        return $this;
    }

    /**
     * @return string
     */
    public function getPatientEmail()
    {
        return $this->patientEmail;
    }

    /**
     * @param string $patientName
     *
     * @return Review
     */
    public function setPatientName($patientName)
    {
        $this->patientName = $patientName;

        return $this;
    }

    /**
     * Get patientName
     *
     * @return string
     */
    public function getPatientName()
    {
        return $this->patientName;
    }

    /**
     * Set rateText
     *
     * @param string $rateText
     *
     * @return Review
     */
    public function setRateText($rateText)
    {
        $this->rateText = $rateText;

        return $this;
    }

    /**
     * Get rateText
     *
     * @return string
     */
    public function getRateText()
    {
        return $this->rateText;
    }

    /**
     * Set ratedAt
     *
     * @param \DateTime $ratedAt
     *
     * @return Review
     */
    public function setRatedAt($ratedAt)
    {
        $this->ratedAt = $ratedAt;

        return $this;
    }

    /**
     * Get ratedAt
     *
     * @return \DateTime
     */
    public function getRatedAt()
    {
        return $this->ratedAt;
    }

    /**
     * Set approvedAt
     *
     * @param \DateTime $approvedAt
     *
     * @return Review
     */
    public function setApprovedAt($approvedAt)
    {
        $this->approvedAt = $approvedAt;

        return $this;
    }

    /**
     * Get approvedAt
     *
     * @return \DateTime
     */
    public function getApprovedAt()
    {
        return $this->approvedAt;
    }

    /**
     * Set bookingId
     *
     * @param integer $bookingId
     *
     * @return Review
     */
    public function setBookingId($bookingId)
    {
        $this->bookingId = $bookingId;

        return $this;
    }

    /**
     * Get bookingId
     *
     * @return integer
     */
    public function getBookingId()
    {
        return $this->bookingId;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Review
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @var User
     */
    private $user;

    /**
     * Set user
     *
     * @param
     *
     * @return Review
     */
    public function setUser($user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param string $rating1
     *
     * @return Review
     */
    public function setRating1($rating1)
    {
        $this->rating_1 = $rating1;

        return $this;
    }

    /**
     * @return float
     */
    public function getRating1()
    {
        return $this->rating_1;
    }

    /**
     * @param string $rating2
     *
     * @return Review
     */
    public function setRating2($rating2)
    {
        $this->rating_2 = $rating2;

        return $this;
    }

    /**
     * @return float
     */
    public function getRating2()
    {
        return $this->rating_2;
    }

    /**
     * @param string $rating3
     *
     * @return Review
     */
    public function setRating3($rating3)
    {
        $this->rating_3 = $rating3;

        return $this;
    }

    /**
     * @return float
     */
    public function getRating3()
    {
        return $this->rating_3;
    }

    /**
     * Set booking
     *
     * @param Booking $booking
     *
     * @return Review
     */
    public function setBooking(Booking $booking = null)
    {
        $this->booking = $booking;

        return $this;
    }

    /**
     * Get booking
     *
     * @return \Arzttermine\BookingBundle\Entity\Booking
     */
    public function getBooking()
    {
        return $this->booking;
    }


    /**
     * Set doctor
     *
     * @param Doctor $doctor
     *
     * @return Review
     */
    public function setDoctor(Doctor $doctor = null)
    {
        $this->doctor = $doctor;

        return $this;
    }

    /**
     * Get doctor
     *
     * @return Doctor
     */
    public function getDoctor()
    {
        return $this->doctor;
    }

    /**
     * @return boolean
     */
    public function isAllowSharing()
    {
        return $this->allowSharing;
    }

    /**
     * @param boolean $allowSharing
     *
     * @return self
     */
    public function setAllowSharing($allowSharing)
    {
        $this->allowSharing = $allowSharing;

        return $this;
    }
}
