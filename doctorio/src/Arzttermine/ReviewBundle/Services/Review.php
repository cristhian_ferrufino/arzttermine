<?php

namespace Arzttermine\ReviewBundle\Services;

use Arzttermine\ReviewBundle\Entity\Review as ReviewEntity;

class Review {

    /**
     * Returns if the review was made anonymous
     *
     * @param ReviewEntity $review
     * @return bool
     */
    public function isAnonymous(ReviewEntity $review) : bool
    {
        return empty($review->getPatientName());
    }

}