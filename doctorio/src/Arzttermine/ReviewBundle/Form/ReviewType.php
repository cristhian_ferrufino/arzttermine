<?php

namespace Arzttermine\ReviewBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class ReviewType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('patient_name', TextType::class, [
                'constraints' => [new NotBlank()]
            ])
            ->add('patient_email', EmailType::class, [
                'constraints' => [new NotBlank(), new Email()]
            ])
            ->add('rating1')
            ->add('rating2')
            ->add('rating3')
            ->add('rate_text', TextType::class)
            ->add('comment', TextType::class)
            ->add('booking', EntityType::class, [
                'class' => 'Arzttermine\BookingBundle\Entity\Booking',
                'choice_label' => 'id' // Label currently not used
            ])
            ->add('doctor', EntityType::class, [
                'class' => 'Arzttermine\UserBundle\Entity\Doctor',
                'choice_label' => 'name'
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Arzttermine\ReviewBundle\Entity\Review'
        ]);
    }
}
