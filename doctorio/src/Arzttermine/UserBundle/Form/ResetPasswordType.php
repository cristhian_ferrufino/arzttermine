<?php

namespace Arzttermine\UserBundle\Form;

use Arzttermine\UserBundle\Model\ResetPassword;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;

class ResetPasswordType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options = [])
    {
        $builder
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'constraints' => [
                    new Length(['min' => 8]),
                ],
                'invalid_message' => 'ui.login.form_lost_password.passwords_must_match',
                'first_name' => 'password',
                'first_options' => [
                    'label' => 'ui.login.form_lost_password.password.label',
                ],
                'second_name' => 'repeatPassword',
                'second_options' => [
                    'label' => 'ui.login.form_lost_password.password.repeat',
                ],
            ])
            ->add('customerId', HiddenType::class, [
                'required' => true,
            ])
            ->add('token', HiddenType::class, [
                'required' => true,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'ui.login.form_lost_password.reset',
                'attr' => [
                    'class' => 'btn btn-primary pull-right',
                ],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ResetPassword::class
        ]);
    }
}
