<?php

namespace Arzttermine\UserBundle\Form;

use AT\Component\Customer\Transfer\Request\UpdateCustomerDto;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UpdateUserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, [
                'label' => 'admin.key.first_name',
                'required' => true,
            ])
            ->add('lastName', TextType::class, [
                'label' => 'admin.key.last_name',
                'required' => true,
            ])
            ->add('title', TextType::class, [
                'label' => 'admin.key.title',
                'required' => false,
            ])
            ->add('gender', ChoiceType::class, [
                'choices' => [
                    'word.gender.male' => 'MALE',
                    'word.gender.female' => 'FEMALE',
                    'word.gender.group' => 'GROUP'
                ],
                'expanded' => false,
                'label' => 'admin.key.gender',
                'required' => true,
            ])
            ->add('language', TextType::class, [
                'required' => false,
                'label' => 'admin.key.language',
            ])
            ->add('email', EmailType::class, [
                'required' => false
            ])
            ->add('password', PasswordType::class, [
                'required' => false,
            ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => UpdateCustomerDto::class,
            'allow_extra_fields' => true,
        ));
    }
}
