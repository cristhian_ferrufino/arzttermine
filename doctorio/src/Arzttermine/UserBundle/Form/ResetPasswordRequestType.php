<?php

namespace Arzttermine\UserBundle\Form;

use AT\Component\Customer\Transfer\Request\PasswordResetRequestDto;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ResetPasswordRequestType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options = [])
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'security.login.username',
                'attr' => [
                    'placeholder' => 'example@example.com',
                ],
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'ui.login.form_lost_password.request',
                'attr' => [
                    'class' => 'btn btn-primary pull-right',
                ]
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PasswordResetRequestDto::class
        ]);
    }
}
