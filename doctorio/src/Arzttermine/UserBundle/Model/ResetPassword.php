<?php

namespace Arzttermine\UserBundle\Model;

use AT\Component\Customer\Transfer\Request\UpdatePasswordDto;

class ResetPassword extends UpdatePasswordDto
{
    /**
     * @var string
     */
    public $token;

    /**
     * @var string
     */
    public $customerId;
}
