<?php

namespace Arzttermine\UserBundle\Controller;

use Arzttermine\UserBundle\Form\ResetPasswordRequestType;
use Arzttermine\UserBundle\Form\ResetPasswordType;
use Arzttermine\UserBundle\Model\ResetPassword;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Security;

class SecurityController extends Controller
{
    /**
     * @Route("/login", name = "login")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function loginAction()
    {
        if ($this->container->get('security.authorization_checker')->isGranted('ROLE_DOCTOR')) {
            return $this->redirectToRoute('doctor_calendar');
        }

        $authenticationUtils = $this->get('security.authentication_utils');

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('Security/login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
        ));
    }

    /**
     * @Route("/request-password", methods={"GET", "POST"}, name = "request_reset_password", options={ "expose": true })
     *
     * @param Request $request
     * @return Response
     */
    public function requestPasswordResetAction(Request $request)
    {
        $form = $this->createForm(ResetPasswordRequestType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $cerror = null;
            if ($form->isValid()) {
                $success = $this->get('arzttermine_service.customers')->requestPasswordReset($form->getData());

                if ($success) {
                    $this->addFlash('success', $this->get('translator')->trans('ui.login.form_lost_password.password_reset_success'));
                    return $this->redirectToRoute('login');
                } else {
                    $cerror = 'ui.login.form_lost_password.invalid_username';
                }
            }

            return $this->render(':Security:reset-password.html.twig', [
                'last_username' => $form->getData()->email,
                'form' => $form->createView(),
                'cerror' => $cerror,
            ]);
        }

        return $this->render(':Security:reset-password.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/reset-password", methods={"GET", "POST"}, name = "reset_password", options={ "expose": true })
     *
     * @param Request $request
     * @return Response
     */
    public function resetPasswordAction(Request $request)
    {
        $customerService = $this->get('arzttermine_service.customers');
        $form = $this->createForm(ResetPasswordType::class);
        $form->handleRequest($request);

        $cerror = null;
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $success = $success = $this->get('arzttermine_service.customers')->resetPassword($form->getData());

                if ($success) {
                    $this->addFlash('success', $this->get('translator')->trans('ui.login.form_lost_password.password_updated'));
                    return $this->redirectToRoute('login');
                } else {
                    $cerror = 'ui.login.form_lost_password.could_not_update';
                }
            }
        } else {
            $token = $request->query->get('token');
            $email = $request->query->get('email');
            if (null === $token || null === $email) {
                throw new NotFoundHttpException();
            }

            $reset = $customerService->getResetRequest($token, $email);
            if ($request->hasSession()) {
                $request->getSession()->set(Security::LAST_USERNAME, $email);
            }

            $data = new ResetPassword();
            $data->token = $token;
            $data->customerId = $reset->customerId;
            $form->setData($data);
        }

        return $this->render(':Security:update-password.html.twig', [
            'form' => $form->createView(),
            'cerror' => $cerror,
        ]);
    }
}
