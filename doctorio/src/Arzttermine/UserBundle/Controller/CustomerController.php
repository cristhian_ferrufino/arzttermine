<?php

namespace Arzttermine\UserBundle\Controller;

use Arzttermine\UserBundle\Form\UpdateUserType;
use AT\Component\Common\Exception\ServiceException;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Security("has_role('ROLE_CUSTOMER')")
 */
class CustomerController extends FOSRestController
{
    /**
     * @Put(
     *     "/customers/{customer}",
     *     name = "update_customer",
     *     options = {"expose" = true, "method_prefix" = false}
     * )
     *
     * @param Request $request
     * @param int $customer
     * @return \FOS\RestBundle\View\View
     */
    public function updateAction(Request $request, int $customer)
    {
        $form = $this->createForm(UpdateUserType::class);
        $form->submit($request->request->get($form->getName()));

        if ($form->isSubmitted() && !$form->isValid() && null !== $form->getData()) {
            return $this->view(['success' => false, 'errors' => $form->getErrors()], 400);
        }

        $client = $this->get('at_client.customers');

        try {
            $client->updateCustomer($this->getUser()->getAccessToken(), $customer, $form->getData());
        } catch (ServiceException $e) {
            return $this->view(['success' => false, 'errors' => $e->getErrors()], $e->getStatus());
        }

        return $this->view(['success' => true], 201);
    }
}
