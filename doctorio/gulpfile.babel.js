import gulp from 'gulp';
import gulpLoadPlugins from 'gulp-load-plugins';
import browserSync from 'browser-sync';
import del from 'del';
import mainBowerFiles from 'main-bower-files';
import karma from 'karma';
import webserver from 'gulp-webserver';
import ngAnnotate from 'gulp-ng-annotate';
import useref from 'gulp-useref';

const $ = gulpLoadPlugins();
const reload = browserSync.reload;

const bowerRoot = 'bower_components';
const appPath = 'app/Resources/js/calendar';
const dist = 'web/calendar';

gulp.task('styles', () => {
    return gulp.src([bowerRoot + '/bootstrap-sass/assets/stylesheets/bootstrap.scss', appPath + '/styles/*.scss'])
        .pipe($.sourcemaps.init())
        .pipe($.sass.sync({
            outputStyle: 'expanded',
            precision: 10
        }).on('error', $.sass.logError))
        .pipe($.autoprefixer({browsers: ['last 1 version']}))
        .pipe($.sourcemaps.write())
        .pipe(gulp.dest('.tmp/styles'))
        .pipe(reload({stream: true}));
});

gulp.task('minify', ['scripts', 'styles'], () => {
    return gulp.src(appPath + '/**/*.html')
        .pipe(useref({searchPath: ['.tmp', appPath]}))
        .pipe($.if('*.js', ngAnnotate()))
        .pipe($.if('*.js', $.uglify()))
        .pipe($.if('*.css', $.minifyCss({compatibility: '*'})))
        .pipe($.if('*.html', $.minifyHtml({conditionals: true, loose: true})))
        .pipe(gulp.dest('.tmp'));
});

gulp.task('scripts', () => {
    return gulp.src(appPath + '/**/*.es6')
        .pipe($.sourcemaps.init())
        .pipe($.babel())
        .pipe($.sourcemaps.write('.'))
        .pipe(gulp.dest('.tmp/scripts'));
});

gulp.task('images', () => {
    return gulp.src(appPath + '/images/**/*')
        .pipe(gulp.dest('.tmp/images'));
});

gulp.task('config', () => {
    return gulp.src(appPath + '/config/**/*.json')
        .pipe(gulp.dest('.tmp/config'));
});

gulp.task('fonts', () => {
    return gulp.src(mainBowerFiles({
        filter: '**/*.{otf,eot,svg,ttf,woff,woff2}'
    }).concat(appPath + '/fonts/**/*'))
        .pipe(gulp.dest('.tmp/fonts'))
        .pipe(gulp.dest(dist + '/fonts'));
});

// .ico and other files
gulp.task('extras', () => {
    return gulp.src([
        appPath + '/*.*',
        '!' + appPath + '/*.html'
    ], {
        dot: true
    }).pipe(gulp.dest(dist));
});

gulp.task('clean', del.bind(null, ['.tmp', dist]));

gulp.task('serve', ['styles', 'scripts', 'fonts'], () => {
    browserSync({
        notify: false,
        port: 9000,
        server: {
            baseDir: ['.tmp', 'app'],
            routes: {
                '/bower_components': 'bower_components'
            }
        }
    });

    gulp.watch([
        appPath + '/*.html',
        appPath + '/scripts/**/*.js',
        appPath + '/scripts/**/*.html',
        appPath + '/config/**/*.json',
        appPath + '/images/**/*',
        '.tmp/fonts/**/*',
        '.tmp/scripts/**/*'
    ]).on('change', reload);

    gulp.watch(appPath + '/styles/**/*.scss', ['styles']);
    gulp.watch(appPath + '/fonts/**/*', ['fonts']);
    gulp.watch(appPath + '/scripts/**/*', ['scripts']);
});

gulp.task('serve:dist', ['build:dist'], () => {
    gulp.src(dist)
        .pipe(webserver({
            host: '0.0.0.0',
            port: 9000
        }));
});

gulp.task('karma', () => {
    new karma.Server({
        configFile: __dirname + '/karma.conf.js',
    }).start();
});

gulp.task('karma:watch', () => {
    new karma.Server({
        configFile: __dirname + '/karma.conf.js',
        singleRun: false
    }).start();
});

gulp.task('build:dist', ['minify', 'images', 'config', 'fonts', 'extras'], () => {
    return gulp.src('.tmp/**/*')
        .pipe($.size({title: 'build', gzip: true}))
        .pipe(gulp.dest(dist));
});

gulp.task('build:clean-start', ['clean'], () => {
    gulp.start('build:dist');
});

gulp.task('test', ['build:dist'], () => {
    gulp.start('karma');
});

gulp.task('test:watch', ['build:dist'], () => {
    gulp.start('karma:watch');
});

gulp.task('default', ['serve']);
