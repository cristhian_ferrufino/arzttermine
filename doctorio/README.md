[![Build Status](https://travis-ci.com/arzttermine/doctorio.svg?token=XHsxG8wHND1xUrHu4SwY&branch=master)](https://travis-ci.com/arzttermine/doctorio)
[![SensioLabsInsight](https://insight.sensiolabs.com/projects/13296cef-c883-4b82-8cba-e3ec07f10e0a/mini.png)](https://insight.sensiolabs.com/projects/13296cef-c883-4b82-8cba-e3ec07f10e0a)

# AtCalendar

This application is a PHP Symfony proxy application for our AtCalendar/AtWidget project.

It includes [nginx configuration](deploy/nginx) for staging and production. The project is responsible to route all traffic from `*atcalendar.arzttermine.de` to either a service/app directly or into this application.

### Requirements

* node.js (<8)
* npm (<5)
* Composer
* ruby & sass (install ruby and then sass using `gem install sass`)
* php7.x-xml php7.x-intl php-memcached (use docker)

### Developement

1. Run `npm start` to do the following:
  - Install npm dependencies
  - Install bower dependencies
  - Building calendar app (`npm run calendar`) (gulp)
  - Building the calendar app (`npm run doctorio`) (grunt)
  - Be warned: `npm run doctorio` is legacy and usually breaks everything
2. Run `composer install` to install all php dependencies and set up Symfony. When prompted accept all the default values.
3. Start docker container `docker-compose up -d` from project root
  3.1 Make sure you are connected with our [docker-registry](https://github.com/arzttermine/documentation/wiki/Docker)
4. For implicit service dependencies, see docker-compose.yml

Flush cache via docker:  
```sh
docker-compose exec cache telnet localhost 11211
> flush_all
STRG+C
> e
```

Production cache: [AWS ElasticCache/Memcached](https://eu-central-1.console.aws.amazon.com/elasticache/home?region=eu-central-1#memcached-nodes:id=atcache;nodes)  
This project does not use its own database. It only makes use of our search engine and services.

### Deployment

The [AtCalendar Pipeline](https://eu-central-1.console.aws.amazon.com/codepipeline/home?region=eu-central-1#/view/atcalendar-frontend-pipeline) is listening on commits into `master` branch.
To specify a different branch, edit the pipeline and change the source.

When merging into master we first deploy to staging. Afterwards an approval is required to continue with production deployment.
You will get a notification in our `development` channel with the link to the approval page.
