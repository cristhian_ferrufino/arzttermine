package com.arzttermine.service.profile.security.support;

import com.arzttermine.service.profile.web.struct.AuthorityRole;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public final class SecurityContextUtil
{
    private static final Logger log = LoggerFactory.getLogger(SecurityContextUtil.class);

    public static UUID currentToken()
    {
        final OAuth2Authentication auth = authentication();

        if (null == auth) {
            return null;
        }

        return UUID.fromString(((OAuth2AuthenticationDetails) auth.getDetails()).getTokenValue());
    }

    public static Long userPrincipal()
    {
        final OAuth2Authentication auth = authentication();

        if (null == auth) {
            return null;
        }

        try {
            final Object principal = auth.getUserAuthentication().getPrincipal();
            if (principal instanceof Long) {
                return (Long) principal;
            }

            return Long.valueOf((String) auth.getUserAuthentication().getPrincipal());
        } catch (Exception e) {
            log.warn("Error while trying to extract principal from authentication", e);
            return null;
        }
    }

    public static String userMail()
    {
        final OAuth2Authentication auth = authentication();

        if (null == auth) {
            return null;
        }

        try {
            return (String) auth.getUserAuthentication().getCredentials();
        } catch (Exception e) {
            log.warn("Error while trying to extract credentials from authentication", e);
            return null;
        }
    }

    public static boolean hasPracticeAuthority(long practiceId)
    {
        return hasAuthorities(AuthorityRole.getPracticeAuthority(practiceId));
    }

    public static boolean hasSystemGrants()
    {
        return hasAuthorities(AuthorityRole.ROLE_SYSTEM.name()) || hasAuthorities(AuthorityRole.ROLE_EMPLOYEE.name());
    }

    public static boolean hasAuthorities(String... authority)
    {
        final OAuth2Authentication auth = authentication();

        if (null == auth) {
            return false;
        }

        final List<String> authorities = Arrays.asList(authority);

        return auth.getAuthorities().stream()
            .filter(a -> authorities.contains(a.getAuthority()))
            .count() == authorities.size();
    }

    private static OAuth2Authentication authentication()
    {
        final Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (null == auth || !(auth instanceof OAuth2Authentication)) {
            log.warn("Seems like you are calling authentication() not within the requesting thread");
            return null;
        }

        return (OAuth2Authentication) auth;
    }
}
