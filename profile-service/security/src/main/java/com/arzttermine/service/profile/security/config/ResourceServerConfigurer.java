package com.arzttermine.service.profile.security.config;

import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@EnableResourceServer
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public abstract class ResourceServerConfigurer extends ResourceServerConfigurerAdapter
{
    @Override
    public void configure(HttpSecurity http) throws Exception
    {
        http.authorizeRequests()
            .regexMatchers(
            "/error.*",
                ".*swagger-ui.*",
                "/health",
                "/.*\\.html",
                "/.*\\.css",
                "/.*\\.js",
                "/.*\\.png",
                "/.*\\.jpe?g",
                "/.*\\.woff2?",
                "/.*\\.ttf",
                "/swagger-resources.*",
                "/v.+/api-docs.*"
            )
            .permitAll()
            .and()
            .authorizeRequests()
            .anyRequest()
            .authenticated()
            .and()
            .sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }
}
