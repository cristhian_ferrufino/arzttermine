package com.arzttermine.service.profile.security.support;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public final class AuthBuilderUtil
{
    public OAuth2Authentication buildAuthForCustomer(long userId, String[] roles)
    {
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(roles);
        OAuth2Request request = new OAuth2Request(new HashMap<>(), "client_name", authorities, true, new HashSet<>(), null, null, null, null);

        Authentication authentication = new UsernamePasswordAuthenticationToken(userId, "", authorities);

        return new OAuth2Authentication(request, authentication);
    }
}
