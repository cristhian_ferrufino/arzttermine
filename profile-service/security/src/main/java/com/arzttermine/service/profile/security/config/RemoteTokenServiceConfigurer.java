package com.arzttermine.service.profile.security.config;

import com.arzttermine.service.player.client.TokenClient;
import com.arzttermine.service.profile.security.config.settings.RemoteTokenServicesSettings;
import com.arzttermine.service.profile.security.service.ResourceClientTokenCache;
import com.arzttermine.service.profile.security.service.TokenCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
import org.springframework.web.util.UriComponentsBuilder;

public abstract class RemoteTokenServiceConfigurer
{
    private static final Logger log = LoggerFactory.getLogger(RemoteTokenServiceConfigurer.class);

    public RemoteTokenServices configure(RemoteTokenServicesSettings servicesSettings)
    {
        if (servicesSettings == null || servicesSettings.getHost() == null || servicesSettings.getHost().isEmpty()) {
            log.warn("Remote token service not created. Missing config.");
            return null;
        }

        final RemoteTokenServices tokenServices = new RemoteTokenServices();
        tokenServices.setClientId(servicesSettings.getClientId());
        tokenServices.setClientSecret(servicesSettings.getClientSecret());

        tokenServices.setCheckTokenEndpointUrl(UriComponentsBuilder.newInstance()
            .host(servicesSettings.getHost())
            .port(servicesSettings.getPort())
            .scheme(servicesSettings.getScheme())
            .pathSegment("oauth", "check_token")
            .build()
            .toUriString());

        return tokenServices;
    }

    public TokenCache clientTokenCache(RemoteTokenServicesSettings servicesSettings, TokenClient tokenClient)
    {
        return new ResourceClientTokenCache(servicesSettings, tokenClient);
    }
}
