package com.arzttermine.service.profile.security.service;

import com.arzttermine.service.profile.web.dto.response.AccessTokenDto;

import java.util.UUID;

public interface TokenCache
{
    AccessTokenDto generateToken();

    default UUID getToken() {
        return generateToken().getToken();
    }
}
