package com.arzttermine.service.profile.security.service;

import com.arzttermine.service.player.client.TokenClient;
import com.arzttermine.service.profile.security.config.settings.RemoteTokenServicesSettings;
import com.arzttermine.service.profile.web.dto.response.AccessTokenDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.util.concurrent.atomic.AtomicReference;

public class ResourceClientTokenCache implements TokenCache
{
    private static final Logger log = LoggerFactory.getLogger(ResourceClientTokenCache.class);

    private final AtomicReference<AccessTokenDto> accessToken = new AtomicReference<>(null);
    private final RemoteTokenServicesSettings settings;
    private final TokenClient tokenClient;

    public ResourceClientTokenCache(RemoteTokenServicesSettings settings, TokenClient client) {
        this.settings = settings;
        this.tokenClient = client;

        // loading initial token
        new Thread(this::generateToken).start();
    }

    @Override
    public AccessTokenDto generateToken() {
        return accessToken.updateAndGet(token -> {
            final Instant now = Instant.now();

            if (null != token && token.expiresAfter(now)) {
                return token;
            }

            log.info("generating new access token for client {}", settings.getClientId());

            final AccessTokenDto result = tokenClient.generateClientToken(
                settings.getClientId(),
                settings.getClientSecret(),
                settings.getScopes()
            );

            log.debug("generated new access token for client {}", settings.getClientId());
            result.retrievedAt(now);

            return result;
        });
    }
}
