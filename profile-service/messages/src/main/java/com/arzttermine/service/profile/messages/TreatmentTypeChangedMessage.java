package com.arzttermine.service.profile.messages;

import com.arzttermine.library.messaging.model.TopicMessage;
import com.arzttermine.library.web.dto.PriceDto;

public class TreatmentTypeChangedMessage extends TopicMessage
{
    private long treatmentTypeId;
    private PriceDto charge;
    private boolean deleted = false;

    public long getTreatmentTypeId() {
        return treatmentTypeId;
    }

    public void setTreatmentTypeId(long treatmentTypeId) {
        this.treatmentTypeId = treatmentTypeId;
    }

    public PriceDto getCharge() {
        return charge;
    }

    public void setCharge(PriceDto charge) {
        this.charge = charge;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    protected String getTopic() {
        return "changed";
    }

    @Override
    protected String getTopicKind() {
        return "treatment-type";
    }
}

