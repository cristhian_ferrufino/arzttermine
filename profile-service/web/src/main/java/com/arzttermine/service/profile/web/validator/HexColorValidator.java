package com.arzttermine.service.profile.web.validator;

import com.arzttermine.service.profile.web.validator.constraint.HexColor;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class HexColorValidator implements ConstraintValidator<HexColor, String>
{
    private static final Pattern pattern = Pattern.compile("^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$");

    @Override
    public void initialize(HexColor hexColor) {
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return pattern.matcher(s).matches();
    }
}
