package com.arzttermine.service.profile.web.dto.response;

import com.arzttermine.library.web.IdAccessor;
import com.arzttermine.library.web.struct.InsuranceType;
import com.arzttermine.service.profile.web.dto.LocationDto;
import com.arzttermine.service.profile.web.dto.OpeningHoursDto;
import com.arzttermine.service.profile.web.dto.WidgetConfigurationDto;
import com.arzttermine.service.profile.web.struct.MedicalSpecialtyType;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.Collection;

public class PracticeDto implements IdAccessor
{
    private long id;

    private String name;

    private LocationDto location;

    private Long ownerId;

    private String preferredReviewSiteType;

    private Collection<MedicalSpecialtyType> medicalSpecialties = new ArrayList<>();

    private Collection<DoctorProfileDto> doctors = new ArrayList<>();

    private Collection<TreatmentTypeDto> treatmentTypes = new ArrayList<>();

    private Collection<ResourceDto> resources = new ArrayList<>();

    private Collection<PaidFeatureAssignmentDto> paidFeatures = new ArrayList<>();

    private Collection<ReviewSiteDto> reviewSites = new ArrayList<>();

    private Collection<InsuranceType> insuranceTypes = new ArrayList<>();

    @ApiModelProperty("might be null or not completely configured")
    private WidgetConfigurationDto widgetConfiguration;

    private OpeningHoursDto openingHours;

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPreferredReviewSiteType(String type) {
        this.preferredReviewSiteType = type;
    }

    public String getPreferredReviewSiteType() {
        return preferredReviewSiteType;
    }

    public LocationDto getLocation() {
        return location;
    }

    public void setLocation(LocationDto location) {
        this.location = location;
    }

    public Collection<DoctorProfileDto> getDoctors() {
        return doctors;
    }

    public void setDoctors(Collection<DoctorProfileDto> doctors) {
        this.doctors = doctors;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public Collection<MedicalSpecialtyType> getMedicalSpecialties() {
        return medicalSpecialties;
    }

    public void setMedicalSpecialties(Collection<MedicalSpecialtyType> medicalSpecialties) {
        this.medicalSpecialties = medicalSpecialties;
    }

    public Collection<ResourceDto> getResources() {
        return resources;
    }

    public void setResources(Collection<ResourceDto> resources) {
        this.resources = resources;
    }

    public Collection<TreatmentTypeDto> getTreatmentTypes() {
        return treatmentTypes;
    }

    public void setTreatmentTypes(Collection<TreatmentTypeDto> treatmentTypes) {
        this.treatmentTypes = treatmentTypes;
    }

    public Collection<PaidFeatureAssignmentDto> getPaidFeatures() {
        return this.paidFeatures;
    }

    public void setPaidFeatures(Collection<PaidFeatureAssignmentDto> paidFeatures) {
        this.paidFeatures = paidFeatures;
    }

    public WidgetConfigurationDto getWidgetConfiguration() {
        return widgetConfiguration;
    }

    public void setWidgetConfiguration(WidgetConfigurationDto widgetConfiguration) {
        this.widgetConfiguration = widgetConfiguration;
    }

    public OpeningHoursDto getOpeningHours() {
        return openingHours;
    }

    public void setOpeningHours(final OpeningHoursDto openingHours) {
        this.openingHours = openingHours;
    }

    public Collection<ReviewSiteDto> getReviewSites() {
        return reviewSites;
    }

    public void setReviewSites(Collection<ReviewSiteDto> reviewSites) {
        this.reviewSites = reviewSites;
    }

    public Collection<InsuranceType> getInsuranceTypes() {
        return insuranceTypes;
    }

    public void setInsuranceTypes(Collection<InsuranceType> insuranceTypes) {
        this.insuranceTypes = insuranceTypes;
    }

}
