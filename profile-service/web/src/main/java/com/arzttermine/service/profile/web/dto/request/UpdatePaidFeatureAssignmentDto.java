package com.arzttermine.service.profile.web.dto.request;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;

public class UpdatePaidFeatureAssignmentDto {

    @NotNull
    @ApiModelProperty("List of paid feature id strings and options to be assigned to this practice. Replaces existing assignments.")
    private Map<String, Map<String, String>> paidFeatures = new HashMap<>();

    public Map<String, Map<String, String>> getPaidFeatures() {
        return paidFeatures;
    }

    public void setPaidFeatures(Map<String, Map<String, String>> paidFeatures) {
        this.paidFeatures = paidFeatures;
    }

}
