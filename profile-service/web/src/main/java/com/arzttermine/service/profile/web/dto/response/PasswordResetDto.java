package com.arzttermine.service.profile.web.dto.response;

import java.util.UUID;

public class PasswordResetDto
{
    private UUID token;
    private long customerId;

    public UUID getToken() {
        return token;
    }

    public void setToken(UUID token) {
        this.token = token;
    }

    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }
}
