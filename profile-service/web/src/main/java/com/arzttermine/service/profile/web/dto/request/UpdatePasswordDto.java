package com.arzttermine.service.profile.web.dto.request;

import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.AssertTrue;

public class UpdatePasswordDto
{
    @Length(min = 6)
    @ApiModelProperty("if supplied, you must also provide repeatPassword and the current(!) email address")
    private String password;

    private String repeatPassword;

    @AssertTrue(message = "passwords must be equal")
    public boolean getPasswordsEquals() {
        return null == password || password.equals(repeatPassword);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRepeatPassword() {
        return repeatPassword;
    }

    public void setRepeatPassword(String repeatPassword) {
        this.repeatPassword = repeatPassword;
    }
}
