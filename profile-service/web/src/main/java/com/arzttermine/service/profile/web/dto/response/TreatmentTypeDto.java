package com.arzttermine.service.profile.web.dto.response;

import com.arzttermine.library.web.IdAccessor;
import com.arzttermine.library.web.dto.PriceDto;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

public class TreatmentTypeDto implements IdAccessor
{
    private long id;

    @ApiModelProperty("id of doctor profiles who are providing this treatment")
    private List<Long> doctorIds;

    @ApiModelProperty("custom string key to identify a treatment type")
    private String type;

    @ApiModelProperty("only present if there is anything to charge")
    private PriceDto charge;

    @ApiModelProperty("whether the customer has to pay in advance or not")
    private boolean forcePay;

    @ApiModelProperty("indicates whether the treatment is active or not")
    private boolean enabled = true;

    private String comment;

    private long practiceId;

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public PriceDto getCharge() {
        return charge;
    }

    public void setCharge(PriceDto charge) {
        this.charge = charge;
    }

    public boolean getForcePay() {
        return forcePay;
    }

    public void setForcePay(boolean forcePay) {
        this.forcePay = forcePay;
    }

    public boolean isFreeTreatment() {
        return null == charge;
    }

    public boolean requiresPayment() {
        return forcePay && !isFreeTreatment();
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setDoctorIds(List<Long> doctorIds) {
        this.doctorIds = doctorIds;
    }

    public List<Long> getDoctorIds() {
        return doctorIds;
    }

    public boolean isForcePay() {
        return forcePay;
    }

    public long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(long practiceId) {
        this.practiceId = practiceId;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
