package com.arzttermine.service.profile.web.dto.request;

import com.arzttermine.library.web.struct.InsuranceType;
import com.arzttermine.library.web.struct.Language;
import com.arzttermine.service.profile.web.struct.AuthorityRole;
import com.arzttermine.service.profile.web.struct.Gender;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.SafeHtml;

import javax.validation.constraints.NotNull;
import java.util.List;

public class CreateCustomerDto
{
    @NotNull @Email
    private String email;

    @Length(min = 6)
    @ApiModelProperty(notes = "can be null")
    private String password;

    @NotNull
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    private String firstName;

    @NotNull
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    private String lastName;

    private String title;

    private InsuranceType insuranceType;

    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    private String phone;

    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    private String phoneMobile;

    private Language language = Language.GERMAN;

    private List<AuthorityRole> roles;

    private Gender gender;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public InsuranceType getInsuranceType() {
        return insuranceType;
    }

    public void setInsuranceType(InsuranceType insuranceType) {
        this.insuranceType = insuranceType;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getPhoneMobile() {
        return phoneMobile;
    }

    public void setPhoneMobile(String phoneMobile) {
        this.phoneMobile = phoneMobile;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public List<AuthorityRole> getRoles() {
        return roles;
    }

    public void setRoles(List<AuthorityRole> roles) {
        this.roles = roles;
    }
}
