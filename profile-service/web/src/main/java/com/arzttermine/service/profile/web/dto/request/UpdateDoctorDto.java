package com.arzttermine.service.profile.web.dto.request;

import com.arzttermine.library.web.struct.Language;
import com.arzttermine.service.profile.web.struct.MedicalSpecialtyType;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Range;

import java.util.List;
import java.util.Set;

public class UpdateDoctorDto
{
    @ApiModelProperty("ids of treatment types which are offered by this doctor. all existing values getting overridden")
    private List<Long> treatmentTypes;

    @Range(min = 1, max = 9999)
    @ApiModelProperty("minimum offset in hours between current date and the date a booking gets created")
    private Integer bookingTimeOffset = 1;

    private Boolean bookable;

    private List<Language> spokenLanguages;

    private Set<MedicalSpecialtyType> medicalSpecialties;

    public List<Long> getTreatmentTypes() {
        return treatmentTypes;
    }

    public void setTreatmentTypes(List<Long> treatmentTypes) {
        this.treatmentTypes = treatmentTypes;
    }

    public Integer getBookingTimeOffset() {
        return bookingTimeOffset;
    }

    public void setBookingTimeOffset(Integer bookingTimeOffset) {
        this.bookingTimeOffset = bookingTimeOffset;
    }

    public Set<MedicalSpecialtyType> getMedicalSpecialties() {
        return medicalSpecialties;
    }

    public void setMedicalSpecialties(Set<MedicalSpecialtyType> medicalSpecialties) {
        this.medicalSpecialties = medicalSpecialties;
    }

    public Boolean getBookable() {
        return bookable;
    }

    public void setBookable(Boolean bookable) {
        this.bookable = bookable;
    }

    public List<Language> getSpokenLanguages() {
        return spokenLanguages;
    }

    public void setSpokenLanguages(List<Language> spokenLanguages) {
        this.spokenLanguages = spokenLanguages;
    }
}
