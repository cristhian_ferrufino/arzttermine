package com.arzttermine.service.profile.web.dto;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class TimeRangeDto
{
    @NotNull
    private Date begin;

    @NotNull
    private Date end;

    public Date getBegin() {
        return begin;
    }

    public void setBegin(final Date begin) {
        this.begin = begin;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(final Date end) {
        this.end = end;
    }
}
