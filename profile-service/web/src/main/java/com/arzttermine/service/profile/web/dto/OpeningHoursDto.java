package com.arzttermine.service.profile.web.dto;

import java.time.DayOfWeek;
import java.util.EnumMap;
import java.util.List;

public class OpeningHoursDto extends EnumMap<DayOfWeek, List<TimeRangeDto>>
{
    public OpeningHoursDto() {
        super(DayOfWeek.class);
    }
}
