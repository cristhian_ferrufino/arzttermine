package com.arzttermine.service.profile.web.dto.request;

import com.arzttermine.library.web.dto.PriceDto;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.SafeHtml;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class CreateTreatmentTypeDto
{
    @NotNull
    private List<Long> doctorIds = new ArrayList<>();

    @NotNull
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    @ApiModelProperty("preferable a translation key which identifies the treatment")
    private String type;

    @ApiModelProperty("if supplied, the treatment type requires payment")
    private PriceDto charge;

    @ApiModelProperty("if true, the patient gets forced to pay in advance")
    private Boolean forcePay = false;

    @Length(max = 255)
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    @ApiModelProperty("optional free text to display a description")
    private String comment;

    @AssertTrue
    public boolean chargMustHigherThan49() {
        return null == charge || charge.getAmount() >= 50;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public PriceDto getCharge() {
        return charge;
    }

    public void setCharge(PriceDto charge) {
        this.charge = charge;
    }

    public List<Long> getDoctorIds() {
        return doctorIds;
    }

    public void setDoctorIds(List<Long> doctorIds) {
        this.doctorIds = doctorIds;
    }

    public Boolean getForcePay() {
        return forcePay;
    }

    public void setForcePay(Boolean forcePay) {
        this.forcePay = forcePay;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
