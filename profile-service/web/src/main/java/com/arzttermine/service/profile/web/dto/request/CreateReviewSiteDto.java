package com.arzttermine.service.profile.web.dto.request;

import javax.validation.constraints.NotNull;

public class CreateReviewSiteDto {

    @NotNull
    private String type;

    @NotNull
    private String data;


    public CreateReviewSiteDto() {
        super();
    }

    public CreateReviewSiteDto(String type, String data) {
        this.type = type;
        this.data = data;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
