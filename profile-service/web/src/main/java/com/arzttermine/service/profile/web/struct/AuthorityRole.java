package com.arzttermine.service.profile.web.struct;

public enum AuthorityRole
{
    ROLE_CLIENT,
    ROLE_CUSTOMER,
    ROLE_DOCTOR,
    ROLE_EMPLOYEE,
    ROLE_SYSTEM,
    ROLE_PRACTICE;

    public static String getPracticeAuthority(final long practiceId) {
        return String.format("%s_%d", ROLE_PRACTICE.name(), practiceId);
    }
}
