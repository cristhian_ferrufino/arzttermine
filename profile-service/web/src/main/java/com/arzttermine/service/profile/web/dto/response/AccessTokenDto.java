package com.arzttermine.service.profile.web.dto.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Instant;
import java.util.UUID;

public class AccessTokenDto
{
    @JsonProperty("access_token")
    private UUID token;

    @JsonProperty("expires_in")
    private int expiresInSeconds;

    @JsonIgnore
    private Instant retrievedAt;

    public UUID getToken() {
        return token;
    }

    public void setToken(UUID token) {
        this.token = token;
    }

    public int getExpiresInSeconds() {
        return expiresInSeconds;
    }

    public void setExpiresInSeconds(int expiresInSeconds) {
        this.expiresInSeconds = expiresInSeconds;
    }

    public void retrievedAt(Instant now) {
        this.retrievedAt = now;
    }

    public boolean expiresAfter(Instant now) {
        return retrievedAt.plusSeconds(expiresInSeconds).isAfter(now);
    }
}
