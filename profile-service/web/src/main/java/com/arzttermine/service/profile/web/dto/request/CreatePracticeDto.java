package com.arzttermine.service.profile.web.dto.request;

import com.arzttermine.service.profile.web.dto.OpeningHoursDto;
import com.arzttermine.service.profile.web.dto.WidgetConfigurationDto;
import com.arzttermine.service.profile.web.struct.MedicalSpecialtyType;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.SafeHtml;


import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CreatePracticeDto extends PracticeRequestDto
{

    private CreateLocationDto location;

    private List<Long> initialDoctors = new ArrayList<>();

    private List<MedicalSpecialtyType> medicalSpecialties = new ArrayList<>();

    private Long ownerId;

    @ApiModelProperty("custom configuration to override widget defaults")
    @Valid
    public WidgetConfigurationDto getWidgetConfiguration() {
        if (super.widgetConfiguration == null) {
            super.widgetConfiguration = new WidgetConfigurationDto();
        }
        return super.widgetConfiguration;
    }

    @NotNull
    @Valid
    public List<CreateReviewSiteDto> getReviewSites() {
        if (super.reviewSites == null) {
            super.reviewSites = new ArrayList<>();
        }
        return reviewSites;
    }

    @NotNull
    @Valid
    public Map<String, Map<String,String>> getPaidFeatures() {
        if (super.paidFeatures == null) {
            super.paidFeatures = new HashMap<>();
        }
        return paidFeatures;
    }

    @NotNull
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    public String getName() {
        return name;
    }

    @Valid
    public OpeningHoursDto getOpeningHours() {
        return openingHours;
    }

    @NotNull
    @Valid
    public CreateLocationDto getLocation() {
        return location;
    }

    public void setLocation(CreateLocationDto location) {
        this.location = location;
    }

    @NotNull
    public List<Long> getInitialDoctors() {
        return initialDoctors;
    }

    public void setInitialDoctors(List<Long> initialDoctors) {
        this.initialDoctors = initialDoctors;
    }


    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    @NotNull
    public List<MedicalSpecialtyType> getMedicalSpecialties() {
        return medicalSpecialties;
    }

    public void setMedicalSpecialties(List<MedicalSpecialtyType> medicalSpecialties) {
        this.medicalSpecialties = medicalSpecialties;
    }

}
