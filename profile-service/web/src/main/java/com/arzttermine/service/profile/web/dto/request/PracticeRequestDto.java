package com.arzttermine.service.profile.web.dto.request;

import com.arzttermine.library.web.struct.InsuranceType;
import com.arzttermine.service.profile.web.dto.OpeningHoursDto;
import com.arzttermine.service.profile.web.dto.WidgetConfigurationDto;
import org.hibernate.validator.constraints.SafeHtml;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

public abstract class PracticeRequestDto {

    protected String name;

    protected WidgetConfigurationDto widgetConfiguration;

    protected List<CreateReviewSiteDto> reviewSites;

    protected Map<String, Map<String, String>> paidFeatures;

    protected OpeningHoursDto openingHours;

    protected List<InsuranceType> insuranceTypes;

    protected String preferredReviewSiteType;

    public void setName(String name) {
        this.name = name;
    }

    public void setWidgetConfiguration(WidgetConfigurationDto widgetConfiguration) {
        this.widgetConfiguration = widgetConfiguration;
    }

    public void setReviewSites(List<CreateReviewSiteDto> reviewSites) {
        this.reviewSites = reviewSites;
    }


    public void setPaidFeatures(Map<String, Map<String, String>> paidFeatures) {
        this.paidFeatures = paidFeatures;
    }

    public void setOpeningHours(OpeningHoursDto openingHours) {
        this.openingHours = openingHours;
    }

    public List<InsuranceType> getInsuranceTypes() {
        return insuranceTypes;
    }

    public void setInsuranceTypes(List<InsuranceType> insuranceTypes) {
        this.insuranceTypes = insuranceTypes;
    }

    public String getPreferredReviewSiteType() {
        return preferredReviewSiteType;
    }

    public void setPreferredReviewSiteType(final String preferredReviewSiteType) {
        this.preferredReviewSiteType = preferredReviewSiteType;
    }
}
