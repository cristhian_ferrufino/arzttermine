package com.arzttermine.service.profile.web.dto.response;

import com.arzttermine.library.web.dto.InsuranceProviderDto;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

public class CustomerDto extends PublicCustomerDto
{
    @ApiModelProperty("if set, customer is somehow a patient")
    private InsuranceProviderDto insurance;

    private boolean enabled;

    private List<String> roles;

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public InsuranceProviderDto getInsurance() {
        return insurance;
    }

    public void setInsurance(InsuranceProviderDto insurance) {
        this.insurance = insurance;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }
}
