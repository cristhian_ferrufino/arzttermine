package com.arzttermine.service.profile.web.dto.request;

import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.NotNull;

public class PasswordResetRequestDto
{
    @Email
    @NotNull
    private String email;

    public PasswordResetRequestDto() {
        this(null);
    }

    public PasswordResetRequestDto(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
