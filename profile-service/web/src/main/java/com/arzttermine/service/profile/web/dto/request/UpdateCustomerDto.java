package com.arzttermine.service.profile.web.dto.request;

import com.arzttermine.library.web.dto.InsuranceProviderDto;
import com.arzttermine.library.web.struct.Language;
import com.arzttermine.service.profile.web.struct.AuthorityRole;
import com.arzttermine.service.profile.web.struct.Gender;
import io.swagger.annotations.ApiModel;

import javax.validation.Valid;
import javax.validation.constraints.AssertTrue;
import java.util.List;

@ApiModel(
    "if password is supplied, " +
    "you must also provide repeatPassword and the current(!) email address " +
    "to update the customers password!"
)
public class UpdateCustomerDto extends UpdatePasswordDto
{
    private String firstName;
    private String lastName;
    private Gender gender;
    private String title;
    private String email;
    private String phone;
    private String phoneMobile;
    private Language preferredLanguage;
    private List<Language> spokenLanguages;
    private List<AuthorityRole> roles;

    @Valid
    private InsuranceProviderDto insuranceProvider;

    @AssertTrue
    public boolean getPasswordChangeRequiresEmail() {
        return null == getPassword() || null != email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoneMobile() {
        return phoneMobile;
    }

    public void setPhoneMobile(String phoneMobile) {
        this.phoneMobile = phoneMobile;
    }

    public InsuranceProviderDto getInsuranceProvider() {
        return insuranceProvider;
    }

    public void setInsuranceProvider(InsuranceProviderDto insuranceProvider) {
        this.insuranceProvider = insuranceProvider;
    }

    public Language getPreferredLanguage() {
        return preferredLanguage;
    }

    public void setPreferredLanguage(Language preferredLanguage) {
        this.preferredLanguage = preferredLanguage;
    }

    public List<Language> getSpokenLanguages() {
        return spokenLanguages;
    }

    public void setSpokenLanguages(List<Language> spokenLanguages) {
        this.spokenLanguages = spokenLanguages;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public List<AuthorityRole> getRoles() {
        return roles;
    }

    public void setRoles(List<AuthorityRole> roles) {
        this.roles = roles;
    }
}
