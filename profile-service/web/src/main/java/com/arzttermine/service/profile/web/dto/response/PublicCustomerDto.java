package com.arzttermine.service.profile.web.dto.response;

import com.arzttermine.library.web.struct.Language;
import com.arzttermine.service.profile.web.struct.Gender;

import java.util.HashSet;
import java.util.Set;

public class PublicCustomerDto
{
    private long id;
    private String email;
    private String firstName;
    private String lastName;
    private String fullName;
    private String title;
    private Gender gender;
    private Language preferredLanguage;
    private Set<Language> languages = new HashSet<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Language getPreferredLanguage() {
        return preferredLanguage;
    }

    public void setPreferredLanguage(Language preferredLanguage) {
        this.preferredLanguage = preferredLanguage;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Set<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(Set<Language> languages) {
        this.languages = languages;
    }

    public String getTitledFullName() {
        String name = firstName + " " + lastName;
        if (null != title)
            name = title + " " + name;

        return name;
    }
}
