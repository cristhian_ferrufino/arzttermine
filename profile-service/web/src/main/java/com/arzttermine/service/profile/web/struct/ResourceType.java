package com.arzttermine.service.profile.web.struct;

public enum ResourceType
{
    DEVICE,
    DOCTOR,
    ASSISTANT,
    ROOM,
}
