package com.arzttermine.service.profile.web.dto.response;

import com.arzttermine.service.profile.web.struct.ResourceType;
import com.arzttermine.service.profile.web.validator.constraint.HexColor;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

public class ResourceDto
{
    private Long id;

    @ApiModelProperty("relation to existing entity in our database. Eg id of customer for DOCTOR resource")
    private Long systemId;

    @NotNull
    private String name;

    @NotNull
    @HexColor
    private String color;

    @NotNull
    private ResourceType type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public ResourceType getType() {
        return type;
    }

    public void setType(ResourceType type) {
        this.type = type;
    }

    public Long getSystemId() {
        return systemId;
    }

    public void setSystemId(Long systemId) {
        this.systemId = systemId;
    }
}
