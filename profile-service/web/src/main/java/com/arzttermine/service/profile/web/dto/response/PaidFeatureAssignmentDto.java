package com.arzttermine.service.profile.web.dto.response;

import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;

public class PaidFeatureAssignmentDto {

    @NotNull
    private Long featureId;

    @NotNull
    private String name;

    private Map<String, Object> options = new HashMap<>();

    public Long getPaidFeatureId() {
        return featureId;
    }

    public void setPaidFeatureId(Long featureId) {
        this.featureId = featureId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, Object> getOptions() {
        return options;
    }

    public void setOptions(Map<String, Object> options) {
        this.options = options;
    }
}
