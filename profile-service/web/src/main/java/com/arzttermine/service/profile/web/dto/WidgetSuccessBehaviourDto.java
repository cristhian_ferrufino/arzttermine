package com.arzttermine.service.profile.web.dto;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.SafeHtml;

public class WidgetSuccessBehaviourDto
{
    @Length(min = 3, max = 255)
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    private String text;

    @Length(min = 3, max = 255)
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    private String link;

    public WidgetSuccessBehaviourDto() {
    }

    public WidgetSuccessBehaviourDto(String text, String link) {
        this.text = text;
        this.link = link;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
