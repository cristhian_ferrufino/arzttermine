package com.arzttermine.service.profile.web.dto.request;

import com.arzttermine.service.profile.web.dto.OpeningHoursDto;
import com.arzttermine.service.profile.web.dto.WidgetConfigurationDto;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.SafeHtml;

import javax.validation.Valid;
import javax.validation.constraints.AssertTrue;
import java.util.List;
import java.util.Map;

public class UpdatePracticeDto extends PracticeRequestDto
{

    @Valid
    public WidgetConfigurationDto getWidgetConfiguration() {
        return super.widgetConfiguration;
    }

    @Valid
    public List<CreateReviewSiteDto> getReviewSites() {
        return reviewSites;
    }

    @Valid
    public Map<String, Map<String, String>> getPaidFeatures() {
        return paidFeatures;
    }

    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    public String getName() {
        return name;
    }

    @Valid
    @ApiModelProperty("general opening hours of practice, must be updated as patch")
    public OpeningHoursDto getOpeningHours() {
        return openingHours;
    }

    @AssertTrue(message = "please update at least one property")
    public boolean assertChange() {
        return (null != name && !name.isEmpty())
            || null != reviewSites
            || null != paidFeatures
            || (null != widgetConfiguration && widgetConfiguration.hasValues());
    }

}
