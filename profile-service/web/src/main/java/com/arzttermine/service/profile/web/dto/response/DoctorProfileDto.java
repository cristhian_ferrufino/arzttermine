package com.arzttermine.service.profile.web.dto.response;

import com.arzttermine.service.profile.web.struct.MedicalSpecialtyType;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class DoctorProfileDto extends PublicCustomerDto
{
    private long profileId;

    @ApiModelProperty("minimum offset in hours between current date and the date a booking gets created")
    private Integer bookingTimeOffset = 1;

    private Collection<TreatmentTypeDto> treatmentTypes = new ArrayList<>();

    private Set<MedicalSpecialtyType> medicalSpecialties = new HashSet<>();

    private boolean bookable;

    public Collection<TreatmentTypeDto> getTreatmentTypes() {
        return treatmentTypes;
    }

    public void setTreatmentTypes(Collection<TreatmentTypeDto> treatmentTypes) {
        this.treatmentTypes = treatmentTypes;
    }

    public long getProfileId() {
        return profileId;
    }

    public void setProfileId(long profileId) {
        this.profileId = profileId;
    }

    public Integer getBookingTimeOffset() {
        return bookingTimeOffset;
    }

    public void setBookingTimeOffset(Integer bookingTimeOffset) {
        this.bookingTimeOffset = bookingTimeOffset;
    }

    public Set<MedicalSpecialtyType> getMedicalSpecialties() {
        return medicalSpecialties;
    }

    public void setMedicalSpecialties(Set<MedicalSpecialtyType> medicalSpecialties) {
        this.medicalSpecialties = medicalSpecialties;
    }

    public boolean isBookable() {
        return bookable;
    }

    public void setBookable(boolean bookable) {
        this.bookable = bookable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DoctorProfileDto that = (DoctorProfileDto) o;

        return getId() == that.getId() || profileId == that.profileId;
    }

    @Override
    public int hashCode() {
        return (int) ((profileId ^ (profileId >>> 6)) + (profileId ^ (profileId >>> 3)));
    }
}
