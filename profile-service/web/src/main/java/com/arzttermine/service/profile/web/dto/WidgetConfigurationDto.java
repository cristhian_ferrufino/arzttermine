package com.arzttermine.service.profile.web.dto;

import io.swagger.annotations.ApiModel;
import org.hibernate.validator.constraints.SafeHtml;

import javax.validation.Valid;

@ApiModel("This configuration is used to customize the widget to the practice needs.")
public class WidgetConfigurationDto
{
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    private String templateId;

    @Valid
    private WidgetSuccessBehaviourDto success;

    public boolean hasValues() {
        return null != templateId
            || null != success;
    }

    public WidgetSuccessBehaviourDto getSuccess() {
        return success;
    }

    public void setSuccess(WidgetSuccessBehaviourDto success) {
        this.success = success;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }
}
