package com.arzttermine.service.profile.web.validator.constraint;

import com.arzttermine.service.profile.web.validator.HexColorValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.NotNull;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@NotNull
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = HexColorValidator.class)
public @interface HexColor
{
    String message() default "'${validatedValue}' is not a valid color representation";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
