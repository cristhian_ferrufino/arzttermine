package com.arzttermine.service.profile.web.dto.response;

import javax.validation.constraints.NotNull;

public class ReviewSiteDto {

    @NotNull
    private Long id;

    @NotNull
    private String type;

    @NotNull
    private String data;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

}
