package com.arzttermine.service.profile.web.struct;

public enum Gender
{
    MALE,
    FEMALE,
}
