INSERT INTO `customers` (`id`, `gender`, `username`, `email`, `first_name`, `last_name`, `enabled`, `created_at`, `updated_at`, `last_credentials_update`, `credentials_expired_notification`) VALUES
    (66, 'MALE', 'doctor-user', 'doctor@user.de', 'first-name', 'last-name', 1, now(), now(), now(), now()),
    (99, 'FEMALE', 'real-doctor', 'real@doctor.de', 'first-name', 'last-name', 1, now(), now(), now(), now());

INSERT INTO `oauth_customer_roles` (`id`, `client_id`, `role`) VALUES
    (10000, 66, 'ROLE_CUSTOMER'),
    (10001, 99, 'ROLE_CUSTOMER'),
    (10002, 99, 'ROLE_DOCTOR');

INSERT INTO `doctor_profiles` (`id`, `customer_id`, `booking_time_offset`, `bookable`) VALUES
    (123, 99, 1, 1);
