INSERT INTO paid_features (id, name) VALUES
    (1, 'sms_appointment_notifications'),
    (2, 'online_payments'),
    (3, 'mail_review_notifications'),
    (4, 'sms_review_notifications'),
    (5, 'multiple_doctors');