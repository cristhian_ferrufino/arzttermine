DELETE FROM oauth_client_details;

INSERT INTO oauth_client_details(access_token_validity, refresh_token_validity, authorities, grant_types, client_id, client_secret, scope) VALUES
    (9999, 99999, 'ROLE_SYSTEM', 'password', 'client', 'secret', 'default');
