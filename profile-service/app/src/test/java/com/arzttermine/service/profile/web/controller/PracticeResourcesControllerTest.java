package com.arzttermine.service.profile.web.controller;

import com.arzttermine.service.profile.web.BaseIntegrationTest;
import com.arzttermine.service.profile.web.dto.request.CreateLocationDto;
import com.arzttermine.service.profile.web.dto.request.CreatePracticeDto;
import com.arzttermine.service.profile.web.dto.request.CreateResourceDto;
import com.arzttermine.service.profile.web.dto.response.CustomerDto;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import com.arzttermine.service.profile.web.dto.response.ResourceDto;
import com.arzttermine.service.profile.web.struct.ResourceType;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;

import javax.annotation.PostConstruct;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class PracticeResourcesControllerTest extends BaseIntegrationTest
{
    private static final long customerId = 55;
    private static Long practiceId;

    @PostConstruct
    public void setupResourceTest() throws Exception {
        if (null != practiceId) {
            return;
        }

        CreateLocationDto location = new CreateLocationDto();
        location.setContactMail("mail@example.com");
        location.setLocation("street 1234");
        location.setLongitude(540f);
        location.setLatitude(450f);
        location.setZip("12345");
        location.setCity("berlin");

        CreatePracticeDto request = new CreatePracticeDto();
        request.setLocation(location);
        request.setName("nice one");
        request.setOwnerId(55L);

        practiceId = objectMapper.readValue(mockMvc.perform(customerRequest(customerId, () -> post("/practices"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsByteArray(), PracticeDto.class)
            .getId();
    }

    @Test
    public void createResource_should_add_new_resource() throws Exception
    {
        CreateResourceDto resourceDto = new CreateResourceDto();
        resourceDto.setType(ResourceType.ASSISTANT);
        resourceDto.setColor("#123456");
        resourceDto.setName("example");

        mockMvc.perform(practiceRequest(practiceId, customerId, () -> post("/practices/" + practiceId + "/resources"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(resourceDto)))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.id", notNullValue()))
            .andExpect(jsonPath("$.name", is("example")))
            .andExpect(jsonPath("$.type", is(ResourceType.ASSISTANT.name())))
            .andExpect(jsonPath("$.color", is("#123456")));
    }

    @Test
    public void createResource_should_report_missing_practice() throws Exception
    {
        CreateResourceDto resourceDto = new CreateResourceDto();
        resourceDto.setType(ResourceType.ASSISTANT);
        resourceDto.setColor("#123456");
        resourceDto.setName("example");

        mockMvc.perform(practiceRequest(999L, customerId, () -> post("/practices/999/resources"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(resourceDto)))
            .andExpect(status().isNotFound())
            .andExpect(jsonPath("$[0].code", is("NOT_FOUND")))
            .andExpect(jsonPath("$[0].reference", is(999)));
    }

    @Test
    public void createResource_should_be_accessible_for_practice_members_and_employees_only() throws Exception
    {
        CreateResourceDto resourceDto = new CreateResourceDto();
        resourceDto.setType(ResourceType.ASSISTANT);
        resourceDto.setColor("#123456");
        resourceDto.setName("example");

        mockMvc.perform(post("/practices/" + practiceId + "/resources")
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(resourceDto)))
            .andExpect(status().isUnauthorized());

        mockMvc.perform(practiceRequest(999L, customerId, () -> post("/practices/" + practiceId + "/resources"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(resourceDto)))
            .andExpect(status().isForbidden());

        mockMvc.perform(customerRequest(customerId, () -> post("/practices/" + practiceId + "/resources"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(resourceDto)))
            .andExpect(status().isForbidden());
    }

    @Test
    @Sql(statements = "DELETE FROM `resources`")
    public void getResources_should_return_sorted_page_of_resources() throws Exception
    {
        CreateResourceDto resourceDto = new CreateResourceDto();
        resourceDto.setType(ResourceType.ASSISTANT);
        resourceDto.setColor("#123456");
        resourceDto.setName("example");

        mockMvc.perform(practiceRequest(practiceId, customerId, () -> post("/practices/" + practiceId + "/resources"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(resourceDto)))
            .andExpect(status().isCreated());

        resourceDto.setName("something else");
        mockMvc.perform(practiceRequest(practiceId, customerId, () -> post("/practices/" + practiceId + "/resources"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(resourceDto)))
            .andExpect(status().isCreated());

        mockMvc.perform(practiceRequest(practiceId, customerId, () -> get("/practices/" + practiceId + "/resources?sort=name,desc")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.page.totalItems", is(2)))
            .andExpect(jsonPath("$.page.totalPages", is(1)))
            .andExpect(jsonPath("$.elements[0].id", notNullValue()))
            .andExpect(jsonPath("$.elements[0].name", is("something else")))
            .andExpect(jsonPath("$.elements[0].type", is(ResourceType.ASSISTANT.name())))
            .andExpect(jsonPath("$.elements[0].color", is("#123456")))
            .andExpect(jsonPath("$.elements[1].id", notNullValue()))
            .andExpect(jsonPath("$.elements[1].name", is("example")))
            .andExpect(jsonPath("$.elements[1].type", is(ResourceType.ASSISTANT.name())))
            .andExpect(jsonPath("$.elements[1].color", is("#123456")));
    }

    @Test
    @Sql(statements = "DELETE FROM `resources`")
    public void getResources_should_filter_by_resource_predicates() throws Exception
    {
        CreateResourceDto resourceDto = new CreateResourceDto();
        resourceDto.setType(ResourceType.ASSISTANT);
        resourceDto.setColor("#123456");
        resourceDto.setName("example");
        resourceDto.setSystemId(42L);

        mockMvc.perform(practiceRequest(practiceId, customerId, () -> post("/practices/" + practiceId + "/resources"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(resourceDto)))
            .andExpect(status().isCreated());

        resourceDto.setName("something else");
        resourceDto.setType(ResourceType.DOCTOR);

        mockMvc.perform(practiceRequest(practiceId, customerId, () -> post("/practices/" + practiceId + "/resources"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(resourceDto)))
            .andExpect(status().isCreated());

        resourceDto.setSystemId(99L);
        resourceDto.setName("another doctor");
        resourceDto.setType(ResourceType.DOCTOR);

        mockMvc.perform(practiceRequest(practiceId, customerId, () -> post("/practices/" + practiceId + "/resources"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(resourceDto)))
            .andExpect(status().isCreated());

        mockMvc.perform(practiceRequest(practiceId, customerId, () -> get("/practices/" + practiceId + "/resources?sort=name,desc&type=DOCTOR")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.page.totalItems", is(2)))
            .andExpect(jsonPath("$.page.totalPages", is(1)))
            .andExpect(jsonPath("$.elements[0].id", notNullValue()))
            .andExpect(jsonPath("$.elements[0].name", is("something else")))
            .andExpect(jsonPath("$.elements[0].type", is(ResourceType.DOCTOR.name())))
            .andExpect(jsonPath("$.elements[0].color", is("#123456")))
            .andExpect(jsonPath("$.elements[1].id", notNullValue()))
            .andExpect(jsonPath("$.elements[1].name", is("another doctor")))
            .andExpect(jsonPath("$.elements[1].type", is(ResourceType.DOCTOR.name())))
            .andExpect(jsonPath("$.elements[1].color", is("#123456")));

        mockMvc.perform(practiceRequest(practiceId, customerId, () -> get("/practices/" + practiceId + "/resources?sort=name,desc&type=DOCTOR&systemId=42")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.page.totalItems", is(1)))
            .andExpect(jsonPath("$.page.totalPages", is(1)))
            .andExpect(jsonPath("$.elements[0].id", notNullValue()))
            .andExpect(jsonPath("$.elements[0].name", is("something else")))
            .andExpect(jsonPath("$.elements[0].systemId", is(42)))
            .andExpect(jsonPath("$.elements[0].type", is(ResourceType.DOCTOR.name())))
            .andExpect(jsonPath("$.elements[0].color", is("#123456")));
    }

    @Test
    @Sql(statements = "DELETE FROM `resources`")
    public void getResource_should_return_single_resource() throws Exception {
        CreateResourceDto resourceDto = new CreateResourceDto();
        resourceDto.setType(ResourceType.DOCTOR);
        resourceDto.setColor("#123456");
        resourceDto.setName("example");
        resourceDto.setSystemId(42L);

        long resourceId = objectMapper.readValue(mockMvc.perform(practiceRequest(practiceId, customerId, () -> post("/practices/" + practiceId + "/resources"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(resourceDto)))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), ResourceDto.class)
            .getId();

        mockMvc.perform(practiceRequest(practiceId, customerId, () -> get("/practices/" + practiceId + "/resources/" + resourceId)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id", is((int) resourceId)))
            .andExpect(jsonPath("$.name", is("example")))
            .andExpect(jsonPath("$.type", is(ResourceType.DOCTOR.name())))
            .andExpect(jsonPath("$.color", is("#123456")));
    }

    @Test
    @Sql(statements = "DELETE FROM `resources`")
    public void updateResource_should_update_resource_as_patch() throws Exception
    {
        CreateResourceDto resourceDto = new CreateResourceDto();
        resourceDto.setType(ResourceType.ASSISTANT);
        resourceDto.setColor("#123456");
        resourceDto.setName("example");

        long id = objectMapper.readValue(mockMvc.perform(practiceRequest(practiceId, customerId, () -> post("/practices/" + practiceId + "/resources"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(resourceDto)))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), ResourceDto.class)
            .getId();

        resourceDto.setName("something else");

        mockMvc.perform(practiceRequest(practiceId, customerId, () -> patch("/practices/" + practiceId + "/resources/" + id))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(resourceDto)))
            .andExpect(status().isNoContent());

        mockMvc.perform(practiceRequest(practiceId, customerId, () -> get("/practices/" + practiceId + "/resources?sort=name,desc")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.page.totalItems", is(1)))
            .andExpect(jsonPath("$.page.totalPages", is(1)))
            .andExpect(jsonPath("$.elements[0].id", is((int) id)))
            .andExpect(jsonPath("$.elements[0].name", is("something else")))
            .andExpect(jsonPath("$.elements[0].type", is(ResourceType.ASSISTANT.name())))
            .andExpect(jsonPath("$.elements[0].color", is("#123456")));
    }

    @Test
    @Sql(statements = "DELETE FROM `resources`")
    public void removeResource_should_remove_resource() throws Exception
    {
        CreateResourceDto resourceDto = new CreateResourceDto();
        resourceDto.setType(ResourceType.ASSISTANT);
        resourceDto.setColor("#123456");
        resourceDto.setName("example");

        long id = objectMapper.readValue(mockMvc.perform(practiceRequest(practiceId, customerId, () -> post("/practices/" + practiceId + "/resources"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(resourceDto)))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), ResourceDto.class)
            .getId();

        mockMvc.perform(practiceRequest(9999L, customerId, () -> delete("/practices/" + practiceId + "/resources/" + id)))
            .andExpect(status().isForbidden());

        mockMvc.perform(practiceRequest(practiceId, customerId, () -> delete("/practices/" + practiceId + "/resources/" + id)))
            .andExpect(status().isNoContent());

        mockMvc.perform(practiceRequest(practiceId, customerId, () -> get("/practices/" + practiceId + "/resources?sort=name,desc")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.page.totalItems", is(0)))
            .andExpect(jsonPath("$.page.totalPages", is(0)));
    }

    @Test
    @Sql({"/db/empty_database.sql", "/db/test_doctors.sql"})
    public void createPractice_should_add_resource() throws Exception
    {
        practiceId = null; // generate new practice after this test

        CreatePracticeDto request = PracticeControllerTest.buildRequestDto();

        mockMvc.perform(customerRequest(66L, () -> post("/practices"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.id").exists())
            .andExpect(jsonPath("$.name", is(request.getName())))
            .andExpect(jsonPath("$.medicalSpecialties").isEmpty())
            .andExpect(jsonPath("$.location.location", is(request.getLocation().getLocation())))
            .andExpect(jsonPath("$.location.zip", is(request.getLocation().getZip())))
            .andExpect(jsonPath("$.location.latitude").value(request.getLocation().getLatitude()))
            .andExpect(jsonPath("$.location.longitude").value(request.getLocation().getLongitude()))
            .andExpect(jsonPath("$.ownerId", is(66)))
            .andExpect(jsonPath("$.resources[*]", hasSize(1)))
            .andExpect(jsonPath("$.resources[0].id", notNullValue()))
            .andExpect(jsonPath("$.resources[0].type", is(ResourceType.DOCTOR.name())))
            .andExpect(jsonPath("$.resources[0].name", is("first-name last-name")));
    }

    @Test
    @Sql({"/db/empty_database.sql", "/db/test_doctors.sql"})
    public void addDoctorToPractice_should_add_new_doctor_resource() throws Exception
    {
        practiceId = null; // generate new practice after this test

        final long practiceId = objectMapper.readValue(mockMvc.perform(customerRequest(66L, () -> post("/practices"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(PracticeControllerTest.buildRequestDto(555, 555))))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), PracticeDto.class)
            .getId();

        final CustomerDto anotherDoctor = objectMapper.readValue(mockMvc.perform(systemRequest(() -> post("/customers"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(CustomerControllerTest.buildRequestDto("another@one.doctor"))))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), CustomerDto.class);

        mockMvc.perform(employeeRequest(42L, () -> post("/practices/" + practiceId + "/doctors/" + anotherDoctor.getId())))
            .andExpect(status().isNoContent());

        mockMvc.perform(customerRequest(77L, () -> get("/practices/" + practiceId)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.resources[*]", hasSize(2)))
            .andExpect(jsonPath("$.resources[0].id", notNullValue()))
            .andExpect(jsonPath("$.resources[1].id", notNullValue()))
            .andExpect(jsonPath("$.resources[*].type", containsInAnyOrder(ResourceType.DOCTOR.name(), ResourceType.DOCTOR.name())))
            .andExpect(jsonPath("$.resources[*].name", containsInAnyOrder("first-name last-name", "example example")));
    }

    @Test
    @Sql({"/db/empty_database.sql", "/db/test_doctors.sql"})
    public void removeDoctorFromPractice_should_remove_doctor_resource() throws Exception
    {
        practiceId = null; // generate new practice after this test

        final long practiceId = objectMapper.readValue(mockMvc.perform(customerRequest(66L, () -> post("/practices"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(PracticeControllerTest.buildRequestDto(555, 555))))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), PracticeDto.class)
            .getId();

        final CustomerDto anotherDoctor = objectMapper.readValue(mockMvc.perform(systemRequest(() -> post("/customers"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(CustomerControllerTest.buildRequestDto("another@one.doctor"))))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), CustomerDto.class);

        mockMvc.perform(employeeRequest(42L, () -> post("/practices/" + practiceId + "/doctors/" + anotherDoctor.getId())))
            .andExpect(status().isNoContent());

        mockMvc.perform(customerRequest(77L, () -> get("/practices/" + practiceId)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.resources[*]", hasSize(2)));

        mockMvc.perform(employeeRequest(42L, () -> delete("/practices/" + practiceId + "/doctors/" + anotherDoctor.getId())))
            .andExpect(status().isNoContent());

        mockMvc.perform(customerRequest(77L, () -> get("/practices/" + practiceId)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.resources[*]", hasSize(1)));
    }
}
