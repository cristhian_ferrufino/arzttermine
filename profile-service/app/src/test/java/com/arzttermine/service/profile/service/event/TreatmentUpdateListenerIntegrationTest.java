package com.arzttermine.service.profile.service.event;

import com.arzttermine.library.elasticsearch.model.BulkAction;
import com.arzttermine.library.elasticsearch.service.DocumentProcessor;
import com.arzttermine.library.web.dto.PriceDto;
import com.arzttermine.service.profile.consumer.TestConsumerCounter;
import com.arzttermine.service.profile.web.BaseIntegrationTest;
import com.arzttermine.service.profile.web.controller.PracticeControllerTest;
import com.arzttermine.service.profile.web.dto.request.CreateTreatmentTypeDto;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import com.arzttermine.service.profile.web.dto.response.TreatmentTypeDto;
import org.junit.After;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;

import java.util.Collection;

import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql({"/db/empty_database.sql", "/db/test_doctors.sql"})
public class TreatmentUpdateListenerIntegrationTest extends BaseIntegrationTest
{
    @Autowired
    private DocumentProcessor documentProcessor;

    @Autowired
    private TestConsumerCounter testCounter;

    @After
    public void teardown() throws Exception {
        reset(documentProcessor);
        testCounter.flush();
    }

    @Test
    public void treatmentUpdated_should_update_according_practices() throws Exception
    {
        final long practiceId = objectMapper.readValue(mockMvc.perform(customerRequest(99L, () -> post("/practices"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(PracticeControllerTest.buildRequestDto(12345, 54321))))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), PracticeDto.class)
            .getId();

        reset(documentProcessor);
        doAnswer(invocation -> {
            @SuppressWarnings("unchecked") Collection<BulkAction<PracticeDto>> actions = invocation.getArgumentAt(2, Collection.class);
            PracticeDto practiceDoc = actions.iterator().next().getDocument();

            assertEquals(1, actions.size());
            assertEquals(practiceId, practiceDoc.getId());

            return null;
        }).when(documentProcessor).execute(eq("practices"), eq("germany"), any());

        CreateTreatmentTypeDto tt = new CreateTreatmentTypeDto();
        tt.setType("a random type");
        tt.setCharge(new PriceDto(100));
        tt.getDoctorIds().add(99L);

        long ttId = objectMapper.readValue(mockMvc.perform(employeeRequest(42L, () -> post("/practices/" + practiceId + "/treatments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(tt)))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.id", notNullValue()))
            .andReturn()
            .getResponse()
            .getContentAsString(), TreatmentTypeDto.class)
            .getId();

        Thread.sleep(50);

        verify(documentProcessor, times(1)).execute(eq("practices"), eq("germany"), any());
        mockMvc.perform(systemRequest(() -> delete("/practices/ " + practiceId + "/treatments/" + ttId)))
            .andExpect(status().isNoContent());

        Thread.sleep(50);

        verify(documentProcessor, times(2)).execute(eq("practices"), eq("germany"), any());
    }
}
