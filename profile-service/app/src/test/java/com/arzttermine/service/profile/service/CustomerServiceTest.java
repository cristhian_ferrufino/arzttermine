package com.arzttermine.service.profile.service;

import com.arzttermine.library.suport.exception.AccessDeniedException;
import com.arzttermine.library.suport.exception.EntityAlreadyExistException;
import com.arzttermine.library.suport.exception.EntityNotFoundException;
import com.arzttermine.library.suport.exception.InvalidArgumentException;
import com.arzttermine.library.web.struct.Language;
import com.arzttermine.service.profile.domain.entity.Customer;
import com.arzttermine.service.profile.domain.entity.OAuthClientRole;
import com.arzttermine.service.profile.domain.repository.CustomerRepository;
import com.arzttermine.service.profile.domain.repository.OAuthAuthorityRepository;
import com.arzttermine.service.profile.service.mapper.CustomerMapper;
import com.arzttermine.service.profile.web.dto.request.CreateCustomerDto;
import com.arzttermine.service.profile.web.dto.request.UpdateCustomerDto;
import com.arzttermine.service.profile.web.struct.AuthorityRole;
import com.arzttermine.service.profile.web.struct.Gender;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CustomerServiceTest
{
    @Mock
    private CustomerRepository customerRepository;

    @Mock
    private CustomerMapper customerMapper;

    @Mock
    private OAuthAuthorityRepository authorityRepository;

    @InjectMocks
    private CustomerService customerService;

    private OAuth2Authentication auth;

    @Before
    public void setup() {
        when(customerMapper.toDto(any())).thenCallRealMethod();

        auth = mock(OAuth2Authentication.class);
        when(auth.getAuthorities()).thenReturn(Collections.singletonList(new SimpleGrantedAuthority("ROLE_EMPLOYEE")));
    }

    @Test(expected = EntityNotFoundException.class)
    public void updateCustomer_should_throw_not_found()
    {
        when(customerRepository.findOne(anyLong())).thenReturn(null);
        customerService.updateCustomer(42L, new UpdateCustomerDto(), auth);
    }

    @Test
    public void updateCustomer_should_update_non_nullable_values()
    {
        Customer customer = new Customer();
        customer.setEmail("current@one.com");
        customer.setPreferredLanguage(Language.GERMAN);

        when(customerRepository.findOne(42L)).thenReturn(customer);

        UpdateCustomerDto request = new UpdateCustomerDto();
        request.setEmail("new@one.com");

        customerService.updateCustomer(42L, request, auth);
        assertEquals(customer.getEmail(), "new@one.com");

        request.setTitle("Dr. Prof.");
        request.setFirstName("first");
        request.setLastName("last");

        customerService.updateCustomer(42L, request, auth);
        assertEquals(customer.getFirstName(), "first");
        assertEquals(customer.getLastName(), "last");
        assertEquals(customer.getTitle(), "Dr. Prof.");

        request.setPreferredLanguage(Language.ENGLISH);

        customerService.updateCustomer(42L, request, auth);
        assertEquals(customer.getPreferredLanguage(), Language.ENGLISH);

        request.setPhone("+493012345");
        request.setPhoneMobile("+49123456789");

        customerService.updateCustomer(42L, request, auth);
        assertEquals(customer.getPhone(), "+493012345");
        assertEquals(customer.getPhoneMobile(), "+49123456789");
    }

    public void updateCustomer_Gender_Can_Be_Null() {
        Customer customer = new Customer();
        customer.setEmail("current@one.com");
        customer.setPreferredLanguage(Language.GERMAN);

        when(customerRepository.findOne(42L)).thenReturn(customer);

        UpdateCustomerDto request = new UpdateCustomerDto();
        request.setGender(Gender.MALE);
        customerService.updateCustomer(42l, request, auth);
        assertEquals(customer.getGender(), Gender.MALE);

        request.setGender(null);
        customerService.updateCustomer(42l, request, auth);
        assertEquals(customer.getGender(), null);
    }

    @Test(expected = EntityAlreadyExistException.class)
    public void updateCustomer_should_report_existing_account_by_email()
    {
        Customer customer = new Customer();
        customer.setEmail("old@one.com");

        UpdateCustomerDto request = new UpdateCustomerDto();
        request.setEmail("existing@mail.com");

        when(customerRepository.findOneByEmail("existing@mail.com")).thenReturn(new Customer());
        when(customerRepository.findOne(42L)).thenReturn(customer);

        customerService.updateCustomer(42L, request, auth);
    }

    @Test
    public void updateCustomer_should_not_update_email_if_equal_to_current()
    {
        Customer customer = new Customer();
        customer.setEmail("one@mail.com");

        UpdateCustomerDto request = new UpdateCustomerDto();
        request.setEmail("one@mail.com");

        when(customerRepository.findOne(42L)).thenReturn(customer);

        customerService.updateCustomer(42L, request, auth);

        verify(customerRepository, never()).findOneByEmail(any());
    }

    @Test(expected = InvalidArgumentException.class)
    public void updateCustomer_should_require_equal_passwords_for_update()
    {
        Customer customer = new Customer();
        customer.setPassword("initial", Instant.now().minus(5, ChronoUnit.DAYS));

        UpdateCustomerDto request = new UpdateCustomerDto();
        request.setPassword("new one");
        request.setRepeatPassword("are not equal");

        when(customerRepository.findOne(42L)).thenReturn(customer);

        customerService.updateCustomer(42L, request, auth);
    }

    @Test
    public void updateCustomer_should_require_current_email()
    {
        Customer customer = new Customer();
        customer.setPassword("initial", Instant.now().minus(5, ChronoUnit.DAYS));
        customer.setEmail("secret@one.com");

        UpdateCustomerDto request = new UpdateCustomerDto();
        request.setPassword("new one");
        request.setRepeatPassword("new one");
        request.setEmail("invalid@one.com");

        when(customerRepository.findOne(42L)).thenReturn(customer);

        customerService.updateCustomer(42L, request, auth);

        assertEquals(customer.getEmail(), "invalid@one.com");
        assertEquals(customer.getPassword(), "initial");
    }

    @Test
    public void updateCustomer_should_update_password()
    {
        Customer customer = new Customer();
        customer.setPassword("initial", Instant.now().minus(5, ChronoUnit.DAYS));
        customer.setEmail("secret@one.com");

        UpdateCustomerDto request = new UpdateCustomerDto();
        request.setPassword("new");
        request.setRepeatPassword("new");
        request.setEmail("secret@one.com");

        when(customerRepository.findOne(42L)).thenReturn(customer);

        customerService.updateCustomer(42L, request, auth);

        assertEquals(customer.getEmail(), "secret@one.com");
        verify(customerMapper, times(1)).setPasswordFor(customer, "new");
    }

    @Test(expected = AccessDeniedException.class)
    public void updateCustomer_should_should_report_invalid_access()
    {
        Customer customer = new Customer();
        customer.setPassword("initial", Instant.now().minus(5, ChronoUnit.DAYS));
        customer.setEmail("secret@one.com");

        UpdateCustomerDto request = new UpdateCustomerDto();
        request.setPassword("new");
        request.setRepeatPassword("new");
        request.setEmail("secret@one.com");

        when(customerRepository.findOne(42L)).thenReturn(customer);

        OAuth2Authentication auth = mock(OAuth2Authentication.class);
        when(auth.getAuthorities()).thenReturn(Collections.emptyList());
        when(auth.getPrincipal()).thenReturn(666L);

        customerService.updateCustomer(42L, request, auth);
    }

    @Test
    public void updateCustomer_should_not_report_invalid_access_for_trusted_practice_clients()
    {
        Customer customer = new Customer();
        customer.setPassword("initial", Instant.now().minus(5, ChronoUnit.DAYS));
        customer.setEmail("secret@one.com");

        UpdateCustomerDto request = new UpdateCustomerDto();
        request.setPassword("new");
        request.setRepeatPassword("new");
        request.setEmail("secret@one.com");

        when(customerRepository.findOne(42L)).thenReturn(customer);

        OAuth2Authentication auth = mock(OAuth2Authentication.class);
        when(auth.getAuthorities()).thenReturn(Collections.singletonList(new SimpleGrantedAuthority("ROLE_DOCTOR")));
        when(auth.getPrincipal()).thenReturn(666L);

        customerService.updateCustomer(42L, request, auth);
    }

    @Test
    public void updateCustomer_should_update_roles_if_requested()
    {
        Customer customer = new Customer();
        customer.addAuthority("ROLE_CLIENT"); // should be removed as patch

        UpdateCustomerDto request = new UpdateCustomerDto();
        request.setRoles(new ArrayList<>(Collections.singletonList(AuthorityRole.ROLE_EMPLOYEE))); // needs to be writeable

        when(customerRepository.findOne(42L)).thenReturn(customer);

        OAuth2Authentication auth = mock(OAuth2Authentication.class);
        when(auth.getAuthorities()).thenReturn(Collections.singletonList(new SimpleGrantedAuthority("ROLE_EMPLOYEE")));
        when(auth.getPrincipal()).thenReturn(666L);

        customerService.updateCustomer(42L, request, auth);

        assertEquals(2, customer.getRoles().size());
    }

    @Test
    public void updateCustomer_should_not_remove_practice_permissions()
    {
        Customer customer = new Customer();
        customer.addAuthority("ROLE_CLIENT");
        customer.addAuthority("ROLE_CUSTOMER");
        customer.addAuthority("ROLE_PRACTICE_42");

        UpdateCustomerDto request = new UpdateCustomerDto();
        request.setRoles(new ArrayList<>(Collections.singletonList(AuthorityRole.ROLE_CUSTOMER)));

        when(customerRepository.findOne(42L)).thenReturn(customer);

        OAuth2Authentication auth = mock(OAuth2Authentication.class);
        when(auth.getAuthorities()).thenReturn(Collections.singletonList(new SimpleGrantedAuthority("ROLE_EMPLOYEE")));
        when(auth.getPrincipal()).thenReturn(666L);

        customerService.updateCustomer(42L, request, auth);

        Iterator<OAuthClientRole> roles = customer.getRoles().iterator();
        assertEquals("ROLE_CUSTOMER", roles.next().getRole().getAuthority());
        assertEquals("ROLE_PRACTICE_42", roles.next().getRole().getAuthority());
        assertFalse(roles.hasNext());
    }

    @Test
    public void createCustomer_should_add_roles_if_requested_and_permissions_granted()
    {
        Customer customer = new Customer();

        CreateCustomerDto request = new CreateCustomerDto();
        request.setRoles(Arrays.asList(AuthorityRole.ROLE_EMPLOYEE, AuthorityRole.ROLE_CUSTOMER));

        when(customerRepository.save(any(Customer.class))).thenReturn(customer);

        OAuth2Authentication auth = mock(OAuth2Authentication.class);
        when(auth.getAuthorities()).thenReturn(Collections.singletonList(new SimpleGrantedAuthority("ROLE_EMPLOYEE")));
        when(auth.getPrincipal()).thenReturn(666L);

        customerService.createCustomer(request, auth);

        assertEquals(2, customer.getRoles().size());
    }

    @Test
    public void createCustomer_should_ignore_roles_if_permissions_not_granted()
    {
        Customer customer = new Customer();

        CreateCustomerDto request = new CreateCustomerDto();
        request.setRoles(Arrays.asList(AuthorityRole.ROLE_EMPLOYEE, AuthorityRole.ROLE_CUSTOMER));

        when(customerRepository.save(any(Customer.class))).thenReturn(customer);

        OAuth2Authentication auth = mock(OAuth2Authentication.class);
        when(auth.getAuthorities()).thenReturn(Collections.emptyList());
        when(auth.getPrincipal()).thenReturn(666L);

        customerService.createCustomer(request, auth);

        assertEquals(1, customer.getRoles().size());
        assertEquals("ROLE_CUSTOMER", customer.getRoles().iterator().next().getRole().getAuthority());
    }
}
