package com.arzttermine.service.profile.service.mapper;

import com.arzttermine.service.profile.domain.entity.Customer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class CustomerMapperTest
{
    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private CustomerMapper customerMapper;

    @Test
    public void setPasswordFor_should_update_password()
    {
        Customer customer = new Customer();
        customer.setPassword("password", Instant.now().minus(2500, ChronoUnit.DAYS));
        assertFalse(customer.isCredentialsNonExpired());

        when(passwordEncoder.encode("new-one")).thenReturn("new-one");

        customerMapper.setPasswordFor(customer, "new-one");

        assertEquals(customer.getPassword(), "new-one");
        assertTrue(customer.isCredentialsNonExpired());
    }
}
