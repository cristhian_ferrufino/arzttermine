package com.arzttermine.service.profile.service;

import com.arzttermine.service.profile.domain.entity.Customer;
import com.arzttermine.service.profile.domain.entity.DoctorProfile;
import com.arzttermine.service.profile.domain.entity.Practice;
import com.arzttermine.service.profile.domain.entity.TreatmentType;
import com.arzttermine.service.profile.domain.repository.CustomerRepository;
import com.arzttermine.service.profile.domain.repository.DoctorProfileRepository;
import com.arzttermine.service.profile.domain.repository.PaidFeatureRepository;
import com.arzttermine.service.profile.domain.repository.PracticeRepository;
import com.arzttermine.service.profile.service.mapper.*;
import com.arzttermine.service.profile.web.dto.request.CreatePracticeDto;
import com.arzttermine.service.profile.web.dto.request.CreateReviewSiteDto;
import com.arzttermine.service.profile.web.dto.request.UpdatePracticeDto;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import com.arzttermine.service.profile.web.struct.AuthorityRole;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PracticeServiceTest
{
    @Spy
    private PracticeMapper practiceMapper = new PracticeMapper(
        mock(LocationMapper.class),
        new CustomerMapper(),
        new ResourceMapper(),
        new PaidFeatureAssignmentMapper(),
        new ReviewSiteMapper()
    );

    @Mock
    private DoctorProfileRepository profileRepository;

    @Mock
    private PracticeRepository practiceRepository;

    @Mock
    private CustomerRepository customerRepository;

    @Mock
    private PaidFeatureRepository paidFeatureRepository;

    @Mock
    private ApplicationEventPublisher eventPublisher;

    @Mock
    private ReviewSiteMapper reviewSiteMapper;

    @InjectMocks
    private PracticeService practiceService;

    @Before
    public void setup() {
        when(profileRepository.findOneByCustomerId(anyLong())).thenReturn(null);
        when(customerRepository.findAll(any(Iterable.class))).thenReturn(Collections.emptyList());

        when(profileRepository.save(any(DoctorProfile.class))).thenAnswer(
            invocation -> invocation.getArgumentAt(0, DoctorProfile.class));

        when(reviewSiteMapper.fromRequest(any(), any())).thenCallRealMethod();
    }

    @Test
    public void createPractice_should_set_owner()
    {
        Customer customer = spy(new Customer());
        when(customer.getId()).thenReturn(42L);
        customer.setEmail("mail");

        when(customerRepository.findOne(42L)).thenReturn(customer);

        OAuth2Authentication auth = mock(OAuth2Authentication.class);
        when(auth.getAuthorities()).thenReturn(Collections.singletonList(new SimpleGrantedAuthority("ROLE_CLIENT")));
        when(auth.getPrincipal()).thenReturn(42L);

        CreatePracticeDto request = new CreatePracticeDto();
        request.setOwnerId(66L);

        Practice practice = new Practice();
        practice.setOwner(profile(customer));

        when(practiceRepository.save(any(Practice.class))).thenReturn(practice);

        PracticeDto dto = practiceService.createPractice(request, auth);

        assertEquals(42L, dto.getOwnerId().longValue());
        verify(customerRepository, times(1)).findOne(42L);
        verify(practiceRepository, times(1)).save(any(Practice.class));
    }

    @Test
    public void createPractice_should_deal_with_null_owner()
    {
        when(customerRepository.findOne(42L)).thenReturn(null);

        CreatePracticeDto request = new CreatePracticeDto();
        request.setOwnerId(42L);

        OAuth2Authentication auth = mock(OAuth2Authentication.class);
        when(auth.getPrincipal()).thenReturn(42L);

        doReturn(null).when(practiceMapper).toDto(any());
        practiceService.createPractice(request, auth);

        verify(customerRepository, times(1)).findOne(42L);
        verify(practiceRepository, times(1)).save(any(Practice.class));
    }

    @Test
    public void createPractice_with_owner_should_not_duplicate_in_doctors() {
        Customer customer = spy(new Customer());
        when(customer.getId()).thenReturn(42L);
        customer.setEmail("mail");

        when(customerRepository.findOne(42L)).thenReturn(customer);

        OAuth2Authentication auth = mock(OAuth2Authentication.class);
        when(auth.getAuthorities()).thenReturn(Collections.singletonList(new SimpleGrantedAuthority("ROLE_CLIENT")));
        when(auth.getPrincipal()).thenReturn(42L);

        CreatePracticeDto request = new CreatePracticeDto();
        request.setOwnerId(42L);
        request.setInitialDoctors(Arrays.asList(42l));

        Practice practice = new Practice();
        practice.setOwner(profile(customer));
        practice.addDoctor(profile(customer));

        when(practiceRepository.save(any(Practice.class))).thenReturn(practice);

        PracticeDto dto = practiceService.createPractice(request, auth);

        assertEquals(42L, dto.getOwnerId().longValue());
        assertEquals(1, dto.getDoctors().size());

    }


    @Test
    public void createPractice_should_set_new_authorities_on_customer()
    {
        Customer customer = new Customer();
        customer.setEmail("mail");
        customer.addAuthority(AuthorityRole.ROLE_CUSTOMER);

        when(customerRepository.findOne(42L)).thenReturn(customer);

        OAuth2Authentication auth = mock(OAuth2Authentication.class);
        when(auth.getAuthorities()).thenReturn(Collections.singletonList(new SimpleGrantedAuthority("ROLE_CLIENT")));
        when(auth.getPrincipal()).thenReturn(42L);

        Practice practice = spy(new Practice());
        when(practice.getId()).thenReturn(999L);

        when(practiceRepository.save(any(Practice.class))).thenReturn(practice);
        practiceService.createPractice(new CreatePracticeDto(), auth);

        Collection<? extends GrantedAuthority> authorities = customer.getAuthorities();
        Iterator<? extends GrantedAuthority> readable = authorities.iterator();

        assertEquals(3, authorities.size());
        assertEquals("ROLE_CUSTOMER", readable.next().getAuthority());
        assertEquals("ROLE_DOCTOR", readable.next().getAuthority());
        assertEquals("ROLE_PRACTICE_999", readable.next().getAuthority());
    }

    @Test
    public void updatePractice_should_set_preferred_review_site_even_review_sites_are_not_provided()
    {
        Practice practice = spy(new Practice());
        when(practice.getId()).thenReturn(42L);
        practice.setPreferredReviewSiteType("invalid");

        UpdatePracticeDto request = new UpdatePracticeDto();
        request.setPreferredReviewSiteType("my preferred one");

        when(practiceRepository.findOne(42L)).thenReturn(practice);

        PracticeDto result = practiceService.updatePractice(42L, request);

        assertEquals("my preferred one", result.getPreferredReviewSiteType());
    }

    @Test
    public void updatePractice_should_set_preferred_review_site()
    {
        Practice practice = spy(new Practice());
        when(practice.getId()).thenReturn(42L);

        UpdatePracticeDto request = new UpdatePracticeDto();
        request.setReviewSites(Collections.singletonList(new CreateReviewSiteDto("my type", "data")));

        when(practiceRepository.findOne(42L)).thenReturn(practice);

        PracticeDto result = practiceService.updatePractice(42L, request);

        assertEquals("my type", result.getPreferredReviewSiteType());
    }

    @Test
    public void addDoctor_should_add_authority()
    {
        Customer customer = new Customer();
        customer.setEmail("mail");
        customer.addAuthority(AuthorityRole.ROLE_CUSTOMER);

        when(customerRepository.findOne(42L)).thenReturn(customer);

        Practice practice = spy(new Practice());
        when(practice.getId()).thenReturn(999L);
        when(practice.getOwner()).thenReturn(customer);

        when(practiceRepository.findOne(66L)).thenReturn(practice);
        practiceService.addDoctor(66L, 42L);

        Collection<? extends GrantedAuthority> authorities = customer.getAuthorities();
        Iterator<? extends GrantedAuthority> readable = authorities.iterator();

        assertEquals(3, authorities.size());
        assertEquals("ROLE_CUSTOMER", readable.next().getAuthority());
        assertEquals("ROLE_DOCTOR", readable.next().getAuthority());
        assertEquals("ROLE_PRACTICE_66", readable.next().getAuthority());
    }

    @Test
    public void addDoctor_should_add_doctor_profile_if_not_present()
    {
        Customer customer = new Customer();
        customer.setEmail("mail");
        customer.addAuthority(AuthorityRole.ROLE_CUSTOMER);

        when(customerRepository.findOne(42L)).thenReturn(customer);

        Practice practice = spy(new Practice());
        when(practice.getId()).thenReturn(999L);
        when(practice.getOwner()).thenReturn(customer);

        when(practiceRepository.findOne(66L)).thenReturn(practice);

        practiceService.addDoctor(66L, 42L);

        assertEquals(3, customer.getAuthorities().size());
        verify(profileRepository, times(1)).save(any(DoctorProfile.class));
    }

    @Test
    public void addDoctor_should_not_add_doctor_profile_if_already_present()
    {
        Customer customer = spy(new Customer());
        when(customer.getId()).thenReturn(42L);
        customer.setEmail("mail");
        customer.addAuthority(AuthorityRole.ROLE_CUSTOMER);
        when(customerRepository.findOne(42L)).thenReturn(customer);
        when(profileRepository.findOneByCustomerId(42L)).thenReturn(profile(customer));

        Practice practice = spy(new Practice());
        when(practice.getId()).thenReturn(999L);
        when(practice.getOwner()).thenReturn(customer);
        when(practiceRepository.findOne(66L)).thenReturn(practice);

        practiceService.addDoctor(66L, 42L);

        assertEquals(3, customer.getAuthorities().size());
        verify(profileRepository, never()).save(any(DoctorProfile.class));
    }

    @Test
    public void removeDoctor_should_remove_authority()
    {
        Customer customer = new Customer();
        customer.setEmail("mail");
        customer.addAuthority(AuthorityRole.ROLE_CUSTOMER);
        customer.addAuthority(AuthorityRole.ROLE_DOCTOR);
        customer.addAuthority("ROLE_PRACTICE_66");

        when(profileRepository.findOneByCustomerId(42L)).thenReturn(profile(customer));

        Practice practice = spy(new Practice());
        when(practice.getId()).thenReturn(999L);
        when(practice.getOwner()).thenReturn(customer);

        when(practiceRepository.findOne(66L)).thenReturn(practice);
        practiceService.removeDoctor(66L, 42L);

        Collection<? extends GrantedAuthority> authorities = customer.getAuthorities();
        Iterator<? extends GrantedAuthority> readable = authorities.iterator();

        assertEquals(2, authorities.size());
        assertEquals("ROLE_CUSTOMER", readable.next().getAuthority());
        assertEquals("ROLE_DOCTOR", readable.next().getAuthority());
    }

    @Test
    public void removeDoctor_should_remove_treatment_provider()
    {
        Customer customer = spy(new Customer());
        when(customer.getId()).thenReturn(42L);
        when(profileRepository.findOneByCustomerId(42L)).thenReturn(profile(customer));

        DoctorProfile profile = new DoctorProfile();
        profile.setCustomer(customer);

        TreatmentType tt = new TreatmentType(42L, "example");
        tt.getDoctors().add(profile);

        Practice practice = new Practice();
        practice.getTreatmentTypes().add(tt);
        practice.getDoctors().add(profile);

        when(practiceRepository.findOne(66L)).thenReturn(practice);
        practiceService.removeDoctor(66L, 42L);

        assertFalse(practice.getTreatmentTypes().isEmpty());
        assertTrue(practice.getTreatmentTypes().iterator().next().getDoctors().isEmpty());
    }


    public DoctorProfile profile(Customer customer) {
        DoctorProfile profile = new DoctorProfile();
        profile.setCustomer(customer);

        return profile;
    }

    public DoctorProfile profile(long id) {
        Customer customer = spy(new Customer());
        when(customer.getId()).thenReturn(id);

        return profile(customer);
    }
}
