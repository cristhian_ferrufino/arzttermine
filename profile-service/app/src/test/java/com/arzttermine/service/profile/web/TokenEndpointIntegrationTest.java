package com.arzttermine.service.profile.web;

import org.junit.Test;
import org.springframework.test.context.jdbc.Sql;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

public class TokenEndpointIntegrationTest extends BaseIntegrationTest
{
    @Test
    public void createToken_should_report_unauthorized_client() throws Exception
    {
        mockMvc.perform(post("/oauth/token"))
            .andExpect(status().isUnauthorized());
    }

    @Test
    public void createToken_should_return_unauthorized_when_not_found() throws Exception
    {
        mockMvc.perform(post("/oauth/token?client_id=example&client_secret=secret&grant_type=client_credentials"))
            .andExpect(status().isUnauthorized())
            .andExpect(jsonPath("$.error", is("unauthorized")));
    }

    @Test
    @Sql("/db/oauth_clients.sql")
    public void createToken_should_return_invalid_client_when_secret_does_not_match() throws Exception
    {
        mockMvc.perform(post("/oauth/token?client_id=client&client_secret=secret2&grant_type=client_credentials"))
            .andExpect(status().isUnauthorized())
            .andExpect(jsonPath("$.error", is("invalid_client")));
    }
}
