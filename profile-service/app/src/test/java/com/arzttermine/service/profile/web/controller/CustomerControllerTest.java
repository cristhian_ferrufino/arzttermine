package com.arzttermine.service.profile.web.controller;

import com.arzttermine.library.elasticsearch.service.DocumentProcessor;
import com.arzttermine.library.web.struct.Language;
import com.arzttermine.service.profile.web.BaseIntegrationTest;
import com.arzttermine.service.profile.web.dto.request.CreateCustomerDto;
import com.arzttermine.service.profile.web.dto.request.UpdateCustomerDto;
import com.arzttermine.service.profile.web.dto.response.CustomerDto;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import com.arzttermine.service.profile.web.struct.Gender;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.test.context.jdbc.Sql;

import java.util.UUID;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Sql("/db/empty_database.sql")
public class CustomerControllerTest extends BaseIntegrationTest
{
    @Autowired
    private DocumentProcessor documentProcessor;

    @Autowired
    private DefaultTokenServices tokenServices;

    @Before
    public void setup() {
        reset(documentProcessor);
    }

    @Test
    public void createCustomer_should_be_restricted_for_employee_system_and_clients_with_registration_scope_only() throws Exception
    {
        mockMvc.perform(post("/customers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(buildRequestDto())))
            .andExpect(status().isUnauthorized());

        mockMvc.perform(scopedRequest(() -> post("/customers"), new String[]{"ROLE_CLIENT"}, "default")
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(buildRequestDto())))
            .andExpect(status().isForbidden());

        mockMvc.perform(scopedRequest(() -> post("/customers"), new String[]{"ROLE_CLIENT"}, "default", "registration")
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(buildRequestDto())))
            .andExpect(status().is2xxSuccessful());

        mockMvc.perform(scopedRequest(() -> post("/customers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(buildRequestDto())),
            new String[]{"ROLE_CLIENT", "ROLE_CUSTOMER"}, "default"))
            .andExpect(status().isForbidden());
    }

    @Test
    public void createCustomer_should_create_new_customer() throws Exception
    {
        mockMvc.perform(systemRequest(() -> post("/customers"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(buildRequestDto())))
            .andExpect(status().isCreated());
    }

    @Test
    public void createCustomer_with_a_title_should_create_an_entitled_customer() throws Exception
    {
        mockMvc.perform(systemRequest(() -> post("/customers"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(buildRequestDto())))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.title", is("Greatest Doctor To Have Ever Doctored A Doctorate")));

    }

    @Test
    public void updateCustomer_should_update_customer_properties() throws Exception
    {
        CustomerDto customer = objectMapper.readValue(mockMvc.perform(systemRequest(() -> post("/customers"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(buildRequestDto("my-current@mail.de"))))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), CustomerDto.class);

        assertEquals("my-current@mail.de", customer.getEmail());

        UpdateCustomerDto update = new UpdateCustomerDto();
        // english is not present, previous preferred language is german. hence we will have two languages
        update.setPreferredLanguage(Language.ENGLISH);
        update.setFirstName("new name");
        update.setEmail("my-new@mail.de");
        update.setTitle("Dr");

        mockMvc.perform(systemRequest(() -> put("/customers/" + customer.getId()))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(update)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.email", is("my-new@mail.de")))
            .andExpect(jsonPath("$.firstName", is("new name")))
            .andExpect(jsonPath("$.lastName", is(customer.getLastName())))
            .andExpect(jsonPath("$.preferredLanguage", is(Language.ENGLISH.name())))
            .andExpect(jsonPath("$.languages[*]", containsInAnyOrder(Language.GERMAN.name(), Language.ENGLISH.name())))
            .andExpect(jsonPath("$.title", is("Dr")));
    }

    @Test
    public void updateCustomer_should_report_existing_mail() throws Exception
    {
        mockMvc.perform(systemRequest(() -> post("/customers"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(buildRequestDto("existing@one.de"))))
            .andExpect(status().isCreated());

        long customerId = objectMapper.readValue(mockMvc.perform(systemRequest(() -> post("/customers"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(buildRequestDto("my-current@mail.de"))))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), CustomerDto.class)
            .getId();

        UpdateCustomerDto update = new UpdateCustomerDto();
        update.setEmail("existing@one.de");

        mockMvc.perform(systemRequest(() -> put("/customers/" + customerId))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(update)))
            .andExpect(status().isConflict())
            .andExpect(jsonPath("$[0].code", is("ALREADY_EXISTS")))
            .andExpect(jsonPath("$[0].reference", is("existing@one.de")));
    }

    @Test
    @Sql("/db/test_doctors.sql")
    public void updateCustomer_should_be_restricted_for_employees_and_system_only() throws Exception
    {
        UpdateCustomerDto update = new UpdateCustomerDto();

        mockMvc.perform(put("/customers/42")
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(update)))
            .andExpect(status().isUnauthorized());

        mockMvc.perform(customerRequest(45L, () -> put("/customers/99"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(update)))
            .andExpect(status().isForbidden());
    }

    @Test
    public void createCustomer_should_report_constraint_violation_on_email() throws Exception
    {
        CreateCustomerDto request = buildRequestDto();

        mockMvc.perform(systemRequest(() -> post("/customers"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated());

        request.setPassword(null);

        mockMvc.perform(systemRequest(() -> post("/customers"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[0].code", is("INVALID_ARGUMENT")))
            .andExpect(jsonPath("$[0].reference", is("password")));
    }

    @Test
    public void getCustomer_should_return_customer() throws Exception
    {
        CreateCustomerDto request = buildRequestDto();

        long customerId = objectMapper.readValue(mockMvc.perform(systemRequest(() -> post("/customers"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), CustomerDto.class)
            .getId();

        mockMvc.perform(employeeRequest(42L, () -> get("/customers/" + customerId)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id", is((int) customerId)))
            .andExpect(jsonPath("$.email", is(request.getEmail())));
    }

    @Test
    public void createCustomer_should_return_existing_customer_if_there_is_no_password() throws Exception
    {
        CreateCustomerDto request = buildRequestDto();
        request.setPassword(null);

        String createdJson = mockMvc.perform(systemRequest(() -> post("/customers"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString();

        String returnedJson = mockMvc.perform(systemRequest(() -> post("/customers"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString();

        assertEquals(createdJson, returnedJson);
    }

    @Test
    public void createCustomer_should_return_existing_customer_if_password_matches() throws Exception
    {
        CreateCustomerDto request = buildRequestDto();
        request.setPassword("my-random-password");

        String createdJson = mockMvc.perform(systemRequest(() -> post("/customers"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString();

        String returnedJson = mockMvc.perform(systemRequest(() -> post("/customers"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString();

        mockMvc.perform(systemRequest(() -> get("/customers")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.page.totalItems", is(1)));

        assertEquals(createdJson, returnedJson);
    }

    @Test
    public void createCustomer_should_reject_access_with_invalid_password() throws Exception
    {
        CreateCustomerDto request = buildRequestDto();
        request.setPassword("my-random-password");

        mockMvc.perform(systemRequest(() -> post("/customers"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated());

        request.setPassword("different one");

        mockMvc.perform(systemRequest(() -> post("/customers"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isForbidden())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$[0].code", is("ACCESS_DENIED")));
    }

    @Test
    public void getCustomer_should_return_authenticated_customer() throws Exception
    {
        long id = objectMapper.readValue(mockMvc.perform(systemRequest(() -> post("/customers"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(buildRequestDto("test@example.com"))))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), CustomerDto.class)
            .getId();

        mockMvc.perform(customerRequest(id, () -> get("/customers/me")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id", is((int) id)))
            .andExpect(jsonPath("$.roles[*]", containsInAnyOrder("ROLE_CUSTOMER")));

        mockMvc.perform(customerRequest(99, () -> get("/customers/me")))
            .andExpect(status().isNotFound());
    }

    @Test
    public void getCustomers_should_sort_and_filter_customers() throws Exception
    {
        mockMvc.perform(systemRequest(() -> post("/customers"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(buildRequestDto("example+1@example.com"))))
            .andExpect(status().isCreated());

        mockMvc.perform(systemRequest(() -> post("/customers"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(buildRequestDto("example+3@example.com"))))
            .andExpect(status().isCreated());

        mockMvc.perform(systemRequest(() -> post("/customers"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(buildRequestDto("example+2@example.com"))))
            .andExpect(status().isCreated());

        mockMvc.perform(customerRequest(42L, () -> get("/customers")))
            .andExpect(status().isForbidden());

        mockMvc.perform(clientRequest(() -> get("/customers")))
            .andExpect(status().isForbidden());

        mockMvc.perform(systemRequest(() -> get("/customers?sort=createdAt,desc&size=2&firstName=example")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.page.totalItems", is(3)))
            .andExpect(jsonPath("$.page.currentPage", is(0)))
            .andExpect(jsonPath("$.elements[*]", hasSize(2)))
            .andExpect(jsonPath("$.elements[0].email", is("example+2@example.com")))
            .andExpect(jsonPath("$.elements[1].email", is("example+3@example.com")));
    }

    @Test
    public void updateDoctor_Should_Update_Practice_Index()  throws Exception{

        final long practiceId = objectMapper.readValue(mockMvc.perform(customerRequest(66L, () -> post("/practices"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(PracticeControllerTest.buildRequestDto(25, 25))))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(), PracticeDto.class)
                .getId();

        final CustomerDto aDoctor = objectMapper.readValue(mockMvc.perform(systemRequest(() -> post("/customers"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(CustomerControllerTest.buildRequestDto())))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(), CustomerDto.class);

        mockMvc.perform(practiceRequest(practiceId, 66L, () -> post("/practices/" + practiceId + "/doctors/" + aDoctor.getId())))
                .andExpect(status().isNoContent());

        UpdateCustomerDto update = new UpdateCustomerDto();
        update.setPreferredLanguage(Language.ENGLISH);
        update.setFirstName("new name");
        update.setEmail("my-new@mail.de");
        update.setTitle("Dr");

        mockMvc.perform(customerRequest(aDoctor.getId(), () -> put("/customers/" + aDoctor.getId()))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(update)))
                .andExpect(status().isOk());
        Thread.sleep(23);
        verify(documentProcessor, times(1)).index(eq("practices"), any(), any());

    }

    @Test
    public void updatePatient_Should_Not_Update_Practice_Index() throws Exception
    {
        CustomerDto customer = objectMapper.readValue(mockMvc.perform(systemRequest(() -> post("/customers"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(buildRequestDto("my-current@mail.de"))))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(), CustomerDto.class);

        assertEquals("my-current@mail.de", customer.getEmail());

        UpdateCustomerDto update = new UpdateCustomerDto();
        update.setPreferredLanguage(Language.ENGLISH);
        update.setFirstName("new name");
        update.setEmail("my-new@mail.de");
        update.setTitle("Dr");

        mockMvc.perform(systemRequest(() -> put("/customers/" + customer.getId()))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(update)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.email", is("my-new@mail.de")))
                .andExpect(jsonPath("$.firstName", is("new name")))
                .andExpect(jsonPath("$.lastName", is(customer.getLastName())))
                .andExpect(jsonPath("$.preferredLanguage", is(Language.ENGLISH.name())))
                .andExpect(jsonPath("$.title", is("Dr")));


        verify(documentProcessor, times(0)).index(eq("practices"), any(), any());
    }

    @Test
    @Sql("/db/oauth_clients.sql")
    public void updateCustomer_should_update_password() throws Exception
    {
        CustomerDto customer = objectMapper.readValue(mockMvc.perform(systemRequest(() -> post("/customers"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(buildRequestDto("my-current@mail.de"))))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), CustomerDto.class);

        UpdateCustomerDto update = new UpdateCustomerDto();
        update.setPassword("new-password");

        mockMvc.perform(systemRequest(() -> put("/customers/" + customer.getId()))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(update)))
            .andExpect(status().isBadRequest());

        update.setRepeatPassword("this is wrong");
        update.setEmail("my-current@mail.de");

        mockMvc.perform(systemRequest(() -> put("/customers/" + customer.getId()))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(update)))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[0].code", is("INVALID_ARGUMENT")))
            .andExpect(jsonPath("$[0].reference", is("passwordsEquals")));

        update.setPassword("new-password");
        update.setRepeatPassword("new-password");

        mockMvc.perform(systemRequest(() -> put("/customers/" + customer.getId()))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(update)))
            .andExpect(status().isOk());

        String tokenUuid = UUID.randomUUID().toString();
        OAuth2AccessToken token = new DefaultOAuth2AccessToken(tokenUuid);
        when(tokenServices.createAccessToken(any())).thenReturn(token);

        mockMvc.perform(post("/oauth/token?client_id=client&client_secret=secret&grant_type=password&username=my-current@mail.de&password=new-password"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.access_token", is(tokenUuid)));
    }

    public static CreateCustomerDto buildRequestDto(String email)
    {
        CreateCustomerDto request = new CreateCustomerDto();
        request.setPassword("thispasswordissostrong");
        request.setFirstName("example");
        request.setLastName("example");
        request.setTitle("Greatest Doctor To Have Ever Doctored A Doctorate");
        request.setGender(Gender.MALE);
        request.setEmail(email);

        return request;
    }

    public static CreateCustomerDto buildRequestDto()
    {
        return buildRequestDto("a@valid.mail");
    }
}
