package com.arzttermine.service.profile.service;

import com.arzttermine.library.suport.exception.EntityNotFoundException;
import com.arzttermine.service.profile.domain.entity.Practice;
import com.arzttermine.service.profile.domain.entity.TreatmentType;
import com.arzttermine.service.profile.domain.repository.PracticeRepository;
import com.arzttermine.service.profile.domain.repository.TreatmentTypeRepository;
import com.arzttermine.service.profile.web.dto.request.CreateTreatmentTypeDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.ApplicationEventPublisher;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TreatmentTypeServiceTest
{
    @Mock
    private ApplicationEventPublisher eventPublisher;

    @Mock
    private TreatmentTypeRepository treatmentRepository;

    @Mock
    private PracticeRepository practiceRepository;

    @InjectMocks
    private TreatmentTypeService treatmentService;

    @Before
    public void setup() {
        reset(eventPublisher, practiceRepository);
        Practice practice = spy(new Practice());
        when(practice.getId()).thenReturn(42L);
        practice.setName("practice");
        practice.getTreatmentTypes().add(new TreatmentType(42L, "treat me"));
        when(practiceRepository.findOne(42L)).thenReturn(practice);
    }

    @Test
    public void update_should_update_treatment_as_patch()
    {
        TreatmentType treatmentType = new TreatmentType(42L, "treatment type");
        treatmentType.setCharge(1000);

        CreateTreatmentTypeDto patch = new CreateTreatmentTypeDto();
        patch.setComment("update the comment!");
        patch.setType("treatment type");
        patch.setForcePay(true);

        when(treatmentRepository.findOne(99L)).thenReturn(treatmentType);
        treatmentService.update(42L, 99L, patch);

        assertTrue(treatmentType.isForcePay());
        assertEquals("update the comment!", treatmentType.getComment());
        assertEquals("treatment type", treatmentType.getType());
        assertEquals(42L, treatmentType.getPracticeId());
        verify(eventPublisher, times(2)).publishEvent(any(Object.class));
    }

    @Test(expected = EntityNotFoundException.class)
    public void update_should_report_invalid_practice()
    {
        TreatmentType treatmentType = new TreatmentType(55L, "treatment type");
        treatmentType.setCharge(1000);

        when(treatmentRepository.findOne(99L)).thenReturn(treatmentType);

        try {
            treatmentService.update(42L, 99L, new CreateTreatmentTypeDto());
        } catch (EntityNotFoundException e) {
            assertEquals("NOT_FOUND", e.getErrorDto().getCode());
            assertEquals(99L, e.getErrorDto().getReference());
            verify(eventPublisher, never()).publishEvent(any());
            throw e;
        }
    }
}
