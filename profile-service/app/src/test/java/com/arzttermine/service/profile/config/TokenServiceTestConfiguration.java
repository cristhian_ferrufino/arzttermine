package com.arzttermine.service.profile.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;

import static org.mockito.Mockito.*;

@Configuration
@Profile("testing")
public class TokenServiceTestConfiguration
{
    @Bean
    @Primary
    public DefaultTokenServices defaultTokenServices()
    {
        return mock(DefaultTokenServices.class);
    }

    @Bean
    public TokenStore tokenStore()
    {
        return new InMemoryTokenStore();
    }
}
