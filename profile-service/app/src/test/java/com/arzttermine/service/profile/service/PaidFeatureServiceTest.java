package com.arzttermine.service.profile.service;

import com.arzttermine.library.suport.exception.EntityNotFoundException;
import com.arzttermine.service.profile.domain.entity.*;
import com.arzttermine.service.profile.domain.repository.CustomerRepository;
import com.arzttermine.service.profile.domain.repository.PaidFeatureRepository;
import com.arzttermine.service.profile.domain.repository.PracticeRepository;
import com.arzttermine.service.profile.web.dto.request.CreatePracticeDto;
import com.arzttermine.service.profile.web.dto.request.UpdatePaidFeatureAssignmentDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PaidFeatureServiceTest {

    @Mock
    private PaidFeatureRepository paidFeatureRepository;

    @Mock
    private PracticeRepository practiceRepository;

    @Mock
    private CustomerRepository customerRepository;

    @Mock
    private PracticeService practiceService;

    @Mock
    private ApplicationEventPublisher eventPublisher;

    @InjectMocks
    private PaidFeatureService paidFeatureService;

    private PaidFeature dummyFeature;
    private Map<String, String> dummyFeatureOptions;

    @Before
    public void setUp() {
        dummyFeature = spy(new PaidFeature());
        when(dummyFeature.getId()).thenReturn(1L);
        dummyFeature.setName("dummy_paid_feature");
        when(paidFeatureRepository.findOne(1L)).thenReturn(dummyFeature);
        when(paidFeatureRepository.findByNameIn(new HashSet<>(Arrays.asList("dummy_paid_feature")))).thenReturn(new HashSet<PaidFeature>(Arrays.asList(dummyFeature)));

        dummyFeatureOptions = new HashMap<String, String>(){{
            put("option1","value1");
        }};

        Customer customer = spy(new Customer());
        when(customer.getId()).thenReturn(42L);
        customer.setEmail("mail");
        when(customerRepository.findAll(any(Iterable.class))).thenReturn(Collections.emptyList());
        when(customerRepository.findOne(42L)).thenReturn(customer);
        OAuth2Authentication auth = mock(OAuth2Authentication.class);
        when(auth.getAuthorities()).thenReturn(Collections.singletonList(new SimpleGrantedAuthority("ROLE_CLIENT")));
        when(auth.getPrincipal()).thenReturn(42L);
        CreatePracticeDto request = new CreatePracticeDto();
        request.setOwnerId(66L);
        practiceService.createPractice(request, auth);
    }

    @Test
    public void add_paid_feature_to_practice_should_add_paid_feature()
    {
        Practice practice = practice();

        paidFeatureService.addPaidFeatureToPractice(practice, 1l, dummyFeatureOptions);

        assertEquals(1, practice.getPaidFeatureAssignments().size());
        PaidFeatureAssignment pfa = practice.getPaidFeatureAssignments().iterator().next();
        assertEquals(dummyFeature, pfa.getPaidFeature());
        assertEquals(1, pfa.getOptions().size());
        assertEquals("value1", pfa.getOptions().get("option1"));

        verify(eventPublisher, times(1)).publishEvent(any(Object.class));
    }

    @Test
    public void updatePractice_should_replace_paid_features()
    {
        Practice practice = practice();
        practice.assignPaidFeature(dummyFeature, dummyFeatureOptions);

        PaidFeature otherFeature = spy(new PaidFeature());
        when(otherFeature.getId()).thenReturn(2L);
        when(otherFeature.getName()).thenReturn("other_paid_feature");
        when(paidFeatureRepository.findOne(2L)).thenReturn(otherFeature);
        when(paidFeatureRepository.findByNameIn(new HashSet<>(Arrays.asList("other_paid_feature")))).thenReturn(new HashSet<PaidFeature>(Arrays.asList(otherFeature)));

        paidFeatureService.updatePaidFeaturesOnPractice(practice, paidFeatureConfig("other_paid_feature"));

        assertEquals(1, practice.getPaidFeatureAssignments().size());
        PaidFeatureAssignment pfa = practice.getPaidFeatureAssignments().iterator().next();
        assertEquals(otherFeature, pfa.getPaidFeature());
        assertEquals(0, pfa.getOptions().size());

    }

    @Test
    public void updatePractice_should_update_paid_features()
    {
        Practice practice = practice();
        practice.assignPaidFeature(dummyFeature, dummyFeatureOptions);

        PaidFeature otherFeature = spy(new PaidFeature());
        when(otherFeature.getId()).thenReturn(2L);
        when(otherFeature.getName()).thenReturn("other_paid_feature");
        when(paidFeatureRepository.findOne(2L)).thenReturn(otherFeature);
        when(paidFeatureRepository.findByNameIn(new HashSet<>(Arrays.asList("other_paid_feature",dummyFeature.getName()))))
            .thenReturn(new HashSet<PaidFeature>(Arrays.asList(otherFeature,dummyFeature)));

        PaidFeatureAssignment dummyPfa = new PaidFeatureAssignment();
        dummyPfa.setPaidFeature(dummyFeature);
        dummyPfa.setPractice(practice);
        PaidFeatureAssignment otherPfa = new PaidFeatureAssignment();
        otherPfa.setPaidFeature(otherFeature);
        otherPfa.setPractice(practice);
        otherPfa.setPractice(practice);

        Map<String, Map<String, String>> pfc = paidFeatureConfig("other_paid_feature", dummyFeature.getName());
        pfc.get(dummyFeature.getName()).putAll(dummyFeatureOptions);
        paidFeatureService.updatePaidFeaturesOnPractice(practice,pfc);

        assertEquals(2, practice.getPaidFeatureAssignments().size());
        assertTrue(practice.getPaidFeatureAssignments().contains(dummyPfa));
        assertTrue(practice.getPaidFeatureAssignments().contains(otherPfa));
        assertEquals(dummyFeatureOptions, practice.getPaidFeatureAssignments().stream()
            .filter(e -> e.getPaidFeature().equals(dummyFeature))
            .findFirst().orElse(null)
            .getOptions());

    }

    @Test
    public void updatePractice_should_clear_paid_features_if_empty_array_is_given()
    {
        Practice practice = practice();
        practice.assignPaidFeature(dummyFeature, dummyFeatureOptions);

        paidFeatureService.updatePaidFeaturesOnPractice(practice, paidFeatureConfig());
        assertTrue(practice.getPaidFeatureAssignments().isEmpty());

    }

    @Test(expected = EntityNotFoundException.class)
    public void updatePractice_should_report_invalid_paid_feature()
    {
        Practice practice = practice();

        try {
            paidFeatureService.updatePaidFeaturesOnPractice(practice, paidFeatureConfig("not_a_paid_feature"));
        }
        catch (EntityNotFoundException e) {
            assertEquals("NOT_FOUND", e.getErrorDto().getCode());
            assertEquals("not_a_paid_feature", e.getErrorDto().getReference());
            verify(eventPublisher, never()).publishEvent(any());
            throw e;
        }
    }

    public Map<String, Map<String, String>> paidFeatureConfig(String... featureNames) {
        Map<String, Map<String, String>> paidFeatureConfig = new HashMap<>();
        for (String s : featureNames) {
            paidFeatureConfig.put(s, new HashMap<>());
        }

        return paidFeatureConfig;
    }

    public Practice practice() {
        Practice practice = new Practice();
        when(practiceRepository.save(any(Practice.class))).thenReturn(practice);

        return practice;
    }

    public DoctorProfile profile(Customer customer) {
        DoctorProfile profile = new DoctorProfile();
        profile.setCustomer(customer);

        return profile;
    }

}