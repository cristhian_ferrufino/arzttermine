package com.arzttermine.service.profile.web.controller;

import com.arzttermine.library.elasticsearch.service.DocumentProcessor;
import com.arzttermine.service.profile.web.BaseIntegrationTest;
import com.arzttermine.service.profile.web.dto.LocationDto;
import com.arzttermine.service.profile.web.dto.request.CreateLocationDto;
import com.arzttermine.service.profile.web.dto.request.CreatePracticeDto;
import com.arzttermine.service.profile.web.dto.request.UpdateLocationDto;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import com.arzttermine.service.profile.web.struct.Country;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;

import java.time.Instant;

import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class LocationControllerTest extends BaseIntegrationTest {

    @Autowired
    private DocumentProcessor documentProcessor;

    @Before
    public void setup() {
        reset(documentProcessor);
    }

    @Test
    public void updateLocation_should_reject_unauthorized_requests() throws Exception {

        mockMvc.perform(post("/locations/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(new UpdateLocationDto())))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void updateLocation_should_return_not_found_if_location_is_not_found() throws Exception {

        UpdateLocationDto locationDto = new UpdateLocationDto();
        locationDto.setLocation("Location");
        locationDto.setPhone("Phone");
        locationDto.setPhoneMobile("PhoneMobile");
        locationDto.setContactMail("a@a.com");
        locationDto.setCity("City");
        locationDto.setCountry(Country.GERMANY);
        locationDto.setLatitude(123.123f);
        locationDto.setLongitude(123.123f);
        locationDto.setZip("Zip");

        mockMvc.perform(employeeRequest(42, () -> put("/locations/23")
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(locationDto))))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.[0].code", CoreMatchers.is("NOT_FOUND")));

    }

    @Test
    public void updateLocation_should_not_update_if_all_properties_null() throws Exception {

        CreatePracticeDto practice = buildPracticeRequestDto();

        LocationDto locationDto = objectMapper.readValue(mockMvc.perform(employeeRequest(42, () -> post("/practices"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(practice)))
                .andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString(), PracticeDto.class).getLocation();

        UpdateLocationDto updateLocation = new UpdateLocationDto();

        LocationDto sameLocationDto = objectMapper.readValue(mockMvc.perform(employeeRequest(42, () -> put("/locations/"+locationDto.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(updateLocation))))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), LocationDto.class);

        Assert.assertEquals(locationDto.getId(), sameLocationDto.getId());
        Assert.assertEquals(locationDto.getCity(), sameLocationDto.getCity());
        Assert.assertEquals(locationDto.getContactMail(), sameLocationDto.getContactMail());
        Assert.assertEquals(locationDto.getCountry(), sameLocationDto.getCountry());
        Assert.assertEquals(locationDto.getLatitude(), sameLocationDto.getLatitude(),0);
        Assert.assertEquals(locationDto.getLongitude(), sameLocationDto.getLongitude(),0);
        Assert.assertEquals(locationDto.getLocation(), sameLocationDto.getLocation());
        Assert.assertEquals(locationDto.getPhone(), sameLocationDto.getPhone());
        Assert.assertEquals(locationDto.getPhoneMobile(), sameLocationDto.getPhoneMobile());
        Assert.assertEquals(locationDto.getZip(), sameLocationDto.getZip());
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void updateLocation_should_update_Location() throws Exception {
        CreatePracticeDto practice = buildPracticeRequestDto();

        LocationDto locationDto = objectMapper.readValue(mockMvc.perform(employeeRequest(42, () -> post("/practices"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(practice)))
                .andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString(), PracticeDto.class).getLocation();

        UpdateLocationDto updateLocation = new UpdateLocationDto();
        updateLocation.setZip("Zippy");

        mockMvc.perform(employeeRequest(42, () -> put("/locations/"+locationDto.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(updateLocation))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.zip", CoreMatchers.is("Zippy")));
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void updateLocation_Should_Update_Practice_Index()  throws Exception{

        final PracticeDto practiceDto = objectMapper.readValue(mockMvc.perform(customerRequest(66L, () -> post("/practices"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(buildPracticeRequestDto())))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), PracticeDto.class);

        final LocationDto locationDto = practiceDto.getLocation();

        UpdateLocationDto update = new UpdateLocationDto();
        update.setLatitude(52.532388f);
        update.setLongitude(13.397630f);


        mockMvc.perform(employeeRequest(42,() -> put("/locations/" + locationDto.getId()))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(update)))
            .andExpect(status().isOk());
        Thread.sleep(100);
        verify(documentProcessor, times(1)).index(eq("practices"), any(), any());

    }

    public static CreateLocationDto buildLocationRequestDto() {
        CreateLocationDto createLocationDto = new CreateLocationDto();
        createLocationDto.setLatitude(52.525482f);
        createLocationDto.setLongitude(13.403770f);
        createLocationDto.setCountry(Country.GERMANY);
        createLocationDto.setLocation("Rosenthaler Straße 51");
        createLocationDto.setCity("Berlin");
        createLocationDto.setZip("10178");
        createLocationDto.setPhone("555-5555");
        createLocationDto.setContactMail("thadoctor@arzttermine.de");

        return createLocationDto;
    }

    public static CreatePracticeDto buildPracticeRequestDto() {
        CreatePracticeDto createPracticeDto = new CreatePracticeDto();
        createPracticeDto.setName("Dummy Practice -"+ Instant.now().toString());
        createPracticeDto.setLocation(buildLocationRequestDto());

        return createPracticeDto;
    }
}
