package com.arzttermine.service.profile.web.controller;

import com.arzttermine.library.elasticsearch.model.BulkAction;
import com.arzttermine.library.elasticsearch.service.DocumentProcessor;
import com.arzttermine.library.web.dto.PriceDto;
import com.arzttermine.library.web.struct.Currency;
import com.arzttermine.service.profile.consumer.TestConsumerCounter;
import com.arzttermine.service.profile.web.BaseIntegrationTest;
import com.arzttermine.service.profile.web.dto.request.CreateTreatmentTypeDto;
import com.arzttermine.service.profile.web.dto.request.CreatePracticeDto;
import com.arzttermine.service.profile.web.dto.response.DoctorProfileDto;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import com.arzttermine.service.profile.web.dto.response.TreatmentTypeDto;
import com.arzttermine.service.profile.web.dto.response.TreatmentTypesPage;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.util.Collections;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql({"/db/empty_database.sql", "/db/test_doctors.sql"})
public class TreatmentTypeControllerTest extends BaseIntegrationTest
{
    @Autowired
    private TestConsumerCounter testCounter;

    @Autowired
    private DocumentProcessor documentProcessor;

    private static PracticeDto practice;

    @Before
    public void setup() throws Exception {
        Thread.sleep(30); // teardown wait for messages
        testCounter.flush();

        CreatePracticeDto request = PracticeControllerTest.buildRequestDto(999, 912);
        request.setInitialDoctors(Collections.singletonList(66L));

        practice = objectMapper.readValue(mockMvc.perform(employeeRequest(42, () -> post("/practices"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andReturn()
            .getResponse()
            .getContentAsString(), PracticeDto.class);

        reset(documentProcessor);
    }

    @Test
    public void addTreatmentType_should_add_treatment_type() throws Exception
    {
        DoctorProfileDto doctor = objectMapper.readValue(mockMvc.perform(employeeRequest(42L, () -> get("/practices/" + practice.getId() + "/doctors")))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString(), DoctorProfileDto[].class)[0];

        CreateTreatmentTypeDto tt = new CreateTreatmentTypeDto();
        tt.setType("a random type");
        tt.setCharge(new PriceDto(100));
        tt.getDoctorIds().add(doctor.getId());

        mockMvc.perform(employeeRequest(42L, () -> post(url()))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(tt)))
            .andExpect(status().isCreated());

        mockMvc.perform(employeeRequest(42L, () -> get("/practices/" + practice.getId() + "/doctors")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$[0].id", is((int) doctor.getId())))
            .andExpect(jsonPath("$[0].profileId", is((int) doctor.getProfileId())))
            .andExpect(jsonPath("$[0].treatmentTypes[*]", hasSize(1)))
            .andExpect(jsonPath("$[0].treatmentTypes[0].practiceId", is((int) practice.getId())))
            .andExpect(jsonPath("$[0].treatmentTypes[0].doctorIds[*]", hasSize(1)))
            .andExpect(jsonPath("$[0].treatmentTypes[0].doctorIds[0]", is(tt.getDoctorIds().get(0).intValue())))
            .andExpect(jsonPath("$[0].treatmentTypes[0].type", is("a random type")))
            .andExpect(jsonPath("$[0].treatmentTypes[0].charge.amount", is(100)));

        Thread.sleep(50);

        verify(documentProcessor, times(1))
            .execute(eq("practices"), eq("germany"), anyCollectionOf(BulkAction.class));
    }

    @Test
    public void updateTreatmentType_should_update_treatment_type() throws Exception
    {
        CreateTreatmentTypeDto tt = new CreateTreatmentTypeDto();
        tt.setType("a random type");
        tt.setComment("temp comment");

        long id = objectMapper.readValue(mockMvc.perform(employeeRequest(42L, () -> post(url()))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(tt)))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), TreatmentTypeDto.class)
            .getId();

        tt.setComment(null);
        tt.setType("a different type");
        tt.setCharge(new PriceDto(1000));
        tt.setForcePay(true);

        mockMvc.perform(practiceRequest(practice.getId(), 42L, () -> patch(url() + "/" + id))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(tt)))
            .andExpect(status().isNoContent());

        mockMvc.perform(employeeRequest(42L, () -> get("/practices/" + practice.getId() + "/treatments/" + id)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id", is((int) id)))
            .andExpect(jsonPath("$.charge.amount", is(1000)))
            .andExpect(jsonPath("$.charge.currency", is(Currency.EUR.name())))
            .andExpect(jsonPath("$.forcePay", is(true)))
            .andExpect(jsonPath("$.type", is("a different type")))
            .andExpect(jsonPath("$.comment").doesNotExist());

        Thread.sleep(20);

        verify(documentProcessor, times(2))
            .execute(eq("practices"), eq("germany"), anyCollectionOf(BulkAction.class));
    }

    @Test
    public void addTreatmentType_should_report_existing_type() throws Exception
    {
        CreateTreatmentTypeDto tt = new CreateTreatmentTypeDto();
        tt.setType("a random type");
        tt.setCharge(new PriceDto(100));

        mockMvc.perform(employeeRequest(42L, () -> post(url()))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(tt)))
            .andExpect(status().isCreated());

        mockMvc.perform(employeeRequest(42L, () -> post(url()))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(tt)))
            .andExpect(status().isConflict())
            .andExpect(jsonPath("$[0].code", is("ALREADY_EXISTS")));
    }

    @Test
    public void removeTreatmentType_should_ignore_non_existing() throws Exception
    {
        mockMvc.perform(employeeRequest(42L, () -> delete(url() + "/99")))
            .andExpect(status().isNoContent());

        mockMvc.perform(employeeRequest(42L, () -> delete("/practices/1234567890/treatments/99")))
            .andExpect(status().isNoContent());
    }

    @Test
    public void add_and_deleteTreatmentType_should_be_restricted() throws Exception
    {
        CreateTreatmentTypeDto tt = new CreateTreatmentTypeDto();
        tt.setType("a random type");
        tt.setCharge(new PriceDto(100));

        mockMvc.perform(practiceRequest(0L, 42L, () -> post(url()))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(tt)))
            .andExpect(status().isForbidden());

        mockMvc.perform(customerRequest(42L, () -> delete(url() + "/13")))
            .andExpect(status().isForbidden());
    }

    @Test
    public void removeTreatmentType_should_remove_existing_type() throws Exception
    {
        CreateTreatmentTypeDto tt = new CreateTreatmentTypeDto();
        tt.setType("a random type");
        tt.setCharge(new PriceDto(100));
        tt.getDoctorIds().add(99L);

        mockMvc.perform(practiceRequest(practice.getId(), 42L, () -> post(url()))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(tt)))
            .andExpect(status().isCreated());

        Thread.sleep(100);

        long ttId = objectMapper.readValue(mockMvc.perform(employeeRequest(42L, () -> get(url())))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.elements[*]", hasSize(1)))
            .andExpect(jsonPath("$.elements[0].type", is(tt.getType())))
            .andReturn()
            .getResponse()
            .getContentAsString(), TreatmentTypesPage.class)
            .getElements().get(0)
            .getId();

        Assert.assertTrue(testCounter.getChanges().size() > 0);
        Assert.assertEquals(ttId, testCounter.getChanges().get(0).getTreatmentTypeId());
        Assert.assertFalse(testCounter.getChanges().get(0).isDeleted());
        testCounter.flush();

        mockMvc.perform(employeeRequest(42L, () -> delete(url() + "/" + ttId)))
            .andExpect(status().isNoContent());

        mockMvc.perform(employeeRequest(42L, () -> get(url() + "?enabled=true")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.elements[*]", hasSize(0)));

        Thread.sleep(50);

        Assert.assertTrue(testCounter.getChanges().size() > 0);
        Assert.assertEquals(ttId, testCounter.getChanges().get(0).getTreatmentTypeId());
        Assert.assertTrue(testCounter.getChanges().get(0).isDeleted());

        verify(documentProcessor, times(2))
            .execute(eq("practices"), eq("germany"), anyCollectionOf(BulkAction.class));
    }

    @Test
    public void removeTreatmentType_should_disable_existing_type_and_remove_it_from_practice() throws Exception
    {
        CreateTreatmentTypeDto tt = new CreateTreatmentTypeDto();
        tt.setType("a random type");
        tt.setCharge(new PriceDto(100));
        tt.getDoctorIds().add(99L);

        mockMvc.perform(practiceRequest(practice.getId(), 42L, () -> post(url()))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(tt)))
            .andExpect(status().isCreated());

        long ttId = objectMapper.readValue(mockMvc.perform(employeeRequest(42L, () -> get(url())))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.elements[*]", hasSize(1)))
            .andExpect(jsonPath("$.elements[0].type", is(tt.getType())))
            .andExpect(jsonPath("$.elements[0].enabled", is(true)))
            .andReturn()
            .getResponse()
            .getContentAsString(), TreatmentTypesPage.class)
            .getElements().get(0)
            .getId();

        mockMvc.perform(employeeRequest(42L, () -> delete(url() + "/" + ttId)))
            .andExpect(status().isNoContent());

        mockMvc.perform(employeeRequest(42L, () -> get(url() + "?enabled=true")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.elements[*]", hasSize(0)));

        // not there anymore in the practice
        mockMvc.perform(employeeRequest(42L, () -> get("/practices/" + practice.getId())))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.treatmentTypes[*]", hasSize(0)));

        // should still be available for id requests
        mockMvc.perform(employeeRequest(42L, () -> get(url() + "/" + ttId)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.type", is(tt.getType())))
            .andExpect(jsonPath("$.enabled", is(false)))
            .andExpect(jsonPath("$.practiceId", is((int) practice.getId())));
    }

    @Test
    public void treatmentType_returns_filtered_results() throws Exception
    {
        CreateTreatmentTypeDto tt = new CreateTreatmentTypeDto();
        tt.setType("a random type");
        tt.setCharge(new PriceDto(100));
        tt.getDoctorIds().add(99L);
        CreateTreatmentTypeDto tt2 = new CreateTreatmentTypeDto();
        tt2.setType("anothertype");
        tt2.setCharge(new PriceDto(110));
        tt2.getDoctorIds().add(99L);

        mockMvc.perform(practiceRequest(practice.getId(), 42L, () -> post(url()))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(tt)))
            .andExpect(status().isCreated());

        mockMvc.perform(practiceRequest(practice.getId(), 42L, () -> post(url()))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(tt2)))
            .andExpect(status().isCreated());

        mockMvc.perform(employeeRequest(42L, () -> get(url())))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.elements[*]", hasSize(2)));

        mockMvc.perform(employeeRequest(42L, () -> get(url() + "?type="+tt2.getType())))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.elements[*]", hasSize(1)))
            .andExpect(jsonPath("$.elements[0].type", is(tt2.getType())));

    }

    private String url() {
        return String.format("/practices/%d/treatments", practice.getId());
    }
}
