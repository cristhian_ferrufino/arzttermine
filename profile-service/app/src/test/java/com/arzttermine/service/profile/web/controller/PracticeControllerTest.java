package com.arzttermine.service.profile.web.controller;

import com.arzttermine.library.web.struct.InsuranceType;
import com.arzttermine.service.profile.web.BaseIntegrationTest;
import com.arzttermine.service.profile.web.dto.WidgetConfigurationDto;
import com.arzttermine.service.profile.web.dto.WidgetSuccessBehaviourDto;
import com.arzttermine.service.profile.web.dto.request.*;
import com.arzttermine.service.profile.web.dto.request.UpdatePaidFeatureAssignmentDto;
import com.arzttermine.service.profile.web.dto.response.CustomerDto;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import com.arzttermine.service.profile.web.dto.response.TreatmentTypeDto;
import com.arzttermine.service.profile.web.struct.MedicalSpecialtyType;
import com.google.common.collect.Sets;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;

import java.util.*;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class PracticeControllerTest extends BaseIntegrationTest
{
    @Test
    public void createPractice_should_be_restricted_for_customer_employees_and_system_only() throws Exception
    {
        CreatePracticeDto request = buildRequestDto();

        mockMvc.perform(post("/practices")
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(request)))
                .andExpect(status().isUnauthorized());

        mockMvc.perform(clientRequest(() -> post("/practices"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(request)))
                .andExpect(status().isForbidden());
    }

    @Test
    @Sql({"/db/empty_database.sql", "/db/test_doctors.sql"})
    public void createPractice_should_create_practice() throws Exception
    {
        CreatePracticeDto request = buildRequestDto();
        request.setOwnerId(42L); // should be ignored as the customer is not able to select an owner by himself

        mockMvc.perform(customerRequest(66L, () -> post("/practices"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(request)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.name", is(request.getName())))
                .andExpect(jsonPath("$.medicalSpecialties").isEmpty())
                .andExpect(jsonPath("$.location.location", is(request.getLocation().getLocation())))
                .andExpect(jsonPath("$.location.zip", is(request.getLocation().getZip())))
                .andExpect(jsonPath("$.location.latitude").value(request.getLocation().getLatitude()))
                .andExpect(jsonPath("$.location.longitude").value(request.getLocation().getLongitude()))
                .andExpect(jsonPath("$.ownerId", is(66)));
    }

    @Test
    @Sql({"/db/empty_database.sql", "/db/test_doctors.sql"})
    public void createPractice_should_create_practice_with_provided_owner() throws Exception
    {
        CreatePracticeDto request = buildRequestDto();
        request.setOwnerId(66L); // use doctor from sql, an employee can select a custom owner

        mockMvc.perform(employeeRequest(142L, () -> post("/practices"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(request)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.ownerId", is(66)));
    }

    @Test
    @Sql({"/db/empty_database.sql", "/db/test_doctors.sql"})
    public void createPractice_should_add_initial_doctors() throws Exception
    {
        final CustomerDto initialDoctor = objectMapper.readValue(mockMvc.perform(systemRequest(() -> post("/customers"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(CustomerControllerTest.buildRequestDto())))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(), CustomerDto.class);

        WidgetConfigurationDto config = new WidgetConfigurationDto();
        config.setSuccess(new WidgetSuccessBehaviourDto("Nice one! Appointment booked successful. You are amazing!", null));
        config.setTemplateId("orange");

        CreatePracticeDto request = buildRequestDto();
        request.setInitialDoctors(Collections.singletonList(initialDoctor.getId()));
        request.setWidgetConfiguration(config);

        mockMvc.perform(customerRequest(66L, () -> post("/practices"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(request)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.ownerId", is(66)))
                .andExpect(jsonPath("$.doctors[*]", hasSize(2)))
                .andExpect(jsonPath("$.doctors[*].id", containsInAnyOrder(66, (int) initialDoctor.getId())))
                .andExpect(jsonPath("$.doctors[*].bookingTimeOffset", containsInAnyOrder(1, 1)))
                .andExpect(jsonPath("$.doctors[*].bookable", containsInAnyOrder(true, true)))
                .andExpect(jsonPath("$.widgetConfiguration.templateId", is("orange")))
                .andExpect(jsonPath("$.widgetConfiguration.success.text", is("Nice one! Appointment booked successful. You are amazing!")));
    }

    @Test
    @Sql({"/db/empty_database.sql", "/db/test_doctors.sql"})
    public void createPractice_should_add_review_sites_and_set_preferred() throws Exception
    {
        CreatePracticeDto request = buildRequestDto();
        request.setOwnerId(42L);
        request = addReviewSiteToDto(request, "jameda", "yaddayaddayaddayadda");
        request = addReviewSiteToDto(request, "google", "blahblahblahblahblah");
        request.setPreferredReviewSiteType("google");

        mockMvc.perform(customerRequest(66L, () -> post("/practices"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(request)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.reviewSites", hasSize(2)))
                .andExpect(jsonPath("$.reviewSites[*].type", containsInAnyOrder("google", "jameda")))
                .andExpect(jsonPath("$.reviewSites[*].data", containsInAnyOrder("blahblahblahblahblah","yaddayaddayaddayadda" )))
                .andExpect(jsonPath("$.preferredReviewSiteType", is("google")));
    }

    @Test
    @Sql({"/db/empty_database.sql", "/db/test_doctors.sql"})
    public void createPractice_should_add_review_sites_and_set_preferred_when_its_not_specified() throws Exception
    {
        CreatePracticeDto request = buildRequestDto();
        request.setOwnerId(42L);
        request = addReviewSiteToDto(request, "jameda", "yaddayaddayaddayadda");
        request = addReviewSiteToDto(request, "google", "blahblahblahblahblah");

        mockMvc.perform(customerRequest(66L, () -> post("/practices"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(request)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.reviewSites", hasSize(2)))
                .andExpect(jsonPath("$.reviewSites[*].type", containsInAnyOrder("google", "jameda")))
                .andExpect(jsonPath("$.reviewSites[*].data", containsInAnyOrder("blahblahblahblahblah","yaddayaddayaddayadda" )))
                .andExpect(jsonPath("$.preferredReviewSiteType", anyOf(is("google"), is("jameda"))));
    }

    @Test
    @Sql({"/db/empty_database.sql", "/db/test_doctors.sql"})
    public void createPractice_should_not_add_review_sites() throws Exception
    {
        CreatePracticeDto request = buildRequestDto();
        request.setOwnerId(42L);

        mockMvc.perform(customerRequest(66L, () -> post("/practices"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(request)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.reviewSites", hasSize(0)))
                .andExpect(jsonPath("$.preferredReviewSiteType").doesNotExist());
    }

    @Test
    @Sql({"/db/empty_database.sql", "/db/test_doctors.sql"})
    public void createPractice_should_add_all_insurance_types_by_default() throws Exception
    {
        CreatePracticeDto request = buildRequestDto();
        request.setOwnerId(42L);

        mockMvc.perform(customerRequest(66L, () -> post("/practices"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.id").exists())
            .andExpect(jsonPath("$.insuranceTypes", hasSize(InsuranceType.values().length)));
    }

    @Test
    @Sql({"/db/empty_database.sql", "/db/test_doctors.sql"})
    public void createPractice_should_add_insurance_types() throws Exception
    {
        CreatePracticeDto request = buildRequestDto();
        request.setOwnerId(42L);
        request.setInsuranceTypes(new ArrayList<>());
        request.getInsuranceTypes().add(InsuranceType.PRIVATE);

        mockMvc.perform(customerRequest(66L, () -> post("/practices"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.id").exists())
            .andExpect(jsonPath("$.insuranceTypes", hasSize(1)))
            .andExpect(jsonPath("$.insuranceTypes[*]", contains("PRIVATE")));
    }

    @Test
    @Sql({"/db/empty_database.sql"})
    public void updatePractice_should_update_insurance_types() throws Exception
    {
        CreatePracticeDto request = buildRequestDto();
        request.setOwnerId(42L);
        request.setInsuranceTypes(new ArrayList<>());
        request.getInsuranceTypes().add(InsuranceType.COMPULSORY);

        final long practiceId = objectMapper.readValue(mockMvc.perform(employeeRequest(99L, () -> post("/practices"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), PracticeDto.class)
            .getId();

        UpdatePracticeDto updateRequest = buildUpdateRequestDto();
        updateRequest.setInsuranceTypes(new ArrayList<>());
        updateRequest.getInsuranceTypes().add(InsuranceType.PRIVATE);

        mockMvc.perform(employeeRequest(99l, () -> put("/practices/"+practiceId))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(updateRequest)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id", is((int) practiceId)))
            .andExpect(jsonPath("$.insuranceTypes", hasSize(1)))
            .andExpect(jsonPath("$.insuranceTypes[*]", contains("PRIVATE")));
    }

    @Test
    @Sql({"/db/empty_database.sql"})
    public void updatePractice_should_update_review_sites() throws Exception
    {
        CreatePracticeDto request = buildRequestDto();
        request.setOwnerId(42L);
        request = addReviewSiteToDto(request, "jameda", "yaddayaddayaddayadda");

        final long practiceId = objectMapper.readValue(mockMvc.perform(employeeRequest(99L, () -> post("/practices"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(request)))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(), PracticeDto.class)
                .getId();

        UpdatePracticeDto updateRequest = buildUpdateRequestDto();

        mockMvc.perform(employeeRequest(99l, () -> put("/practices/"+practiceId))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(updateRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is((int) practiceId)))
                .andExpect(jsonPath("$.reviewSites[*]", hasSize(2)))
                .andExpect(jsonPath("$.reviewSites[*].type", containsInAnyOrder("google", "jameda")))
                .andExpect(jsonPath("$.reviewSites[*].data", containsInAnyOrder("google_place_data", "jameda_data" )));
    }

    @Test
    @Sql({"/db/empty_database.sql", "/db/test_paid_features.sql"})
    public void updatePractice_should_update_paid_features() throws Exception
    {
        CreatePracticeDto request = buildRequestDto();
        request.setPaidFeatures(constructPaidFeatures("sms_appointment_notifications", "online_payments"));
        request.getPaidFeatures().get("sms_appointment_notifications").put("option1","value1");
        request.getPaidFeatures().get("sms_appointment_notifications").put("option2","value2");

        final long practiceId = objectMapper.readValue(mockMvc.perform(employeeRequest(99L, () -> post("/practices"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(request)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.paidFeatures[*]", hasSize(2)))
                .andExpect(jsonPath("$.paidFeatures[*].paidFeatureId", containsInAnyOrder(1, 2)))
                .andExpect(jsonPath("$.paidFeatures[*].name", containsInAnyOrder("sms_appointment_notifications", "online_payments")))
                .andExpect(jsonPath("$.paidFeatures[*].options.option1", hasItem("value1")))
                .andExpect(jsonPath("$.paidFeatures[*].options.option2", hasItem("value2")))
                .andReturn()
                .getResponse()
                .getContentAsString(), PracticeDto.class)
                .getId();

        UpdatePracticeDto updateRequest = buildUpdateRequestDto();
        updateRequest.setPaidFeatures(constructPaidFeatures("online_payments", "sms_review_notifications"));
        updateRequest.getPaidFeatures().get("online_payments").put("option1","othervalue");


        mockMvc.perform(employeeRequest(99L, () -> put("/practices/" + practiceId))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(updateRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is((int) practiceId)))
                .andExpect(jsonPath("$.paidFeatures[*]", hasSize(2)))
                .andExpect(jsonPath("$.paidFeatures[*].paidFeatureId", containsInAnyOrder(2, 4)))
                .andExpect(jsonPath("$.paidFeatures[*].options.option1", hasItem("othervalue")))
                .andExpect(jsonPath("$.paidFeatures[*].options.option1", not(hasItem("value1"))))
                .andExpect(jsonPath("$.paidFeatures[*].options.option2", not(hasItem("value2"))));
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void getPractice_should_return_practice() throws Exception
    {
        CreatePracticeDto request = buildRequestDto();

        final long practiceId = objectMapper.readValue(mockMvc.perform(employeeRequest(99L, () -> post("/practices"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(request)))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(), PracticeDto.class)
                .getId();

        mockMvc.perform(clientRequest(() -> get("/practices/" + practiceId)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is((int) practiceId)))
                .andExpect(jsonPath("$.name", is(request.getName())));
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void getPractice_should_return_practice_with_specialties() throws Exception
    {
        CreatePracticeDto request = buildRequestDto();

        final long practiceId = objectMapper.readValue(mockMvc.perform(employeeRequest(99L, () -> post("/practices"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(request)))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(), PracticeDto.class)
                .getId();

        final CustomerDto anotherDoctor = objectMapper.readValue(mockMvc.perform(systemRequest(() -> post("/customers"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(CustomerControllerTest.buildRequestDto("another@one.doctor"))))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(), CustomerDto.class);

        mockMvc.perform(systemRequest(() -> post("/practices/" + practiceId + "/doctors/" + anotherDoctor.getId())))
                .andExpect(status().isNoContent());

        UpdateDoctorDto update = new UpdateDoctorDto();
        update.setMedicalSpecialties(Sets.newHashSet(MedicalSpecialtyType.ALLERGY_AND_IMMUNOLOGY, MedicalSpecialtyType.CARDIOLOGY));

        mockMvc.perform(practiceRequest(practiceId, anotherDoctor.getId(), () -> put("/practices/" + practiceId + "/doctors/" + anotherDoctor.getId()))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(update)))
                .andExpect(status().isNoContent());

        mockMvc.perform(clientRequest(() -> get("/practices/" + practiceId)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is((int) practiceId)))
                .andExpect(jsonPath("$.name", is(request.getName())))
                .andExpect(jsonPath("$.medicalSpecialties[*]", hasSize(2)))
                .andExpect(jsonPath("$.medicalSpecialties[*]",
                        containsInAnyOrder(MedicalSpecialtyType.ALLERGY_AND_IMMUNOLOGY.name(), MedicalSpecialtyType.CARDIOLOGY.name())));
    }

    @Test
    @Sql({"/db/empty_database.sql", "/db/test_doctors.sql"})
    public void getPractices_should_sort_and_filter_practices() throws Exception
    {
        mockMvc.perform(employeeRequest(99L, () -> post("/practices"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(buildRequestDto(22, 22))))
                .andExpect(status().isCreated());

        mockMvc.perform(customerRequest(66L, () -> post("/practices"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(buildRequestDto(21, 21))))
                .andExpect(status().isCreated());

        mockMvc.perform(customerRequest(66L, () -> post("/practices"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(buildRequestDto(23, 23))))
                .andExpect(status().isCreated());

        mockMvc.perform(clientRequest(() -> get("/practices?sort=location.longitude,desc")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.page.totalItems", is(3)))
                .andExpect(jsonPath("$.page.totalPages", is(1)))
                .andExpect(jsonPath("$.page.currentPage", is(0)))
                .andExpect(jsonPath("$.elements[*]", hasSize(3)))
                .andExpect(jsonPath("$.elements[0].location.longitude", is(23.0)))
                .andExpect(jsonPath("$.elements[1].location.longitude", is(22.0)))
                .andExpect(jsonPath("$.elements[2].location.longitude", is(21.0)));

        // only 2 practices are eligible (owned by customer with id 66)
        mockMvc.perform(clientRequest(() -> get("/practices?sort=location.longitude&owner.id=66")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.page.totalItems", is(2)))
                .andExpect(jsonPath("$.page.totalPages", is(1)))
                .andExpect(jsonPath("$.page.currentPage", is(0)))
                .andExpect(jsonPath("$.elements[*]", hasSize(2)))
                .andExpect(jsonPath("$.elements[0].location.longitude", is(21.0)))
                .andExpect(jsonPath("$.elements[1].location.longitude", is(23.0)));
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void addDoctorToPractice_should_add_doctor_and_authority_to_customer() throws Exception
    {
        final long owningDoctorId = objectMapper.readValue(mockMvc.perform(systemRequest(() -> post("/customers"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(CustomerControllerTest.buildRequestDto())))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(), CustomerDto.class)
                .getId();

        final long practiceId = objectMapper.readValue(mockMvc.perform(customerRequest(owningDoctorId, () -> post("/practices"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(buildRequestDto(25, 25))))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(), PracticeDto.class)
                .getId();

        final CustomerDto anotherDoctor = objectMapper.readValue(mockMvc.perform(systemRequest(() -> post("/customers"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(CustomerControllerTest.buildRequestDto("another@one.doctor"))))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(), CustomerDto.class);

        mockMvc.perform(practiceRequest(practiceId, owningDoctorId, () -> post("/practices/" + practiceId + "/doctors/" + anotherDoctor.getId())))
                .andExpect(status().isNoContent());

        UpdateDoctorDto doctorDto = new UpdateDoctorDto();
        doctorDto.setTreatmentTypes(Collections.emptyList());
        doctorDto.setBookingTimeOffset(48);

        mockMvc.perform(practiceRequest(practiceId, owningDoctorId, () -> put("/practices/" + practiceId + "/doctors/" + anotherDoctor.getId()))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(doctorDto)))
                .andExpect(status().isNoContent());

        mockMvc.perform(customerRequest(77L, () -> get("/practices?owner.id=" + owningDoctorId)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.elements[0].doctors[*]", hasSize(2)))
                .andExpect(jsonPath("$.elements[0].doctors[*].id", containsInAnyOrder((int) anotherDoctor.getId(), (int) owningDoctorId)))
                .andExpect(jsonPath("$.elements[0].doctors[*].bookingTimeOffset", containsInAnyOrder(1, 48)));
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void addDoctorToPractice_should_add_new_doctor_profile_if_not_present_yet() throws Exception
    {
        final long practiceId = objectMapper.readValue(mockMvc.perform(employeeRequest(42L, () -> post("/practices"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(buildRequestDto(25, 25))))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(), PracticeDto.class)
                .getId();

        final CustomerDto anotherDoctor = objectMapper.readValue(mockMvc.perform(systemRequest(() -> post("/customers"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(CustomerControllerTest.buildRequestDto("another@one.doctor"))))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(), CustomerDto.class);

        mockMvc.perform(employeeRequest(42L, () -> post("/practices/" + practiceId + "/doctors/" + anotherDoctor.getId())))
                .andExpect(status().isNoContent());

        mockMvc.perform(customerRequest(77L, () -> get("/practices/" + practiceId + "/doctors")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*]", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is((int) anotherDoctor.getId())))
                .andExpect(jsonPath("$[0].email", is("another@one.doctor")))
                .andExpect(jsonPath("$[0].treatmentTypes").exists());
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void updateDoctorInPractice_should_update_profile_details_if_necessary() throws Exception
    {
        final long practiceId = objectMapper.readValue(mockMvc.perform(employeeRequest(42L, () -> post("/practices"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(buildRequestDto(185, 233))))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(), PracticeDto.class)
                .getId();

        final CustomerDto anotherDoctor = objectMapper.readValue(mockMvc.perform(systemRequest(() -> post("/customers"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(CustomerControllerTest.buildRequestDto("another@one.doctor"))))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(), CustomerDto.class);

        mockMvc.perform(employeeRequest(42L, () -> post("/practices/" + practiceId + "/doctors/" + anotherDoctor.getId())))
                .andExpect(status().isNoContent());

        CreateTreatmentTypeDto ttRequest = new CreateTreatmentTypeDto();
        ttRequest.setType("my new type");
        ttRequest.setDoctorIds(Collections.singletonList(anotherDoctor.getId()));

        CreateTreatmentTypeDto tt2Request = new CreateTreatmentTypeDto();
        tt2Request.setType("my second type");

        int doctorId = (int)anotherDoctor.getId();

        long tt1Id = objectMapper.readValue(mockMvc.perform(employeeRequest(42L, () -> post("/practices/" + practiceId + "/treatments"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(ttRequest)))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(), TreatmentTypeDto.class)
                .getId();

        long tt2Id = objectMapper.readValue(mockMvc.perform(employeeRequest(42L, () -> post("/practices/" + practiceId + "/treatments"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(tt2Request)))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(), TreatmentTypeDto.class)
                .getId();

        mockMvc.perform(customerRequest(77L, () -> get("/practices/" + practiceId  + "/treatments")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.elements[*]", hasSize(2)))
                .andExpect(jsonPath("$.elements[*].doctorIds[*]", containsInAnyOrder(doctorId)));

        UpdateDoctorDto request = new UpdateDoctorDto();
        request.setTreatmentTypes(Arrays.asList(tt1Id, tt2Id));
        request.setMedicalSpecialties(Sets.newHashSet(MedicalSpecialtyType.NEUROLOGY));
        request.setBookable(false);

        mockMvc.perform(employeeRequest(42L, () -> put("/practices/" + practiceId + "/doctors/" + anotherDoctor.getId()))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(request)))
                .andExpect(status().isNoContent());

        mockMvc.perform(systemRequest(() -> get("/practices/" + practiceId + "/doctors")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*]", hasSize(1)))
                .andExpect(jsonPath("$[0].bookable", is(false)))
                .andExpect(jsonPath("$[0].treatmentTypes[*]", hasSize(2)))
                .andExpect(jsonPath("$[0].treatmentTypes[*].type", containsInAnyOrder("my new type", "my second type")))
                .andExpect(jsonPath("$[0].medicalSpecialties[*]", containsInAnyOrder(MedicalSpecialtyType.NEUROLOGY.name())));

        mockMvc.perform(customerRequest(77L, () -> get("/practices/" + practiceId  + "/treatments")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.elements[*]", hasSize(2)))
                .andExpect(jsonPath("$.elements[*].doctorIds[*]", containsInAnyOrder(doctorId, doctorId)));

        request.setTreatmentTypes(Collections.singletonList(tt2Id));

        mockMvc.perform(employeeRequest(42L, () -> put("/practices/" + practiceId + "/doctors/" + anotherDoctor.getId()))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(request)))
                .andExpect(status().isNoContent());

        mockMvc.perform(customerRequest(77L, () -> get("/practices/" + practiceId  + "/treatments?sort=id,desc")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.elements[*]", hasSize(2)))
                .andExpect(jsonPath("$.elements[*].doctorIds[*]", containsInAnyOrder(doctorId)));
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void removeDoctorFromPractice_should_remove_doctor() throws Exception
    {
        final long owningDoctorId = objectMapper.readValue(mockMvc.perform(systemRequest(() -> post("/customers"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(CustomerControllerTest.buildRequestDto())))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(), CustomerDto.class)
                .getId();

        final long practiceId = objectMapper.readValue(mockMvc.perform(customerRequest(owningDoctorId, () -> post("/practices"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(buildRequestDto(25, 25))))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(), PracticeDto.class)
                .getId();

        final CustomerDto anotherDoctor = objectMapper.readValue(mockMvc.perform(systemRequest(() -> post("/customers"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(CustomerControllerTest.buildRequestDto("another@doctor.mail"))))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(), CustomerDto.class);

        mockMvc.perform(practiceRequest(practiceId, owningDoctorId, () -> post("/practices/" + practiceId + "/doctors/" + anotherDoctor.getId())))
                .andExpect(status().isNoContent());

        mockMvc.perform(customerRequest(77L, () -> get("/practices?owner.id=" + owningDoctorId)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.elements[0].doctors[*]", hasSize(2)))
                .andExpect(jsonPath("$.elements[0].doctors[*].id", containsInAnyOrder((int) anotherDoctor.getId(), (int) owningDoctorId)));

        mockMvc.perform(practiceRequest(practiceId, owningDoctorId, () -> delete("/practices/" + practiceId + "/doctors/" + anotherDoctor.getId())))
                .andExpect(status().isNoContent());

        mockMvc.perform(customerRequest(77L, () -> get("/practices?owner.id=" + owningDoctorId)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.elements[0].doctors[*]", hasSize(1)))
                .andExpect(jsonPath("$.elements[0].doctors[0].id", is((int) owningDoctorId)));
    }

    @Test
    @Sql({"/db/empty_database.sql", "/db/test_doctors.sql"})
    public void removeDoctorFromPractice_and_addDoctorToPractice_should_be_available_for_owner_and_system_only() throws Exception
    {
        final long practiceId = objectMapper.readValue(mockMvc.perform(customerRequest(66L, () -> post("/practices"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(buildRequestDto(25, 25))))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(), PracticeDto.class)
                .getId();

        mockMvc.perform(customerRequest(42L, () -> delete("/practices/" + practiceId + "/doctors/42")))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.error", is("access_denied")));

        mockMvc.perform(customerRequest(42L, () -> post("/practices/" + practiceId + "/doctors/42")))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.error", is("access_denied")));

        mockMvc.perform(employeeRequest(42L, () -> delete("/practices/" + practiceId + "/doctors/42")))
                .andExpect(status().isNoContent());
    }

    @Test
    @Sql({"/db/empty_database.sql", "/db/test_doctors.sql"})
    public void getDoctors_should_return_doctors_from_practice() throws Exception
    {
        final long practiceId = objectMapper.readValue(mockMvc.perform(customerRequest(66L, () -> post("/practices"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(buildRequestDto(25, 25))))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(), PracticeDto.class)
                .getId();

        final CustomerDto oneDoctor = objectMapper.readValue(mockMvc.perform(systemRequest(() -> post("/customers"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(CustomerControllerTest.buildRequestDto())))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(), CustomerDto.class);

        final CustomerDto anotherDoctor = objectMapper.readValue(mockMvc.perform(systemRequest(() -> post("/customers"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(CustomerControllerTest.buildRequestDto("another@user.com"))))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(), CustomerDto.class);

        mockMvc.perform(practiceRequest(practiceId, 66L, () -> post("/practices/" + practiceId + "/doctors/" + oneDoctor.getId())))
                .andExpect(status().isNoContent());
        mockMvc.perform(practiceRequest(practiceId, 66L, () -> post("/practices/" + practiceId + "/doctors/" + anotherDoctor.getId())))
                .andExpect(status().isNoContent());

        mockMvc.perform(clientRequest(() -> get("/practices/" + practiceId + "/doctors")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*]", hasSize(3)))
                .andExpect(jsonPath("$[*].id", containsInAnyOrder(66, (int) oneDoctor.getId(), (int) anotherDoctor.getId())));
    }

    @Test
    @Sql({"/db/empty_database.sql", "/db/test_paid_features.sql"})
    public void getPractice_should_return_paid_feature() throws Exception
    {
        CreatePracticeDto request = buildRequestDto();
        request.setPaidFeatures(constructPaidFeatures("sms_appointment_notifications"));
        request.getPaidFeatures().get("sms_appointment_notifications").put("option1","value1");

        final long practiceId = objectMapper.readValue(mockMvc.perform(employeeRequest(99L, () -> post("/practices"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(request)))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(), PracticeDto.class)
                .getId();

        mockMvc.perform(clientRequest(() -> get("/practices/" + practiceId)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.paidFeatures[*]", hasSize(1)))
                .andExpect(jsonPath("$.paidFeatures[0].paidFeatureId", is(1)))
                .andExpect(jsonPath("$.paidFeatures[0].name", is("sms_appointment_notifications")))
                .andExpect(jsonPath("$.paidFeatures[0].options.option1", is("value1")));
    }

    @Test
    @Sql("/db/test_paid_features.sql")
    public void getPractice_should_return_multiple_paid_features() throws Exception
    {
        CreatePracticeDto request = buildRequestDto();
        request.setPaidFeatures(constructPaidFeatures("sms_appointment_notifications", "online_payments"));
        request.getPaidFeatures().get("sms_appointment_notifications").put("option1","value1");

        final long practiceId = objectMapper.readValue(mockMvc.perform(employeeRequest(99L, () -> post("/practices"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(request)))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(), PracticeDto.class)
                .getId();

        mockMvc.perform(clientRequest(() -> get("/practices/" + practiceId)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.paidFeatures[*]", hasSize(2)))
                .andExpect(jsonPath("$.paidFeatures[*].paidFeatureId", containsInAnyOrder(1,2)))
                .andExpect(jsonPath("$.paidFeatures[*].name", containsInAnyOrder("sms_appointment_notifications", "online_payments")))
                .andExpect(jsonPath("$.paidFeatures[*].options.option1", hasItem("value1")));
    }

    @Test
    @Sql({"/db/empty_database.sql", "/db/test_paid_features.sql"})
    public void updatePaidFeatures_should_add_paid_features() throws Exception
    {
        CreatePracticeDto request = buildRequestDto();

        final long practiceId = objectMapper.readValue(mockMvc.perform(employeeRequest(99L, () -> post("/practices"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(request)))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(), PracticeDto.class)
                .getId();

        UpdatePaidFeatureAssignmentDto update = new UpdatePaidFeatureAssignmentDto();
        update.setPaidFeatures(constructPaidFeatures("online_payments"));
        update.getPaidFeatures().get("online_payments").put("option1","value1");


        mockMvc.perform(employeeRequest(99L, () -> put("/practices/" + practiceId + "/paidFeatures"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(update)))
                .andExpect(status().isOk())
                .andReturn();

        mockMvc.perform(clientRequest(() -> get("/practices/" + practiceId)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.paidFeatures", hasSize(1)))
                .andExpect(jsonPath("$.paidFeatures[0].paidFeatureId", is(2)))
                .andExpect(jsonPath("$.paidFeatures[0].name", is("online_payments")))
                .andExpect(jsonPath("$.paidFeatures[0].options.option1", is("value1")));
    }

    @Test
    @Sql({"/db/empty_database.sql", "/db/test_paid_features.sql"})
    public void updatePaidFeatures_should_replace_paid_features() throws Exception
    {
        CreatePracticeDto request = buildRequestDto();

        final long practiceId = objectMapper.readValue(mockMvc.perform(employeeRequest(99L, () -> post("/practices"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(request)))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(), PracticeDto.class)
                .getId();

        UpdatePaidFeatureAssignmentDto update = new UpdatePaidFeatureAssignmentDto();
        update.setPaidFeatures(constructPaidFeatures("sms_appointment_notifications"));
        update.getPaidFeatures().get("sms_appointment_notifications").put("option1","value1");

        mockMvc.perform(employeeRequest(99L, () -> put("/practices/" + practiceId + "/paidFeatures"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(update)))
                .andExpect(status().isOk())
                .andReturn();

        mockMvc.perform(clientRequest(() -> get("/practices/" + practiceId)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.paidFeatures", hasSize(1)))
                .andExpect(jsonPath("$.paidFeatures[0].paidFeatureId", is(1)))
                .andExpect(jsonPath("$.paidFeatures[0].name", is("sms_appointment_notifications")))
                .andExpect(jsonPath("$.paidFeatures[0].options.option1", is("value1")));
    }

    @Test
    @Sql({"/db/empty_database.sql", "/db/test_paid_features.sql"})
    public void updatePaidFeatures_should_fail_if_any_paid_feature_doesnt_exist() throws Exception
    {
        CreatePracticeDto request = buildRequestDto();

        request.setPaidFeatures(constructPaidFeatures("sms_appointment_notifications"));
        request.getPaidFeatures().get("sms_appointment_notifications").put("option1","value1");
        request.getPaidFeatures().get("sms_appointment_notifications").put("option2","value2");

        final long practiceId = objectMapper.readValue(mockMvc.perform(employeeRequest(99L, () -> post("/practices"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(request)))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(), PracticeDto.class)
                .getId();

        UpdatePaidFeatureAssignmentDto update = new UpdatePaidFeatureAssignmentDto();
        update.setPaidFeatures(constructPaidFeatures("online_payments", "not_a_real_paid_feature"));
        update.getPaidFeatures().get("online_payments").put("option1","othervalue");

        mockMvc.perform(employeeRequest(99L, () -> put("/practices/" + practiceId + "/paidFeatures"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(update)))
                .andExpect(status().isNotFound())
                .andReturn();

        mockMvc.perform(clientRequest(() -> get("/practices/" + practiceId)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.paidFeatures", hasSize(1)))
                .andExpect(jsonPath("$.paidFeatures[0].paidFeatureId", is(1)))
                .andExpect(jsonPath("$.paidFeatures[0].name", is("sms_appointment_notifications")))
                .andExpect(jsonPath("$.paidFeatures[*].options.option1", not(hasItem("othervalue"))))
                .andExpect(jsonPath("$.paidFeatures[*].options.option1", hasItem("value1")))
                .andExpect(jsonPath("$.paidFeatures[*].options.option2", hasItem("value2")));
    }

    @Test
    @Sql({"/db/empty_database.sql", "/db/test_paid_features.sql"})
    public void getPaidFeatures_should_return_paid_features() throws Exception
    {
        CreatePracticeDto request = buildRequestDto();
        request.setPaidFeatures(constructPaidFeatures("sms_appointment_notifications"));
        request.getPaidFeatures().get("sms_appointment_notifications").put("option1","value1");

        final long practiceId = objectMapper.readValue(mockMvc.perform(employeeRequest(99L, () -> post("/practices"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(request)))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(), PracticeDto.class)
                .getId();


        mockMvc.perform(employeeRequest(99L, () -> get("/practices/" + practiceId + "/paidFeatures"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].paidFeatureId", is(1)))
                .andExpect(jsonPath("$[0].name", is("sms_appointment_notifications")))
                .andExpect(jsonPath("$[0].options.option1", is("value1")));

    }

    @Test
    @Sql({"/db/empty_database.sql", "/db/test_paid_features.sql"})
    public void getReviewSite_should_return_preferred_review_site() throws Exception
    {
        CreatePracticeDto request = buildRequestDto();
        request.setPaidFeatures(constructPaidFeatures("sms_review_notifications"));
        request = addReviewSiteToDto(request, "jameda", "yaddayaddayaddayadda");
        request = addReviewSiteToDto(request, "google", "blahblahblahblahblah");
        request = addReviewSiteToDto(request, "jameda", "hahmanahamanahamanah");

        final long practiceId = objectMapper.readValue(mockMvc.perform(employeeRequest(99L, () -> post("/practices"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(request)))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(), PracticeDto.class)
                .getId();

        mockMvc.perform(employeeRequest(99L, () -> get("/practices/" + practiceId + "/reviewSites")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.type", is("jameda")))
                .andExpect(jsonPath("$.data", is("yaddayaddayaddayadda")));
    }

    public static CreatePracticeDto buildRequestDto()
    {
        return buildRequestDto(42.1f, 31.23f);
    }

    public static CreatePracticeDto buildRequestDto(float longitude, float latitude)
    {
        CreateLocationDto location = new CreateLocationDto();
        location.setLongitude(longitude);
        location.setLatitude(latitude);
        location.setContactMail("my@valid.mail");
        location.setLocation("street 123");
        location.setZip("12345");
        location.setCity("berlin");

        CreatePracticeDto request = new CreatePracticeDto();
        request.setName("random-practice-name-" + Math.random() * 1000);
        request.setLocation(location);

        return request;
    }

    public static UpdatePracticeDto buildUpdateRequestDto() {
        UpdatePracticeDto request = new UpdatePracticeDto();

        List<CreateReviewSiteDto> reviewSites = new ArrayList<>();
        reviewSites.add(new CreateReviewSiteDto("google", "google_place_data"));
        reviewSites.add(new CreateReviewSiteDto("jameda", "jameda_data"));
        request.setReviewSites(reviewSites);

        return request;
    }

    public static CreatePracticeDto addReviewSiteToDto(CreatePracticeDto createPracticeDto, String type, String data) {
        createPracticeDto.getReviewSites().add(new CreateReviewSiteDto(type, data));
        return createPracticeDto;
    }

    public Map<String, Map<String, String>> constructPaidFeatures(String... featureNames) {
        Map<String, Map<String, String>> paidFeatureConfig = new HashMap<>();
        for (String s : featureNames) {
            paidFeatureConfig.put(s, new HashMap<>());
        }

        return paidFeatureConfig;
    }

}
