package com.arzttermine.service.profile.web;

import com.arzttermine.library.suport.functional.ThrowableSupplier;
import com.arzttermine.service.profile.Application;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Sets;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;
import java.util.function.Supplier;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
    classes = Application.class
)
public abstract class BaseIntegrationTest
{
    protected MockMvc mockMvc;

    @Autowired
    protected ObjectMapper objectMapper;

    @Autowired
    private WebApplicationContext applicationContext;

    @Autowired
    private FilterChainProxy filterChainProxy;

    @Autowired
    private DefaultTokenServices defaultTokenServices;

    private final TestAuthBuilder authBuilder = new TestAuthBuilder();

    @PostConstruct
    public void baseIntegrationSetup()
    {
        mockMvc = MockMvcBuilders.webAppContextSetup(applicationContext)
            .addFilter(filterChainProxy)
            .build();
    }

    protected String sendDto(Object dto) throws JsonProcessingException
    {
        return objectMapper.writeValueAsString(dto);
    }

    private MockHttpServletRequestBuilder configureAuthenticatedRequest(long customerId, String[] roles, ThrowableSupplier<MockHttpServletRequestBuilder> requestAction, String... scopes) throws Exception
    {
        final OAuth2Authentication oauth = authBuilder.buildCustomer(customerId, roles, (null == scopes || scopes.length == 0) ? new String[]{"default", "registration"} : scopes);
        final UUID token = UUID.randomUUID();

        when(defaultTokenServices.loadAuthentication(eq(token.toString()))).thenReturn(oauth);

        final MockHttpServletRequestBuilder request = requestAction.get();
        request.header("Authorization", "Bearer " + token);
        SecurityContextHolder.getContext().setAuthentication(oauth);

        return request;
    }

    protected MockHttpServletRequestBuilder customerRequest(long customer, ThrowableSupplier<MockHttpServletRequestBuilder> requestAction) throws Exception
    {
        return configureAuthenticatedRequest(customer, new String[]{"ROLE_CUSTOMER", "ROLE_CLIENT"}, requestAction);
    }

    protected MockHttpServletRequestBuilder practiceRequest(long practiceId, long customer, ThrowableSupplier<MockHttpServletRequestBuilder> requestAction) throws Exception
    {
        return configureAuthenticatedRequest(customer, new String[]{"ROLE_CUSTOMER", "ROLE_CLIENT", "ROLE_DOCTOR", "ROLE_PRACTICE_" + practiceId}, requestAction);
    }

    protected MockHttpServletRequestBuilder employeeRequest(long employee, ThrowableSupplier<MockHttpServletRequestBuilder> requestAction) throws Exception
    {
        return configureAuthenticatedRequest(employee, new String[]{"ROLE_EMPLOYEE", "ROLE_CUSTOMER", "ROLE_CLIENT"}, requestAction);
    }

    protected MockHttpServletRequestBuilder systemRequest(ThrowableSupplier<MockHttpServletRequestBuilder> requestAction) throws Exception
    {
        return configureAuthenticatedRequest(42L, new String[]{"ROLE_SYSTEM", "ROLE_CLIENT"}, requestAction);
    }

    protected MockHttpServletRequestBuilder clientRequest(ThrowableSupplier<MockHttpServletRequestBuilder> requestAction) throws Exception
    {
        return configureAuthenticatedRequest(23L, new String[]{"ROLE_CLIENT"}, requestAction);
    }

    protected MockHttpServletRequestBuilder scopedRequest(ThrowableSupplier<MockHttpServletRequestBuilder> requestAction, String[] roles, String... scopes) throws Exception
    {
        return configureAuthenticatedRequest(23L, roles, requestAction, scopes);
    }

    private static final class TestAuthBuilder
    {
        OAuth2Authentication buildCustomer(long customer, String[] roles, String[] scopes) {
            List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(roles);
            OAuth2Request request = new OAuth2Request(new HashMap<>(), "random_client_42", authorities, true, Sets.newHashSet(scopes), null, null, null, null);

            // in this service the principal of an user authentication is an customer entity. external silos uses the customer-id only.
            Authentication authentication = new UsernamePasswordAuthenticationToken(customer, "", authorities);

            return new OAuth2Authentication(request, authentication);
        }
    }
}
