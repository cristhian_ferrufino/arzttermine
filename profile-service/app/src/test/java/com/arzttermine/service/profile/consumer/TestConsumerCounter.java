package com.arzttermine.service.profile.consumer;

import com.arzttermine.library.messaging.consumer.Consumer;
import com.arzttermine.service.notification.messages.customer.PasswordResetMessage;
import com.arzttermine.service.profile.messages.TreatmentTypeChangedMessage;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Profile("testing")
public class TestConsumerCounter
{
    private final List<TreatmentTypeChangedMessage> changes = new ArrayList<>();

    private final List<PasswordResetMessage> passwordResets = new ArrayList<>();

    @Consumer
    public void treatmentTypeChanged(TreatmentTypeChangedMessage message)
    {
        changes.add(message);
    }

    @Consumer
    public void passwordResetRequested(PasswordResetMessage message)
    {
        passwordResets.add(message);
    }

    public List<TreatmentTypeChangedMessage> getChanges() {
        return changes;
    }

    public List<PasswordResetMessage> getPasswordResets() {
        return passwordResets;
    }

    public void flush() {
        changes.clear();
        passwordResets.clear();
    }
}
