package com.arzttermine.service.profile.web.controller;

import com.arzttermine.service.profile.consumer.TestConsumerCounter;
import com.arzttermine.service.profile.web.BaseIntegrationTest;
import com.arzttermine.service.profile.web.dto.request.PasswordResetRequestDto;
import com.arzttermine.service.profile.web.dto.request.UpdatePasswordDto;
import com.arzttermine.service.profile.web.dto.response.CustomerDto;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Sql("/db/empty_database.sql")
public class PasswordResetControllerTest extends BaseIntegrationTest
{
    @Autowired
    private TestConsumerCounter messageCounter;

    @Before
    public void before() {
        messageCounter.flush();
    }

    @Test
    public void createResetRequest_should_validate_requesting_mail() throws Exception
    {
        PasswordResetRequestDto requestDto = new PasswordResetRequestDto();
        requestDto.setEmail("my@customer.com");

        mockMvc.perform(systemRequest(() -> post("/passwords/requests"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(new PasswordResetRequestDto("my@customer.com"))))
            .andExpect(status().isNotFound());
    }

    @Test
    public void createResetRequest_should_create_request() throws Exception
    {
        long customerId = objectMapper.readValue(mockMvc.perform(systemRequest(() -> post("/customers"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(CustomerControllerTest.buildRequestDto("my@customer.com"))))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), CustomerDto.class)
            .getId();

        PasswordResetRequestDto requestDto = new PasswordResetRequestDto();
        requestDto.setEmail("my@customer.com");

        String token = (String) mockMvc.perform(systemRequest(() -> post("/passwords/requests"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(new PasswordResetRequestDto("my@customer.com"))))
            .andExpect(status().isNoContent())
            .andReturn()
            .getResponse()
            .getHeaderValue("X-Reset-Token");

        mockMvc.perform(systemRequest(() -> get("/passwords/requests/" + token + "?email=my@customer.com")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.token", is(token)))
            .andExpect(jsonPath("$.customerId", is((int) customerId)));
    }

    @Test
    public void resetPassword_should_require_equal_and_valid_passwords() throws Exception
    {
        long customerId = objectMapper.readValue(mockMvc.perform(systemRequest(() -> post("/customers"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(CustomerControllerTest.buildRequestDto("my@customer.com"))))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), CustomerDto.class)
            .getId();

        PasswordResetRequestDto requestDto = new PasswordResetRequestDto();
        requestDto.setEmail("my@customer.com");

        String token = (String) mockMvc.perform(systemRequest(() -> post("/passwords/requests"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(new PasswordResetRequestDto("my@customer.com"))))
            .andExpect(status().isNoContent())
            .andReturn()
            .getResponse()
            .getHeaderValue("X-Reset-Token");

        UpdatePasswordDto updateDto = new UpdatePasswordDto();
        updateDto.setPassword("short");
        updateDto.setRepeatPassword("short");

        mockMvc.perform(systemRequest(() -> post("/passwords/requests/" + token + "/customers/" + customerId))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(updateDto)))
            .andExpect(status().isBadRequest());

        updateDto.setPassword("valid-@one");
        updateDto.setRepeatPassword("invalid-repeat");

        mockMvc.perform(systemRequest(() -> post("/passwords/requests/" + token + "/customers/" + customerId))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(updateDto)))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void resetPassword_should_update_password() throws Exception
    {
        long customerId = objectMapper.readValue(mockMvc.perform(systemRequest(() -> post("/customers"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(CustomerControllerTest.buildRequestDto("my@customer.com"))))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), CustomerDto.class)
            .getId();

        String token = (String) mockMvc.perform(systemRequest(() -> post("/passwords/requests"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(new PasswordResetRequestDto("my@customer.com"))))
            .andExpect(status().isNoContent())
            .andReturn()
            .getResponse()
            .getHeaderValue("X-Reset-Token");

        UpdatePasswordDto updateDto = new UpdatePasswordDto();
        updateDto.setPassword("new-password");
        updateDto.setRepeatPassword("new-password");

        mockMvc.perform(systemRequest(() -> post("/passwords/requests/" + token + "/customers/" + customerId))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(updateDto)))
            .andExpect(status().isNoContent());
    }

    @Test
    public void resetPassword_should_send_out_notification_to_customer() throws Exception
    {
        long customerId = objectMapper.readValue(mockMvc.perform(systemRequest(() -> post("/customers"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(CustomerControllerTest.buildRequestDto("my@customer.com"))))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), CustomerDto.class)
            .getId();

        String token = (String) mockMvc.perform(systemRequest(() -> post("/passwords/requests"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(new PasswordResetRequestDto("my@customer.com"))))
            .andExpect(status().isNoContent())
            .andReturn()
            .getResponse()
            .getHeaderValue("X-Reset-Token");

        String expectedLink = String.format("https://test.frontend.de/test/reset-password?token=%s&email=my@customer.com", token);

        Thread.sleep(200);
        assertFalse(messageCounter.getPasswordResets().isEmpty());
        assertEquals(messageCounter.getPasswordResets().get(0).request.getCustomerId().longValue(), customerId);
        assertEquals(messageCounter.getPasswordResets().get(0).request.getParams().get("resetLink"), expectedLink);
    }
}
