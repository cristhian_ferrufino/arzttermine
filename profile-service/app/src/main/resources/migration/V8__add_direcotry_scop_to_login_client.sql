USE profile;

UPDATE oauth_client_details
    SET scope = 'default,directories'
    WHERE client_id = 'login-client';

INSERT INTO oauth_client_resource_grants VALUES
    (2, 'notification-service');
