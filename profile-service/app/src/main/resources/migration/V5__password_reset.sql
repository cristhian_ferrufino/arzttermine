USE profile;

CREATE TABLE `password_resets` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `token` varchar(128) NOT NULL,
  `active` bit(1) NOT NULL DEFAULT 1,
  `expires` datetime NULL DEFAULT NULL,
  `customer_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `id_password_reset_request_token` (`token`),
  CONSTRAINT `fk_password_reset_for_customer` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

INSERT INTO oauth_client_details (`id`, `access_token_validity`, `refresh_token_validity`, `authorities`, `grant_types`, `client_id`, `client_secret`, `scope`) VALUES
    (8, 57600, 115200, 'ROLE_CLIENT', 'client_credentials', 'atcalendar-client', 'bRqm*.87fTDz=c@5', 'default,reset-password');

INSERT INTO oauth_client_resource_grants (`client_id`, `resource_id`) VALUES
    (8, 'calendar-service'),
    (8, 'profile-service');
