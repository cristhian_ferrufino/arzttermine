USE profile;

CREATE TABLE `practice_review_sites` (
  `id` bigint(20) unsigned not null auto_increment,
  `practice_id` bigint(20) unsigned not null,
  `type` varchar(56) not null,
  `data` varchar(255) not null,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_practice_review_sites_practice_id` FOREIGN KEY (`practice_id`) REFERENCES `practices` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

ALTER TABLE practices
    ADD COLUMN `preferred_review_site_type` varchar(56) default null;

