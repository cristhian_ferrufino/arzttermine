USE profile;

CREATE TABLE `paid_features` (
  `id` bigint(20) unsigned not null auto_increment,
  `name` varchar(128) not null,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `practice_paid_features` (
  `practice_id` bigint(20) unsigned NOT NULL,
  `feature_id` bigint(20) unsigned NOT NULL,
  UNIQUE KEY `uk_paid_feature_per_practice_id` (`practice_id`, `feature_id`),
  CONSTRAINT `fk_paid_feature_practice_id` FOREIGN KEY (`practice_id`) REFERENCES `practices` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_paid_feature_feature_id` FOREIGN KEY (`feature_id`) REFERENCES `paid_features` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

INSERT INTO paid_features (id, name) VALUES
    (1, 'sms_booking_confirmation'),
    (2, 'sms_booking_reminder'),
    (3, 'online_payments'),
    (4, 'mail_review_reminder'),
    (5, 'sms_review_reminder'),
    (6, 'additional_resources'),
    (7, 'booking_customization');