USE profile;

CREATE TABLE `practice_insurance_types` (
    `insurance_type` varchar(56) NOT NULL,
    `practice_id` bigint(20) unsigned NOT NULL,
    INDEX `id_practice_insurance_practice_id` (`practice_id`),
    CONSTRAINT `fk_practice_insurance_practice_id` FOREIGN KEY (`practice_id`) REFERENCES `practices` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

INSERT INTO practice_insurance_types (practice_id, insurance_type) SELECT id, 'COMPULSORY' FROM practices;
INSERT INTO practice_insurance_types (practice_id, insurance_type) SELECT id, 'PRIVATE' FROM practices;
