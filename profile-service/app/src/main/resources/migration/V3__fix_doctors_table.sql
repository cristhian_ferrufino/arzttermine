USE profile;

ALTER TABLE `practices_doctors` DROP FOREIGN KEY `fk_practice_doctor_doctor_id`;
ALTER TABLE `practices_doctors` ADD CONSTRAINT `fk_practice_doctor_doctor_id` FOREIGN KEY (`doctors_id`) REFERENCES `doctor_profiles` (`id`) ON DELETE CASCADE;
