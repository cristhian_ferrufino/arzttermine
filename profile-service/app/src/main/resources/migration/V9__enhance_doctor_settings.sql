ALTER TABLE profile.doctor_profiles
    ADD COLUMN `booking_time_offset` int(4) unsigned NOT NULL DEFAULT 1;

CREATE TABLE `doctors_medical_specialties` (
  `doctor_profile_id` bigint(20) unsigned NOT NULL,
  `specialty_type` varchar(128) NOT NULL,
  UNIQUE KEY `uk_specialty_type_per_profile_id` (`doctor_profile_id`, `specialty_type`),
  CONSTRAINT `fk_doctors_specialties_profile_id` FOREIGN KEY (`doctor_profile_id`) REFERENCES `doctor_profiles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

DROP TABLE `practice_specialties`;
