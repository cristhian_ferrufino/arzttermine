USE profile;

CREATE TABLE `customers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL DEFAULT NOW(),
  `credentials_expired_notification` datetime NOT NULL DEFAULT NOW(),
  `last_credentials_update` datetime NOT NULL DEFAULT NOW(),
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `enabled` bit(1) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `updated_at` datetime NOT NULL DEFAULT NOW(),
  `username` varchar(255) NOT NULL,
  `default_insurance_type` varchar(56) NULL DEFAULT NULL,
  `preferred_language` varchar(56) NULL DEFAULT NULL,
  `phone` varchar(56) NULL DEFAULT NULL,
  `phone_mobile` varchar(56) NULL DEFAULT NULL,
  `gender` varchar(32) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_customer_email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `locations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `contact_mail` varchar(255) DEFAULT NULL,
  `country` varchar(56) NOT NULL,
  `city` varchar(128) NOT NULL,
  `location` varchar(255) NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  `phone` varchar(56) DEFAULT NULL,
  `phone_mobile` varchar(56) DEFAULT NULL,
  `zip` varchar(12) NOT NULL,
  `primary_location` bit(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `un_location_per_coordinate` (`longitude`,`latitude`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `oauth_access_token` (
  `token_id` varchar(128) NOT NULL,
  `token` blob,
  `authentication_id` varchar(128) NOT NULL,
  `user_name` varchar(128) DEFAULT NULL,
  `client_id` varchar(128) NOT NULL,
  `authentication` blob,
  `refresh_token` blob,
  PRIMARY KEY (`authentication_id`),
  INDEX `id_oauth_access_token_client_id` (`client_id`),
  INDEX `id_oauth_access_token_token_id` (`token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `oauth_client_details` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `access_token_validity` int(11) unsigned NOT NULL,
  `authorities` varchar(255) NOT NULL,
  `grant_types` varchar(255) NOT NULL,
  `client_id` varchar(56) NOT NULL,
  `client_secret` varchar(255) NOT NULL,
  `refresh_token_validity` int(11) unsigned NOT NULL,
  `scope` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_oauth_client_client_id` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `oauth_client_resource_grants` (
  `client_id` bigint(20) unsigned NOT NULL,
  `resource_id` varchar(128) NOT NULL,
  PRIMARY KEY (`client_id`,`resource_id`),
  CONSTRAINT `fk_resource_id_grant_client_id` FOREIGN KEY (`client_id`) REFERENCES `oauth_client_details` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `oauth_customer_roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) unsigned NOT NULL,
  `role` varchar(56) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `un_authority_role_per_client_id` (`client_id`,`role`),
  INDEX `id_oauth_customer_role_owner` (`client_id`),
  CONSTRAINT `fk_oauth_customer_role_owner_id` FOREIGN KEY (`client_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `widget_configurations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `background` varchar(30) DEFAULT NULL,
  `font_family` varchar(30) DEFAULT NULL,
  `foreground_primary_color` varchar(30) DEFAULT NULL,
  `foreground_secondary_color` varchar(30) DEFAULT NULL,
  `success_url` varchar(255) DEFAULT NULL,
  `success_text` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `practices` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `location_id` bigint(20) unsigned NOT NULL,
  `practice_owner_id` bigint(20) unsigned DEFAULT NULL,
  `widget_configuration_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `id_practice_location_id` (`location_id`),
  INDEX `id_practice_owner_id` (`practice_owner_id`),
  CONSTRAINT `fk_practice_location_id` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE RESTRICT,
  CONSTRAINT `fk_practice_owner_id` FOREIGN KEY (`practice_owner_id`) REFERENCES `customers` (`id`) ON DELETE SET NULL,
  CONSTRAINT fk_practice_widget_configuration_id FOREIGN KEY (`widget_configuration_id`) REFERENCES `widget_configurations`(`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `practices_doctors` (
  `practice_id` bigint(20) unsigned NOT NULL,
  `doctors_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`practice_id`, `doctors_id`),
  CONSTRAINT `fk_practice_doctor_doctor_id` FOREIGN KEY (`doctors_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_practice_doctor_practice_id` FOREIGN KEY (`practice_id`) REFERENCES `practices` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `customer_locations` (
  `customer_id` bigint(20) unsigned NOT NULL,
  `location_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`customer_id`, `location_id`),
  CONSTRAINT `fk_customer_location_location_id` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_customer_location_customer_id` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `resources` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `color` varchar(28) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `type` varchar(56) NOT NULL,
  `system_id` bigint(20) unsigned NOT NULL,
  `practice_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_resource_practice_id` (`practice_id`),
  CONSTRAINT `fk_resource_practice_id` FOREIGN KEY (`practice_id`) REFERENCES `practices` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `doctor_profiles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `id_doctor_profile_customer_id` (`customer_id`),
  CONSTRAINT `fk_doctor_profile_customer_id` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `treatment_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(128) NOT NULL,
  `charge` int(4) unsigned NULL DEFAULT NULL,
  `practice_id` bigint(20) unsigned NOT NULL,
  `force_payment` bit(1) NOT NULL DEFAULT FALSE,
  `enabled` bit(1) NOT NULL DEFAULT 1,
  `comment` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `id_treatment_type_practice_id` (`practice_id`),
  UNIQUE KEY `un_treatment_type_per_doctor_id` (`practice_id`, `type`, `enabled`),
  CONSTRAINT `fk_treatment_type_practice_id` FOREIGN KEY (`practice_id`) REFERENCES `practices` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `treatment_type_provider` (
  `doctor_profile_id` bigint(20) unsigned NOT NULL,
  `treatment_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`doctor_profile_id`, `treatment_id`),
  CONSTRAINT `fk_provider_profile_id` FOREIGN KEY (`doctor_profile_id`) REFERENCES `doctor_profiles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_providing_treatment_id` FOREIGN KEY (`treatment_id`) REFERENCES `treatment_types` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `practice_specialties` (
  `practice_id` bigint(20) unsigned NOT NULL,
  `specialty_type` varchar(128) NOT NULL,
  UNIQUE KEY `uk_specialty_type_per_practice_id` (`practice_id`, `specialty_type`),
  CONSTRAINT `fk_practice_specialties_practice_id` FOREIGN KEY (`practice_id`) REFERENCES `practices` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;


