ALTER TABLE practice_paid_features ADD id bigint(20) unsigned PRIMARY KEY AUTO_INCREMENT;

CREATE TABLE `paid_feature_options` (
  `assignment_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  UNIQUE KEY `uk_option_per_feature_assignment` (`assignment_id`, `name`),
  CONSTRAINT `fk_practice_paid_features_id` FOREIGN KEY (`assignment_id`) REFERENCES `practice_paid_features` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

