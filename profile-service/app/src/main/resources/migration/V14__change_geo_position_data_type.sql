ALTER TABLE profile.locations
    MODIFY COLUMN longitude decimal(9, 6) NOT NULL,
    MODIFY COLUMN latitude decimal(9, 6) NOT NULL;
