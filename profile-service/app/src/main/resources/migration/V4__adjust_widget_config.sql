ALTER TABLE `profile`.`widget_configurations`
    DROP COLUMN `background`,
    DROP COLUMN `font_family`,
    DROP COLUMN `foreground_primary_color`,
    DROP COLUMN `foreground_secondary_color`,
    ADD COLUMN `template_id` varchar(128) NULL DEFAULT 'default';
