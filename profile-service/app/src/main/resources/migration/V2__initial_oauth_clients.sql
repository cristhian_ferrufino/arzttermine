USE profile;

INSERT INTO oauth_client_details (`id`, `access_token_validity`, `refresh_token_validity`, `authorities`, `grant_types`, `client_id`, `client_secret`, `scope`) VALUES
    (1, 7200, 14400, 'ROLE_CLIENT', 'client_credentials', 'calendar-client', '3]?4StH!<j6*8Wj#', 'default'),
    (2, 86400, 108000, 'ROLE_ANONYMOUS', 'password', 'login-client', 'wz$kkY<rx!PhE6kc', 'default'),
    (3, 7200, 14400, 'ROLE_ANONYMOUS', 'client_credentials', 'register-client', 'Z<93^}xCtMSNe)5[', 'registration'),
    (4, 7200, 14400, 'ROLE_CLIENT', 'client_credentials', 'booking-client', '7v)y>2AQX;.?v9V#', 'default,registration,payment'),
    (5, 7200, 14400, 'ROLE_CLIENT', 'client_credentials', 'payment-client', '>$ZmsWG%94)]_27*', 'default'),
    (6, 14400, 28800, 'ROLE_CLIENT', 'client_credentials', 'notification-client', 'wcFD7A6jG/Qm-\Sz', 'default'),
    (7, 14400, 28800, 'ROLE_CLIENT', 'client_credentials', 'history-client', '2Tb)M9<g+a37SH@L', 'default');

INSERT INTO oauth_client_resource_grants (`client_id`, `resource_id`) VALUES
    (1, 'oauth2-resource'),
    (1, 'profile-service'),
    (2, 'profile-service'),
    (2, 'calendar-service'),
    (2, 'booking-service'),
    (3, 'profile-service'),
    (4, 'oauth2-resource'),
    (4, 'profile-service'),
    (4, 'calendar-service'),
    (4, 'payment-service'),
    (5, 'oauth2-resource'),
    (5, 'booking-service'),
    (5, 'profile-service'),
    (6, 'oauth2-resource'),
    (6, 'profile-service'),
    (7, 'oauth2-resource'),
    (7, 'profile-service');
