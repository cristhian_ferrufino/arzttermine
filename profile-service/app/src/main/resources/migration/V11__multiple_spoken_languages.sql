USE profile;

CREATE TABLE `customers_languages` (
    `language` varchar(56) NOT NULL,
    `customer_id` bigint unsigned NOT NULL,
    UNIQUE KEY `un_language_for_each_customer` (`language`, `customer_id`),
    CONSTRAINT `fk_customer_languages_customer_id` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE
);
