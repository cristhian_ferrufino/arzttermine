ALTER TABLE profile.doctor_profiles
    ADD COLUMN `bookable` bit(1) NOT NULL DEFAULT 1;
