package com.arzttermine.service.profile.service.mapper;

import com.arzttermine.service.profile.domain.entity.PaidFeatureAssignment;
import com.arzttermine.service.profile.web.dto.response.PaidFeatureAssignmentDto;
import org.springframework.stereotype.Component;

@Component
public class PaidFeatureAssignmentMapper {


    public PaidFeatureAssignmentDto toDto(final PaidFeatureAssignment paidFeatureAssignment)
    {
        final PaidFeatureAssignmentDto dto = new PaidFeatureAssignmentDto();
        dto.setPaidFeatureId(paidFeatureAssignment.getPaidFeature().getId());
        dto.setName(paidFeatureAssignment.getPaidFeature().getName());

        dto.getOptions().putAll(paidFeatureAssignment.getOptions());

        return dto;

    }

}
