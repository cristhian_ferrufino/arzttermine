package com.arzttermine.service.profile.service.event;

import com.arzttermine.library.messaging.processor.MessageBus;
import com.arzttermine.library.web.dto.PriceDto;
import com.arzttermine.service.profile.messages.TreatmentTypeChangedMessage;
import com.arzttermine.service.profile.service.event.model.TreatmentUpdateEvent;
import com.arzttermine.service.profile.service.mapper.CustomerMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

import java.util.Collections;

@Component
public class TreatmentUpdateListener
{
    private static final Logger log = LoggerFactory.getLogger(TreatmentUpdateListener.class);

    @Autowired
    private MessageBus messageBus;

    @Autowired
    private CustomerMapper mapper;

    @Async
    @TransactionalEventListener
    public void treatmentUpdated(final TreatmentUpdateEvent event)
    {
        log.info(
            "treatment type {} was {}",
            event.getTreatmentTypeId(),
            null == event.getTreatmentType() ? "deleted" : "created or updated"
        );

        final TreatmentTypeChangedMessage message = new TreatmentTypeChangedMessage();
        message.setDeleted(null == event.getTreatmentType());
        message.setTreatmentTypeId(event.getTreatmentTypeId());

        if (!message.isDeleted() && null != event.getTreatmentType().getCharge()) {
            message.setCharge(new PriceDto(event.getTreatmentType().getCharge()));
        }

        messageBus.sendAsync(Collections.singletonList(message));
    }
}
