package com.arzttermine.service.profile.domain.repository;

import com.arzttermine.service.profile.domain.entity.OAuthClient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends JpaRepository<OAuthClient, Long>
{
    OAuthClient findByClientId(String clientId);
}