package com.arzttermine.service.profile.domain.repository;

import com.arzttermine.service.profile.domain.entity.TreatmentType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TreatmentTypeRepository extends PagingAndSortingRepository<TreatmentType, Long>, QueryDslPredicateExecutor<TreatmentType>
{
    @Query("SELECT COUNT(tt.id) > 0 FROM TreatmentType tt WHERE tt.practiceId = :practice AND tt.type = :type AND tt.enabled IS TRUE")
    boolean existsInPractice(@Param("practice") long practiceId, @Param("type") String type);

    @Query("SELECT tt FROM TreatmentType tt WHERE tt.practiceId = :practice AND tt.enabled IS TRUE")
    Page<TreatmentType> findByPracticeId(@Param("practice") long practiceId, Pageable pageable);
}
