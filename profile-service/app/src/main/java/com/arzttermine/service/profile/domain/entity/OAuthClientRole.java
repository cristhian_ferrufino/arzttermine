package com.arzttermine.service.profile.domain.entity;

import com.arzttermine.service.profile.support.OAuthAuthorityConverter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.persistence.*;

@Entity
@Table(
    name = "oauth_customer_roles",
    indexes = {
        @Index(name = "id_oauth_customer_role_owner", columnList = "client_id")
    },
    uniqueConstraints = {
        @UniqueConstraint(name = "un_authority_role_per_client_id", columnNames = {"client_id", "role"})
    }
)
public class OAuthClientRole
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "client_id", nullable = false, updatable = false)
    private long clientId;

    @Convert(converter = OAuthAuthorityConverter.class)
    @Column(nullable = false, updatable = false)
    private GrantedAuthority role;

    protected OAuthClientRole() {
    }

    private OAuthClientRole(Builder builder) {
        this.clientId = builder.id;
        this.role = builder.authority;
    }

    public long getId() {
        return id;
    }

    public long getClientId() {
        return clientId;
    }

    public GrantedAuthority getRole() {
        return role;
    }

    static Builder builder() {
        return new Builder();
    }

    static final class Builder
    {
        private long id;
        private GrantedAuthority authority;

        Builder clientId(long id) {
            this.id = id;
            return this;
        }

        Builder role(String authority) {
            this.authority = new SimpleGrantedAuthority(authority);
            return this;
        }

        OAuthClientRole build() {
            return new OAuthClientRole(this);
        }
    }
}
