package com.arzttermine.service.profile.domain.entity;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Entity
@Table(name = "widget_configurations")
public class WidgetConfiguration
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false, insertable = false)
    private long id;

    @Column
    private String templateId;

    @Length(min = 3, max = 255)
    @Column(name = "success_url")
    private String successUrl;

    @Length(min = 3, max = 255)
    @Column(name = "success_text")
    private String successText;

    public long getId() {
        return id;
    }

    public String getSuccessUrl() {
        return successUrl;
    }

    public void setSuccessUrl(String successUrl) {
        this.successUrl = successUrl;
    }

    public String getSuccessText() {
        return successText;
    }

    public void setSuccessText(String successText) {
        this.successText = successText;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }
}
