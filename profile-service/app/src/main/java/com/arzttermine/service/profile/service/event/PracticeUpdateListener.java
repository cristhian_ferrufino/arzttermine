package com.arzttermine.service.profile.service.event;

import com.arzttermine.library.elasticsearch.model.BulkAction;
import com.arzttermine.library.elasticsearch.service.DocumentProcessor;
import com.arzttermine.service.profile.domain.entity.Practice;
import com.arzttermine.service.profile.domain.repository.PracticeRepository;
import com.arzttermine.service.profile.service.event.model.PracticeUpdateEvent;
import com.arzttermine.service.profile.service.mapper.PracticeMapper;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionalEventListener;

import java.util.Collections;

@Component
public class PracticeUpdateListener
{
    private static final Logger log = LoggerFactory.getLogger(PracticeUpdateListener.class);

    @Autowired
    private DocumentProcessor documentProcessor;

    @Autowired
    private PracticeRepository repository;

    @Autowired
    private PracticeMapper mapper;

    @Async
    @TransactionalEventListener
    @Transactional(readOnly = true)
    public void practiceUpdated(final PracticeUpdateEvent event)
    {
        final BulkAction action;
        final PracticeDto dto;

        if (null == event.getPractice()) {
            final Practice practice = repository.findOne(event.getPracticeId());
            if (null == practice) {
                log.warn("practice {} was updated but not found", event.getPracticeId());
                return;
            }

            dto = mapper.toDto(practice);
        } else {
            dto = event.getPractice();
        }

        log.info(
            "practice {} was updated in mode {}, updating elasticsearch...",
            dto.getId(),
            event.getUpdateType().name()
        );

        final String type = dto.getLocation().getCountry().name().toLowerCase();

        if (event.getUpdateType() == BulkAction.ActionType.DELETE) {
            action = BulkAction.delete(dto);
        }
        else if (event.getUpdateType() == BulkAction.ActionType.INDEX) {
            documentProcessor.index("practices", type, dto);
            return;
        }
        else {
            action = BulkAction.update(dto);
        }

        documentProcessor.execute("practices", type, Collections.singletonList(action));
    }
}
