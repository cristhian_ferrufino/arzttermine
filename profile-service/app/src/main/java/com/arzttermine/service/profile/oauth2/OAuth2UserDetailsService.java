package com.arzttermine.service.profile.oauth2;

import com.arzttermine.service.profile.domain.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class OAuth2UserDetailsService implements UserDetailsService
{
    @Autowired
    private CustomerRepository repository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return Optional.ofNullable(repository.findOneByEmail(email))
            .orElseThrow(() -> new UsernameNotFoundException(String.format("User %s not found", email)));
    }
}
