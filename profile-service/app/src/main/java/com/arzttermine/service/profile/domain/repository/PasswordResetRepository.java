package com.arzttermine.service.profile.domain.repository;

import com.arzttermine.service.profile.domain.entity.PasswordReset;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface PasswordResetRepository extends JpaRepository<PasswordReset, Long>
{
    PasswordReset findOneByToken(UUID token);
}
