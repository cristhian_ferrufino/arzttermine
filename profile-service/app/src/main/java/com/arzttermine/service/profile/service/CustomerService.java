package com.arzttermine.service.profile.service;

import com.arzttermine.library.suport.exception.AccessDeniedException;
import com.arzttermine.library.suport.exception.EntityAlreadyExistException;
import com.arzttermine.library.suport.exception.EntityNotFoundException;
import com.arzttermine.library.suport.exception.InvalidArgumentException;
import com.arzttermine.library.web.dto.response.paging.PageDto;
import com.arzttermine.service.profile.domain.entity.Customer;
import com.arzttermine.service.profile.domain.entity.DoctorProfile;
import com.arzttermine.service.profile.domain.repository.CustomerRepository;
import com.arzttermine.service.profile.domain.repository.DoctorProfileRepository;
import com.arzttermine.service.profile.domain.repository.OAuthAuthorityRepository;
import com.arzttermine.service.profile.domain.repository.PracticeRepository;
import com.arzttermine.service.profile.service.event.model.PracticeUpdateEvent;
import com.arzttermine.service.profile.service.mapper.CustomerMapper;
import com.arzttermine.service.profile.util.SecurityContextUtil;
import com.arzttermine.service.profile.web.dto.request.CreateCustomerDto;
import com.arzttermine.service.profile.web.dto.request.UpdateCustomerDto;
import com.arzttermine.service.profile.web.dto.response.CustomerDto;
import com.arzttermine.service.profile.web.dto.response.CustomersPage;
import com.arzttermine.service.profile.web.struct.AuthorityRole;
import com.querydsl.core.types.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CustomerService
{
    private static final Logger log = LoggerFactory.getLogger(CustomerService.class);

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private OAuthAuthorityRepository authorityRepository;

    @Autowired
    private PracticeRepository practiceRepository;

    @Autowired
    private DoctorProfileRepository doctorProfileRepository;

    @Autowired
    private CustomerMapper customerMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Transactional(readOnly = false)
    public CustomerDto createCustomer(final CreateCustomerDto request, final OAuth2Authentication auth)
    {
        final Customer existing = customerRepository.findOneByEmail(request.getEmail());

        if (null != existing && existing.hasPassword() && null == request.getPassword()) {
            throw new InvalidArgumentException("password", "customer requires a password check");
        }
        else if (null != existing && existing.hasPassword() && null != request.getPassword()) {
            final boolean matched = passwordEncoder.matches(request.getPassword(), existing.getPassword());
            request.setPassword(null);

            if (matched) {
                return customerMapper.toDto(existing);
            } else {
                throw new AccessDeniedException();
            }
        }
        else if (null != existing && !existing.hasPassword()) {
            return customerMapper.toDto(existing);
        }

        final Customer customer = customerRepository.save(customerMapper.fromRequest(request));

        if (null == request.getRoles() || !SecurityContextUtil.validateIsEmployeeOrSystem(auth)) {
            customer.addAuthority(AuthorityRole.ROLE_CUSTOMER);
        } else {
            if (request.getRoles().contains(AuthorityRole.ROLE_EMPLOYEE) && !request.getRoles().contains(AuthorityRole.ROLE_CUSTOMER)) {
                request.getRoles().add(AuthorityRole.ROLE_CUSTOMER);
            }

            request.getRoles().forEach(customer::addAuthority);
        }

        authorityRepository.save(customer.getRoles()); // customer.save already called to get the id

        return customerMapper.toDto(customer);
    }

    @Transactional(readOnly = true)
    public CustomerDto getCustomer(final long id)
    {
        final Customer customer = findOneNullsafe(id);

        return customerMapper.toDto(customer);
    }

    @Transactional(readOnly = false)
    public CustomerDto updateCustomer(final long id, final UpdateCustomerDto update, final OAuth2Authentication auth)
    {
        final Customer customer = findOneNullsafe(id);
        if (!SecurityContextUtil.validateIsEmployeeOrSystem(auth)) {

            // this doesnt work atm as everyone in a practice should be able to update customers
            if (id != (Long) auth.getPrincipal()) {
                // @TODO remove when we have proper security grants
                if (!SecurityContextUtil.isDoctorOrPractice(auth)) {
                    throw new AccessDeniedException();
                }
            }
        }

        customer.setTitle(update.getTitle());
        customer.setGender(update.getGender());

        Optional.ofNullable(update.getFirstName()).ifPresent(customer::setFirstName);
        Optional.ofNullable(update.getLastName()).ifPresent(customer::setLastName);
        Optional.ofNullable(update.getPreferredLanguage()).ifPresent(customer::setPreferredLanguage);
        Optional.ofNullable(update.getPhoneMobile()).ifPresent(customer::setPhoneMobile);
        Optional.ofNullable(update.getPhone()).ifPresent(customer::setPhone);
        Optional.ofNullable(update.getInsuranceProvider())
            .ifPresent(ipd -> customer.setDefaultInsuranceType(ipd.getType()));

        Optional.ofNullable(update.getSpokenLanguages()).ifPresent(languages -> {
            customer.getSpokenLanguages().clear();
            customer.getSpokenLanguages().addAll(languages);
        });

        if (! customer.getSpokenLanguages().contains(customer.getPreferredLanguage())) {
            customer.getSpokenLanguages().add(customer.getPreferredLanguage());
        }

        // updating roles if set
        if (SecurityContextUtil.validateIsEmployeeOrSystem(auth)) {
            Optional.ofNullable(update.getRoles()).ifPresent(roles -> {
                if (roles.contains(AuthorityRole.ROLE_EMPLOYEE) && !roles.contains(AuthorityRole.ROLE_CUSTOMER)) {
                    roles.add(AuthorityRole.ROLE_CUSTOMER);
                }

                customer.getAuthorities().stream()
                    .filter(role -> !role.getAuthority().startsWith("ROLE_PRACTICE_")) // use delete doctor instead to revoke permissions
                    .filter(role -> roles.stream().map(AuthorityRole::name).noneMatch(name -> name.equals(role.getAuthority())))
                    .forEach(role -> customer.removeAuthority(role.getAuthority()));

                roles.forEach(role -> {
                    if (!customer.hasAuthority(role)) {
                        customer.addAuthority(role);
                    }
                });
            });
        }

        Optional.ofNullable(update.getPassword()).ifPresent(password -> {
            if (!password.equals(update.getRepeatPassword())) {
                throw new InvalidArgumentException("repeatPassword", "passwords must be equal");
            }

            if (!customer.getEmail().equals(update.getEmail())) {
                log.warn("could not update passwords, email {} is not valid.", update.getEmail());
                return;
            }

            customerMapper.setPasswordFor(customer, update.getPassword());
        });

        if (null != update.getEmail() && !customer.getEmail().equals(update.getEmail())) {
            final Customer existing = customerRepository.findOneByEmail(update.getEmail());

            if (null != existing) {
                throw new EntityAlreadyExistException("Customer", update.getEmail());
            }

            customer.setEmail(update.getEmail());
        }

        // @TODO move to doctors update and allow to update customer details through the doctors endpoint as well (?)
        if (customer.isDoctor()) {
            DoctorProfile doctorProfile = doctorProfileRepository.findOneByCustomerId(customer.getId());
            Collection<Long> practiceIds = practiceRepository.findPracticeIdByDoctorProfile(doctorProfile.getId());
            for (Long practiceId : practiceIds) {
                final PracticeUpdateEvent event = PracticeUpdateEvent.ofUpdated(practiceId);
                eventPublisher.publishEvent(event);
            }
        }

        return customerMapper.toDto(customer);
    }

    @Transactional(readOnly = true)
    public CustomersPage getCustomers(final Predicate predicate, final Pageable pageable)
    {
        final Page<Customer> customers = customerRepository.findAll(predicate, pageable);
        final PageDto page = new PageDto();
        page.setTotalPages(customers.getTotalPages());
        page.setTotalItems(customers.getTotalElements());
        page.setPageSize(pageable.getPageSize());
        page.setCurrentPage(pageable.getPageNumber());

        final CustomersPage result = new CustomersPage();
        result.setElements(customers.getContent().stream().map(customerMapper::toDto).collect(Collectors.toList()));
        result.setPage(page);

        return result;
    }

    private Customer findOneNullsafe(final long id)
    {
        return Optional.ofNullable(customerRepository.findOne(id))
            .orElseThrow(() -> new EntityNotFoundException("Customer", id));
    }
}
