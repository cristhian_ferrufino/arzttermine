package com.arzttermine.service.profile.domain.repository;

import com.arzttermine.service.profile.domain.entity.Customer;
import com.arzttermine.service.profile.domain.entity.QCustomer;
import com.querydsl.core.types.dsl.SimpleExpression;
import com.querydsl.core.types.dsl.StringExpression;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends PagingAndSortingRepository<Customer, Long>,
                                            QueryDslPredicateExecutor<Customer>,
                                            QuerydslBinderCustomizer<QCustomer>
{
    Customer findOneByEmail(String email);

    Customer getByUsernameOrEmail(String username, String email);

    @Override
    default void customize(QuerydslBindings bindings, QCustomer root) {
        bindings.bind(root.enabled).first(SimpleExpression::eq);
        bindings.bind(root.email).first(StringExpression::containsIgnoreCase);
    }
}
