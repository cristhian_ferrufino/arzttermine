package com.arzttermine.service.profile.service.mapper;

import com.arzttermine.service.profile.domain.entity.Location;
import com.arzttermine.service.profile.web.dto.LocationDto;
import com.arzttermine.service.profile.web.dto.request.CreateLocationDto;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class LocationMapper
{
    public Location fromRequest(final CreateLocationDto request) {
        final Location location = new Location(request.getCountry());
        Optional.ofNullable(request.getCity()).ifPresent(location::setCity);
        Optional.ofNullable(request.getContactMail()).ifPresent(location::setContactMail);
        Optional.ofNullable(request.getLatitude()).ifPresent(location::setLatitude);
        Optional.ofNullable(request.getLongitude()).ifPresent(location::setLongitude);
        Optional.ofNullable(request.getPhone()).ifPresent(location::setPhone);
        Optional.ofNullable(request.getPhoneMobile()).ifPresent(location::setPhoneMobile);
        Optional.ofNullable(request.getLocation()).ifPresent(location::setLocation);
        Optional.ofNullable(request.getZip()).ifPresent(location::setZip);
        Optional.ofNullable(request.getPrimary()).ifPresent(location::setPrimary);

        return location;
    }

    public LocationDto toDto(final Location location) {
        final LocationDto dto = new LocationDto();
        dto.setId(location.getId());
        dto.setContactMail(location.getContactMail());
        dto.setPhoneMobile(location.getPhoneMobile());
        dto.setPhone(location.getPhone());
        dto.setLongitude(location.getLongitude());
        dto.setLatitude(location.getLatitude());
        dto.setCountry(location.getCountry());
        dto.setLocation(location.getLocation());
        dto.setPrimary(location.isPrimary());
        dto.setCity(location.getCity());
        dto.setZip(location.getZip());

        return dto;
    }
}
