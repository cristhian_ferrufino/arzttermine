package com.arzttermine.service.profile.web.controller;

import com.arzttermine.service.profile.service.LocationService;
import com.arzttermine.service.profile.web.dto.LocationDto;
import com.arzttermine.service.profile.web.dto.request.UpdateLocationDto;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/locations")
public class LocationController
{
    @Autowired
    private LocationService service;


    @ApiOperation("Updates and returns a single location")
    @RequestMapping(method = RequestMethod.PUT, value="/{id}", consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyRole('ROLE_CUSTOMER', 'ROLE_EMPLOYEE', 'ROLE_SYSTEM')")
    public LocationDto updateLocation(@PathVariable long id, @RequestBody @Valid UpdateLocationDto update)
    {
        return service.updateLocation(id, update);
    }
}
