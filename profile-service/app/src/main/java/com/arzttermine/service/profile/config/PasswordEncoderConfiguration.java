package com.arzttermine.service.profile.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class PasswordEncoderConfiguration
{
    @Bean
    @Profile("!testing")
    public PasswordEncoder passwordEncoder()
    {
        return new BCryptPasswordEncoder(12);
    }

    @Bean
    @Profile("testing")
    public PasswordEncoder testPasswordEncoder()
    {
        return new BCryptPasswordEncoder(4);
    }
}
