package com.arzttermine.service.profile.service;

import com.arzttermine.library.suport.exception.EntityNotFoundException;
import com.arzttermine.library.suport.exception.InvalidArgumentException;
import com.arzttermine.library.web.dto.response.paging.PageDto;
import com.arzttermine.library.web.struct.InsuranceType;
import com.arzttermine.service.profile.domain.entity.*;
import com.arzttermine.service.profile.domain.repository.CustomerRepository;
import com.arzttermine.service.profile.domain.repository.DoctorProfileRepository;
import com.arzttermine.service.profile.domain.repository.OAuthAuthorityRepository;
import com.arzttermine.service.profile.domain.repository.PracticeRepository;
import com.arzttermine.service.profile.service.event.model.PracticeUpdateEvent;
import com.arzttermine.service.profile.service.mapper.CustomerMapper;
import com.arzttermine.service.profile.service.mapper.PracticeMapper;
import com.arzttermine.service.profile.service.mapper.ReviewSiteMapper;
import com.arzttermine.service.profile.util.SecurityContextUtil;
import com.arzttermine.service.profile.web.dto.request.*;
import com.arzttermine.service.profile.web.dto.response.DoctorProfileDto;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import com.arzttermine.service.profile.web.dto.response.PracticesPage;
import com.arzttermine.service.profile.web.struct.AuthorityRole;
import com.arzttermine.service.profile.web.struct.ResourceType;
import com.querydsl.core.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class PracticeService
{
    @Autowired
    private PaidFeatureService paidFeatureService;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private PracticeRepository practiceRepository;

    @Autowired
    private PracticeMapper practiceMapper;

    @Autowired
    private CustomerMapper customerMapper;

    @Autowired
    private ReviewSiteMapper reviewSiteMapper;

    @Autowired
    private DoctorProfileRepository profileRepository;

    @Autowired
    private OAuthAuthorityRepository authorityRepository;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    /**
     * If an employee or a system creates a new practice,
     * you can provide a property in {@link CreatePracticeDto} called ownerId to specify the practice owner.
     * The corresponding customer will be the new owner of the practice.
     * Otherwise, if a customer is logged in, use the requesting customer as owner.
     *
     * @param request CreatePracticeDto
     * @param auth OAuth2Authentication
     * @return PracticeDto which was created
     */
    @Transactional(readOnly = false)
    public PracticeDto createPractice(final CreatePracticeDto request, final OAuth2Authentication auth)
    {
        final Practice practice = practiceMapper.fromRequest(request);
        final boolean isAdmin = SecurityContextUtil.validateIsEmployeeOrSystem(auth);

        if (!isAdmin) {
            request.setOwnerId((long) auth.getPrincipal());
        }
        if (null != request.getOwnerId()) {
            final DoctorProfile owner = Optional.ofNullable(customerRepository.findOne(request.getOwnerId()))
                    .map(this::createProfileIfNotExist).orElse(null);

            if (null != owner) {
                practice.setOwner(owner);
                appendProfileToPractice(practice, owner);
            }
        }

        //If not further specified allow all insurance types
        if (request.getInsuranceTypes() != null) {
            practice.getInsuranceTypes().addAll(request.getInsuranceTypes());
        } else {
            practice.getInsuranceTypes().addAll(Arrays.asList(InsuranceType.values()));
        }

        final List<Long> initialDoctors = request.getInitialDoctors().stream()
                .distinct()
                .filter(doctorId -> null == request.getOwnerId() || request.getOwnerId().longValue() != doctorId)
                .collect(Collectors.toList());

        customerRepository.findAll(initialDoctors).forEach(c -> {
            final DoctorProfile profile = createProfileIfNotExist(c);
            appendProfileToPractice(practice, profile);
        });

        if (null != request.getPaidFeatures() && !request.getPaidFeatures().isEmpty()) {
            paidFeatureService.updatePaidFeaturesOnPractice(practice, request.getPaidFeatures());
        }

        // @TODO move to reviewSite mapper or service
        if (!request.getReviewSites().isEmpty()) {
            Set<ReviewSite> reviewSites = request.getReviewSites().stream()
                    .map(reviewSite -> reviewSiteMapper.fromRequest(practice, reviewSite))
                    .collect(Collectors.toSet());

            practice.setReviewSites(reviewSites);

            // If there isnt a preferred review site type in the request, use the first review site type.
            if (null == request.getPreferredReviewSiteType()) {
                practice.setPreferredReviewSiteType(reviewSites.iterator().next().getType());
            }
            else
                practice.setPreferredReviewSiteType(request.getPreferredReviewSiteType());
        }

        practiceMapper.attachConfigToPractice(request.getWidgetConfiguration(), practice);

        final Practice result = practiceRepository.save(practice);

        practice.getDoctors().stream().map(DoctorProfile::getCustomer).forEach(d ->
                d.addAuthority(AuthorityRole.getPracticeAuthority(result.getId())));

        final PracticeDto dto = practiceMapper.toDto(result);
        eventPublisher.publishEvent(PracticeUpdateEvent.ofNew(dto));

        return dto;
    }

    @Transactional(readOnly = false)
    public void addDoctor(final long practiceId, final long customerId)
    {
        final Practice practice = findOneNullsafe(practiceId);

        if (practice.getDoctors().stream().anyMatch(d -> d.getCustomer().getId() == customerId)) {
            return;
        }

        final Customer customer = customerRepository.findOne(customerId);

        if (null == customer) {
            throw new EntityNotFoundException("Customer", String.valueOf(customerId));
        }

        final DoctorProfile profile = createProfileIfNotExist(customer);
        appendProfileToPractice(practice, profile);

        customer.addAuthority(AuthorityRole.getPracticeAuthority(practiceId));
        eventPublisher.publishEvent(PracticeUpdateEvent.ofUpdated(practiceId));
    }

    @Transactional(readOnly = false)
    public void addDoctor(final long practiceId, final CreateCustomerDto request)
    {
        final Practice practice = findOneNullsafe(practiceId);
        Customer customer = customerRepository.findOneByEmail(request.getEmail());

        if (null == customer) {
            if (null == request.getPassword() || request.getPassword().isEmpty()) {
                throw new InvalidArgumentException("password", "Doctor must have a password");
            }

            customer = customerMapper.fromRequest(request);
            customerRepository.save(customer);
        }

        customer.addAuthority(AuthorityRole.ROLE_CUSTOMER);
        customer.addAuthority(AuthorityRole.getPracticeAuthority(practiceId));
        authorityRepository.save(customer.getRoles());

        final DoctorProfile profile = createProfileIfNotExist(customer);
        appendProfileToPractice(practice, profile);

        eventPublisher.publishEvent(PracticeUpdateEvent.ofUpdated(practiceId));
    }

    @Transactional(readOnly = false)
    public void removeDoctor(final long practiceId, final long customerId)
    {
        final Practice practice = findOneNullsafe(practiceId);
        final DoctorProfile doctor = profileRepository.findOneByCustomerId(customerId);

        if (null == doctor) {
            return;
        }

        practice.getDoctors().remove(doctor);
        practice.getTreatmentTypes().forEach(tt -> tt.getDoctors().removeIf(d -> d.getCustomer().getId() == customerId));

        doctor.getCustomer().removeAuthority(AuthorityRole.getPracticeAuthority(practiceId));

        practice.getResources().stream()
                .filter(r -> r.getType().equals(ResourceType.DOCTOR))
                .filter(r -> r.getSystemId() == customerId)
                .findFirst()
                .ifPresent(r -> practice.getResources().remove(r));

        eventPublisher.publishEvent(PracticeUpdateEvent.ofUpdated(practiceId));
    }

    @Transactional(readOnly = true)
    public PracticeDto getPractice(final long practiceId)
    {
        return practiceMapper.toDto(findOneNullsafe(practiceId));
    }

    @Transactional(readOnly = true)
    public PracticesPage getPractices(final Predicate predicate, final Pageable pageable)
    {
        final Page<Practice> practices = practiceRepository.findAll(predicate, pageable);
        final PageDto page = new PageDto();
        page.setCurrentPage(pageable.getPageNumber());
        page.setPageSize(pageable.getPageSize());
        page.setTotalItems(practices.getTotalElements());
        page.setTotalPages(practices.getTotalPages());

        final PracticesPage result = new PracticesPage();
        result.setPage(page);
        result.setElements(practices.getContent().stream()
                .map(practiceMapper::toDto)
                .collect(Collectors.toList()));

        return result;
    }

    @Transactional(readOnly = false)
    public PracticeDto updatePractice(final long practiceId, final UpdatePracticeDto update)
    {
        final Practice practice = findOneNullsafe(practiceId);

        if (null != update.getName() && !update.getName().isEmpty()) {
            practice.setName(update.getName());
        }

        if (null != update.getInsuranceTypes()) {
            practice.getInsuranceTypes().clear();
            practice.getInsuranceTypes().addAll(update.getInsuranceTypes());
        }

        // @TODO move to review site mapper or service, or use practice mapper likewise with widget config?
        if (null != update.getReviewSites()) {
            if (!practice.getReviewSites().isEmpty()) {
                practice.getReviewSites().clear();
            }

            List<ReviewSite> reviewSites = update.getReviewSites().stream()
                    .map(reviewSite -> reviewSiteMapper.fromRequest(practice, reviewSite))
                    .collect(Collectors.toList());

            practice.getReviewSites().addAll(reviewSites);

            if (!reviewSites.isEmpty() && null == update.getPreferredReviewSiteType()) {
                practice.setPreferredReviewSiteType(reviewSites.iterator().next().getType());
            }
        }

        if (null != update.getPreferredReviewSiteType()) {
            practice.setPreferredReviewSiteType(update.getPreferredReviewSiteType());
        }

        practiceMapper.attachConfigToPractice(update.getWidgetConfiguration(), practice);

        if (null != update.getPaidFeatures()) {
            paidFeatureService.updatePaidFeaturesOnPractice(practice, update.getPaidFeatures());
        }

        final PracticeUpdateEvent event = PracticeUpdateEvent.ofUpdated(practiceId);
        eventPublisher.publishEvent(event);

        return practiceMapper.toDto(practice);
    }

    @Transactional(readOnly = false)
    public void updateDoctor(final long practiceId, final long customerId, final UpdateDoctorDto update)
    {
        final Practice practice = findOneNullsafe(practiceId);
        final DoctorProfile profile = Optional.ofNullable(profileRepository.findOneByCustomerId(customerId))
                .orElseThrow(() -> new EntityNotFoundException("Customer", customerId));

        if (null != update.getBookingTimeOffset()) {
            profile.setBookingTimeOffset(update.getBookingTimeOffset());
        }
        if (null != update.getBookable()) {
            profile.setBookable(update.getBookable());
        }
        if (null != update.getMedicalSpecialties()) {
            profile.getMedicalSpecialties().clear();
            profile.getMedicalSpecialties().addAll(update.getMedicalSpecialties());
        }
        if (null != update.getSpokenLanguages()) {
            profile.getCustomer().getSpokenLanguages().clear();
            profile.getCustomer().getSpokenLanguages().addAll(update.getSpokenLanguages());
        }
        if (null != update.getTreatmentTypes()) {
            practice.getTreatmentTypes().stream()
                    .filter(tt -> update.getTreatmentTypes().contains(tt.getId()))
                    .filter(tt -> !tt.getDoctors().contains(profile))
                    .forEach(tt -> tt.getDoctors().add(profile));

            practice.getTreatmentTypes().stream()
                    .filter(tt -> !update.getTreatmentTypes().contains(tt.getId()))
                    .filter(tt -> tt.getDoctors().contains(profile))
                    .forEach(tt -> tt.getDoctors().remove(profile));
        }

        final PracticeUpdateEvent event = PracticeUpdateEvent.ofUpdated(practiceId);
        eventPublisher.publishEvent(event);
    }

    @Transactional(readOnly = true)
    public Collection<DoctorProfileDto> getDoctors(final long practiceId)
    {
        final Practice practice = findOneNullsafe(practiceId);

        return practice.getDoctors().stream()
                .map(doctor -> customerMapper.toDoctorProfileDto(doctor, practice.getTreatmentTypes().stream()
                        .filter(tt -> tt.getDoctors().stream().anyMatch(d -> d.getCustomer().getId() == doctor.getCustomer().getId()))
                        .collect(Collectors.toList())))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    private void addDoctorResource(final Practice practice, final DoctorProfile doctor)
    {
        final Resource doctorResource = new Resource();
        doctorResource.setType(ResourceType.DOCTOR);
        doctorResource.setName(String.format("%s %s", doctor.getCustomer().getFirstName(), doctor.getCustomer().getLastName()));
        doctorResource.setSystemId(doctor.getCustomer().getId());
        doctorResource.setPractice(practice);

        practice.getResources().add(doctorResource);
    }

    private DoctorProfile createProfileIfNotExist(final Customer doctor)
    {
        return Optional.ofNullable(profileRepository.findOneByCustomerId(doctor.getId())).orElseGet(() -> {
            final DoctorProfile newProfile = new DoctorProfile();
            newProfile.setCustomer(doctor);

            return profileRepository.save(newProfile);
        });
    }


    private Practice findOneNullsafe(final long practiceId)
    {
        final Practice practice = practiceRepository.findOne(practiceId);

        if (null == practice) {
            throw new EntityNotFoundException("Practice", practiceId);
        }

        return practice;
    }

    private void appendProfileToPractice(Practice practice, DoctorProfile profile) {
        practice.addDoctor(profile);
        addDoctorResource(practice, profile);
    }
}
