package com.arzttermine.service.profile.support;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.persistence.AttributeConverter;
import java.sql.Timestamp;
import java.time.Instant;

public final class OAuthAuthorityConverter implements AttributeConverter<GrantedAuthority, String>
{
    public String convertToDatabaseColumn(GrantedAuthority attribute)
    {
        return attribute == null ? null : attribute.getAuthority();
    }

    public GrantedAuthority convertToEntityAttribute(String dbData)
    {
        return dbData == null || dbData.isEmpty() ? null : new SimpleGrantedAuthority(dbData);
    }
}
