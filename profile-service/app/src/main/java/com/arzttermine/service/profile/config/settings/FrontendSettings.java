package com.arzttermine.service.profile.config.settings;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.Collection;

@Configuration
@ConfigurationProperties(
    locations = "classpath:${spring.profiles.active:default}/clients.yml",
    prefix = "frontend"
)
public class FrontendSettings
{
    private String host;
    private String scheme = "https";
    private Collection<String> path = new ArrayList<>();

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Collection<String> getPath() {
        return path;
    }

    public void setPath(Collection<String> path) {
        this.path = path;
    }
}
