package com.arzttermine.service.profile.service;

import com.arzttermine.library.suport.exception.EntityNotFoundException;
import com.arzttermine.service.profile.domain.entity.Customer;
import com.arzttermine.service.profile.domain.entity.PasswordReset;
import com.arzttermine.service.profile.domain.repository.CustomerRepository;
import com.arzttermine.service.profile.domain.repository.PasswordResetRepository;
import com.arzttermine.service.profile.service.event.model.PasswordResetEvent;
import com.arzttermine.service.profile.service.mapper.CustomerMapper;
import com.arzttermine.service.profile.web.dto.request.PasswordResetRequestDto;
import com.arzttermine.service.profile.web.dto.request.UpdatePasswordDto;
import com.arzttermine.service.profile.web.dto.response.PasswordResetDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.UUID;

@Service
public class PasswordResetService
{
    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private PasswordResetRepository resetRepository;

    @Autowired
    private CustomerMapper customerMapper;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Transactional(readOnly = false)
    public UUID createReset(final PasswordResetRequestDto request)
    {
        final Customer customer = Optional.ofNullable(customerRepository.findOneByEmail(request.getEmail()))
            .orElseThrow(() -> new EntityNotFoundException("Customer", request.getEmail()));

        final UUID token = UUID.randomUUID();
        final Instant validity = Instant.now().plus(14, ChronoUnit.DAYS);
        final PasswordReset reset = new PasswordReset(token, customer, validity);

        resetRepository.save(reset);
        eventPublisher.publishEvent(PasswordResetEvent.of(reset));

        return token;
    }

    @Transactional(readOnly = true)
    public PasswordResetDto getResetRequest(final UUID token, final String email)
    {
        final PasswordReset request = Optional.ofNullable(resetRepository.findOneByToken(token))
            .orElseThrow(() -> new EntityNotFoundException("PasswordReset", token));

        if (!request.getCustomer().getEmail().equals(email)) {
            throw new EntityNotFoundException("PasswordReset", token);
        }

        final PasswordResetDto response = new PasswordResetDto();
        response.setCustomerId(request.getCustomer().getId());
        response.setToken(request.getToken());

        return response;
    }

    @Transactional(readOnly = false)
    public void reset(final UUID token, final long customerId, final UpdatePasswordDto update)
    {
        final PasswordReset request = Optional.ofNullable(resetRepository.findOneByToken(token))
            .orElseThrow(() -> new EntityNotFoundException("PasswordReset", token));

        if (request.getCustomer().getId() != customerId) {
            throw new EntityNotFoundException("PasswordReset", token);
        }

        customerMapper.setPasswordFor(request.getCustomer(), update.getPassword());
        request.use();
    }
}
