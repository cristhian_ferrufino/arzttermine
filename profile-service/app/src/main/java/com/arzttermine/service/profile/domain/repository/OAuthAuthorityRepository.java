package com.arzttermine.service.profile.domain.repository;

import com.arzttermine.service.profile.domain.entity.OAuthClientRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OAuthAuthorityRepository extends JpaRepository<OAuthClientRole, Long>
{
}
