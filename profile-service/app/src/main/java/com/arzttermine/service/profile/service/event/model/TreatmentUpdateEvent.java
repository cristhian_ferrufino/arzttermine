package com.arzttermine.service.profile.service.event.model;

import com.arzttermine.service.profile.domain.entity.TreatmentType;

public class TreatmentUpdateEvent
{
    private final Long treatmentTypeId;
    private final TreatmentType treatmentType;

    private TreatmentUpdateEvent(long id) {
        this.treatmentTypeId = id;
        this.treatmentType = null;
    }

    private TreatmentUpdateEvent(TreatmentType newType) {
        this.treatmentTypeId = newType.getId();
        this.treatmentType = newType;
    }

    public static TreatmentUpdateEvent ofDeleted(long id) {
        return new TreatmentUpdateEvent(id);
    }

    public static TreatmentUpdateEvent of(TreatmentType treatment) {
        return new TreatmentUpdateEvent(treatment);
    }

    public Long getTreatmentTypeId() {
        return treatmentTypeId;
    }

    public TreatmentType getTreatmentType() {
        return treatmentType;
    }
}
