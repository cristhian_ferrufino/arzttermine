package com.arzttermine.service.profile.domain.repository;

import com.arzttermine.service.profile.domain.entity.Practice;
import com.arzttermine.service.profile.domain.entity.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ResourceRepository extends PagingAndSortingRepository<Resource, Long>, QueryDslPredicateExecutor<Resource>
{
    Page<Resource> findByPractice(Practice practice, Pageable pageable);

    @Query("SELECT r FROM Resource r WHERE r.id = :id AND r.practice.id = :practice")
    Resource findOneByPracticeId(@Param("id") long id, @Param("practice") long practice);
}
