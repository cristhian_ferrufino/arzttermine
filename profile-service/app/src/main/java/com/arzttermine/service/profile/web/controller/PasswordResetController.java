package com.arzttermine.service.profile.web.controller;

import com.arzttermine.service.profile.service.PasswordResetService;
import com.arzttermine.service.profile.web.dto.request.PasswordResetRequestDto;
import com.arzttermine.service.profile.web.dto.request.UpdatePasswordDto;
import com.arzttermine.service.profile.web.dto.response.PasswordResetDto;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/passwords")
public class PasswordResetController
{
    @Autowired
    private PasswordResetService resetService;

    @ApiOperation("initialises a new password-reset")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAnyRole('ROLE_CLIENT', 'ROLE_EMPLOYEE')")
    @RequestMapping(value = "/requests", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void createResetRequest(@RequestBody @Valid PasswordResetRequestDto dto, HttpServletResponse response)
    {
        final UUID token = resetService.createReset(dto);
        response.setHeader("X-Reset-Token", token.toString());
    }

    @ApiOperation("returns the password-reset request")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("#oauth2.hasScope('reset-password') or hasAnyRole('ROLE_EMPLOYEE', 'ROLE_SYSTEM')")
    @RequestMapping(value = "/requests/{token}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public PasswordResetDto getResetRequest(@PathVariable UUID token, @RequestParam String email)
    {
        return resetService.getResetRequest(token, email);
    }

    @ApiOperation("resets the password using a reset request")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("#oauth2.hasScope('reset-password') or hasAnyRole('ROLE_EMPLOYEE', 'ROLE_SYSTEM')")
    @RequestMapping(value = "/requests/{token}/customers/{customerId}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void resetPassword(@PathVariable UUID token, @PathVariable long customerId, @RequestBody @Valid UpdatePasswordDto update)
    {
        resetService.reset(token, customerId, update);
    }
}
