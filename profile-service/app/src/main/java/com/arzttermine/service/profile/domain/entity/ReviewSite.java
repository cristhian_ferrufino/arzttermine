package com.arzttermine.service.profile.domain.entity;

import javax.persistence.*;

@Entity
@Table(
    name = "practice_review_sites",
    indexes = {
        @Index(name = "id_review_site_practice_id", columnList = "practice_id")
    }
)
public class ReviewSite {

    @Id
    @Column(nullable = false, updatable = false, insertable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String type;

    @Column(nullable = false)
    private String data;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "practice_id", nullable = false, updatable = false)
    private Practice practice;


    public long getId() {
        return this.id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Practice getPractice() {
        return practice;
    }

    public void setPractice(Practice practice) {
        this.practice = practice;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReviewSite that = (ReviewSite) o;

        if (id != that.id) return false;
        if (!type.equals(that.type)) return false;
        return data.equals(that.data);
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + type.hashCode();
        result = 31 * result + data.hashCode();
        return result;
    }
}
