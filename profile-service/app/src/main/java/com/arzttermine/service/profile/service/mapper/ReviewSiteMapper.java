package com.arzttermine.service.profile.service.mapper;

import com.arzttermine.service.profile.domain.entity.Practice;
import com.arzttermine.service.profile.domain.entity.ReviewSite;
import com.arzttermine.service.profile.web.dto.request.CreateReviewSiteDto;
import com.arzttermine.service.profile.web.dto.response.ReviewSiteDto;
import org.springframework.stereotype.Component;

@Component
public class ReviewSiteMapper {

    public ReviewSiteDto toDto(final ReviewSite reviewSite) {
        final ReviewSiteDto dto = new ReviewSiteDto();
        dto.setId(reviewSite.getId());
        dto.setType(reviewSite.getType());
        dto.setData(reviewSite.getData());
        return dto;
    }

    public ReviewSite fromRequest(final Practice practice, final CreateReviewSiteDto request) {
        final ReviewSite reviewSite = new ReviewSite();
        reviewSite.setData(request.getData());
        reviewSite.setType(request.getType());
        reviewSite.setPractice(practice);
        return reviewSite;
    }

}
