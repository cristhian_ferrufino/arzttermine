package com.arzttermine.service.profile.domain.entity;

import com.arzttermine.library.web.struct.InsuranceType;
import com.arzttermine.library.web.struct.Language;
import com.arzttermine.service.profile.support.InstantToTimestampConverter;
import com.arzttermine.service.profile.web.struct.AuthorityRole;
import com.arzttermine.service.profile.web.struct.Gender;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(
    name = "customers",
    indexes = {
        @Index(name = "id_customer_email", columnList = "email", unique = true)
    }
)
public class Customer implements UserDetails
{
    private static final int CREDENTIALS_VALIDITY = (3600 * 24) * 365;

    @Id
    @Column(nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    private String password;

    @Column(nullable = false, updatable = false)
    private String username;

    @Column(nullable = false)
    private String email;

    @Column
    private String title;

    @Column(nullable = false)
    private boolean enabled = true;

    @Enumerated(EnumType.STRING)
    @Column
    private Gender gender;

    private String phone;

    @Column(name = "phone_mobile")
    private String phoneMobile;

    @Convert(converter = InstantToTimestampConverter.class)
    @Column(name = "last_credentials_update", nullable = false)
    private Instant credentialsUpdate = Instant.now();

    @Convert(converter = InstantToTimestampConverter.class)
    @Column(name = "credentials_expired_notification", nullable = false)
    private Instant credentialsExpiredNotification = Instant.now();

    @JoinColumn(name = "client_id", nullable = false, updatable = false, insertable = false)
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<OAuthClientRole> authorities = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinTable(
        name = "customer_locations",
        joinColumns = {@JoinColumn(name = "customer_id", referencedColumnName = "id")},
        inverseJoinColumns = {@JoinColumn(name = "location_id", referencedColumnName = "id", unique = false)}
    )
    private Set<Location> locations = new HashSet<>();

    @Enumerated(EnumType.STRING)
    @Column(name = "default_insurance_type")
    private InsuranceType defaultInsuranceType;

    @Enumerated(EnumType.STRING)
    @Column(name = "preferred_language")
    private Language preferredLanguage;

    @Column(name = "language", nullable = false)
    @ElementCollection(fetch = FetchType.EAGER)
    @Enumerated(EnumType.STRING)
    @CollectionTable(
        name = "customers_languages",
        joinColumns = {@JoinColumn(name = "customer_id")}
    )
    private Set<Language> spokenLanguages = new HashSet<>();

    @Convert(converter = InstantToTimestampConverter.class)
    @Column(name = "created_at", nullable = false, updatable = false)
    private Instant createdAt = Instant.now();

    @Convert(converter = InstantToTimestampConverter.class)
    @Column(name = "updated_at", nullable = false)
    private Instant updatedAt = Instant.now();

    public long getId() {
        return id;
    }

    @PreUpdate
    public void beforeUpdate() {
        updatedAt = Instant.now();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities.stream().map(OAuthClientRole::getRole).collect(Collectors.toList());
    }

    public Collection<OAuthClientRole> getRoles() {
        return authorities;
    }

    public Customer addAuthority(AuthorityRole authority) {
        final String role = authority.name();

        return addAuthority(role);
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password, Instant at) {
        this.password = password;
        this.credentialsUpdate = at;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return enabled;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return null == password || credentialsUpdate.plusSeconds(CREDENTIALS_VALIDITY).isAfter(Instant.now());
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @return Instant representing the time when the customer was notified about expired credentials.
     *                 This is used to determine whether we have to send a notification or not.
     */
    public Instant getCredentialsExpiredNotificationTime() {
        return credentialsExpiredNotification;
    }

    public void credentialsExpiredNotificationSentAt(Instant time) {
        this.credentialsExpiredNotification = time;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public Instant getUpdatedAt() {
        return updatedAt;
    }

    public boolean isDoctor() {
        return authorities.stream().anyMatch(a ->
            a.getRole().getAuthority().equals(AuthorityRole.ROLE_DOCTOR.name()));
    }

    public void removeAuthority(String authority) {
        authorities.stream()
            .filter(a -> a.getRole().getAuthority().equals(authority))
            .findFirst()
            .ifPresent(a -> authorities.remove(a));
    }

    public Customer addAuthority(String authority) {
        if (authorities.stream().noneMatch(a -> a.getRole().getAuthority().equals(authority))) {
            authorities.add(OAuthClientRole.builder().role(authority).clientId(id).build());
        }

        return this;
    }

    public boolean hasAuthority(AuthorityRole role) {
        return authorities.stream()
            .map(OAuthClientRole::getRole)
            .anyMatch(r -> r.getAuthority().equals(role.name()));
    }

    public Set<Location> getLocations() {
        return locations;
    }

    public InsuranceType getDefaultInsuranceType() {
        return defaultInsuranceType;
    }

    public void setDefaultInsuranceType(InsuranceType defaultInsuranceType) {
        this.defaultInsuranceType = defaultInsuranceType;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Language getPreferredLanguage() {
        return preferredLanguage;
    }

    public void setPreferredLanguage(Language preferredLanguage) {
        this.preferredLanguage = preferredLanguage;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoneMobile() {
        return phoneMobile;
    }

    public void setPhoneMobile(String phoneMobile) {
        this.phoneMobile = phoneMobile;
    }

    public boolean hasPassword() {
        return null != password && !password.isEmpty();
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Set<Language> getSpokenLanguages() {
        return spokenLanguages;
    }

    public @Transient String getFullName() {

        String salutation = "";

        if (gender == Gender.MALE)
            salutation = "Herr";
        else if (gender == Gender.FEMALE)
            salutation = "Frau";

        if (null == title) {
            return String.format("%s %s %s", salutation, firstName, lastName);
        }

        return String.format("%s %s %s %s", salutation, title, firstName, lastName);
    }
}
