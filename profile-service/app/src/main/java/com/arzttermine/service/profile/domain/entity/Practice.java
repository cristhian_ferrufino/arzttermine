package com.arzttermine.service.profile.domain.entity;

import com.arzttermine.library.web.struct.InsuranceType;
import com.arzttermine.service.profile.web.struct.AuthorityRole;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.*;

@Entity
@Table(
    name = "practices",
    indexes = {
        @Index(name = "id_practice_owner_id", columnList = "practice_owner_id")
    }
)
public class Practice
{
    @Id
    @Column(nullable = false, updatable = false, insertable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String name;

    @NotNull
    @LazyToOne(LazyToOneOption.FALSE)
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "location_id", nullable = false)
    private Location location;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "practice_owner_id")
    private Customer owner;

    @JoinColumn(name = "doctor_id")
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    private Set<DoctorProfile> doctors = new HashSet<>();

    @BatchSize(size = 15)
    @OneToMany(mappedBy = "practice", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Resource> resources = new HashSet<>();

    @BatchSize(size = 10)
    @OneToMany(mappedBy = "practice", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<PaidFeatureAssignment> paidFeatureAssignments = new HashSet<>();

    @OneToMany(mappedBy = "practice", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<ReviewSite> reviewSites = new HashSet<>();

    @ElementCollection
    @Enumerated(EnumType.STRING)
    @Column(name = "insurance_type", nullable = false)
    @LazyCollection(LazyCollectionOption.FALSE)
    @CollectionTable(
        name = "practice_insurance_types",
        joinColumns = @JoinColumn(name = "practice_id", nullable = false)
    )
    private Set<InsuranceType> insuranceTypes = new HashSet<>();

    @Column(name = "preferred_review_site_type", nullable = true)
    private String preferredReviewSiteType;

    @JoinColumn(name = "widget_configuration_id")
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private WidgetConfiguration widgetConfiguration;

    @Where(clause = "enabled = 1")
    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany(mappedBy = "practiceId", cascade = CascadeType.ALL)
    private Set<TreatmentType> treatmentTypes = new HashSet<>();

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Customer getOwner() {
        return owner;
    }

    public void setOwner(DoctorProfile owner) {
        this.owner = Optional.ofNullable(owner).map(DoctorProfile::getCustomer).orElse(null);

        if (null != owner && !doctors.contains(owner)) {
            addDoctor(owner);
        }
    }

    public Set<DoctorProfile> getDoctors() {
        return doctors;
    }

    public void addDoctor(DoctorProfile profile) {

        final Customer doctor = profile.getCustomer();
        if (!doctor.isDoctor())
            doctor.addAuthority(AuthorityRole.ROLE_DOCTOR);

        if (!doctors.contains(profile))
            doctors.add(profile);
    }

    public void addReviewSite(ReviewSite reviewSite) {
        if (!reviewSites.contains(reviewSite))
            reviewSites.add(reviewSite);

        if (null == preferredReviewSiteType)
            preferredReviewSiteType = reviewSite.getType();
    }

    public void addInsuranceType(InsuranceType insuranceType) {
        if (!insuranceTypes.contains(insuranceType))
            insuranceTypes.add(insuranceType);
    }

    public void assignPaidFeature(PaidFeature paidFeature, Map<String, String> options) {
        PaidFeatureAssignment paidFeatureAssignment = new PaidFeatureAssignment();
        paidFeatureAssignment.setPaidFeature(paidFeature);
        paidFeatureAssignment.getOptions().putAll(options);
        this.paidFeatureAssignments.add(paidFeatureAssignment);
        paidFeatureAssignment.setPractice(this);
    }


    public Set<Resource> getResources() {
        return resources;
    }

    public Set<TreatmentType> getTreatmentTypes() {
        return treatmentTypes;
    }

    public Set<PaidFeatureAssignment> getPaidFeatureAssignments() {
        return paidFeatureAssignments;
    }

    public Set<ReviewSite> getReviewSites() {
        return reviewSites;
    }

    public Set<InsuranceType> getInsuranceTypes() {
        return insuranceTypes;
    }

    public void setReviewSites(Set<ReviewSite> reviewSites) {
        this.reviewSites = reviewSites;

        if (null == preferredReviewSiteType && !reviewSites.isEmpty())
            preferredReviewSiteType = reviewSites.iterator().next().getType();

    }

    public String getPreferredReviewSiteType() {
        return preferredReviewSiteType;
    }

    public void setPreferredReviewSiteType(String preferredReviewSiteType) {
        this.preferredReviewSiteType = preferredReviewSiteType;
    }

    public ReviewSite getPreferredReviewSite() {
        ReviewSite preferredReviewSite = null;
        if (null != preferredReviewSiteType)
            preferredReviewSite = reviewSites.stream()
                .filter(reviewSite -> reviewSite.getType().equals(preferredReviewSiteType))
                .findFirst()
                .get();

        return preferredReviewSite;
    }

    public WidgetConfiguration getWidgetConfiguration() {
        if (null == widgetConfiguration) {
            widgetConfiguration = new WidgetConfiguration();
        }

        return widgetConfiguration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Practice practice = (Practice) o;

        return id == practice.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "Practice{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", location=" + location +
                ", owner=" + owner +
                ", doctors=" + doctors +
                ", resources=" + resources +
                ", paidFeatureAssignments=" + paidFeatureAssignments +
                ", reviewSites=" + reviewSites +
                ", preferredReviewSiteType='" + preferredReviewSiteType + '\'' +
                ", widgetConfiguration=" + widgetConfiguration +
                ", treatmentTypes=" + treatmentTypes +
                '}';
    }

}
