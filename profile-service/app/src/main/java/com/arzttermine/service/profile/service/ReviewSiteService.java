package com.arzttermine.service.profile.service;

import com.arzttermine.library.suport.exception.EntityNotFoundException;
import com.arzttermine.service.profile.domain.entity.ReviewSite;
import com.arzttermine.service.profile.domain.repository.ReviewSiteRepository;
import com.arzttermine.service.profile.service.mapper.ReviewSiteMapper;
import com.arzttermine.service.profile.web.dto.response.ReviewSiteDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ReviewSiteService
{
    @Autowired
    private ReviewSiteRepository reviewSiteRepository;

    @Autowired
    private ReviewSiteMapper reviewSiteMapper;

    @Transactional(readOnly = true)
    public ReviewSiteDto getReviewSiteByPracticeId(final long practiceId) {
        final ReviewSite reviewSite = reviewSiteRepository.findFirstByPracticeId(practiceId);

        if (null == reviewSite)
            throw new EntityNotFoundException("Practice", practiceId);

        return reviewSiteMapper.toDto(reviewSite);
    }
}
