package com.arzttermine.service.profile.domain.repository;

import com.arzttermine.service.profile.domain.entity.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationRepository extends JpaRepository<Location, Long>
{
}
