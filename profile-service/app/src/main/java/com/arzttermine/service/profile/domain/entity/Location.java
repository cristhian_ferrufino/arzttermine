package com.arzttermine.service.profile.domain.entity;

import com.arzttermine.service.profile.web.struct.Country;

import javax.persistence.*;

@Entity
@Table(
    name = "locations",
    uniqueConstraints = {
        @UniqueConstraint(name = "un_location_per_coordinate", columnNames = {"longitude", "latitude"})
    }
)
public class Location
{
    @Id
    @Column(nullable = false, updatable = false, insertable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, updatable = false)
    private Country country = Country.GERMANY;

    @Column(nullable = false)
    private float longitude;

    @Column(nullable = false)
    private float latitude;

    @Column(nullable = false)
    private String location;

    @Column(nullable = false)
    private String zip;

    @Column(nullable = false)
    private String city;

    private String phone;

    @Column(name = "phone_mobile")
    private String phoneMobile;

    @Column(name = "contact_mail")
    private String contactMail;

    @Column(name = "primary_location", nullable = false)
    private boolean primary = false;

    protected Location() {
    }

    public Location(Country country) {
        this.country = country;
    }

    public long getId() {
        return id;
    }

    public Country getCountry() {
        return country;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoneMobile() {
        return phoneMobile;
    }

    public void setPhoneMobile(String phoneMobile) {
        this.phoneMobile = phoneMobile;
    }

    public String getContactMail() {
        return contactMail;
    }

    public void setContactMail(String contactMail) {
        this.contactMail = contactMail;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public boolean isPrimary() {
        return primary;
    }

    public void setPrimary(boolean primary) {
        this.primary = primary;
    }
}
