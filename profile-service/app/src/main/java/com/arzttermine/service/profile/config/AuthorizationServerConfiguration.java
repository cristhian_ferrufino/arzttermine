package com.arzttermine.service.profile.config;

import com.arzttermine.service.profile.oauth2.OAuth2ClientDetailsService;
import com.arzttermine.service.profile.oauth2.granter.UsernamePasswordTokenGranter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.CompositeTokenGranter;
import org.springframework.security.oauth2.provider.OAuth2RequestFactory;
import org.springframework.security.oauth2.provider.TokenGranter;
import org.springframework.security.oauth2.provider.client.ClientCredentialsTokenGranter;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestFactory;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;

import java.util.Arrays;

@Configuration
// @EnableOAuth2Sso no need for that atm
@EnableOAuth2Client
@EnableAuthorizationServer
public class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter
{
    @Autowired
    private DefaultTokenServices tokenServices;

    @Autowired
    private OAuth2ClientDetailsService clientDetailsService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer configurer)
    {
        configurer
            .authenticationManager(authenticationManager)
            .tokenGranter(getCompositeGranter())
            .tokenServices(tokenServices)
            .reuseRefreshTokens(false);
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception
    {
        oauthServer
            .checkTokenAccess("isAuthenticated()")
            .allowFormAuthenticationForClients();
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer configurer) throws Exception
    {
        configurer
            .withClientDetails(clientDetailsService)
            .clients(clientDetailsService);
    }

    private TokenGranter getCompositeGranter()
    {
        final OAuth2RequestFactory requestFactory = new DefaultOAuth2RequestFactory(clientDetailsService);

        final ClientCredentialsTokenGranter clientCredentialsTokenGranter = new ClientCredentialsTokenGranter(
            tokenServices,
            clientDetailsService,
            requestFactory
        );

        final UsernamePasswordTokenGranter passwordTokenGranter = new UsernamePasswordTokenGranter(
            authenticationManager,
            tokenServices,
            clientDetailsService,
            requestFactory
        );

        return new CompositeTokenGranter(Arrays.asList(
            clientCredentialsTokenGranter,
            passwordTokenGranter
        ));
    }
}
