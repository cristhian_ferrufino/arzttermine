package com.arzttermine.service.profile.web.advice;

import com.arzttermine.library.web.dto.response.ErrorDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
public class PayloadArgumentAdvice extends AbstractAdvice
{
    @ExceptionHandler
    public ResponseEntity<List<ErrorDto>> handleMethodArgumentNotValidException(MethodArgumentNotValidException exc)
    {
        final List<ErrorDto> errors = exc.getBindingResult()
            .getFieldErrors()
            .stream()
            .map(err -> getErrorDto("INVALID_ARGUMENT", err.getField(),err.getField() + " argument is not valid"))
            .collect(Collectors.toList());

        errors.addAll(exc.getBindingResult()
            .getGlobalErrors()
            .stream()
            .map((err) -> getErrorDto("INVALID_ARGUMENT", err.getObjectName(),err.getObjectName() + " argument is not valid"))
            .collect(Collectors.toList()));

        return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public ResponseEntity<List<ErrorDto>> handleHttpMessageConversionException(HttpMessageConversionException exc)
    {
        final ErrorDto error = getErrorDto("JSON_PARSER_ERROR", "request", exc.getMessage());

        return new ResponseEntity<>(Collections.singletonList(error), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public ResponseEntity<List<ErrorDto>> handleHttpMediaTypeNotSupportedException(HttpMediaTypeNotSupportedException exc)
    {
        final ErrorDto error = getErrorDto("MEDIA_TYPE_NOT_SUPPORTED", "request", exc.getMessage());

        return new ResponseEntity<>(Collections.singletonList(error), HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }
}
