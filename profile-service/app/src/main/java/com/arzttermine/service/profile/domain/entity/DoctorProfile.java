package com.arzttermine.service.profile.domain.entity;

import com.arzttermine.service.profile.web.struct.MedicalSpecialtyType;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.validator.constraints.Range;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(
    name = "doctor_profiles"
)
public class DoctorProfile
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false, insertable = false)
    private long id;

    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "customer_id", nullable = false, updatable = false)
    private Customer customer;

    @Range(min = 1, max =  9999)
    @Column(name = "booking_time_offset", nullable = false)
    private int bookingTimeOffset = 1;

    @ElementCollection
    @Enumerated(EnumType.STRING)
    @Column(name = "specialty_type")
    @LazyCollection(LazyCollectionOption.FALSE)
    @CollectionTable(
        name = "doctors_medical_specialties",
        joinColumns = @JoinColumn(name = "doctor_profile_id"),
        uniqueConstraints = @UniqueConstraint(
            name = "uk_specialty_type_per_profile_id",
            columnNames = {"specialty_type", "doctor_profile_id"}
        )
    )
    private Set<MedicalSpecialtyType> medicalSpecialties = new HashSet<>();

    @Column(nullable = false)
    private boolean bookable = true;

    public long getId() {
        return id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public int getBookingTimeOffset() {
        return bookingTimeOffset;
    }

    public void setBookingTimeOffset(int bookingTimeOffset) {
        this.bookingTimeOffset = bookingTimeOffset;
    }

    public Set<MedicalSpecialtyType> getMedicalSpecialties() {
        return medicalSpecialties;
    }

    public boolean isBookable() {
        return bookable;
    }

    public void setBookable(boolean bookable) {
        this.bookable = bookable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DoctorProfile that = (DoctorProfile) o;

        if (id != that.id) return false;
        return customer != null ? customer.equals(that.customer) : that.customer == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (customer != null ? customer.hashCode() : 0);
        return result;
    }
}
