package com.arzttermine.service.profile.oauth2;

import com.arzttermine.service.profile.domain.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class OAuth2ClientDetailsService implements ClientDetailsService
{
    @Autowired
    private ClientRepository clientRepository;

    @Override
    public ClientDetails loadClientByClientId(String s) throws ClientRegistrationException {
        return Optional.ofNullable(clientRepository.findByClientId(s))
            .orElseThrow(() -> new ClientRegistrationException(String.format("Client %s does not exist", s)));
    }
}
