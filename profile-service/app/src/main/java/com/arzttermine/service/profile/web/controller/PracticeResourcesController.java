package com.arzttermine.service.profile.web.controller;

import com.arzttermine.service.profile.domain.entity.Resource;
import com.arzttermine.service.profile.service.ResourceService;
import com.arzttermine.service.profile.web.dto.request.CreateResourceDto;
import com.arzttermine.service.profile.web.dto.response.ResourceDto;
import com.arzttermine.service.profile.web.dto.response.ResourcesPage;
import com.querydsl.core.types.Predicate;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@RestController
@RequestMapping("/practices/{practice}/resources")
public class PracticeResourcesController
{
    @Autowired
    private ResourceService resourceService;

    @ApiOperation("Creates new resources for the corresponding practice")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        ),
        @ApiImplicitParam(
            name = "practice",
            dataType = "long",
            required = true,
            paramType = "path"
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_CREATED, message = "resource was created"),
        @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "request body is not valid"),
        @ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "practice was not found")
    })
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasAnyRole('ROLE_EMPLOYEE', 'ROLE_PRACTICE_' + #practice)")
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResourceDto addResource(@PathVariable long practice, @RequestBody @Valid CreateResourceDto request)
    {
        return resourceService.createResource(practice, request);
    }

    @ApiOperation("Removes the requested resource id")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        ),
        @ApiImplicitParam(
            name = "practice",
            dataType = "long",
            required = true,
            paramType = "path"
        ),
        @ApiImplicitParam(
            name = "resourceId",
            dataType = "long",
            required = true,
            paramType = "path"
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_NO_CONTENT, message = "resource was removed or not found"),
        @ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "practice was not found")
    })
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAnyRole('ROLE_EMPLOYEE', 'ROLE_PRACTICE_' + #practice)")
    @RequestMapping(value = "/{resourceId}", method = RequestMethod.DELETE)
    public void removeResource(@PathVariable long practice, @PathVariable long resourceId)
    {
        resourceService.removeResource(practice, resourceId);
    }

    @ApiOperation("Updates the requested resource id. This is a PATCH request, hence you have to provide ALL values")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        ),
        @ApiImplicitParam(
            name = "practice",
            dataType = "long",
            required = true,
            paramType = "path"
        ),
        @ApiImplicitParam(
            name = "resourceId",
            dataType = "long",
            required = true,
            paramType = "path"
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_NO_CONTENT, message = "resource was updated"),
        @ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "resource or practice was not found")
    })
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAnyRole('ROLE_EMPLOYEE', 'ROLE_PRACTICE_' + #practice)")
    @RequestMapping(value = "/{resourceId}", method = RequestMethod.PATCH, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateResource(@PathVariable long practice, @PathVariable long resourceId, @RequestBody @Valid CreateResourceDto request)
    {
        resourceService.updateResource(practice, resourceId, request);
    }

    @ApiOperation("Returns all resources for the corresponding practice")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        ),
        @ApiImplicitParam(
            name = "practice",
            dataType = "long",
            required = true,
            paramType = "path"
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "resources are returned"),
        @ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "practice was not found")
    })
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyRole('ROLE_SYSTEM', 'ROLE_EMPLOYEE', 'ROLE_PRACTICE_' + #practice)")
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResourcesPage getResources(@PathVariable long practice, @QuerydslPredicate(root = Resource.class) Predicate predicate, Pageable pageable)
    {
        return resourceService.getResources(practice, predicate, pageable);
    }

    @ApiOperation("Returns a single resource for the corresponding practice")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        ),
        @ApiImplicitParam(
            name = "practice",
            dataType = "long",
            required = true,
            paramType = "path"
        ),
        @ApiImplicitParam(
            name = "id",
            dataType = "long",
            required = true,
            paramType = "path"
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "resource is returned"),
        @ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "practice or resource was not found")
    })
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyRole('ROLE_SYSTEM', 'ROLE_EMPLOYEE', 'ROLE_PRACTICE_' + #practice)")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResourceDto getResource(@PathVariable long practice, @PathVariable long id)
    {
        return resourceService.getResource(practice, id);
    }
}
