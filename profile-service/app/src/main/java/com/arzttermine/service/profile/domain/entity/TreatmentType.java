package com.arzttermine.service.profile.domain.entity;

import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(
    name = "treatment_types",
    indexes = {
        @Index(name = "id_treatment_type_practice_id", columnList = "practice_id")
    },
    uniqueConstraints = {
        @UniqueConstraint(name = "un_treatment_type_practice_id", columnNames = {"practice_id", "type"})
    }
)
public class TreatmentType
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false, insertable = false)
    private long id;

    @Column(nullable = false)
    private String type;

    private Integer charge;

    @Column(name = "practice_id", nullable = false, updatable = false)
    private long practiceId;

    @Column(name = "force_payment", nullable = false)
    private boolean forcePay = false;

    private String comment;

    private boolean enabled = true;

    @BatchSize(size = 5)
    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(
        name = "treatment_type_provider",
        joinColumns = @JoinColumn(name = "treatment_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "doctor_profile_id", referencedColumnName = "id")
    )
    private Set<DoctorProfile> doctors = new HashSet<>();

    protected TreatmentType() {
    }

    public TreatmentType(long practiceId, String type) {
        this(practiceId, type, null);
    }

    public TreatmentType(long practiceId, String type, Integer charge) {
        this.type = type;
        this.charge = charge;
        this.practiceId = practiceId;
    }

    public Integer getCharge() {
        return charge;
    }

    public void setCharge(Integer charge) {
        this.charge = charge;
    }

    public long getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getPracticeId() {
        return practiceId;
    }

    public boolean isForcePay() {
        return forcePay;
    }

    public void setForcePay(boolean forcePay) {
        this.forcePay = forcePay;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Set<DoctorProfile> getDoctors() {
        return doctors;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void disable() {
        this.enabled = false;
    }
}
