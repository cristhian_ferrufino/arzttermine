package com.arzttermine.service.profile.domain.repository;

import com.arzttermine.service.profile.domain.entity.Practice;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface PracticeRepository extends PagingAndSortingRepository<Practice, Long>, QueryDslPredicateExecutor<Practice>
{
    @Query("SELECT p.id FROM Practice p INNER JOIN p.doctors c WHERE c.id = :doctorId")
    Collection<Long> findPracticeIdByDoctorProfile(@Param("doctorId") long doctorProfileId);

    @Query("SELECT p.id FROM Practice p WHERE p.location.id = :locationId")
    Collection<Long> findPracticeIdsByLocationId(@Param("locationId") long locationId);

}
