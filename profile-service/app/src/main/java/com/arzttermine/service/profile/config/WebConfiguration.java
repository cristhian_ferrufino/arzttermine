package com.arzttermine.service.profile.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.web.SortHandlerMethodArgumentResolver;
import org.springframework.data.web.config.SpringDataWebConfiguration;

@Configuration
public class WebConfiguration extends SpringDataWebConfiguration
{
    @Override
    public SortHandlerMethodArgumentResolver sortResolver() {
        SortHandlerMethodArgumentResolver resolver = super.sortResolver();
        resolver.setQualifierDelimiter(".");

        return resolver;
    }
}
