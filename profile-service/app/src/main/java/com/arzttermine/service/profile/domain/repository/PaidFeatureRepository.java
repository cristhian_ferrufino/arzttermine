package com.arzttermine.service.profile.domain.repository;

import com.arzttermine.service.profile.domain.entity.PaidFeature;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Set;

@Repository
public interface PaidFeatureRepository extends JpaRepository<PaidFeature, Long> {

    Set<PaidFeature> findByNameIn(Collection<String> name);

}
