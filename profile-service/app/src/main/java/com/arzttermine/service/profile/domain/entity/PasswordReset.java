package com.arzttermine.service.profile.domain.entity;

import com.arzttermine.service.profile.support.InstantToTimestampConverter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.Instant;
import java.util.UUID;

@Entity
@Table(name = "password_resets")
@Where(clause = "active = 1 AND (expires IS NULL OR expires >= NOW())")
public class PasswordReset
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false, insertable = false)
    private long id;

    @Type(type = "uuid-char")
    @Column(nullable = false)
    private UUID token;

    @Column(nullable = false)
    private boolean active = true;

    @Column(nullable = true, updatable = false)
    @Convert(converter = InstantToTimestampConverter.class)
    private Instant expires;

    @OneToOne
    @JoinColumn(name = "customer_id", nullable = false, updatable = false)
    private Customer customer;

    private PasswordReset() {
    }

    public PasswordReset(UUID token, Customer customer, Instant expires) {
        this.customer = customer;
        this.token = token;
        this.expires = expires;
    }

    public PasswordReset(UUID token, Customer customer) {
        this(token, customer, null);
    }

    public long getId() {
        return id;
    }

    public UUID getToken() {
        return token;
    }

    public Customer getCustomer() {
        return customer;
    }

    public boolean isActive() {
        return active;
    }

    public Instant getExpires() {
        return expires;
    }

    public void use() {
        active = false;
    }
}
