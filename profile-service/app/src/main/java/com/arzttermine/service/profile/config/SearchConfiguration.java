package com.arzttermine.service.profile.config;

import com.arzttermine.library.elasticsearch.config.ElasticsearchConfiguration;
import com.arzttermine.library.elasticsearch.config.settings.ElasticsearchSettings;
import com.arzttermine.library.elasticsearch.service.DocumentProcessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.concurrent.ExecutorService;

@Configuration
@Profile("!testing")
public class SearchConfiguration extends ElasticsearchConfiguration
{
    @Autowired
    private ElasticsearchSettings settings;

    @Autowired
    private ExecutorService executorService;

    @Autowired
    private ObjectMapper objectMapper;

    @Bean
    public DocumentProcessor documentProcessor()
    {
        return super.documentProcessor(settings, executorService, objectMapper);
    }
}
