package com.arzttermine.service.profile.web.advice;

import com.arzttermine.library.web.dto.response.ErrorDto;

abstract class AbstractAdvice
{
    ErrorDto getErrorDto(String code, Object reference, String message)
    {
        final ErrorDto error = new ErrorDto();
        error.setReference(reference);
        error.setMessage(message);
        error.setCode(code);

        return error;
    }
}
