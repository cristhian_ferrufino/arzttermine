package com.arzttermine.service.profile.util;

import com.arzttermine.service.profile.web.struct.AuthorityRole;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

public class SecurityContextUtil
{
    public static boolean validateIsEmployeeOrSystem(final OAuth2Authentication authentication)
    {
        return authentication.getAuthorities().stream()
            .map(GrantedAuthority::getAuthority)
            .anyMatch(a -> a.equals(AuthorityRole.ROLE_EMPLOYEE.name()) || a.equals(AuthorityRole.ROLE_SYSTEM.name()));
    }

    public static boolean isDoctorOrPractice(final OAuth2Authentication authentication)
    {
        return authentication.getAuthorities().stream()
            .map(GrantedAuthority::getAuthority)
            .anyMatch(a -> a.equals(AuthorityRole.ROLE_PRACTICE.name()) || a.equals(AuthorityRole.ROLE_DOCTOR.name()));
    }
}
