package com.arzttermine.service.profile.service;

import com.arzttermine.library.suport.exception.AccessDeniedException;
import com.arzttermine.library.suport.exception.EntityAlreadyExistException;
import com.arzttermine.library.suport.exception.EntityNotFoundException;
import com.arzttermine.library.web.dto.PriceDto;
import com.arzttermine.library.web.dto.response.paging.PageDto;
import com.arzttermine.service.profile.domain.entity.Practice;
import com.arzttermine.service.profile.domain.entity.QTreatmentType;
import com.arzttermine.service.profile.domain.entity.TreatmentType;
import com.arzttermine.service.profile.domain.repository.DoctorProfileRepository;
import com.arzttermine.service.profile.domain.repository.PracticeRepository;
import com.arzttermine.service.profile.domain.repository.TreatmentTypeRepository;
import com.arzttermine.service.profile.service.event.model.PracticeUpdateEvent;
import com.arzttermine.service.profile.service.event.model.TreatmentUpdateEvent;
import com.arzttermine.service.profile.service.mapper.CustomerMapper;
import com.arzttermine.service.profile.web.dto.request.CreateTreatmentTypeDto;
import com.arzttermine.service.profile.web.dto.response.TreatmentTypeDto;
import com.arzttermine.service.profile.web.dto.response.TreatmentTypesPage;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TreatmentTypeService
{
    @Autowired
    private DoctorProfileRepository profileRepository;

    @Autowired
    private TreatmentTypeRepository treatmentTypeRepository;

    @Autowired
    private CustomerMapper customerMapper;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private PracticeRepository practiceRepository;

    @Transactional(readOnly = false)
    public TreatmentTypeDto create(final long practiceId, final CreateTreatmentTypeDto request)
    {
        final boolean exists = treatmentTypeRepository.existsInPractice(practiceId, request.getType());
        if (exists) {
            throw new EntityAlreadyExistException("TreatmentType", request.getType());
        }

        final Practice practice = practiceRepository.findOne(practiceId);
        if (null == practice) {
            throw new EntityNotFoundException("Practice", practiceId);
        }

        final TreatmentType tt = new TreatmentType(practiceId, request.getType());
        tt.setCharge(Optional.ofNullable(request.getCharge()).map(PriceDto::getAmount).orElse(null));
        tt.setForcePay(Optional.ofNullable(request.getForcePay()).orElse(false));
        tt.setComment(request.getComment());

        practice.getDoctors().stream()
            .filter(d -> request.getDoctorIds().contains(d.getCustomer().getId()))
            .forEach(p -> tt.getDoctors().add(p));

        practice.getTreatmentTypes().add(tt);
        treatmentTypeRepository.save(tt);

        publishTreatmentUpdate(tt, practiceId);

        return customerMapper.toTreatmentDto(tt);
    }

    @Transactional(readOnly = false)
    public void update(final long practiceId, final long typeId, final CreateTreatmentTypeDto update)
    {
        final TreatmentType treatmentType = treatmentTypeRepository.findOne(typeId);
        if (null == treatmentType || treatmentType.getPracticeId() != practiceId) {
            throw new EntityNotFoundException("TreatmentType", typeId);
        }

        treatmentType.setCharge(Optional.ofNullable(update.getCharge()).map(PriceDto::getAmount).orElse(null));
        treatmentType.setForcePay(Optional.ofNullable(update.getForcePay()).orElse(false));
        treatmentType.setComment(update.getComment());
        treatmentType.setType(update.getType());

        publishTreatmentUpdate(treatmentType, practiceId);
    }

    @Transactional(readOnly = false)
    public void remove(final long practiceId, final long typeId)
    {
        final TreatmentType obj = treatmentTypeRepository.findOne(typeId);
        if (null == obj) {
            return;
        }
        if (obj.getPracticeId() != practiceId) {
            throw new AccessDeniedException();
        }

        obj.disable();
        publishTreatmentUpdate(TreatmentUpdateEvent.ofDeleted(typeId), practiceId);
    }

    @Transactional(readOnly = true)
    public TreatmentTypeDto get(final long practiceId, final long typeId)
    {
        final TreatmentType tt = Optional.ofNullable(treatmentTypeRepository.findOne(typeId))
            .filter(type -> type.getPracticeId() == practiceId)
            .orElseThrow(() -> new EntityNotFoundException("TreatmentType", typeId));

        return customerMapper.toTreatmentDto(tt);
    }

    @Transactional(readOnly = true)
    public TreatmentTypesPage getTreatmentTypes(final long practiceId, final Predicate predicate, final Pageable pageable)
    {

        final BooleanBuilder search = new BooleanBuilder(predicate).and(QTreatmentType.treatmentType.practiceId.eq(practiceId));

        final Page<TreatmentType> types = treatmentTypeRepository.findAll(search, pageable);
        final PageDto page = new PageDto();
        page.setTotalItems(types.getTotalElements());
        page.setTotalPages(types.getTotalPages());
        page.setCurrentPage(pageable.getPageNumber());
        page.setPageSize(pageable.getPageSize());

        final TreatmentTypesPage result = new TreatmentTypesPage();
        result.setPage(page);
        result.setElements(types.getContent().stream()
            .map(customerMapper::toTreatmentDto)
            .collect(Collectors.toList()));

        return result;
    }

    private void publishTreatmentUpdate(TreatmentType type, long practiceId) {
        publishTreatmentUpdate(TreatmentUpdateEvent.of(type), practiceId);
    }

    private void publishTreatmentUpdate(TreatmentUpdateEvent event, long practiceId) {
        eventPublisher.publishEvent(event);
        eventPublisher.publishEvent(PracticeUpdateEvent.ofUpdated(practiceId));
    }
}
