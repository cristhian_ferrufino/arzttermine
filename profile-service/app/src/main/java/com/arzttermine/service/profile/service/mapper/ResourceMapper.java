package com.arzttermine.service.profile.service.mapper;

import com.arzttermine.service.profile.domain.entity.Practice;
import com.arzttermine.service.profile.domain.entity.Resource;
import com.arzttermine.service.profile.web.dto.request.CreateResourceDto;
import com.arzttermine.service.profile.web.dto.response.ResourceDto;
import org.springframework.stereotype.Component;

@Component
public class ResourceMapper
{
    public ResourceDto toDto(final Resource resource)
    {
        final ResourceDto dto = new ResourceDto();
        dto.setId(resource.getId());
        dto.setColor(resource.getColor());
        dto.setName(resource.getName());
        dto.setType(resource.getType());
        dto.setSystemId(resource.getSystemId());

        return dto;
    }

    public Resource fromRequest(final Practice practice, final CreateResourceDto request)
    {
        final Resource resource = new Resource();
        resource.setColor(request.getColor());
        resource.setType(request.getType());
        resource.setName(request.getName());

        if (null != request.getSystemId()) {
            resource.setSystemId(request.getSystemId());
        }

        practice.getResources().add(resource);
        resource.setPractice(practice);

        return resource;
    }
}
