package com.arzttermine.service.profile.domain.repository;

import com.arzttermine.service.profile.domain.entity.PaidFeatureAssignment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface PaidFeatureAssignmentRepository extends JpaRepository<PaidFeatureAssignment, Long> {

    Set<PaidFeatureAssignment> findByPracticeId(long id);

}
