package com.arzttermine.service.profile.web.controller;

import com.arzttermine.service.profile.service.PracticeService;
import com.arzttermine.service.profile.web.dto.request.CreateCustomerDto;
import com.arzttermine.service.profile.web.dto.request.UpdateDoctorDto;
import com.arzttermine.service.profile.web.dto.response.DoctorProfileDto;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Collection;

@RestController
@RequestMapping("/practices/{practiceId}/doctors")
public class DoctorController
{
    @Autowired
    private PracticeService practiceService;

    @ApiOperation("Returns list of doctors in a certain practice")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "doctors are returned"),
        @ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "practice was not found")
    })
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyRole('ROLE_CLIENT', 'ROLE_CUSTOMER')")
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<DoctorProfileDto> getDoctors(@PathVariable long practiceId)
    {
        return practiceService.getDoctors(practiceId);
    }

    @ApiOperation("Adds another doctor to the given practice")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        ),
        @ApiImplicitParam(
            name = "practiceId",
            dataType = "long",
            required = true,
            paramType = "path"
        ),
        @ApiImplicitParam(
            name = "doctorId",
            dataType = "long",
            required = true,
            paramType = "path"
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_NO_CONTENT, message = "doctor was added to the practice"),
        @ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "doctor or practice was not found")
    })
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAnyRole('ROLE_EMPLOYEE', 'ROLE_SYSTEM', 'ROLE_PRACTICE_' + #practiceId)")
    @RequestMapping(value = "/{customerId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void addDoctorToPractice(@ApiIgnore @PathVariable long practiceId, @ApiIgnore @PathVariable long customerId)
    {
        practiceService.addDoctor(practiceId, customerId);
    }

    @ApiOperation("Adds another doctor to the given practice and creates a new customer if does not exist")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        ),
        @ApiImplicitParam(
            name = "practiceId",
            dataType = "long",
            required = true,
            paramType = "path"
        ),
        @ApiImplicitParam(
            name = "doctorId",
            dataType = "long",
            required = true,
            paramType = "path"
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_NO_CONTENT, message = "doctor was added to the practice"),
        @ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "doctor or practice was not found")
    })
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAnyRole('ROLE_EMPLOYEE', 'ROLE_SYSTEM', 'ROLE_PRACTICE_' + #practiceId)")
    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void addNewDoctorToPractice(@ApiIgnore @PathVariable long practiceId, @RequestBody @Valid CreateCustomerDto customer)
    {
        practiceService.addDoctor(practiceId, customer);
    }

    @ApiOperation("Removes a doctor from the given practice")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        ),
        @ApiImplicitParam(
            name = "practiceId",
            dataType = "long",
            required = true,
            paramType = "path"
        ),
        @ApiImplicitParam(
            name = "doctorId",
            dataType = "long",
            required = true,
            paramType = "path"
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_NO_CONTENT, message = "doctor was removed from the practice"),
        @ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "practice was not found. Do not expect this when doctor does not exist")
    })
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAnyRole('ROLE_EMPLOYEE', 'ROLE_SYSTEM', 'ROLE_PRACTICE_' + #practiceId)")
    @RequestMapping(value = "{customerId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void removeDoctorFromPractice(@ApiIgnore @PathVariable long practiceId, @ApiIgnore @PathVariable long customerId)
    {
        practiceService.removeDoctor(practiceId, customerId);
    }

    @ApiOperation("Updates a doctor from a given practice perspective")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        ),
        @ApiImplicitParam(
            name = "practiceId",
            dataType = "long",
            required = true,
            paramType = "path"
        ),
        @ApiImplicitParam(
            name = "profileId",
            dataType = "long",
            required = true,
            paramType = "path"
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_NO_CONTENT, message = "doctors are updated"),
        @ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "practice or doctor profile was not found")
    })
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAnyRole('ROLE_SYSTEM', 'ROLE_EMPLOYEE', 'ROLE_PRACTICE_' + #practiceId)")
    @RequestMapping(value = "/{customerId}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateDoctors(@PathVariable long practiceId, @PathVariable long customerId, @RequestBody @Valid UpdateDoctorDto update)
    {
        practiceService.updateDoctor(practiceId, customerId, update);
    }
}
