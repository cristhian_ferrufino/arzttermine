package com.arzttermine.service.profile;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableAsync
@EnableJpaRepositories
@EnableAspectJAutoProxy
@EnableSpringDataWebSupport
@EnableTransactionManagement(proxyTargetClass = true)
public class Application
{
    public static void main(String[] args)
    {
        new SpringApplicationBuilder()
            .sources(Application.class)
            .run(args);
    }
}
