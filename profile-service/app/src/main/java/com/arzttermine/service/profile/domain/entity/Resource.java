package com.arzttermine.service.profile.domain.entity;

import com.arzttermine.service.profile.web.struct.ResourceType;

import javax.persistence.*;

@Entity
@Table(
    name = "resources",
    indexes = {
        @Index(name = "id_resource_practice_id", columnList = "practice_id")
    }
)
public class Resource
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false, insertable = false)
    private long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "practice_id", nullable = false, updatable = false)
    private Practice practice;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private ResourceType type;

    @Column(nullable = false)
    private String name;

    @Column(name = "system_id")
    private long systemId;

    private String color;

    public long getId() {
        return id;
    }

    public Practice getPractice() {
        return practice;
    }

    public void setPractice(Practice practice) {
        this.practice = practice;
    }

    public ResourceType getType() {
        return type;
    }

    public void setType(ResourceType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public long getSystemId() {
        return systemId;
    }

    public void setSystemId(long systemId) {
        this.systemId = systemId;
    }
}
