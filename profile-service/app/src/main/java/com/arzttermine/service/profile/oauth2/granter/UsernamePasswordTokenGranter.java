package com.arzttermine.service.profile.oauth2.granter;

import com.arzttermine.service.profile.domain.entity.Customer;
import org.springframework.security.core.Authentication;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.provider.*;
import org.springframework.security.oauth2.provider.token.AbstractTokenGranter;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class UsernamePasswordTokenGranter extends AbstractTokenGranter
{
    private static final String GRANT_TYPE = "password";

    private final AuthenticationManager authenticationManager;

    public UsernamePasswordTokenGranter(
        AuthenticationManager authenticationManager,
        AuthorizationServerTokenServices tokenServices,
        ClientDetailsService clientDetailsService,
        OAuth2RequestFactory requestFactory
    ) {
        super(tokenServices, clientDetailsService, requestFactory, GRANT_TYPE);
        this.authenticationManager = authenticationManager;
    }

    @Override
    protected OAuth2Authentication getOAuth2Authentication(ClientDetails client, TokenRequest tokenRequest)
    {
        final Map<String, String> parameters = new HashMap<>(tokenRequest.getRequestParameters());
        final String username = parameters.get("username");
        final String password = parameters.get("password");
        parameters.remove("password");

        // client must have grant_type password to login a customer
        final Authentication passwordAuthentication = new UsernamePasswordAuthenticationToken(username, password, client.getAuthorities());
        final Authentication customerAuth = authenticationManager.authenticate(passwordAuthentication);

        final OAuth2Request storedOAuth2Request = getRequestFactory().createOAuth2Request(client, tokenRequest);
        final Customer customer = (Customer) customerAuth.getPrincipal();

        final Collection<GrantedAuthority> authorities = Stream.concat(customer.getAuthorities().stream(), customerAuth.getAuthorities().stream())
            .distinct().collect(Collectors.toList());

        return new OAuth2Authentication(storedOAuth2Request, new UsernamePasswordAuthenticationToken(
            customer.getId(),
            customer.getEmail(),
            authorities
        ));
    }
}
