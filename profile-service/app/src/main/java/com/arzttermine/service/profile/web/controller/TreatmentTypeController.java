package com.arzttermine.service.profile.web.controller;

import com.arzttermine.service.profile.domain.entity.TreatmentType;
import com.arzttermine.service.profile.service.TreatmentTypeService;
import com.arzttermine.service.profile.web.dto.request.CreateTreatmentTypeDto;
import com.arzttermine.service.profile.web.dto.response.TreatmentTypeDto;
import com.arzttermine.service.profile.web.dto.response.TreatmentTypesPage;
import com.querydsl.core.types.Predicate;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@RestController
@RequestMapping(value = "/practices/{practice}/treatments")
public class TreatmentTypeController
{
    @Autowired
    private TreatmentTypeService treatmentTypeService;

    @ApiOperation("Add a new treatment type to practice profile and attach them to doctor profiles")
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_CREATED, message = "success, treatment returned"),
        @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "request body is not valid"),
        @ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "doctor was not found")
    })
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        )
    })
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasAnyRole('ROLE_EMPLOYEE', 'ROLE_SYSTEM', 'ROLE_PRACTICE_' + #practice)")
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public TreatmentTypeDto addTreatmentType(@PathVariable long practice, @RequestBody @Valid CreateTreatmentTypeDto request)
    {
        return treatmentTypeService.create(practice, request);
    }

    @ApiOperation("Updates a given treatment type")
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_NO_CONTENT, message = "success, treatment returned"),
        @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "request body is not valid"),
        @ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "practice was not found")
    })
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        )
    })
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAnyRole('ROLE_EMPLOYEE', 'ROLE_SYSTEM', 'ROLE_PRACTICE_' + #practice)")
    @RequestMapping(value = "/{typeId}", method = RequestMethod.PATCH, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateTreatmentType(@PathVariable long practice, @PathVariable long typeId, @RequestBody @Valid CreateTreatmentTypeDto request)
    {
        treatmentTypeService.update(practice, typeId, request);
    }

    @ApiOperation("Removes a treatment type from doctors profile")
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_NO_CONTENT, message = "success")
    })
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        ),
        @ApiImplicitParam(
            name = "customer",
            dataType = "long",
            paramType = "path",
            required = true
        ),
        @ApiImplicitParam(
            name = "id",
            dataType = "long",
            paramType = "path",
            required = true
        )
    })
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAnyRole('ROLE_EMPLOYEE', 'ROLE_SYSTEM', 'ROLE_PRACTICE_' + #practice)")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void removeTreatmentType(@PathVariable long practice, @PathVariable long id)
    {
        treatmentTypeService.remove(practice, id);
    }

    @ApiOperation("Returns a treatment type")
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "success"),
        @ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "treatment type was not found")
    })
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        ),
        @ApiImplicitParam(
            name = "id",
            dataType = "long",
            paramType = "path",
            required = true
        )
    })
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyRole('ROLE_SYSTEM', 'ROLE_CLIENT', 'ROLE_PRACTICE_' + #practice)")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public TreatmentTypeDto getTreatmentType(@PathVariable long practice, @PathVariable long id)
    {
        return treatmentTypeService.get(practice, id);
    }

    @ApiOperation("Returns treatment types from doctors profile")
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "success"),
        @ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "doctor was not found")
    })
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        ),
        @ApiImplicitParam(
            name = "customer",
            dataType = "long",
            paramType = "path",
            required = true
        )
    })
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyRole('ROLE_CLIENT', 'ROLE_EMPLOYEE', 'ROLE_SYSTEM', 'ROLE_DOCTOR')")
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public TreatmentTypesPage getTreatmentTypes(@QuerydslPredicate(root = TreatmentType.class) Predicate predicate, @PathVariable long practice, Pageable pageable)
    {
        return treatmentTypeService.getTreatmentTypes(practice, predicate, pageable);
    }
}
