package com.arzttermine.service.profile.service.mapper;

import com.arzttermine.service.profile.domain.entity.Customer;
import com.arzttermine.service.profile.domain.entity.DoctorProfile;
import com.arzttermine.service.profile.domain.entity.Practice;
import com.arzttermine.service.profile.domain.entity.TreatmentType;
import com.arzttermine.service.profile.domain.entity.WidgetConfiguration;
import com.arzttermine.service.profile.web.dto.WidgetConfigurationDto;
import com.arzttermine.service.profile.web.dto.WidgetSuccessBehaviourDto;
import com.arzttermine.service.profile.web.dto.request.CreatePracticeDto;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class PracticeMapper
{
    private final LocationMapper locationMapper;
    private final CustomerMapper customerMapper;
    private final ResourceMapper resourceMapper;
    private final PaidFeatureAssignmentMapper paidFeatureAssignmentMapper;
    private final ReviewSiteMapper reviewSiteMapper;

    @Autowired
    public PracticeMapper(LocationMapper locationMapper, CustomerMapper customerMapper, ResourceMapper resourceMapper, PaidFeatureAssignmentMapper paidFeatureAssignmentMapper, ReviewSiteMapper reviewSiteMapper) {
        this.locationMapper = locationMapper;
        this.customerMapper = customerMapper;
        this.resourceMapper = resourceMapper;
        this.paidFeatureAssignmentMapper = paidFeatureAssignmentMapper;
        this.reviewSiteMapper = reviewSiteMapper;
    }

    public Practice fromRequest(final CreatePracticeDto request)
    {
        final Practice practice = new Practice();
        practice.setName(request.getName());
        practice.setLocation(locationMapper.fromRequest(request.getLocation()));
        practice.setPreferredReviewSiteType(request.getPreferredReviewSiteType());

        return practice;
    }

    public PracticeDto toDto(final Practice practice)
    {
        final PracticeDto dto = new PracticeDto();
        dto.setId(practice.getId());
        dto.setName(practice.getName());
        dto.setLocation(locationMapper.toDto(practice.getLocation()));
        dto.setPreferredReviewSiteType(practice.getPreferredReviewSiteType());
        attachConfigToPractice(practice.getWidgetConfiguration(), dto);

        dto.setMedicalSpecialties(practice.getDoctors().stream()
            .map(DoctorProfile::getMedicalSpecialties)
            .flatMap(Collection::stream)
            .distinct()
            .collect(Collectors.toList()));

        dto.setTreatmentTypes(practice.getTreatmentTypes().stream()
            .filter(TreatmentType::isEnabled)
            .map(customerMapper::toTreatmentDto)
            .collect(Collectors.toList()));

        dto.setResources(practice.getResources().stream()
            .map(resourceMapper::toDto)
            .collect(Collectors.toList()));

        dto.setDoctors(practice.getDoctors().stream()
            .map(dp -> customerMapper.toDoctorProfileDto(dp, practice.getTreatmentTypes().stream()
                .filter(tt -> tt.getDoctors().contains(dp))
                .collect(Collectors.toList())))
            .collect(Collectors.toList()));

        dto.setPaidFeatures(practice.getPaidFeatureAssignments().stream()
            .map(paidFeatureAssignmentMapper::toDto)
            .collect(Collectors.toList())
        );

        dto.setReviewSites(practice.getReviewSites().stream()
            .map(reviewSiteMapper::toDto)
            .collect(Collectors.toList())
        );

        dto.setInsuranceTypes(practice.getInsuranceTypes());

        dto.setOwnerId(Optional.ofNullable(practice.getOwner())
            .map(Customer::getId)
            .orElse(null));

        return dto;
    }

    public void attachConfigToPractice(final WidgetConfigurationDto config, final Practice practice)
    {
        if (null == config) {
            return;
        }

        Optional.ofNullable(config.getTemplateId()).ifPresent(tid -> practice.getWidgetConfiguration().setTemplateId(tid));
         Optional.ofNullable(config.getSuccess()).ifPresent(behaviour -> {
            practice.getWidgetConfiguration().setSuccessText(behaviour.getText());
            practice.getWidgetConfiguration().setSuccessUrl(behaviour.getLink());
        });
    }

    private void attachConfigToPractice(final WidgetConfiguration config, final PracticeDto practice)
    {
        if (null == config) {
            return;
        }

        final WidgetConfigurationDto dto = new WidgetConfigurationDto();
        dto.setTemplateId(Optional.ofNullable(config.getTemplateId()).orElse("default"));
        if (null != config.getSuccessText() || null != config.getSuccessUrl()) {
            dto.setSuccess(new WidgetSuccessBehaviourDto(config.getSuccessText(), config.getSuccessUrl()));
        }

        practice.setWidgetConfiguration(dto);
    }
}
