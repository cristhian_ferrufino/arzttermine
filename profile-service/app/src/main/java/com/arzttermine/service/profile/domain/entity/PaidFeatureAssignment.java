package com.arzttermine.service.profile.domain.entity;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

@Entity
@Table(name = "practice_paid_features")
public class PaidFeatureAssignment {

    @Id
    @Column(nullable = false, updatable = false, insertable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @JoinColumn(name = "practice_id")
    @ManyToOne(fetch=FetchType.LAZY)
    private Practice practice;

    @JoinColumn(name = "feature_id")
    @ManyToOne(fetch=FetchType.EAGER)
    private PaidFeature paidFeature;

    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    @MapKeyColumn(name="name")
    @Column(name="value")
    @CollectionTable(name="paid_feature_options", joinColumns=@JoinColumn(name="assignment_id"))
    private Map<String, String> options = new HashMap<>();

    public long getId() {
        return id;
    }

    public Practice getPractice() {
        return practice;
    }

    public void setPractice(Practice practice) {
        this.practice = practice;
    }

    public PaidFeature getPaidFeature() {
        return paidFeature;
    }

    public void setPaidFeature(PaidFeature paidFeature) {
        this.paidFeature = paidFeature;
    }

    public Map<String, String> getOptions() {
        return options;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PaidFeatureAssignment that = (PaidFeatureAssignment) o;

        if (practice != null ? !practice.equals(that.practice) : that.practice != null) return false;
        return paidFeature != null ? paidFeature.equals(that.paidFeature) : that.paidFeature == null;
    }

    @Override
    public int hashCode() {
        int result = practice != null ? practice.hashCode() : 0;
        result = 31 * result + (paidFeature != null ? paidFeature.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PaidFeatureAssignment{" +
                "id=" + id +
                ", practice=" + practice.getName() +
                ", paidFeature=" + paidFeature.getName() +
                ", options=" + options +
                '}';
    }
}
