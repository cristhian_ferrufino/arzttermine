package com.arzttermine.service.profile.service;

import com.arzttermine.library.suport.exception.EntityNotFoundException;
import com.arzttermine.service.profile.domain.entity.PaidFeature;
import com.arzttermine.service.profile.domain.entity.PaidFeatureAssignment;
import com.arzttermine.service.profile.domain.entity.Practice;
import com.arzttermine.service.profile.domain.repository.PaidFeatureAssignmentRepository;
import com.arzttermine.service.profile.domain.repository.PaidFeatureRepository;
import com.arzttermine.service.profile.domain.repository.PracticeRepository;
import com.arzttermine.service.profile.service.event.model.PracticeUpdateEvent;
import com.arzttermine.service.profile.service.mapper.PaidFeatureAssignmentMapper;
import com.arzttermine.service.profile.web.dto.request.UpdatePaidFeatureAssignmentDto;
import com.arzttermine.service.profile.web.dto.response.PaidFeatureAssignmentDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import javax.swing.text.html.Option;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class PaidFeatureService {

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private PaidFeatureRepository paidFeatureRepository;

    @Autowired
    private PaidFeatureAssignmentRepository paidFeatureAssignmentRepository;

    @Autowired
    private PaidFeatureAssignmentMapper paidFeatureAssignmentMapper;


    @Transactional(readOnly = false)
    public void addPaidFeatureToPractice(Practice practice, final long paidFeatureId, final Map<String, String> options) {

        final PaidFeature paidFeature = paidFeatureRepository.findOne(paidFeatureId);

        if (null != paidFeature) {


            practice.assignPaidFeature(paidFeature,options);
            final PracticeUpdateEvent event = PracticeUpdateEvent.ofUpdated(practice.getId());
            eventPublisher.publishEvent(event);

        }

    }

    @Transactional(readOnly = false)
    public void updatePaidFeaturesOnPractice(Practice practice, final Map<String, Map<String, String>> update) {

        if (update.isEmpty()) {
            practice.getPaidFeatureAssignments().clear();

        } else {
            //Check if paid features of update can be found in persistence
            final Map<String, PaidFeature> paidFeaturePool = paidFeatureRepository.findByNameIn(update.keySet())
                .stream().collect(Collectors.toMap(PaidFeature::getName, Function.identity()));
            if (paidFeaturePool.size() < update.size()) {
                Optional<String> invalidPaidFeature = update.keySet().stream().filter(e -> !paidFeaturePool.keySet().contains(e)).findFirst();
                throw new EntityNotFoundException("PaidFeature", invalidPaidFeature.orElse(null));
            }

            //Check if currently assigned paidfeatures should be updated or removed

            for (PaidFeatureAssignment pf : practice.getPaidFeatureAssignments().stream().collect(Collectors.toList())) {
                String pfName = pf.getPaidFeature().getName();
                if (update.containsKey(pfName)) {
                    pf.getOptions().clear();
                    pf.getOptions().putAll(update.get(pfName));
                    update.remove(pfName);
                } else {
                    practice.getPaidFeatureAssignments().remove(pf);
                }
            }

            //Add remaining (new) paidfeature assignments
            update.entrySet().stream().forEach(e -> {
                practice.assignPaidFeature(paidFeaturePool.get(e.getKey()),e.getValue());
            });

        }

    }

    @Transactional(readOnly = true)
    public Set<PaidFeatureAssignmentDto> getPaidFeatureAssignmentsByPracticeId(final long practiceId) {

        final Set<PaidFeatureAssignment> paidFeatures = paidFeatureAssignmentRepository.findByPracticeId(practiceId);

        if (null == paidFeatures)
            throw new EntityNotFoundException("Practice", practiceId);

        return paidFeatures.stream()
                .map(paidFeature -> paidFeatureAssignmentMapper.toDto(paidFeature))
                .collect(Collectors.toSet());
    }

}