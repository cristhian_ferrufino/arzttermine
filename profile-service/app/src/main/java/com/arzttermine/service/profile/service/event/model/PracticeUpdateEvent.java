package com.arzttermine.service.profile.service.event.model;

import com.arzttermine.library.elasticsearch.model.BulkAction;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;

public class PracticeUpdateEvent
{
    private final Long practiceId;
    private final PracticeDto practice;
    private final BulkAction.ActionType updateType;

    private PracticeUpdateEvent(PracticeDto practice, BulkAction.ActionType type) {
        this.practice = practice;
        this.updateType = type;
        this.practiceId = null;
    }

    private PracticeUpdateEvent(Long practiceid, BulkAction.ActionType type) {
        this.practiceId = practiceid;
        this.updateType = type;
        this.practice = null;
    }

    public static PracticeUpdateEvent ofUpdated(long practiceId) {
        return new PracticeUpdateEvent(practiceId, BulkAction.ActionType.UPDATE);
    }

    public static PracticeUpdateEvent ofDeleted(PracticeDto practice) {
        return new PracticeUpdateEvent(practice, BulkAction.ActionType.DELETE);
    }

    public static PracticeUpdateEvent ofNew(PracticeDto practice) {
        return new PracticeUpdateEvent(practice, BulkAction.ActionType.INDEX);
    }

    public PracticeDto getPractice() {
        return practice;
    }

    public BulkAction.ActionType getUpdateType() {
        return updateType;
    }

    public Long getPracticeId() {
        return practiceId;
    }
}
