package com.arzttermine.service.profile.service;

import com.arzttermine.library.suport.exception.EntityNotFoundException;
import com.arzttermine.service.profile.domain.entity.Location;
import com.arzttermine.service.profile.domain.repository.LocationRepository;
import com.arzttermine.service.profile.domain.repository.PracticeRepository;
import com.arzttermine.service.profile.service.event.model.PracticeUpdateEvent;
import com.arzttermine.service.profile.service.mapper.LocationMapper;
import com.arzttermine.service.profile.web.dto.LocationDto;
import com.arzttermine.service.profile.web.dto.request.UpdateLocationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

@Service
public class LocationService {

    @Autowired
    private LocationRepository repository;

    @Autowired
    private PracticeRepository practiceRepository;

    @Autowired
    private LocationMapper mapper;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Transactional
    public LocationDto updateLocation(final long id, final UpdateLocationDto update) {
        Location location = repository.findOne(id);
        if (location == null) {
            throw new EntityNotFoundException("Location", id);
        }

        Optional.ofNullable(update.getCity()).ifPresent(location::setCity);
        Optional.ofNullable(update.getContactMail()).ifPresent(location::setContactMail);
        Optional.ofNullable(update.getLatitude()).ifPresent(location::setLatitude);
        Optional.ofNullable(update.getLongitude()).ifPresent(location::setLongitude);
        Optional.ofNullable(update.getPhone()).ifPresent(location::setPhone);
        Optional.ofNullable(update.getPhoneMobile()).ifPresent(location::setPhoneMobile);
        Optional.ofNullable(update.getLocation()).ifPresent(location::setLocation);
        Optional.ofNullable(update.getZip()).ifPresent(location::setZip);
        Optional.ofNullable(update.getPrimary()).ifPresent(location::setPrimary);


        Collection<Long> practiceIds = practiceRepository.findPracticeIdsByLocationId(location.getId());
        for (Long practiceId : practiceIds) {
            final PracticeUpdateEvent event = PracticeUpdateEvent.ofUpdated(practiceId);
            eventPublisher.publishEvent(event);
        }

        return mapper.toDto(location);
    }
}
