package com.arzttermine.service.profile.service;

import com.arzttermine.library.suport.exception.EntityNotFoundException;
import com.arzttermine.library.web.dto.response.paging.PageDto;
import com.arzttermine.service.profile.domain.entity.Practice;
import com.arzttermine.service.profile.domain.entity.QResource;
import com.arzttermine.service.profile.domain.entity.Resource;
import com.arzttermine.service.profile.domain.repository.PracticeRepository;
import com.arzttermine.service.profile.domain.repository.ResourceRepository;
import com.arzttermine.service.profile.service.mapper.ResourceMapper;
import com.arzttermine.service.profile.web.dto.request.CreateResourceDto;
import com.arzttermine.service.profile.web.dto.response.ResourceDto;
import com.arzttermine.service.profile.web.dto.response.ResourcesPage;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ResourceService
{
    @Autowired
    private PracticeRepository practiceRepository;

    @Autowired
    private ResourceRepository resourceRepository;

    @Autowired
    private ResourceMapper resourceMapper;

    @Transactional(readOnly = false)
    public ResourceDto createResource(final long practiceId, final CreateResourceDto request)
    {
        final Practice practice = findOneNullsafe(practiceId);
        final Resource resource = resourceMapper.fromRequest(practice, request);
        resourceRepository.save(resource);

        return resourceMapper.toDto(resource);
    }

    @Transactional(readOnly = false)
    public void updateResource(final long practiceId, final long resourceId, final CreateResourceDto request)
    {
        final Optional<Resource> resource = getResourceFromPractice(practiceId, resourceId);

        if (!resource.isPresent()) {
            return;
        }

        resource.get().setName(request.getName());
        resource.get().setType(request.getType());
        resource.get().setColor(request.getColor());
    }

    @Transactional(readOnly = false)
    public void removeResource(final long practiceId, final long resourceId)
    {
        final Practice practice = findOneNullsafe(practiceId);

        practice.getResources().stream()
            .filter(r -> r.getId() == resourceId)
            .findFirst()
            .ifPresent(r -> {
                practice.getResources().remove(r);
                resourceRepository.delete(r);
            });
    }

    @Transactional(readOnly = true)
    public ResourcesPage getResources(final long practice, final Predicate predicate, final Pageable pageable)
    {
        final Predicate filters = new BooleanBuilder(predicate).and(QResource.resource.practice.id.eq(practice));
        final Page<Resource> resources = resourceRepository.findAll(filters, pageable);

        final PageDto page = new PageDto();
        page.setCurrentPage(pageable.getPageNumber());
        page.setPageSize(pageable.getPageSize());
        page.setTotalItems(resources.getTotalElements());
        page.setTotalPages(resources.getTotalPages());

        final ResourcesPage result = new ResourcesPage();
        result.setPage(page);
        result.setElements(resources.getContent().stream()
            .map(resourceMapper::toDto)
            .collect(Collectors.toList()));

        return result;
    }

    @Transactional(readOnly = true)
    public ResourceDto getResource(final long practice, final long id)
    {
        final Resource resource = Optional.ofNullable(resourceRepository.findOneByPracticeId(id, practice))
            .orElseThrow(() -> new EntityNotFoundException("Resource", id));

        return resourceMapper.toDto(resource);
    }

    private Optional<Resource> getResourceFromPractice(final long practiceId, final long resourceId)
    {
        final Practice practice = findOneNullsafe(practiceId);

        return practice.getResources().stream()
            .filter(r -> r.getId() == resourceId)
            .findFirst();
    }

    private Practice findOneNullsafe(final long practiceId)
    {
        return Optional.ofNullable(practiceRepository.findOne(practiceId))
            .orElseThrow(() -> new EntityNotFoundException("Practice", practiceId));
    }
}
