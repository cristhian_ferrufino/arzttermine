package com.arzttermine.service.profile.domain.repository;

import com.arzttermine.service.profile.domain.entity.ReviewSite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Set;


@Repository
public interface ReviewSiteRepository extends JpaRepository<ReviewSite, Long> {

    @Query( value = "select r.id, r.type, r.data, r.practice_id from practice_review_sites r inner join practices p on p.id=:practiceId and r.type=p.preferred_review_site_type order by r.id limit 1", nativeQuery = true)
    ReviewSite findFirstByPracticeId(@Param("practiceId") long practiceId);

}
