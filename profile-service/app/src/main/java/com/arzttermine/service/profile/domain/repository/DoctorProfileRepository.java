package com.arzttermine.service.profile.domain.repository;

import com.arzttermine.service.profile.domain.entity.DoctorProfile;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DoctorProfileRepository extends PagingAndSortingRepository<DoctorProfile, Long>
{
    @Query("SELECT dp FROM DoctorProfile dp WHERE dp.customer.id = :id")
    DoctorProfile findOneByCustomerId(@Param("id") long id);
}
