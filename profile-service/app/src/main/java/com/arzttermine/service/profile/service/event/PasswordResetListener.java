package com.arzttermine.service.profile.service.event;

import com.arzttermine.library.messaging.processor.MessageBus;
import com.arzttermine.library.web.struct.Language;
import com.arzttermine.service.notification.messages.customer.PasswordResetMessage;
import com.arzttermine.service.profile.config.settings.FrontendSettings;
import com.arzttermine.service.profile.domain.entity.Customer;
import com.arzttermine.service.profile.domain.entity.PasswordReset;
import com.arzttermine.service.profile.service.event.model.PasswordResetEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Optional;

@Component
public class PasswordResetListener
{
    private static final Logger log = LoggerFactory.getLogger(PasswordResetListener.class);

    @Autowired
    private MessageBus messageBus;

    @Autowired
    private FrontendSettings frontendSettings;

    @Async
    @TransactionalEventListener
    public void passwordResetted(PasswordResetEvent event)
    {
        final PasswordReset reset = event.getReset();
        final Customer customer = reset.getCustomer();

        final PasswordResetMessage.PasswordResetBag bag = new PasswordResetMessage.PasswordResetBag();
        bag.customerName = customer.getFullName();
        bag.resetLink = UriComponentsBuilder.newInstance()
            .scheme(frontendSettings.getScheme())
            .host(frontendSettings.getHost())
            .pathSegment(frontendSettings.getPath().toArray(new String[frontendSettings.getPath().size()]))
            .pathSegment("reset-password")
            .queryParam("token", reset.getToken().toString())
            .queryParam("email", reset.getCustomer().getEmail())
            .build()
            .toString();

        final PasswordResetMessage message = new PasswordResetMessage(
            bag,
            customer.getId(),
            customer.getEmail(),
            Optional.ofNullable(customer.getPreferredLanguage()).orElse(Language.GERMAN)
        );

        messageBus.send(message);
    }
}
