package com.arzttermine.service.profile.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
public class SchedulerConfiguration
{
    @Bean
    public ExecutorService executorService()
    {
        return Executors.newScheduledThreadPool(23);
    }

    @Bean
    public TaskExecutor taskExecutor()
    {
        final ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setAwaitTerminationSeconds((2 * 60) * 60);
        executor.setWaitForTasksToCompleteOnShutdown(true);
        executor.setCorePoolSize(23);

        return executor;
    }
}
