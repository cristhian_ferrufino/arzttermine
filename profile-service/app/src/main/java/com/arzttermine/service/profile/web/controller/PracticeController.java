package com.arzttermine.service.profile.web.controller;

import com.arzttermine.service.profile.domain.entity.Practice;
import com.arzttermine.service.profile.service.PaidFeatureService;
import com.arzttermine.service.profile.service.PracticeService;
import com.arzttermine.service.profile.service.ReviewSiteService;
import com.arzttermine.service.profile.web.dto.request.CreatePracticeDto;
import com.arzttermine.service.profile.web.dto.request.UpdatePaidFeatureAssignmentDto;
import com.arzttermine.service.profile.web.dto.request.UpdatePracticeDto;
import com.arzttermine.service.profile.web.dto.response.*;
import com.querydsl.core.types.Predicate;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Set;

@RestController
@RequestMapping("/practices")
public class PracticeController
{
    @Autowired
    private PracticeService practiceService;

    @Autowired
    private PaidFeatureService paidFeatureService;

    @Autowired
    private ReviewSiteService reviewSiteService;

    @ApiOperation("Creates a new practice")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_CREATED, message = "practice was created"),
        @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "request body is not valid")
    })
    @PreAuthorize("hasAnyRole('ROLE_EMPLOYEE', 'ROLE_CUSTOMER', 'ROLE_SYSTEM')")
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public PracticeDto createPractice(@RequestBody @Valid CreatePracticeDto request, @ApiIgnore OAuth2Authentication authentication)
    {
        return practiceService.createPractice(request, authentication);
    }

    @ApiOperation("Returns a single practice")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        ),
        @ApiImplicitParam(
            name = "practiceId",
            dataType = "long",
            required = true,
            paramType = "path"
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "practice was returned"),
        @ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "practice was not found")
    })
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyRole('ROLE_CLIENT', 'ROLE_CUSTOMER')")
    @RequestMapping(value = "/{practiceId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public PracticeDto getPractice(@ApiIgnore @PathVariable long practiceId)
    {
        return practiceService.getPractice(practiceId);
    }

    @ApiOperation("Returns a single practice")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        ),
        @ApiImplicitParam(
            name = "practiceId",
            dataType = "long",
            required = true,
            paramType = "path"
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "practice was updated and returned"),
        @ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "practice was not found")
    })
    @PreAuthorize("hasAnyRole('ROLE_EMPLOYEE', 'ROLE_SYSTEM') or hasRole('ROLE_PRACTICE_' + #practiceId)")
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{practiceId}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public PracticeDto updatePractice(@ApiIgnore @PathVariable long practiceId, @Valid @RequestBody UpdatePracticeDto update)
    {
        return practiceService.updatePractice(practiceId, update);
    }

    @ApiOperation("Returns page of practices")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "practices are returned")
    })
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyRole('ROLE_CLIENT', 'ROLE_CUSTOMER')")
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public PracticesPage getPractices(@QuerydslPredicate(root = Practice.class) Predicate predicate, Pageable pageable)
    {
        return practiceService.getPractices(predicate, pageable);
    }

    @ApiOperation("Update paid features on a practice")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        ),
        @ApiImplicitParam(
            name = "practiceId",
            dataType = "long",
            required = true,
            paramType = "path"
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "paid features were updated."),
        @ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "practice was not found")
    })
    @PreAuthorize("hasAnyRole('ROLE_EMPLOYEE', 'ROLE_SYSTEM', 'ROLE_PRACTICE_' + #practiceId)")
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{practiceId}/paidFeatures", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void updatePaidFeatures(@ApiIgnore @PathVariable long practiceId, @Valid @RequestBody UpdatePaidFeatureAssignmentDto update)
    {
        UpdatePracticeDto updatePractice = new UpdatePracticeDto();
        updatePractice.setPaidFeatures(update.getPaidFeatures());

        practiceService.updatePractice(practiceId,updatePractice);
    }

    @ApiOperation("Returns list of paid features for a certain practice")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "paid features are returned"),
        @ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "practice was not found")
    })
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyRole('ROLE_CLIENT', 'ROLE_CUSTOMER', 'ROLE_SYSTEM')")
    @RequestMapping(value = "/{practiceId}/paidFeatures", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Set<PaidFeatureAssignmentDto> getPaidFeatures(@PathVariable long practiceId)
    {
        return paidFeatureService.getPaidFeatureAssignmentsByPracticeId(practiceId);
    }

    @ApiOperation("Returns the preferred review site for a specific practice")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        ),
        @ApiImplicitParam(
            name = "practiceId",
            dataType = "long",
            required = true,
            paramType = "path"
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "review site returned"),
        @ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "practice not found")
    })
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyRole('ROLE_CLIENT', 'ROLE_CUSTOMER', 'ROLE_SYSTEM')")
    @RequestMapping(value = "/{practiceId}/reviewSites", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ReviewSiteDto getReviewSite(@PathVariable long practiceId)
    {
        return reviewSiteService.getReviewSiteByPracticeId(practiceId);
    }
}
