package com.arzttermine.service.profile.service.event.model;

import com.arzttermine.service.profile.domain.entity.PasswordReset;

public class PasswordResetEvent
{
    private final PasswordReset reset;

    private PasswordResetEvent(PasswordReset reset) {
        this.reset = reset;
    }

    public static PasswordResetEvent of(PasswordReset reset) {
        return new PasswordResetEvent(reset);
    }

    public PasswordReset getReset() {
        return reset;
    }
}
