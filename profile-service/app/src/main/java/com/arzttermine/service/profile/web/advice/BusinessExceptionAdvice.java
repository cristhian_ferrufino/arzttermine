package com.arzttermine.service.profile.web.advice;

import com.arzttermine.library.suport.exception.BusinessException;
import com.arzttermine.library.web.dto.response.ErrorDto;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Collections;
import java.util.List;

@ControllerAdvice
public class BusinessExceptionAdvice extends AbstractAdvice
{
    @ExceptionHandler
    public ResponseEntity<List<ErrorDto>> handleBusinessException(BusinessException exc)
    {
        final List<ErrorDto> errors = Collections.singletonList(exc.getErrorDto());

        return new ResponseEntity<>(errors, HttpStatus.valueOf(exc.getStatus().getCode()));
    }

    @ExceptionHandler
    public ResponseEntity<List<ErrorDto>> handleDataIntegrityViolationException(DataIntegrityViolationException exc)
    {
        final ErrorDto error = getErrorDto("ALREADY_EXISTS", "", exc.getMessage());

        return new ResponseEntity<>(Collections.singletonList(error), HttpStatus.CONFLICT);
    }
}
