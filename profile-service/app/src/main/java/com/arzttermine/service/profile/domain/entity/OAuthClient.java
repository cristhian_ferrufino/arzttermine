package com.arzttermine.service.profile.domain.entity;

import com.google.common.collect.Sets;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.provider.ClientDetails;

import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * The content of this table is managed via migrations only.
 * You should not create new oauth clients during the runtime!!
 * Therefore we do not permit any SET-access on this entity.
 * We also store some details (strings) comma separated as we want to gain some performance benefits
 * You might want to create new {@link Customer}s instead.
 */

@Entity
@Table(
    name = "oauth_client_details",
    indexes = {
        @Index(name = "id_oauth_client_client_id", columnList = "client_id", unique = true)
    }
)
public class OAuthClient implements ClientDetails
{
    @Id
    @Column(nullable = false, updatable = false, insertable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(updatable = false, nullable = false, name = "client_id", columnDefinition = "varchar(56) not null")
    private String clientId;

    @Column(nullable = false, updatable = false, name = "client_secret")
    private String clientSecret;

    @Column(name = "resource_id", nullable = false)
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(
        name = "oauth_client_resource_grants",
        joinColumns = {@JoinColumn(name = "client_id")}
    )
    private Set<String> resourceIds;

    @Column(nullable = false)
    private String scope = "default";

    @Column(nullable = false, name = "access_token_validity")
    private int accessTokenValiditySeconds;

    @Column(nullable = false, name = "refresh_token_validity")
    private int refreshTokenValiditySeconds;

    // store it comma separated, we cannot join across so many tables without loosing performance
    // normally there is only "client_credentials"
    @Column(name = "grant_types", nullable = false)
    private String authorizedGrantTypes;

    // store it comma separated, we cannot join across so many tables without loosing performance
    // for possible values see {@link AuthorityRole}
    @Column(name = "authorities", nullable = false)
    private String authorities;

    @Override
    public String getClientId() {
        return clientId;
    }

    @Override
    public Set<String> getResourceIds() {
        return resourceIds;
    }

    @Override
    public boolean isSecretRequired() {
        return null != clientSecret;
    }

    @Override
    public String getClientSecret() {
        return clientSecret;
    }

    @Override
    public boolean isScoped() {
        return true;
    }

    @Override
    public Set<String> getScope() {
        return Sets.newHashSet(this.scope.split(","));
    }

    @Override
    public Set<String> getAuthorizedGrantTypes() {
        if (null == authorizedGrantTypes || authorizedGrantTypes.isEmpty()) {
            // grant access via client_credentials is always supported
            return Sets.newHashSet("client_credentials");
        }

        return Sets.newHashSet(authorizedGrantTypes.trim().split(","));
    }

    @Override
    public Set<String> getRegisteredRedirectUri() {
        // no authorization codes or sso provided atm
        return Collections.emptySet();
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return Stream.of(authorities.trim().split(","))
            .map(SimpleGrantedAuthority::new)
            .collect(Collectors.toList());
    }

    @Override
    public Integer getAccessTokenValiditySeconds() {
        return accessTokenValiditySeconds;
    }

    @Override
    public Integer getRefreshTokenValiditySeconds() {
        return refreshTokenValiditySeconds;
    }

    @Override
    public boolean isAutoApprove(String s) {
        return true;
    }

    @Override
    public Map<String, Object> getAdditionalInformation() {
        throw new UnsupportedOperationException("no additional information available");
    }
}
