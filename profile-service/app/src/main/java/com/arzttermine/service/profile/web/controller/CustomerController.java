package com.arzttermine.service.profile.web.controller;

import com.arzttermine.service.profile.domain.entity.Customer;
import com.arzttermine.service.profile.service.CustomerService;
import com.arzttermine.service.profile.web.dto.request.CreateCustomerDto;
import com.arzttermine.service.profile.web.dto.request.UpdateCustomerDto;
import com.arzttermine.service.profile.web.dto.response.CustomerDto;
import com.arzttermine.service.profile.web.dto.response.CustomersPage;
import com.querydsl.core.types.Predicate;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@RestController
@RequestMapping("/customers")
public class CustomerController
{
    @Autowired
    private CustomerService customerService;

    @ApiOperation("Creates an new customer")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_CREATED, message = "customer was created successfully"),
        @ApiResponse(code = HttpServletResponse.SC_CONFLICT, message = "customer already exists"),
        @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "request body is not valid")
    })
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("#oauth2.hasScope('registration') or hasAnyRole('ROLE_SYSTEM', 'ROLE_EMPLOYEE')")
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public CustomerDto createCustomer(@RequestBody @Valid CreateCustomerDto request, OAuth2Authentication auth)
    {
        return customerService.createCustomer(request, auth);
    }

    @ApiOperation("Returns a single customer")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        ),
        @ApiImplicitParam(
            name = "id",
            dataType = "long",
            paramType = "path"
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "customer was created successfully"),
        @ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "customer was not found")
    })
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyRole('ROLE_CLIENT', 'ROLE_EMPLOYEE', 'ROLE_SYSTEM')")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public CustomerDto getCustomer(@ApiIgnore @PathVariable long id)
    {
        return customerService.getCustomer(id);
    }

    @ApiOperation("Returns the authenticated customer")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "customer was returned")
    })
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyRole('ROLE_CUSTOMER', 'ROLE_EMPLOYEE', 'ROLE_SYSTEM')")
    @RequestMapping(value = "/me", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public CustomerDto getCustomer(final OAuth2Authentication auth)
    {
        final long id = (Long) auth.getPrincipal();
        return customerService.getCustomer(id);
    }

    @ApiOperation("Updates a single customer")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        ),
        @ApiImplicitParam(
            name = "id",
            dataType = "long",
            paramType = "path"
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "customer was updated successfully"),
        @ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "customer was not found"),
        @ApiResponse(code = HttpServletResponse.SC_CONFLICT, message = "if update tries to use existing email address")
    })
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyRole('ROLE_EMPLOYEE', 'ROLE_SYSTEM', 'ROLE_CUSTOMER')")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public CustomerDto updateCustomer(@ApiIgnore @PathVariable long id, @RequestBody @Valid UpdateCustomerDto update, OAuth2Authentication auth)
    {
        return customerService.updateCustomer(id, update, auth);
    }

    @ApiOperation("Returns a page of customers")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        ),
        @ApiImplicitParam(
            name = "page",
            dataType = "object",
            paramType = "path"
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "customer page was returned successfully")
    })
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyRole('ROLE_EMPLOYEE', 'ROLE_SYSTEM')") // never grant customers, clients or doctors here!
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public CustomersPage getCustomers(@QuerydslPredicate(root = Customer.class) Predicate predicate, Pageable pageable)
    {
        return customerService.getCustomers(predicate, pageable);
    }
}
