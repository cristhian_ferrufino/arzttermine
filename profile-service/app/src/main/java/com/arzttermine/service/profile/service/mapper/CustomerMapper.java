package com.arzttermine.service.profile.service.mapper;

import com.arzttermine.library.web.dto.InsuranceProviderDto;
import com.arzttermine.library.web.dto.PriceDto;
import com.arzttermine.service.profile.domain.entity.Customer;
import com.arzttermine.service.profile.domain.entity.DoctorProfile;
import com.arzttermine.service.profile.domain.entity.OAuthClientRole;
import com.arzttermine.service.profile.domain.entity.TreatmentType;
import com.arzttermine.service.profile.web.dto.request.CreateCustomerDto;
import com.arzttermine.service.profile.web.dto.response.CustomerDto;
import com.arzttermine.service.profile.web.dto.response.DoctorProfileDto;
import com.arzttermine.service.profile.web.dto.response.PublicCustomerDto;
import com.arzttermine.service.profile.web.dto.response.TreatmentTypeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.time.Instant;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class CustomerMapper
{
    @Autowired
    private PasswordEncoder passwordEncoder;

    public Customer fromRequest(final CreateCustomerDto request)
    {
        final String password = (null != request.getPassword())
            ? passwordEncoder.encode(request.getPassword())
            : null;

        final Customer customer = new Customer();
        customer.setDefaultInsuranceType(request.getInsuranceType());
        customer.getSpokenLanguages().add(request.getLanguage());
        customer.setPreferredLanguage(request.getLanguage());
        customer.setFirstName(request.getFirstName());
        customer.setLastName(request.getLastName());
        customer.setTitle( request.getTitle());
        customer.setUsername(request.getEmail());
        customer.setGender(request.getGender());
        customer.setEmail(request.getEmail());
        customer.setPassword(password, Instant.now());
        customer.setPhone(request.getPhone());
        customer.setPhoneMobile(request.getPhoneMobile());

        return customer;
    }

    public void setPasswordFor(final Customer customer, final String newPassword)
    {
        final String encoded = passwordEncoder.encode(newPassword);
        customer.setPassword(encoded, Instant.now());

        Assert.isTrue(customer.isCredentialsNonExpired(), "credentials are not expired after update");
    }

    public CustomerDto toDto(final Customer customer)
    {
        final InsuranceProviderDto insurance = new InsuranceProviderDto();
        insurance.setType(customer.getDefaultInsuranceType());

        final CustomerDto dto = new CustomerDto();
        customerProfile(dto, customer);

        dto.setEnabled(customer.isEnabled());
        dto.setInsurance(insurance);
        dto.setRoles(customer.getRoles().stream()
            .map(OAuthClientRole::getRole)
            .map(GrantedAuthority::getAuthority)
            .collect(Collectors.toList()));

        return dto;
    }

    public DoctorProfileDto toDoctorProfileDto(final DoctorProfile profile, final List<TreatmentType> treatmentTypes)
    {
        final DoctorProfileDto profileDto = new DoctorProfileDto();
        profileDto.setTreatmentTypes(Optional.ofNullable(treatmentTypes).map(this::toTreatmentDtos).orElse(Collections.emptyList()));
        profileDto.setMedicalSpecialties(profile.getMedicalSpecialties());
        profileDto.setBookingTimeOffset(profile.getBookingTimeOffset());
        profileDto.setBookable(profile.isBookable());
        profileDto.setProfileId(profile.getId());
        customerProfile(profileDto, profile.getCustomer());

        return profileDto;
    }

    public List<TreatmentTypeDto> toTreatmentDtos(Collection<TreatmentType> types)
    {
        return types.stream()
            .map(this::toTreatmentDto)
            .collect(Collectors.toList());
    }

    public TreatmentTypeDto toTreatmentDto(final TreatmentType tt)
    {
        final TreatmentTypeDto type = new TreatmentTypeDto();
        type.setPracticeId(tt.getPracticeId());
        type.setForcePay(tt.isForcePay());
        type.setComment(tt.getComment());
        type.setEnabled(tt.isEnabled());
        type.setType(tt.getType());
        type.setId(tt.getId());

        type.setDoctorIds(tt.getDoctors().stream()
            .map(DoctorProfile::getCustomer)
            .map(Customer::getId)
            .collect(Collectors.toList()));

        Optional.ofNullable(tt.getCharge()).ifPresent(charge ->
            type.setCharge(new PriceDto(tt.getCharge())));

        return type;
    }

    private <T extends PublicCustomerDto> T customerProfile(final T dto, final Customer customer) {
        dto.setId(customer.getId());
        dto.setEmail(customer.getEmail());
        dto.setGender(customer.getGender());
        dto.setFirstName(customer.getFirstName());
        dto.setLastName(customer.getLastName());
        dto.setTitle(customer.getTitle());
        dto.setFullName(customer.getFullName());
        dto.setPreferredLanguage(customer.getPreferredLanguage());
        dto.setLanguages(customer.getSpokenLanguages());

        return dto;
    }
}
