[![Build Status](https://travis-ci.com/arzttermine/profile-service.svg?token=XHsxG8wHND1xUrHu4SwY&branch=master)](https://travis-ci.com/arzttermine/profile-service)

# profile-service

[Read more](https://github.com/arzttermine/documentation/wiki/Java-in-Detail) about how to manage java applications

### Documentation
[Search / Document](doc/search.md)

To see how to [setup java applications](https://github.com/arzttermine/documentation/wiki/Service-Setup#java--backend) follow the link.

Our API is documented using Swagger. You can access the documentation in development/default or docker profile via:
`/swagger-ui.html`

### Security:
[Read more](doc/oauth2.md) about security stuff
