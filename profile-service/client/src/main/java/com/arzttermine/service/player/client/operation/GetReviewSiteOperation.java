package com.arzttermine.service.player.client.operation;

import com.arzttermine.library.client.model.Operation;
import com.arzttermine.library.client.model.param.Parameter;
import com.arzttermine.library.client.model.struct.RequestMethod;
import com.arzttermine.service.profile.web.dto.response.ReviewSiteDto;

public class GetReviewSiteOperation extends Operation.Builder<ReviewSiteDto>{

    @Override
    public Operation<ReviewSiteDto> build()
    {
        setPath("/practices/{practiceId}/reviewSites");
        setMethod(RequestMethod.GET);
        addParameter(new Parameter("practiceId"));
        setResponseTypeClass(ReviewSiteDto.class);

        return new Operation<>(this);
    }
}
