package com.arzttermine.service.player.client.operation;

import com.arzttermine.library.client.model.Operation;
import com.arzttermine.library.client.model.struct.RequestMethod;
import com.arzttermine.service.profile.web.dto.response.CustomerDto;

public class CreateCustomerOperation extends Operation.Builder<CustomerDto>
{
    @Override
    public Operation<CustomerDto> build() {
        setPath("/customers");
        setMethod(RequestMethod.POST);
        setResponseTypeClass(CustomerDto.class);

        return new Operation<>(this);
    }
}
