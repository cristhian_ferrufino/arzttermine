package com.arzttermine.service.player.client.operation;

import com.arzttermine.library.client.model.Operation;
import com.arzttermine.library.client.model.param.Parameter;
import com.arzttermine.library.client.model.struct.RequestMethod;
import com.arzttermine.service.profile.web.dto.response.PaidFeatureAssignmentDto;

public class GetPaidFeaturesOperation extends Operation.Builder<PaidFeatureAssignmentDto[]>{

    @Override
    public Operation<PaidFeatureAssignmentDto[]> build()
    {
        setPath("/practices/{practiceId}/paidFeatures");
        setMethod(RequestMethod.GET);
        addParameter(new Parameter("practiceId"));
        setResponseTypeClass(PaidFeatureAssignmentDto[].class);

        return new Operation<>(this);
    }
}
