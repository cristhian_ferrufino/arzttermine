package com.arzttermine.service.player.client;

import com.arzttermine.library.client.config.settings.ClientHostSettings;
import com.arzttermine.library.client.model.RequestOptions;
import com.arzttermine.library.client.support.ClientAdapter;
import com.arzttermine.service.player.client.operation.CreateCustomerOperation;
import com.arzttermine.service.player.client.operation.GetCustomerOperation;
import com.arzttermine.service.player.client.operation.GetCustomersOperation;
import com.arzttermine.service.profile.web.dto.request.CreateCustomerDto;
import com.arzttermine.service.profile.web.dto.response.CustomerDto;
import com.arzttermine.service.profile.web.dto.response.CustomersPage;

public class CustomerClient extends ClientAdapter
{
    public CustomerClient(ClientHostSettings settings) {
        super(settings.getHost(), settings.getPort(), settings.getScheme());
    }

    public CustomerDto create(CreateCustomerDto request, RequestOptions options) {
        CreateCustomerOperation operation = new CreateCustomerOperation();
        operation.setRequestBody(request);

        return execute(operation, options);
    }

    public CustomerDto getById(RequestOptions options) {
        return execute(new GetCustomerOperation(), options);
    }

    public CustomersPage getCustomers(RequestOptions options) {
        return execute(new GetCustomersOperation(), options);
    }


}
