package com.arzttermine.service.player.client.operation;

import com.arzttermine.library.client.model.Operation;
import com.arzttermine.library.client.model.struct.RequestMethod;
import com.arzttermine.service.profile.web.dto.response.PracticesPage;

public class GetPracticesOperation extends Operation.Builder<PracticesPage>
{
    @Override
    public Operation<PracticesPage> build()
    {
        setPath("/practices");
        setMethod(RequestMethod.GET);

        setResponseTypeClass(PracticesPage.class);

        return new Operation<>(this);
    }
}
