package com.arzttermine.service.player.client.operation;

import com.arzttermine.library.client.model.Operation;
import com.arzttermine.library.client.model.param.Parameter;
import com.arzttermine.library.client.model.struct.RequestMethod;
import com.arzttermine.service.profile.web.dto.response.TreatmentTypeDto;

public class GetTreatmentTypeOperation extends Operation.Builder<TreatmentTypeDto>
{
    @Override
    public Operation<TreatmentTypeDto> build() {
        setPath("/practices/{practice}/treatments/{id}");
        setMethod(RequestMethod.GET);
        addParameter(new Parameter("id"));
        addParameter(new Parameter("practice"));
        setResponseTypeClass(TreatmentTypeDto.class);

        return new Operation<>(this);
    }
}
