package com.arzttermine.service.player.client;

import com.arzttermine.library.client.config.settings.ClientHostSettings;
import com.arzttermine.library.client.model.RequestOptions;
import com.arzttermine.library.client.model.param.Parameter;
import com.arzttermine.library.client.support.ClientAdapter;
import com.arzttermine.service.player.client.operation.GetTreatmentTypeOperation;
import com.arzttermine.service.player.client.operation.GetTreatmentTypesOperation;
import com.arzttermine.service.profile.web.dto.response.TreatmentTypeDto;
import com.arzttermine.service.profile.web.dto.response.TreatmentTypesPage;

public class TreatmentClient extends ClientAdapter
{
    public TreatmentClient(ClientHostSettings settings) {
        super(settings.getHost(), settings.getPort(), settings.getScheme());
    }

    public TreatmentTypeDto getById(long id, long practiceId, RequestOptions options) {
        GetTreatmentTypeOperation operation = new GetTreatmentTypeOperation();
        operation.set(Parameter.Type.PATH, "id", String.valueOf(id));
        operation.set(Parameter.Type.PATH, "practice", String.valueOf(practiceId));

        return execute(operation, options);
    }

    public TreatmentTypesPage getTreatmentTypes(long practiceId, RequestOptions options) {
        options.addParameter(new Parameter(Parameter.Type.PATH, "practice",String.valueOf(practiceId)));
        return execute(new GetTreatmentTypesOperation(), options);
    }
}
