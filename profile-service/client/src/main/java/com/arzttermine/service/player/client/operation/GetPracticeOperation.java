package com.arzttermine.service.player.client.operation;

import com.arzttermine.library.client.model.Operation;
import com.arzttermine.library.client.model.param.Parameter;
import com.arzttermine.library.client.model.struct.RequestMethod;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;

public class GetPracticeOperation extends Operation.Builder<PracticeDto>
{
    @Override
    public Operation<PracticeDto> build()
    {
        setPath("/practices/{id}");
        setMethod(RequestMethod.GET);
        addParameter(new Parameter("id"));

        setResponseTypeClass(PracticeDto.class);

        return new Operation<>(this);
    }
}
