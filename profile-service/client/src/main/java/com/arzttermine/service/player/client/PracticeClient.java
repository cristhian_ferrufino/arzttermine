package com.arzttermine.service.player.client;

import com.arzttermine.library.client.config.settings.ClientHostSettings;
import com.arzttermine.library.client.model.RequestOptions;
import com.arzttermine.library.client.model.param.Parameter;
import com.arzttermine.library.client.support.ClientAdapter;
import com.arzttermine.service.player.client.operation.GetPaidFeaturesOperation;
import com.arzttermine.service.player.client.operation.GetPracticeOperation;
import com.arzttermine.service.player.client.operation.GetPracticesOperation;
import com.arzttermine.service.player.client.operation.GetReviewSiteOperation;
import com.arzttermine.service.profile.web.dto.response.PaidFeatureAssignmentDto;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import com.arzttermine.service.profile.web.dto.response.PracticesPage;
import com.arzttermine.service.profile.web.dto.response.ReviewSiteDto;

public class PracticeClient extends ClientAdapter
{
    public PracticeClient(ClientHostSettings settings) {
        super(settings.getHost(), settings.getPort(), settings.getScheme());
    }

    public PracticeDto getById(long id, RequestOptions options) {
        GetPracticeOperation operation = new GetPracticeOperation();
        operation.set(Parameter.Type.PATH, "id", String.valueOf(id));

        return execute(operation, options);
    }

    public PracticesPage getPractices(RequestOptions options) {
        return execute(new GetPracticesOperation(), options);
    }

    public PaidFeatureAssignmentDto[] getPaidFeaturesByPracticeId(long practiceId, RequestOptions options) {
        GetPaidFeaturesOperation operation = new GetPaidFeaturesOperation();
        operation.set(Parameter.Type.PATH, "practiceId", String.valueOf(practiceId));

        return execute(operation, options);
    }

    public ReviewSiteDto getReviewSiteByPracticeId(long practiceId, RequestOptions options) {
        GetReviewSiteOperation operation = new GetReviewSiteOperation();
        operation.set(Parameter.Type.PATH, "practiceId", String.valueOf(practiceId));

        return execute(operation, options);
    }
}
