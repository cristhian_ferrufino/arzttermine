package com.arzttermine.service.player.client.operation;

import com.arzttermine.library.client.model.Operation;
import com.arzttermine.library.client.model.param.Parameter;
import com.arzttermine.library.client.model.struct.RequestMethod;
import com.arzttermine.service.profile.web.dto.response.TreatmentTypesPage;

public class GetTreatmentTypesOperation extends Operation.Builder<TreatmentTypesPage>
{
    @Override
    public Operation<TreatmentTypesPage> build()
    {
        setPath("/practices/{practice}/treatments");
        setMethod(RequestMethod.GET);
        addParameter(new Parameter("practice"));

        setResponseTypeClass(TreatmentTypesPage.class);

        return new Operation<>(this);
    }
}