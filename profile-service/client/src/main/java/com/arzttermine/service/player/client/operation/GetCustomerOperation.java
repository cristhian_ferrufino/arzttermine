package com.arzttermine.service.player.client.operation;

import com.arzttermine.library.client.model.Operation;
import com.arzttermine.library.client.model.param.Parameter;
import com.arzttermine.library.client.model.struct.RequestMethod;
import com.arzttermine.service.profile.web.dto.response.CustomerDto;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;

public class GetCustomerOperation extends Operation.Builder<CustomerDto>
{
    @Override
    public Operation<CustomerDto> build()
    {
        setPath("/customers/{id}");
        setMethod(RequestMethod.GET);
        addParameter(new Parameter("id"));

        setResponseTypeClass(CustomerDto.class);

        return new Operation<>(this);
    }
}
