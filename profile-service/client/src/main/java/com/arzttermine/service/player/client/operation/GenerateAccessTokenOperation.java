package com.arzttermine.service.player.client.operation;

import com.arzttermine.library.client.model.Operation;
import com.arzttermine.library.client.model.param.QueryParameter;
import com.arzttermine.library.client.model.struct.RequestMethod;
import com.arzttermine.service.profile.web.dto.response.AccessTokenDto;

import java.util.HashMap;

public class GenerateAccessTokenOperation extends Operation.Builder<AccessTokenDto>
{
    @Override
    public Operation<AccessTokenDto> build() {
        setPath("/oauth/token");
        setMethod(RequestMethod.POST);
        addParameter(new QueryParameter("client_id", null));
        addParameter(new QueryParameter("client_secret", null));
        addParameter(new QueryParameter("username", null));
        addParameter(new QueryParameter("password", null));
        addParameter(new QueryParameter("grant_type", null));
        addParameter(new QueryParameter("scope", null));
        setResponseTypeClass(AccessTokenDto.class);
        setRequestBody(new HashMap<>()); // POST needs a body

        return new Operation<>(this);
    }
}
