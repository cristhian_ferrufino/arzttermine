package com.arzttermine.service.player.client;

import com.arzttermine.library.client.config.settings.ClientHostSettings;
import com.arzttermine.library.client.model.param.Parameter;
import com.arzttermine.library.client.support.ClientAdapter;
import com.arzttermine.service.player.client.operation.GenerateAccessTokenOperation;
import com.arzttermine.service.profile.web.dto.response.AccessTokenDto;

import java.util.Collection;
import java.util.Collections;

public class TokenClient extends ClientAdapter
{
    public TokenClient(ClientHostSettings settings) {
        super(settings.getHost(), settings.getPort(), settings.getScheme());
    }

    public AccessTokenDto generateClientToken(String clientId, String clientSecret, Collection<String> scopes)
    {
        GenerateAccessTokenOperation operation = new GenerateAccessTokenOperation();
        operation.set(Parameter.Type.QUERY, "client_id", clientId);
        operation.set(Parameter.Type.QUERY, "client_secret", clientSecret);
        operation.set(Parameter.Type.QUERY, "grant_type", "client_credentials");

        if (null != scopes && !scopes.isEmpty()) {
            operation.set(Parameter.Type.QUERY, "scope", String.join(",", scopes));
        }

        return execute(operation);
    }

    public AccessTokenDto generateClientToken(String clientId, String clientSecret)
    {
        return generateClientToken(clientId, clientSecret, Collections.singletonList("default"));
    }
}
