package com.arzttermine.service.player.client.operation;

import com.arzttermine.library.client.model.Operation;
import com.arzttermine.library.client.model.struct.RequestMethod;
import com.arzttermine.service.profile.web.dto.response.CustomersPage;

public class GetCustomersOperation extends Operation.Builder<CustomersPage>
{
    @Override
    public Operation<CustomersPage> build()
    {
        setPath("/customers");
        setMethod(RequestMethod.GET);

        setResponseTypeClass(CustomersPage.class);

        return new Operation<>(this);
    }
}
