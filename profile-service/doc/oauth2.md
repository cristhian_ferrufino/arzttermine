# OAuth2

We are using OAuth2 to authorize our clients and customer against our [profile-service](https://github.com/arzttermine/profile-service)
which acts as `AuthorizationServer` in our environment.
 
You can create new tokens and validate existing tokens. In most of our cases are roles sufficient to manage our permissions.
Therefore we currently only support `scope=default` (can be omitted in requests).

Find out more details about [OAuth2](https://tools.ietf.org/html/rfc6749) from RFC.

### Details

#### How to authorize a customer
To authenticate a customer using his credentials you need to request our [token endpoint](#oauthtoken). 
A customer in our system is by default an advanced oauth2-client. Therefore a customer must have accessible resource-ids assigned.

To grant customers access on certain resource-ids, simply use the `login-client` (client_secret: `wz$kkY<rx!PhE6kc`) when creating the authorization token.
This client should have all necessary resource-ids assigned. This client can only be used for `password` grant_types.

#### How to register a new customer
To register a new customer you must use an authorization from a client which has the `registration` scope or the `ROLE_EMPLOYEE` role assigned.
Eg: `register-client@Z<93^}xCtMSNe)5[`.

You must create a token using the client_credentials. Then you are able to use this token to register new customers.
This is necessary to avoid 3rd parties to create customers and to get rid of a `ROLE_SYSTEM` client for this purpose.

in HTTP:
- `POST localhost:15100/oauth/token?client_id=register-client&grant_type=client_credentials&client_secret=Z<93^}xCtMSNe)5[`
- `POST localhost:15100/customers` using created token
```json
{
	"email": "my@mail.de",
	"username": "my@mail.de",
	"password": "my@mail.de",
	"firstName": "someone",
	"lastName": "as example"
}
```
- `POST localhost:15100/oauth/token?client_id=login-client&grant_type=password&client_secret=wz$kkY<rx!PhE6kc&username=my@mail.de&password=my@mail.de`
- Grab the generated token and create a practice / appointments etc

---

A "token" is identified by a UUID. If you want to use a token to authorize your request, 
simply add the `Authorization`-Header to your request.

Example:  
`Authorization: Bearer my-token`

Currently we only support the following `grant_type`s:
- `client_credenials`
- `password`

### `security` module
To make things easier, you could use our `com.arzttermine.service.profile:security` module.
This module provides some useful utilities and makes the configuration of our security easier.

- `RemoteTokenServicesSettings` requied settings object to configure your application client
- `RemotetokenServiceConfigurer` to configure springs `RemoteTokenServices` to authorize against our profile service API
- `ResourceServerConfigurer` to configure a new resource-server in our system (see OAuth2 for more details)
- `SecurityContextUtil` providing some utilities to access the current authentication and the requesting principal
  - Please note that this utility is not thread safe!
  - The principal of an `UserAuthentication` is the corresponding customerId (long)
  
This is how your configuration could look like:
```java
@Configuration
public class ResourceServerConfiguration extends ResourceServerConfigurer
{
    @Autowired
    private RemoteTokenServices remoteTokenServices;

    @Value("${spring.application.name}")
    private String applicationName;

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception
    {
        resources.resourceId(applicationName); // we use the service name as resource-id
        resources.tokenServices(remoteTokenServices);
    }

    @Configuration
    @Profile("!testing")
    public static class RemoteTokenServicesConfiguration extends RemoteTokenServiceConfigurer
    {
        @Autowired
        private RemoteTokenServicesSettings tokenServicesSettings;

        @Bean
        public RemoteTokenServices remoteTokenServices()
        {
            return configure(tokenServicesSettings);
        }
    }
}
```
  
Even for testing purposes there is an helper called `AuthBuilderUtil`. 
This can be used in tests to create a valid OAuth2-authentication.

### API

#### `/oauth/token`
This endpoint creates new tokens if there is no one present. 
You should not use this endpoint to validate an existing token (for performance reasons).
Consider to use `/oauth/check_token` instead. 

This is how your request could look like:

```
POST /oauth/token?client_id=calendar-client&client_secret=calendar-client-secret&grant_type=client_credentials
```

If you want to use `password`-grant_type to authenticate a customer, you have to add a username and password:

```
POST /oauth/token?client_id=client-with-password-grant&client_secret=secret&grant_type=password&username=my-username&password=plain-password
```

**Please note:** 

You can use the token for granted resources only.  
*Example:* `calendar-client` only has the granted `resource_id` "profile-service". 
Hence you cannot use any tokens, created with `calendar-client` to request another service (eg: history-service).

Additionally you need a client which supports the desired `grant_type`.   
*Example:* `calendar-client` only has the grant_type `client_credentials`. 
Hence you cannot use any tokens, created with `calendar-client` to authenticate a customer using grant_type `password`.

Doctors in our system are normal customer. The only way to distinguish between doctors and customers is an additional authority.
Doctors gain the `ROLE_DOCTOR` permission and additional permissions for the corresponding practice `ROLE_PRACTICE_{PRACTICE_ID}`.

Example response:
```json
{
  "access_token": "250c8b49-abef-4df0-ab93-7c6ea1871bdb",
  "token_type": "bearer",
  "expires_in": 9999,
  "scope": "default"
}
```

#### `/oauth/check_token`
To validate an existing token you can use the `check_token` endpoint. 
This endpoint returns the following information for an successful authorization:
- expiration time (`exp` as UNIX timestamp)
- granted authorities (`authorities` as list of strings)
- client-id used to create the token (`client_id` as string)
- granted scopes (`scope` as list of strings)
- granted resource-ids (`aud` as list of strings)

```
POST /oauth/check_token?token=46c34813-4eeb-4d75-8ba2-e05dfc84dd83
Authorization: Basic Ym9va2luZy1jbGllbnQ6N3YpeT4yQVFYOy4/djlWIw==
```

To avoid brute-force attacks even when we go live with our profile-service, you need a valid client authentication to ask for the validity of an token. You have to use http-basic additional to authenticate the requesting client (see Authorization example using `booking-client`). 

If the token is not valid anymore we will return `400` including a json object:
```json
{
  "error": "invalid_token",
  "error_description": "Token was not recognised"
}
```

You might also get the following if we know whether the token is not recognised or expired:
```json
{
  "error": "invalid_token",
  "error_description": "Access token expired: a2206013-df2b-4b3b-a59b-d786143833a1"
}
```
