# search

To search for practices and doctors use elasticsearch!

You'll get the following DTO as practice document:
(Please double check as the structure might change a lot over the time)

```json
{
  "id": 1,
  "name": "Example office",
  "location": {
    "id": 0,
    "country": "GERMANY",
    "longitude": 13.4039,
    "latitude": 52.5255,
    "location": "Rosenthaler Straße 51",
    "city": "Berlin",
    "zip": "12345",
    "phone": "optional phone",
    "phoneMobile": "optional mobile phone",
    "contactMail": "optional mail",
    "primary": false
  },
  "ownerId": 1,
  "medicalSpecialties": [
    "DENTIST"
  ],
  "doctors": [
    {
      "id": 1,
      "email": "example@example.com",
      "firstName": "Doctor",
      "lastName": "Sowieso",
      "preferredLanguage": "GERMAN",
      "gender": "MALE",
      "profileId": 1,
      "fullName": "Doctor Sowieso"
    }
  ],
  "treatmentTypes": [
    {
      "id": 2,
      "doctorIds": [
        1 
      ],
      "type": "TYPE2",
      "forcePay": false,
      "practiceId": 1,
      "freeTreatment": true
    },
    {
      "id": 1,
      "doctorIds": [
        1
      ],
      "type": "TYPE",
      "charge": {
        "amount": 1000,
        "currency": "EUR"
      },
      "forcePay": true,
      "practiceId": 1,
      "freeTreatment": false
    },
    {
      "id": 3,
      "doctorIds": [
        1
      ],
      "type": "TYPE3",
      "charge": {
        "amount": 100,
        "currency": "EUR"
      },
      "forcePay": false,
      "practiceId": 1,
      "freeTreatment": false
    }
  ],
  "resources": [
    {
      "id": 1,
      "systemId": 1,
      "name": "Doctor Sowieso",
      "type": "DOCTOR"
    }
  ],
  "widgetConfiguration": {
    "fontFamily": "Arial",
    "foregroundPrimaryColor": "#333",
    "foregroundSecondaryColor": "#666",
    "background": "rgba(200, 220, 210, .8)",
    "success": {
      "text": "thanks mate!",
      "link": "http://optional/go/to"
    }
  }
}
```

### Notes:
- `treatmentTypes[*].charge` may not be set when there is no charge
- `ownerId` may not be set when there is no owner
- consider to use customerId/practiceId or something else to manage the avatar path
- `treatments[*].doctorIds` refers to the `doctors[*].profileId`
