package com.arzttermine.service.payment.messages;

import com.arzttermine.library.messaging.model.TopicMessage;
import com.arzttermine.library.web.dto.PriceDto;

import java.time.Instant;
import java.util.Date;
import java.util.Map;

public class TransactionPaidMessage extends TopicMessage
{
    private long transactionId;
    private Long bookingId;
    private PriceDto total;
    private Date paidAt = Date.from(Instant.now());
    private Date createdAt;
    private String payerFullName;
    private String payerEmail;
    private Map<String, String> paymentDetails;

    public long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(long transactionId) {
        this.transactionId = transactionId;
    }

    public Long getBookingId() {
        return bookingId;
    }

    public void setBookingId(Long bookingId) {
        this.bookingId = bookingId;
    }

    public PriceDto getTotal() {
        return total;
    }

    public void setTotal(PriceDto total) {
        this.total = total;
    }

    public Date getPaidAt() {
        return paidAt;
    }

    public void setPaidAt(Date paidAt) {
        this.paidAt = paidAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Map<String, String> getPaymentDetails() {
        return paymentDetails;
    }

    public void setPaymentDetails(Map<String, String> paymentDetails) {
        this.paymentDetails = paymentDetails;
    }

    public String getPayerFullName() {
        return payerFullName;
    }

    public void setPayerFullName(String payerFullName) {
        this.payerFullName = payerFullName;
    }

    public String getPayerEmail() {
        return payerEmail;
    }

    public void setPayerEmail(String payerEmail) {
        this.payerEmail = payerEmail;
    }

    @Override
    public String getTopic() {
        return "paid";
    }

    @Override
    public String getTopicKind() {
        return "transaction";
    }
}
