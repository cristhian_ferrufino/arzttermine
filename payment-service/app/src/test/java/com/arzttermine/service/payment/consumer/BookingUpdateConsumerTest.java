package com.arzttermine.service.payment.consumer;

import com.arzttermine.service.booking.messages.BookingCancelledMessage;
import com.arzttermine.service.payment.domain.entity.PayingCustomer;
import com.arzttermine.service.payment.domain.entity.PaymentMethod;
import com.arzttermine.service.payment.domain.entity.PaymentTransaction;
import com.arzttermine.service.payment.domain.repository.PayingCustomerRepository;
import com.arzttermine.service.payment.domain.repository.PaymentTransactionRepository;
import com.arzttermine.service.payment.service.payments.RefundProvider;
import com.arzttermine.service.payment.service.payments.RefundResult;
import com.arzttermine.service.payment.web.struct.PaymentMethodType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class BookingUpdateConsumerTest
{
    @Mock
    private PaymentTransactionRepository transactionRepository;

    @Mock
    private PayingCustomerRepository customerRepository;

    @Spy
    private List<RefundProvider> refundProviders = new ArrayList<>();

    @InjectMocks
    private BookingUpdateConsumer updateConsumer;

    @Test
    public void refund_should_ignore_non_existing_transactions()
    {
        BookingCancelledMessage cancelledMessage = new BookingCancelledMessage();
        cancelledMessage.setBookingId(42L);

        when(transactionRepository.findOneByBookingId(42L)).thenReturn(null);

        updateConsumer.cancelled(cancelledMessage);
    }

    @Test
    public void refund_should_ignore_transactions_without_payment_methods()
    {
        BookingCancelledMessage cancelledMessage = new BookingCancelledMessage();
        cancelledMessage.setBookingId(42L);

        PaymentTransaction transaction = new PaymentTransaction();
        when(transactionRepository.findOneByBookingId(42L)).thenReturn(transaction);

        updateConsumer.cancelled(cancelledMessage);
    }

    @Test
    public void refund_should_ignore_missing_refund_provider()
    {
        BookingCancelledMessage cancelledMessage = new BookingCancelledMessage();
        cancelledMessage.setBookingId(42L);

        PayingCustomer customer = new PayingCustomer();
        PaymentTransaction transaction = new PaymentTransaction();
        transaction.setPaymentMethod(PaymentMethod.builder(PaymentMethodType.CREDIT_CARD)
            .customer(customer)
            .build());

        when(transactionRepository.findOneByBookingId(42L)).thenReturn(transaction);

        updateConsumer.cancelled(cancelledMessage);
    }

    @Test
    public void refund_should_ignore_transactions_from_different_customers()
    {
        BookingCancelledMessage cancelledMessage = new BookingCancelledMessage();
        cancelledMessage.setBookingId(42L);
        cancelledMessage.setCustomerId(23L);

        PayingCustomer customer = new PayingCustomer();
        PaymentTransaction transaction = new PaymentTransaction();
        transaction.setCustomerId(999999999L);
        transaction.setPaymentMethod(PaymentMethod.builder(PaymentMethodType.CREDIT_CARD)
            .externalId("externalId")
            .customer(customer)
            .build());

        RefundResult refundResult = RefundResult.of("refund_id");

        RefundProvider provider = mock(RefundProvider.class);
        when(provider.supports(PaymentMethodType.CREDIT_CARD)).thenReturn(true);
        when(provider.createRefund(transaction)).thenReturn(refundResult);
        refundProviders.add(provider);

        when(transactionRepository.findOneByBookingId(42L)).thenReturn(transaction);
        when(customerRepository.findByCustomerId(23L)).thenReturn(customer);

        updateConsumer.cancelled(cancelledMessage);

        assertTrue(customer.getTransactions().isEmpty());
    }

    @Test
    public void refund_add_outcome_transaction_to_customer()
    {
        BookingCancelledMessage cancelledMessage = new BookingCancelledMessage();
        cancelledMessage.setBookingId(42L);
        cancelledMessage.setCustomerId(23L);

        PayingCustomer customer = spy(new PayingCustomer());
        when(customer.getCustomerId()).thenReturn(cancelledMessage.getCustomerId());

        PaymentMethod paymentMethod = PaymentMethod.builder(PaymentMethodType.CREDIT_CARD)
            .externalId("externalId")
            .customer(customer)
            .build();

        PaymentTransaction transaction = new PaymentTransaction();
        transaction.setPaymentMethod(paymentMethod);
        transaction.confirm(Instant.now());
        transaction.setCustomerId(23L);
        transaction.setAmount(400);

        RefundResult refundResult = RefundResult.of("refund_id");

        RefundProvider provider = mock(RefundProvider.class);
        when(provider.supports(PaymentMethodType.CREDIT_CARD)).thenReturn(true);
        when(provider.createRefund(transaction)).thenReturn(refundResult);
        refundProviders.add(provider);

        when(transactionRepository.findOneByBookingId(42L)).thenReturn(transaction);
        when(customerRepository.findByCustomerId(23L)).thenReturn(customer);

        updateConsumer.cancelled(cancelledMessage);

        assertFalse(customer.getTransactions().isEmpty());
        assertEquals("refund_id", customer.getTransactions().get(0).getExternalId());
        assertEquals(-400, customer.getTransactions().get(0).getAmount());
        assertEquals(paymentMethod, customer.getTransactions().get(0).getPaymentMethod());
        assertFalse(customer.getTransactions().get(0).isIncome());
        assertFalse(customer.getTransactions().get(0).isPaid()); // unconfirmed
    }
}
