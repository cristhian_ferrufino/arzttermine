package com.arzttermine.service.payment.service.externals.test;

import com.arzttermine.service.payment.config.settings.StripeSettings;
import com.arzttermine.service.payment.service.externals.impl.StripeSourceClient;
import com.stripe.model.Customer;
import com.stripe.model.ExternalAccount;

import java.util.concurrent.CompletableFuture;

import static org.mockito.Mockito.*;

public class TestStripeSourceClient extends StripeSourceClient
{
    public TestStripeSourceClient() {
        super(new StripeSettings());
    }

    @Override
    public CompletableFuture<ExternalAccount> getFrom(final Customer customer, final String sourceId)
    {
        return customer.getSources().getData().stream()
            .filter(s -> s.getId().equals(sourceId))
            .map(CompletableFuture::completedFuture)
            .findFirst()
            .orElseGet(() -> create(customer, sourceId));
    }

    @Override
    public CompletableFuture<ExternalAccount> create(final Customer customer, final String sourceId)
    {
        ExternalAccount account = spy(new ExternalAccount());
        when(account.getId()).thenReturn(sourceId);
        account.setCustomer(customer.getId());

        return CompletableFuture.completedFuture(account);
    }
}
