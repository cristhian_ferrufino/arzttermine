package com.arzttermine.service.payment;

import com.arzttermine.service.payment.domain.repository.PaymentTransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.Instant;

@Component
@Profile("testing")
public class PaymentTransactionTestHelper
{
    @Autowired
    private PaymentTransactionRepository transactionRepository;

    @Transactional(readOnly = false)
    public void markAsPaid(@PathVariable long bookingId) {
        transactionRepository.findOneByBookingId(bookingId).confirm(Instant.now());
    }
}
