package com.arzttermine.service.payment.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;

import javax.sql.DataSource;

@Configuration
@Profile("testing")
public class DataSourceTestConfiguration
{
    @Bean
    public DataSource dataSource()
    {
        return new EmbeddedDatabaseBuilder()
            .setName("payment")
            .setType(EmbeddedDatabaseType.H2)
            .build();
    }

    @Bean
    public JpaTransactionManager transactionManager()
    {
        return new JpaTransactionManager();
    }
}
