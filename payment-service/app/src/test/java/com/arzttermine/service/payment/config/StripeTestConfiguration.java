package com.arzttermine.service.payment.config;

import com.arzttermine.service.payment.service.externals.impl.StripeChargeClient;
import com.arzttermine.service.payment.service.externals.impl.StripeCustomerClient;
import com.arzttermine.service.payment.service.externals.impl.StripeSourceClient;
import com.arzttermine.service.payment.service.externals.test.TestStripeChargeClient;
import com.arzttermine.service.payment.service.externals.test.TestStripeCustomerClient;
import com.arzttermine.service.payment.service.externals.test.TestStripeSourceClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import static org.mockito.Mockito.*;

@Configuration
@Profile("testing")
public class StripeTestConfiguration
{
    @Bean
    public StripeChargeClient stripeChargeClient()
    {
        return spy(new TestStripeChargeClient());
    }

    @Bean
    public StripeSourceClient stripeSourceClient()
    {
        return spy(new TestStripeSourceClient());
    }

    @Bean
    public StripeCustomerClient stripeCustomerClient()
    {
        return spy(new TestStripeCustomerClient());
    }
}
