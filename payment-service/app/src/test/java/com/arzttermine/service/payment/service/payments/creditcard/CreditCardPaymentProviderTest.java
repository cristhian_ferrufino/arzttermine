package com.arzttermine.service.payment.service.payments.creditcard;

import com.arzttermine.library.suport.exception.EntityNotFoundException;
import com.arzttermine.library.web.dto.PriceDto;
import com.arzttermine.service.payment.domain.entity.PayingCustomer;
import com.arzttermine.service.payment.domain.repository.PayingCustomerRepository;
import com.arzttermine.service.payment.domain.repository.PaymentMethodRepository;
import com.arzttermine.service.payment.exception.ChargeNotCreatedException;
import com.arzttermine.service.payment.exception.PaymentNotAuthorizedException;
import com.arzttermine.service.payment.service.externals.impl.StripeChargeClient;
import com.arzttermine.service.payment.service.externals.impl.StripeCustomerClient;
import com.arzttermine.service.payment.service.externals.impl.StripeSourceClient;
import com.arzttermine.service.payment.service.payments.creditcard.CreditCardPaymentProvider;
import com.arzttermine.service.payment.web.dto.request.CreateChargeDto;
import com.stripe.model.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class CreditCardPaymentProviderTest
{
    @Mock
    private StripeCustomerClient customerClient;

    @Mock
    private StripeChargeClient chargeClient;

    @Mock
    private StripeSourceClient sourceClient;

    @Mock
    private PayingCustomerRepository customerRepository;

    @Mock
    private PaymentMethodRepository paymentMethodRepository;

    @InjectMocks
    private CreditCardPaymentProvider ccProvider;

    @Before
    public void setup() {
        when(paymentMethodRepository.findOneByCustomerIdAndExternalId(anyLong(), anyString()))
            .thenReturn(Optional.empty());
    }

    @Test(expected = EntityNotFoundException.class)
    public void createCharge_should_report_invalid_account()
    {
        when(customerRepository.save(any(PayingCustomer.class)))
            .thenAnswer(invocation -> invocation.getArgumentAt(0, PayingCustomer.class));

        ExternalAccount account = spy(new ExternalAccount());
        when(account.getId()).thenReturn("accountId");
        account.setCustomer("different-one-is-the-owner");

        ExternalAccountCollection accounts = new ExternalAccountCollection();
        accounts.setData(Collections.singletonList(account));

        Customer stripeCustomer = new Customer();
        stripeCustomer.setId("externalId");
        stripeCustomer.setSources(accounts);
        stripeCustomer.setEmail("example@customer.de");

        PriceDto charge = new PriceDto(1000);

        when(customerClient.create(42L, "example@customer.de", "token"))
            .thenReturn(CompletableFuture.completedFuture(stripeCustomer));

        CreateChargeDto cc = requestDto("token", charge);
        ccProvider.createPayment(cc, null);
    }

    @Test(expected = PaymentNotAuthorizedException.class)
    public void createCharge_should_report_invalid_account_when_email_differs()
    {
        PayingCustomer customer = new PayingCustomer();
        customer.setExternalId("externalId");
        customer.setCustomerId(42L);

        ExternalAccount account = spy(new ExternalAccount());
        when(account.getId()).thenReturn("accountId");
        account.setCustomer("externalId");

        ExternalAccountCollection accounts = new ExternalAccountCollection();
        accounts.setData(Collections.singletonList(account));

        Customer stripeCustomer = new Customer();
        stripeCustomer.setId("externalId");
        stripeCustomer.setSources(accounts);
        stripeCustomer.setEmail("somone@else.come");

        PriceDto charge = new PriceDto(1000);

        when(customerClient.get("externalId"))
            .thenReturn(CompletableFuture.completedFuture(stripeCustomer));

        CreateChargeDto cc = requestDto("token", charge);
        ccProvider.createPayment(cc, customer);
    }

    @Test(expected = ChargeNotCreatedException.class)
    public void createCharge_should_report_failed_charging()
    {
        when(customerRepository.save(any(PayingCustomer.class)))
            .thenAnswer(invocation -> invocation.getArgumentAt(0, PayingCustomer.class));

        ExternalAccount account = spy(new ExternalAccount());
        when(account.getId()).thenReturn("accountId");
        account.setCustomer("externalId");

        ExternalAccountCollection accounts = new ExternalAccountCollection();
        accounts.setData(Collections.singletonList(account));

        Customer stripeCustomer = new Customer();
        stripeCustomer.setId("externalId");
        stripeCustomer.setSources(accounts);
        stripeCustomer.setEmail("example@customer.de");

        ChargeOutcome outcome = new ChargeOutcome();
        outcome.setNetworkStatus("fraud dude!");

        PriceDto charge = new PriceDto(1000);
        Charge result = new Charge();
        result.setId("charge1");
        result.setOutcome(outcome);

        when(chargeClient.create("charge_42_for_66", charge, account, "example"))
            .thenReturn(CompletableFuture.completedFuture(result));

        when(customerClient.create(42L, "example@customer.de", "token"))
            .thenReturn(CompletableFuture.completedFuture(stripeCustomer));

        CreateChargeDto cc = requestDto("token", charge);
        ccProvider.createPayment(cc, null);
    }

    @Test
    public void createCharge_should_create_new_paying_customer_if_not_present()
    {
        when(customerRepository.save(any(PayingCustomer.class))).thenAnswer(invocation -> {
            PayingCustomer customer = invocation.getArgumentAt(0, PayingCustomer.class);
            assertEquals("externalId", customer.getExternalId());

            return customer;
        });

        ExternalAccount account = spy(new ExternalAccount());
        when(account.getId()).thenReturn("accountId");
        account.setCustomer("externalId");

        ExternalAccountCollection accounts = new ExternalAccountCollection();
        accounts.setData(Collections.singletonList(account));

        Customer stripeCustomer = new Customer();
        stripeCustomer.setId("externalId");
        stripeCustomer.setSources(accounts);
        stripeCustomer.setEmail("example@customer.de");

        ChargeOutcome outcome = new ChargeOutcome();
        outcome.setNetworkStatus("approved_by_network");

        PriceDto charge = new PriceDto(1000);
        Charge result = new Charge();
        result.setId("charge1");
        result.setOutcome(outcome);

        when(chargeClient.create("charge_42_for_66", charge, account, "example"))
            .thenReturn(CompletableFuture.completedFuture(result));

        when(customerClient.create(42L, "example@customer.de", "token"))
            .thenReturn(CompletableFuture.completedFuture(stripeCustomer));

        CreateChargeDto cc = requestDto("token", charge);
        ccProvider.createPayment(cc, null);

        verify(customerRepository, times(1)).save(any(PayingCustomer.class));
        verify(customerClient, times(1)).create(42L, "example@customer.de", "token");
        verify(chargeClient, times(1)).create("charge_42_for_66", charge, account, "example");
    }

    @Test
    public void createCharge_should_charge_existing_customer_if_present()
    {
        PayingCustomer customer = new PayingCustomer();
        customer.setExternalId("externalId");
        customer.setCustomerId(42L);

        when(customerRepository.findByCustomerId(42L)).thenReturn(customer);

        ExternalAccount account = spy(new ExternalAccount());
        when(account.getId()).thenReturn("accountId");
        account.setCustomer("externalId");

        ExternalAccountCollection accounts = new ExternalAccountCollection();
        accounts.setData(Collections.singletonList(account));

        Customer stripeCustomer = new Customer();
        stripeCustomer.setId("externalId");
        stripeCustomer.setSources(accounts);
        stripeCustomer.setEmail("example@customer.de");

        ChargeOutcome outcome = new ChargeOutcome();
        outcome.setNetworkStatus("approved_by_network");

        PriceDto charge = new PriceDto(1000);
        Charge result = new Charge();
        result.setId("charge1");
        result.setOutcome(outcome);

        when(chargeClient.create("charge_42_for_66", charge, account, "example"))
            .thenReturn(CompletableFuture.completedFuture(result));

        when(sourceClient.getFrom(stripeCustomer, "may-existing"))
            .thenReturn(CompletableFuture.completedFuture(account));

        when(customerClient.get("externalId"))
            .thenReturn(CompletableFuture.completedFuture(stripeCustomer));

        CreateChargeDto cc = requestDto("may-existing", charge);
        ccProvider.createPayment(cc, customer);

        verify(customerRepository, never()).save(any(PayingCustomer.class));
        verify(customerClient, never()).create(anyLong(), anyString(), anyString());
        verify(sourceClient, times(1)).getFrom(stripeCustomer, "may-existing");
        verify(chargeClient, times(1)).create("charge_42_for_66", charge, account, "example");
    }

    private CreateChargeDto requestDto(String token, PriceDto price)
    {
        final CreateChargeDto charge = new CreateChargeDto();
        charge.setDescription("example");
        charge.setBookingId(66L);
        charge.setToken(token);
        charge.setPrice(price);
        charge.setCustomerId(42L);
        charge.setCustomerMail("example@customer.de");

        return charge;
    }
}
