package com.arzttermine.service.payment.service;

import com.arzttermine.service.payment.domain.entity.PayingCustomer;
import com.arzttermine.service.payment.domain.entity.PaymentTransaction;
import com.arzttermine.service.payment.domain.repository.PayingCustomerRepository;
import com.arzttermine.service.payment.domain.repository.PaymentTransactionRepository;
import com.arzttermine.service.payment.exception.AlreadyPaidException;
import com.arzttermine.service.payment.exception.UnsupportedPaymentException;
import com.arzttermine.service.payment.service.payments.creditcard.CreditCardPaymentProvider;
import com.arzttermine.service.payment.service.payments.PaymentProvider;
import com.arzttermine.service.payment.web.dto.request.CreateChargeDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PaymentServiceTest
{
    @Spy
    private List<PaymentProvider> providers = new ArrayList<>(
        Collections.singletonList(new CreditCardPaymentProvider())
    );

    @Mock
    private PayingCustomerRepository customerRepository;

    @Mock
    private PaymentTransactionRepository transactionRepository;

    @InjectMocks
    private PaymentService paymentService;

    @Before
    public void setup() {
        PayingCustomer customer = new PayingCustomer();
        customer.setCustomerId(42L);
        customer.setExternalId("egal");

        when(customerRepository.findByCustomerId(42L)).thenReturn(customer);
        when(transactionRepository.findOneByBookingId(anyLong())).thenReturn(null);
    }

    @Test(expected = UnsupportedPaymentException.class)
    public void createCharge_should_find_corresponding_provider()
    {
        CreateChargeDto cc = new CreateChargeDto();
        cc.setCustomerId(42L);
        cc.setMethodType(null);

        paymentService.createCharge(cc);
    }

    @Test(expected = AlreadyPaidException.class)
    public void createCharge_should_prevent_duplicate_transactions()
    {
        PaymentTransaction transaction = new PaymentTransaction();
        transaction.confirm(Instant.now());

        when(transactionRepository.findOneByBookingId(anyLong())).thenReturn(transaction);

        CreateChargeDto cc = new CreateChargeDto();
        cc.setCustomerId(42L);
        cc.setMethodType(null);

        paymentService.createCharge(cc);
    }
}
