package com.arzttermine.service.payment.service.externals.test;

import com.arzttermine.library.web.dto.PriceDto;
import com.arzttermine.service.payment.config.settings.StripeSettings;
import com.arzttermine.service.payment.domain.entity.PaymentTransaction;
import com.arzttermine.service.payment.service.externals.impl.StripeChargeClient;
import com.stripe.model.*;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

public class TestStripeChargeClient extends StripeChargeClient
{
    private static final Map<String, ChargeOutcome> outcomes = new ConcurrentHashMap<>();

    public TestStripeChargeClient() {
        super(new StripeSettings());
    }

    public static void outcome(String onIdentifier, ChargeOutcome outcome) {
        outcomes.put(onIdentifier, outcome);
    }

    @Override
    public CompletableFuture<Charge> create(String identifier, PriceDto price, String customerId, String sourceId, String description)
    {
        ChargeOutcome defaultOutcome = new ChargeOutcome();
        defaultOutcome.setNetworkStatus("approved_by_network");
        defaultOutcome.setType("authorized");

        Charge charge = new Charge();
        charge.setId(identifier);
        charge.setCustomer(customerId);
        charge.setDescription(description);
        charge.setAmount((long) price.getAmount());
        charge.setCurrency(price.getCurrency().name());
        charge.setOutcome(outcomes.getOrDefault(identifier, defaultOutcome));

        try {
            charge.setSource(new TestStripeSourceClient().create(new Customer(), sourceId).get());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return CompletableFuture.completedFuture(charge);
    }

    @Override
    public CompletableFuture<Refund> refund(PaymentTransaction transaction)
    {
        Refund refund = new Refund();
        refund.setId("refund_id");
        refund.setAmount((long) transaction.getAmount());

        return CompletableFuture.completedFuture(refund);
    }

    @Override
    public CompletableFuture<Charge> create(String identifier, PriceDto price, ExternalAccount account, String description)
    {
        return create(identifier, price, account.getCustomer(), account.getId(), description);
    }

    @Override
    public CompletableFuture<Charge> get(String id)
    {
        return execute(() -> Charge.retrieve(id, options));
    }
}
