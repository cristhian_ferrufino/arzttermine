package com.arzttermine.service.payment.consumer;

import com.arzttermine.library.messaging.processor.MessageBus;
import com.arzttermine.library.web.dto.PriceDto;
import com.arzttermine.service.booking.messages.BookingCancelledMessage;
import com.arzttermine.service.payment.PaymentTransactionTestHelper;
import com.arzttermine.service.payment.service.externals.impl.StripeChargeClient;
import com.arzttermine.service.payment.web.BaseIntegrationTest;
import com.arzttermine.service.payment.web.controller.PaymentControllerTest;
import com.arzttermine.service.payment.web.dto.request.CreateChargeDto;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class BookingUpdateConsumerIntegrationTest extends BaseIntegrationTest
{
    @Autowired
    private MessageBus messageBus;

    @Autowired
    private StripeChargeClient chargeClient;

    @Autowired
    private PaymentTransactionTestHelper transactionTestHelper;

    @Before
    public void setup() {
        reset(chargeClient);
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void refund_should_initiate_refund_to_customer() throws Exception
    {
        CreateChargeDto request = PaymentControllerTest.chargeRequestDto();
        request.setPrice(new PriceDto(666));
        request.setCustomerId(42L);
        request.setToken("tokenized");
        request.setBookingId(66L);

        mockMvc.perform(systemRequest(() -> post("/payments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isNoContent());

        verify(chargeClient, times(1)).create(eq("charge_42_for_66"), any(), any(), anyString());

        transactionTestHelper.markAsPaid(66L);

        BookingCancelledMessage cancel = new BookingCancelledMessage();
        cancel.setAppointmentId(23L);
        cancel.setCustomerId(42L);
        cancel.setBookingId(66L);
        messageBus.send(cancel);

        Thread.sleep(200);

        mockMvc.perform(systemRequest(() -> get("/payments/transactions")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.elements[*]", hasSize(2)))
            .andExpect(jsonPath("$.elements[0].paid", is(true)))
            .andExpect(jsonPath("$.elements[1].paid", is(false)))
            .andExpect(jsonPath("$.elements[0].externalId", is("charge_42_for_66")))
            .andExpect(jsonPath("$.elements[1].externalId", is("refund_id")))
            .andExpect(jsonPath("$.elements[*].bookingId", containsInAnyOrder(66, 66)))
            .andExpect(jsonPath("$.elements[0].price.amount", is(666)))
            .andExpect(jsonPath("$.elements[1].price.amount", is(-666)));

        verify(chargeClient, times(1)).refund(any());
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void refund_should_not_initiate_refund_to_customer_if_transaction_was_not_paid() throws Exception
    {
        CreateChargeDto request = PaymentControllerTest.chargeRequestDto();
        request.setPrice(new PriceDto(666));
        request.setCustomerId(42L);
        request.setToken("tokenized");
        request.setBookingId(66L);

        mockMvc.perform(systemRequest(() -> post("/payments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isNoContent());

        verify(chargeClient, times(1)).create(eq("charge_42_for_66"), any(), any(), anyString());

        BookingCancelledMessage cancel = new BookingCancelledMessage();
        cancel.setAppointmentId(23L);
        cancel.setCustomerId(42L);
        cancel.setBookingId(66L);
        messageBus.send(cancel);

        Thread.sleep(200);

        mockMvc.perform(systemRequest(() -> get("/payments/transactions")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.elements[*]", hasSize(1)))
            .andExpect(jsonPath("$.elements[0].paid", is(false)))
            .andExpect(jsonPath("$.elements[0].externalId", is("charge_42_for_66")))
            .andExpect(jsonPath("$.elements[0].bookingId", is(66)))
            .andExpect(jsonPath("$.elements[0].price.amount", is(666)));

        verify(chargeClient, never()).refund(any());
    }
}
