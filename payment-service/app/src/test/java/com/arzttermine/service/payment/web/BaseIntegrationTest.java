package com.arzttermine.service.payment.web;

import com.arzttermine.library.suport.functional.ThrowableSupplier;
import com.arzttermine.service.payment.Application;
import com.arzttermine.service.profile.security.support.AuthBuilderUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.PostConstruct;
import java.util.UUID;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

@SpringBootTest(
    classes = {Application.class},
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@RunWith(SpringRunner.class)
public abstract class BaseIntegrationTest
{
    protected MockMvc mockMvc;

    @Autowired
    protected ObjectMapper objectMapper;

    @Autowired
    private WebApplicationContext applicationContext;

    @Autowired
    private FilterChainProxy filterChainProxy;

    @Autowired
    private RemoteTokenServices tokenServices;

    private final AuthBuilderUtil authBuilder = new AuthBuilderUtil();

    @PostConstruct
    public void setupSpringBootIntegrationTest()
    {
        mockMvc = MockMvcBuilders.webAppContextSetup(applicationContext)
            .addFilter(filterChainProxy).build();
    }

    protected String sendDto(Object dto) throws JsonProcessingException
    {
        return objectMapper.writeValueAsString(dto);
    }

    private MockHttpServletRequestBuilder configureAuthenticatedRequest(long customerId, String[] roles, ThrowableSupplier<MockHttpServletRequestBuilder> requestAction) throws Exception
    {
        final OAuth2Authentication oauth = authBuilder.buildAuthForCustomer(customerId, roles);
        final UUID token = UUID.randomUUID();

        when(tokenServices.loadAuthentication(eq(token.toString()))).thenReturn(oauth);

        final MockHttpServletRequestBuilder request = requestAction.get();
        request.header("Authorization", "Bearer " + token);
        SecurityContextHolder.getContext().setAuthentication(oauth);

        return request;
    }

    protected MockHttpServletRequestBuilder customerRequest(long customer, ThrowableSupplier<MockHttpServletRequestBuilder> requestAction) throws Exception
    {
        return configureAuthenticatedRequest(customer, new String[]{"ROLE_CUSTOMER", "ROLE_CLIENT"}, requestAction);
    }

    protected MockHttpServletRequestBuilder employeeRequest(long employee, ThrowableSupplier<MockHttpServletRequestBuilder> requestAction) throws Exception
    {
        return configureAuthenticatedRequest(employee, new String[]{"ROLE_EMPLOYEE", "ROLE_CUSTOMER", "ROLE_CLIENT"}, requestAction);
    }

    protected MockHttpServletRequestBuilder systemRequest(ThrowableSupplier<MockHttpServletRequestBuilder> requestAction) throws Exception
    {
        return configureAuthenticatedRequest(42L, new String[]{"ROLE_SYSTEM", "ROLE_CLIENT"}, requestAction);
    }

    protected MockHttpServletRequestBuilder clientRequest(ThrowableSupplier<MockHttpServletRequestBuilder> requestAction) throws Exception
    {
        return configureAuthenticatedRequest(23L, new String[]{"ROLE_CLIENT"}, requestAction);
    }
}
