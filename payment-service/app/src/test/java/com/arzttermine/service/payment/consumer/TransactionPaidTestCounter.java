package com.arzttermine.service.payment.consumer;

import com.arzttermine.library.messaging.consumer.Consumer;
import com.arzttermine.service.payment.messages.TransactionPaidMessage;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicInteger;

@Component
@Profile("testing")
public class TransactionPaidTestCounter
{
    private final AtomicInteger counter = new AtomicInteger(0);

    @Consumer
    public void customerCharged(TransactionPaidMessage message)
    {
        counter.incrementAndGet();
    }

    public int getCounter() {
        return counter.intValue();
    }

    public void flush() {
        counter.set(0);
    }
}
