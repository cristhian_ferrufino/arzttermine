package com.arzttermine.service.payment.service.externals.test;

import com.arzttermine.service.payment.config.settings.StripeSettings;
import com.arzttermine.service.payment.service.externals.impl.StripeCustomerClient;
import com.stripe.model.Customer;
import com.stripe.model.ExternalAccount;
import com.stripe.model.ExternalAccountCollection;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import static org.mockito.Mockito.*;

public class TestStripeCustomerClient extends StripeCustomerClient
{
    private static final Map<String, Customer> customers = new HashMap<>();

    public TestStripeCustomerClient() {
        super(new StripeSettings());
    }

    public static void customer(String id, Customer customer) {
        customers.put(id, customer);
    }

    @Override
    public CompletableFuture<Customer> create(Long id, String email, String source)
    {
        ExternalAccount account = spy(new ExternalAccount());
        when(account.getId()).thenReturn("accountId");
        account.setCustomer("externalId");

        ExternalAccountCollection collection = new ExternalAccountCollection();
        collection.setData(Collections.singletonList(account));
        collection.setCount(1);

        Customer customer = new Customer();
        customer.setId("externalId");
        customer.setAccountBalance(0L);
        customer.setDefaultSource(source);
        customer.setEmail(email);
        customer.setSources(collection);

        return CompletableFuture.completedFuture(customer);
    }

    @Override
    public CompletableFuture<Customer> get(String id)
    {
        if (customers.containsKey(id)) {
            return CompletableFuture.completedFuture(customers.get(id));
        }

        Customer customer;

        try {
            customer = create(42L, "test@example.com", "tokenized").get();
        } catch(Exception e) {
            throw new RuntimeException(e);
        }

        return CompletableFuture.completedFuture(customer);
    }
}
