package com.arzttermine.service.payment.web.controller;

import com.arzttermine.library.suport.exception.ServiceUnavailableException;
import com.arzttermine.library.web.dto.PriceDto;
import com.arzttermine.service.payment.consumer.TransactionPaidTestCounter;
import com.arzttermine.service.payment.service.externals.impl.StripeChargeClient;
import com.arzttermine.service.payment.service.externals.impl.StripeCustomerClient;
import com.arzttermine.service.payment.service.externals.impl.StripeSourceClient;
import com.arzttermine.service.payment.web.BaseIntegrationTest;
import com.arzttermine.service.payment.web.dto.request.CreateChargeDto;
import com.arzttermine.service.payment.web.struct.PaymentMethodType;
import com.stripe.model.Charge;
import com.stripe.model.ChargeOutcome;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicInteger;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class PaymentControllerTest extends BaseIntegrationTest
{
    @Autowired
    private StripeCustomerClient customerClient;

    @Autowired
    private StripeSourceClient sourceClient;

    @Autowired
    private StripeChargeClient chargeClient;

    @Autowired
    private TransactionPaidTestCounter transactionPaidTestCounter;

    private AtomicInteger counter = new AtomicInteger(0);

    @After
    public void flush() {
        counter = new AtomicInteger(0);
    }

    @Before
    public void init() {
        // maybe there are already some messages
        transactionPaidTestCounter.flush();
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void pay_should_create_charge_and_transaction() throws Exception
    {
        reset(chargeClient);

        CreateChargeDto request = chargeRequestDto();
        request.setPrice(new PriceDto(666));
        request.setCustomerId(42L);
        request.setToken("tokenized");
        request.setBookingId(66L);

        mockMvc.perform(systemRequest(() -> post("/payments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isNoContent());

        mockMvc.perform(systemRequest(() -> get("/payments/transactions")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.elements[*]", hasSize(1)))
            .andExpect(jsonPath("$.elements[0].paid", is(false)))
            .andExpect(jsonPath("$.elements[0].externalId", is("charge_42_for_66")))
            .andExpect(jsonPath("$.elements[0].bookingId", is(66)));

        verify(chargeClient, times(1)).create(eq("charge_42_for_66"), any(), any(), anyString());

        Thread.sleep(150);
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void pay_should_retry_on_service_outage() throws Exception
    {
        CreateChargeDto request = chargeRequestDto();
        request.setPrice(new PriceDto(666));
        request.setCustomerId(42L);
        request.setToken("tokenized");
        request.setBookingId(66L);

        ChargeOutcome defaultOutcome = new ChargeOutcome();
        defaultOutcome.setNetworkStatus("approved_by_network");
        defaultOutcome.setType("authorized");

        Charge charge = new Charge();
        charge.setId("something");
        charge.setCustomer("externalId");
        charge.setDescription("description");
        charge.setAmount((long) request.getPrice().getAmount());
        charge.setCurrency(request.getPrice().getCurrency().name());
        charge.setOutcome(defaultOutcome);

        reset(chargeClient);
        doAnswer(invocation -> failUntil(3, () -> CompletableFuture.completedFuture(charge)))
            .when(chargeClient).create(eq("charge_42_for_66"), any(), anyString(), anyString(), anyString());

        mockMvc.perform(systemRequest(() -> post("/payments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isNoContent());

        verify(chargeClient, times(3)).create(eq("charge_42_for_66"), any(), any(), anyString());
    }

    @Test
    public void pay_should_retry_on_service_outage_max_3_times() throws Exception
    {
        CreateChargeDto request = chargeRequestDto();
        request.setPrice(new PriceDto(666));
        request.setCustomerId(42L);
        request.setToken("tokenized");
        request.setBookingId(67L);

        reset(chargeClient);
        doAnswer(invocation -> failUntil(99, () -> null)).when(chargeClient)
            .create(eq("charge_42_for_67"), any(), anyString(), anyString(), anyString());

        mockMvc.perform(systemRequest(() -> post("/payments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isServiceUnavailable());

        verify(chargeClient, times(3)).create(eq("charge_42_for_67"), any(), anyString(), anyString(), anyString());
    }

    private <T> T failUntil(int call, ThrowableSupplier<T> result) throws Throwable
    {
        if (counter.incrementAndGet() == call) {
            return result.supply();
        }

        throw new ServiceUnavailableException("something is not available");
    }

    public static CreateChargeDto chargeRequestDto()
    {
        CreateChargeDto dto = new CreateChargeDto();
        dto.setCustomerFullName("full name");
        dto.setCustomerMail("test@example.com");
        dto.setMethodType(PaymentMethodType.CREDIT_CARD);
        dto.setDescription("example");
        return dto;
    }

    @FunctionalInterface
    public interface ThrowableSupplier<T>
    {
        T supply() throws java.lang.Throwable;
    }
}
