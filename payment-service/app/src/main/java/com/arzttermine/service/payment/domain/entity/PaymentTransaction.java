package com.arzttermine.service.payment.domain.entity;

import com.arzttermine.service.payment.support.InstantToTimestampConverter;
import com.arzttermine.service.payment.web.struct.AdditionalData;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;

@Entity
@Table(
    name = "payment_transactions"
)
public class PaymentTransaction
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false, insertable = false)
    private long id;

    @Column(name = "customer_id", nullable = false, updatable = false)
    private long customerId;

    @JoinColumn(
        name = "payment_method_id",
        updatable = false,
        referencedColumnName = "id"
    )
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private PaymentMethod paymentMethod;

    @Column(name = "external_id", nullable = false, updatable = false)
    private String externalId;

    @Column(nullable = false, updatable = false)
    private int amount;

    @Convert(converter = InstantToTimestampConverter.class)
    @Column(name = "created_at", nullable = false, updatable = false)
    private Instant createdAt = Instant.now();

    @Convert(converter = InstantToTimestampConverter.class)
    private Instant captured = Instant.now();

    @Column(name = "confirmed")
    @Convert(converter = InstantToTimestampConverter.class)
    private Instant confirmedAt;

    @Column(name = "booking_id", nullable = false, updatable = false)
    private long bookingId;

    @Column(name = "full_name", updatable = false)
    private String fullName;

    @Column(nullable = false, updatable = false)
    private String email;

    @Transient
    private transient AdditionalData data;

    public long getId() {
        return id;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public Instant getCaptured() {
        return captured;
    }

    public void setCaptured(Instant captured) {
        this.captured = captured;
    }

    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public long getBookingId() {
        return bookingId;
    }

    public void setBookingId(long bookingId) {
        this.bookingId = bookingId;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public boolean isPaid() {
        return null != confirmedAt && null != captured;
    }

    public boolean isIncome() {
        return amount >= 0;
    }

    public Instant getConfirmedAt() {
        return confirmedAt;
    }

    public void confirm(Instant at) {
        confirmedAt = at;
        if (null == captured) {
            captured = at;
        }
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public @NotNull @Transient AdditionalData getData() {
        if (null == data) {
            data = new AdditionalData();
        }

        return data;
    }
}
