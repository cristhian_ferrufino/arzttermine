package com.arzttermine.service.payment.service.externals.stripe;

import com.stripe.exception.*;

@FunctionalInterface
public interface StripeSupplier<T>
{
    T get() throws AuthenticationException,
                InvalidRequestException,
                APIConnectionException,
                CardException,
                APIException;
}
