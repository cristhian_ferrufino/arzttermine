package com.arzttermine.service.payment.consumer;

import com.arzttermine.library.messaging.consumer.Consumer;
import com.arzttermine.library.suport.exception.ServiceUnavailableException;
import com.arzttermine.service.booking.messages.BookingCancelledMessage;
import com.arzttermine.service.payment.domain.entity.PayingCustomer;
import com.arzttermine.service.payment.domain.entity.PaymentTransaction;
import com.arzttermine.service.payment.domain.repository.PayingCustomerRepository;
import com.arzttermine.service.payment.domain.repository.PaymentTransactionRepository;
import com.arzttermine.service.payment.service.payments.RefundProvider;
import com.arzttermine.service.payment.service.payments.RefundResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class BookingUpdateConsumer
{
    private static final Logger log = LoggerFactory.getLogger(BookingUpdateConsumer.class);

    @Autowired
    private PaymentTransactionRepository transactionRepository;

    @Autowired
    private PayingCustomerRepository customerRepository;

    @Autowired
    private List<RefundProvider> refundProviders;

    @Consumer
    @Retryable(
        maxAttempts = 3,
        backoff = @Backoff(value = 500, multiplier = 2),
        include = {ServiceUnavailableException.class}
    )
    public void cancelled(BookingCancelledMessage message)
    {
        final PaymentTransaction trx = transactionRepository.findOneByBookingId(message.getBookingId());
        if (null == trx) {
            log.info("could not find transaction related to booking {}", message.getBookingId());
            return;
        }

        log.info("trying to refund money for transaction {} to customer {}", trx.getId(), message.getCustomerId());

        final PayingCustomer customer = customerRepository.findByCustomerId(message.getCustomerId());
        if (null == customer) {
            log.error("could not find customer {} to refund money to", message.getCustomerId());
            return;
        }
        else if (null == trx.getPaymentMethod()) {
            log.error("cannot refund money to a non existing account for customer {}", message.getCustomerId());
            return;
        }
        else if (!trx.isPaid() || message.getCustomerId() != customer.getCustomerId()) {
            log.info("transaction was not paid from customer {}. Nothing to do here", message.getCustomerId());
            return;
        }

        final Optional<RefundProvider> provider = refundProviders.stream()
            .filter(p -> p.supports(trx.getPaymentMethod().getType()))
            .findFirst();

        if (!provider.isPresent()) {
            log.error("no provider found to refund money to payment method {}", trx.getPaymentMethod().getId());
            return;
        }

        final RefundResult result = provider.get().createRefund(trx);

        final PaymentTransaction refund = new PaymentTransaction();
        refund.setExternalId(result.getExternalRefundId());
        refund.setPaymentMethod(trx.getPaymentMethod());
        refund.setCustomerId(trx.getCustomerId());
        refund.setBookingId(message.getBookingId());
        refund.setAmount(-1 * trx.getAmount());
        refund.setFullName(trx.getFullName());
        refund.setEmail(trx.getEmail());
        customer.getTransactions().add(refund);

        log.info(
            "refunded money {} to customer {} for booking {}",
            trx.getAmount(),
            message.getCustomerId(),
            message.getBookingId()
        );
    }
}
