package com.arzttermine.service.payment.service.externals.impl;

import com.arzttermine.service.payment.config.settings.StripeSettings;
import com.arzttermine.service.payment.service.externals.stripe.StripeClient;
import com.stripe.model.Customer;
import com.stripe.model.ExternalAccount;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

public class StripeSourceClient extends StripeClient
{
    public StripeSourceClient(StripeSettings settings) {
        super(settings);
    }

    public CompletableFuture<ExternalAccount> getFrom(final Customer customer, final String sourceId)
    {
        return customer.getSources().getData().stream()
            .filter(s -> s.getId().equals(sourceId))
            .findFirst()
            .map(CompletableFuture::completedFuture)
            .orElseGet(() -> create(customer, sourceId));
    }

    public CompletableFuture<ExternalAccount> create(final Customer customer, final String sourceId)
    {
        final Map<String, Object> params = new HashMap<>();
        params.put("source", sourceId);

        return execute(() -> customer.getSources().create(params, options));
    }
}
