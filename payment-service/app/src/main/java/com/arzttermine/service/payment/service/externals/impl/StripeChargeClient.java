package com.arzttermine.service.payment.service.externals.impl;

import com.arzttermine.library.web.dto.PriceDto;
import com.arzttermine.service.payment.config.settings.StripeSettings;
import com.arzttermine.service.payment.domain.entity.PaymentTransaction;
import com.arzttermine.service.payment.service.externals.stripe.StripeClient;
import com.stripe.model.Charge;
import com.stripe.model.ExternalAccount;
import com.stripe.model.Refund;
import com.stripe.net.RequestOptions;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

public class StripeChargeClient extends StripeClient
{
    public StripeChargeClient(StripeSettings settings) {
        super(settings);
    }

    public CompletableFuture<Charge> create(String identifier, PriceDto price, String customerId, String sourceId, String description)
    {
        final Map<String, Object> params = new HashMap<>();
        params.put("capture", true);
        params.put("source", sourceId);
        params.put("customer", customerId);
        params.put("description", description);
        params.put("amount", price.getAmount());
        params.put("currency", price.getCurrency().name());
        params.put("statement_descriptor", "Terminbuchung");

        final RequestOptions idempotent = new RequestOptions.RequestOptionsBuilder()
            .setApiKey(options.getApiKey())
            .setIdempotencyKey(identifier)
            .build();

        return execute(() -> Charge.create(params, idempotent));
    }

    public CompletableFuture<Charge> create(String identifier, PriceDto price, ExternalAccount account, String description)
    {
        return create(identifier, price, account.getCustomer(), account.getId(), description);
    }

    public CompletableFuture<Refund> refund(PaymentTransaction transaction)
    {
        final Map<String, Object> params = new HashMap<>();
        params.put("charge", transaction.getExternalId());

        final String identifier = String.format("refund_%d_for_%d", transaction.getCustomerId(), transaction.getBookingId());
        final RequestOptions idempotent = new RequestOptions.RequestOptionsBuilder()
            .setApiKey(options.getApiKey())
            .setIdempotencyKey(identifier)
            .build();

        return execute(() -> Refund.create(params, idempotent));
    }

    public CompletableFuture<Charge> get(String id)
    {
        return execute(() -> Charge.retrieve(id, options));
    }
}
