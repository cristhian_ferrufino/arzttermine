package com.arzttermine.service.payment.service.payments;

import com.arzttermine.service.payment.domain.entity.PayingCustomer;
import com.arzttermine.service.payment.web.dto.request.CreateChargeDto;
import com.arzttermine.service.payment.web.struct.PaymentMethodType;

public interface PaymentProvider
{
    PaymentResult createPayment(CreateChargeDto request, PayingCustomer customer);

    boolean supports(PaymentMethodType paymentMethodType);
}
