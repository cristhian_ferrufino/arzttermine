package com.arzttermine.service.payment.web.controller;

import com.arzttermine.service.payment.service.WebhookService;
import com.stripe.model.Event;
import com.stripe.net.APIResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/payments/webhooks")
public class WebhookController
{
    @Autowired
    private WebhookService webhookService;

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAnyRole('ROLE_STRIPE', 'ROLE_SYSTEM')")
    @RequestMapping(value = "/charges/stripe", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void stripeChargeProcessed(@RequestBody String plain)
    {
        final Event event = APIResource.GSON.fromJson(plain, Event.class);
        webhookService.stripeChargeSucceeded(event);
    }
}
