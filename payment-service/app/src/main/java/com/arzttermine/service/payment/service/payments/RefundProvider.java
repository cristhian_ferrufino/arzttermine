package com.arzttermine.service.payment.service.payments;

import com.arzttermine.service.payment.domain.entity.PaymentTransaction;
import com.arzttermine.service.payment.web.struct.PaymentMethodType;

public interface RefundProvider
{
    RefundResult createRefund(PaymentTransaction transaction);

    boolean supports(PaymentMethodType paymentMethodType);
}
