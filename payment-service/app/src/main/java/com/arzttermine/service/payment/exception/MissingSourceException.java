package com.arzttermine.service.payment.exception;

import com.arzttermine.library.suport.exception.BusinessException;

public class MissingSourceException extends BusinessException
{
    public MissingSourceException(boolean multiple) {
        super((multiple) ? "NO_UNIQUE_SOURCE" : "MISSING_SOURCE", null, "The customer does not have any sources attached or could not be uniquely identified.");
    }
}
