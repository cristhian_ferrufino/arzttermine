package com.arzttermine.service.payment.domain.repository;

import com.arzttermine.service.payment.domain.entity.PaymentTransaction;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentTransactionRepository extends PagingAndSortingRepository<PaymentTransaction, Long>
{
    PaymentTransaction findOneByBookingId(long bookingId);

    PaymentTransaction findOneByExternalId(String externalId);
}
