package com.arzttermine.service.payment.config;

import com.arzttermine.service.payment.config.settings.StripeSettings;
import com.arzttermine.service.payment.service.externals.impl.StripeChargeClient;
import com.arzttermine.service.payment.service.externals.impl.StripeCustomerClient;
import com.arzttermine.service.payment.service.externals.impl.StripeSourceClient;
import com.stripe.Stripe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import javax.annotation.PostConstruct;

@Configuration
@Profile("!testing")
public class StripeConfiguration
{
    @Autowired
    private StripeSettings settings;

    @PostConstruct
    public void setGlobalToken()
    {
        Stripe.apiKey = settings.getToken();
    }

    @Bean
    public StripeChargeClient stripeChargeClient()
    {
        return new StripeChargeClient(settings);
    }

    @Bean
    public StripeSourceClient stripeSourceClient()
    {
        return new StripeSourceClient(settings);
    }

    @Bean
    public StripeCustomerClient stripeCustomerClient()
    {
        return new StripeCustomerClient(settings);
    }
}
