package com.arzttermine.service.payment.service.mapper;

import com.arzttermine.library.web.dto.PriceDto;
import com.arzttermine.service.payment.domain.entity.PaymentTransaction;
import com.arzttermine.service.payment.web.dto.response.TransactionDto;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class TransactionMapper
{
    public TransactionDto toDto(final PaymentTransaction transaction)
    {
        final TransactionDto dto = new TransactionDto();
        dto.setId(transaction.getId());
        dto.setExternalId(transaction.getExternalId());
        dto.setBookingId(transaction.getBookingId());
        dto.setPaid(transaction.isPaid());
        dto.setCreatedAt(Date.from(transaction.getCreatedAt()));
        dto.setPrice(new PriceDto(transaction.getAmount()));

        return dto;
    }
}
