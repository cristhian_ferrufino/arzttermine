package com.arzttermine.service.payment.service.payments.creditcard;

import com.arzttermine.library.suport.exception.ServiceUnavailableException;
import com.arzttermine.service.payment.domain.entity.PaymentTransaction;
import com.arzttermine.service.payment.service.externals.impl.StripeChargeClient;
import com.arzttermine.service.payment.service.payments.RefundProvider;
import com.arzttermine.service.payment.service.payments.RefundResult;
import com.arzttermine.service.payment.web.struct.PaymentMethodType;
import com.stripe.model.Refund;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutionException;

@Component
public class CreditCardRefundProvider implements RefundProvider
{
    private static final Logger log = LoggerFactory.getLogger(CreditCardRefundProvider.class);

    @Autowired
    private StripeChargeClient chargeClient;

    @Override
    public RefundResult createRefund(PaymentTransaction transaction) {
        final Refund refund;

        try {
            refund = chargeClient.refund(transaction).get();
        } catch (InterruptedException e) {
            log.warn("could not await refund creation", e);
            throw new ServiceUnavailableException("refund");
        } catch (ExecutionException e) {
            log.info("error while trying to refund money from transaction {}", transaction.getId(), e);
            throw (RuntimeException) e.getCause();
        }

        return RefundResult.of(refund.getId());
    }

    @Override
    public boolean supports(PaymentMethodType paymentMethodType) {
        return null != paymentMethodType && paymentMethodType.equals(PaymentMethodType.CREDIT_CARD);
    }
}
