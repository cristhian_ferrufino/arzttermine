package com.arzttermine.service.payment.service.payments.creditcard;

import com.arzttermine.library.suport.exception.EntityNotFoundException;
import com.arzttermine.library.suport.exception.ServiceUnavailableException;
import com.arzttermine.library.web.dto.PriceDto;
import com.arzttermine.service.payment.domain.entity.PayingCustomer;
import com.arzttermine.service.payment.domain.entity.PaymentMethod;
import com.arzttermine.service.payment.domain.repository.PayingCustomerRepository;
import com.arzttermine.service.payment.domain.repository.PaymentMethodRepository;
import com.arzttermine.service.payment.exception.ChargeNotCreatedException;
import com.arzttermine.service.payment.exception.MissingSourceException;
import com.arzttermine.service.payment.exception.PaymentNotAuthorizedException;
import com.arzttermine.service.payment.service.externals.impl.StripeChargeClient;
import com.arzttermine.service.payment.service.externals.impl.StripeCustomerClient;
import com.arzttermine.service.payment.service.externals.impl.StripeSourceClient;
import com.arzttermine.service.payment.service.payments.PaymentProvider;
import com.arzttermine.service.payment.service.payments.PaymentResult;
import com.arzttermine.service.payment.web.dto.request.CreateChargeDto;
import com.arzttermine.service.payment.web.struct.PaymentMethodType;
import com.stripe.model.Charge;
import com.stripe.model.Customer;
import com.stripe.model.ExternalAccount;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.concurrent.ExecutionException;

@Component
public class CreditCardPaymentProvider implements PaymentProvider
{
    private static final Logger log = LoggerFactory.getLogger(CreditCardPaymentProvider.class);

    @Autowired
    private PayingCustomerRepository customerRepository;

    @Autowired
    private PaymentMethodRepository paymentMethodRepository;

    @Autowired
    private StripeCustomerClient customerClient;

    @Autowired
    private StripeSourceClient sourceClient;

    @Autowired
    private StripeChargeClient chargeClient;

    @Override
    public PaymentResult createPayment(final CreateChargeDto request, PayingCustomer customer) {
        final long customerId = request.getCustomerId();
        final PaymentMethod paymentMethod;
        final ExternalAccount account;

        if (null == customer) {
            final Customer externalCustomer = createCustomer(customerId, request.getCustomerMail(), request.getToken());

            if (externalCustomer.getSources().getData().isEmpty()) {
                throw new MissingSourceException(false);
            }

            account = externalCustomer.getSources().getData().get(0);
            customer = newPayingCustomer(customerId, externalCustomer.getId());
            paymentMethod = newPaymentMethod(customer, account.getId());
        } else {
            final Customer externalCustomer = getCustomer(customer.getExternalId());
            if (!externalCustomer.getEmail().equals(request.getCustomerMail())) {
                throw new PaymentNotAuthorizedException(null);
            }

            account = getOrCreateAccount(externalCustomer, request.getToken());

            final Optional<PaymentMethod> existing = paymentMethodRepository
                .findOneByCustomerIdAndExternalId(customer.getCustomerId(), request.getToken());

            if (!existing.isPresent()) {
                paymentMethod = newPaymentMethod(customer, account.getId());
            } else {
                paymentMethod = existing.get();
            }
        }

        if (!account.getCustomer().equals(customer.getExternalId())) {
            throw new EntityNotFoundException("Source", null);
        }

        final String guid = String.format("charge_%d_for_%d", customerId, request.getBookingId());
        final Charge charge = createCharge(guid, request.getPrice(), account, request.getDescription());
        final String response = charge.getOutcome().getNetworkStatus();

        if (!response.equals("approved_by_network")) {
            throw new ChargeNotCreatedException(response, charge.getOutcome().getReason());
        }

        return PaymentResult.of(charge.getId(), paymentMethod, customer);
    }

    @Override
    public boolean supports(PaymentMethodType paymentMethodType) {
        return null != paymentMethodType && paymentMethodType.equals(PaymentMethodType.CREDIT_CARD);
    }

    private PayingCustomer newPayingCustomer(final long customerId, final String externalId) {
        final PayingCustomer customer = new PayingCustomer();
        customer.setExternalId(externalId);
        customer.setCustomerId(customerId);

        return customerRepository.save(customer);
    }

    private PaymentMethod newPaymentMethod(final PayingCustomer customer, final String externalAccountId)
    {
        final PaymentMethod paymentMethod = PaymentMethod.builder(PaymentMethodType.CREDIT_CARD)
            .customer(customer)
            .externalId(externalAccountId)
            .build();

        customer.getPaymentMethods().add(paymentMethod);
        return paymentMethodRepository.save(paymentMethod);
    }

    /**
     * Creates a new customer on stripes side.
     * As we do it just in time when a new customer should be charged,
     * we also attach a new payment-method to this customer immediately.
     * Using {@code tokenizedSource} which represents the tokenized payment-method
     *
     * @param id our own customer-id
     * @param email customers-email (used for receipts etc)
     * @param tokenizedSource token for a new or even existing payment-method from stripe
     * @return Customer from stripe
     */
    private Customer createCustomer(final long id, String email, String tokenizedSource) {
        try {
            return customerClient.create(id, email, tokenizedSource).get();
        } catch (Exception e) {
            log.error("could not create new customer", e);
            throw wrap(e);
        }
    }

    private Customer getCustomer(final String customerId) {
        try {
            return customerClient.get(customerId).get();
        } catch (Exception e) {
            log.error("could not retrieve customer {} from stripe", customerId, e);
            throw wrap(e);
        }
    }

    private Charge createCharge(final String idempotentKey, final PriceDto price, final ExternalAccount account, String description) {
        try {
            return chargeClient.create(idempotentKey, price, account, description).get();
        } catch (Exception e) {
            log.error("could not charge customer on external account {}", account.getId(), e);
            throw wrap(e);
        }
    }

    private ExternalAccount getOrCreateAccount(final Customer customer, final String token) {
        try {
            return sourceClient.getFrom(customer, token).get();
        } catch (Exception e) {
            log.error("could not get or create external account", e);
            throw wrap(e);
        }
    }

    private RuntimeException wrap(Exception e) {
        if (e instanceof ExecutionException) {
            return (RuntimeException) e.getCause();
        }
        if (e instanceof InterruptedException) {
            return new ServiceUnavailableException("payment");
        }

        return new ServiceUnavailableException("stripe");
    }
}
