package com.arzttermine.service.payment.domain.repository;

import com.arzttermine.service.payment.domain.entity.PaymentMethod;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PaymentMethodRepository extends JpaRepository<PaymentMethod, Long>
{
    Optional<PaymentMethod> findOneByCustomerIdAndExternalId(long customerId, String externalId);
}
