package com.arzttermine.service.payment.service.payments;

import com.arzttermine.service.payment.domain.entity.PayingCustomer;
import com.arzttermine.service.payment.domain.entity.PaymentMethod;

public class PaymentResult
{
    private final PayingCustomer customer;
    private final PaymentMethod paymentMethod;
    private final String externalChargeId;

    private PaymentResult(String externalChargeId, PaymentMethod paymentMethod, PayingCustomer customer) {
        this.externalChargeId = externalChargeId;
        this.paymentMethod = paymentMethod;
        this.customer = customer;
    }

    public static PaymentResult of(String externalChargeId, PaymentMethod paymentMethod, PayingCustomer customer) {
        return new PaymentResult(externalChargeId, paymentMethod, customer);
    }

    public PayingCustomer getCustomer() {
        return customer;
    }

    public String getExternalChargeId() {
        return externalChargeId;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }
}
