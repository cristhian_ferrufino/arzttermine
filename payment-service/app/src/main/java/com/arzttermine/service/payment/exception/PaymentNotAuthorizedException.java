package com.arzttermine.service.payment.exception;

import com.arzttermine.library.suport.exception.BusinessException;

public class PaymentNotAuthorizedException extends BusinessException
{
    public PaymentNotAuthorizedException(String capture) {
        super("UNAUTHORIZED_PAYMENT", capture, "The payment authorization is expired, not found or not valid");
    }
}
