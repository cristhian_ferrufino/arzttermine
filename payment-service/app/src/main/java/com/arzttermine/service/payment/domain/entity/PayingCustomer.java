package com.arzttermine.service.payment.domain.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(
    name = "paying_customers",
    indexes = {
        @Index(name = "id_customer_id", columnList = "customer_id")
    }
)
public class PayingCustomer
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false, insertable = false)
    private long id;

    @OneToMany(
        mappedBy = "customerId",
        fetch = FetchType.LAZY,
        cascade = {CascadeType.PERSIST, CascadeType.DETACH}
    )
    private List<PaymentTransaction> transactions = new ArrayList<>();

    @OneToMany(
        mappedBy = "customerId",
        fetch = FetchType.LAZY,
        cascade = CascadeType.ALL,
        orphanRemoval = true
    )
    private List<PaymentMethod> paymentMethods = new ArrayList<>();

    @Column(name = "external_id", nullable = false)
    private String externalId;

    @Column(name = "customer_id", nullable = false, updatable = false)
    private long customerId;

    public long getId() {
        return id;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public List<PaymentTransaction> getTransactions() {
        return transactions;
    }

    public List<PaymentMethod> getPaymentMethods() {
        return paymentMethods;
    }
}
