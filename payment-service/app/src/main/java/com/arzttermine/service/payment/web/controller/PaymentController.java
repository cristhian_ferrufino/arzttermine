package com.arzttermine.service.payment.web.controller;

import com.arzttermine.service.payment.service.PaymentService;
import com.arzttermine.service.payment.web.dto.request.CreateChargeDto;
import com.arzttermine.service.payment.web.dto.response.TransactionsPageDto;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/payments")
public class PaymentController
{
    @Autowired
    private PaymentService paymentService;

    @ApiOperation("Creates a new charge")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("#oauth2.hasScope('payment') or hasRole('ROLE_SYSTEM')")
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void pay(@RequestBody @Valid CreateChargeDto request)
    {
        paymentService.createCharge(request);
    }

    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("#oauth2.hasScope('transactions') or hasRole('ROLE_SYSTEM')")
    @RequestMapping(value = "/transactions", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public TransactionsPageDto getTransactions(Pageable pageable)
    {
        return paymentService.getTransactions(pageable);
    }
}
