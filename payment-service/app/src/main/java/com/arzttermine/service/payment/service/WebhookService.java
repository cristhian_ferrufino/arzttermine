package com.arzttermine.service.payment.service;

import com.arzttermine.library.messaging.processor.MessageBus;
import com.arzttermine.library.web.dto.PriceDto;
import com.arzttermine.service.payment.domain.entity.PaymentTransaction;
import com.arzttermine.service.payment.domain.repository.PaymentTransactionRepository;
import com.arzttermine.service.payment.messages.TransactionPaidMessage;
import com.arzttermine.service.payment.web.struct.AdditionalData;
import com.stripe.model.Card;
import com.stripe.model.Charge;
import com.stripe.model.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionalEventListener;

import java.sql.Date;
import java.time.Instant;
import java.util.Map;

@Service
public class WebhookService
{
    private static final Logger log = LoggerFactory.getLogger(WebhookService.class);

    @Autowired
    private PaymentTransactionRepository transactionRepository;

    @Autowired
    private MessageBus messageBus;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Async
    @Transactional(readOnly = false)
    public void stripeChargeSucceeded(final Event event)
    {
        final Charge charge = (Charge) event.getData().getObject();
        log.info("got webhook about succeeded stripe charge {}", charge.getId());

        final PaymentTransaction trx = transactionRepository.findOneByExternalId(charge.getId());
        if (null == trx) {
            log.error("PLEASE CHECK: could not find transaction related to charge {}", charge.getId());
            return;
        }

        if (trx.isPaid()) {
            log.info("{} related payment transaction {} is already paid", charge.getId(), trx.getId());
            return;
        }

        if (trx.getAmount() != charge.getAmount()) {
            log.warn("PLEASE CHECK: transaction {} was not paid as desired by charge {} (expected: {}, paid: {})",
                trx.getId(), charge.getId(), trx.getAmount(), charge.getAmount());
        }

        final Card card = (Card) charge.getSource();
        trx.getData().getMap().put(AdditionalData.DataType.LAST_4.getDetailsType(), card.getLast4());
        trx.getData().getMap().put(AdditionalData.DataType.METHOD_BRAND.getDetailsType(), card.getBrand());
        trx.getData().getMap().put(AdditionalData.DataType.EXPIRE_MONTH.getDetailsType(), card.getExpMonth().toString());
        trx.getData().getMap().put(AdditionalData.DataType.EXPIRE_YEAR.getDetailsType(), card.getExpYear().toString());

        trx.confirm(Instant.now());
        eventPublisher.publishEvent(trx);
    }

    @TransactionalEventListener(condition = "#transaction.isPaid()")
    public void customerChargedForBooking(final PaymentTransaction transaction)
    {
        log.info("publishing transaction paid message for transaction {}", transaction.getId());

        final TransactionPaidMessage message = new TransactionPaidMessage();
        message.setBookingId(transaction.getBookingId());
        message.setTotal(new PriceDto(transaction.getAmount()));
        message.setTransactionId(transaction.getId());
        message.setPayerFullName(transaction.getFullName());
        message.setPayerEmail(transaction.getEmail());
        message.setCreatedAt(Date.from(transaction.getCreatedAt()));
        message.setPaidAt(Date.from(Instant.now()));

        final Map<String, String> data = transaction.getData().getMap();

        message.setPaymentDetails(data);

        messageBus.send(message);
    }
}
