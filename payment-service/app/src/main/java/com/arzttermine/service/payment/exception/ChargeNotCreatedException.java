package com.arzttermine.service.payment.exception;

import com.arzttermine.library.suport.exception.BusinessException;

public class ChargeNotCreatedException extends BusinessException
{
    public ChargeNotCreatedException(String reason, String reference) {
        super(reason, reference, "Payment is not authorized due to %s", reason);
    }
}
