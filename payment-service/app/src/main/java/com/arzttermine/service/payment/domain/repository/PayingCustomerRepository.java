package com.arzttermine.service.payment.domain.repository;

import com.arzttermine.service.payment.domain.entity.PayingCustomer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PayingCustomerRepository extends JpaRepository<PayingCustomer, Long>
{
    PayingCustomer findByCustomerId(long customerId);
}
