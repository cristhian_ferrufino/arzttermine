package com.arzttermine.service.payment.exception;

import com.arzttermine.library.suport.exception.BusinessException;

public class UnsupportedPaymentException extends BusinessException
{
    public UnsupportedPaymentException() {
        super("UNSUPPORTED_PAYMENT", "methodType", "The desired payment method is not supported");
    }
}
