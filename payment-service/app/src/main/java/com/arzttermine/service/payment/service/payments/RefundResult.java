package com.arzttermine.service.payment.service.payments;

public class RefundResult
{
    private String externalRefundId;

    private RefundResult(String externalRefundId) {
        this.externalRefundId = externalRefundId;
    }

    public static RefundResult of(String externalRefundId) {
        return new RefundResult(externalRefundId);
    }

    public String getExternalRefundId() {
        return externalRefundId;
    }
}
