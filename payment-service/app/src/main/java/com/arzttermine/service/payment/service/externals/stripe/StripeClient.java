package com.arzttermine.service.payment.service.externals.stripe;

import com.arzttermine.library.suport.exception.AccessDeniedException;
import com.arzttermine.library.suport.exception.InvalidArgumentException;
import com.arzttermine.library.suport.exception.ResponseErrorException;
import com.arzttermine.library.suport.exception.ServiceUnavailableException;
import com.arzttermine.service.payment.config.settings.StripeSettings;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.net.RequestOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

public abstract class StripeClient
{
    private static final Logger log = LoggerFactory.getLogger(StripeClient.class);

    private final ExecutorService executorService;
    protected final RequestOptions options;

    protected StripeClient(StripeSettings settings) {
        this.executorService = settings.getExecutorService();
        this.options = new RequestOptions.RequestOptionsBuilder()
            .setApiKey(settings.getToken())
            .build();
    }

    protected <T> CompletableFuture<T> execute(StripeSupplier<T> runnable) {
        return runInBackground(runnable);
    }

    private <T> CompletableFuture<T> runInBackground(StripeSupplier<T> supplier) {
        if (null != executorService) {
            return CompletableFuture.supplyAsync(() -> run(supplier), executorService);
        }

        return CompletableFuture.supplyAsync(() -> run(supplier));
    }

    private <T> T run(StripeSupplier<T> supplier) {
        try {
            return supplier.get();
        } catch (InvalidRequestException e) {
            log.warn("invalid request to stripe detected", e);
            throw new ResponseErrorException(e.getMessage(), e.getParam(), e.getStatusCode(), e);
        } catch (APIConnectionException e) {
            log.warn("stripe service seems to be unavailable", e);
            throw new ServiceUnavailableException("stripe");
        } catch (AuthenticationException e) {
            log.error("access to stripe was rejected", e);
            throw new AccessDeniedException();
        } catch (CardException e) {
            log.warn("stripe command does not succeed", e);
            throw new InvalidArgumentException(e.getParam(), e.getMessage());
        } catch (APIException e) {
            log.warn("something went wrong while trying to communicate with stripe", e);
            throw new ResponseErrorException(null, e.getStatusCode(), e);
        }
    }
}
