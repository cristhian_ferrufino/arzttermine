package com.arzttermine.service.payment.domain.entity;

import com.arzttermine.service.payment.support.InstantToTimestampConverter;
import com.arzttermine.service.payment.web.struct.PaymentMethodType;
import org.springframework.util.Assert;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(
    name = "payment_methods",
    indexes = {
        @Index(name = "id_payment_method_external_id", columnList = "external_id"),
        @Index(name = "id_payment_method_customer_id", columnList = "customer_id")
    }
)
public class PaymentMethod
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false, insertable = false)
    private long id;

    @Enumerated(EnumType.STRING)
    private PaymentMethodType type;

    @Column(name = "external_id")
    private String externalId;

    @Column(name = "customer_id", nullable = false, updatable = false)
    private long customerId;

    @Convert(converter = InstantToTimestampConverter.class)
    @Column(name = "created_at", nullable = false, updatable = false)
    private Instant createdAt = Instant.now();

    protected PaymentMethod() {
    }

    public PaymentMethod(Builder builder) {
        type = builder.type;
        externalId = builder.externalId;
        customerId = builder.customer.getId();
    }

    public long getId() {
        return id;
    }

    public PaymentMethodType getType() {
        return type;
    }

    public String getExternalId() {
        return externalId;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public long getCustomerId() {
        return customerId;
    }

    public static Builder builder(PaymentMethodType type) {
        return new Builder(type);
    }

    public static final class Builder
    {
        private final PaymentMethodType type;
        private PayingCustomer customer;
        private String externalId;

        private Builder(PaymentMethodType type) {
            Assert.notNull(type);
            this.type = type;
        }

        public Builder externalId(String id) {
            this.externalId = id;
            return this;
        }

        public Builder customer(PayingCustomer customer) {
            this.customer = customer;
            return this;
        }

        public PaymentMethod build() {
            Assert.notNull(customer);
            Assert.notNull(type);

            return new PaymentMethod(this);
        }
    }
}
