package com.arzttermine.service.payment.config.settings;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "webhooks")
public class ExternalClientSettings
{
    private List<ClientCredentials> grants = new ArrayList<>();

    public List<ClientCredentials> getGrants() {
        return grants;
    }

    public void setGrants(List<ClientCredentials> grants) {
        this.grants = grants;
    }

    public static final class ClientCredentials {
        private String id;
        private String secret;
        private List<String> roles;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSecret() {
            return secret;
        }

        public void setSecret(String secret) {
            this.secret = secret;
        }

        public List<String> getRoles() {
            if (null == roles) {
                roles = new ArrayList<>();
            }
            return roles;
        }

        public void setRoles(List<String> roles) {
            this.roles = roles;
        }
    }
}
