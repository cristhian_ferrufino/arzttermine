package com.arzttermine.service.payment.config;

import com.arzttermine.service.payment.config.settings.ExternalClientSettings;
import com.arzttermine.service.profile.security.config.RemoteTokenServiceConfigurer;
import com.arzttermine.service.profile.security.config.ResourceServerConfigurer;
import com.arzttermine.service.profile.security.config.settings.RemoteTokenServicesSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.common.exceptions.BadClientCredentialsException;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Configuration
public class ResourceServerConfiguration extends ResourceServerConfigurer
{
    @Autowired
    private RemoteTokenServices remoteTokenServices;

    @Value("${spring.application.name}")
    private String applicationName;

    @Autowired
    private ExternalClientSettings config;

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception
    {
        resources.resourceId(applicationName);
        resources.tokenServices(remoteTokenServices);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception
    {
        http.authorizeRequests()
            .regexMatchers(
                "/error.*",
                ".*swagger-ui.*",
                "/health",
                "/.*\\.html",
                "/.*\\.css",
                "/.*\\.js",
                "/.*\\.png",
                "/.*\\.jpe?g",
                "/.*\\.woff2?",
                "/.*\\.ttf",
                "/swagger-resources.*",
                "/v.+/api-docs.*",
                "/payments/webhooks/*"
            )
            .permitAll()
            .and()
            .authorizeRequests()
            .regexMatchers(HttpMethod.POST, "/payments/webhooks/.*")
            .authenticated()
            .and()
            .cors().disable()
            .httpBasic()
            .and()
            .authorizeRequests()
            .anyRequest()
            .authenticated()
            .and()
            .sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        BasicAuthenticationFilter basicFilter = new BasicAuthenticationFilter(new HttpBasicAuthManager(config));
        http.addFilterBefore(basicFilter, UsernamePasswordAuthenticationFilter.class);
    }

    @Component
    public final static class HttpBasicAuthManager implements AuthenticationManager
    {
        private final ExternalClientSettings config;

        HttpBasicAuthManager(ExternalClientSettings config) {
            this.config = config;
        }

        @Override
        public Authentication authenticate(Authentication authentication) throws AuthenticationException {
            final String user = (String) authentication.getPrincipal();
            final String password = (String) authentication.getCredentials();

            final Optional<ExternalClientSettings.ClientCredentials> client = config.getGrants().stream()
                .filter(c -> c.getId().equals(user))
                .filter(c -> c.getSecret().equals(password))
                .findFirst();

            if (!client.isPresent()) {
                throw new BadClientCredentialsException();
            }

            final List<GrantedAuthority> authorities = new ArrayList<>(client.get().getRoles().stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList()));

            authorities.add(new SimpleGrantedAuthority("TRUSTED_EXTERNAL"));
            return new UsernamePasswordAuthenticationToken(user, null, authorities);
        }
    }

    @Configuration
    @Profile("!testing")
    public static class RemoteTokenServicesConfigration extends RemoteTokenServiceConfigurer
    {
        @Autowired
        private RemoteTokenServicesSettings tokenServicesSettings;

        @Bean
        public RemoteTokenServices remoteTokenServices()
        {
            return configure(tokenServicesSettings);
        }
    }
}
