package com.arzttermine.service.payment.service;

import com.arzttermine.library.suport.exception.ServiceUnavailableException;
import com.arzttermine.library.web.dto.response.paging.PageDto;
import com.arzttermine.service.payment.domain.entity.PayingCustomer;
import com.arzttermine.service.payment.domain.entity.PaymentTransaction;
import com.arzttermine.service.payment.domain.repository.PayingCustomerRepository;
import com.arzttermine.service.payment.domain.repository.PaymentTransactionRepository;
import com.arzttermine.service.payment.exception.AlreadyPaidException;
import com.arzttermine.service.payment.exception.UnsupportedPaymentException;
import com.arzttermine.service.payment.service.mapper.TransactionMapper;
import com.arzttermine.service.payment.service.payments.PaymentProvider;
import com.arzttermine.service.payment.service.payments.PaymentResult;
import com.arzttermine.service.payment.web.dto.request.CreateChargeDto;
import com.arzttermine.service.payment.web.dto.response.TransactionsPageDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PaymentService
{
    private static final Logger log = LoggerFactory.getLogger(PaymentService.class);

    @Autowired
    private PayingCustomerRepository customerRepository;

    @Autowired
    private PaymentTransactionRepository transactionRepository;

    @Autowired
    private TransactionMapper transactionMapper;

    @Autowired
    private List<PaymentProvider> paymentProviders;

    @Transactional(readOnly = true)
    public TransactionsPageDto getTransactions(Pageable pageable)
    {
        final Page<PaymentTransaction> transactions = transactionRepository.findAll(pageable);
        final PageDto page = new PageDto();
        page.setTotalPages(transactions.getTotalPages());
        page.setTotalItems(transactions.getTotalElements());
        page.setCurrentPage(pageable.getPageNumber());
        page.setPageSize(pageable.getPageSize());

        final TransactionsPageDto result = new TransactionsPageDto();
        result.setPage(page);
        result.setElements(transactions.getContent().stream()
            .map(transactionMapper::toDto)
            .collect(Collectors.toList()));

        return result;
    }

    @Retryable(
        maxAttempts = 3,
        backoff = @Backoff(value = 600),
        include = {ServiceUnavailableException.class}
    )
    @Transactional(readOnly = false)
    public void createCharge(final CreateChargeDto paymentDto)
    {
        final PaymentTransaction existing = transactionRepository.findOneByBookingId(paymentDto.getBookingId());

        if (null != existing && existing.isPaid()) {
            throw new AlreadyPaidException();
        }

        PayingCustomer customer = customerRepository.findByCustomerId(paymentDto.getCustomerId());

        final PaymentResult result = paymentProviders.stream()
            .filter(p -> p.supports(paymentDto.getMethodType()))
            .findFirst()
            .orElseThrow(UnsupportedPaymentException::new)
            .createPayment(paymentDto, customer);

        if (null != result.getCustomer()) {
            customer = result.getCustomer();
        }

        final PaymentTransaction trx = new PaymentTransaction();
        trx.setCustomerId(customer.getId());
        trx.setExternalId(result.getExternalChargeId());
        trx.setBookingId(paymentDto.getBookingId());
        trx.setPaymentMethod(result.getPaymentMethod());
        trx.setAmount(paymentDto.getPrice().getAmount());
        trx.setFullName(paymentDto.getCustomerFullName());
        trx.setEmail(paymentDto.getCustomerMail());
        customer.getTransactions().add(trx);

        log.info(
            "initiated charge by booking {} for customer {} with amount {}",
            paymentDto.getBookingId(),
            paymentDto.getCustomerId(),
            paymentDto.getPrice().getAmount()
        );
    }
}
