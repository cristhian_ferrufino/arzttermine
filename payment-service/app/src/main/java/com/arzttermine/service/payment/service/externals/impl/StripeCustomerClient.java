package com.arzttermine.service.payment.service.externals.impl;

import com.arzttermine.service.payment.config.settings.StripeSettings;
import com.arzttermine.service.payment.service.externals.stripe.StripeClient;
import com.stripe.model.Customer;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

public class StripeCustomerClient extends StripeClient
{
    public StripeCustomerClient(StripeSettings settings) {
        super(settings);
    }

    public CompletableFuture<Customer> create(Long id, String email, String source)
    {
        final Map<String, Object> params = new HashMap<>();
        params.put("email", email);
        params.put("source", source);
        params.put("account_balance", 0);
        params.put("description", String.format("Customer %s #%d", email, id));

        return execute(() -> Customer.create(params, options));
    }

    public CompletableFuture<Customer> get(String id)
    {
        return execute(() -> Customer.retrieve(id, options));
    }
}
