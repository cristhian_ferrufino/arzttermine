USE payment;

CREATE TABLE `paying_customers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `customer_id` bigint(20) NOT NULL,
  `external_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `id_customer_id` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `payment_methods` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `customer_id` bigint(20) NOT NULL,
  `external_id` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `id_payment_method_external_id` (`external_id`),
  INDEX `id_payment_method_customer_id` (`customer_id`),
  CONSTRAINT `fk_payment_method_owner_id` FOREIGN KEY (`customer_id`) REFERENCES `paying_customers` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `payment_transactions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amount` int(11) NOT NULL,
  `booking_id` bigint(20) NOT NULL,
  `captured` datetime DEFAULT NULL,
  `confirmed` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `customer_id` bigint(20) NULL,
  `external_id` varchar(255) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `payment_method_id` bigint(20) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_payment_transaction_external_reference` (`external_id`),
  INDEX `fk_payment_transaction_payment_method` (`payment_method_id`),
  INDEX `fk_payment_transaction_customer_id` (`customer_id`),
  CONSTRAINT `fk_payment_transaction_payment_method_id` FOREIGN KEY (`payment_method_id`) REFERENCES `payment_methods` (`id`) ON DELETE SET NULL,
  CONSTRAINT `fk_payment_transaction_customer_id` FOREIGN KEY (`customer_id`) REFERENCES `paying_customers` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;
