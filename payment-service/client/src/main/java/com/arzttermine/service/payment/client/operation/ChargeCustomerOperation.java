package com.arzttermine.service.payment.client.operation;

import com.arzttermine.library.client.model.Operation;
import com.arzttermine.library.client.model.struct.RequestMethod;

public class ChargeCustomerOperation extends Operation.Builder<Void>
{
    @Override
    public Operation<Void> build() {
        setPath("/payments");
        setMethod(RequestMethod.POST);

        return new Operation<>(this);
    }
}
