package com.arzttermine.service.payment.client;

import com.arzttermine.library.client.config.settings.ClientHostSettings;
import com.arzttermine.library.client.model.RequestOptions;
import com.arzttermine.library.client.support.ClientAdapter;
import com.arzttermine.service.payment.client.operation.ChargeCustomerOperation;
import com.arzttermine.service.payment.web.dto.request.CreateChargeDto;

public class PaymentClient extends ClientAdapter
{
    public PaymentClient(ClientHostSettings settings) {
        super(settings.getHost(), settings.getPort(), settings.getScheme());
    }

    public void charge(CreateChargeDto request, RequestOptions options)
    {
        final ChargeCustomerOperation operation = new ChargeCustomerOperation();
        operation.setRequestBody(request);

        execute(operation, options);
    }
}
