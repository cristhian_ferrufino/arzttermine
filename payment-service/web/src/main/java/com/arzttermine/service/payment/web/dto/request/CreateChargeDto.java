package com.arzttermine.service.payment.web.dto.request;

import com.arzttermine.library.web.dto.PriceDto;
import com.arzttermine.service.payment.web.struct.PaymentMethodType;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class CreateChargeDto
{
    @NotNull @Valid
    @ApiModelProperty("amount of money which should to be withdrawn")
    private PriceDto price;

    @NotNull
    @ApiModelProperty("token for new payment-method generated with stripe.js, or existing token for recurring customers")
    private String token;

    @NotNull
    @ApiModelProperty("description of the charge")
    private String description;

    @NotNull
    @ApiModelProperty("the booking id to uniquely identify the booking")
    private long bookingId;

    @NotNull
    @ApiModelProperty("id of customer to charge")
    private long customerId;

    @NotNull
    @ApiModelProperty("email from customer. must exactly match customers email")
    private String customerMail;

    @NotNull
    @ApiModelProperty("name from customer. used for invoices")
    private String customerFullName;

    @NotNull
    @ApiModelProperty("the desired payment method")
    private PaymentMethodType methodType;

    public PriceDto getPrice() {
        return price;
    }

    public void setPrice(PriceDto price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getBookingId() {
        return bookingId;
    }

    public void setBookingId(long bookingId) {
        this.bookingId = bookingId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public String getCustomerMail() {
        return customerMail;
    }

    public void setCustomerMail(String customerMail) {
        this.customerMail = customerMail;
    }

    public PaymentMethodType getMethodType() {
        return methodType;
    }

    public void setMethodType(PaymentMethodType methodType) {
        this.methodType = methodType;
    }

    public String getCustomerFullName() {
        return customerFullName;
    }

    public void setCustomerFullName(String customerFullName) {
        this.customerFullName = customerFullName;
    }
}
