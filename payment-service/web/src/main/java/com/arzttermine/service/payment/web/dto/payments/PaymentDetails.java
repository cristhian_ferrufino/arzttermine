package com.arzttermine.service.payment.web.dto.payments;

import java.util.Map;

public abstract class PaymentDetails
{
    abstract Map<String, String> toDetailsMap();
}
