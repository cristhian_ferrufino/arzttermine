package com.arzttermine.service.payment.web.struct;

import java.util.*;
import java.util.stream.Collectors;

public class AdditionalData
{
    private Map<String, String> map = new HashMap<>();

    public Map<String, String> getMap() {
        return map;
    }

    public enum DataType {
        LAST_4("ccLastDigits"),
        METHOD_BRAND("ccType"),
        EXPIRE_MONTH("ccExpiryMonth"),
        EXPIRE_YEAR("ccExpiryYear"),
        EMAIL("email");

        private String detailsType;

        private static final List<DataType> dataTypeList = Collections.unmodifiableList(Arrays.asList(DataType.values()));

        DataType(String detailsType) {
            this.detailsType = detailsType;
        }

        public String getDetailsType() {
            return detailsType;
        }

        public static Optional<DataType> fromDetailsType(String detailsType) {
            return dataTypeList.stream()
                .filter(dataType->dataType.getDetailsType().equals(detailsType) )
                .findFirst();
        }

    }
}
