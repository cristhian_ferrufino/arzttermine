package com.arzttermine.service.payment.web.dto.response;

import com.arzttermine.library.web.dto.response.paging.PageResponseDto;

public class TransactionsPageDto extends PageResponseDto<TransactionDto>
{
}
