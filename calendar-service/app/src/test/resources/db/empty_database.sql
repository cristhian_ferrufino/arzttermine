DELETE FROM availabilities_appointments;

DELETE FROM availabilities_excluded_days;
DELETE FROM availabilities_treatment_types;
DELETE FROM availabilities_insurance_types;
DELETE FROM availabilities_resources;
DELETE FROM availabilities;
DELETE FROM recurring_availability_configurations;

DELETE FROM appointments_resources;
DELETE FROM appointments_languages;
DELETE FROM appointments_insurance_types;
DELETE FROM appointments_treatment_types;
DELETE FROM appointments;

DELETE FROM appointment_resources;

DELETE FROM holidays_resource_ids;
DELETE FROM holidays;
