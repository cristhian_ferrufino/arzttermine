package com.arzttermine.service.calendar.web.controller;

import com.arzttermine.service.calendar.web.BaseIntegrationTest;
import com.arzttermine.service.calendar.web.dto.request.CreateHolidaysDto;
import com.arzttermine.service.calendar.web.dto.request.HolidayTimeRangeDto;
import com.arzttermine.service.calendar.web.dto.response.HolidayDto;
import com.google.common.collect.Sets;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class HolidayControllerTest extends BaseIntegrationTest
{
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @Test
    public void createHolidays_should_report_bad_request_when_dates_are_not_in_the_future() throws Exception
    {
        OffsetDateTime todayMorning = Instant.now().atOffset(ZoneOffset.UTC).minusDays(42)
            .withHour(0).withMinute(0).withSecond(0);

        HolidayTimeRangeDto range = new HolidayTimeRangeDto();
        range.setName("my long awaited holiday");
        range.setBegin(todayMorning.toLocalDate());
        range.setEnd(todayMorning.plusDays(2).toLocalDate());
        range.setResourceIds(Sets.newHashSet(23L));

        CreateHolidaysDto request = new CreateHolidaysDto();
        request.setTimes(Collections.singletonList(range));
        request.setPracticeId(42L);

        mockMvc.perform(employeeRequest(42L, () -> post("/holidays"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$[0].reference", is("times[0].beginInFuture")));
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void createHolidays_should_create_and_get_should_return_holiday() throws Exception
    {
        OffsetDateTime todayMorning = Instant.now().atOffset(ZoneOffset.UTC).plusDays(1)
            .withHour(0).withMinute(0).withSecond(0);

        HolidayTimeRangeDto range = new HolidayTimeRangeDto();
        range.setName("my long awaited holiday");
        range.setBegin(todayMorning.toLocalDate());
        range.setEnd(todayMorning.plusDays(2).toLocalDate());
        range.setResourceIds(Sets.newHashSet(23L));

        CreateHolidaysDto request = new CreateHolidaysDto();
        request.setTimes(Collections.singletonList(range));
        request.setPracticeId(42L);

        long id = objectMapper.readValue(mockMvc.perform(employeeRequest(42L, () -> post("/holidays"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andReturn()
            .getResponse()
            .getContentAsString(), HolidayDto[].class)[0]
            .getId();

        mockMvc.perform(systemRequest(() -> get("/holidays")))
            .andExpect(jsonPath("$.page.currentPage", is(0)))
            .andExpect(jsonPath("$.page.pageSize", is(0)))
            .andExpect(jsonPath("$.elements[*]", hasSize(1)))
            .andExpect(jsonPath("$.elements[0].id", is((int) id)))
            .andExpect(jsonPath("$.elements[0].begin", is(todayMorning.format(formatter))))
            .andExpect(jsonPath("$.elements[0].end", is(range.getEnd().format(formatter))))
            .andExpect(jsonPath("$.elements[0].publicHoliday", is(false)))
            .andExpect(jsonPath("$.elements[0].resourceIds", containsInAnyOrder(23)));
    }

    @Test
    public void createHolidays_should_be_restricted() throws Exception
    {
        OffsetDateTime todayMorning = OffsetDateTime.now().plusDays(1);

        HolidayTimeRangeDto range = new HolidayTimeRangeDto();
        range.setName("my long awaited holiday");
        range.setBegin(todayMorning.toLocalDate());
        range.setEnd(todayMorning.plusDays(2).toLocalDate());
        range.setResourceIds(Sets.newHashSet(23L));

        CreateHolidaysDto request = new CreateHolidaysDto();
        request.setTimes(Collections.singletonList(range));
        request.setPracticeId(123L);

        mockMvc.perform(doctorRequest(42L, () -> post("/holidays"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isForbidden());

        mockMvc.perform(customerRequest(42L, () -> post("/holidays"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isForbidden());

        mockMvc.perform(practiceRequest(42L, 404L, () -> post("/holidays"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isForbidden());

        mockMvc.perform(practiceRequest(42L, 123L, () -> post("/holidays"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated());
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void getHolidays_should_filter_results_by_permissions_on_practice() throws Exception
    {
        OffsetDateTime todayMorning = OffsetDateTime.now().plusDays(1);

        HolidayTimeRangeDto range1 = new HolidayTimeRangeDto();
        range1.setName("my long awaited holiday");
        range1.setBegin(todayMorning.toLocalDate());
        range1.setEnd(todayMorning.plusDays(2).toLocalDate());
        range1.setResourceIds(Sets.newHashSet(23L));

        CreateHolidaysDto request = new CreateHolidaysDto();
        request.setTimes(Collections.singletonList(range1));
        request.setPracticeId(123L);

        mockMvc.perform(systemRequest(() -> post("/holidays"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated());

        request.setPracticeId(321L);

        mockMvc.perform(employeeRequest(42L, () -> post("/holidays"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated());

        mockMvc.perform(systemRequest(() -> get("/holidays")))
            .andExpect(jsonPath("$.elements[*]", hasSize(2)));

        mockMvc.perform(employeeRequest(42L, () -> get("/holidays")))
            .andExpect(jsonPath("$.elements[*]", hasSize(2)));

        mockMvc.perform(practiceRequest(42L, 123L, () -> get("/holidays")))
            .andExpect(jsonPath("$.elements[*]", hasSize(1)));

        mockMvc.perform(practiceRequest(42L, 123L, () -> get("/holidays?practiceId=321")))
            .andExpect(jsonPath("$.elements[*]", hasSize(0)));

        mockMvc.perform(doctorRequest(42L, () -> get("/holidays")))
            .andExpect(jsonPath("$.elements[*]", hasSize(0)));
    }

    @Test
    public void getHolidays_should_filter_holidays() throws Exception
    {
        OffsetDateTime todayMorning = OffsetDateTime.now().plusDays(55);

        HolidayTimeRangeDto range1 = new HolidayTimeRangeDto();
        range1.setName("my long awaited holiday");
        range1.setBegin(todayMorning.toLocalDate());
        range1.setEnd(todayMorning.plusDays(2).toLocalDate());
        range1.setResourceIds(Sets.newHashSet(23L));

        HolidayTimeRangeDto range2 = new HolidayTimeRangeDto();
        range2.setName("a public holiday");
        range2.setBegin(todayMorning.plusDays(2).toLocalDate());
        range2.setEnd(todayMorning.plusDays(4).toLocalDate());
        range2.setResourceIds(Sets.newHashSet(23L));
        range2.setPublicHoliday(true);

        CreateHolidaysDto request = new CreateHolidaysDto();
        request.setTimes(Arrays.asList(range1, range2));
        request.setPracticeId(55L);

        mockMvc.perform(systemRequest(() -> post("/holidays"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$[*]", hasSize(2)));

        request.setPracticeId(65L);

        mockMvc.perform(employeeRequest(42L, () -> post("/holidays"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$[*]", hasSize(2)));

        mockMvc.perform(systemRequest(() -> get("/holidays?publicHoliday=1&practiceId=55")))
            .andExpect(jsonPath("$.page.currentPage", is(0)))
            .andExpect(jsonPath("$.page.pageSize", is(0)))
            .andExpect(jsonPath("$.elements[*]", hasSize(1)))
            .andExpect(jsonPath("$.elements[0].begin", is(range2.getBegin().format(formatter))))
            .andExpect(jsonPath("$.elements[0].end", is(range2.getEnd().format(formatter))))
            .andExpect(jsonPath("$.elements[0].publicHoliday", is(true)))
            .andExpect(jsonPath("$.elements[0].resourceIds", containsInAnyOrder(23)));
    }
}
