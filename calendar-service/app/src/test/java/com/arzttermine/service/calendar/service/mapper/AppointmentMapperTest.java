package com.arzttermine.service.calendar.service.mapper;

import com.arzttermine.service.calendar.domain.entity.Appointment;
import com.arzttermine.service.calendar.web.dto.response.AppointmentDto;
import com.arzttermine.service.profile.web.dto.LocationDto;
import com.arzttermine.service.profile.web.dto.response.DoctorProfileDto;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.Instant;
import java.util.Collections;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class AppointmentMapperTest
{
    @InjectMocks
    private AppointmentMapper mapper;

    @Test
    public void toDto_should_calculate_duration()
    {
        Appointment appointment = new Appointment();
        appointment.setStart(Instant.now());
        appointment.setEnd(appointment.getStart().plusSeconds(15 * 60));

        AppointmentDto result = mapper.toDto(appointment, null);

        assertEquals(15, result.getDuration());
    }

    @Test
    public void toDto_should_ignore_practice_details_if_not_present()
    {
        Appointment appointment = new Appointment();
        appointment.setStart(Instant.now());
        appointment.setEnd(appointment.getStart());

        AppointmentDto result = mapper.toDto(appointment, null);

        assertNull(result.getDoctor());
        assertNull(result.getPractice());
    }

    @Test
    public void toDto_should_set_treatment_types_from_appointment_if_doctor_types_not_present()
    {
        Appointment appointment = new Appointment();
        appointment.setStart(Instant.now());
        appointment.setEnd(appointment.getStart());
        appointment.setTreatmentTypes(Collections.singletonList(11L));

        DoctorProfileDto doctor = new DoctorProfileDto();
        doctor.setId(42L);

        PracticeDto practiceDto = new PracticeDto();
        practiceDto.getDoctors().add(doctor);
        practiceDto.setLocation(new LocationDto());

        AppointmentDto result = mapper.toDto(appointment, practiceDto);

        assertNull(result.getDoctor());
        assertEquals(1, result.getAvailableTreatmentTypes().size());
        assertEquals(11L, result.getAvailableTreatmentTypes().iterator().next().longValue());
    }

    @Test
    public void toDto_should_add_practice_details_if_present()
    {
        Appointment appointment = new Appointment();
        appointment.setStart(Instant.now());
        appointment.setEnd(appointment.getStart().plusSeconds(15 * 60));
        appointment.setDoctorId(42L);

        DoctorProfileDto doctor = new DoctorProfileDto();
        doctor.setId(42L);

        PracticeDto practiceDto = new PracticeDto();
        practiceDto.getDoctors().add(doctor);
        practiceDto.setLocation(new LocationDto());

        AppointmentDto result = mapper.toDto(appointment, practiceDto);

        assertEquals(42L, result.getDoctor().getId());
        assertNotNull(result.getPractice());
    }
}
