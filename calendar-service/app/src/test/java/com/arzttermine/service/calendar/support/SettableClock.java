package com.arzttermine.service.calendar.support;

import com.arzttermine.library.suport.functional.ThrowableSupplier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;

@Component
@Profile("testing")
public final class SettableClock extends Clock
{
    private static final ThreadLocal<Clock> clocks = ThreadLocal.withInitial(Clock::systemDefaultZone);

    public ZoneId getZone() {
        return clocks.get().getZone();
    }

    public Clock withZone(ZoneId zone) {
        throw new UnsupportedOperationException();
    }

    public Instant instant() {
        return clocks.get().instant();
    }

    public static <T> T withTimeFrozen(Instant instant, ThrowableSupplier<T> action) throws Exception
    {
        final Clock old = clocks.get();
        final Clock fix = Clock.fixed(instant, old.getZone());
        clocks.set(fix);

        try {
            return action.get();
        } finally {

            clocks.set(old);
        }
    }
}
