package com.arzttermine.service.calendar.consumer;

import com.arzttermine.library.elasticsearch.service.DocumentProcessor;
import com.arzttermine.library.messaging.processor.MessageBus;
import com.arzttermine.service.booking.messages.AppointmentBookedMessage;
import com.arzttermine.service.booking.messages.BookingCancelledMessage;
import com.arzttermine.service.calendar.service.AppointmentServiceTest;
import com.arzttermine.service.calendar.service.externals.PracticeApi;
import com.arzttermine.service.calendar.web.BaseIntegrationTest;
import com.arzttermine.service.calendar.web.dto.request.CreateAppointmentsDto;
import com.arzttermine.service.calendar.web.dto.response.AppointmentDto;
import com.arzttermine.service.profile.messages.TreatmentTypeChangedMessage;
import com.arzttermine.service.profile.web.dto.LocationDto;
import com.arzttermine.service.profile.web.dto.response.DoctorProfileDto;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import com.arzttermine.service.profile.web.dto.response.ResourceDto;
import com.arzttermine.service.profile.web.dto.response.TreatmentTypeDto;
import com.arzttermine.service.profile.web.struct.Country;
import com.arzttermine.service.profile.web.struct.ResourceType;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;

import java.sql.Date;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql("/db/empty_database.sql")
public class AppointmentsSyncConsumerTest extends BaseIntegrationTest
{
    @Autowired
    private MessageBus messageBus;

    @Autowired
    private DocumentProcessor documentProcessor;

    @Autowired
    private PracticeApi practiceApi;

    @Before
    public void setup() {
        reset(practiceApi, documentProcessor);
    }

    @Test
    public void appointmentBooked_should_set_appointment_to_booked() throws Exception
    {
        reset(practiceApi);
        validPractice(42L);

        long id = objectMapper.readValue(mockMvc.perform(employeeRequest(42L, () -> post("/appointments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(AppointmentServiceTest.buildRequestDto(1))))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), AppointmentDto[].class)[0]
            .getId();

        mockMvc.perform(employeeRequest(42L, () -> get("/appointments/" + id)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.booked", is(false)));

        reset(documentProcessor);

        AppointmentBookedMessage message = new AppointmentBookedMessage();
        message.setAppointmentId(id);
        message.setCustomerId(42L);
        messageBus.send(message);

        Thread.sleep(100);

        mockMvc.perform(employeeRequest(42L, () -> get("/appointments/" + id)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.booked", is(true)));

        verify(documentProcessor, times(1)).execute(eq("appointments"), anyString(), any());
    }

    @Test
    public void appointmentCanceled_should_release_appointment() throws Exception
    {
        reset(practiceApi, documentProcessor);
        validPractice(42L);

        long id = objectMapper.readValue(mockMvc.perform(employeeRequest(42L, () -> post("/appointments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(AppointmentServiceTest.buildRequestDto(1))))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), AppointmentDto[].class)[0]
            .getId();

        AppointmentBookedMessage message = new AppointmentBookedMessage();
        message.setAppointmentId(id);
        message.setCustomerId(42L);
        messageBus.send(message);

        Thread.sleep(80);

        BookingCancelledMessage message2 = new BookingCancelledMessage();
        message2.setAppointmentAvailable(true);
        message2.setAppointmentId(id);
        message2.setCustomerId(42L);
        message2.setBookingId(42L);
        messageBus.send(message2);

        Thread.sleep(80);

        mockMvc.perform(employeeRequest(42L, () -> get("/appointments/" + id)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.booked", is(false)));

        verify(documentProcessor, times(2)).execute(eq("appointments"), anyString(), any());
        verify(documentProcessor, times(1)).executeAsync(eq("appointments"), eq("practice-42"), any());
    }

    @Test
    public void appointmentCancelled_should_remove_released_appointment_if_no_longer_needed() throws Exception
    {
        reset(practiceApi, documentProcessor);
        validPractice(42L);

        long id = objectMapper.readValue(mockMvc.perform(employeeRequest(42L, () -> post("/appointments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(AppointmentServiceTest.buildRequestDto(1))))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), AppointmentDto[].class)[0]
            .getId();

        AppointmentBookedMessage message = new AppointmentBookedMessage();
        message.setAppointmentId(id);
        message.setCustomerId(42L);
        messageBus.send(message);

        Thread.sleep(80);
        reset(documentProcessor);

        BookingCancelledMessage message2 = new BookingCancelledMessage();
        message2.setAppointmentAvailable(false);
        message2.setAppointmentId(id);
        message2.setCustomerId(42L);
        message2.setBookingId(42L);
        messageBus.send(message2);

        Thread.sleep(100);

        mockMvc.perform(employeeRequest(42L, () -> get("/appointments/" + id)))
            .andExpect(status().isNotFound());

        verify(documentProcessor, times(1)).executeAsync(eq("appointments"), eq("practice-42"), any());
    }

    @Test
    public void treatmentTypeChanged_should_remove_treatment_offer_and_update_affected_appointment_documents() throws Exception
    {
        reset(practiceApi);
        validPractice(42L);

        // 5555 should be omitted as it doesnt belong to the practice
        CreateAppointmentsDto request = AppointmentServiceTest.buildRequestDto(1, 99L, 199L, 5555L);

        long id = objectMapper.readValue(mockMvc.perform(employeeRequest(42L, () -> post("/appointments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), AppointmentDto[].class)[0]
            .getId();

        mockMvc.perform(employeeRequest(42L, () -> get("/appointments/" + id)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.availableTreatmentTypes[*]", containsInAnyOrder(99, 199)));

        validPractice(666L);
        Instant next = Instant.now().plusSeconds(999999999);

        request.setPracticeId(666L);
        request.setStart(Date.from(next));
        request.setEnd(Date.from(next.plusSeconds(request.getDuration() * 60)));

        long id2 = objectMapper.readValue(mockMvc.perform(employeeRequest(42L, () -> post("/appointments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), AppointmentDto[].class)[0]
            .getId();

        TreatmentTypeChangedMessage message = new TreatmentTypeChangedMessage();
        message.setTreatmentTypeId(99);
        message.setDeleted(true);
        messageBus.send(message);

        Thread.sleep(70);

        mockMvc.perform(employeeRequest(42L, () -> get("/appointments/" + id)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.availableTreatmentTypes[*]", hasSize(1)))
            .andExpect(jsonPath("$.availableTreatmentTypes[0]", is(199)));

        mockMvc.perform(employeeRequest(42L, () -> get("/appointments/" + id2)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.availableTreatmentTypes[*]", hasSize(1)))
            .andExpect(jsonPath("$.availableTreatmentTypes[0]", is(199)));

        // index + update
        verify(documentProcessor, times(2)).executeAsync(eq("appointments"), eq("practice-42"), any());
        verify(documentProcessor, times(2)).executeAsync(eq("appointments"), eq("practice-666"), any());
    }

    private void validPractice(long practiceId)
    {
        TreatmentTypeDto tt1 = new TreatmentTypeDto();
        tt1.setDoctorIds(Collections.singletonList(42L));
        tt1.setType("besuch");
        tt1.setId(99L);

        TreatmentTypeDto tt2 = new TreatmentTypeDto();
        tt2.setDoctorIds(Collections.singletonList(42L));
        tt2.setType("treat me");
        tt2.setId(199L);

        DoctorProfileDto profileDto = new DoctorProfileDto();
        profileDto.setId(42L);

        LocationDto location = new LocationDto();
        location.setCountry(Country.GERMANY);
        location.setLocation("buxtehude 123");

        ResourceDto r1 = new ResourceDto();
        r1.setType(ResourceType.DOCTOR);
        r1.setName("the doctor");
        r1.setSystemId(42L);
        r1.setId(345L);

        PracticeDto practice = new PracticeDto();
        practice.setDoctors(Collections.singletonList(profileDto));
        practice.setResources(Collections.singletonList(r1));
        practice.setTreatmentTypes(Arrays.asList(tt1, tt2));
        practice.setLocation(location);
        practice.setId(practiceId);

        Map<Long, PracticeDto> practices = new HashMap<>();
        practices.put(practiceId, practice);

        when(practiceApi.findById(any(), eq(practiceId))).thenReturn(practice);
        when(practiceApi.findByIds(any(), any())).thenReturn(practices);
    }
}
