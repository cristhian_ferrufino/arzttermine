package com.arzttermine.service.calendar.service;

import com.arzttermine.library.suport.exception.EntityNotFoundException;
import com.arzttermine.library.suport.exception.ResponseErrorListException;
import com.arzttermine.library.web.dto.response.ErrorDto;
import com.arzttermine.service.calendar.domain.entity.Appointment;
import com.arzttermine.service.calendar.domain.entity.AppointmentResource;
import com.arzttermine.service.calendar.domain.entity.Availability;
import com.arzttermine.service.calendar.domain.entity.RecurringConfiguration;
import com.arzttermine.service.calendar.domain.repository.AvailabilityRepository;
import com.arzttermine.service.calendar.domain.repository.RecurringConfigurationRepository;
import com.arzttermine.service.calendar.service.event.model.AvailabilityChangedEvent;
import com.arzttermine.service.calendar.service.externals.PracticeApi;
import com.arzttermine.service.calendar.service.mapper.AvailabilityMapper;
import com.arzttermine.service.calendar.service.processor.AvailabilityProcessor;
import com.arzttermine.service.calendar.web.dto.RecurringConfigDto;
import com.arzttermine.service.calendar.web.dto.request.CreateAvailabilityDto;
import com.arzttermine.service.calendar.web.dto.request.DeleteAvailabilityDto;
import com.arzttermine.service.calendar.web.dto.request.ResourceRequestDto;
import com.arzttermine.service.calendar.web.dto.response.AvailabilityTimeFrameDto;
import com.arzttermine.service.calendar.web.struct.RecurringType;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import com.arzttermine.service.profile.web.dto.response.ResourceDto;
import com.arzttermine.service.profile.web.dto.response.TreatmentTypeDto;
import com.arzttermine.service.profile.web.struct.ResourceType;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AvailabilityServiceTest
{
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @Spy
    private AvailabilityMapper mapper = new AvailabilityMapper();

    @Mock
    private AvailabilityRepository repository;

    @Mock
    private RecurringConfigurationRepository configurationRepository;

    @Mock
    private ApplicationEventPublisher eventPublisher;

    @Mock
    private PracticeApi practiceClient;

    @Mock
    private AvailabilityProcessor processor;

    @InjectMocks
    private AvailabilityService service;

    private PracticeDto practice;

    @Before
    public void setup() {
        OAuth2Authentication auth = mock(OAuth2Authentication.class);
        when(auth.getAuthorities()).thenReturn(Collections.singletonList(new SimpleGrantedAuthority("ROLE_SYSTEM")));
        SecurityContextHolder.getContext().setAuthentication(auth);

        ResourceDto doctor = new ResourceDto();
        doctor.setType(ResourceType.DOCTOR);
        doctor.setSystemId(23L);
        doctor.setId(12345L);

        TreatmentTypeDto tt = new TreatmentTypeDto();
        tt.setDoctorIds(Collections.singletonList(12345L));
        tt.setType("example");
        tt.setId(333L);

        practice = new PracticeDto();
        practice.setId(42L);
        practice.setName("my practice");
        practice.setResources(new ArrayList<>(Collections.singletonList(doctor)));
        practice.getTreatmentTypes().add(tt);

        Map<Long, PracticeDto> practiceMap = new HashMap<>();
        practiceMap.put(42L, practice);

        reset(practiceClient);
        when(practiceClient.findById(anyLong())).thenReturn(practice);
        when(practiceClient.findByIds(any())).thenReturn(practiceMap);
    }

    @Test(expected = EntityNotFoundException.class)
    public void createAvailabilities_should_report_missing_practice()
    {
        CreateAvailabilityDto request = buildRequestDto(Instant.now());
        request.setPracticeId(404L);

        try {
            service.createAvailabilities(Collections.singletonList(request), null);
        } catch (EntityNotFoundException e) {
            assertEquals(404L, e.getErrorDto().getReference());
            throw e;
        }
    }

    @Test(expected = EntityNotFoundException.class)
    public void createAvailabilities_should_report_missing_resource()
    {
        CreateAvailabilityDto request = buildRequestDto(Instant.now());
        request.getResourceRequests().add(new ResourceRequestDto(404L));

        try {
            service.createAvailabilities(Collections.singletonList(request), null);
        } catch (EntityNotFoundException e) {
            assertEquals(404L, e.getErrorDto().getReference());
            throw e;
        }
    }

    @Test
    public void createAvailabilities_should_add_appointment_resources_and_dispatch_event()
    {
        CreateAvailabilityDto request = prepareInPractice(Instant.now());

        when(repository.save(any(Availability.class))).thenAnswer(invocation -> {
            Availability entity = invocation.getArgumentAt(0, Availability.class);
            assertFalse(entity.getResources().isEmpty());
            assertTrue(entity.getAppointments().isEmpty());
            return entity;
        });

        List<AvailabilityTimeFrameDto> frames = service.createAvailabilities(Collections.singletonList(request), null);

        assertFalse(frames.isEmpty());
        verify(repository, times(1)).save(any(Availability.class));
        verify(eventPublisher, times(1)).publishEvent(any(AvailabilityChangedEvent.class));
    }

    @Test(expected = ResponseErrorListException.class)
    public void deleteAvailability_should_report_booked_appointments()
    {
        Appointment a1 = new Appointment();
        a1.setStart(Instant.now().plusSeconds(999999));
        a1.setEnd(a1.getStart().plusSeconds(1800));
        Appointment a2 = new Appointment(a1);
        a2.setStart(Instant.now().plusSeconds(999999));
        a2.setEnd(a1.getStart().plusSeconds(1800));
        a2.setBookedByCustomer(42L);
        Appointment a3 = new Appointment(a2);
        a3.setStart(Instant.now().plusSeconds(999999));
        a3.setEnd(a1.getStart().plusSeconds(1800));

        Availability availability = new Availability();
        availability.getAppointments().addAll(Arrays.asList(a1, a2, a3));

        when(repository.findOne(42L)).thenReturn(availability);

        try {
            service.deleteAvailability(42L, null);
        } catch (ResponseErrorListException e) {
            Iterator<ErrorDto> errors = e.getErrors().iterator();
            assertEquals(errors.next().getCode(), "REQUIRES_CANCELLATION");
            assertEquals(errors.next().getCode(), "REQUIRES_CANCELLATION");
            assertFalse(errors.hasNext());
            throw e;
        }
    }

    @Test
    public void deleteAvailability_should_exclude_single_days_from_recurring_availabilities()
    {
        Availability availability = new Availability();
        availability.setRecurringConfig(new RecurringConfiguration(RecurringType.WEEKLY));
        availability.setBegin(Instant.now());
        availability.setEnd(availability.getBegin().plusSeconds(1800));
        availability.setDuration(30);

        when(repository.findOne(42L)).thenReturn(availability);

        OffsetDateTime exclusion = availability.getBegin().atOffset(ZoneOffset.UTC).plusWeeks(1).withHour(0).withMinute(0).withSecond(0).withNano(0);

        DeleteAvailabilityDto request = new DeleteAvailabilityDto();
        request.setDate(exclusion.toLocalDate());
        request.setRemoveAll(false);

        service.deleteAvailability(42L, request);

        verify(repository, never()).delete(any(Availability.class));
        assertTrue(availability.isActive());
        assertEquals(1, availability.getExcludedDays().size());
        assertEquals(exclusion.toInstant(), availability.getExcludedDays().get(0));
    }

    @Test
    public void deleteAvailability_should_not_report_not_affected_appointments()
    {
        Availability availability = new Availability();
        availability.setRecurringConfig(new RecurringConfiguration(RecurringType.WEEKLY));
        availability.setBegin(Instant.now());
        availability.setEnd(availability.getBegin().plusSeconds(1800));
        availability.setDuration(30);

        Appointment a1 = new Appointment();
        a1.setStart(availability.getBegin());
        a1.setEnd(availability.getEnd());
        a1.setBookedByCustomer(42L);
        availability.getAppointments().add(a1);

        when(repository.findOne(42L)).thenReturn(availability);

        OffsetDateTime exclusion = availability.getBegin().atOffset(ZoneOffset.UTC).plusWeeks(1)
            .withHour(0).withMinute(0).withSecond(0).withNano(0);

        DeleteAvailabilityDto request = new DeleteAvailabilityDto();
        request.setDate(exclusion.toLocalDate());
        request.setRemoveAll(false);

        service.deleteAvailability(42L, request);

        verify(repository, never()).delete(any(Availability.class));
        assertTrue(availability.isActive());
        assertEquals(1, availability.getAppointments().size());
        assertEquals(1, availability.getExcludedDays().size());
        assertEquals(exclusion.toInstant(), availability.getExcludedDays().get(0));
    }

    @Test
    public void deleteAvailability_should_ignore_non_existent()
    {
        when(repository.findOne(42L)).thenReturn(null);
        service.deleteAvailability(42L, null);
    }

    @Test
    public void getAvailabilities_should_group_time_frames()
    {
        Availability a1 = new Availability();
        a1.getResources().add(new AppointmentResource(42L, Instant.now(), 30));
        a1.getResources().add(new AppointmentResource(43L, Instant.now(), 30));
        a1.setBegin(Instant.now());
        a1.setEnd(Instant.now().plusSeconds(180));

        Availability a2 = new Availability();
        a2.getResources().add(new AppointmentResource(42L, Instant.now(), 30));
        a2.setBegin(Instant.now());
        a2.setEnd(Instant.now().plusSeconds(180));

        List<Availability> availabilities = new ArrayList<>(2);
        availabilities.addAll(Arrays.asList(a1, a2));

        Pageable pageable = new PageRequest(0, 25);
        Page<Availability> page = new PageImpl<>(availabilities, pageable, 2);

        when(repository.findAll(any(Predicate.class), any(Pageable.class))).thenReturn(page);

        Map<Long, List<AvailabilityTimeFrameDto>> frames = service.getAvailabilities(new BooleanBuilder().getValue(), pageable);

        assertEquals(frames.size(), 2);
        assertEquals(frames.get(42L).size(), 2);
        assertEquals(frames.get(43L).size(), 1);
    }

    @Test(expected = EntityNotFoundException.class)
    public void updateAvailability_should_report_missing_availability()
    {
        CreateAvailabilityDto update = buildRequestDto(Instant.now().plus(1, ChronoUnit.DAYS));
        service.updateAvailability(42L, update, null, null);
    }

    @Test
    public void updateAvailability_should_do_update()
    {
        Availability a1 = spy(new Availability());
        a1.getResources().add(new AppointmentResource(42L, Instant.now(), 30));
        a1.setBegin(Instant.now());
        a1.setEnd(a1.getBegin().plusSeconds(180));
        a1.setDuration(30);
        a1.setPracticeId(42L);
        when(a1.getId()).thenReturn(42L);

        CreateAvailabilityDto update = buildRequestDto(Instant.now().plus(1, ChronoUnit.DAYS));

        when(repository.findOne(42L)).thenReturn(a1);

        AvailabilityTimeFrameDto frame = service.updateAvailability(42L, update, null, null);

        assertEquals(frame.getId(), 42L);
        assertEquals(frame.getBegin(), update.getFrom());
        assertEquals(frame.getEnd(), update.getTo());
        assertEquals(frame.getDuration(), 30);
        assertNull(frame.getRecurringConfig());
    }

    @Test
    public void updateAvailability_should_merge_overlapping_availabilities()
    {
        Availability a1 = spy(new Availability());
        a1.setBegin(Instant.now());
        a1.setEnd(a1.getBegin().plusSeconds(180));
        a1.setDuration(30);
        a1.setPracticeId(42L);
        when(a1.getId()).thenReturn(95L);

        Instant next = Instant.now().plus(1, ChronoUnit.DAYS);
        Availability overlapping = new Availability();
        overlapping.setBegin(next);
        overlapping.setPracticeId(42L);
        overlapping.setEnd(next.plus(30, ChronoUnit.MINUTES));

        CreateAvailabilityDto update = buildRequestDto(next);

        when(repository.findOne(95L)).thenReturn(a1);

        AvailabilityTimeFrameDto frame = service.updateAvailability(95L, update, null, null);

        assertEquals(frame.getId(), 95L);
        assertEquals(frame.getBegin(), update.getFrom());
        assertEquals(frame.getEnd(), Date.from(overlapping.getEnd()));
        assertEquals(frame.getDuration(), 30);
        assertEquals(frame.getResourceRequests().size(), 1);

        verify(processor, times(1)).mergeOverlappingInto(a1);
    }

    @Test
    public void updateAvailability_should_merge_resources()
    {
        ResourceDto r1 = new ResourceDto();
        r1.setId(42L);
        ResourceDto r2 = new ResourceDto();
        r2.setId(55L);
        ResourceDto r3 = new ResourceDto();
        r3.setId(66L);

        practice.getResources().addAll(Arrays.asList(r1, r2, r3));

        Availability a1 = spy(new Availability());
        a1.getResources().add(new AppointmentResource(42L, Instant.now(), 30));
        a1.getResources().add(new AppointmentResource(55L, Instant.now(), 30));
        a1.setBegin(Instant.now());
        a1.setEnd(a1.getBegin().plusSeconds(180));
        a1.setDuration(30);
        a1.setPracticeId(42L);
        when(a1.getId()).thenReturn(42L);

        Instant next = Instant.now().plus(1, ChronoUnit.DAYS);
        Availability overlapping = new Availability();
        overlapping.setBegin(next);
        overlapping.setPracticeId(42L);
        overlapping.setEnd(next.plus(30, ChronoUnit.MINUTES));
        overlapping.getResources().add(new AppointmentResource(42L, overlapping.getBegin(), overlapping.getEnd()));

        CreateAvailabilityDto update = buildRequestDto(next);
        update.getResourceRequests().add(new ResourceRequestDto(42L));
        update.getResourceRequests().add(new ResourceRequestDto(66L));

        when(repository.findOne(42L)).thenReturn(a1);

        AvailabilityTimeFrameDto frame = service.updateAvailability(42L, update, null, null);
        Iterator<ResourceRequestDto> resourceRequests = frame.getResourceRequests().iterator();

        assertEquals(resourceRequests.next().getResourceId().intValue(), 42);
        assertEquals(resourceRequests.next().getResourceId().intValue(), 12345);
        assertEquals(resourceRequests.next().getResourceId().intValue(), 66);
        assertFalse(resourceRequests.hasNext());
    }

    @Test
    public void updateAvailability_should_change_recurring_configuration()
    {
        Availability a1 = spy(new Availability());
        a1.getResources().add(new AppointmentResource(42L, Instant.now(), 30));
        a1.setRecurringConfig(new RecurringConfiguration(RecurringType.WEEKLY));
        a1.setBegin(Instant.now());
        a1.setEnd(a1.getBegin().plusSeconds(180));
        a1.setDuration(30);
        a1.setPracticeId(42L);
        when(a1.getId()).thenReturn(42L);
        when(repository.findOne(42L)).thenReturn(a1);

        Instant next = Instant.now().plus(1, ChronoUnit.DAYS);
        CreateAvailabilityDto update = buildRequestDto(next);

        Instant disabledAt = next.plus(35, ChronoUnit.DAYS).atOffset(ZoneOffset.UTC).withSecond(0).withNano(0).toInstant();
        service.updateAvailability(42L, update, null, disabledAt.atOffset(ZoneOffset.UTC));

        assertEquals(disabledAt, a1.getBegin());
        assertNull(a1.getRecurringConfig());
        assertFalse(a1.isRecurring());
    }

    @Test
    public void updateAvailability_should_create_new_recurring_configuration_and_ignore_disabled_at()
    {
        Instant now = Instant.now().atOffset(ZoneOffset.UTC).withSecond(0).withNano(0).toInstant();

        Availability a1 = spy(new Availability());
        a1.getResources().add(new AppointmentResource(42L, Instant.now(), 30));
        a1.setBegin(now);
        a1.setEnd(now.plusSeconds(180));
        a1.setDuration(30);
        a1.setPracticeId(42L);
        when(a1.getId()).thenReturn(42L);
        when(repository.findOne(42L)).thenReturn(a1);

        assertFalse(a1.isRecurring());

        CreateAvailabilityDto update = buildRequestDto(now);

        RecurringConfigDto configDto = new RecurringConfigDto();
        configDto.setType(RecurringType.WEEKLY);

        when(configurationRepository.save(any(RecurringConfiguration.class)))
            .thenReturn(new RecurringConfiguration(RecurringType.WEEKLY));

        Instant disabledAt = now.plus(35, ChronoUnit.DAYS);

        service.updateAvailability(42L, update, configDto, disabledAt.atOffset(ZoneOffset.UTC));

        assertEquals(now, a1.getBegin());
        assertNotNull(a1.getRecurringConfig());
        assertTrue(a1.isRecurring());
        assertEquals(RecurringType.WEEKLY, a1.getRecurringConfig().getType());
        assertTrue(a1.getRecurringConfig().is(RecurringType.WEEKLY));

        // should be resetted
        assertNull(a1.getLastIteration());

        verify(configurationRepository, times(1)).save(any(RecurringConfiguration.class));
    }

    @Test
    public void createAvailabilities_should_set_recurring_configuration_if_set()
    {
        CreateAvailabilityDto request = prepareInPractice(Instant.now());

        RecurringConfigDto config = new RecurringConfigDto();
        config.setType(RecurringType.WEEKLY);
        config.setDayOfWeek(DayOfWeek.MONDAY);
        config.setEndsAt(Instant.now().atOffset(ZoneOffset.UTC).plusHours(360).toLocalDate());

        when(configurationRepository.save(any(RecurringConfiguration.class))).thenAnswer(invocation -> {
            RecurringConfiguration entity = invocation.getArgumentAt(0, RecurringConfiguration.class);
            assertEquals(RecurringType.WEEKLY, entity.getType());
            assertEquals(config.getEndsAt(), entity.getEndsAt().atOffset(ZoneOffset.UTC).toLocalDate());
            return entity;
        });

        when(repository.save(any(Availability.class))).thenAnswer(invocation -> {
            Availability entity = invocation.getArgumentAt(0, Availability.class);
            assertNotNull(entity.getRecurringConfig());
            return entity;
        });

        service.createAvailabilities(Collections.singletonList(request), config);

        verify(configurationRepository, times(1)).save(any(RecurringConfiguration.class));
        verify(repository, times(1)).save(any(Availability.class));
    }

    @Test
    public void createAvailabilities_should_create_recurring_config_for_each_availability()
    {
        CreateAvailabilityDto request = prepareInPractice(Instant.now().plus(5, ChronoUnit.HOURS));
        CreateAvailabilityDto lowest = prepareInPractice(Instant.now());
        Iterator<Instant> recurringStarts = Arrays.asList(request.getFrom().toInstant(), lowest.getFrom().toInstant()).iterator();

        RecurringConfigDto config = new RecurringConfigDto();
        config.setType(RecurringType.WEEKLY);
        config.setDayOfWeek(DayOfWeek.MONDAY);

        when(configurationRepository.save(any(RecurringConfiguration.class))).thenAnswer(invocation -> {
            RecurringConfiguration entity = invocation.getArgumentAt(0, RecurringConfiguration.class);
            assertEquals(recurringStarts.next(), entity.getStartsAt());
            return entity;
        });

        service.createAvailabilities(Arrays.asList(request, lowest), config);

        verify(configurationRepository, times(2)).save(any(RecurringConfiguration.class));
    }

    public static CreateAvailabilityDto buildRequestDto(Instant from)
    {
        return buildRequestDto(from, 1);
    }

    public static CreateAvailabilityDto buildRequestDto(Instant from, int count)
    {
        CreateAvailabilityDto request = new CreateAvailabilityDto();
        request.setTo(Date.from(from.plus(count * 30, ChronoUnit.MINUTES)));
        request.setFrom(Date.from(from));
        request.setPracticeId(42L);
        request.setDuration(30);
        request.getResourceRequests().add(new ResourceRequestDto(12345L)); // doctor
        request.getTreatmentTypes().add(333L);
        return request;
    }

    private CreateAvailabilityDto prepareInPractice(Instant from)
    {
        CreateAvailabilityDto dto = buildRequestDto(from);

        ResourceDto resource = new ResourceDto();
        resource.setSystemId(12345L);
        resource.setType(ResourceType.DOCTOR);
        resource.setId(12345L);

        TreatmentTypeDto tt = new TreatmentTypeDto();
        tt.setDoctorIds(Collections.singletonList(12345L));
        tt.setType("example");
        tt.setId(333L);

        PracticeDto practice = new PracticeDto();
        practice.setResources(Collections.singletonList(resource));
        practice.setTreatmentTypes(Collections.singletonList(tt));

        Map<Long, PracticeDto> practiceMap = new HashMap<>();
        practiceMap.put(dto.getPracticeId(), practice);
        when(practiceClient.findByIds(any())).thenReturn(practiceMap);

        return dto;
    }
}
