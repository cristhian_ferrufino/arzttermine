package com.arzttermine.service.calendar.service.processor.transformer;

import com.arzttermine.service.calendar.domain.entity.Appointment;
import com.arzttermine.service.calendar.domain.entity.Availability;
import com.arzttermine.service.calendar.domain.entity.RecurringConfiguration;
import com.arzttermine.service.calendar.domain.repository.AvailabilityRepository;
import com.arzttermine.service.calendar.web.struct.RecurringType;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.Duration;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.Iterator;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AvailabilityIteratorTest
{
    private static final PracticeDto emptyPractice = new PracticeDto();

    @Mock
    private AvailabilityRepository availabilityRepository;

    @Mock
    private AppointmentChangeCrudTranslator crudTranslator;

    @Mock
    private AvailabilityToAppointmentTransformer toAppointmentTransformer;

    @InjectMocks
    private AvailabilityIterator iterator;

    @Test
    public void createOrUpdateRanges_should_not_create_range_for_non_recurring_availabilities()
    {
        OffsetDateTime now = Instant.now().atOffset(ZoneOffset.UTC).withSecond(0).withNano(0);

        Availability availability = spy(new Availability());
        when(availability.getId()).thenReturn(42L);
        availability.setBegin(now.toInstant());
        availability.setEnd(now.plusMinutes(30).toInstant());

        when(availabilityRepository.findOne(42L)).thenReturn(availability);

        iterator.createOrUpdateRanges(42L, emptyPractice);

        verify(toAppointmentTransformer, times(1)).transform(availability, emptyPractice, now);
    }

    @Test
    public void createOrUpdateRanges_should_set_last_iteration_and_apply_changes()
    {
        OffsetDateTime now = Instant.now().atOffset(ZoneOffset.UTC).withSecond(0).withNano(0);

        Availability availability = spy(new Availability());
        when(availability.getId()).thenReturn(42L);
        availability.setBegin(now.toInstant());
        availability.setEnd(now.plusMinutes(30).toInstant());
        assertNull(availability.getLastIteration());

        when(availabilityRepository.findOne(42L)).thenReturn(availability);

        iterator.createOrUpdateRanges(42L, emptyPractice);

        assertTrue(availability.getAppointments().isEmpty());
        assertEquals(now.plusDays(1).toInstant(), availability.getLastIteration());

        verify(crudTranslator, times(1)).applyChanges(eq(availability), any(), eq(emptyPractice));
    }

    @Test
    public void createOrUpdateRanges_should_create_range_for_recurring_availabilities()
    {
        OffsetDateTime now = Instant.now().atOffset(ZoneOffset.UTC).withSecond(0).withNano(0);

        Availability availability = spy(new Availability());
        when(availability.getId()).thenReturn(42L);
        availability.setBegin(now.toInstant());
        availability.setEnd(now.plusMinutes(30).toInstant());
        availability.setRecurringConfig(new RecurringConfiguration(RecurringType.WEEKLY));

        when(availabilityRepository.findOne(42L)).thenReturn(availability);

        iterator.createOrUpdateRanges(42L, emptyPractice);

        verify(toAppointmentTransformer, times(1)).transform(availability, emptyPractice, now);
        verify(toAppointmentTransformer, times(1)).transform(availability, emptyPractice, now.plusWeeks(1));
        verify(toAppointmentTransformer, times(1)).transform(availability, emptyPractice, now.plusWeeks(2));
        verify(toAppointmentTransformer, times(1)).transform(availability, emptyPractice, now.plusWeeks(3));
        verify(toAppointmentTransformer, times(1)).transform(availability, emptyPractice, now.plusWeeks(4));
        verify(toAppointmentTransformer, times(1)).transform(availability, emptyPractice, now.plusWeeks(5));
        verify(toAppointmentTransformer, times(1)).transform(availability, emptyPractice, now.plusWeeks(6));
        verify(toAppointmentTransformer, times(1)).transform(availability, emptyPractice, now.plusWeeks(7));
        verify(toAppointmentTransformer, times(1)).transform(availability, emptyPractice, now.plusWeeks(8));
        verify(toAppointmentTransformer, times(1)).transform(availability, emptyPractice, now.plusWeeks(9));
        verify(toAppointmentTransformer, times(1)).transform(availability, emptyPractice, now.plusWeeks(10));
        verify(toAppointmentTransformer, times(1)).transform(availability, emptyPractice, now.plusWeeks(11));
        verify(toAppointmentTransformer, times(1)).transform(availability, emptyPractice, now.plusWeeks(12));
        verify(toAppointmentTransformer, times((int) Duration.between(now, now.plusMonths(3)).toDays() +1)).transform(any(), any(), any());
    }

    @Test
    public void createOrUpdateRanges_should_remove_appointments_outside_of_availability_range()
    {
        OffsetDateTime now = Instant.now().atOffset(ZoneOffset.UTC).plusDays(5).withSecond(0).withNano(0);

        Appointment appointment1 = new Appointment();
        appointment1.setStart(now.minusMinutes(40).toInstant());
        appointment1.setEnd(now.plusMinutes(70).toInstant());

        Appointment appointment2 = new Appointment();
        appointment2.setStart(now.minusMinutes(100).toInstant());
        appointment2.setEnd(appointment2.getStart().plus(30, ChronoUnit.MINUTES));

        Appointment appointment3 = new Appointment();
        appointment3.setStart(now.plusMinutes(200).toInstant());
        appointment3.setEnd(appointment3.getStart().plus(30, ChronoUnit.MINUTES));
        appointment3.setBookedByCustomer(42L);

        Availability availability = spy(new Availability());
        when(availability.getId()).thenReturn(42L);
        availability.setBegin(now.toInstant());
        availability.setEnd(now.plusMinutes(30).toInstant());
        availability.getAppointments().add(appointment1);
        availability.getAppointments().add(appointment2);
        availability.getAppointments().add(appointment3); // should not be changed

        when(availabilityRepository.findOne(42L)).thenReturn(availability);

        doAnswer(invocation -> {

            Iterator<AvailabilityToAppointmentTransformer.AppointmentChange> changes = invocation.getArgumentAt(1, Collection.class).iterator();
            assertTrue(changes.next().isDeleted());
            assertTrue(changes.next().isDeleted());
            assertFalse(changes.hasNext());

            return invocation;
        }).when(crudTranslator).applyChanges(eq(availability), any(), eq(emptyPractice));

        iterator.createOrUpdateRanges(42L, emptyPractice);

        verify(crudTranslator, times(1)).applyChanges(eq(availability), any(), eq(emptyPractice));
        verify(toAppointmentTransformer, times(6)).transform(any(), any(), any());
    }

    @Test
    public void createOrUpdateDayRange_should_detect_and_apply_changes_on_a_certain_day()
    {
        OffsetDateTime now = Instant.now().atOffset(ZoneOffset.UTC).withSecond(0).withNano(0);

        Availability availability = spy(new Availability());
        when(availability.getId()).thenReturn(42L);
        availability.setBegin(now.toInstant());
        availability.setEnd(now.plusMinutes(30).toInstant());

        when(availabilityRepository.findOne(42L)).thenReturn(availability);

        iterator.createOrUpdateDayRange(42L, emptyPractice, now.toInstant());

        assertNull(availability.getLastIteration());
        verify(crudTranslator, times(1)).applyChanges(eq(availability), any(), eq(emptyPractice));
        verify(toAppointmentTransformer, times(1)).transform(availability, emptyPractice, now);
        verify(toAppointmentTransformer, times(1)).transform(any(), any(), any());
    }

    @Test
    public void createOrUpdateDayRange_should_remove_appointments_outside_of_availability_range()
    {
        OffsetDateTime now = Instant.now().atOffset(ZoneOffset.UTC).withSecond(0).withNano(0);

        Appointment appointment1 = new Appointment();
        appointment1.setStart(now.minusMinutes(40).toInstant());
        appointment1.setEnd(appointment1.getStart().plus(30, ChronoUnit.MINUTES));

        Appointment appointment2 = new Appointment();
        appointment2.setStart(now.plusMinutes(100).toInstant());
        appointment2.setEnd(appointment2.getStart().plus(30, ChronoUnit.MINUTES));

        Availability availability = spy(new Availability());
        when(availability.getId()).thenReturn(42L);
        availability.setBegin(now.toInstant());
        availability.setEnd(now.plusMinutes(30).toInstant());
        availability.getAppointments().add(appointment1);
        availability.getAppointments().add(appointment2);

        when(availabilityRepository.findOne(42L)).thenReturn(availability);

        doAnswer(invocation -> {

            Iterator<AvailabilityToAppointmentTransformer.AppointmentChange> changes = invocation.getArgumentAt(1, Collection.class).iterator();
            assertTrue(changes.hasNext());
            assertTrue(changes.next().isDeleted());
            assertTrue(changes.next().isDeleted());
            assertFalse(changes.hasNext());

            return invocation;
        }).when(crudTranslator).applyChanges(eq(availability), any(), eq(emptyPractice));

        iterator.createOrUpdateDayRange(42L, emptyPractice, now.toInstant());

        assertNull(availability.getLastIteration());
        verify(crudTranslator, times(1)).applyChanges(eq(availability), any(), eq(emptyPractice));
        verify(toAppointmentTransformer, times(1)).transform(any(), any(), any());
    }

}
