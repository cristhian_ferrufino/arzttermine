package com.arzttermine.service.calendar.service.processor.transformer;

import com.arzttermine.library.suport.exception.ResponseErrorListException;
import com.arzttermine.library.web.dto.response.ErrorDto;
import com.arzttermine.library.web.struct.InsuranceType;
import com.arzttermine.service.calendar.domain.entity.Appointment;
import com.arzttermine.service.calendar.domain.entity.AppointmentResource;
import com.arzttermine.service.calendar.domain.entity.Availability;
import com.arzttermine.service.calendar.domain.entity.RecurringConfiguration;
import com.arzttermine.service.calendar.domain.repository.AvailabilityRepository;
import com.arzttermine.service.calendar.domain.repository.HolidayRepository;
import com.arzttermine.service.calendar.service.mapper.AvailabilityMapper;
import com.arzttermine.service.calendar.service.processor.ResourceAvailabilityProcessor;
import com.arzttermine.service.calendar.service.processor.transformer.exclusion.AvailabilityConfigurationStrategy;
import com.arzttermine.service.calendar.service.processor.transformer.exclusion.ExcludedAvailabilityStrategy;
import com.arzttermine.service.calendar.service.processor.transformer.exclusion.ExclusionStrategy;
import com.arzttermine.service.calendar.service.processor.transformer.exclusion.OpeningHoursStrategy;
import com.arzttermine.service.calendar.service.processor.transformer.exclusion.PracticeHolidayStrategy;
import com.arzttermine.service.calendar.web.dto.request.ResourceRequestDto;
import com.arzttermine.service.calendar.web.struct.RecurringType;
import com.arzttermine.service.profile.web.dto.OpeningHoursDto;
import com.arzttermine.service.profile.web.dto.response.DoctorProfileDto;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import com.arzttermine.service.profile.web.dto.response.ResourceDto;
import com.arzttermine.service.profile.web.dto.response.TreatmentTypeDto;
import com.arzttermine.service.profile.web.struct.ResourceType;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.annotation.Bean;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AvailabilityToAppointmentTransformerTest
{
    private static final OffsetDateTime TODAY_AT_MIDNIGHT = Instant.now().atOffset(ZoneOffset.UTC)
        .withHour(0).withMinute(0).withSecond(0).withNano(0);

    @Mock
    private HolidayRepository holidayRepository;

    @Mock
    private AvailabilityRepository availabilityRepository;

    @Mock
    private ResourceAvailabilityProcessor resourceProcessor;

    @Spy
    private AvailabilityMapper availabilityMapper = new AvailabilityMapper();

    private AvailabilityToAppointmentTransformer toAppointmentTransformer;

    @Before
    public void before() {
        toAppointmentTransformer = new AvailabilityToAppointmentTransformer(Arrays.asList(
            new AvailabilityConfigurationStrategy(),
            new ExcludedAvailabilityStrategy(),
            new OpeningHoursStrategy(),
            new PracticeHolidayStrategy(holidayRepository)
        ), resourceProcessor, availabilityMapper);
    }

    @Test
    public void transform_should_respect_excluded_days_and_send_remove_instructions()
    {
        Availability availability = new Availability();
        availability.setBegin(Instant.now());
        availability.setEnd(Instant.now().plus(1, ChronoUnit.HOURS));
        availability.setDuration(30);
        availability.getExcludedDays().add(TODAY_AT_MIDNIGHT.toInstant());
        availability.getAppointments().add(dummyAppointmentWithId(42L, TODAY_AT_MIDNIGHT.plusHours(6).toInstant()));

        Iterator<AvailabilityToAppointmentTransformer.AppointmentChange> changes = toAppointmentTransformer
            .transform(availability, new PracticeDto(), TODAY_AT_MIDNIGHT)
            .iterator();

        assertTrue(changes.hasNext());
        assertEquals(42, changes.next().getDeletedAppointmentId().intValue());
        assertFalse(changes.hasNext());
    }

    @Test
    public void transform_should_respect_holidays_and_send_remove_instructions()
    {
        Availability availability = new Availability();
        availability.setPracticeId(42L);
        availability.setDuration(30);
        availability.setBegin(Instant.now());
        availability.setEnd(availability.getBegin().plus(1, ChronoUnit.HOURS));
        availability.getResources().add(new AppointmentResource(42L, availability.getBegin(), availability.getEnd()));
        availability.getAppointments().add(dummyAppointmentWithId(42L, TODAY_AT_MIDNIGHT.plusHours(6).toInstant()));

        Instant dayBegin = TODAY_AT_MIDNIGHT.withHour(0).withMinute(0).withSecond(0).withNano(0).toInstant();
        Instant dayEnd = TODAY_AT_MIDNIGHT.withHour(23).withMinute(59).withSecond(59).withNano(999999999).toInstant();

        when(holidayRepository.findInRange(42L, dayBegin, dayEnd, Sets.newHashSet(42L)))
            .thenReturn(2);

        Iterator<AvailabilityToAppointmentTransformer.AppointmentChange> changes = toAppointmentTransformer
            .transform(availability, new PracticeDto(), TODAY_AT_MIDNIGHT)
            .iterator();

        assertTrue(changes.hasNext());
        assertEquals(42, changes.next().getDeletedAppointmentId().intValue());
        assertFalse(changes.hasNext());

        verify(holidayRepository, times(1)).findInRange(42L, dayBegin, dayEnd, Sets.newHashSet(42L));
    }

    @Test
    public void transform_should_remove_appointments_if_doctor_is_no_longer_valid()
    {
        Availability availability = new Availability();
        availability.setDuration(30);
        availability.setBegin(Instant.now());
        availability.setEnd(Instant.now().plus(1, ChronoUnit.HOURS));
        availability.getResources().add(new AppointmentResource(42L, availability.getBegin(), availability.getEnd()));
        availability.getAppointments().add(dummyAppointmentWithId(42L, TODAY_AT_MIDNIGHT.plusHours(6).toInstant()));

        Iterator<AvailabilityToAppointmentTransformer.AppointmentChange> changes = toAppointmentTransformer
            .transform(availability, new PracticeDto(), TODAY_AT_MIDNIGHT)
            .iterator();

        assertTrue(changes.hasNext());
        assertEquals(42, changes.next().getDeletedAppointmentId().intValue());
        assertFalse(changes.hasNext());
    }

    @Test
    public void transform_should_use_correct_doctor_even_if_multiple_resources_exists_and_are_applied()
    {
        Availability availability = new Availability();
        availability.setDuration(30);
        availability.setBegin(TODAY_AT_MIDNIGHT.toInstant());
        availability.setEnd(TODAY_AT_MIDNIGHT.plusHours(1).toInstant());
        availability.getResources().add(new AppointmentResource(42L, availability.getBegin(), availability.getEnd()));
        availability.getResources().add(new AppointmentResource(55L, availability.getBegin(), availability.getEnd()));
        availability.getResources().add(new AppointmentResource(66L, availability.getBegin(), availability.getEnd()));

        PracticeDto practice = dummyPracticeWithDoctor(42L, 42L);

        DoctorProfileDto anotherDoc = new DoctorProfileDto();
        anotherDoc.setId(55L);

        ResourceDto anotherDocResource = new ResourceDto();
        anotherDocResource.setId(55L);
        anotherDocResource.setSystemId(55L);
        anotherDocResource.setType(ResourceType.DOCTOR);

        practice.getDoctors().add(anotherDoc);
        practice.getResources().add(anotherDocResource);

        Iterator<AvailabilityToAppointmentTransformer.AppointmentChange> changes = toAppointmentTransformer
            .transform(availability, practice, TODAY_AT_MIDNIGHT)
            .iterator();

        assertTrue(changes.hasNext());
        assertTrue(changes.next().isNew());
        assertTrue(changes.next().isNew());
        assertFalse(changes.hasNext());
    }

    @Test
    public void transform_should_remove_existing_appointment_if_not_booked_and_no_longer_valid_in_the_range()
    {
        Availability availability = new Availability();
        availability.setDuration(30);
        availability.setBegin(Instant.now());
        availability.setEnd(Instant.now().plus(availability.getDuration(), ChronoUnit.MINUTES));
        availability.getResources().add(new AppointmentResource(55L, true, availability.getBegin(), availability.getEnd()));

        Appointment appointment1 = dummyAppointmentWithId(65L, availability.getBegin());
        appointment1.getResources().add(new AppointmentResource(55L, availability.getBegin(), availability.getEnd()));
        availability.getAppointments().add(appointment1);

        Appointment appointment2 = dummyAppointmentWithId(66L, availability.getBegin());
        appointment2.getResources().add(new AppointmentResource(55L, availability.getBegin(), availability.getEnd()));
        appointment2.setBookedByCustomer(42L);
        availability.getAppointments().add(appointment2);

        PracticeDto practice = dummyPracticeWithDoctor(55L, 55L);
        ErrorDto error = new ErrorDto("CODE", 5, "not available");

        // 0L => id of appointment resource (!= resource id)
        doThrow(new ResponseErrorListException(Collections.singletonList(error))).when(resourceProcessor)
            .resolveAndAttachResources(any(), eq(practice), any(), eq(Sets.newHashSet(0L)), eq(false));

        Iterator<AvailabilityToAppointmentTransformer.AppointmentChange> changes = toAppointmentTransformer
            .transform(availability, practice, TODAY_AT_MIDNIGHT)
            .iterator();

        assertTrue(changes.hasNext());

        AvailabilityToAppointmentTransformer.AppointmentChange change = changes.next();
        assertTrue(change.isDeleted());
        assertEquals(65, change.getDeletedAppointmentId().intValue());
        assertFalse(changes.hasNext());

        verify(resourceProcessor, times(1))
            .resolveAndAttachResources(any(), eq(practice), any(), eq(Sets.newHashSet(0L)), eq(false));
    }

    @Test
    public void transform_should_ignore_booked_or_disabled_appointments()
    {
        Availability availability = new Availability();
        availability.setDuration(30);
        availability.setBegin(TODAY_AT_MIDNIGHT.toInstant());
        availability.setEnd(TODAY_AT_MIDNIGHT.plusMinutes(availability.getDuration() * 2).toInstant());
        availability.getTreatmentTypes().addAll(Arrays.asList(21L, 22L, 23L));
        availability.getResources().add(new AppointmentResource(55L, availability.getBegin(), availability.getEnd()));

        Appointment appointment1 = dummyAppointmentWithId(42L, availability.getBegin());
        appointment1.setBookedByCustomer(12345678L);

        Appointment appointment2 = dummyAppointmentWithId(43L, availability.getBegin().plus(30, ChronoUnit.MINUTES));
        appointment2.wipeOut();

        availability.getAppointments().add(appointment1);
        availability.getAppointments().add(appointment2);

        PracticeDto practice = dummyPracticeWithDoctor(55L, 55L);

        Iterator<AvailabilityToAppointmentTransformer.AppointmentChange> changes = toAppointmentTransformer
            .transform(availability, practice, TODAY_AT_MIDNIGHT)
            .iterator();

        assertTrue(changes.hasNext());
        assertFalse(changes.next().isEdited());
        assertFalse(changes.next().isEdited());
        assertFalse(changes.hasNext());
    }

    @Test
    public void transform_should_edit_existing_appointment_if_present_and_add_a_new_one_for_greater_ranges()
    {
        Availability availability = new Availability();
        availability.setDuration(30);
        availability.setBegin(Instant.now().atOffset(ZoneOffset.UTC).plusDays(2).withSecond(0).withNano(0).toInstant());
        availability.setEnd(availability.getBegin().plus(1, ChronoUnit.HOURS));
        availability.getResources().add(new AppointmentResource(55L, availability.getBegin(), availability.getEnd()));
        availability.getAppointments().add(dummyAppointmentWithId(42L, TODAY_AT_MIDNIGHT.plusDays(2).plusHours(1).toInstant()));

        PracticeDto practice = dummyPracticeWithDoctor(55L, 55L);

        Iterator<AvailabilityToAppointmentTransformer.AppointmentChange> changes = toAppointmentTransformer
            .transform(availability, practice, TODAY_AT_MIDNIGHT.plusDays(2))
            .iterator();

        assertTrue(changes.hasNext());

        Instant firstEnd = availability.getBegin().plus(availability.getDuration(), ChronoUnit.MINUTES);
        AvailabilityToAppointmentTransformer.AppointmentChange updated = changes.next();
        assertTrue(updated.isEdited());
        assertNull(updated.getDeletedAppointmentId());
        assertEquals(availability.getBegin(), updated.getAppointment().getStart());
        assertEquals(firstEnd, updated.getAppointment().getEnd());

        AvailabilityToAppointmentTransformer.AppointmentChange newAppointment = changes.next();
        assertFalse(newAppointment.isEdited());
        assertFalse(newAppointment.isDeleted());
        assertEquals(firstEnd, newAppointment.getAppointment().getStart());
        assertEquals(firstEnd.plus(availability.getDuration(), ChronoUnit.MINUTES), newAppointment.getAppointment().getEnd());
        assertFalse(changes.hasNext());
    }

    @Test
    public void transform_should_remove_appointments_scheduled_on_invalid_or_previous_recurring_config()
    {
        Availability availability = new Availability();
        availability.setDuration(30);
        availability.setBegin(Instant.now().atOffset(ZoneOffset.UTC).plusDays(3).withSecond(0).withNano(0).toInstant());
        availability.setEnd(availability.getBegin().plus(1, ChronoUnit.HOURS));
        availability.getResources().add(new AppointmentResource(55L, availability.getBegin(), availability.getEnd()));
        availability.getAppointments().add(dummyAppointmentWithId(42L, TODAY_AT_MIDNIGHT.plusHours(6).toInstant()));
        availability.setRecurringConfig(new RecurringConfiguration(RecurringType.WEEKLY));

        PracticeDto practice = dummyPracticeWithDoctor(55L, 55L);

        Iterator<AvailabilityToAppointmentTransformer.AppointmentChange> changes = toAppointmentTransformer
            .transform(availability, practice, TODAY_AT_MIDNIGHT)
            .iterator();

        assertTrue(changes.hasNext());

        AvailabilityToAppointmentTransformer.AppointmentChange updated = changes.next();
        assertTrue(updated.isEdited());
        assertEquals(42L, updated.getDeletedAppointmentId().longValue());
        assertFalse(changes.hasNext());
    }

    @Test
    public void transform_should_set_insurance_type_to_appointment()
    {
        Availability availability = new Availability();
        availability.setDuration(15);
        availability.setBegin(TODAY_AT_MIDNIGHT.withHour(23).withMinute(30).withSecond(0).withNano(0).toInstant());
        availability.setEnd(availability.getBegin().plus(30, ChronoUnit.MINUTES));
        availability.getResources().add(new AppointmentResource(55L, availability.getBegin(), availability.getEnd()));
        availability.getInsuranceTypes().addAll(Collections.singletonList(InsuranceType.PRIVATE)); // private only

        Appointment appointment = dummyAppointmentWithId(42L, availability.getBegin());
        appointment.getInsuranceTypes().addAll(Arrays.asList(InsuranceType.COMPULSORY, InsuranceType.PRIVATE));
        availability.getAppointments().add(appointment);

        PracticeDto practice = dummyPracticeWithDoctor(55L, 55L);

        Iterator<AvailabilityToAppointmentTransformer.AppointmentChange> changes = toAppointmentTransformer
            .transform(availability, practice, TODAY_AT_MIDNIGHT)
            .iterator();

        assertTrue(changes.hasNext());

        AvailabilityToAppointmentTransformer.AppointmentChange updated = changes.next();
        assertTrue(updated.isEdited());
        assertEquals(1, updated.getAppointment().getInsuranceTypes().size());
        assertEquals(InsuranceType.PRIVATE, updated.getAppointment().getInsuranceTypes().iterator().next());


        AvailabilityToAppointmentTransformer.AppointmentChange newOne = changes.next();
        assertEquals(1, newOne.getAppointment().getInsuranceTypes().size());
        assertEquals(InsuranceType.PRIVATE, newOne.getAppointment().getInsuranceTypes().iterator().next());

        assertFalse(changes.hasNext());
    }

    @Test
    public void transform_should_omit_new_appointments_if_scheduled_by_recurring_config()
    {
        Availability availability = new Availability();
        availability.setDuration(30);
        availability.setBegin(Instant.now().atOffset(ZoneOffset.UTC).plusDays(3).withSecond(0).withNano(0).toInstant());
        availability.setEnd(availability.getBegin().plus(1, ChronoUnit.HOURS));
        availability.getResources().add(new AppointmentResource(55L, availability.getBegin(), availability.getEnd()));
        availability.setRecurringConfig(new RecurringConfiguration(RecurringType.WEEKLY));

        PracticeDto practice = dummyPracticeWithDoctor(55L, 55L);

        Iterator<AvailabilityToAppointmentTransformer.AppointmentChange> changes = toAppointmentTransformer
            .transform(availability, practice, TODAY_AT_MIDNIGHT)
            .iterator();

        assertFalse(changes.hasNext());
    }

    @Test
    public void transform_should_omit_appointments_outside_of_opening_hours()
    {
        OffsetDateTime sunday = TODAY_AT_MIDNIGHT.with(DayOfWeek.SUNDAY).plusHours(6);

        Availability availability = new Availability();
        availability.setDuration(30);
        availability.setBegin(sunday.withSecond(0).withNano(0).toInstant());
        availability.getResources().add(new AppointmentResource(55L, true, availability.getBegin(), availability.getEnd()));
        availability.setEnd(availability.getBegin().plus(1, ChronoUnit.HOURS));
        availability.getAppointments().add(dummyAppointmentWithId(55L, TODAY_AT_MIDNIGHT.with(DayOfWeek.MONDAY).toInstant()));
        availability.getAppointments().add(dummyAppointmentWithId(56L, sunday.toInstant()));

        OpeningHoursDto openingHours = new OpeningHoursDto();
        openingHours.put(DayOfWeek.MONDAY, Collections.emptyList());

        PracticeDto practice = dummyPracticeWithDoctor(55L, 55L);
        practice.setOpeningHours(openingHours);

        Iterator<AvailabilityToAppointmentTransformer.AppointmentChange> changes = toAppointmentTransformer
            .transform(availability, practice, TODAY_AT_MIDNIGHT.with(DayOfWeek.SUNDAY))
            .iterator();

        assertTrue(changes.hasNext());

        AvailabilityToAppointmentTransformer.AppointmentChange change = changes.next();
        assertTrue(change.isDeleted());
        assertEquals(56L, change.getDeletedAppointmentId().longValue());
        assertFalse(changes.hasNext());
    }

    @Test
    public void transform_should_remove_existing_unbooked_appointments_which_does_not_take_place_anymore()
    {
        Availability availability = new Availability();
        availability.setDuration(30);
        availability.setBegin(TODAY_AT_MIDNIGHT.toInstant());
        availability.setEnd(TODAY_AT_MIDNIGHT.plusMinutes(30).toInstant());
        availability.getResources().add(new AppointmentResource(55L, true, availability.getBegin(), availability.getEnd()));
        availability.getAppointments().add(dummyAppointmentWithId(55L, TODAY_AT_MIDNIGHT.toInstant()));
        availability.getAppointments().add(dummyAppointmentWithId(56L, TODAY_AT_MIDNIGHT.plusMinutes(30).toInstant()));

        PracticeDto practice = dummyPracticeWithDoctor(55L, 55L);

        Iterator<AvailabilityToAppointmentTransformer.AppointmentChange> changes = toAppointmentTransformer
            .transform(availability, practice, TODAY_AT_MIDNIGHT)
            .iterator();

        assertTrue(changes.hasNext());

        AvailabilityToAppointmentTransformer.AppointmentChange change = changes.next();
        assertTrue(change.isEdited() && !change.isDeleted());
        assertEquals(availability.getAppointments().get(0), change.getAppointment());

        change = changes.next();
        assertTrue(change.isDeleted());
        assertEquals(56L, change.getDeletedAppointmentId().longValue());

        assertFalse(changes.hasNext());
    }

    @Test
    public void transform_should_generate_multiple_appointments_for_weekly_recurring_availability()
    {
        OffsetDateTime start = Instant.now().atOffset(ZoneOffset.UTC).withSecond(0).withNano(0);

        Availability availability = spy(new Availability());
        availability.getResources().add(new AppointmentResource(66L, start.toInstant(), availability.getEnd()));
        when(availability.getId()).thenReturn(66L);
        availability.setBegin(start.toInstant());
        availability.setEnd(start.plusHours(1).toInstant());
        availability.setDuration(60);

        RecurringConfiguration config = new RecurringConfiguration(RecurringType.WEEKLY);
        config.setEndsAt(start.plusDays(365).toInstant());
        availability.setRecurringConfig(config);

        PracticeDto practice = dummyPracticeWithDoctor(55L, 66L);
        long distanceInDays = Duration.between(start, start.plusMonths(3)).toDays();
        List<AvailabilityToAppointmentTransformer.AppointmentChange> changes = new ArrayList<>();

        for (int day = 0; day < distanceInDays; day++) {
            changes.addAll(toAppointmentTransformer.transform(availability, practice, start.plusDays(day)));
        }

        /*
         * Example: (now -> 3 months (max future))
         * 2017-05-18T15:59:00Z - 2017-05-18T16:59:00Z
         * 2017-05-25T15:59:00Z - 2017-05-25T16:59:00Z
         * 2017-06-01T15:59:00Z - 2017-06-01T16:59:00Z
         * 2017-06-08T15:59:00Z - 2017-06-08T16:59:00Z
         * 2017-06-15T15:59:00Z - 2017-06-15T16:59:00Z
         * 2017-06-22T15:59:00Z - 2017-06-22T16:59:00Z
         * 2017-06-29T15:59:00Z - 2017-06-29T16:59:00Z
         * 2017-07-06T15:59:00Z - 2017-07-06T16:59:00Z
         * 2017-07-13T15:59:00Z - 2017-07-13T16:59:00Z
         * 2017-07-20T15:59:00Z - 2017-07-20T16:59:00Z
         * 2017-07-27T15:59:00Z - 2017-07-27T16:59:00Z
         * 2017-08-03T15:59:00Z - 2017-08-03T16:59:00Z
         * 2017-08-10T15:59:00Z - 2017-08-10T16:59:00Z
         * 2017-08-17T15:59:00Z - 2017-08-17T16:59:00Z
         */
        assertEquals(14, changes.size());
        assertEquals(start.toInstant(), changes.get(0).getAppointment().getStart());
        assertEquals(start.plusDays(7).toInstant(), changes.get(1).getAppointment().getStart());
        assertEquals(start.plusDays(14).toInstant(), changes.get(2).getAppointment().getStart());
        assertEquals(start.plusDays(21).toInstant(), changes.get(3).getAppointment().getStart());
        assertEquals(start.plusDays(28).toInstant(), changes.get(4).getAppointment().getStart());
        assertEquals(start.plusDays(35).toInstant(), changes.get(5).getAppointment().getStart());
        assertEquals(start.plusDays(42).toInstant(), changes.get(6).getAppointment().getStart());
        assertEquals(start.plusDays(49).toInstant(), changes.get(7).getAppointment().getStart());
        assertEquals(start.plusDays(56).toInstant(), changes.get(8).getAppointment().getStart());
        assertEquals(start.plusDays(63).toInstant(), changes.get(9).getAppointment().getStart());
        assertEquals(start.plusDays(70).toInstant(), changes.get(10).getAppointment().getStart());
        assertEquals(start.plusDays(77).toInstant(), changes.get(11).getAppointment().getStart());
        assertEquals(start.plusDays(84).toInstant(), changes.get(12).getAppointment().getStart());
        assertEquals(start.plusDays(91).toInstant(), changes.get(13).getAppointment().getStart());
    }

    @Test
    public void transform_should_generate_multiple_appointments_for_daily_recurring_availability()
    {
        OffsetDateTime start = Instant.now().atOffset(ZoneOffset.UTC).withSecond(0).withNano(0);

        Availability availability = spy(new Availability());
        availability.setRecurringConfig(new RecurringConfiguration(RecurringType.DAILY));
        availability.getResources().add(new AppointmentResource(66L, start.toInstant(), availability.getEnd()));
        when(availability.getId()).thenReturn(66L);
        availability.setBegin(start.toInstant());
        availability.setEnd(start.plusHours(1).toInstant());
        availability.setDuration(60);

        PracticeDto practice = dummyPracticeWithDoctor(55L, 66L);
        long distanceInDays = Duration.between(start, start.plusMonths(3)).toDays();
        List<AvailabilityToAppointmentTransformer.AppointmentChange> changes = new ArrayList<>();

        for (int day = 0; day < distanceInDays; day++) {
            changes.addAll(toAppointmentTransformer.transform(availability, practice, start.plusDays(day)));
        }

        assertEquals(distanceInDays, changes.size());
    }

    @Test
    public void transform_should_generate_appointments_for_weekly_recurring_availabilities_and_must_respect_recurring_end()
    {
        Instant start = Instant.now();

        Availability availability = new Availability();
        availability.setDuration(60);
        availability.setBegin(start);
        availability.setEnd(start.plus(1, ChronoUnit.HOURS));
        availability.getResources().add(new AppointmentResource(42L, availability.getBegin(), availability.getEnd()));

        RecurringConfiguration config = new RecurringConfiguration(RecurringType.WEEKLY);
        config.setEndsAt(start.plus(16, ChronoUnit.DAYS));
        availability.setRecurringConfig(config);

        PracticeDto practice = dummyPracticeWithDoctor(55L, 42L);

        List<AvailabilityToAppointmentTransformer.AppointmentChange> changes = new ArrayList<>();
        long distanceInDays = Duration.between(start, start.atOffset(ZoneOffset.UTC).plusMonths(3)).toDays();

        for (int day = 0; day < distanceInDays; day++) {
            changes.addAll(toAppointmentTransformer.transform(availability, practice, start.atOffset(ZoneOffset.UTC).plusDays(day)));
        }

        /*
         * Example: (now -> 3 months (max future))
         * 2017-05-18T15:59:00Z - 2017-05-18T16:59:00Z (rem. 16)
         * 2017-05-25T15:59:00Z - 2017-05-25T16:59:00Z (rem. 9)
         * 2017-06-01T15:59:00Z - 2017-06-01T16:59:00Z (rem 2)
         */
        assertEquals(3, changes.size());
        assertTrue(changes.get(0).isNew());
        assertTrue(changes.get(1).isNew());
        assertTrue(changes.get(2).isNew());
    }

    @Test
    public void transform_should_remove_appointments_for_daily_recurring_availabilities_outside_of_the_availability()
    {
        OffsetDateTime start = Instant.now().atOffset(ZoneOffset.UTC).minusDays(10).withSecond(0).withNano(0);

        Availability availability = new Availability();
        availability.setDuration(60);
        availability.setBegin(start.toInstant());
        availability.setEnd(start.plusHours(1).toInstant());
        availability.getResources().add(new AppointmentResource(42L, availability.getBegin(), availability.getEnd()));
        availability.getAppointments().add(dummyAppointmentWithId(1L, start.toInstant()));
        availability.getAppointments().add(dummyAppointmentWithId(2L, start.plusMonths(1).toInstant()));
        availability.getAppointments().add(dummyAppointmentWithId(3L, start.plusMonths(2).toInstant()));
        // ....

        boolean is1stMonthOnWeekDay = start.plusMonths(1).getDayOfWeek().equals(start.getDayOfWeek());
        boolean is2ndMonthOnWeekDay = start.plusMonths(2).getDayOfWeek().equals(start.getDayOfWeek());

        // tested change: change from monthly to weekly recurring availability
        availability.setRecurringConfig(new RecurringConfiguration(RecurringType.WEEKLY));

        PracticeDto practice = dummyPracticeWithDoctor(55L, 42L);

        List<AvailabilityToAppointmentTransformer.AppointmentChange> changes = new ArrayList<>();
        long distanceInDays = Duration.between(start, start.plusMonths(3)).toDays();

        for (int day = 0; day < distanceInDays; day++) {
            changes.addAll(toAppointmentTransformer.transform(availability, practice, start.plusDays(day)));
        }

        int offset = 0;
        Iterator<AvailabilityToAppointmentTransformer.AppointmentChange> iterator = changes.iterator();
        assertTrue(iterator.hasNext());

        AvailabilityToAppointmentTransformer.AppointmentChange first = iterator.next();
        assertTrue(first.isEdited()); // pick up the first appointment generated for monthly recurring
        assertEquals(start, first.getAppointment().getStart().atOffset(ZoneOffset.UTC));
        assertEquals(start.plusWeeks(1), iterator.next().getAppointment().getStart().atOffset(ZoneOffset.UTC));
        assertEquals(start.plusWeeks(2), iterator.next().getAppointment().getStart().atOffset(ZoneOffset.UTC));
        assertEquals(start.plusWeeks(3), iterator.next().getAppointment().getStart().atOffset(ZoneOffset.UTC));
        assertEquals(start.plusWeeks(4), iterator.next().getAppointment().getStart().atOffset(ZoneOffset.UTC));

        // pick up the second appointment generated for monthly recurring if its on the same weekday
        if (is1stMonthOnWeekDay)
            assertEquals(start.plusWeeks(5), iterator.next().getAppointment().getStart().atOffset(ZoneOffset.UTC));
        else {
            assertTrue(iterator.next().isDeleted());
            offset++;
        }

        assertEquals(start.plusWeeks(6 - offset), iterator.next().getAppointment().getStart().atOffset(ZoneOffset.UTC));
        assertEquals(start.plusWeeks(7 - offset), iterator.next().getAppointment().getStart().atOffset(ZoneOffset.UTC));
        assertEquals(start.plusWeeks(8 - offset), iterator.next().getAppointment().getStart().atOffset(ZoneOffset.UTC));
        assertEquals(start.plusWeeks(9 - offset), iterator.next().getAppointment().getStart().atOffset(ZoneOffset.UTC));

        // pick up the third appointment generated for monthly recurring if its on the same weekday
        if (is2ndMonthOnWeekDay)
            assertEquals(start.plusWeeks(10 - offset), iterator.next().getAppointment().getStart().atOffset(ZoneOffset.UTC));
        else {
            assertTrue(iterator.next().isDeleted());
            offset++;
        }

        assertEquals(start.plusWeeks(11 - offset), iterator.next().getAppointment().getStart().atOffset(ZoneOffset.UTC));
        assertEquals(start.plusWeeks(12 - offset), iterator.next().getAppointment().getStart().atOffset(ZoneOffset.UTC));
        assertEquals(start.plusWeeks(13 - offset), iterator.next().getAppointment().getStart().atOffset(ZoneOffset.UTC));
    }

    private Appointment dummyAppointmentWithId(long id, Instant start) {

        Appointment appointment = spy(new Appointment());
        when(appointment.getId()).thenReturn(id);
        appointment.setStart(start);
        appointment.setEnd(appointment.getStart().plus(30, ChronoUnit.MINUTES));

        return appointment;
    }

    private PracticeDto dummyPracticeWithDoctor(long doctorId, long resourceId) {
        DoctorProfileDto profile = new DoctorProfileDto();
        profile.setId(doctorId);

        ResourceDto resource = new ResourceDto();
        resource.setSystemId(doctorId);
        resource.setType(ResourceType.DOCTOR);
        resource.setId(resourceId);

        PracticeDto practiceDto = new PracticeDto();
        practiceDto.getDoctors().add(profile);
        practiceDto.getResources().add(resource);

        return practiceDto;
    }
}
