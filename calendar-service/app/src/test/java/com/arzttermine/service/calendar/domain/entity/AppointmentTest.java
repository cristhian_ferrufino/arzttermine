package com.arzttermine.service.calendar.domain.entity;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class AppointmentTest
{
    @Test
    public void equals_returns_false_when_comparator_is_null()
    {
        Appointment appointment1 = spy(new Appointment());
        when(appointment1.getId()).thenReturn(55L);

        assertFalse(appointment1.equals(null));
    }

    @Test
    public void equals_returns_false_when_id_does_not_match()
    {
        Appointment appointment1 = spy(new Appointment());
        when(appointment1.getId()).thenReturn(55L);

        Appointment appointment2 = spy(new Appointment());
        when(appointment2.getId()).thenReturn(66L);

        assertFalse(appointment1.equals(appointment2));
    }

    @Test
    public void equals_returns_false_when_comparator_is_booked_by_a_different_customer()
    {
        Appointment appointment1 = new Appointment();
        appointment1.setBookedByCustomer(42L);

        Appointment appointment2 = new Appointment();
        appointment2.setBookedByCustomer(24L);

        assertFalse(appointment1.equals(appointment2));
    }

    @Test
    public void equals_returns_false_when_comparator_disabled()
    {
        Appointment appointment1 = new Appointment();
        appointment1.setBookedByCustomer(42L);

        Appointment appointment2 = new Appointment();
        appointment2.wipeOut();

        assertFalse(appointment1.equals(appointment2));
    }

    @Test
    public void equals_returns_true_for_equal_appointments()
    {
        Appointment appointment1 = new Appointment();
        appointment1.setPracticeId(42L);
        appointment1.setDoctorId(55L);
        appointment1.setEnabled(true);

        Appointment appointment2 = new Appointment();
        appointment2.setPracticeId(42L);
        appointment2.setDoctorId(55L);
        appointment2.setEnabled(true);

        assertTrue(appointment1.equals(appointment2));
    }

    @Test
    public void equals_from_copy_constructor_should_return_true()
    {
        Appointment appointment1 = new Appointment();
        appointment1.setPracticeId(42L);
        appointment1.setDoctorId(55L);
        appointment1.setEnabled(true);

        assertTrue(appointment1.equals(new Appointment(appointment1)));
    }

    @Test
    public void equals_from_copy_constructor_with_id_should_return_false()
    {
        Appointment appointment1 = spy(new Appointment());
        when(appointment1.getId()).thenReturn(12345L);
        appointment1.setPracticeId(42L);
        appointment1.setDoctorId(55L);
        appointment1.setEnabled(true);

        assertFalse(appointment1.equals(new Appointment(appointment1)));
    }

    @Test
    public void isDuringTime_should_return_true_for_valid_time_around_start_time()
    {
        Appointment appointment = new Appointment();
        appointment.setStart(Instant.now().atOffset(ZoneOffset.UTC).withHour(10).withMinute(30).toInstant());
        appointment.setEnd(appointment.getStart().plusSeconds(360));

        assertFalse(appointment.isDuringTime(10, 29));
        assertTrue(appointment.isDuringTime(10, 30));
        assertTrue(appointment.isDuringTime(10, 31));
    }

    @Test
    public void isDuringTime_should_return_true_for_valid_time_around_end_time()
    {
        OffsetDateTime start = Instant.now().atOffset(ZoneOffset.UTC).withHour(10).withMinute(30);

        Appointment appointment = new Appointment();
        appointment.setStart(start.toInstant());
        appointment.setEnd(start.plusHours(1).toInstant());

        assertTrue(appointment.isDuringTime(11, 29));
        assertTrue(appointment.isDuringTime(11, 30));
        assertFalse(appointment.isDuringTime(11, 31));
    }
}
