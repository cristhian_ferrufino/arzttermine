package com.arzttermine.service.calendar.service.event;

import com.arzttermine.service.calendar.service.AvailabilityServiceTest;
import com.arzttermine.service.calendar.service.externals.PracticeApi;
import com.arzttermine.service.calendar.web.BaseIntegrationTest;
import com.arzttermine.service.calendar.web.dto.RecurringConfigDto;
import com.arzttermine.service.calendar.web.dto.request.CreateAvailabilitiesConfigurationDto;
import com.arzttermine.service.calendar.web.dto.request.CreateHolidaysDto;
import com.arzttermine.service.calendar.web.dto.request.HolidayTimeRangeDto;
import com.arzttermine.service.calendar.web.dto.response.HolidayDto;
import com.arzttermine.service.calendar.web.struct.RecurringType;
import com.arzttermine.service.profile.web.dto.response.DoctorProfileDto;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import com.arzttermine.service.profile.web.dto.response.ResourceDto;
import com.arzttermine.service.profile.web.dto.response.TreatmentTypeDto;
import com.arzttermine.service.profile.web.struct.ResourceType;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@Sql("/db/empty_database.sql")
public class HolidayEventListenerTest extends BaseIntegrationTest
{
    private static final OffsetDateTime START = OffsetDateTime.now().plusDays(1);
    private static PracticeDto practice;

    @Autowired
    private PracticeApi practiceClient;

    @Before
    public void beforeTest() throws Exception {

        ResourceDto resource = new ResourceDto();
        resource.setType(ResourceType.DOCTOR);
        resource.setSystemId(23L);
        resource.setId(12345L);

        TreatmentTypeDto tt = new TreatmentTypeDto();
        tt.setDoctorIds(Collections.singletonList(23L));
        tt.setType("example");
        tt.setId(333L);

        practice = new PracticeDto();
        practice.setResources(Collections.singletonList(resource));
        practice.setName("practice example");
        practice.setId(42L);

        DoctorProfileDto profile = new DoctorProfileDto();
        profile.setProfileId(99L);
        profile.setId(23L);
        practice.getDoctors().add(profile);
        practice.getTreatmentTypes().add(tt);

        Map<Long, PracticeDto> practiceMap = new HashMap<>();
        practiceMap.put(42L, practice);

        when(practiceClient.findByIds(any())).thenReturn(practiceMap);
        when(practiceClient.findById(eq(42L))).thenReturn(practice);

        RecurringConfigDto recurringConfig = new RecurringConfigDto();
        recurringConfig.setEndsAt(START.plusDays(30).toLocalDate());
        recurringConfig.setType(RecurringType.DAILY);

        CreateAvailabilitiesConfigurationDto request = new CreateAvailabilitiesConfigurationDto();
        request.getRequests().add(AvailabilityServiceTest.buildRequestDto(START.toInstant()));
        request.setRecurringConfig(recurringConfig);

        mockMvc.perform(systemRequest(() -> post("/availabilities"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated());

        Thread.sleep(280);

        mockMvc.perform(systemRequest(() -> get("/appointments")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.page.totalItems", is(30)));
    }

    @Test
    public void createHolidays_should_remove_excluded_and_existing_appointments_in_range_between_min_and_max_day() throws Exception
    {
        HolidayTimeRangeDto range1 = new HolidayTimeRangeDto();
        range1.setResourceIds(Sets.newHashSet(12345L));
        range1.setBegin(START.plusDays(6).toLocalDate());
        range1.setEnd(START.plusDays(7).toLocalDate());
        range1.setName("christmas");

        HolidayTimeRangeDto range2 = new HolidayTimeRangeDto();
        range2.setBegin(START.plusDays(9).toLocalDate());
        range2.setName("public holiday");
        range2.setPublicHoliday(true);

        CreateHolidaysDto request = new CreateHolidaysDto();
        request.getTimes().add(range1);
        request.getTimes().add(range2);
        request.setPracticeId(42L);

        mockMvc.perform(systemRequest(() -> post("/holidays"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated());

        Thread.sleep(250);

        mockMvc.perform(systemRequest(() -> get("/appointments")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.page.totalItems", is(30 - 3)));
    }

    @Test
    public void createHolidays_should_remove_existing_appointments_in_complete_range() throws Exception
    {
        HolidayTimeRangeDto range1 = new HolidayTimeRangeDto();
        range1.setResourceIds(Sets.newHashSet(12345L));
        range1.setBegin(START.plusDays(5).toLocalDate());
        range1.setEnd(START.plusDays(9).toLocalDate());
        range1.setName("christmas");

        CreateHolidaysDto request = new CreateHolidaysDto();
        request.getTimes().add(range1);
        request.setPracticeId(42L);

        mockMvc.perform(systemRequest(() -> post("/holidays"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated());

        Thread.sleep(250);

        mockMvc.perform(systemRequest(() -> get("/appointments")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.page.totalItems", is(30 - 5)));
    }

    @Test
    public void deleteHoliday_should_regenerate_appointments_for_previous_holiday() throws Exception
    {
        HolidayTimeRangeDto range1 = new HolidayTimeRangeDto();
        range1.setResourceIds(Sets.newHashSet(12345L));
        range1.setBegin(START.plusDays(5).toLocalDate());
        range1.setEnd(START.plusDays(5).toLocalDate());
        range1.setName("christmas");

        CreateHolidaysDto request = new CreateHolidaysDto();
        request.getTimes().add(range1);
        request.setPracticeId(42L);

        long id = objectMapper.readValue(mockMvc.perform(systemRequest(() -> post("/holidays"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsByteArray(), HolidayDto[].class)[0]
            .getId();

        Thread.sleep(180);

        mockMvc.perform(systemRequest(() -> delete("/holidays/" + id)))
            .andExpect(status().isNoContent());

        Thread.sleep(200);

        mockMvc.perform(systemRequest(() -> get("/appointments")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.page.totalItems", is(30)));
    }
}
