package com.arzttermine.service.calendar.config;

import com.arzttermine.library.elasticsearch.service.DocumentProcessor;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("testing")
public class SearchTestConfiguration
{
    @Bean
    public DocumentProcessor documentProcessor()
    {
        return Mockito.mock(DocumentProcessor.class);
    }
}
