package com.arzttermine.service.calendar.service.processor.transformer.exclusion;

import com.arzttermine.service.calendar.domain.entity.Availability;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.Instant;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class ExcludedAvailabilityStrategyTest
{
    private ExclusionStrategy strategy = new ExcludedAvailabilityStrategy();

    @Test
    public void isExcluded_should_return_true_for_excluded_day()
    {
        Availability availability = availabilityWith(Instant.now(), 30, LocalDate.now());

        boolean excluded = strategy.isExcluded(availability, new PracticeDto(), availability.getBegin().atOffset(ZoneOffset.UTC));
        assertTrue(excluded);
    }

    @Test
    public void isExcluded_should_return_false_for_days_between_excluded_days()
    {
        OffsetDateTime atDate = Instant.now().atOffset(ZoneOffset.UTC);

        Availability availability = availabilityWith(atDate.toInstant(), 30, atDate.toLocalDate(), atDate.plusDays(2).toLocalDate());

        boolean excluded = strategy.isExcluded(availability, new PracticeDto(), atDate.minusDays(1));
        assertFalse(excluded);

        excluded = strategy.isExcluded(availability, new PracticeDto(), atDate);
        assertTrue(excluded);

        excluded = strategy.isExcluded(availability, new PracticeDto(), atDate.plusDays(1));
        assertFalse(excluded);

        excluded = strategy.isExcluded(availability, new PracticeDto(), atDate.plusDays(2));
        assertTrue(excluded);

        excluded = strategy.isExcluded(availability, new PracticeDto(), atDate.plusDays(3));
        assertFalse(excluded);
    }

    private Availability availabilityWith(Instant begin, int duration, LocalDate ...excluded) {
        Availability availability = new Availability();
        availability.setBegin(begin);
        availability.setEnd(begin.plus(duration, ChronoUnit.MINUTES));
        availability.getExcludedDays().addAll(Arrays.stream(excluded)
            .map(LocalDate::atStartOfDay)
            .map(dt -> dt.toInstant(ZoneOffset.UTC))
            .collect(Collectors.toSet()));

        return availability;
    }
}
