package com.arzttermine.service.calendar.config;

import com.arzttermine.service.profile.security.service.TokenCache;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;

@Configuration
@Profile("testing")
public class RemotetokenServicesTestConfiguration extends ResourceServerConfiguration
{
    @Bean
    public RemoteTokenServices remoteTokenServices()
    {
        return Mockito.mock(RemoteTokenServices.class);
    }

    @Bean
    public TokenCache tokenCache()
    {
        return Mockito.mock(TokenCache.class);
    }
}
