package com.arzttermine.service.calendar.domain.entity;

import com.arzttermine.service.calendar.web.struct.RecurringType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.Instant;
import java.time.OffsetDateTime;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class AvailabilityTest
{
    @Test
    public void isAppointmentOutsideOfRange_should_return_true_for_appointments_outside_recurring_config()
    {
        OffsetDateTime now = OffsetDateTime.now();

        Appointment appointment = new Appointment();
        appointment.setStart(now.plusDays(20).toInstant());
        appointment.setEnd(appointment.getStart().plusSeconds(3600));

        Availability availability = new Availability();
        availability.setRecurringConfig(new RecurringConfiguration(RecurringType.WEEKLY));
        availability.getRecurringConfig().setEndsAt(now.plusDays(18).toInstant());

        assertTrue(availability.isAppointmentOutsideOfRange(appointment));
    }

    @Test
    public void isAppointmentOutsideOfRange_should_return_true_for_appointments_recurring_availability_and_before_begin()
    {
        OffsetDateTime now = OffsetDateTime.now();

        Appointment appointment = new Appointment();
        appointment.setStart(now.plusDays(20).toInstant());
        appointment.setEnd(appointment.getStart().plusSeconds(3600));

        Availability availability = new Availability();
        availability.setRecurringConfig(new RecurringConfiguration(RecurringType.WEEKLY));
        availability.setBegin(now.plusDays(30).toInstant());

        assertTrue(availability.isAppointmentOutsideOfRange(appointment));
    }

    @Test
    public void isAppointmentOutsideOfRange_should_return_true_for_appointments_outside_non_recurring_availability_range()
    {
        OffsetDateTime now = OffsetDateTime.now();

        Appointment appointment = new Appointment();
        appointment.setStart(now.minusHours(2).toInstant());
        appointment.setEnd(appointment.getStart().plusSeconds(3600));

        Availability availability = new Availability();
        availability.setBegin(now.toInstant());
        availability.setEnd(now.plusHours(1).toInstant());

        assertTrue(availability.isAppointmentOutsideOfRange(appointment));
    }

    @Test
    public void isAppointmentOutsideOfRange_should_return_true_for_appointments_ending_after_availability()
    {
        OffsetDateTime now = OffsetDateTime.now();

        Appointment appointment = new Appointment();
        appointment.setStart(now.plusMinutes(40).toInstant());
        appointment.setEnd(appointment.getStart().plusSeconds(3600));

        Availability availability = new Availability();
        availability.setBegin(now.toInstant());
        availability.setEnd(now.plusHours(1).toInstant());

        assertTrue(availability.isAppointmentOutsideOfRange(appointment));
    }

    @Test
    public void isAppointmentOutsideOfRange_should_return_false_for_appointment_in_basic_recurring_availability_range()
    {
        OffsetDateTime now = OffsetDateTime.now();

        Appointment appointment = new Appointment();
        appointment.setStart(now.plusDays(40).toInstant());
        appointment.setEnd(appointment.getStart().plusSeconds(3600));

        Availability availability = new Availability();
        availability.setRecurringConfig(new RecurringConfiguration(RecurringType.WEEKLY));
        availability.getRecurringConfig().setEndsAt(now.plusDays(50).toInstant());
        availability.setBegin(now.toInstant());
        availability.setEnd(now.plusHours(1).toInstant());

        assertFalse(availability.isAppointmentOutsideOfRange(appointment));
    }

    @Test
    public void isAppointmentOutsideOfRange_should_return_false_for_appointment_within_non_recurring_availability_range()
    {
        OffsetDateTime now = OffsetDateTime.now();

        Appointment appointment = new Appointment();
        appointment.setStart(now.plusMinutes(40).toInstant());
        appointment.setEnd(appointment.getStart().plusSeconds(3600));

        Availability availability = new Availability();
        availability.setBegin(now.toInstant());
        availability.setEnd(now.plusHours(2).toInstant());

        assertFalse(availability.isAppointmentOutsideOfRange(appointment));
    }

    @Test
    public void isRecurring_returns_true_for_recurring_availability()
    {
        Availability availability = new Availability();
        availability.setRecurringConfig(new RecurringConfiguration(RecurringType.WEEKLY));

        assertTrue(availability.isRecurring());
    }

    @Test
    public void isRecurring_returns_false_for_never_or_non_recurring_availability()
    {
        Availability availability = new Availability();
        assertFalse(availability.isRecurring());

        availability.setRecurringConfig(new RecurringConfiguration(RecurringType.NEVER));
        assertFalse(availability.isRecurring());
    }
}
