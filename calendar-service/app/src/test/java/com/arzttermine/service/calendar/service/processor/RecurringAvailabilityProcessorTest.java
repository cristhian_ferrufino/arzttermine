package com.arzttermine.service.calendar.service.processor;

import com.arzttermine.service.calendar.domain.entity.AppointmentResource;
import com.arzttermine.service.calendar.domain.entity.Availability;
import com.arzttermine.service.calendar.domain.entity.RecurringConfiguration;
import com.arzttermine.service.calendar.service.mapper.AvailabilityMapper;
import com.arzttermine.service.calendar.web.dto.request.PlannedAvailabilitiesRequestDto;
import com.arzttermine.service.calendar.web.dto.response.AvailabilityTimeFrameDto;
import com.arzttermine.service.calendar.web.struct.RecurringType;
import com.google.common.collect.Sets;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class RecurringAvailabilityProcessorTest
{
    @Spy
    private AvailabilityMapper mapper = new AvailabilityMapper();
    
    @InjectMocks
    private RecurringAvailabilityProcessor processor;
    
    @Test
    public void searchTimeFramesIn_should_add_frames_for_weekly_recurring_availabilities()
    {
        OffsetDateTime tuesday = LocalDateTime.now().minusMonths(1).with(DayOfWeek.TUESDAY).atOffset(ZoneOffset.UTC);

        Availability availability = availability(tuesday.toInstant(), new RecurringConfiguration(RecurringType.WEEKLY), 42L, 24L);
        List<Availability> availabilities = new ArrayList<>();
        availabilities.add(availability);

        PlannedAvailabilitiesRequestDto request = new PlannedAvailabilitiesRequestDto();
        request.setStart(Date.from(LocalDateTime.now().with(DayOfWeek.MONDAY).toInstant(ZoneOffset.UTC)));
        request.setEnd(Date.from(LocalDateTime.now().with(DayOfWeek.SUNDAY).toInstant(ZoneOffset.UTC)));
        request.setResourceIds(Sets.newHashSet(42L));
        request.setPracticeId(42L);

        Map<Long, List<AvailabilityTimeFrameDto>> frames = processor.searchTimeFramesIn(availabilities, request);

        assertEquals(1, frames.size());
        assertFalse(frames.containsKey(24L)); // second resource wasn't requested
        assertEquals(42L, frames.keySet().iterator().next().longValue());
        assertEquals(1, frames.get(42L).size());

        OffsetDateTime desiredStart = LocalDateTime.now().with(DayOfWeek.TUESDAY)
            .atOffset(ZoneOffset.UTC)
            .withHour(tuesday.getHour())
            .withMinute(tuesday.getMinute())
            .withSecond(0)
            .withNano(0);

        assertEquals(desiredStart.toInstant(), frames.get(42L).get(0).getBegin().toInstant());
        assertEquals(desiredStart.plus(30, ChronoUnit.MINUTES).toInstant(), frames.get(42L).get(0).getEnd().toInstant());
    }

    @Test
    public void searchTimeFramesIn_should_add_frames_for_monthly_recurring_availabilities()
    {
        OffsetDateTime tuesday = LocalDateTime.now().minusMonths(3).atOffset(ZoneOffset.UTC).with(DayOfWeek.TUESDAY);

        Availability availability = availability(tuesday.toInstant(), new RecurringConfiguration(RecurringType.MONTHLY), 42L);
        List<Availability> availabilities = new ArrayList<>();
        availabilities.add(availability);

        OffsetDateTime searchStart = LocalDateTime.now().with(DayOfWeek.MONDAY).minusWeeks(3).atOffset(ZoneOffset.UTC);

        PlannedAvailabilitiesRequestDto request = new PlannedAvailabilitiesRequestDto();
        request.setStart(Date.from(searchStart.toInstant()));
        request.setEnd(Date.from(LocalDateTime.now().with(DayOfWeek.SUNDAY).plusWeeks(2).toInstant(ZoneOffset.UTC)));
        request.setResourceIds(Sets.newHashSet(42L));
        request.setPracticeId(42L);

        Map<Long, List<AvailabilityTimeFrameDto>> frames = processor.searchTimeFramesIn(availabilities, request);
        assertEquals(1, frames.size());

        OffsetDateTime desiredStart = searchStart
            .withDayOfMonth(tuesday.getDayOfMonth())
            .withHour(tuesday.getHour())
            .withMinute(tuesday.getMinute())
            .withSecond(0)
            .withNano(0);

        // we need to move a month to the future to get the next monthly event
        if (searchStart.getDayOfMonth() > desiredStart.getDayOfMonth()) {
            desiredStart = desiredStart.plusMonths(1);
        }

        assertEquals(1, frames.get(42L).size());
        assertEquals(desiredStart.toInstant(), frames.get(42L).get(0).getBegin().toInstant());
        assertEquals(desiredStart.plus(30, ChronoUnit.MINUTES).toInstant(), frames.get(42L).get(0).getEnd().toInstant());
    }

    @Test
    public void searchTimeFramesIn_should_not_add_frame_for_monthly_recurring_availability_if_outside_of_requested_time_range()
    {
        OffsetDateTime tuesday = LocalDateTime.now().minusMonths(3).withDayOfMonth(1).atOffset(ZoneOffset.UTC);

        Availability availability = availability(tuesday.toInstant(), new RecurringConfiguration(RecurringType.MONTHLY), 42L);
        List<Availability> availabilities = new ArrayList<>();
        availabilities.add(availability);

        PlannedAvailabilitiesRequestDto request = new PlannedAvailabilitiesRequestDto();
        request.setStart(Date.from(LocalDateTime.now().withDayOfMonth(10).toInstant(ZoneOffset.UTC)));
        request.setEnd(Date.from(LocalDateTime.now().withDayOfMonth(17).toInstant(ZoneOffset.UTC)));
        request.setResourceIds(Sets.newHashSet(42L));
        request.setPracticeId(42L);

        Map<Long, List<AvailabilityTimeFrameDto>> frames = processor.searchTimeFramesIn(availabilities, request);

        assertTrue(frames.isEmpty());
    }

    @Test
    public void searchTimeFramesIn_should_return_non_recurring_availabilities()
    {
        Instant now = Instant.now();

        Availability availability = availability(now, null, 42L);
        List<Availability> availabilities = new ArrayList<>();
        availabilities.add(availability);

        PlannedAvailabilitiesRequestDto request = new PlannedAvailabilitiesRequestDto();
        request.setStart(Date.from(LocalDateTime.now().withDayOfMonth(10).toInstant(ZoneOffset.UTC)));
        request.setEnd(Date.from(LocalDateTime.now().withDayOfMonth(17).toInstant(ZoneOffset.UTC)));
        request.setResourceIds(Sets.newHashSet(42L));
        request.setPracticeId(42L);

        Map<Long, List<AvailabilityTimeFrameDto>> frames = processor.searchTimeFramesIn(availabilities, request);

        assertEquals(1, frames.size());
        assertEquals(now, frames.get(42L).get(0).getBegin().toInstant());
    }

    @Test
    public void searchTimeFramesIn_should_add_daily_frames()
    {
        OffsetDateTime aDay = LocalDateTime.now().minusDays(40).atOffset(ZoneOffset.UTC);

        Availability availability = availability(aDay.toInstant(), new RecurringConfiguration(RecurringType.DAILY), 42L);
        List<Availability> availabilities = new ArrayList<>();
        availabilities.add(availability);

        LocalDateTime begin = LocalDateTime.now().with(DayOfWeek.MONDAY);

        PlannedAvailabilitiesRequestDto request = new PlannedAvailabilitiesRequestDto();
        request.setStart(Date.from(begin.toInstant(ZoneOffset.UTC)));
        request.setEnd(Date.from(LocalDateTime.now().with(DayOfWeek.WEDNESDAY).toInstant(ZoneOffset.UTC)));
        request.setResourceIds(Sets.newHashSet(42L));
        request.setPracticeId(42L);

        Map<Long, List<AvailabilityTimeFrameDto>> frames = processor.searchTimeFramesIn(availabilities, request);

        assertEquals(1, frames.size());
        assertEquals(3, frames.get(42L).size());

        OffsetDateTime desiredStart = begin
            .atOffset(ZoneOffset.UTC)
            .withHour(aDay.getHour())
            .withMinute(aDay.getMinute())
            .withSecond(0)
            .withNano(0);

        assertEquals(desiredStart.toInstant(), frames.get(42L).get(0).getBegin().toInstant());
        assertEquals(desiredStart.plusDays(1).toInstant(), frames.get(42L).get(1).getBegin().toInstant());
        assertEquals(desiredStart.plusDays(2).toInstant(), frames.get(42L).get(2).getBegin().toInstant());
    }

    private Availability availability(Instant begin, RecurringConfiguration configuration, Long... resourceIds) {
        final Availability availability = new Availability();
        availability.setBegin(begin);
        availability.setEnd(begin.plus(30, ChronoUnit.MINUTES));
        availability.setRecurringConfig(configuration);
        availability.setDuration(30);

        availability.getResources().addAll(Arrays.stream(resourceIds)
            .map(rid -> new AppointmentResource(rid, true, begin, availability.getEnd()))
            .collect(Collectors.toList()));

        return availability;
    }
}
