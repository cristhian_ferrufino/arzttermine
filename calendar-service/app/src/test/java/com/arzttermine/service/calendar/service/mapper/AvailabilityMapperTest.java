package com.arzttermine.service.calendar.service.mapper;

import com.arzttermine.service.calendar.domain.entity.Availability;
import com.arzttermine.service.calendar.domain.entity.RecurringConfiguration;
import com.arzttermine.service.calendar.web.dto.response.AvailabilityTimeFrameDto;
import com.arzttermine.service.calendar.web.struct.RecurringType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class AvailabilityMapperTest
{
    @InjectMocks
    private AvailabilityMapper mapper;

    @Test
    public void toDto_should_set_weekly_recurring_config_if_recurring_is_activated()
    {
        Instant begin = Instant.now();

        RecurringConfiguration config = new RecurringConfiguration(RecurringType.WEEKLY);
        config.setEndsAt(begin.atOffset(ZoneOffset.UTC).plusDays(2).withHour(0).withMinute(0).withSecond(0).withNano(0).toInstant());

        Availability availability = new Availability();
        availability.setRecurringConfig(config);
        availability.setEnd(begin.plus(1, ChronoUnit.HOURS));
        availability.setBegin(begin);

        AvailabilityTimeFrameDto frame = mapper.toDto(availability);

        assertNotNull(frame.getRecurringConfig());
        assertEquals(RecurringType.WEEKLY, frame.getRecurringConfig().getType());
        assertEquals(config.getEndsAt(), frame.getRecurringConfig().getEndsAt().atStartOfDay().toInstant(ZoneOffset.UTC));
        assertEquals(LocalDateTime.from(begin.atZone(ZoneId.systemDefault())).getDayOfWeek(), frame.getRecurringConfig().getDayOfWeek());
    }

    @Test
    public void toDto_should_set_monthly_recurring_config_if_recurring_is_activated()
    {
        Instant begin = Instant.now();

        RecurringConfiguration config = new RecurringConfiguration(RecurringType.MONTHLY);

        Availability availability = new Availability();
        availability.setRecurringConfig(config);
        availability.setEnd(begin.plus(1, ChronoUnit.HOURS));
        availability.setBegin(begin);

        AvailabilityTimeFrameDto frame = mapper.toDto(availability);

        assertNotNull(frame.getRecurringConfig());
        assertNull(frame.getRecurringConfig().getEndsAt());
        assertEquals(RecurringType.MONTHLY, frame.getRecurringConfig().getType());
        assertEquals(LocalDateTime.from(begin.atZone(ZoneId.systemDefault())).getDayOfMonth(), frame.getRecurringConfig().getDateOfMonth().intValue());
    }
}
