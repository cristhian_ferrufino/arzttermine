package com.arzttermine.service.calendar.web.controller;

import com.arzttermine.library.elasticsearch.model.BulkAction;
import com.arzttermine.library.elasticsearch.service.DocumentProcessor;
import com.arzttermine.library.messaging.processor.MessageBus;
import com.arzttermine.library.suport.DateFormatterUtil;
import com.arzttermine.library.web.struct.InsuranceType;
import com.arzttermine.service.booking.messages.AppointmentBookedMessage;
import com.arzttermine.service.calendar.service.AvailabilityServiceTest;
import com.arzttermine.service.calendar.service.externals.PracticeApi;
import com.arzttermine.service.calendar.service.processor.transformer.AvailabilityIterator;
import com.arzttermine.service.calendar.web.BaseIntegrationTest;
import com.arzttermine.service.calendar.web.dto.RecurringConfigDto;
import com.arzttermine.service.calendar.web.dto.request.CreateAvailabilitiesConfigurationDto;
import com.arzttermine.service.calendar.web.dto.request.CreateAvailabilityDto;
import com.arzttermine.service.calendar.web.dto.request.DeleteAvailabilityDto;
import com.arzttermine.service.calendar.web.dto.request.PlannedAvailabilitiesRequestDto;
import com.arzttermine.service.calendar.web.dto.request.UpdateAvailabilitiesConfigurationDto;
import com.arzttermine.service.calendar.web.dto.response.AppointmentDto;
import com.arzttermine.service.calendar.web.dto.response.AvailabilityTimeFrameDto;
import com.arzttermine.service.calendar.web.dto.response.paging.AppointmentsPageDto;
import com.arzttermine.service.calendar.web.struct.RecurringType;
import com.arzttermine.service.profile.web.dto.response.DoctorProfileDto;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import com.arzttermine.service.profile.web.dto.response.ResourceDto;
import com.arzttermine.service.profile.web.dto.response.TreatmentTypeDto;
import com.arzttermine.service.profile.web.struct.ResourceType;
import com.google.common.collect.Sets;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class AvailabilityControllerTest extends BaseIntegrationTest
{
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    private static PracticeDto practice;

    @Autowired
    private PracticeApi practiceClient;

    @Autowired
    private MessageBus messageBus;

    @Autowired
    private DocumentProcessor documentProcessor;

    @Before
    public void setup() {
        ResourceDto resource = new ResourceDto();
        resource.setType(ResourceType.DOCTOR);
        resource.setSystemId(23L);
        resource.setId(12345L);

        TreatmentTypeDto tt = new TreatmentTypeDto();
        tt.setDoctorIds(Collections.singletonList(23L));
        tt.setType("example");
        tt.setId(333L);

        practice = new PracticeDto();
        practice.setResources(Collections.singletonList(resource));
        practice.setName("practice example");
        practice.setId(42L);

        DoctorProfileDto profile = new DoctorProfileDto();
        profile.setProfileId(99L);
        profile.setId(23L);
        practice.getDoctors().add(profile);
        practice.getTreatmentTypes().add(tt);

        Map<Long, PracticeDto> practiceMap = new HashMap<>();
        practiceMap.put(42L, practice);

        when(practiceClient.findByIds(any())).thenReturn(practiceMap);
        when(practiceClient.findById(eq(42L))).thenReturn(practice);
    }

    @After
    public void teardown() throws Exception {
        stopRunningThreads();

        // in this test we create a lot of appointments asynchronously so lets wait for them before running empty_database.sql
        Thread.sleep(200);

        reset(documentProcessor);
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void createAvailability_should_create_and_return_availability() throws Exception
    {
        CreateAvailabilityDto request = AvailabilityServiceTest.buildRequestDto(Instant.now().plus(1, ChronoUnit.DAYS));

        mockMvc.perform(systemRequest(() -> post("/availabilities"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(createRequestDto(request))))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$[0].id", notNullValue()))
            .andExpect(jsonPath("$[0].begin", is(DateFormatterUtil.format(request.getFrom()))))
            .andExpect(jsonPath("$[0].end", is(DateFormatterUtil.format(request.getTo()))));
    }

    @Test
    public void createAvailability_should_return_bad_request_for_invalid_dates() throws Exception
    {
        CreateAvailabilityDto request = AvailabilityServiceTest.buildRequestDto(Instant.now().plus(1, ChronoUnit.DAYS));
        request.setTo(request.getFrom());

        mockMvc.perform(systemRequest(() -> post("/availabilities"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(createRequestDto(request))))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$[0].code", is("INVALID_ARGUMENT")))
            .andExpect(jsonPath("$[0].reference", is("requests[0].fromBeforeTo")));

        request.setTo(Date.from(request.getFrom().toInstant().minusSeconds(360)));

        mockMvc.perform(systemRequest(() -> post("/availabilities"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(createRequestDto(request))))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$[0].code", is("INVALID_ARGUMENT")))
            .andExpect(jsonPath("$[0].reference", is("requests[0].fromBeforeTo")));
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void createAvailabilities_should_merge_overlapping_availabilities() throws Exception
    {
        Instant begin = Instant.now().plus(11, ChronoUnit.DAYS);
        CreateAvailabilityDto request = AvailabilityServiceTest.buildRequestDto(begin);

        mockMvc.perform(systemRequest(() -> post("/availabilities"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(createRequestDto(request))))
            .andExpect(status().isCreated());

        Instant finalEnd = request.getTo().toInstant().plus(request.getDuration(), ChronoUnit.MINUTES);
        request.setFrom(request.getTo());
        request.setTo(Date.from(finalEnd));

        Thread.sleep(100);

        mockMvc.perform(systemRequest(() -> post("/availabilities"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(createRequestDto(request))))
            .andExpect(status().isCreated());

        Instant newStart = begin.minus(request.getDuration(), ChronoUnit.MINUTES);
        request.setFrom(Date.from(newStart));
        request.setTo(Date.from(begin));

        Thread.sleep(100);

        mockMvc.perform(systemRequest(() -> post("/availabilities"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(createRequestDto(request))))
            .andExpect(status().isCreated());

        mockMvc.perform(systemRequest(() -> get("/availabilities?practiceId=" + request.getPracticeId())))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$.12345.[*]", hasSize(1)))
            .andExpect(jsonPath("$.12345.[0].begin", is(DateFormatterUtil.format(newStart))))
            .andExpect(jsonPath("$.12345.[0].end", is(DateFormatterUtil.format(finalEnd))));
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void createAvailabilities_should_report_invalid_range() throws Exception
    {
        Instant begin = Instant.now().atOffset(ZoneOffset.UTC).plusWeeks(1).with(DayOfWeek.MONDAY).toInstant();
        CreateAvailabilityDto request = AvailabilityServiceTest.buildRequestDto(begin, 3);

        mockMvc.perform(systemRequest(() -> post("/availabilities"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(createRequestDto(request))))
            .andExpect(status().isCreated());

        Instant newEnd = request.getTo().toInstant().plus(3 * request.getDuration(), ChronoUnit.MINUTES);
        request.setFrom(Date.from(begin.plus(request.getDuration(), ChronoUnit.MINUTES)));
        request.setTo(Date.from(newEnd));

        mockMvc.perform(systemRequest(() -> post("/availabilities"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(createRequestDto(request))))
            .andExpect(status().isConflict())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$[0].code", is("RANGE_ALREADY_BLOCKED")));

        request.setFrom(Date.from(begin.minus(request.getDuration(), ChronoUnit.MINUTES)));
        request.setTo(Date.from(begin.plus(2 * request.getDuration(), ChronoUnit.MINUTES)));

        mockMvc.perform(systemRequest(() -> post("/availabilities"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(createRequestDto(request))))
            .andExpect(status().isConflict())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$[0].code", is("RANGE_ALREADY_BLOCKED")));

        request.setFrom(Date.from(begin.plus(request.getDuration(), ChronoUnit.MINUTES)));
        request.setTo(Date.from(begin.plus(2 * request.getDuration(), ChronoUnit.MINUTES)));

        mockMvc.perform(systemRequest(() -> post("/availabilities"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(createRequestDto(request))))
            .andExpect(status().isConflict())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$[0].code", is("RANGE_ALREADY_BLOCKED")));
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void updateAvailabilities_should_concat_availabilities_and_trigger_appointment_updates() throws Exception
    {
        reset(documentProcessor);

        Instant initial = Instant.now().atOffset(ZoneOffset.UTC).plusDays(20).with(DayOfWeek.TUESDAY).withHour(10).toInstant();
        CreateAvailabilityDto request = AvailabilityServiceTest.buildRequestDto(initial, 2);

        AvailabilityTimeFrameDto frame = objectMapper.readValue(mockMvc.perform(systemRequest(() -> post("/availabilities"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(createRequestDto(request))))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), AvailabilityTimeFrameDto[].class)[0];

        CreateAvailabilityDto anotherOne = AvailabilityServiceTest.buildRequestDto(initial.plus(4 * request.getDuration(), ChronoUnit.MINUTES));
        Instant finalEnd = anotherOne.getTo().toInstant();

        mockMvc.perform(systemRequest(() -> post("/availabilities"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(createRequestDto(anotherOne))))
            .andExpect(status().isCreated());

        Thread.sleep(150);

        mockMvc.perform(systemRequest(() -> get("/appointments?start=" + initial.atOffset(ZoneOffset.UTC).minusDays(1).toString())))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.elements[*]", hasSize(3)));

        mockMvc.perform(systemRequest(() -> get("/availabilities?practiceId=" + request.getPracticeId())))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$.12345.[*]", hasSize(2)));

        request.setFrom(Date.from(initial)); // original start of availability
        request.setTo(anotherOne.getFrom()); // start of second availability, adds 2 availabilities and combines 3 availabilities in one

        verify(documentProcessor, atLeast(2)).executeAsync(eq("appointments"), eq("practice-42"), anyCollection());
        verify(documentProcessor, never()).executeAsync(eq("appointments"), eq(null), anyCollection());
        reset(documentProcessor);

        doAnswer(invocation -> {
            List<BulkAction> actions = invocation.getArgumentAt(2, List.class);
            Assert.assertEquals(1, actions.size()); // only the one from the last availability gets removed
            return null;
        }).when(documentProcessor).executeAsync(eq("appointments"), eq(null), anyCollection());

        mockMvc.perform(systemRequest(() -> patch("/availabilities"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(createUpdateDto(frame.getId(), request))))
            .andExpect(status().isOk());

        mockMvc.perform(systemRequest(() -> get("/availabilities?practiceId=" + request.getPracticeId())))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$.12345.[*]", hasSize(1)));

        Thread.sleep(200);

        mockMvc.perform(systemRequest(() -> get("/appointments?sort=start,asc")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.elements[*]", hasSize(5)))
            .andExpect(jsonPath("$.elements[0].start", is(DateFormatterUtil.format(initial))))
            .andExpect(jsonPath("$.elements[0].end", is(DateFormatterUtil.format(initial.plus(request.getDuration(), ChronoUnit.MINUTES)))))
            .andExpect(jsonPath("$.elements[4].start", is(DateFormatterUtil.format(finalEnd.minus(request.getDuration(), ChronoUnit.MINUTES)))))
            .andExpect(jsonPath("$.elements[4].end", is(DateFormatterUtil.format(finalEnd))));

        verify(documentProcessor, times(1)).executeAsync(eq("appointments"), eq("practice-42"), anyCollection());
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void updateAvailabilities_should_disable_recurring_event_except_for_the_requested_date() throws Exception
    {
        reset(documentProcessor);

        Instant initial = Instant.now().plus(1, ChronoUnit.DAYS);
        CreateAvailabilityDto request = AvailabilityServiceTest.buildRequestDto(initial, 1);

        RecurringConfigDto recurringConfig = new RecurringConfigDto();
        recurringConfig.setType(RecurringType.WEEKLY);

        CreateAvailabilitiesConfigurationDto dto = createRequestDto(request);
        dto.setRecurringConfig(recurringConfig);

        AvailabilityTimeFrameDto frame = objectMapper.readValue(mockMvc.perform(systemRequest(() -> post("/availabilities"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(dto)))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), AvailabilityTimeFrameDto[].class)[0];

        Thread.sleep(25);

        PlannedAvailabilitiesRequestDto search = new PlannedAvailabilitiesRequestDto();
        search.setStart(Date.from(initial.minus(1, ChronoUnit.DAYS)));
        search.setEnd(Date.from(initial.plus(16, ChronoUnit.DAYS)));
        search.setResourceIds(Sets.newHashSet(12345L));
        search.setPracticeId(42L);

        mockMvc.perform(doctorRequest(42L, () -> post("/availabilities/search"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(search)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$.12345.[*]", hasSize(3)))
            .andExpect(jsonPath("$.12345.[0].begin", is(DateFormatterUtil.format(initial))))
            .andExpect(jsonPath("$.12345.[1].begin", is(DateFormatterUtil.format(initial.plus(7, ChronoUnit.DAYS)))))
            .andExpect(jsonPath("$.12345.[2].begin", is(DateFormatterUtil.format(initial.plus(14, ChronoUnit.DAYS)))));

        UpdateAvailabilitiesConfigurationDto update = createUpdateDto(frame.getId(), request);
        OffsetDateTime newAvailabilityStartDate = initial.plus(7, ChronoUnit.DAYS).atOffset(ZoneOffset.UTC);

        update.setDisableRecurringAt(newAvailabilityStartDate.toLocalDate());
        update.setRecurringConfig(null);

        mockMvc.perform(systemRequest(() -> patch("/availabilities"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(update)))
            .andExpect(status().isOk());

        // behind the scenes: merge of availability is happening
        Thread.sleep(225);

        mockMvc.perform(doctorRequest(42L, () -> post("/availabilities/search"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(search)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$.12345.[*]", hasSize(1)))
            .andExpect(jsonPath("$.12345.[0].begin", is(DateFormatterUtil.format(newAvailabilityStartDate.toInstant()))));
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void updateAvailabilities_should_remove_appointments_when_setting_previous_recurring_end() throws Exception
    {
        Instant initial = Instant.now().plus(1, ChronoUnit.DAYS);
        int weeks = ((int) Duration.between(initial.atOffset(ZoneOffset.UTC), initial.atOffset(ZoneOffset.UTC).plusMonths(3)).toDays() / 7) +1;

        CreateAvailabilityDto request = AvailabilityServiceTest.buildRequestDto(initial, 2);

        RecurringConfigDto recurringConfig = new RecurringConfigDto();
        recurringConfig.setType(RecurringType.WEEKLY);

        CreateAvailabilitiesConfigurationDto dto = createRequestDto(request);
        dto.setRecurringConfig(recurringConfig);

        AvailabilityTimeFrameDto frame = objectMapper.readValue(mockMvc.perform(systemRequest(() -> post("/availabilities"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(dto)))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), AvailabilityTimeFrameDto[].class)[0];

        Thread.sleep(280);

        mockMvc.perform(systemRequest(() -> get("/appointments")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.page.totalItems", is(2 * weeks)))
            .andExpect(jsonPath("$.elements[*]", hasSize(20)));

        OffsetDateTime recurringEnd = initial.atOffset(ZoneOffset.UTC).plusMonths(1);
        int weeksTillRecurringEnd = ((int) Duration.between(initial.atOffset(ZoneOffset.UTC), recurringEnd).toDays() / 7) + 1;

        recurringConfig.setEndsAt(recurringEnd.toLocalDate());

        UpdateAvailabilitiesConfigurationDto update = createUpdateDto(frame.getId(), request);
        update.setRecurringConfig(recurringConfig);

        mockMvc.perform(systemRequest(() -> patch("/availabilities"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(update)))
            .andExpect(status().isOk());

        Thread.sleep(250);

        mockMvc.perform(systemRequest(() -> get("/appointments")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.page.totalItems", is(2 * weeksTillRecurringEnd)))
            .andExpect(jsonPath("$.elements[*]", hasSize((weeksTillRecurringEnd > 9) ? 20 : 2 * weeksTillRecurringEnd)));
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void getAvailabilities_should_return_availabilities() throws Exception
    {
        CreateAvailabilityDto request = AvailabilityServiceTest.buildRequestDto(Instant.now().plus(1, ChronoUnit.DAYS));

        mockMvc.perform(systemRequest(() -> post("/availabilities"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(createRequestDto(request))))
            .andExpect(status().isCreated());

        mockMvc.perform(systemRequest(() -> get("/availabilities?practiceId=" + request.getPracticeId())))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$.12345.[*]", hasSize(1)))
            .andExpect(jsonPath("$.12345.[0].id", notNullValue()))
            .andExpect(jsonPath("$.12345.[0].duration", is(request.getDuration())))
            .andExpect(jsonPath("$.12345.[0].begin", is(DateFormatterUtil.format(request.getFrom()))))
            .andExpect(jsonPath("$.12345.[0].end", is(DateFormatterUtil.format(request.getTo()))))
            .andExpect(jsonPath("$.12345.[0].treatmentTypes[*]", hasSize(1)))
            .andExpect(jsonPath("$.12345.[0].treatmentTypes[0]", is(333)));
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void deleteAvailability_should_report_booked_appointments() throws Exception
    {
        CreateAvailabilityDto request = AvailabilityServiceTest.buildRequestDto(Instant.now()
            .atOffset(ZoneOffset.UTC).plusWeeks(1).with(DayOfWeek.WEDNESDAY).toInstant(), 2);

        AvailabilityTimeFrameDto[] frames = objectMapper.readValue(mockMvc.perform(systemRequest(() -> post("/availabilities"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(createRequestDto(request))))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), AvailabilityTimeFrameDto[].class);

        mockMvc.perform(systemRequest(() -> get("/availabilities?practiceId=" + request.getPracticeId())))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$.12345.[*]", hasSize(1)))
            .andExpect(jsonPath("$.12345.[0].id", notNullValue()))
            .andExpect(jsonPath("$.12345.[0].duration", is(request.getDuration())))
            .andExpect(jsonPath("$.12345.[0].begin", is(DateFormatterUtil.format(request.getFrom()))))
            .andExpect(jsonPath("$.12345.[0].end", is(DateFormatterUtil.format(request.getTo()))));

        Thread.sleep(50);

        List<Long> appointmentIds = objectMapper.readValue(mockMvc.perform(systemRequest(() -> get("/appointments?practiceId=" + request.getPracticeId())))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.elements[*]", hasSize(2)))
            .andReturn()
            .getResponse()
            .getContentAsString(), AppointmentsPageDto.class)
            .getElements()
            .stream()
            .map(AppointmentDto::getId)
            .collect(Collectors.toList());

        AppointmentBookedMessage b1 = new AppointmentBookedMessage();
        b1.setAppointmentId(appointmentIds.get(0));
        b1.setCustomerId(42L);

        AppointmentBookedMessage b2 = new AppointmentBookedMessage();
        b2.setAppointmentId(appointmentIds.get(1));
        b2.setCustomerId(42L);

        messageBus.send(b1);
        messageBus.send(b2);
        Thread.sleep(220);

        mockMvc.perform(systemRequest(() -> delete("/availabilities/" + frames[0].getId()))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(new DeleteAvailabilityDto())))
            .andExpect(status().isConflict())
            .andExpect(jsonPath("$[*]", hasSize(2)))
            .andExpect(jsonPath("$[0].code", is("REQUIRES_CANCELLATION")))
            .andExpect(jsonPath("$[1].code", is("REQUIRES_CANCELLATION")))
            .andExpect(jsonPath("$[*].reference", containsInAnyOrder(appointmentIds.get(0).intValue(), appointmentIds.get(1).intValue())));
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void deleteAvailability_should_delete_availability() throws Exception
    {
        CreateAvailabilityDto request = AvailabilityServiceTest.buildRequestDto(Instant.now().plus(42, ChronoUnit.DAYS), 1);

        AvailabilityTimeFrameDto dto = objectMapper.readValue(mockMvc.perform(systemRequest(() -> post("/availabilities"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(createRequestDto(request))))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), AvailabilityTimeFrameDto[].class)[0];

        mockMvc.perform(systemRequest(() -> get("/availabilities?practiceId=" + request.getPracticeId())))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$.12345.[*]", hasSize(1)))
            .andExpect(jsonPath("$.12345.[0].id", notNullValue()))
            .andExpect(jsonPath("$.12345.[0].duration", is(request.getDuration())))
            .andExpect(jsonPath("$.12345.[0].begin", is(DateFormatterUtil.format(request.getFrom()))))
            .andExpect(jsonPath("$.12345.[0].end", is(DateFormatterUtil.format(request.getTo()))))
            .andExpect(jsonPath("$.12345.[0].treatmentTypes[*]", containsInAnyOrder(333)));

        Thread.sleep(100);
        reset(documentProcessor);

        doAnswer(invocation -> {
            List<BulkAction> actions = invocation.getArgumentAt(2, List.class);
            Assert.assertEquals(2, actions.size());
            Assert.assertEquals(BulkAction.ActionType.DELETE, actions.get(0).getType());
            Assert.assertEquals(BulkAction.ActionType.DELETE, actions.get(1).getType());
            Assert.assertNotEquals(dto.getId(), actions.get(0).getDocument().getId());
            Assert.assertNotEquals(dto.getId(), actions.get(1).getDocument().getId());
            return null;
        }).when(documentProcessor).executeAsync(eq("appointments"), eq(null), anyCollection());

        mockMvc.perform(systemRequest(() -> delete("/availabilities/" + dto.getId()))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(new DeleteAvailabilityDto())))
            .andExpect(status().isNoContent());

        mockMvc.perform(systemRequest(() -> get("/availabilities?id=" + dto.getId())))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*]").isEmpty());

        Thread.sleep(100);
        verify(documentProcessor, times(1)).executeAsync(eq("appointments"), eq("practice-42"), anyCollection());
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void deleteAvailability_should_delete_only_single_day_availability_if_desired() throws Exception
    {
        RecurringConfigDto recurringConfig = new RecurringConfigDto();
        recurringConfig.setType(RecurringType.WEEKLY);

        Instant start = Instant.now().plus(30, ChronoUnit.DAYS);
        CreateAvailabilityDto request = AvailabilityServiceTest.buildRequestDto(start);
        CreateAvailabilitiesConfigurationDto list = createRequestDto(request);
        list.setRecurringConfig(recurringConfig);

        AvailabilityTimeFrameDto dto = objectMapper.readValue(mockMvc.perform(systemRequest(() -> post("/availabilities"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(list)))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), AvailabilityTimeFrameDto[].class)[0];

        mockMvc.perform(systemRequest(() -> get("/availabilities?practiceId=" + request.getPracticeId())))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$.12345.[*]", hasSize(1)))
            .andExpect(jsonPath("$.12345.[0].id", notNullValue()))
            .andExpect(jsonPath("$.12345.[0].duration", is(request.getDuration())))
            .andExpect(jsonPath("$.12345.[0].begin", is(DateFormatterUtil.format(request.getFrom()))))
            .andExpect(jsonPath("$.12345.[0].end", is(DateFormatterUtil.format(request.getTo()))))
            .andExpect(jsonPath("$.12345.[0].treatmentTypes[*]", containsInAnyOrder(333)))
            .andExpect(jsonPath("$.12345.[0].recurringConfig", notNullValue()))
            .andExpect(jsonPath("$.12345.[0].recurringConfig.type", is(RecurringType.WEEKLY.name())));

        Thread.sleep(50);
        reset(documentProcessor);

        DeleteAvailabilityDto delete = new DeleteAvailabilityDto();
        delete.setDate(start.atOffset(ZoneOffset.UTC).plusWeeks(1).toLocalDate());
        delete.setRemoveAll(false);

        mockMvc.perform(systemRequest(() -> delete("/availabilities/" + dto.getId()))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(delete)))
            .andExpect(status().isNoContent());

        PlannedAvailabilitiesRequestDto search = new PlannedAvailabilitiesRequestDto();
        search.setResourceIds(Sets.newHashSet(12345L));
        search.setPracticeId(42L);
        search.setStart(Date.from(start.minus(1, ChronoUnit.DAYS)));
        search.setEnd(Date.from(start.plus(18, ChronoUnit.DAYS)));

        mockMvc.perform(systemRequest(() -> post("/availabilities/search?sort=begin,desc"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(search)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$.12345.[*]", hasSize(2)))
            .andExpect(jsonPath("$.12345.[0].begin", is(DateFormatterUtil.format(start))))
            .andExpect(jsonPath("$.12345.[1].begin", is(DateFormatterUtil.format(start.atOffset(ZoneOffset.UTC).plusWeeks(2).toInstant()))));
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void getPlannedAvailabilities_should_return_recurring_availabilities() throws Exception
    {
        Instant start = Instant.now().plus(55, ChronoUnit.DAYS);
        Instant endsAt = Instant.now().plus(85, ChronoUnit.DAYS);

        RecurringConfigDto recurringConfig = new RecurringConfigDto();
        recurringConfig.setEndsAt(endsAt.atOffset(ZoneOffset.UTC).toLocalDate());
        recurringConfig.setType(RecurringType.DAILY);

        CreateAvailabilityDto createAvailability = AvailabilityServiceTest.buildRequestDto(start);
        CreateAvailabilitiesConfigurationDto request = createRequestDto(createAvailability);
        request.setRecurringConfig(recurringConfig);

        mockMvc.perform(systemRequest(() -> post("/availabilities"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated());

        PlannedAvailabilitiesRequestDto search = new PlannedAvailabilitiesRequestDto();
        search.setPracticeId(createAvailability.getPracticeId());
        search.setResourceIds(Sets.newHashSet(12345L));
        search.setStart(Date.from(start.minus(5, ChronoUnit.DAYS)));
        search.setEnd(Date.from(start.minus(1, ChronoUnit.DAYS)));

        // no availabilities are there before the initial start
        mockMvc.perform(systemRequest(() -> post("/availabilities/search"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(search)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*]", hasSize(0)));

        search.setStart(Date.from(start));
        search.setEnd(Date.from(start.plus(5, ChronoUnit.DAYS)));

        // all day in range
        mockMvc.perform(systemRequest(() -> post("/availabilities/search"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(search)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$.12345.[*]", hasSize(6)));

        search.setStart(Date.from(endsAt.minus(2, ChronoUnit.DAYS)));
        search.setEnd(Date.from(endsAt.plus(5, ChronoUnit.DAYS)));

        // overlapping between still active recurring config and end of recurring config (endsAt is exclusive)
        mockMvc.perform(systemRequest(() -> post("/availabilities/search"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(search)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$.12345.[*]", hasSize(2)));

        search.setStart(Date.from(endsAt.atOffset(ZoneOffset.UTC).plusDays(1).toInstant()));
        search.setEnd(Date.from(endsAt.plus(5, ChronoUnit.DAYS)));

        // recurring config already ended
        mockMvc.perform(systemRequest(() -> post("/availabilities/search"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(search)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*]", hasSize(0)));
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void getPlannedAvailabilities_should_return_monthly_recurring_availabilities() throws Exception
    {
        Instant start = Instant.now().plus(3, ChronoUnit.DAYS);

        RecurringConfigDto recurringConfig = new RecurringConfigDto();
        recurringConfig.setType(RecurringType.MONTHLY);

        CreateAvailabilityDto createAvailability = AvailabilityServiceTest.buildRequestDto(start);
        CreateAvailabilitiesConfigurationDto request = createRequestDto(createAvailability);
        request.setRecurringConfig(recurringConfig);

        mockMvc.perform(systemRequest(() -> post("/availabilities"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated());

        PlannedAvailabilitiesRequestDto search = new PlannedAvailabilitiesRequestDto();
        search.setPracticeId(createAvailability.getPracticeId());
        search.setResourceIds(Sets.newHashSet(12345L));
        search.setStart(Date.from(start.minus(10, ChronoUnit.DAYS)));
        search.setEnd(Date.from(start.plus(10, ChronoUnit.DAYS)));

        mockMvc.perform(systemRequest(() -> post("/availabilities/search"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(search)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$.12345.[*]", hasSize(1)));
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void getPlannedAvailabilities_should_return_correct_insurance_types() throws Exception
    {
        Instant start = Instant.now().plus(5, ChronoUnit.DAYS);

        CreateAvailabilityDto createAvailability = AvailabilityServiceTest.buildRequestDto(start);
        createAvailability.getInsuranceTypes().add(InsuranceType.COMPULSORY);

        CreateAvailabilitiesConfigurationDto request = createRequestDto(createAvailability);

        mockMvc.perform(systemRequest(() -> post("/availabilities"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated());

        PlannedAvailabilitiesRequestDto search = new PlannedAvailabilitiesRequestDto();
        search.setPracticeId(createAvailability.getPracticeId());
        search.setResourceIds(Sets.newHashSet(12345L));
        search.setStart(Date.from(start.minus(10, ChronoUnit.DAYS)));
        search.setEnd(Date.from(start.plus(10, ChronoUnit.DAYS)));

        mockMvc.perform(systemRequest(() -> post("/availabilities/search"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(search)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$.12345.[*]", hasSize(1)))
            .andExpect(jsonPath("$.12345.[0].insuranceTypes[*]", containsInAnyOrder(InsuranceType.COMPULSORY.name())));
    }

    @Test
    @Sql("/db/empty_database.sql")
    public void getPlannedAvailabilities_should_return_recurring_and_non_recurring_events() throws Exception
    {
        Instant start = Instant.now().plus(3, ChronoUnit.DAYS);

        CreateAvailabilityDto createAvailability = AvailabilityServiceTest.buildRequestDto(start);
        CreateAvailabilitiesConfigurationDto request = createRequestDto(createAvailability);

        mockMvc.perform(systemRequest(() -> post("/availabilities"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated());

        RecurringConfigDto recurringConfig = new RecurringConfigDto();
        recurringConfig.setType(RecurringType.WEEKLY);

        Instant recurringStart = Instant.now().plusSeconds(360);
        request.getRequests().get(0).setFrom(Date.from(recurringStart));
        request.getRequests().get(0).setTo(Date.from(recurringStart.plus(1, ChronoUnit.HOURS)));
        request.getRequests().get(0).setDuration(60);
        request.setRecurringConfig(recurringConfig);

        mockMvc.perform(systemRequest(() -> post("/availabilities"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated());

        PlannedAvailabilitiesRequestDto search = new PlannedAvailabilitiesRequestDto();
        search.setPracticeId(createAvailability.getPracticeId());
        search.setResourceIds(Sets.newHashSet(12345L));
        search.setStart(Date.from(start.minus(6, ChronoUnit.DAYS)));
        search.setEnd(Date.from(recurringStart.plus(6, ChronoUnit.DAYS)));

        mockMvc.perform(systemRequest(() -> post("/availabilities/search?sort=begin,desc"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(search)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$.12345.[*]", hasSize(3)))
            .andExpect(jsonPath("$.12345.[0].begin", is(DateFormatterUtil.format(start))))
            .andExpect(jsonPath("$.12345.[0].end", is(DateFormatterUtil.format(start.plusSeconds(30 * 60)))))
            .andExpect(jsonPath("$.12345.[1].begin", is(DateFormatterUtil.format(recurringStart.atOffset(ZoneOffset.UTC).toInstant()))))
            .andExpect(jsonPath("$.12345.[1].end", is(DateFormatterUtil.format(recurringStart.atOffset(ZoneOffset.UTC).plusMinutes(60).toInstant()))))
            .andExpect(jsonPath("$.12345.[2].begin", is(DateFormatterUtil.format(recurringStart.atOffset(ZoneOffset.UTC).plusWeeks(1).toInstant()))))
            .andExpect(jsonPath("$.12345.[2].end", is(DateFormatterUtil.format(recurringStart.atOffset(ZoneOffset.UTC).plusWeeks(1).plusMinutes(60).toInstant()))));
    }

    private CreateAvailabilitiesConfigurationDto createRequestDto(CreateAvailabilityDto request) {
        CreateAvailabilitiesConfigurationDto config = new CreateAvailabilitiesConfigurationDto();
        config.getRequests().add(request);
        config.setRecurringConfig(null);

        return config;
    }

    private UpdateAvailabilitiesConfigurationDto createUpdateDto(long id, CreateAvailabilityDto request) {
        UpdateAvailabilitiesConfigurationDto config = new UpdateAvailabilitiesConfigurationDto();
        config.getUpdates().put(id, request);

        return config;
    }
}
