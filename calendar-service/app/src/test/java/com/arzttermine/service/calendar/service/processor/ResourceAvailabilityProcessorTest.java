package com.arzttermine.service.calendar.service.processor;

import com.arzttermine.library.suport.exception.ResponseErrorListException;
import com.arzttermine.library.web.dto.response.ErrorDto;
import com.arzttermine.service.calendar.domain.entity.Appointment;
import com.arzttermine.service.calendar.domain.repository.AppointmentResourceRepository;
import com.arzttermine.service.calendar.web.dto.request.ResourceRequestDto;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import com.arzttermine.service.profile.web.dto.response.ResourceDto;
import com.arzttermine.service.profile.web.struct.ResourceType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class ResourceAvailabilityProcessorTest
{
    @Mock
    private AppointmentResourceRepository resourceRepository;

    @InjectMocks
    private ResourceAvailabilityProcessor processor;

    @Test
    public void resolveAndAttachResources_should_do_nothing_when_no_resources_are_requested()
    {
        processor.resolveAndAttachResources(new Appointment(), new PracticeDto(), Collections.emptyList(), false);
    }

    @Test(expected = ResponseErrorListException.class)
    public void resolveAndAttachResources_should_report_resources_which_do_not_belong_to_practice()
    {
        ResourceRequestDto r1 = new ResourceRequestDto(123456789L);
        ResourceRequestDto r2 = new ResourceRequestDto(987654321L);
        ResourceRequestDto r3 = new ResourceRequestDto(42L);

        PracticeDto practice = new PracticeDto();
        ResourceDto resource = new ResourceDto();
        resource.setType(ResourceType.DOCTOR);
        resource.setSystemId(123L);
        resource.setId(42L);

        practice.setResources(Collections.singletonList(resource));

        try {
            processor.resolveAndAttachResources(new Appointment(), practice, Arrays.asList(r1, r2, r3), false);
        } catch (ResponseErrorListException errorList) {

            Iterator<ErrorDto> errors = errorList.getErrors().iterator();
            assertEquals(2, errorList.getErrors().size());
            assertEquals(123456789L, errors.next().getReference());
            assertEquals(987654321L, errors.next().getReference());

            throw errorList;
        }
    }

    @Test
    public void resolveAndAttachResources_should_not_report_anything_if_resource_attachment_was_forced()
    {
        ResourceRequestDto r1 = new ResourceRequestDto(42L);
        ResourceRequestDto r2 = new ResourceRequestDto(123L);

        ResourceDto resource = new ResourceDto();
        resource.setType(ResourceType.DOCTOR);
        resource.setSystemId(123L);
        resource.setId(42L);
        ResourceDto resource2 = new ResourceDto();
        resource2.setType(ResourceType.ASSISTANT);
        resource2.setId(123L);

        PracticeDto practice = new PracticeDto();
        practice.setResources(Arrays.asList(resource, resource2));

        when(resourceRepository.findOccupiedResources(any(), any(), any()))
            .thenReturn(Collections.singletonList(123L));

        Appointment appointment = new Appointment();

        processor.resolveAndAttachResources(appointment, practice, Arrays.asList(r1, r2), true);

        assertEquals(2, appointment.getResources().size());
    }

    @Test(expected = ResponseErrorListException.class)
    public void resolveAndAttachResources_should_report_resources_which_are_not_available()
    {
        ResourceRequestDto r1 = new ResourceRequestDto(42L);
        ResourceRequestDto r2 = new ResourceRequestDto(123L);

        ResourceDto resource = new ResourceDto();
        resource.setType(ResourceType.DOCTOR);
        resource.setSystemId(123L);
        resource.setId(42L);
        ResourceDto resource2 = new ResourceDto();
        resource2.setType(ResourceType.ASSISTANT);
        resource2.setId(123L);

        PracticeDto practice = new PracticeDto();
        practice.setResources(Arrays.asList(resource, resource2));

        when(resourceRepository.findOccupiedResources(any(), any(), any()))
            .thenReturn(Collections.singletonList(123L));

        try {
            processor.resolveAndAttachResources(new Appointment(), practice, Arrays.asList(r1, r2), false);
        } catch (ResponseErrorListException errorList) {

            Iterator<ErrorDto> errors = errorList.getErrors().iterator();
            assertEquals(1, errorList.getErrors().size());
            assertEquals(123L, errors.next().getReference());

            throw errorList;
        }
    }

    @Test
    public void resolveAndAttachResources_should_attach_resources_to_appointment()
    {
        ResourceRequestDto r1 = new ResourceRequestDto(42L);

        ResourceDto resource = new ResourceDto();
        resource.setType(ResourceType.DOCTOR);
        resource.setSystemId(123L);
        resource.setId(42L);

        PracticeDto practice = new PracticeDto();
        practice.setResources(Collections.singletonList(resource));

        when(resourceRepository.findOccupiedResources(any(), any(), any()))
            .thenReturn(Collections.emptyList());

        Appointment appointment = new Appointment();

        processor.resolveAndAttachResources(appointment, practice, Collections.singletonList(r1), false);

        assertEquals(1, appointment.getResources().size());
        assertEquals(42L, appointment.getResources().get(0).getResourceId());
    }

    @Test
    public void resolveAndAttachResources_should_exclude_excluded_ids()
    {
        ResourceRequestDto r1 = new ResourceRequestDto(42L);

        ResourceDto resource = new ResourceDto();
        resource.setType(ResourceType.DOCTOR);
        resource.setSystemId(123L);
        resource.setId(42L);

        PracticeDto practice = new PracticeDto();
        practice.setResources(Collections.singletonList(resource));

        Collection<Long> excluded = Collections.singletonList(42L);

        when(resourceRepository.findOtherOccupiedResources(any(), any(), any(), eq(excluded)))
            .thenReturn(Collections.emptyList());

        Appointment appointment = new Appointment();

        processor.resolveAndAttachResources(appointment, practice, Collections.singletonList(r1), excluded, false);

        verify(resourceRepository, times(1)).findOtherOccupiedResources(any(), any(), any(), eq(excluded));
        verify(resourceRepository, never()).findOccupiedResources(any(), any(), any());
    }
}
