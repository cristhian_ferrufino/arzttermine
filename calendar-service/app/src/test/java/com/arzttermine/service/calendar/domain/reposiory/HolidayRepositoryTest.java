package com.arzttermine.service.calendar.domain.reposiory;

import com.arzttermine.service.calendar.domain.entity.Holiday;
import com.arzttermine.service.calendar.domain.repository.HolidayRepository;
import com.arzttermine.service.calendar.web.BaseIntegrationTest;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.*;

public class HolidayRepositoryTest extends BaseIntegrationTest
{
    private static final OffsetDateTime today = Instant.now().atOffset(ZoneOffset.UTC);

    @Autowired
    private HolidayRepository repository;

    @Before
    public void beforeTest() {
        repository.deleteAll();

        Holiday h1 = Holiday.builder()
            .begin(today.plusDays(5).with(LocalTime.MIN).toInstant())
            .end(today.plusDays(6).with(LocalTime.MAX).toInstant())
            .name("my holidays")
            .practiceId(42L)
            .resource(42L)
            .build();

        Holiday h2 = Holiday.builder()
            .begin(today.plusDays(8).with(LocalTime.MIN).toInstant())
            .end(today.plusDays(8).with(LocalTime.MAX).toInstant())
            .name("my holidays")
            .practiceId(42L)
            .resource(42L)
            .build();

        Holiday h3 = Holiday.builder()
            .begin(today.plusDays(8).with(LocalTime.MIN).toInstant())
            .end(today.plusDays(8).with(LocalTime.MAX).toInstant())
            .name("my holidays in a different practice")
            .practiceId(23L)
            .resource(42L)
            .build();

        repository.save(h1);
        repository.save(h2);
        repository.save(h3);
    }

    @Test
    public void findInRange_should_not_pick_up_disabled_holidays()
    {
        Holiday h4 = Holiday.builder()
            .begin(today.plusDays(15).with(LocalTime.MIN).toInstant())
            .end(today.plusDays(16).with(LocalTime.MAX).toInstant())
            .name("my disabled holidays")
            .practiceId(42L)
            .resource(42L)
            .build();

        h4.markForRemoval();
        repository.save(h4);

        int count = repository.findInRange(42L, today.plusDays(10).toInstant(), today.plusDays(20).toInstant(), Collections.singletonList(42L));
        assertEquals(0, count);
    }

    @Test
    public void findInRange_should_pick_up_any_item_in_range()
    {
        int count = repository.findInRange(42L, today.minusDays(1).toInstant(), today.plusDays(10).toInstant(), Collections.singletonList(42L));
        assertEquals(2, count);
    }

    @Test
    public void findInRange_should_pick_up_any_item_in_range_independent_on_start_or_end()
    {
        int count = repository.findInRange(42L, today.plusDays(6).toInstant(), today.plusDays(10).toInstant(), Collections.singletonList(42L));
        assertEquals(2, count);
    }

    @Test
    public void findInRange_should_pick_up_any_item_even_if_start_and_end_are_before_and_after_date()
    {
        repository.save(Holiday.builder()
            .begin(today.plusDays(80).with(LocalTime.MIN).toInstant())
            .end(today.plusDays(90).with(LocalTime.MAX).toInstant())
            .name("my public holidays")
            .publicHoliday(true)
            .practiceId(42L)
            .build());

        int count = repository.findInRange(42L, today.plusDays(82).toInstant(), today.plusDays(87).toInstant(), Collections.singletonList(42L));
        assertEquals(1, count);
    }

    @Test
    public void findInRange_should_pick_up_holidays_from_correct_practice()
    {
        int count = repository.findInRange(42L, today.plusDays(7).toInstant(), today.plusDays(10).toInstant(), Collections.singletonList(42L));
        assertEquals(1, count);

        count = repository.findInRange(23L, today.plusDays(7).toInstant(), today.plusDays(10).toInstant(), Collections.singletonList(42L));
        assertEquals(1, count);
    }

    @Test
    public void findInRange_should_only_include_holidays_in_range()
    {
        int count = repository.findInRange(42L, today.minusDays(1).toInstant(), today.plusDays(5).toInstant(), Collections.singletonList(42L));
        assertEquals(1, count);

        count = repository.findInRange(42L, today.minusDays(1).toInstant(), today.with(LocalTime.MAX).toInstant(), Collections.singletonList(42L));
        assertEquals(0, count);
    }

    @Test
    public void findInRange_should_return_public_holidays_for_each_resource()
    {
        repository.save(Holiday.builder()
            .begin(today.plusDays(50).with(LocalTime.MIN).toInstant())
            .end(today.plusDays(50).with(LocalTime.MAX).toInstant())
            .name("my public holidays")
            .publicHoliday(true)
            .practiceId(42L)
            .build());

        int count = repository.findInRange(42L, today.plusDays(45).toInstant(), today.plusDays(55).toInstant(), Arrays.asList(42L, 23L));
        assertEquals(1, count);
    }
}
