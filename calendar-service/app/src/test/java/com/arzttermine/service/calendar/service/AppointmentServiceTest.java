package com.arzttermine.service.calendar.service;

import com.arzttermine.library.elasticsearch.model.BulkAction;
import com.arzttermine.library.elasticsearch.service.DocumentProcessor;
import com.arzttermine.library.suport.exception.EntityNotFoundException;
import com.arzttermine.library.web.struct.InsuranceType;
import com.arzttermine.library.web.struct.Language;
import com.arzttermine.service.calendar.domain.entity.Appointment;
import com.arzttermine.service.calendar.domain.repository.AppointmentRepository;
import com.arzttermine.service.calendar.exception.RangeAlreadyBlockedException;
import com.arzttermine.service.calendar.service.event.model.AppointmentsBulkChangeEvent;
import com.arzttermine.service.calendar.service.externals.PracticeApi;
import com.arzttermine.service.calendar.service.mapper.AppointmentMapper;
import com.arzttermine.service.calendar.service.processor.ResourceAvailabilityProcessor;
import com.arzttermine.service.calendar.web.dto.request.CreateAppointmentsDto;
import com.arzttermine.service.calendar.web.dto.request.MoveAppointmentDto;
import com.arzttermine.service.calendar.web.dto.response.AppointmentDto;
import com.arzttermine.service.profile.web.dto.response.DoctorProfileDto;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import com.arzttermine.service.profile.web.dto.response.ResourceDto;
import com.arzttermine.service.profile.web.struct.AuthorityRole;
import com.arzttermine.service.profile.web.struct.ResourceType;
import com.querydsl.core.types.Predicate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;

import java.sql.Date;
import java.time.Instant;
import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AppointmentServiceTest
{
    @Spy
    private AppointmentMapper appointmentMapper = new AppointmentMapper();

    @Mock
    private PracticeApi practiceApi;

    @Mock
    private AppointmentRepository appointmentRepository;

    @Mock
    private ResourceAvailabilityProcessor availabilityProcessor;

    @Mock
    private ApplicationEventPublisher eventPublisher;

    @InjectMocks
    private AppointmentService appointmentService;

    @Mock
    private DocumentProcessor documentProcessor;

    @Before
    public void setup() {
        DoctorProfileDto doctor = new DoctorProfileDto();
        doctor.setId(42L);

        ResourceDto doctorResource = new ResourceDto();
        doctorResource.setType(ResourceType.DOCTOR);
        doctorResource.setSystemId(42L);
        doctorResource.setId(123456789L);

        PracticeDto practice = new PracticeDto();
        practice.getDoctors().add(doctor);
        practice.getResources().add(doctorResource);

        doReturn(practice).when(practiceApi).findById(any(), anyLong());
    }

    @Test
    public void createAppointments_should_create_appointments_for_time_between_start_and_end()
    {
        CreateAppointmentsDto dto = buildRequestDto(3);

        Collection<AppointmentDto> appointments = appointmentService.createAppointments(dto);

        assertEquals("requested appointments generated", 3, appointments.size());

        verify(appointmentRepository, times(1)).save(any(Collection.class));
    }

    @Test(expected = RangeAlreadyBlockedException.class)
    public void createAppointments_should_report_booked_appointment_when_trying_to_replace()
    {
        CreateAppointmentsDto dto = buildRequestDto(1);
        dto.setReplaceExisting(true);

        Appointment exisitng = new Appointment();
        exisitng.setBookedByCustomer(42L);

        when(appointmentRepository.findFromPracticeInRange(eq(42L), eq(dto.getStart().toInstant()), eq(dto.getEnd().toInstant()), any()))
            .thenReturn(Collections.singletonList(exisitng));

        appointmentService.createAppointments(dto);
    }

    @Test
    public void createAppointments_should_add_and_remove_appointments_when_replacing()
    {
        CreateAppointmentsDto dto = buildRequestDto(1);
        dto.setReplaceExisting(true);

        when(appointmentRepository.findFromPracticeInRange(eq(42L), eq(dto.getStart().toInstant()), eq(dto.getEnd().toInstant()), any()))
            .thenReturn(Collections.singletonList(new Appointment()));

        doAnswer(invocation -> {
            AppointmentsBulkChangeEvent event = invocation.getArgumentAt(0, AppointmentsBulkChangeEvent.class);
            assertEquals(1, event.getDeleted().size());
            assertEquals(1, event.getCreated().size());
            return invocation;
        }).when(eventPublisher).publishEvent(any(AppointmentsBulkChangeEvent.class));

        Collection<AppointmentDto> appointments = appointmentService.createAppointments(dto);
        assertEquals("requested appointments are generated", 1, appointments.size());
    }

    @Test(expected = EntityNotFoundException.class)
    public void createAppointments_should_report_missing_doctor_resource()
    {
        DoctorProfileDto doctor = new DoctorProfileDto();
        doctor.setId(42L);

        PracticeDto practice = new PracticeDto();
        practice.getDoctors().add(doctor);

        doReturn(practice).when(practiceApi).findById(any(), anyLong());

        CreateAppointmentsDto dto = buildRequestDto(1);

        appointmentService.createAppointments(dto);
    }

    @Test
    public void createAppointments_should_ignore_too_many_appointments_in_range()
    {
        CreateAppointmentsDto dto = buildRequestDto(5);
        dto.setEnd(java.util.Date.from(dto.getStart().toInstant().plusSeconds(3 * (15 * 60)))); // only 3 appointments are eligible in range

        prepareAuth(42L);

        Collection<AppointmentDto> appointments = appointmentService.createAppointments(dto);

        assertEquals("only appointments in range are generated", 3, appointments.size());

        verify(appointmentRepository, times(1)).save(any(Collection.class));
    }

    @Test
    public void updateAppointment_should_move_appointment_from_beginning()
    {
        Appointment existing = spy(Appointment.class);
        when(existing.getId()).thenReturn(42L);
        when(existing.getPracticeId()).thenReturn(23L);
        when(existing.getStart()).thenReturn(Instant.now().plusSeconds(10));
        when(existing.getEnd()).thenReturn(Instant.now().plusSeconds(600));

        MoveAppointmentDto update = new MoveAppointmentDto();
        update.setStart(Date.from(Instant.now().plusSeconds(120)));

        prepareAuth(23L);

        when(appointmentRepository.findOne(42L)).thenReturn(existing);

        AppointmentDto result = appointmentService.moveAppointment(42L, update);

        assertEquals(update.getStart(), result.getStart());
        assertEquals(9, result.getDuration());
    }

    @Test
    public void updateAppointment_should_wipe_out_previous_appointment()
    {
        Appointment existing = spy(new Appointment());
        when(existing.getId()).thenReturn(42L);
        when(existing.getPracticeId()).thenReturn(23L);
        when(existing.getStart()).thenReturn(Instant.now().plusSeconds(10));
        when(existing.getEnd()).thenReturn(Instant.now().plusSeconds(600));
        existing.getInsuranceTypes().add(InsuranceType.PRIVATE);
        existing.getLanguages().add(Language.GERMAN);

        MoveAppointmentDto update = new MoveAppointmentDto();
        update.setStart(Date.from(Instant.now().plusSeconds(120)));

        prepareAuth(23L);

        when(appointmentRepository.findOne(42L)).thenReturn(existing);

        appointmentService.moveAppointment(42L, update);

        assertTrue(existing.getResources().isEmpty());
        assertTrue(existing.getInsuranceTypes().isEmpty());
        assertTrue(existing.getTreatmentTypes().isEmpty());
        assertTrue(existing.getLanguages().isEmpty());
        assertFalse(existing.isEnabled());
        assertFalse(existing.isBooked());

        verify(appointmentRepository, never()).delete(any(Appointment.class));
    }

    @Test
    public void updateAppointment_should_move_appointment_from_ending()
    {
        Appointment existing = spy(Appointment.class);
        when(existing.getId()).thenReturn(42L);
        when(existing.getPracticeId()).thenReturn(23L);
        when(existing.getStart()).thenReturn(Instant.now());
        when(existing.getEnd()).thenReturn(Instant.now().plusSeconds(600));

        MoveAppointmentDto update = new MoveAppointmentDto();
        update.setEnd(Date.from(Instant.now().plusSeconds(600)));

        prepareAuth(23L);

        when(appointmentRepository.findOne(42L)).thenReturn(existing);

        AppointmentDto result = appointmentService.moveAppointment(42L, update);

        assertEquals(update.getEnd(), result.getEnd());
        assertEquals(update.getEnd().toInstant().minusSeconds(600).toEpochMilli() / 1000, result.getStart().toInstant().toEpochMilli() / 1000);
        assertEquals(10, result.getDuration());
    }

    @Test
    public void updateAppointment_should_move_change_duration()
    {
        Appointment existing = spy(Appointment.class);
        when(existing.getId()).thenReturn(42L);
        when(existing.getPracticeId()).thenReturn(23L);
        when(existing.getStart()).thenReturn(Instant.now());
        when(existing.getEnd()).thenReturn(Instant.now().plusSeconds(600));

        MoveAppointmentDto update = new MoveAppointmentDto();
        update.setStart(Date.from(Instant.now().plusSeconds(300)));
        update.setEnd(Date.from(Instant.now().plusSeconds(600)));

        prepareAuth(23L);

        when(appointmentRepository.findOne(42L)).thenReturn(existing);

        AppointmentDto result = appointmentService.moveAppointment(42L, update);

        assertEquals(update.getEnd(), result.getEnd());
        assertEquals(update.getEnd().toInstant().minusSeconds(300).getEpochSecond(), result.getStart().toInstant().getEpochSecond());
        assertEquals(5, result.getDuration());
    }

    @Test(expected = EntityNotFoundException.class)
    public void updateAppointment_should_report_missing_appointment()
    {
        appointmentService.moveAppointment(42L, new MoveAppointmentDto());
    }

    @Test
    public void getAppointments_should_map_practices_accordingly()
    {
        DoctorProfileDto doctor1 = new DoctorProfileDto();
        doctor1.setId(11);

        DoctorProfileDto doctor2 = new DoctorProfileDto();
        doctor2.setId(12);

        DoctorProfileDto doctor3 = new DoctorProfileDto();
        doctor3.setId(13);

        Appointment existing1 = createAppointment(65, 66, 11);
        Appointment existing2 = createAppointment(67, 68, 12);
        Appointment existing3 = createAppointment(69, 70, 13);

        prepareAuth(23L);

        when(appointmentRepository.findAll(any(Predicate.class), any(Pageable.class)))
            .thenReturn(new PageImpl<>(Arrays.asList(existing1, existing2, existing3)));

        PracticeDto p1 = new PracticeDto();
        p1.setDoctors(Collections.singletonList(doctor1));
        PracticeDto p2 = new PracticeDto();
        p2.setDoctors(Collections.singletonList(doctor2));
        PracticeDto p3 = new PracticeDto();
        p3.setDoctors(Collections.singletonList(doctor3));

        Map<Long, PracticeDto> map = new HashMap<>();
        map.put(66L, p1);
        map.put(68L, p2);
        map.put(70L, p3);

        reset(practiceApi);
        when(practiceApi.findByIds(any(), anyCollection())).thenReturn(map);

        List<AppointmentDto> page = appointmentService.getAppointments(null, new PageRequest(0, 10)).getElements();

        assertEquals(11L, page.get(0).getDoctor().getId());
        assertEquals(12L, page.get(1).getDoctor().getId());
        assertEquals(13L, page.get(2).getDoctor().getId());
    }

    @Test
    public void reindexAppointments_Should_Reindex_Appointments()
    {
        PracticeDto practice = new PracticeDto();
        practice.setId(42L);

        when(practiceApi.findById(42L)).thenReturn(practice);

        Appointment anAppointment = new Appointment();
        anAppointment.setPracticeId(42L);
        anAppointment.setStart(Instant.now());
        anAppointment.setEnd(Instant.now());

        Appointment orphanAppointment = new Appointment();
        orphanAppointment.setPracticeId(42L);
        orphanAppointment.setStart(Instant.now());
        orphanAppointment.setEnd(Instant.now());
        orphanAppointment.setEnabled(false);

        Appointment bookedAppointment = new Appointment();
        bookedAppointment.setPracticeId(42L);
        bookedAppointment.setStart(Instant.now());
        bookedAppointment.setEnd(Instant.now());
        bookedAppointment.setBookedByCustomer(42L);

        doAnswer(invocation -> {
            List<BulkAction> actions = invocation.getArgumentAt(2, List.class);
            assertEquals(BulkAction.ActionType.INDEX, actions.get(0).getType());
            assertEquals(BulkAction.ActionType.DELETE, actions.get(1).getType());
            assertEquals(BulkAction.ActionType.DELETE, actions.get(2).getType());
            return null;
        }).when(documentProcessor).executeAsync(eq("appointments"), eq("practice-42"), anyCollectionOf(BulkAction.class));

        when(appointmentRepository.findByPracticeId(42L, new PageRequest(0, 100, new Sort("start"))))
            .thenReturn(new PageImpl<>(Arrays.asList(anAppointment, orphanAppointment, bookedAppointment)));

        appointmentService.reindexAppointments(42L);

        verify(documentProcessor, times(1)).executeAsync(eq("appointments"), eq("practice-42"), anyCollectionOf(BulkAction.class));
    }

    @Test
    public void reindexAppointments_should_add_delete_and_index_actions()
    {
        PracticeDto practice = new PracticeDto();
        practice.setId(42L);

        when(practiceApi.findById(42L)).thenReturn(practice);

        Appointment anAppointment = new Appointment();
        anAppointment.setPracticeId(42l);
        anAppointment.setStart(Instant.now());
        anAppointment.setEnd(Instant.now());


        when(appointmentRepository.findByPracticeId(42l, new PageRequest(0, 50))).thenReturn( new PageImpl<>(Collections.singletonList(anAppointment)));

        appointmentService.reindexAppointments(42);
        verify(documentProcessor, times(1)).executeAsync(eq("appointments"), eq("practice-42"), anyCollectionOf(BulkAction.class));
    }

    public static CreateAppointmentsDto buildRequestDto(int count, Long... ttids)
    {
        Instant begin = Instant.now().plusSeconds(8000);
        return buildRequestDto(begin, count, ttids);
    }

    public static CreateAppointmentsDto buildRequestDto(Instant start, int count, Long... ttids)
    {
        CreateAppointmentsDto request = new CreateAppointmentsDto();
        request.setInsuranceTypes(Collections.singletonList(InsuranceType.COMPULSORY));
        request.setEnd(Date.from(start.plusSeconds((15 * 60) * count)));
        request.setTreatmentTypes(Arrays.asList(ttids));
        request.setStart(Date.from(start));
        request.setPracticeId(42L);
        request.setDoctorId(42L);
        request.setDuration(15);

        return request;
    }

    public static CreateAppointmentsDto buildRequestDto(int count)
    {
        return buildRequestDto(count, 99L);
    }

    private Appointment createAppointment(long id, long practice, long doctor)
    {
        Appointment spy = spy(Appointment.class);
        when(spy.getId()).thenReturn(id);
        when(spy.getPracticeId()).thenReturn(practice);
        when(spy.getDoctorId()).thenReturn(doctor);
        when(spy.getStart()).thenReturn(Instant.now());
        when(spy.getEnd()).thenReturn(Instant.now());

        return spy;
    }

    private void prepareAuth(long practiceId)
    {
        OAuth2AuthenticationDetails authDetails = mock(OAuth2AuthenticationDetails.class);
        when(authDetails.getTokenValue()).thenReturn(UUID.randomUUID().toString());

        OAuth2Authentication auth = mock(OAuth2Authentication.class);
        when(auth.getAuthorities()).thenReturn(Collections.singletonList(new SimpleGrantedAuthority(AuthorityRole.getPracticeAuthority(practiceId))));
        when(auth.getDetails()).thenReturn(authDetails);

        SecurityContextHolder.getContext().setAuthentication(auth);
    }
}
