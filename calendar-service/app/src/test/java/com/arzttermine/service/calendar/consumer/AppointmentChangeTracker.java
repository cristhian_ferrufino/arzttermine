package com.arzttermine.service.calendar.consumer;

import com.arzttermine.library.messaging.consumer.Consumer;
import com.arzttermine.service.calendar.config.settings.MessagingSettings;
import com.arzttermine.service.calendar.messages.AppointmentChangedMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Profile("testing")
public class AppointmentChangeTracker
{
    @Autowired
    private MessagingSettings settings;

    private final List<AppointmentChangedMessage> published = new ArrayList<>();

    @Consumer
    public void track(AppointmentChangedMessage message)
    {
        published.add(message);
    }

    public List<AppointmentChangedMessage> getPublished() {
        return published;
    }

    public void flush() {
        published.clear();
    }
}
