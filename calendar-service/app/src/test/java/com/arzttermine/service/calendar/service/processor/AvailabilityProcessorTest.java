package com.arzttermine.service.calendar.service.processor;

import com.arzttermine.service.calendar.domain.entity.AppointmentResource;
import com.arzttermine.service.calendar.domain.entity.Availability;
import com.arzttermine.service.calendar.domain.entity.RecurringConfiguration;
import com.arzttermine.service.calendar.domain.repository.AvailabilityRepository;
import com.arzttermine.service.calendar.web.struct.RecurringType;
import com.google.common.collect.Sets;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.ApplicationEventPublisher;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AvailabilityProcessorTest
{
    @Mock
    private AvailabilityRepository repository;

    @Mock
    private ApplicationEventPublisher eventPublisher;

    @InjectMocks
    private AvailabilityProcessor processor;

    @Test
    public void updateAppointments_should_generate_new_appointments()
    {

    }

    @Test
    public void mergeOverlapping_should_merge_overlapping_into_target()
    {
        Instant firstStart = Instant.now();
        Instant firstEnd = firstStart.plusSeconds(60 * 15);

        Availability a1 = spy(new Availability());
        a1.setBegin(firstStart);
        a1.setEnd(firstEnd);
        a1.setDuration(30);
        a1.setPracticeId(42L);
        a1.getResources().add(new AppointmentResource(12345L, a1.getBegin(), a1.getEnd()));
        when(a1.getId()).thenReturn(95L);

        Availability overlapping = new Availability();
        overlapping.setBegin(a1.getEnd());
        overlapping.setPracticeId(42L);
        overlapping.setEnd(a1.getEnd().plus(30, ChronoUnit.MINUTES));

        List<Availability> allOverlapping = Collections.singletonList(overlapping);
        Set<Long> resourceIds = Sets.newHashSet(12345L);

        when(repository.findOverlapping(95L, 42L, firstStart, firstEnd, a1.getDuration(), resourceIds)).thenReturn(allOverlapping);
        when(repository.findOne(95L)).thenReturn(a1);

        processor.mergeOverlappingInto(a1);

        assertEquals(a1.getId(), 95L);
        assertEquals(a1.getEnd(), overlapping.getEnd());
        assertEquals(a1.getDuration(), 30);
        assertEquals(a1.getResources().size(), 1);

        verify(repository, times(1)).delete(allOverlapping);
        verify(repository, times(1)).findOverlapping(95L, 42L, firstStart, firstEnd, a1.getDuration(), resourceIds);
    }

    @Test
    public void mergeOverlapping_should_merge_if_recurring_availabilities_are_combined()
    {
        Instant firstStart = Instant.now();
        Instant firstEnd = firstStart.plusSeconds(60 * 15);

        RecurringConfiguration recurringConfig = spy(new RecurringConfiguration(RecurringType.WEEKLY));
        when(recurringConfig.getId()).thenReturn(42L);

        Availability a1 = spy(new Availability());
        a1.setBegin(firstStart);
        a1.setEnd(firstEnd);
        a1.setDuration(30);
        a1.setPracticeId(42L);
        a1.getResources().add(new AppointmentResource(12345L, a1.getBegin(), a1.getEnd()));
        a1.setRecurringConfig(recurringConfig);
        when(a1.getId()).thenReturn(95L);

        Availability overlapping = new Availability();
        overlapping.setBegin(a1.getEnd());
        overlapping.setPracticeId(42L);
        overlapping.setEnd(a1.getEnd().plus(30, ChronoUnit.MINUTES));

        List<Availability> allOverlapping = Collections.singletonList(overlapping);
        Set<Long> resourceIds = Sets.newHashSet(12345L);

        when(repository.findOverlapping(95L, 42L, firstStart, firstEnd, a1.getDuration(), resourceIds)).thenReturn(allOverlapping);
        when(repository.findOne(95L)).thenReturn(a1);

        processor.mergeOverlappingInto(a1);

        verify(repository, never()).delete(allOverlapping);
        assertEquals(firstStart, a1.getBegin());
        assertEquals(firstEnd, a1.getEnd());

        overlapping.setRecurringConfig(recurringConfig);

        processor.mergeOverlappingInto(a1);

        verify(repository, times(1)).delete(allOverlapping);
        assertEquals(firstStart, a1.getBegin());
        assertEquals(overlapping.getEnd(), a1.getEnd());
    }
}
