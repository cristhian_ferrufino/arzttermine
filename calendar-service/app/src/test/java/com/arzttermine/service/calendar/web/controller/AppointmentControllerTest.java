package com.arzttermine.service.calendar.web.controller;

import com.arzttermine.library.elasticsearch.model.BulkAction;
import com.arzttermine.library.elasticsearch.service.DocumentProcessor;
import com.arzttermine.library.messaging.processor.MessageBus;
import com.arzttermine.library.suport.DateFormatterUtil;
import com.arzttermine.library.suport.exception.EntityNotFoundException;
import com.arzttermine.library.web.struct.InsuranceType;
import com.arzttermine.library.web.struct.Language;
import com.arzttermine.service.booking.messages.AppointmentBookedMessage;
import com.arzttermine.service.calendar.consumer.AppointmentChangeTracker;
import com.arzttermine.service.calendar.service.AppointmentServiceTest;
import com.arzttermine.service.calendar.service.externals.PracticeApi;
import com.arzttermine.service.calendar.web.BaseIntegrationTest;
import com.arzttermine.service.calendar.web.dto.request.CreateAppointmentsDto;
import com.arzttermine.service.calendar.web.dto.request.MoveAppointmentDto;
import com.arzttermine.service.calendar.web.dto.request.ResourceRequestDto;
import com.arzttermine.service.calendar.web.dto.request.UpdateAppointmentDto;
import com.arzttermine.service.calendar.web.dto.response.AppointmentDto;
import com.arzttermine.service.notifications.web.struct.NotificationType;
import com.arzttermine.service.profile.web.dto.LocationDto;
import com.arzttermine.service.profile.web.dto.response.DoctorProfileDto;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import com.arzttermine.service.profile.web.dto.response.ResourceDto;
import com.arzttermine.service.profile.web.struct.Country;
import com.arzttermine.service.profile.web.struct.ResourceType;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Sql("/db/empty_database.sql")
public class AppointmentControllerTest extends BaseIntegrationTest
{
    @Autowired
    private PracticeApi practiceApi;

    @Autowired
    private AppointmentChangeTracker changeTracker;

    @Autowired
    private MessageBus messageBus;

    @Autowired
    private DocumentProcessor documentProcessor;

    @Before
    public void setup() throws Exception {
        doReturn(new PracticeDto()).when(practiceApi).findById(any(), eq(42L));

        Thread.sleep(50);
        reset(documentProcessor);
    }

    @Test
    public void createAppointments_should_be_restricted_for_doctors_employees_and_system_clients_only() throws Exception
    {
        CreateAppointmentsDto request = AppointmentServiceTest.buildRequestDto(1);

        mockMvc.perform(post("/appointments")
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isUnauthorized());

        mockMvc.perform(clientRequest(() -> post("/appointments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isForbidden());

        mockMvc.perform(customerRequest(42L, () -> post("/appointments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isForbidden());
    }

    @Test
    public void createAppointments_should_report_errors_and_bad_request_for_empty_dto() throws Exception
    {
        mockMvc.perform(doctorRequest(42L, () -> post("/appointments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(new CreateAppointmentsDto())))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[*]", hasSize(7)))
            .andExpect(jsonPath("$[*].code", containsInAnyOrder("INVALID_ARGUMENT", "INVALID_ARGUMENT", "INVALID_ARGUMENT", "INVALID_ARGUMENT", "INVALID_ARGUMENT", "INVALID_ARGUMENT", "INVALID_ARGUMENT")))
            .andExpect(jsonPath("$[*].reference", containsInAnyOrder("start", "end", "insuranceTypes", "doctorId", "practiceId", "duration", "treatmentTypes")));
    }

    @Test
    public void createAppointments_should_report_invalid_duration() throws Exception
    {
        CreateAppointmentsDto request = AppointmentServiceTest.buildRequestDto(1);
        request.setDuration(3); // should at least 5minutes

        mockMvc.perform(doctorRequest(42L, () -> post("/appointments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$[0].code", is("INVALID_ARGUMENT")))
            .andExpect(jsonPath("$[0].reference", is("duration")));
    }

    @Test
    public void createAppointments_should_report_invalid_start_date() throws Exception
    {
        CreateAppointmentsDto request = AppointmentServiceTest.buildRequestDto(1);
        request.setStart(request.getEnd());

        mockMvc.perform(doctorRequest(42L, () -> post("/appointments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$[0].code", is("INVALID_ARGUMENT")))
            .andExpect(jsonPath("$[0].reference", is("startBeforeEnd")));
    }

    @Test
    public void createAppointments_should_report_blocked_range() throws Exception
    {
        CreateAppointmentsDto request = AppointmentServiceTest.buildRequestDto(1);
        request.setEnd(Date.from(request.getEnd().toInstant().plusSeconds(10000)));

        validPracticeMock(42L);

        mockMvc.perform(employeeRequest(42L, () -> post("/appointments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated());

        mockMvc.perform(doctorRequest(42L, () -> post("/appointments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isConflict())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$[0].code", is("RESOURCE_NOT_AVAILABLE")))
            .andExpect(jsonPath("$[0].reference", is(345))); // doctor is not available
    }

    @Test
    public void createAppointments_should_report_missing_practice() throws Exception
    {
        CreateAppointmentsDto request = AppointmentServiceTest.buildRequestDto(1);

        reset(practiceApi);
        when(practiceApi.findById(any(), anyLong())).thenThrow(new EntityNotFoundException("Practice", 42));

        mockMvc.perform(doctorRequest(42L, () -> post("/appointments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isNotFound())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$[0].code", is("NOT_FOUND")))
            .andExpect(jsonPath("$[0].reference", is(42)));
    }

    @Test
    public void createAppointments_should_report_missing_doctor() throws Exception
    {
        CreateAppointmentsDto request = AppointmentServiceTest.buildRequestDto(1);

        mockMvc.perform(doctorRequest(42L, () -> post("/appointments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isNotFound())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$[0].code", is("NOT_FOUND")))
            .andExpect(jsonPath("$[0].reference", is(42)));
    }

    @Test
    public void createAppointments_should_create_appointments() throws Exception
    {
        CreateAppointmentsDto request = AppointmentServiceTest.buildRequestDto(2);
        Instant firstEnd = request.getStart().toInstant().plusSeconds(15 * 60);

        validPracticeMock(42L);

        mockMvc.perform(employeeRequest(42L, () -> post("/appointments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$[*]", hasSize(2)))
            .andExpect(jsonPath("$[0].id", notNullValue()))
            .andExpect(jsonPath("$[0].booked", is(false)))
            .andExpect(jsonPath("$[0].start", is(DateFormatterUtil.format(request.getStart()))))
            .andExpect(jsonPath("$[0].end", is(DateFormatterUtil.format(firstEnd))))
            .andExpect(jsonPath("$[0].availableInsuranceTypes", containsInAnyOrder(InsuranceType.COMPULSORY.name())))
            .andExpect(jsonPath("$[0].resourceRequests[*]", hasSize(1)))
            .andExpect(jsonPath("$[0].resourceRequests[0].resourceId").exists())
            .andExpect(jsonPath("$[0].practice").exists())
            .andExpect(jsonPath("$[0].doctor").exists())
            .andExpect(jsonPath("$[0].doctor.id", is(42)))
            .andExpect(jsonPath("$[1].id", notNullValue()))
            .andExpect(jsonPath("$[1].booked", is(false)))
            .andExpect(jsonPath("$[1].start", is(DateFormatterUtil.format(firstEnd))))
            .andExpect(jsonPath("$[1].end", is(DateFormatterUtil.format(firstEnd.plusSeconds(15 * 60)))))
            .andExpect(jsonPath("$[1].availableInsuranceTypes", containsInAnyOrder(InsuranceType.COMPULSORY.name())))
            .andExpect(jsonPath("$[1].practice").exists())
            .andExpect(jsonPath("$[1].doctor").exists())
            .andExpect(jsonPath("$[1].doctor.id", is(42)));
    }

    @Test
    public void createAppointments_should_replace_previous_appointments_if_desired() throws Exception
    {
        validPracticeMock(42L);

        CreateAppointmentsDto request1 = AppointmentServiceTest.buildRequestDto(2);
        CreateAppointmentsDto request2 = AppointmentServiceTest.buildRequestDto(1);
        // move request2 to be after the first one including a 30 minute gap
        request2.setStart(Date.from(request1.getStart().toInstant().plus(request1.getDuration() * 3, ChronoUnit.MINUTES)));
        request2.setEnd(Date.from(request1.getStart().toInstant().plus(request1.getDuration() * 4, ChronoUnit.MINUTES)));

        mockMvc.perform(employeeRequest(42L, () -> post("/appointments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request1)))
            .andExpect(status().isCreated());

        mockMvc.perform(employeeRequest(42L, () -> post("/appointments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request2)))
            .andExpect(status().isCreated());

        CreateAppointmentsDto request3 = AppointmentServiceTest.buildRequestDto(2);
        request3.setStart(Date.from(request1.getStart().toInstant().plus(request1.getDuration(), ChronoUnit.MINUTES))); // leave the first appointment of {@code request1} as it is
        request3.setEnd(Date.from(request3.getStart().toInstant().plus(request3.getDuration() * 2, ChronoUnit.MINUTES))); // replace second appointment of {@code request1} and the one from {@code request2}
        request3.setReplaceExisting(true);

        mockMvc.perform(employeeRequest(42L, () -> post("/appointments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request3)))
            .andExpect(status().isCreated());

        Thread.sleep(120);

        mockMvc.perform(systemRequest(() -> get("/appointments?sort=start")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.elements[*]", hasSize(4)))
            .andExpect(jsonPath("$.elements[0].start", is(DateFormatterUtil.format(request1.getStart()))))
            .andExpect(jsonPath("$.elements[1].start", is(DateFormatterUtil.format(request3.getStart()))))
            .andExpect(jsonPath("$.elements[2].start", is(DateFormatterUtil.format(request3.getStart().toInstant().plus(request3.getDuration(), ChronoUnit.MINUTES)))))
            .andExpect(jsonPath("$.elements[3].start", is(DateFormatterUtil.format(request2.getEnd().toInstant().minus(request2.getDuration(), ChronoUnit.MINUTES)))));
    }

    @Test
    public void createAppointments_should_report_non_available_resources() throws Exception
    {
        CreateAppointmentsDto request = AppointmentServiceTest.buildRequestDto(1);

        ResourceRequestDto resource = new ResourceRequestDto();
        resource.setResourceId(42L);
        request.setResourceRequests(Collections.singletonList(resource));

        validPracticeMock(42L);

        mockMvc.perform(employeeRequest(42L, () -> post("/appointments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated());

        mockMvc.perform(employeeRequest(42L, () -> post("/appointments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isConflict())
            .andExpect(jsonPath("$[*]", hasSize(2)))
            .andExpect(jsonPath("$[0].code", is("RESOURCE_NOT_AVAILABLE")))
            .andExpect(jsonPath("$[1].code", is("RESOURCE_NOT_AVAILABLE")))
            .andExpect(jsonPath("$[*].reference", containsInAnyOrder(42, 345)));
    }

    @Test
    public void createAppointments_should_not_report_resource_if_usage_ends_and_starts_in_the_same_minute() throws Exception
    {
        CreateAppointmentsDto request = AppointmentServiceTest.buildRequestDto(1);
        validPracticeMock(42L);

        mockMvc.perform(employeeRequest(42L, () -> post("/appointments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated());

        request.setStart(request.getEnd());
        request.setEnd(Date.from(request.getEnd().toInstant().plus(1, ChronoUnit.HOURS)));

        mockMvc.perform(employeeRequest(42L, () -> post("/appointments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated());
    }

    @Test
    public void moveAppointment_should_move_appointment() throws Exception
    {
        CreateAppointmentsDto request = AppointmentServiceTest.buildRequestDto(1);
        validPracticeMock(42L);

        long id = objectMapper.readValue(mockMvc.perform(employeeRequest(42L, () -> post("/appointments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), AppointmentDto[].class)[0]
            .getId();

        MoveAppointmentDto update = new MoveAppointmentDto();
        update.setEnd(Date.from(request.getEnd().toInstant().plusSeconds(25 * 60)));
        update.getNotificationTypes().add(NotificationType.EMAIL);

        changeTracker.flush();

        mockMvc.perform(employeeRequest(42L, () -> post("/appointments/" + id + "/move"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(update)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.duration", is(40)))
            .andExpect(jsonPath("$.start", is(DateFormatterUtil.format(request.getStart()))))
            .andExpect(jsonPath("$.end", is(DateFormatterUtil.format(update.getEnd()))));

        Thread.sleep(100);

        assertFalse(changeTracker.getPublished().isEmpty());
        assertEquals(id, changeTracker.getPublished().get(0).getPrevious());
        assertNotEquals(id, changeTracker.getPublished().get(0).getNewId());
        assertEquals(NotificationType.EMAIL, changeTracker.getPublished().get(0).getNotificationTypes().get(0));
    }

    @Test
    public void updateAppointment_should_update_appointment() throws Exception
    {
        CreateAppointmentsDto request = AppointmentServiceTest.buildRequestDto(1);
        validPracticeMock(42L);

        long id = objectMapper.readValue(mockMvc.perform(employeeRequest(42L, () -> post("/appointments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), AppointmentDto[].class)[0]
            .getId();

        UpdateAppointmentDto update = new UpdateAppointmentDto();
        update.setLanguages(Arrays.asList(Language.GERMAN, Language.ENGLISH));
        update.setEnabled(false);

        mockMvc.perform(employeeRequest(42L, () -> put("/appointments/" + id))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(update)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.enabled", is(false)))
            .andExpect(jsonPath("$.duration", is(request.getDuration())))
            .andExpect(jsonPath("$.availableLanguages[*]", hasSize(2)))
            .andExpect(jsonPath("$.availableLanguages[*]", containsInAnyOrder(Language.GERMAN.name(), Language.ENGLISH.name())));
    }

    @Test
    public void getOrphanAppointment_should_return_disabled_appointments_only() throws Exception
    {
        CreateAppointmentsDto request = AppointmentServiceTest.buildRequestDto(2);
        validPracticeMock(42L);

        List<AppointmentDto> appointments = Arrays.asList(objectMapper.readValue(mockMvc.perform(employeeRequest(42L, () -> post("/appointments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), AppointmentDto[].class));

        assertEquals(2, appointments.size());
        int id1 = (int) appointments.get(0).getId();
        int id2 = (int) appointments.get(1).getId();

        mockMvc.perform(employeeRequest(42L, () -> get("/appointments/" + id1)))
            .andExpect(status().isOk());

        mockMvc.perform(employeeRequest(42L, () -> get("/appointments/" + id2)))
            .andExpect(status().isOk());

        mockMvc.perform(employeeRequest(42L, () -> delete("/appointments/practices/42"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(Collections.singletonList(id1))))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$[0]", is(id1)));

        mockMvc.perform(employeeRequest(42L, () -> get("/appointments/" + id1)))
            .andExpect(status().isNotFound());

        mockMvc.perform(employeeRequest(42L, () -> get("/appointments/" + id2)))
            .andExpect(status().isOk());

        mockMvc.perform(employeeRequest(42L, () -> get("/appointments/orphans/" + id1)))
            .andExpect(status().isOk());

        mockMvc.perform(employeeRequest(42L, () -> get("/appointments/orphans/" + id2)))
            .andExpect(status().isNotFound());
    }

    @Test
    public void moveAppointment_should_create_new_appointment_and_publish_message() throws Exception
    {
        CreateAppointmentsDto request = AppointmentServiceTest.buildRequestDto(1);
        validPracticeMock(42L);

        changeTracker.flush();

        long id = objectMapper.readValue(mockMvc.perform(employeeRequest(42L, () -> post("/appointments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), AppointmentDto[].class)[0]
            .getId();

        Thread.sleep(50);

        MoveAppointmentDto update = new MoveAppointmentDto();
        update.setStart(Date.from(request.getStart().toInstant().plusSeconds(5 * 60)));

        mockMvc.perform(employeeRequest(42L, () -> post("/appointments/" + id + "/move"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(update)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id", notNullValue()))
            .andExpect(jsonPath("$.id", not(id)))
            .andExpect(jsonPath("$.duration", is(15)))
            .andExpect(jsonPath("$.start", is(DateFormatterUtil.format(update.getStart()))))
            // end should be increased by 5 minutes as the duration does not changed
            .andExpect(jsonPath("$.end", is(DateFormatterUtil.format(request.getEnd().toInstant().plusSeconds(5 * 60)))));

        Thread.sleep(200);

        assertEquals(1, changeTracker.getPublished().size());
        assertEquals(id, changeTracker.getPublished().get(0).getPrevious());
    }

    @Test
    public void moveAppointment_should_report_already_blocked_range() throws Exception
    {
        CreateAppointmentsDto request = AppointmentServiceTest.buildRequestDto(1);
        Date start = request.getStart();

        validPracticeMock(42L);

        long existingBooked = objectMapper.readValue(mockMvc.perform(employeeRequest(42L, () -> post("/appointments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), AppointmentDto[].class)[0]
            .getId();

        AppointmentBookedMessage message = new AppointmentBookedMessage();
        message.setAppointmentId(existingBooked);
        message.setCustomerId(42L);
        messageBus.send(message);
        Thread.sleep(50);

        request.setStart(Date.from(Instant.now().plusSeconds(9999)));
        request.setEnd(Date.from(Instant.now().plusSeconds(9999 + (request.getDuration() * 60))));

        long id = objectMapper.readValue(mockMvc.perform(employeeRequest(42L, () -> post("/appointments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), AppointmentDto[].class)[0]
            .getId();

        // make appointment which should be moved booked
        message = new AppointmentBookedMessage();
        message.setAppointmentId(id);
        message.setCustomerId(42L);
        messageBus.send(message);
        Thread.sleep(100);

        MoveAppointmentDto update = new MoveAppointmentDto();
        update.setStart(Date.from(start.toInstant().plusSeconds(300)));

        mockMvc.perform(employeeRequest(42L, () -> post("/appointments/" + id + "/move"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(update)))
            .andExpect(status().isConflict())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$[0].code", is("RANGE_ALREADY_BLOCKED")))
            .andExpect(jsonPath("$[0].reference", is("345"))); // dat doctor timing
    }

    @Test
    public void moveAppointment_should_report_already_blocked_range_even_when_start_is_now_before_the_initial_start() throws Exception
    {
        reset(practiceApi);
        Instant earliest = Instant.now().plusSeconds(300);

        CreateAppointmentsDto request = AppointmentServiceTest.buildRequestDto(1);
        request.setStart(Date.from(earliest));
        request.setEnd(Date.from(earliest.plusSeconds(request.getDuration() * 60)));

        validPracticeMock(42L);

        long existingBooked = objectMapper.readValue(mockMvc.perform(employeeRequest(42L, () -> post("/appointments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), AppointmentDto[].class)[0]
            .getId();

        AppointmentBookedMessage message = new AppointmentBookedMessage();
        message.setAppointmentId(existingBooked);
        message.setCustomerId(42L);
        messageBus.send(message);
        Thread.sleep(50);

        Instant farAway = Instant.now().plusSeconds(9999);
        request.setStart(Date.from(farAway));
        request.setEnd(Date.from(farAway.plusSeconds(1200)));

        long id = objectMapper.readValue(mockMvc.perform(employeeRequest(42L, () -> post("/appointments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), AppointmentDto[].class)[0]
            .getId();

        MoveAppointmentDto update = new MoveAppointmentDto();
        update.setStart(Date.from(earliest.plusSeconds(130)));

        mockMvc.perform(employeeRequest(42L, () -> post("/appointments/" + id + "/move"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(update)))
            .andExpect(status().isConflict())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$[0].code", is("RANGE_ALREADY_BLOCKED")))
            .andExpect(jsonPath("$[0].reference", is("345"))); // dat doctor timing again
    }

    @Test
    public void deleteAppointments_should_only_search_for_requesting_practice() throws Exception
    {
        reset(practiceApi);

        CreateAppointmentsDto request = AppointmentServiceTest.buildRequestDto(1);
        validPracticeMock(42L);
        validPracticeMock(55L);

        long notValid = objectMapper.readValue(mockMvc.perform(employeeRequest(42L, () -> post("/appointments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), AppointmentDto[].class)[0]
            .getId();

        request.setPracticeId(55L);
        request.setDoctorId(49L);

        int validId = (int) objectMapper.readValue(mockMvc.perform(employeeRequest(42L, () -> post("/appointments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), AppointmentDto[].class)[0]
            .getId();

        mockMvc.perform(employeeRequest(42L, () -> delete("/appointments/practices/" + request.getPracticeId()))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(Arrays.asList(validId, notValid))))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$[0]", is(validId)));
    }

    @Test
    public void deleteAppointments_should_return_deleted_ids() throws Exception
    {
        CreateAppointmentsDto request = AppointmentServiceTest.buildRequestDto(1);
        validPracticeMock(42L);

        int id = (int) objectMapper.readValue(mockMvc.perform(employeeRequest(42L, () -> post("/appointments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), AppointmentDto[].class)[0]
            .getId();

        mockMvc.perform(employeeRequest(42L, () -> delete("/appointments/practices/" + request.getPracticeId()))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(Arrays.asList(1, 2, 3, id))))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$[0]", is(id)));
    }

    @Test
    public void deleteAppointments_should_initiate_elasticsearch_update() throws Exception
    {
        CreateAppointmentsDto request = AppointmentServiceTest.buildRequestDto(1);
        validPracticeMock(42L);

        int id = (int) objectMapper.readValue(mockMvc.perform(employeeRequest(42L, () -> post("/appointments"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(request)))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString(), AppointmentDto[].class)[0]
            .getId();

        Thread.sleep(120);
        reset(documentProcessor);

        mockMvc.perform(employeeRequest(42L, () -> delete("/appointments/practices/" + request.getPracticeId()))
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendDto(Arrays.asList(1, 2, 3, id))))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$[0]", is(id)));

        Thread.sleep(70);

        verify(documentProcessor, times(1))
            .executeAsync(eq("appointments"), eq("practice-42"), anyCollectionOf(BulkAction.class));
    }

    @Test
    public void reindexAppointments_should_reindex_appointments() throws Exception
    {
        CreateAppointmentsDto request = AppointmentServiceTest.buildRequestDto(2);
        request.getStart().toInstant().plusSeconds(15 * 60);

        validPracticeMock(42L);

        mockMvc.perform(employeeRequest(42L, () -> post("/appointments"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(sendDto(request)))
                .andExpect(status().isCreated());

        Thread.sleep(100);
        reset(documentProcessor);

        mockMvc.perform(employeeRequest(42L, () -> post("/appointments/practices/" + request.getPracticeId() + "/reindex"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        verify(documentProcessor, times(1)).executeAsync(eq("appointments"), eq("practice-42"), anyCollectionOf(BulkAction.class));
    }

    private void validPracticeMock(long practiceId)
    {
        DoctorProfileDto doctor = new DoctorProfileDto();
        doctor.setId(42L);

        DoctorProfileDto doctor2 = new DoctorProfileDto();
        doctor2.setId(49L);

        LocationDto location = new LocationDto();
        location.setCountry(Country.GERMANY);
        location.setLocation("buxtehude 123");

        ResourceDto r1 = new ResourceDto();
        r1.setType(ResourceType.DOCTOR);
        r1.setName("the doctor");
        r1.setSystemId(42L);
        r1.setId(345L);

        ResourceDto r2 = new ResourceDto();
        r2.setType(ResourceType.DOCTOR);
        r2.setName("the doctor 2");
        r2.setSystemId(49L);
        r2.setId(346L);

        ResourceDto r3 = new ResourceDto();
        r3.setType(ResourceType.ASSISTANT);
        r3.setName("example resource");
        r3.setId(42L);

        ResourceDto r4 = new ResourceDto();
        r4.setType(ResourceType.DEVICE);
        r4.setName("der gerät");
        r4.setId(55L);

        PracticeDto practice = new PracticeDto();
        practice.setLocation(location);
        practice.setId(practiceId);
        practice.setDoctors(Arrays.asList(doctor, doctor2));
        practice.setResources(Arrays.asList(r1, r2, r3, r4));

        Map<Long, PracticeDto> practices = new HashMap<>();
        practices.put(practiceId, practice);

        when(practiceApi.findById(practiceId)).thenReturn(practice);
        when(practiceApi.findById(any(), eq(practiceId))).thenReturn(practice);
        when(practiceApi.findByIds(any(), eq(Collections.singletonList(practiceId)))).thenReturn(practices);
    }
}
