package com.arzttermine.service.calendar.config;

import com.arzttermine.service.calendar.service.externals.PracticeApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import static org.mockito.Mockito.*;

@Configuration
@Profile("testing")
public class ExternalClientTestConfiguration
{
    @Bean
    public PracticeApi practiceApi() {
        return mock(PracticeApi.class);
    }
}
