package com.arzttermine.service.calendar.domain.entity;

import com.arzttermine.service.calendar.web.struct.RecurringType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.Instant;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class RecurringConfigurationTest
{
    @Test
    public void equals_should_return_false_if_comparator_are_null()
    {
        RecurringConfiguration config1 = new RecurringConfiguration(RecurringType.WEEKLY);

        assertFalse(config1.equals(null));
    }

    @Test
    public void equals_should_return_false_if_comparator_has_different_type()
    {
        RecurringConfiguration config1 = new RecurringConfiguration(RecurringType.WEEKLY);
        RecurringConfiguration config2 = new RecurringConfiguration(RecurringType.MONTHLY);

        assertFalse(config1.equals(config2));
    }

    @Test
    public void equals_should_return_false_if_comparator_has_different_end_date()
    {
        RecurringConfiguration config1 = new RecurringConfiguration(RecurringType.WEEKLY);
        RecurringConfiguration config2 = new RecurringConfiguration(RecurringType.WEEKLY);
        config2.setEndsAt(Instant.now());

        assertFalse(config1.equals(config2));
    }

    @Test
    public void equals_should_return_false_if_comparator_has_different_start_date()
    {
        RecurringConfiguration config1 = new RecurringConfiguration(RecurringType.WEEKLY);
        config1.setStartsAt(Instant.now().minusSeconds(360));

        RecurringConfiguration config2 = new RecurringConfiguration(RecurringType.WEEKLY);
        config2.setStartsAt(Instant.now());

        assertFalse(config1.equals(config2));
    }

    @Test
    public void equals_should_return_true_if_comparator_has_same_type_and_nothing_else()
    {
        RecurringConfiguration config1 = new RecurringConfiguration(RecurringType.WEEKLY);
        config1.setStartsAt(Instant.now());

        RecurringConfiguration config2 = new RecurringConfiguration(RecurringType.WEEKLY);
        config2.setStartsAt(config1.getStartsAt());

        assertTrue(config1.equals(config2));
    }

    @Test
    public void equals_should_return_true_if_comparator_has_same_type_and_start_and_end()
    {
        RecurringConfiguration config1 = new RecurringConfiguration(RecurringType.WEEKLY);
        config1.setStartsAt(Instant.now());
        config1.setEndsAt(Instant.now());

        RecurringConfiguration config2 = new RecurringConfiguration(RecurringType.WEEKLY);
        config2.setStartsAt(config1.getStartsAt());
        config2.setEndsAt(config1.getEndsAt());

        assertTrue(config1.equals(config2));
    }
}
