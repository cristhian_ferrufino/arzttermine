package com.arzttermine.service.calendar.service.processor.transformer.exclusion;

import com.arzttermine.service.calendar.domain.entity.Availability;
import com.arzttermine.service.calendar.domain.entity.RecurringConfiguration;
import com.arzttermine.service.calendar.web.struct.RecurringType;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class AvailabilityConfigurationStrategyTest
{
    private AvailabilityConfigurationStrategy configurationStrategy = new AvailabilityConfigurationStrategy();

    @Test
    public void isExcluded_should_return_true_when_atDate_is_before_start()
    {
        OffsetDateTime atDate = Instant.now().atOffset(ZoneOffset.UTC);
        Availability availability = availabilityWith(atDate.plusDays(1).toInstant(), 30, null);

        boolean result = configurationStrategy.isExcluded(availability, new PracticeDto(), atDate);

        assertTrue(result);
    }

    @Test
    public void isExcluded_should_only_return_true_when_atDate_is_really_on_a_different_day()
    {
        OffsetDateTime atDate = Instant.now().atOffset(ZoneOffset.UTC).minusDays(1).withHour(23).withMinute(59).withSecond(59);
        Availability availability = availabilityWith(atDate.plusMinutes(1).toInstant(), 30, null);

        boolean result = configurationStrategy.isExcluded(availability, new PracticeDto(), atDate);

        assertTrue(result);
    }

    @Test
    public void isExcluded_should_return_true_when_non_recurring_availability_does_not_match_atDate()
    {
        Availability availability = availabilityWith(Instant.now(), 30, null);
        OffsetDateTime atDate = Instant.now().atOffset(ZoneOffset.UTC).plusYears(1).plusDays(5);

        boolean result = configurationStrategy.isExcluded(availability, new PracticeDto(), atDate);

        assertTrue(result);
    }

    @Test
    public void isExcluded_should_return_false_when_non_recurring_availability_matches_atDate()
    {
        Availability availability = availabilityWith(Instant.now(), 30, null);

        boolean result = configurationStrategy.isExcluded(availability, new PracticeDto(), availability.getBegin().atOffset(ZoneOffset.UTC).minusHours(3));

        assertFalse(result);
    }

    @Test
    public void isExcluded_should_return_true_when_recurring_end_exceeds_atDate()
    {
        RecurringConfiguration config = new RecurringConfiguration(RecurringType.WEEKLY);
        config.setEndsAt(Instant.now().plus(90, ChronoUnit.DAYS));
        Availability availability = availabilityWith(Instant.now(), 30, config);

        boolean result = configurationStrategy.isExcluded(availability, new PracticeDto(), availability.getBegin().atOffset(ZoneOffset.UTC).plusMonths(5));

        assertTrue(result);
    }

    @Test
    public void isExcluded_should_return_true_when_recurring_begin_is_before_atDate()
    {
        RecurringConfiguration config = new RecurringConfiguration(RecurringType.WEEKLY);
        Availability availability = availabilityWith(Instant.now().plus(20, ChronoUnit.DAYS), 30, config);

        boolean result = configurationStrategy.isExcluded(availability, new PracticeDto(), Instant.now().atOffset(ZoneOffset.UTC));

        assertTrue(result);
    }

    @Test
    public void isExcluded_should_return_false_for_daily_weekly_and_monthly_valid_recurring_configs()
    {
        RecurringConfiguration config = new RecurringConfiguration(RecurringType.WEEKLY);
        config.setEndsAt(Instant.now().plus(200, ChronoUnit.DAYS));
        Availability availability = availabilityWith(Instant.now(), 30, config);

        boolean result = configurationStrategy.isExcluded(availability, new PracticeDto(), Instant.now().atOffset(ZoneOffset.UTC));
        assertFalse(result);

        availability.setRecurringConfig(new RecurringConfiguration(RecurringType.MONTHLY));
        result = configurationStrategy.isExcluded(availability, new PracticeDto(), Instant.now().atOffset(ZoneOffset.UTC));
        assertFalse(result);

        availability.setRecurringConfig(new RecurringConfiguration(RecurringType.DAILY));
        result = configurationStrategy.isExcluded(availability, new PracticeDto(), Instant.now().atOffset(ZoneOffset.UTC));
        assertFalse(result);
    }

    private Availability availabilityWith(Instant begin, int duration, RecurringConfiguration config) {
        Availability availability = new Availability();
        availability.setBegin(begin);
        availability.setEnd(begin.plus(duration, ChronoUnit.MINUTES));
        availability.setRecurringConfig(config);
        return availability;
    }
}
