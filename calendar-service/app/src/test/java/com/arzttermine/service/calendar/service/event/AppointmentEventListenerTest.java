package com.arzttermine.service.calendar.service.event;

import com.arzttermine.library.elasticsearch.model.BulkAction;
import com.arzttermine.library.elasticsearch.service.DocumentProcessor;
import com.arzttermine.library.messaging.processor.MessageBus;
import com.arzttermine.service.calendar.domain.entity.Appointment;
import com.arzttermine.service.calendar.service.event.model.AppointmentChangedEvent;
import com.arzttermine.service.calendar.service.mapper.AppointmentMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.Instant;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AppointmentEventListenerTest
{
    @Mock
    private DocumentProcessor documentProcessor;

    @Mock
    private MessageBus messageBus;

    @Mock
    private AppointmentMapper mapper;

    @InjectMocks
    private AppointmentEventListener eventListener;

    @Test
    public void appointmentChanged_should_remove_old_and_index_new_document()
    {
        Appointment app1 = new Appointment();
        app1.setPracticeId(1);
        app1.setStart(Instant.now());

        Appointment app2 = new Appointment();
        app2.setPracticeId(2);

        AppointmentChangedEvent event = new AppointmentChangedEvent(app1, app2, Collections.emptyList());

        doAnswer(invocation -> {
            List<BulkAction<?>> actions = invocation.getArgumentAt(2, List.class);
            assertEquals(2, actions.size());
            assertEquals(BulkAction.ActionType.DELETE, actions.get(0).getType());
            assertEquals("appointments", actions.get(0).getIndex());
            assertEquals("practice-1", actions.get(0).getIndexType());
            assertEquals(BulkAction.ActionType.INDEX, actions.get(1).getType());

            return invocation;
        }).when(documentProcessor).executeAsync(eq("appointments"), eq("practice-2"), any());

        eventListener.appointmentChanged(event);

        verify(mapper, times(1)).toDto(app2);
        verify(messageBus, times(1)).sendAsync(any());
        verify(documentProcessor, times(1)).executeAsync(eq("appointments"), eq("practice-2"), any());
    }

    @Test
    public void appointmentChanged_should_remove_old_and_not_index_new_document_if_booked()
    {
        Appointment app1 = new Appointment();
        app1.setPracticeId(1);
        app1.setStart(Instant.now());

        Appointment app2 = new Appointment();
        app2.setPracticeId(2);
        app2.setBookedByCustomer(42L);

        AppointmentChangedEvent event = new AppointmentChangedEvent(app1, app2, Collections.emptyList());

        doAnswer(invocation -> {
            List<BulkAction<?>> actions = invocation.getArgumentAt(2, List.class);
            assertEquals(1, actions.size());
            assertEquals(BulkAction.ActionType.DELETE, actions.get(0).getType());

            return invocation;
        }).when(documentProcessor).executeAsync(eq("appointments"), eq("practice-2"), any());

        eventListener.appointmentChanged(event);

        verify(mapper, never()).toDto(any());
        verify(messageBus, times(1)).sendAsync(any());
        verify(documentProcessor, times(1)).executeAsync(eq("appointments"), eq("practice-2"), any());
    }
}
