package com.arzttermine.service.calendar.domain.repository;

import com.arzttermine.service.calendar.domain.entity.RecurringConfiguration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RecurringConfigurationRepository extends JpaRepository<RecurringConfiguration, Long>
{
}
