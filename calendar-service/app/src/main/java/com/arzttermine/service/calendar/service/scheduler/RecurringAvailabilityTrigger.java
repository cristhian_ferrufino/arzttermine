package com.arzttermine.service.calendar.service.scheduler;

import com.arzttermine.library.spring.data.BatchProcessingUtil;
import com.arzttermine.service.calendar.domain.AvailabilityQueryResult;
import com.arzttermine.service.calendar.domain.repository.AvailabilityRepository;
import com.arzttermine.service.calendar.service.externals.PracticeApi;
import com.arzttermine.service.calendar.service.processor.transformer.AvailabilityIterator;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@Profile("!testing")
public class RecurringAvailabilityTrigger
{
    private static final Logger log = LoggerFactory.getLogger(RecurringAvailabilityTrigger.class);

    @Autowired
    private AvailabilityRepository repository;

    @Autowired
    private AvailabilityIterator availabilityIterator;

    @Autowired
    private PracticeApi practiceClient;

    /**
     * @TODO fetch practices and process batches for each practice separately
     * we gain: indexing performance, clear separation, fault tolerant (vs trying to fetch practices on each batch)
     */
    @Transactional(readOnly = true)
    //@Scheduled(cron = "0 0 3/21 * * ?")
    @Scheduled(fixedDelay = 60000)
    public void triggerAppointmentUpdates()
    {
        log.info("scanning for recurring availabilities to generate further appointments for...");

        final OffsetDateTime now = Instant.now().atOffset(ZoneOffset.UTC);
        final Instant from = now.minusMonths(3).plusDays(1).toInstant();
        final Instant to = now.plusMonths(3).toInstant();

        BatchProcessingUtil.process(
            pageable -> repository.queryForResults(from, to, now.toInstant(), pageable),
            batch -> consumeBatch(batch, to),

            success -> log.info("finished appointment updates for recurring availabilities"),

            new Sort("begin"),
            20,
            10000
        );
    }

    private void consumeBatch(final Collection<AvailabilityQueryResult> availabilities, final Instant maxFuture) {
        log.info("found batch of {} recurring availabilities to generate further appointments for", availabilities.size());

        final Map<Long, PracticeDto> practices = practiceClient.findByIds(availabilities.stream()
            .map(AvailabilityQueryResult::getPracticeId)
            .collect(Collectors.toSet()));

        availabilities.stream().filter(a -> practices.containsKey(a.getPracticeId())).forEach(a -> {
            final PracticeDto practice = practices.get(a.getPracticeId());

            availabilityIterator.createOrUpdateRanges(
                a.getAvailabilityId(),
                practice,
                Optional.ofNullable(a.getLastIteration()).map(d -> d.atOffset(ZoneOffset.UTC)).orElse(null),
                maxFuture.atOffset(ZoneOffset.UTC)
            );
        });
    }
}
