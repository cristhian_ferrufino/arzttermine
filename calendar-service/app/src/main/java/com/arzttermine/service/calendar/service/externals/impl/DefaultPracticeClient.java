package com.arzttermine.service.calendar.service.externals.impl;

import com.arzttermine.library.client.model.RequestOptions;
import com.arzttermine.library.client.model.param.QueryParameter;
import com.arzttermine.service.calendar.service.externals.PracticeApi;
import com.arzttermine.service.player.client.PracticeClient;
import com.arzttermine.service.profile.security.service.TokenCache;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
@Profile("!testing")
public class DefaultPracticeClient implements PracticeApi
{
    @Autowired
    private PracticeClient client;

    @Autowired
    private TokenCache tokenCache;

    @Override
    public PracticeDto findById(UUID token, long id) {
        final RequestOptions options = new RequestOptions();
        options.setAuthorization(token);

        return client.getById(id, options);
    }

    @Override
    public PracticeDto findById(long id) {
        return findById(tokenCache.getToken(), id);
    }

    @Override
    public Map<Long, PracticeDto> findByIds(UUID token, Collection<Long> ids)
    {
        if (ids.isEmpty()) {
            return Maps.newHashMap();
        }

        final RequestOptions options = new RequestOptions();
        options.setPaging(0, ids.size());
        options.setAuthorization(token);

        ids.forEach(id -> options.addParameter(new QueryParameter("id", id)));

        return client.getPractices(options).getElements().stream()
            .collect(Collectors.toMap(PracticeDto::getId, Function.identity()));
    }

    @Override
    public Map<Long, PracticeDto> findByIds(Collection<Long> ids) {
        return findByIds(tokenCache.getToken(), ids);
    }
}
