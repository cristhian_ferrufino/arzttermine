package com.arzttermine.service.calendar.service.processor.transformer.exclusion;

import com.arzttermine.service.calendar.domain.entity.Availability;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.OffsetDateTime;

@Component
public class ExcludedAvailabilityStrategy extends ExclusionStrategy
{
    @Override
    boolean isExcluded(final Availability availability, final PracticeDto practice, final OffsetDateTime atDate) {

        return availability.getExcludedDays().stream()
            .anyMatch(ed -> Duration.between(ed, atDate.withHour(0).withMinute(0).withSecond(0)).toMinutes() == 0);

    }
}
