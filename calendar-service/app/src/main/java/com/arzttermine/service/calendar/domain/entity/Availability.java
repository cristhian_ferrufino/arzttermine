package com.arzttermine.service.calendar.domain.entity;

import com.arzttermine.library.web.struct.InsuranceType;
import com.arzttermine.service.calendar.support.InstantToTimestampConverter;
import com.arzttermine.service.calendar.web.struct.RecurringType;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.validator.constraints.Range;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Entity
@Table(name = "availabilities")
public class Availability extends CalendarTimeRange
{
    @Column(nullable = false)
    @Range(min = 5, max = 999999)
    private int duration;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<AppointmentResource> resources = new ArrayList<>();

    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    @CollectionTable(
        name = "availabilities_treatment_types",
        joinColumns = @JoinColumn(name = "availability_id", nullable = false)
    )
    private List<Long> treatmentTypes = new ArrayList<>();

    @ElementCollection
    @Enumerated(EnumType.STRING)
    @Column(name = "insurance_type")
    @LazyCollection(LazyCollectionOption.FALSE)
    @CollectionTable(
        name = "availabilities_insurance_types",
        joinColumns = @JoinColumn(name = "availability_id", nullable = false)
    )
    private List<InsuranceType> insuranceTypes = new ArrayList<>();

    @OrderBy("start")
    @BatchSize(size = 20)
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.DETACH}, fetch = FetchType.LAZY)
    private List<Appointment> appointments = new ArrayList<>();

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "recurring_config_id", unique = true)
    private RecurringConfiguration recurringConfig;

    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    @Convert(converter = InstantToTimestampConverter.class)
    @Column(name = "excluded_day", nullable = false, unique = false)
    @CollectionTable(
        name = "availabilities_excluded_days",
        joinColumns = @JoinColumn(name = "availability_id", nullable = false)
    )
    private List<Instant> excludedDays = new ArrayList<>();

    @Column(name = "last_appointment_updates", nullable = true)
    @Convert(converter = InstantToTimestampConverter.class)
    private Instant lastIteration;

    public List<AppointmentResource> getResources() {
        return resources;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public List<Long> getTreatmentTypes() {
        return treatmentTypes;
    }

    public List<InsuranceType> getInsuranceTypes() {
        return insuranceTypes;
    }

    public List<Appointment> getAppointments() {
        return appointments;
    }

    public RecurringConfiguration getRecurringConfig() {
        return recurringConfig;
    }

    public void setRecurringConfig(RecurringConfiguration recurringConfig) {
        this.recurringConfig = recurringConfig;
    }

    public boolean isRecurring() {
        return null != recurringConfig && !recurringConfig.getType().equals(RecurringType.NEVER);
    }

    public List<Instant> getExcludedDays() {
        return excludedDays;
    }

    public void setBegin(Instant begin) {
        this.begin = begin;
    }

    public void setEnd(Instant end) {
        this.end = end;
    }

    public void setPracticeId(long id) {
        this.practiceId = id;
    }

    public Instant getLastIteration() {
        return lastIteration;
    }

    public void iterationDone(Instant at) {
        this.lastIteration = at;
    }

    public boolean isAppointmentOutsideOfRange(Appointment appointment) {
        if (isRecurring()) {
            if (null != recurringConfig.getEndsAt() && recurringConfig.getEndsAt().isBefore(appointment.getEnd())) {
                return true;
            }
        } else {
            if (getBegin().isAfter(appointment.getStart()) || getEnd().isBefore(appointment.getEnd())) {
                return true;
            }
        }
        return getBegin().compareTo(appointment.getStart()) > 0;
    }

    @Override
    public String toString() {
        return String.format("%d (%s - %s recurring: %s), practice: %d, required resources: %s", id, begin, end, (isRecurring() ? "yes" : "no"), practiceId, resources.stream()
            .map(AppointmentResource::getResourceId)
            .map(String::valueOf)
            .collect(Collectors.joining(", ")));
    }
}
