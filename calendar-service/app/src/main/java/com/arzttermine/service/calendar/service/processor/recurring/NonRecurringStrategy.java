package com.arzttermine.service.calendar.service.processor.recurring;

import com.arzttermine.service.calendar.domain.entity.Availability;
import com.arzttermine.service.calendar.service.mapper.AvailabilityMapper;
import com.arzttermine.service.calendar.web.dto.response.AvailabilityTimeFrameDto;

import java.time.Duration;
import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class NonRecurringStrategy extends TimeFrameContainer implements AvailabilityRecurringStrategy
{
    public NonRecurringStrategy(AvailabilityMapper mapper) {
        super(mapper);
    }

    @Override
    public void generateFor(Map<Long, List<AvailabilityTimeFrameDto>> frames, Availability availability, Instant begin, Instant end, Duration duration, Collection<Long> resourceIds) {
        final AvailabilityTimeFrameDto frame = mapper.toDto(availability);
        addFrame(frames, availability, resourceIds, frame);
    }
}
