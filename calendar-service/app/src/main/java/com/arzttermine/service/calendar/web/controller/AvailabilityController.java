package com.arzttermine.service.calendar.web.controller;

import com.arzttermine.service.calendar.domain.entity.Availability;
import com.arzttermine.service.calendar.service.AvailabilityService;
import com.arzttermine.service.calendar.web.dto.request.CreateAvailabilitiesConfigurationDto;
import com.arzttermine.service.calendar.web.dto.request.CreateAvailabilityWithRecurringConfigDto;
import com.arzttermine.service.calendar.web.dto.request.DeleteAvailabilityDto;
import com.arzttermine.service.calendar.web.dto.request.PlannedAvailabilitiesRequestDto;
import com.arzttermine.service.calendar.web.dto.request.UpdateAvailabilitiesConfigurationDto;
import com.arzttermine.service.calendar.web.dto.response.AvailabilityTimeFrameDto;
import com.querydsl.core.types.Predicate;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Nullable;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/availabilities")
public class AvailabilityController
{
    @Autowired
    private AvailabilityService service;

    @ApiOperation("Creates availability timeframes for doctors")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_CREATED, message = "no conflict, availabilities are created"),
        @ApiResponse(code = HttpServletResponse.SC_CONFLICT, message = "time range conflict")
    })
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<AvailabilityTimeFrameDto> createAvailability(@Valid @RequestBody CreateAvailabilitiesConfigurationDto config)
    {
        return service.createAvailabilities(config.getRequests(), config.getRecurringConfig());
    }

    @ApiOperation("Updates availability timeframes for doctors")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        ),
        @ApiImplicitParam(
            name = "id",
            dataType = "long",
            required = true,
            paramType = "path"
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "no conflict, availabilities are updated"),
        @ApiResponse(code = HttpServletResponse.SC_CONFLICT, message = "time range conflict")
    })
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{id}", method = RequestMethod.PATCH, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public AvailabilityTimeFrameDto updateAvailability(@PathVariable long id, @Valid @RequestBody CreateAvailabilityWithRecurringConfigDto availability)
    {
        final OffsetDateTime disableRecurringAtDate = Optional.ofNullable(availability.getDisableRecurringAt())
            .map(date -> date.atStartOfDay().atOffset(ZoneOffset.UTC))
            .orElse(null);

        return service.updateAvailability(
            id,
            availability,
            availability.getRecurringConfig(),
            disableRecurringAtDate
        );
    }

    @ApiOperation("Updates multiple availability timeframes for doctors in one request")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        ),
        @ApiImplicitParam(
            name = "id",
            dataType = "long",
            required = true,
            paramType = "path"
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "no conflict, availabilities are updated"),
        @ApiResponse(code = HttpServletResponse.SC_CONFLICT, message = "time range conflict")
    })
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PATCH, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<Long, AvailabilityTimeFrameDto> updateAvailabilitis(@Valid @RequestBody UpdateAvailabilitiesConfigurationDto availabilities)
    {
        return service.updateAvailabilities(availabilities);
    }

    @ApiOperation("Removes an availability timeframe for doctors")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        ),
        @ApiImplicitParam(
            name = "id",
            dataType = "long",
            required = true,
            paramType = "path"
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_NO_CONTENT, message = "no conflict, availabilities are deleted"),
        @ApiResponse(code = HttpServletResponse.SC_CONFLICT, message = "could not remove related appointments or missing instructions for recurring availability")
    })
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteAvailability(@PathVariable long id, @RequestBody @Nullable DeleteAvailabilityDto request)
    {
        service.deleteAvailability(id, request);
    }

    @ApiOperation("Endpoint to retrieve time frames within a specified time range mapped to single resource ids")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "availabilities are returned")
    })
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<Long, List<AvailabilityTimeFrameDto>> getAvailabilities(@QuerydslPredicate(root = Availability.class) Predicate predicate, Pageable pageable)
    {
        return service.getAvailabilities(predicate, pageable);
    }

    @ApiOperation("Endpoint to retrieve planned or recurring time frames within a specified time range mapped to single resource ids")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "availabilities are returned")
    })
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/search", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<Long, List<AvailabilityTimeFrameDto>> requestPlannedAvailabilities(@RequestBody @Valid PlannedAvailabilitiesRequestDto request, Sort pageable)
    {
        return service.getPlannedAvailabilities(request, pageable);
    }
}
