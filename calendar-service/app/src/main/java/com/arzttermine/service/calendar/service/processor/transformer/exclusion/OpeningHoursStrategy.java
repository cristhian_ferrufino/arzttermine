package com.arzttermine.service.calendar.service.processor.transformer.exclusion;

import com.arzttermine.service.calendar.domain.entity.Availability;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;

@Component
public class OpeningHoursStrategy extends ExclusionStrategy
{
    @Override
    boolean isExcluded(final Availability availability, final PracticeDto practice, final OffsetDateTime atDate) {

        // @TODO proper handling of opening hours

        return null != practice.getOpeningHours()
            && !practice.getOpeningHours().containsKey(atDate.getDayOfWeek());
    }
}
