package com.arzttermine.service.calendar.service.processor.recurring;

import com.arzttermine.service.calendar.domain.entity.Availability;
import com.arzttermine.service.calendar.service.mapper.AvailabilityMapper;
import com.arzttermine.service.calendar.web.dto.response.AvailabilityTimeFrameDto;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class WeeklyRecurringStrategy extends TimeFrameContainer implements AvailabilityRecurringStrategy
{
    public WeeklyRecurringStrategy(AvailabilityMapper mapper) {
        super(mapper);
    }

    @Override
    public void generateFor(Map<Long, List<AvailabilityTimeFrameDto>> frames, Availability availability, Instant begin, Instant end, Duration duration, Collection<Long> resourceIds) {
        OffsetDateTime pointer = begin.atOffset(ZoneOffset.UTC);

        final OffsetDateTime initialBegin = availability.getBegin().atOffset(ZoneOffset.UTC);

        final DayOfWeek atDay = initialBegin.getDayOfWeek();
        final double weeks = Math.ceil(ChronoUnit.DAYS.between(pointer, end.atOffset(ZoneOffset.UTC)) / 7.0);

        for (int week = 0; week < weeks; week++) {
            final Instant adjustedFrom = pointer.with(TemporalAdjusters.nextOrSame(atDay))
                .plus(week, ChronoUnit.WEEKS)
                .withHour(initialBegin.getHour())
                .withMinute(initialBegin.getMinute())
                .withSecond(0)
                .withNano(0)
                .toInstant();

            final Instant adjustedEnd = adjustedFrom.plus(duration.toMinutes(), ChronoUnit.MINUTES);

            final AvailabilityTimeFrameDto frame = mapper.toDto(availability, adjustedFrom, adjustedEnd);
            addFrameToAllResources(frames, availability, resourceIds, frame);
        }
    }
}
