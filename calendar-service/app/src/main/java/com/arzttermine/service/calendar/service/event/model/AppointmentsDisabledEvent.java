package com.arzttermine.service.calendar.service.event.model;

import com.arzttermine.service.calendar.domain.entity.Appointment;

import java.util.Collection;

public final class AppointmentsDisabledEvent
{
    private final Collection<Appointment> disabled;

    public AppointmentsDisabledEvent(Collection<Appointment> disabled) {
        this.disabled = disabled;
    }

    public Collection<Appointment> getDisabled() {
        return disabled;
    }
}
