package com.arzttermine.service.calendar.service.processor.transformer;

import com.arzttermine.service.calendar.domain.entity.Availability;
import com.arzttermine.service.calendar.domain.repository.AppointmentRepository;
import com.arzttermine.service.calendar.service.event.model.AppointmentsBulkChangeEvent;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Component
class AppointmentChangeCrudTranslator
{
    @Autowired
    private AppointmentRepository repository;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    void applyChanges(final Availability availability, final Collection<AvailabilityToAppointmentTransformer.AppointmentChange> changes, final PracticeDto practice) {
        if (changes.isEmpty()) {
            return;
        }

        final Set<Long> newAppointments = new HashSet<>();
        final Set<Long> deletedAppointments = new HashSet<>();

        // update existing and save new appointments
        changes.stream()
            .filter(change -> !change.isDeleted())
            .forEach(added -> {
                if (added.isNew()) {
                    repository.save(added.getAppointment());
                    availability.getAppointments().add(added.getAppointment());
                }

                newAppointments.add(added.getAppointment().getId());
            });

        // filter for delete eligible appointments, delete them, add search bulk actions
        // avoid EmptyResultDataAccessException when trying to delete non existing appointment
        changes.stream()
            .filter(AvailabilityToAppointmentTransformer.AppointmentChange::isDeleted)
            .map(AvailabilityToAppointmentTransformer.AppointmentChange::getDeletedAppointmentId)
            .filter(id -> !newAppointments.contains(id)) // make sure we do not remove updated or new documents
            .distinct()
            .map(repository::findOne)
            .filter(Objects::nonNull)
            .filter(a -> !a.isBooked()) // paranoia
            .forEach(deleted -> {
                deletedAppointments.add(deleted.getId());
                availability.getAppointments().remove(deleted);

                repository.delete(deleted);
            });

        eventPublisher.publishEvent(AppointmentsBulkChangeEvent.of(newAppointments, deletedAppointments, practice));
    }
}
