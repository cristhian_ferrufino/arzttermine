package com.arzttermine.service.calendar.service.processor.transformer.exclusion;

import com.arzttermine.service.calendar.domain.entity.AppointmentResource;
import com.arzttermine.service.calendar.domain.entity.Availability;
import com.arzttermine.service.calendar.domain.repository.HolidayRepository;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.util.stream.Collectors;

@Component
public class PracticeHolidayStrategy extends ExclusionStrategy
{
    private final HolidayRepository holidayRepository;

    @Autowired
    public PracticeHolidayStrategy(HolidayRepository holidayRepository) {
        this.holidayRepository = holidayRepository;
    }

    @Override
    boolean isExcluded(final Availability availability, final PracticeDto practice, final OffsetDateTime atDate) {

        final Instant rangeStart = atDate.with(LocalTime.MIN).toInstant();
        final Instant rangeEnd = atDate.with(LocalTime.MAX).toInstant();

        final int holidaysInRange = holidayRepository.findInRange(availability.getPracticeId(), rangeStart, rangeEnd, availability.getResources().stream()
            .map(AppointmentResource::getResourceId)
            .collect(Collectors.toSet()));

        return holidaysInRange > 0;

    }
}
