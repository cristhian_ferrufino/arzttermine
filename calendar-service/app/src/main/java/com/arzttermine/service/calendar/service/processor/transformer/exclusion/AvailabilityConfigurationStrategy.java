package com.arzttermine.service.calendar.service.processor.transformer.exclusion;

import com.arzttermine.service.calendar.domain.entity.Availability;
import com.arzttermine.service.calendar.domain.entity.RecurringConfiguration;
import com.arzttermine.service.calendar.web.struct.RecurringType;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

@Component
public class AvailabilityConfigurationStrategy extends ExclusionStrategy
{
    @Override
    boolean isExcluded(final Availability availability, final PracticeDto practice, final OffsetDateTime atDate) {

        final RecurringConfiguration config = availability.getRecurringConfig();
        final OffsetDateTime availabilityBegin = availability.getBegin().atOffset(ZoneOffset.UTC);

        if (availabilityBegin.toLocalDate().isAfter(atDate.toLocalDate())) {
            return true;
        }

        if (null != config) {
            if (null != config.getEndsAt() && config.getEndsAt().isBefore(atDate.toInstant())) {
                return true;
            }

            if (config.is(RecurringType.MONTHLY) && atDate.getDayOfMonth() == availabilityBegin.getDayOfMonth()) {
                return false;
            } else if (config.is(RecurringType.WEEKLY) && atDate.getDayOfWeek().equals(availabilityBegin.getDayOfWeek())) {
                return false;
            } else if (config.is(RecurringType.DAILY)) {
                return false;
            }
        }

        return availabilityBegin.getDayOfYear() != atDate.getDayOfYear()
            || availabilityBegin.getYear() != atDate.getYear();

    }
}
