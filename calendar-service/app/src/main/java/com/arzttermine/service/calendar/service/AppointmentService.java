package com.arzttermine.service.calendar.service;

import com.arzttermine.library.elasticsearch.model.BulkAction;
import com.arzttermine.library.elasticsearch.service.DocumentProcessor;
import com.arzttermine.library.spring.data.BatchProcessingUtil;
import com.arzttermine.library.suport.exception.EntityNotFoundException;
import com.arzttermine.library.web.dto.response.paging.PageDto;
import com.arzttermine.service.calendar.domain.entity.Appointment;
import com.arzttermine.service.calendar.domain.entity.AppointmentResource;
import com.arzttermine.service.calendar.domain.entity.QAppointment;
import com.arzttermine.service.calendar.domain.repository.AppointmentRepository;
import com.arzttermine.service.calendar.exception.AlreadyBookedException;
import com.arzttermine.service.calendar.exception.RangeAlreadyBlockedException;
import com.arzttermine.service.calendar.service.event.model.AppointmentChangedEvent;
import com.arzttermine.service.calendar.service.event.model.AppointmentReservationEvent;
import com.arzttermine.service.calendar.service.event.model.AppointmentsBulkChangeEvent;
import com.arzttermine.service.calendar.service.event.model.AppointmentsDisabledEvent;
import com.arzttermine.service.calendar.service.externals.PracticeApi;
import com.arzttermine.service.calendar.service.mapper.AppointmentMapper;
import com.arzttermine.service.calendar.service.processor.ResourceAvailabilityProcessor;
import com.arzttermine.service.calendar.web.dto.request.CreateAppointmentsDto;
import com.arzttermine.service.calendar.web.dto.request.MoveAppointmentDto;
import com.arzttermine.service.calendar.web.dto.request.ResourceRequestDto;
import com.arzttermine.service.calendar.web.dto.request.UpdateAppointmentDto;
import com.arzttermine.service.calendar.web.dto.response.AppointmentDto;
import com.arzttermine.service.calendar.web.dto.response.paging.AppointmentsPageDto;
import com.arzttermine.service.profile.security.support.SecurityContextUtil;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import com.arzttermine.service.profile.web.dto.response.ResourceDto;
import com.arzttermine.service.profile.web.struct.ResourceType;
import com.google.common.collect.Lists;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class AppointmentService
{
    private static final Logger log = LoggerFactory.getLogger(AppointmentService.class);

    @Value("${processing.config.reindex.batchSize}")
    private int REINDEX_BATCH_SIZE = 100;

    @Value("${processing.config.reindex.delay}")
    private int REINDEX_DELAY = 30000;

    @Autowired
    private AppointmentRepository appointmentRepository;

    @Autowired
    private PracticeApi practiceClient;

    @Autowired
    private AppointmentMapper mapper;

    @Autowired
    private ResourceAvailabilityProcessor availabilityProcessor;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private DocumentProcessor documentProcessor;

    @Transactional(readOnly = false)
    public Collection<AppointmentDto> createAppointments(final CreateAppointmentsDto request) {
        Instant start = request.getStart().toInstant();
        final Instant end = request.getEnd().toInstant();

        final UUID token = SecurityContextUtil.currentToken();
        final PracticeDto practice = practiceClient.findById(token, request.getPracticeId());
        final Collection<Appointment> appointmentReplacements = new HashSet<>();

        // forcing the doctor resource to be available during the appointment
        request.getResourceRequests().add(new ResourceRequestDto(practice.getResources().stream()
            .filter(r -> r.getType().equals(ResourceType.DOCTOR))
            .filter(r -> Objects.equals(r.getSystemId(), request.getDoctorId()))
            .findFirst()
            .orElseThrow(() -> new EntityNotFoundException("Doctor", request.getDoctorId()))
            .getId()));

        if (request.getReplaceExisting()) {
            final Collection<Long> resourceIds = request.getResourceRequests().stream().map(ResourceRequestDto::getResourceId).collect(Collectors.toSet());
            appointmentReplacements.addAll(appointmentRepository.findFromPracticeInRange(request.getPracticeId(), start, end, resourceIds));

            if (appointmentReplacements.stream().anyMatch(Appointment::isBooked)) {
                throw new RangeAlreadyBlockedException(resourceIds, start, end);
            }

            log.info("removing {} appointments due to forced replacement for appointment in {} - {}", appointmentReplacements.size(), start, end);
        }

        final int durationInSeconds = request.getDuration() * 60;
        final List<Appointment> appointments = new LinkedList<>();

        while (start.isBefore(end)) {
            final Appointment appointment = mapper.fromRequest(start, request, practice);
            availabilityProcessor.resolveAndAttachResources(appointment, practice, request.getResourceRequests(), request.getReplaceExisting());

            appointments.add(appointment);
            start = start.plusSeconds(durationInSeconds);
        }

        appointmentRepository.save(appointments);
        if (!appointmentReplacements.isEmpty()) {
            appointmentRepository.delete(appointmentReplacements);
        }

        eventPublisher.publishEvent(AppointmentsBulkChangeEvent.of(
            appointments.stream().map(Appointment::getId).collect(Collectors.toList()),
            appointmentReplacements.stream().map(Appointment::getId).collect(Collectors.toSet()),
            practice
        ));

        final Map<Long, PracticeDto> practiceMap = new HashMap<>();
        practiceMap.put(practice.getId(), practice);

        return mapper.toDtos(appointments, practiceMap);
    }

    @Transactional(readOnly = true)
    public AppointmentsPageDto getAppointments(final Predicate predicate, final Pageable pageable) {
        final BooleanBuilder builder = new BooleanBuilder(predicate).and(QAppointment.appointment.enabled.isTrue());
        final Page<Appointment> appointments = appointmentRepository.findAll(builder.getValue(), pageable);

        final PageDto page = new PageDto();
        page.setTotalPages(appointments.getTotalPages());
        page.setTotalItems(appointments.getTotalElements());
        page.setCurrentPage(pageable.getPageNumber());
        page.setPageSize(pageable.getPageSize());

        final UUID token = SecurityContextUtil.currentToken();

        final Map<Long, PracticeDto> practices = practiceClient.findByIds(token, appointments.getContent().stream()
                .map(Appointment::getPracticeId)
                .distinct()
                .collect(Collectors.toList()));

        final AppointmentsPageDto result = new AppointmentsPageDto();
        result.setElements(mapper.toDtos(appointments.getContent(), practices));
        result.setPage(page);

        return result;
    }

    @Transactional(readOnly = true)
    public AppointmentDto getAppointment(final long appointmentId) {
        final Appointment appointment = findOneNullsafe(appointmentId);

        final UUID token = SecurityContextUtil.currentToken();
        final PracticeDto practice = practiceClient.findById(token, appointment.getPracticeId());

        return mapper.toDto(appointment, practice);
    }

    @Transactional(readOnly = true)
    public AppointmentDto getOrphanAppointment(final long appointmentId) {
        final Appointment appointment = appointmentRepository.findOrphanById(appointmentId);

        if (null == appointment) {
            throw new EntityNotFoundException("Appointment", appointmentId);
        }

        return mapper.toDto(appointment);
    }

    @Transactional(readOnly = false)
    public Collection<Long> deleteAppointments(final long practiceId, final Collection<Long> appointmentIds) {
        if (appointmentIds.isEmpty()) {
            return Collections.emptyList();
        }

        final Collection<Appointment> candidates = appointmentRepository.findAvailableFromPractice(practiceId, appointmentIds);

        if (candidates.isEmpty()) {
            return Collections.emptyList();
        }

        final AppointmentsDisabledEvent disabled = new AppointmentsDisabledEvent(Lists.newArrayList(candidates));
        eventPublisher.publishEvent(disabled);

        candidates.forEach(Appointment::wipeOut);

        return candidates.stream().map(Appointment::getId).collect(Collectors.toList());
    }

    @Transactional(readOnly = false)
    public AppointmentDto moveAppointment(final long appointmentId, final MoveAppointmentDto update) {
        final Appointment appointment = findOneNullsafe(appointmentId);

        final UUID token = SecurityContextUtil.currentToken();
        final PracticeDto practice = practiceClient.findById(token, appointment.getPracticeId());

        final long currentDuration = appointment.getEnd().getEpochSecond() - appointment.getStart().getEpochSecond();
        final OffsetDateTime newBegin = Optional.ofNullable(update.getStart()).map(Date::toInstant).orElse(appointment.getStart()).atOffset(ZoneOffset.UTC);
        final OffsetDateTime newEnd = Optional.ofNullable(update.getEnd()).map(Date::toInstant)
                .orElseGet(() -> newBegin.plusSeconds(currentDuration).toInstant()).atOffset(ZoneOffset.UTC);

        final Instant newBeginInstant = newBegin.toInstant();
        final Instant newEndInstant = newEnd.toInstant();
        final Collection<Long> resourceIds = appointment.getResources().stream()
            .map(AppointmentResource::getResourceId)
            .collect(Collectors.toSet());

        final Collection<Appointment> appointments = appointmentRepository
            .findFromPracticeInRange(appointment.getPracticeId(), newBeginInstant, newEndInstant, resourceIds);

        if (appointments.stream().filter(a -> a.getId() != appointmentId).anyMatch(Appointment::isBooked)) {
            throw new RangeAlreadyBlockedException(resourceIds, newBeginInstant, newEndInstant);
        }

        final List<ResourceRequestDto> resources = appointment.getResources().stream()
            .map(r -> new ResourceRequestDto(r.getResourceId()))
            .collect(Collectors.toList());

        appointment.getResources().clear();

        final Appointment newAppointment = new Appointment(appointment);
        newAppointment.setPractice(practice);
        newAppointment.setStart(newBeginInstant);
        newAppointment.setEnd(newEndInstant);

        availabilityProcessor.resolveAndAttachResources(newAppointment, practice, resources, true);
        appointmentRepository.save(newAppointment);


        // release previous appointment and disable all old/replaced appointments
        appointment.setBookedByCustomer(null);
        appointment.wipeOut();

        // if present in list, remove as it will be treated separately
        appointments.remove(appointment);
        appointments.forEach(Appointment::wipeOut);

        eventPublisher.publishEvent(new AppointmentsDisabledEvent(appointments));
        eventPublisher.publishEvent(new AppointmentChangedEvent(appointment, newAppointment, update.getNotificationTypes()));

        return mapper.toDto(newAppointment, practice);
    }

    public AppointmentDto updateAppointment(final long appointmentId, final UpdateAppointmentDto update) {
        final Appointment appointment = appointmentRepository.findOne(appointmentId);
        if (null == appointment || !appointment.isEnabled()) {
            throw new EntityNotFoundException("Appointment", appointmentId);
        }

        if (appointment.isBooked()) {
            throw new AlreadyBookedException(appointment.getBookedByCustomer(), appointmentId);
        }

        final UUID token = SecurityContextUtil.currentToken();
        final PracticeDto practice = practiceClient.findById(token, appointment.getPracticeId());

        Optional.ofNullable(update.getEnabled()).ifPresent(appointment::setEnabled);
        Optional.ofNullable(update.getTreatmentTypeIds()).ifPresent(appointment::setTreatmentTypes);
        Optional.ofNullable(update.getInsuranceTypes()).ifPresent(appointment::setInsuranceTypes);
        Optional.ofNullable(update.getLanguages()).ifPresent(appointment::setLanguages);
        Optional.ofNullable(update.getResources()).ifPresent(list -> {
            appointment.getResources().clear();

            final ResourceDto doctor = practice.getResources().stream()
                .filter(r -> r.getType().equals(ResourceType.DOCTOR))
                .filter(d -> d.getSystemId() == appointment.getDoctorId())
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException("Doctor", appointment.getDoctorId()));

            // still, treat doctor in a different way :(
            if (list.stream().noneMatch(rr -> rr.getResourceId().longValue() == doctor.getId())) {
                list.add(new ResourceRequestDto(doctor.getId()));
            }

            availabilityProcessor.resolveAndAttachResources(appointment, practice, list.stream()
                    .map(rr -> new ResourceRequestDto(rr.getResourceId())).collect(Collectors.toList()), false);
        });

        return mapper.toDto(appointment, practice);
    }

    @Transactional(readOnly = false)
    public void updateAppointmentPatient(final long appointmentId, final Long patientId, final boolean makeAvailable, final Instant time) {
        final Appointment appointment = findOneNullsafe(appointmentId);
        final boolean bookedByPatient = Objects.equals(patientId, appointment.getBookedByCustomer());

        if (null != patientId && !bookedByPatient && appointment.isBooked()) {
            throw new AlreadyBookedException(appointment.getBookedByCustomer(), appointmentId);
        } else if (null != patientId && bookedByPatient) {
            return;
        }

        if (appointment.isBooked()) {
            log.info("appointment {} booked by customer {} was released at {}", appointmentId, appointment.getBookedByCustomer(), Date.from(time));
        } else {
            log.info("appointment {} was booked by customer {} at {}", appointmentId, patientId, Date.from(time));
        }

        if (null == patientId && !makeAvailable) {
            log.info("removing appointment {} due to cancelled booking and disabled availability", appointmentId);

            appointmentRepository.delete(appointment);
            eventPublisher.publishEvent(AppointmentsBulkChangeEvent
                .ofDeleted(Collections.singletonList(appointmentId), appointment.getPracticeId()));
        }
        else {
            appointment.setBookedByCustomer(patientId);
            eventPublisher.publishEvent(AppointmentReservationEvent.of(appointment.getId()));
        }
    }

    @Transactional(readOnly = true)
    public void reindexAppointments(final long practiceId) {

        log.info("Starting reindex of appointments for practice {}", practiceId);
        final PracticeDto practice = practiceClient.findById(practiceId);
        final Collection<BulkAction> actions = new ArrayList<>();

        BatchProcessingUtil.process(
            request -> appointmentRepository.findByPracticeId(practiceId, request),
            appointments -> appointments.forEach(appointment -> {
                if (appointment.isBooked() || !appointment.isEnabled()) {
                    AppointmentDto doc = new AppointmentDto();
                    doc.setId(appointment.getId());

                    actions.add(BulkAction.delete(doc));
                } else {
                    AppointmentDto appointmentDto = mapper.toDto(appointment, practice);
                    actions.add(BulkAction.index(appointmentDto));
                }
            }),
            success -> {
                documentProcessor.executeAsync("appointments", "practice-" + practice.getId(), actions);
                log.info("Finished reindexing appointments for practice {}", practice.getId());
            },
            new Sort("start"),
            REINDEX_BATCH_SIZE,
            REINDEX_DELAY
        );
    }

    private Appointment findOneNullsafe(final long appointmentId) {
        final Appointment appointment = appointmentRepository.findOne(appointmentId);

        if (null == appointment) {
            throw new EntityNotFoundException("Appointment", appointmentId);
        }

        return appointment;
    }
}
