package com.arzttermine.service.calendar.exception;

import com.arzttermine.library.suport.exception.BusinessException;

public class AlreadyBookedException extends BusinessException
{
    public AlreadyBookedException(Long patientId, long appointmentId) {
        super("ALREADY_BOOKED", patientId, "Appointment %d was already booked by %s", appointmentId, String.valueOf(patientId));
    }
}
