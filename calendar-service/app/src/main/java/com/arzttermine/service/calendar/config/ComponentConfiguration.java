package com.arzttermine.service.calendar.config;

import com.arzttermine.library.suport.DateFormatterUtil;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Clock;
import java.time.ZoneId;
import java.util.TimeZone;

@Configuration
public class ComponentConfiguration
{
    @Bean
    public ObjectMapper objectMapper()
    {
        DateFormat dateFormat = new SimpleDateFormat(DateFormatterUtil.applicationFormat);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        final ObjectMapper mapper = new ObjectMapper();
        mapper.setDateFormat(dateFormat);
        mapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.PUBLIC_ONLY); // do not return private properties
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL); // ignore null values in response
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        mapper.registerModule(new JavaTimeModule());

        return mapper;
    }

    @Bean
    @Profile("!testing")
    public Clock clock()
    {
        return Clock.system(ZoneId.of("UTC"));
    }
}
