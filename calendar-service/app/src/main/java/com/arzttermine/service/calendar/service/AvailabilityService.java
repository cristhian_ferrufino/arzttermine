package com.arzttermine.service.calendar.service;

import com.arzttermine.library.suport.exception.AccessDeniedException;
import com.arzttermine.library.suport.exception.BusinessException;
import com.arzttermine.library.suport.exception.EntityNotFoundException;
import com.arzttermine.library.suport.exception.ResponseErrorException;
import com.arzttermine.library.suport.exception.ResponseErrorListException;
import com.arzttermine.service.calendar.domain.entity.Appointment;
import com.arzttermine.service.calendar.domain.entity.AppointmentResource;
import com.arzttermine.service.calendar.domain.entity.Availability;
import com.arzttermine.service.calendar.domain.entity.QAvailability;
import com.arzttermine.service.calendar.domain.entity.RecurringConfiguration;
import com.arzttermine.service.calendar.domain.repository.AvailabilityRepository;
import com.arzttermine.service.calendar.domain.repository.RecurringConfigurationRepository;
import com.arzttermine.service.calendar.exception.RangeAlreadyBlockedException;

import com.arzttermine.service.calendar.exception.RequiresCancellationException;
import com.arzttermine.service.calendar.service.event.model.AvailabilityChangedEvent;
import com.arzttermine.service.calendar.service.event.model.AvailabilityRemovedEvent;
import com.arzttermine.service.calendar.service.externals.PracticeApi;
import com.arzttermine.service.calendar.service.mapper.AvailabilityMapper;
import com.arzttermine.service.calendar.service.processor.AvailabilityProcessor;
import com.arzttermine.service.calendar.service.processor.RecurringAvailabilityProcessor;
import com.arzttermine.service.calendar.web.dto.RecurringConfigDto;
import com.arzttermine.service.calendar.web.dto.request.CreateAvailabilityDto;
import com.arzttermine.service.calendar.web.dto.request.DeleteAvailabilityDto;
import com.arzttermine.service.calendar.web.dto.request.PlannedAvailabilitiesRequestDto;
import com.arzttermine.service.calendar.web.dto.request.ResourceRequestDto;
import com.arzttermine.service.calendar.web.dto.request.UpdateAvailabilitiesConfigurationDto;
import com.arzttermine.service.calendar.web.dto.response.AvailabilityTimeFrameDto;
import com.arzttermine.service.calendar.web.struct.RecurringType;
import com.arzttermine.service.profile.security.support.SecurityContextUtil;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import com.arzttermine.service.profile.web.dto.response.ResourceDto;
import com.arzttermine.service.profile.web.dto.response.TreatmentTypeDto;
import com.arzttermine.service.profile.web.struct.ResourceType;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nullable;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class AvailabilityService
{
    private static final Logger log = LoggerFactory.getLogger(AvailabilityService.class);

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private PracticeApi practiceClient;

    @Autowired
    private AvailabilityRepository repository;

    @Autowired
    private RecurringConfigurationRepository configurationRepository;

    @Autowired
    private RecurringAvailabilityProcessor recurringAvailabilities;

    @Autowired
    private AvailabilityProcessor processor;

    @Autowired
    private AvailabilityMapper mapper;

    @Transactional(readOnly = false)
    public List<AvailabilityTimeFrameDto> createAvailabilities(final List<CreateAvailabilityDto> availabilities, @Nullable final RecurringConfigDto recurringConfig)
    {
        final Map<Long, PracticeDto> practices = practiceClient.findByIds(
            availabilities.stream().unordered().map(CreateAvailabilityDto::getPracticeId).distinct().collect(Collectors.toList()));

        log.info("creating new availabilities for practices {}", practices.keySet().stream().map(String::valueOf).collect(Collectors.joining(",")));

        return availabilities.stream().map(a -> {
            if (!practices.containsKey(a.getPracticeId())) {
                throw new EntityNotFoundException("Practice", a.getPracticeId());
            }

            ensurePermissions(a.getPracticeId());

            final PracticeDto practice = practices.get(a.getPracticeId());
            final Instant from = a.getFrom().toInstant();
            final Instant to = a.getTo().toInstant();
            ensureAvailabilityAndResources(a, practice, from, to, Collections.emptyList());

            final Availability availability = new Availability();
            final RecurringConfiguration configuration;

            if (null != recurringConfig && !recurringConfig.getType().equals(RecurringType.NEVER)) {
                configuration = createRecurringConfiguration(from, recurringConfig);
            } else {
                configuration = null;
            }

            mapper.update(availability, a, practice, configuration);
            processor.mergeOverlappingInto(availability);

            repository.save(availability);

            eventPublisher.publishEvent(AvailabilityChangedEvent.of(
                availability.getId(),
                practice.getId(),
                availability.getBegin(),
                availability.isRecurring()
            ));

            return mapper.toDto(availability);
        }).collect(Collectors.toList());
    }

    @Transactional(readOnly = false)
    public Map<Long, AvailabilityTimeFrameDto> updateAvailabilities(final UpdateAvailabilitiesConfigurationDto updates)
    {
        return updates.getUpdates().entrySet().stream()
            .map(update -> updateAvailability(
                update.getKey(),
                update.getValue(),
                updates.getRecurringConfig(),
                Optional.ofNullable(updates.getDisableRecurringAt()).map(date -> date.atStartOfDay().atOffset(ZoneOffset.UTC)).orElse(null)
            ))
            .collect(Collectors.toMap(AvailabilityTimeFrameDto::getId, Function.identity()));
    }

    @Transactional(readOnly = false)
    public AvailabilityTimeFrameDto updateAvailability(final long id, final CreateAvailabilityDto update, @Nullable final RecurringConfigDto config, @Nullable final OffsetDateTime dayAtRecurringRemoval)
    {
        final Availability availability = Optional.ofNullable(repository.findOne(id))
            .orElseThrow(() -> new EntityNotFoundException("Availability", id));

        if (availability.getPracticeId() != update.getPracticeId()) {
            throw new UnsupportedOperationException("cannot change practiceId when updating availabilities");
        }

        ensurePermissions(availability.getPracticeId());

        final PracticeDto practice = practiceClient.findById(update.getPracticeId());
        Instant from = update.getFrom().toInstant();
        Instant to = update.getTo().toInstant();

        log.info("updating availability {} for practice {}", availability, update.getPracticeId());

        RecurringConfiguration existingConfig = availability.getRecurringConfig();

        if (null == existingConfig && null != config) {
            existingConfig = createRecurringConfiguration(from, config);
        } else if (null != existingConfig && (null == config || config.getType().equals(RecurringType.NEVER))) {
            existingConfig = null;

            if (null != dayAtRecurringRemoval) {
                final OffsetDateTime currentBegin = availability.getBegin().atOffset(ZoneOffset.UTC);
                final Duration distance = Duration.between(currentBegin, availability.getEnd().atOffset(ZoneOffset.UTC));

                from = dayAtRecurringRemoval.withHour(currentBegin.getHour()).withMinute(currentBegin.getMinute())
                    .withSecond(0).withNano(0).toInstant();

                to = from.plus(distance.toMinutes(), ChronoUnit.MINUTES);

                // reset excluded days
                availability.getExcludedDays().clear();
            }
        } else if (null != config && !config.getType().equals(RecurringType.NEVER)) {
            existingConfig.setType(config.getType());
            existingConfig.setEndsAt(Optional.ofNullable(config.getEndsAt())
                .map(e -> e.atStartOfDay().toInstant(ZoneOffset.UTC)).orElse(null));

            if (Duration.between(from, existingConfig.getStartsAt()).toMinutes() != 0) {
                existingConfig.setStartsAt(from);
            }
        }

        ensureAvailabilityAndResources(update, practice, from, to, Collections.singletonList(id));

        mapper.update(availability, update, practice, from, to, existingConfig);
        processor.mergeOverlappingInto(availability);

        eventPublisher.publishEvent(AvailabilityChangedEvent.of(
            availability.getId(),
            availability.getPracticeId(),
            availability.getBegin(),
            availability.isRecurring()
        ));

        return mapper.toDto(availability);
    }

    @Transactional(readOnly = false)
    public void deleteAvailability(final long id, final DeleteAvailabilityDto request)
    {
        final Availability availability = repository.findOne(id);
        if (null == availability || !availability.isActive()) {
            return;
        }

        ensurePermissions(availability.getPracticeId());

        final Instant now = Instant.now();
        final OffsetDateTime exclusion;

        if (null != request && null != request.getDate()) {
            exclusion = request.getDate().atStartOfDay().atOffset(ZoneOffset.UTC);
        } else {
            exclusion = null;
        }

        final List<RequiresCancellationException> conflicts = availability.getAppointments().stream()
            .filter(Appointment::isBooked)
            .filter(Appointment::isEnabled)
            .filter(a -> a.getEnd().isAfter(now))
            .filter(a -> null == exclusion || Duration.between(a.getStart().atOffset(ZoneOffset.UTC), exclusion).toDays() == 0)
            .map(a -> new RequiresCancellationException(a.getId()))
            .collect(Collectors.toList());

        if (!conflicts.isEmpty()) {
            throw new ResponseErrorListException(conflicts.stream().map(BusinessException::getErrorDto).collect(Collectors.toList()));
        }

        if (availability.isRecurring() && null == request) {
            throw new ResponseErrorException("recurring event cannot be removed without instructions", "removeAll", 409, null);
        } else if (availability.isRecurring() && null != request) {

            if (!request.getRemoveAll() && null != exclusion) {
                availability.getExcludedDays().add(exclusion.toInstant());

                if (log.isDebugEnabled())
                    log.debug("exclude availability {} on date {}", availability, exclusion.toLocalDate());

                eventPublisher.publishEvent(AvailabilityChangedEvent.of(
                    availability.getId(),
                    availability.getPracticeId(),
                    exclusion.toInstant(),
                    false
                ));

                return;
            }
        }

        availability.markForRemoval();
        eventPublisher.publishEvent(AvailabilityRemovedEvent.of(availability.getId(), availability.getPracticeId()));
    }

    @Transactional(readOnly = true)
    public Map<Long, List<AvailabilityTimeFrameDto>> getAvailabilities(final Predicate predicate, final Pageable pageable)
    {
        final Predicate query = new BooleanBuilder(predicate).and(QAvailability.availability.active.isTrue()).getValue();
        final List<Availability> content = repository.findAll(query, pageable).getContent();

        final List<Long> resourceIds = content.stream()
            .map(a -> a.getResources().stream().map(AppointmentResource::getResourceId).collect(Collectors.toList()))
            .flatMap(Collection::stream)
            .distinct()
            .collect(Collectors.toList());

        return resourceIds.stream().collect(Collectors.toMap(Function.identity(), resourceId -> content.stream()
            .filter(a -> a.getResources().stream().anyMatch(r -> r.getResourceId() == resourceId))
            .map(mapper::toDto)
            .collect(Collectors.toList())));
    }

    @Transactional(readOnly = true)
    public Map<Long, List<AvailabilityTimeFrameDto>> getPlannedAvailabilities(final PlannedAvailabilitiesRequestDto request, final Sort sort)
    {
        final Set<Long> resourceIds = (null == request.getResourceIds() || request.getResourceIds().isEmpty())
            ? practiceClient.findById(SecurityContextUtil.currentToken(), request.getPracticeId()).getResources().stream()
            .filter(r -> r.getType().equals(ResourceType.DOCTOR)) // @TODO if other resources should be visible in the dienstplan this must be removed
            .map(ResourceDto::getId)
            .collect(Collectors.toSet())
            : request.getResourceIds();

        if (log.isDebugEnabled())
            log.debug("searching for availabilities in range: {} - {}, practice: {}, for resources: {}", request.getStart(), request.getEnd(), request.getPracticeId(), resourceIds.stream().map(String::valueOf).collect(Collectors.joining(",")));

        return fetchAndSearchAllFrames(request, resourceIds, sort);
    }

    private Map<Long, List<AvailabilityTimeFrameDto>> fetchAndSearchAllFrames(final PlannedAvailabilitiesRequestDto request, final Set<Long> resourceIds, final Sort sort)
    {
        final List<Availability> availabilities = repository.search(
            request.getPracticeId(),
            request.getStart().toInstant(),
            request.getEnd().toInstant(),
            resourceIds,
            sort
        );

        request.setResourceIds(resourceIds);

        return recurringAvailabilities.searchTimeFramesIn(availabilities, request);
    }

    private void ensurePermissions(long practiceId) {
        final boolean granted = SecurityContextUtil.hasPracticeAuthority(practiceId) || SecurityContextUtil.hasSystemGrants();

        if (!granted) {
            throw new AccessDeniedException();
        }
    }

    // @TODO just out of curious and the fast growth: possible to use resource processor instead?
    private void ensureAvailabilityAndResources(CreateAvailabilityDto update, PracticeDto practice, Instant from, Instant to, Collection<Long> availabilityIds) {
        final Collection<Long> resourceIds = update.getResourceRequests().stream()
            .map(ResourceRequestDto::getResourceId)
            .collect(Collectors.toSet());

        resourceIds.forEach(id -> {
            if (practice.getResources().stream().noneMatch(r -> r.getId().equals(id))) {
                throw new EntityNotFoundException("Resource", id);
            }
        });

        // @TODO include recurring events in query
        final Collection<Long> occupiedIds = (availabilityIds.isEmpty())
            ? repository.findInRange(update.getPracticeId(), from, to, resourceIds)
            : repository.findOtherInRange(update.getPracticeId(), from, to, resourceIds, availabilityIds);

        if (!occupiedIds.isEmpty()) {
            throw new RangeAlreadyBlockedException(occupiedIds, from, to);
        }
    }

    private RecurringConfiguration createRecurringConfiguration(final Instant startsAt, final RecurringConfigDto dto) {
        final RecurringConfiguration config = new RecurringConfiguration(dto.getType());
        config.setEndsAt(Optional.ofNullable(dto.getEndsAt()).map(e -> e.atStartOfDay().toInstant(ZoneOffset.UTC)).orElse(null));
        config.setStartsAt(startsAt);

        return configurationRepository.save(config);
    }
}
