package com.arzttermine.service.calendar.exception;

import com.arzttermine.library.suport.exception.BusinessException;

public class RequiresCancellationException extends BusinessException
{
    public RequiresCancellationException(long appointmentId) {
        super("REQUIRES_CANCELLATION", appointmentId, "Appointment %d is already booked and needs to be cancelled first", appointmentId);
    }
}
