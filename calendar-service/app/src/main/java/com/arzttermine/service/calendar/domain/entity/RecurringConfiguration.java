package com.arzttermine.service.calendar.domain.entity;

import com.arzttermine.service.calendar.support.InstantToTimestampConverter;
import com.arzttermine.service.calendar.web.struct.RecurringType;
import org.springframework.util.Assert;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Instant;

@Entity
@Table(name = "recurring_availability_configurations")
public class RecurringConfiguration
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false, insertable = false)
    private long id;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private RecurringType type;

    @Column(name = "ends_at", nullable = true) // infinite if null
    @Convert(converter = InstantToTimestampConverter.class)
    private Instant endsAt;

    @Column(name = "starts_at", nullable = false)
    @Convert(converter = InstantToTimestampConverter.class)
    private Instant startsAt;

    protected RecurringConfiguration() {
    }

    public RecurringConfiguration(RecurringType type) {
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public RecurringType getType() {
        return type;
    }

    public void setType(RecurringType type) {
        this.type = type;
    }

    public Instant getEndsAt() {
        return endsAt;
    }

    public void setEndsAt(Instant endsAt) {
        this.endsAt = endsAt;
    }

    public Instant getStartsAt() {
        return startsAt;
    }

    public void setStartsAt(Instant startsAt) {
        this.startsAt = startsAt;
    }

    public boolean is(RecurringType type) {
        Assert.notNull(type, "comparison with null is not allowed");
        return type.equals(this.type);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RecurringConfiguration that = (RecurringConfiguration) o;

        if (type != that.type) return false;
        if (endsAt != null ? !endsAt.equals(that.endsAt) : that.endsAt != null) return false;
        return startsAt.equals(that.startsAt);
    }

    @Override
    public int hashCode() {
        int result = type.hashCode();
        result = 31 * result + (endsAt != null ? endsAt.hashCode() : 0);
        result = 31 * result + startsAt.hashCode();
        return result;
    }
}
