package com.arzttermine.service.calendar.config;

import com.arzttermine.library.client.config.RequestProcessorConfigurer;
import com.arzttermine.library.client.config.settings.HttpClientSettings;
import com.arzttermine.library.client.service.RequestProcessor;
import com.arzttermine.service.calendar.config.settings.client.ProfileClientSettings;
import com.arzttermine.service.player.client.PracticeClient;
import com.arzttermine.service.player.client.TokenClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
@Profile("!testing")
public class ExternalClientConfiguration extends RequestProcessorConfigurer
{
    @Autowired
    private HttpClientSettings clientSettings;

    @Bean
    public RequestProcessor requestProcessor()
    {
        return super.requestSender(clientSettings, Collections.emptyList());
    }

    @Bean
    public PracticeClient practiceClient(ProfileClientSettings settings)
    {
        return new PracticeClient(settings);
    }

    @Bean
    public TokenClient tokenClient(ProfileClientSettings settings)
    {
        return new TokenClient(settings);
    }
}
