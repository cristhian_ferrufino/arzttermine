package com.arzttermine.service.calendar.service.externals;

import com.arzttermine.service.profile.web.dto.response.PracticeDto;

import java.util.Collection;
import java.util.Map;
import java.util.UUID;

public interface PracticeApi
{
    PracticeDto findById(UUID token, long id);

    PracticeDto findById(long id);

    Map<Long, PracticeDto> findByIds(UUID token, Collection<Long> ids);

    Map<Long, PracticeDto> findByIds(Collection<Long> ids);
}
