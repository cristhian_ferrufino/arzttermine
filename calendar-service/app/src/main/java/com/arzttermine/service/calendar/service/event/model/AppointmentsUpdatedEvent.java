package com.arzttermine.service.calendar.service.event.model;

import java.util.Collection;

/**
 * The difference between this and the {@link AppointmentChangedEvent} event is,
 * that a change means "drop old and create a new one" .
 * Updating an appointment means a property for example a treatment type is not available anymore etc.
 */
public final class AppointmentsUpdatedEvent
{
    private final Collection<Long> affectedIds;

    private AppointmentsUpdatedEvent(Collection<Long> affected) {
        this.affectedIds = affected;
    }

    public static AppointmentsUpdatedEvent of(Collection<Long> affected) {
        return new AppointmentsUpdatedEvent(affected);
    }

    public Collection<Long> getAffectedIds() {
        return affectedIds;
    }
}
