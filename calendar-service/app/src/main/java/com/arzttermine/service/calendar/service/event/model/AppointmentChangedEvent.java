package com.arzttermine.service.calendar.service.event.model;

import com.arzttermine.service.calendar.domain.entity.Appointment;
import com.arzttermine.service.notifications.web.struct.NotificationType;

import java.util.Collection;

public final class AppointmentChangedEvent
{
    private final Appointment previous;
    private final Appointment updated;
    private final Collection<NotificationType> notificationTypes;

    public AppointmentChangedEvent(Appointment previous, Appointment updated, Collection<NotificationType> types) {
        this.previous = previous;
        this.updated = updated;
        this.notificationTypes = types;
    }

    public Collection<NotificationType> getNotificationTypes() {
        return notificationTypes;
    }

    public Appointment getPrevious() {
        return previous;
    }

    public Appointment getUpdated() {
        return updated;
    }
}
