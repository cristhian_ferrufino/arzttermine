package com.arzttermine.service.calendar.domain.repository;

import com.arzttermine.service.calendar.domain.entity.AppointmentResource;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.Collection;

@Repository
public interface AppointmentResourceRepository extends JpaRepository<AppointmentResource, Long>, QueryDslPredicateExecutor<AppointmentResource>
{
    @Query(
        "SELECT DISTINCT r.resourceId FROM AppointmentResource r WHERE " +
        "r.resourceId IN (:ids) AND r.draft = 0 AND " +
        "(:start BETWEEN r.begin AND r.ending OR :end BETWEEN r.begin AND r.ending) AND " +
        "(r.begin != :end AND r.ending != :start)"
    )
    Collection<Long> findOccupiedResources(@Param("ids") Collection<Long> resourceIds, @Param("start") Instant start, @Param("end") Instant end);

    @Query(
        "SELECT DISTINCT r.resourceId FROM AppointmentResource r WHERE " +
        "r.resourceId IN (:ids) AND r.draft = 0 AND " +
        "(:start BETWEEN r.begin AND r.ending OR :end BETWEEN r.begin AND r.ending) AND " +
        "(r.begin != :end AND r.ending != :start) AND r.id NOT IN (:excluded)"
    )
    Collection<Long> findOtherOccupiedResources(@Param("ids") Collection<Long> resourceIds, @Param("start") Instant start, @Param("end") Instant end, @Param("excluded") Collection<Long> excluded);
}
