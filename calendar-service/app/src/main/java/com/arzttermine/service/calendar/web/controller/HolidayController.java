package com.arzttermine.service.calendar.web.controller;

import com.arzttermine.service.calendar.domain.entity.Holiday;
import com.arzttermine.service.calendar.service.HolidayService;
import com.arzttermine.service.calendar.web.dto.request.CreateHolidaysDto;
import com.arzttermine.service.calendar.web.dto.response.HolidayDto;
import com.arzttermine.service.calendar.web.dto.response.paging.HolidayPageDto;
import com.querydsl.core.types.Predicate;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Collection;

@RestController
@RequestMapping("/holidays")
public class HolidayController
{
    @Autowired
    private HolidayService holidayService;

    @ApiOperation("Creates new public holidays or excluded days")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_CREATED, message = "holiday was created")
    })
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasAnyRole('ROLE_SYSTEM', 'ROLE_EMPLOYEE', 'ROLE_DOCTOR')")
    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Collection<HolidayDto> createHoliday(@RequestBody @Valid CreateHolidaysDto request)
    {
        return holidayService.createHolidays(request);
    }

    @ApiOperation("Returns public holidays and resources based absence")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "page of holidays was returned")
    })
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyRole('ROLE_SYSTEM', 'ROLE_EMPLOYEE', 'ROLE_DOCTOR')")
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public HolidayPageDto getHoliday(@QuerydslPredicate(root = Holiday.class) Predicate predicate, Pageable pageable)
    {
        return holidayService.getHolidays(predicate, pageable);
    }

    @ApiOperation("Removes holiday")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        ),
        @ApiImplicitParam(
            name = "holidayId",
            dataType = "long",
            required = true,
            paramType = "path"
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_NO_CONTENT, message = "not found or successfully removed")
    })
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAnyRole('ROLE_SYSTEM', 'ROLE_EMPLOYEE', 'ROLE_DOCTOR')")
    @RequestMapping(value = "/{holidayId}", method = RequestMethod.DELETE)
    public void getHoliday(@PathVariable long holidayId)
    {
        holidayService.deleteHoliday(holidayId);
    }
}
