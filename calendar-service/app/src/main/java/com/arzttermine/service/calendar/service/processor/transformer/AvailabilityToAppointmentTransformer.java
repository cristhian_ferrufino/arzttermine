package com.arzttermine.service.calendar.service.processor.transformer;

import com.arzttermine.library.suport.exception.ResponseErrorListException;
import com.arzttermine.library.web.dto.response.ErrorDto;
import com.arzttermine.library.web.struct.InsuranceType;
import com.arzttermine.service.calendar.domain.entity.Appointment;
import com.arzttermine.service.calendar.domain.entity.AppointmentResource;
import com.arzttermine.service.calendar.domain.entity.Availability;
import com.arzttermine.service.calendar.domain.entity.Holiday;
import com.arzttermine.service.calendar.domain.entity.RecurringConfiguration;
import com.arzttermine.service.calendar.service.mapper.AvailabilityMapper;
import com.arzttermine.service.calendar.service.processor.ResourceAvailabilityProcessor;
import com.arzttermine.service.calendar.service.processor.transformer.exclusion.ExclusionStrategy;
import com.arzttermine.service.calendar.web.dto.request.ResourceRequestDto;
import com.arzttermine.service.profile.web.dto.response.DoctorProfileDto;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import com.arzttermine.service.profile.web.dto.response.ResourceDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @TODO Only handle availabilities and no longer distinguish between appointments and availabilities? This is legacy!
 *
 * This component is used to transform {@link Availability} into multiple {@link Appointment}s.
 * As result of the transformation you will receive a list of {@link AppointmentChange}.
 * Please make sure you never change the {@link Availability} within this method, only copies! This class should act leniently
 *
 * An {@link AppointmentChange} can either consist of the following:
 *  - delete instruction {@code getDeletedAppointmentId} returns an id
 *  - add instruction {@code getAppointment} returns new appointment (unsaved)
 *  - update instruction {@code getAppointment} returns updated appointment (not persisted) and {@code isEdited} returns <i>true</i>
 *
 * Several things to be noted for appointment generation:
 *  - Its only allowed to have appointments till the maximum range (if an appointment would overlap with this end, it does not get created (rg: 10-11, 45min -> only 1 appointment)
 *  - Resources must be available for updated or new appointments (if not available and tried to update an existing appointment, we need to remove it)
 *  - When moving appointments from a certain availability all resources attached to an unbooked {@link Appointment} are excluded from potential unavailable resources
 *  - Booked appointments are never moved or deleted, even if the availability resize or moves
 *  - Days which are excluded by the {@link Availability} or due to {@link Holiday}s need to be filtered out
 *  - Appointments must be generated based on the {@link Availability}'s {@link RecurringConfiguration}
 *  - When a recurring end is changed all appointments after the recurring end must be removed
 *  - This transformer does not remove or add appointments outside of the range from the availability
 *
 *  {@link ExclusionStrategy}
 *  {@link AvailabilityIterator}
 *  {@link AppointmentChangeCrudTranslator}
 */
@Component
class AvailabilityToAppointmentTransformer
{
    private static final Logger log = LoggerFactory.getLogger(AvailabilityToAppointmentTransformer.class);

    private final List<ExclusionStrategy> exclusionStrategies;

    private final ResourceAvailabilityProcessor resourceProcessor;

    private final AvailabilityMapper availabilityMapper;

    @Autowired
    public AvailabilityToAppointmentTransformer(List<ExclusionStrategy> exclusionStrategies, ResourceAvailabilityProcessor resourceProcessor, AvailabilityMapper availabilityMapper) {
        this.exclusionStrategies = exclusionStrategies;
        this.resourceProcessor = resourceProcessor;
        this.availabilityMapper = availabilityMapper;
    }

    Collection<AppointmentChange> transform(final Availability availability, final PracticeDto practice, final OffsetDateTime atDate)
    {
        final Iterator<Appointment> existing = getExistingCandidates(availability, atDate);
        final OffsetDateTime availabilityBegin = availability.getBegin().atOffset(ZoneOffset.UTC);

        if (exclusionStrategies.stream().anyMatch(strategy -> strategy.isUnavailable(availability, practice, atDate))) {
            return appointmentRemovalChangesFor(existing);
        }

        final Optional<DoctorProfileDto> doctorProfile = getDoctor(availability, practice);
        if (!doctorProfile.isPresent()) {
            // that is somewhat strange
            log.info("missing doctor for availability {} on date {}", availability.getId(), atDate.toString());
            return appointmentRemovalChangesFor(existing);
        }

        final List<AppointmentChange> changes = new ArrayList<>();

        // point to requested date and time of availability begin
        OffsetDateTime pointer = atDate
            .withHour(availabilityBegin.getHour())
            .withMinute(availabilityBegin.getMinute())
            .withSecond(0).withNano(0);

        // we need to make sure that appointments are only generated within the range (note the -duration +1)
        // eg: 10:00-10:30 with 20 Minute duration should result in 1 appointment
        final long duration = (Duration.between(availabilityBegin, availability.getEnd().atOffset(ZoneOffset.UTC)).toMinutes() - availability.getDuration()) + 1;

        for (int step = 0; step < duration; step += availability.getDuration()) {

            checkAndCreateOrUpdateAppointment(availability, practice, doctorProfile.get(), existing, pointer)
                .ifPresent(changes::add);

            pointer = pointer.plusMinutes(availability.getDuration());
        }

        // remove remaining appointments which doesn't take place anymore
        changes.addAll(appointmentRemovalChangesFor(existing));

        log.info("detected {} changes on date {} for availability {} in practice {}", changes.size(), atDate.toLocalDate(), availability, practice.getId());

        return changes;
    }

    private Optional<AppointmentChange> checkAndCreateOrUpdateAppointment(
        final Availability availability,
        final PracticeDto practice,
        final DoctorProfileDto doctorProfile,
        final Iterator<Appointment> existing,
        final OffsetDateTime pointer
    ) {
        final Set<Long> usedResourceIds;
        final Appointment appointment;
        final boolean editExisting;

        if (existing.hasNext()) {
            appointment = existing.next();
            editExisting = true;

            usedResourceIds = appointment.getResources().stream()
                .map(AppointmentResource::getId)
                .collect(Collectors.toSet());

            appointment.reset();

        } else {
            usedResourceIds = Collections.emptySet();
            appointment = availabilityMapper.toBasicAppointment(availability, doctorProfile);
            editExisting = false;
        }

        appointment.setStart(pointer.toInstant());
        appointment.setEnd(pointer.plusMinutes(availability.getDuration()).toInstant());
        appointment.getTreatmentTypes().addAll(availability.getTreatmentTypes());

        appointment.getInsuranceTypes().addAll((availability.getInsuranceTypes().isEmpty())
            ? Arrays.asList(InsuranceType.COMPULSORY, InsuranceType.PRIVATE)
            : availability.getInsuranceTypes());

        try {
            resourceProcessor.resolveAndAttachResources(
                appointment,
                practice,
                availability.getResources().stream().map(r -> new ResourceRequestDto(r.getResourceId())).collect(Collectors.toList()),
                // excluded resources which are already used by this appointment
                usedResourceIds,
                false
            );

            logIfDebugEnabled("{} appointment {} with treatments {} and doctor {}", (editExisting) ? "updating" : "creating", appointment, appointment.getTreatmentTypes().stream()
                .map(String::valueOf).collect(Collectors.joining(", ")), appointment.getDoctorId());

            return Optional.of(new AppointmentChange(appointment, editExisting));

        } catch (ResponseErrorListException e) {
            logIfDebugEnabled("skipping appointment {} from availability {} due to unavailable resources: {}",
                appointment, availability.getId(), e.getErrors().stream().map(ErrorDto::getReference).map(String::valueOf).collect(Collectors.joining(", ")));

            return (editExisting)
                ? Optional.of(new AppointmentChange(appointment.getId()))
                : Optional.empty();
        }
    }

    private Collection<AppointmentChange> appointmentRemovalChangesFor(final Iterator<Appointment> existing) {
        final List<AppointmentChange> changes = new ArrayList<>();
        existing.forEachRemaining(appointment -> changes.add(new AppointmentChange(appointment.getId())));

        return changes;
    }

    // find active and unbooked appointments which are placed on the requested date
    private Iterator<Appointment> getExistingCandidates(final Availability availability, final OffsetDateTime atDate) {
        final LocalDate atLocalDate = atDate.toLocalDate();

        return availability.getAppointments().stream()
            .filter(Appointment::isEnabled)
            .filter(a -> !a.isBooked())
            .filter(a -> a.getStart().atOffset(ZoneOffset.UTC).toLocalDate().equals(atLocalDate))
            // the collect call seems pointless, but it creates a new detached list so we do not work on an hibernate iterator
            .collect(Collectors.toList())
            .iterator();
    }

    private Optional<DoctorProfileDto> getDoctor(final Availability availability, final PracticeDto practice) {
        final Collection<Long> resourceIds = availability.getResources().stream()
            .map(AppointmentResource::getResourceId)
            .collect(Collectors.toSet());

        return practice.getResources().stream()
            .filter(resource -> resourceIds.contains(resource.getId()))
            .map(resource -> getDoctorByResource(practice, resource))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .findFirst();
    }

    private Optional<DoctorProfileDto> getDoctorByResource(PracticeDto practice, ResourceDto resource) {
        return practice.getDoctors().stream()
            .filter(doctor -> resource.getSystemId().equals(doctor.getId()))
            .findFirst();
    }

    private void logIfDebugEnabled(String message, Object ...params) {
        if (log.isDebugEnabled())
            log.debug(message, params);
    }

    static final class AppointmentChange
    {
        private final Appointment appointment;
        private final Long deletedAppointmentId;
        private final boolean edited;

        AppointmentChange(Appointment appointment, boolean edited) {
            this.appointment = appointment;
            this.edited = edited;
            this.deletedAppointmentId = null;
        }

        AppointmentChange(long deletedAppointmentId) {
            this.deletedAppointmentId = deletedAppointmentId;
            this.appointment = null;
            this.edited = true;
        }

        public Long getDeletedAppointmentId() {
            return deletedAppointmentId;
        }

        public Appointment getAppointment() {
            return appointment;
        }

        public boolean isEdited() {
            return edited || isDeleted();
        }

        public boolean isDeleted() {
            return null != deletedAppointmentId && null == appointment;
        }

        public boolean isNew() {
            return null != appointment && !isEdited();
        }
    }
}
