package com.arzttermine.service.calendar.domain.entity;

import com.arzttermine.service.calendar.support.InstantToTimestampConverter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.time.Instant;

@MappedSuperclass
public abstract class CalendarTimeRange
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false, insertable = false)
    protected long id;

    @Column
    private Boolean active = true;

    @Column(nullable = false)
    @Convert(converter = InstantToTimestampConverter.class)
    protected Instant begin;

    @Column(nullable = false)
    @Convert(converter = InstantToTimestampConverter.class)
    protected Instant end;

    @Column(name = "practice_id", nullable = false, updatable = false)
    protected long practiceId;

    public long getId() {
        return id;
    }

    public Instant getBegin() {
        return begin;
    }

    public Instant getEnd() {
        return end;
    }

    public long getPracticeId() {
        return practiceId;
    }

    public boolean isActive() {
        return null != active && active;
    }

    public void markForRemoval() {
        active = null;
    }

}
