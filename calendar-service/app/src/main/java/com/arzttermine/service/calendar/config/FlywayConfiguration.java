package com.arzttermine.service.calendar.config;

import com.arzttermine.service.calendar.config.settings.FlywaySettings;
import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import javax.sql.DataSource;

@Configuration
@Profile("!testing")
public class FlywayConfiguration
{
    @Autowired
    private DataSource dataSource;

    @Bean
    public Flyway flyway(FlywaySettings flywaySettings)
    {
        final Flyway flyway = new Flyway();

        if (flywaySettings.isEnabled()) {
            flyway.setDataSource(dataSource);
            flyway.setTable(flywaySettings.getTable());
            flyway.setSchemas(flywaySettings.getDatabase());
            flyway.setLocations(flywaySettings.getLocation());
            flyway.setBaselineVersionAsString("0");
            flyway.setBaselineOnMigrate(true);
            flyway.migrate();
        }

        return flyway;
    }
}
