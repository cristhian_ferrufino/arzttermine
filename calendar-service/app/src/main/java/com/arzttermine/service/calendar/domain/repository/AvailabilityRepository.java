package com.arzttermine.service.calendar.domain.repository;

import com.arzttermine.service.calendar.domain.AvailabilityQueryResult;
import com.arzttermine.service.calendar.domain.entity.AppointmentResource;
import com.arzttermine.service.calendar.domain.entity.Availability;
import com.arzttermine.service.calendar.domain.entity.QAvailability;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.ComparableExpression;
import com.querydsl.core.types.dsl.SimpleExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

// @TODO data jpa test
@Repository
public interface AvailabilityRepository extends PagingAndSortingRepository<Availability, Long>,
                                                QueryDslPredicateExecutor<Availability>,
                                                QuerydslBinderCustomizer<QAvailability>
{
    @Query(
        "SELECT a FROM Availability a JOIN a.resources ar WHERE a.id != :source AND " +
        "a.practiceId = :practice AND (a.begin = :end OR a.end = :start) AND " +
        "a.duration = :duration AND ar.resourceId IN (:resources) GROUP BY a.id"
    )
    List<Availability> findOverlapping(
        @Param("source") long id,
        @Param("practice") long practiceId,
        @Param("start") Instant start,
        @Param("end") Instant end,
        @Param("duration") int duration,
        @Param("resources") Collection<Long> resourceIds
    );

    @Query(
        "SELECT DISTINCT a.id FROM Availability a JOIN a.resources ar WHERE a.active IS TRUE AND a.practiceId = :practice AND " +
        "(:start BETWEEN a.begin AND a.end OR :end BETWEEN a.begin AND a.end) AND a.begin != :end AND a.end != :start AND " +
        "ar.resourceId IN (:resources)"
    )
    Collection<Long> findInRange(
        @Param("practice") long practiceId,
        @Param("start") Instant start,
        @Param("end") Instant end,
        @Param("resources") Collection<Long> resourceIds
    );

    @Query(
        "SELECT DISTINCT a.id FROM Availability a JOIN a.resources ar WHERE a.active IS TRUE AND a.practiceId = :practice AND " +
        "(:start BETWEEN a.begin AND a.end OR :end BETWEEN a.begin AND a.end) AND a.begin != :end AND a.end != :start AND " +
        "ar.resourceId IN (:resources) AND a.id NOT IN (:excluded)"
    )
    Collection<Long> findOtherInRange(
        @Param("practice") long practiceId,
        @Param("start") Instant start,
        @Param("end") Instant end,
        @Param("resources") Collection<Long> resourceIds,
        @Param("excluded") Collection<Long> excludedAvailabilities
    );

    @Query(
        "SELECT a FROM Availability a JOIN a.resources ar LEFT JOIN a.recurringConfig rc WHERE " +
        "(a.active IS TRUE AND ar.resourceId IN (:resources) AND a.practiceId = :practice) AND " +
        "(rc IS NULL AND a.begin <= :end AND a.end >= :start) OR " +
        "((rc IS NOT NULL AND rc.endsAt IS NULL OR rc.endsAt >= :start) AND rc.startsAt <= :end) " +
        "GROUP BY a.id"
    )
    List<Availability> search(
        @Param("practice") long practiceId,
        @Param("start") Instant start,
        @Param("end") Instant end,
        @Param("resources") Collection<Long> resourceIds,
        Sort sort
    );

    @Query(
        "SELECT new com.arzttermine.service.calendar.domain.AvailabilityQueryResult(a.id, a.practiceId, a.lastIteration) FROM Availability a LEFT JOIN a.recurringConfig rc WHERE " +
        "rc IS NOT NULL AND a.active = 1 AND a.begin >= :minDate AND ((rc.endsAt IS NULL OR rc.endsAt >= :today) AND rc.startsAt <= :maxDate) AND " +
        "(a.lastIteration IS NULL OR a.lastIteration <= :today)"
    )
    Page<AvailabilityQueryResult> queryForResults(@Param("minDate") Instant minDate, @Param("maxDate") Instant maxDate, @Param("today") Instant today, Pageable pageable);

    @Override
    default void customize(QuerydslBindings bindings, QAvailability root) {
        bindings.bind(root.begin).first(ComparableExpression::goe);
        bindings.bind(root.end).first(ComparableExpression::loe);
        bindings.bind(root.practiceId).all(SimpleExpression::in);
        bindings.bind(root.resources).all((path, value) -> path.any().resourceId.in(value.stream()
            .map(a -> a.stream().map(AppointmentResource::getResourceId)).flatMap(Stream::distinct).collect(Collectors.toList())));
    }
}
