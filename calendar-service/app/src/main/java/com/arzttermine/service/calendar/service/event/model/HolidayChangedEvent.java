package com.arzttermine.service.calendar.service.event.model;

import java.time.LocalDate;

public class HolidayChangedEvent
{
    private final LocalDate from;
    private final LocalDate to;
    private final long practiceId;

    private HolidayChangedEvent(final LocalDate from, final LocalDate to, final long practiceId) {
        this.from = from;
        this.to = to;
        this.practiceId = practiceId;
    }

    public static HolidayChangedEvent of(LocalDate from, LocalDate to, long practiceId) {
        return new HolidayChangedEvent(from, to, practiceId);
    }

    public LocalDate getFrom() {
        return from;
    }

    public LocalDate getTo() {
        return to;
    }

    public long getPracticeId() {
        return practiceId;
    }
}
