package com.arzttermine.service.calendar.service.event.model;

public class AvailabilityRemovedEvent
{
    private final long availabilityId;
    private final long practiceId;

    private AvailabilityRemovedEvent(long availabilityId, long practiceId) {
        this.availabilityId = availabilityId;
        this.practiceId = practiceId;
    }

    public static AvailabilityRemovedEvent of(long availabilityId, long practiceId) {
        return new AvailabilityRemovedEvent(availabilityId, practiceId);
    }

    public long getAvailabilityId() {
        return availabilityId;
    }

    public long getPracticeId() {
        return practiceId;
    }
}
