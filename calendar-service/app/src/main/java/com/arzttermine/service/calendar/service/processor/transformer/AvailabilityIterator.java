package com.arzttermine.service.calendar.service.processor.transformer;

import com.arzttermine.library.suport.exception.ResponseErrorListException;
import com.arzttermine.service.calendar.domain.entity.Availability;
import com.arzttermine.service.calendar.domain.repository.AvailabilityRepository;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nullable;
import java.time.Clock;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class AvailabilityIterator
{
    private static final int DEFAULT_MONTH_IN_FUTURE = 3;

    private static final Logger log = LoggerFactory.getLogger(AvailabilityIterator.class);

    @Autowired(required = false)
    private Clock clock;

    @Autowired
    private AvailabilityToAppointmentTransformer appointmentTransformer;

    @Autowired
    private AppointmentChangeCrudTranslator crudTranslator;

    @Autowired
    private AvailabilityRepository availabilityRepository;

    @Async
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW, noRollbackFor = {ResponseErrorListException.class, EmptyResultDataAccessException.class})
    public void createOrUpdateRanges(final long availabilityId, final PracticeDto practice) {
        createOrUpdateRanges(availabilityId, practice, null, null);
    }

    @Async
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW, noRollbackFor = {ResponseErrorListException.class, EmptyResultDataAccessException.class})
    public void createOrUpdateRanges(final long availabilityId, final PracticeDto practice, final @Nullable OffsetDateTime begin, final @Nullable OffsetDateTime maxFuture) {
        final Availability availability = availabilityRepository.findOne(availabilityId);
        final Collection<AvailabilityToAppointmentTransformer.AppointmentChange> changes = new ArrayList<>();

        final OffsetDateTime today = ((null == clock) ? Instant.now() : Instant.now(clock)).atOffset(ZoneOffset.UTC);
        OffsetDateTime pointer = Optional.ofNullable(begin).orElse(today).withSecond(0).withNano(0);

        if (pointer.isBefore(today)) {
            pointer = today.withSecond(0).withNano(0);
        }

        OffsetDateTime endOfGeneration = (!availability.isRecurring())
            ? availability.getEnd().atOffset(ZoneOffset.UTC)
            : Optional.ofNullable(maxFuture).orElse(today.plusMonths(DEFAULT_MONTH_IN_FUTURE));

        if (availability.isRecurring() && null != availability.getRecurringConfig().getEndsAt()) {
            final OffsetDateTime maxEnd = availability.getRecurringConfig().getEndsAt().atOffset(ZoneOffset.UTC);

            if (endOfGeneration.isAfter(maxEnd)) {
                endOfGeneration = maxEnd;
            }
        }


        Optional.of(appointmentRemovalChangesOutsideOf(availability))
            .filter(list -> !list.isEmpty())
            .ifPresent(changes::addAll);

        log.info("generating range between {} and {} for availability {}", pointer.toLocalDate(), endOfGeneration.toLocalDate(), availability);

        while (pointer.isBefore(endOfGeneration)) {
            changes.addAll(appointmentTransformer.transform(availability, practice, pointer));
            pointer = pointer.plusDays(1);
        }

        crudTranslator.applyChanges(availability, changes, practice);
        availability.iterationDone(pointer.toInstant());
    }

    @Async
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW, noRollbackFor = {ResponseErrorListException.class, EmptyResultDataAccessException.class})
    public void createOrUpdateDayRange(final long availabilityId, final PracticeDto practice, final Instant atDate)
    {
        final OffsetDateTime dateTime = atDate.atOffset(ZoneOffset.UTC);

        final Availability availability = availabilityRepository.findOne(availabilityId);
        log.info("updating appointments on date {} from availability {}", dateTime.toLocalDate(), availability);

        final Collection<AvailabilityToAppointmentTransformer.AppointmentChange> changes =
            appointmentTransformer.transform(availability, practice, dateTime);

        Optional.of(appointmentRemovalChangesOutsideOf(availability))
            .filter(list -> !list.isEmpty())
            .ifPresent(changes::addAll);

        crudTranslator.applyChanges(availability, changes, practice);
    }

    /**
     * fire delete change for appointments outside the availabilities recurring config
     * appointmentTransformer.transform will never touch the same appointments due to {@code endOfGeneration}
     **/
    private Collection<AvailabilityToAppointmentTransformer.AppointmentChange> appointmentRemovalChangesOutsideOf(Availability availability) {
        return availability.getAppointments().stream()
            .filter(a -> !a.isBooked())
            .filter(availability::isAppointmentOutsideOfRange)
            .map(a -> {
                logIfDebugEnabled("removing unbooked appointment {} from availability {} due to shortened availability", a, availability);

                return new AvailabilityToAppointmentTransformer.AppointmentChange(a.getId());
            })
            .collect(Collectors.toList());
    }

    private void logIfDebugEnabled(String message, Object... params) {
        if (log.isDebugEnabled())
            log.debug(message, params);
    }
}
