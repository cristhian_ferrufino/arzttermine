package com.arzttermine.service.calendar.config.settings.client;

import com.arzttermine.library.client.config.settings.ClientHostSettings;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(
    locations = "classpath:${spring.profiles.active:default}/clients.yml",
    prefix = "profile"
)
public class ProfileClientSettings extends ClientHostSettings
{
}
