package com.arzttermine.service.calendar.service.mapper;

import com.arzttermine.library.web.dto.response.paging.PageDto;
import com.arzttermine.service.calendar.domain.entity.Holiday;
import com.arzttermine.service.calendar.web.dto.request.CreateHolidaysDto;
import com.arzttermine.service.calendar.web.dto.response.HolidayDto;
import com.arzttermine.service.calendar.web.dto.response.paging.HolidayPageDto;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.time.ZoneOffset;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Component
public class HolidayMapper
{
    public List<Holiday> fromRequest(final CreateHolidaysDto request)
    {
        return request.getTimes().stream()
            .map(range -> Holiday.builder()
                .begin(range.getBegin().atStartOfDay().toInstant(ZoneOffset.UTC))
                .end(Optional.ofNullable(range.getEnd()).orElse(range.getBegin()).atStartOfDay().toInstant(ZoneOffset.UTC))
                .publicHoliday(Optional.ofNullable(range.getPublicHoliday()).orElse(false))
                .practiceId(request.getPracticeId())
                .resources(range.getResourceIds())
                .name(range.getName())
                .build())
            .collect(Collectors.toList());
    }

    public HolidayDto toDto(Holiday holiday)
    {
        final HolidayDto dto = new HolidayDto();
        dto.setBegin(holiday.getBegin().atOffset(ZoneOffset.UTC).toLocalDate());
        dto.setEnd(holiday.getEnd().atOffset(ZoneOffset.UTC).toLocalDate());
        dto.setPracticeId(holiday.getPracticeId());
        dto.setResourceIds(holiday.getResourceIds());
        dto.setPublicHoliday(holiday.isPublicHoliday());
        dto.setName(holiday.getName());
        dto.setId(holiday.getId());

        return dto;
    }

    public HolidayPageDto toPageDto(final Page<Holiday> holidayPage, final Predicate<Holiday> permissionCheck)
    {
        final PageDto page = new PageDto();
        page.setCurrentPage(page.getCurrentPage());
        page.setPageSize(page.getPageSize());
        page.setTotalItems(page.getTotalItems());
        page.setTotalPages(page.getTotalPages());

        final HolidayPageDto result = new HolidayPageDto();
        result.setPage(page);

        result.setElements(holidayPage.getContent().stream()
            .filter(permissionCheck)
            .map(this::toDto)
            .collect(Collectors.toList()));

        return result;
    }
}
