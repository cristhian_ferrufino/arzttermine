package com.arzttermine.service.calendar.service.event.model;

import java.time.Instant;

public class AvailabilityChangedEvent
{
    private final long availabilityId;
    private final long practiceId;
    private final Instant availabilityStart;
    private final boolean multipleDayRanges;

    private AvailabilityChangedEvent(long id, long practiceId, final Instant rangeUpdateStart, final boolean multipleDayRanges) {
        this.availabilityId = id;
        this.practiceId = practiceId;
        this.availabilityStart = rangeUpdateStart;
        this.multipleDayRanges = multipleDayRanges;
    }

    public static AvailabilityChangedEvent of(long id, long practiceId, Instant availabilityStart, boolean multipleDayRanges) {
        return new AvailabilityChangedEvent(id, practiceId, availabilityStart, multipleDayRanges);
    }

    public long getAvailabilityId() {
        return availabilityId;
    }

    public long getPracticeId() {
        return practiceId;
    }

    public Instant getAvailabilityStart() {
        return availabilityStart;
    }

    /**
     * @return boolean indicating basically whether the availability is recurring or not.
     *                 Can also be used to regenerate appointments only for a certain date {@code availabilityStart}
     */
    public boolean isMultipleDayRanges() {
        return multipleDayRanges;
    }
}
