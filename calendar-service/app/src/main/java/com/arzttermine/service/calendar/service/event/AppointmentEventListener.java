package com.arzttermine.service.calendar.service.event;

import com.arzttermine.library.elasticsearch.model.BulkAction;
import com.arzttermine.library.elasticsearch.service.DocumentProcessor;
import com.arzttermine.library.messaging.processor.MessageBus;
import com.arzttermine.service.calendar.domain.entity.Appointment;
import com.arzttermine.service.calendar.domain.repository.AppointmentRepository;
import com.arzttermine.service.calendar.messages.AppointmentChangedMessage;
import com.arzttermine.service.calendar.service.event.model.AppointmentChangedEvent;
import com.arzttermine.service.calendar.service.event.model.AppointmentReservationEvent;
import com.arzttermine.service.calendar.service.event.model.AppointmentsBulkChangeEvent;
import com.arzttermine.service.calendar.service.event.model.AppointmentsDisabledEvent;
import com.arzttermine.service.calendar.service.event.model.AppointmentsUpdatedEvent;
import com.arzttermine.service.calendar.service.externals.PracticeApi;
import com.arzttermine.service.calendar.service.mapper.AppointmentMapper;
import com.arzttermine.service.calendar.web.dto.response.AppointmentDto;
import com.arzttermine.service.calendar.web.dto.response.AppointmentLocation;
import com.arzttermine.service.notifications.web.struct.NotificationType;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionalEventListener;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @TODO clean up
 */
@Component
public class AppointmentEventListener
{
    private static final Logger log = LoggerFactory.getLogger(AppointmentEventListener.class);

    @Autowired
    private MessageBus messageBus;

    @Autowired
    private DocumentProcessor processor;

    @Autowired
    private AppointmentMapper mapper;

    @Autowired
    private PracticeApi practiceClient;

    @Autowired
    private AppointmentRepository repository;

    @Async
    @TransactionalEventListener
    public void appointmentChanged(final AppointmentChangedEvent change)
    {
        log.info("appointment {} was changed to {} using notification types: {}", change.getPrevious().toString(), change.getUpdated().toString(), change.getNotificationTypes().stream()
            .map(NotificationType::name)
            .collect(Collectors.joining(", ")));

        final AppointmentChangedMessage message = new AppointmentChangedMessage();
        message.setPreviousAppointmentStart(Date.from(change.getPrevious().getStart()));
        message.setBookedByCustomerId(change.getUpdated().getBookedByCustomer());
        message.setNewId(change.getUpdated().getId());
        message.setPrevious(change.getPrevious().getId());
        message.getNotificationTypes().addAll(change.getNotificationTypes());

        messageBus.sendAsync(Collections.singletonList(message));

        final AppointmentDto previous = new AppointmentDto();
        previous.setId(change.getPrevious().getId());

        BulkAction delete = BulkAction.delete(previous);
        delete.setIndexType(AppointmentDto.indexType(change.getPrevious().getPracticeId()));
        delete.setIndex("appointments");

        final List<BulkAction> actions = new ArrayList<>();
        actions.add(delete);

        if (!change.getUpdated().isBooked()) {
            actions.add(BulkAction.index(mapper.toDto(change.getUpdated())));
        }

        processor.executeAsync("appointments", AppointmentDto.indexType(change.getUpdated().getPracticeId()), actions);
    }

    @Async
    @TransactionalEventListener
    public void appointmentsDisabled(final AppointmentsDisabledEvent change)
    {
        if (change.getDisabled().isEmpty()) {
            return;
        }

        String ids = change.getDisabled().stream().map(Appointment::getId).map(String::valueOf).collect(Collectors.joining(","));
        log.info("deleted appointments {}, removing documents from elasticsearch...", ids);

        final Map<Long, List<Appointment>> actions = change.getDisabled().stream()
            .collect(Collectors.groupingBy(Appointment::getPracticeId));

        actions.entrySet()
            .forEach(e -> processor.executeAsync("appointments", AppointmentDto.indexType(e.getKey()), e.getValue().stream()
            .map(a -> {
                final AppointmentDto dto = new AppointmentDto();
                dto.setId(a.getId());

                return BulkAction.delete(dto);
            }).collect(Collectors.toList())));
    }

    @Async
    @TransactionalEventListener
    @Transactional(readOnly = true)
    public void bulkAppointmentsUpdate(AppointmentsBulkChangeEvent event)
    {
        if (event.getCreated().isEmpty() && event.getDeleted().isEmpty()) {
            return;
        }

        log.info(
            "bulk actions on appointments: created/updated {}, deleted {}, trying to update appointments in elasticsearch...",
            event.getCreated().size(),
            event.getDeleted().size()
        );

        final PracticeDto practice = event.getPractice();
        final Collection<BulkAction> actions = new ArrayList<>();

        if (!event.getCreated().isEmpty()) {
            repository.findAll(event.getCreated()).forEach(a -> {
                final AppointmentDto doc = mapper.toDto(a, practice);
                actions.add(BulkAction.index(doc));
            });
        }

        event.getDeleted().stream().distinct().forEach(id -> {
            final AppointmentDto doc = new AppointmentDto();
            doc.setId(id);

            AppointmentLocation location = new AppointmentLocation();
            location.setId(practice.getId());
            doc.setPractice(location);

            actions.add(BulkAction.delete(doc));
        });

        actions.stream()
            .collect(Collectors.groupingBy(ba -> Optional.ofNullable(((AppointmentDto) ba.getDocument()).getPractice()).map(AppointmentLocation::getId).orElse(0L)))
            .forEach((key, value) -> processor.executeAsync("appointments", AppointmentDto.indexType(key), value));
    }

    @Async
    @TransactionalEventListener
    @Transactional(readOnly = true)
    public void appointmentsUpdated(AppointmentsUpdatedEvent updates)
    {
        final Collection<Appointment> appointments = Lists.newArrayList(repository.findAll(updates.getAffectedIds()));
        final Map<Long, List<Appointment>> entries = appointments.stream().collect(Collectors.groupingBy(Appointment::getPracticeId));
        final Map<Long, PracticeDto> practices = practiceClient.findByIds(entries.keySet());

        entries.entrySet().forEach(e -> processor.executeAsync("appointments", AppointmentDto.indexType(e.getKey()), e.getValue().stream()
            .map(a -> mapper.toDto(a, practices.get(a.getPracticeId())))
            .map(BulkAction::update)
            .collect(Collectors.toList())));
    }

    @Async
    @TransactionalEventListener
    @Transactional(readOnly = true)
    public void bookingStatusChanged(AppointmentReservationEvent reservationEvent)
    {
        final List<BulkAction> actions = new ArrayList<>(1);
        final Appointment readOnlyAppointment = repository.findOne(reservationEvent.getAppointmentId());

        if (readOnlyAppointment.isBooked()) {
            final AppointmentDto dto = new AppointmentDto();
            dto.setId(reservationEvent.getAppointmentId());

            actions.add(BulkAction.delete(dto));
        }
        else {

            final PracticeDto practice = practiceClient.findById(readOnlyAppointment.getPracticeId());
            final AppointmentDto dto = mapper.toDto(readOnlyAppointment, practice);

            actions.add(BulkAction.index(dto));
        }

        processor.execute("appointments", AppointmentDto.indexType(readOnlyAppointment.getPracticeId()), actions);
    }
}
