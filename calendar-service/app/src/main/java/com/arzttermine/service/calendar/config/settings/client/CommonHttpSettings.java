package com.arzttermine.service.calendar.config.settings.client;

import com.arzttermine.library.client.config.settings.HttpClientSettings;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(
    locations = "classpath:${spring.profiles.active:default}/clients.yml",
    prefix = "common"
)
public class CommonHttpSettings extends HttpClientSettings
{
}
