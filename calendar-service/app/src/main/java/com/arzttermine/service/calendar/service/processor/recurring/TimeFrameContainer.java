package com.arzttermine.service.calendar.service.processor.recurring;

import com.arzttermine.service.calendar.domain.entity.Availability;
import com.arzttermine.service.calendar.service.mapper.AvailabilityMapper;
import com.arzttermine.service.calendar.service.processor.transformer.exclusion.ExcludedAvailabilityStrategy;
import com.arzttermine.service.calendar.web.dto.response.AvailabilityTimeFrameDto;

import java.time.Duration;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public abstract class TimeFrameContainer
{
    protected final AvailabilityMapper mapper;

    TimeFrameContainer(AvailabilityMapper mapper) {
        this.mapper = mapper;
    }

    /**
     * {@TODO use {@link com.arzttermine.service.calendar.service.processor.transformer.exclusion.ExclusionStrategy}} !!
     *
     * @param frames
     * @param availability
     * @param resourceIds
     * @param frame
     */
    void addFrameToAllResources(Map<Long, List<AvailabilityTimeFrameDto>> frames, final Availability availability, final Collection<Long> resourceIds, final AvailabilityTimeFrameDto frame) {
        // perform a final check to make sure no "unwanted" data is returned
        final Instant frameStart = frame.getBegin().toInstant();

        if (frameStart.isBefore(availability.getBegin())) {
            return;
        }
        if (availability.isRecurring() && availability.getRecurringConfig().getEndsAt() != null
            && availability.getRecurringConfig().getEndsAt().isBefore(frameStart)) {
            return;
        }

        // check if frame is on an excluded day (eg if availability was removed for a certain day)
        if (availability.getExcludedDays().stream()
            .map(d -> d.atOffset(ZoneOffset.UTC))
            .map(d -> Duration.between(d, frameStart.atOffset(ZoneOffset.UTC).withHour(0).withMinute(0).withSecond(0)))
            .anyMatch(d -> d.toMinutes() == 0)) {
            return;
        }

        addFrame(frames, availability, resourceIds, frame);
    }

    void addFrame(Map<Long, List<AvailabilityTimeFrameDto>> frames, Availability availability, Collection<Long> resourceIds, AvailabilityTimeFrameDto frame) {
        // iterating over availabilities resources makes sure no other resources can be used via the request
        availability.getResources().stream()
            .filter(r -> resourceIds.contains(r.getResourceId()))
            .forEach(r -> {
                frames.computeIfAbsent(r.getResourceId(), (key) -> new LinkedList<>());
                frames.get(r.getResourceId()).add(frame);
            });
    }
}
