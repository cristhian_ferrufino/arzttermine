package com.arzttermine.service.calendar.exception;

import com.arzttermine.library.suport.exception.BusinessException;

import java.time.Instant;
import java.util.Collection;
import java.util.stream.Collectors;

public class RangeAlreadyBlockedException extends BusinessException
{
    public RangeAlreadyBlockedException(Collection<Long> resourceIds, Instant from, Instant to) {
        super(
            "RANGE_ALREADY_BLOCKED",
            resourceIds.stream().map(String::valueOf).distinct().collect(Collectors.joining(",")),
            "The range between %s and %s is already blocked for some resources", from, to
        );
    }
}
