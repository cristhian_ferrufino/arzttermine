package com.arzttermine.service.calendar.service.event;

import com.arzttermine.library.suport.exception.ServiceUnavailableException;
import com.arzttermine.service.calendar.domain.repository.AvailabilityRepository;
import com.arzttermine.service.calendar.service.event.model.HolidayChangedEvent;
import com.arzttermine.service.calendar.service.externals.PracticeApi;
import com.arzttermine.service.calendar.service.processor.transformer.AvailabilityIterator;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import com.arzttermine.service.profile.web.dto.response.ResourceDto;
import org.hibernate.dialect.lock.LockingStrategyException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Collection;
import java.util.stream.Collectors;

@Component
public class HolidayEventListener
{
    @Autowired
    private AvailabilityRepository availabilityRepository;

    @Autowired
    private AvailabilityIterator availabilityIterator;

    @Autowired
    private PracticeApi practiceClient;

    @Async
    @TransactionalEventListener
    @Retryable(maxAttempts = 5, include = {ServiceUnavailableException.class, LockingStrategyException.class}, backoff = @Backoff(value = 5000, multiplier = 3))
    public void holidayChanged(HolidayChangedEvent event) {

        final PracticeDto practice = practiceClient.findById(event.getPracticeId());

        final Collection<Long> resourceIds = practice.getResources().stream()
            .map(ResourceDto::getId)
            .collect(Collectors.toSet());

        final OffsetDateTime from = event.getFrom().atStartOfDay().atOffset(ZoneOffset.UTC);
        final OffsetDateTime to = event.getTo().atTime(LocalTime.MAX).atOffset(ZoneOffset.UTC);

        availabilityRepository.search(practice.getId(), from.toInstant(), to.toInstant(), resourceIds, new Sort("id"))
            .forEach(availability -> availabilityIterator.createOrUpdateRanges(availability.getId(), practice, from, to));
    }
}
