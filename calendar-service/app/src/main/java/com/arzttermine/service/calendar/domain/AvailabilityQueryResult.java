package com.arzttermine.service.calendar.domain;

import java.time.Instant;

public class AvailabilityQueryResult
{
    private final Instant lastIteration;
    private final long availabilityId;
    private final long practiceId;

    public AvailabilityQueryResult(final long availabilityId, final long practiceId, final Instant lastIteration) {
        this.availabilityId = availabilityId;
        this.lastIteration = lastIteration;
        this.practiceId = practiceId;
    }

    public Instant getLastIteration() {
        return lastIteration;
    }

    public long getAvailabilityId() {
        return availabilityId;
    }

    public long getPracticeId() {
        return practiceId;
    }
}
