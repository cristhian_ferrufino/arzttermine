package com.arzttermine.service.calendar;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableAsync
@EnableRetry
@EnableScheduling
@EnableJpaRepositories
@EnableAspectJAutoProxy
@EnableTransactionManagement
@ComponentScans({
    @ComponentScan("com.arzttermine.service.calendar"),
    @ComponentScan("com.arzttermine.library.spring")
})
public class Application
{
    public static void main(String[] args)
    {
        new SpringApplicationBuilder()
            .sources(Application.class)
            .run(args);
    }
}
