package com.arzttermine.service.calendar.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
public class ExecutorConfiguration
{
    @Value("${processing.executor.corePoolSize}")
    private int corePoolSize;

    @Value("${processing.executor.awaitTerminationSeconds}")
    private int awaitTerminationSeconds;

    @Bean
    public ExecutorService executorService()
    {
        return Executors.newScheduledThreadPool(corePoolSize);
    }

    @Bean
    public TaskExecutor taskExecutor()
    {
        final ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setAwaitTerminationSeconds(awaitTerminationSeconds);
        executor.setWaitForTasksToCompleteOnShutdown(true);
        executor.setCorePoolSize(corePoolSize);

        return executor;
    }
}
