package com.arzttermine.service.calendar.domain.entity;

import com.arzttermine.service.calendar.support.InstantToTimestampConverter;

import javax.persistence.*;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

@Entity
@Table(name = "appointment_resources")
public class AppointmentResource
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false, insertable = false)
    private long id;

    @Column(name = "resource_id", nullable = false)
    private long resourceId;

    @Column(nullable = false)
    @Convert(converter = InstantToTimestampConverter.class)
    private Instant begin;

    @Column(nullable = false)
    @Convert(converter = InstantToTimestampConverter.class)
    private Instant ending;

    @Column(nullable = false, columnDefinition = "bit NOT NULL DEFAULT 0")
    private boolean draft;

    public AppointmentResource() {
    }

    public AppointmentResource(long resourceId, boolean draft, Instant begin, Instant end) {
        this.begin = begin;
        this.ending = end;
        this.draft = draft;
        setResourceId(resourceId);
    }

    public AppointmentResource(long resourceId, Instant begin, Instant end) {
        this(resourceId, false, begin, end);
    }

    public AppointmentResource(long resourceId, Instant begin, int duration) {
        this(resourceId, begin, begin.plus(duration, ChronoUnit.MINUTES));
    }

    public AppointmentResource(AppointmentResource copy) {
        resourceId = copy.getResourceId();
        begin = copy.getBegin();
        ending = copy.getEnding();
        draft = copy.isDraft();
    }

    public long getId() {
        return id;
    }

    public long getResourceId() {
        return resourceId;
    }

    public void setResourceId(long resourceId) {
        this.resourceId = resourceId;
    }

    public Instant getBegin() {
        return begin;
    }

    public void setBegin(Instant begin) {
        this.begin = begin;
    }

    public Instant getEnding() {
        return ending;
    }

    public void setEnding(Instant ending) {
        this.ending = ending;
    }

    public boolean isDraft() {
        return draft;
    }

    public void setDraft(boolean draft) {
        this.draft = draft;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AppointmentResource that = (AppointmentResource) o;
        return id == that.id || resourceId == that.resourceId;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (resourceId ^ (resourceId >>> 32));
        return result;
    }
}
