package com.arzttermine.service.calendar.service.processor.recurring;

import com.arzttermine.service.calendar.domain.entity.Availability;
import com.arzttermine.service.calendar.domain.entity.RecurringConfiguration;
import com.arzttermine.service.calendar.service.mapper.AvailabilityMapper;
import com.arzttermine.service.calendar.web.dto.response.AvailabilityTimeFrameDto;

import java.time.Duration;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class DailyRecurringStrategy extends TimeFrameContainer implements AvailabilityRecurringStrategy
{
    public DailyRecurringStrategy(AvailabilityMapper mapper) {
        super(mapper);
    }

    @Override
    public void generateFor(Map<Long, List<AvailabilityTimeFrameDto>> frames, Availability availability, Instant begin, Instant end, Duration duration, Collection<Long> resourceIds) {
        OffsetDateTime inclusiveEnd = end.atOffset(ZoneOffset.UTC).plusDays(1).withHour(0).withMinute(0);
        OffsetDateTime pointer = begin.atOffset(ZoneOffset.UTC);

        final OffsetDateTime recurringEnd = Optional.ofNullable(availability.getRecurringConfig())
            .map(RecurringConfiguration::getEndsAt)
            .map(e -> e.atOffset(ZoneOffset.UTC))
            .orElse(null);

        if (null != recurringEnd && recurringEnd.isBefore(inclusiveEnd)) {
            inclusiveEnd = recurringEnd.plusDays(2).withHour(0).withMinute(0);
        }

        final OffsetDateTime initialBegin = availability.getBegin().atOffset(ZoneOffset.UTC);

        while (pointer.isBefore(inclusiveEnd)) {
            final OffsetDateTime nextAvailability = pointer
                .withHour(initialBegin.getHour())
                .withMinute(initialBegin.getMinute())
                .withSecond(0)
                .withNano(0);

            final AvailabilityTimeFrameDto frame = mapper.toDto(availability, nextAvailability.toInstant(), nextAvailability.plusMinutes(duration.toMinutes()).toInstant());

            addFrameToAllResources(frames, availability, resourceIds, frame);
            pointer = pointer.plus(1, ChronoUnit.DAYS);
        }
    }
}
