package com.arzttermine.service.calendar.service.processor.transformer.exclusion;

import com.arzttermine.service.calendar.domain.entity.Availability;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.OffsetDateTime;

public abstract class ExclusionStrategy
{
    private static final Logger log = LoggerFactory.getLogger(ExclusionStrategy.class);

    abstract boolean isExcluded(Availability availability, PracticeDto practice, OffsetDateTime atDate);

    public boolean isUnavailable(final Availability availability, final PracticeDto practice, final OffsetDateTime atDate) {

        final boolean unavailable = isExcluded(availability, practice, atDate);

        if (unavailable && log.isDebugEnabled())
            log.debug("excluding availability {} from date {} due to {}", availability.getId(), atDate.toLocalDate(), getClass());

        return unavailable;
    }
}
