package com.arzttermine.service.calendar.domain.repository;

import com.arzttermine.service.calendar.domain.entity.Appointment;
import com.arzttermine.service.calendar.domain.entity.QAppointment;
import com.querydsl.core.types.dsl.ComparableExpression;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.Collection;

@Repository
public interface AppointmentRepository extends PagingAndSortingRepository<Appointment, Long>,
                                               QueryDslPredicateExecutor<Appointment>,
                                               QuerydslBinderCustomizer<QAppointment>
{
    @Query("SELECT a FROM Appointment a WHERE a.id = :appointmentId AND a.enabled IS TRUE")
    Appointment findOne(@Param("appointmentId") long appointmentId);

    @Query("SELECT a FROM Appointment a WHERE a.id = :appointmentId AND a.enabled IS FALSE")
    Appointment findOrphanById(@Param("appointmentId") long appointmentId);

    @Query("SELECT a FROM Appointment a WHERE a.id IN (:ids) AND a.practiceId = :practice AND a.start > NOW() AND a.bookedByCustomer IS NULL AND a.enabled IS TRUE")
    Collection<Appointment> findAvailableFromPractice(@Param("practice") long practiceId, @Param("ids") Collection<Long> ids);

    @Query(
        "SELECT a FROM Appointment a JOIN a.resources ar WHERE a.practiceId = :practice AND a.enabled IS TRUE AND " +
        "(:start BETWEEN a.start AND a.end OR :end BETWEEN a.start AND a.end) AND (a.start != :end AND a.end != :start) AND " +
        "ar.resourceId IN (:resources)"
    )
    Collection<Appointment> findFromPracticeInRange(
        @Param("practice") long practiceId,
        @Param("start") Instant from,
        @Param("end") Instant to,
        @Param("resources") Collection<Long> resourceIds
    );

    @Query("SELECT a FROM Appointment a WHERE :id MEMBER OF a.treatmentTypes AND a.start > NOW() AND a.enabled IS TRUE")
    Collection<Appointment> findByTreatmentType(@Param("id") Long treatmentTypeId);

    Page<Appointment> findByPracticeId(long practiceId, Pageable pageable);

    @Override
    default void customize(QuerydslBindings bindings, QAppointment root) {
        bindings.bind(root.start).first(ComparableExpression::goe);
        bindings.bind(root.end).first(ComparableExpression::loe);
    }
}
