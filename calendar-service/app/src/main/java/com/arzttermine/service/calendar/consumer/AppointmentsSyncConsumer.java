package com.arzttermine.service.calendar.consumer;

import com.arzttermine.library.messaging.consumer.Consumer;
import com.arzttermine.library.suport.exception.BusinessException;
import com.arzttermine.service.booking.messages.AppointmentBookedMessage;
import com.arzttermine.service.booking.messages.BookingCancelledMessage;
import com.arzttermine.service.calendar.domain.entity.Appointment;
import com.arzttermine.service.calendar.domain.repository.AppointmentRepository;
import com.arzttermine.service.calendar.service.AppointmentService;
import com.arzttermine.service.calendar.service.event.model.AppointmentsUpdatedEvent;
import com.arzttermine.service.profile.messages.TreatmentTypeChangedMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class AppointmentsSyncConsumer
{
    private static final Logger log = LoggerFactory.getLogger(AppointmentsSyncConsumer.class);

    @Autowired
    private AppointmentService appointmentService;

    @Autowired
    private AppointmentRepository repository;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Consumer
    public void appointmentBooked(AppointmentBookedMessage message)
    {
        log.info(
            "updating appointment {} after it was booked by customer {}",
            message.getAppointmentId(),
            message.getCustomerId()
        );

        try {
            appointmentService.updateAppointmentPatient(message.getAppointmentId(), message.getCustomerId(), false, Instant.now());
        } catch (BusinessException e) {
            log.warn("could not update appointment {}", message.getAppointmentId(), e);
        }
    }

    @Consumer
    public void appointmentCancelled(BookingCancelledMessage message)
    {
        log.info("releasing appointment {} after booking was cancelled", message.getAppointmentId());
        final boolean makeAvailable = Optional.ofNullable(message.isAppointmentAvailable()).orElse(true);

        try {
            appointmentService.updateAppointmentPatient(message.getAppointmentId(), null, makeAvailable, Instant.now());
        } catch (BusinessException e) {
            log.warn("could not update appointment {}", message.getAppointmentId(), e);
        }
    }

    @Consumer
    public void treatmentTypeChanged(TreatmentTypeChangedMessage message)
    {
        if (!message.isDeleted()) {
            return; // then we dont care
        }

        final Collection<Appointment> affected = repository.findByTreatmentType(message.getTreatmentTypeId());
        if (affected.isEmpty()) {
            return;
        }

        log.info("updating appointments {} after treatment type was deleted", affected.stream()
            .map(Appointment::getId)
            .map(String::valueOf)
            .collect(Collectors.joining(",")));

        affected.forEach(a -> a.getTreatmentTypes().remove(message.getTreatmentTypeId()));
        eventPublisher.publishEvent(AppointmentsUpdatedEvent.of(affected.stream().map(Appointment::getId).collect(Collectors.toList())));
    }
}
