package com.arzttermine.service.calendar.domain.repository;

import com.arzttermine.service.calendar.domain.entity.Holiday;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.Collection;

@Repository
public interface HolidayRepository extends PagingAndSortingRepository<Holiday, Long>, QueryDslPredicateExecutor<Holiday>
{
    @Cacheable
    @Query(
        "SELECT COUNT(h.id) FROM Holiday h WHERE h.active IS TRUE AND h.practiceId = :practice AND " +
        "((:resources) IN elements(h.resourceIds) OR h.publicHoliday IS TRUE) AND " +
        "((h.begin BETWEEN :from AND :end OR h.end BETWEEN :from AND :end) OR " +
        "(:from BETWEEN h.begin AND h.end OR :end BETWEEN h.begin AND h.end))"
    )
    int findInRange(@Param("practice") long practiceId, @Param("from") Instant from, @Param("end") Instant end, @Param("resources") Collection<Long> resourceIds);
}
