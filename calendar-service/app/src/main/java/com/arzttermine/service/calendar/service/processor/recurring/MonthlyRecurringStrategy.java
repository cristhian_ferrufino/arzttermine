package com.arzttermine.service.calendar.service.processor.recurring;

import com.arzttermine.service.calendar.domain.entity.Availability;
import com.arzttermine.service.calendar.service.mapper.AvailabilityMapper;
import com.arzttermine.service.calendar.web.dto.response.AvailabilityTimeFrameDto;

import java.time.Duration;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class MonthlyRecurringStrategy extends TimeFrameContainer implements AvailabilityRecurringStrategy
{
    public MonthlyRecurringStrategy(AvailabilityMapper mapper) {
        super(mapper);
    }

    @Override
    public void generateFor(Map<Long, List<AvailabilityTimeFrameDto>> frames, Availability availability, Instant begin, Instant end, Duration duration, Collection<Long> resourceIds) {

        final OffsetDateTime initialBegin = availability.getBegin().atOffset(ZoneOffset.UTC);

        OffsetDateTime pointer = begin.atOffset(ZoneOffset.UTC);

        final int atDate = initialBegin.getDayOfMonth();
        final long months = ChronoUnit.MONTHS.between(pointer.withDayOfMonth(1), end.atOffset(ZoneOffset.UTC).withDayOfMonth(1).plusMonths(1));

        for (int month = 0; month < months; month++) {
            final Instant nextAvailability = pointer
                .withDayOfMonth(atDate)
                .plus(month, ChronoUnit.MONTHS)
                .withHour(initialBegin.getHour())
                .withMinute(initialBegin.getMinute())
                .withSecond(0)
                .withNano(0)
                .toInstant();

            if (nextAvailability.isBefore(begin) || nextAvailability.isAfter(end)) {
                continue;
            }

            final AvailabilityTimeFrameDto frame = mapper.toDto(availability, nextAvailability, nextAvailability.plus(duration.toMinutes(), ChronoUnit.MINUTES));
            addFrameToAllResources(frames, availability, resourceIds, frame);
        }
    }
}
