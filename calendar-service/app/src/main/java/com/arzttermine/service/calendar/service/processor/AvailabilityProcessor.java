package com.arzttermine.service.calendar.service.processor;

import com.arzttermine.service.calendar.domain.entity.AppointmentResource;
import com.arzttermine.service.calendar.domain.entity.Availability;
import com.arzttermine.service.calendar.domain.repository.AvailabilityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.ZoneOffset;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class AvailabilityProcessor
{
    private static final Logger log = LoggerFactory.getLogger(AvailabilityProcessor.class);

    @Autowired
    private AvailabilityRepository repository;

    public void mergeOverlappingInto(final Availability target) {
        final List<Availability> overlapping = repository.findOverlapping(
            target.getId(),
            target.getPracticeId(),
            target.getBegin(),
            target.getEnd(),
            target.getDuration(),
            target.getResources().stream().unordered().map(AppointmentResource::getResourceId).collect(Collectors.toSet())
        ).stream()
            .filter(ov -> ov.getTreatmentTypes().containsAll(target.getTreatmentTypes()))
            .filter(ov -> ov.getTreatmentTypes().size() == target.getTreatmentTypes().size())
            .filter(ov -> ov.getInsuranceTypes().containsAll(target.getInsuranceTypes()))
            .filter(ov -> ov.getInsuranceTypes().size() == target.getInsuranceTypes().size())
            // either left hand side has a recurring config which is equal to {@code target} or both are null
            .filter(ov -> (ov.getRecurringConfig() != null && ov.getRecurringConfig().equals(target.getRecurringConfig())) || (ov.getRecurringConfig() == null && target.getRecurringConfig() == null))
            .collect(Collectors.toList());

        if (overlapping.isEmpty()) {
            return;
        }

        log.info("found {} overlapping availabilities for {}", overlapping.size(), target);

        // merge overlapping availabilities into target that the timeframe resizes
        overlapping.forEach(ab -> {
            if (ab.getBegin().isBefore(target.getBegin())) {
                target.setBegin(ab.getBegin());

                if (target.isRecurring()) {
                    if (target.getRecurringConfig().getStartsAt().isAfter(ab.getBegin())) {
                        target.getRecurringConfig().setStartsAt(ab.getBegin());
                    }

                    target.getExcludedDays().addAll(ab.getExcludedDays().stream()
                        .filter(d -> !target.getExcludedDays().contains(d))
                        .map(d -> d.atOffset(ZoneOffset.UTC).toInstant()) // copy entity
                        .collect(Collectors.toList()));
                }
            }

            if (ab.getEnd().isAfter(target.getEnd())) {
                target.setEnd(ab.getEnd());
            }

            target.getAppointments().addAll(ab.getAppointments());
            ab.getAppointments().clear();
        });

        repository.delete(overlapping);
    }
}
