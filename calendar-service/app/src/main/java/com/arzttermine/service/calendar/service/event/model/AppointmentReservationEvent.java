package com.arzttermine.service.calendar.service.event.model;

public final class AppointmentReservationEvent
{
    private final long appointmentId;

    private AppointmentReservationEvent(long appointmentId) {
        this.appointmentId = appointmentId;
    }

    public static AppointmentReservationEvent of(long affectedAppointmentId) {
        return new AppointmentReservationEvent(affectedAppointmentId);
    }

    public long getAppointmentId() {
        return appointmentId;
    }
}
