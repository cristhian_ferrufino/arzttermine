package com.arzttermine.service.calendar.service.processor;

import com.arzttermine.library.suport.exception.ResponseErrorListException;
import com.arzttermine.library.web.dto.response.ErrorDto;
import com.arzttermine.library.web.struct.HttpStatus;
import com.arzttermine.service.calendar.domain.entity.Appointment;
import com.arzttermine.service.calendar.domain.entity.AppointmentResource;
import com.arzttermine.service.calendar.domain.repository.AppointmentResourceRepository;
import com.arzttermine.service.calendar.web.dto.request.ResourceRequestDto;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class ResourceAvailabilityProcessor
{
    @Autowired
    private AppointmentResourceRepository resourceRepository;

    /**
     * Checks the availability of resources and
     * resolves these resources to appointment related resources.
     *
     * @TODO maybe introduce handling of optional resources
     *
     * @param appointment to attach the resources to
     * @param practice owner of resources
     * @param resources requested resources
     * @param excluded ids of
     * @param noValidationRequired do not validate availability, only attach resources to appointment
     *
     * @throws ResponseErrorListException if at least one resource is not available
     */
    public void resolveAndAttachResources(final Appointment appointment, final PracticeDto practice, final Collection<ResourceRequestDto> resources, final Collection<Long> excluded, final boolean noValidationRequired)
    {
        if (resources.isEmpty()) {
            return;
        }

        final Collection<ResourceRequestDto> invalidResources = resources.stream()
            .filter(r -> practice.getResources().stream().noneMatch(pr -> Objects.equals(pr.getId(), r.getResourceId())))
            .collect(Collectors.toList());

        if (!invalidResources.isEmpty()) {
            throw new ResponseErrorListException(invalidResources.stream()
                .map(r -> new ErrorDto("RESOURCE_NOT_AVAILABLE", r.getResourceId(), "The requested resource is not available in the practice"))
                .collect(Collectors.toList()), HttpStatus.CONFLICT);
        }

        if (noValidationRequired) {
            attachResources(appointment, resources);
            return;
        }

        final Collection<Long> requestedIds = resources.stream()
            .map(ResourceRequestDto::getResourceId)
            .collect(Collectors.toList());

        final Collection<ErrorDto> errors = ((excluded.isEmpty())
            ? resourceRepository.findOccupiedResources(requestedIds, appointment.getStart(), appointment.getEnd())
            : resourceRepository.findOtherOccupiedResources(requestedIds, appointment.getStart(), appointment.getEnd(), excluded))
            .stream()
            .map(occupied -> new ErrorDto(
                "RESOURCE_NOT_AVAILABLE",
                occupied,
                "The requested resource is not available in the specified time range"
            ))
            .collect(Collectors.toMap(ErrorDto::getReference, Function.identity(), (a, b) -> a))
            .values();

        if (!errors.isEmpty()) {
            throw new ResponseErrorListException(errors, HttpStatus.CONFLICT);
        }

        attachResources(appointment, resources);
    }

    /**
     * convinient method without excluded resources
     *
     * @param appointment
     * @param practice
     * @param resources
     * @param noValidationRequired
     */
    public void resolveAndAttachResources(final Appointment appointment, final PracticeDto practice, final Collection<ResourceRequestDto> resources, final boolean noValidationRequired)
    {
        resolveAndAttachResources(appointment, practice, resources, Collections.emptyList(), noValidationRequired);
    }

    private void attachResources(final Appointment appointment, final Collection<ResourceRequestDto> resources)
    {
        resources.forEach(r -> {
            final AppointmentResource reservation = new AppointmentResource();
            reservation.setResourceId(r.getResourceId());
            reservation.setBegin(appointment.getStart());
            reservation.setEnding(appointment.getEnd());
            appointment.getResources().add(reservation);
        });
    }
}
