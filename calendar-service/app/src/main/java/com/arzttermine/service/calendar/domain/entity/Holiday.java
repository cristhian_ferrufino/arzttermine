package com.arzttermine.service.calendar.domain.entity;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.util.Assert;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import java.time.Instant;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "holidays")
public class Holiday extends CalendarTimeRange
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false, insertable = false)
    private long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private boolean publicHoliday = false;

    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    @Column(name = "resource_id", nullable = false)
    @CollectionTable(
        name = "holidays_resource_ids",
        joinColumns = @JoinColumn(name = "holiday_id", nullable = false)
    )
    private Set<Long> resourceIds = new HashSet<>();

    // jpa
    protected Holiday() {
    }

    public Holiday(Builder builder) {
        this.name = builder.name;
        this.begin = builder.begin;
        this.end = builder.end;
        this.publicHoliday = builder.publicHoliday;
        this.resourceIds = builder.resourceIds;
        this.practiceId = builder.practiceId;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Set<Long> getResourceIds() {
        return resourceIds;
    }

    public boolean isPublicHoliday() {
        return publicHoliday;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder
    {
        private String name;
        private Instant begin;
        private Instant end;
        private Long practiceId;
        private boolean publicHoliday = false;
        private Set<Long> resourceIds = new HashSet<>();

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder begin(Instant begin) {
            this.begin = begin;
            return this;
        }

        public Builder end(Instant end) {
            this.end = end;
            return this;
        }

        public Builder publicHoliday(boolean publicHoliday) {
            this.publicHoliday = publicHoliday;
            return this;
        }

        public Builder resource(long id) {
            this.resourceIds.add(id);
            return this;
        }

        public Builder resources(Collection<Long> ids) {
            this.resourceIds.addAll(ids);
            return this;
        }

        public Builder practiceId(long id) {
            this.practiceId = id;
            return this;
        }

        public Holiday build() {
            Assert.notNull(practiceId, "practice id must be set");
            Assert.notNull(begin, "practice id must be set");
            Assert.notNull(end, "practice id must be set");

            return new Holiday(this);
        }
    }
}
