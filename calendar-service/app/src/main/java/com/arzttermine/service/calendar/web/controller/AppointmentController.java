package com.arzttermine.service.calendar.web.controller;

import com.arzttermine.service.calendar.domain.entity.Appointment;
import com.arzttermine.service.calendar.service.AppointmentService;
import com.arzttermine.service.calendar.web.dto.request.CreateAppointmentsDto;
import com.arzttermine.service.calendar.web.dto.request.MoveAppointmentDto;
import com.arzttermine.service.calendar.web.dto.request.UpdateAppointmentDto;
import com.arzttermine.service.calendar.web.dto.response.AppointmentDto;
import com.arzttermine.service.calendar.web.dto.response.paging.AppointmentsPageDto;
import com.querydsl.core.types.Predicate;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/appointments")
public class AppointmentController
{
    @Autowired
    private AppointmentService service;

    @ApiOperation("Returns a page of appointments, filtered by provided filter")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "page of appointments was returned")
    })
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyRole('ROLE_CLIENT', 'ROLE_CUSTOMER')")
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public AppointmentsPageDto getAppointments(@QuerydslPredicate(root = Appointment.class) Predicate predicate, Pageable pageable)
    {
        return service.getAppointments(predicate, pageable);
    }

    @ApiOperation(
        value = "Delete appointments by id",
        notes = "removing appointments which are booked, from a different practice or in the past are ignored"
    )
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        ),
        @ApiImplicitParam(
            name = "practiceId",
            dataType = "long",
            required = true,
            paramType = "path"
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "appointments are deleted")
    })
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyRole('ROLE_EMPLOYEE', 'ROLE_SYSTEM', 'ROLE_PRACTICE_' + #practiceId)")
    @RequestMapping(value = "/practices/{practiceId}", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Collection<Long> deleteAppointments(@PathVariable long practiceId, @RequestBody List<Long> ids)
    {
        return service.deleteAppointments(practiceId, ids);
    }

    @ApiOperation("Returns a single appointment")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        ),
        @ApiImplicitParam(
            name = "id",
            dataType = "long",
            paramType = "path",
            required = true
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "appointment was returned"),
        @ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "appointment was not found")
    })
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyRole('ROLE_CLIENT', 'ROLE_CUSTOMER')")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public AppointmentDto getAppointment(@ApiIgnore @PathVariable long id)
    {
        return service.getAppointment(id);
    }

    @ApiOperation("Returns a single orphan appointment")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        ),
        @ApiImplicitParam(
            name = "id",
            dataType = "long",
            paramType = "path",
            required = true
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "appointment was returned"),
        @ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "appointment was not found")
    })
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasRole('ROLE_CLIENT')")
    @RequestMapping(value = "/orphans/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public AppointmentDto getOrphanAppointment(@ApiIgnore @PathVariable long id)
    {
        return service.getOrphanAppointment(id);
    }

    @ApiOperation("Creates new appointments for a certain doctor")
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_CREATED, message = "appointment was created"),
        @ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "the doctorId or practiceId could not be verified"),
        @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "the request body is not valid"),
        @ApiResponse(code = HttpServletResponse.SC_CONFLICT, message = "could not create appointment due to business rules (eg: duplicate appointment etc.)")
    })
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasAnyRole('ROLE_DOCTOR', 'ROLE_EMPLOYEE', 'ROLE_SYSTEM')")
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<AppointmentDto> createAppointments(@RequestBody @Valid CreateAppointmentsDto dto)
    {
        return service.createAppointments(dto);
    }

    @ApiOperation("updates a given appointment (eg: wipeOut appointment, which will no longer be available)")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        ),
        @ApiImplicitParam(
            name = "id",
            dataType = "long",
            paramType = "path",
            required = true
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "appointment was updated"),
        @ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "appointment was not found"),
        @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "the request body is not valid")
    })
    @PreAuthorize("hasAnyRole('ROLE_DOCTOR', 'ROLE_EMPLOYEE', 'ROLE_SYSTEM')")
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{id}/move", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public AppointmentDto moveAppointment(@ApiIgnore @PathVariable long id, @RequestBody @Valid MoveAppointmentDto update)
    {
        return service.moveAppointment(id, update);
    }

    @ApiOperation("updates a given appointment")
    @ApiImplicitParams({
        @ApiImplicitParam(
            name = "Authorization",
            dataType = "string",
            required = true,
            paramType = "header"
        ),
        @ApiImplicitParam(
            name = "id",
            dataType = "long",
            paramType = "path",
            required = true
        )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "appointment was updated"),
        @ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "appointment or practice was not found"),
        @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "the request body is not valid")
    })
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyRole('ROLE_DOCTOR', 'ROLE_EMPLOYEE', 'ROLE_SYSTEM')")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public AppointmentDto updateAppointment(@ApiIgnore @PathVariable long id, @RequestBody @Valid UpdateAppointmentDto update)
    {
        return service.updateAppointment(id, update);
    }

    @ApiOperation("Reindexes all appointments for a practice")
    @ApiImplicitParams({
            @ApiImplicitParam(
                    name = "Authorization",
                    dataType = "string",
                    required = true,
                    paramType = "header"
            ),
            @ApiImplicitParam(
                    name = "practiceId",
                    dataType = "long",
                    required = true,
                    paramType = "path"
            )
    })
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_NO_CONTENT, message = "Appointments are reindexed.")
    })
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAnyRole('ROLE_EMPLOYEE', 'ROLE_SYSTEM', 'ROLE_PRACTICE_'+#practiceId)")
    @RequestMapping(value = "/practices/{practiceId}/reindex", method = RequestMethod.POST)
    public void reindexAppointments(@PathVariable long practiceId)
    {
        service.reindexAppointments(practiceId);
    }
}
