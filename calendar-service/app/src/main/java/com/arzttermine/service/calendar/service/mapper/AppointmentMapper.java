package com.arzttermine.service.calendar.service.mapper;

import com.arzttermine.service.calendar.domain.entity.Appointment;
import com.arzttermine.service.calendar.web.dto.request.CreateAppointmentsDto;
import com.arzttermine.service.calendar.web.dto.request.ResourceRequestDto;
import com.arzttermine.service.calendar.web.dto.response.AppointmentDto;
import com.arzttermine.service.calendar.web.dto.response.AppointmentLocation;
import com.arzttermine.service.profile.web.dto.response.DoctorProfileDto;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import com.arzttermine.service.profile.web.dto.response.TreatmentTypeDto;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import java.sql.Date;
import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class AppointmentMapper
{
    public Appointment fromRequest(Instant begin, CreateAppointmentsDto request, PracticeDto practiceDto)
    {
        final Appointment appointment = new Appointment();
        appointment.setPracticeId(request.getPracticeId());
        appointment.setDoctorId(request.getDoctorId());
        appointment.setInsuranceTypes(request.getInsuranceTypes());
        appointment.setEnd(begin.plusSeconds(request.getDuration() * 60));
        appointment.getLanguages().addAll(request.getLanguages());
        appointment.setStart(begin);

        final List<Long> availableTreatments = practiceDto.getTreatmentTypes().stream()
            .filter(tt -> tt.getDoctorIds().contains(request.getDoctorId()))
            .map(TreatmentTypeDto::getId)
            .collect(Collectors.toList());

        appointment.setTreatmentTypes(request.getTreatmentTypes().stream()
            .distinct()
            .filter(availableTreatments::contains)
            .collect(Collectors.toList()));

        return appointment;
    }

    public AppointmentDto toDto(Appointment appointment)
    {
        return toDto(appointment, appointment.getPractice());
    }

    public AppointmentDto toDto(Appointment appointment, @Nullable PracticeDto practice)
    {
        final int durationInMinutes = Math.round(appointment.getEnd()
            .minusSeconds(appointment.getStart().getEpochSecond())
            .getEpochSecond() / 60);

        final AppointmentDto dto = new AppointmentDto();
        dto.setAvailableInsuranceTypes(appointment.getInsuranceTypes());
        dto.setAvailableTreatmentTypes(appointment.getTreatmentTypes());
        dto.setStart(Date.from(appointment.getStart()));
        dto.setAvailableLanguages(appointment.getLanguages());
        dto.setEnd(Date.from(appointment.getEnd()));
        dto.setEnabled(appointment.isEnabled());
        dto.setBooked(appointment.isBooked());
        dto.setDuration(durationInMinutes);
        dto.setId(appointment.getId());

        dto.setResourceRequests(appointment.getResources().stream()
            .map(r -> new ResourceRequestDto(r.getResourceId()))
            .collect(Collectors.toList()));

        mapPracticeInfo(appointment, dto, practice);

        return dto;
    }

    public List<AppointmentDto> toDtos(Collection<Appointment> appointments, Map<Long, PracticeDto> practices)
    {
        return appointments.stream().map(a -> toDto(a, practices.get(a.getPracticeId()))).collect(Collectors.toList());
    }

    public AppointmentLocation toLocation(PracticeDto practice)
    {
        final AppointmentLocation location = new AppointmentLocation();
        location.setMedicalSpecialties(practice.getMedicalSpecialties());
        location.setLocation(practice.getLocation());
        location.setId(practice.getId());
        location.setName(practice.getName());

        return location;
    }

    private void mapPracticeInfo(final Appointment appointment, final AppointmentDto dto, @Nullable final PracticeDto practice)
    {
        if (null == practice) {
            return;
        }

        final DoctorProfileDto doctor = practice.getDoctors().stream()
            .filter(d -> d.getId() == appointment.getDoctorId())
            .findFirst()
            .orElse(null);

        dto.setDoctor(doctor);
        dto.setPractice(toLocation(practice));
    }
}
