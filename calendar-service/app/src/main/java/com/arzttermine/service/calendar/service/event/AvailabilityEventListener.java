package com.arzttermine.service.calendar.service.event;

import com.arzttermine.library.suport.exception.ServiceUnavailableException;
import com.arzttermine.service.calendar.domain.entity.Appointment;
import com.arzttermine.service.calendar.domain.entity.Availability;
import com.arzttermine.service.calendar.domain.repository.AppointmentRepository;
import com.arzttermine.service.calendar.domain.repository.AvailabilityRepository;
import com.arzttermine.service.calendar.service.event.model.AppointmentsBulkChangeEvent;
import com.arzttermine.service.calendar.service.event.model.AvailabilityChangedEvent;
import com.arzttermine.service.calendar.service.event.model.AvailabilityRemovedEvent;
import com.arzttermine.service.calendar.service.externals.PracticeApi;
import com.arzttermine.service.calendar.service.processor.transformer.AvailabilityIterator;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import org.hibernate.dialect.lock.LockingStrategyException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionalEventListener;

import java.util.stream.Collectors;

@Component
public class AvailabilityEventListener
{
    private static final Logger log = LoggerFactory.getLogger(AvailabilityEventListener.class);

    @Autowired
    private AvailabilityRepository repository;

    @Autowired
    private PracticeApi practiceClient;

    @Autowired
    private AvailabilityIterator availabilityIterator;

    @Autowired
    private AppointmentRepository appointmentRepository;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Async
    @TransactionalEventListener
    @Retryable(maxAttempts = 5, include = {ServiceUnavailableException.class, LockingStrategyException.class}, backoff = @Backoff(multiplier = 3, delay = 5000))
    public void availabilityChanged(AvailabilityChangedEvent event)
    {
        log.info("availability {} in practice {} changed successfully, trying to update appointments if necessary",
            event.getAvailabilityId(), event.getPracticeId());

        final PracticeDto practice = practiceClient.findById(event.getPracticeId());

        if (event.isMultipleDayRanges()) {
            availabilityIterator.createOrUpdateRanges(event.getAvailabilityId(), practice);
        } else {
            availabilityIterator.createOrUpdateDayRange(event.getAvailabilityId(), practice, event.getAvailabilityStart());
        }
    }

    @Async
    @TransactionalEventListener
    @Transactional(readOnly = false)
    public void availabilityRemoved(AvailabilityRemovedEvent event)
    {
        final long availabilityId = event.getAvailabilityId();
        final long practiceId = event.getPracticeId();
        log.info("removing availability {} from practice {} including not booked appointments", availabilityId, practiceId);

        final Availability availability = repository.findOne(availabilityId);
        if (null == availability) {
            log.info("availability {} not found and cannot be removed", availabilityId);
            return;
        }

        if (!availability.getAppointments().isEmpty()) {
            eventPublisher.publishEvent(AppointmentsBulkChangeEvent.ofDeleted(availability.getAppointments().stream()
                .map(Appointment::getId)
                .collect(Collectors.toList()), practiceId));

            // hard remove but only unbooked
            appointmentRepository.delete(availability.getAppointments().stream()
                .filter(a -> !a.isBooked())
                .collect(Collectors.toList()));
        }

        // availabilities do not cascade delete operations on appointments
        repository.delete(availability);
    }
}
