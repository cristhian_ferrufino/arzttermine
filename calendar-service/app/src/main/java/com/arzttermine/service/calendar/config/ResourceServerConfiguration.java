package com.arzttermine.service.calendar.config;

import com.arzttermine.service.player.client.TokenClient;
import com.arzttermine.service.profile.security.config.RemoteTokenServiceConfigurer;
import com.arzttermine.service.profile.security.config.ResourceServerConfigurer;
import com.arzttermine.service.profile.security.config.settings.RemoteTokenServicesSettings;
import com.arzttermine.service.profile.security.service.TokenCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;

@Configuration
public class ResourceServerConfiguration extends ResourceServerConfigurer
{
    @Autowired
    private RemoteTokenServices remoteTokenServices;

    @Value("${spring.application.name}")
    private String applicationName;

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception
    {
        resources.resourceId(applicationName);
        resources.tokenServices(remoteTokenServices);
    }

    @Configuration
    @Profile("!testing")
    public static class RemoteTokenServicesConfigration extends RemoteTokenServiceConfigurer
    {
        @Autowired
        private RemoteTokenServicesSettings tokenServicesSettings;

        @Autowired
        private TokenClient tokenClient;

        @Bean
        public RemoteTokenServices remoteTokenServices()
        {
            return configure(tokenServicesSettings);
        }

        @Bean
        @Primary
        public TokenCache clientTokenCache()
        {
            return super.clientTokenCache(tokenServicesSettings, tokenClient);
        }
    }
}
