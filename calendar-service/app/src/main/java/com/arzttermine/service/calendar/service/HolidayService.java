package com.arzttermine.service.calendar.service;

import com.arzttermine.library.suport.exception.AccessDeniedException;
import com.arzttermine.service.calendar.domain.entity.Holiday;
import com.arzttermine.service.calendar.domain.repository.HolidayRepository;
import com.arzttermine.service.calendar.service.event.model.HolidayChangedEvent;
import com.arzttermine.service.calendar.service.mapper.HolidayMapper;
import com.arzttermine.service.calendar.web.dto.request.CreateHolidaysDto;
import com.arzttermine.service.calendar.web.dto.request.HolidayTimeRangeDto;
import com.arzttermine.service.calendar.web.dto.response.HolidayDto;
import com.arzttermine.service.calendar.web.dto.response.paging.HolidayPageDto;
import com.arzttermine.service.profile.security.support.SecurityContextUtil;
import com.querydsl.core.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Collection;
import java.util.Comparator;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class HolidayService
{
    @Autowired
    private HolidayRepository repository;

    @Autowired
    private HolidayMapper mapper;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Transactional(readOnly = false)
    public Collection<HolidayDto> createHolidays(final CreateHolidaysDto request)
    {
        if (!SecurityContextUtil.hasPracticeAuthority(request.getPracticeId()) && !SecurityContextUtil.hasSystemGrants()) {
            throw new AccessDeniedException();
        }

        final Collection<Holiday> holidays = mapper.fromRequest(request);

        final LocalDate lowest = request.getTimes().stream()
            .map(HolidayTimeRangeDto::getBegin)
            .sorted()
            .findFirst()
            .get();

        // get latest end date, if not present get latest start date (end date can be null)
        final LocalDate highest = Stream.concat(
                request.getTimes().stream().map(HolidayTimeRangeDto::getEnd).filter(Objects::nonNull),
                request.getTimes().stream().map(HolidayTimeRangeDto::getBegin)
            )
            .sorted(Comparator.reverseOrder())
            .findFirst()
            .get();

        eventPublisher.publishEvent(HolidayChangedEvent.of(lowest, highest, request.getPracticeId()));

        return holidays.stream()
            .map(repository::save)
            .map(mapper::toDto)
            .collect(Collectors.toList());
    }

    @Transactional(readOnly = false)
    public void deleteHoliday(final long id)
    {
        final Holiday holiday = repository.findOne(id);

        if (null == holiday || (!SecurityContextUtil.hasPracticeAuthority(holiday.getPracticeId()) && !SecurityContextUtil.hasSystemGrants())) {
            return;
        }

        eventPublisher.publishEvent(HolidayChangedEvent.of(
            holiday.getBegin().atOffset(ZoneOffset.UTC).toLocalDate(),
            holiday.getEnd().atOffset(ZoneOffset.UTC).toLocalDate(),
            holiday.getPracticeId()
        ));

        repository.delete(holiday);
    }

    @Transactional(readOnly = true)
    public HolidayPageDto getHolidays(Predicate predicate, Pageable pageable)
    {
        final Page<Holiday> holidays = repository.findAll(predicate, pageable);
        final boolean systemGrant = SecurityContextUtil.hasSystemGrants();

        return mapper.toPageDto(holidays, holiday ->
            systemGrant || SecurityContextUtil.hasPracticeAuthority(holiday.getPracticeId()));
    }
}
