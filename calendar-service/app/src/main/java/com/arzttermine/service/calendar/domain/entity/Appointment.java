package com.arzttermine.service.calendar.domain.entity;

import com.arzttermine.library.web.struct.InsuranceType;
import com.arzttermine.library.web.struct.Language;
import com.arzttermine.service.calendar.support.InstantToTimestampConverter;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.util.Assert;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

@Entity
@Table(
    name = "appointments",
    indexes = {
        @Index(name = "id_appointment_doctor_id", columnList = "doctor_id"),
        @Index(name = "id_appointment_practice_id", columnList = "practice_id")
    }
)
public class Appointment
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false, insertable = false)
    private long id;

    @Column(name = "doctor_id", nullable = false, updatable = false)
    private long doctorId;

    @Column(name = "practice_id", nullable = false, updatable = false)
    private long practiceId;

    @Column(nullable = false)
    private boolean enabled = true;

    @Column(nullable = false)
    @Convert(converter = InstantToTimestampConverter.class)
    private Instant start;

    @Column(nullable = false)
    @Convert(converter = InstantToTimestampConverter.class)
    private Instant end;

    @ElementCollection
    @Enumerated(EnumType.STRING)
    @LazyCollection(LazyCollectionOption.FALSE)
    @CollectionTable(
        name = "appointments_languages",
        joinColumns = @JoinColumn(name = "appointment_id", nullable = false)
    )
    private Collection<Language> languages = new HashSet<>();

    @ElementCollection
    @Enumerated(EnumType.STRING)
    @LazyCollection(LazyCollectionOption.FALSE)
    @CollectionTable(
        name = "appointments_insurance_types",
        joinColumns = @JoinColumn(name = "appointment_id", nullable = false)
    )
    private Collection<InsuranceType> insuranceTypes = new HashSet<>();

    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    @CollectionTable(
        name = "appointments_treatment_types",
        joinColumns = @JoinColumn(name = "appointment_id", nullable = false)
    )
    private Collection<Long> treatmentTypes = new HashSet<>();

    @Column(name = "booked_by_customer_id")
    private Long bookedByCustomer;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    private List<AppointmentResource> resources = new ArrayList<>();

    @Transient
    private transient PracticeDto practice;

    public Appointment() {
    }

    public Appointment(Appointment appointment) {
        doctorId = appointment.getDoctorId();
        practiceId = appointment.getPracticeId();
        bookedByCustomer = appointment.getBookedByCustomer();
        treatmentTypes.addAll(appointment.getTreatmentTypes());
        insuranceTypes.addAll(appointment.getInsuranceTypes());
        resources.addAll(appointment.getResources());
        languages.addAll(appointment.getLanguages());
        enabled = appointment.isEnabled();
        start = appointment.getStart();
        end = appointment.getEnd();
    }

    public long getId() {
        return id;
    }

    public long getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(long doctorId) {
        this.doctorId = doctorId;
    }

    public long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(long practiceId) {
        this.practiceId = practiceId;
    }

    public Instant getStart() {
        return start;
    }

    public void setStart(Instant start) {
        this.start = start;
    }

    public Instant getEnd() {
        return end;
    }

    public void setEnd(Instant end) {
        this.end = end;
    }

    public Collection<InsuranceType> getInsuranceTypes() {
        return insuranceTypes;
    }

    public void setInsuranceTypes(Collection<InsuranceType> insuranceTypes) {
        this.insuranceTypes = insuranceTypes;
    }

    public Collection<Long> getTreatmentTypes() {
        return treatmentTypes;
    }

    public void setTreatmentTypes(Collection<Long> treatmentTypes) {
        this.treatmentTypes = treatmentTypes;
    }

    public Collection<Language> getLanguages() {
        return languages;
    }

    public boolean isBooked() {
        return null != bookedByCustomer;
    }

    public Long getBookedByCustomer() {
        return bookedByCustomer;
    }

    public void setBookedByCustomer(Long bookedByCustomer) {
        this.bookedByCustomer = bookedByCustomer;
    }

    public List<AppointmentResource> getResources() {
        return resources;
    }

    public PracticeDto getPractice() {
        return practice;
    }

    public void setPractice(PracticeDto practice) {
        this.practice = practice;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void setLanguages(Collection<Language> languages) {
        this.languages = languages;
    }

    public boolean isDuringTime(int hour, int minute) {
        final OffsetDateTime utcStart = start.atOffset(ZoneOffset.UTC);

        if (utcStart.getHour() > hour || (utcStart.getHour() == hour && utcStart.getMinute() > minute)) {
            return false;
        } else {
            final OffsetDateTime utcEnd = end.atOffset(ZoneOffset.UTC);

            if (utcEnd.getHour() < hour || (utcEnd.getHour() == hour && utcEnd.getMinute() < minute)) {
                return false;
            }

            return true;
        }
    }

    public void reset() {
        if (!resources.isEmpty())
            resources.clear();

        if (!treatmentTypes.isEmpty())
            treatmentTypes.clear();

        if (!insuranceTypes.isEmpty())
            insuranceTypes.clear();
    }

    public void wipeOut() {
        Assert.state(!isBooked(), "appointment is not allowed to be booked at this moment");

        reset();
        languages.clear();
        enabled = false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Appointment that = (Appointment) o;

        if (id != that.id) return false;
        else if (doctorId != that.doctorId) return false;
        else if (practiceId != that.practiceId) return false;
        else if (enabled != that.enabled) return false;
        else if (bookedByCustomer != null ? !bookedByCustomer.equals(that.bookedByCustomer) : that.bookedByCustomer != null) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (doctorId ^ (doctorId >>> 32));
        result = 31 * result + (int) (practiceId ^ (practiceId >>> 32));
        result = 31 * result + (enabled ? 1 : 0);
        result = 31 * result + (bookedByCustomer != null ? bookedByCustomer.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return String.format(
            "%d [%s]-[%s] (booked: %s, enabled: %s)",
            id, start, end, String.valueOf(isBooked()), String.valueOf(isEnabled())
        );
    }
}
