package com.arzttermine.service.calendar.service.processor;

import com.arzttermine.service.calendar.domain.entity.Availability;
import com.arzttermine.service.calendar.service.mapper.AvailabilityMapper;
import com.arzttermine.service.calendar.service.processor.recurring.AvailabilityRecurringStrategy;
import com.arzttermine.service.calendar.service.processor.recurring.DailyRecurringStrategy;
import com.arzttermine.service.calendar.service.processor.recurring.MonthlyRecurringStrategy;
import com.arzttermine.service.calendar.service.processor.recurring.NonRecurringStrategy;
import com.arzttermine.service.calendar.service.processor.recurring.WeeklyRecurringStrategy;
import com.arzttermine.service.calendar.web.dto.request.PlannedAvailabilitiesRequestDto;
import com.arzttermine.service.calendar.web.dto.response.AvailabilityTimeFrameDto;
import com.arzttermine.service.calendar.web.struct.RecurringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.Instant;
import java.util.Collection;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class RecurringAvailabilityProcessor
{
    private final Map<RecurringType, AvailabilityRecurringStrategy> strategies = new EnumMap<>(RecurringType.class);

    @Autowired
    public RecurringAvailabilityProcessor(AvailabilityMapper mapper) {
        strategies.put(RecurringType.NEVER, new NonRecurringStrategy(mapper));
        strategies.put(RecurringType.DAILY, new DailyRecurringStrategy(mapper));
        strategies.put(RecurringType.WEEKLY, new WeeklyRecurringStrategy(mapper));
        strategies.put(RecurringType.MONTHLY, new MonthlyRecurringStrategy(mapper));
    }

    public Map<Long, List<AvailabilityTimeFrameDto>> searchTimeFramesIn(final List<Availability> availabilities, final PlannedAvailabilitiesRequestDto request)
    {
        final Collection<Long> resourceIds = request.getResourceIds();
        final Instant from = request.getStart().toInstant();
        final Instant end = request.getEnd().toInstant();

        final Map<Long, List<AvailabilityTimeFrameDto>> frames = new HashMap<>();

        for (final Availability availability : availabilities) {
            final Duration distance = Duration.between(availability.getBegin(), availability.getEnd());

            final RecurringType recurringType = (availability.isRecurring())
                ? availability.getRecurringConfig().getType()
                : RecurringType.NEVER;

            strategies.get(recurringType).generateFor(frames, availability, from, end, distance, resourceIds);
        }

        return frames;
    }
}
