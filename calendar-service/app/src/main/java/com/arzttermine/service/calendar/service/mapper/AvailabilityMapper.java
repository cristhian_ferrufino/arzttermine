package com.arzttermine.service.calendar.service.mapper;

import com.arzttermine.library.web.struct.InsuranceType;
import com.arzttermine.library.web.struct.Language;
import com.arzttermine.service.calendar.domain.entity.Appointment;
import com.arzttermine.service.calendar.domain.entity.AppointmentResource;
import com.arzttermine.service.calendar.domain.entity.Availability;
import com.arzttermine.service.calendar.domain.entity.RecurringConfiguration;
import com.arzttermine.service.calendar.web.dto.RecurringConfigDto;
import com.arzttermine.service.calendar.web.dto.request.CreateAvailabilityDto;
import com.arzttermine.service.calendar.web.dto.request.ResourceRequestDto;
import com.arzttermine.service.calendar.web.dto.response.AvailabilityTimeFrameDto;
import com.arzttermine.service.calendar.web.struct.RecurringType;
import com.arzttermine.service.profile.web.dto.response.DoctorProfileDto;
import com.arzttermine.service.profile.web.dto.response.PracticeDto;
import com.arzttermine.service.profile.web.dto.response.TreatmentTypeDto;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class AvailabilityMapper
{
    public AvailabilityTimeFrameDto toDto(final Availability availability) {
        return toDto(availability, null, null);
    }

    public AvailabilityTimeFrameDto toDto(final Availability availability, final Instant begin, final Instant end) {
        final AvailabilityTimeFrameDto dto = new AvailabilityTimeFrameDto();
        dto.setId(availability.getId());
        dto.setDuration(availability.getDuration());
        dto.setBegin(Optional.ofNullable(begin).map(Date::from).orElse(Date.from(availability.getBegin())));
        dto.setEnd(Optional.ofNullable(end).map(Date::from).orElse(Date.from(availability.getEnd())));
        dto.setTreatmentTypes(availability.getTreatmentTypes());

        dto.setInsuranceTypes((availability.getInsuranceTypes().isEmpty())
            ? Arrays.asList(InsuranceType.PRIVATE, InsuranceType.COMPULSORY)
            : availability.getInsuranceTypes());

        dto.setResourceRequests(availability.getResources().stream()
            .map(r -> new ResourceRequestDto(r.getResourceId()))
            .collect(Collectors.toList()));

        Optional.ofNullable(availability.getRecurringConfig()).ifPresent(config -> {
            final RecurringConfigDto recurringConfig = new RecurringConfigDto();
            recurringConfig.setType(config.getType());
            recurringConfig.setEndsAt(Optional.ofNullable(config.getEndsAt())
                .map(e -> e.atOffset(ZoneOffset.UTC).toLocalDate()).orElse(null));

            if (config.is(RecurringType.WEEKLY))
                recurringConfig.setDayOfWeek(LocalDateTime.from(availability.getBegin().atZone(ZoneId.systemDefault())).getDayOfWeek());
            else if (config.is(RecurringType.MONTHLY))
                recurringConfig.setDateOfMonth(LocalDateTime.from(availability.getBegin().atZone(ZoneId.systemDefault())).getDayOfMonth());

            dto.setRecurringConfig(recurringConfig);
        });

        return dto;
    }

    public Availability update(Availability availability, CreateAvailabilityDto dto, PracticeDto practice, @Nullable RecurringConfiguration recurringConfig) {
        return update(availability, dto, practice, null, null, recurringConfig);
    }

    public Availability update(Availability availability, CreateAvailabilityDto dto, PracticeDto practice, @Nullable Instant from, @Nullable Instant end, @Nullable RecurringConfiguration recurringConfig) {
        final Instant begin = Optional.ofNullable(from).orElse(dto.getFrom().toInstant());
        final Instant ending = Optional.ofNullable(end).orElse(dto.getTo().toInstant());

        availability.setBegin(begin);
        availability.setEnd(ending);
        availability.setDuration(dto.getDuration());
        availability.setRecurringConfig(recurringConfig);
        availability.getInsuranceTypes().clear();
        availability.getInsuranceTypes().addAll(dto.getInsuranceTypes());
        availability.setPracticeId(practice.getId());

        availability.getResources()
            .removeIf(r -> dto.getResourceRequests().stream().noneMatch(rr -> rr.getResourceId().equals(r.getResourceId())));

        availability.getResources().addAll(dto.getResourceRequests().stream()
            .filter(rr -> availability.getResources().stream().noneMatch(r -> rr.getResourceId().equals(r.getResourceId())))
            .map(rr -> new AppointmentResource(rr.getResourceId(), true, availability.getBegin(), availability.getEnd()))
            .collect(Collectors.toList()));

        availability.getTreatmentTypes()
            .removeIf(tt -> !dto.getTreatmentTypes().contains(tt) || practice.getTreatmentTypes().stream().noneMatch(t -> t.getId() == tt));

        availability.getTreatmentTypes().addAll(dto.getTreatmentTypes().stream()
            .filter(tt -> practice.getTreatmentTypes().stream().filter(TreatmentTypeDto::isEnabled).anyMatch(t -> t.getId() == tt))
            .filter(tt -> !availability.getTreatmentTypes().contains(tt))
            .collect(Collectors.toList()));

        // @TODO for now we do not have separate times for resource-requests
        for (AppointmentResource r : availability.getResources()) {
            r.setBegin(availability.getBegin());
            r.setBegin(availability.getEnd());
        }

        // reset the last iteration to guarantee fresh results
        availability.iterationDone(null);

        return availability;
    }

    public Appointment toBasicAppointment(Availability availability, DoctorProfileDto doctorProfile) {
        final Appointment appointment = new Appointment();
        appointment.setDoctorId(doctorProfile.getId());
        appointment.setPracticeId(availability.getPracticeId());
        appointment.getLanguages().addAll(Collections.singletonList(Language.GERMAN));

        return appointment;
    }
}
