package com.arzttermine.service.calendar.service.event.model;

import com.arzttermine.service.profile.web.dto.response.PracticeDto;

import java.util.Collection;
import java.util.Collections;

public class AppointmentsBulkChangeEvent
{
    private final PracticeDto practice;
    private final Collection<Long> created;
    private final Collection<Long> deleted;

    private AppointmentsBulkChangeEvent(Collection<Long> created, Collection<Long> deleted, PracticeDto practice) {
        this.created = created;
        this.deleted = deleted;
        this.practice = practice;
    }

    public static AppointmentsBulkChangeEvent of(Collection<Long> created, Collection<Long> deleted, PracticeDto practice) {
        return new AppointmentsBulkChangeEvent(created, deleted, practice);
    }

    public static AppointmentsBulkChangeEvent ofCreated(Collection<Long> created, PracticeDto practice) {
        return new AppointmentsBulkChangeEvent(created, Collections.emptyList(), practice);
    }

    public static AppointmentsBulkChangeEvent ofDeleted(Collection<Long> deleted, long practiceId) {
        PracticeDto practice = new PracticeDto();
        practice.setId(practiceId);

        return new AppointmentsBulkChangeEvent(Collections.emptyList(), deleted, practice);
    }

    public Collection<Long> getCreated() {
        return created;
    }

    public Collection<Long> getDeleted() {
        return deleted;
    }

    public PracticeDto getPractice() {
        return practice;
    }
}
