package com.arzttermine.service.calendar.service.processor.recurring;

import com.arzttermine.service.calendar.domain.entity.Availability;
import com.arzttermine.service.calendar.web.dto.response.AvailabilityTimeFrameDto;

import java.time.Duration;
import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface AvailabilityRecurringStrategy
{
    void generateFor(
        Map<Long, List<AvailabilityTimeFrameDto>> frames,
        Availability availability,
        Instant begin,
        Instant end,
        Duration duration,
        Collection<Long> resourceIds
    );
}
