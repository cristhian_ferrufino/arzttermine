USE calendar;

CREATE TABLE `appointments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `doctor_id` bigint(20) NOT NULL,
  `enabled` bit(1) NOT NULL DEFAULT 1,
  `booked_by_customer_id` bigint(20) NULL DEFAULT NULL,
  `end` datetime NOT NULL,
  `practice_id` int(11) NOT NULL,
  `start` datetime NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `id_appointment_doctor_id` (`doctor_id`),
  INDEX `id_appointment_practice_id` (`practice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `appointment_insurance_types` (
  `appointment_id` bigint(20) NOT NULL,
  `insurance_types` varchar(128) DEFAULT NULL,
  INDEX `id_insurance_type_appointment_id` (`appointment_id`),
  CONSTRAINT `fk_insurance_type_appointment_id` FOREIGN KEY (`appointment_id`) REFERENCES `appointments` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `appointment_treatment_types` (
  `appointment_id` bigint(20) NOT NULL,
  `treatment_types` bigint(20) NOT NULL,
  INDEX `id_treatment_type_appointment_id` (`appointment_id`),
  CONSTRAINT `fk_treatment_type_appointment_id` FOREIGN KEY (`appointment_id`) REFERENCES `appointments` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `appointment_languages` (
  `appointment_id` bigint(20) NOT NULL,
  `languages` varchar(56) NOT NULL,
  INDEX `id_language_appointment_id` (`appointment_id`),
  CONSTRAINT `fk_language_appointment_id` FOREIGN KEY (`appointment_id`) REFERENCES `appointments` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `appointment_resources` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `begin` datetime NOT NULL,
  `ending` datetime NOT NULL,
  `resource_id` bigint(20) unsigned NOT NULL,
  `appointment_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `id_appointment_resource_appointment_id` (`appointment_id`),
  INDEX `id_appointment_resource_resource_id` (`resource_id`),
  CONSTRAINT `fk_appointment_resource_appointment_id` FOREIGN KEY (`appointment_id`) REFERENCES `appointments` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `appointment_block_configurations` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enabled` bit(1) NOT NULL DEFAULT TRUE,
  `max_usages` int(8) DEFAULT NULL,
  `practice_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `id_appointment_block_config_practice_id` (`practice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `appointment_blocks` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `begin` time NOT NULL,
  `enabled` bit(1) NOT NULL DEFAULT TRUE,
  `ending` time NOT NULL,
  `last_iteration` datetime NOT NULL DEFAULT NOW(),
  `max_usages` int(8) DEFAULT NULL,
  `practice_id` bigint(20) NOT NULL,
  `version` int(4) NOT NULL DEFAULT 1,
  `configuration_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `id_appointment_block_configuration_id` (`configuration_id`),
  CONSTRAINT `fk_appointment_block_configuration_id` FOREIGN KEY (`configuration_id`) REFERENCES `appointment_block_configurations` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `appointment_block_offers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `begin` time NOT NULL,
  `doctor_id` bigint(20) NOT NULL,
  `ending` time NOT NULL,
  `block_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `id_appointment_block_offer_parent_id` (`block_id`),
  CONSTRAINT `fk_appointment_block_offer_parent_id` FOREIGN KEY (`block_id`) REFERENCES `appointment_blocks` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `appointment_block_days` (
  `block_id` bigint(20) NOT NULL,
  `days` varchar(28) NOT NULL,
  INDEX `id_appointment_block_day_block_id` (`block_id`),
  CONSTRAINT `fk_appointment_block_day_block_id` FOREIGN KEY (`block_id`) REFERENCES `appointment_blocks` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `appointment_block_insurance_types` (
  `block_offer_id` bigint(20) NOT NULL,
  `insurance_types` varchar(56) DEFAULT NULL,
  INDEX `id_appointment_block_insurances_offer_id` (`block_offer_id`),
  CONSTRAINT `fk_appointment_block_insurances_offer_id` FOREIGN KEY (`block_offer_id`) REFERENCES `appointment_block_offers` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `appointment_block_languages` (
  `block_offer_id` bigint(20) NOT NULL,
  `languages` varchar(56) DEFAULT NULL,
  INDEX `id_appointment_block_languages_offer_id` (`block_offer_id`),
  CONSTRAINT `fk_appointment_block_languages_offer_id` FOREIGN KEY (`block_offer_id`) REFERENCES `appointment_block_offers` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `appointment_block_resources` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `resource_id` bigint(20) NOT NULL,
  `block_offer_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `id_appointment_block_offer_resource_block_id` (`block_offer_id`),
  CONSTRAINT `fk_appointment_block_offer_resource_block_id` FOREIGN KEY (`block_offer_id`) REFERENCES `appointment_block_offers` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `appointment_block_treatment_types` (
  `block_offer_id` bigint(20) NOT NULL,
  `treatment_types` bigint(20) DEFAULT NULL,
  INDEX `id_appointment_block_treatments_block_offer_id` (`block_offer_id`),
  CONSTRAINT `fk_appointment_block_treatments_block_offer_id` FOREIGN KEY (`block_offer_id`) REFERENCES `appointment_block_offers` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;
