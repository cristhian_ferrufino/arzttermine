USE calendar;
-- nothing to migrate

ALTER TABLE appointment_block_configurations
    DROP INDEX id_appointment_block_config_practice_id;

ALTER TABLE appointment_blocks
    DROP FOREIGN KEY fk_appointment_block_configuration_id,
    DROP INDEX id_appointment_block_configuration_id;

ALTER TABLE appointment_block_offers
    DROP FOREIGN KEY fk_appointment_block_offer_parent_id,
    DROP INDEX id_appointment_block_offer_parent_id;

ALTER TABLE appointment_block_days
    DROP FOREIGN KEY fk_appointment_block_day_block_id,
    DROP INDEX id_appointment_block_day_block_id;

ALTER TABLE appointment_block_languages
    DROP FOREIGN KEY fk_appointment_block_languages_offer_id,
    DROP INDEX id_appointment_block_languages_offer_id;

ALTER TABLE appointment_block_resources
    DROP FOREIGN KEY fk_appointment_block_offer_resource_block_id,
    DROP INDEX id_appointment_block_offer_resource_block_id;

ALTER TABLE appointment_block_insurance_types
    DROP FOREIGN KEY fk_appointment_block_insurances_offer_id,
    DROP INDEX id_appointment_block_insurances_offer_id;

ALTER TABLE appointment_block_treatment_types
    DROP FOREIGN KEY fk_appointment_block_treatments_block_offer_id,
    DROP INDEX id_appointment_block_treatments_block_offer_id;


DROP TABLE appointment_block_resources;
DROP TABLE appointment_block_languages;
DROP TABLE appointment_block_treatment_types;
DROP TABLE appointment_block_insurance_types;
DROP TABLE appointment_block_days;
DROP TABLE appointment_block_offers;
DROP TABLE appointment_blocks;
DROP TABLE appointment_block_configurations;


-- adapt appointment tables to our naming convention
RENAME TABLE appointment_languages TO appointments_languages;
RENAME TABLE appointment_insurance_types TO appointments_insurance_types;
RENAME TABLE appointment_treatment_types TO appointments_treatment_types;
