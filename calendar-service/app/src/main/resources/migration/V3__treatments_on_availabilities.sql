USE calendar;

CREATE TABLE `availabilities_treatment_types` (
  `availability_id` bigint(20) unsigned NOT NULL,
  `treatment_types` bigint(20) unsigned NOT NULL,
  INDEX `id_availability_treatment_availability_id` (`availability_id`),
  CONSTRAINT `fk_availability_treatment_availability_id` FOREIGN KEY (`availability_id`) REFERENCES `availabilities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;
