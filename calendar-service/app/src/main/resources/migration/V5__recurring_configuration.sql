USE calendar;

CREATE TABLE `recurring_availability_configurations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ends_at` datetime DEFAULT NULL,
  `starts_at` datetime NOT NULL,
  `type` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `availabilities_excluded_days` (
  `availability_id` bigint(20) unsigned NOT NULL,
  `excluded_day` datetime NOT NULL,
  INDEX `id_availability_excluded_day_availability_id` (`availability_id`),
  CONSTRAINT `fk_availability_excluded_day_availability` FOREIGN KEY (`availability_id`) REFERENCES `availabilities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

ALTER TABLE availabilities
    ADD COLUMN active bit(1) NULL DEFAULT TRUE,
    ADD COLUMN recurring_config_id bigint(20) unsigned NULL DEFAULT NULL,
    ADD CONSTRAINT fk_recurring_configuration FOREIGN KEY (recurring_config_id) REFERENCES recurring_availability_configurations(id) ON DELETE RESTRICT,
    ADD INDEX `id_availability_recurring_config_id` (`recurring_config_id`);
