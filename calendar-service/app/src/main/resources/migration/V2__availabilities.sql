CREATE TABLE `appointments_resources` (
  `appointment_id` bigint(20) NOT NULL,
  `resources_id` bigint(20) unsigned NOT NULL,
  INDEX `id_appointments_resources_appointment_id` (`appointment_id`),
  UNIQUE KEY `un_resources_foreach_appointment` (`appointment_id`, `resources_id`),
  CONSTRAINT `fk_appointments_resources_resource_id` FOREIGN KEY (`resources_id`) REFERENCES `appointment_resources` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_appointments_resources_appointment_id` FOREIGN KEY (`appointment_id`) REFERENCES `appointments` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

INSERT INTO `appointments_resources` (`resources_id`, `appointment_id`)
    (SELECT `resource_id`, `appointment_id` FROM `appointment_resources` GROUP BY `resource_id`, `appointment_id`);

ALTER TABLE `appointment_resources`
    DROP FOREIGN KEY fk_appointment_resource_appointment_id,
    DROP INDEX id_appointment_resource_appointment_id,
    DROP COLUMN `appointment_id`,
    ADD COLUMN `draft` bit(1) NOT NULL DEFAULT 0;

CREATE TABLE `availabilities` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `begin` datetime NOT NULL,
  `duration` int(8) NOT NULL,
  `end` datetime NOT NULL,
  `practice_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `id_avilability_practice_id` (`practice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `availabilities_appointments` (
  `availability_id` bigint(20) unsigned NOT NULL,
  `appointments_id` bigint(20) NOT NULL,
  UNIQUE KEY `un_appointment_for_each_availability` (`appointments_id`),
  INDEX `id_availability_appointment_availability_id` (`availability_id`),
  CONSTRAINT `fk_availability_appointments_appointment_id` FOREIGN KEY (`appointments_id`) REFERENCES `appointments` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_availability_appointments_availability_id` FOREIGN KEY (`availability_id`) REFERENCES `availabilities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `availabilities_resources` (
  `availability_id` bigint(20) unsigned NOT NULL,
  `resources_id` bigint(20) unsigned NOT NULL,
  INDEX `id_availability_resource_availability_id` (`availability_id`),
  CONSTRAINT `fk_availability_resources_availability_id` FOREIGN KEY (`availability_id`) REFERENCES `availabilities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_availability_resources_appointment_id` FOREIGN KEY (`resources_id`) REFERENCES `appointment_resources` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;
