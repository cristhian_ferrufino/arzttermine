ALTER TABLE calendar.availabilities
    ADD COLUMN `last_appointment_updates` datetime DEFAULT NULL;
