USE calendar;

CREATE TABLE `availabilities_insurance_types` (
    `insurance_type` varchar(56) NOT NULL,
    `availability_id` bigint(20) unsigned NOT NULL,
    INDEX `id_availability_insurance_availability_id` (`availability_id`),
    CONSTRAINT `fk_availability_insurance_availability_id` FOREIGN KEY (`availability_id`) REFERENCES `availabilities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;
