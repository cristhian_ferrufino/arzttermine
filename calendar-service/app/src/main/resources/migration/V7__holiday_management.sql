USE calendar;

CREATE TABLE `holidays` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `active` bit(1) DEFAULT TRUE,
  `begin` datetime NOT NULL,
  `end` datetime NOT NULL,
  `practice_id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `public_holiday` bit(1) NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`),
  INDEX `id_holidays_practice_id` (`practice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

CREATE TABLE `holidays_resource_ids` (
  `holiday_id` bigint(20) unsigned NOT NULL,
  `resource_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`holiday_id`,`resource_id`),
  CONSTRAINT `fk_holidays_resources_holiday_id` FOREIGN KEY (`holiday_id`) REFERENCES `holidays` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;
