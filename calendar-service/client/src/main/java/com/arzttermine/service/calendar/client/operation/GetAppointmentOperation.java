package com.arzttermine.service.calendar.client.operation;

import com.arzttermine.library.client.model.Operation;
import com.arzttermine.library.client.model.param.Parameter;
import com.arzttermine.service.calendar.web.dto.response.AppointmentDto;

public class GetAppointmentOperation extends Operation.Builder<AppointmentDto>
{
    @Override
    public Operation<AppointmentDto> build() {
        setPath("/appointments/{id}");
        addParameter(new Parameter("id"));

        setResponseTypeClass(AppointmentDto.class);

        return new Operation<>(this);
    }
}
