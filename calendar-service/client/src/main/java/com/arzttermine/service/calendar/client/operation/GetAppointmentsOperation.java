package com.arzttermine.service.calendar.client.operation;

import com.arzttermine.library.client.model.Operation;
import com.arzttermine.library.client.model.param.Parameter;
import com.arzttermine.library.client.model.struct.RequestMethod;
import com.arzttermine.service.calendar.web.dto.response.AppointmentDto;
import com.arzttermine.service.calendar.web.dto.response.paging.AppointmentsPageDto;

public class GetAppointmentsOperation extends Operation.Builder<AppointmentsPageDto>
{
    @Override
    public Operation<AppointmentsPageDto> build() {
        setPath("/appointments");
        setMethod(RequestMethod.GET);
        setResponseTypeClass(AppointmentsPageDto.class);

        return new Operation<>(this);
    }
}
