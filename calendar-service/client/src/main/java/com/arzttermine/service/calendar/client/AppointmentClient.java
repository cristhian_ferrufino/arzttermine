package com.arzttermine.service.calendar.client;

import com.arzttermine.library.client.config.settings.ClientHostSettings;
import com.arzttermine.library.client.model.RequestOptions;
import com.arzttermine.library.client.model.param.Parameter;
import com.arzttermine.library.client.support.ClientAdapter;
import com.arzttermine.service.calendar.client.operation.GetAppointmentOperation;
import com.arzttermine.service.calendar.client.operation.GetAppointmentsOperation;
import com.arzttermine.service.calendar.web.dto.response.AppointmentDto;
import com.arzttermine.service.calendar.web.dto.response.paging.AppointmentsPageDto;

public class AppointmentClient extends ClientAdapter
{
    public AppointmentClient(ClientHostSettings settings) {
        super(settings.getHost(), settings.getPort(), settings.getScheme());
    }

    public AppointmentDto getAppointment(final long id, final RequestOptions options)
    {
        GetAppointmentOperation operation = new GetAppointmentOperation();
        operation.set(Parameter.Type.PATH, "id", String.valueOf(id));

        return execute(operation, options);
    }

    public AppointmentsPageDto getAppointments(final RequestOptions options)
    {
        return execute(new GetAppointmentsOperation(), options);
    }
}
