# search

To search for appointments use elasticsearch!

You'll get the following DTO as practice document:
(Please double check as the structure might change a lot over the time)


```json
{
  "id": 1,
  "start": "2017-02-09T19:00Z",
  "end": "2017-02-09T20:30Z",
  "duration": 90,
  "booked": false,
  "practice": {
    "id": 1,
    "name": "rgtfff",
    "location": {
      "id": 0,
      "country": "GERMANY",
      "longitude": 1000,
      "latitude": 0,
      "location": "somewhere 123",
      "zip": "12345",
      "phone": "optional phone",
      "phone_mobile": "optional mobile phone",
      "contactMail": "my@mail.com",
      "primary": false
    },
    "medicalSpecialties": [
      "DENTIST"
    ]
  },
  "doctor": {
    "id": 1,
    "email": "my@mail.de",
    "firstName": "someone",
    "lastName": "as example",
    "fullName": "someone as example",
    "language": "GERMAN"
  },
  "availableInsuranceTypes": [
    "COMPULSORY",
    "PRIVATE"
  ],
  "availableTreatmentTypes": [
    1,
    3
  ],
  "availableLanguages": [
    "GERMAN"
  ]
}
```

### Notes:
- date times should be converted to the corresponding timezone (TODO: Add "timezone"-header to API)
- `doctor.profile.treatmentTypes` might contain more elements than `availableTreatmentTypes`.
