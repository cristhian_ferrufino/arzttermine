# Availabilities

Availabilities are used to define the Availability of resources. Its a simple time range including many resources.

### Creating & Updating 
![Chart not found](charts/Availabilities.png "Creating and updating availabilities")

### Appointment creating & merge
![Chart not found](charts/Appointment-Creation.png "Appointment creation and merge of appointments when changing availabilities")
