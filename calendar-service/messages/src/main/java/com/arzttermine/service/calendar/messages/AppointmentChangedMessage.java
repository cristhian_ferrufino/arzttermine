package com.arzttermine.service.calendar.messages;

import com.arzttermine.library.messaging.model.TopicMessage;
import com.arzttermine.service.notifications.web.struct.NotificationType;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AppointmentChangedMessage extends TopicMessage
{
    private long previous;
    private long newId;
    private Long bookedByCustomerId;
    private Date previousAppointmentStart;
    private List<NotificationType> notificationTypes = new ArrayList<>();

    public Date getPreviousAppointmentStart() {
        return previousAppointmentStart;
    }

    public void setPreviousAppointmentStart(Date previousAppointmentStart) {
        this.previousAppointmentStart = previousAppointmentStart;
    }

    public long getPrevious() {
        return previous;
    }

    public void setPrevious(long previous) {
        this.previous = previous;
    }

    public long getNewId() {
        return newId;
    }

    public void setNewId(long newId) {
        this.newId = newId;
    }

    public Long getBookedByCustomerId() {
        return bookedByCustomerId;
    }

    public void setBookedByCustomerId(Long bookedByCustomerId) {
        this.bookedByCustomerId = bookedByCustomerId;
    }

    public List<NotificationType> getNotificationTypes() {
        return notificationTypes;
    }

    public void setNotificationTypes(List<NotificationType> notificationTypes) {
        this.notificationTypes = notificationTypes;
    }

    @Override
    protected String getTopic() {
        return "changed";
    }

    @Override
    protected String getTopicKind() {
        return "appointment";
    }
}
