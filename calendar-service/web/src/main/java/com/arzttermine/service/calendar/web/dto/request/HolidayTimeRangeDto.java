package com.arzttermine.service.calendar.web.dto.request;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.chrono.ChronoLocalDate;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class HolidayTimeRangeDto
{
    @NotNull
    private String name;

    @NotNull
    private Set<Long> resourceIds = new HashSet<>();

    @NotNull
    private LocalDate begin;

    private LocalDate end;

    @NotNull
    private Boolean publicHoliday = false;

    @AssertTrue
    public boolean getBeginInFuture() {
        return null == begin || begin.isAfter(Instant.now().atOffset(ZoneOffset.UTC).minusDays(1).toLocalDate());
    }

    @AssertTrue
    public boolean getBeginBeforeEnd() {
        return null == begin || null == end || begin.compareTo(end) < 1;
    }

    public Set<Long> getResourceIds() {
        return resourceIds;
    }

    public void setResourceIds(Set<Long> resourceIds) {
        this.resourceIds = resourceIds;
    }

    public LocalDate getBegin() {
        return begin;
    }

    public void setBegin(LocalDate begin) {
        this.begin = begin;
    }

    public LocalDate getEnd() {
        return end;
    }

    public void setEnd(LocalDate end) {
        this.end = end;
    }

    public Boolean getPublicHoliday() {
        return publicHoliday;
    }

    public void setPublicHoliday(Boolean publicHoliday) {
        this.publicHoliday = publicHoliday;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
