package com.arzttermine.service.calendar.web.dto.response;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Date;

public class HolidayDto
{
    private long id;
    private String name;
    private long practiceId;
    private LocalDate begin;
    private LocalDate end;
    private boolean publicHoliday = false;
    private Collection<Long> resourceIds;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(long practiceId) {
        this.practiceId = practiceId;
    }

    public LocalDate getBegin() {
        return begin;
    }

    public void setBegin(LocalDate begin) {
        this.begin = begin;
    }

    public LocalDate getEnd() {
        return end;
    }

    public void setEnd(LocalDate end) {
        this.end = end;
    }

    public Collection<Long> getResourceIds() {
        return resourceIds;
    }

    public void setResourceIds(Collection<Long> resourceIds) {
        this.resourceIds = resourceIds;
    }

    public boolean isPublicHoliday() {
        return publicHoliday;
    }

    public void setPublicHoliday(boolean publicHoliday) {
        this.publicHoliday = publicHoliday;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
