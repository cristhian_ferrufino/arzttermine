package com.arzttermine.service.calendar.web.dto.request;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import java.util.Date;

public class CreateAppointmentsDto extends BaseAppointmentRequest
{
    @NotNull
    @Future
    @ApiModelProperty("date time when the first appointment should start. This value MUST be lower than end")
    private Date start;

    @NotNull
    @Future
    @ApiModelProperty("end date of appointments. appointments are generated for each duration within start and end")
    private Date end;

    @NotNull
    @ApiModelProperty("practiceId where the appointment should be held")
    private Long practiceId;

    @NotNull
    private Boolean replaceExisting = false;

    @AssertTrue
    public boolean isStartBeforeEnd() {
        return null == start || null == end || start.before(end);
    }

    public Boolean getReplaceExisting() {
        return replaceExisting;
    }

    public void setReplaceExisting(final Boolean replaceExisting) {
        this.replaceExisting = replaceExisting;
    }

    public Long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(Long practiceId) {
        this.practiceId = practiceId;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }
}
