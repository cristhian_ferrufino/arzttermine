package com.arzttermine.service.calendar.web.struct;

public enum RecurringType
{
    NEVER,
    MONTHLY,
    WEEKLY,
    DAILY,
}
