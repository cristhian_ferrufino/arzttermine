package com.arzttermine.service.calendar.web.dto.response;

import com.arzttermine.service.profile.web.dto.LocationDto;
import com.arzttermine.service.profile.web.struct.MedicalSpecialtyType;

import java.util.ArrayList;
import java.util.Collection;

public class AppointmentLocation
{
    private long id;

    private String name;

    private LocationDto location;

    private Collection<MedicalSpecialtyType> medicalSpecialties = new ArrayList<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocationDto getLocation() {
        return location;
    }

    public void setLocation(LocationDto location) {
        this.location = location;
    }

    public Collection<MedicalSpecialtyType> getMedicalSpecialties() {
        return medicalSpecialties;
    }

    public void setMedicalSpecialties(Collection<MedicalSpecialtyType> medicalSpecialties) {
        this.medicalSpecialties = medicalSpecialties;
    }
}
