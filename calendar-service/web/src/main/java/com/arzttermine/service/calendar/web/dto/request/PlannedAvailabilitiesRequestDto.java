package com.arzttermine.service.calendar.web.dto.request;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Set;

public class PlannedAvailabilitiesRequestDto
{
    @NotNull
    private Long practiceId;

    @ApiModelProperty("list of desired resources to retrieve availabilities for. If not supplied or empty all are used")
    private Set<Long> resourceIds;

    @NotNull
    @ApiModelProperty("start of range, consider to add date only (no time)")
    private Date start;

    @NotNull
    @ApiModelProperty("end of range, consider to add date only (no time)")
    private Date end;

    public Set<Long> getResourceIds() {
        return resourceIds;
    }

    public void setResourceIds(Set<Long> resourceIds) {
        this.resourceIds = resourceIds;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(Long practiceId) {
        this.practiceId = practiceId;
    }
}
