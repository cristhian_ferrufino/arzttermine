package com.arzttermine.service.calendar.web.dto.request;

import com.arzttermine.service.calendar.web.dto.RecurringConfigDto;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class UpdateAvailabilitiesConfigurationDto
{
    @Valid
    private RecurringConfigDto recurringConfig;

    @ApiModelProperty(
        "if recurringConfig is changed to null, the availability will be changed to this date exclusively. " +
        "if not set and the recurringConfig is changed to null, the original availability will be still there at its first day." +
        "example: 2017-05-10"
    )
    private LocalDate disableRecurringAt;

    @NotNull
    private Map<Long, CreateAvailabilityDto> updates = new HashMap<>();

    public RecurringConfigDto getRecurringConfig() {
        return recurringConfig;
    }

    public void setRecurringConfig(RecurringConfigDto recurringConfig) {
        this.recurringConfig = recurringConfig;
    }

    public Map<Long, CreateAvailabilityDto> getUpdates() {
        return updates;
    }

    public void setUpdates(Map<Long, CreateAvailabilityDto> updates) {
        this.updates = updates;
    }

    public LocalDate getDisableRecurringAt() {
        return disableRecurringAt;
    }

    public void setDisableRecurringAt(LocalDate disableRecurringAt) {
        this.disableRecurringAt = disableRecurringAt;
    }
}
