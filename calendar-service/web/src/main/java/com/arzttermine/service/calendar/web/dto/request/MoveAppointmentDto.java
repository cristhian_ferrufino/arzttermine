package com.arzttermine.service.calendar.web.dto.request;

import com.arzttermine.service.notifications.web.struct.NotificationType;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

public class MoveAppointmentDto
{
    @Future
    @ApiModelProperty("if supplied, updates the start date of the appointment")
    private Date start;

    @Future
    @ApiModelProperty("if supplied, updates the end date of the appointment")
    private Date end;

    @NotNull
    private Collection<NotificationType> notificationTypes = new ArrayList<>();

    @AssertTrue
    public boolean isStartBeforeEnd() {
        return null == start || null == end || start.before(end);
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Collection<NotificationType> getNotificationTypes() {
        return notificationTypes;
    }

    public void setNotificationTypes(Collection<NotificationType> notificationTypes) {
        this.notificationTypes = notificationTypes;
    }
}
