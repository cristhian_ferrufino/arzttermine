package com.arzttermine.service.calendar.web.dto.response.paging;

import com.arzttermine.library.web.dto.response.paging.PageResponseDto;
import com.arzttermine.service.calendar.web.dto.response.HolidayDto;

public class HolidayPageDto extends PageResponseDto<HolidayDto>
{
}
