package com.arzttermine.service.calendar.web.dto;

import com.arzttermine.service.calendar.web.struct.RecurringType;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Date;

public class RecurringConfigDto
{
    @NotNull
    private RecurringType type;

    @ApiModelProperty("if null the event will be recurred indefinitely")
    private LocalDate endsAt;

    @ApiModelProperty("the day of the week, if weekly type is selected")
    private DayOfWeek dayOfWeek;

    @ApiModelProperty("the date of the month, if monthly type is selected")
    private Integer dateOfMonth;

    @AssertTrue
    public boolean getEndsAtInFuture() {
        return null == endsAt || endsAt.isAfter(Instant.now().atOffset(ZoneOffset.UTC).toLocalDate());
    }

    public RecurringType getType() {
        return type;
    }

    public void setType(RecurringType type) {
        this.type = type;
    }

    public LocalDate getEndsAt() {
        return endsAt;
    }

    public void setEndsAt(LocalDate endsAt) {
        this.endsAt = endsAt;
    }

    public DayOfWeek getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(DayOfWeek dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public Integer getDateOfMonth() {
        return dateOfMonth;
    }

    public void setDateOfMonth(Integer dateOfMonth) {
        this.dateOfMonth = dateOfMonth;
    }
}
