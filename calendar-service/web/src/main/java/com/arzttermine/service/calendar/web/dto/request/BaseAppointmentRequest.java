package com.arzttermine.service.calendar.web.dto.request;

import com.arzttermine.library.web.struct.InsuranceType;
import com.arzttermine.library.web.struct.Language;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class BaseAppointmentRequest
{
    @NotNull @Min(5)
    @ApiModelProperty("duration in minutes. This time will be blocked during an event transition")
    private Integer duration;

    @NotEmpty
    @ApiModelProperty("the appointment will be available for patients with provided insurance types")
    private List<InsuranceType> insuranceTypes;

    @NotEmpty
    @ApiModelProperty("the available treatment type ids for this appointments")
    private List<Long> treatmentTypes = new ArrayList<>();

    @NotEmpty
    @ApiModelProperty("this appointment can be booked for provided languages")
    private List<Language> languages = Collections.singletonList(Language.GERMAN);

    @Valid
    @NotNull
    @ApiModelProperty("request additional resources for this appointment")
    private List<ResourceRequestDto> resourceRequests = new ArrayList<>();

    @NotNull
    @ApiModelProperty("doctorId who is providing the appointment")
    private Long doctorId;

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public List<InsuranceType> getInsuranceTypes() {
        return insuranceTypes;
    }

    public void setInsuranceTypes(List<InsuranceType> insuranceTypes) {
        this.insuranceTypes = insuranceTypes;
    }

    public List<Long> getTreatmentTypes() {
        return treatmentTypes;
    }

    public void setTreatmentTypes(List<Long> treatmentTypes) {
        this.treatmentTypes = treatmentTypes;
    }

    public List<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(List<Language> languages) {
        this.languages = languages;
    }

    public List<ResourceRequestDto> getResourceRequests() {
        return resourceRequests;
    }

    public void setResourceRequests(List<ResourceRequestDto> resourceRequests) {
        this.resourceRequests = resourceRequests;
    }

    public Long getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Long doctorId) {
        this.doctorId = doctorId;
    }
}
