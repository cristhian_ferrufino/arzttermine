package com.arzttermine.service.calendar.web.dto.response.paging;

import com.arzttermine.library.web.dto.response.paging.PageResponseDto;
import com.arzttermine.service.calendar.web.dto.response.AppointmentDto;

public final class AppointmentsPageDto extends PageResponseDto<AppointmentDto>
{
}
