package com.arzttermine.service.calendar.web.dto.response;

import com.arzttermine.library.web.IdAccessor;
import com.arzttermine.library.web.struct.InsuranceType;
import com.arzttermine.library.web.struct.Language;
import com.arzttermine.service.calendar.web.dto.request.ResourceRequestDto;
import com.arzttermine.service.profile.web.dto.response.DoctorProfileDto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

public class AppointmentDto implements IdAccessor
{
    private long id;

    private Date start;

    private Date end;

    private int duration;

    private boolean booked;

    private boolean enabled;

    private AppointmentLocation practice;

    private DoctorProfileDto doctor;

    private Collection<InsuranceType> availableInsuranceTypes = new ArrayList<>();

    private Collection<Long> availableTreatmentTypes = new ArrayList<>();

    private Collection<Language> availableLanguages = new ArrayList<>();

    private Collection<ResourceRequestDto> resourceRequests = new ArrayList<>();

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Collection<InsuranceType> getAvailableInsuranceTypes() {
        return availableInsuranceTypes;
    }

    public void setAvailableInsuranceTypes(Collection<InsuranceType> availableInsuranceTypes) {
        this.availableInsuranceTypes = availableInsuranceTypes;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Collection<Long> getAvailableTreatmentTypes() {
        return availableTreatmentTypes;
    }

    public void setAvailableTreatmentTypes(Collection<Long> availableTreatmentTypes) {
        this.availableTreatmentTypes = availableTreatmentTypes;
    }

    public Collection<Language> getAvailableLanguages() {
        return availableLanguages;
    }

    public void setAvailableLanguages(Collection<Language> availableLanguages) {
        this.availableLanguages = availableLanguages;
    }

    public DoctorProfileDto getDoctor() {
        return doctor;
    }

    public void setDoctor(DoctorProfileDto doctor) {
        this.doctor = doctor;
    }

    public AppointmentLocation getPractice() {
        return practice;
    }

    public void setPractice(AppointmentLocation practice) {
        this.practice = practice;
    }

    public boolean isBooked() {
        return booked;
    }

    public void setBooked(boolean booked) {
        this.booked = booked;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Collection<ResourceRequestDto> getResourceRequests() {
        return resourceRequests;
    }

    public void setResourceRequests(Collection<ResourceRequestDto> resourceRequests) {
        this.resourceRequests = resourceRequests;
    }

    public static String indexType(long id) {
        return String.format("practice-%d", id);
    }
}
