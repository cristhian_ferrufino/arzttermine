package com.arzttermine.service.calendar.web.dto.request;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

public class ResourceRequestDto
{
    @NotNull
    @ApiModelProperty("the id of the required resource")
    private Long resourceId;

    public ResourceRequestDto() {
    }

    public ResourceRequestDto(long resourceId) {
        this.resourceId = resourceId;
    }

    public Long getResourceId() {
        return resourceId;
    }

    public void setResourceId(Long resourceId) {
        this.resourceId = resourceId;
    }
}
