package com.arzttermine.service.calendar.web.dto.response;

import com.arzttermine.library.web.struct.InsuranceType;
import com.arzttermine.service.calendar.web.dto.RecurringConfigDto;
import com.arzttermine.service.calendar.web.dto.request.ResourceRequestDto;

import java.util.Collection;
import java.util.Date;

public class AvailabilityTimeFrameDto
{
    private long id;
    private Date begin;
    private Date end;
    private int duration;
    private Collection<ResourceRequestDto> resourceRequests;
    private Collection<Long> treatmentTypes;
    private Collection<InsuranceType> insuranceTypes;
    private RecurringConfigDto recurringConfig;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getBegin() {
        return begin;
    }

    public void setBegin(Date begin) {
        this.begin = begin;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Collection<ResourceRequestDto> getResourceRequests() {
        return resourceRequests;
    }

    public void setResourceRequests(Collection<ResourceRequestDto> resourceRequests) {
        this.resourceRequests = resourceRequests;
    }

    public Collection<Long> getTreatmentTypes() {
        return treatmentTypes;
    }

    public void setTreatmentTypes(Collection<Long> treatmentTypes) {
        this.treatmentTypes = treatmentTypes;
    }

    public Collection<InsuranceType> getInsuranceTypes() {
        return insuranceTypes;
    }

    public void setInsuranceTypes(final Collection<InsuranceType> insuranceTypes) {
        this.insuranceTypes = insuranceTypes;
    }

    public RecurringConfigDto getRecurringConfig() {
        return recurringConfig;
    }

    public void setRecurringConfig(RecurringConfigDto recurringConfig) {
        this.recurringConfig = recurringConfig;
    }
}
