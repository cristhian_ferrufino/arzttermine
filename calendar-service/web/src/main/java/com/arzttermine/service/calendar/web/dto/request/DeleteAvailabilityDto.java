package com.arzttermine.service.calendar.web.dto.request;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;
import java.util.Date;

public class DeleteAvailabilityDto
{
    @NotNull
    private Boolean removeAll = false;

    @ApiModelProperty("exclude availability on a certain day (eg: 2017-07-07")
    private LocalDate date;

    public void setRemoveAll(Boolean removeAll) {
        this.removeAll = removeAll;
    }

    public Boolean getRemoveAll() {
        return removeAll;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}
