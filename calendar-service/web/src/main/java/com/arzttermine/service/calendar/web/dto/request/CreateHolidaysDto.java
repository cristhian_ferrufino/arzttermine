package com.arzttermine.service.calendar.web.dto.request;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class CreateHolidaysDto
{
    @NotNull
    private Long practiceId;

    @Valid
    @NotEmpty
    private List<HolidayTimeRangeDto> times = new ArrayList<>();

    public Long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(Long practiceId) {
        this.practiceId = practiceId;
    }

    public List<HolidayTimeRangeDto> getTimes() {
        return times;
    }

    public void setTimes(List<HolidayTimeRangeDto> times) {
        this.times = times;
    }
}
