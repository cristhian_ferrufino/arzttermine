package com.arzttermine.service.calendar.web.dto.request;

import com.arzttermine.library.web.struct.InsuranceType;
import com.arzttermine.library.web.struct.Language;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

public class UpdateAppointmentDto
{
    @ApiModelProperty("should be send as patch")
    private List<Long> treatmentTypeIds;

    @ApiModelProperty("should be send as patch")
    private List<Language> languages;

    @ApiModelProperty("should be send as patch")
    private List<ResourceRequestDto> resources;

    @ApiModelProperty("should be send as patch")
    private List<InsuranceType> insuranceTypes;

    private Boolean enabled;

    public List<Long> getTreatmentTypeIds() {
        return treatmentTypeIds;
    }

    public void setTreatmentTypeIds(List<Long> treatmentTypeIds) {
        this.treatmentTypeIds = treatmentTypeIds;
    }

    public List<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(List<Language> languages) {
        this.languages = languages;
    }

    public List<ResourceRequestDto> getResources() {
        return resources;
    }

    public void setResources(List<ResourceRequestDto> resources) {
        this.resources = resources;
    }

    public List<InsuranceType> getInsuranceTypes() {
        return insuranceTypes;
    }

    public void setInsuranceTypes(List<InsuranceType> insuranceTypes) {
        this.insuranceTypes = insuranceTypes;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
