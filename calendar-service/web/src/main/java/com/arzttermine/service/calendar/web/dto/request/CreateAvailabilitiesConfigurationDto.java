package com.arzttermine.service.calendar.web.dto.request;

import com.arzttermine.service.calendar.web.dto.RecurringConfigDto;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

public class CreateAvailabilitiesConfigurationDto
{
    @Valid
    @NotEmpty
    private List<CreateAvailabilityDto> requests = new ArrayList<>();

    @Valid
    private RecurringConfigDto recurringConfig;

    public List<CreateAvailabilityDto> getRequests() {
        return requests;
    }

    public void setRequests(List<CreateAvailabilityDto> requests) {
        this.requests = requests;
    }

    public RecurringConfigDto getRecurringConfig() {
        return recurringConfig;
    }

    public void setRecurringConfig(RecurringConfigDto recurringConfig) {
        this.recurringConfig = recurringConfig;
    }
}
