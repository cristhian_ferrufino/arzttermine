package com.arzttermine.service.calendar.web.dto.request;

import com.arzttermine.library.web.struct.InsuranceType;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

public class CreateAvailabilityDto
{
    @Future
    @NotNull
    private Date from;

    @NotNull
    private Date to;

    @NotNull
    @Range(min = 5, max = 99999)
    private Integer duration;

    @NotNull
    private Long practiceId;

    @NotEmpty
    private List<ResourceRequestDto> resourceRequests = new ArrayList<>();

    @NotEmpty
    private List<Long> treatmentTypes = new ArrayList<>();

    @NotNull
    private Collection<InsuranceType> insuranceTypes = new HashSet<>();

    @AssertTrue
    public boolean getFromBeforeTo() {
        return null == from || null == to || from.compareTo(to) < 0;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public List<ResourceRequestDto> getResourceRequests() {
        return resourceRequests;
    }

    public void setResourceRequests(List<ResourceRequestDto> resourceRequests) {
        this.resourceRequests = resourceRequests;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(Long practiceId) {
        this.practiceId = practiceId;
    }

    public List<Long> getTreatmentTypes() {
        return treatmentTypes;
    }

    public void setTreatmentTypes(List<Long> treatmentTypes) {
        this.treatmentTypes = treatmentTypes;
    }

    public Collection<InsuranceType> getInsuranceTypes() {
        return insuranceTypes;
    }

    public void setInsuranceTypes(final Collection<InsuranceType> insuranceTypes) {
        this.insuranceTypes = insuranceTypes;
    }
}
