<?php namespace AtSearch\Client;

use Proxy\Proxy;
use AtSearch\Proxy\Response\Filter\PregReplaceFilter;
use AtSearch\Proxy\Response\Filter\RewriteContentReferencesFilter;
use AtSearch\Proxy\Response\Filter\GoogleTagManagerFilter;
use AtSearch\Proxy\Response\Filter\FreespeeFilter;
use Symfony\Component\HttpFoundation\Request;

class DrPletz implements ClientInterface {

    protected $url = 'http://www.dr-pletz.de'; // no leading slash

    /**
     * @inheritdoc
     */
    public function getBaseUrl() {
        return $this->url;
    }

    /**
     * @inheritdoc
     */
    public function getUrl(Request $request) {
        return $this->getBaseUrl() . $request->getRequestUri();
    }

    /**
     * @inheritdoc
     */
    public function setFilters(Proxy $proxy) {
        $proxy->addResponseFilter(new RewriteContentReferencesFilter());

        // add GoogleTagManager with new ID
        $filter = new GoogleTagManagerFilter();
        $filter->setGoogleTagManagerId('GTM-PW4GJC');
        $proxy->addResponseFilter($filter);

        /*
         * phone: add the city code
         */
        $filter = new PregReplaceFilter();
        $filter
            ->setPattern('#96662980#')
            ->setReplacement('0211 - 96662980')
            ->setFilterMimeTypeClasses(array('html'));
        $proxy->addResponseFilter($filter);

        /*
         * fax: add the city code
         */
        $filter = new PregReplaceFilter();
        $filter
            ->setPattern('#96662989#')
            ->setReplacement('0211 - 96662989')
            ->setFilterMimeTypeClasses(array('html'));
        $proxy->addResponseFilter($filter);

        $filter = new FreespeeFilter();
        $filter->setFreespeeId('4323fc4f-a6f8-450f-96bf-4cefb32d4bfe');
        $proxy->addResponseFilter($filter);

        return true;
    }
}
