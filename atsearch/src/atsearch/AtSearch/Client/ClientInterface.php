<?php namespace AtSearch\Client;

use Proxy\Proxy;
use Symfony\Component\HttpFoundation\Request;

interface ClientInterface {

    /**
     * Returns the base url (schema + hostname) where the content is coming from
     *
     * @return string url (no leading slash)
     */
    public function getBaseUrl();

    /**
     * Returns the url where the content is coming from
     *
     * @return string url
     */
    public function getUrl(Request $request);

    /**
     * Set the proxy filters for this client
     *
     * @param  Proxy $proxy
     * @return bool
     */
    public function setFilters(Proxy $proxy);
}
