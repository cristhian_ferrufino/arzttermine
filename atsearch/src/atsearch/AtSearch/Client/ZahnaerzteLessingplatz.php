<?php namespace AtSearch\Client;

use Proxy\Proxy;
use AtSearch\Proxy\Response\Filter\GoogleTagManagerFilter;
use AtSearch\Proxy\Response\Filter\RewriteContentReferencesFilter;
use AtSearch\Proxy\Response\Filter\PregReplaceFilter;
use AtSearch\Proxy\Response\Filter\FreespeeFilter;
use Symfony\Component\HttpFoundation\Request;

class ZahnaerzteLessingplatz implements ClientInterface {

    protected $url = 'http://www.zahnaerzte-lessingplatz.de'; // no leading slash

    /**
     * @inheritdoc
     */
    public function getBaseUrl() {
        return $this->url;
    }

    /**
     * @inheritdoc
     */
    public function getUrl(Request $request) {
        return $this->getBaseUrl() . $request->getRequestUri();
    }

    /**
     * @inheritdoc
     */
    public function setFilters(Proxy $proxy) {
        $proxy->addResponseFilter(new RewriteContentReferencesFilter());

        /*
        * add the phone number
        */
        $filter = new PregReplaceFilter();
        $filter
            ->setPattern('%(<h2>Ihre Zahngesundheit<br />im Visier!</h2>)%Uis')
            ->setReplacement('<h2 style="margin-bottom: 1em;">Ihre Zahngesundheit<br />im Visier!</h2><div style="color: #3f9ed3; margin-bottom: 85px; font-size:18px; font-weight:bold;">Telefon: 0211 - 7221 59</div>')
            ->setLimit(1)
            ->setFilterMimeTypeClasses(array('html'));
        $proxy->addResponseFilter($filter);

        // add GoogleTagManager with new ID
        $filter = new GoogleTagManagerFilter();
        $filter->setGoogleTagManagerId('GTM-MPK9LC');
        $proxy->addResponseFilter($filter);

        $filter = new FreespeeFilter();
        $filter->setFreespeeId('20bd4be0-7117-4082-9ce3-647ed8f3e6c9');
        $proxy->addResponseFilter($filter);

        return true;
    }
}
