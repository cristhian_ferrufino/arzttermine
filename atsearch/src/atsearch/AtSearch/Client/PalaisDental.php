<?php namespace AtSearch\Client;

use Proxy\Proxy;
use AtSearch\Proxy\Response\Filter\RewriteContentReferencesFilter;
use AtSearch\Proxy\Response\Filter\GoogleTagManagerFilter;
use AtSearch\Proxy\Response\Filter\PregReplaceFilter;
use AtSearch\Proxy\Response\Filter\FreespeeFilter;
use Symfony\Component\HttpFoundation\Request;

class PalaisDental implements ClientInterface {

    protected $url = 'http://www.palais-dental.de'; // no leading slash

    /**
     * @inheritdoc
     */
    public function getBaseUrl() {
        return $this->url;
    }

    /**
     * @inheritdoc
     */
    public function getUrl(Request $request) {
        return $this->getBaseUrl() . $request->getRequestUri();
    }

    /**
     * @inheritdoc
     */
    public function setFilters(Proxy $proxy) {
        $proxy->addResponseFilter(new RewriteContentReferencesFilter());

        /*
        * add the comment
        */
        $filter = new PregReplaceFilter();
        $filter
            ->setPattern('%(<div id="kontakt_box">.*<!-- #kontakt_box -->)%Uis')
            ->setReplacement('<div id="kontakt_box">
				<a href="tel:089255448115">
				<div id="kontakt_box_text" style="font-size:20px;font-weight:bold;">089 - 2 55 44 81 15</div>
				<div id="kontakt_box_icon"><img src="/wp-content/themes/palais_dental/images/kontakt_icon.png" width="24" height="24"></div>
				</a>
			</div><!-- #kontakt_box -->')
            ->setLimit(1)
            ->setFilterMimeTypeClasses(array('html'));
        $proxy->addResponseFilter($filter);

        // add GoogleTagManager with new ID
        $filter = new GoogleTagManagerFilter();
        $filter->setGoogleTagManagerId('GTM-53MN9V');
        $proxy->addResponseFilter($filter);

        $filter = new FreespeeFilter();
        $filter->setFreespeeId('cd235e0e-5b57-4ce3-af1b-c2613ec2a0b5');
        $proxy->addResponseFilter($filter);

        return true;
    }
}
