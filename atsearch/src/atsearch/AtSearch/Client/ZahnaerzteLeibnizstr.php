<?php namespace AtSearch\Client;

use Proxy\Proxy;
use AtSearch\Proxy\Response\Filter\GoogleTagManagerFilter;
use AtSearch\Proxy\Response\Filter\RewriteContentReferencesFilter;
use AtSearch\Proxy\Response\Filter\FreespeeFilter;
use Symfony\Component\HttpFoundation\Request;

class ZahnaerzteLeibnizStr implements ClientInterface {

    protected $url = 'http://www.zahnaerzte-leibnizstr.de'; // no leading slash

    /**
     * @inheritdoc
     */
    public function getBaseUrl() {
        return $this->url;
    }

    /**
     * @inheritdoc
     */
    public function getUrl(Request $request) {
        return $this->getBaseUrl() . $request->getRequestUri();
    }

    /**
     * @inheritdoc
     */
    public function setFilters(Proxy $proxy) {
        $proxy->addResponseFilter(new RewriteContentReferencesFilter());

/*
        $filter = new PregReplaceFilter();
        $filter
            ->setPattern('#<title>([^<]*)</title>#')
            ->setReplacement('<title>Neuer Title</title>')
            ->setLimit(1)
            ->setFilterRequestUriPattern('#/#')
            ->setFilterMimeTypeClasses(array('html'));
        $proxy->addResponseFilter($filter);
*/
        // remove GoogleTagManager
        $filter = new GoogleTagManagerFilter();
        $filter->setAction('remove');
        $proxy->addResponseFilter($filter);

        // add GoogleTagManager with new ID
        $filter = new GoogleTagManagerFilter();
        $filter->setGoogleTagManagerId('GTM-KDP64D');
        $proxy->addResponseFilter($filter);

        $filter = new FreespeeFilter();
        $filter->setFreespeeId('50d19171-69b9-48dd-90bc-ba5493f4f9a9');
        $filter->setFreespeeOptions("__fs_conf.push(['addElementClass', 'phone']);");
        $proxy->addResponseFilter($filter);

        return true;
    }
}
