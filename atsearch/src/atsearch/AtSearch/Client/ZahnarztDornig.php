<?php namespace AtSearch\Client;

use Proxy\Proxy;
use AtSearch\Proxy\Response\Filter\RewriteContentReferencesFilter;
use AtSearch\Proxy\Response\Filter\GoogleTagManagerFilter;
use AtSearch\Proxy\Response\Filter\PregReplaceFilter;
use AtSearch\Proxy\Response\Filter\FreespeeFilter;
use Symfony\Component\HttpFoundation\Request;

class ZahnarztDornig implements ClientInterface {

    protected $url = 'http://www.zahnarzt-dornig.de'; // no leading slash

    /**
     * @inheritdoc
     */
    public function getBaseUrl() {
        return $this->url;
    }

    /**
     * @inheritdoc
     */
    public function getUrl(Request $request) {
        return $this->getBaseUrl() . $request->getRequestUri();
    }

    /**
     * @inheritdoc
     */
    public function setFilters(Proxy $proxy) {
        $proxy->addResponseFilter(new RewriteContentReferencesFilter());

        /*
        * replace the logo (without the phone number)
        */
        $filter = new PregReplaceFilter();
        $filter
            ->setPattern('%(<div id="sidebar">)%Uis')
            ->setReplacement('\1<div class="phone-replacement" style="background: url(/client/zahnarzt-dornig/icon-phone.png) no-repeat; margin: 10px 0 15px 25px; padding: 0 0 5px 40px;">089 / 980460</div>')
            ->setLimit(1)
            ->setFilterMimeTypeClasses(array('html'));
        $proxy->addResponseFilter($filter);

        /*
        * add the phone number
        */
        $filter = new PregReplaceFilter();
        $filter
            ->setPattern('%(http://u.jimdo.com/www70/o/s484798950e6b0757/emotion/crop/header.jpg)%Uis')
            ->setReplacement('/client/zahnarzt-dornig/header-atsearch-dorig.jpg')
            ->setLimit(1)
            ->setFilterMimeTypeClasses(array('html'));
        $proxy->addResponseFilter($filter);

        // add GoogleTagManager with new ID
        $filter = new GoogleTagManagerFilter();
        $filter->setGoogleTagManagerId('GTM-5Z25XP');
        $proxy->addResponseFilter($filter);

        $filter = new FreespeeFilter();
        $filter->setFreespeeId('ec2d14f1-cc34-403c-8bdd-e371f5f0fb6e');
        $proxy->addResponseFilter($filter);

        return true;
    }
}
