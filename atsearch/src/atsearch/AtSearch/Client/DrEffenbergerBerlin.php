<?php namespace AtSearch\Client;

use AtSearch\Proxy\Response\Filter\AtWidgetFilter;
use Proxy\Proxy;
use AtSearch\Proxy\Response\Filter\RewriteContentReferencesFilter;
use AtSearch\Proxy\Response\Filter\PregReplaceFilter;
use AtSearch\Proxy\Response\Filter\GoogleTagManagerFilter;
use AtSearch\Proxy\Response\Filter\FreespeeFilter;
use Symfony\Component\HttpFoundation\Request;

class DrEffenbergerBerlin implements ClientInterface {

    protected $url = 'http://www.dr-effenberger-berlin.de'; // no leading slash

    /**
     * @inheritdoc
     */
    public function getBaseUrl() {
        return $this->url;
    }

    /**
     * @inheritdoc
     */
    public function getUrl(Request $request) {
        return $this->getBaseUrl() . $request->getRequestUri();
    }

    /**
     * @inheritdoc
     */
    public function setFilters(Proxy $proxy) {
        $proxy->addResponseFilter(new RewriteContentReferencesFilter());

        /*
         * remove the flash player and everything under that to the "Herzlich Willkommen" from the homepage
         */
        $filter = new PregReplaceFilter();
        $filter
            ->setPattern('#<div align="left"><div id="mediaPlayer_5ba1a119_d0ec_49d2_8856_04d1ac158c8e_container"(.*)<div align="left"><span class="text-class-4">Herzlich Willkommen#Uis')
            ->setReplacement('<div align="left"><span class="text-class-4">Herzlich Willkommen')
            ->setLimit(1)
            ->setFilterMimeTypeClasses(array('html'));
        $proxy->addResponseFilter($filter);

        /*
         * add the phone number
         */
        $filter = new PregReplaceFilter();
        $filter
            ->setPattern('#(<tr height="280" id="midrow">)#Uis')
            ->setReplacement('<tr><td><div style="padding-right:70px; text-align: right;color: #4b8ac9; font-size: 10px;background-color:#ffffff;">Tel: <a class="phone" href="tel:0302912161" style="color: #4b8ac9;font-size:16px; text-decoration: none;">030 291 21 61</a></div></td><td style="background-color: #ffffff;">&nbsp;</td><td style="background-color: #ffffff;">&nbsp;</td></tr>$1')
            ->setLimit(1)
            ->setFilterMimeTypeClasses(array('html'));
        $proxy->addResponseFilter($filter);

        $filter = new GoogleTagManagerFilter();
        $filter->setGoogleTagManagerId('GTM-T79PGM');
        $proxy->addResponseFilter($filter);

        $filter = new FreespeeFilter();
        $filter->setFreespeeId('c952bd15-6964-4ccd-939d-2027b28b7ee4');
        $proxy->addResponseFilter($filter);

        $filter = new AtWidgetFilter();
        $filter->setPracticeId(30);
        $proxy->addResponseFilter($filter);

        return true;
    }
}
