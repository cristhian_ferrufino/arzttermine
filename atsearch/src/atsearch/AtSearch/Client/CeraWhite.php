<?php namespace AtSearch\Client;

use Proxy\Proxy;
use AtSearch\Proxy\Response\Filter\RewriteContentReferencesFilter;
use AtSearch\Proxy\Response\Filter\PregReplaceFilter;
use AtSearch\Proxy\Response\Filter\GoogleTagManagerFilter;
use Symfony\Component\HttpFoundation\Request;

class CeraWhite implements ClientInterface {

    protected $url = 'http://www.cerawhite.com'; // no leading slash

    /**
     * @inheritdoc
     */
    public function getBaseUrl() {
        return $this->url;
    }

    /**
     * @inheritdoc
     */
    public function getUrl(Request $request) {
        return $this->getBaseUrl() . $request->getRequestUri();
    }

    /**
     * @inheritdoc
     */
    public function setFilters(Proxy $proxy) {
        $proxy->addResponseFilter(new RewriteContentReferencesFilter());

        /*
         * remove the phone number image and replace it with a new image and some text
         */
        $filter = new PregReplaceFilter();
        $filter
            ->setPattern('#<div class=[\'|"]head-phone[\'|"]></div>#Uis')
            ->setReplacement('<div style="background-image: url(\'/client/cerawhite/static/img/phone.png\');background-position:left center;background-repeat:no-repeat; float: left; height: 55px;margin-top: -85px;width: 278px; padding-left: 70px;"><div style="padding-top:6px;">MO.-SA 09:00 BIS 21:00 Uhr<br ><span><a href="tel:021130293029" style="font-weight: bold; font-size: 20px;">0211 - 3029 3029</a></span></div></div>')
            ->setLimit(1)
            ->setFilterMimeTypeClasses(array('html'));
        $proxy->addResponseFilter($filter);

        // add GoogleTagManager with new ID
        $filter = new GoogleTagManagerFilter();
        $filter->setGoogleTagManagerId('GTM-N9BQ6M');
        $proxy->addResponseFilter($filter);

        /*
         * replace the number before we send it to freespee
         */
        $filter = new PregReplaceFilter();
        $filter
            ->setPattern('#0211[ -/]*3029[ ]*3029#Uis')
            ->setReplacement('0211 - 74953715')
            ->setFilterMimeTypeClasses(array('html'));
        $proxy->addResponseFilter($filter);


        /*
         * add a class to linked phone numbers so that freespee can replace them too
         */
        $filter = new PregReplaceFilter();
        $filter
            ->setPattern('#<a href="tel:#Uis')
            ->setReplacement('<a class="phone" href="tel:')
            ->setFilterMimeTypeClasses(array('html'));
        $proxy->addResponseFilter($filter);

        return true;
    }
}
