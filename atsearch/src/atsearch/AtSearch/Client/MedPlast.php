<?php namespace AtSearch\Client;

use Proxy\Proxy;
use AtSearch\Proxy\Response\Filter\RewriteContentReferencesFilter;
use AtSearch\Proxy\Response\Filter\GoogleTagManagerFilter;
use Symfony\Component\HttpFoundation\Request;

class MedPlast implements ClientInterface {

    protected $url = 'http://www.med-plast.de'; // no leading slash

    /**
     * @inheritdoc
     */
    public function getBaseUrl() {
        return $this->url;
    }

    /**
     * @inheritdoc
     */
    public function getUrl(Request $request) {
        return $this->getBaseUrl() . $request->getRequestUri();
    }

    /**
     * @inheritdoc
     */
    public function setFilters(Proxy $proxy) {
        $proxy->addResponseFilter(new RewriteContentReferencesFilter());

        // add GoogleTagManager with new ID
        $filter = new GoogleTagManagerFilter();
        $filter->setGoogleTagManagerId('GTM-KHHB4Q');
        $proxy->addResponseFilter($filter);

        /*
         * commented out on request from jb:hipchat:20141210-16:57
        $filter = new FreespeeFilter();
        $filter->setFreespeeId('1ea60f5d-5b82-4444-959c-f352bc37b61d');
        $proxy->addResponseFilter($filter);
        */

        return true;
    }
}
