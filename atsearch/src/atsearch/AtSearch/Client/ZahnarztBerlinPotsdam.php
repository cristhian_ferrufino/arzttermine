<?php namespace AtSearch\Client;

use Proxy\Proxy;
use AtSearch\Proxy\Response\Filter\RewriteContentReferencesFilter;
use AtSearch\Proxy\Response\Filter\GoogleTagManagerFilter;
use AtSearch\Proxy\Response\Filter\FreespeeFilter;
use Symfony\Component\HttpFoundation\Request;

class ZahnarztBerlinPotsdam implements ClientInterface {

    protected $url = 'http://www.zahnarztberlinpotsdam.de';

    /**
     * @inheritdoc
     */
    public function getBaseUrl() {
        return $this->url;
    }

    /**
     * @inheritdoc
     */
    public function getUrl(Request $request) {
        return $this->getBaseUrl() . $request->getRequestUri();
    }

    /**
     * @inheritdoc
     */
    public function setFilters(Proxy $proxy) {

        $proxy->addResponseFilter(new RewriteContentReferencesFilter());

        // add GoogleTagManager with new ID
        $filter = new GoogleTagManagerFilter();
        $filter->setAction('remove');
        $proxy->addResponseFilter($filter);

        // add GoogleTagManager with new ID
        $filter = new GoogleTagManagerFilter();
        $filter->setGoogleTagManagerId('GTM-MLNFJWD');
        $proxy->addResponseFilter($filter);

        $filter = new FreespeeFilter();
        $filter->setFreespeeId('cc05ccac-fc45-4e2f-aa34-446b30fa7c6c');
        $proxy->addResponseFilter($filter);

        return true;
    }
}
