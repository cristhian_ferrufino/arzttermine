<?php namespace AtSearch\Client;

use AtSearch\Proxy\Response\Filter\GoogleAnalyticsFilter;
use Proxy\Proxy;
use AtSearch\Proxy\Response\Filter\RewriteContentReferencesFilter;
use AtSearch\Proxy\Response\Filter\GoogleTagManagerFilter;
use AtSearch\Proxy\Response\Filter\FreespeeFilter;
use AtSearch\Proxy\Response\Filter\PregReplaceFilter;
use Symfony\Component\HttpFoundation\Request;

class SchmidtPich implements ClientInterface {

    protected $url = 'https://www.schmidt-pich.de'; // no leading slash

    const googleTrackingReplacement = '$1<!-- Google Code for Terminbuchung Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 960398075;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "eojWCKD-xlkQ-4X6yQM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
<noscript><div style="display:inline;"><img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/960398075/?label=eojWCKD-xlkQ-4X6yQM&amp;guid=ON&amp;script=0"/></div>
</noscript>';

    /**
     * @inheritdoc
     */
    public function getBaseUrl() {
        return $this->url;
    }

    /**
     * @inheritdoc
     */
    public function getUrl(Request $request) {
        return $this->getBaseUrl() . $request->getRequestUri();
    }

    /**
     * @inheritdoc
     */
    public function setFilters(Proxy $proxy) {
        $proxy->addResponseFilter(new RewriteContentReferencesFilter());

        /*
        * add a tracking pixel on the booking confirmation
        */
        $filter = new PregReplaceFilter();
        $filter
            ->setPattern('%(</h3><strong>Ihr Termin wurde verbindlich gebucht.</strong>)%Uis')
            ->setReplacement(self::googleTrackingReplacement)
            ->setLimit(1)
            ->setFilterMimeTypeClasses(array('html'));
        $proxy->addResponseFilter($filter);

        $filter = new FreespeeFilter();
        $filter->setFreespeeId('6806c2c7-0064-4aaa-a074-6c0287fdef84');
        $proxy->addResponseFilter($filter);

        return true;
    }
}
