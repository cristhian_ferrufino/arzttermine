<?php namespace AtSearch\Client;

use AtSearch\Proxy\Response\Filter\PregReplaceFilter;
use Proxy\Proxy;
use AtSearch\Proxy\Response\Filter\RewriteContentReferencesFilter;
use AtSearch\Proxy\Response\Filter\GoogleTagManagerFilter;
use Symfony\Component\HttpFoundation\Request;

class KompetenznetzFranken implements ClientInterface {

    protected $url = 'http://www.kompetenznetz-franken.de'; // no leading slash

    /**
     * @inheritdoc
     */
    public function getBaseUrl() {
        return $this->url;
    }

    /**
     * @inheritdoc
     */
    public function getUrl(Request $request) {
        return $this->getBaseUrl() . $request->getRequestUri();
    }

    /**
     * @inheritdoc
     */
    public function setFilters(Proxy $proxy) {
        $proxy->addResponseFilter(new RewriteContentReferencesFilter());

        /*
         * replace the base
         */
        $filter = new PregReplaceFilter();
        $filter
            ->setPattern('#"typo3#')
            ->setReplacement('"/typo3')
            ->setFilterMimeTypeClasses(array('html'));
        $proxy->addResponseFilter($filter);

        /*
         * remove the base (if exists)
         */
        $filter = new PregReplaceFilter();
        $filter
            ->setPattern('#<base href="http://www.kompetenznetz-franken.de/">#')
            ->setReplacement('')
            ->setFilterMimeTypeClasses(array('html'));
        $proxy->addResponseFilter($filter);

        /*
         * set the base (as all links are relative)
         */
        $filter = new PregReplaceFilter();
        $filter
            ->setPattern('#<meta charset="utf-8">#')
            ->setReplacement('<meta charset="utf-8"><base href="http://kompetenznetz-franken.atsearch.de/">')
            ->setFilterMimeTypeClasses(array('html'));
        $proxy->addResponseFilter($filter);

        /*
         * replace some numbers with matelso
         * if <a href> is used than it needs to be changed in the controll pannel
         */
        $filter = new PregReplaceFilter();
        $filter
            ->setPattern('#<p class="bodytext">Tel.: 0911 - 80 124 100([^<]*)</p>#Uis')
//            ->setReplacement("<a href='tel:0911 / 95159676-0' data-role=\"button\" class=\"tracking_tn\">'0911 / 95159676-0'</a>")
            ->setReplacement("<span data-role=\"button\" class=\"tracking_tn\"></span>")
            ->setFilterMimeTypeClasses(array('html'));
        $proxy->addResponseFilter($filter);

        /*
         * add some js for matelso
         */
        $filter = new PregReplaceFilter();
        $filter
            ->setPattern('#</body>#')
            ->setReplacement("<script type=\"text/javascript\">
                tn_pool_id='XfQQZfDz8vRpFeFnp9Ukfig1ayl9u2';
                fallback_tn='0911 / 95159676-0';
                var th = document.getElementsByTagName('body')[0];
                var s = document.createElement('script');
                s.setAttribute('type','text/javascript');
                s.setAttribute('src','http' + (document.location.protocol == 'https:' ? 's' : '')+'://rns.matelso.de/webtracking.js?tn_pool_id='+tn_pool_id+'&type='+(typeof type == 'undefined' ? '' : type));
                th.appendChild(s);
                function GEBCN(e){if(document.getElementsByClassName){return document.getElementsByClassName(e);}e=e.replace(/ *$/,\"\");if(document.querySelectorAll){return document.querySelectorAll((\" \"+e).replace(/ +/g,\".\"));}e=e.replace(/^ */,\"\");var t=e.split(/ +/),n=t.length;var r=document.getElementsByTagName(\"*\"),i=r.length;var s=[];var o,u,a;for(o=0;o<i;o++){a=true;for(u=n;u--;){if(!RegExp(\" \"+t[u]+\" \").test(\" \"+r[o].className+\" \")){a=false;}}if(a){s.push(r[o]);}}return s;}
        if (typeof get_cookie != 'function'){var els = GEBCN('tracking_tn');for ( var i = 0; i < els.length; i++ ) {var el = els[i];    el.innerHTML = fallback_tn;el.href = fallback_tn;};};
</script>")
            ->setFilterMimeTypeClasses(array('html'));
        $proxy->addResponseFilter($filter);

        // add GoogleTagManager with new ID
        $filter = new GoogleTagManagerFilter();
        $filter->setGoogleTagManagerId('GTM-MX777W');
        $proxy->addResponseFilter($filter);

        return true;
    }
}
