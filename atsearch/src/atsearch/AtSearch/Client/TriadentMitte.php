<?php namespace AtSearch\Client;

use Proxy\Proxy;
use AtSearch\Proxy\Response\Filter\RewriteContentReferencesFilter;
use AtSearch\Proxy\Response\Filter\GoogleTagManagerFilter;
use AtSearch\Proxy\Response\Filter\PregReplaceFilter;
use AtSearch\Proxy\Response\Filter\FreespeeFilter;
use Symfony\Component\HttpFoundation\Request;

class TriadentMitte implements ClientInterface {

    protected $url = 'http://www.triadent-mitte.de'; // no leading slash

    /**
     * @inheritdoc
     */
    public function getBaseUrl() {
        return $this->url;
    }

    /**
     * @inheritdoc
     */
    public function getUrl(Request $request) {
        $url = $request->getRequestUri();
        // this client can't take ANY parameters. So remove them for the request.
        $url = strtok($url, '?');
        return $this->getBaseUrl() . $url;
    }

    /**
     * @inheritdoc
     */
    public function setFilters(Proxy $proxy) {
        $proxy->addResponseFilter(new RewriteContentReferencesFilter());

        /*
        * add the comment
        */
        $filter = new PregReplaceFilter();
        $filter
            ->setPattern('%(<a class="termin" href="/kontakt/mail.html">Terminanfrage</a>)%Uis')
            ->setReplacement('<a class="termin" href="/terminanfrage.html">Terminanfrage</a>')
            ->setLimit(1)
            ->setFilterMimeTypeClasses(array('html'));
        $proxy->addResponseFilter($filter);

        // add GoogleTagManager with new ID
        $filter = new GoogleTagManagerFilter();
        $filter->setGoogleTagManagerId('GTM-MR5DXT');
        $proxy->addResponseFilter($filter);

        $filter = new FreespeeFilter();
        $filter->setFreespeeId('6b32be97-d1fe-4bef-a517-1fc1224fe544');
        $proxy->addResponseFilter($filter);

        return true;
    }
}
