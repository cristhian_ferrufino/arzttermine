<?php namespace AtSearch\Client;

use Proxy\Proxy;
use AtSearch\Proxy\Response\Filter\RewriteContentReferencesFilter;
use AtSearch\Proxy\Response\Filter\GoogleTagManagerFilter;
use AtSearch\Proxy\Response\Filter\PregReplaceFilter;
use AtSearch\Proxy\Response\Filter\FreespeeFilter;
use Symfony\Component\HttpFoundation\Request;

class SpecialDoctors implements ClientInterface {

    protected $url = 'http://www.special-doctors.de'; // no leading slash

    /**
     * @inheritdoc
     */
    public function getBaseUrl() {
        return $this->url;
    }

    /**
     * @inheritdoc
     */
    public function getUrl(Request $request) {
        return $this->getBaseUrl() . $request->getRequestUri();
    }

    /**
     * @inheritdoc
     */
    public function setFilters(Proxy $proxy) {
        $proxy->addResponseFilter(new RewriteContentReferencesFilter());

        /*
        * add the comment
        */
        $filter = new PregReplaceFilter();
        $filter
            ->setPattern('%(</div> <!-- end #header -->)%Uis')
            ->setReplacement('$1<div style="color:#000; margin-top: -10px; margin-bottom: 8px; text-align: center; font-size: 20px; font-family:\'ColaborateThinRegular\', Arial, sans-serif;">Special Doctors ist eine Privat- &amp; Selbstzahlerpraxis!<span style="display: inline; color:#000; text-align: right; position: absolute; margin-right: 10px; right: 0; font-size: 16px; font-family:\'ColaborateThinRegular\', Arial, sans-serif;">Tel: <a href="tel:030226053000" style="color:#000;">+49 30 2260 53000</a></span></div>')
            ->setLimit(1)
            ->setFilterMimeTypeClasses(array('html'));
        $proxy->addResponseFilter($filter);

        // add GoogleTagManager with new ID
        $filter = new GoogleTagManagerFilter();
        $filter->setGoogleTagManagerId('GTM-MCQSRH');
        $proxy->addResponseFilter($filter);

        $filter = new FreespeeFilter();
        $filter->setFreespeeId('1f8886d9-06c3-46f7-95f3-42e3aeec2d24');
        $proxy->addResponseFilter($filter);

        return true;
    }
}
