<?php namespace AtSearch\Client;

use AtSearch\Proxy\Response\Filter\AtWidgetFilter;
use AtSearch\Proxy\Response\Filter\PregReplaceFilter;
use Proxy\Proxy;
use AtSearch\Proxy\Response\Filter\RewriteContentReferencesFilter;
use AtSearch\Proxy\Response\Filter\GoogleTagManagerFilter;
use AtSearch\Proxy\Response\Filter\FreespeeFilter;
use Symfony\Component\HttpFoundation\Request;

class Zahnarztfriedrichshain implements ClientInterface {

    protected $url = 'http://zahnarztfriedrichshain.de'; // no leading slash

    const atWidgetReplacement = '$1<div class="full_column text-left" data-animation-delay="100"><h3>Jetzt Termin vereinbaren
  </h3>
<div data-color="#003e89" id="atwidget-outer-container"></div><script src="https://atwidget.arzttermine.de/w/walter-Effenberger-1471509511.js"></script><p style="text-align: center; font-size:13px">Mit freundlicher Unterstützung von <a href="https://www.arzttermine.de/" title="Arzttermine beim Zahnarzt kostenfrei online buchen auf Arzttermine.de" target="_blank">Arzttermine.de</a></p>
</div>';

    /**
     * @inheritdoc
     */
    public function getBaseUrl() {
        return $this->url;
    }

    /**
     * @inheritdoc
     */
    public function getUrl(Request $request) {
        return $this->getBaseUrl() . $request->getRequestUri();
    }

    /**
     * @inheritdoc
     */
    public function setFilters(Proxy $proxy) {
        $proxy->addResponseFilter(new RewriteContentReferencesFilter());

        $filter = new GoogleTagManagerFilter();
        $filter->setAction('remove');
        $proxy->addResponseFilter($filter);

        $filter = new GoogleTagManagerFilter();
        $filter->setGoogleTagManagerId('GTM-T79PGM');
        $proxy->addResponseFilter($filter);

        /*
        * add atWidget
        */
        $filter = new PregReplaceFilter();
        $filter
            ->setPattern('%(<div class="new_section container-fluid "  style=" background-color:#fff;"   ><div class="container"  ><div class="row"><div class="section_visible_column  col-md-12"><div class="asalah_row row" >)%Uis')
            ->setReplacement(self::atWidgetReplacement)
            ->setLimit(1)
            ->setFilterMimeTypeClasses(array('html'));
        $proxy->addResponseFilter($filter);

        $filter = new FreespeeFilter();
        $filter->setFreespeeId('c952bd15-6964-4ccd-939d-2027b28b7ee4');
        $proxy->addResponseFilter($filter);

        $filter = new AtWidgetFilter();
        $filter->setPracticeId(30);
        $proxy->addResponseFilter($filter);

        return true;
    }
}
