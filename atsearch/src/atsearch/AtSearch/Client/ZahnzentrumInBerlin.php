<?php namespace AtSearch\Client;

use Proxy\Proxy;
use AtSearch\Proxy\Response\Filter\RewriteContentReferencesFilter;
use AtSearch\Proxy\Response\Filter\GoogleTagManagerFilter;
use AtSearch\Proxy\Response\Filter\FreespeeFilter;
use Symfony\Component\HttpFoundation\Request;

class ZahnzentrumInBerlin implements ClientInterface {

    protected $url = 'http://www.zahnzentrum-in-berlin.de'; // no leading slash

    /**
     * @inheritdoc
     */
    public function getBaseUrl() {
        return $this->url;
    }

    /**
     * @inheritdoc
     */
    public function getUrl(Request $request) {
        return $this->getBaseUrl() . $request->getRequestUri();
    }

    /**
     * @inheritdoc
     */
    public function setFilters(Proxy $proxy) {
        $proxy->addResponseFilter(new RewriteContentReferencesFilter());

        // add GoogleTagManager with new ID
        $filter = new GoogleTagManagerFilter();
        $filter->setGoogleTagManagerId('GTM-WDR9VRT');
        $proxy->addResponseFilter($filter);

        $filter = new FreespeeFilter();
        $filter->setFreespeeId('78709e24-c349-4e87-bb78-5abbee11c704');
        $proxy->addResponseFilter($filter);

        return true;
    }
}
