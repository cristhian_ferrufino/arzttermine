<?php namespace AtSearch\Client;

use Proxy\Proxy;
use AtSearch\Proxy\Response\Filter\RewriteContentReferencesFilter;
use AtSearch\Proxy\Response\Filter\GoogleAnalyticsFilter;
use AtSearch\Proxy\Response\Filter\GoogleTagManagerFilter;
use AtSearch\Proxy\Response\Filter\FreespeeFilter;
use Symfony\Component\HttpFoundation\Request;

class ZahnarztSuelz implements ClientInterface {

    protected $url = 'http://zahnarzt-suelz.de'; // no leading slash

    /**
     * @inheritdoc
     */
    public function getBaseUrl() {
        return $this->url;
    }

    /**
     * @inheritdoc
     */
    public function getUrl(Request $request) {
        return $this->getBaseUrl() . $request->getRequestUri();
    }

    /**
     * @inheritdoc
     */
    public function setFilters(Proxy $proxy) {
        $proxy->addResponseFilter(new RewriteContentReferencesFilter());

        // remove GoogleAnalytics
        $filter = new GoogleAnalyticsFilter();
        $filter->setAction('remove');
        $proxy->addResponseFilter($filter);

        // add GoogleTagManager with new ID
        $filter = new GoogleTagManagerFilter();
        $filter->setGoogleTagManagerId('GTM-TV8RGK');
        $proxy->addResponseFilter($filter);

        $filter = new FreespeeFilter();
        $filter->setFreespeeId('3a91ecfb-ed40-43ac-b41e-62114e4fd3ea');
        $proxy->addResponseFilter($filter);

        return true;
    }
}
