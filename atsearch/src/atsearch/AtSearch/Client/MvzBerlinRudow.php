<?php namespace AtSearch\Client;

use AtSearch\Proxy\Response\Filter\PregReplaceFilter;
use Proxy\Proxy;
use AtSearch\Proxy\Response\Filter\RewriteContentReferencesFilter;
use AtSearch\Proxy\Response\Filter\GoogleAnalyticsFilter;
use AtSearch\Proxy\Response\Filter\GoogleTagManagerFilter;
use AtSearch\Proxy\Response\Filter\FreespeeFilter;
use Symfony\Component\HttpFoundation\Request;

class MvzBerlinRudow implements ClientInterface {

    protected $url = 'http://www.mvz-berlin-rudow.de'; // no leading slash

    /**
     * @inheritdoc
     */
    public function getBaseUrl() {
        return $this->url;
    }

    /**
     * @inheritdoc
     */
    public function getUrl(Request $request) {
        return $this->getBaseUrl() . $request->getRequestUri();
    }

    /**
     * @inheritdoc
     */
    public function setFilters(Proxy $proxy) {
        $proxy->addResponseFilter(new RewriteContentReferencesFilter());

        // remove GoogleAnalytics
        $filter = new GoogleAnalyticsFilter();
        $filter->setAction('remove');
        $proxy->addResponseFilter($filter);

        // add GoogleTagManager with new ID
        $filter = new GoogleTagManagerFilter();
        $filter->setGoogleTagManagerId('GTM-TKQQV3');
        $proxy->addResponseFilter($filter);

        /*
        * enlarge the number field
        */
        $filter = new PregReplaceFilter();
        $filter
            ->setPattern('%<div class="tel-contact hidden-phone">%Uis')
            ->setReplacement('<div class="tel-contact hidden-phone" style="width:245px">')
            ->setLimit(1)
            ->setFilterMimeTypeClasses(array('html'));
        $proxy->addResponseFilter($filter);

        $filter = new FreespeeFilter();
        $filter->setFreespeeId('c62881c6-946a-457f-8143-90ed040ece93');
        $proxy->addResponseFilter($filter);

        return true;
    }
}
