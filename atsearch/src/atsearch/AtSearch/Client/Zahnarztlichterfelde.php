<?php namespace AtSearch\Client;

use Proxy\Proxy;
use AtSearch\Proxy\Response\Filter\RewriteContentReferencesFilter;
use AtSearch\Proxy\Response\Filter\GoogleTagManagerFilter;
use AtSearch\Proxy\Response\Filter\FreespeeFilter;
use Symfony\Component\HttpFoundation\Request;

class Zahnarztlichterfelde implements ClientInterface {

    protected $url = 'http://www.zahnarztlichterfelde.de'; // no leading slash

    /**
     * @inheritdoc
     */
    public function getBaseUrl() {
        return $this->url;
    }

    /**
     * @inheritdoc
     */
    public function getUrl(Request $request) {
        return $this->getBaseUrl() . $request->getRequestUri();
    }

    /**
     * @inheritdoc
     */
    public function setFilters(Proxy $proxy) {
        $proxy->addResponseFilter(new RewriteContentReferencesFilter());

        $filter = new GoogleTagManagerFilter();
        $filter->setAction('remove');
        $proxy->addResponseFilter($filter);

        $filter = new GoogleTagManagerFilter();
        $filter->setGoogleTagManagerId('GTM-TJSFCN');
        $proxy->addResponseFilter($filter);

        $filter = new FreespeeFilter();
        $filter->setFreespeeId('2935abc7-a462-434f-af81-a0292bcf47d4');
        $proxy->addResponseFilter($filter);

        return true;
    }
}
