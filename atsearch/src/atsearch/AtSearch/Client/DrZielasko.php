<?php namespace AtSearch\Client;

use Proxy\Proxy;
use AtSearch\Proxy\Response\Filter\RewriteContentReferencesFilter;
use AtSearch\Proxy\Response\Filter\PregReplaceFilter;
use AtSearch\Proxy\Response\Filter\GoogleTagManagerFilter;
use Symfony\Component\HttpFoundation\Request;

class DrZielasko implements ClientInterface {

    protected $url = 'http://www.dr-zielasko.de'; // no leading slash

    /**
     * @inheritdoc
     */
    public function getBaseUrl() {
        return $this->url;
    }

    /**
     * @inheritdoc
     */
    public function getUrl(Request $request) {
        return $this->getBaseUrl() . $request->getRequestUri();
    }

    /**
     * @inheritdoc
     */
    public function setFilters(Proxy $proxy) {
        /*
         * remove the Doxter iframe
         */
        $filter = new PregReplaceFilter();
        $filter
            ->setPattern('#<h3>Terminvereinbarung</h3>(.*)</div></div>#Uis')
            ->setReplacement('')
            ->setLimit(1)
            ->setFilterMimeTypeClasses(array('html'));
        $proxy->addResponseFilter($filter);
        /*
         * remove the Doxter button
         */
        $filter = new PregReplaceFilter();
        $filter
            ->setPattern('#<div class="textwidget"><a href="http://www.doxter.de/zahnarzt-berlin/dr-johannes-zielasko" target="_blank"><img src="/wp-content/uploads/2014/08/doxter.jpg" style="float: left;"/></a></div>#Uis')
            ->setReplacement('')
            ->setLimit(1)
            ->setFilterMimeTypeClasses(array('html'));
        $proxy->addResponseFilter($filter);

        $proxy->addResponseFilter(new RewriteContentReferencesFilter());

        // add GoogleTagManager with new ID
        $filter = new GoogleTagManagerFilter();
        $filter->setGoogleTagManagerId('GTM-PSKTZM');
        $proxy->addResponseFilter($filter);

        /*
         * replace the number before we send it to freespee
         */
        $filter = new PregReplaceFilter();
        $filter
            ->setPattern('#030[ -/]*7725003#Uis')
            ->setReplacement('030 - 208 98 31 92')
            ->setFilterMimeTypeClasses(array('html'));
        $proxy->addResponseFilter($filter);

        return true;
    }
}
