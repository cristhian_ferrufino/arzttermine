<?php namespace AtSearch\Client;

use Proxy\Proxy;
use AtSearch\Proxy\Response\Filter\RewriteContentReferencesFilter;
use AtSearch\Proxy\Response\Filter\GoogleTagManagerFilter;
use AtSearch\Proxy\Response\Filter\PregReplaceFilter;
use AtSearch\Proxy\Response\Filter\FreespeeFilter;
use Symfony\Component\HttpFoundation\Request;

class MasterEndodontieBerlin implements ClientInterface {

    protected $url = 'http://www.master-endodontie-berlin.de'; // no leading slash

    /**
     * @inheritdoc
     */
    public function getBaseUrl() {
        return $this->url;
    }

    /**
     * @inheritdoc
     */
    public function getUrl(Request $request) {
        return $this->getBaseUrl() . $request->getRequestUri();
    }

    /**
     * @inheritdoc
     */
    public function setFilters(Proxy $proxy) {
        $proxy->addResponseFilter(new RewriteContentReferencesFilter());

        /*
        * add the comment
        */
        $filter = new PregReplaceFilter();
        $filter
            ->setPattern('%(<ul class="cssMenu cssMenum">)%Uis')
            ->setReplacement('<div style="float:right;display: block;margin-top:17px;margin-right:30px;"><span style="color:#fff; text-align: right; font-size: 16px; font-family:Arial, sans-serif;">Tel: <a href="tel:0309711525" style="color:#fff;">030 - 971 15 25</a></span></div>$1')
            ->setLimit(1)
            ->setFilterMimeTypeClasses(array('html'));
        $proxy->addResponseFilter($filter);

        // add GoogleTagManager with new ID
        $filter = new GoogleTagManagerFilter();
        $filter->setGoogleTagManagerId('GTM-TJQBVF');
        $proxy->addResponseFilter($filter);

        $filter = new FreespeeFilter();
        $filter->setFreespeeId('efbe961e-987e-47fd-96e6-4de20ca0797d');
        $proxy->addResponseFilter($filter);

        return true;
    }
}
