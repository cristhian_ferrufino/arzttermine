<?php namespace AtSearch\Client;

use AtSearch\Proxy\Response\Filter\PregReplaceFilter;
use Proxy\Proxy;
use AtSearch\Proxy\Response\Filter\RewriteContentReferencesFilter;
use AtSearch\Proxy\Response\Filter\PiwikFilter;
use AtSearch\Proxy\Response\Filter\GoogleTagManagerFilter;
use AtSearch\Proxy\Response\Filter\FreespeeFilter;
use Symfony\Component\HttpFoundation\Request;

class ZahnzentrumHoffmeier implements ClientInterface {

    protected $url = 'http://zahnzentrum-hoffmeier.de'; // no leading slash

    /**
     * @inheritdoc
     */
    public function getBaseUrl() {
        return $this->url;
    }

    /**
     * @inheritdoc
     */
    public function getUrl(Request $request) {
        return $this->getBaseUrl() . $request->getRequestUri();
    }

    /**
     * @inheritdoc
     */
    public function setFilters(Proxy $proxy) {
        $proxy->addResponseFilter(new RewriteContentReferencesFilter());

        // remove Piwik
        $filter = new PiwikFilter();
        $filter->setAction('remove');
        $proxy->addResponseFilter($filter);

        // add GoogleTagManager with new ID
        $filter = new GoogleTagManagerFilter();
        $filter->setGoogleTagManagerId('GTM-N422RD');
        $filter->setInjectJSTemplatePregFilter('#(<header id="masthead">)#i');
        $proxy->addResponseFilter($filter);

        /*
         * remove the other practice
         */
        $filter = new PregReplaceFilter();
        $filter
            ->setPattern('#<aside class="small" id="pos_9">(.*)</aside>#Uis')
            ->setReplacement('')
            ->setLimit(1)
            ->setFilterMimeTypeClasses(array('html'));
        $proxy->addResponseFilter($filter);

        $filter = new PregReplaceFilter();
        // (?:(?!string).)
        // http://php.net/manual/de/function.preg-match.php#103015
        // - remove the Marzahn part
        // - it's two times on the page, so set limit or remove the limit completly
        $filter
            ->setPattern('#<address>(?:(?!Lichtenberg).)*Marzahn(.*)</address>#Uis')
            ->setReplacement('')
            ->setLimit(2)
            ->setFilterMimeTypeClasses(array('html'));
        $proxy->addResponseFilter($filter);

        // /termin.php
        $filter = new PregReplaceFilter();
        $filter
            ->setPattern('#<h2><span class="nowrap marzahn">Berlin Marzahn</span></h2>#Uis')
            ->setReplacement('')
            ->setLimit(1)
            ->setFilterMimeTypeClasses(array('html'));
        $proxy->addResponseFilter($filter);

        $filter = new FreespeeFilter();
        $filter->setFreespeeId('8ed430a9-1966-4253-9b00-a80d257db1aa');
        $proxy->addResponseFilter($filter);

        return true;
    }
}
