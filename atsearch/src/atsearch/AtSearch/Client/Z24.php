<?php namespace AtSearch\Client;

use AtSearch\Proxy\Response\Filter\PregReplaceFilter;
use Proxy\Proxy;
use AtSearch\Proxy\Response\Filter\RewriteContentReferencesFilter;
use AtSearch\Proxy\Response\Filter\GoogleAnalyticsFilter;
use AtSearch\Proxy\Response\Filter\GoogleTagManagerFilter;
use AtSearch\Proxy\Response\Filter\FreespeeFilter;
use Symfony\Component\HttpFoundation\Request;

class Z24 implements ClientInterface {

    protected $url = 'http://www.z-24.de'; // no leading slash

    /**
     * @inheritdoc
     */
    public function getBaseUrl() {
        return $this->url;
    }

    /**
     * @inheritdoc
     */
    public function getRawUrl(Request $request) {
        return $this->getBaseUrl() . $request->getRequestUri();
    }

    /**
     * @inheritdoc
     */
    public function getUrl(Request $request) {
        $url = $request->getRequestUri();

        // there are some references that accept the "?" like
        // http://www.z-24.de/scripts/get.aspx?media=/shop/layout/home/z-24-header-start.jpg
        if (preg_match('#scripts#i', $url)) {
            return $this->getRawUrl($request);
        }
        // this client can't take ANY parameters. So remove them for the request.
        $url = strtok($url, '?');
        return $this->getBaseUrl() . $url;
    }

    /**
     * @inheritdoc
     */
    public function setFilters(Proxy $proxy) {
        $proxy->addResponseFilter(new RewriteContentReferencesFilter());

        // remove GoogleAnalytics
        $filter = new GoogleAnalyticsFilter();
        $filter->setAction('remove');
        $proxy->addResponseFilter($filter);

        // add GoogleTagManager with new ID
        $filter = new GoogleTagManagerFilter();
        $filter->setGoogleTagManagerId('GTM-T8NQ9J');
        $proxy->addResponseFilter($filter);

        /*
        * enlarge the number field
        */
        $filter = new PregReplaceFilter();
        $filter
            ->setPattern('%<div class="icon-navi hidden-xs">%Uis')
            ->setReplacement('<div class="icon-navi hidden-xs" style="width:420px">')
            ->setLimit(1)
            ->setFilterMimeTypeClasses(array('html'));
        $proxy->addResponseFilter($filter);

        $filter = new FreespeeFilter();
        $filter->setFreespeeId('c1bec8c6-a888-4155-9a43-3dba9c00608b');
        $proxy->addResponseFilter($filter);

        return true;
    }
}
