<?php namespace AtSearch\Client;

use Proxy\Proxy;
use AtSearch\Proxy\Response\Filter\RewriteContentReferencesFilter;
use AtSearch\Proxy\Response\Filter\GoogleTagManagerFilter;
use AtSearch\Proxy\Response\Filter\PregReplaceFilter;
use Symfony\Component\HttpFoundation\Request;

class DrMeine implements ClientInterface {

    protected $url = 'http://www.dr-meine.de'; // no leading slash

    /**
     * @inheritdoc
     */
    public function getBaseUrl() {
        return $this->url;
    }

    /**
     * @inheritdoc
     */
    public function getUrl(Request $request) {
        return $this->getBaseUrl() . $request->getRequestUri();
    }

    /**
     * @inheritdoc
     */
    public function setFilters(Proxy $proxy) {
        $proxy->addResponseFilter(new RewriteContentReferencesFilter());

        // add GoogleTagManager with new ID
        $filter = new GoogleTagManagerFilter();
        $filter->setGoogleTagManagerId('GTM-WT4G5R');
        $proxy->addResponseFilter($filter);

        /*
         * replace the number before we send it to freespee
         */
        $html = '<div style="height: 3em; width: 100%; background-color: ffddb4; color:#ff9600; text-align: center; line-height: 3em; font-size: 1.2em; font-weight:bold;">Kontakt: <a class="phone" style="color:#ff9600; text-decoration: none;" href="tel:042140894919">0421 - 40894919</a></div>';
        $filter = new PregReplaceFilter();
        $filter
            ->setPattern('#(<body[^>]*>)#i')
            ->setReplacement('$1' . $html)
            ->setLimit(1)
            ->setFilterMimeTypeClasses(array('html'));
        $proxy->addResponseFilter($filter);

        /*
         * replace the number before we send it to freespee
         */
        $filter = new PregReplaceFilter();
        $filter
            ->setPattern('#0421[ -/]*44 16 12#Uis')
            ->setReplacement('0421 - 40894919')
            ->setFilterMimeTypeClasses(array('html'));
        $proxy->addResponseFilter($filter);

        return true;
    }
}
