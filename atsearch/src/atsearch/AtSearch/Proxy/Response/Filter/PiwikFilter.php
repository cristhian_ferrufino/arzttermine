<?php namespace AtSearch\Proxy\Response\Filter;

use Proxy\Response\Filter\ResponseFilterInterface;
use Symfony\Component\HttpFoundation\Response;
use AtSearch\Application\Application;

/**
 * Class PiwikFilter
 * @package AtSearch\Proxy\Response\Filter
 *
 * Add/remove a Piwik analytics js
 *
 * Use like this:
 *  $filter = new PiwikFilter();
 *  $filter
 *  ->setGoogleAnalyticsId('GTM-ABCDEF');
 *  $proxy->addResponseFilter($filter);
 */
class PiwikFilter implements ResponseFilterInterface {

    /**
     * @var string
     * Can be 'add' || 'remove'
     */
    protected $action = 'add';

    /**
     * Execute the filter
     *
     * @param  Response $response
     * @return Response
     */
    public function filter(Response $response)
    {
        $app = Application::getInstance();
        // execute this filter only for specific mimeTypeClasses
        $mimeTypeClass = $app->getResponseMimeTypeClass($response);
        if (!in_array($mimeTypeClass, array('html'))) {
            return;
        }

        $content = $response->getContent();

        switch ($this->getAction()) {
            case 'remove':
                $content = preg_replace('#<!-- Piwik -->(.*)<!-- End Piwik Tracking Code -->#Uis', '', $content);   // watch the U modifier
                break;
        }

        $response->setContent($content);
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string $action
     * @return self
     */
    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }
}
