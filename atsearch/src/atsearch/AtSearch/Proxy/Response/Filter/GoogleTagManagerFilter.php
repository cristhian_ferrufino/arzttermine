<?php namespace AtSearch\Proxy\Response\Filter;

use Proxy\Response\Filter\ResponseFilterInterface;
use Symfony\Component\HttpFoundation\Response;
use AtSearch\Application\Application;
use AtSearch\Proxy\Response\Filter\Traits\FilterRequestUriPatternTrait;

/**
 * Class GoogleTagManagerFilter
 * @package AtSearch\Proxy\Response\Filter
 *
 * Add a google tag manager js
 *
 * Use like this:
 *  $filter = new GoogleTagManagerFilter();
 *  $filter
 *  ->setGoogleTagManagerId('GTM-ABCDEF');
 *  $proxy->addResponseFilter($filter);
 */
class GoogleTagManagerFilter implements ResponseFilterInterface {

    /**
     * @var string
     */
    protected $googleTagManagerId;

    /**
     * @var string
     */
    protected $injectJSTemplatePregFilter = '#(<body[^>]*>)#i';

    /**
     * @var string
     * Can be 'add' || 'remove'
     */
    protected $action = 'add';

    /**
     * @var string
     */
    const JAVASCRIPT_TEMPLATE = <<<EOF
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-ID" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-ID');</script>
<!-- End Google Tag Manager -->

EOF;

    use FilterRequestUriPatternTrait;

    /**
     * Execute the filter
     *
     * @param  Response $response
     * @return Response
     */
    public function filter(Response $response)
    {
        $app = Application::getInstance();
        // execute this filter only for specific mimeTypeClasses
        $mimeTypeClass = $app->getResponseMimeTypeClass($response);
        if (!in_array($mimeTypeClass, array('html'))) {
            return;
        }
        if (!$this->filterRequestUriPattern($response))
            return;

        $content = $response->getContent();

        switch ($this->getAction()) {
            case 'remove':
                // first try the simple way
                $content = preg_replace('#<!-- Google Tag Manager -->(.*)<!-- End Google Tag Manager -->#Uis', '', $content);   // watch the U modifier
                // if comments have been cut out try to match as much as possible
                $content = preg_replace('#<noscript><iframe src="//www.googletagmanager.com/ns.html(.*)</script>#Uis', '', $content);   // watch the U modifier
                break;
            case 'add':
                $googleTagManagerId = $this->getGoogleTagManagerId();
                // prepare the js
                $javascriptTemplate = str_replace('GTM-ID', $googleTagManagerId, self::JAVASCRIPT_TEMPLATE);

                $injectJSTemplatePregFilter = $this->getInjectJSTemplatePregFilter();
                // put in the js under the <body> tag (default)
                $content = preg_replace($injectJSTemplatePregFilter, '$1' . $javascriptTemplate, $content, 1);
                break;
        }

        $response->setContent($content);
    }

    /**
     * @return string
     */
    public function getInjectJSTemplatePregFilter()
    {
        return $this->injectJSTemplatePregFilter;
    }

    /**
     * @param string $injectJSTemplatePregFilter
     * @return self
     */
    public function setInjectJSTemplatePregFilter($injectJSTemplatePregFilter)
    {
        $this->injectJSTemplatePregFilter = $injectJSTemplatePregFilter;
        return $this;
    }

    /**
     * @return string
     */
    public function getGoogleTagManagerId()
    {
        return $this->googleTagManagerId;
    }

    /**
     * @param string $googleTagManagerId
     */
    public function setGoogleTagManagerId($googleTagManagerId)
    {
        $this->googleTagManagerId = $googleTagManagerId;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string $action
     * @return self
     */
    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }
}
