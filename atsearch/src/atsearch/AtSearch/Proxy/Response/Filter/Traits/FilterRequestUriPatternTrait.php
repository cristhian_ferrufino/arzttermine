<?php namespace AtSearch\Proxy\Response\Filter\Traits;

use Symfony\Component\HttpFoundation\Response;
use AtSearch\Application\Application;

/**
 * Trait FilterRequestUriPatternTrait
 * @package AtSearch\Proxy\Response\Filter\Traits
 *
 */
trait FilterRequestUriPatternTrait {
    /**
     * Execute this filter(s) only for this url
     * Can be string or array of strings
     *
     * @var mixed preg_match() regex
     */
    protected $filterRequestUriPattern = '';

    /**
     * Rewrite all anchors
     *
     * @param  Response $response
     * @return bool
     */
    public function filterRequestUriPattern(Response $response)
    {
        $app = Application::getInstance();
        // execute this filter only for specific urls
        $match = true;
        $filterRequestUriPatterns = $this->getFilterRequestUriPattern();
        if (!empty($filterRequestUriPatterns)) {
            $match = false;
            // if its only one filter than convert it also to an array
            if (!is_array($filterRequestUriPatterns)) {
                $filterRequestUriPatterns = array($filterRequestUriPatterns);
            }
            // at least one filter should match
            $requestUri = $app->getRequest()->getRequestUri();
            foreach ($filterRequestUriPatterns as $filterRequestUriPattern) {
                if (preg_match($filterRequestUriPattern, $requestUri)) {
                    $match = true;
                }
            }
        }
        return $match;
    }

    /**
     * @return string
     */
    public function getFilterRequestUriPattern()
    {
        return $this->filterRequestUriPattern;
    }

    /**
     * @param mixed $filterRequestUriPattern
     * @return self
     */
    public function setFilterRequestUriPattern($filterRequestUriPattern)
    {
        $this->filterRequestUriPattern = $filterRequestUriPattern;
        return $this;
    }
}
