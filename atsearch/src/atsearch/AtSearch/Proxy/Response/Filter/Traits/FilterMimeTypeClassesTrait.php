<?php namespace AtSearch\Proxy\Response\Filter\Traits;

use Symfony\Component\HttpFoundation\Response;
use AtSearch\Application\Application;

/**
 * Trait FilterMimeTypeClassesTrait
 * @package AtSearch\Proxy\Response\Filter\Traits
 *
 */
trait FilterMimeTypeClassesTrait {
    /**
     * Execute this filter only for these mimeTypeClasses
     *
     * @var array
     */
    protected $filterMimeTypeClasses = array();

    /**
     * Execute the filter
     *
     * @param  Response $response
     * @return bool
     */
    public function filterMimeTypeClasses(Response $response)
    {
        $app = Application::getInstance();
        // execute this filter only for specific mimeTypeClasses
        $mimeTypeClass = $app->getResponseMimeTypeClass($response);
        $filterMimeTypeClasses = $this->getFilterMimeTypeClasses();
        if (!empty($filterMimeTypeClasses)) {
            if (!in_array($mimeTypeClass, $filterMimeTypeClasses)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @return array
     */
    public function getFilterMimeTypeClasses()
    {
        return $this->filterMimeTypeClasses;
    }

    /**
     * @param array $mimeTypeClasses
     * @return self
     */
    public function setFilterMimeTypeClasses($mimeTypeClasses)
    {
        $this->filterMimeTypeClasses = $mimeTypeClasses;
        return $this;
    }
}
