<?php namespace AtSearch\Proxy\Response\Filter;

use Proxy\Response\Filter\ResponseFilterInterface;
use Symfony\Component\HttpFoundation\Response;
use AtSearch\Application\Application;

/**
 * Class GoogleAnalyticsFilter
 * @package AtSearch\Proxy\Response\Filter
 *
 * Add/remove a google analytics js
 *
 * Use like this:
 *  $filter = new GoogleAnalyticsFilter();
 *  $filter
 *  ->setGoogleAnalyticsId('GTM-ABCDEF');
 *  $proxy->addResponseFilter($filter);
 */
class GoogleAnalyticsFilter implements ResponseFilterInterface {

    /**
     * @var string
     */
    protected $googleAnalyticsId;

    /**
     * @var string
     * Can be 'add' || 'remove'
     */
    protected $action = 'add';

    /**
     * @var string
     */
    const JAVASCRIPT_TEMPLATE = <<<EOF
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'GOOGLEANALYTICS-ID', 'auto');
  ga('send', 'pageview');

</script>

EOF;

    /**
     * Execute the filter
     *
     * @param  Response $response
     * @return Response
     */
    public function filter(Response $response)
    {
        $app = Application::getInstance();
        // execute this filter only for specific mimeTypeClasses
        $mimeTypeClass = $app->getResponseMimeTypeClass($response);
        if (!in_array($mimeTypeClass, array('html'))) {
            return;
        }

        $content = $response->getContent();

        switch ($this->getAction()) {
            case 'remove':
                // old version
                $content = preg_replace('#<script>([^</script]*)var _gaq = _gaq \|\| \[\];(.*)</script>#Uis', '', $content);   // watch the U modifier
                $content = preg_replace('#<script type="text/javascript">([^</script]*)var _gaq = _gaq \|\| \[\];(.*)</script>#Uis', '', $content);   // watch the U modifier
                // universal analytics
                $content = preg_replace('#<script>(.*)\(function\(i,s,o,g,r,a,m\){i\[\'GoogleAnalyticsObject\'\](.*)</script>#Uis', '', $content);   // watch the U modifier
                break;
            case 'add':
                $googleAnalyticsId = $this->getGoogleAnalyticsId();
                // prepare the js
                $javascriptTemplate = str_replace('GOOGLEANALYTICS-ID', $googleAnalyticsId, self::JAVASCRIPT_TEMPLATE);
                // put in the js under the <body> tag
                $content = preg_replace('#(<body[^>]*>)#i', '$1' . $javascriptTemplate, $content, 1);
                break;
        }

        $response->setContent($content);
    }

    /**
     * @return string
     */
    public function getGoogleAnalyticsId()
    {
        return $this->googleAnalyticsId;
    }

    /**
     * @param string $googleAnalyticsId
     */
    public function setGoogleAnalyticsId($googleAnalyticsId)
    {
        $this->googleAnalyticsId = $googleAnalyticsId;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string $action
     * @return self
     */
    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }
}
