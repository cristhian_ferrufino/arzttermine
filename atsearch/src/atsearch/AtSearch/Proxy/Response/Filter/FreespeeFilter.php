<?php namespace AtSearch\Proxy\Response\Filter;

use Proxy\Response\Filter\ResponseFilterInterface;
use Symfony\Component\HttpFoundation\Response;
use AtSearch\Application\Application;

/**
 * Class FreespeeFilter
 * @package AtSearch\Proxy\Response\Filter
 *
 * Add a google tag manager js
 *
 * Use like this:
 *  $filter = new GoogleTagManagerFilter();
 *  $filter
 *  ->setGoogleTagManagerId('GTM-ABCDEF');
 *  $proxy->addResponseFilter($filter);
 *
 * Possible options:
 *  $filter->setFreespeeOptions("__fs_conf.push(['addElementClass', 'icon-phone']);");
 */
class FreespeeFilter implements ResponseFilterInterface {

    /**
     * @var string
     */
    protected $freespeeId;

    /**
     * @var string
     */
    protected $freespeeOptions = '';

    /**
     * @var string
     * Can be 'add' || 'remove'
     */
    protected $action = 'add';

    /**
     * @var string
     */
    const JAVASCRIPT_TEMPLATE = <<<EOF
<script type="text/javascript">
    var __fs_conf = __fs_conf || [];
    __fs_conf.push(['setAdv',{'id': 'FREESPEE-ID'}]);
    /* FREESPEE-OPTIONS */
</script>

<script type="text/javascript" src="//analytics.freespee.com/js/external/fs.js"></script>

EOF;

    /**
     * Execute the filter
     *
     * @param  Response $response
     * @return Response
     */
    public function filter(Response $response)
    {
        $app = Application::getInstance();
        // execute this filter only for specific mimeTypeClasses
        $mimeTypeClass = $app->getResponseMimeTypeClass($response);
        if (!in_array($mimeTypeClass, array('html'))) {
            return;
        }

        $content = $response->getContent();

        switch ($this->getAction()) {
            case 'remove':
                // @todo: not tested
                $content = preg_replace('#<script type="text/javascript">[\w]+var __fs_conf = __fs_conf || \[\];(.*)<script type="text/javascript" src="//analytics.freespee.com/js/external/fs.js"></script>#Uis', '', $content);   // watch the U modifier
                break;
            case 'add':
                /*
                 * Replace the "bad" utf-8 dashes with "normal" dashes.
                 */
                $content = str_replace('–', '-', $content); // two different dashes. You can't see it. But believe me they are there :)
                $content = str_replace('&#8211;', '-', $content);

                // add the freespee js
                $freespeeId = $this->getFreespeeId();
                // prepare the js
                $javascriptTemplate = str_replace('FREESPEE-ID', $freespeeId, self::JAVASCRIPT_TEMPLATE);

                // replace the options (if any)
                $javascriptTemplate = str_replace('/* FREESPEE-OPTIONS */', $this->getFreespeeOptions(), $javascriptTemplate);

                // put in the js above the </body> tag
                $content = preg_replace('#(</body>)#i', $javascriptTemplate . '$1', $content, 1);
                break;
        }

        $response->setContent($content);
    }

    /**
     * @return string
     */
    public function getFreespeeId()
    {
        return $this->freespeeId;
    }

    /**
     * @param string $freespeeId
     */
    public function setFreespeeId($freespeeId)
    {
        $this->freespeeId = $freespeeId;
    }

    /**
     * @return string
     */
    public function getFreespeeOptions()
    {
        return $this->freespeeOptions;
    }

    /**
     * @param string $freespeeOptions
     */
    public function setFreespeeOptions($freespeeOptions)
    {
        $this->freespeeOptions = $freespeeOptions;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string $action
     * @return self
     */
    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }
}
