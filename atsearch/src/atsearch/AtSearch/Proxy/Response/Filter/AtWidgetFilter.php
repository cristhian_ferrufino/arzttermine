<?php

namespace AtSearch\Proxy\Response\Filter;

use Proxy\Response\Filter\ResponseFilterInterface;
use Symfony\Component\HttpFoundation\Response;
use AtSearch\Application\Application;

/**
 * Class AtWidgetFilter
 * @package AtSearch\Proxy\Response\Filter
 *
 * Add our at widget
 *
 * Use like this:
 *  $filter = new AtWidgetFilter();
 *  $filter->setPracticeId(42);
 *  $proxy->addResponseFilter($filter);
 */
class AtWidgetFilter implements ResponseFilterInterface {

    /**
     * @var string
     */
    protected $practiceId;

    /**
     * @var string
     * Can be 'add'
     */
    protected $action = 'add';

    /**
     * @var string
     */
    const JAVASCRIPT_TEMPLATE = <<<EOF
<script type="text/javascript">
    window.at_practice = PRACTICE_ID;
    window.at_token = 'd2lkZ2V0LWJvb2tpbmctY2xpZW50Om9mZmljaWFsLWNsaWVudA==';
</script>
<script src="https://widget.arzttermine.de/widget.js" id="atwidget_script" async></script>
EOF;

    /**
     * Execute the filter
     *
     * @param  Response $response
     * @return Response
     */
    public function filter(Response $response)
    {
        $app = Application::getInstance();
        // execute this filter only for specific mimeTypeClasses
        $mimeTypeClass = $app->getResponseMimeTypeClass($response);
        if (!in_array($mimeTypeClass, array('html'))) {
            return;
        }

        $content = $response->getContent();

        if ($this->getAction() == 'add') {
            $template = str_replace('PRACTICE_ID', $this->getPracticeId(), self::JAVASCRIPT_TEMPLATE);
            $content = str_replace('<span id="AtWidgetSpace"></span>', $template, $content);
        }

        $response->setContent($content);
    }

    /**
     * @return string
     */
    public function getPracticeId()
    {
        return $this->practiceId;
    }

    /**
     * @param int $practiceId
     */
    public function setPracticeId($practiceId)
    {
        $this->practiceId = $practiceId;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string $action
     * @return self
     */
    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }
}
