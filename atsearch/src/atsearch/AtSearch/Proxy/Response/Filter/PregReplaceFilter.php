<?php namespace AtSearch\Proxy\Response\Filter;

use Proxy\Response\Filter\ResponseFilterInterface;
use Symfony\Component\HttpFoundation\Response;
use AtSearch\Proxy\Response\Filter\Traits\FilterRequestUriPatternTrait;
use AtSearch\Proxy\Response\Filter\Traits\FilterMimeTypeClassesTrait;

/**
 * Class PregReplaceFilter
 * @package AtSearch\Proxy\Response\Filter
 *
 * Replace or add a string
 *
 * REPLACEMENT:
 *  $filter = new PregReplaceFilter();
 *  $filter
 *  ->setPattern('#<title>([^<]*)</title>#')
 *  ->setReplacement('<title>Neuer Title</title>')
 *  ->setLimit(1)
 *  ->setFilterMimeTypeClasses(array('html'));
 *  $proxy->addResponseFilter($filter);
 *
 * INSERT ABOVE:
 *  $filter = new PregReplaceFilter();
 *  $filter
 *  ->setPattern('#(</body[^>]*>)#i')
 *  ->setReplacement($content . '$1')
 *  ->setLimit(1)
 *  ->setFilterMimeTypeClasses(array('html'));
 *  $proxy->addResponseFilter($filter);
 *
 * INSERT AFTER:
 *  $filter = new PregReplaceFilter();
 *  $filter
 *  ->setPattern('#(<body[^>]*>)#i')
 *  ->setReplacement('$1' . $content)
 *  ->setLimit(1)
 *  ->setFilterMimeTypeClasses(array('html'));
 *  $proxy->addResponseFilter($filter);
 */
class PregReplaceFilter implements ResponseFilterInterface {
    /**
     * @var string
     */
    protected $pattern;

    /**
     * @var string
     */
    protected $replacement;

    /**
     * The maximum possible replacements for each pattern in each string. Defaults to -1 (no limit).
     * Matches preg_replace() parameter limit
     *
     * @var int
     */
    protected $limit = -1;

    use FilterMimeTypeClassesTrait;
    use FilterRequestUriPatternTrait;

    /**
     * Execute the filter
     *
     * @param  Response $response
     * @return Response
     */
    public function filter(Response $response)
    {
        if (!$this->filterMimeTypeClasses($response))
            return;

        if (!$this->filterRequestUriPattern($response))
            return;

        $content = $response->getContent();
        $pattern = $this->getPattern();
        $replacement = $this->getReplacement();
        $limit = $this->getLimit();

        $content = preg_replace($pattern, $replacement, $content, $limit, $tmp);

        $response->setContent($content);
    }

    /**
     * @return string
     */
    public function getPattern()
    {
        return $this->pattern;
    }

    /**
     * Set the string to match
     *
     * @param  string
     * @return self
     */
    public function setPattern($pattern)
    {
        $this->pattern = $pattern;
        return $this;
    }

    /**
     * @return string
     */
    public function getReplacement()
    {
        return $this->replacement;
    }

    /**
     * Add the string to be replaced or added
     *
     * @param  string
     * @return self
     */
    public function setReplacement($string)
    {
        $this->replacement = $string;
        return $this;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param  int
     * @return self
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
        return $this;
    }
}
