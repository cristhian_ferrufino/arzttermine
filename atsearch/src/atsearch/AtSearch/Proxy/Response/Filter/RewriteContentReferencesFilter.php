<?php namespace AtSearch\Proxy\Response\Filter;

use Proxy\Response\Filter\ResponseFilterInterface;
use Symfony\Component\HttpFoundation\Response;
use AtSearch\Parser\Parser;
use AtSearch\Application\Application;
use AtSearch\Proxy\Response\Filter\Traits\FilterRequestUriPatternTrait;
use AtSearch\Proxy\Response\Filter\Traits\FilterMimeTypeClassesTrait;

/**
 * Class RewriteContentReferencesFilter
 * @package AtSearch\Proxy\Response\Filter
 *
 * Wrapper for the main filter which converts all references like href, links, etc
 * in html, css and js
 */

class RewriteContentReferencesFilter implements ResponseFilterInterface {
    use FilterMimeTypeClassesTrait;
    use FilterRequestUriPatternTrait;

    /**
     * Rewrite all anchors
     *
     * @param  Response $response
     * @return Response
     */
    public function filter(Response $response)
    {
        if (!$this->filterMimeTypeClasses($response))
            return;

        if (!$this->filterRequestUriPattern($response))
            return;

        $app = Application::getInstance();
        $mimeTypeClass = $app->getResponseMimeTypeClass($response);
        $content = $response->getContent();

        $parser = new Parser();
        switch ($mimeTypeClass) {
            case 'html':
                $content = $parser->HTMLDocument($content);
                break;
            case 'css':
                $content = $parser->CSS($content);
                break;
            case 'js':
                // This requires more investigations to work
                // $content = $parser->JS($content);
                break;
            default:
                // don't touch anything else
                break;
        }

        $response->setContent($content);
    }
}
