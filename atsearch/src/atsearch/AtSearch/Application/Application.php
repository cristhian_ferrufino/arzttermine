<?php
/**
 * This App is a wrapper for the proxy class:
 * https://github.com/jenssegers/php-proxy
 *
 * http://symfony.com/doc/current/components/http_foundation/introduction.html
 */

namespace AtSearch\Application;

use Proxy\Factory;
use Proxy\Proxy;
use Proxy\Response\Filter\RemoveEncodingFilter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AtSearch\Proxy\Response\Filter\PregReplaceFilter;

/**
 * Class Application
 */
class Application {

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var string
     */
    protected $host;

    /**
     * @var string
     */
    protected $subDomain;

    /**
     * @var \AtSearch\Client\ClientInterface
     */
    protected $client;

    /**
     * Singleton instance of the class
     *
     * @var Application
     * @static
     */
    private static $instance;

    /**
     * Singleton accessor
     *
     * @return self
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Proxy the Request
     *
     * @return self
     */
    public function proxyRequest()
    {
        // Create the proxy factory.
        $proxy = Factory::create();

        // Add a response filter that removes the encoding headers.
        $proxy->addResponseFilter(new RemoveEncodingFilter());

        // Create a Symfony request based on the current browser request.
        $request = Request::createFromGlobals();
        // store the request here if a filter needs it
        $this->setRequest($request);
        $this->setHost($request->getHost());

        // get the client and set all needed filters
        try {
            $client = $this->createClient($this->getHost());
        } catch (\ErrorException $e) {
            echo 'Error: Domain not configured.';
            self::log('Exception: '.$e->getMessage());
            exit;
        }
        $this->setClient($client);
        $client->setFilters($proxy);
        $this->addGlobalFilters($proxy);

        $url = $client->getUrl($request);

        Application::debug('Fetching: ' . $url);
        // Forward the request and get the response.
        $response = $proxy->forward($request)->to($url);

        // Remove the Content-Length as we already removed
        // the header "Content-Encoding" in RemoveEncodingFilter().
        // If the Content-Encoding was set by the origin server then the length will not match anymore at this stage.
        // Also: Usually the webserver (apache, ...) also can gzip the content and modifies the
        // content-length as well. So let him do the job and gzip + recalc the content-length.
        $response->headers->remove('Content-Length');

        // Output response to the browser.
        $response->send();
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param Request $request
     * @return self
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;
        return $this;
    }

    /**
     * Set the Host
     *
     * @return self
     */
    public function setHost($host)
    {
        $this->host = $host;
        return $this;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param Response $response
     * @return string
     */
    public static function getResponseMimeType(Response $response) {
        $mimeType = '';
        if ($response->headers->has('Content-Type')) {
            /* Get the MIME type and character set */
            preg_match( '@([\w/+]+)(;\s+charset=(\S+))?@i', $response->headers->get('Content-Type'), $matches );
            if ( isset( $matches[1] ) ) {
                $mimeType = $matches[1];
            }
        }
        return $mimeType;
    }

    /**
     * @param Response $response
     * @return string
     */
    public static function getResponseCharset(Response $response) {
        $charset = '';
        if ($response->headers->has('Content-Type')) {
            /* Get the MIME type and character set */
            preg_match( '@([\w/+]+)(;\s+charset=(\S+))?@i', $response->headers->get('Content-Type'), $matches );
            if ( isset( $matches[3] ) ) {
                $charset = $matches[3];
            }
        }
        return $charset;
    }

    /**
     * Try to get the best bet on the mimeTypeClass. In this case the mimeTypeClass is just a classification of the document type like
     * "html", "css" or "js"
     * Hint: If a parameter is given in the url (like /styles.css?version=1) then the server does not send a content-type.
     * In that case we need to guess by the extension.
     *
     * @param Response $response
     * @return string
     */
    public function getResponseMimeTypeClass(Response $response) {
        $app = Application::getInstance();
        $mimeTypeClass = '';
        $mimeType = $app->getResponseMimeType($response);
        if (!empty($mimeType)) {
            if (0 === stripos($mimeType, 'text/html')) {
                $mimeTypeClass = 'html';
            } elseif (0 === stripos($mimeType, 'text/css')) {
                $mimeTypeClass = 'css';
            } elseif (0 === stripos($mimeType, 'application/x-javascript')) {
                $mimeTypeClass = 'js';
            }
        }

        // try to find out by the extension
        if (empty($mimeTypeClass)) {
            $request = $app->getRequest();
            $pathinfo = pathinfo($request->getPathInfo());
            if (!empty($pathinfo['extension'])) {
                $mimeTypeClass = mb_strtolower($pathinfo['extension']);
            }
        }
        return $mimeTypeClass;
    }

    /**
     * @param \AtSearch\Client\ClientInterface $client
     * @return self
     */
    public function setClient($client)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * @return \AtSearch\Client\ClientInterface
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Returns the client by the domain
     *
     * @param string
     * @return \AtSearch\Client\ClientInterface
     */
    public function createClient($host)
    {
        $classname = '';

        // Check if the host is configured
        if (isset($GLOBALS['CONFIG']['CLIENTS'][$host])) {
            $classname = $GLOBALS['CONFIG']['CLIENTS'][$host];
        } else {
            // Check if the subdomain is configured
            $domain = preg_replace('#(.*)\.'.$GLOBALS['CONFIG']['APP_HOSTNAME'].'$#', '$1', $host);
            $domain = preg_replace('#^www\.#', '', mb_strtolower($domain)); // register domains/fqdns without www.
            if (isset($GLOBALS['CONFIG']['CLIENTS'][$domain])) {
                $classname = $GLOBALS['CONFIG']['CLIENTS'][$domain];
            }
        }

        $class = '\\AtSearch\\Client\\' . $classname;

        if (!empty($classname) && class_exists($class))
        {
            /**
             * @var $class \AtSearch\Client\ClientInterface
             */
            return new $class;
        }
        throw new \ErrorException("$host / Client [$class] not found.");
    }

    /**
     * Add filters that every client needs.
     *
     * @param Proxy $proxy
     */
    private function addGlobalFilters(Proxy $proxy) {
        /*
         * add <meta name="robots" content="noindex,nofollow" />
         */
        $html = '<meta name="robots" content="noindex,nofollow" />';
        $filter = new PregReplaceFilter();
        $filter
            ->setPattern('#(<head[^>]*>)#i')
            ->setReplacement('$1' . $html)
            ->setLimit(1)
            ->setFilterMimeTypeClasses(array('html'));
        $proxy->addResponseFilter($filter);

        return;
    }

    /**
     * Log
     */
    public static function log($message)
    {
        error_log($message);
    }

    /**
     * @return Request
     */
    public static function debug($message)
    {
        if (isset($GLOBALS['CONFIG']['DEBUG']) && $GLOBALS['CONFIG']['DEBUG'] == true) {
            self::log($message);
        }
    }
}
