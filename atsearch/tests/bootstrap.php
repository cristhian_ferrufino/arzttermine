<?php
// use autoloader from composer and add src/atsearch/AtSearch (\AtSearch\) and tests/AtSearch (\AtSearch\Tests\)
if ( ! is_file($autoloadFile = __DIR__.'/../vendor/autoload.php'))
{
    throw new \LogicException('Could not find autoload.php in vendor/. Did you run "composer install --dev"?');
}

$loader = require $autoloadFile;

define('SYSTEM_PATH',      dirname(dirname(__FILE__)));
require_once SYSTEM_PATH . '/config/config.php';
