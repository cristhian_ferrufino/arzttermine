<?php
namespace AtSearch\Parser;

use AtSearch\Application\Application;
use AtSearch\Tests\Client\TestClient;

class ParserTest extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        /* the client class is used inside the parser (Client->getUrl()) to distinguish between internal and external links */
        $app = Application::getInstance();
        $client = new TestClient();
        $app->setClient($client);
    }

    /**
     * @test
     */
    public function checks_if_parser_handles_external_links()
    {
        /* this content should be changed by the parser */
        $contentSource = <<<EOF
Text blabla <a href="http://www.domain.com/abc.html">external link</a>,
Text blabla <a href="http://www.test-client.de/abc.html">internal absolute link with domain</a>,
Text blabla <a href="/abc.html">internal absolute link</a>
Text blabla <a href="http://www.test-client.de">internal absolute link with domain only and no ending slash</a>,
Text blabla <a href="../abc.html">internal relative link</a>
EOF;
        /* result should be this */
        $contentTarget = <<<EOF
Text blabla <a href="http://www.domain.com/abc.html">external link</a>,
Text blabla <a href="/abc.html">internal absolute link with domain</a>,
Text blabla <a href="/abc.html">internal absolute link</a>
Text blabla <a href="/">internal absolute link with domain only and no ending slash</a>,
Text blabla <a href="../abc.html">internal relative link</a>
EOF;

        $parser = new Parser();
        $contentSource = $parser->HTMLDocument($contentSource);

        $this->assertTrue($contentSource === $contentTarget);
    }
}
