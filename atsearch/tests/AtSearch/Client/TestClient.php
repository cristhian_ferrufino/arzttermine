<?php namespace AtSearch\Tests\Client;

use Proxy\Proxy;
use AtSearch\Client\ClientInterface;

class TestClient extends \PHPUnit_Framework_TestCase implements ClientInterface {

    protected $url = 'http://www.test-client.de'; // no leading slash

    /**
     * @test
     */
    public function getUrl() {
        return $this->url;
    }

    /**
     * @test
     */
    public function setFilters(Proxy $proxy) {
    }
}
