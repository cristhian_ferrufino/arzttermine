<?php

// Definitions
// =====================================================================================================================

define('DS', DIRECTORY_SEPARATOR);
define('PS', PATH_SEPARATOR);

define('SYSTEM_PATH',      dirname(dirname(__FILE__)));
define('SHARED_PATH',      SYSTEM_PATH . '/../../shared');
define('DISTRIBUTED_PATH', SYSTEM_PATH . '/../../shared/distributed');
define('CACHE_PATH',       SHARED_PATH . '/cache');

// Dependencies
// =====================================================================================================================

global $CONFIG;

require_once SYSTEM_PATH . '/config/config.php';

require_once SYSTEM_PATH . '/vendor/autoload.php';
require_once SYSTEM_PATH . '/vendor/symfony/class-loader/Symfony/Component/ClassLoader/UniversalClassLoader.php';

// System configuration
// =====================================================================================================================

ini_set('gd.jpeg_ignore_warning', 1);
setlocale(LC_MESSAGES, 'de_DE'); // DON'T USE LC_ALL

date_default_timezone_set($CONFIG["SYSTEM_TIMEZONE"]);

// Components
// =====================================================================================================================

// Autoloader
/* **************************************************************************** */

use Symfony\Component\ClassLoader\UniversalClassLoader;

$autoloader = new UniversalClassLoader();
$autoloader->registerNamespaces(
    array(
        'AtSearch'  => SYSTEM_PATH . '/src/atsearch/'
    ));

$autoloader->register();
