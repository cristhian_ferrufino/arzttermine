atsearch
========

Interesting:
https://github.com/jenssegers/php-proxy
https://www.glype.com/ as the major proxy engine.

Make sure to comment out "AddDefaultCharset" on the httpd/apache server.
The server should not set any default charset as it could be that a proxied site has no charset set but only in the html e.g. <meta content="text/html; charset=windows-1252">.
