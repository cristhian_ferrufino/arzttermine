# config valid only for Capistrano 3.2.1
#lock '3.2.1'

# =============================================================================
# REQUIRED VARIABLES
# =============================================================================
# You must always specify the application and repository for every recipe. The
# repository must be the URL of the repository you want this recipe to
# correspond to. The deploy_to path must be the path on each machine that will
# form the root of the application path.

set :application, "atsearch"
set :use_sudo, false


# =============================================================================
# REPOSITORY
# =============================================================================
# To deploy certain branch, otherwise master
# Example: BRANCH=foo cap production deploy
set :scm_verbose, true
set :repo_url, 'git@github.com:arzttermine/atsearch.git'
set :git_shallow_clone, "1"
set :branch, ENV['BRANCH'] || "master"
set :deploy_via, :remote_cache


# =============================================================================
# DEFAULTS
# =============================================================================
# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }

# Default deploy_to directory is /var/www/my_app
# set :deploy_to, '/var/www/my_app'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# set :linked_files, %w{config/database.yml}

# Default value for linked_dirs is []
# set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5
