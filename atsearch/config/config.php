<?php
if (!isset($ROOT)) { $ROOT = realpath($_SERVER["DOCUMENT_ROOT"].'/..'); }
if (!isset($SHARED)) { $SHARED = realpath($_SERVER["DOCUMENT_ROOT"].'/../../../shared'); }

$CONFIG = array(
    "SYSTEM_CHARSET"							=>'UTF-8',
    "SYSTEM_DB_CHARSET"							=>'UTF8',
    "SYSTEM_TIMEZONE"							=>'Europe/Berlin',
    "SYSTEM_SESSION_NAME"						=>'PHPSESSID',
    "SYSTEM_SESSION_MAXLIFETIME"				=>3600,
    "SYSTEM_SESSION_SAVE_PATH"					=>DISTRIBUTED_PATH."/sessions",
);

if ((isset($_ENV['SYSTEM_ENVIRONMENT']) && $_ENV['SYSTEM_ENVIRONMENT'] == 'development') ||	preg_match('/dev/i',$_SERVER['HTTP_HOST'])) {
    ini_set('display_errors',true);
    ini_set('error_reporting', E_ALL);
    $CONFIG["SYSTEM_ENVIRONMENT"] = 'development';
    $CONFIG['APP_HOSTNAME'] = 'atsearch.dev';
    $CONFIG['DEBUG'] = true;

} elseif ((isset($_ENV['SYSTEM_ENVIRONMENT']) && $_ENV['SYSTEM_ENVIRONMENT'] == 'staging') ||	preg_match('/test/i',$_SERVER['HTTP_HOST'])) {
    $CONFIG["SYSTEM_ENVIRONMENT"]='staging';
    $CONFIG['APP_HOSTNAME'] = 'test.atsearch.de';

} else {
    ini_set('display_errors',false);
    $CONFIG["SYSTEM_ENVIRONMENT"] = 'production';
    $CONFIG['APP_HOSTNAME'] = 'atsearch.de';
}

// overwrite local settings ******************************
if (is_file($SHARED.'/config/config.php')) include_once $SHARED.'/config/config.php';
// *******************************************************

// can be a fqdn (prio 1) or just a subdomain which will be extended with .atsearch.de(v) (prio 2)
$CONFIG['CLIENTS'] = array(
    'arthron'                       => 'Arthron',
    'augen-arzt-berlin'             => 'AugenArztBerlin',
    'berliner-zahnarzt-praxis'      => 'BerlinerZahnarztPraxis',
    'casa-dentalis'                 => 'CasaDentalis',
    'cerawhite'                     => 'CeraWhite',
    'cosmetic-dental'               => 'CosmeticDental',
    'die-hautambulanz'              => 'DieHautambulanz',
    'dr-effenberger-berlin'         => 'DrEffenbergerBerlin',
    'dr-muhle'                      => 'DrMuhle',
    'dr-pletz'                      => 'DrPletz',
    'www.dr-pletz'                  => 'DrPletz',
    'schmidt-pich'                  => 'SchmidtPich',
    'master-endodontie-berlin'      => 'MasterEndodontieBerlin',
    'dentaloft'                     => 'Dentaloft',
    'm.dr-effenberger-berlin'       => 'DrEffenbergerBerlinMobile',
    'dr-meine'                      => 'DrMeine',
    'dr-zielasko'                   => 'DrZielasko',
    'implantologe-hamburg'          => 'ImplantologeHamburg',
    'kfo-muellermi'                 => 'KfoMuellermi',
    'kompetenznetz-franken'         => 'KompetenznetzFranken',
    'med-plast'                     => 'MedPlast',
    'mvz-berlin-rudow'              => 'MvzBerlinRudow',
    'palais-dental'                 => 'PalaisDental',
    'physiomed-sued'                => 'PhysiomedSued',
    'special-doctors'               => 'SpecialDoctors',
    'triadent-mitte'                => 'TriadentMitte',
    'z-24'                          => 'Z24',
    'www1.zahnaerzte-leibnizstr.de' => 'ZahnaerzteLeibnizstr',
    'zahnaerzte-leibnizstr'         => 'ZahnaerzteLeibnizstr',
    'zahnaerzte-lessingplatz'       => 'ZahnaerzteLessingplatz',
    'zahnarzt-dornig'               => 'ZahnarztDornig',
    'zahnarzt-suelz'                => 'ZahnarztSuelz',
    'zahnarztpraxis-roman-kogan'    => 'ZahnarztpraxisRomanKogan',
    'zahnzentrum-hoffmeier'         => 'ZahnzentrumHoffmeier',
    'zahnarztfriedrichshain'        => 'Zahnarztfriedrichshain',
    'zahnarztlichterfelde'          => 'Zahnarztlichterfelde',
    'zahnzentrum-in-berlin'         => 'ZahnzentrumInBerlin',
    'zahnarztberlinpotsdam'         => 'ZahnarztBerlinPotsdam',
    'dentalist'                     => 'Dentalist'
);
