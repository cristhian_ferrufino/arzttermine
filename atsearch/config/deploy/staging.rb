server 'web05.arzttermine.de', user: 'deploy', roles: %w[web app db]

set :deploy_to, "/www/test.atsearch.de"

# =============================================================================
# HOOKS
# =============================================================================

# remove all but the last 5 releases
after "deploy", "deploy:cleanup"
# clear the smarty cache
after "deploy:cleanup", "deploy:app:clear_templates_c"
