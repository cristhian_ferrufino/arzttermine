server 'ip-172-31-36-250.eu-central-1.compute.internal', user: 'deploy', roles: %w[web app db]

set :deploy_to, "/usr/share/nginx/atsearch"

set :webserver_user, "www-data"

# =============================================================================
# HOOKS
# =============================================================================

# remove all but the last 5 releases
after "deploy", "deploy:cleanup"
# clear the smarty cache
after "deploy:cleanup", "deploy:app:clear_templates_c"
