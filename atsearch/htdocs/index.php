<?php
require_once '../vendor/autoload.php';

require_once dirname(dirname(__FILE__)) . '/include/bootstrap.php';

use AtSearch\Application\Application;
$app = Application::getInstance();

$app->proxyRequest();

exit;
