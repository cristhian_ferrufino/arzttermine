<?php

namespace AppBundle\Service\Mapper;

use AT\Component\Customer\Transfer\LocationDto;

class LocationMapper extends FromArrayMapper
{
    /**
     * @param $data
     * @return LocationDto
     */
    public function fromArray($data): LocationDto
    {
        $dto = new LocationDto();
        $dto->id = $this->getPropertySafely('id', $data);
        $dto->country = $this->getPropertySafely('country', $data);
        $dto->latitude = $this->getPropertySafely('latitude', $data);
        $dto->longitude = $this->getPropertySafely('longitude', $data);
        $dto->city = $this->getPropertySafely('city', $data);
        $dto->zip = $this->getPropertySafely('zip', $data);
        $dto->location = $this->getPropertySafely('location', $data);
        $dto->contactMail = $this->getPropertySafely('contactMail', $data);
        $dto->phone = $this->getPropertySafely('phone', $data);
        $dto->phoneMobile = $this->getPropertySafely('phoneMobile', $data);

        return $dto;
    }
}
