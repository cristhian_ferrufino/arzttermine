<?php

namespace AppBundle\Service\Mapper;

abstract class FromArrayMapper
{
    protected function getPropertySafely($property, $data) {
        $isObject = is_object($data) || is_array($data);

        if ($isObject && array_key_exists($property, $data)) {
            $value = $data[$property];
            if (is_numeric($value)) {
                return 1* $value;
            } else {
                return $value;
            }
        } else if (!$isObject) {
            return $data;
        }

        return null;
    }
}
