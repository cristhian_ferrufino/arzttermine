<?php

namespace AppBundle\Service\Mapper;

use AT\Component\Customer\Transfer\CustomerDto;
use AT\Component\Customer\Transfer\Request\UpdateCustomerDto;

class CustomerMapper
{
    /**
     * @param CustomerDto $dto
     * @return UpdateCustomerDto
     */
    public function toRequestDto(CustomerDto $dto): UpdateCustomerDto
    {
        $result = new UpdateCustomerDto();
        $result->email = $dto->email;
        $result->firstName = $dto->firstName;
        $result->lastName = $dto->lastName;
        $result->gender = $dto->gender;
        $result->roles = $dto->roles;
        $result->title = $dto->title;
        $result->language = $dto->preferredLanguage;

        return $result;
    }
}
