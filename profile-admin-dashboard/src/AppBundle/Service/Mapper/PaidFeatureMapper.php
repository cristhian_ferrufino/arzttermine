<?php

namespace AppBundle\Service\Mapper;

use AppBundle\Form\Model\PaidFeatures;
use AT\Component\Payment\Transfer\PaidFeatureDto;
use AT\Component\Practice\Transfer\PaidFeatureAssignmentDto;
use AT\Component\Practice\Transfer\PracticeDto;



class PaidFeatureMapper
{

    /**
     * Generates an array of all available paidfeatures configured according to the given practice's current setup.
     * @param PracticeDto $practice
     * @return array
     */
    public function assignPaidFeatures(PracticeDto $practice)
    {
        $paidFeatures = array_map(function ($featureName) use ($practice) {

            $field = new PaidFeatures();
            $field->name = $featureName;

            $existingFeatures = $practice->paidFeatures->filter(function (PaidFeatureAssignmentDto $existing) use ($featureName) {
                return $existing->paidFeatureId == PaidFeatureDto::PAID_FEATURES[$featureName];
            });

            if (count($existingFeatures) > 0) {
                $field->active = true;
                $field->options = $existingFeatures->first()->options;
            }

            return $field;
        }, array_keys(PaidFeatureDto::PAID_FEATURES));

        return $paidFeatures;
    }

    /**
     * Takes an array of paidfeatures of response-dto format and transforms them to request-dto format.
     * @param $paidFeatures
     * @return array
     */
    public function transformPaidFeatures($responsePaidFeatures) {
        $requestPaidFeatures = [];
        foreach($responsePaidFeatures as $value) {
            if ($value->active) {
                $requestPaidFeatures[$value->name] = $value->options;
            }
        }
        return $requestPaidFeatures;
    }
}
