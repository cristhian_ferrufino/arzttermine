<?php

namespace AppBundle\Service;

use AT\Bundle\CustomerBundle\Service\OAuth2TokenStorage;
use AT\Component\Common\PageRequest;
use AT\Component\Common\RequestOptions;
use AT\Component\Customer\CustomerClient;
use AT\Component\Customer\Transfer\CustomerDto;
use AT\Component\Customer\Transfer\CustomersPageDto;
use AT\Component\Customer\Transfer\LocationDto;
use AT\Component\Customer\Transfer\Request\UpdateCustomerDto;
use Doctrine\Common\Cache\Cache;
use GuzzleHttp\Psr7\Request;

class CustomerService
{
    const CUSTOMERS_DETAILS_CACHE_KEY = 'at_dashboard_customer_details';

    /**
     * @var CustomerClient
     */
    private $client;

    /**
     * @var Cache
     */
    private $cache;

    /**
     * @var OAuth2TokenStorage
     */
    private $tokenStorage;

    /**
     * @param CustomerClient $client
     * @param OAuth2TokenStorage $tokenStorage
     * @param Cache $cache
     */
    public function __construct(CustomerClient $client, OAuth2TokenStorage $tokenStorage, Cache $cache)
    {
        $this->client = $client;
        $this->cache = $cache;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param RequestOptions $options
     * @return CustomersPageDto
     */
    public function getCustomers(RequestOptions $options): CustomersPageDto
    {
        $token = $this->tokenStorage->getAccessToken();
        return $this->client->getCustomers($token, $options);
    }

    /**
     * @param int $id
     * @return CustomerDto
     */
    public function getCustomer(int $id): CustomerDto
    {
        $token = $this->tokenStorage->getAccessToken();
        return $this->client->getCustomer($token, $id);
    }

    /**
     * @param int $id
     * @param LocationDto $update
     * @return bool
     */
    public function updateLocation(int $id, LocationDto $update): bool
    {
        $token = $this->tokenStorage->getAccessToken();
        return $this->client->updateLocation($token, $id, $update);
    }

    /**
     * Employees are filtered out
     *
     * @return array [id => 'email']
     */
    public function getCustomersDetails(): array
    {
        $entry = $this->cache->fetch(self::CUSTOMERS_DETAILS_CACHE_KEY);

        if (false === $entry) {
            $options = new RequestOptions();
            $options->setPageRequest(new PageRequest(0, 5000)); // @TODO for now enough, but should definitely change
            $options->addFilter('enabled', 'true');

            $customers = $this->getCustomers($options);
            $entry = [];

            foreach ($customers->elements as $element) {
                if (! in_array(CustomerDto::ROLE_EMPLOYEE, $element->roles)) {
                    $entry[$element->id] = $element->email;
                }
            }

            try {
                $this->cache->save(self::CUSTOMERS_DETAILS_CACHE_KEY, $entry, 7200);
            } catch (\Exception $e) {
                // @TODO logging (write into cache should better fail silently atm)
            }
        }

        return $entry;
    }

    /**
     * @param UpdateCustomerDto $request
     * @return CustomerDto
     */
    public function createCustomer(UpdateCustomerDto $request): CustomerDto
    {
        $result = $this->client->createCustomer($this->tokenStorage->getAccessToken(), $request);

        try {
            $this->cache->delete(self::CUSTOMERS_DETAILS_CACHE_KEY);
        } catch (\Exception $e) {
            // @TODO logging
        }

        return $result;
    }

    /**
     * @param int $id
     * @param UpdateCustomerDto $update
     * @return CustomerDto
     */
    public function updateCustomer(int $id, UpdateCustomerDto $update): CustomerDto
    {
        $result = $this->client->updateCustomer($this->tokenStorage->getAccessToken(), $id, $update);

        try {
            $this->cache->delete(self::CUSTOMERS_DETAILS_CACHE_KEY);
        } catch (\Exception $e) {
            // @TODO logging
        }

        return $result;
    }
}
