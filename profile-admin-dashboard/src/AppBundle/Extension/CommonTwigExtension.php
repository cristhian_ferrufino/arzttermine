<?php

namespace AppBundle\Extension;

use Symfony\Component\HttpFoundation\Request;

class CommonTwigExtension extends \Twig_Extension
{
    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('referrer', [$this, 'getReferrer']),
        ];
    }

    /**
     * @param Request $request
     * @return string
     */
    public function getReferrer(Request $request): string
    {
        return $request->headers->get('referer') ?: '/home';
    }
}
