<?php

namespace AppBundle\Form;

use AppBundle\Form\Model\PaidFeatures;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PaidFeatureAssignmentType extends AbstractType
{
    const PAID_FEATURE_ASSIGNMENT_OPTIONS = [
        'additional_resources' => [
            'limit'
        ]
    ];

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', HiddenType::class, [
                'required' => true,
            ])
            ->add('active', CheckboxType::class)
            ->add('options', CollectionType::class, [
                'label' => false,
                'required' => true,
                'allow_add' => true,
                'allow_delete' => true,
                'entry_type' => TextType::class,
            ]);

        $callable = function (FormEvent $event) {
            /** @var PaidFeatures $paidFeature */
            $paidFeature = $event->getData();

            if (null === $paidFeature) {
                return;
            }

            $event->getForm()->remove('active');
            $event->getForm()->add('active', CheckboxType::class, [
                'label' => PaidFeatures::KEY_TO_ID_MAPPING[$paidFeature->name],
            ]);

            //Derive array of eligible options for current paidfeature
            $eligibleOptions = array_key_exists($paidFeature->name, self::PAID_FEATURE_ASSIGNMENT_OPTIONS) ? self::PAID_FEATURE_ASSIGNMENT_OPTIONS[$paidFeature->name] : [];

            //for each eligibleOption set value if given by current paidFeature, or initialize empty
            $options = [];
            foreach ($eligibleOptions as $key) {
                if (null !== $paidFeature->options && array_key_exists($key, $paidFeature->options)) {
                    $options[$key] = $paidFeature->options[$key];
                } else {
                    $options[$key] = '';
                }
            }

            $event->getForm()->remove('options');
            $event->getForm()->add('options', CollectionType::class, [
                'label' => false,
                'required' => true,
                'allow_add' => true,
                'allow_delete' => true,
                'entry_type' => TextType::class,
                'data' => $options,
            ]);
        };

        $builder->addEventListener(FormEvents::PRE_SET_DATA, $callable);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PaidFeatures::class,
        ]);
    }
}
