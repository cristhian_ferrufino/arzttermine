<?php

namespace AppBundle\Form\Model;

use AT\Component\Practice\Transfer\ReviewSiteDto;
use AT\Component\Practice\Transfer\WidgetConfigurationDto;

class UpdatePracticeRequest
{
    /** @var int only a convenient property to store id, not used */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var ReviewSiteDto[]
     */
    public $reviewSites;

    /**
     * {@link MedicalSpecialtyType}
     *
     * @var string[]|null
     */
    public $medicalSpecialties;

    /**
     * @var WidgetConfigurationDto|null
     */
    public $widgetConfiguration;

    /**
     * @var PaidFeatures[]
     */
    public $paidFeatures;

    /**
     * @var string[]
     */
    public $insuranceTypes;
}
