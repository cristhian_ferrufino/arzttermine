<?php

namespace AppBundle\Form\Model;

class PaidFeatures
{
    const KEY_TO_ID_MAPPING = [
        'sms_booking_confirmation' => 'SMS - Buchungsbestätigung',
        'sms_booking_reminder' => 'SMS - Buchungserinnerungen',
        'sms_review_reminder' => 'SMS - Reviewerinnerung',
        'sms_booking_movement' => 'SMS - Buchungsänderungen',
        'mail_review_reminder' => 'Mail - Reviewerinnerung',
        'online_payments' => 'Online Zahlungsfeatures',
        'booking_customization' => 'eigene Buchungsanpassungen',
        'additional_resources' => 'Mehrere Doktoren',
    ];

    /**
     * @var bool
     */
    public $active;

    /**
     * @var string
     */
    public $name;

    /**
     * @var array<string, string>
     */
    public $options;
}
