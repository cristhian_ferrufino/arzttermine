<?php

namespace AppBundle\Form;

use AT\Component\Customer\Transfer\LocationDto;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Range;

class LocationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', HiddenType::class, [
                'required' => false,
                'label' => false,
            ])
            ->add('location', TextType::class, [
                'required' => true,
                'label' => 'Street / Hsnr.',
                'attr' => [
                    'placeholder' => 'Straße Hsnr.'
                ],
            ])
            ->add('city', TextType::class, [
                'required' => true,
                'attr' => [
                    'placeholder' => 'Stadt'
                ],
            ])
            ->add('zip', IntegerType::class, [
                'required' => true,
                'constraints' => [
                    new Range(['min' => 10000, 'max' => 99999]),
                ],
                'attr' => [
                    'placeholder' => '12345',
                    'step' => 1,
                    'min' => 10000,
                    'max' => 99999,
                ],
            ])
            ->add('contactMail', EmailType::class, [
                'required' => true,
                'attr' => [
                    'placeholder' => 'example@example.com'
                ],
            ])
            ->add('phone', TextType::class, [
                'required' => true,
                'attr' => [
                    'placeholder' => 'Festnetz'
                ],
            ])
            ->add('phoneMobile', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Mobil'
                ],
            ])
            ->add('longitude', TextType::class, [
                'required' => true,
            ])
            ->add('latitude', TextType::class, [
                'required' => true,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => LocationDto::class,
        ]);
    }
}
