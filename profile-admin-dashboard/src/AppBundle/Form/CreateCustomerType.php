<?php

namespace AppBundle\Form;

use AT\Component\Customer\Transfer\CustomerDto;
use AT\Component\Customer\Transfer\Request\UpdateCustomerDto;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreateCustomerType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'required' => true,
            ])
            ->add('firstName', TextType::class, [
                'required' => true,
            ])
            ->add('lastName', TextType::class, [
                'required' => true,
            ])
            ->add('gender', ChoiceType::class, [
                'required' => true,
                'choices' => [
                    'Männlich' => 'MALE',
                    'Weiblich' => 'FEMALE',
                ],
            ])
            ->add('title', TextType::class, [
                'required' => false,
            ])
            ->add('password', PasswordType::class, [
                'required' => true,
            ])
            ->add('roles', ChoiceType::class, [
                'expanded' => true,
                'multiple' => true,
                'required' => false,
                'choices' => [
                    'Mitarbeiter' => CustomerDto::ROLE_EMPLOYEE,
                    'Kunde' => CustomerDto::ROLE_CUSTOMER,
                ],
                'empty_data' => [
                    CustomerDto::ROLE_CUSTOMER,
                ]
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UpdateCustomerDto::class,
        ]);
    }
}
