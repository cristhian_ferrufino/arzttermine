<?php

namespace AppBundle\Form;

use AppBundle\Form\Model\UpdatePracticeRequest;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class UpdatePracticeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', HiddenType::class, [
                'required' => true,
            ])
            ->add('name', TextType::class, [
                'required' => true,
                'constraints' => [
                    new Length(['min' => 5, 'max' => 56]),
                ],
                'attr' => [
                    'placeholder' => 'Praxisname'
                ],
            ])
            ->add('reviewSites', CollectionType::class, [
                'allow_add' => true,
                'allow_delete' => true,
                'entry_type' => ReviewSiteType::class,
                'label' => false,
                'required' => false,
            ])
            ->add('insuranceTypes', ChoiceType::class, [
                'expanded' => true,
                'multiple' => true,
                'choices' => [
                    'Privat' => 'PRIVATE',
                    'Gesetzlich' => 'COMPULSORY'
                ],
                'label' => false,
            ])
            ->add('paidFeatures', CollectionType::class, [
                'entry_type' => PaidFeatureAssignmentType::class,
                'entry_options' => ['label' => false],
                'allow_add' => true,
                'allow_delete' => true,
                'label' => false,
                'required' => false,
            ])
            ->add('widgetConfiguration', WidgetConfigurationType::class, [
                'label' => false,
                'required' => false,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UpdatePracticeRequest::class,
        ]);
    }
}
