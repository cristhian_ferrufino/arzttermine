<?php

namespace AppBundle\Form;

use AppBundle\Form\Model\PaidFeatures;
use AT\Component\Practice\Transfer\Request\CreatePracticeDto;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;

class CreatePracticeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'constraints' => [
                    new Length(['min' => 5, 'max' => 112]),
                ],
                'attr' => [
                    'placeholder' => 'Praxisname'
                ],
            ])
            ->add('location', LocationType::class, [
                'required' => true,
                'label' => false,
            ])
            ->add('reviewSites', CollectionType::class, [
                'allow_add' => true,
                'allow_delete' => true,
                'entry_type' => ReviewSiteType::class,
                'label' => false,
                'required' => false,
            ])
            ->add('insuranceTypes', ChoiceType::class, [
                'expanded' => true,
                'multiple' => true,
                'choices' => [
                    'Privat' => 'PRIVATE',
                    'Gesetzlich' => 'COMPULSORY'
                ],
                'label' => false,
            ])
            ->add('paidFeatures', CollectionType::class, [
                'entry_type' => PaidFeatureAssignmentType::class,
                'entry_options' => ['label' => false],
                'allow_add' => true,
                'allow_delete' => true,
                'label' => false,
                'required' => false,
            ])
            ->add('widgetConfiguration', WidgetConfigurationType::class, [
                'label' => false,
                'required' => false,
            ])
            ->add('initialDoctors', CollectionType::class, [
                'required' => false,
                'allow_add' => true,
                'allow_delete' => true,
                'entry_type' => IntegerType::class,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CreatePracticeDto::class,
        ]);
    }
}
