<?php

namespace AppBundle\Controller;

use AppBundle\Form\CreateCustomerType;
use AppBundle\Form\CreatePracticeType;
use AppBundle\Form\LocationType;
use AppBundle\Form\Model\PaidFeatures;
use AppBundle\Form\Model\UpdatePracticeRequest;
use AppBundle\Form\PaidFeatureAssignmentType;
use AppBundle\Form\UpdatePracticeType;
use AT\Component\Common\Exception\NotFoundException;
use AT\Component\Common\PageRequest;
use AT\Component\Common\RequestOptions;
use AT\Component\Common\SortRequest;
use AT\Component\Common\Transfer\PageDto;
use AT\Component\Payment\Transfer\PaidFeatureDto;
use AT\Component\Practice\Transfer\PaidFeatureAssignmentDto;
use AT\Component\Practice\Transfer\PracticeDto;
use AT\Component\Practice\Transfer\Request\CreatePracticeDto;
use AT\Component\Search\Filter\BasicFilter;
use AT\Component\Search\Filter\PagingFilter;
use AT\Component\Search\Filter\ProjectorFilter;
use AT\Component\Search\Filter\SortFilter;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcher;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Security("has_role('ROLE_EMPLOYEE')")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/home", name="homepage")
     */
    public function indexAction()
    {
        return $this->render('default/index.html.twig', [
        ]);
    }

    /**
     * @Route("/practices", name="practices")
     * @QueryParam(name = "page", nullable = true, description = "page index 0 based")
     *
     * @param ParamFetcher $fetcher
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function practicesAction(ParamFetcher $fetcher)
    {
        $page = new PageDto();
        $page->currentPage = $fetcher->get('page', 0) ?: 0;
        $page->pageSize = 25;

        $filters = [];
        $filters[] = new SortFilter('id');
        $filters[] = new ProjectorFilter('id');
        $filters[] = new ProjectorFilter('name');
        $filters[] = new ProjectorFilter('doctors.id');
        $filters[] = new ProjectorFilter('location.id');
        $filters[] = new ProjectorFilter('location.city');
        $filters[] = new ProjectorFilter('widgetConfiguration');

        $result = $this->get('at_client.practices.search')->findPractices($filters, $page);

        return $this->render(':default/practice:list.html.twig', [
            'title' => 'Praxisübersicht',
            'practices' => $result->elements,
            'page' => $result->page,
        ]);
    }

    /**
     * @Route("/practices/create", name = "create_practice")
     */
    public function createPracticeAction()
    {
        $dto = new CreatePracticeDto();
        $practice = new PracticeDto();
        $practice->paidFeatures = new ArrayCollection();
        $dto->paidFeatures = $this->get('app.mapper.paidFeautures')->assignPaidFeatures($practice);
        $dto->insuranceTypes = [ 'PRIVATE', 'COMPULSORY' ];

        $form = $this->createForm(CreatePracticeType::class, $dto)->createView();

        return $this->render(':default/practice:create.html.twig', [
            'title' => 'Neue Praxis',
            'createPractice' => $form,
        ]);
    }

    /**
     * @Route("/practices/update/{id}", name = "update_practice")
     *
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updatePracticeAction(int $id)
    {
        $token = $this->get('at_customer.token_store')->getAccessToken();

        try {
            $practice = $this->get('at_client.practices')->getPractice($token, $id);
        } catch (NotFoundException $e) {
            return $this->render(':default:404.html.twig');
        }

        $dto = new UpdatePracticeRequest();
        $dto->id = $practice->id;
        $dto->name = $practice->name;
        $dto->medicalSpecialties = $practice->medicalSpecialties;
        $dto->reviewSites = $practice->reviewSites;
        $dto->paidFeatures = $this->get('app.mapper.paidFeautures')->assignPaidFeatures($practice);
        $dto->insuranceTypes = $practice->insuranceTypes  ?: ['COMPULSORY', 'PRIVATE'];

        $form = $this->createForm(UpdatePracticeType::class, $dto)->createView();


        return $this->render(':default/practice:update.html.twig', [
            'title' => sprintf('Praxis #%d bearbeiten', $practice->id),
            'updatePractice' => $form,
            'practice' => $practice,
            'locationId' => $practice->location->id,
        ]);
    }

    /**
     * @Route("/customers", name = "customers")
     * @QueryParam(name = "page", nullable = true, description = "page index 0 based")
     *
     * @param ParamFetcher $fetcher
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function customersAction(ParamFetcher $fetcher)
    {
        $options = new RequestOptions();
        $options->addSortRequest(new SortRequest('id', 'desc'));
        $options->setPageRequest(new PageRequest($fetcher->get('page', 0) ?: 0, 25));

        $token = $this->get('at_customer.token_store')->getAccessToken();
        $customers = $this->get('at_client.customers')->getCustomers($token, $options);

        return $this->render(':default/customer:list.html.twig', [
            'title' => 'Praxismanagement',
            'customers' => $customers->elements,
            'page' => $customers->page,
        ]);
    }

    /**
     * @Route("/customers/create", name = "create_customer")
     */
    public function createCustomerAction()
    {
        return $this->render(':default/customer:create.html.twig', [
            'title' => 'Neuen Mitarbeiter - Kunden - Doktor',
            'createCustomer' => $this->createForm(CreateCustomerType::class)->createView(),
        ]);
    }

    /**
     * @Route("/customers/update/{id}", name = "update_customer")
     *
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateCustomerAction(int $id)
    {
        try {
            $customer = $this->get('app.customers')->getCustomer($id);
        } catch (NotFoundException $e) {
            return $this->render(':default:404.html.twig');
        }

        $formData = $this->get('app.mapper.customers')->toRequestDto($customer);

        return $this->render(':default/customer:update.html.twig', [
            'customer' => $customer,
            'title' => sprintf('User #%d bearbeiten', $id),
            'form' => $this->createForm(CreateCustomerType::class, $formData)->createView(),
        ]);
    }

    /**
     * @Route("/locations/update/{id}", name = "update_location")
     *
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateLocationAction(int $id)
    {
        // there is no endpoint to retrieve locations, therefore use elasticsearch and only allow updates on public locations for now
        // but this is also great for showing related practice details

        $filters = [
            new ProjectorFilter('id'),
            new ProjectorFilter('location'),
            new BasicFilter('location.id', $id),
            new PagingFilter(0, 1),
        ];

        $documents = $this->get('at_client.practices.search')->findPractices($filters);
        $location = null;
        $document = null;

        if (count($documents) > 0) {
            $mapper = $this->get('app.mapper.locations');
            $document = $documents[0];

            $location = $mapper->fromArray($document['location']);
        }

        $form = $this->createForm(LocationType::class, $location)->createView();

        return $this->render(':default/location:create.html.twig', [
            'title' => sprintf('Standort #%d bearbeiten', $id),
            'form' => $form,
            'location' => $location,
            'practiceId' => ($document) ? $document['id'] : null,
        ]);
    }

    /**
     * @Route("/bookings", name = "bookings")
     * @QueryParam(name = "page", nullable = true, description = "page index 0 based")
     *
     * @param ParamFetcher $fetcher
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function bookingsAction(ParamFetcher $fetcher)
    {
        $page = new PageDto();
        $page->currentPage = $fetcher->get('page', 0) ?: 0;
        $page->pageSize = 25;

        $filters = [
            new SortFilter('id', 'desc'),
        ];

        $bookings = $this->get('at_client.bookings.search')->getBookings($page, $filters);

        return $this->render(':default/booking:list.html.twig', [
            'title' => 'Buchungsübersicht',
            'bookings' => $bookings->elements,
            'page' => $bookings->page,
        ]);
    }
}
