<?php

namespace AppBundle\Controller\Api;

use AppBundle\Form\CreatePracticeType;
use AppBundle\Form\Model\PaidFeatures;
use AppBundle\Form\Model\UpdatePracticeRequest;
use AppBundle\Form\UpdatePracticeType;
use AT\Component\Common\Exception\ServiceException;
use AT\Component\Payment\Transfer\PaidFeatureDto;
use AT\Component\Practice\Transfer\PaidFeatureAssignmentDto;
use AT\Component\Practice\Transfer\Request\UpdatePracticeDto;
use Elasticsearch\Endpoints\Update;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Request;

class PracticeController extends FOSRestController
{
    /**
     * @View()
     * @Post("/practices", name = "api_create_practice")
     *
     * @param Request $request
     * @return \FOS\RestBundle\View\View|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function createAction(Request $request)
    {
        $form = $this->createForm(CreatePracticeType::class);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->view(['success' => false, 'errors' => $form->getErrors(true, false)], 400);
        }

        $formData = $form->getData();
        $formData->paidFeatures = $this->get('app.mapper.paidFeautures')->transformPaidFeatures($formData->paidFeatures);

        $client = $this->get('at_client.practices');

        try {
            $token = $this->get('at_customer.token_store')->getAccessToken();
            $client->createPractice($token, $formData);
        } catch (ServiceException $e) {
            return $this->view(['success' => false, 'errors' => $e->getErrors()], $e->getStatus());
        }

        return $this->view(null, 204);
    }

    /**
     * @View()
     * @Put("/practices", name = "api_update_practice")
     *
     * @param Request $request
     * @return \FOS\RestBundle\View\View|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function updateAction(Request $request)
    {
        $form = $this->createForm(UpdatePracticeType::class);
        $form->submit($request->request->get($form->getName()));

        //Map form to dto format
        $formData = $form->getData();
        $dto = new UpdatePracticeDto();
        $dto->id = $formData->id;
        $dto->name = $formData->name;
        $dto->reviewSites = $formData->reviewSites;
        $dto->medicalSpecialties = $formData->medicalSpecialties;
        $dto->widgetConfiguration = $formData->widgetConfiguration;
        $dto->paidFeatures = $this->get('app.mapper.paidFeautures')->transformPaidFeatures($formData->paidFeatures);
        $dto->insuranceTypes = $formData->insuranceTypes;

        $client = $this->get('at_client.practices');

        try {
            $token = $this->get('at_customer.token_store')->getAccessToken();
            $client->updatePractice($token, $form->getData()->id, $dto);
        } catch (ServiceException $e) {
            return $this->view(['success' => false, 'errors' => $e->getErrors()], $e->getStatus());
        }

        return $this->view(null, 204);
    }

}
