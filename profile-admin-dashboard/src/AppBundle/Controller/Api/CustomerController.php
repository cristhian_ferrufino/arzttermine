<?php

namespace AppBundle\Controller\Api;

use AppBundle\Form\CreateCustomerType;
use AppBundle\Form\LocationType;
use AT\Component\Common\Exception\ServiceException;
use AT\Component\Common\PageRequest;
use AT\Component\Common\RequestOptions;
use AT\Component\Common\SortRequest;
use AT\Component\Search\Filter\BasicFilter;
use AT\Component\Search\Filter\PagingFilter;
use AT\Component\Search\Filter\ProjectorFilter;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Request;

class CustomerController extends FOSRestController
{
    /**
     * @View()
     * @Post("/customers", name = "api_create_customer")
     *
     * @param Request $request
     * @return \FOS\RestBundle\View\View|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function createAction(Request $request)
    {
        $form = $this->createForm(CreateCustomerType::class);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->view(['success' => false, 'errors' => $form->getErrors(true, false)], 400);
        }

        $customer = null;

        try {
            $customer = $this->get('app.customers')->createCustomer($form->getData());
        } catch (ServiceException $e) {
            return $this->view(['success' => false, 'errors' => $e->getErrors()], $e->getStatus());
        }

        return $this->view(['success' => true, 'customerName' => $customer->fullName]);
    }

    /**
     * @View()
     * @Put("/customers/{id}", name = "api_update_customer")
     *
     * @param int $id
     * @param Request $request
     * @return \FOS\RestBundle\View\View|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function updateAction(int $id, Request $request)
    {
        $form = $this->createForm(CreateCustomerType::class);
        $form->submit($request->request->get($form->getName()));

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->view(['success' => false, 'errors' => $form->getErrors(true, false)], 400);
        }

        $customer = null;

        try {
            $customer = $this->get('app.customers')->updateCustomer($id, $form->getData());
        } catch (ServiceException $e) {
            return $this->view(['success' => false, 'errors' => $e->getErrors()], $e->getStatus());
        }

        return $this->view(['success' => true, 'customerName' => $customer->fullName]);
    }

    /**
     * @View()
     * @Get("/customers", name = "api_get_customers")
     * @QueryParam(name = "email", nullable = true, description = "Filters by email")
     *
     * @param ParamFetcher $fetcher
     * @return \FOS\RestBundle\View\View
     */
    public function getCustomers(ParamFetcher $fetcher)
    {
        $options = new RequestOptions();
        $options->addFilter('enabled', '1');
        $options->addSortRequest(new SortRequest('createdAt'));
        $options->setPageRequest(new PageRequest(0, 25));

        $email = $fetcher->get('email', null);
        if (!empty($email)) {
            $options->addFilter('email', $email);
        }

        return $this->view($this->get('app.customers')->getCustomers($options)->elements);
    }

    /**
     * @View()
     * @Put("/locations/{id}", name = "api_update_location")
     *
     * @param int $id
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function updateLocation(int $id, Request $request)
    {
        $form = $this->createForm(LocationType::class);
        $form->submit($request->request->get($form->getName()));

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->view(['success' => false, 'errors' => $form->getErrors(), 'submitted' => $form->isSubmitted()], 400);
        }

        try {
            $this->get('app.customers')->updateLocation($id, $form->getData());
        } catch (ServiceException $e) {
            return $this->view(['success' => false, 'errors' => $e->getErrors()], $e->getStatus());
        }

        return $this->view(['success' => true], 200);
    }
}
